/* ###################################################################
**     Filename    : main.c
**     Processor   : S32K1xx
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */


/* Including necessary module. Cpu.h contains other modules needed for compiling.*/

/* User includes (#include below this line is not maintained by Processor Expert) */
#include "includes.h"			/* include peripheral declarations S32K144 */
#include "SystemScheduler.h"
#include "Diag/dtc_mgr.h"
#include "CanNm_Cfg.h"

#ifdef SI_LABS_TUNER
#include "sg_defines.h"
#include "main.h"
#endif
extern void VectorIsrDeInit(void);
extern volatile uint32 nm_start_timer_tick;
extern volatile uint32 ig_start_timer_tick;

volatile int exit_code = 0;
SECTOR_STAT_T Self_BootSectorStat;
//volatile DWORD __attribute__((section (".bootJumpStat"))) BootJumpStatus;
//SECTOR_STAT_T __attribute__((section (".bootStatus"))) SectorStatInitVal;
SECTOR_STAT_T __attribute__((section (".bootStatus"))) SectorStatInitVal={(DWORD)APP_OK, (DWORD)APP_MODE};

DEVICE r_Device;
ENCODER r_Encoder;
uint8 r_sonysound=1;
uint8 StartDefaultSet;

volatile uint32_t nm_task_tick=0;

static void jmain(void);
void CAN_Il_Task(void);
void JumpToUserApplication(DWORD userSP, DWORD userStartup);
static void SysTick_Handle(void);
static void Systick_Config(uint32_t TickFrq);
static void Systick_Disable(void);

void INIT_SYSTEM(void);
void INIT_GR_DATA(void);

void JumpToUserApplication(DWORD userSP, DWORD userStartup)
{
	/* Check if Entry address is erased and return if erased */

//	DPRINTF(DBG_MSG1,"\t JumpToUserApplication : %x %x\n\r",userSP,userStartup);
	
	if(userSP == 0xFFFFFFFF){
		return;
	}

	/* Set up stack pointer */
	__asm("msr msp, r0");
	__asm("msr psp, r0");

	/* Jump to application PC (r1) */
	__asm("mov pc, r1");
	for (;;)
	{
	}
}



/*! 
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */
    jmain();

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

volatile uint32_t check_tick=0;
uint32 Reset_SRS=0;

static void jmain(void)
{
	ak7739_priv *ak7739;
	static uint8  u8StartFlag = 0u;
	static uint32 u32TimeTick = 0u;
	Reset_SRS=RCM->SRS;

	INIT_SYSTEM();
	INIT_GR_DATA();
	//DSP_Init_GEQ();
	nvic_init_fnc();
	INIT_FLASH_DATA();	
	SelfFlashInit();
	ak7739 = malloc(sizeof(ak7739_priv));

	#if 0 //def SI_LABS_TUNER
	//initialize tuner
	stTuner.prc =TUNER_PRC_NONE;
	stTuner.task =TUNER_TASK_NONE;
	#endif

	BootStatusFlashBlock_init();
	BootStatusFlashBlock_getStatus(&Self_BootSectorStat);
	

	r_Device.r_System->s_state = SYS_CHECK_DELAY_0;

	#if 1 //read flash
	flash_read_func(FLASH_DATA0_ADDR, &e2p_save_data, sizeof(DATA_SAVE));
	delay_ms(1);
	flash_read_func(FLASH_DATA1_ADDR, &e2p_sys_data, sizeof(E2P_SYSTEM));
	#endif
	
	#ifdef CX62C_FUNCTION
	DPRINTF(DBG_INFO,"System is inialized V%d.%d.%d %s %s %x \n\r", Version_1,Version_2,Version_3,__DATE__,__TIME__,Reset_SRS);
	#else
	DPRINTF(DBG_INFO,"System is inialized V65.00.07 - 210202 %d\n\r",Check_HW_PCB_Ver());
	#endif
	if (0x400==Reset_SRS)
	{
		CanRestoreDefVal();
	}
	//vector function
	CclInitPowerOn();
	//CclSet_Com_Request0();
	CanChangeDefValAfterReset();
	VectorIsrInit();
	//rDiagCmd.diag_cmd =DIA_CMD_INIT;
	r_Device.r_System->ccl_active = OFF;
	r_Device.r_System->prev_ccl_active = OFF;
	r_Device.r_System->can_wakeup = 0;

	StartDefaultSet = 1;
	ProcessReset();

	while(1)
	{	
		#if 1
		#ifdef WATCHDOG_ENABLE
		WDOG_DRV_Trigger(INST_WATCHDOG1);
		#endif
		CAN_Il_Task();
		System_Proc(&r_Device);							//SYSTEM
		Power_Proc(&r_Device);							//POWER 
		ak7739_proc(&r_Device, ak7739);	
		#ifdef INTERNAL_AMP
		AMP_proc(&r_Device);
		#endif
		//TEF6686_Proc(&r_Device);						//TEF6686	TUNER
		UART_Comm_Proc(&r_Device);				//CPU-MCU PROTOCOL
		SYS_LOOP_MGR();
		WDOG_DRV_Trigger(INST_WATCHDOG1);
		Serivce_Proc(&r_Device);
		#if 0 //def SI_LABS_TUNER
		TunerProcess();
		#endif
		
		if(2<=GetElapsedTime(nm_task_tick))
		{
			//upgarde
			if(r_Device.r_System->mcu_update == ON)
			{
				r_Device.r_System->mcu_update = OFF;
				#ifdef WATCHDOG_ENABLE
				WDOG_DRV_Deinit(INST_WATCHDOG1);
				#endif
				Systick_Disable();
				INT_SYS_DisableIRQ(PORTA_IRQn);
				INT_SYS_DisableIRQ(PORTB_IRQn);
				INT_SYS_DisableIRQ(PORTC_IRQn);
				VectorIsrDeInit();/*disable can interrupt*/
				uart0_deinit();
				uart1_deinit();
				Disable_ADC0_Ch();
				Disable_ADC1_Ch();
				spi0_deinit();
				spi1_deinit();
				i2c_deinit();
				flexio_i2c_deinit();
				INT_SYS_DisableIRQGlobal();
				JumpToUserApplication(*((DWORD*)BOOT_ROM_START_ADDR), *((DWORD*)(BOOT_ROM_START_ADDR + 4u)));
			}
			check_tick = GetTickCount();
		}
		//vector function
//		CclScheduleTask();
//		CclPollingTask();

		/* wl20220809 */
		if((nm_wakeup_flag == ON) && (20u <= GetElapsedTime(nm_start_timer_tick)))
		{
			#ifdef VECTOR_PASSIVE_WAKEUP
			MCU_CAN0_EN(ON);
			MCU_CAN0_STB(ON);
			r_Device.r_System->can_wakeup = ON;  //220519
			DPRINTF(DBG_INFO,"CAN0 Err nm_wakeup_flag\n\r");
			#ifdef CHERY_VECTOR_SPEC1
			CclInitPowerOn();
			#else
			CclCanWakeUpInt();  //220519
			#endif
			#endif
			nm_wakeup_flag = OFF;
		}
		/* wl20220810 */
		if((ignacc_wakeup_flag == ON) && (50u <= GetElapsedTime(ig_start_timer_tick)))
		{
			CanNm_TxMessageData[0][1] |= 0x10;
			r_Device.r_System->ccl_active = ON;
			ignacc_wakeup_flag =	OFF;
		}


		nm_task_tick = GetTickCount();

		/* wl20220803 read NVM data after init */
		NvmDataInit();

		#else
		asm (" nop");
		#endif
	}
}

//START_FUNCTION_DEFINITION_RAMSECTION
#define    SYSTEM_CORE_FREQ			80000000u

extern void StdTimerTick(void);
void CAN_Il_Task(void)
{

	static uint32_t CanTaskTime = 0;

	if(10<=GetElapsedTime(CanTaskTime))
	{
		CanTaskTime = GetTickCount();

		CclScheduleTask();
	}

}
static void SysTick_Handle(void)
{

//  powerProcTimerCnt++;
//	PTE->PTOR |= 1<<0;   	//TEST CLK
	timertick_1ms ++;
	StdTimerTick();
}

//END_FUNCTION_DEFINITION_RAMSECTION
static void Systick_Config(uint32_t TickFrq)
{
    S32_SysTick->RVR = S32_SysTick_RVR_RELOAD(SYSTEM_CORE_FREQ / TickFrq) - 1u;
    S32_SysTick->CVR = 0;
    S32_SysTick->CSR = S32_SysTick_CSR_ENABLE(1u) | S32_SysTick_CSR_TICKINT(1u) | S32_SysTick_CSR_CLKSOURCE(1u);
}
static void Systick_Disable(void)
{
    S32_SysTick->CSR = 0x00u;
}

void INIT_SYSTEM(void)
{
	CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
									 g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
	CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);
	 /* 128khz lpo, CNT=64000 (timeout500ms) */
	#ifdef WATCHDOG_ENABLE
	WDOG_DRV_Init(INST_WATCHDOG1, &watchdog1_Config0);
	#endif

#if 0
//	INT_SYS_InstallHandler(SysTick_IRQn, SysTick_Handle, NULL);
//	Systick_Config(1000u);
	/* Enaable interrupts */
//	INT_SYS_EnableIRQGlobal();
#endif
	EDMA_DRV_Init(&dmaController1_State, &dmaController1_InitConfig0, edmaChnStateArray, edmaChnConfigArray, EDMA_CONFIGURED_CHANNELS_COUNT);
	//CclInit();
	//InitIfActiveFlags();
	VectorIsrInit();
	
//	IlInitPowerOn();
//	CclComStart();

	//CclInitPowerOn();
	//CclSet_Com_Request0();


	Uart_Init_Comm();
	//CAN_Init_Comm();//JANG_211116
	DSP_Init_GEQ();
	ak7739_Init_Comm();
	#ifdef CX62C_FUNCTION
	//CANTp_Init_data();
	#endif
	TEF6686_Init_Comm();
	rrm_tx_cmd_Init();
	sys_init_fnc();
	CanInitPowerOn();
}

void INIT_GR_DATA(void)
{
//ÃÊ±â°ª Ã¼Å©ÇÊ¿ä...
	static TEF6686 init_tuner={0,};
	static SYSTEM init_system={0,};
	static POWER init_power={0,};
	static AK7739 init_dsp={0,};
	static CPU_MSG init_sendcpu={0,};
	static BEEP init_beep={0,};
	static A2B init_A2B={0,};

	static UART_JYI_TEST init_UART_JYI_TEST = {0,};	// only for test

	r_Device.X50_UART = &init_UART_JYI_TEST;	// only for test

	//initialize data
	r_Device.r_Tuner = &init_tuner;
	r_Device.r_System = &init_system;
	r_Device.r_Power = &init_power;
	r_Device.r_Dsp = &init_dsp;
	r_Device.r_SendCPU = &init_sendcpu;
	r_Device.r_Beep = &init_beep;
	r_Device.r_A2B = &init_A2B;

	r_Encoder.encoder= 0x00;
	r_Encoder.oldstatus = 0x00;
	r_Encoder.laststatus = J_ENCODER_STOP;
	r_Encoder.direction=	J_ENCODER_STOP;
	r_Encoder.enable=1;
//	r_Encoder.code=0xa0;
	r_Encoder.txed=FALSE;
	r_Encoder.ack=FALSE;
	r_Encoder.retry=0;
	r_Encoder.click=0;
	r_Encoder.needstable=OFF;
	r_Device.r_LeftEncoder = &r_Encoder;

	r_Device.r_Power->p_state= PWR_IDLE;
	r_Device.r_Tuner->t_state = TUNER_IDLE;
	r_Device.r_Dsp->d_state = DSP_IDLE;
	r_Device.r_A2B->a_state =A2B_IDLE;
	r_Device.r_System->s_state = SYS_STANDBY_MODE_ENTER;
	r_Device.r_System->amp_state = INTERNAL_AMP_IDLE;
	r_Device.r_System->init_ok = OFF;
	r_Device.r_System->lcd_ok= 0;
	r_Device.r_System->boot_seq1= 0;
	r_Device.r_System->gear_status= ON;		//gear is "p"
	r_Device.r_System->s_state_lcd_state = SYS_IDLE_MODE;
	r_Device.r_System->s_state_prev_lcd_state = SYS_IDLE_MODE;
	#ifdef VECTOR_CCL_TEST
	r_Device.r_System->usrdata1 = 0;
	r_Device.r_System->wakup_userdata =0;
	#endif

	//clear ps_hold,update 
	r_Device.r_System->soc_update= OFF;
	#ifdef QFI_MODE_SEQ_CHANGE
	r_Device.r_System->soc_update_excute= OFF;
	#endif
	r_Device.r_System->pshold =OFF;	
	r_Device.r_System->soc_monitor =0;
	r_Device.r_System->alivecleartime = OFF;		//alive clear
	r_Device.r_System->alivesignal = OFF;
	r_Device.r_System->canmn_set_standby= OFF;
	r_Device.r_System->gps_time.enable = OFF;

	r_Device.r_System->no_response_active=0;
	r_Device.r_System->no_response_screenoff=0;
	r_Device.r_System->no_response_15min=0;
	r_Device.r_System->no_response_screensaver=0;

	r_Device.r_Dsp->d_boot_ok= OFF;
	r_Device.r_Dsp->d_state_lock = OFF;
	r_Device.r_Beep->beep_active = OFF;
	r_Device.r_Beep->beep_pre_active = OFF;
	r_Device.r_Dsp->d_mix_status = OFF;

	r_Device.r_Dsp->d_prev_audo_mode = 0;
	r_Device.r_Dsp->d_audo_mode = 0;
	#ifdef CHANG_BL_TIMING
	r_Device.r_System->blpwmenable =OFF;
	#endif
	QFIModeState = QFI_MODE_IDLE;	
	#ifdef SVCD_ENABLE
	r_Device.r_System->svcd_speed = 0;	//clear speed
	r_Device.r_System->prev_svcd_speed = 0;	//clear speed
	r_Device.r_System->prev_svcd_e2p = e2p_save_data.E2P_SVCD;
	#endif

	r_Device.r_A2B->a_init_cnt = 0;	

	check_tick = GetTickCount();
	nm_task_tick = GetTickCount();
}

extern void DIAG_ReadDataRESETINFOFromNVM(void);
extern void WriteDataResetInfoToNVM(uint8* u8ErrCode);
extern uint8 u8ErrCode[];
extern uint16 Version_1;

void ProcessReset(void)
{
	uint8* pu8ErrLog=u8ErrCode;
	const uint32 ru32Mask=0x2E3E;
	uint32 u32Val;
	if ((Reset_SRS!=0x82)&&(Reset_SRS&ru32Mask)!=0u)
	{
		DIAG_ReadDataRESETINFOFromNVM();
		u32Val=RCM->SRS;
		pu8ErrLog[0]=((uint8)(u32Val>>24)&0xFF);
		pu8ErrLog[1]=((uint8)(u32Val>>16)&0xFF);
		pu8ErrLog[2]=((uint8)(u32Val>>8)&0xFF);
		pu8ErrLog[3]=((uint8)(u32Val   )&0xFF);

		u32Val=RCM->SSRS;
		pu8ErrLog[4]=((uint8)(u32Val>>24)&0xFF);
		pu8ErrLog[5]=((uint8)(u32Val>>16)&0xFF);
		pu8ErrLog[6]=((uint8)(u32Val>>8)&0xFF);
		pu8ErrLog[7]=((uint8)(u32Val   )&0xFF);

		pu8ErrLog[12]++;
		WriteDataResetInfoToNVM(pu8ErrLog);
	}
}

void RecordReset(uint8 u8Identifier,uint8* pu8UserInfo)
{
	uint8* pu8ErrLog=u8ErrCode;
	uint32 u32tick = GetTickCount();
	DIAG_ReadDataRESETINFOFromNVM();
	pu8ErrLog[19]=r_Device.r_Power->p_state; //0x18 PWR_ON_ACTIVE
	pu8ErrLog[20]=r_Device.r_System->s_state; //0x07 SYS_STANDBY_MODE_IDLE
	pu8ErrLog[21]=r_Device.r_System->amp_state; //0x00 INTERNAL_AMP_IDLE
	pu8ErrLog[22]=r_Device.r_System->s_state_lcd_state; //0x07 SYS_STANDBY_MODE_IDLE
	pu8ErrLog[23]=r_Device.r_Dsp->d_state; //0x08 DSP_INIT_7
	pu8ErrLog[24]=r_Device.r_Dsp->d_state_lock; //0x00

	pu8ErrLog[28]=((uint8)(u32tick>>24)&0xFF); //0x012ef2
	pu8ErrLog[29]=((uint8)(u32tick>>16)&0xFF);
	pu8ErrLog[30]=((uint8)(u32tick>>8)&0xFF);
	pu8ErrLog[31]=((uint8)(u32tick   )&0xFF);

	if (NULL!=pu8UserInfo)
	{
		for (uint8 i=0;i<8;i++)
		{
			pu8ErrLog[35+i]=*(pu8UserInfo);
			pu8UserInfo++;
		}
	}

	pu8ErrLog[43]=u8Identifier;
	pu8ErrLog[44]++;
	pu8ErrLog[45]=(uint8)Version_1;
	WriteDataResetInfoToNVM(pu8ErrLog);
}

void EraseRecord(void)
{
	uint8* pu8ErrLog=u8ErrCode;
	for (uint8 i=0;i<64;i++)
	{
		pu8ErrLog[i]=0;
	}
	WriteDataResetInfoToNVM(pu8ErrLog);
}

/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the NXP S32K series of microcontrollers.
**
** ###################################################################
*/
