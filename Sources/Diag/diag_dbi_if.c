/*
 * diag_dbi_if.c
 *
 *  Created on: 2020. 6. 10.
 *      Author: Digen
 */

/******************************************************************************
* INCLUDE FILES
******************************************************************************/
#include "diag_dbi_if.h"
#include "diag_dbi.h"
#include "../Service/std_util.h"
#include "includes.h"

extern DATA_SAVE e2p_save_data;
extern uint8 u8ErrCode[];
extern uint8 gDiagHardReset;
/******************************************************************************
* CONSTANT DEFINITIONS
******************************************************************************/

/******************************************************************************
* MACRO DEFINITIONS
******************************************************************************/

/******************************************************************************
* TYPE DEFINITIONS
******************************************************************************/

/******************************************************************************
* GLOBAL VARIABLE PROTOTYPES
******************************************************************************/
static uint8 u8IsFound=0u,u8Matched=0u;
static uint8 radioAntennaSignalStrength = 0x0A;
static uint8 sourceMode = 0x02;
static uint8 volume = 0x10;
static uint8 bassStep = 0x07;
static uint8 trebleStep = 0x06;
static uint8 balanceStep = 0x11;
static uint8 faderStep = 0xAA;
static uint8 eqMode = 0x05;
static uint8 batteryVoltage = 0x0F;
static uint8 microphonenum = 0;
#if 1
      	uint8 softwareConfiguration[DIAGDBI_SOFTWARE_CONFIGURATION_LEN] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
      													0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
      													0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
#else
		uint8 softwareConfiguration[DIAGDBI_SOFTWARE_CONFIGURATION_LEN] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
#endif
static uint8 loudspeakerFLcontrol = 0x02;
static uint8 loudspeakerFRcontrol = 0x02;
static uint8 loudspeakerRLcontrol = 0x02;
static uint8 loudspeakerRRcontrol = 0x02;
static uint8 displayControl = 0x02;
//static uint8 volumeControl;
static uint8 radioSeek = 0x03;
static uint8 onAnimation = 0x02;
static uint8 VolumeControl = 0x02;

#ifdef CX62C_FUNCTION
uint8 manufacture_maxim=0;
#endif

#ifdef INTERNAL_AMP
#ifdef INTERNAL_APA
#ifdef CX62C_FUNCTION
//const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MD"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
/* wl20220730 */
const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "J72-7911071ND"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
const uint8 vehicleManufactSparePartNum1[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011ME"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
#else
//static const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MC"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MC"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
#endif
#else
#ifdef CX62C_FUNCTION
const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MD"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
const uint8 vehicleManufactSparePartNum1[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011ME"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
#else
//static const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MA"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MA"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
#endif
#endif
#else
//default APA
#ifdef CX62C_FUNCTION
const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MD"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
const uint8 vehicleManufactSparePartNum1[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011ME"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
#else
//static const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MB"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1] = "F18-7930011MB"; // {'S', 'P', 'A', 'R', 'E', 'P', 'A', 'R', 'T', 'N', 'U', 'M', 'L'}
#endif
#endif
// static const uint8 vehicleManufactECUSWNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_SOFTWARE_NUM_LEN] = {'C', 'H', 'E', 'R', 'Y', 'R', 'R', 'M'};
/* wl20220730 */
static const uint8 vehicleManufactECUSWNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_SOFTWARE_NUM_LEN] = "SH52.BP.22.01.01";//{'2', '2', '.', '0', '1', '.', '0', '0'};
//static const uint8 systemSupplierId[DIAGDBI_SYSTEM_SUPPLIER_ID_LEN + 1] = "7MP";
const uint8 systemSupplierId[DIAGDBI_SYSTEM_SUPPLIER_ID_LEN + 1] = "7MP";

#if  1
#define TOTAL_OF_CUSTOM_VIN  2
const uint8 rau8Custom_VINS[TOTAL_OF_CUSTOM_VIN][DIAGDBI_VIM_LEN] ={
	{'L', 'U', 'R', 'J', 'A', 'V', 'E', 'X', '9', 'N', 'A', '0', '3', '8', '3', '1', '7'},
	{'L', 'U', 'R', 'J', 'A', 'V', 'E', 'S', '7', 'N', 'A', '0', '3', '6', '1', '9', '6'}
};
uint8 u8Vin[DIAGDBI_VIM_LEN] = {'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'};
#else
			uint8 vin[DIAGDBI_VIM_LEN] = {'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F','F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'};
#endif
			uint8 u8VariantCoding[DIAGDBI_IHUVariantCoding_LEN] = {0u};

#ifdef CX62C_FUNCTION
const uint8 vehicleManufactECUHwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN + 1] = "H01.00.00   "; // {'E', 'C', 'U', '-', 'H', 'W', 'N', 'U', 'M', 'B'}    //JANG_210927
// const uint8 systemSupplierECUSWNum [DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN + 1] = "01.07.00  "; // {'E', 'C', 'U', '-', 'S', 'W', 'N', 'U', 'M', 'B'} //JANG_220308
/* wl20220730 */
const uint8 systemSupplierECUSWNum [DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN + 1] = "BP220101  ";
#else
//static const uint8 vehicleManufactECUHwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN + 1] = "4.0.0     "; // {'E', 'C', 'U', '-', 'H', 'W', 'N', 'U', 'M', 'B'}
//static const uint8 systemSupplierECUSWNum [DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN + 1] = "01.04.00  "; // {'E', 'C', 'U', '-', 'S', 'W', 'N', 'U', 'M', 'B'}
const uint8 vehicleManufactECUHwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN + 1] = "4.0.0     "; // {'E', 'C', 'U', '-', 'H', 'W', 'N', 'U', 'M', 'B'}
const uint8 systemSupplierECUSWNum [DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN + 1] = "01.04.00  "; // {'E', 'C', 'U', '-', 'S', 'W', 'N', 'U', 'M', 'B'}
#endif
static uint8 originVolume;

static uint8 originLoudspeakerFLcontrol;
static uint8 originLoudspeakerFRcontrol;
static uint8 originLoudspeakerRLcontrol;
static uint8 originLoudspeakerRRcontrol;
static uint8 originDisplayControl;
static uint8 originRadioSeek;
static uint8 originOnAnimation;
static uint8 originVolumeControl;

static uint8 ioVolumeCtrlFlg;
static uint8 ioLoudspeakerFLcontrolFlg;
static uint8 ioLoudspeakerFRcontrolFlg;
static uint8 ioLoudspeakerRLcontrolFlg;
static uint8 ioLoudspeakerRRcontrolFlg;
static uint8 ioDisplayControlFlg;
static uint8 ioRadioSeekFlg;
static uint8 ioOnAnimationFlg;
static uint8 ioVolumeControlFlg;

DiagIoCtrlCmd rDiagCmd;

/******************************************************************************
* FUNCTION PROTOTYPES
******************************************************************************/

/******************************************************************************
* FUNCTION 
******************************************************************************/

// #define BAT_ADC_6V_LEVEL   455 //423~4451
// #define BAT_ADC_9V_LEVEL   705 //690~705
// #define BAT_ADC_16V_LEVEL 1465 //1400->1460 //1393~1406

#define VOL_VAL 0
#define VOL_MIN	1
#define VOL_MAX 2
uint32 VoltageTable[][3] = {
/* VOL_VAL,VOL_MIN,VOL_MAX */
	{  0u,    0u,   18u},
	{  5u,   18u,   54u},
	{ 10u,   54u,   90u},
	{ 15u,   90u,  126u},
	{ 20u,  126u,  162u},
	{ 25u,  162u,  198u},
	{ 30u,  198u,  235u},
	{ 35u,  235u,  271u},
	{ 40u,  271u,  307u},
	{ 45u,  307u,  343u},
	{ 50u,  343u,  379u},
	{ 55u,  379u,  415u},
	{ 60u,  415u,  455u},
	{ 65u,  455u,  499u},
	{ 70u,  499u,  543u},
	{ 75u,  543u,  587u},
	{ 80u,  587u,  631u},
	{ 85u,  631u,  675u},
	{ 90u,  675u,  723u},
	{ 95u,  723u,  775u},
	{100u,  775u,  828u},
	{105u,  828u,  880u},
	{110u,  880u,  932u},
	{115u,  932u,  985u},
	{120u,  985u, 1037u},
	{125u, 1037u, 1089u},
	{130u, 1089u, 1142u},
	{135u, 1142u, 1194u},
	{140u, 1194u, 1246u},
	{145u, 1246u, 1299u},
	{150u, 1299u, 1351u},
	{155u, 1351u, 1403u},
	{160u, 1403u, 1460u},
	{165u, 1460u, 1521u},
	{170u, 1521u, 1582u},
	{175u, 1582u, 1643u},
	{180u, 1643u, 1704u},
	{185u, 1704u, 1765u},
	{190u, 1765u, 1826u},
	{195u, 1826u, 1887u},
	{200u, 1887u, 1948u},
	{205u, 1948u, 2009u},
	{210u, 2009u, 2070u},
	{215u, 2070u, 2131u},
	{220u, 2131u, 2192u},
	{225u, 2192u, 2253u},
	{230u, 2253u, 2314u},
	{235u, 2314u, 2375u},
	{240u, 2375u, 2436u},
	{245u, 2436u, 2497u},
	{250u, 2497u, 2558u},
	{255u, 2558u, 2619u}
};

void DIAG_WriteBatteryVoltage(uint32 value)
{
	uint16 i;

	for (i = 0; i < sizeof(VoltageTable) / 3; i++)
	{
		if (VoltageTable[i][VOL_MIN] <= value && value <= VoltageTable[i][VOL_MAX]) break;
	}
	
	batteryVoltage = VoltageTable[i][VOL_VAL];
}

void DIAG_WriteAntennaSignalStrength(uint16 value)
{
	uint8	tmp=0;

	tmp = (uint8)(value/10);

	if(90<=tmp)
		tmp =90;
		
	radioAntennaSignalStrength = tmp;
}
void DIAG_WriteSourceMode(uint8 value)
{
	sourceMode = value;
}
void DIAG_WriteVolume(uint8 value)
{
	volume = value;
}
void DIAG_WriteVolumeControl(uint8 value)
{
	volume = value;
}
void DIAG_WriteBassStep(uint8 value)
{
	bassStep = value;
}
void DIAG_WriteTrebleStep(uint8 value)
{
	trebleStep = value;
}
void DIAG_WriteFaderStep(uint8 value)
{
	faderStep = value;
}
void DIAG_WriteBalanceStep(uint8 value)
{
	balanceStep = value;
}
void DIAG_WriteEQMode(uint8 value)
{
	eqMode = value;
}
void DIAG_WriteRadioSeek(uint8 value)
{
	radioSeek = value;
}
void DIAG_WriteDisplayControl(uint8 value)
{
	displayControl = value;
}




void DIAG_WriteOnAnimation(uint8 value)
{
	onAnimation = value;
}

void DIAG_WriteLoudspeakerFLcontrol(uint8 value)
{
	loudspeakerFLcontrol = value;
}
void DIAG_WriteLoudspeakerFRcontrol(uint8 value)
{
	loudspeakerFRcontrol = value;
}
void DIAG_WriteLoudspeakerRLcontrol(uint8 value)
{
	loudspeakerRLcontrol = value;
}
void DIAG_WriteLoudspeakerRRcontrol(uint8 value)
{
	loudspeakerRRcontrol = value;
}



/******************************************************************************
* RadioAntennaSignalStrength 
******************************************************************************/
void ReadAntennaSignalStrength(uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &radioAntennaSignalStrength, *len);
}

/******************************************************************************
* Source Mode 
******************************************************************************/
void ReadSourceMode(uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &sourceMode, *len);
}
/******************************************************************************
* Volume 
******************************************************************************/
void ReadVolume(uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &volume, *len);
}

/******************************************************************************
* Bass Step 
******************************************************************************/
void ReadBassStep(uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &bassStep, *len);
}

/******************************************************************************
* Treble Step 
******************************************************************************/
void ReadTrebleStep(uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &trebleStep, *len);
}

/******************************************************************************
* Balance Step 
******************************************************************************/
void ReadBalanceStep (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &balanceStep, *len);
}

/******************************************************************************
* Fader Step 
******************************************************************************/
void ReadFaderStep (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &faderStep, *len);
}

/******************************************************************************
* EQ Mode 
******************************************************************************/
void ReadEQMode (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &eqMode, *len);
}

/******************************************************************************
* Battery Voltage 
******************************************************************************/
void ReadBatteryVoltage (uint8* data, uint16* len)
{
	uint32 adc_value=0;
	
	*len = 1;
	adc_value = Check_Batt_Value();
	DIAG_WriteBatteryVoltage(adc_value);
	UTL_COPY_DATA(data, &batteryVoltage, *len);
}

/******************************************************************************
* Microphone Number
******************************************************************************/
void ReadMicrophoneNum (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &microphonenum, *len);
}

/******************************************************************************
* IHU Variant Coding
******************************************************************************/
extern void DIAG_ReadDataVCODINGFromNVM(void);
void ReadIHUVariantCoding (uint8* data, uint16* len)
{
	DIAG_ReadDataVCODINGFromNVM();

	*len = DIAGDBI_IHUVariantCoding_LEN;
	UTL_COPY_DATA(data, u8VariantCoding, *len);
}

/******************************************************************************
* Software Configuration 
******************************************************************************/
extern void DIAG_ReadDataSCFromNVM(void);
void ReadSoftwareConfiguration (uint8* data, uint16* len)
{
	DIAG_ReadDataSCFromNVM();

	*len = DIAGDBI_SOFTWARE_CONFIGURATION_LEN;
	UTL_COPY_DATA(data, softwareConfiguration, *len);
}

/******************************************************************************
* SoftwareReset Info 
******************************************************************************/
extern void DIAG_ReadDataRESETINFOFromNVM(void);
void ReadDataResetInfo (uint8* data, uint16* len)
{
	DIAG_ReadDataRESETINFOFromNVM();

	*len = DIAGDBI_RESET_INFO_LEN;
	UTL_COPY_DATA(data, u8ErrCode, *len);
}

/******************************************************************************
* Loud Speaker FL Control 
******************************************************************************/
void ReadLoudspeakerFLcontrol (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &loudspeakerFLcontrol, *len);
}

/******************************************************************************
* LoudSpeaker FR Control 
******************************************************************************/
void ReadLoudspeakerFRcontrol (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &loudspeakerFRcontrol, *len);
}

/******************************************************************************
* Loud Speaker RL Control 
******************************************************************************/
void ReadLoudspeakerRLcontrol (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &loudspeakerRLcontrol, *len);
}

/******************************************************************************
* Loud Speaker RR Control 
******************************************************************************/
void ReadLoudspeakerRRcontrol (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &loudspeakerRRcontrol, *len);
}

/******************************************************************************
* Display Control 
******************************************************************************/
void ReadDisplayControl (uint8* data, uint16* len)
{
	*len = 1;
	/* wl20220730 */
	displayControl = e2p_save_data.E2P_LCD_BRIGHTNESS;
	UTL_COPY_DATA(data, &displayControl, *len);
}

/******************************************************************************
* Volume Control 
******************************************************************************/
void ReadVolumeControl (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &volume, *len);
}

/******************************************************************************
* Radio Seek 
******************************************************************************/
void ReadRadioSeek (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &radioSeek, *len);
}

/******************************************************************************
* On Animation 
******************************************************************************/
void ReadOnAnimation (uint8* data, uint16* len)
{
	*len = 1;
	UTL_COPY_DATA(data, &onAnimation, *len);
}

/******************************************************************************
* Vehicle Manufacturer Spare part Number 
******************************************************************************/
void ReadVehicleManufactSparePartNum (uint8* data, uint16* len)
{
	*len = DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN;
	#ifdef CX62C_FUNCTION
	if(manufacture_maxim==ON)
		UTL_COPY_DATA(data, vehicleManufactSparePartNum1, *len);
	else
	#endif
		UTL_COPY_DATA(data, vehicleManufactSparePartNum, *len);
}

/******************************************************************************
* Vehicle Manufacturer ECU SW Number 
******************************************************************************/
void ReadVehicleManufactECUSWNum (uint8* data, uint16* len)
{
	*len = DIAGDBI_VEHICLE_MANUFACTURER_ECU_SOFTWARE_NUM_LEN;
	UTL_COPY_DATA(data, vehicleManufactECUSWNum, *len);
}

/******************************************************************************
* System Supplier ID 
******************************************************************************/
void ReadSystemSupplierId (uint8* data, uint16* len)
{
	*len = DIAGDBI_SYSTEM_SUPPLIER_ID_LEN;
	UTL_COPY_DATA(data, systemSupplierId, *len);
}

/******************************************************************************
* VIN 
******************************************************************************/
extern void DIAG_ReadDataVINFromNVM(void);
void ReadVIN (uint8* data, uint16* len)
{
	DIAG_ReadDataVINFromNVM();
	
	*len = DIAGDBI_VIM_LEN;
	UTL_COPY_DATA(data, u8Vin, *len);
}

/******************************************************************************
* Vehicle Manufacturer ECU HW Number 
******************************************************************************/
void ReadVehicleManufactECUHwNum (uint8* data, uint16* len)
{
	*len = DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN;
	UTL_COPY_DATA(data, vehicleManufactECUHwNum, *len);
}

/******************************************************************************
* System Supplier ECU SW Number 
******************************************************************************/
void ReadSystemSupplierECUSWNum (uint8* data, uint16* len)
{
	*len = DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN;
	UTL_COPY_DATA(data, systemSupplierECUSWNum, *len);
}

/******************************************************************************
* Software Configuration 
******************************************************************************/
extern void DIAG_WriteDataSCnVINToNVM(void);
void WriteSoftwareConfiguration(uint8* data, uint16 len)
{	
	UTL_COPY_DATA(softwareConfiguration, data, len);

	if(softwareConfiguration[23]!=0xff)
	{
		shell_value_0 = 60;
		r_sonysound  = (softwareConfiguration[23]&0x01);//JANG_210819_1
	}

	DIAG_WriteDataSCnVINToNVM();
}

/******************************************************************************
* VIN 
******************************************************************************/
void WriteVIN(uint8* data, uint16 len)
{
	UTL_COPY_DATA(u8Vin, data, len);
	
	DIAG_WriteDataSCnVINToNVM();
}

/******************************************************************************
* VIN 
******************************************************************************/
void WriteIHUVariantCoding(uint8* data, uint16 len)
{
	UTL_COPY_DATA(u8VariantCoding, data, len);	
	gDiagHardReset = TRUE;
	DIAG_WriteDataSCnVINToNVM();
}

/* IO CONTROL */
/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlLoudspeakerFLcontrol(uint8 ioctrl)
{
	ioLoudspeakerFLcontrolFlg = TRUE;
	originLoudspeakerFLcontrol = loudspeakerFLcontrol;

	rDiagCmd.data= loudspeakerFLcontrol = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_FL_CTRL;

	DPRINTF(DBG_MSG1, "%s()  %d return %d\n", __FUNCTION__,loudspeakerFLcontrol, originLoudspeakerFLcontrol);
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnLoudspeakerFLcontrol(void)
{
	if( FALSE != ioLoudspeakerFLcontrolFlg)
	{
		ioLoudspeakerFLcontrolFlg = FALSE;
		loudspeakerFLcontrol = originLoudspeakerFLcontrol;
		
		rDiagCmd.data= loudspeakerFLcontrol;
		rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_FL_CTRL;
		DPRINTF(DBG_MSG1, "%s()  %d return %d\n", __FUNCTION__,loudspeakerFLcontrol, originLoudspeakerFLcontrol);
	}

	return loudspeakerFLcontrol;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlLoudspeakerFRcontrol(uint8 ioctrl)
{
	ioLoudspeakerFRcontrolFlg = TRUE;
	originLoudspeakerFRcontrol = loudspeakerFRcontrol;

	rDiagCmd.data= loudspeakerFRcontrol = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_FR_CTRL;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnLoudspeakerFRcontrol(void)
{
	if(FALSE != ioLoudspeakerFRcontrolFlg)
	{
		ioLoudspeakerFRcontrolFlg = FALSE;
		loudspeakerFRcontrol = originLoudspeakerFRcontrol;

		rDiagCmd.data= loudspeakerFRcontrol;
		rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_FR_CTRL;
	}

	return loudspeakerFRcontrol;
}


/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlLoudspeakerRLcontrol(uint8 ioctrl)
{
	ioLoudspeakerRLcontrolFlg = TRUE;
	originLoudspeakerRLcontrol = loudspeakerRLcontrol;

	rDiagCmd.data= loudspeakerRLcontrol = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_RL_CTRL;
	DPRINTF(DBG_MSG1, "%s()  %d return %d\n", __FUNCTION__,loudspeakerRLcontrol, originLoudspeakerRLcontrol);
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnLoudspeakerRLcontrol(void)
{
	if(FALSE != ioLoudspeakerRLcontrolFlg)
	{
		ioLoudspeakerRLcontrolFlg = FALSE;
		loudspeakerRLcontrol = originLoudspeakerRLcontrol;

		rDiagCmd.data= loudspeakerRLcontrol;
		rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_RL_CTRL;
		DPRINTF(DBG_MSG1, "%s()  %d return %d\n", __FUNCTION__,loudspeakerRLcontrol, originLoudspeakerRLcontrol);
	}

	return loudspeakerRLcontrol;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlLoudspeakerRRcontrol(uint8 ioctrl)
{
	ioLoudspeakerRRcontrolFlg = TRUE;
	originLoudspeakerRRcontrol = loudspeakerRRcontrol;

	rDiagCmd.data= loudspeakerRRcontrol = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_RR_CTRL;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnLoudspeakerRRcontrol(void)
{
	if(FALSE != ioLoudspeakerRRcontrolFlg)
	{
		ioLoudspeakerRRcontrolFlg = FALSE;
		loudspeakerRRcontrol = originLoudspeakerRRcontrol;

		rDiagCmd.data= loudspeakerRRcontrol;
		rDiagCmd.diag_cmd = DIA_CMD_SPEAKER_RR_CTRL;
	}

	return loudspeakerRRcontrol;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlDisplayControl(uint8 ioctrl)
{
	ioDisplayControlFlg = TRUE;
	originDisplayControl = displayControl;

	rDiagCmd.data=displayControl = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_DISPLAY_ONOFF;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnDisplayControl(void)
{
	if(FALSE != ioDisplayControlFlg)
	{
		ioDisplayControlFlg = FALSE;
		displayControl = originDisplayControl;

		rDiagCmd.data=displayControl;
		rDiagCmd.diag_cmd = DIA_CMD_DISPLAY_ONOFF;
	}

	return displayControl;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlVolumeControl(uint8 ioctrl)
{
	ioVolumeCtrlFlg = TRUE;
	originVolume = volume;
	rDiagCmd.data=volume = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_VOLUME_CTRL;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnVolumeControl(void)
{

	if(FALSE != ioVolumeCtrlFlg)	
	{
		ioVolumeCtrlFlg = FALSE;
		volume = originVolume;

		rDiagCmd.data=volume;
		rDiagCmd.diag_cmd = DIA_CMD_VOLUME_CTRL;
	}

	return volume;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlRadioSeek(uint8 ioctrl)
{
	ioRadioSeekFlg = TRUE;
	originRadioSeek = radioSeek;

	rDiagCmd.data=radioSeek = ioctrl;
	rDiagCmd.diag_cmd = DIA_CMD_RADIO_SEEK;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnRadioSeek(void)
{
	if(FALSE != ioRadioSeekFlg)
	{
		ioRadioSeekFlg = FALSE;
		radioSeek = originRadioSeek;

		rDiagCmd.data=radioSeek;
		rDiagCmd.diag_cmd = DIA_CMD_RADIO_SEEK;
	}

	return radioSeek;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
void IoCtrlOnAnimation(uint8 ioctrl)
{
	ioOnAnimationFlg = TRUE;
	originOnAnimation = onAnimation;

	//not used no define

	onAnimation = ioctrl;
}

/******************************************************************************
* IoCtrlSetSourceMode 
******************************************************************************/
uint8 IoRtnOnAnimation(void)
{
	if(FALSE != ioOnAnimationFlg)
	{
		ioOnAnimationFlg = FALSE;
		onAnimation = originOnAnimation;
	}

	return onAnimation;
}

bool IsCustomMatched(void)
{
	uint8 i,j;
	bool u8diff;
	if (0u==u8IsFound)
	{
		for (i=0u;(0u==u8Matched&&i<TOTAL_OF_CUSTOM_VIN);i++)
		{
			u8diff=0u;
			for (j=0u;(0u==u8diff&&j<DIAGDBI_VIM_LEN);j++)
			{
				if (u8Vin[j]!=rau8Custom_VINS[i][j])
				{
					u8diff=1u;
				}
			}
			if (0u==u8diff)
			{
				u8Matched=1u;
			}
		}
		u8IsFound=1u;
	}
	return (bool)(1u==u8Matched);
}
