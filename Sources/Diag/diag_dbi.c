/*****************************************************************************
* INCLUDE FILES
*****************************************************************************/
#include <stdio.h>

#include "diag_dbi.h"
#include "diag.h"

#include "../Service/std_util.h"

#include "diag_dbi_if.h"
#include "includes.h"

/*****************************************************************************
* MACRO DEFINITIONS
*****************************************************************************/


/*****************************************************************************
* CONSTANT DEFINITIONS
*****************************************************************************/
#define DIAG_DBI_LOG			(1u)


#define DIAGDBI_NORMAL_LEN		(24u)

#define DIGDBI_IGN_ADC_ONE_VOL			(97u)
#define DIGDBI_IGN_ADC_CONVERSION		(10u)

#define DIGDBI_IO_CTRL_DLC_MIN			(1u)

#define DIAGDBI_RDBI_SID					(0x22u)
#define DIAGDBI_RDBI_POSITIVE_DLC			(512u)

#define DIAGDBI_WDBI_SID					(0x2Eu)
#define DIAGDBI_WDBI_POSITIVE_DLC			(3u)

/*****************************************************************************
* TYPE DEFINITIONS
*****************************************************************************/
typedef uint8 (*hDiagDbiReadFuncType)(uint8 *dst, uint16 *len);
typedef uint8 (*hDiagDbiWriteFuncType)(uint8 *src, uint16 len);

typedef uint8 (*hDiagDbiIORtnECUFuncType)(uint8 *resp, uint16 *respLen);
typedef uint8 (*hDiagDbiIOShortTermAdjFuncType)(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);


typedef struct {
	uint16 did;
	hDiagDbiReadFuncType func;
} hDiagDbiReadTableType;

typedef struct {
	uint16 did;
	uint16 dlc;
	hDiagDbiWriteFuncType func;
} hDiagDbiWriteTableType;

typedef struct {
	uint16 did;
	uint16 dlcRtnECU;
	hDiagDbiIORtnECUFuncType funcRtnECU;
	uint16 dlcShortTermAdj;
	hDiagDbiIOShortTermAdjFuncType funcShortTermAdj;
} hDiagDbiIOTableType;


/*
	READ DataByIdentifier
*/
typedef enum {
	DIAGDBI_R_DISPLAY_CONTROL,								/* 0x7015  Display control */
	DIAGDBI_R_BATTERY_VOLTAGE,								/* 0xD000  Battery Voltage */
	DIAGDBI_R_MICROPHONE_NUMBER,							/* 0x150F  Microphone Number */
	DIAGDBI_R_IHUVARIANTCODING,							    /* 0x1510  IHU Variant Coding */

	DIAGDBI_R_RADIO_ANTENNA_SIGNAL_STRENGTH,				/* 0x6500  Radio Antenna Signal Strength */
	DIAGDBI_R_SOURCE_MODE,									/* 0x6501  Source Mode */
	DIAGDBI_R_VOLUME,										/* 0x6502  Volume */
	DIAGDBI_R_BASS_STEP,									/* 0x6503  BassStep */
	DIAGDBI_R_TREBLE_STEP,									/* 0x6504  TrebleStep */
	DIAGDBI_R_BALANCE_STEP,									/* 0x6505  Balance Step */
	DIAGDBI_R_FADER_STEP, 									/* 0x6506  Fader Step */
	DIAGDBI_R_EQ_MODE,										/* 0x6507  EQ Mode */
	DIAGDBI_R_DISPLAY_CONTROL1,								/* 0x7006  Display control */
	DIAGDBI_R_BATTERY_VOLTAGE1,								/* 0x700D  Battery Voltage */
	DIAGDBI_R_MICROPHONE_NUMBER1,							/* 0x700F  Microphone Number */
	DIAGDBI_R_IHUVARIANTCODING1,						    /* 0x7010  IHU Variant Coding */	
	DIAGDBI_R_LOUD_SPEAKER_FL_CONTRL,						/* 0x7011  Loudspeaker FL control */	
	DIAGDBI_R_LOUD_SPEAKER_FR_CONTRL,						/* 0x7012  Loudspeaker FR control */
	DIAGDBI_R_LOUD_SPEAKER_RL_CONTRL,						/* 0x7013  Loudspeaker RL control */
	DIAGDBI_R_LOUD_SPEAKER_RR_CONTRL,						/* 0x7014  Loudspeaker RR control */
	
	DIAGDBI_R_VOLUME_CONTROL,								/* 0x7016  Volume control */
	DIAGDBI_R_RADIO_SEEK,									/* 0x7017  Radio seek */
	DIAGDBI_R_ON_ANIMATION,									/* 0x7018  On animation */
	
	DIAGDBI_R_SOFTWARE_CONFIGURATION,						/* 0xD009  Software Configuration */	

	DIAGDBI_R_ErrCode,						                /* 0xDEAD  SoftwareReset info */

	DIAGDBI_R_VEHICLE_MANUFACTURER_SPARE_PART_NUMBER,		/* 0xF187  Vehicle Manufacturer Spare Part Number */
	DIAGDBI_R_VEHICLE_MANUFACTURER_ECU_SOFTWARE_NUMBER,		/* 0xF188  Vehicle Manufacturer ECU Software Number */
	DIAGDBI_R_SYSTEM_SUPPLIER_ID,							/* 0xF18A  System Supplier Identifier */
	DIAGDBI_R_VIN,											/* 0xF190  VIN */
	DIAGDBI_R_VEHICLE_MANUFACTURER_ECU_HARDWARE_NUMBER,		/* 0xF191  Vehicle Manufacturer ECU Hardware Number */
	DIAGDBI_R_SYSTEM_SUPPLIER_ECU_SOFTWARE_NUMBER,			/* 0xF194  System Supplier ECU Software Number */
	DIAGDBI_R_MAX
} hDiagDbiReadIdxType;

/*
	WRITE DataByIdentifier
*/
typedef enum {
	DIAGDBI_W_IHUVariantCoding,											/* 0x1510  VariantCoding */
	DIAGDBI_W_IHUVariantCoding1,										/* 0x7010  VariantCoding */
	DIAGDBI_W_SOFTWARE_CONFIGURATION,									/* 0xD009  Software Configuration */
	DIAGDBI_W_QFI_MODE,													/* 0xF100  QFI_MODE */
	DIAGDBI_W_USB_MODE,													/* 0xF101  USB_MODE */
	DIAGDBI_W_VIN,														/* 0xF190  VIN */
	DIAGDBI_W_MAX
} hDiagDbiWriteIdxType;


/*
	I/O DataByIdentifier
*/
typedef enum {
	DIAGDBI_IO_LOUD_SPEAKER_FL_CONTRL_CUSTOM,		/* 0x15D0  Loudspeaker FL control */
	DIAGDBI_IO_LOUD_SPEAKER_FR_CONTRL_CUSTOM,		/* 0x15D1  Loudspeaker FR control */
	DIAGDBI_IO_VOL_CONTRL,	             	/* 0x15D3  Volume control control */
	DIAGDBI_IO_RADIO_SEEK_CONTRL,		    /* 0x15D4  Radio seek control */
	DIAGDBI_IO_ANIMATION_CONTRL,		    /* 0x15D5  On animation control */
#if 1
	DIAGDBI_IO_LOUD_SPEAKER_FL_CONTRL,		/* 0x7011  Loudspeaker FL control */
	DIAGDBI_IO_LOUD_SPEAKER_FR_CONTRL,		/* 0x7012  Loudspeaker FR control */
	DIAGDBI_IO_LOUD_SPEAKER_RL_CONTRL,		/* 0x7013  Loudspeaker RL control */	
	DIAGDBI_IO_LOUD_SPEAKER_RR_CONTRL,		/* 0x7014  Loudspeaker RR control */
	DIAGDBI_IO_DISPLAY_CONTROL,				/* 0x7015  Display control */	
	DIAGDBI_IO_VOLUME_CONTROL,				/* 0x7016  Volume control */		
	DIAGDBI_IO_RADIO_SEEK,					/* 0x7017  Radio seek */		
	DIAGDBI_IO_ON_ANIMATION,				/* 0x7018  On animation */		
#endif
	DIAGDBI_IO_MAX
} hDiagDbiIOIdxType;


typedef enum {
	DIAGDBI_IO_OPT_RETURN,		/* returnControlToECU */
	DIAGDBI_IO_OPT_RESET,		/* resetToDefault */
	DIAGDBI_IO_OPT_FREEZE,		/* freezeCurrentState */
	DIAGDBI_IO_OPT_ADJUSTMENT,	/* shortTermAdjustment */
	DIAGDBI_IO_OPT_MAX
} hDigDbiIOOptType;

/*****************************************************************************
* EXTERNAL VARIABLE DECLARALATIONS
*****************************************************************************/
/*****************************************************************************
* FUNCTION PROTOTYPES
*****************************************************************************/
/*
	READ DataByIdentifier
*/
static uint8 DiagDbiReadRadioAntennaSignalStrength(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadSourceMode(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadVolume(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadBassStep(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadTrebleStep(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadBalanceStep(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadFaderStep(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadEQMode(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadBatteryVoltage(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadSoftwareConfiguration(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadSoftwareResetInfo(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadLoudspeakerFLcontrol(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadLoudspeakerFRcontrol(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadLoudspeakerRLcontrol(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadLoudspeakerRRcontrol(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadDisplayControl(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadVolumeControl(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadRadioSeek(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadOnAnimation(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadVehicleManufactSparePartNum(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadVehicleManufactECUSWNum(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadSystemSupplierId(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadVIN(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadVehicleManufactECUHwNum(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadSystemSupplierECUSWNum(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadMicrophoneNum(uint8 *dst, uint16 *len);
static uint8 DiagDbiReadIHUVariantCoding(uint8 *dst, uint16 *len);

/*
	WRITE DataByIdentifier
*/
static uint8 DiagDbiWriteSoftwareConfiguration(uint8 *src, uint16 len);
static uint8 DiagDbiWriteVIN(uint8 *src, uint16 len);
static uint8 DiagDbiWriteQFIMode(uint8 *src, uint16 len);
static uint8 DiagDbiWriteUSBMode(uint8 *src, uint16 len);
static uint8 DiagDbiWriteIHUVariantCoding(uint8 *src, uint16 len);

/*
	I/O DataByIdentifier
*/
static uint8 DiagDbiIORtnECULoudspeakerFLcontrol(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjLoudspeakerFLcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECULoudspeakerFRcontrol(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjLoudspeakerFRcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECULoudspeakerRLcontrol(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjLoudspeakerRLcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECULoudspeakerRRcontrol(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjLoudspeakerRRcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECUDisplayControl(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjDisplayControl(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECUVolumeControl(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjVolumeControl(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECURadioSeek(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjRadioSeek(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIORtnECUOnAnimation(uint8 *resp, uint16 *respLen);
static uint8 DiagDbiIOShortTermAdjOnAnimation(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

static uint8 DiagDbiIOAction(const hDiagDbiIOTableType *ioCtrl, uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);



/*****************************************************************************
* GLOBAL VARIABLE DEFINITIONS AND PROTOTYPES
*****************************************************************************/
/******************************************************************************
* STATIC VARIABLE DEFINITIONS
******************************************************************************/
/*
	READ DataByIdentifier
*/
const hDiagDbiReadTableType gDiagDbiReadTable[DIAGDBI_R_MAX] = {
	{ (0x1506u),		DiagDbiReadDisplayControl				}, 			/* 0x7015  Display control */
	{ (0x150Du),		DiagDbiReadBatteryVoltage				}, 			/* 0xD000  Battery Voltage */
	{ (0x150Fu),		DiagDbiReadMicrophoneNum				}, 			/* 0x150F  Microphone Number */
	{ (0x1510u),		DiagDbiReadIHUVariantCoding				}, 			/* 0x1510  IHU Variant Coding */
	{ (0x6500u),		DiagDbiReadRadioAntennaSignalStrength 	}, 			/* 0x6500  Radio Antenna Signal Strength */
	{ (0x6501u),		DiagDbiReadSourceMode					},			/* 0x6501  Source Mode */
	{ (0x6502u),		DiagDbiReadVolume 						}, 			/* 0x6502  Volume */
	{ (0x6503u),		DiagDbiReadBassStep 					},			/* 0x6503  BassStep */
	{ (0x6504u),		DiagDbiReadTrebleStep					},			/* 0x6504  TrebleStep */
	{ (0x6505u),		DiagDbiReadBalanceStep 					},			/* 0x6505  Balance Step */
	{ (0x6506u),		DiagDbiReadFaderStep					},			/* 0x6506  Fader Step */
	{ (0x6507u),		DiagDbiReadEQMode						},			/* 0x6507  EQ Mode */
	{ (0x7006u),		DiagDbiReadDisplayControl				}, 			/* 0x7006  Display control */
	{ (0x700Du),		DiagDbiReadBatteryVoltage				}, 			/* 0x700D  Battery Voltage */
	{ (0x700Fu),		DiagDbiReadMicrophoneNum				}, 			/* 0x700F  Microphone Number */
	{ (0x7010u),		DiagDbiReadIHUVariantCoding			    },	 		/* 0x7010  IHU Variant Coding1 */	
	{ (0x7011u),		DiagDbiReadLoudspeakerFLcontrol			},	 		/* 0x7011  Loudspeaker FL control */	
	{ (0x7012u),		DiagDbiReadLoudspeakerFRcontrol		 	}, 			/* 0x7012  Loudspeaker FR control */
	{ (0x7013u),		DiagDbiReadLoudspeakerRLcontrol			}, 			/* 0x7013  Loudspeaker RL control */
	{ (0x7014u),		DiagDbiReadLoudspeakerRRcontrol			}, 			/* 0x7014  Loudspeaker RR control */
	{ (0x7016u),		DiagDbiReadVolumeControl				},			/* 0x7016  Volume control */
	{ (0x7017u),		DiagDbiReadRadioSeek 					},			/* 0x7017  Radio seek */
	{ (0x7018u),		DiagDbiReadOnAnimation			 		}, 			/* 0x7018  On animation */
	{ (0xD009u),		DiagDbiReadSoftwareConfiguration 		}, 			/* 0xD009  Software Configuration */
	{ (0xDEADu),		DiagDbiReadSoftwareResetInfo			}, 			/* 0xDEAD  SoftwareReset info */
	{ (0xF187u),		DiagDbiReadVehicleManufactSparePartNum 	},			/* 0xF187  Vehicle Manufacturer Spare Part Number */
	{ (0xF188u),		DiagDbiReadVehicleManufactECUSWNum		},			/* 0xF188  Vehicle Manufacturer ECU Software Number */
	{ (0xF18Au),		DiagDbiReadSystemSupplierId				},			/* 0xF18A  System Supplier Identifier */
	{ (0xF190u),		DiagDbiReadVIN							},			/* 0xF190  VIN */
	{ (0xF191u),		DiagDbiReadVehicleManufactECUHwNum		},			/* 0xF191  Vehicle Manufacturer ECU Hardware Number */
	{ (0xF194u),		DiagDbiReadSystemSupplierECUSWNum		},			/* 0xF194  System Supplier ECU Software Number */
};


/*
	WRITE DataByIdentifier
*/
const hDiagDbiWriteTableType gDiagDbiWriteTable[DIAGDBI_W_MAX] = {
	{ (0x1510u),	DIAGDBI_IHUVariantCoding_LEN,									DiagDbiWriteIHUVariantCoding	    },		/* 0x1510  IHUVariantCoding */
	{ (0x7010u),	DIAGDBI_IHUVariantCoding_LEN,									DiagDbiWriteIHUVariantCoding	    },		/* 0x7010  IHUVariantCoding */
	{ (0xD009u),	DIAGDBI_SOFTWARE_CONFIGURATION_LEN, 		        			DiagDbiWriteSoftwareConfiguration	},		/* 0xD009  Software Configuration */
	{ (0xF100u),	DIAGDBI_QFI_MODE_LEN,											DiagDbiWriteQFIMode					},		/* 0xF100  QFI_MODE */
	{ (0xF101u),	DIAGDBI_USB_MODE_LEN,											DiagDbiWriteUSBMode					},		/* 0xF101  USB_MODE */
	{ (0xF190u),	DIAGDBI_VIM_LEN,												DiagDbiWriteVIN						},		/* 0xF190  VIN */	
};


/*
	I/O CONTROL DataByIdentifier
*/
const hDiagDbiIOTableType gDiagDbiIOTable[DIAGDBI_IO_MAX] = {

	{ (0x15D0u),	1u,		DiagDbiIORtnECULoudspeakerFLcontrol, 		2u,		DiagDbiIOShortTermAdjLoudspeakerFLcontrol 		}, 		/* 0x15D0  Loudspeaker FL control */
	{ (0x15D1u),	1u,		DiagDbiIORtnECULoudspeakerFRcontrol,		2u,		DiagDbiIOShortTermAdjLoudspeakerFRcontrol 		}, 		/* 0x15D1  Loudspeaker FR control */

	{ (0x15D3u),	1u,		DiagDbiIORtnECUVolumeControl,		        2u,		DiagDbiIOShortTermAdjVolumeControl 		}, 		        /* 0x15D3  Volume Control */
	{ (0x15D4u),	1u,		DiagDbiIORtnECULoudspeakerFLcontrol, 		2u,		DiagDbiIOShortTermAdjLoudspeakerFLcontrol 		}, 		/* 0x15D4  Loudspeaker FL control */
	{ (0x15D5u),	1u,		DiagDbiIORtnECULoudspeakerFRcontrol,		2u,		DiagDbiIOShortTermAdjLoudspeakerFRcontrol 		}, 		/* 0x15D5  Loudspeaker FR control */
#if 1
	{ (0x7011u),	1u,		DiagDbiIORtnECULoudspeakerFLcontrol, 		2u,		DiagDbiIOShortTermAdjLoudspeakerFLcontrol 		}, 		/* 0x7011  Loudspeaker FL control */
	{ (0x7012u),	1u,		DiagDbiIORtnECULoudspeakerFRcontrol,		2u,		DiagDbiIOShortTermAdjLoudspeakerFRcontrol 		}, 		/* 0x7012  Loudspeaker FR control */
	{ (0x7013u),	1u,		DiagDbiIORtnECULoudspeakerRLcontrol, 		2u,		DiagDbiIOShortTermAdjLoudspeakerRLcontrol       }, 		/* 0x7013  Loudspeaker RL control */
	{ (0x7014u),	1u,		DiagDbiIORtnECULoudspeakerRRcontrol, 		2u,		DiagDbiIOShortTermAdjLoudspeakerRRcontrol 		}, 		/* 0x7014  Loudspeaker RR control */
	{ (0x7015u),	1u,		DiagDbiIORtnECUDisplayControl, 				2u,		DiagDbiIOShortTermAdjDisplayControl 			}, 		/* 0x7015  Display control */	
	{ (0x7016u),	1u,		DiagDbiIORtnECUVolumeControl, 				2u,		DiagDbiIOShortTermAdjVolumeControl 				}, 		/* 0x7016  Volume control */		
	{ (0x7017u),	1u,		DiagDbiIORtnECURadioSeek, 					2u,		DiagDbiIOShortTermAdjRadioSeek 					}, 		/* 0x7017  Radio seek */			
	{ (0x7018u),	1u,		DiagDbiIORtnECUOnAnimation, 				2u,		DiagDbiIOShortTermAdjOnAnimation 				},		/* 0x7018  On animation */				
#endif
	
};

/*
	DataByIdentifier
*/
/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagDbiInit(void)
{
	/* none */
}


/*
	READ DataByIdentifier
*/
/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagDbiReadGetIdx(uint16 did)
{
	uint8 rpos;
	uint8 mpos;
	uint8 lpos;
	uint8 idx = DIAGDBI_R_MAX;

	rpos = DIAGDBI_R_MAX - 1;
	lpos = 0;

	while(lpos < rpos)
	{
		/* get middle */
		mpos = STD_SHIFT_R((lpos + rpos));

		if(gDiagDbiReadTable[mpos].did > did)
		{/* id is under */
			rpos = mpos - 1;
		}
		else if(gDiagDbiReadTable[mpos].did < did)
		{/* id id over */
			lpos = mpos + 1;
		}
		else
		{ /* gDiagDbiReadTable[mpos].did == did */
			idx = mpos;
			break;
		}
	}

	if((lpos == rpos) && (gDiagDbiReadTable[lpos].did == did))
	{/* last check */
		idx = lpos;
	}

	return idx;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagDbiRead(uint16 did, uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx;

	idx = DiagDbiReadGetIdx(did);

	if(DIAGDBI_R_MAX > idx)
	{
		nrc = gDiagDbiReadTable[idx].func(dst, len);
	}
	else
	{
		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadRadioAntennaSignalStrength(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 signalStrn = 0;
	uint16 length = 0;

	ReadAntennaSignalStrength(&signalStrn, &length);

	*dst = signalStrn; idx ++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadSourceMode(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 radioSource = 0;
	uint16 size = 0;

	ReadSourceMode(&radioSource, &size);

	*dst = radioSource; idx++;

	*len = idx;
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadVolume(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 volume = 0;
	uint16 size = 0;

	ReadVolume(&volume, &size);

	*dst = volume; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadBassStep(uint8 *data, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 bassStep = 0;
	uint16 size = 0;

	ReadBassStep(&bassStep, &size);

	*data = bassStep; idx ++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadTrebleStep(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 trebleStep = 0;
	uint16 size = 0;

	ReadTrebleStep(&trebleStep, &size);

	*dst = trebleStep; idx ++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadBalanceStep(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 balanceStep = 0;
	uint16 size = 0;

	ReadBalanceStep(&balanceStep, &size);
	*dst = balanceStep; idx++;

	*len = idx;
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadFaderStep(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 faderStep = 0;
	uint16 size = 0;

	ReadFaderStep(&faderStep, &size);
	*dst = faderStep; idx++;

	*len = idx;
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadEQMode(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 eqMode = 0;
	uint16 size = 0;

	ReadEQMode(&eqMode, &size);
	*dst = eqMode; idx++;

	*len = idx;
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadBatteryVoltage(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 batteryVoltage = 0;
	uint16 size = 0;

	ReadBatteryVoltage(&batteryVoltage, &size);
	*dst = batteryVoltage; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadMicrophoneNum(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 microphonenum = 0;
	uint16 size = 0;

	ReadMicrophoneNum(&microphonenum, &size);
	*dst = microphonenum; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadIHUVariantCoding(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 IHUVariantCoding[8] = {0};
	uint16 size = 0;

	ReadIHUVariantCoding(dst, &size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadSoftwareConfiguration(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 config[DIAGDBI_SOFTWARE_CONFIGURATION_LEN] = {0, };
	uint16 size = 0;
	
	ReadSoftwareConfiguration(config, &size);
	UTL_COPY_DATA(dst, config, size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadSoftwareResetInfo(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 u8ErrCode[DIAGDBI_RESET_INFO_LEN] = {0, };
	uint16 size = 0;
	
	ReadDataResetInfo(u8ErrCode, &size);
	UTL_COPY_DATA(dst, u8ErrCode, size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadLoudspeakerFLcontrol(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 loudSpeakerFlCtrl = 0;
	uint16 size = 0;

	ReadLoudspeakerFLcontrol(&loudSpeakerFlCtrl, &size);
	*dst = loudSpeakerFlCtrl; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadLoudspeakerFRcontrol(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 loudSpeakerFrCtrl = 0;
	uint16 size = 0;

	ReadLoudspeakerFRcontrol(&loudSpeakerFrCtrl, &size);
	*dst = loudSpeakerFrCtrl; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadLoudspeakerRLcontrol(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 loudSpeakerRlCtrl = 0;
	uint16 size = 0;

	ReadLoudspeakerRLcontrol(&loudSpeakerRlCtrl, &size);
	*dst = loudSpeakerRlCtrl; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadLoudspeakerRRcontrol(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 loudSpeakerRrCtrl = 0;
	uint16 size = 0;

	ReadLoudspeakerRRcontrol(&loudSpeakerRrCtrl, &size);
	*dst = loudSpeakerRrCtrl; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadDisplayControl(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 displayCtrl = 0;
	uint16 size = 0;

	ReadDisplayControl(&displayCtrl, &size);
	*dst = displayCtrl; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadVolumeControl(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 volume = 0;
	uint16 size = 0;

	ReadVolumeControl(&volume, &size);
	*dst = volume; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadRadioSeek(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 radioSeek = 0;
	uint16 size = 0;

	ReadRadioSeek(&radioSeek, &size);
	*dst = radioSeek; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadOnAnimation(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 idx = 0;
	uint8 onAni = 0;
	uint16 size = 0;

	ReadOnAnimation(&onAni, &size);
	*dst = onAni; idx++;

	*len = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadVehicleManufactSparePartNum(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 vehManuSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN] = {0, };
	uint16 size = 0;
	
	ReadVehicleManufactSparePartNum(vehManuSparePartNum, &size);
	UTL_COPY_DATA(dst, vehManuSparePartNum, size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadVehicleManufactECUSWNum(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 vehManEcuSwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_SOFTWARE_NUM_LEN] = {0, };
	uint16 size = 0;
	
	ReadVehicleManufactECUSWNum(vehManEcuSwNum, &size);
	UTL_COPY_DATA(dst, vehManEcuSwNum, size);

	*len = size;	

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadSystemSupplierId(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 suppId[DIAGDBI_SYSTEM_SUPPLIER_ID_LEN] = {0, };
	uint16 size = 0;
	
	ReadSystemSupplierId(suppId, &size);
	UTL_COPY_DATA(dst, suppId, size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadVIN(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 vin[DIAGDBI_VIM_LEN] = {0, };
	uint16 size = 0;
	
	ReadVIN(vin, &size);
	UTL_COPY_DATA(dst, vin, size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadVehicleManufactECUHwNum(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 vehManuEcuHwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN] = {0, };
	uint16 size = 0;
	
	ReadVehicleManufactECUHwNum(vehManuEcuHwNum, &size);
	UTL_COPY_DATA(dst, vehManuEcuHwNum, size);

	*len = size;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiReadSystemSupplierECUSWNum(uint8 *dst, uint16 *len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 sysSupEcuSwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN] = {0, };
	uint16 size = 0;
	
	ReadSystemSupplierECUSWNum(sysSupEcuSwNum, &size); // ReadVehicleManufactECUHwNum
	UTL_COPY_DATA(dst, sysSupEcuSwNum, size);

	*len = size;

	return nrc;
}

/*
	WRITE DataByIdentifier
*/
/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagDbiWriteGetIdx(uint16 did)
{
	uint8 idx = DIAGDBI_W_MAX;

	uint8 rpos = 0;
	uint8 mpos = 0;
	uint8 lpos = 0;


	rpos = DIAGDBI_W_MAX - 1;
	lpos = 0;

	while(lpos < rpos)
	{
		/* get middle */
		mpos = STD_SHIFT_R((lpos + rpos));

		if(gDiagDbiWriteTable[mpos].did > did)
		{/* id is under */
			rpos = mpos - 1;
		}
		else if(gDiagDbiWriteTable[mpos].did < did)
		{/* id id over */
			lpos = mpos + 1;
		}
		else
		{ /* gDiagDbiWriteTable[mpos].did == did */
			idx = mpos;
			break;
		}
	}

	if((lpos == rpos) && (gDiagDbiWriteTable[lpos].did == did))
	{/* last check */
		idx = lpos;
	}

	return idx;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagDbiWrite(uint16 did, uint8 *src, uint16 len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;

	idx = DiagDbiWriteGetIdx(did);

	if(DIAGDBI_W_MAX > idx)
	{
		if(len != gDiagDbiWriteTable[idx].dlc)
		{


			/* Negative incorrectMessageLengthOrInvalidFormat */
			nrc = DIAG_NRC_IMLOIF;
		}
		else
		{
			nrc = gDiagDbiWriteTable[idx].func(src, len);
		}
	}
	else
	{
		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
//	DPRINTF(DBG_MSG3,"%s %x %x\n\r",__FUNCTION__, idx ,did);

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiWriteSoftwareConfiguration(uint8 *src, uint16 len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	WriteSoftwareConfiguration(src, len);

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiWriteVIN(uint8 *src, uint16 len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	WriteVIN(src, len);

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiWriteIHUVariantCoding(uint8 *src, uint16 len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	WriteIHUVariantCoding(src, len);

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiWriteQFIMode(uint8 *src, uint16 len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	//excute Diagnostic
	DPRINTF(DBG_MSG1,"%s  %x %x %x\n\r",__FUNCTION__,src[0],src[1],src[2] );
	if((src[0]==0x20)&&(src[1]==0x10)&&(src[2]==0x30)
		#if 1	//JANG_210830
		&&(r_Device.r_System->gear_status==ON)
		#endif
		)
	{
		/* Received data is in range process further. */
		//enter QFI mode
		QFIModeState = QFI_MODE_ENTER;//JANG_210823
		nrc = DIAG_NRC_NO_ERROR;
	}
	else
	{
		/* Request contains invalid data - send negative response! */
		nrc = DIAG_NRC_ROOR;
	}
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiWriteUSBMode(uint8 *src, uint16 len)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	//excute Diagnostic
	DPRINTF(DBG_MSG1,"%s  %x %x %x\n\r",__FUNCTION__,src[0],src[1],src[2],src[3]);
	if((src[0]==0x20)&&(src[1]==0x10)&&(src[2]==0x30)&&((src[3]==1)||(src[3]==2)))
	{
		/* Received data is in range process further. */
		//enter USB mode
		//<<JANG_210920_4
		diag_usb_device.flag = ON;
		diag_usb_device.trycnt = 0;
		diag_usb_device.buf[0] = 0x01;				//USD device No.
		diag_usb_device.buf[1] = src[3];			//device 0x1, host 0x2
		//>>
			
		nrc = DIAG_NRC_NO_ERROR;
	}
	else
	{
		/* Request contains invalid data - send negative response! */
		nrc = DIAG_NRC_ROOR;
	}
	
	return nrc;
}


/*
	I/O CONTROL DataByIdentifier
*/
/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagDbiIOGetIdx(uint16 did)
{
	uint8 rpos;
	uint8 mpos;
	uint8 lpos;
	uint8 idx = DIAGDBI_IO_MAX;

	rpos = DIAGDBI_IO_MAX - 1;
	lpos = 0;

	while(lpos < rpos)
	{
		/* get middle */
		mpos = STD_SHIFT_R((lpos + rpos));

		if(gDiagDbiIOTable[mpos].did > did)
		{/* id is under */
			rpos = mpos - 1;
		}
		else if(gDiagDbiIOTable[mpos].did < did)
		{/* id id over */
			lpos = mpos + 1;
		}
		else
		{ /* gDiagDbiIOTable[mpos].did == did */
			idx = mpos;
			break;
		}
	}

	if((lpos == rpos) && (gDiagDbiIOTable[lpos].did == did))
	{/* last check */
		idx = lpos;
	}

	return idx;
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOAction(const hDiagDbiIOTableType *ioCtrl, uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{

	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 ctrlOpt = 0;

	STD_GET_1BYTE(ctrlOpt, req, idx);

	if(DIAGDBI_IO_OPT_RETURN == ctrlOpt)
	{

		if(reqLen != ioCtrl->dlcRtnECU)
		{
			/* Negative incorrectMessageLengthOrInvalidFormat */
			nrc = DIAG_NRC_IMLOIF;
		}
		else
		{
			nrc = ioCtrl->funcRtnECU(resp, respLen);
		}
	}
	else if(DIAGDBI_IO_OPT_ADJUSTMENT == ctrlOpt)
	{
		if(reqLen != ioCtrl->dlcShortTermAdj)
		{
			/* Negative incorrectMessageLengthOrInvalidFormat */
			nrc = DIAG_NRC_IMLOIF;
		}
		else
		{
			nrc = ioCtrl->funcShortTermAdj(&(req[idx]), (reqLen - idx), resp, respLen);
		}
	}
	else
	{
		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}

	return nrc;
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagDbiIO(uint16 did, uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx;

	idx = DiagDbiIOGetIdx(did);

	if(DIAGDBI_IO_MAX > idx)
	{
		if(reqLen < DIGDBI_IO_CTRL_DLC_MIN)
		{
			/* Negative incorrectMessageLengthOrInvalidFormat */
			nrc = DIAG_NRC_IMLOIF;
		}
		else
		{
			nrc = DiagDbiIOAction(&(gDiagDbiIOTable[idx]), req, reqLen, resp, respLen);
		}
	}
	else
	{
		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}

	return nrc;
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagDbiIORtnAll(void)
{
	IoRtnLoudspeakerFLcontrol();
	IoRtnLoudspeakerFRcontrol();
	IoRtnLoudspeakerRLcontrol();
	IoRtnLoudspeakerRRcontrol();
	IoRtnDisplayControl();
	IoRtnVolumeControl();
	IoRtnRadioSeek();
	IoRtnOnAnimation();
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECULoudspeakerFLcontrol(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnLoudspeakerFLcontrol();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjLoudspeakerFLcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x03 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlLoudspeakerFLcontrol(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECULoudspeakerFRcontrol(uint8 *resp, uint16 *respLen)
{

	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnLoudspeakerFRcontrol();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjLoudspeakerFRcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x03 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlLoudspeakerFRcontrol(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECULoudspeakerRLcontrol(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnLoudspeakerRLcontrol();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjLoudspeakerRLcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x03 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlLoudspeakerRLcontrol(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}
	
	return nrc;
}
/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECULoudspeakerRRcontrol(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnLoudspeakerRRcontrol();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjLoudspeakerRRcontrol(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x03 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlLoudspeakerRRcontrol(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECUDisplayControl(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnDisplayControl();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjDisplayControl(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x03 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlDisplayControl(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECUVolumeControl(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnVolumeControl();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjVolumeControl(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x31 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlVolumeControl(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}

	return nrc;
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECURadioSeek(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnRadioSeek();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjRadioSeek(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x04 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlRadioSeek(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}
	
	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIORtnECUOnAnimation(uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	mode = IoRtnOnAnimation();

	STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_RETURN, idx);
	STD_PUT_1BYTE(resp, mode, idx);

	*respLen = idx;

	return nrc;	
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
static uint8 DiagDbiIOShortTermAdjOnAnimation(uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 idx = 0;
	uint8 mode = 0;

	STD_GET_1BYTE(mode, req, idx);

	if(0x03 < mode)
	{

		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		IoCtrlOnAnimation(mode);

		idx = 0;
		STD_PUT_1BYTE(resp, DIAGDBI_IO_OPT_ADJUSTMENT, idx);
		STD_PUT_1BYTE(resp, mode, idx);

		*respLen = idx;
	}
	
	return nrc;
}

