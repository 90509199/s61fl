#ifndef __DIAG_DBI__
#define __DIAG_DBI__

/******************************************************************************
* INCLUDE FILES
******************************************************************************/
#include "../includes.h"

/******************************************************************************
* CONSTANT DEFINITIONS
******************************************************************************/
#if 1
#define DIAGDBI_SOFTWARE_CONFIGURATION_LEN					(32u)
#define DIAGDBI_VIM_LEN										(17u)
#define DIAGDBI_IHUVariantCoding_LEN                        (8u)
#define DIAGDBI_QFI_MODE_LEN								(3u)
#define DIAGDBI_USB_MODE_LEN								(4u)
#define DIAGDBI_RESET_INFO_LEN								(64u)
#else
#define DIAGDBI_SOFTWARE_CONFIGURATION_LEN					(8u)
#define DIAGDBI_VIM_LEN										(17u)
#endif
#define DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN		(13u)
#define DIAGDBI_VEHICLE_MANUFACTURER_ECU_SOFTWARE_NUM_LEN	(16u)
#define DIAGDBI_SYSTEM_SUPPLIER_ID_LEN						(3u)
#define DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN			(12u)
#define DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN			(10u)

/******************************************************************************
* MACRO DEFINITIONS
******************************************************************************/


/******************************************************************************
* TYPE DEFINITIONS
******************************************************************************/


/******************************************************************************
* GLOBAL VARIABLE PROTOTYPES
******************************************************************************/


/******************************************************************************
* FUNCTION PROTOTYPES
******************************************************************************/
extern void DiagDbiInit(void);

extern uint8 DiagDbiRead(uint16 did, uint8 *dst, uint16 *len);
extern uint8 DiagDbiWrite(uint16 did, uint8 *src, uint16 len);
extern uint8 DiagDbiIO(uint16 did, uint8 *req, uint16 reqLen, uint8 *resp, uint16 *respLen);

extern void DiagDbiIORtnAll(void);

#endif /* __DIAG_DBI__ */
