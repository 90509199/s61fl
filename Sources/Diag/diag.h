#ifndef __DIAG_H__
#define __DIAG_H__

/******************************************************************************
* INCLUDE FILES
******************************************************************************/
#include "../includes.h"


/******************************************************************************
* CONSTANT DEFINITIONS
******************************************************************************/

/*
	Error codes
*/
#define E_OK						0
#define E_ERROR						1
#define E_FLASHODDACCESS			2
#define E_FLASHNOTERASED			3
#define E_ACCESSERROR				4
#define E_PROTECTIONERROR			5
#define E_NOTSTARTOFSECTOR			6
#define E_PROGRAMERASEIMPOSSIBLE	7
#define E_ERRRANGE					8
#define E_ERRNOTAVAIL				9
#define E_ERRBUSY					10
#define E_ERRVALUE					11
#define E_PAR						12


/* Negative Response Codes */
#define DIAG_NRC_NO_ERROR	(0x00u)				/* no negative */

#define DIAG_NRC_SNS		(0x11u)				/* serviceNotSupported */
#define DIAG_NRC_SFNS		(0x12u)				/* sub-functionNotSupported */
#define DIAG_NRC_IMLOIF		(0x13u)				/* incorrectMessageLengthOrInvalidFormat */

#define DIAG_NRC_CNC		(0x22u)				/* conditionsNotCorrect */
#define DIAG_NRC_RSE		(0x24u)				/* requestSequenceError */

#define DIAG_NRC_ROOR		(0x31u)				/* requestOutOfRange */
#define DIAG_NRC_SAD		(0x33u)				/* securityAccessDenied */
#define DIAG_NRC_IK			(0x35u)				/* invalidKey */
#define DIAG_NRC_ENOA		(0x36u)				/* exceededNumberOfAttempts */
#define DIAG_NRC_RTDNE		(0x37u)				/* requiredTimeDelayNotExpired */

#define DIAG_NRC_GPF		(0x72u)				/* generalProgrammingFailure */
#define DIAG_NRC_RCRRP		(0x78u)				/* requestCorrectlyReceived-ResponsePending */
#define DIAG_NRC_SFNSIAS	(0x7Eu)				/* sub-functionNotSupportedInActiveSession */
#define DIAG_NRC_SNSIAS		(0x7Fu)				/* serviceNotSupportedInActiveSession */





/* Negative Response Service ID */
#define DIAG_NR_SI						(0x7Fu)




#define DIAG_CAL_POSITIVE_SID(d)		((d) + 0x40u)



#define DIAG_SESSION_DEFAULT			(0x01u)
#define DIAG_SESSION_PROGRAMING			(0x02u)
#define DIAG_SESSION_EXTENDED			(0x04u)
#define DIAG_SESSION_ALL				(0xFFu)

/******************************************************************************
* MACRO DEFINITIONS
******************************************************************************/

/******************************************************************************
* TYPE DEFINITIONS
******************************************************************************/

/******************************************************************************
* GLOBAL VARIABLE PROTOTYPES
******************************************************************************/

/******************************************************************************
* FUNCTION PROTOTYPES
******************************************************************************/
extern void DiagResponseDoenEvent(uint8 err);
extern void DiagRequestFFIndiEvent(uint8 cnt);
extern void DiagRequestEvent(uint8 isFId, uint8 *data, uint16 cnt, uint8 err);


extern void DiagInit(void);
extern void DiagMgr(void);


extern uint8 DiagGetSession(void);

extern void DiagPostiveRespMsg(uint8 *data, uint16 dlc);
extern void DiagNegativeRespMsg(uint8 sid, uint8 nrc);




#endif	/* #ifndef __DIAG_H__ */
