/*
 * diag_dbi_if.h
 *
 *  Created on: 2020. 6. 10.
 *      Author: Digen
 */

#ifndef DIAG_DIAG_DBI_IF_H_
#define DIAG_DIAG_DBI_IF_H_

/******************************************************************************
* INCLUDE FILES
******************************************************************************/
#include "../includes.h"
/******************************************************************************
* CONSTANT DEFINITIONS
******************************************************************************/

/******************************************************************************
* MACRO DEFINITIONS
******************************************************************************/

/******************************************************************************
* TYPE DEFINITIONS
******************************************************************************/

/******************************************************************************
* GLOBAL VARIABLE PROTOTYPES
******************************************************************************/

typedef struct tagDiagIoCtrlCmd{
	uint8 diag_cmd;
	uint8 data;
} DiagIoCtrlCmd;


extern DiagIoCtrlCmd rDiagCmd;
/******************************************************************************
* FUNCTION PROTOTYPES
******************************************************************************/
extern void DIAG_WriteAntennaSignalStrength(uint16 value);
extern void DIAG_WriteSourceMode(uint8 value);
extern void DIAG_WriteVolume(uint8 value);
extern void DIAG_WriteBassStep(uint8 value);
extern void DIAG_WriteTrebleStep(uint8 value);
extern void DIAG_WriteFaderStep(uint8 value);
extern void DIAG_WriteBalanceStep(uint8 value);
extern void DIAG_WriteEQMode(uint8 value);
extern void DIAG_WriteBatteryVoltage(uint32 value);
extern void DIAG_WriteLoudspeakerFLcontrol(uint8 value);
extern void DIAG_WriteLoudspeakerFRcontrol(uint8 value);
extern void DIAG_WriteLoudspeakerRLcontrol(uint8 value);
extern void DIAG_WriteLoudspeakerRRcontrol(uint8 value);
extern void DIAG_WriteDisplayControl(uint8 value);
extern void DIAG_WriteVolumeControl(uint8 value);
extern void DIAG_WriteRadioSeek(uint8 value);
extern void DIAG_WriteOnAnimation(uint8 value);

extern void ReadAntennaSignalStrength(uint8* data, uint16* len);
extern void ReadSourceMode(uint8* data, uint16* len);
extern void ReadVolume(uint8* data, uint16* len);
extern void ReadBassStep(uint8* data, uint16* len);
extern void ReadTrebleStep(uint8* data, uint16* len);
extern void ReadBalanceStep (uint8* data, uint16* len);
extern void ReadFaderStep (uint8* data, uint16* len);
extern void ReadEQMode (uint8* data, uint16* len);
extern void ReadBatteryVoltage (uint8* data, uint16* len);
extern void ReadMicrophoneNum (uint8* data, uint16* len);
extern void ReadIHUVariantCoding (uint8* data, uint16* len);
extern void ReadSoftwareConfiguration (uint8* data, uint16* len);
extern void ReadDataResetInfo (uint8* data, uint16* len);
extern void ReadLoudspeakerFLcontrol (uint8* data, uint16* len);
extern void ReadLoudspeakerFRcontrol (uint8* data, uint16* len);
extern void ReadLoudspeakerRLcontrol (uint8* data, uint16* len);
extern void ReadLoudspeakerRRcontrol (uint8* data, uint16* len);
extern void ReadDisplayControl (uint8* data, uint16* len);
extern void ReadVolumeControl (uint8* data, uint16* len);
extern void ReadRadioSeek (uint8* data, uint16* len);
extern void ReadOnAnimation (uint8* data, uint16* len);
extern void ReadVehicleManufactSparePartNum (uint8* data, uint16* len);
extern void ReadVehicleManufactECUSWNum (uint8* data, uint16* len);
extern void ReadSystemSupplierId (uint8* data, uint16* len);
extern void ReadVIN (uint8* data, uint16* len);
extern void ReadVehicleManufactECUHwNum (uint8* data, uint16* len);
extern void ReadSystemSupplierECUSWNum (uint8* data, uint16* len);

extern void WriteSoftwareConfiguration(uint8* data, uint16 len);
extern void WriteVIN(uint8* data, uint16 len);
extern void WriteIHUVariantCoding(uint8* data, uint16 len);

extern void IoCtrlLoudspeakerFLcontrol(uint8 ioctrl);
extern uint8 IoRtnLoudspeakerFLcontrol(void);
extern void IoCtrlLoudspeakerFRcontrol(uint8 ioctrl);
extern uint8 IoRtnLoudspeakerFRcontrol(void);
extern void IoCtrlLoudspeakerRLcontrol(uint8 ioctrl);
extern uint8 IoRtnLoudspeakerRLcontrol(void);
extern void IoCtrlLoudspeakerRRcontrol(uint8 ioctrl);
extern uint8 IoRtnLoudspeakerRRcontrol(void);
extern void IoCtrlDisplayControl(uint8 ioctrl);
extern uint8 IoRtnDisplayControl(void);
extern void IoCtrlVolumeControl(uint8 ioctrl);
extern uint8 IoRtnVolumeControl(void);
extern void IoCtrlRadioSeek(uint8 ioctrl);
extern uint8 IoRtnRadioSeek(void);
extern void IoCtrlOnAnimation(uint8 ioctrl);
extern uint8 IoRtnOnAnimation(void);
extern void IoCtrlVolumeControl(uint8 ioctrl);
extern uint8 IoRtnVolumeControl(void);

#endif /* DIAG_DIAG_DBI_IF_H_ */
