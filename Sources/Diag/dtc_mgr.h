/*
 * dtc_mgr.h
 *
 *  Created on: 2020. 7. 2.
 *      Author: Digen
 */

#ifndef DIAG_DTC_MGR_H_
#define DIAG_DTC_MGR_H_

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#include "../includes.h"

/**********************************************************************************************************************
  DEFINE
**********************************************************************************************************************/

/**********************************************************************************************************************
  STRUCTURE
**********************************************************************************************************************/
typedef enum{
#if 0
//	DTC_POWER_OPEN = 0,		/* B1100_13 (910013)*/
//	DTC_POWER_LOW,			/* B1100_16 (910016)*/
//	DTC_POWER_HIGH,			/* B1100_17 (910017)*/
	
    DTC_BT_MODULE_FAILURE,	/* B1115_04 (911504)*/
    DTC_NAVI_MOUNT,                          /* B1116_76 (911676)*/
//	DTC_DMS_SDK_LICENSE,	/* B181A_04 (981A04)*/
//	DTC_DMS_HARWARE,		/* B181B_04 (981B04)*/
	
    DTC_BUS_OFF_FAULT,                       /* U0073_88 (C07388)*/

    DTC_LOS_COM_CEM,                         /* U0140_87 (C14087)*/
    DTC_LOS_COM_IPM,                         /* U0145_87 (C14587)*/
    DTC_LOS_COM_ICM,                         /* U0155_87 (C15587)*/
    #ifdef DTC_LIST_AVMCOM
    DTC_LOS_COM_AVM,						/* U0253_87 (C25387)*/
    #endif

    DTC_SW_CONFIG_ERROR,                     /* U1300_55 (D30055)*/ 
#endif
  DTC_VOLT_BELOW        ,    /* B1500-16 (950016) */
  DTC_VOLT_ABOVE        ,    /* B1501-16 (950116) */
  DTC_SPK_CNECT_FAIL    ,    /* B1505-00 (950500) */
  DTC_AMP_FAIL          ,    /* B1507-04 (950704) */
  DTC_Tuner_IC_FAIL     ,    /* B1508-04 (950804) */
  DTC_Voice_IC_FAIL     ,    /* B1509-04 (950904) */
  DTC_MCU_SOC_COM_FAIL  ,    /* B150A-00 (950A00) */
  DTC_SOC_OVERTEMP      ,    /* B150B-98 (950B98) */
  DTC_USB1_CURRENT_ABOVE,    /* B150C-19 (950C19) */
  DTC_USB2_CURRENT_ABOVE,    /* B150D-19 (950D19) */
  DTC_SOC_DES_COM_FAIL  ,    /* B150E-04 (950E04) */
  DTC_SOC_TFT_COM_FAIL  ,    /* B150F-04 (950F04) */
  DTC_SOFT_CONFIG_Error ,    /* B1510-55 (951055) */
  DTC_BUS_OFF_FAULT     ,    /* U3300-88 (F30088) */

  DTC_LOS_COM_VCU       ,    /* U3310-87 (F31087) */
  DTC_LOS_COM_BMS       ,    /* U3311-87 (F31187) */
  DTC_LOS_COM_BCM       ,    /* U3312-87 (F31287) */
  DTC_LOS_COM_ICM       ,    /* U3314-87 (F31487) */
  DTC_LOS_COM_SAS       ,    /* U3315-87 (F31587) */
  DTC_LOS_COM_ACCM      ,    /* U3317-87 (F31787) */
  DTC_LOS_COM_SCM_R     ,    /* U3318-87 (F31887) */
  DTC_LOS_COM_AVAS      ,    /* U3319-87 (F31987) */
  DTC_LOS_COM_BSD       ,    /* U331C-87 (F31C87) */
  DTC_LOS_COM_FCM       ,    /* U331E-87 (F31E87) */
  DTC_LOS_COM_TBOX      ,    /* U3320-87 (F32087) */
  DTC_LOS_COM_MFS       ,    /* U3321-87 (F32187) */
  DTC_LOS_COM_ABM       ,    /* U3322-87 (F32287) */
  DTC_LOS_COM_WCM       ,    /* U3323-87 (F32387) */

  DTC_INDEX_MAX
}tDtcMgrIdListType;

typedef enum{
    DTC_NO_TEST,
    DTC_FAILED,
    DTC_PASSED
}tDtcMgrResultType;




#define DIAG_ROM_SIZE_SECTOR	( 0x00001000u )			//  4 KB
// #define DIAG_ROM_ADDR_START		( 0x00010000u )			// 64 KB (Total 4 MB)
/* wl20220819 */
#define DIAG_ROM_ADDR_START		( 0x00000100u )			// (Total 32KB)
#define DIAG_ROM_ADDR_SC		( DIAG_ROM_ADDR_START )
#define DIAG_ROM_ADDR_VIN		( DIAG_ROM_ADDR_SC    + DIAGDBI_SOFTWARE_CONFIGURATION_LEN )
/* wl20220730 */
#define DIAG_ROM_ADDR_VCODING   ( DIAG_ROM_ADDR_VIN    + DIAGDBI_VIM_LEN)
#define DIAG_ROM_NVM_LEN   ( DIAGDBI_SOFTWARE_CONFIGURATION_LEN + DIAGDBI_VIM_LEN + DIAGDBI_IHUVariantCoding_LEN)

#define DIAG_ROM_ADDR_RESET_INFO   ( 0x00000150u)
#define DIAG_ROM_RESET_INFO_LEN    ( 64u)

// #define DIAG_ROM_ADDR_DTC		( DIAG_ROM_ADDR_START + DIAG_ROM_SIZE_SECTOR )	// 68 KB
/* wl20220819 */
#define DIAG_ROM_ADDR_DTC		( DIAG_ROM_ADDR_START + 0x100)

#define DIAG_ROM_SIZE_DTC_MAX	( 1024u ) // Shoult be bigger than sizeof(gDtcMgrDataList)

#define DTC_DATA_READ()							DIAG_ReadDataDTCFromNVM()
/* wl20220802 */
#define DID_DATA_READ()							DIAG_ReadDataSCFromNVM();\
                                    DIAG_ReadDataVINFromNVM();\
                                    DIAG_ReadDataVCODINGFromNVM()

#define SYS_NVM_UPDATE_DTC_DATA(data, length)	DIAG_WriteDataDTCToNVM(data, length)
/**********************************************************************************************************************
  FUNCTION
**********************************************************************************************************************/


/* DTC_DATA_READ() */
uint8 *DIAG_ReadDataDTCFromNVM(void);
/* SYS_NVM_UPDATE_DTC_DATA(data, length) */
void DIAG_WriteDataDTCToNVM(uint8 *data, uint32 length);
void DIAG_ReadDataSCFromNVM(void);
void DIAG_ReadDataVINFromNVM(void);
void DIAG_ReadDataVCODINGFromNVM(void);
void DIAG_WriteDataSCnVINToNVM(void);
void WriteDataResetInfoToNVM(uint8* u8ErrCode);
void EraseFRAM(void);
uint8 DIAG_CheckValidationSC(void);

extern void DtcMgrInit(void);
extern void DtcMgrSetDtcStatus(tDtcMgrIdListType dtcId, tDtcMgrResultType result);
extern uint8 DtcMgrGetDtcFailedState(tDtcMgrIdListType dtcIdx);
extern void DtcMgrAgingProcess(void);  // Should be called when entering shutdown.
uint32 DtcMgrReadInfoReqProcess(uint8 dtcStatusMask, uint8* array, uint32 arraySize);
uint32 DtcMgrReadInfoSupportedDtcReqProcess(uint8 dtcStatusMask, uint8* array, uint32 arraySize);
	
extern uint8 DtcMgrGetAvailStsMask(void);

extern uint8 DtcMgrClearDtcReqProcess(uint32 groupOfDTC);
extern uint8 DtcMgrGroupClearDtcReqProcess(uint32 groupOfDTC);
extern void DtcMgrAllClearDtcReqProcess(void);


extern uint8 DtcMgrGetDtcFailedAndFailedThisOp(tDtcMgrIdListType dtcIdx); // If fail is occured, This function should be called.

extern uint8 DtcMgrChkDtcSetting(void);
extern uint8 DtcMgrChkDtcConfirmed(void);

extern uint8* GetDtcMgrDataPtr(void);
extern uint16 GetDtcMgrDataSize(void);
extern void   NvmDataInit(void);
extern void WriteDTCData(void);

#endif /* DIAG_DTC_MGR_H_ */
