/*****************************************************************************
* INCLUDE FILES
*****************************************************************************/
#include <stdlib.h>

#include "diag.h"
#include "diag_dbi.h"

#include "mnt_flt.h"
#include "dtc_mgr.h"

#include "Service/std_util.h"
#include "Service/std_timer.h"
#include "CanTp/srv_cantp.h"
#include "Service/srv_mcan.h"

/*****************************************************************************
* MACRO DEFINITIONS
*****************************************************************************/
/*****************************************************************************
* CONSTANT DEFINITIONS
*****************************************************************************/

#define DIAG_LOG						(1)

#define DIAG_SIG_SET(s)					UTL_MASK_SET(gDiagSignal, (s))
#define DIAG_SIG_CLR(s)					UTL_MASK_CLR(gDiagSignal, (s))
#define DIAG_SIG_IS(s)					UTL_MASK_IS(gDiagSignal, (s))

#define DAIG_SESSION_IS_SUPPORT(m)		((gDiagSessionMask & (m)) != 0)
#define DAIG_SA_IS_SUPPORT(m)			((gDiagSAMask & (m)) != 0)


#define DAIG_GET_SID(d)					((d)[0])
#define DIAG_GET_POSITIVE_SID(d)		DIAG_CAL_POSITIVE_SID(DAIG_GET_SID(d))



#define DIAG_GET_1BYTE(d, s, i)		(d) = ((s)[(i)]);		(i)++
#define DIAG_PUT_1BYTE(d, s, i)		(d)[(i)] = (s);			(i)++


#define DIAG_GET_2BYTE(d, s, i)		(d) =  ((uint16)((s)[(i)]) << 8);			(i)++; \
									(d) |=  ((uint16)(s)[(i)]);					(i)++

#define DIAG_PUT_2BYTE(d, s, i)		(d)[(i)] = (uint8)(((s) >> 8) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)((s) & 0xFFu);			(i)++


#define DIAG_GET_3BYTE(d, s, i)		(d) =  ((uint32)((s)[(i)]) << 16);			(i)++; \
									(d) |=  ((uint32)((s)[(i)]) << 8);			(i)++; \
									(d) |=  ((uint32)(s)[(i)]);					(i)++

#define DIAG_PUT_3BYTE(d, s, i)		(d)[(i)] = (uint8)(((s) >> 16) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)(((s) >> 8) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)((s) & 0xFFu);			(i)++


#define DIAG_GET_4BYTE(d, s, i)		(d) =  ((uint32)((s)[(i)]) << 24);			(i)++; \
									(d) |=  ((uint32)((s)[(i)]) << 16);			(i)++; \
									(d) |=  ((uint32)((s)[(i)]) << 8);			(i)++; \
									(d) |=  ((uint32)(s)[(i)]);					(i)++

#define DIAG_PUT_4BYTE(d, s, i)		(d)[(i)] = (uint8)(((s) >> 24) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)(((s) >> 16) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)(((s) >> 8) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)((s) & 0xFFu);			(i)++



#define DIAG_UDS_SUPPRESS_RESP_MASK						(0x80u)		/* suppressPosRspMsgIndicationBit = TRUE */
#define DIAG_KWP_TESTER_PRESENT_NO_RESP_MASK			(0x02u)		/* KWP200 Tester present no response */



#define DIAG_GET_TRANFER_DATA(d, i)			(&((d)[i]))

#define DIAG_P2_TIME					((uint16)50u)			/* 50ms : P2server time out for response message */
#define DIAG_P2_STAR_TIME				((uint16)5000u)			/* 5000 ms : P2*server time out for response message after pending message */
#define DIAG_P2_STAR_TIME_RES			((uint16)10u)			/* 10 ms Resolution*/


#define DIAG_PENDING_TIME				((uint16)1500u)			/* 1500 ms : 0.3 * P2*server time */

#define DIAG_S3_SERVER_TIME				(5000u)					/* 5000 ms : S3server time out Default Session active while not receving any request message */

#define DIAG_SA_TIME					(10000u)				/* 10s * 1000 ms : SA key Fail */


#define DIAG_SIGNAL_REQ					(0x0001u)
#define DIAG_SIGNAL_RESP_DONE			(0x0002u)
#define DIAG_SIGNAL_FF_INDI				(0x0004u)
#define DIAG_SIGNAL_ERR					(0x0008u)



#define DIAG_SA_LEVLE0					(0x01u)
#define DIAG_SA_LEVLE1					(0x02u)
#define DIAG_SA_LEVLE2					(0x04u)
#define DIAG_SA_ALL						(0xFFu)

#define DIAG_SAT_RSD_DLC				(2u)
#define DIAG_SAT_SK_DLC					(4u)

#define DIAG_SAT_RSD					(0x03u)			/* RequestSeed to unlock the ECU */
#define DIAG_SAT_SK						(0x04u)			/* SendKey to unlock the ECU */

#define DIAG_SAT_PBL_RSD				(0x05u)			/* RequestSeed to unlock the ECU */
#define DIAG_SAT_PBL_SK					(0x06u)			/* SendKey to unlock the ECU */

#define DIAG_CC_CTRL_TYPE_EN_RX_EN_TX	(0x00)			/* 0x00 enableRxAndTx */
#define DIAG_CC_CTRL_TYPE_EN_RX_DI_TX	(0x01)			/* 0x01 enableRxAndDisableTx */

#define DIAG_CC_CTRL_TYPE_DI_RX_EN_TX	(0x02)			/* 0x02 disableRxAndEnableTx */
#define DIAG_CC_CTRL_TYPE_DI_RX_DI_TX	(0x03)			/* 0x03 disableRxAndTx */

#define DIAG_CC_COM_TYPE_RESERVED		(0x00u)			/* 0x00 Reserved */
#define DIAG_CC_COM_TYPE_NORMAL			(0x01u)			/* 0x01 Normal Communication Message */
#define DIAG_CC_COM_TYPE_NM				(0x02u)			/* 0x02 Network Management Communication Message */
#define DIAG_CC_COM_TYPE_NORMAL_NM		(0x03u)			/* 0x03 Normal / Network Management Communication Message */


#define DIAG_HARD_RESET					(0x01)			/* HardReset */
#define DIAG_SOFT_RESET					(0x03)			/* SoftReset */


#define DIAG_DTC_ALL_GROUPS					(0x00FFFFFFu)		/* DTC clear: All Groups */
#define DIAG_DTC_GROUPS_MASK				(0xFF3FFFFFu)		/* DTC clear: Groups Mask */



#define DIAG_DTC_REPORT_TYPE_STATUS_DTCBYSTATUS_MASK	(0x02u)			/* DTC read reportType: reportDTCByStatusMask */
#define DIAG_DTC_REPORT_TYPE_STATUS_SUPPORTEDDTC_MASK	(0x0Au)			/* DTC read reportType: reportSupportedDTC */
#define DIAG_DTC_DISABLE_STATUS_MASK		(0xF6u)			/* DTC read DTCStatusMask: Disable status mask */


#define DIAG_DTC_READ_STATUS_RECORD_POS		(3u)			/* StatusRecord data position*/


#define DIAG_DTC_SETTING_ON			(0x01u)
#define DIAG_DTC_SETTING_OFF		(0x02u)

#define DIAG_SA_NUM_MAX			(3u)

#define DIAG_SA_MAGIC_KEY			(0xFD6Cu)

#define SYS_CANTP_DATA_LEN_MAX				SRVCANTP_DATA_LEN_MAX
#define	SYS_CANTP_RESP(d, c)				SrvCanResp(d, c)

#define SRV_CANTP_REQ_LOCK()		SrvCanTpReqLock(TRUE)
#define SRV_CANTP_REQ_UNLOCK()		SrvCanTpReqLock(FALSE)

#define HARD_RESET()			SystemSoftwareReset()	

#if 0
#define DIAG_SET_SESSION_CTRL_MASK(data)		SrvNvmWrite(SRVNVM_NVM_SECTOR2_ADDR, (uint8 *)&(data), sizeof(tDiagSessionDataType))	
#define DIAG_GET_SESSION_CTRL_MASK()			SrvNvmGetData(SRVNVM_DATATYPE_SESSION_CHANGE)
#define DIAG_CLR_SESSION_CTRL_MASK()			SrvNvmErase(SRVNVM_NVM_SECTOR2_ADDR, 1)
#endif

#define INFO_REQ_STAY_IN_BOOT_MASK				(0x44474E6Eu)
#define INFO_REQ_STAY_IN_BOOT_NO_RESP_MASK		(0x44474E7Au)

#define INFO_REQ_DEFAULT_RESP_MASK				(0x44474ED2u)



/*****************************************************************************
* TYPE DEFINITIONS
*****************************************************************************/
typedef struct {
    uint8* Data;
    uint16 Length;
    uint8 isFId;
} hDiagMessageType;

typedef struct
{
	uint8 serviceID;
	uint16 minDLC;
	uint16 dlc;
	uint8 sessionMask;
	uint8 saMask;
	uint8 functional;
	uint8 (*service)(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
} DIAG_SVR;

typedef struct{
	uint32 mask;
	uint8 comCtrl;
	uint8 reserved1;
	uint8 reserved2;
	uint8 reserved3;
}tDiagSessionDataType;

enum {
	DIAG_SERVICE_DiagnosticSessionControl,
	DIAG_SERVICE_ECUReset,
	DIAG_SERVICE_DTCClear,
	DIAG_SERVICE_DTCRead,
	DIAG_SERVICE_ReadDataByIdentifier,
	DIAG_SERVICE_SecurityAccess,
	DIAG_SERVICE_CommunicationControl,
	DIAG_SERVICE_WriteDataByIdentifier,
	DIAG_SERVICE_IOControl,
	//DIAG_SERVICE_RequestDownload,
	//DIAG_SERVICE_TransferData,
	//DIAG_SERVICE_RequestTransferExit,
	DIAG_SERVICE_TesterPresent,
	DIAG_SERVICE_ControlDTCSetting,
	DIAG_SERVICE_MAX,
};


/* DiagServiceDiagnosticSessionControl */
enum {
	SESSION_RESERVED,
	SESSION_DEFAULT,
	SESSION_PROGRAMMING,
	SESSION_EXTENDED,
	SESSION_MAX
};


/* diag_serviceRoutineControl */
enum {
	ROUTINE_RESERVED,
	ROUTINE_START,
	ROUTINE_STOP,
	ROUTINE_RESULT,
	ROUTINE_MAX
};

enum {
	ROUTINE_STATUS_SUCCESS = 0x04u,			/* 0x04 Routine Completed successfully */
	ROUTINE_STATUS_ERROR,					/* 0x05 General Error */
	ROUTINE_STATUS_NOT_ALL_MBP,				/* 0x06 Not all mandatory blocks present */
	ROUTINE_STATUS_HW_INCOMPATIBILITY,		/* 0x07 Hw incompatibility */
	ROUTINE_STATUS_SW_INCOMPATIBILITY,		/* 0x08 Sw incompatibility */
	ROUTINE_STATUS_MAX
};


enum {
	PENDING_NONE,
	PENDING_NORMAL,
	/* PENDING_ERASE, */
	PENDING_MAX
};




/*****************************************************************************
* EXTERNAL VARIABLE DECLARALATIONS
*****************************************************************************/
/*****************************************************************************
* FUNCTION PROTOTYPES
*****************************************************************************/
extern void DiagInitProgramming(void);
extern void DiagGenSA(uint16 *seed, uint16 *key);

extern void DiagGenNegativeMsg(hDiagMessageType *respMsg, uint8 sid, uint8 nrc);
extern void DiagReleaseRequestMsg(void);



extern uint8 DiagServicesAction(const DIAG_SVR *diagSvr, hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern void DiagServices(void);
extern void DiagResponseDone(void);


extern void DiagSetSessionDefault(void);


extern uint8 DiagSubSessionDefault(uint8 *currentSessionMask);
extern uint8 DiagSubSessionProgramming(uint8 *currentSessionMask);
extern uint8 DiagSubSessionExtended(uint8 *currentSessionMask);
extern uint8 DiagServiceDiagnosticSessionControl(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceECUReset(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagServiceDTCClear(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagServiceDTCRead(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);


extern uint8 DiagSubRdbiHWVer(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubRdbiSerialNum(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubRdbiSWDLVer(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubRdbiBootSWVer(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubRdbiAppSWVer(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagServiceReadDataByIdentifier(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagSubSATSeed(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubSATKey(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubSATPBLSeed(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubSATPBLKey(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagServiceSecurityAccess(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceCommunicationControl(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceWriteDataByIdentifier(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagSubRcCheckSum(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubRcEraseFlashSector(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);
extern uint8 DiagSubRcCheckDependencies(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceIOControl(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceRequestDownload(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceTransferData(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceRequestTransferExit(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceTesterPresent(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);

extern uint8 DiagServiceControlDTCSetting(hDiagMessageType *respMsg, hDiagMessageType *reqMsg);



extern uint8 DiagIsDone(void);

/*****************************************************************************
* GLOBAL VARIABLE DEFINITIONS AND PROTOTYPES
*****************************************************************************/
/******************************************************************************
* STATIC VARIABLE DEFINITIONS
******************************************************************************/
static uint16 gDiagSignal = 0;
static uint8 gDiagSessionMask = DIAG_SESSION_DEFAULT;
static uint8 gDiagSAMask = DIAG_SA_LEVLE0;
static uint16 gDiagSAKey = 0;

static uint8 gDiagNoResp = FALSE;
/*static*/ uint8 gDiagHardReset = FALSE;


static stTimerType gDiagSessionTimer;  //S3server Timer
static stTimerType gDiagPendingTimer; //P2CAN*_Server Timer

static stTimerType gDiagSATimer; //Security Access Timer
static uint8 gDiagSAFailCnt = 0;

static uint8 gDiagPendingReason = 0;


static uint8 gDiagPendingState = 0;
static uint8 gDiagPendingDone = 0;



static uint8 gDiagResponseBuffer[SYS_CANTP_DATA_LEN_MAX];
static hDiagMessageType gDiagResponseMsg = {
	gDiagResponseBuffer, 			/* Data */
	sizeof(gDiagResponseBuffer), 	/* Size */
};
static hDiagMessageType gDiagRequestMsg;

const DIAG_SVR gDiagServices[DIAG_SERVICE_MAX] = {
	/* 	serviceID	minDLC 	dlc 		sessionMask 										saMask 			functional		service function */
	{	0x10u, 		2u,		2u,			DIAG_SESSION_ALL,									DIAG_SA_ALL,	TRUE, 			DiagServiceDiagnosticSessionControl		}, /* DIAG_SERVICE_DiagnosticSessionControl */
	{	0x11u, 		2u,		2u,			(DIAG_SESSION_PROGRAMING | DIAG_SESSION_EXTENDED),  DIAG_SA_ALL,	FALSE, 			DiagServiceECUReset						}, /* DIAG_SERVICE_ECUReset */
	{	0x14u,		4u, 	4u, 		(DIAG_SESSION_DEFAULT | DIAG_SESSION_EXTENDED),		DIAG_SA_ALL,	TRUE, 			DiagServiceDTCClear 					}, /* DIAG_SERVICE_DTCClear */

	{	0x19u,		2u, 	3u, 		(DIAG_SESSION_DEFAULT | DIAG_SESSION_EXTENDED),		DIAG_SA_ALL,	FALSE,			DiagServiceDTCRead						}, /* DIAG_SERVICE_DTCRead */

	{	0x22u, 		3u,		3u,			DIAG_SESSION_ALL,									DIAG_SA_ALL,	FALSE, 			DiagServiceReadDataByIdentifier			}, /* DIAG_SERVICE_ReadDataByIdentifier */
	{	0x27u, 		2u,		0u,			(DIAG_SESSION_PROGRAMING | DIAG_SESSION_EXTENDED),	DIAG_SA_ALL,	FALSE, 			DiagServiceSecurityAccess				}, /* DIAG_SERVICE_SecurityAccess */
	{	0x28u, 		3u,		3u,			DIAG_SESSION_EXTENDED,								DIAG_SA_ALL,	TRUE, 			DiagServiceCommunicationControl			}, /* DIAG_SERVICE_CommunicationControl */
	{	0x2Eu,		4u, 	0u, 		DIAG_SESSION_EXTENDED,								DIAG_SA_LEVLE2,	FALSE, 			DiagServiceWriteDataByIdentifier		}, /* DIAG_SERVICE_WriteDataByIdentifier */

	{	0x2Fu,		4u, 	0u, 		DIAG_SESSION_EXTENDED,								DIAG_SA_LEVLE2,	FALSE,			DiagServiceIOControl					}, /* DIAG_SERVICE_IOControl */
		
	{	0x3Eu,		2u, 	2u, 		DIAG_SESSION_ALL,									DIAG_SA_ALL,	TRUE, 			DiagServiceTesterPresent				}, /* DIAG_SERVICE_TesterPresent */
		
	{	0x85u,		2u, 	2u, 		DIAG_SESSION_EXTENDED,								DIAG_SA_ALL,	TRUE, 			DiagServiceControlDTCSetting			}, /* DIAG_SERVICE_ControlDTCSetting */

};

const uint8 gDiagPendingId[PENDING_MAX] = {
	0x00u, 	/* PENDING_NONE */
	/*0x31u, 	 PENDING_ERASE */
};

/*****************************************************************************
* PROGRAM BODY
*****************************************************************************/
/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagResponseDoenEvent(uint8 err)
{
	if(FALSE == err)
	{
		DIAG_SIG_SET(DIAG_SIGNAL_RESP_DONE);
	}
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagRequestFFIndiEvent(uint8 cnt)
{
	UNUSED_V(cnt);

	DIAG_SIG_SET(DIAG_SIGNAL_FF_INDI);
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagRequestEvent(uint8 isFId, uint8 *data, uint16 cnt, uint8 err)
{
	if(NULL != data)
	{
		gDiagRequestMsg.Data = data;
		gDiagRequestMsg.Length = cnt;
		gDiagRequestMsg.isFId = isFId;

		if(FALSE == err)
		{
			/* set event */
			DIAG_SIG_SET(DIAG_SIGNAL_REQ);
		}
		else
		{
			/* set event */
			DIAG_SIG_SET(DIAG_SIGNAL_ERR);
		}
	}
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagInit(void)
{
	uint8 idx = 0;
	hDiagMessageType *respMsg = NULL;
	uint8 session = 0;
	//tDiagSessionDataType *sessionCtrlData = NULL;
	uint8 requestFromApp = FALSE;
	
	DiagDbiInit();

	//DiagInitProgramming();

	gDiagSignal = 0;
	gDiagSAMask = DIAG_SA_LEVLE0;
	gDiagSAKey = 0;

	gDiagSAFailCnt = 0;

	STOP_TIMER(gDiagSessionTimer);

	//sessionCtrlData = (tDiagSessionDataType *)DIAG_GET_SESSION_CTRL_MASK();

#if 0
	if(INFO_REQ_STAY_IN_BOOT_MASK == sessionCtrlData->mask)
	{
		/* To do... SetCom Control */
		requestFromApp = TRUE;
	}
#endif
	if(requestFromApp != TRUE)
	{
		DPRINTF(DBG_MSG7, "[DIAG] Init Default Session\n");
		gDiagSessionMask = DIAG_SESSION_DEFAULT;

#if 0
		if(sessionCtrlData->mask == INFO_REQ_DEFAULT_RESP_MASK)
		{
			respMsg = &gDiagResponseMsg;
			session = SESSION_DEFAULT;
		}
#endif
	}
	else
	{/* send positive message with programming session */
		DPRINTF(DBG_MSG7, "[DIAG] Init Programming Session\n");

		gDiagSessionMask = DIAG_SESSION_PROGRAMING;

		respMsg = &gDiagResponseMsg;
		session = SESSION_PROGRAMMING;
	}

	//DIAG_CLR_SESSION_CTRL_MASK();

	if(NULL != respMsg)
	{
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_CAL_POSITIVE_SID(gDiagServices[DIAG_SERVICE_DiagnosticSessionControl].serviceID), idx);
		DIAG_PUT_1BYTE(respMsg->Data, session, idx);

		/* P2 server time */
		DIAG_PUT_2BYTE(respMsg->Data, DIAG_P2_TIME, idx);
		/* P*2 server time */
		DIAG_PUT_2BYTE(respMsg->Data, (DIAG_P2_STAR_TIME/DIAG_P2_STAR_TIME_RES), idx);

		respMsg->Length = idx;

		/* send response message */
		SYS_CANTP_RESP(gDiagResponseMsg.Data, gDiagResponseMsg.Length);
	}
	
	/* RX / TX enable */
	SrvMcanComCtrl(SRVMCAN_RX_ALL_ENABLE_IDX);
	SrvMcanComCtrl(SRVMCAN_TX_ALL_ENABLE_IDX);
	/* wl20221014 */
	Can_SetComSendState(1);
	Can_SetComRecvState(1);
	Can_SetNMSendState(1);
	Can_SetNMRecvState(1);

}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagInitProgramming(void)
{

}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagGenSA(uint16 *seed, uint16 *key)
{
	#define TOPBIT 0x8000
	#define POLYNOM_1 0x8408
	#define POLYNOM_2 0x8025
	#define BITMASK 0x0080
	#define INITIAL_REMINDER 0xFFFE
	#define MSG_LEN 2

	uint8 bSeed[2];
	uint16 remainder = 0;
	uint8 n = 0;
	uint8 i = 0;

	*seed = rand();

	bSeed[0] = (uint8)(*seed >> 8);
	bSeed[1] = (uint8)(*seed & 0xFF);
	remainder = INITIAL_REMINDER;

	for(n = 0 ; n < MSG_LEN ; n++)
	{
		/* Bring the next bye into the remainder */
		remainder ^= ((bSeed[n]) << 8);

		/* Perform modulo-2 division, a bit at a time */
		for(i = 0 ; i < 8 ; i++)
		{
			/* Try to divide the current data bit */
			if(remainder & TOPBIT)
			{
				if(remainder & BITMASK)
				{
					remainder = (remainder << 1) ^ POLYNOM_1;
				}
				else
				{
					remainder = (remainder << 1) ^ POLYNOM_2;
				}
			}
			else
			{
				remainder = (remainder << 1);
			}
		}
	}
	/* The final remainder is the key */
	*key = remainder;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagDoPendingResp(void)
{

	/* negative response */
	DiagGenNegativeMsg(&gDiagResponseMsg, gDiagPendingId[gDiagPendingState], DIAG_NRC_RCRRP);

	/* send response message */
	SYS_CANTP_RESP(gDiagResponseMsg.Data, gDiagResponseMsg.Length);
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagGenNegativeMsg(hDiagMessageType *respMsg, uint8 sid, uint8 nrc)
{
	uint8 idx = 0;

	respMsg->Data[idx] = DIAG_NR_SI;					idx++;
	respMsg->Data[idx] = sid;							idx++;
	respMsg->Data[idx] = nrc;							idx++;
	respMsg->Length = idx;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagReleaseRequestMsg(void)
{
	gDiagNoResp = FALSE;

	gDiagRequestMsg.Data = NULL;
	gDiagRequestMsg.Length = 0;
	gDiagRequestMsg.isFId = FALSE;

	SRV_CANTP_REQ_UNLOCK();
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServicesAction(const DIAG_SVR *diagSvr, hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	if((TRUE == reqMsg->isFId) && (FALSE == diagSvr->functional))
	{
		/* no action */
		gDiagNoResp = TRUE;
	}
	else if(!(DAIG_SESSION_IS_SUPPORT(diagSvr->sessionMask)))
	{
		/* Negative serviceNotSupportedInActiveSession 0x7F */
		nrc = DIAG_NRC_SNSIAS;
	}
	else if(diagSvr->minDLC > reqMsg->Length)
	{
		/* Negative incorrectMessageLengthOrInvalidFormat 0x13 */
		nrc = DIAG_NRC_IMLOIF;
	}
//	else if((0 != diagSvr->dlc) && (diagSvr->dlc != reqMsg->Length))
//	{
		/* Negative incorrectMessageLengthOrInvalidFormat 0x13 */
//		nrc = DIAG_NRC_IMLOIF;
//	}
	else if(!(DAIG_SA_IS_SUPPORT(diagSvr->saMask)))
	{
		/* Negative securityAccessDenied 0x33 */
		nrc = DIAG_NRC_SAD;
	}
	else
	{
		/* action service */
		nrc = diagSvr->service(respMsg, reqMsg);
	}

	return nrc;
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagServices(void)
{
	uint8 idx;
	uint8 nrc = DIAG_NRC_SNS; /* Negative serviceNotSupported 0x11 */

	#if 0 //S3 should start the condition that no default session and Diag Response Finish(Do not any action in non default session)
	if(gDiagSessionMask != DIAG_SESSION_DEFAULT)
	{
		START_TIMER(gDiagSessionTimer, DIAG_S3_SERVER_TIME);
	}
	#endif
	
	for(idx = 0; idx < DIAG_SERVICE_MAX; idx ++)
	{
		if(gDiagServices[idx].serviceID == DAIG_GET_SID(gDiagRequestMsg.Data))
		{
			nrc = DiagServicesAction(&(gDiagServices[idx]), &gDiagResponseMsg, &gDiagRequestMsg);
			break;
		}
	}

	if(nrc != DIAG_NRC_NO_ERROR)
	{
		/* negative response */
		DiagGenNegativeMsg(&gDiagResponseMsg, DAIG_GET_SID(gDiagRequestMsg.Data), nrc);
	}

	if(gDiagNoResp != TRUE)
	{
		if((gDiagRequestMsg.isFId == FALSE) ||
			((DIAG_NRC_SNS != nrc) && (DIAG_NRC_SFNS != nrc) &&
				(DIAG_NRC_ROOR != nrc) && (DIAG_NRC_SNSIAS != nrc) &&
				(DIAG_NRC_SFNSIAS != nrc)))
		{
			if(DIAG_NRC_RCRRP == nrc)
			{
				gDiagPendingReason = PENDING_NORMAL;
			}
			/* send response message */
			SYS_CANTP_RESP(gDiagResponseMsg.Data, gDiagResponseMsg.Length);
		}
	}
	else
	{
		if(gDiagHardReset == TRUE)
		{
			/* real hard reset */
			save_last_memory();//JANG_211203
			RecordReset(0x82,NULL);
			delay_ms(100);
			HARD_RESET();
		}
	}

	DiagReleaseRequestMsg();
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagResponseDone(void)
{
	DPRINTF(DBG_MSG7, "[CAN_TP] Resp Done\n");
	if(gDiagSessionMask != DIAG_SESSION_DEFAULT)
	{
// CHERY SPEC (DIAG)		DPRINTF(DBG_MSG7, "[DIAG] S3 Timer Start\n");
		/* wl20220802 */
		START_TIMER(gDiagSessionTimer, DIAG_S3_SERVER_TIME);
	}

	if(gDiagHardReset == TRUE)
	{
		/* real hard reset */
		save_last_memory();//JANG_211203
		RecordReset(0x81,NULL);
		delay_ms(100);
		HARD_RESET();
	}

	if(PENDING_NONE != gDiagPendingReason)
	{
		gDiagPendingState = gDiagPendingReason;
		gDiagPendingReason = PENDING_NONE;

		gDiagPendingDone = FALSE;
		/* start Pending timer */
		START_TIMER(gDiagPendingTimer, DIAG_PENDING_TIME);
	}

	/* wl20220811 */
	memset(gDiagResponseMsg.Data, 0u, gDiagResponseMsg.Length);
	gDiagResponseMsg.Length = 0u;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagPendingState(void)
{
	if(PENDING_NONE != gDiagPendingState)
	{
		if(PENDING_NORMAL == gDiagPendingState)
		{
			gDiagPendingState = DiagIsDone();
		}
		else
		{
			/* none */
		}

		if(PENDING_NONE == gDiagPendingState)
		{
			STOP_TIMER(gDiagPendingTimer);
		}
	}
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagIsDone(void)
{
	uint8 state = PENDING_NONE;

	if(TRUE != gDiagPendingDone)
	{
		state = PENDING_NORMAL;
	}

	return state;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagGetSession(void)
{
	uint8 session;

	session = gDiagSessionMask;
	if(DIAG_SESSION_EXTENDED == session)
	{
		session --;
	}

	return session;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagPostiveRespMsg(uint8 *data, uint16 dlc)
{
	UTL_COPY_DATA(gDiagResponseMsg.Data, data, dlc);

	gDiagResponseMsg.Length = dlc;

	/* send response message */
	SYS_CANTP_RESP(gDiagResponseMsg.Data, gDiagResponseMsg.Length);

	gDiagPendingDone = TRUE;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagNegativeRespMsg(uint8 sid, uint8 nrc)
{
	DiagGenNegativeMsg(&gDiagResponseMsg, sid, nrc);

	/* send response message */
	SYS_CANTP_RESP(gDiagResponseMsg.Data, gDiagResponseMsg.Length);
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagSetSessionDefault(void)
{
	/* stop ioctrl*/
	DiagDbiIORtnAll();

	SrvMcanComCtrl(SRVMCAN_RX_ALL_ENABLE_IDX);
	SrvMcanComCtrl(SRVMCAN_TX_ALL_ENABLE_IDX);
	
	MntFltSetMntDisabled(FALSE);

	DPRINTF(DBG_MSG7, "[DIAG] Chenaged to Default Session...\n");
	gDiagSAMask = DIAG_SA_LEVLE0;

	gDiagSessionMask = DIAG_SESSION_DEFAULT;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
void DiagMgr(void)
{
	uint8 timerStatus;

	if(0 != gDiagSignal)
	{
		if(DIAG_SIG_IS(DIAG_SIGNAL_REQ))
		{
			DIAG_SIG_CLR(DIAG_SIGNAL_REQ);

			/* event request from Tester */
			if(gDiagRequestMsg.Data != NULL)
			{
				DiagServices();
			}
		}

		if(DIAG_SIG_IS(DIAG_SIGNAL_RESP_DONE))
		{
			DIAG_SIG_CLR(DIAG_SIGNAL_RESP_DONE);
			/* event request from CAN Driver */
			DiagResponseDone();
		}

		if(DIAG_SIG_IS(DIAG_SIGNAL_FF_INDI))
		{
			DIAG_SIG_CLR(DIAG_SIGNAL_FF_INDI);

			/* wl20220802 */
			#if 1
			START_TIMER(gDiagSessionTimer, DIAG_S3_SERVER_TIME);
			#else
			STOP_TIMER(gDiagSessionTimer);
			#endif
		}

		if(DIAG_SIG_IS(DIAG_SIGNAL_ERR))
		{
			DIAG_SIG_CLR(DIAG_SIGNAL_ERR);

			/* negative response */
			DiagGenNegativeMsg(&gDiagResponseMsg, DAIG_GET_SID(gDiagRequestMsg.Data), DIAG_NRC_IMLOIF);

			/* send response message */
			SYS_CANTP_RESP(gDiagResponseMsg.Data, gDiagResponseMsg.Length);

			DiagReleaseRequestMsg();
		}
	}

	DiagPendingState();

	timerStatus = IS_TIMER(gDiagSessionTimer);
	if(TIMER_STATE_TIMEOUT == timerStatus)
	{
		DPRINTF(DBG_MSG7, "[DIAG] S3 Timer Expired\n");
		
		STOP_TIMER(gDiagSessionTimer);

		/* RX / TX enable */
		SrvMcanComCtrl(SRVMCAN_RX_ALL_ENABLE_IDX);
		SrvMcanComCtrl(SRVMCAN_TX_ALL_ENABLE_IDX);
		/* wl20221014 */
		Can_SetComSendState(1);
		Can_SetComRecvState(1);
		Can_SetNMSendState(1);
		Can_SetNMRecvState(1);

		/* goto default session */
		if(DIAG_SESSION_DEFAULT != gDiagSessionMask)
		{
			DiagSetSessionDefault();
		}
	}

	timerStatus = IS_TIMER(gDiagPendingTimer);
	if(TIMER_STATE_TIMEOUT == timerStatus)
	{
		RESTART_TIMER(gDiagPendingTimer);
		DiagDoPendingResp();
	}

	timerStatus = IS_TIMER(gDiagSATimer);
	if(TIMER_STATE_TIMEOUT == timerStatus)
	{
		STOP_TIMER(gDiagSATimer);
		gDiagSAFailCnt = 0;
	}
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSessionDefault(uint8 *currentSessionMask)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	
	DPRINTF(DBG_MSG7, "[DIAG] Change Default Session\n");

	(*currentSessionMask) = DIAG_SESSION_DEFAULT;

	DiagSetSessionDefault();

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSessionProgramming(uint8 *currentSessionMask)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;
	#if 0
	tDiagSessionDataType sessionCtrlData;
	#endif

	DPRINTF(DBG_MSG7, "[DIAG] Change Programming Session\n");

	/* wl20220808 */
	#if 1
	// /* request goto bootloader */
	// if(TRUE != gDiagNoResp)
	// {
	// 	// sessionCtrlData.mask = INFO_REQ_STAY_IN_BOOT_MASK;
	// 	// //sessionCtrlData.comCtrl = 
	// 	// nrc = DIAG_NRC_RCRRP; /* Resonse Pending 0x78 */
	// }
	// else
	// {
	// 	// sessionCtrlData.mask = INFO_REQ_STAY_IN_BOOT_NO_RESP_MASK;
	// 	// //sessionCtrlData.comCtrl = 		
	// }

	/* wl20220811 Default session cannot enter Programming session directly */
	if((*currentSessionMask) == DIAG_SESSION_DEFAULT)
	{
		/* from Programming Session */
		/* sub-functionNotSupported 0x12 */
		nrc = DIAG_NRC_SFNS;
	}
	else
	{
		/* go to Programing Session */
		(*currentSessionMask) = DIAG_SESSION_PROGRAMING;
	}

	
	// DIAG_SET_SESSION_CTRL_MASK(sessionCtrlData);
	
	// gDiagHardReset = TRUE;
	#else
	
	nrc = DIAG_NRC_SNS;

	#endif

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSessionExtended(uint8 *currentSessionMask)
{
	uint8 nrc = DIAG_NRC_NO_ERROR;

	DPRINTF(DBG_MSG7, "[DIAG] Changed to Extended Session\n");

	if((*currentSessionMask) == DIAG_SESSION_PROGRAMING)
	{
		/* from Programming Session */
		/* sub-functionNotSupported 0x12 */
		nrc = DIAG_NRC_SFNS;
	}
	else
	{
		/* from Default Session or Extended Session */
		(*currentSessionMask) = DIAG_SESSION_EXTENDED;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceDiagnosticSessionControl(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x10 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 session = (uint8)SESSION_RESERVED;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(session, reqMsg->Data, idx);

	/* check suppress */
	if(DIAG_UDS_SUPPRESS_RESP_MASK == (DIAG_UDS_SUPPRESS_RESP_MASK & session))
	{/* no response */
		gDiagNoResp = TRUE;
		session &= ~(DIAG_UDS_SUPPRESS_RESP_MASK);
	}

	switch(session)
	{
		case SESSION_DEFAULT:
			nrc = DiagSubSessionDefault(&gDiagSessionMask);
			break;
		case SESSION_PROGRAMMING:
			nrc = DiagSubSessionProgramming(&gDiagSessionMask);
			break;
		case SESSION_EXTENDED:
			nrc = DiagSubSessionExtended(&gDiagSessionMask);
			break;
		default:
			/* Negative sub-functionNotSupported 0x12 */
			nrc = DIAG_NRC_SFNS;
			break;
	}

	if(nrc == DIAG_NRC_NO_ERROR)
	{
		gDiagSAMask = DIAG_SA_LEVLE0;

		if(TRUE != gDiagNoResp)
		{
			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, session, idx);

			/* P2 server time */
			DIAG_PUT_2BYTE(respMsg->Data, DIAG_P2_TIME, idx);
			/* P*2 server time */
			DIAG_PUT_2BYTE(respMsg->Data, (DIAG_P2_STAR_TIME/DIAG_P2_STAR_TIME_RES), idx);

			respMsg->Length = idx;
		}
	}
	else
	{
		gDiagNoResp = FALSE;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceECUReset(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x11 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 resetType;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(resetType, reqMsg->Data, idx);

	if(DIAG_UDS_SUPPRESS_RESP_MASK == (DIAG_UDS_SUPPRESS_RESP_MASK & resetType))
	{/* no response */
		gDiagNoResp = TRUE;
		resetType &= ~(DIAG_UDS_SUPPRESS_RESP_MASK);
	}

	/* wl20220725 */
	if((resetType != DIAG_HARD_RESET) && (resetType != DIAG_SOFT_RESET))
	{
		/* Negative sub-functionNotSupported */
		nrc = DIAG_NRC_SFNS;
	}
	else
	{/* action */
		gDiagHardReset = TRUE;
	}

	if(nrc == DIAG_NRC_NO_ERROR)
	{
		if(TRUE != gDiagNoResp)
		{
			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, resetType, idx);
			respMsg->Length = idx;
		}
	}
	else
	{
		gDiagNoResp = FALSE;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceDTCClear(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x14 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint32 groupOfDTC;
	uint8 exist = FALSE;

	idx ++; /* for service ID */
	DIAG_GET_3BYTE(groupOfDTC, reqMsg->Data, idx);

	if(DIAG_DTC_ALL_GROUPS == groupOfDTC)
	{
		DtcMgrAllClearDtcReqProcess();
		exist = TRUE;
	}
	else if(0 == (groupOfDTC & DIAG_DTC_GROUPS_MASK))
	{
		exist = DtcMgrGroupClearDtcReqProcess(groupOfDTC);
	}
	else
	{
		exist = DtcMgrClearDtcReqProcess(groupOfDTC);
	}


	if(TRUE == exist)
	{
		DtcMgrClearDtcReqProcess(groupOfDTC);
		/* positive response */
		idx = 0;
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
		respMsg->Length = idx;
	}
	else
	{
		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceDTCRead(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x19 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 reportType = 0;
	uint8 DTCStatusMask = 0;
	uint8 DTCAvailMask = 0;
	uint8 dlc = 0;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(reportType, reqMsg->Data, idx);
	DIAG_GET_1BYTE(DTCStatusMask, reqMsg->Data, idx);

	DTCAvailMask = DtcMgrGetAvailStsMask();

	if (DTCStatusMask == 0x00u) DTCStatusMask = DTCAvailMask; // CHERY SPEC (DIAG)

	/* check suppress */
	if(DIAG_UDS_SUPPRESS_RESP_MASK == (DIAG_UDS_SUPPRESS_RESP_MASK & reportType))
	{/* no response */
		gDiagNoResp = TRUE;
		reportType &= ~(DIAG_UDS_SUPPRESS_RESP_MASK);
	}

	if( (DIAG_DTC_REPORT_TYPE_STATUS_DTCBYSTATUS_MASK != reportType) && (DIAG_DTC_REPORT_TYPE_STATUS_SUPPORTEDDTC_MASK != reportType) )
	{
		/* Negative sub-functionNotSupported */
		nrc = DIAG_NRC_SFNS;
	}
//	else if(0 == (DTCAvailMask & DTCStatusMask))
	else if((0 == (DTCAvailMask & DTCStatusMask)) && (DIAG_DTC_REPORT_TYPE_STATUS_SUPPORTEDDTC_MASK != reportType)) // CHERY SPEC (DIAG)
	{
		/* requestOutOfRange */
		nrc = DIAG_NRC_ROOR;
	}
	else
	{
		if(DIAG_DTC_REPORT_TYPE_STATUS_DTCBYSTATUS_MASK == reportType)
		{
			dlc = DtcMgrReadInfoReqProcess(DTCStatusMask, &(respMsg->Data[DIAG_DTC_READ_STATUS_RECORD_POS]),
											(SYS_CANTP_DATA_LEN_MAX - DIAG_DTC_READ_STATUS_RECORD_POS));
		}
		else if(DIAG_DTC_REPORT_TYPE_STATUS_SUPPORTEDDTC_MASK == reportType)
		{
			dlc = DtcMgrReadInfoSupportedDtcReqProcess(DTCStatusMask, &(respMsg->Data[DIAG_DTC_READ_STATUS_RECORD_POS]),
											(SYS_CANTP_DATA_LEN_MAX - DIAG_DTC_READ_STATUS_RECORD_POS));
		}
		else
		{
			;
		}
	}

	if(nrc == DIAG_NRC_NO_ERROR)
	{
		if(TRUE != gDiagNoResp)
		{
			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, reportType, idx);

//			DIAG_PUT_1BYTE(respMsg->Data, DTCAvailMask, idx);
			DIAG_PUT_1BYTE(respMsg->Data, (uint8)0xFFu, idx); // CHERY SPEC (DIAG)

			/* already copy dtc status data */
			idx += dlc;

			respMsg->Length = idx;
		}
	}
	else
	{
		gDiagNoResp = FALSE;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceReadDataByIdentifier(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x22 */
	uint16 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 DID = 0;
	uint16 len = 0;
	uint16 i = 0;

	idx ++; /* for service ID */
	DIAG_GET_2BYTE(DID, reqMsg->Data, idx);

	nrc = DiagDbiRead(DID, &(respMsg->Data[idx]), &len);

	DPRINTF(DBG_MSG7, "[DIAG] Read By ID DID : %X, len : %d [ ", DID, len);

	for(i = 0 ; i < len ; i++)
	{
		DPRINTF(DBG_MSG7, "%X ", respMsg->Data[i]);
	}
	DPRINTF(DBG_MSG7, "]\n");

	if(DIAG_NRC_NO_ERROR == nrc)
	{
		/* positive response */
		idx = 0;
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
		DIAG_PUT_2BYTE(respMsg->Data, DID, idx);

		/* already copy data */
		idx += len;

		respMsg->Length = idx;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSATSeed(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x27u 0x03u*/
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 seed = 0;

	if(reqMsg->Length != DIAG_SAT_RSD_DLC)
	{
		/* Negative incorrectMessageLengthOrInvalidFormat */
		nrc = DIAG_NRC_IMLOIF;
	}
	else if(DIAG_SA_NUM_MAX <= gDiagSAFailCnt)
	{
		/* requiredTimeDelayNotExpired */
		nrc = DIAG_NRC_RTDNE;
	}
	else
	{
		if(DIAG_SA_LEVLE0 == gDiagSAMask)
		{
			DiagGenSA(&seed, &gDiagSAKey);
		}

		/* positive response */
		idx = 0;
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_SAT_RSD, idx);
		DIAG_PUT_2BYTE(respMsg->Data, seed, idx);
		respMsg->Length = idx;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSATKey(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x27u 0x04u*/
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 keyType = 0;

	if(reqMsg->Length != DIAG_SAT_SK_DLC)
	{
		/* Negative incorrectMessageLengthOrInvalidFormat */
		nrc = DIAG_NRC_IMLOIF;
	}
	else
	{
		idx ++; /* for service ID */
		idx ++;	/* for securityAccessType */
		DIAG_GET_2BYTE(keyType, reqMsg->Data, idx);

		if(gDiagSAKey == 0)
		{
			/* Negative requestSequenceError */
			nrc = DIAG_NRC_RSE;
		}
		else if(DIAG_SA_NUM_MAX <= gDiagSAFailCnt)
		{
			/* requiredTimeDelayNotExpired */
			nrc = DIAG_NRC_RTDNE;
		}
		else if( (keyType != gDiagSAKey) && (DIAG_SA_MAGIC_KEY != keyType) )
		{
			/* Negative invalidKey */
			nrc = DIAG_NRC_IK;
			gDiagSAFailCnt ++;

			if(DIAG_SA_NUM_MAX <= gDiagSAFailCnt)
			{
				START_TIMER(gDiagSATimer, DIAG_SA_TIME);
				nrc = DIAG_NRC_ENOA;
			}
		}
		else
		{
			gDiagSAKey = 0;
			gDiagSAMask = DIAG_SA_LEVLE2;

			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_SAT_SK, idx);
			respMsg->Length = idx;
		}
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSATPBLSeed(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x27u 0x03u*/
	uint8 nrc = DIAG_NRC_NO_ERROR;

	if(reqMsg->Length != DIAG_SAT_RSD_DLC)
	{
		/* Negative incorrectMessageLengthOrInvalidFormat */
		nrc = DIAG_NRC_IMLOIF;
	}
	else
	{
		/* sub-functionNotSupportedInActiveSession */
		nrc = DIAG_NRC_SFNSIAS;
	}


	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagSubSATPBLKey(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x27u 0x04u*/
	uint8 nrc = DIAG_NRC_NO_ERROR;

	if(reqMsg->Length != DIAG_SAT_SK_DLC)
	{
		/* Negative incorrectMessageLengthOrInvalidFormat */
		nrc = DIAG_NRC_IMLOIF;
	}
	else
	{
		/* sub-functionNotSupportedInActiveSession */
		nrc = DIAG_NRC_SFNSIAS;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceSecurityAccess(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x27 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 securityAccessType;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(securityAccessType, reqMsg->Data, idx);

	switch(securityAccessType)
	{
		case DIAG_SAT_RSD:
			/* RequestSeed to unlock the ECU */
			nrc = DiagSubSATSeed(respMsg, reqMsg);
			break;
		case DIAG_SAT_SK:
			/* SendKey to unlock the ECU */
			nrc = DiagSubSATKey(respMsg, reqMsg);
			break;

		case DIAG_SAT_PBL_RSD:
			/* RequestSeed to unlock the ECU */
			nrc = DiagSubSATPBLSeed(respMsg, reqMsg);
			break;

		case DIAG_SAT_PBL_SK:
			/* SendKey to unlock the ECU */
			nrc = DiagSubSATPBLKey(respMsg, reqMsg);
			break;

		default:
			/* Negative sub-functionNotSupported */
			nrc = DIAG_NRC_SFNS;
			break;
	}

	return nrc;
}


/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceCommunicationControl(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x28 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 controlType;
	uint8 communicationType;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(controlType, reqMsg->Data, idx);
	DIAG_GET_1BYTE(communicationType, reqMsg->Data, idx);

	/* check suppress */
	if(DIAG_UDS_SUPPRESS_RESP_MASK == (DIAG_UDS_SUPPRESS_RESP_MASK & controlType))
	{/* no response */
		gDiagNoResp = TRUE;
		controlType &= ~(DIAG_UDS_SUPPRESS_RESP_MASK);
	}

	/* 0x00 enableRxAndTx */
	if(DIAG_CC_CTRL_TYPE_EN_RX_EN_TX == controlType)
	{
		/* NORMAL */
		if(DIAG_CC_COM_TYPE_NORMAL == communicationType)
		{
			/* RX / TX enable */
			SrvMcanComCtrl(SRVMCAN_RX_NORMAL_ENABLE_IDX);
			SrvMcanComCtrl(SRVMCAN_TX_NORMAL_ENABLE_IDX);
			/* wl20220725 */
			Can_SetComSendState(1);
			Can_SetComRecvState(1);
		}
		/* NORMAL / NM */
		else if(DIAG_CC_COM_TYPE_NORMAL_NM == communicationType)
		{
			/* RX / TX enable */
			SrvMcanComCtrl(SRVMCAN_RX_ALL_ENABLE_IDX);
			SrvMcanComCtrl(SRVMCAN_TX_ALL_ENABLE_IDX);
			/* wl20220725 */
			Can_SetComSendState(1);
			Can_SetComRecvState(1);
			Can_SetNMSendState(1);
			Can_SetNMRecvState(1);
		}
		else if(DIAG_CC_COM_TYPE_NM == communicationType)
		{
			/* requestOutOfRange */
			nrc = DIAG_NRC_SFNS;
		}
		else
		{
			/* requestOutOfRange */
			nrc = DIAG_NRC_ROOR;
		}
	}
	/* 0x03 disableRxAndTx */
	else if(DIAG_CC_CTRL_TYPE_DI_RX_DI_TX == controlType)
	{
		/* NORMAL */		
		if(DIAG_CC_COM_TYPE_NORMAL == communicationType)
		{
			/* RX / TX disable */
			SrvMcanComCtrl(SRVMCAN_RX_NORMAL_DISABLE_IDX);
			SrvMcanComCtrl(SRVMCAN_TX_NORMAL_DISABLE_IDX);
			/* wl20220725 */
			Can_SetComSendState(0);
			Can_SetComRecvState(0);
		}
		/* NORMAL / NM */		
		else if(DIAG_CC_COM_TYPE_NORMAL_NM == communicationType)
		{
			/* RX / TX disable */
			SrvMcanComCtrl(SRVMCAN_RX_ALL_DISABLE_IDX);
			SrvMcanComCtrl(SRVMCAN_TX_ALL_DISABLE_IDX);
			/* wl20220725 */
			Can_SetComSendState(0);
			Can_SetComRecvState(0);
			Can_SetNMSendState(0);
			Can_SetNMRecvState(0);
		}
		else if(DIAG_CC_COM_TYPE_NM == communicationType)
		{
			/* requestOutOfRange */
			nrc = DIAG_NRC_SFNS;
		}
		else
		{
			/* requestOutOfRange */
			nrc = DIAG_NRC_ROOR;
		}
	}
	else
	{
		/* sub-functionNotSupported */
		nrc = DIAG_NRC_SFNS;
	}

	if(nrc == DIAG_NRC_NO_ERROR)
	{
		if(TRUE != gDiagNoResp)
		{
			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, controlType, idx);
			respMsg->Length = idx;
		}
	}
	else
	{
		gDiagNoResp = FALSE;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceWriteDataByIdentifier(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x2E */
	uint16 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 DID;

	idx ++; /* for service ID */
	DIAG_GET_2BYTE(DID, reqMsg->Data, idx);

	nrc = DiagDbiWrite(DID, &(reqMsg->Data[idx]), (reqMsg->Length - idx));

	if(DIAG_NRC_NO_ERROR == nrc)
	{
		/* positive response */
		idx = 0;
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
		DIAG_PUT_2BYTE(respMsg->Data, DID, idx);

		respMsg->Length = idx;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceIOControl(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x31 */
	uint16 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint16 DID;
	uint16 len = 0;

	idx ++; /* for service ID */
	DIAG_GET_2BYTE(DID, reqMsg->Data, idx);

	nrc = DiagDbiIO(DID, &(reqMsg->Data[idx]), (reqMsg->Length - idx), &(respMsg->Data[idx]), &len);

	if(DIAG_NRC_NO_ERROR == nrc)
	{
		/* positive response */
		idx = 0;
		DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
		DIAG_PUT_2BYTE(respMsg->Data, DID, idx);

		/* already copy data */
		idx += len;

		respMsg->Length = idx;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceRequestDownload(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x34 */

	UNUSED_V(respMsg);
	UNUSED_V(reqMsg);

	return DIAG_NRC_SNSIAS;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceTransferData(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x36 */
	UNUSED_V(respMsg);
	UNUSED_V(reqMsg);

	return DIAG_NRC_SNSIAS;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceRequestTransferExit(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x37 */
	UNUSED_V(respMsg);
	UNUSED_V(reqMsg);

	return DIAG_NRC_SNSIAS;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceTesterPresent(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x3E */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 zeroSubFunc;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(zeroSubFunc, reqMsg->Data, idx);

	/* If Rx Tester Present, S3 Timer should stop. */
	STOP_TIMER(gDiagSessionTimer);

	if(DIAG_UDS_SUPPRESS_RESP_MASK == (DIAG_UDS_SUPPRESS_RESP_MASK & zeroSubFunc))
	{/* no response */
		gDiagNoResp = TRUE;
		zeroSubFunc &= ~(DIAG_UDS_SUPPRESS_RESP_MASK);
	}
	else if(DIAG_KWP_TESTER_PRESENT_NO_RESP_MASK == zeroSubFunc)
	{
		gDiagNoResp = TRUE;
		zeroSubFunc = 0;
	}
	else
	{
		;/* none */
	}

	if(0 != zeroSubFunc)
	{
		/* Negative sub-functionNotSupported */
		nrc = DIAG_NRC_SFNS;
	}

	if(nrc == DIAG_NRC_NO_ERROR)
	{
		if(TRUE != gDiagNoResp)
		{
			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, zeroSubFunc, idx);
			respMsg->Length = idx;
		}
		else
		{
// CHERY SPEC (DIAG)			DPRINTF(DBG_MSG7, "[DIAG] Tester Present S3 Timer Start\n");
			/* wl20220802 */
			START_TIMER(gDiagSessionTimer, DIAG_S3_SERVER_TIME);
		}
	}
	else
	{
		gDiagNoResp = FALSE;
	}

	return nrc;
}

/******************************************************************************
* Function:
* Description:
* Inputs:
* Outputs:
* Returns:
******************************************************************************/
uint8 DiagServiceControlDTCSetting(hDiagMessageType *respMsg, hDiagMessageType *reqMsg)
{/* 0x85 */
	uint8 idx = 0;
	uint8 nrc = DIAG_NRC_NO_ERROR;
	uint8 DTCSettingType;

	idx ++; /* for service ID */
	DIAG_GET_1BYTE(DTCSettingType, reqMsg->Data, idx);

	if(DIAG_UDS_SUPPRESS_RESP_MASK == (DIAG_UDS_SUPPRESS_RESP_MASK & DTCSettingType))
	{/* no response */
		gDiagNoResp = TRUE;
		DTCSettingType &= ~(DIAG_UDS_SUPPRESS_RESP_MASK);
	}

	if(DIAG_DTC_SETTING_ON == DTCSettingType)
	{
		MntFltSetMntDisabled(FALSE);
	}
	else if(DIAG_DTC_SETTING_OFF == DTCSettingType)
	{
		MntFltSetMntDisabled(TRUE);
	}
	else
	{
		/* Negative sub-functionNotSupported */
		nrc = DIAG_NRC_SFNS;
	}

	if(nrc == DIAG_NRC_NO_ERROR)
	{
		if(TRUE != gDiagNoResp)
		{
			/* positive response */
			idx = 0;
			DIAG_PUT_1BYTE(respMsg->Data, DIAG_GET_POSITIVE_SID(reqMsg->Data), idx);
			DIAG_PUT_1BYTE(respMsg->Data, DTCSettingType, idx);
			respMsg->Length = idx;
		}
	}
	else
	{
		gDiagNoResp = FALSE;
	}

	return nrc;
}

