/*
 * dtc_mgr.c
 *
 *  Created on: 2020. 7. 2.
 *      Author: Digen
 */

#include "dtc_mgr.h"
#include "service/std_util.h"
#include "mnt_flt.h"
#include "diag\diag_dbi.h"

#include "includes.h"
#include "Diag/dtc_mgr.h"

/**********************************************************************************************************************
  DEFINE
**********************************************************************************************************************/
#define DTCMGR_LOG_MSG   (0u)
#define DTCMGR_LOG_EVENT (1u)
#define DTCMGR_LOG_ERR   (0u)

#define DTCMGR_AVAILABLE_MASK   (0x4Bu)

#define DTCMGR_DTC_STATUS_FAILED                    (0x01u)
#define DTCMGR_DTC_STATUS_FAILED_THIS_OP            (0x02u)
#define DTCMGR_DTC_STATUS_CONFIRMED                 (0x08u)
#define DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP    (0x40u)

#define DTCMGR_SET_DTC_VALUE(v, m)    ( (v) |= (m) )
#define DTCMGR_CLR_DTC_VALUE(v, m)    ( (v) &= ~(m) )
#define DTCMGR_DTC_VALUE_IS(v, m)     (0 != ((v) & (m)))


#define DTCMGR_GROUPS_MASK			(0xC000u)		/* DTC clear: Groups Mask */

extern uint8 *DIAG_ReadDataDTCFromNVM(void);
extern void   DIAG_WriteDataDTCToNVM (uint8 *data, uint32 length);

extern uint8 softwareConfiguration[];
extern uint8 u8VariantCoding[];
extern uint8 u8Vin[];

void WriteDTCData(void);

uint8 u8NvmData[DIAG_ROM_NVM_LEN] = {0u};

uint8 u8ErrCode[DIAG_ROM_RESET_INFO_LEN] = {0u};

/**********************************************************************************************************************
  STRUCTURE
**********************************************************************************************************************/
typedef struct{
    uint8 tripCnt;
    uint8 agingCnt;
    uint8 statusValue;
    uint8 reserved;
}tDtcMgrDataTableType;

typedef struct{
    uint16 dtcId;
    uint8 symtomId;
    uint8 testFailedThisOp;
    uint8 testCompletedThisOp;
    uint8 cfmThrshld;
    uint8 agingThrshld;
}tDtcMgrInfoTableType;

typedef struct{
    uint16 dtcId ;
    uint8 symtomId;
    uint8 statusOfDTC;
}tDtcMgrStatusRecordType;
/**********************************************************************************************************************
  GLABAL VARIABLE
**********************************************************************************************************************/
static tDtcMgrInfoTableType gDtcMgrInfoList[DTC_INDEX_MAX] = {
#if 0
  /* dtcId | symtomId | testFailedThisOp | testCompletedThisOp | cfmThrshld | agingThrshld */
 //    {0x9100, 0x13,      0x00,              0x00,                 0x03,        0x28}, /* DTC_POWER_OPEN        - B1100_13 (910013) */
 //    {0x9100, 0x16,      0x00,              0x00,                 0x03,        0x28}, /* DTC_POWER_LOW         - B1100_16 (910016) */
 //    {0x9100, 0x17,      0x00,              0x00,                 0x03,        0x28}, /* DTC_POWER_HIGH        - B1100_17 (910017) */
     {0x9115, 0x04,     0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_BT_MODULE_FAILURE - B1115_04 (911504) */
     {0x9116, 0x76, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_NAVI_MOUNT        - B1116_76 (911676) */
 //    {0x981A, 0x04,      0x00,			   0x00,				 0x03,		  0x28}, /* DTC_DMS_SDK_LICENSE   - B181A_04 (981A04) */
 //    {0x981B, 0x04,      0x00,			   0x00,				 0x03,		  0x28}, /* DTC_DMS_HARWARE       - B181B_04 (981B04) */
     {0xC073, 0x88,		0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_BUS_OFF_FAULT     - U0073_88 (C07388) */
     {0xC140, 0x87,     0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_LOS_COM_CEM       - U0140_87 (C14087) */
     {0xC155, 0x87,     0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_LOS_COM_ICM       - U0155_87 (C15587) */
     #ifdef DTC_LIST_AVMCOM
     {0xC253, 0x87,     0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_LOS_COM_AVM       - U0253_87 (C25387) */
     #endif
     {0xC145, 0x87,     0x00, 			   0x00,			 	 	0x03, 		  0x28}, /* DTC_LOS_COM_IPM       - U0145_87 (C14587) */
     {0xD300, 0x55, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_SW_CONFIG_ERROR   - U1300_55 (D30055) */
#endif
    /* dtcId | symtomId | testFailedThisOp | testCompletedThisOp | cfmThrshld | agingThrshld */
    {0x9500, 0x16, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_VOLT_BELOW             - B1500-16 (950016) */
    {0x9501, 0x16, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_VOLT_ABOVE             - B1501-16 (950116) */
    {0x9505, 0x00, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_SPK_CNECT_FAIL         - B1505-00 (950500) */
    {0x9507, 0x04, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_AMP_FAIL               - B1507-04 (950704) */
    {0x9508, 0x04, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_Tuner_IC_FAIL          - B1508-04 (950804) */
    {0x9509, 0x04, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_Voice_IC_FAIL          - B1509-04 (950904) */
    {0x950A, 0x00, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_MCU_SOC_COM_FAIL       - B150A-00 (950A00) */
    {0x950B, 0x98, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_SOC_OVERTEMP           - B150B-98 (950B98) */
    {0x950C, 0x19, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_USB1_CURRENT_ABOVE     - B150C-19 (950C19) */
    {0x950D, 0x19, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_USB2_CURRENT_ABOVE     - B150D-19 (950D19) */
    {0x950E, 0x04, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_SOC_DES_COM_FAIL       - B150E-04 (950E04) */
    {0x950F, 0x04, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_SOC_TFT_COM_FAIL       - B150F-04 (950F04) */
    {0x9510, 0x55, 	0x00, 			   0x00, 				0x03, 		  0x28}, /* DTC_SOFT_CONFIG_Error      - B1510-55 (951055) */

    {0xF300, 0x88, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_BUS_OFF_FAULT          - U3300-88 (F30088) */
    {0xF310, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_VCU            - U3310-87 (F31087) */
    {0xF311, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_BMS            - U3311-87 (F31187) */
    {0xF312, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_BCM            - U3312-87 (F31287) */
    {0xF314, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_ICM            - U3314-87 (F31487) */
    {0xF315, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_SAS            - U3315-87 (F31587) */
    {0xF317, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_ACCM           - U3317-87 (F31787) */
    {0xF318, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_SCM_R          - U3318-87 (F31887) */
    {0xF319, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_AVAS           - U3319-87 (F31987) */
    {0xF31C, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_BSD            - U331C-87 (F31C87) */
    {0xF31E, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_FCM            - U331E-87 (F31E87) */
    {0xF320, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_TBOX           - U3320-87 (F32087) */
    {0xF321, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_MFS            - U3321-87 (F32187) */
    {0xF322, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}, /* DTC_LOS_COM_ABM            - U3322-87 (F32287) */
    {0xF323, 0x87, 	0x00, 			   0x00, 				0x03, 		  0x32}  /* DTC_LOS_COM_WCM            - U3323-87 (F32387) */
};

static tDtcMgrDataTableType gDtcMgrDataList[DTC_INDEX_MAX] = {
#if 0
  /* tripCnt | agingCnt | statusValue | reserved */
//    {0x0000,    0x0000,     0x00,       0x00}, /* DTC_POWER_OPEN        - B1116_76 (911676) */
//    {0x0000,    0x0000,     0x00,       0x00}, /* DTC_POWER_LOW         - B1116_76 (911676) */
//    {0x0000,    0x0000,     0x00,       0x00}, /* DTC_POWER_HIGH        - B1116_76 (911676) */
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_BT_MODULE_FAILURE - B1115_04 (911504) */
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_NAVI_MOUNT        - B1116_76 (911676) */
//    {0x0000,	0x0000, 	0x00,		0x00}, /* DTC_DMS_SDK_LICENSE   - B181A_04 (981A04) */
//    {0x0000,	0x0000, 	0x00,		0x00}, /* DTC_DMS_HARWARE       - B181B_04 (981B04) */
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_BUS_OFF_FAULT     - U0073_88 (C07388) */
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_LOS_COM_CEM       - U0140_87 (C14087) */
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_LOS_COM_ICM       - U0155_87 (C15587) */
    #ifdef DTC_LIST_AVMCOM
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_LOS_COM_AVM       - U0253_87 (C25387) */
    #endif
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_LOS_COM_IPM       - U0145_87 (C14587) */    
    {0x0000, 	0x0000, 	0x00, 		0x00}, /* DTC_SW_CONFIG_ERROR   - U1300_55 (D30055) */
#endif
    /* tripCnt | agingCnt | statusValue | reserved */
    {0x0000, 	  0x0000, 	   0x00, 		0x00}, /* DTC_VOLT_BELOW             - B1500-16 (950016) */   
    {0x0000, 	  0x0000, 	   0x00, 		0x00}, /* DTC_VOLT_ABOVE             - B1501-16 (950116) */
    {0x0000, 	  0x0000, 	   0x00, 		0x00}, /* DTC_SPK_CNECT_FAIL         - B1505-00 (950500) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_AMP_FAIL               - B1507-04 (950704) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_Tuner_IC_FAIL          - B1508-04 (950804) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_Voice_IC_FAIL          - B1509-04 (950904) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_MCU_SOC_COM_FAIL       - B150A-00 (950A00) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_SOC_OVERTEMP           - B150B-98 (950B98) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_USB1_CURRENT_ABOVE     - B150C-19 (950C19) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_USB2_CURRENT_ABOVE     - B150D-19 (950D19) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_SOC_DES_COM_FAIL       - B150E-04 (950E04) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_SOC_TFT_COM_FAIL       - B150F-04 (950F04) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_SOFT_CONFIG_Error      - B1510-55 (951055) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_BUS_OFF_FAULT          - U3300-88 (F30088) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_VCU            - U3310-87 (F31087) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_BMS            - U3311-87 (F31187) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_BCM            - U3312-87 (F31287) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_ICM            - U3314-87 (F31487) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_SAS            - U3315-87 (F31587) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_ACCM           - U3317-87 (F31787) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_SCM_R          - U3318-87 (F31887) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_AVAS           - U3319-87 (F31987) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_BSD            - U331C-87 (F31C87) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_FCM            - U331E-87 (F31E87) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_TBOX           - U3320-87 (F32087) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_MFS            - U3321-87 (F32187) */
    {0x0000,      0x0000, 	   0x00, 		0x00}, /* DTC_LOS_COM_ABM            - U3322-87 (F32287) */
    {0x0000,      0x0000, 	   0x00, 		0x00}  /* DTC_LOS_COM_WCM            - U3323-87 (F32387) */
};


uint8* GetDtcMgrDataPtr(void)
{
    return (uint8*)gDtcMgrDataList;
}

uint16 GetDtcMgrDataSize(void)
{
    return sizeof(tDtcMgrDataTableType);
}
/**********************************************************************************************************************
  Function declare
**********************************************************************************************************************/
static void DtcMgrSetConfirmed(tDtcMgrDataTableType* data, tDtcMgrInfoTableType* info, tDtcMgrResultType result);
static uint16 DtcMgrGetDtcList(uint8 dtcStatusMask, tDtcMgrStatusRecordType* statusRecord);
static uint16 DtcMgrGetSupportedDtcList(uint8 dtcStatusMask, tDtcMgrStatusRecordType* statusRecord);
static void DtcMgrOpCycleEnd(void);



/* DTC_DATA_READ() */
uint8 *DIAG_ReadDataDTCFromNVM(void)
{
	static uint8 rxbuf[DIAG_ROM_SIZE_DTC_MAX];

	// flash_read_func ((uint32)DIAG_ROM_ADDR_DTC, rxbuf, DIAG_ROM_SIZE_DTC_MAX);
    flash_read_func ((uint32)DIAG_ROM_ADDR_DTC, rxbuf, sizeof(gDtcMgrDataList));

	return (rxbuf);
}

/* SYS_NVM_UPDATE_DTC_DATA(data, length) */
void DIAG_WriteDataDTCToNVM(uint8 *data, uint32 length)
{
	#if 1
	// flash_sector_erase((uint32)DIAG_ROM_ADDR_DTC); 
    flash_write_func((uint32)DIAG_ROM_ADDR_DTC, data, length);
	#else
	#endif
	DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void DIAG_ReadDataSCFromNVM(void)
{
	//flash_read_func ((uint32)DIAG_ROM_ADDR_SC,  softwareConfiguration, (int)DIAGDBI_SOFTWARE_CONFIGURATION_LEN);
    /* wl20220801 */
    flash_read_func ((uint32)DIAG_ROM_ADDR_SC,  u8NvmData, (int)DIAG_ROM_NVM_LEN);
    UTL_COPY_DATA(softwareConfiguration, &u8NvmData[0u], DIAGDBI_SOFTWARE_CONFIGURATION_LEN);
	DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void DIAG_ReadDataVINFromNVM(void)
{
	//flash_read_func ((uint32)DIAG_ROM_ADDR_VIN, u8Vin, (int)DIAGDBI_VIM_LEN);
    /* wl20220801 */
    flash_read_func ((uint32)DIAG_ROM_ADDR_SC,  u8NvmData, (int)DIAG_ROM_NVM_LEN);
    UTL_COPY_DATA(u8Vin, &u8NvmData[DIAGDBI_SOFTWARE_CONFIGURATION_LEN], DIAGDBI_VIM_LEN);
	DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void DIAG_ReadDataVCODINGFromNVM(void)
{
    //flash_read_func ((uint32)DIAG_ROM_ADDR_VCODING, u8VariantCoding, (int)DIAGDBI_IHUVariantCoding_LEN);
	/* wl20220801 */
    flash_read_func ((uint32)DIAG_ROM_ADDR_SC,  u8NvmData, (int)DIAG_ROM_NVM_LEN);
    UTL_COPY_DATA(u8VariantCoding, &u8NvmData[DIAGDBI_SOFTWARE_CONFIGURATION_LEN + DIAGDBI_VIM_LEN], DIAGDBI_IHUVariantCoding_LEN);
    DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void DIAG_ReadDataRESETINFOFromNVM(void)
{
	/* wl20220826 */
    flash_read_func ((uint32)DIAG_ROM_ADDR_RESET_INFO,  u8ErrCode, (int)DIAG_ROM_RESET_INFO_LEN);
    DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void DIAG_WriteDataSCnVINToNVM(void)
{
	#if 0
	flash_sector_erase((uint32)DIAG_ROM_ADDR_SC); 
	delay_ms(100); // Reduce if possible
	
	flash_write_func((uint32)DIAG_ROM_ADDR_SC,  softwareConfiguration, (int)DIAGDBI_SOFTWARE_CONFIGURATION_LEN);
//	delay_ms(10);  // Reduce if possible
	delay_ms(1);  // Reduce if possible
	flash_write_func((uint32)DIAG_ROM_ADDR_VIN, u8Vin, (int)DIAGDBI_VIM_LEN);
    /* wl20220730 */
    delay_ms(1);  // Reduce if possible
	flash_write_func((uint32)DIAG_ROM_ADDR_VCODING, u8VariantCoding, (int)DIAGDBI_IHUVariantCoding_LEN);
	#else
	#endif
    /* wl20220801 */
    UTL_COPY_DATA(&u8NvmData[0u], softwareConfiguration, DIAGDBI_SOFTWARE_CONFIGURATION_LEN);
    UTL_COPY_DATA(&u8NvmData[DIAGDBI_SOFTWARE_CONFIGURATION_LEN], u8Vin, DIAGDBI_VIM_LEN);
    UTL_COPY_DATA(&u8NvmData[DIAGDBI_SOFTWARE_CONFIGURATION_LEN + DIAGDBI_VIM_LEN], u8VariantCoding, DIAGDBI_IHUVariantCoding_LEN);
    flash_write_func((uint32)DIAG_ROM_ADDR_SC,  u8NvmData, (int)DIAG_ROM_NVM_LEN);
	DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void WriteDataResetInfoToNVM(uint8* u8ErrCode)
{
    /* wl20220826 */
    flash_write_func((uint32)DIAG_ROM_ADDR_RESET_INFO,  u8ErrCode, (int)DIAG_ROM_RESET_INFO_LEN);
	DPRINTF(DBG_MSG5_1, "%s\n",__FUNCTION__);
}

void EraseFRAM(void)
{
    DATA_SAVE e2p_save_data = {0};
    E2P_SYSTEM e2p_sys_data = {0};
    uint8 u8NvmDataTemp[DIAG_ROM_NVM_LEN] = {0u};
    uint8 u8ErrCodeTemp[DIAG_ROM_RESET_INFO_LEN] = {0u};
    tDtcMgrDataTableType gDtcMgrDataListTemp[DTC_INDEX_MAX] = {0};

    flash_write_func(FLASH_DATA0_ADDR, (const unsigned char *)&e2p_save_data, sizeof(DATA_SAVE));
	delay_ms(50);
    #ifdef WATCHDOG_ENABLE
	WDOG_DRV_Deinit(INST_WATCHDOG1);
	#endif
	flash_write_func(FLASH_DATA1_ADDR, (const unsigned char *)&e2p_sys_data, sizeof(E2P_SYSTEM));
    delay_ms(50);
    #ifdef WATCHDOG_ENABLE
	WDOG_DRV_Deinit(INST_WATCHDOG1);
	#endif
    flash_write_func(DIAG_ROM_ADDR_SC, u8NvmDataTemp, (int)DIAG_ROM_NVM_LEN);
    delay_ms(50);
    #ifdef WATCHDOG_ENABLE
	WDOG_DRV_Deinit(INST_WATCHDOG1);
	#endif
    flash_write_func(DIAG_ROM_ADDR_RESET_INFO, u8ErrCodeTemp, (int)DIAG_ROM_RESET_INFO_LEN);
    delay_ms(50);
    #ifdef WATCHDOG_ENABLE
	WDOG_DRV_Deinit(INST_WATCHDOG1);
	#endif
    flash_write_func(DIAG_ROM_ADDR_DTC, gDtcMgrDataListTemp, sizeof(gDtcMgrDataListTemp));
    DPRINTF(DBG_INFO, "Erase fram successfully!\n");
}

uint8 DIAG_CheckValidationSC(void)
{
	uint8 valid = FALSE;
	
	if (0xFFu != softwareConfiguration[0]) valid = TRUE;

	return (valid);
}

/*******************************************************************************
*
* NAME:              DtcMgrInit
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void DtcMgrInit(void)
{
	uint8 *data;
	// data = DTC_DATA_READ();
    /* wl20220802 delete */
	// UTL_COPY_DATA((uint8 *)gDtcMgrDataList, data, sizeof(gDtcMgrDataList));
}

/*                      START DTC STATUS SETTING PROCESS                       */
/*******************************************************************************
*
* NAME:              DtcMgrSetDtcStatus
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void DtcMgrSetDtcStatus(tDtcMgrIdListType dtcIdx, tDtcMgrResultType result)
{
    if(DTC_FAILED == result)
    {
        DTCMGR_SET_DTC_VALUE(gDtcMgrDataList[dtcIdx].statusValue, DTCMGR_DTC_STATUS_FAILED);
        DTCMGR_SET_DTC_VALUE(gDtcMgrInfoList[dtcIdx].testCompletedThisOp, DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP);

    }
    else if(DTC_PASSED == result)
    {
        DTCMGR_CLR_DTC_VALUE(gDtcMgrDataList[dtcIdx].statusValue, DTCMGR_DTC_STATUS_FAILED);
        DTCMGR_SET_DTC_VALUE(gDtcMgrInfoList[dtcIdx].testCompletedThisOp, DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP);
    }
    else
    {
        ;
    }

    DtcMgrSetConfirmed(&gDtcMgrDataList[dtcIdx], &gDtcMgrInfoList[dtcIdx], result);
}

/*******************************************************************************
*
* NAME:              DtcMgrSetConfirmed
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static void DtcMgrSetConfirmed(tDtcMgrDataTableType* data, tDtcMgrInfoTableType* info, tDtcMgrResultType result)
{
    if(!DTCMGR_DTC_VALUE_IS(data->statusValue, DTCMGR_DTC_STATUS_CONFIRMED))
    {
        if(!DTCMGR_DTC_VALUE_IS(info->testFailedThisOp, DTCMGR_DTC_STATUS_FAILED_THIS_OP) && (DTC_FAILED == result) )
        {
            data->tripCnt += 1;
            if( data->tripCnt >= info->cfmThrshld )
            {
                /* wl20220801 delete */
                //DTCMGR_SET_DTC_VALUE(data->statusValue, DTCMGR_DTC_STATUS_CONFIRMED);
                data->agingCnt = 0;
                data->tripCnt = 0;
                /* wl20220729 put in Prepare Bus sleep, save only one time */
                // SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
            }
        }
        /* wl20220801 when a triggered DTC change state to pass,set confirm bit */
        else if(DTCMGR_DTC_VALUE_IS(info->testFailedThisOp, DTCMGR_DTC_STATUS_FAILED_THIS_OP) && (DTC_PASSED == result))
        {
            DTCMGR_SET_DTC_VALUE(data->statusValue, DTCMGR_DTC_STATUS_CONFIRMED);
        }
        else
        {
            /* Do nothing */
        }
    }
    else if(data->agingCnt >= info->agingThrshld)
    {
        DTCMGR_CLR_DTC_VALUE(data->statusValue, DTCMGR_DTC_STATUS_CONFIRMED);
        data->agingCnt = 0;
        /* wl20220729 put in IG OFF, save only one time */
        // SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
    }
    else
    {
        ;
    }

    if(DTC_FAILED == result)
    {
        data->agingCnt = 0;
        DTCMGR_SET_DTC_VALUE(info->testFailedThisOp, DTCMGR_DTC_STATUS_FAILED_THIS_OP);
    }
}

/*******************************************************************************
*
* NAME:              DtcMgrAgingProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void DtcMgrAgingProcess(void)
{
    uint8 index = 0;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
        if( (DTCMGR_DTC_VALUE_IS(gDtcMgrDataList[index].statusValue, DTCMGR_DTC_STATUS_CONFIRMED))
             &&(DTCMGR_DTC_VALUE_IS(gDtcMgrInfoList[index].testCompletedThisOp, DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP))
          )
        {
            DTCMGR_DTC_VALUE_IS(gDtcMgrDataList[index].statusValue, DTCMGR_DTC_STATUS_FAILED) ? (gDtcMgrDataList[index].agingCnt = 0) : (gDtcMgrDataList[index].agingCnt += 1);

            if(gDtcMgrDataList[index].agingCnt >= gDtcMgrInfoList[index].agingThrshld)
            {
                DTCMGR_CLR_DTC_VALUE(gDtcMgrDataList[index].statusValue, DTCMGR_DTC_STATUS_CONFIRMED);

                //DPRINTF(DBG_MSG7, "[DTC_MGR] DtcMgrAgingProcess DTC[%x%x] clear \n", gDtcMgrInfoList[index].dtcId, gDtcMgrInfoList[index].symtomId);
            }
        }
    }

    DtcMgrOpCycleEnd();
    // SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
}

/*******************************************************************************
*
* NAME:              DtcMgrOpCycleEnd
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static void DtcMgrOpCycleEnd(void)
{
    uint8 index = 0;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
        if( (!DTCMGR_DTC_VALUE_IS(gDtcMgrInfoList[index].testFailedThisOp, DTCMGR_DTC_STATUS_FAILED_THIS_OP))
            && DTCMGR_DTC_VALUE_IS(gDtcMgrInfoList[index].testCompletedThisOp, DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP) )
        {
            gDtcMgrDataList[index].tripCnt = 0;
        }

        DTCMGR_CLR_DTC_VALUE(gDtcMgrInfoList[index].testFailedThisOp, DTCMGR_DTC_STATUS_FAILED_THIS_OP);
        DTCMGR_CLR_DTC_VALUE(gDtcMgrInfoList[index].testCompletedThisOp, DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP);

        MntFltDtcClearValue(index);
    }
}


/*******************************************************************************
*
* NAME:              DtcMgrGetStatus
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
uint8 DtcMgrGetDtcFailedState(tDtcMgrIdListType dtcIdx)
{
    return (gDtcMgrDataList[dtcIdx].statusValue & DTCMGR_DTC_STATUS_FAILED);
}

/*                      END DTC STATUS SETTING PROCESS                       */

/*                      START SERVICE REQUEST PROCESS                        */
/*******************************************************************************
*
* NAME:              DtcMgrReadInfoReqProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
uint32 DtcMgrReadInfoReqProcess(uint8 dtcStatusMask, uint8* array, uint32 arraySize)
{
    tDtcMgrStatusRecordType statusRecord[DTC_INDEX_MAX] = {0};
    uint16 dataNum = 0;
    uint32 totalLength = 0;

    if(0 != (DTCMGR_AVAILABLE_MASK & dtcStatusMask))
    {
        dataNum = DtcMgrGetDtcList(dtcStatusMask, statusRecord);
        totalLength = (dataNum * sizeof(tDtcMgrStatusRecordType));

        if(arraySize > totalLength)
        {
            UTL_COPY_DATA(array, (uint8 *)statusRecord, totalLength);
            DPRINTF(DBG_MSG7, "[DTC_MGR] DtcMgrReadInfoReqProcess - array size : %d total : %d\n", arraySize, totalLength);
        }
        else
        {
            totalLength = 0;

            DPRINTF(DBG_MSG7, "##ERR##[DTC_MGR] DtcMgrReadInfoReqProcess - Memory Overflow array : %d total : %d\n", arraySize, totalLength);
        }
    }

    return totalLength;
}

/*******************************************************************************
*
* NAME:              DtcMgrReadInfoReqProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
uint32 DtcMgrReadInfoSupportedDtcReqProcess(uint8 dtcStatusMask, uint8* array, uint32 arraySize)
{
    tDtcMgrStatusRecordType statusRecord[DTC_INDEX_MAX] = {0};
    uint16 dataNum = 0;
    uint32 totalLength = 0;

    if(0 != (DTCMGR_AVAILABLE_MASK & dtcStatusMask))
    {
        dataNum = DtcMgrGetSupportedDtcList(dtcStatusMask, statusRecord);
        totalLength = (dataNum * sizeof(tDtcMgrStatusRecordType));

        if(arraySize > totalLength)
        {
            UTL_COPY_DATA(array, (uint8 *)statusRecord, totalLength);
            DPRINTF(DBG_MSG7, "[DTC_MGR] DtcMgrReadInfoSupportedDtcReqProcess - array size : %d total : %d\n", arraySize, totalLength);
        }
        else
        {
            totalLength = 0;

            DPRINTF(DBG_MSG7, "##ERR##[DTC_MGR] DtcMgrReadInfoSupportedDtcReqProcess - Memory Overflow array : %d total : %d\n", arraySize, totalLength);
        }
    }

    return totalLength;
}

/*******************************************************************************
*
* NAME:              DtcMgrGetDtcList
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static uint16 DtcMgrGetDtcList(uint8 dtcStatusMask, tDtcMgrStatusRecordType* statusRecord)
{
    uint8 index = 0;
    uint8 stats = 0;
    uint8 dataNum = 0;
    uint8 testNotCompThisOp = 0;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
        (gDtcMgrInfoList[index].testCompletedThisOp == 0) ? (testNotCompThisOp = DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP) : (testNotCompThisOp = 0);

        stats = (gDtcMgrDataList[index].statusValue | gDtcMgrInfoList[index].testFailedThisOp | testNotCompThisOp);

        if(0 != (stats & dtcStatusMask))
        {
			statusRecord[dataNum].dtcId = ((gDtcMgrInfoList[index].dtcId >> 8) & 0x00FFu) | ((gDtcMgrInfoList[index].dtcId << 8) & 0xFF00u); // CHERY SPEC (DIAG)
            statusRecord[dataNum].symtomId = gDtcMgrInfoList[index].symtomId;
            statusRecord[dataNum].statusOfDTC = stats;
            dataNum += 1;
        }
    }

    return dataNum;
}

/*******************************************************************************
*
* NAME:              DtcMgrGetSupportedDtcList
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static uint16 DtcMgrGetSupportedDtcList(uint8 dtcStatusMask, tDtcMgrStatusRecordType* statusRecord)
{
    uint8 index = 0;
    uint8 stats = 0;
    uint8 dataNum = 0;
    uint8 testNotCompThisOp = 0;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
        (gDtcMgrInfoList[index].testCompletedThisOp == 0) ? (testNotCompThisOp = DTCMGR_DTC_STATUS_TEST_COMPLETED_THIS_OP) : (testNotCompThisOp = 0);

        stats = (gDtcMgrDataList[index].statusValue | gDtcMgrInfoList[index].testFailedThisOp | testNotCompThisOp);
/* wl20220723 */
#if 0
        statusRecord[dataNum].dtcId = ((gDtcMgrInfoList[index].dtcId >> 8) & 0x00FFu) | ((gDtcMgrInfoList[index].dtcId << 8) & 0xFF00u); // CHERY SPEC (DIAG)
        statusRecord[dataNum].symtomId = gDtcMgrInfoList[index].symtomId;
        statusRecord[dataNum].statusOfDTC = stats;
        dataNum += 1;
#else      
        statusRecord[dataNum].dtcId = (((gDtcMgrInfoList[index].dtcId & 0xFF) << 8) | ((gDtcMgrInfoList[index].dtcId >> 8) & 0xFF));
        statusRecord[dataNum].symtomId = gDtcMgrInfoList[index].symtomId;
        statusRecord[dataNum].statusOfDTC = stats;
        dataNum += 1;
#endif        
    }

    return dataNum;
}

/*******************************************************************************
*
* NAME:              DtcMgrGetAvailStsMask
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
uint8 DtcMgrGetAvailStsMask(void)
{
    uint8 mask = 0;

    mask = DTCMGR_AVAILABLE_MASK;

    return mask;
}
/*                      END SERVICE REQUEST PROCESS                        */

/*******************************************************************************
*
* NAME:              DtcMgrChkDtcSetting
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      state
* DESCRIPTION:
*
*******************************************************************************/
uint8 DtcMgrChkDtcSetting(void)
{
    uint8 idx = 0;
    uint8 result = FALSE ;

    for(idx = 0 ; idx < DTC_INDEX_MAX ; idx++)
    {
        if(DTCMGR_DTC_VALUE_IS(gDtcMgrDataList[idx].statusValue, DTCMGR_DTC_STATUS_FAILED))
        {
            result = TRUE;
            idx = DTC_INDEX_MAX;
        }
    }

    return result;
}

uint8 DtcMgrChkDtcConfirmed(void)
{
    uint8 idx = 0;
    uint8 result = FALSE ;

    for(idx = 0 ; idx < DTC_INDEX_MAX ; idx++)
    {
        if(DTCMGR_DTC_VALUE_IS(gDtcMgrDataList[idx].statusValue, DTCMGR_DTC_STATUS_CONFIRMED))
        {
            result = TRUE;
            idx = DTC_INDEX_MAX;
        }
    }

    return result;
}

/*                        START CLEAR DTC PROCESS                          */
/*******************************************************************************
*
* NAME:              DtcMgrClearDtcReqProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
uint8 DtcMgrClearDtcReqProcess(uint32 groupOfDTC)
{
    uint8 index = 0;
    uint8 exist = FALSE;
    uint16 dtcId;
    uint8 symtomId;

    dtcId = groupOfDTC >> 8;
	symtomId = groupOfDTC & 0xFFu;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
    	if((dtcId == gDtcMgrInfoList[index].dtcId)
    		&& (symtomId == gDtcMgrInfoList[index].symtomId))
    	{
	        gDtcMgrDataList[index].agingCnt = 0;
	        gDtcMgrDataList[index].tripCnt = 0;
	        gDtcMgrDataList[index].statusValue = 0;

	        gDtcMgrInfoList[index].testFailedThisOp = 0;
	        gDtcMgrInfoList[index].testCompletedThisOp = 0;

	        MntFltDtcClearValue(index);

	        exist = TRUE;
	        break;
        }
    }

    if(TRUE == exist)
    {
		// SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
        /* wl20220819 */
        WriteDTCData();
    }

    return exist;
}

/*******************************************************************************
*
* NAME:              DtcMgrClearDtcReqProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
uint8 DtcMgrGroupClearDtcReqProcess(uint32 groupOfDTC)
{
    uint8 index = 0;
    uint8 exist = FALSE;
    uint16 groupId;

    groupId = groupOfDTC >> 8;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
    	if(groupId == (DTCMGR_GROUPS_MASK & gDtcMgrInfoList[index].dtcId))
    	{
	        gDtcMgrDataList[index].agingCnt = 0;
	        gDtcMgrDataList[index].tripCnt = 0;
	        gDtcMgrDataList[index].statusValue = 0;

	        gDtcMgrInfoList[index].testFailedThisOp = 0;
	        gDtcMgrInfoList[index].testCompletedThisOp = 0;

	        MntFltDtcClearValue(index);

	        exist = TRUE;
        }
    }

    if(TRUE == exist)
    {
		// SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
        /* wl20220819 */
        WriteDTCData();
    }

    return exist;
}


/*******************************************************************************
*
* NAME:              DtcMgrClearDtcReqProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void WriteDTCData(void)
{
	SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
}

void DtcMgrAllClearDtcReqProcess(void)
{
    uint8 index = 0;

    for(index = 0 ; index < DTC_INDEX_MAX ; index++)
    {
        gDtcMgrDataList[index].agingCnt = 0;
        gDtcMgrDataList[index].tripCnt = 0;
        gDtcMgrDataList[index].statusValue = 0;

        gDtcMgrInfoList[index].testFailedThisOp = 0;
        gDtcMgrInfoList[index].testCompletedThisOp = 0;

        MntFltDtcClearValue(index);
    }

//    SYS_NVM_UPDATE_DTC_DATA((uint8 *)gDtcMgrDataList, sizeof(gDtcMgrDataList));
    /* wl20220819, write 32 bytes everytime */
    WriteDTCData();
}




/*******************************************************************************
*
* NAME:              DtcMgrGetDtcFailedAndFailedThisOp
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      state
* DESCRIPTION:
*
*******************************************************************************/
uint8 DtcMgrGetDtcFailedAndFailedThisOp(tDtcMgrIdListType dtcIdx)
{
	uint8 state = FALSE;

	state = ((0 != gDtcMgrInfoList[dtcIdx].testCompletedThisOp) &&
		(0 != (gDtcMgrDataList[dtcIdx].statusValue & DTCMGR_DTC_STATUS_FAILED))) ? TRUE : FALSE;

	return state;
}

void NvmDataInit(void)
{
    static uint8  u8K30Reset = 0u;
    static uint8  u8GetStartTime = 0u;
    static uint16 u16TaskTick = 0u;
    uint8 *data;

    if(!u8GetStartTime)
    {
        u8GetStartTime = 1u;
        u16TaskTick = GetTickCount();
    }
    else
    {
        /* Do nothing */    
    }
    
    /* wl20220802 init Nvm data */
    if((!u8K30Reset) && (GetElapsedTime(u16TaskTick) >= 3000u))
    {
    	u8K30Reset = 1u;
        data = DTC_DATA_READ();
	    DID_DATA_READ();
        UTL_COPY_DATA((uint8 *)gDtcMgrDataList, data, sizeof(gDtcMgrDataList));
    }
    else
    {
        /* Do nothing */
    }
}

/*                        END CLEAR DTC PROCESS                          */

