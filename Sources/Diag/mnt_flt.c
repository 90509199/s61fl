/*
 * mnt_flt.c
 *
 *  Created on: 2020. 7. 22.
 *      Author: Digen
 */

#include "mnt_flt.h"
#include "Service/std_util.h"
#include "Service/std_timer.h"
#include "Service/srv_mcan.h"
/**********************************************************************************************************************
  DEFINE
**********************************************************************************************************************/
#define MNTFLT_LOG_MSG      (0u)
#define MNTFLT_LOG_EVENT    (1u)
#define MNTFLT_LOG_ERR      (0u)
#define MNTFLT_LOG_SPECIAL  (0u)

#define MNTFLT_5MS_TIMER   (5u)
#define MNTFLT_100MS_TIMER (100u)

#define MNTFLT_DIAG_START_TIME 	(3000u)

#define MNT_FLT_500MS			(5u)

#define MNT_FLT_FLG_IS(m, s)    STD_MASK_IS(m, s)
#define MNT_FLT_FLG_CLR(m, s)    STD_MASK_CLR(m, s)
#define MNT_FLT_FLG_SET(m, s)    STD_MASK_SET(m, s)

#define MCU_ACC_DET()			ON

#define MNT_FLT_GET_IGN_STATE()		MCU_ACC_DET()
/**********************************************************************************************************************
  STRUCTURE
**********************************************************************************************************************/
typedef struct{
    mntFltResultType preResult;
    uint8 stepUp;
    uint8 stepDown;
    uint8 faultDtcCnt;
    uint8 fdcMax;
}tMntFltDtcListTableType;

typedef struct{
	uint8 isFault;
	mntFltIdListType dtcIdx;
}tMntFltLosComTableType;
/**********************************************************************************************************************
  GLOBAL VARIABLE
**********************************************************************************************************************/

/**********************************************************************************************************************
  Function declare
**********************************************************************************************************************/
static void MntFltCanBusOff(void);
static void MntFltDtcLosCom(void);
static mntFltResultType  MntFltDtcCntControl(tMntFltDtcListTableType* mntFlt, mntFltResultType mntResult );
static uint8 MntFltCheckVoltage(void);
/**********************************************************************************************************************
  GLABAL VARIABLE
**********************************************************************************************************************/
static tMntFltDtcListTableType gMntFltDtcTable[DTC_INDEX_MAX] = {
    /*  preResult | stepUp | stepDown | faultDtcCnt | fdcMax  */
// //    {DTC_NO_TEST, 0x40, 0x40, 0x00, 0x80}, /* DTC_POWER_OPEN		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
// //    {DTC_NO_TEST, 0x40, 0x40, 0x00, 0x80}, /* DTC_POWER_LOW			idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
// //    {DTC_NO_TEST, 0x40, 0x40, 0x00, 0x80}, /* DTC_POWER_HIGH		idx : 02 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */

//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_BT_MODULE_FAILURE	idx : 03 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_NAVI_MOUNT		idx : 04 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
// //    {DTC_NO_TEST, 0x40, 0x40, 0x00, 0x80}, /* DTC_DMS_SDK_LICENSE	idx : 05 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
// //    {DTC_NO_TEST, 0x40, 0x40, 0x00, 0x80}, /* DTC_DMS_HARWARE		idx : 06 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */

//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_BUS_OFF_FAULT		idx : 07 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */

//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_CEM		idx : 08 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_IPM		idx : 09 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_ICM		idx : 10 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
//     #ifdef DTC_LIST_AVMCOM
//     {DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_AVM		idx : 11 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
//     #endif

//     {DTC_NO_TEST, 0x40, 0x40, 0x00, 0x80}, /* DTC_SW_CONFIG_ERROR	idx : 12 | stepUp : 64 | stepDown : 64  | FDC MAX : 128 */

	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_VOLT_BELOW		        idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_VOLT_ABOVE		        idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_SPK_CNECT_FAIL    		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_AMP_FAIL          		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_Tuner_IC_FAIL     		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_Voice_IC_FAIL     		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_MCU_SOC_COM_FAIL  		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_SOC_OVERTEMP      		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_USB1_CURRENT_ABOVE		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_USB2_CURRENT_ABOVE		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_SOC_DES_COM_FAIL  		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_SOC_TFT_COM_FAIL  		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_SOFT_CONFIG_Error 		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_BUS_OFF_FAULT     		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_VCU       		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_BMS       		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_BCM       		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_ICM       		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_SAS       		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_ACCM      		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_SCM_R     		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_AVAS      		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_BSD       		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_FCM       		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_TBOX      		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_MFS       		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_ABM       		idx : 00 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
	{DTC_NO_TEST, 0x2B, 0x2B, 0x00, 0x80}, /* DTC_LOS_COM_WCM       		idx : 01 | stepUp : 43 | stepDown : 43  | FDC MAX : 128 */
};

static tMntFltLosComTableType gMntFltLosComFault[LOS_COM_INDEX_MAX] = {
#if 0
	{FALSE, DTC_LOS_COM_CEM},
	{FALSE, DTC_LOS_COM_IPM},
	{FALSE, DTC_LOS_COM_ICM},
	#ifdef DTC_LIST_AVMCOM
	{FALSE, DTC_LOS_COM_AVM},	
	#endif
#endif
	{FALSE, DTC_LOS_COM_VCU  },
	{FALSE, DTC_LOS_COM_BMS  },
	{FALSE, DTC_LOS_COM_BCM  },
	{FALSE, DTC_LOS_COM_ICM  },
	{FALSE, DTC_LOS_COM_SAS  },
	{FALSE, DTC_LOS_COM_ACCM },
	{FALSE, DTC_LOS_COM_SCM_R},
	{FALSE, DTC_LOS_COM_AVAS },
	{FALSE, DTC_LOS_COM_BSD  },
	{FALSE, DTC_LOS_COM_FCM  },
	{FALSE, DTC_LOS_COM_TBOX },
	{FALSE, DTC_LOS_COM_MFS  },
	{FALSE, DTC_LOS_COM_ABM  },
	{FALSE, DTC_LOS_COM_WCM  }
};

static uint8 gMntFltLosComFault_DVR = FALSE;

static stTimerType gMntFlt100msTmr;
static stTimerType gMntFlt5msTmr;
static stTimerType gMntFltDiagStartTmr;

static uint8 gMntFltDisabled = FALSE;

#if MNTFLT_LOG_SPECIAL
static uint8 gMntFltLogFlag = FALSE;
#endif

static uint8 gMntFltDtcAvailableFlg = FALSE;

/*******************************************************************************
*
* NAME:              MntInit
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltInit(void)
{
    START_TIMER(gMntFlt100msTmr, MNTFLT_100MS_TIMER);
    START_TIMER(gMntFlt5msTmr, MNTFLT_5MS_TIMER);
}

/*******************************************************************************
*
* NAME:              MntFltDtcClearValue
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltDtcClearValue(uint8 index)
{
    gMntFltDtcTable[index].faultDtcCnt = 0;
}

/*******************************************************************************
*
* NAME:              MntFltMgr
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltMgr(void)
{
    uint8 tmrSts = TIMER_STATE_STOP;
    uint8 tmr5mSts = TIMER_STATE_STOP;
	uint8 diagStartTmrSts = TIMER_STATE_STOP;
	uint8 ignSts = OFF;
	uint8 vBatIsNormal = FALSE;
	static uint8 preVbatIsNormal = FALSE;
	static uint8 preIgnSts = FALSE;

	/* Check Monitoring possiblity */
	ignSts = MNT_FLT_GET_IGN_STATE();
	vBatIsNormal = MntFltCheckVoltage();

	if(OFF != ignSts)
	{
		if( (preIgnSts != ignSts) && (FALSE != vBatIsNormal) )
		{
			DPRINTF(DBG_MSG7, "[MNT_FLT] Start Diag Start Timer\n");
			START_TIMER(gMntFltDiagStartTmr, MNTFLT_DIAG_START_TIME);
		}
		else if(preVbatIsNormal != vBatIsNormal)
		{
			if(FALSE != vBatIsNormal)
			{
				DPRINTF(DBG_MSG7, "[MNT_FLT] Start Diag Start Timer\n");
				START_TIMER(gMntFltDiagStartTmr, MNTFLT_DIAG_START_TIME);
			}
			else
			{
				DPRINTF(DBG_MSG7, "[MNT_FLT] Fault Monitoring is not available\n");

				gMntFltDtcAvailableFlg = FALSE;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		DPRINTF(DBG_MSG7, "[MNT_FLT] Fault Monitoring is not available\n");

		gMntFltDtcAvailableFlg = FALSE;
	}
	
	preVbatIsNormal = vBatIsNormal;
	preIgnSts = ignSts;

	/* Check Diag Start Timer */
	diagStartTmrSts = IS_TIMER(gMntFltDiagStartTmr);

	if(TIMER_STATE_TIMEOUT == diagStartTmrSts)
	{
		STOP_TIMER(gMntFltDiagStartTmr);

		DPRINTF(DBG_MSG7, "[MNT_FLT] Fault Monitoring is available\n");		

		gMntFltDtcAvailableFlg = TRUE;
	}

	/* Fault Monitoring */
	if(FALSE != gMntFltDtcAvailableFlg)
	{
	    tmrSts = IS_TIMER(gMntFlt100msTmr);
		tmr5mSts = IS_TIMER(gMntFlt5msTmr);

	    if(TIMER_STATE_TIMEOUT== tmr5mSts)
	    {
	    	RESTART_TIMER(gMntFlt5msTmr);
			/* wl20220730 */
			// MntFltDtcLosCom();
		}

	    if(TIMER_STATE_TIMEOUT == tmrSts)
	    {
	    	RESTART_TIMER(gMntFlt100msTmr);
			MntFltCanBusOff();
	    }
	}
}

void MntFltDtcCpuCheck(void)
{
	// Send command "MSG_DTC_CMD" to CPU
}

void MntFltDtcCpuSts(uint8 msg)
{
#if 0
	//BT MODULE	
	if (msg & 0x01) { MntFltSetResultProcess(DTC_BT_MODULE_FAILURE, DTC_FAILED); }
	else       { MntFltSetResultProcess(DTC_BT_MODULE_FAILURE, DTC_PASSED); }     

	//NAVI MOUNT
	if (msg & 0x02) { MntFltSetResultProcess(DTC_NAVI_MOUNT, DTC_FAILED); }
	else            { MntFltSetResultProcess(DTC_NAVI_MOUNT, DTC_PASSED); }

	//DMS SDK LICENSE
	if (msg & 0x04) { MntFltSetResultProcess(DTC_DMS_SDK_LICENSE, DTC_FAILED); }
	else            { MntFltSetResultProcess(DTC_DMS_SDK_LICENSE, DTC_PASSED); }

	//DMS HARDWARE
	if (msg & 0x08) { MntFltSetResultProcess(DTC_DMS_HARWARE, DTC_FAILED); }
	else            { MntFltSetResultProcess(DTC_DMS_HARWARE, DTC_PASSED); }
#else
// 	//BT MODULE 
// 	if (msg & 0x01) { MntFltSetResultProcess(DTC_BT_MODULE_FAILURE, DTC_PASSED); }
// 	else			 { MntFltSetResultProcess(DTC_BT_MODULE_FAILURE, DTC_FAILED); } 		

// 	//NAVI MOUNT
// 	if (msg & 0x02) { MntFltSetResultProcess(DTC_NAVI_MOUNT, DTC_PASSED); }
// 	else						{ MntFltSetResultProcess(DTC_NAVI_MOUNT, DTC_FAILED); }

// 	//DMS SDK LICENSE
// //	if (msg & 0x04) { MntFltSetResultProcess(DTC_DMS_SDK_LICENSE, DTC_PASSED); }
// //	else						{ MntFltSetResultProcess(DTC_DMS_SDK_LICENSE, DTC_FAILED); }

// 	//DMS HARDWARE
// //	if (msg & 0x08) { MntFltSetResultProcess(DTC_DMS_HARWARE, DTC_PASSED); }
// //	else						{ MntFltSetResultProcess(DTC_DMS_HARWARE, DTC_FAILED); }

#endif
}

/*******************************************************************************
*
* NAME:              MntFltCanBusOff
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
extern uint8 gDiagSessionDefaultByBussOffFlag;
extern uint8 GetBusOffNum(void);
extern uint8 GetCANTxConfirmation(void);
eCanControllerSts eCanErrSts = CAN_ERROR_ACTIVE_STS;
void BusoffDTCDetect(void)
{
	static uint16 u16Counter = 0u;

	/* wl20220801 */
	/* Set busoff DTC */
	if(GetBusOffNum() >= 3u)
	{
		u16Counter = 3000u;
		eCanErrSts = CAN_BUS_OFF_STS;
	}
	/* Bus normal from busoff,Wait 3s to clear busoff DTC */
	else if((GetCANTxConfirmation() == 1u) && (u16Counter > 0u))
	{
		u16Counter -= 1u;
		eCanErrSts = CAN_BUS_OFF_STS;
	}
	/* Clear busoff DTC */
	else if((GetCANTxConfirmation() == 1u) && (u16Counter == 0u))
	{
		eCanErrSts = CAN_ERROR_PASSIVE_STS;
	}
	else
	{
		/* Do nothing */
	}
}

uint8 GetBusoffState(void)
{
	return eCanErrSts;
}

static void MntFltCanBusOff(void)
{
    eCanControllerSts canErrSts = CAN_ERROR_ACTIVE_STS;
    mntFltResultType mntResult = DTC_PASSED;

    // canErrSts = SrvMcanGetErrSts();
	/* wl20220801 */
	canErrSts = GetBusoffState();
    if(CAN_BUS_OFF_STS == canErrSts)
    {
        mntResult = DTC_FAILED;
		gDiagSessionDefaultByBussOffFlag = TRUE; // CHERY SPEC (DIAG)
    }
    else
    {
        mntResult = DTC_PASSED;
    }

   	MntFltSetResultProcess(DTC_BUS_OFF_FAULT, mntResult);

}


/*******************************************************************************
*
* NAME:              MntFltDtcLosCom
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static void MntFltDtcLosCom(void)
{
#if 1
	static uint8 index = 0;
	
    eCanControllerSts canErrSts = CAN_ERROR_ACTIVE_STS;
    mntFltResultType mntResult = DTC_PASSED;

    canErrSts = SrvMcanGetErrSts();	

	if(CAN_ERROR_ACTIVE_STS == canErrSts)
	{
		if(FALSE != gMntFltLosComFault[index].isFault)
		{
            mntResult = DTC_FAILED;
        }
        else
        {
            mntResult = DTC_PASSED;
        }

        // MntFltSetResultProcess(gMntFltLosComFault[index].dtcIdx, mntResult);
	}

	index = ( (index + 1) % (uint8)LOS_COM_INDEX_MAX );
#endif

	// /* Add DTC start check condition 20220728 */
	// if(GetDTCStartProcessState() == 1u)
	// {
	// 	CommDTCDetect();
	// }
	// else
	// {
	// 	/* Do nothing */
	// }
}

/*******************************************************************************
*
* NAME:              MntFltSetResultProcess
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltSetResultProcess(mntFltIdListType dtcIdx, mntFltResultType mntResult)
{
    mntFltResultType result = DTC_NO_TEST;

    if(FALSE == gMntFltDisabled)
    {
        #if MNTFLT_LOG_SPECIAL
        if(dtcIdx == 0)
        {
            DPRINTF(DBG_MSG7, "[MNT_FLT] dtcIdx[%d] :  \n", dtcIdx );
            gMntFltLogFlag = TRUE;
        }
        #endif

        result = MntFltDtcCntControl(&gMntFltDtcTable[dtcIdx], mntResult );
        DtcMgrSetDtcStatus(dtcIdx, result);
    }
}

/*******************************************************************************
*
* NAME:              MntFltDtcCntControl
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static mntFltResultType  MntFltDtcCntControl(tMntFltDtcListTableType* mntFlt, mntFltResultType mntResult)
{
    uint8 addCnt = 0;
    mntFltResultType result = DTC_NO_TEST;

    if(DTC_FAILED == mntResult)
    {
        addCnt = mntFlt->stepUp;
    }
    else
    {
        addCnt = mntFlt->stepDown;
    }

    if(mntResult != mntFlt->preResult)
    {
        mntFlt->faultDtcCnt = addCnt;

        #if MNTFLT_LOG_SPECIAL
        if(TRUE == gMntFltLogFlag)
        {
            DPRINTF(DBG_MSG7, "[CNT START] result : %x, Cnt : [%d] \n", mntResult, mntFlt->faultDtcCnt);
        }
        #endif
    }
    else
    {
        mntFlt->faultDtcCnt += addCnt;

        #if MNTFLT_LOG_SPECIAL
        if(TRUE == gMntFltLogFlag)
        {
            DPRINTF(DBG_MSG7, "[CNT ING] [%d]\n", mntFlt->faultDtcCnt);
        }
        #endif

    }

    if(mntFlt->faultDtcCnt >= mntFlt->fdcMax)
    {
        #if MNTFLT_LOG_SPECIAL
        if(TRUE == gMntFltLogFlag)
        {
            DPRINTF(DBG_MSG7, "[CNT MAX] [%d] = [%d] \n", mntFlt->faultDtcCnt, mntFlt->fdcMax);
        }
        #endif
        result = mntResult;
    }

    mntFlt->preResult = mntResult;

    #if MNTFLT_LOG_SPECIAL
    gMntFltLogFlag = FALSE;
    #endif

    return result;

}

/*******************************************************************************
*
* NAME:              DtcMgrSetMntDisabled
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:         mode ( TRUE ,  FALSE)
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
static uint8 MntFltCheckVoltage(void)
{
	uint8 isNormal = FALSE;

	/* To do - Read battery state and decision normal or not. */
	
	isNormal = TRUE;
	
	return isNormal;
}
	
/*******************************************************************************
*
* NAME:              DtcMgrSetMntDisabled
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:         mode ( TRUE ,  FALSE)
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltSetMntDisabled(uint8 mode)
{
	if(FALSE != mode)
	{
		STOP_TIMER(gMntFlt100msTmr);
		STOP_TIMER(gMntFlt5msTmr);
	}
	else
	{
		START_TIMER(gMntFlt100msTmr, MNTFLT_100MS_TIMER);
		START_TIMER(gMntFlt5msTmr, MNTFLT_5MS_TIMER);
	}

    gMntFltDisabled = mode;
}

/*******************************************************************************
*
* NAME:              MntFltSetLosComFalut
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:         losComFltIdx
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltSetLosComFalut(tLosComListType losComFltIdx)
{
	gMntFltLosComFault[losComFltIdx].isFault = TRUE;
}

/*******************************************************************************
*
* NAME:              MntFltClrLosComFalut
*
* CALLED BY:
* PRECONDITIONS:
*
* PARAMETER:         losComFltIdx
* RETURN VALUE:      none
* DESCRIPTION:
*
*******************************************************************************/
void MntFltClrLosComFalut(tLosComListType losComFltIdx)
{
	gMntFltLosComFault[losComFltIdx].isFault = FALSE;
}

void MntFltSetLosComFalut_DVR(void)
{
	gMntFltLosComFault_DVR = TRUE;
}

void MntFltClrLosComFalut_DVR(void)
{
	gMntFltLosComFault_DVR = FALSE;
}

uint8 MntFltGetLosComFalut_DVR(void)
{
	return (gMntFltLosComFault_DVR);
}

