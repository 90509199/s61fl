/*
 * mnt_flt.h
 *
 *  Created on: 2020. 7. 22.
 *      Author: Digen
 */

#ifndef DIAG_MNT_FLT_H_
#define DIAG_MNT_FLT_H_

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#include "../includes.h"
#include "dtc_mgr.h"
/**********************************************************************************************************************
  DEFINE
**********************************************************************************************************************/

/**********************************************************************************************************************
  STRUCTURE
**********************************************************************************************************************/
typedef tDtcMgrResultType mntFltResultType;
typedef tDtcMgrIdListType mntFltIdListType;

typedef enum{
#if 0
    LOS_COM_CEM = 0,                      
    LOS_COM_IPM,                         
    LOS_COM_ICM, 
    #ifdef DTC_LIST_AVMCOM
    LOS_COM_AVM,
    #endif
#endif    
    LOS_COM_VCU,
    LOS_COM_BMS,
    LOS_COM_BCM,
    LOS_COM_ICM,
    LOS_COM_SAS,
    LOS_COM_ACCM,
    LOS_COM_SCM_R,
    LOS_COM_AVAS,
    LOS_COM_BSD,
    LOS_COM_FCM,
    LOS_COM_TBOX,
    LOS_COM_MFS,
    LOS_COM_ABM,
    LOS_COM_WCM,
    LOS_COM_INDEX_MAX
}tLosComListType;

/**********************************************************************************************************************
  FUNCTION
**********************************************************************************************************************/
extern void MntFltInit(void);
extern void MntFltDtcClearValue(uint8 index);
extern void MntFltClearValue(void);
extern void MntFltMgr(void);

extern void MntFltDtcCpuCheck(void);
extern void MntFltDtcCpuSts(uint8 msg);
extern void MntFltSetResultProcess(mntFltIdListType dtcIdx, mntFltResultType mntResult); //To provide for set or clear fault state

extern void MntFltSetMntDisabled(uint8 mode);

extern void MntFltSetLosComFalut(tLosComListType losComFltIdx);
extern void MntFltClrLosComFalut(tLosComListType losComFltIdx);
extern void  MntFltSetLosComFalut_DVR(void);
extern void  MntFltClrLosComFalut_DVR(void);
extern uint8 MntFltGetLosComFalut_DVR(void);

#endif /* DIAG_MNT_FLT_H_ */
