#ifndef __SRV_CANTP__
#define __SRV_CANTP__
#include "../includes.h"

#define SRVCANTP_STMIN_TIME		(0u)

#define SRVCANTP_DATA_DLC			(8u)


/* maxNumberOfBlockLength */
#define SRVCANTP_BLOCK_LEN_MAX		(1026)


#define SRVCANTP_CR_MAX				(150)	/* CR : 150ms */ /* timer Between consecutive messages */


#define SRVCANTP_DATA_LEN_MAX		(1034)	/* (SRVCANTP_BLOCK_LEN_MAX + 8 dummy bytes) */
#define SRVCANTP_RESP_LEN_MAX		(8u)


#define SRVCANTP_REQ_FID				(0x101u)

#define SRVCANTP_REQ_PID				(0x722u)
#define SRVCANTP_RESP_PID				(0x72Au)


typedef struct
{
    uint8 *data;
    uint16 length;
    uint16 pos;
    uint8 cfNum;
}hSrvCanTpMessageType;

typedef struct{
    uint16 id;
    uint8 dlc;
    uint8 canData[8];
}tCanTpRawDataType;

extern uint8 SrvCanTpRxEnQueue(uint16 id, uint8* data, uint8 dlc);

extern void SrvCanTpInit(void);
extern void SrvCanTpSetRespID(uint16 id);
extern void SrvCanTpReqLock(uint8 lock);

extern void SrvCanTpRx(uint8 isFId, uint8 *pData, uint8 cnt);
extern void SrvCanResp(uint8 *pData, uint16 cnt);

extern void SrvCanTpMgr(void);

extern void SrvCanTpTxConfirm(void);
extern void CanTpSetRxFlg(uint8 isFId);


#endif /* __SRV_CANTP__ */
