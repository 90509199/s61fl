/**************************************************************************
 *  PROJECT:        CSP
 **************************************************************************/
/**********************************************************************************************************************
  INCLUDE
**********************************************************************************************************************/
#include "srv_cantp.h"
#include "../Service/std_util.h"
#include "../Service/std_timer.h"
#include "../Service/srv_mcan.h"
#include "CanIl/canil_MCan_if.h"
#include "Diag/diag.h"
/**********************************************************************************************************************
  DEFINE
**********************************************************************************************************************/
#define SRVCANTP_PHYSICAL_ID		(0x743)

#define SRVCANTP_SF					(0x00u)
#define SRVCANTP_FF					(0x10u)
#define SRVCANTP_CF					(0x20u)

#define SRVCANTP_PADDING_DATA		(0x00u)

#define SRVCANTP_IS_SF(d)				(((d)[0] & 0xF0u) == SRVCANTP_SF)
#define SRVCANTP_IS_FF(d)				(((d)[0] & 0xF0u) == SRVCANTP_FF)
#define SRVCANTP_IS_CF(d)				(((d)[0] & 0xF0u) == SRVCANTP_CF)
#define SRVCANTP_IS_FC(d)				((d)[0] == 0x30u)
#define SRVCANTP_IS_VALID_CF(d, c)	(((d)[0] & 0x0Fu) == ((c) & 0x0Fu))

#define SRVCANTP_DLC					(7u)

#define SRVCANTP_SF_DATA_POS(d)		((d)[1])
#define SRVCANTP_FF_DATA_POS(d)		((d)[2])
#define SRVCANTP_CF_DATA_POS(d)		((d)[1])

#define SRVCANTP_SF_GET_LEN(d)		((d)[0])
#define SRVCANTP_MF_GET_LEN(d)		(uint16)(((uint16)((d)[0] & 0x0Fu) << 8) | ((d)[1]))

#define SRVCANTP_FC_GET_STMIN(d)		((d)[2])
#define SRVCANTP_FC_GET_BSMAX(d)		((d)[1])

#define SRVCANTP_S3_DEFAULT_TIME		(0xFFu)

#define SRVCANTP_IS_TESTER_PRESENT(d)		(((d)[0] == 0x02u) && ((d)[1] == 0x3Eu))

#define SRVCANTP_N_ASAR_TIME		(25u)

#define SRVCANTP_PID_RX_FLAG	(0x01)
#define SRVCANTP_FID_RX_FLAG	(0x02)

#define SRV_CANTP_REQ_FF_INDI_EVENT(c)			DiagRequestFFIndiEvent((c))
#define SRV_CANTP_REQ_EVENT(f, d, c, e)			DiagRequestEvent((f), (d), (c), (e))
#define SRV_CANTP_RESP_DONE_EVENT(e)			DiagResponseDoenEvent((e))

#define SRV_CANTP_QUEUE_BUFF_SIZE (16u)

#define Q_DEFAULT (0x00u)
#define Q_EMPTY   (0x01u)
#define Q_FULL    (0x02u)

#define QUEUE_IS_FULL(state)           ( 0 != ((state) & (Q_FULL)) )
#define QUEUE_IS_EMPTY(state)          ( 0 != ((state) & (Q_EMPTY)) )

#define QUEUE_IS_NOT_EMPTY(state)          ( 0 == ((state) & (Q_EMPTY)) )

#define QUEUE_SET_FULL_STATE(state)    ( (state) |= (Q_FULL)  )
#define QUEUE_SET_EMPTY_STATE(state)   ( (state) |= (Q_EMPTY) )

#define QUEUE_FULL_STATE_CLEAR(state)  ( (state) &= ~(Q_FULL) )
#define QUEUE_EMPTY_STATE_CLEAR(state) ( (state) &= ~(Q_EMPTY) )
/**********************************************************************************************************************
  STRUCTURE
**********************************************************************************************************************/
typedef struct{
    uint8 front;
    uint8 rear;
    uint8 state;
    tCanTpRawDataType qBuf[SRV_CANTP_QUEUE_BUFF_SIZE];
}tSrvCanTpQueueDataType;

/**********************************************************************************************************************
  GLABAL VARIABLE
**********************************************************************************************************************/
/*
*	Addressing mode   normal
*	BSmax			   0
*	STmin			   0.0		ms
*/

static tSrvCanTpQueueDataType gRxDataQueue;

static const uint8 gSrvCanTpFlowCtrl[SRVCANTP_RESP_LEN_MAX] = {
	0x30u, 0x00, SRVCANTP_STMIN_TIME, 0x00u,
	0x00u, 0x00u, 0x00u, 0x00u
};

stTimerType gSrvCanTpCRTimer; //Consecutive Message Timer
stTimerType gSrvCanTpS3Timer;//STMin Timer
uint16 gSrvCanTpRespID = SRVCANTP_RESP_PID;

uint8 gSrvCanTpReqData[SRVCANTP_DATA_LEN_MAX];

hSrvCanTpMessageType gSrvCanTpReqMsg = {
	gSrvCanTpReqData,
	0, 0, 0
};

hSrvCanTpMessageType gSrvCanTpRespMsg = {
	NULL,
	0, 0, 0
};


static uint8 gSrvCanTpReqLock = FALSE; // Block during Rx Request processing
static uint8 gSrvCanTpS3TimerRestart = FALSE;

static uint8 gSrvCanTpFCBSmax = 0;
static uint8 gSrvCanTpFCBScnt = 0;

static stTimerType gSrvCanTpNAsArTimer;
static uint8 gSrvCanTpTxConfirm;
static uint8 gSrvCanTpRxFlg;

static tCanTpRawDataType gSrvCanTpRxDataBuff;

/**********************************************************************************************************************
  FUNCTION PROTOTYPE
**********************************************************************************************************************/
static uint8 DrvCanTpDeQueue(tCanTpRawDataType* output);
static void SrvCanTpReinit(void);
static uint8 SrvCanTpRxSF(hSrvCanTpMessageType *msg, uint8 *pData, uint8 cnt);
static uint8 SrvCanTpRxMF(hSrvCanTpMessageType *msg, uint8 *pData, uint8 cnt);
static void SrvCanTpTxData(uint8 *pData);
static uint8 SrvCanTpTx(hSrvCanTpMessageType *msg);

/**************************************************************************
 * FUNCTION NAME
 **************************************************************************/
 
 /**************************************************************************
 * SrvCanTpRxEnQueue
 **************************************************************************/
 uint8 SrvCanTpRxEnQueue(uint16 id, uint8* data, uint8 dlc)
{
    uint8 *rxData;
	uint8 idx = 0;
	uint8 qIsFull = FALSE;

    rxData = (uint8 *)data;

	if( QUEUE_IS_FULL(gRxDataQueue.state) )
	{
        qIsFull = TRUE;
        DPRINTF(DBG_MSG7, "[CAN_TP] Queue is full.\n");

	}
	else
	{
		gRxDataQueue.qBuf[gRxDataQueue.rear].id = id;
        gRxDataQueue.qBuf[gRxDataQueue.rear].dlc = dlc;

		for(idx = 0 ; idx < dlc ; idx ++)
		{
	        gRxDataQueue.qBuf[gRxDataQueue.rear].canData[idx] = rxData[idx];
		}
		
        DPRINTF(DBG_MSG7, "[CAN_TP] Rx id = %x, dlc = %x \n", id, dlc);

		QUEUE_EMPTY_STATE_CLEAR(gRxDataQueue.state);

		gRxDataQueue.rear = (gRxDataQueue.rear + 1) % SRV_CANTP_QUEUE_BUFF_SIZE;

		if(gRxDataQueue.rear == gRxDataQueue.front)
		{
			QUEUE_SET_FULL_STATE(gRxDataQueue.state);
		}
	}

	return qIsFull;
}

 /**************************************************************************
 * SrvCanTpRxEnQueue
 **************************************************************************/
static uint8 DrvCanTpDeQueue(tCanTpRawDataType* output)
{
	uint8 returnValue = TRUE;
	uint8 idx = 0;

	if( QUEUE_IS_EMPTY(gRxDataQueue.state))
	{
		returnValue = FALSE;
        DPRINTF(DBG_MSG7, "[CAN_TP] Queue is empty\n");
	}
	else
	{
		output->id = gRxDataQueue.qBuf[gRxDataQueue.front].id;
		output->dlc = gRxDataQueue.qBuf[gRxDataQueue.front].dlc;

		gRxDataQueue.qBuf[gRxDataQueue.front].id = 0;
		gRxDataQueue.qBuf[gRxDataQueue.front].dlc = 0;		

		for(idx = 0 ; idx < output->dlc ; idx ++)
		{
			output->canData[idx] = gRxDataQueue.qBuf[gRxDataQueue.front].canData[idx];

			gRxDataQueue.qBuf[gRxDataQueue.front].canData[idx] = 0x00;
		}
		
		gRxDataQueue.front = (gRxDataQueue.front + 1) % SRV_CANTP_QUEUE_BUFF_SIZE;
		QUEUE_FULL_STATE_CLEAR(gRxDataQueue.state);


    	if(gRxDataQueue.rear == gRxDataQueue.front)
    	{
    		QUEUE_SET_EMPTY_STATE(gRxDataQueue.state);
    	}
	}

	return returnValue;
}

 /**************************************************************************
 * SrvCanTpReinit
 **************************************************************************/
static void SrvCanTpReinit(void)
{
	STOP_TIMER(gSrvCanTpCRTimer);

	gSrvCanTpReqMsg.cfNum = 0;
	gSrvCanTpReqMsg.pos = 0;
}

 /**************************************************************************
 * SrvCanTpInit
 **************************************************************************/
void SrvCanTpInit(void)
{
	SrvCanTpReinit();

	STOP_TIMER(gSrvCanTpS3Timer);
}

 /**************************************************************************
 * SrvCanTpSetRespID
 **************************************************************************/
void SrvCanTpSetRespID(uint16 id)
{
	gSrvCanTpRespID = id;
}

 /**************************************************************************
 * SrvCanTpReqLock
 **************************************************************************/
void SrvCanTpReqLock(uint8 lock)
{
	gSrvCanTpReqLock = lock;
}

 /**************************************************************************
 * SrvCanTpRxSF
 **************************************************************************/
static uint8 SrvCanTpRxSF(hSrvCanTpMessageType *msg, uint8 *pData, uint8 cnt)
{
	uint8 bTestPresent = FALSE;

	if(0 != msg->cfNum)
	{
		/* check tester present message */
		bTestPresent = SRVCANTP_IS_TESTER_PRESENT(pData);
	}

	if(FALSE == bTestPresent)
	{/* single frame */

		/* Reset info for next message */
		SrvCanTpReinit();

		msg->length = SRVCANTP_SF_GET_LEN(pData);
		if((0 != msg->length) && (SRVCANTP_DATA_DLC > msg->length))
		{
			cnt--; /* length data */

			UTL_COPY_DATA(msg->data, &(SRVCANTP_SF_DATA_POS(pData)), cnt);
			msg->pos = cnt;
		}
	}

	return FALSE;
}

 /**************************************************************************
 * SrvCanTpRxMF
 **************************************************************************/
static uint8 SrvCanTpRxMF(hSrvCanTpMessageType *msg, uint8 *pData, uint8 cnt)
{
	uint8 bError = FALSE;
	uint16 stMin = 0;

	cnt--; /* header data */
	/* multi frame */
	if(TRUE == SRVCANTP_IS_CF(pData))
	{/* CF */
		if(TRUE == SRVCANTP_IS_VALID_CF(pData, msg->cfNum)) // Check SN
		{
			RESTART_TIMER(gSrvCanTpCRTimer);
			if(SRVCANTP_DATA_LEN_MAX >= (msg->pos + cnt))
			{
				UTL_COPY_DATA(&(msg->data[msg->pos]), &(SRVCANTP_CF_DATA_POS(pData)), cnt);
			}

			msg->pos += cnt;
			msg->cfNum ++;
		}
		else
		{
			/* bError */
			bError = TRUE;
			gSrvCanTpReqMsg.pos = 0;
		}
	}
	else if(TRUE == SRVCANTP_IS_FF(pData))
	{/* FF */
		/* Reset info for next message */
		SrvCanTpReinit();

		msg->length = SRVCANTP_MF_GET_LEN(pData);
		if(0 != msg->length)
		{
			cnt--; /* length data */

			UTL_COPY_DATA(msg->data, &(SRVCANTP_FF_DATA_POS(pData)), cnt);
			msg->pos = cnt;
			msg->cfNum ++;

			/* Send flow control message */
			SrvCanTpTxData((uint8 *)gSrvCanTpFlowCtrl);

			SRV_CANTP_REQ_FF_INDI_EVENT(msg->length);

			DPRINTF(DBG_MSG7, "[CAN_TP] Start CR Timer\n");

			START_TIMER(gSrvCanTpCRTimer, SRVCANTP_CR_MAX);
		}
	}
	else if(TRUE == SRVCANTP_IS_FC(pData))
	{/* flow control */
			
		gSrvCanTpFCBSmax = SRVCANTP_FC_GET_BSMAX(pData);
		gSrvCanTpFCBScnt = 0;

		gSrvCanTpS3TimerRestart = FALSE;

		stMin = SRVCANTP_FC_GET_STMIN(pData);

		/* wl20220723 */
		//if(0 != stMin)
		{
			stMin ++; /* for over time */
			START_TIMER(gSrvCanTpS3Timer, stMin);
		}
		// else
		// {
		// 	STOP_TIMER(gSrvCanTpS3Timer);
		// }
	}
	else
	{
		/* unknown bError */
		/* bError = TRUE; */
		RESTART_TIMER(gSrvCanTpCRTimer);
	}

	return bError;
}

 /**************************************************************************
 * SrvCanTpRx
 **************************************************************************/
void SrvCanTpRx(uint8 isFId, uint8 *pData, uint8 cnt)
{
	uint8 bError = FALSE;

	if(FALSE == gSrvCanTpReqLock)
	{
		if(TRUE == SRVCANTP_IS_SF(pData))
		{ /* single frame */
			
			bError = SrvCanTpRxSF(&gSrvCanTpReqMsg, pData, cnt);
		}
		else
		{ /* multi frame or flow control */
			if(TRUE != isFId)
			{
				bError = SrvCanTpRxMF(&gSrvCanTpReqMsg, pData, cnt);
			}
		}

		if(TRUE == bError)
		{
			/* Reset info for next message */
			SrvCanTpReinit();
		}
		else
		{/* no Error */
			if((0 != gSrvCanTpReqMsg.pos) && (gSrvCanTpReqMsg.pos >= gSrvCanTpReqMsg.length))
			{
				bError = (SRVCANTP_BLOCK_LEN_MAX < gSrvCanTpReqMsg.length);

				SrvCanTpReqLock(TRUE);
				/* call uds function */ /* TP Transmition Finish Pass to Session layer */
				SRV_CANTP_REQ_EVENT(isFId, gSrvCanTpReqMsg.data, gSrvCanTpReqMsg.length, bError);

				/* Reset info for next message */
				SrvCanTpReinit();
			}
		}
	}
}

 /**************************************************************************
 * SrvCanTpTxData
 **************************************************************************/
static void SrvCanTpTxData(uint8 *pData)
{
	gSrvCanTpTxConfirm = FALSE;
    SrvMcanSendReqWithData(DIAG_TX_75A_IDX, pData);
}

 /**************************************************************************
 * SrvCanResp
 **************************************************************************/
void SrvCanResp(uint8 *pData, uint16 cnt)
{
	uint8 txDone = FALSE;

	gSrvCanTpRespMsg.data = pData;
	gSrvCanTpRespMsg.length = cnt;

	gSrvCanTpRespMsg.pos = 0;
	gSrvCanTpRespMsg.cfNum = 0;

	gSrvCanTpFCBSmax = 0;
	gSrvCanTpFCBScnt = 0;

	gSrvCanTpS3TimerRestart = FALSE;
	STOP_TIMER(gSrvCanTpS3Timer);

	if(NULL != gSrvCanTpRespMsg.data)
	{
		txDone = SrvCanTpTx(&gSrvCanTpRespMsg);
		if(TRUE == txDone)
		{
			STOP_TIMER(gSrvCanTpNAsArTimer);

			gSrvCanTpRespMsg.data = NULL;
			SRV_CANTP_RESP_DONE_EVENT(FALSE);
		}	
	}
}

 /**************************************************************************
 * SrvCanTpTx
 **************************************************************************/
static uint8 SrvCanTpTx(hSrvCanTpMessageType *msg)
{
	uint8 txDone = FALSE;
	uint8 timerStatus = 0;
	uint8 idx = 0;
	uint8 data[SRVCANTP_DATA_DLC];
	uint16 duration = 0;

	//N_AS TIMER START
	START_TIMER(gSrvCanTpNAsArTimer, SRVCANTP_N_ASAR_TIME);
	
	if(msg->length > msg->pos)
	{
		UTL_SET_DATA(data, SRVCANTP_PADDING_DATA, SRVCANTP_DATA_DLC);

		if(0 == msg->cfNum)
		{/* SF or FF */
			if(msg->length > SRVCANTP_DLC)
			{/* FF */
				data[idx] = SRVCANTP_FF;
				data[idx] |= (uint8)((msg->length >> 8) & 0x0F); 		idx ++;
				data[idx] = (uint8)(msg->length & 0xFF); 				idx ++;

				START_TIMER(gSrvCanTpS3Timer, SRVCANTP_S3_DEFAULT_TIME);
				msg->pos = SRVCANTP_DATA_DLC - idx;
			}
			else
			{/* SF */
				data[idx] = msg->length; 				idx ++;
				msg->pos = msg->length;
			}

			UTL_COPY_DATA(&(data[idx]), msg->data, msg->pos);
			msg->cfNum ++;

			SrvCanTpTxData(data);
		}
		else
		{/* only CF */
			duration = GET_TIME_DURATION(gSrvCanTpS3Timer);
			if((FALSE == gSrvCanTpS3TimerRestart) || (0 == duration))// Why duration 0 is condition? If STmin is zero, Server send the CF messgae without delay.
			{
				timerStatus = IS_TIMER(gSrvCanTpS3Timer);
				if(TIMER_STATE_RUN != timerStatus)
				{/* timer is timeout or stop */
					if(SRVCANTP_S3_DEFAULT_TIME == duration)//Do not receive FC message after sending FF message.
					{
						/* reset */
						SrvCanResp(NULL, 0);
					}
					else //Send CF Message
					{
						if((0 == gSrvCanTpFCBSmax) || (gSrvCanTpFCBSmax > gSrvCanTpFCBScnt))
						{
							data[idx] = SRVCANTP_CF | (msg->cfNum & 0x0F); 	idx ++;

							UTL_COPY_DATA(&(data[idx]), &(msg->data[msg->pos]), SRVCANTP_DLC);
							msg->cfNum ++;
							msg->pos += SRVCANTP_DLC;

							SrvCanTpTxData(data);

							/* wl20220723 */
							//gSrvCanTpS3TimerRestart = TRUE;
							gSrvCanTpFCBScnt++;

							/* wl20220723 */
							RESTART_TIMER(gSrvCanTpS3Timer);
						}
					}
				}
			}
			else//STmin Timer Start
			{
				/* wl20220723 */
				// gSrvCanTpS3TimerRestart = FALSE;
				// RESTART_TIMER(gSrvCanTpS3Timer);
			}
		}
	}
	else
	{
		txDone = TRUE;
	}

	return txDone;
}

 /**************************************************************************
 * SrvCanTpMgr
 **************************************************************************/
void SrvCanTpMgr(void)
{
	uint8 txDone = FALSE;
	uint8 timerStatus = 0;

	if( QUEUE_IS_NOT_EMPTY(gRxDataQueue.state) )
	{
		DrvCanTpDeQueue(&gSrvCanTpRxDataBuff);

		if(SRVCANTP_PHYSICAL_ID == gSrvCanTpRxDataBuff.id)
		{
			SrvCanTpRx(FALSE, gSrvCanTpRxDataBuff.canData, gSrvCanTpRxDataBuff.dlc);
		}
		else
		{
			SrvCanTpRx(TRUE, gSrvCanTpRxDataBuff.canData, gSrvCanTpRxDataBuff.dlc);
		}
		
	}

	if( (NULL != gSrvCanTpRespMsg.data) && (FALSE != gSrvCanTpTxConfirm) )
	{
		txDone = SrvCanTpTx(&gSrvCanTpRespMsg);
		if(TRUE == txDone)
		{
			STOP_TIMER(gSrvCanTpNAsArTimer);

			gSrvCanTpRespMsg.data = NULL;
			SRV_CANTP_RESP_DONE_EVENT(FALSE);

		}
	}

	//CHECK N_AS TIMER
	timerStatus = IS_TIMER(gSrvCanTpNAsArTimer);
	if(TIMER_STATE_TIMEOUT == timerStatus)
	{
		STOP_TIMER(gSrvCanTpNAsArTimer);
		SrvCanResp(NULL, 0);
		//To do.. TP_IND to session layer 
	}

	timerStatus = IS_TIMER(gSrvCanTpCRTimer);
	if(TIMER_STATE_TIMEOUT == timerStatus)
	{
		STOP_TIMER(gSrvCanTpCRTimer);
		/* Reset info for next message */
		SrvCanTpReinit();
	}
}

 /**************************************************************************
 * SrvCanTpTxConfirm
 **************************************************************************/
void SrvCanTpTxConfirm(void)
{
	//N_AS TIMER STOP
	STOP_TIMER(gSrvCanTpNAsArTimer);
	
	gSrvCanTpTxConfirm = TRUE;
}

 /**************************************************************************
 * CanTpSetRxFlg
 **************************************************************************/
void CanTpSetRxFlg(uint8 isFId) 
 {
 	if(FALSE != isFId)
 	{
		UTL_MASK_SET(gSrvCanTpRxFlg, SRVCANTP_FID_RX_FLAG);
	}
	else
	{
		UTL_MASK_SET(gSrvCanTpRxFlg, SRVCANTP_PID_RX_FLAG);
	}
 }

