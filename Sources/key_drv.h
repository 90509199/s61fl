/*
 * key_drv.h
 *
 *  Created on: 2019. 1. 4.
 *      Author: Digen
 */

#ifndef KEY_DRV_H_
#define KEY_DRV_H_


#define   RELEASE   0
#define      PUSH   1

struct KEYDRV_DEV
{
	void (*Push)(void);
	void (*Release)(void);
	void (*LongKey)(void);
	void (*LongKeyRelease)(void);
	void (*Repeat)(void);
    uint8_t (*GetKeyPinStat)(void);
	uint32_t (*GetTickCount)(void);
	uint32_t tick_cnt;
	uint32_t longkey_time;
	uint32_t repeat_time;
	uint8_t stat;
};

typedef struct KEYDRV_DEV KEYDRV_DEV_t;

enum
{
    KEYDRV_IDLE = 0,		// 0
    KEYDRV_LONGKEY,		// 1
    KEYDRV_REPEAT,		// 2
    KEYDRV_RELEASE,		// 3
    KEYDRV_PUSH				// 4
};



void InitKeyStat(struct KEYDRV_DEV* keydrv);
void KeyTask(struct KEYDRV_DEV* keydrv);
static uint32_t Keydrv_GetElapsedTime(struct KEYDRV_DEV* _keydrv);


#endif /* KEY_DRV_H_ */
