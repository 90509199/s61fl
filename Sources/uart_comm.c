/*
 * uart_comm.c
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */
#include "includes.h"

uint8 CanKeyWorkMode = 0;
uint8 IHU_DVR_OperFlag;
uint8 Setting_Update = OFF;
uint8 u8Set1, u8Set2, u8Set3;

uint8 Uart_Data_Shft(uint8 Data, uint8 ShftNum);
uint8 UART_Data_Maching(uint8 Data, uint8 Len);

//#define UPRINTF(lvl, fmt, args...)		DPRINTF(lvl, fmt, ##args)
#define UPRINTF(lvl, fmt, args...)

#define uart_rx_tx_que_buf (1024) //(1024*3)

#ifdef CX62C_FUNCTION
#define uart_rx_buf_max (210 + 8) //(64+8)
#else
#define uart_rx_buf_max (64 + 8)
#endif

extern SECTOR_STAT_T   Self_BootSectorStat;
static uint8           TX_MSG_BUF[512];
static volatile uint32 uart_tx_timer_tick = 0;
static volatile uint32 uart_timer_tick    = 0;

static volatile uint16 tx_msg_cnt = 0;
static void            X50_UART_Rx_Service(DEVICE *dev);

static uint8_t uart_rx_data_buf[uart_rx_tx_que_buf];
static QUE_t   uart_rx_data_q;

#ifdef NEW_URAT_RX0
uint8_t uart0_rx_buf[uart_rx_tx_que_buf];
QUE_t   uart0_rx_q;
#endif

#ifdef NEW_URAT_RX1
uint8_t uart1_rx_buf[uart_rx_tx_que_buf];
QUE_t   uart1_rx_q;
#endif

#ifdef NEW_URAT_TX
QUE_t   uart0_tx_q;
uint8_t uart0_tx_buf[1024];
QUE_t   uart1_tx_q;
uint8_t uart1_tx_buf[1024];
#endif

uint16 MCU_TX_CNT = 0;
uint16 Version_1  = 162;
uint16 Version_2  = 20;
uint16 Version_3  = 101;

MSGPacketInfo UART_Packet_Info;
MSGKeyInfo    KeyInfo_323;

static BUTTON_TX_t       button_tx_data;
static volatile uint32_t uart0_tx_tick = 0;

// tx packet
SendMSGbuf acc_packet;
SendMSGbuf radio_packet;
SendMSGbuf key_packet;
SendMSGbuf fm_check_radio_packet;
SendMSGbuf dtc_list_cmd;
SendMSGbuf dvr_ctl_cmd;
SendMSGbuf acc_ign_status;
SendMSGbuf radio_signal_status;

// data buf size
uint8 acc_buf[2];
uint8 radio_buf[255];
uint8 key_buf[4];
uint8 fm_check_radio_buf[10];
uint8 dtc_request[1];
uint8 dvr_ctl_buf[1];
uint8 acc_ign_buf[1];
uint8 radio_signal_buf[6];
uint8 manufacture_maxim;

SendMSGbuf Last_Memory;

SendMSGbuf CGW_VCU_P_5_123; // Cycle : 20ms
SendMSGbuf CGW_BCM_MFS_1_294;
SendMSGbuf CGW_BCM_1_245;
SendMSGbuf CGW_EGS_1_215;
SendMSGbuf CGW_EPB_1_231;
SendMSGbuf CGW_BCS_C_1_225;
SendMSGbuf CGW_ICM_1_271;
SendMSGbuf CGW_BMS_3_25A;
SendMSGbuf CGW_EPS_1_238;
SendMSGbuf CGW_BCM_PEPS_1_291;
SendMSGbuf CGW_BSD_1_298;
SendMSGbuf CGW_VCU_3_4_28A;
SendMSGbuf CGW_FCM_10_3FC;
SendMSGbuf CGW_FCM_2_307;
SendMSGbuf CGW_FCM_6_3DC;
SendMSGbuf CGW_FCM_7_3DE;
SendMSGbuf CGW_FCM_FRM_6_387;
SendMSGbuf CGW_FCM_FRM_8_3ED;
SendMSGbuf CGW_FCM_FRM_9_3FA;
SendMSGbuf ACCM_1_3D2;
SendMSGbuf CGW_BCM_4_324;
SendMSGbuf ICM_2_3B1;
SendMSGbuf CGW_BCM_5_323;
SendMSGbuf BCM_6_457;
SendMSGbuf BCM_TPMS_1_326;
SendMSGbuf BCM_ALM_45D;
SendMSGbuf CGW_BCM_SCM_3_45C;
SendMSGbuf CGW_BMS_3_46B;
SendMSGbuf CGW_BCM_SCM_1_45A;
SendMSGbuf CGW_RCM_1_328;
SendMSGbuf CGW_BCM_EAC_1_3D8;
SendMSGbuf CGW_SAS_2_504;
SendMSGbuf CGW_VCU_3_6_313;
SendMSGbuf CGW_VCU_3_7_314;
SendMSGbuf CGW_VCU_B_5_315;
SendMSGbuf CGW_BMS_9_355;
SendMSGbuf CGW_FCM_5_4DD;
SendMSGbuf CGW_FCM_FRM_5_4E3;
SendMSGbuf CGW_PLG_1_32A;
SendMSGbuf CGW_BCM_FM_458;
SendMSGbuf CGW_VCU_3_12_347;
SendMSGbuf CGW_BCM_TPMS_2_455;
SendMSGbuf CGW_SCM_R_45B;
SendMSGbuf TBOX_4_7_4A8;
SendMSGbuf CGW_OBC_1_461;
SendMSGbuf AVAS_1_4B1;
SendMSGbuf WCM_1_4BA;
SendMSGbuf DVR_1_3A8;

SendMSGbuf ACCM_3_3D4;

#ifdef CX62C_FUNCTION
TpNamebuf Road_Name;
TpNamebuf Navi_Infomation;
TpNamebuf Song_Name;
TpNamebuf Artist_Name;
TpNamebuf Phone_Number;
TpNamebuf Call_Name;
TpNamebuf Phone_Information;

uint8 road_name_buf[200];
uint8 navi_info_buf[5];
uint8 song_name_buf[200];
uint8 artist_name_buf[200];
uint8 phone_num_buf[50];
uint8 call_name_buf[50];
uint8 phone_info_buf[4];

static uint8 prev_road_name_buf[200] = {
    0,
};
static uint8 prev_navi_info_buf[5] = {
    0,
};
static uint8 prev_song_name_buf[200] = {
    0,
};
static uint8 prev_artist_name_buf[200] = {
    0,
};
static uint8 prev_phone_num_buf[50];
static uint8 prev_call_name_buf[50];
static uint8 prev_phone_info_buf[4];

uint8 displayclear_onoff = 0;

static volatile uint32 tp_road_timer_tick         = 0;
static volatile uint32 tp_navi_timer_tick         = 0;
static volatile uint32 tp_song_timer_tick         = 0;
static volatile uint32 tp_artist_timer_tick       = 0;
static volatile uint32 tp_phone_number_timer_tick = 0;
static volatile uint32 tp_call_timer_tick         = 0;
static volatile uint32 tp_phone_infor_timer_tick  = 0;
#endif

SendMSGbuf diag_usb_device; // JANG_210920_4
uint8      diag_usb_buf[2];
#ifdef CAN_RXTX_TEST_FUNC
SendMSGbuf             uart_rxtx_test[10];
uint8                  uart_rxtx_buf[3];
static volatile uint32 uart_txrx_test_tick = 0;
#endif

extern uint8 u8SOCAlive;

// data buf size
#if 0
uint8 gear_buf[2];
uint8 str_angle_buf[5];
uint8 speed_buf[3];
uint8 seat_buf[3];
uint8 fuellevel_buf[3];
uint8 dte_buf[3];
uint8 dvr_buf[2];
uint8 seatbelt_buf[2];
uint8 bcm1_buf[2];
uint8 bcm2_buf[7];
uint8 bcm3_buf[2];
uint8 bcm4_buf[3];
uint8 abs_esp5_buf[9];
uint8 pm25_buf[7];
uint8 bsd1_buf[2];
uint8 odo_meter_buf[4];
uint8 turnlight_buf[2];
uint8 radar1_buf[5];
uint8 avm0_buf[2];
uint8 avm1_buf[2];
uint8 avm2_buf[3];
uint8 fcm2_buf[3];
uint8 frm3_buf[2];
uint8 radar_buf[10];

uint8 wpc_buf[2];
uint8 breath_buf[4];
uint8 oms_buf[2];
uint8 drivemode_buf[2];
uint8 enginespeed_buf[4];
uint8 ctl_buf[2];
uint8 rollingcounter_buf[2];
uint8 anglespeed_buf[2];
uint8 mfs_heartrate_buf[2];
uint8 afu_buf[4];
uint8 amp_buf[3];
uint8 cem_ipm_buf[6];
uint8 icm2_buf[2];
#endif
#ifdef CX62C_FUNCTION
uint8 icm4_buf[6];
#else
uint8 icm4_buf[2];
#endif
uint8 cem_peps_buf[2];
uint8 plg1_buf[3];
uint8 cgw_eps_g_buf[2];
uint8 cgw_abm_g_buf[3];

uint8 last_mem_buf[50]; // 512->128->50

uint8 wifi_address[6] = {
    0,
};
uint8 bt_address[6] = {
    0,
};
uint8 CGW_VCU_P_5_123_buf[11];   // 10ms
uint8 CGW_BCM_MFS_1_294_buf[11]; //�޸İ��������
uint8 DVR_1_3A8_buf[11];

uint8 CGW_BCM_1_245_buf[11]; // 20ms
uint8 CGW_EGS_1_215_buf[11];
uint8 CGW_EPB_1_231_buf[11];
uint8 CGW_ICM_1_271_buf[11];
uint8 CGW_BMS_3_25A_buf[11];
uint8 CGW_EPS_1_238_buf[11];
uint8 CGW_BCS_C_1_225_buf[11];

uint8 CGW_BCM_PEPS_1_291_buf[11]; // 50ms
uint8 CGW_BSD_1_298_buf[11];
uint8 CGW_VCU_3_4_28A_buf[11];
uint8 CGW_FCM_10_3FC_buf[11];
uint8 CGW_FCM_2_307_buf[11];
uint8 CGW_FCM_6_3DC_buf[11];
uint8 CGW_FCM_7_3DE_buf[11];
uint8 CGW_FCM_FRM_6_387_buf[11];
uint8 CGW_FCM_FRM_8_3ED_buf[11];
uint8 CGW_FCM_FRM_9_3FA_buf[11];

uint8 ACCM_1_3D2_buf[11]; // 100ms
uint8 CGW_BCM_4_324_buf[11];
uint8 CGW_BCM_5_323_buf[11];
uint8 BCM_6_457_buf[11];
uint8 BCM_TPMS_1_326_buf[11];
uint8 BCM_ALM_45D_buf[11];
uint8 ICM_2_3B1_buf[11];

uint8 CGW_BCM_SCM_3_45C_buf[11];
uint8 CGW_BMS_3_46B_buf[11];
uint8 CGW_BCM_SCM_1_45A_buf[11];
uint8 CGW_RCM_1_328_buf[11];
uint8 CGW_BCM_EAC_1_3D8_buf[11];
uint8 CGW_SAS_2_504_buf[11];

uint8 CGW_VCU_3_6_313_buf[11];
uint8 CGW_VCU_3_7_314_buf[11];
uint8 CGW_VCU_B_5_315_buf[11];
uint8 CGW_BMS_9_355_buf[11];
uint8 CGW_FCM_5_4DD_buf[11];
uint8 CGW_FCM_FRM_5_4E3_buf[11];
uint8 CGW_PLG_1_32A_buf[11];

uint8 CGW_BCM_FM_458_buf[11]; // 200ms
uint8 CGW_VCU_3_12_347_buf[11];

uint8 CGW_BCM_TPMS_2_455_buf[11]; // 500ms
uint8 CGW_SCM_R_45B_buf[11];
uint8 TBOX_4_7_4A8_buf[11];
uint8 CGW_OBC_1_461_buf[11];
uint8 ACCM_3_3D4_buf[11];

uint8 AVAS_1_4B1_buf[11]; // 1000ms
uint8 WCM_1_4BA_buf[11];
uint8 softwareConfiguration[];
/* wl20220803 */
extern uint8 u8VariantCoding[];
extern uint8 u8Vin[];

#ifdef SEPERATE_VOLUME_TABLE
uint8 mediachtype = CH_TYPE_MEDIA;
#endif
static void Uart_Tx_TestMode(DEVICE *dev);
static void Uart_Tx_Service(DEVICE *dev);
static void Uart_Rx_Service(DEVICE *dev);
static void Uart1_Rx_Service(DEVICE *dev);

static void uart_recv_data(DEVICE *dev, uint8 *rx_data, uint16 len);

void Uart_Init_Comm(void)
{
    uint8 i = 0;

    init_q(&uart_rx_data_q, &uart_rx_data_buf[0], sizeof(uart_rx_data_buf));
#ifdef NEW_URAT_RX0
    init_q(&uart0_rx_q, &uart0_rx_buf[0], sizeof(uart0_rx_buf));
#endif
#ifdef NEW_URAT_RX1
    init_q(&uart1_rx_q, &uart1_rx_buf[0], sizeof(uart1_rx_buf));
#endif
#ifdef NEW_URAT_TX
    init_q(&uart0_tx_q, &uart0_tx_buf[0], sizeof(uart0_tx_buf));
    init_q(&uart1_tx_q, &uart1_tx_buf[0], sizeof(uart1_tx_buf));
#endif
    // set init value
    memset(&UART_Packet_Info, 0x00, sizeof(MSGPacketInfo));
    memset(&KeyInfo_323, 0x00, sizeof(MSGKeyInfo));

    // set init value
    memset(&acc_packet, 0x00, sizeof(SendMSGbuf));
    memset(&radio_packet, 0x00, sizeof(SendMSGbuf));
    memset(&key_packet, 0x00, sizeof(SendMSGbuf));
    memset(&fm_check_radio_packet, 0x00, sizeof(SendMSGbuf));
    memset(&dtc_list_cmd, 0x00, sizeof(SendMSGbuf));
    memset(&dvr_ctl_cmd, 0x00, sizeof(SendMSGbuf));
    memset(&acc_ign_status, 0x00, sizeof(SendMSGbuf));
    memset(&radio_signal_status, 0x00, sizeof(SendMSGbuf));
    memset(&Last_Memory, 0x00, sizeof(SendMSGbuf));

    memset(&CGW_VCU_P_5_123, 0x00, sizeof(SendMSGbuf)); // cycle : 20ms
    memset(&CGW_BCM_MFS_1_294, 0x00, sizeof(SendMSGbuf));
    memset(&DVR_1_3A8, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_1_245, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_EGS_1_215, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_EPB_1_231, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_ICM_1_271, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BMS_3_25A, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_EPS_1_238, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCS_C_1_225, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_PEPS_1_291, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BSD_1_298, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_VCU_3_4_28A, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_10_3FC, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_2_307, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_6_3DC, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_7_3DE, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_FRM_6_387, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_FRM_8_3ED, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_FRM_9_3FA, 0x00, sizeof(SendMSGbuf));
    memset(&ACCM_1_3D2, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_4_324, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_5_323, 0x00, sizeof(SendMSGbuf));
    memset(&BCM_6_457, 0x00, sizeof(SendMSGbuf));
    memset(&BCM_TPMS_1_326, 0x00, sizeof(SendMSGbuf));
    memset(&BCM_ALM_45D, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_SCM_3_45C, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BMS_3_46B, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_SCM_1_45A, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_RCM_1_328, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_EAC_1_3D8, 0x00, sizeof(SendMSGbuf));
    memset(&ICM_2_3B1, 0x00, sizeof(ICM_2_3B1));

    memset(&CGW_SAS_2_504, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_VCU_3_6_313, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_VCU_3_7_314, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_VCU_B_5_315, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BMS_9_355, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_5_4DD, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_FCM_FRM_5_4E3, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_PLG_1_32A, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_FM_458, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_VCU_3_12_347, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_BCM_TPMS_2_455, 0x00, sizeof(SendMSGbuf));
    memset(&ACCM_3_3D4, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_SCM_R_45B, 0x00, sizeof(SendMSGbuf));
    memset(&TBOX_4_7_4A8, 0x00, sizeof(SendMSGbuf));
    memset(&CGW_OBC_1_461, 0x00, sizeof(SendMSGbuf));
    memset(&AVAS_1_4B1, 0x00, sizeof(SendMSGbuf));
    memset(&WCM_1_4BA, 0x00, sizeof(SendMSGbuf));

#ifdef CX62C_FUNCTION
    memset(&Road_Name, 0x00, sizeof(TpNamebuf));
    memset(&Navi_Infomation, 0x00, sizeof(TpNamebuf));
    memset(&Song_Name, 0x00, sizeof(TpNamebuf));
    memset(&Artist_Name, 0x00, sizeof(TpNamebuf));
    memset(&Phone_Number, 0x00, sizeof(TpNamebuf));
    memset(&Call_Name, 0x00, sizeof(TpNamebuf));
    memset(&Phone_Information, 0x00, sizeof(TpNamebuf));

    Road_Name.buf         = road_name_buf;
    Navi_Infomation.buf   = navi_info_buf;
    Song_Name.buf         = song_name_buf;
    Artist_Name.buf       = artist_name_buf;
    Phone_Number.buf      = phone_num_buf;
    Call_Name.buf         = call_name_buf;
    Phone_Information.buf = phone_info_buf;

    memset(&prev_road_name_buf, 0x00, sizeof(prev_road_name_buf));
    memset(&prev_navi_info_buf, 0x00, sizeof(prev_navi_info_buf));
    memset(&prev_song_name_buf, 0x00, sizeof(prev_song_name_buf));
    memset(&prev_artist_name_buf, 0x00, sizeof(prev_artist_name_buf));
    memset(&prev_phone_num_buf, 0x00, sizeof(prev_phone_num_buf));
    memset(&prev_call_name_buf, 0x00, sizeof(prev_call_name_buf));
    memset(&prev_phone_info_buf, 0x00, sizeof(prev_phone_info_buf));
#endif

    memset(&diag_usb_device, 0x00, sizeof(SendMSGbuf)); // JANG_210920_4
#ifdef CAN_RXTX_TEST_FUNC
    for (i = 0; i < 10; i++)
    {
        memset(&uart_rxtx_test[i], 0x00, sizeof(SendMSGbuf));
        uart_rxtx_test[i].buf     = uart_rxtx_buf;
        uart_rxtx_test[i].len     = sizeof(uart_rxtx_buf);
        uart_rxtx_test[i].message = MSG_RXTX_TEST_EXCUTE_CMD;
    }
#endif
    //<<JANG_210920_4
    diag_usb_device.buf     = diag_usb_buf;
    diag_usb_device.len     = sizeof(diag_usb_buf);
    diag_usb_device.message = MSG_DIAG_USB_CMD;
    //>>

    acc_packet.buf     = acc_buf;
    acc_packet.len     = sizeof(acc_buf);
    acc_packet.message = MSG_SYS_MODE_REQ;

    radio_packet.buf     = radio_buf;
    radio_packet.message = MSG_RDS;

    key_packet.buf     = key_buf;
    key_packet.len     = sizeof(key_buf);
    key_packet.message = MSG_HARDKEY;

    fm_check_radio_packet.buf = fm_check_radio_buf;

    dtc_list_cmd.buf     = dtc_request;
    dtc_list_cmd.len     = 1;
    dtc_list_cmd.message = MSG_DTC_CMD;

    dvr_ctl_cmd.buf     = dvr_ctl_buf;
    dvr_ctl_cmd.len     = sizeof(dvr_ctl_buf);
    dvr_ctl_cmd.message = MSG_DVR_CONT_CMD;

    acc_ign_status.buf     = acc_ign_buf;
    acc_ign_status.len     = sizeof(acc_ign_buf);
    acc_ign_status.message = MSG_ACC_IGN_STATUS;

    radio_signal_status.buf     = radio_signal_buf;
    radio_signal_status.len     = sizeof(radio_signal_buf);
    radio_signal_status.message = MSG_RADIO_STATE;
#if 0
	CAR_SIG_GEAR.buf = gear_buf;
	CAR_SIG_GEAR.len= sizeof(gear_buf);
	CAR_SIG_GEAR.message =MSG_CARSIGNAL;
	CAR_SIG_GEAR.buf[0] = CAR_SIG_TX_GEAR_CMD;
	
	CAR_SIG_STR_ANGLE.buf = str_angle_buf;
	CAR_SIG_STR_ANGLE.len= sizeof(str_angle_buf);
	CAR_SIG_STR_ANGLE.message =MSG_CARSIGNAL;
	CAR_SIG_STR_ANGLE.buf[0] = CAR_SIG_TX_CGW_SAM_G_CMD;
	
	CAR_SIG_SPEED.buf = speed_buf;
	CAR_SIG_SPEED.len = sizeof(speed_buf);
	CAR_SIG_SPEED.message =MSG_CARSIGNAL;
	CAR_SIG_SPEED.buf[0] = CAR_SIG_TX_SPEED_CMD;
	
	CAR_SIG_SEATSTATUS.buf = seat_buf;
	CAR_SIG_SEATSTATUS.len= sizeof(seat_buf);
	CAR_SIG_SEATSTATUS.message =MSG_CARSIGNAL;
	CAR_SIG_SEATSTATUS.buf[0] = CAR_SIG_TX_SEAT_CMD;

	CAR_SIG_FUEL.buf = fuellevel_buf;
	CAR_SIG_FUEL.len= sizeof(fuellevel_buf);
	CAR_SIG_FUEL.message =MSG_CARSIGNAL;
	CAR_SIG_FUEL.buf[0] = CAR_SIG_TX_FUELLEVEL_CMD;

	CAR_SIG_DTE.buf = dte_buf;
	CAR_SIG_DTE.len= sizeof(dte_buf);
	CAR_SIG_DTE.message =MSG_CARSIGNAL;
	CAR_SIG_DTE.buf[0] = CAR_SIG_TX_DTE_CMD;
	
	CAR_SIG_DVR1.buf = dvr_buf;
	CAR_SIG_DVR1.len= sizeof(dvr_buf);
	CAR_SIG_DVR1.message =MSG_CARSIGNAL;
	CAR_SIG_DVR1.buf[0] = CAR_SIG_TX_DVR1_CMD;
	
	CAR_SIG_SEATBELT.buf = seatbelt_buf;
	CAR_SIG_SEATBELT.len= sizeof(seatbelt_buf);
	CAR_SIG_SEATBELT.message =MSG_CARSIGNAL;
	CAR_SIG_SEATBELT.buf[0] = CAR_SIG_TX_SEATBELT_CMD;
	
	CAR_SIG_CEM_BCM1.buf = bcm1_buf;
	CAR_SIG_CEM_BCM1.len= sizeof(bcm1_buf);
	CAR_SIG_CEM_BCM1.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_BCM1.buf[0] = CAR_SIG_TX_CEM_BCM1_CMD;
	
	CAR_SIG_CEM_BCM2.buf = bcm2_buf;
	CAR_SIG_CEM_BCM2.len= sizeof(bcm2_buf);
	CAR_SIG_CEM_BCM2.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_BCM2.buf[0] = CAR_SIG_TX_CEM_BCM2_CMD;

	CAR_SIG_CEM_BCM3.buf = bcm3_buf;
	CAR_SIG_CEM_BCM3.len= sizeof(bcm3_buf);
	CAR_SIG_CEM_BCM3.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_BCM3.buf[0] = CAR_SIG_TX_CEM_BCM3_CMD;

	CAR_SIG_CEM_BCM4.buf = bcm4_buf;
	CAR_SIG_CEM_BCM4.len= sizeof(bcm4_buf);
	CAR_SIG_CEM_BCM4.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_BCM4.buf[0] = CAR_SIG_TX_CEM_BCM4_CMD;

	CAR_SIG_ABS_ESP5.buf = abs_esp5_buf;
	CAR_SIG_ABS_ESP5.len= sizeof(abs_esp5_buf);
	CAR_SIG_ABS_ESP5.message =MSG_CARSIGNAL;
	CAR_SIG_ABS_ESP5.buf[0] = CAR_SIG_TX_ABS_ESP5_CMD;

	CAR_SIG_PM25.buf = pm25_buf;
	CAR_SIG_PM25.len= sizeof(pm25_buf);
	CAR_SIG_PM25.message =MSG_CARSIGNAL;
	CAR_SIG_PM25.buf[0] = CAR_SIG_TX_PM25_1_CMD;

	CAR_SIG_BSD1.buf = bsd1_buf;
	CAR_SIG_BSD1.len= sizeof(bsd1_buf);
	CAR_SIG_BSD1.message =MSG_CARSIGNAL;
	CAR_SIG_BSD1.buf[0] = CAR_SIG_TX_BSD1_CMD;	

	CAR_SIG_CEM_RADAR1.buf = radar_buf;
	CAR_SIG_CEM_RADAR1.len= sizeof(radar_buf);
	CAR_SIG_CEM_RADAR1.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_RADAR1.buf[0] = CAR_SIG_TX_CEM_RADAR1_CMD;

	CAR_SIG_TURNLIGHT.buf = turnlight_buf;
	CAR_SIG_TURNLIGHT.len= sizeof(turnlight_buf);
	CAR_SIG_TURNLIGHT.message =MSG_CARSIGNAL;
	CAR_SIG_TURNLIGHT.buf[0] = CAR_SIG_TX_TURNSIGNAL_CMD;

	CAR_SIG_ODOMETER.buf = odo_meter_buf;
	CAR_SIG_ODOMETER.len= sizeof(odo_meter_buf);
	CAR_SIG_ODOMETER.message =MSG_CARSIGNAL;
	CAR_SIG_ODOMETER.buf[0] = CAR_SIG_TX_ODO_CMD;

	CAR_SIG_AVM_APA4.buf = radar1_buf;
	CAR_SIG_AVM_APA4.len= sizeof(radar1_buf);
	CAR_SIG_AVM_APA4.message =MSG_CARSIGNAL;
	CAR_SIG_AVM_APA4.buf[0] = CAR_SIG_TX_AVM_APA4_CMD;

	CAR_SIG_AVM_APA2.buf = avm1_buf;
	CAR_SIG_AVM_APA2.len= sizeof(avm1_buf);
	CAR_SIG_AVM_APA2.message =MSG_CARSIGNAL;
	CAR_SIG_AVM_APA2.buf[0] = CAR_SIG_TX_AVM_APA2_CMD;

	CAR_SIG_AVM_APA3.buf = avm0_buf;
	CAR_SIG_AVM_APA3.len= sizeof(avm0_buf);
	CAR_SIG_AVM_APA3.message =MSG_CARSIGNAL;
	CAR_SIG_AVM_APA3.buf[0] = CAR_SIG_TX_AVM_APA3_CMD;

	CAR_SIG_AVM2.buf = avm2_buf;
	CAR_SIG_AVM2.len= sizeof(avm2_buf);
	CAR_SIG_AVM2.message =MSG_CARSIGNAL;
	CAR_SIG_AVM2.buf[0] = CAR_SIG_TX_AVM2_CMD;

	CAR_SIG_FCM2.buf = fcm2_buf;
	CAR_SIG_FCM2.len= sizeof(fcm2_buf);
	CAR_SIG_FCM2.message =MSG_CARSIGNAL;
	CAR_SIG_FCM2.buf[0] = CAR_SIG_TX_FCM2_CMD;

	CAR_SIG_FRM3.buf = frm3_buf;
	CAR_SIG_FRM3.len= sizeof(frm3_buf);
	CAR_SIG_FRM3.message =MSG_CARSIGNAL;
	CAR_SIG_FRM3.buf[0] = CAR_SIG_TX_FRM3_CMD;

	CAR_SIG_WPC.buf = wpc_buf;
	CAR_SIG_WPC.len= sizeof(wpc_buf);
	CAR_SIG_WPC.message =MSG_CARSIGNAL;
	CAR_SIG_WPC.buf[0] = CAR_SIG_TX_WPC_CMD;

	CAR_SIG_CEM_ALM1.buf = breath_buf;
	CAR_SIG_CEM_ALM1.len= sizeof(breath_buf);
	CAR_SIG_CEM_ALM1.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_ALM1.buf[0] = CAR_SIG_TX_CEM_ALM1_CMD;

	CAR_SIG_TBOX2.buf = oms_buf;
	CAR_SIG_TBOX2.len= sizeof(oms_buf);
	CAR_SIG_TBOX2.message =MSG_CARSIGNAL;
	CAR_SIG_TBOX2.buf[0] = CAR_SIG_TX_TBOX2_CMD;

	CAR_SIG_ENGINESPEED.buf = enginespeed_buf;
	CAR_SIG_ENGINESPEED.len= sizeof(enginespeed_buf);
	CAR_SIG_ENGINESPEED.message =MSG_CARSIGNAL;
	CAR_SIG_ENGINESPEED.buf[0] = CAR_SIG_TX_ENGINESPEED_CMD;

	CAR_SIG_CLT.buf = ctl_buf;
	CAR_SIG_CLT.len= sizeof(ctl_buf);
	CAR_SIG_CLT.message =MSG_CARSIGNAL;
	CAR_SIG_CLT.buf[0] = CAR_SIG_TX_CLT_CMD;

	CAR_SIG_ROLLCOUNT.buf = rollingcounter_buf;
	CAR_SIG_ROLLCOUNT.len= sizeof(rollingcounter_buf);
	CAR_SIG_ROLLCOUNT.message =MSG_CARSIGNAL;
	CAR_SIG_ROLLCOUNT.buf[0] = CAR_SIG_TX_ROLLINGCOUNTER_CMD;
	
	CAR_SIG_MFS_HEARTRATE.buf = mfs_heartrate_buf;
	CAR_SIG_MFS_HEARTRATE.len= sizeof(mfs_heartrate_buf);
	CAR_SIG_MFS_HEARTRATE.message =MSG_CARSIGNAL;
	CAR_SIG_MFS_HEARTRATE.buf[0] = CAR_SIG_TX_MFS_HEARTRATE_CMD;

	CAR_SIG_AFU.buf = afu_buf;
	CAR_SIG_AFU.len= sizeof(afu_buf);
	CAR_SIG_AFU.message =MSG_CARSIGNAL;
	CAR_SIG_AFU.buf[0] = CAR_SIG_TX_AFU_CMD;

	CAR_SIG_AMP.buf = amp_buf;
	CAR_SIG_AMP.len= sizeof(amp_buf);
	CAR_SIG_AMP.message =MSG_CARSIGNAL;
	CAR_SIG_AMP.buf[0] = CAR_SIG_TX_AMP_CMD;

	CAR_SIG_CEM_IPM.buf = cem_ipm_buf;
	CAR_SIG_CEM_IPM.len= sizeof(cem_ipm_buf);
	CAR_SIG_CEM_IPM.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_IPM.buf[0] = CAR_SIG_TX_CEM_IPM_CMD;

	CAR_SIG_ICM2.buf = icm2_buf;
	CAR_SIG_ICM2.len= sizeof(icm2_buf);
	CAR_SIG_ICM2.message =MSG_CARSIGNAL;
	CAR_SIG_ICM2.buf[0] = CAR_SIG_TX_ICM2_CMD;

	CAR_SIG_ICM4.buf = icm4_buf;
	CAR_SIG_ICM4.len= sizeof(icm4_buf);
	CAR_SIG_ICM4.message =MSG_CARSIGNAL;
	CAR_SIG_ICM4.buf[0] = CAR_SIG_TX_ICM4_CMD;

	CAR_SIG_CEM_PEPS.buf = cem_peps_buf;
	CAR_SIG_CEM_PEPS.len= sizeof(cem_peps_buf);
	CAR_SIG_CEM_PEPS.message =MSG_CARSIGNAL;
	CAR_SIG_CEM_PEPS.buf[0] = CAR_SIG_TX_CEM_PEPS_CMD;

	CAR_SIG_PLG1.buf = plg1_buf;
	CAR_SIG_PLG1.len= sizeof(plg1_buf);
	CAR_SIG_PLG1.message =MSG_CARSIGNAL;
	CAR_SIG_PLG1.buf[0] = CAR_SIG_TX_PLG1_CMD;
		
	CAR_SIG_CGW_EPS_G.buf = cgw_eps_g_buf;
	CAR_SIG_CGW_EPS_G.len= sizeof(cgw_eps_g_buf);
	CAR_SIG_CGW_EPS_G.message =MSG_CARSIGNAL;
	CAR_SIG_CGW_EPS_G.buf[0] = CAR_SIG_TX_CGW_EPS_G_CMD;
	
	CAR_SIG_CGW_ABM_G.buf = cgw_abm_g_buf;
	CAR_SIG_CGW_ABM_G.len= sizeof(cgw_abm_g_buf);
	CAR_SIG_CGW_ABM_G.message =MSG_CARSIGNAL;
	CAR_SIG_CGW_ABM_G.buf[0] = CAR_SIG_TX_CGW_ABM_G_CMD;
#endif
    Last_Memory.buf     = last_mem_buf;
    Last_Memory.len     = sizeof(last_mem_buf);
    Last_Memory.message = MSG_LAST_READ;

    CGW_VCU_P_5_123.buf     = CGW_VCU_P_5_123_buf;
    CGW_VCU_P_5_123.len     = sizeof(CGW_VCU_P_5_123_buf);
    CGW_VCU_P_5_123.state   = MCU_COMMAND;
    CGW_VCU_P_5_123.message = MSG_CARSIGNAL; // 0x84
    CGW_VCU_P_5_123.buf[0]  = 0x01;
    CGW_VCU_P_5_123.buf[1]  = 0x23;

    CGW_BCM_MFS_1_294.buf     = CGW_BCM_MFS_1_294_buf;
    CGW_BCM_MFS_1_294.len     = sizeof(CGW_BCM_MFS_1_294_buf);
    CGW_BCM_MFS_1_294.state   = MCU_COMMAND;
    CGW_BCM_MFS_1_294.message = MSG_HARDKEY;
    CGW_BCM_MFS_1_294.buf[0]  = 0x02;
    CGW_BCM_MFS_1_294.buf[1]  = 0x94;

    // DVR_1_3A8.buf = DVR_1_3A8_buf;
    // DVR_1_3A8.len = sizeof(DVR_1_3A8_buf);
    // DVR_1_3A8.state = MCU_COMMAND;
    // DVR_1_3A8.message = MSG_CARSIGNAL;				// 0x84
    // DVR_1_3A8.buf[0] = 0x03;
    // DVR_1_3A8.buf[1] = 0xA8;

    CGW_BCM_1_245.buf     = CGW_BCM_1_245_buf;
    CGW_BCM_1_245.len     = sizeof(CGW_BCM_1_245_buf);
    CGW_BCM_1_245.state   = MCU_COMMAND;
    CGW_BCM_1_245.message = MSG_CARSIGNAL;
    CGW_BCM_1_245.buf[0]  = 0x02;
    CGW_BCM_1_245.buf[1]  = 0x45;

    CGW_EGS_1_215.buf     = CGW_EGS_1_215_buf;
    CGW_EGS_1_215.len     = sizeof(CGW_EGS_1_215_buf);
    CGW_EGS_1_215.state   = MCU_COMMAND;
    CGW_EGS_1_215.message = MSG_CARSIGNAL;
    CGW_EGS_1_215.buf[0]  = 0x02;
    CGW_EGS_1_215.buf[1]  = 0x15;

    CGW_EPB_1_231.buf     = CGW_EPB_1_231_buf;
    CGW_EPB_1_231.len     = sizeof(CGW_EPB_1_231_buf);
    CGW_EPB_1_231.state   = MCU_COMMAND;
    CGW_EPB_1_231.message = MSG_CARSIGNAL;
    CGW_EPB_1_231.buf[0]  = 0x02;
    CGW_EPB_1_231.buf[1]  = 0x31;

    CGW_ICM_1_271.buf     = CGW_ICM_1_271_buf;
    CGW_ICM_1_271.len     = sizeof(CGW_ICM_1_271_buf);
    CGW_ICM_1_271.state   = MCU_COMMAND;
    CGW_ICM_1_271.message = MSG_CARSIGNAL;
    CGW_ICM_1_271.buf[0]  = 0x02;
    CGW_ICM_1_271.buf[1]  = 0x71;

    CGW_BCS_C_1_225.buf     = CGW_BCS_C_1_225_buf;
    CGW_BCS_C_1_225.len     = sizeof(CGW_BCS_C_1_225_buf);
    CGW_BCS_C_1_225.state   = MCU_COMMAND;
    CGW_BCS_C_1_225.message = MSG_CARSIGNAL;
    CGW_BCS_C_1_225.buf[0]  = 0x02;
    CGW_BCS_C_1_225.buf[1]  = 0x25;

    CGW_BMS_3_25A.buf     = CGW_BMS_3_25A_buf;
    CGW_BMS_3_25A.len     = sizeof(CGW_BMS_3_25A_buf);
    CGW_BMS_3_25A.state   = MCU_COMMAND;
    CGW_BMS_3_25A.message = MSG_CARSIGNAL;
    CGW_BMS_3_25A.buf[0]  = 0x02;
    CGW_BMS_3_25A.buf[1]  = 0x5A;

    CGW_EPS_1_238.buf     = CGW_EPS_1_238_buf;
    CGW_EPS_1_238.len     = sizeof(CGW_EPS_1_238_buf);
    CGW_EPS_1_238.state   = MCU_COMMAND;
    CGW_EPS_1_238.message = MSG_CARSIGNAL;
    CGW_EPS_1_238.buf[0]  = 0x02;
    CGW_EPS_1_238.buf[1]  = 0x38;

    CGW_BCM_PEPS_1_291.buf     = CGW_BCM_PEPS_1_291_buf;
    CGW_BCM_PEPS_1_291.len     = sizeof(CGW_BCM_PEPS_1_291_buf);
    CGW_BCM_PEPS_1_291.state   = MCU_COMMAND;
    CGW_BCM_PEPS_1_291.message = MSG_CARSIGNAL;
    CGW_BCM_PEPS_1_291.buf[0]  = 0x02;
    CGW_BCM_PEPS_1_291.buf[1]  = 0x91;

    CGW_BSD_1_298.buf     = CGW_BSD_1_298_buf;
    CGW_BSD_1_298.len     = sizeof(CGW_BSD_1_298_buf);
    CGW_BSD_1_298.state   = MCU_COMMAND;
    CGW_BSD_1_298.message = MSG_CARSIGNAL;
    CGW_BSD_1_298.buf[0]  = 0x02;
    CGW_BSD_1_298.buf[1]  = 0x98;

    CGW_VCU_3_4_28A.buf     = CGW_VCU_3_4_28A_buf;
    CGW_VCU_3_4_28A.len     = sizeof(CGW_VCU_3_4_28A_buf);
    CGW_VCU_3_4_28A.state   = MCU_COMMAND;
    CGW_VCU_3_4_28A.message = MSG_CARSIGNAL;
    CGW_VCU_3_4_28A.buf[0]  = 0x02;
    CGW_VCU_3_4_28A.buf[1]  = 0x8A;

    CGW_FCM_10_3FC.buf     = CGW_FCM_10_3FC_buf;
    CGW_FCM_10_3FC.len     = sizeof(CGW_FCM_10_3FC_buf);
    CGW_FCM_10_3FC.state   = MCU_COMMAND;
    CGW_FCM_10_3FC.message = MSG_CARSIGNAL;
    CGW_FCM_10_3FC.buf[0]  = 0x03;
    CGW_FCM_10_3FC.buf[1]  = 0xFC;

    CGW_FCM_2_307.buf     = CGW_FCM_2_307_buf;
    CGW_FCM_2_307.len     = sizeof(CGW_FCM_2_307_buf);
    CGW_FCM_2_307.state   = MCU_COMMAND;
    CGW_FCM_2_307.message = MSG_CARSIGNAL;
    CGW_FCM_2_307.buf[0]  = 0x03;
    CGW_FCM_2_307.buf[1]  = 0x07;

    CGW_FCM_6_3DC.buf     = CGW_FCM_6_3DC_buf;
    CGW_FCM_6_3DC.len     = sizeof(CGW_FCM_6_3DC_buf);
    CGW_FCM_6_3DC.state   = MCU_COMMAND;
    CGW_FCM_6_3DC.message = MSG_CARSIGNAL;
    CGW_FCM_6_3DC.buf[0]  = 0x03;
    CGW_FCM_6_3DC.buf[1]  = 0xDC;

    CGW_FCM_7_3DE.buf     = CGW_FCM_7_3DE_buf;
    CGW_FCM_7_3DE.len     = sizeof(CGW_FCM_7_3DE_buf);
    CGW_FCM_7_3DE.state   = MCU_COMMAND;
    CGW_FCM_7_3DE.message = MSG_CARSIGNAL;
    CGW_FCM_7_3DE.buf[0]  = 0x03;
    CGW_FCM_7_3DE.buf[1]  = 0xDE;

    CGW_FCM_FRM_6_387.buf     = CGW_FCM_FRM_6_387_buf;
    CGW_FCM_FRM_6_387.len     = sizeof(CGW_FCM_FRM_6_387_buf);
    CGW_FCM_FRM_6_387.state   = MCU_COMMAND;
    CGW_FCM_FRM_6_387.message = MSG_CARSIGNAL;
    CGW_FCM_FRM_6_387.buf[0]  = 0x03;
    CGW_FCM_FRM_6_387.buf[1]  = 0x87;

    CGW_FCM_FRM_8_3ED.buf     = CGW_FCM_FRM_8_3ED_buf;
    CGW_FCM_FRM_8_3ED.len     = sizeof(CGW_FCM_FRM_8_3ED_buf);
    CGW_FCM_FRM_8_3ED.state   = MCU_COMMAND;
    CGW_FCM_FRM_8_3ED.message = MSG_CARSIGNAL;
    CGW_FCM_FRM_8_3ED.buf[0]  = 0x03;
    CGW_FCM_FRM_8_3ED.buf[1]  = 0xED;

    CGW_FCM_FRM_9_3FA.buf     = CGW_FCM_FRM_9_3FA_buf;
    CGW_FCM_FRM_9_3FA.len     = sizeof(CGW_FCM_FRM_9_3FA_buf);
    CGW_FCM_FRM_9_3FA.state   = MCU_COMMAND;
    CGW_FCM_FRM_9_3FA.message = MSG_CARSIGNAL;
    CGW_FCM_FRM_9_3FA.buf[0]  = 0x03;
    CGW_FCM_FRM_9_3FA.buf[1]  = 0xFA;

    ICM_2_3B1.buf             =ICM_2_3B1_buf;
    ICM_2_3B1.len             = sizeof(ICM_2_3B1_buf);
    ICM_2_3B1.state           = MCU_COMMAND;
    ICM_2_3B1.message         = MSG_CARSIGNAL;
    ICM_2_3B1.buf[0]          = 0x03;
    ICM_2_3B1.buf[1]          = 0xB1;

    ACCM_1_3D2.buf     = ACCM_1_3D2_buf;
    ACCM_1_3D2.len     = sizeof(ACCM_1_3D2_buf);
    ACCM_1_3D2.state   = MCU_COMMAND;
    ACCM_1_3D2.message = MSG_CARSIGNAL;
    ACCM_1_3D2.buf[0]  = 0x03;
    ACCM_1_3D2.buf[1]  = 0xD2;

    CGW_BCM_4_324.buf     = CGW_BCM_4_324_buf;
    CGW_BCM_4_324.len     = sizeof(CGW_BCM_4_324_buf);
    CGW_BCM_4_324.state   = MCU_COMMAND;
    CGW_BCM_4_324.message = MSG_CARSIGNAL;
    CGW_BCM_4_324.buf[0]  = 0x03;
    CGW_BCM_4_324.buf[1]  = 0x24;

    CGW_BCM_5_323.buf     = CGW_BCM_5_323_buf;
    CGW_BCM_5_323.len     = sizeof(CGW_BCM_5_323_buf);
    CGW_BCM_5_323.state   = MCU_COMMAND;
    CGW_BCM_5_323.message = MSG_CARSIGNAL;
    CGW_BCM_5_323.buf[0]  = 0x03;
    CGW_BCM_5_323.buf[1]  = 0x23;

    BCM_6_457.buf     = BCM_6_457_buf;
    BCM_6_457.len     = sizeof(BCM_6_457_buf);
    BCM_6_457.state   = MCU_COMMAND;
    BCM_6_457.message = MSG_CARSIGNAL;
    BCM_6_457.buf[0]  = 0x04;
    BCM_6_457.buf[1]  = 0x57;

    BCM_TPMS_1_326.buf     = BCM_TPMS_1_326_buf;
    BCM_TPMS_1_326.len     = sizeof(BCM_TPMS_1_326_buf);
    BCM_TPMS_1_326.state   = MCU_COMMAND;
    BCM_TPMS_1_326.message = MSG_CARSIGNAL;
    BCM_TPMS_1_326.buf[0]  = 0x03;
    BCM_TPMS_1_326.buf[1]  = 0x26;

    BCM_ALM_45D.buf     = BCM_ALM_45D_buf;
    BCM_ALM_45D.len     = sizeof(BCM_ALM_45D_buf);
    BCM_ALM_45D.state   = MCU_COMMAND;
    BCM_ALM_45D.message = MSG_CARSIGNAL;
    BCM_ALM_45D.buf[0]  = 0x04;
    BCM_ALM_45D.buf[1]  = 0x5D;

    CGW_BCM_SCM_3_45C.buf     = CGW_BCM_SCM_3_45C_buf;
    CGW_BCM_SCM_3_45C.len     = sizeof(CGW_BCM_SCM_3_45C_buf);
    CGW_BCM_SCM_3_45C.state   = MCU_COMMAND;
    CGW_BCM_SCM_3_45C.message = MSG_CARSIGNAL;
    CGW_BCM_SCM_3_45C.buf[0]  = 0x04;
    CGW_BCM_SCM_3_45C.buf[1]  = 0x5C;

    CGW_BMS_3_46B.buf     = CGW_BMS_3_46B_buf;
    CGW_BMS_3_46B.len     = sizeof(CGW_BMS_3_46B_buf);
    CGW_BMS_3_46B.state   = MCU_COMMAND;
    CGW_BMS_3_46B.message = MSG_CARSIGNAL;
    CGW_BMS_3_46B.buf[0]  = 0x04;
    CGW_BMS_3_46B.buf[1]  = 0x6B;

    CGW_BCM_SCM_1_45A.buf     = CGW_BCM_SCM_1_45A_buf;
    CGW_BCM_SCM_1_45A.len     = sizeof(CGW_BCM_SCM_1_45A_buf);
    CGW_BCM_SCM_1_45A.state   = MCU_COMMAND;
    CGW_BCM_SCM_1_45A.message = MSG_CARSIGNAL;
    CGW_BCM_SCM_1_45A.buf[0]  = 0x04;
    CGW_BCM_SCM_1_45A.buf[1]  = 0x5A;

    CGW_RCM_1_328.buf     = CGW_RCM_1_328_buf;
    CGW_RCM_1_328.len     = sizeof(CGW_RCM_1_328_buf);
    CGW_RCM_1_328.state   = MCU_COMMAND;
    CGW_RCM_1_328.message = MSG_CARSIGNAL;
    CGW_RCM_1_328.buf[0]  = 0x03;
    CGW_RCM_1_328.buf[1]  = 0x28;

    CGW_BCM_EAC_1_3D8.buf     = CGW_BCM_EAC_1_3D8_buf;
    CGW_BCM_EAC_1_3D8.len     = sizeof(CGW_BCM_EAC_1_3D8_buf);
    CGW_BCM_EAC_1_3D8.state   = MCU_COMMAND;
    CGW_BCM_EAC_1_3D8.message = MSG_CARSIGNAL;
    CGW_BCM_EAC_1_3D8.buf[0]  = 0x03;
    CGW_BCM_EAC_1_3D8.buf[1]  = 0xD8;

    CGW_SAS_2_504.buf     = CGW_SAS_2_504_buf;
    CGW_SAS_2_504.len     = sizeof(CGW_SAS_2_504_buf);
    CGW_SAS_2_504.state   = MCU_COMMAND;
    CGW_SAS_2_504.message = MSG_CARSIGNAL;
    CGW_SAS_2_504.buf[0]  = 0x05;
    CGW_SAS_2_504.buf[1]  = 0x04;

    CGW_VCU_3_6_313.buf     = CGW_VCU_3_6_313_buf;
    CGW_VCU_3_6_313.len     = sizeof(CGW_VCU_3_6_313_buf);
    CGW_VCU_3_6_313.state   = MCU_COMMAND;
    CGW_VCU_3_6_313.message = MSG_CARSIGNAL;
    CGW_VCU_3_6_313.buf[0]  = 0x03;
    CGW_VCU_3_6_313.buf[1]  = 0x13;

    CGW_VCU_3_7_314.buf     = CGW_VCU_3_7_314_buf;
    CGW_VCU_3_7_314.len     = sizeof(CGW_VCU_3_7_314_buf);
    CGW_VCU_3_7_314.state   = MCU_COMMAND;
    CGW_VCU_3_7_314.message = MSG_CARSIGNAL;
    CGW_VCU_3_7_314.buf[0]  = 0x03;
    CGW_VCU_3_7_314.buf[1]  = 0x14;

    CGW_VCU_B_5_315.buf     = CGW_VCU_B_5_315_buf;
    CGW_VCU_B_5_315.len     = sizeof(CGW_VCU_B_5_315_buf);
    CGW_VCU_B_5_315.state   = MCU_COMMAND;
    CGW_VCU_B_5_315.message = MSG_CARSIGNAL;
    CGW_VCU_B_5_315.buf[0]  = 0x03;
    CGW_VCU_B_5_315.buf[1]  = 0x15;

    CGW_BMS_9_355.buf     = CGW_BMS_9_355_buf;
    CGW_BMS_9_355.len     = sizeof(CGW_BMS_9_355_buf);
    CGW_BMS_9_355.state   = MCU_COMMAND;
    CGW_BMS_9_355.message = MSG_CARSIGNAL;
    CGW_BMS_9_355.buf[0]  = 0x03;
    CGW_BMS_9_355.buf[1]  = 0x55;

    CGW_FCM_5_4DD.buf     = CGW_FCM_5_4DD_buf;
    CGW_FCM_5_4DD.len     = sizeof(CGW_FCM_5_4DD_buf);
    CGW_FCM_5_4DD.state   = MCU_COMMAND;
    CGW_FCM_5_4DD.message = MSG_CARSIGNAL;
    CGW_FCM_5_4DD.buf[0]  = 0x04;
    CGW_FCM_5_4DD.buf[1]  = 0xDD;

    CGW_FCM_FRM_5_4E3.buf     = CGW_FCM_FRM_5_4E3_buf;
    CGW_FCM_FRM_5_4E3.len     = sizeof(CGW_FCM_FRM_5_4E3_buf);
    CGW_FCM_FRM_5_4E3.state   = MCU_COMMAND;
    CGW_FCM_FRM_5_4E3.message = MSG_CARSIGNAL;
    CGW_FCM_FRM_5_4E3.buf[0]  = 0x04;
    CGW_FCM_FRM_5_4E3.buf[1]  = 0xE3;

    CGW_PLG_1_32A.buf     = CGW_PLG_1_32A_buf;
    CGW_PLG_1_32A.len     = sizeof(CGW_PLG_1_32A_buf);
    CGW_PLG_1_32A.state   = MCU_COMMAND;
    CGW_PLG_1_32A.message = MSG_CARSIGNAL;
    CGW_PLG_1_32A.buf[0]  = 0x03;
    CGW_PLG_1_32A.buf[1]  = 0x2A;

    CGW_BCM_FM_458.buf     = CGW_BCM_FM_458_buf;
    CGW_BCM_FM_458.len     = sizeof(CGW_BCM_FM_458_buf);
    CGW_BCM_FM_458.state   = MCU_COMMAND;
    CGW_BCM_FM_458.message = MSG_CARSIGNAL;
    CGW_BCM_FM_458.buf[0]  = 0x02;
    CGW_BCM_FM_458.buf[1]  = 0x34;

    CGW_VCU_3_12_347.buf     = CGW_VCU_3_12_347_buf;
    CGW_VCU_3_12_347.len     = sizeof(CGW_VCU_3_12_347_buf);
    CGW_VCU_3_12_347.state   = MCU_COMMAND;
    CGW_VCU_3_12_347.message = MSG_CARSIGNAL;
    CGW_VCU_3_12_347.buf[0]  = 0x03;
    CGW_VCU_3_12_347.buf[1]  = 0x47;

    CGW_BCM_TPMS_2_455.buf     = CGW_BCM_TPMS_2_455_buf;
    CGW_BCM_TPMS_2_455.len     = sizeof(CGW_BCM_TPMS_2_455_buf);
    CGW_BCM_TPMS_2_455.state   = MCU_COMMAND;
    CGW_BCM_TPMS_2_455.message = MSG_CARSIGNAL;
    CGW_BCM_TPMS_2_455.buf[0]  = 0x04;
    CGW_BCM_TPMS_2_455.buf[1]  = 0x55;

    ACCM_3_3D4.buf     = ACCM_3_3D4_buf;
    ACCM_3_3D4.len     = sizeof(ACCM_3_3D4_buf);
    ACCM_3_3D4.state   = MCU_COMMAND;
    ACCM_3_3D4.message = MSG_CARSIGNAL;
    ACCM_3_3D4.buf[0]  = 0x03;
    ACCM_3_3D4.buf[1]  = 0xD4;

    CGW_SCM_R_45B.buf     = CGW_SCM_R_45B_buf;
    CGW_SCM_R_45B.len     = sizeof(CGW_SCM_R_45B_buf);
    CGW_SCM_R_45B.state   = MCU_COMMAND;
    CGW_SCM_R_45B.message = MSG_CARSIGNAL;
    CGW_SCM_R_45B.buf[0]  = 0x04;
    CGW_SCM_R_45B.buf[1]  = 0x5B;

    TBOX_4_7_4A8.buf     = TBOX_4_7_4A8_buf;
    TBOX_4_7_4A8.len     = sizeof(TBOX_4_7_4A8_buf);
    TBOX_4_7_4A8.state   = MCU_COMMAND;
    TBOX_4_7_4A8.message = MSG_CARSIGNAL;
    TBOX_4_7_4A8.buf[0]  = 0x04;
    TBOX_4_7_4A8.buf[1]  = 0xA8;

    CGW_OBC_1_461.buf     = CGW_OBC_1_461_buf;
    CGW_OBC_1_461.len     = sizeof(CGW_OBC_1_461_buf);
    CGW_OBC_1_461.state   = MCU_COMMAND;
    CGW_OBC_1_461.message = MSG_CARSIGNAL;
    CGW_OBC_1_461.buf[0]  = 0x04;
    CGW_OBC_1_461.buf[1]  = 0x61;

    AVAS_1_4B1.buf     = AVAS_1_4B1_buf;
    AVAS_1_4B1.len     = sizeof(AVAS_1_4B1_buf);
    AVAS_1_4B1.state   = MCU_COMMAND;
    AVAS_1_4B1.message = MSG_CARSIGNAL;
    AVAS_1_4B1.buf[0]  = 0x04;
    AVAS_1_4B1.buf[1]  = 0xB1;

    WCM_1_4BA.buf     = WCM_1_4BA_buf;
    WCM_1_4BA.len     = sizeof(WCM_1_4BA_buf);
    WCM_1_4BA.state   = MCU_COMMAND;
    WCM_1_4BA.message = MSG_CARSIGNAL;
    WCM_1_4BA.buf[0]  = 0x04;
    WCM_1_4BA.buf[1]  = 0xBA;

    Uart_Rx_Set_Default_Data();
}

void Uart_Rx_Set_Default_Data(void)
{
#if 0
	CAR_SIG_SEATSTATUS.buf[1] = 0x05;	//PersonalSeatEn =01	, PersonalBeamEn=01
	
	CAR_SIG_CEM_BCM2.buf[1] = 0x40;//FollowMeHomeTimeSts=01, LaserhighbeamassistStatus =01,AutoUnlockSts =01,MirrorFlipSts=01, HazardLampForUrgency =01, AutolockSts =01
	CAR_SIG_CEM_BCM2.buf[4] = 0x14;
	CAR_SIG_CEM_BCM2.buf[5] = 0x16;

	CAR_SIG_BSD1.buf[1] = 0x23;//BSDState= 01, RCWSts = 01,DOWSts =01

	CAR_SIG_FCM2.buf[1] = 0x51; //HMAoNoFFSts =01, LDW_LKA_LaneAssitfee=01, SLAOnoffsts =01,TJA_ICA_ON_OFF_STS=01,
	CAR_SIG_FCM2.buf[2] = 0x04;

	CAR_SIG_FRM3.buf[1] = 0x17;	//FCW_ON_OFF_sts =1, FCW_OPTION_sts= 01, AEB_ON_OFF_sts=01, DistanceWarning_on_off =01
	CAR_SIG_CEM_ALM1.buf[1] = 0x0d;//STAT_StaticEffect=03, STAT_Apiluminance=04 Music=01, 
	CAR_SIG_CEM_ALM1.buf[2] = 0x04;

	CAR_SIG_AFU.buf[1] = 0x80;//AirFragCh1Detn=01
	CAR_SIG_AFU.buf[2] = 0x02;//AirFragLvlRspn=02

	CAR_SIG_CEM_PEPS.buf[1] =0x01; //Pollingsts =01

	CAR_SIG_PLG1.buf[2] =0x5F;	//PLGSASSts =0x5F
	
	CAR_SIG_CGW_EPS_G.buf[1] = 0x2;//EpsmodeSts = Comfort
	//<<JANG_211114
	CAR_SIG_CEM_IPM.buf[4] = 0x03; //IPM blower delaySts =1 , BTReduceWindSpeedSts =1
	CAR_SIG_CEM_IPM.buf[5] = 0x40;//IPM First BlowingSts = 1
	//>>
#endif
}
void Uart_Rx_Check_Set_Init_Data(void)
{
    uint8  tmp8;
    uint16 tmp16;
    uint32 tmp32, tmp32_1;
#if 0
	if(CanIfGetMsg_gCanIf_CGW_EMS_G_flag()==ON)
	{
		//CGW_EMS_G_280_IDX
		CAR_SIG_ENGINESPEED.flag = ON;
		CAR_SIG_ENGINESPEED.trycnt = 0;
		CAR_SIG_ENGINESPEED.buf[1]= (CanIfGet_gCanIf_CGW_EMS_G_TargetGearPosition()<<1)|CanIfGet_gCanIf_CGW_EMS_G_EngineSts();
		CAR_SIG_ENGINESPEED.buf[2] = CanIfGet_gCanIf_CGW_EMS_G_EngineSpeed_0();
		CAR_SIG_ENGINESPEED.buf[3] = CanIfGet_gCanIf_CGW_EMS_G_EngineSpeed_1();

		CAR_SIG_CLT.flag = ON;
		CAR_SIG_CLT.trycnt = 0;
		CAR_SIG_CLT.buf[1] = CanIfGet_gCanIf_CGW_EMS_G_EngineCoolantTemperture();

		CAR_SIG_ROLLCOUNT.flag = ON;
		CAR_SIG_ROLLCOUNT.trycnt = 0;
		CAR_SIG_ROLLCOUNT.buf[1] = CanIfGet_gCanIf_CGW_EMS_G_FuelRollingCounter();
	}

	if(CanIfGetMsg_gCanIf_ABS_ESP_1_flag()==ON)
	{
		//ABS_ESP_1_2E9_IDX
		CAR_SIG_SPEED.flag = ON;
		CAR_SIG_SPEED.trycnt = 0;
		CAR_SIG_SPEED.buf[1] = CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSig_0();	
		CAR_SIG_SPEED.buf[2] = CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSig_1();
	}

	if(CanIfGetMsg_gCanIf_ABS_ESP_1_flag()==ON)
	{
		//ABS_ESP_1_2E9_IDX
		CAR_SIG_SPEED.flag = ON;
		CAR_SIG_SPEED.trycnt = 0;
		CAR_SIG_SPEED.buf[1] = CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSig_0();	
		CAR_SIG_SPEED.buf[2] = CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSig_1();
	}

	if(CanIfGetMsg_gCanIf_CGW_TCU_G_flag()==ON)
	{
		//CGW_TCU_G_301_IDX
		CAR_SIG_GEAR.flag = ON;
		CAR_SIG_GEAR.trycnt = 0;
		CAR_SIG_GEAR.buf[1] = CanIfGet_gCanIf_CGW_TCU_G_GBPositoionDisplay()|(CanIfGet_gCanIf_CGW_TCU_G_DriverMode()<<6);		
		DPRINTF(DBG_MSG1,"uart ok after CGW_TCU_G_301_IDX %x \n\r",CAR_SIG_GEAR.buf[1]);
	}

	if(CanIfGetMsg_gCanIf_FRM_3_flag()==ON)
	{
		//FRM_3_305_IDX
		tmp8 = (CanIfGet_gCanIf_FRM_3_TimeGapSet_DVD()<<6)|(CanIfGet_gCanIf_FRM_3_TimeGapLastSet_DVD()<<5)|
					(CanIfGet_gCanIf_FRM_3_FCW_ON_OFF_sts()<<4)|(CanIfGet_gCanIf_FRM_3_FCW_OPTION_sts()<<2)|
					(CanIfGet_gCanIf_FRM_3_AEB_ON_OFF_sts()<<1)|CanIfGet_gCanIf_FRM_3_DistanceWarning_on_off_sts();
		CAR_SIG_FRM3.flag = ON;
		CAR_SIG_FRM3.trycnt = 0;
		CAR_SIG_FRM3.buf[1] = (uint8)tmp8;			
//		DPRINTF(DBG_MSG1,"init can value FRM_3_305_IDX %x \n\r",CAR_SIG_FRM3.buf[1]);
	}
	prev_frm3 = CAR_SIG_FRM3.buf[1];//JANG_211116


	if(CanIfGetMsg_gCanIf_FCM_2_flag()==ON)
	{
		//FCM_2_307_IDX
		tmp8 =(CanIfGet_gCanIf_FCM_2_LDW_LKA_LaneAssitfee()<<6)|(CanIfGet_gCanIf_FCM_2_HMAoNoFFSts()<<4)|
					(CanIfGet_gCanIf_FCM_2_LDW_LKA_Senaitivityfeedback()<<2)|CanIfGet_gCanIf_FCM_2_SLAOnOffsts();
		tmp16 = (uint16)tmp8<<8;
		tmp8 = (CanIfGet_gCanIf_FCM_2_TJA_ICA_ON_OFF_STS()<<2)|CanIfGet_gCanIf_FCM_2_LDWOnOffSts();
		tmp16 |=(uint16)tmp8;
		CAR_SIG_FCM2.flag = ON;	
		CAR_SIG_FCM2.trycnt= 0;	
		CAR_SIG_FCM2.buf[1] = (uint8)(tmp16>>8);
		CAR_SIG_FCM2.buf[2] = (uint8)tmp16;		
	}		
	prev_fcm2 = ((uint16)CAR_SIG_FCM2.buf[1]<<8)|(uint16)CAR_SIG_FCM2.buf[2];		//JANG_211116

	if(CanIfGetMsg_gCanIf_CGW_ABM_G_flag()==ON)
	{
		//CGW_ABM_G_320_IDX
		tmp8 = CanIfGet_gCanIf_CGW_ABM_G_PsngrSeatBeltWarning();	
		tmp16 = (uint16)tmp8<<8;
		tmp8 = CanIfGet_gCanIf_CGW_ABM_G_CrashOutputSts();
		tmp16 |= (uint16)tmp8;
		CAR_SIG_CGW_ABM_G.flag = ON;
		CAR_SIG_CGW_ABM_G.trycnt = 0;
		CAR_SIG_CGW_ABM_G.buf[1] = (uint8)(tmp16>>8);
		CAR_SIG_CGW_ABM_G.buf[2] = (uint8)tmp16;	
	}

	if(CanIfGetMsg_gCanIf_CGW_SAM_G_flag()==ON)
	{
		//CGW_SAM_G_340_IDX
		tmp8 = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_0();		//speed 
		tmp32 =(uint32)tmp8<<24;
		tmp8 = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_1();
		tmp32 |=(uint32)tmp8<<16;
		tmp32 =(tmp32/4);																			// CAN filter : apply for value divde 4
		tmp8 = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngleSpeed();
		tmp32 |=(uint32)tmp8<<8;
		tmp8 = (CanIfGet_gCanIf_CGW_SAM_G_SASCalibrated()<<1)|CanIfGet_gCanIf_CGW_SAM_G_AngleFailure(); //fixed anglefailure
		tmp32 |=(uint32)tmp8;
		CAR_SIG_STR_ANGLE.flag = ON;
		CAR_SIG_STR_ANGLE.trycnt = 0;
		CAR_SIG_STR_ANGLE.buf[1] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_0();
		CAR_SIG_STR_ANGLE.buf[2] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_1();
		CAR_SIG_STR_ANGLE.buf[3] = (uint8)(tmp32>>8);
		CAR_SIG_STR_ANGLE.buf[4] = (uint8)tmp32;
	}

	if(CanIfGetMsg_gCanIf_CGW_EPS_G_flag()==ON)
	{
		//CGW_EPS_G_380_IDX
		CAR_SIG_CGW_EPS_G.flag = ON;						 
		CAR_SIG_CGW_EPS_G.trycnt = 0;
		CAR_SIG_CGW_EPS_G.buf[1] = CanIfGet_gCanIf_CGW_EPS_G_EpsmodeSts();
	}
	prev_drivemode = CAR_SIG_CGW_EPS_G.buf[1]; //JANG_211116

	if(CanIfGetMsg_gCanIf_CEM_BCM_1_flag()==ON)
	{
		//CEM_BCM_1_391_IDX
		CAR_SIG_CEM_BCM1.flag= ON;
		CAR_SIG_CEM_BCM1.trycnt = 0;
		CAR_SIG_CEM_BCM1.buf[1] =  (CanIfGet_gCanIf_CEM_BCM_1_HazardLightSW()<<1)|CanIfGet_gCanIf_CEM_BCM_1_DriverDoorLockSts();
	}

	if(CanIfGetMsg_gCanIf_CEM_BCM_2_flag()==ON)
	{
		//CEM_BCM_2_392_IDX
		tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeTimeSts()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeSts()<<5)|
				(CanIfGet_gCanIf_CEM_BCM_2_KeySts()<<3)|(CanIfGet_gCanIf_CEM_BCM_2_BlankchanginglaneSts()<<2);
		tmp32 =(uint32)tmp8<<24;

		tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_ParkTailLightSts()<<7)|(CanIfGet_gCanIf_CEM_BCM_2_HighBeamSts()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_LowBeamSts()<<5)|
				(CanIfGet_gCanIf_CEM_BCM_2_DRLSts()<<4)|(CanIfGet_gCanIf_CEM_BCM_2_BlankingnumberSts()<<2)|(CanIfGet_gCanIf_CEM_BCM_2_DriverDoorSts()<<1)|
				CanIfGet_gCanIf_CEM_BCM_2_PsngrDoorSts();
		tmp32 |=(uint32)tmp8<<16;
		
		tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_BonnetSts()<<7)|(CanIfGet_gCanIf_CEM_BCM_2_RHRDoorSts()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_LHRDoorSts()<<5)|
				(CanIfGet_gCanIf_CEM_BCM_2_Trunk_BackDoor_Sts()<<4)|(CanIfGet_gCanIf_CEM_BCM_2_MirrorFoldSts()<<3)|
				(CanIfGet_gCanIf_CEM_BCM_2_External_Welcome_LightSts()<<1)|CanIfGet_gCanIf_CEM_BCM_2_RearFogLightSts();
		tmp32 |=(uint32)tmp8<<8;	

		tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_ReverseGearSwitch()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_LaserhighbeamassistStatus()<<4)|
				(CanIfGet_gCanIf_CEM_BCM_2_FrontFogLightSts()<<3)|(CanIfGet_gCanIf_CEM_BCM_2_AutoUnlockSts()<<2)|CanIfGet_gCanIf_CEM_BCM_2_RemoteLockFeedbackSts();
		tmp32 |=(uint32)tmp8;	

		tmp8 = /*(CanIfGet_gCanIf_CEM_BCM_2_UnlockingBreathStatus()<<5)|*/(CanIfGet_gCanIf_CEM_BCM_2_MirrorFlipSts()<<4)|
				(CanIfGet_gCanIf_CEM_BCM_2_AidTurningIlluminationSts()<<3)|(CanIfGet_gCanIf_CEM_BCM_2_HazardLampForUrgencyBrakeSts()<<2)|
				(CanIfGet_gCanIf_CEM_BCM_2_AutoLockSts()<<1)|CanIfGet_gCanIf_CEM_BCM_2_RemoteTrunkOnlySts();
		tmp16 =((uint16)tmp8<<8);
		tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_BrakePedalSts()<<4)|CanIfGet_gCanIf_CEM_BCM_2_AlarmMode();	
		tmp16 |=(uint16)tmp8;
		CAR_SIG_CEM_BCM2.flag= ON;
		CAR_SIG_CEM_BCM2.trycnt = 0;
		CAR_SIG_CEM_BCM2.buf[1] = (uint8)(tmp32>>24);
		CAR_SIG_CEM_BCM2.buf[2] = (uint8)(tmp32>>16);
		CAR_SIG_CEM_BCM2.buf[3] = (uint8)(tmp32>>8);
		CAR_SIG_CEM_BCM2.buf[4] = (uint8)tmp32;
		CAR_SIG_CEM_BCM2.buf[5] = (uint8)(tmp16>>8);
		CAR_SIG_CEM_BCM2.buf[6] = (uint8)tmp16;
		
		CAR_SIG_TURNLIGHT.flag = ON;
		CAR_SIG_TURNLIGHT.trycnt = 0;
		CAR_SIG_TURNLIGHT.buf[1] = (CanIfGet_gCanIf_CEM_BCM_2_RHTurnlightSts()<<4)|CanIfGet_gCanIf_CEM_BCM_2_LHTurnlightSts();
	}
	//JANG_211116
	 prev_bcm2_status_0= ((uint32)CAR_SIG_CEM_BCM2.buf[1]<<24)|((uint32)CAR_SIG_CEM_BCM2.buf[2]>>16)|
	 	((uint32)CAR_SIG_CEM_BCM2.buf[3]>>8)|((uint32)CAR_SIG_CEM_BCM2.buf[4]);
	 prev_bcm2_status_1 = ((uint16)CAR_SIG_CEM_BCM2.buf[5]>>8)|((uint16)CAR_SIG_CEM_BCM2.buf[6]);	

	if(CanIfGetMsg_gCanIf_AVM_APA_3_flag()==ON)
	{
		//AVM_APA_3_41A_IDX
		tmp8 =	(CanIfGet_gCanIf_AVM_APA_3_AVMDisplaySts()<<2)|(CanIfGet_gCanIf_AVM_APA_3_AVMFeedbackSts_RemoteMode()<<1)
						|CanIfGet_gCanIf_AVM_APA_3_AVM_FactoryMode();
		CAR_SIG_AVM_APA3.flag = ON;
		CAR_SIG_AVM_APA3.trycnt = 0;
		CAR_SIG_AVM_APA3.buf[1] = tmp8;
	}

	if(CanIfGetMsg_gCanIf_ICM_1_flag()==ON)
	{
		//ICM_1_430_IDX
		CAR_SIG_FUEL.flag = ON;
		CAR_SIG_FUEL.trycnt = 0;
		CAR_SIG_FUEL.buf[1] = CanIfGet_gCanIf_ICM_1_FuelLevel();

		CAR_SIG_SEATBELT.flag = ON;
		CAR_SIG_SEATBELT.trycnt = 0;
		CAR_SIG_SEATBELT.buf[1] = (CanIfGet_gCanIf_ICM_1_DriverSeatBeltWarningSts()<<4)|CanIfGet_gCanIf_ICM_1_PassengerSeatBeltWarningSts();

		tmp32 = (((uint32)CanIfGet_gCanIf_ICM_1_TotalOdometer_km_0()<<16)|((uint32)CanIfGet_gCanIf_ICM_1_TotalOdometer_km_1()<<8)
						|CanIfGet_gCanIf_ICM_1_TotalOdometer_km_2());

		CAR_SIG_ODOMETER.flag = ON;
		CAR_SIG_ODOMETER.trycnt = 0;
		CAR_SIG_ODOMETER.buf[1] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_0();
		CAR_SIG_ODOMETER.buf[2] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_1();
		CAR_SIG_ODOMETER.buf[3] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_2();

	}

	if(CanIfGetMsg_gCanIf_PLG_flag()==ON)
	{
		//PLG_436_IDX
		CAR_SIG_PLG1.flag = ON;
		CAR_SIG_PLG1.trycnt = 0;
		CAR_SIG_PLG1.buf[1] = CanIfGet_gCanIf_PLG_RearDoorStatus();
		CAR_SIG_PLG1.buf[2] =CanIfGet_gCanIf_PLG_PLGSASSts();
	}
	prev_plg = (uint16)CAR_SIG_PLG1.buf[2]|((uint16)CAR_SIG_PLG1.buf[1]<<8); //JANG_211116


	if(CanIfGetMsg_gCanIf_RADAR_1_flag()==ON)
	{
		//RADAR_1_440_IDX
		CAR_SIG_CEM_RADAR1.buf[1] = CanIfGet_gCanIf_RADAR_1_RHRRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[2] = CanIfGet_gCanIf_RADAR_1_LHRRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[3] = CanIfGet_gCanIf_RADAR_1_RHMRRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[4] = CanIfGet_gCanIf_RADAR_1_LHMRRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[5] = CanIfGet_gCanIf_RADAR_1_LHFRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[6] = CanIfGet_gCanIf_RADAR_1_RHFRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[7] = CanIfGet_gCanIf_RADAR_1_RHMFRadarSensorDistance();
		CAR_SIG_CEM_RADAR1.buf[8] = CanIfGet_gCanIf_RADAR_1_LHMFRadarSensorDistance();
		tmp8 = (CanIfGet_gCanIf_RADAR_1_AudibleBeepRate()<<3)|(CanIfGet_gCanIf_RADAR_1_RadarDetectSts()<<1)|
			(CanIfGet_gCanIf_RADAR_1_RadarWorkSts());

		CAR_SIG_CEM_RADAR1.buf[9] = tmp8;
		CAR_SIG_CEM_RADAR1.flag = ON;
		CAR_SIG_CEM_RADAR1.trycnt = 0;
	}

	if(CanIfGetMsg_gCanIf_BSD_1_flag()==ON)
	{
		//BSD_1_449_IDX
		tmp8 = (CanIfGet_gCanIf_BSD_1_SystemState()<<6)|(CanIfGet_gCanIf_BSD_1_BSDState()<<5)|
		(CanIfGet_gCanIf_BSD_1_RCTAState()<<4)|(CanIfGet_gCanIf_BSD_1_RCTAWarningLeft()<<3)|
		(CanIfGet_gCanIf_BSD_1_RCTAWarningRight()<<2)|(CanIfGet_gCanIf_BSD_1_RCWSts()<<1)| CanIfGet_gCanIf_BSD_1_DOWSts();
		CAR_SIG_BSD1.flag = ON;
		CAR_SIG_BSD1.trycnt = 0;
		CAR_SIG_BSD1.buf[1] = tmp8;
	}
	prev_bsd1_status = CAR_SIG_BSD1.buf[1];//JANG_211116

	if(CanIfGetMsg_gCanIf_ICM_2_flag()==ON)
	{
		//ICM_2_452_IDX
		CAR_SIG_ICM2.flag = ON;
		CAR_SIG_ICM2.trycnt=0;
		CAR_SIG_ICM2.buf[1] = CanIfGet_gCanIf_ICM_2_STAT_Chime();
	}

	if(CanIfGetMsg_gCanIf_WPC_1_flag()==ON)
	{
		//WPC_1_460_IDX
		CAR_SIG_WPC.flag = ON;
		CAR_SIG_WPC.trycnt = 0;
		CAR_SIG_WPC.buf[1] = (CanIfGet_gCanIf_WPC_1_WPC_WirelessChargingStatus()<<1)|CanIfGet_gCanIf_WPC_1_NFCSts();
	}

	if(CanIfGetMsg_gCanIf_AVM_APA_4_flag()==ON)
	{
		//AVM_APA_4_475_IDX
		CAR_SIG_AVM_APA4.flag = ON;
		CAR_SIG_AVM_APA4.trycnt = 0;
		CAR_SIG_AVM_APA4.buf[1] = CanIfGet_gCanIf_AVM_APA_4_RHSRRadarSensorDistance();
		CAR_SIG_AVM_APA4.buf[2] = CanIfGet_gCanIf_AVM_APA_4_LHSRRadarSensorDistance();
		CAR_SIG_AVM_APA4.buf[3] = CanIfGet_gCanIf_AVM_APA_4_RHSFRadarSensorDistance();
		CAR_SIG_AVM_APA4.buf[4] = CanIfGet_gCanIf_AVM_APA_4_LHSFRadarSensorDistance();
	}

	if(CanIfGetMsg_gCanIf_CEM_PEPS_flag()==ON)
	{
		//CEM_PEPS_480_IDX
		CAR_SIG_CEM_PEPS.flag =	ON;
		CAR_SIG_CEM_PEPS.trycnt = 0;
		CAR_SIG_CEM_PEPS.buf[1] = CanIfGet_gCanIf_CEM_PEPS_Pollingsts();
	}
	prev_cem_peps = CAR_SIG_CEM_PEPS.buf[1];//JANG_211116

	if(CanIfGetMsg_gCanIf_CEM_ALM_1_flag()==ON)
	{
		//CEM_ALM_1_490_IDX
		tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_SeatSetPop_Up()<<4)|(CanIfGet_gCanIf_CEM_ALM_1_PersonalSeatEn()<<2)|	
			CanIfGet_gCanIf_CEM_ALM_1_PersonalBeamEn();
		tmp16 =(uint16)tmp8<<8;
		tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_SeatSetStatus()<<4)|CanIfGet_gCanIf_CEM_ALM_1_DeletePeronalSet_FB();
		tmp16 |=(uint16)tmp8;	
		CAR_SIG_SEATSTATUS.flag = ON;
		CAR_SIG_SEATSTATUS.trycnt = 0;
		CAR_SIG_SEATSTATUS.buf[1] = (uint8)(tmp16>>8);
		CAR_SIG_SEATSTATUS.buf[2] = (uint8)tmp16;

		tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_STAT_ControlSwitch()<<5)|(CanIfGet_gCanIf_CEM_ALM_1_STAT_StaticEffect()<<2)|
			CanIfGet_gCanIf_CEM_ALM_1_STAT_Music();
		tmp32 = (uint32)tmp8<<16;
		tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_STAT_StaticColour()<<4)| CanIfGet_gCanIf_CEM_ALM_1_STAT_Apiluminance();
		tmp32 |= (uint32)tmp8<<8;
		tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_STAT_AssociatedWithDriverMode()<<4)
			|(CanIfGet_gCanIf_CEM_ALM_1_STAT_WelcomeWaterLamp()<<2)| CanIfGet_gCanIf_CEM_ALM_1_Wash_Car_Status();

		tmp32 |= (uint32)tmp8;
		CAR_SIG_CEM_ALM1.flag = ON;
		CAR_SIG_CEM_ALM1.trycnt = 0;
		CAR_SIG_CEM_ALM1.buf[1] = (uint8)(tmp32>>16);
		CAR_SIG_CEM_ALM1.buf[2] = (uint8)(tmp32>>8);
		CAR_SIG_CEM_ALM1.buf[3] = (uint8)tmp32;
	}
	prev_seat_status = ((uint16)CAR_SIG_SEATSTATUS.buf[1]<<8)|CAR_SIG_SEATSTATUS.buf[2];//JANG_211116
	prev_breath_status= ((uint32)CAR_SIG_CEM_ALM1.buf[1]<<16)|((uint32)CAR_SIG_CEM_ALM1.buf[2]<<8)|CAR_SIG_CEM_ALM1.buf[3];//JANG_211116						


	if(CanIfGetMsg_gCanIf_ABS_ESP_5_flag()==ON)
	{
		//ABS_ESP_5_4E5_IDX
		tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_LHRPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_LHRPulseCounter();
		tmp32 = (uint32)tmp16<<16;
		tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_RHRPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_RHRPulseCounter();
		tmp32 |= (uint32)tmp16;
		tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_LHFPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_LHFPulseCounter();
		tmp32_1 = (uint32)tmp16<<16;
		tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_RHFPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_RHFPulseCounter();
		tmp32_1 |= (uint32)tmp16;
		CAR_SIG_ABS_ESP5.flag = ON;
		CAR_SIG_ABS_ESP5.trycnt = 0;
		CAR_SIG_ABS_ESP5.buf[1] = (uint8)(tmp32>>24);
		CAR_SIG_ABS_ESP5.buf[2] = (uint8)(tmp32>>16);
		CAR_SIG_ABS_ESP5.buf[3] = (uint8)(tmp32>>8);
		CAR_SIG_ABS_ESP5.buf[4] = (uint8)tmp32;
		CAR_SIG_ABS_ESP5.buf[5] = (uint8)(tmp32_1>>24);
		CAR_SIG_ABS_ESP5.buf[6] = (uint8)(tmp32_1>>16);
		CAR_SIG_ABS_ESP5.buf[7] = (uint8)(tmp32_1>>8);
		CAR_SIG_ABS_ESP5.buf[8] = (uint8)tmp32_1;
	}

	if(CanIfGetMsg_gCanIf_AVM_APA_2_flag()==ON)
	{
		//AVM_APA_2_508_IDX
		tmp8 = (CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraFront()<<4)|(CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraLeft()<<3)	
			|(CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraRear()<<2)|(CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraRight()<<1)
			|CanIfGet_gCanIf_AVM_APA_2_AVMStatusFault();
		CAR_SIG_AVM_APA2.flag = ON;
		CAR_SIG_AVM_APA2.trycnt = 0;
		CAR_SIG_AVM_APA2.buf[1] = tmp8;
	}

	if(CanIfGetMsg_gCanIf_TBOX_2_flag()==ON)
	{
		//TBOX_2_519_IDX
		CAR_SIG_TBOX2.flag = ON;
		CAR_SIG_TBOX2.trycnt = 0;
		CAR_SIG_TBOX2.buf[1] = CanIfGet_gCanIf_TBOX_2_TriggerAlarmSts();
	}

	if(CanIfGetMsg_gCanIf_CEM_IPM_1_flag()==ON)
	{
		//CEM_IPM_1_51A_IDX
		tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_CEM_FLTempsts()<<2)|CanIfGet_gCanIf_CEM_IPM_1_CEM_RecyMode();		//speed 
		tmp32 = (uint32)tmp8<<24;
		tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_CEM_FRTempsts()<<2)|(CanIfGet_gCanIf_CEM_IPM_1_ACStatus()<<1)|	
			CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontOFFSts();
		tmp32 |= (uint32)tmp8<<16;
		tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontBlowSpdCtrlsts()<<4)|	CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontBlowModeSts();
		tmp32 |= (uint32)tmp8<<8;
		tmp8 = /*(CanIfGet_gCanIf_CEM_IPM_1_ACStatus()<<7)|(CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontOFFSts()<<6)|*/
			(CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontAutoACSts()<<5)|(CanIfGet_gCanIf_CEM_IPM_1_CEM_SyncSts()<<4)|
			(CanIfGet_gCanIf_CEM_IPM_1_CEM_PM25_Detect()<<3)|(CanIfGet_gCanIf_CEM_IPM_1_CEM_AnionPurify()<<2)|
			(CanIfGet_gCanIf_CEM_IPM_1_BTReduceWindSpeedSts()<<1)|	CanIfGet_gCanIf_CEM_IPM_1_IPMblowerdelaySts();
		tmp32 |= (uint32)tmp8;

		tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_IPMFirstBlowingSts()<<6)|(CanIfGet_gCanIf_CEM_IPM_1_AC_DisplaySts()<<5)|
			CanIfGet_gCanIf_CEM_IPM_1_CEM_TempknobRollingCounter();
		
		CAR_SIG_CEM_IPM.flag =ON;
		CAR_SIG_CEM_IPM.trycnt=0;
		CAR_SIG_CEM_IPM.buf[1] =(uint8)(tmp32>>24);
		CAR_SIG_CEM_IPM.buf[2] =(uint8)(tmp32>>16);
		CAR_SIG_CEM_IPM.buf[3] =(uint8)(tmp32>>8);
		CAR_SIG_CEM_IPM.buf[4] =(uint8)tmp32;
		CAR_SIG_CEM_IPM.buf[5] =tmp8;
		prev_cem_ipm_0 = tmp32;
		prev_cem_ipm_1 = tmp8;	
	}
	else
	{
		prev_cem_ipm_0 =  (uint32)CAR_SIG_CEM_IPM.buf[4];//JANG_211116
		prev_cem_ipm_1 =  CAR_SIG_CEM_IPM.buf[5];	//JANG_211116
	}

	if(CanIfGetMsg_gCanIf_AVM_2_flag()==ON)
	{
		//AVM_2_51C_IDX
		tmp8 = (CanIfGet_gCanIf_AVM_2_AVM_DisplaySts()<<6)|(CanIfGet_gCanIf_AVM_2_AVM_WorkModeSts()<<4)|
			(CanIfGet_gCanIf_AVM_2_AVM_LanguageSts()<<1)|CanIfGet_gCanIf_AVM_2_AVM_SteeringWheelSts();
		tmp16 = (uint16)tmp8<<8;
		tmp8 = (CanIfGet_gCanIf_AVM_2_AVM_RadarWariningSts()<<6)|(CanIfGet_gCanIf_AVM_2_AVM_LDW_FunSts()<<5)|
			(CanIfGet_gCanIf_AVM_2_AVM_BSD_FunSts()<<4)|(CanIfGet_gCanIf_AVM_2_AVM_DefDispMode()<<3)|CanIfGet_gCanIf_AVM_2_DVR_WorkState();
		tmp16 |= (uint16)tmp8;
		CAR_SIG_AVM2.flag= ON;
		CAR_SIG_AVM2.trycnt=0;
		CAR_SIG_AVM2.buf[1] = (uint8)(tmp16>>8);
		CAR_SIG_AVM2.buf[2] = (uint8)tmp16;
	}

	if(CanIfGetMsg_gCanIf_ICM_4_flag()==ON)
	{
		//ICM_4_530_IDX
//		CAR_SIG_FUEL.flag = ON;
//		CAR_SIG_FUEL.trycnt = 0;
//		CAR_SIG_FUEL.buf[2] = CanIfGet_gCanIf_ICM_1_FuelLevelFailSts();

		CAR_SIG_DTE.flag = ON;
		CAR_SIG_DTE.trycnt = 0;
		CAR_SIG_DTE.buf[1] = CanIfGet_gCanIf_ICM_4_DistanceToEmpty_Km_0();
		CAR_SIG_DTE.buf[2] = CanIfGet_gCanIf_ICM_4_DistanceToEmpty_Km_1();

#ifdef CX62C_FUNCTION
		tmp8	= (CanIfGet_gCanIf_ICM_4_ThemeSys()<<2)|CanIfGet_gCanIf_ICM_4_ICMLanguageSet();
		
		tmp16 = ((uint16)CanIfGet_gCanIf_ICM_4_Brake_Fuel_Level()<<12)|((uint16)CanIfGet_gCanIf_ICM_4_AverageFuelConsume_0()<<8)|
					CanIfGet_gCanIf_ICM_4_AverageFuelConsume_1();

		tmp32 |= (uint32)tmp16<<16;
		tmp16 = ((uint16)CanIfGet_gCanIf_ICM_4_Engine_oil_Pressure()<<13)|((uint16)CanIfGet_gCanIf_ICM_4_AverageVehicleSpeed_0()<<8)|
			CanIfGet_gCanIf_ICM_4_AverageVehicleSpeed_1();
		tmp32 |= (uint32)tmp16;

		CAR_SIG_ICM4.flag = ON;
		CAR_SIG_ICM4.trycnt = 0;
		CAR_SIG_ICM4.buf[1] = tmp8; 
		CAR_SIG_ICM4.buf[2] = (uint8)(tmp32>>24); 	
		CAR_SIG_ICM4.buf[3] = (uint8)(tmp32>>16);
		CAR_SIG_ICM4.buf[4] = (uint8)(tmp32>>8);
		CAR_SIG_ICM4.buf[5] = (uint8)tmp32;
#else
		CAR_SIG_ICM4.flag = ON;
		CAR_SIG_ICM4.trycnt = 0;
		CAR_SIG_ICM4.buf[1] = (CanIfGet_gCanIf_ICM_4_ThemeSys()<<2)|CanIfGet_gCanIf_ICM_4_ICMLanguageSet();
#endif
	}
	
	
	if(CanIfGetMsg_gCanIf_PM25_1_flag()==ON)
	{
		//PM25_1_53A_IDX
		tmp8 = CanIfGet_gCanIf_PM25_1_PM2_5Indensity_0();
		tmp32 = (uint32)tmp8<<24;
		tmp8 = CanIfGet_gCanIf_PM25_1_PM2_5Indensity_1();
		tmp32 |= (uint32)tmp8<<16;
		tmp8	= CanIfGet_gCanIf_PM25_1_PM2_5Outdensity_0();
		tmp32 |= (uint32)tmp8<<8;
		tmp8	= CanIfGet_gCanIf_PM25_1_PM2_5Outdensity_1();
		tmp32 |= (uint32)tmp8;

		tmp8	= (CanIfGet_gCanIf_PM25_1_AirInQLevel()<<4)|CanIfGet_gCanIf_PM25_1_AirOutQLevel();
		tmp16 |= (uint16)tmp8<<8;
		tmp8	= (CanIfGet_gCanIf_PM25_1_PM25Sts()<<2)|CanIfGet_gCanIf_PM25_1_PM25ErrSts();
		tmp16 |= (uint16)tmp8;

		CAR_SIG_PM25.flag = ON;
		CAR_SIG_PM25.trycnt = 0;
		CAR_SIG_PM25.buf[1] = (uint8)(tmp32>>24);
		CAR_SIG_PM25.buf[2] = (uint8)(tmp32>>16);
		CAR_SIG_PM25.buf[3] = (uint8)(tmp32>>8);
		CAR_SIG_PM25.buf[4] = (uint8)(tmp32);
		CAR_SIG_PM25.buf[5] = (uint8)(tmp16>>8);
		CAR_SIG_PM25.buf[6] = (uint8)(tmp16);
	}

	if(CanIfGetMsg_gCanIf_AFU_1_flag()==ON)
	{
		//AFU_1_560_IDX
		tmp8 = (CanIfGet_gCanIf_AFU_1_AirFragCh1Detn()<<7)|(CanIfGet_gCanIf_AFU_1_AirFragCh1Type()<<4)|
			(CanIfGet_gCanIf_AFU_1_AirFragCh2Detn()<<3)|CanIfGet_gCanIf_AFU_1_AirFragCh2Type();
		tmp32 = (uint32)tmp8<<16;
		tmp8  = (CanIfGet_gCanIf_AFU_1_AirFragCh3Detn()<<7)|(CanIfGet_gCanIf_AFU_1_AirFragCh3Type()<<4)|
			(CanIfGet_gCanIf_AFU_1_AFU_ON_button()<<3)|CanIfGet_gCanIf_AFU_1_AirFragLvlRspn();
		tmp32 |= (uint32)tmp8<<8;
		tmp8 = (CanIfGet_gCanIf_AFU_1_AirFragChChgSts()<<6)|(CanIfGet_gCanIf_AFU_1_AirFragInitSts()<<4)|
			(CanIfGet_gCanIf_AFU_1_AirFragSwthSts()<<2)|CanIfGet_gCanIf_AFU_1_AirFragCh1Button();
		tmp32 |= (uint32)tmp8;

		CAR_SIG_AFU.flag = ON;			
		CAR_SIG_AFU.trycnt = 0;
		CAR_SIG_AFU.buf[1] = (uint8)(tmp32>>16);
		CAR_SIG_AFU.buf[2] = (uint8)(tmp32>>8);
		CAR_SIG_AFU.buf[3] = (uint8)(tmp32);
	}
	prev_afu = ((uint32)CAR_SIG_AFU.buf[1]<<16)|((uint32)CAR_SIG_AFU.buf[2]<<8)|(uint32)CAR_SIG_AFU.buf[3];	//JANG_211116	

	if(CanIfGetMsg_gCanIf_DVR_1_flag()==ON)
	{
		//DVR_1_58A_IDX
		tmp8 = (CanIfGet_gCanIf_DVR_1_DVR_Sdcard_Sts()<<5)|(CanIfGet_gCanIf_DVR_1_DVR_SystemSts()<<4)
			|(CanIfGet_gCanIf_DVR_1_DVR_LocalPhotographResult()<<2)|CanIfGet_gCanIf_DVR_1_DVRLocalContinuousPhotograph();
		
		CAR_SIG_DVR1.flag = ON;
		CAR_SIG_DVR1.trycnt = 0;
		CAR_SIG_DVR1.buf[1] = tmp8;
	}

	if(CanIfGetMsg_gCanIf_CEM_BCM_4_flag()==ON)
	{
		//CEM_BCM_4_58D_IDX
		tmp8 = (CanIfGet_gCanIf_CEM_BCM_4_FL_WIN_Position()<<4)|CanIfGet_gCanIf_CEM_BCM_4_FR_WIN_Position();
		tmp16 = (uint16)tmp8;
		tmp16 = (uint16)(tmp16<<8)|(CanIfGet_gCanIf_CEM_BCM_4_RL_WIN_Position()<<4)|CanIfGet_gCanIf_CEM_BCM_4_RR_WIN_Position();
		CAR_SIG_CEM_BCM4.flag= ON;
		CAR_SIG_CEM_BCM4.trycnt = 0;
		CAR_SIG_CEM_BCM4.buf[1] =(uint8)(tmp16>>8);
		CAR_SIG_CEM_BCM4.buf[2] = (uint8)tmp16;
	}

	if(CanIfGetMsg_gCanIf_CEM_BCM_3_flag()==ON)
	{
		CAR_SIG_CEM_BCM3.flag= ON;
		CAR_SIG_CEM_BCM3.trycnt = 0;
		CAR_SIG_CEM_BCM3.buf[1] = (CanIfGet_gCanIf_CEM_BCM_3_EngineControlSts()<<2)|CanIfGet_gCanIf_CEM_BCM_3_DWM_Sts_Msg();
	}
#endif

#if 0
	if(CanIfGetMsg_gCanIf_CEM_PEPS_flag()&&(CanIfGet_gCanIf_CEM_PEPS_Pollingsts()==0))
		{
			CAR_SIG_CEM_PEPS.buf[1] =0; //Pollingsts =01
		}

	if(CanIfGetMsg_gCanIf_CEM_ALM_1_flag())
	{
		if((CanIfGet_gCanIf_CEM_ALM_1_STAT_Apiluminance()==0)&&(CanIfGet_gCanIf_CEM_ALM_1_STAT_StaticEffect()==0))
		{
			CAR_SIG_CEM_ALM1.buf[1] = 0;
			CAR_SIG_CEM_ALM1.buf[2] = 0;
		}

		if((CanIfGet_gCanIf_CEM_ALM_1_PersonalSeatEn()==OFF)&&(CanIfGet_gCanIf_CEM_ALM_1_PersonalBeamEn()==OFF))
		{
			CAR_SIG_SEATSTATUS.buf[1] = 0;	//PersonalSeatEn =01	, PersonalBeamEn=01
		}
		
	}

	if(CanIfGetMsg_gCanIf_BSD_1_flag()&&(CanIfGet_gCanIf_BSD_1_BSDState()==0)&&(CanIfGet_gCanIf_BSD_1_RCWSts()==0)
		&&(CanIfGet_gCanIf_BSD_1_DOWSts()==0))
		{
			CAR_SIG_BSD1.buf[1] = 0;//BSDState= 01, RCWSts = 01,DOWSts =01
		}
	
	if(CanIfGetMsg_gCanIf_PLG_flag()&&(CanIfGet_gCanIf_PLG_PLGSASSts()==0))
		{
			CAR_SIG_PLG1.buf[2] =0;	//PLGSASSts =0x5F
		}
	if(CanIfGetMsg_gCanIf_CEM_BCM_2_flag()&&(CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeTimeSts()==0)&&
		(CanIfGet_gCanIf_CEM_BCM_2_AutoUnlockSts()==0)&&(CanIfGet_gCanIf_CEM_BCM_2_LaserhighbeamassistStatus()==0)&&(CanIfGet_gCanIf_CEM_BCM_2_MirrorFlipSts()==0)
		&&(CanIfGet_gCanIf_CEM_BCM_2_HazardLampForUrgencyBrakeSts()==0)&&(CanIfGet_gCanIf_CEM_BCM_2_AutoLockSts()==0))
		{
			CAR_SIG_CEM_BCM2.buf[1] = 0;//FollowMeHomeTimeSts=01, LaserhighbeamassistStatus =01,AutoUnlockSts =01,MirrorFlipSts=01, HazardLampForUrgency =01, AutolockSts =01
			CAR_SIG_CEM_BCM2.buf[4] = 0;
			CAR_SIG_CEM_BCM2.buf[5] = 0;
		}
	if(CanIfGetMsg_gCanIf_CGW_EPS_G_flag()&&(CanIfGet_gCanIf_CGW_EPS_G_EpsmodeSts()==0))
		{
			CAR_SIG_CGW_EPS_G.buf[1] = 0;//EpsmodeSts = Comfort
		}
	if(CanIfGetMsg_gCanIf_FCM_2_flag()&&(CanIfGet_gCanIf_FCM_2_HMAoNoFFSts()==0)&&
		(CanIfGet_gCanIf_FCM_2_LDW_LKA_LaneAssitfee()==0)&&(CanIfGet_gCanIf_FCM_2_SLAOnOffsts()==0)&&(CanIfGet_gCanIf_FCM_2_TJA_ICA_ON_OFF_STS()==0))
		{
			CAR_SIG_FCM2.buf[1] = 0; //HMAoNoFFSts =01, LDW_LKA_LaneAssitfee=01, SLAOnoffsts =01,TJA_ICA_ON_OFF_STS=01,
			CAR_SIG_FCM2.buf[2] = 0;
		}
	if(CanIfGetMsg_gCanIf_FRM_3_flag()&&(CanIfGet_gCanIf_FRM_3_FCW_ON_OFF_sts()==0)&&(CanIfGet_gCanIf_FRM_3_FCW_OPTION_sts()==0)&&
		(CanIfGet_gCanIf_FRM_3_AEB_ON_OFF_sts()==0)&&(CanIfGet_gCanIf_FRM_3_DistanceWarning_on_off_sts()==0))
		{
			CAR_SIG_FRM3.buf[1] = 0;	//FCW_ON_OFF_sts =1, FCW_OPTION_sts= 01, AEB_ON_OFF_sts=01, DistanceWarning_on_off =01
		}
	if(CanIfGetMsg_gCanIf_AFU_1_flag()&&(CanIfGet_gCanIf_AFU_1_AirFragCh1Detn()==0)&&
		(CanIfGet_gCanIf_AFU_1_AirFragLvlRspn()==0))
		{
			CAR_SIG_AFU.buf[1] = 0;//AirFragCh1Detn=01
			CAR_SIG_AFU.buf[2] = 0;//AirFragLvlRspn=02
		}
#endif
}

void UART_Comm_Proc(DEVICE *dev)
{
    Uart_Rx_Service(dev);
#ifdef CPU_COMM_TEST_MODE
    Uart1_Rx_Service(dev);
#endif
    // 1->2
    if (1 <= GetElapsedTime(uart_tx_timer_tick))
    {
        Uart_Tx_Service(dev);
        uart_tx_timer_tick = GetTickCount();
    }
    // X50_Uart_TX_Action(&r_Device);		// for test

    // X50_UART_Rx_Service(dev);
    Uart_Tx_TestMode(dev);
    if (Setting_Update == ON)
    {
        Setting_Update = OFF;
        save_last_memory();
    }
}

static void Uart_Tx_TestMode(DEVICE *dev)
{

#ifdef CAN_RXTX_TEST_FUNC
    if (dev->r_System->rxtx_test.state == ON)
    {
        if (10 <= GetElapsedTime(uart_txrx_test_tick))
        {
            tx_msg_cnt++;
            dev->r_System->rxtx_test.cnt++;
            uart_rxtx_test[(dev->r_System->rxtx_test.cnt % 10)].msg_cnt = tx_msg_cnt;
            if ((dev->r_System->rxtx_test.cnt % 10) == 0)
                uart_rxtx_test[(dev->r_System->rxtx_test.cnt % 10)].buf[0] = 10;
            else
                uart_rxtx_test[(dev->r_System->rxtx_test.cnt % 10)].buf[0] = (dev->r_System->rxtx_test.cnt % 10);

            uart_rxtx_test[(dev->r_System->rxtx_test.cnt % 10)].buf[1] = (dev->r_System->rxtx_test.txcnt >> 8);
            uart_rxtx_test[(dev->r_System->rxtx_test.cnt % 10)].buf[2] = (dev->r_System->rxtx_test.txcnt & 0xFF);
            Uart_Send_Func(&uart_rxtx_test[(dev->r_System->rxtx_test.cnt % 10)]);

            uart_txrx_test_tick            = GetTickCount();
            dev->r_System->rxtx_test.txcnt = (dev->r_System->rxtx_test.cnt / 10);

            // 10000 is stop
            if (dev->r_System->rxtx_test.txcnt == 1000)
                dev->r_System->rxtx_test.state = OFF;
        }
    }
#endif
}

static void Uart_Tx_Service(DEVICE *dev)
{
    uint8 tx_buf[300] = {
        0,
    };
    uint16 i = 0, tmp16 = 0;

    tx_buf[0] = CPU_STX;

    if (dev->r_System->uart_ok == OFF)
        return;

#if 1
    if (acc_packet.flag != OFF)
    {
        if ((acc_packet.trycnt < CPU_SEND_RETRY_CNT) && (CPU_SEND_200_TIME <= GetElapsedTime(acc_packet.timeout)))
        {
            if (acc_packet.trycnt == 0)
            {
                tx_msg_cnt++;
                acc_packet.msg_cnt = tx_msg_cnt;
            }
            Uart_Send_Func(&acc_packet);
            acc_packet.trycnt++;
            acc_packet.timeout = GetTickCount();
            return;
        }
        else if (CPU_SEND_RETRY_CNT <= acc_packet.trycnt)
        {
            acc_packet.trycnt = 0;
            acc_packet.flag   = OFF;
        }
    }

    if (radio_packet.flag == ON)
    {
        // 20->5
        if (5 <= GetElapsedTime(radio_packet.timeout))
        {
            tx_msg_cnt++;
            radio_packet.msg_cnt = tx_msg_cnt;
            Uart_Send_Func(&radio_packet);
            radio_packet.flag    = OFF;
            radio_packet.timeout = GetTickCount();
            return;
        }
    }
    else if (radio_packet.flag == 2)
    {
        if ((radio_packet.trycnt < CPU_SEND_RETRY_CNT) && (CPU_SEND_RETRY_TIME <= GetElapsedTime(radio_packet.timeout)))
        {
            if (radio_packet.trycnt == 0)
            {
                tx_msg_cnt++;
                radio_packet.msg_cnt = tx_msg_cnt;
            }
            Uart_Send_Func(&radio_packet);
            radio_packet.trycnt++;
            radio_packet.timeout = GetTickCount();
            return;
        }
        else if (CPU_SEND_RETRY_CNT <= radio_packet.trycnt)
        {
            radio_packet.trycnt = 0;
            radio_packet.flag   = OFF;
        }
    }
#if 0
	if(dev->r_SendCPU->send_radio_finish_ast!=OFF){
		if((radio_packet.trycnt<CPU_SEND_RETRY_CNT)&&(CPU_SEND_RETRY_TIME<=GetElapsedTime(radio_packet.timeout)))
		{
			if(radio_packet.trycnt==0){
				tx_msg_cnt++;
				radio_packet.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&radio_packet);
			radio_packet.trycnt++;
			radio_packet.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_RETRY_CNT<=radio_packet.trycnt){
			radio_packet.trycnt =0;
			dev->r_SendCPU->send_radio_finish_ast = OFF;
		}		
	}
#endif

    if (key_packet.flag != OFF)
    {
        if (/*(radio_packet.trycnt<3)&&*/ (CPU_SEND_20_TIME <= GetElapsedTime(key_packet.timeout)))
        {
            tx_msg_cnt++;
            key_packet.msg_cnt = tx_msg_cnt;
            Uart_Send_Func(&key_packet);
            //			key_packet.trycnt++;
            key_packet.flag    = OFF;
            key_packet.timeout = GetTickCount();
            return;
        }
#if 0
		else if(1<=key_packet.trycnt/*3<=radio_packet.trycnt*/){
			key_packet.trycnt =0;
			key_packet.flag = OFF;
			}
#endif
    }

    if (radio_signal_status.flag != OFF)
    {
        radio_signal_status.msg_cnt = tx_msg_cnt;
        Uart_Send_Func(&radio_signal_status);
        radio_signal_status.flag    = OFF;
        radio_signal_status.timeout = GetTickCount();
        return;
    }

#if 1
    if (acc_ign_status.flag != OFF)
    {
        if ((acc_ign_status.trycnt < CPU_SEND_RETRY_CNT) && (CPU_SEND_200_TIME <= GetElapsedTime(acc_ign_status.timeout)))
        {
            if (acc_ign_status.trycnt == 0)
            {
                tx_msg_cnt++;
                acc_ign_status.msg_cnt = tx_msg_cnt;
            }
            Uart_Send_Func(&acc_ign_status);
            acc_ign_status.trycnt++;
            acc_ign_status.timeout = GetTickCount();

            return;
        }
        else if (CPU_SEND_RETRY_CNT <= acc_ign_status.trycnt)
        {
            acc_ign_status.trycnt = 0;
            acc_ign_status.flag   = OFF;
        }
    }
#endif

    //<<JANG_210920_4
    if (diag_usb_device.flag != OFF)
    {
        if ((diag_usb_device.trycnt < CPU_SEND_RETRY_CNT) && (CPU_SEND_200_TIME <= GetElapsedTime(diag_usb_device.timeout)))
        {
            if (diag_usb_device.trycnt == 0)
            {
                tx_msg_cnt++;
                diag_usb_device.msg_cnt = tx_msg_cnt;
            }
            Uart_Send_Func(&diag_usb_device);
            diag_usb_device.trycnt++;
            diag_usb_device.timeout = GetTickCount();

            return;
        }
        else if (CPU_SEND_RETRY_CNT <= diag_usb_device.trycnt)
        {
            diag_usb_device.trycnt = 0;
            diag_usb_device.flag   = OFF;
        }
    }

    //>>
#if 0
	if(CAR_SIG_GEAR.flag !=OFF)
	{
		if((CAR_SIG_GEAR.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_GEAR.timeout))){
			if(CAR_SIG_GEAR.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_GEAR.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_GEAR);
			CAR_SIG_GEAR.trycnt++;
			CAR_SIG_GEAR.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_GEAR.trycnt){
			CAR_SIG_GEAR.trycnt =0;
			CAR_SIG_GEAR.flag  = OFF;
		}
	}
	
	if(CAR_SIG_STR_ANGLE.flag !=OFF)
	{
		if((CAR_SIG_STR_ANGLE.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_STR_ANGLE.timeout))){
			if(CAR_SIG_STR_ANGLE.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_STR_ANGLE.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_STR_ANGLE);
			CAR_SIG_STR_ANGLE.trycnt++;
//			CAR_SIG_STR_ANGLE.flag = OFF;
			CAR_SIG_STR_ANGLE.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_STR_ANGLE.trycnt){
			CAR_SIG_STR_ANGLE.trycnt =0;
			CAR_SIG_STR_ANGLE.flag = OFF;
		}
	}

#if 1
	if(CAR_SIG_SPEED.flag!=OFF)
	{
		if((CAR_SIG_SPEED.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_SPEED.timeout)))
		{
				if(CAR_SIG_SPEED.trycnt==0){
					tx_msg_cnt++;
					CAR_SIG_SPEED.msg_cnt = tx_msg_cnt;
				}
				Uart_Send_Func(&CAR_SIG_SPEED);
				CAR_SIG_SPEED.timeout = GetTickCount();
	//			CAR_SIG_SPEED.flag = OFF;
				CAR_SIG_SPEED.trycnt++;
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_SPEED.trycnt)
		{
			CAR_SIG_SPEED.trycnt =0;
			CAR_SIG_SPEED.flag = OFF;
		}
	}
#else
		if(100<=GetElapsedTime(CAR_SIG_SPEED.timeout))
		{
			Uart_Send_Func(&CAR_SIG_SPEED);
			CAR_SIG_SPEED.timeout = GetTickCount();
		}
#endif

	if(CAR_SIG_SEATSTATUS.flag !=OFF)
	{
		if((CAR_SIG_SEATSTATUS.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_SEATSTATUS.timeout))){
			if(CAR_SIG_SEATSTATUS.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_SEATSTATUS.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_SEATSTATUS);
			CAR_SIG_SEATSTATUS.trycnt++;
//			CAR_SIG_SEATSTATUS.flag = OFF;
			CAR_SIG_SEATSTATUS.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_SEATSTATUS.trycnt){
			CAR_SIG_SEATSTATUS.trycnt =0;
			CAR_SIG_SEATSTATUS.flag = OFF;
		}
	}

	if(CAR_SIG_FUEL.flag !=OFF)
	{
		if((CAR_SIG_FUEL.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_FUEL.timeout))){
			if(CAR_SIG_FUEL.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_FUEL.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_FUEL);
			CAR_SIG_FUEL.trycnt++;
//			CAR_SIG_FUEL.flag = OFF;
			CAR_SIG_FUEL.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_FUEL.trycnt){
			CAR_SIG_FUEL.trycnt =0;
			CAR_SIG_FUEL.flag = OFF;
		}
	}

	if(CAR_SIG_DTE.flag !=OFF)
	{
		if((CAR_SIG_DTE.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_DTE.timeout))){
			if(CAR_SIG_DTE.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_DTE.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_DTE);
			CAR_SIG_DTE.trycnt++;
//			CAR_SIG_DTE.flag= OFF;
			CAR_SIG_DTE.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_DTE.trycnt){
			CAR_SIG_DTE.trycnt =0;
			CAR_SIG_DTE.flag = OFF;
		}
	}
	
	if(CAR_SIG_DVR1.flag !=OFF)
	{
		if((CAR_SIG_DVR1.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_DVR1.timeout))){
			if(CAR_SIG_DVR1.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_DVR1.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_DVR1);
			CAR_SIG_DVR1.trycnt++;
//			CAR_SIG_DVR1.flag = OFF;
			CAR_SIG_DVR1.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_DVR1.trycnt){
			CAR_SIG_DVR1.trycnt =0;
			CAR_SIG_DVR1.flag  = OFF;
		}
	}

	if(CAR_SIG_SEATBELT.flag !=OFF)
	{
		if((CAR_SIG_SEATBELT.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_SEATBELT.timeout))){
			if(CAR_SIG_SEATBELT.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_SEATBELT.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_SEATBELT);
			CAR_SIG_SEATBELT.trycnt++;
//			CAR_SIG_SEATBELT.flag = OFF;
			CAR_SIG_SEATBELT.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_SEATBELT.trycnt){
			CAR_SIG_SEATBELT.trycnt =0;
			CAR_SIG_SEATBELT.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_BCM1.flag !=OFF)
	{
		if((CAR_SIG_CEM_BCM1.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_CEM_BCM1.timeout))){
			if(CAR_SIG_CEM_BCM1.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_BCM1.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_BCM1);
			CAR_SIG_CEM_BCM1.trycnt++;
//			CAR_SIG_CEM_BCM1.flag = OFF;
			CAR_SIG_CEM_BCM1.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_BCM1.trycnt){
			CAR_SIG_CEM_BCM1.trycnt =0;
			CAR_SIG_CEM_BCM1.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_BCM2.flag !=OFF)
	{
		if((CAR_SIG_CEM_BCM2.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_CEM_BCM2.timeout))){
			if(CAR_SIG_CEM_BCM2.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_BCM2.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_BCM2);
			CAR_SIG_CEM_BCM2.trycnt++;
//			CAR_SIG_CEM_BCM2.flag = OFF;
			CAR_SIG_CEM_BCM2.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_BCM2.trycnt){
			CAR_SIG_CEM_BCM2.trycnt =0;
			CAR_SIG_CEM_BCM2.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_BCM3.flag !=OFF)
	{
		if((CAR_SIG_CEM_BCM3.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_CEM_BCM3.timeout))){
			if(CAR_SIG_CEM_BCM3.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_BCM3.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_BCM3);
			CAR_SIG_CEM_BCM3.trycnt++;
//			CAR_SIG_CEM_BCM3.flag = OFF;
			CAR_SIG_CEM_BCM3.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_BCM3.trycnt){
			CAR_SIG_CEM_BCM3.trycnt =0;
			CAR_SIG_CEM_BCM3.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_BCM4.flag !=OFF)
	{
		if((CAR_SIG_CEM_BCM4.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_CEM_BCM4.timeout))){
			if(CAR_SIG_CEM_BCM4.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_BCM4.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_BCM4);
			CAR_SIG_CEM_BCM4.trycnt++;
	//			CAR_SIG_CEM_BCM4.flag = OFF;
			CAR_SIG_CEM_BCM4.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_BCM4.trycnt){
			CAR_SIG_CEM_BCM4.trycnt =0;
			CAR_SIG_CEM_BCM4.flag = OFF;
		}
	}

	if(CAR_SIG_ABS_ESP5.flag !=OFF)
	{
		if((CAR_SIG_ABS_ESP5.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_ABS_ESP5.timeout))){
			if(CAR_SIG_ABS_ESP5.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_ABS_ESP5.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_ABS_ESP5);
			CAR_SIG_ABS_ESP5.trycnt++;
	//			CAR_SIG_ABS_ESP5.flag = OFF;
			CAR_SIG_ABS_ESP5.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_ABS_ESP5.trycnt){
			CAR_SIG_ABS_ESP5.trycnt =0;
			CAR_SIG_ABS_ESP5.flag = OFF;
		}
	}

	if(CAR_SIG_PM25.flag !=OFF)
	{
		if((CAR_SIG_PM25.trycnt<CPU_SEND_CNT)&&(CPU_SEND_1000_TIME<=GetElapsedTime(CAR_SIG_PM25.timeout))){
			if(CAR_SIG_PM25.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_PM25.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_PM25);
			CAR_SIG_PM25.trycnt++;
	//			CAR_SIG_PM25.flag = OFF;
			CAR_SIG_PM25.timeout = GetTickCount();
			return;	
		}
		else if(CPU_SEND_CNT<=CAR_SIG_PM25.trycnt){
			CAR_SIG_PM25.trycnt =0;
			CAR_SIG_PM25.flag = OFF;
		}
	}

	if(CAR_SIG_BSD1.flag !=OFF)
	{
		if((CAR_SIG_BSD1.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_BSD1.timeout))){
			if(CAR_SIG_BSD1.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_BSD1.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_BSD1);
			CAR_SIG_BSD1.trycnt++;
	//			CAR_SIG_BSD1.flag = OFF;
			CAR_SIG_BSD1.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_BSD1.trycnt){
			CAR_SIG_BSD1.trycnt =0;
			CAR_SIG_BSD1.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_RADAR1.flag !=OFF)
	{
		if((CAR_SIG_CEM_RADAR1.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_CEM_RADAR1.timeout))){
			if(CAR_SIG_CEM_RADAR1.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_RADAR1.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_RADAR1);
			CAR_SIG_CEM_RADAR1.trycnt++;
//			CAR_SIG_CEM_RADAR1.flag = OFF;
			CAR_SIG_CEM_RADAR1.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_RADAR1.trycnt){
			CAR_SIG_CEM_RADAR1.trycnt =0;
			CAR_SIG_CEM_RADAR1.flag = OFF;
		}
	}

	if(CAR_SIG_TURNLIGHT.flag !=OFF)
	{
		if((CAR_SIG_TURNLIGHT.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_TURNLIGHT.timeout))){
			if(CAR_SIG_TURNLIGHT.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_TURNLIGHT.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_TURNLIGHT);
			CAR_SIG_TURNLIGHT.trycnt++;
//			CAR_SIG_TURNLIGHT.flag= OFF;
			CAR_SIG_TURNLIGHT.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_TURNLIGHT.trycnt){
			CAR_SIG_TURNLIGHT.trycnt =0;
			CAR_SIG_TURNLIGHT.flag = OFF;
		}
	}

	if(CAR_SIG_ODOMETER.flag !=OFF)
	{
		if((CAR_SIG_ODOMETER.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_ODOMETER.timeout))){
			if(CAR_SIG_ODOMETER.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_ODOMETER.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_ODOMETER);
			CAR_SIG_ODOMETER.trycnt++;
//			dev->r_SendCPU->sig_odo_meter = OFF;
			CAR_SIG_ODOMETER.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_ODOMETER.trycnt){
			CAR_SIG_ODOMETER.trycnt =0;
			CAR_SIG_ODOMETER.flag = OFF;
		}
	}

	if(CAR_SIG_AVM_APA4.flag!=OFF)
	{
		if((CAR_SIG_AVM_APA4.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_AVM_APA4.timeout))){
			if(CAR_SIG_AVM_APA4.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_AVM_APA4.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_AVM_APA4);
			CAR_SIG_AVM_APA4.trycnt++;
//			CAR_SIG_AVM_APA4.flag = OFF;
			CAR_SIG_AVM_APA4.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_AVM_APA4.trycnt){
			CAR_SIG_AVM_APA4.trycnt =0;
			CAR_SIG_AVM_APA4.flag = OFF;
		}
	}

	if(CAR_SIG_AVM_APA2.flag!=OFF)
	{
		if((CAR_SIG_AVM_APA2.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_AVM_APA2.timeout))){
			if(CAR_SIG_AVM_APA2.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_AVM_APA2.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_AVM_APA2);
			CAR_SIG_AVM_APA2.trycnt++;
//			CAR_SIG_AVM_APA2.flag = OFF;
			CAR_SIG_AVM_APA2.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_AVM_APA2.trycnt){
			CAR_SIG_AVM_APA2.trycnt =0;
			CAR_SIG_AVM_APA2.flag = OFF;
		}
	}

	if(CAR_SIG_AVM_APA3.flag !=OFF)
	{
		if((CAR_SIG_AVM_APA3.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_AVM_APA3.timeout))){
			if(CAR_SIG_AVM_APA3.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_AVM_APA3.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_AVM_APA3);
			CAR_SIG_AVM_APA3.trycnt++;
//			CAR_SIG_AVM_APA3.flag  = OFF;
			CAR_SIG_AVM_APA3.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_AVM_APA3.trycnt){
			CAR_SIG_AVM_APA3.trycnt =0;
			CAR_SIG_AVM_APA3.flag  = OFF;
		}
	}

	if(CAR_SIG_AVM2.flag !=OFF)
	{
		if((CAR_SIG_AVM2.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_AVM2.timeout))){
			if(CAR_SIG_AVM2.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_AVM2.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_AVM2);
			CAR_SIG_AVM2.trycnt++;
//			CAR_SIG_AVM2.flag  = OFF;
			CAR_SIG_AVM2.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_AVM2.trycnt){
			CAR_SIG_AVM2.trycnt =0;
			CAR_SIG_AVM2.flag  = OFF;
		}
	}
	
	if(CAR_SIG_FCM2.flag !=OFF)
	{
		if((CAR_SIG_FCM2.trycnt<CPU_SEND_CNT)&&(CPU_SEND_50_TIME<=GetElapsedTime(CAR_SIG_FCM2.timeout))){
			if(CAR_SIG_FCM2.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_FCM2.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_FCM2);
			CAR_SIG_FCM2.trycnt++;
	//			CAR_SIG_FCM2.flag  = OFF;
			CAR_SIG_FCM2.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_FCM2.trycnt){
			CAR_SIG_FCM2.trycnt =0;
			CAR_SIG_FCM2.flag  = OFF;
		}
	}

	if(CAR_SIG_FRM3.flag !=OFF)
	{
		if((CAR_SIG_FRM3.trycnt<CPU_SEND_CNT)&&(CPU_SEND_50_TIME<=GetElapsedTime(CAR_SIG_FRM3.timeout))){
			if(CAR_SIG_FRM3.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_FRM3.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_FRM3);
			CAR_SIG_FRM3.trycnt++;
//			CAR_SIG_FRM3.flag  = OFF;
			CAR_SIG_FRM3.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_FRM3.trycnt){
			CAR_SIG_FRM3.trycnt =0;
			CAR_SIG_FRM3.flag  = OFF;
		}
	}

	if(CAR_SIG_WPC.flag!=OFF)
	{
		if((CAR_SIG_WPC.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_WPC.timeout))){
			if(CAR_SIG_WPC.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_WPC.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_WPC);
			CAR_SIG_WPC.trycnt++;
//			CAR_SIG_WPC.flag = OFF;
			CAR_SIG_WPC.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_WPC.trycnt){
			CAR_SIG_WPC.trycnt =0;
			CAR_SIG_WPC.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_ALM1.flag!=OFF)
	{
		if((CAR_SIG_CEM_ALM1.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_CEM_ALM1.timeout))){
			if(CAR_SIG_CEM_ALM1.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_ALM1.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_ALM1);
			CAR_SIG_CEM_ALM1.trycnt++;
//			CAR_SIG_CEM_ALM1.flag = OFF;
			CAR_SIG_CEM_ALM1.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_ALM1.trycnt){
			CAR_SIG_CEM_ALM1.trycnt =0;
			CAR_SIG_CEM_ALM1.flag = OFF;
		}
	}	

	if(CAR_SIG_TBOX2.flag!=OFF)
	{
		if((CAR_SIG_TBOX2.trycnt<CPU_SEND_CNT)&&(CPU_SEND_RETRY_TIME<=GetElapsedTime(CAR_SIG_TBOX2.timeout))){
			if(CAR_SIG_TBOX2.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_TBOX2.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_TBOX2);
			CAR_SIG_TBOX2.trycnt++;
//			CAR_SIG_TBOX2.flag = OFF;
			CAR_SIG_TBOX2.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_TBOX2.trycnt){
			CAR_SIG_TBOX2.trycnt =0;
			CAR_SIG_TBOX2.flag = OFF;
		}
	}

	if(CAR_SIG_ENGINESPEED.flag !=OFF)
	{
		if((CAR_SIG_ENGINESPEED.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_ENGINESPEED.timeout))){
			if(CAR_SIG_ENGINESPEED.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_ENGINESPEED.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_ENGINESPEED);
			CAR_SIG_ENGINESPEED.trycnt++;
//			CAR_SIG_ENGINESPEED.flag  = OFF;
			CAR_SIG_ENGINESPEED.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_ENGINESPEED.trycnt){
			CAR_SIG_ENGINESPEED.trycnt =0;
			CAR_SIG_ENGINESPEED.flag  = OFF;
		}
	}

	if(CAR_SIG_CLT.flag!=OFF)
	{
		if((CAR_SIG_CLT.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_CLT.timeout))){
			if(CAR_SIG_CLT.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CLT.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CLT);
			CAR_SIG_CLT.trycnt++;
//			CAR_SIG_CLT.flag = OFF;
			CAR_SIG_CLT.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_CLT.trycnt){
			CAR_SIG_CLT.trycnt =0;
			CAR_SIG_CLT.flag = OFF;
		}
	}

	if(CAR_SIG_ROLLCOUNT.flag!=OFF)
	{
		if((CAR_SIG_ROLLCOUNT.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_ROLLCOUNT.timeout))){
			if(CAR_SIG_ROLLCOUNT.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_ROLLCOUNT.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_ROLLCOUNT);
			CAR_SIG_ROLLCOUNT.trycnt++;
//			CAR_SIG_ROLLCOUNT.flag = OFF;
			CAR_SIG_ROLLCOUNT.timeout = GetTickCount();
			return;
		}
		else if(CPU_SEND_CNT<=CAR_SIG_ROLLCOUNT.trycnt){
			CAR_SIG_ROLLCOUNT.trycnt =0;
			CAR_SIG_ROLLCOUNT.flag = OFF;
		}
	}

	if(CAR_SIG_MFS_HEARTRATE.flag!=OFF)
	{
		if((CAR_SIG_MFS_HEARTRATE.trycnt<CPU_SEND_CNT)&&(CPU_SEND_20_TIME<=GetElapsedTime(CAR_SIG_MFS_HEARTRATE.timeout))){
			if(CAR_SIG_MFS_HEARTRATE.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_MFS_HEARTRATE.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_MFS_HEARTRATE);
			CAR_SIG_MFS_HEARTRATE.trycnt++;
//			CAR_SIG_MFS_HEARTRATE.flag = OFF;
			CAR_SIG_MFS_HEARTRATE.timeout = GetTickCount();
		return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_MFS_HEARTRATE.trycnt){
			CAR_SIG_MFS_HEARTRATE.trycnt =0;
			CAR_SIG_MFS_HEARTRATE.flag = OFF;
		}
	}

	if(CAR_SIG_AFU.flag!=OFF)
	{
		if((CAR_SIG_AFU.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_AFU.timeout))){
			if(CAR_SIG_AFU.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_AFU.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_AFU);
			CAR_SIG_AFU.trycnt++;
//			CAR_SIG_AFU.flag = OFF;
			CAR_SIG_AFU.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_AFU.trycnt){
			CAR_SIG_AFU.trycnt =0;
			CAR_SIG_AFU.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_IPM.flag !=OFF)
	{
		if((CAR_SIG_CEM_IPM.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_CEM_IPM.timeout))){
			if(CAR_SIG_CEM_IPM.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_IPM.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_IPM);
			CAR_SIG_CEM_IPM.trycnt++;
//			CAR_SIG_CEM_IPM.flag = OFF;
			CAR_SIG_CEM_IPM.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_IPM.trycnt){
			CAR_SIG_CEM_IPM.trycnt =0;
			CAR_SIG_CEM_IPM.flag = OFF;
		}
	}

	if(CAR_SIG_ICM2.flag!=OFF)
	{
		if((CAR_SIG_ICM2.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_ICM2.timeout))){
			if(CAR_SIG_ICM2.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_ICM2.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_ICM2);
			CAR_SIG_ICM2.trycnt++;
//			CAR_SIG_ICM2.flag = OFF;
			CAR_SIG_ICM2.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_ICM2.trycnt){
			CAR_SIG_ICM2.trycnt =0;
			CAR_SIG_ICM2.flag = OFF;
		}
	}

	if(CAR_SIG_ICM4.flag!=OFF)
	{
		if((CAR_SIG_ICM4.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_ICM4.timeout))){
			if(CAR_SIG_ICM4.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_ICM4.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_ICM4);
			CAR_SIG_ICM4.trycnt++;
//			CAR_SIG_ICM4.flag = OFF;
			CAR_SIG_ICM4.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_ICM4.trycnt){
			CAR_SIG_ICM4.trycnt =0;
			CAR_SIG_ICM4.flag = OFF;
		}
	}

	if(CAR_SIG_CEM_PEPS.flag!=OFF)
	{
		if((CAR_SIG_CEM_PEPS.trycnt<CPU_SEND_CNT)&&(CPU_SEND_50_TIME<=GetElapsedTime(CAR_SIG_CEM_PEPS.timeout))){
			if(CAR_SIG_CEM_PEPS.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CEM_PEPS.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CEM_PEPS);
			CAR_SIG_CEM_PEPS.trycnt++;
//			CAR_SIG_CEM_PEPS.flag = OFF;
			CAR_SIG_CEM_PEPS.timeout = GetTickCount();
		return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_CEM_PEPS.trycnt){
			CAR_SIG_CEM_PEPS.trycnt =0;
			CAR_SIG_CEM_PEPS.flag = OFF;
		}
	}

	if(CAR_SIG_PLG1.flag!=OFF)
	{
		if((CAR_SIG_PLG1.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_PLG1.timeout))){
			if(CAR_SIG_PLG1.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_PLG1.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_PLG1);
			CAR_SIG_PLG1.trycnt++;
//			CAR_SIG_PLG1.flag = OFF;
			CAR_SIG_PLG1.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_PLG1.trycnt){
			CAR_SIG_PLG1.trycnt =0;
			CAR_SIG_PLG1.flag = OFF;
		}
	}

	if(CAR_SIG_CGW_EPS_G.flag!=OFF)
	{
		if((CAR_SIG_CGW_EPS_G.trycnt<CPU_SEND_CNT)&&(CPU_SEND_100_TIME<=GetElapsedTime(CAR_SIG_CGW_EPS_G.timeout))){
			if(CAR_SIG_CGW_EPS_G.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CGW_EPS_G.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CGW_EPS_G);
			CAR_SIG_CGW_EPS_G.trycnt++;
//			CAR_SIG_CGW_EPS_G.flag = OFF;
			CAR_SIG_CGW_EPS_G.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_CGW_EPS_G.trycnt){
			CAR_SIG_CGW_EPS_G.trycnt =0;
			CAR_SIG_CGW_EPS_G.flag = OFF;
		}
	}

	if(CAR_SIG_CGW_ABM_G.flag!=OFF)
	{
		if((CAR_SIG_CGW_ABM_G.trycnt<CPU_SEND_CNT)&&(CPU_SEND_500_TIME<=GetElapsedTime(CAR_SIG_CGW_ABM_G.timeout))){
			if(CAR_SIG_CGW_ABM_G.trycnt==0){
				tx_msg_cnt++;
				CAR_SIG_CGW_ABM_G.msg_cnt = tx_msg_cnt;
			}
			Uart_Send_Func(&CAR_SIG_CGW_ABM_G);
			CAR_SIG_CGW_ABM_G.trycnt++;
//			CAR_SIG_CGW_ABM_G.flag = OFF;
			CAR_SIG_CGW_ABM_G.timeout = GetTickCount();
			return;

		}
		else if(CPU_SEND_CNT<=CAR_SIG_CGW_ABM_G.trycnt){
			CAR_SIG_CGW_ABM_G.trycnt =0;
			CAR_SIG_CGW_ABM_G.flag = OFF;
		}
	}
#endif
    if (dtc_list_cmd.flag != OFF)
    {
        if ((dtc_list_cmd.trycnt < CPU_SEND_RETRY_CNT) && (CPU_SEND_200_TIME <= GetElapsedTime(dtc_list_cmd.timeout)))
        {
            if (dtc_list_cmd.trycnt == 0)
            {
                tx_msg_cnt++;
                dtc_list_cmd.msg_cnt = tx_msg_cnt;
            }
            Uart_Send_Func(&dtc_list_cmd);
            DPRINTF(DBG_MSG1, "send dtc_list_cmd\n\r");
            dtc_list_cmd.trycnt++;
            dtc_list_cmd.timeout = GetTickCount();
            return;
        }
        else if (CPU_SEND_RETRY_CNT <= dtc_list_cmd.trycnt)
        {
            dtc_list_cmd.trycnt = 0;
            dtc_list_cmd.flag   = OFF;
        }
    }

    if (dvr_ctl_cmd.flag != OFF)
    {
        if ((dvr_ctl_cmd.trycnt < CPU_SEND_RETRY_CNT) && (CPU_SEND_200_TIME <= GetElapsedTime(dvr_ctl_cmd.timeout)))
        {
            if (dvr_ctl_cmd.trycnt == 0)
            {
                tx_msg_cnt++;
                dvr_ctl_cmd.msg_cnt = tx_msg_cnt;
            }
            Uart_Send_Func(&dvr_ctl_cmd);
            dvr_ctl_cmd.trycnt++;
            dvr_ctl_cmd.timeout = GetTickCount();
            return;
        }
        else if (CPU_SEND_RETRY_CNT <= dvr_ctl_cmd.trycnt)
        {
            dvr_ctl_cmd.trycnt = 0;
            dvr_ctl_cmd.flag   = OFF;
        }
    }

    if (dev->r_SendCPU->send_last_memory != OFF)
    {
        if (CPU_SEND_50_TIME <= GetElapsedTime(Last_Memory.timeout))
        {
            tx_buf[1] = MCU_RESPONSE;
            tx_buf[2] = ((Last_Memory.len + 4) + PACKET_HEADER_SIZE + 1) >> 8;
            tx_buf[3] = ((Last_Memory.len + 4) + PACKET_HEADER_SIZE + 1);
            tx_buf[4] = Last_Memory.message;
            tx_buf[5] = (uint8)(Last_Memory.msg_cnt >> 8);
            tx_buf[6] = (uint8)Last_Memory.msg_cnt;
            memcpy(&tx_buf[PACKET_HEADER_SIZE], &Last_Memory.buf[0], (Last_Memory.len + 4));
            for (i = 0; i < (Last_Memory.len + 4) + PACKET_HEADER_SIZE; i++)
                tmp16 += tx_buf[i];
            tx_buf[(Last_Memory.len + 4) + PACKET_HEADER_SIZE] = (uint8)(tmp16 & 0xff); // checksum

#if 1
            DPRINTF(DBG_MSG1, "Last_Memory :  ");
            for (i = 7; i < ((Last_Memory.len + 4) + PACKET_HEADER_SIZE + 1); i++)
                DPRINTF(DBG_MSG1, "%x ", tx_buf[i]);
            DPRINTF(DBG_MSG1, " \n\r");
#endif
            uputa0(&tx_buf[0], (Last_Memory.len + 4) + PACKET_HEADER_SIZE + 1);
            dev->r_SendCPU->send_last_memory = OFF;
            Last_Memory.timeout              = GetTickCount();
            return;
        }
    }
#endif

#if 1
    if (dev->r_SendCPU->send_radio_valid_signal != OFF)
    {
        if (10 <= GetElapsedTime(fm_check_radio_packet.timeout))
        {
            // response
            tx_buf[1] = MCU_RESPONSE;
            tx_buf[2] = 0x00;
            tx_buf[3] = fm_check_radio_packet.len + PACKET_HEADER_SIZE + 1;
            tx_buf[4] = fm_check_radio_packet.message;
            tx_buf[5] = (uint8)(fm_check_radio_packet.msg_cnt >> 8);
            tx_buf[6] = (uint8)fm_check_radio_packet.msg_cnt;

            memcpy(&tx_buf[PACKET_HEADER_SIZE], &fm_check_radio_packet.buf[0], fm_check_radio_packet.len);
            for (i = 0; i < fm_check_radio_packet.len + PACKET_HEADER_SIZE; i++)
                tmp16 += tx_buf[i];
            tx_buf[fm_check_radio_packet.len + PACKET_HEADER_SIZE] = (uint8)(tmp16 & 0xff);

#if 0
			UPRINTF(DBG_MSG1,"FM signal valid data :  ");
			for(i=0;i<(fm_check_radio_packet.len+PACKET_HEADER_SIZE+1);i++)
				UPRINTF(DBG_MSG1,"%x ",tx_buf[i]);
			UPRINTF(DBG_MSG1," \n\r");
#endif

            uputa0(&tx_buf[0], fm_check_radio_packet.len + PACKET_HEADER_SIZE + 1);
            dev->r_SendCPU->send_radio_valid_signal = OFF;
            fm_check_radio_packet.timeout           = GetTickCount();
        }
    }
#endif
}

uint8 uart_rx_buffer[uart_rx_buf_max] = {
    0,
};
uint8 rx_backup_packet[2] = {
    0,
};
volatile static uint16 rx_counter = 0;
volatile static uint16 rx_len     = 0;

volatile static uint16 rx_error_cnt        = 0;
static uint8           last_rx_data_backup = 0;

static void Uart_Rx_Service(DEVICE *dev)
{
    uint16        tmp16 = 0;
    static uint16 len   = 0;
    int           i     = 0;

#if 1
    while (kbhit0() != 0)
    {
        uart_rx_buffer[rx_counter] = (uint8)ugetc0();
        rx_counter++;

        if (uart_rx_buffer[0] == CPU_STX)
        {
            tmp16  = (uint16)uart_rx_buffer[2];
            rx_len = (uint16)(tmp16 << 8) + (uint16)uart_rx_buffer[3];

            if (rx_counter == rx_len)
            {
                // calcurate checksum
                dev->r_System->uart_send_on = ON;
                break;
            }
            else if (rx_counter == 3)
            {
                if (((uart_rx_buffer[1] == CPU_COMMAND) || (uart_rx_buffer[1] == CPU_RESPONSE)) && (uart_rx_buffer[2] == 0))
                    ;
                else
                {
                    if ((uart_rx_buffer[1] == CPU_STX) && ((uart_rx_buffer[2] == CPU_COMMAND) || (uart_rx_buffer[2] == CPU_RESPONSE)))
                    {
                        uart_rx_buffer[0] = uart_rx_buffer[1];
                        uart_rx_buffer[1] = uart_rx_buffer[2];
                        rx_counter--;
                    }
                    else if (uart_rx_buffer[2] == CPU_STX)
                    {
                        uart_rx_buffer[0] = CPU_STX;
                        rx_counter        = 1;
                    }
                    DPRINTF(DBG_ERR, "packet err\n\r");
                    rx_error_cnt++;
                    rx_len = 0;
                }
            }
            else if (rx_counter == 5)
            {
                // check size
                if ((uart_rx_buffer[4] == MSG_LAST_READ) || (uart_rx_buffer[4] == MSG_LAST_WRITE))
                {
                    if ((uart_rx_buf_max < rx_len) || (rx_len == 0))
                    {
                        DPRINTF(DBG_ERR, "max len err %d\n\r", rx_len);
                        rx_len     = 0;
                        rx_counter = 0;
                        rx_error_cnt++;
                        memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
                    }
                }
                else
                {
                    //    20  ->uart_rx_buf_max
                    if ((uart_rx_buf_max < rx_len) || (rx_len == 0))
                    {
                        DPRINTF(DBG_ERR, "packet len err %d\n\r", rx_len);
                        rx_len     = 0;
                        rx_counter = 0;
                        rx_error_cnt++;
                        memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
                    }
                }
            }
        }
        else
        {
            if (last_rx_data_backup)
            {
                if (last_rx_data_backup == 1)
                {
                    uart_rx_buffer[1] = uart_rx_buffer[0];
                    uart_rx_buffer[0] = rx_backup_packet[0];
                    rx_counter++;
                }
                else
                {
                    uart_rx_buffer[2] = uart_rx_buffer[0];
                    uart_rx_buffer[1] = rx_backup_packet[1];
                    uart_rx_buffer[0] = rx_backup_packet[0];
                    rx_counter += 2;
                }
                last_rx_data_backup = 0;
            }
            else
            {
                DPRINTF(DBG_ERR, "first err: %x\n\r", uart_rx_buffer[0]);
                rx_len     = 0;
                rx_counter = 0;
                rx_error_cnt++;
                memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
            }
        }
    }

    if (dev->r_System->uart_send_on == ON)
    {
        dev->r_System->uart_send_on = OFF;
        uart_recv_data(dev, &uart_rx_buffer[0], rx_len);
        memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
        rx_counter = 0;
        rx_len     = 0;
    }
#else
    if (kbhit0() != 0)
    {
#if 0
		q_en(&uart_rx_data_q, (uint8_t) ugetc0());
#else
        uart_rx_buffer[rx_counter] = (uint8)ugetc0();
        rx_counter++;
#endif
    }
    else
    {
        return;
    }
#if 0
	//check rx_data
	//check first data
	if(uart_rx_buffer[0] == CPU_STX)
	{
		tmp16 =(uint16)uart_rx_buffer[2];
		rx_len = (uint16)(tmp16<<8)+(uint16)uart_rx_buffer[3];
		
		if(rx_len == rx_counter)
		{
			//calcurate checksum
			uart_recv_data(dev,&uart_rx_buffer[0], rx_len);
			memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
			rx_counter=0;
			rx_len=0;
		return;
	}

		if(rx_counter==3)
		{
			
			if(((uart_rx_buffer[1] == CPU_COMMAND)||(uart_rx_buffer[1] == CPU_RESPONSE))&&(uart_rx_buffer[2] == 0))
			;
			else
			{
				DPRINTF(DBG_ERR, "packet error:");
				for(i=0;i<(rx_counter);i++)
					DPRINTF(DBG_ERR,"%x ",uart_rx_buffer[i]);
				DPRINTF(DBG_ERR," \n\r");
				rx_error_cnt++;
				rx_counter=0;
				memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
				rx_len=0;
			return;
		}
	}

		//check size
		if((uart_rx_buffer[4]==MSG_LAST_READ)||(uart_rx_buffer[4]==MSG_LAST_WRITE))
		{
			if(uart_rx_buf_max<rx_len)
			{
				DPRINTF(DBG_ERR,"max len err %d\n\r",rx_len);
				rx_len=0;
				rx_counter=0;
				rx_error_cnt++;
				memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
			}
		}
		else
		{
			if(20<rx_len)
			{
				DPRINTF(DBG_ERR,"packet len err %d\n\r",rx_len);
				rx_len=0;
				rx_counter=0;
				rx_error_cnt++;
				memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
			}
		}
	}
	else
	{
		if(last_rx_data_backup)
		{
			if(last_rx_data_backup==1)
			{
				uart_rx_buffer[1]= uart_rx_buffer[0];
				uart_rx_buffer[0]= rx_backup_packet[0];
				rx_counter++;
			}
			else
			{
				uart_rx_buffer[2]= uart_rx_buffer[0];
				uart_rx_buffer[1]= rx_backup_packet[1];
				uart_rx_buffer[0]= rx_backup_packet[0];
				rx_counter++;
				rx_counter++;
			}
			last_rx_data_backup =0;
		}
		else
		{
			DPRINTF(DBG_ERR,"start err: %x\n\r",uart_rx_buffer[0]);	
			rx_len=0;
			rx_counter=0;
			rx_error_cnt++;
			memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
//			return;
		}
	}

#else
    // check first data
    if (uart_rx_buffer[0] != CPU_STX)
    {
        if (last_rx_data_backup)
        {
            if (last_rx_data_backup == 1)
            {
                uart_rx_buffer[1] = uart_rx_buffer[0];
                uart_rx_buffer[0] = rx_backup_packet[0];
                rx_counter++;
            }
            else
            {
                uart_rx_buffer[2] = uart_rx_buffer[0];
                uart_rx_buffer[1] = rx_backup_packet[1];
                uart_rx_buffer[0] = rx_backup_packet[0];
                rx_counter++;
                rx_counter++;
            }
            last_rx_data_backup = 0;
        }
        else
        {
            DPRINTF(DBG_ERR, "uart err1: %x\n\r", uart_rx_buffer[0]);
            rx_counter = 0;
            rx_len = 0;
            memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
            rx_error_cnt++;
            return;
        }
    }

    // check second data
    if (2 == rx_counter)
    {
        if ((uart_rx_buffer[1] == CPU_COMMAND) || (uart_rx_buffer[1] == CPU_RESPONSE))
            ;
        else
        {
            DPRINTF(DBG_ERR, "uart err2:");
            for (i = 0; i < (rx_counter); i++)
                DPRINTF(DBG_ERR, "%x ", uart_rx_buffer[i]);
            DPRINTF(DBG_ERR, " \n\r");
            rx_error_cnt++;
            rx_counter = 0;
            memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
            rx_len = 0;
            return;
        }
    }

    // check length data
    if (4 == rx_counter)
    {
        tmp16 = (uint16)uart_rx_buffer[2];
        rx_len = (uint16)(tmp16 << 8) + (uint16)uart_rx_buffer[3];

        //		if((uart_rx_buf_max<=rx_len)||(rx_len==0))
        if (uart_rx_buf_max <= rx_len)
        {
            DPRINTF(DBG_ERR, "max len:");
            for (i = 0; i < (rx_counter); i++)
                DPRINTF(DBG_ERR, "%x ", uart_rx_buffer[i]);
            DPRINTF(DBG_ERR, " \n\r");
            rx_len = 0;
            rx_counter = 0;
            memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
            rx_error_cnt++;
            return;
        }
    }

    // application
    if ((4 < rx_counter) && ((uart_rx_buffer[4] == MSG_LAST_READ) || (uart_rx_buffer[4] == MSG_LAST_WRITE)))
    {
    }
    else
    {
        if (20 < rx_len)
        {
            DPRINTF(DBG_ERR, "packet len err %d\n\r", rx_len);
            rx_len = 0;
            rx_counter = 0;
            memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
            return;
        }
    }

    // check checksum
    if (rx_counter == rx_len)
    {
        uart_recv_data(dev, &uart_rx_buffer[0], rx_len);
        rx_counter = 0;
        memset(&uart_rx_buffer[0], 0, sizeof(uart_rx_buffer));
        rx_len = 0;
    }
#endif
#endif
}

static void Uart1_Rx_Service(DEVICE *dev)
{
    uint8 tmp8 = 0;

    if (kbhit1() != 0)
    {
        tmp8 = ugetc1();
        DPRINTF(DBG_MSG1, "\n recv : %x \n\r", tmp8);
    }
    else
    {
        return;
    }

    switch (tmp8)
    {
    case 0x31:
        Self_BootSectorStat.AppStatus     = APP_UPGRADE1;
        Self_BootSectorStat.BootSectorNum = BOOT_MARK_PREV;

        /* �����Ҷ� ���� 5ȸ ��õ�? */
        tmp8 = 5;
        while (tmp8--)
        {
            if (BootStatusFlashBlock_writeStatus(&Self_BootSectorStat) == STATUS_SUCCESS)
            {
                DPRINTF(DBG_MSG1, "  flash write : %x \n\r", tmp8);
                break;
            }
        }
        BootStatusFlashBlock_init();
        BootStatusFlashBlock_getStatus(&Self_BootSectorStat);

        DPRINTF(DBG_MSG1, "read vale : %x %x \n\r", Self_BootSectorStat.AppStatus, Self_BootSectorStat.BootSectorNum);
        dev->r_System->mcu_update = ON;
        break;
    case 0x32:
        dev->r_System->mcu_update = ON;
        break;
    case 0x33:
        break;
    case 0x34:
        break;
    case 0x35:
        break;
    case 0x36:
        break;
    default:
        break;
    }
}

uint16 check_msg_cnt     = 0;
uint16 pre_check_msg_cnt = 0;

#if 1 // JANG_211123
uint16 check_msg_cnt_road_name   = 0;
uint16 check_msg_cnt_navi_info   = 0;
uint16 check_msg_cnt_song_name   = 0;
uint16 check_msg_cnt_artist_name = 0;
uint16 check_msg_cnt_call_num    = 0;
uint16 check_msg_cnt_call_name   = 0;
uint16 check_msg_cnt_call_inform = 0;
#endif

static void uart_recv_data(DEVICE *dev, uint8 *rx_data, uint16 len)
{
    MSGbuf msg;
    uint8  buf[300];
    uint8  rx_chk   = 0;
    uint8  calc_chk = 0;
    uint8  seq_chk  = 0;
    uint8  value    = 0;
    uint8  tmp8 = 0, tmp8_1 = 0, tmp8_2 = 0;
    uint16 i     = 0;
    uint16 tmp16 = 0;

    uint8 tempData[8] = {
        0x00,
    };
    static uint8  rrm_dvr3_cnt = 0;
    static uint16 old_switch   = 999;

    msg.buf = buf;
    seq_chk = len - 1;

    rx_chk = (uint8)rx_data[seq_chk];
    for (i = 0; i < seq_chk; i++)
    {
        tmp16 += rx_data[i];
    }
    calc_chk = (uint8)(tmp16 & 0xff);

#if 1
    if (rx_chk != calc_chk)
    {
        // last value is stx date
        if (rx_chk == CPU_STX)
        {
            last_rx_data_backup = 1;
            rx_backup_packet[0] = CPU_STX;
        }
        // last value is stx date
        if ((rx_data[seq_chk - 1] == CPU_STX) && (rx_chk == 0x0A))
        {
            last_rx_data_backup = 2;
            rx_backup_packet[0] = CPU_STX;
            rx_backup_packet[1] = rx_chk;
        }

        DPRINTF(DBG_ERR, "%s() - checksum ERROR!\n", __FUNCTION__);
        DPRINTF(DBG_ERR, ":	");
        for (i = 0; i < (seq_chk + 1); i++)
            DPRINTF(DBG_ERR, "%x ", rx_data[i]);
        DPRINTF(DBG_ERR, "que : %d ", kbhit0());
        DPRINTF(DBG_ERR, " \n\r");
    }
    else
#else
    DPRINTF(DBG_MSG1, "%s() - checksum %x %x %x\n", __FUNCTION__, rx_chk, calc_chk, rx_data[PACKET_ID_SIZE]);
#endif
    {
        tmp8          = rx_data[PACKET_ID_SIZE];
        check_msg_cnt = (uint16)rx_data[PACKET_CNT_SIZE] << 8 | rx_data[PACKET_CNT_SIZE + 1];
// DPRINTF(DBG_INFO, "UART SOC->MCU Data [%x] [%x] [%x] [%x] [%x] [%x] [%x] [%x] [%x] [%x]\n\r",rx_data[7], rx_data[8], rx_data[9], rx_data[10], rx_data[11], rx_data[12], rx_data[13], rx_data[14],rx_data[15], rx_data[16]);
// apply to filter about soc msg counter   //nocheck mcu response msg
#if 1
        if ((tmp8 != MSG_ACC_IGN_STATUS) && (tmp8 != MSG_SYS_MODE_REQ) && (tmp8 != MSG_RADIO_STATE) && (tmp8 != MSG_HARDKEY) && (tmp8 != MSG_DTC_CMD) && (tmp8 != MSG_DVR_CONT_CMD) &&
            (tmp8 != MSG_MCU_UPGRADE_START) && (tmp8 != MSG_VIN_CMD))
        {
            if ((pre_check_msg_cnt != check_msg_cnt) || (check_msg_cnt == 0))
            {
                pre_check_msg_cnt = check_msg_cnt;
            }
            else
            {
                // repeat
                DPRINTF(DBG_MSG5_1, "same data check %d %d cnt:%d\n", check_msg_cnt, pre_check_msg_cnt, rx_error_cnt);
                return;
            }
        }
#else
        if ((pre_check_msg_cnt != check_msg_cnt) || (check_msg_cnt == 0))
        {
            pre_check_msg_cnt = check_msg_cnt;
        }
        else
        {
            // repeat
            DPRINTF(DBG_MSG5_1, "same data check %d %d cnt:%d\n", check_msg_cnt, pre_check_msg_cnt, rx_error_cnt);
            return;
        }
#endif
        if (old_switch != tmp8)
        {
            DPRINTF(DBG_MSG3, "SWITCH = %x\n\r", tmp8);
            old_switch = tmp8;
        }

        switch (tmp8)
        {
        case MSG_ACC_IGN_STATUS:
            acc_ign_status.flag   = OFF;
            acc_ign_status.trycnt = 0;
            //			acc_ign_status.timeout = GetTickCount();
            break;

        // SOC偶发重启的情况，导致车况信息初始状态丢失，出现显示不同步的问题
        case MSG_UART_OK:
            EnterSocFlag = TRUE;
            TimerCount   = 0;

            msg.message = MSG_UART_OK;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 5; // 7->5
            msg.buf[0]  = 0x01;
            msg.buf[1]  = 0x01; // DATA Normal 0x1, recover 0x3
            msg.buf[2]  = 0x01; // NC
            msg.buf[3]  = 0x01; // X70:0x0, CX6C:0x1, X90P:0x2, X50:0x3
            msg.buf[4]  = 0x00;
#if 0
			msg.buf[5] =	0x00;
			msg.buf[6] =	0x00;
#endif
            Uart_Response(&msg);

            dev->r_System->alivecleartime = ON; // first signal
            dev->r_System->uart_ok        = ON;

            // reset after soc update
            if (dev->r_System->soc_update == ON)
            {
#ifdef INTERNAL_AMP
                MCU_AMP_MUTE(ON); // SOUND OFF
#endif
                DPRINTF(DBG_ERR, "SystemSoftwareReset	11\n\r");
                e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
                save_last_memory();
                RecordReset(11, NULL);
                delay_ms(100);
                SystemSoftwareReset();
            }

            // dtc system check
            //			dtc_list_cmd.flag = ON;

            // set uart first data

            DPRINTF(DBG_MSG1, "MSG_UART_OK %d reboot %d\n", rx_data[PACKET_ID_SIZE + 2], dev->r_System->soc_reboot);

            // check reboot
            if (dev->r_System->soc_reboot)
            {
                dev->r_System->soc_reboot = OFF;
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_UNMUTE_1, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_UNMUTE_1);
#endif
#ifdef INTERNAL_AMP
                MCU_AMP_MUTE(ON); // SOUND OFF
#endif
                switch (dev->r_System->s_state)
                {
                case SYS_STANDBY_MODE_CHECK:
                case SYS_STANDBY_MODE_ENTER:
                case SYS_STANDBY_MODE_IDLE:
                    if (dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
                        dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
                    break;
                case SYS_ACTIVE_MODE_CHECK:
                case SYS_ACTIVE_MODE_ENTER:
                case SYS_ACTIVE_MODE_IDLE:
                    if (dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
                        dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
                    break;
                case SYS_SCREENOFF_1_MODE_CHECK:
                case SYS_SCREENOFF_1_MODE_ENTER:
                case SYS_SCREENOFF_1_MODE_IDLE:
                    if (dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
                        dev->r_System->s_state = SYS_SCREENOFF_1_MODE_CHECK;
                    break;
                case SYS_SCREEN_SAVE_MODE_CHECK:
                case SYS_SCREEN_SAVE_MODE_ENTER:
                case SYS_SCREEN_SAVE_MODE_IDLE:
                    if (dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
                        dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_CHECK;
                    break;
                case SYS_15MIN_MODE_CHECK:
                case SYS_15MIN_MODE_ENTER:
                case SYS_15MIN_MODE_IDLE:
                    if (dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
                        dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
                    break;
                default:
                    break;
                }
            }

            break;

        case MSG_SYS_MODE_REQ:
            acc_packet.trycnt = 0;
            acc_packet.flag   = OFF;
            //			acc_packet.timeout = GetTickCount();
            DPRINTF(DBG_MSG1, "MSG_SYS_MODE_REQ %d %d %d %d\n", acc_packet.buf[0], acc_packet.buf[1], acc_packet.buf[2], acc_packet.buf[3]);
            //			uart_timer_tick = GetTickCount();
            //			DPRINTF(DBG_MSG1, " MSG_SYS_MODE_REQ %d %d %d\n", kkk ,uart_timer_tick, uart_timer_tick-kkk);
            break;

        case MSG_RDS:
            radio_packet.trycnt = 0;
            radio_packet.flag   = OFF;
            //			radio_packet.timeout = GetTickCount();
            DPRINTF(DBG_MSG1, "MSG_RDS\n");
            break;

        case MSG_SYS_MODE_REV:
            msg.message = MSG_SYS_MODE_REV;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            tmp8        = rx_data[PACKET_HEADER_SIZE];
            msg.buf[0]  = tmp8;
            if ((tmp8 != 1) && (tmp8 != 2) && (tmp8 != 6) && (tmp8 != 7))
            {
                tmp8   = 0;
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];
                if ((tmp8_1 == 1))
                {
                    tmp8 = SYSTEM_MODE_SCREENSAVE;
                }
                tmp8_2 = rx_data[PACKET_HEADER_SIZE + 2];
                if ((tmp8_2 == 1))
                {
                    tmp8 = SYSTEM_MODE_SCREENOFF_1;
                }
            }
            Uart_Response(&msg);

// don't escape low, high voltage
#if 0
			if((dev->r_System->s_state==SYS_BATTERY_LOW_MODE_ENTER)||(dev->r_System->s_state==SYS_BATTERY_LOW_MODE_IDLE)
				||(dev->r_System->s_state==SYS_BATTERY_HIGH_MODE_ENTER)||(dev->r_System->s_state==SYS_BATTERY_HIGH_MODE_IDLE))
#else
            if ((SYS_BATTERY_LOW_MODE_ENTER <= dev->r_System->s_state) && (dev->r_System->s_state <= SYS_BATTERY_16V_OVER_MODE_IDLE))
#endif
            return;

            switch (tmp8)
            {
            case SYSTEM_MODE_STANDBY:
                dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
                break;
            case SYSTEM_MODE_ACTIVE:
                dev->r_System->s_state = SYS_ACTIVE_MODE_ENTER;
                break;
            case SYSTEM_MODE_SCREENSAVE:
                dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_ENTER;
                break;
            case SYSTEM_MODE_SCREENOFF_1:
                dev->r_System->s_state = SYS_SCREENOFF_1_MODE_ENTER;
                break;
            case SYSTEM_MODE_SCREENOFF_2:
                dev->r_System->s_state = SYS_SCREENOFF_2_MODE_ENTER;
                break;
            case SYSTEM_MODE_15MIN:
                dev->r_System->s_state = SYS_15MIN_MODE_ENTER;
                break;
            case SYSTEM_MODE_SLEEP:
                if (dev->r_System->s_state == SYS_SLEEP_MODE_CHECK)
                {
                    Uart_Init_Comm(); // uart buf clear to enter sleep mode
                    dev->r_System->s_state = SYS_SLEEP_MODE_ENTER;
                }
                break;
            default:
                break;
            }
            DPRINTF(DBG_MSG1, "MSG_SYS_MODE_REV %d %d %d\n", rx_data[PACKET_HEADER_SIZE], rx_data[PACKET_HEADER_SIZE + 1], rx_data[PACKET_HEADER_SIZE + 2]);
            break;

        case MSG_CARSIGNAL:
#if 0
			msg.message	= MSG_CARSIGNAL;
			msg.msg_cnt = check_msg_cnt;
			msg.len = 1;
			msg.buf[0] =	rx_data[PACKET_HEADER_SIZE];
			tmp8 =rx_data[PACKET_HEADER_SIZE];//DATA0

			DPRINTF(DBG_MSG1, "MSG_CARSIGNAL ID : %d \n",msg.buf[0]);

			switch(tmp8)
			{
			//tx response
			case CAR_SIG_TX_GEAR_CMD:
				CAR_SIG_GEAR.flag = OFF;
				break;
			case CAR_SIG_TX_CGW_SAM_G_CMD:
				CAR_SIG_STR_ANGLE.flag = OFF;
				break;
			case CAR_SIG_TX_SPEED_CMD:
				CAR_SIG_SPEED.flag = OFF;
				break;
			case CAR_SIG_TX_SEAT_CMD:
				CAR_SIG_SEATSTATUS.flag = OFF;
				break;
			case CAR_SIG_TX_FUELLEVEL_CMD:
				CAR_SIG_FUEL.flag = OFF;
				break;
			case CAR_SIG_TX_DTE_CMD:
				CAR_SIG_DTE.flag = OFF;
				break;
			case CAR_SIG_TX_DVR1_CMD:
				CAR_SIG_DVR1.flag  = OFF;
				break;
			case CAR_SIG_TX_SEATBELT_CMD:
				CAR_SIG_SEATBELT.flag = OFF;
				break;
			case CAR_SIG_TX_CEM_BCM1_CMD:
				CAR_SIG_CEM_BCM1.flag = OFF;
				break;
			case CAR_SIG_TX_CEM_BCM2_CMD:
				CAR_SIG_CEM_BCM2.flag = OFF;
				break;
			case CAR_SIG_TX_CEM_BCM3_CMD:
				CAR_SIG_CEM_BCM3.flag = OFF;
				break;
			case CAR_SIG_TX_CEM_BCM4_CMD:
				CAR_SIG_CEM_BCM4.flag = OFF;
				break;
			case CAR_SIG_TX_ABS_ESP5_CMD:
				CAR_SIG_ABS_ESP5.flag = OFF;
				break;
			case CAR_SIG_TX_PM25_1_CMD:
				CAR_SIG_PM25.flag = OFF;
				break;
			case CAR_SIG_TX_BSD1_CMD:
				CAR_SIG_BSD1.flag = OFF;
				break;
			case CAR_SIG_TX_CEM_RADAR1_CMD:
				CAR_SIG_CEM_RADAR1.flag = OFF;
				break;
			case CAR_SIG_TX_TURNSIGNAL_CMD:
				CAR_SIG_TURNLIGHT.flag  = OFF;
				break;
			case CAR_SIG_TX_ODO_CMD:
				CAR_SIG_ODOMETER.flag = OFF;
				break;
			case CAR_SIG_TX_AVM_APA4_CMD:
				CAR_SIG_AVM_APA4.flag = OFF;
				break;
			case CAR_SIG_TX_AVM_APA2_CMD:
				CAR_SIG_AVM_APA2.flag = OFF;
				break;
			case CAR_SIG_TX_AVM_APA3_CMD:
				CAR_SIG_AVM_APA3.flag = OFF;
				break;
			case CAR_SIG_TX_AVM2_CMD:
				CAR_SIG_AVM2.flag = OFF;
				break;
			case CAR_SIG_TX_FCM2_CMD:
				CAR_SIG_FCM2.flag = OFF;
				break;
			case CAR_SIG_TX_FRM3_CMD:
				CAR_SIG_FRM3.flag = OFF;
				break;
			case CAR_SIG_TX_WPC_CMD:
				CAR_SIG_WPC.flag = OFF;
				break;
			case CAR_SIG_TX_CEM_ALM1_CMD:
				CAR_SIG_CEM_ALM1.flag = OFF;
				break;
			case CAR_SIG_TX_TBOX2_CMD:
				CAR_SIG_TBOX2.flag = OFF;
				break;
			case CAR_SIG_TX_ENGINESPEED_CMD:
				CAR_SIG_ENGINESPEED.flag= OFF;
				break;
			case CAR_SIG_TX_CLT_CMD:
				CAR_SIG_CLT.flag= OFF;
				break;
			case CAR_SIG_TX_ROLLINGCOUNTER_CMD:
				CAR_SIG_ROLLCOUNT.flag= OFF;
				break;
			case CAR_SIG_TX_MFS_HEARTRATE_CMD:
				CAR_SIG_MFS_HEARTRATE.flag = OFF;
				break;
			case CAR_SIG_TX_AFU_CMD:
				CAR_SIG_AFU.flag = OFF;
				break;
			case CAR_SIG_TX_AMP_CMD:
				CAR_SIG_AMP.flag= OFF;
				break;
			case CAR_SIG_TX_CEM_IPM_CMD:
				CAR_SIG_CEM_IPM.flag= OFF;
				break;
			case CAR_SIG_TX_ICM2_CMD:
				CAR_SIG_ICM2.flag= OFF;
				break;
			case CAR_SIG_TX_ICM4_CMD:
				CAR_SIG_ICM4.flag= OFF;
				break;
			case CAR_SIG_TX_CEM_PEPS_CMD:
				CAR_SIG_CEM_PEPS.flag= OFF;
				break;	
			case CAR_SIG_TX_PLG1_CMD:
				CAR_SIG_PLG1.flag= OFF;
				break;
			case CAR_SIG_TX_CGW_EPS_G_CMD:
				CAR_SIG_CGW_EPS_G.flag = OFF;
				break;
			case CAR_SIG_TX_CGW_ABM_G_CMD:
				CAR_SIG_CGW_ABM_G.flag = OFF;
				break;
			default:
				break;
			}
			break;
#endif
        case MSG_VEHICLE_SIG:
            // KHI 20220504
            msg.message = MSG_VEHICLE_SIG;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = rx_data[PACKET_HEADER_SIZE];
            tmp8        = rx_data[PACKET_HEADER_SIZE]; // DATA0

            //			msg.message = MSG_VEHICLE_SIG;
            //			msg.msg_cnt = check_msg_cnt;
            //			msg.state = MCU_RESPONSE;
            //			msg.len = (rx_data[3]-8);
            //			msg.buf = &rx_data[PACKET_HEADER_SIZE];
            // Uart_Response(&msg);
            CAN_Msg_Tx_Set_Process(dev, &rx_data[0], tmp8);
            DPRINTF(DBG_MSG3, "MSG_VEHICLE_SIG %x %x %x %x %x %x %x %x %x %x\n", rx_data[7], rx_data[8], rx_data[9], rx_data[10], rx_data[11], rx_data[12], rx_data[13], rx_data[14], rx_data[15], rx_data[16], rx_data[17]);
            Uart_Response(&msg);
            //		UART_Send_Book(dev, &msg);
            break;

        case MSG_PRESET: // OK
            msg.message = MSG_PRESET;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 6;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            // performance
            tmp16 = (uint16)((rx_data[PACKET_ID_SIZE + 2] << 8) | rx_data[PACKET_ID_SIZE + 3]);
            if (dev->r_Tuner->t_mode == FM_MODE)
            {
                if ((FM_MIN_FREQ <= tmp16) && (tmp16 <= FM_MAX_FREQ))
                    dev->r_Tuner->fm_freq = tmp16;
            }
            else
                dev->r_Tuner->am_freq = tmp16;

            q_en(&tef6686_cmd_q, (uint8)TUNER_TUNE_0);

            DPRINTF(DBG_MSG5_1, "%s() MSG_PRESET %d\n", __FUNCTION__, dev->r_Tuner->fm_freq);
            break;
        case MSG_SEEK:
            msg.message = MSG_SEEK;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 4;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            tmp16 = (uint16)((rx_data[PACKET_HEADER_SIZE + 2] << 8) | rx_data[PACKET_HEADER_SIZE + 3]);
            if (dev->r_Tuner->t_mode == FM_MODE)
            {
                if ((FM_MIN_FREQ <= tmp16) && (tmp16 <= FM_MAX_FREQ))
                    dev->r_Tuner->fm_freq = tmp16;
            }
            else
                dev->r_Tuner->am_freq = tmp16;

            if (RADIO_SEEK_UP == rx_data[PACKET_HEADER_SIZE])
                q_en(&tef6686_cmd_q, (uint8)TUNER_SEEK_PLUS_0);
            else if (RADIO_SEEK_DN == rx_data[PACKET_HEADER_SIZE])
                q_en(&tef6686_cmd_q, (uint8)TUNER_SEEK_MINUS_0);
            else if (RADIO_FUNC_CANCEAL == rx_data[PACKET_HEADER_SIZE])
                q_en(&tef6686_cmd_q, (uint8)TUNER_TUNE_0);

            DPRINTF(DBG_MSG3, "%s() MSG_SEEK\n", __FUNCTION__);
            break;
        case MSG_AST:
            msg.message = MSG_AST;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 4;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            tmp16 = (uint16)((rx_data[PACKET_HEADER_SIZE + 2] << 8) | rx_data[PACKET_HEADER_SIZE + 3]);

            if (dev->r_Tuner->t_mode == FM_MODE)
            {
                if (RADIO_FUNC_CANCEAL == rx_data[PACKET_HEADER_SIZE])
                {
                    if ((FM_MIN_FREQ <= tmp16) && (tmp16 <= FM_MAX_FREQ))
                    {
                        dev->r_Tuner->fm_freq = tmp16;
                        q_en(&tef6686_cmd_q, (uint8)TUNER_TUNE_0);
                    }
                }
                else
                {
                    if ((FM_MIN_FREQ <= tmp16) && (tmp16 <= FM_MAX_FREQ))
                    {
                        dev->r_Tuner->fm_freq = tmp16;
                        q_en(&tef6686_cmd_q, (uint8)TUNER_ATS_0);
                    }
                }
            }
            else
            {
                if (RADIO_FUNC_CANCEAL == rx_data[PACKET_HEADER_SIZE])
                {
                    if ((AM_MIN_FREQ <= tmp16) && (tmp16 <= AM_MAX_FREQ))
                    {
                        dev->r_Tuner->am_freq = tmp16;
                        q_en(&tef6686_cmd_q, (uint8)TUNER_TUNE_0);
                    }
                }
                else
                {
                    if ((AM_MIN_FREQ <= tmp16) && (tmp16 <= AM_MAX_FREQ))
                    {
                        q_en(&tef6686_cmd_q, (uint8)TUNER_ATS_0);
                        dev->r_Tuner->am_freq = tmp16;
                    }
                }
            }
            DPRINTF(DBG_MSG3, "%s() MSG_AST freq: %d valid \n", __FUNCTION__, tmp16, rx_data[PACKET_HEADER_SIZE]);
            break;
        case MSG_BAND:
            msg.message = MSG_BAND;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 5;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            tmp16 = (uint16)((rx_data[PACKET_HEADER_SIZE + 2] << 8) | rx_data[PACKET_HEADER_SIZE + 3]);

#if 1 // ok
            if (rx_data[PACKET_HEADER_SIZE] == 0x01)
            {
                // FM���?
                if ((FM_MIN_FREQ <= tmp16) && (tmp16 <= FM_MAX_FREQ))
                {
                    dev->r_Tuner->fm_freq = tmp16;
                    IlPutTxIHU_RadioFrequanceMode(0x01); // FM_MODE
                    // SrvMcanSendReq(RRM_2_516_IDX);
                    if (dev->r_Tuner->t_mode == FM_MODE)
                        q_en(&tef6686_cmd_q, (uint8)TUNER_TUNE_0);
                    else
                    {
                        q_en(&tef6686_cmd_q, (uint8)TUNER_CHANG_TO_FM_0);
#ifdef SEPERATE_VOLUME_TABLE
#ifdef DSP_DOUBLE_QUE
                        q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                        q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
#endif
#endif
                    }
                    dev->r_Tuner->t_mode = FM_MODE;
                }
#ifdef SEPERATE_VOLUME_TABLE
                if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_FM)
                {
                    if (dev->r_Tuner->t_mode == FM_MODE)
                        mediachtype = CH_TYPE_FM;
                    else
                        mediachtype = CH_TYPE_AM;
                }
#endif
                DPRINTF(DBG_MSG3, "%s() MSG_BAND FM: %d mode %d\n", __FUNCTION__, dev->r_Tuner->fm_freq, dev->r_Tuner->t_mode);
            }
            else if (rx_data[PACKET_HEADER_SIZE] == 0x02)
            { // AM���?
                if ((AM_MIN_FREQ <= tmp16) && (tmp16 <= AM_MAX_FREQ))
                {
                    dev->r_Tuner->am_freq = tmp16;
                    IlPutTxIHU_RadioFrequanceMode(0x02); // AM_MODE
                    // SrvMcanSendReq(RRM_2_516_IDX);
                    if (dev->r_Tuner->t_mode == AM_MODE)
                        q_en(&tef6686_cmd_q, (uint8)TUNER_TUNE_0);
                    else
                    {
                        q_en(&tef6686_cmd_q, (uint8)TUNER_CHANG_TO_AM_0);
#ifdef SEPERATE_VOLUME_TABLE
#ifdef DSP_DOUBLE_QUE
                        q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                        q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
#endif
#endif
                    }
                    dev->r_Tuner->t_mode = AM_MODE;
                }

#ifdef SEPERATE_VOLUME_TABLE
                if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_FM)
                {
                    if (dev->r_Tuner->t_mode == FM_MODE)
                        mediachtype = CH_TYPE_FM;
                    else
                        mediachtype = CH_TYPE_AM;
                }
#endif
                DPRINTF(DBG_MSG3, "%s() MSG_BAND AM:%d  mode %d\n", __FUNCTION__, dev->r_Tuner->am_freq, dev->r_Tuner->t_mode);
            }
#endif
            //			DPRINTF(DBG_MSG3, "%s() MSG_BAND AM:%d FM: %d mode %d\n", __FUNCTION__,dev->r_Tuner->am_freq,dev->r_Tuner->fm_freq,dev->r_Tuner->t_mode );
            break;
        case MSG_AUDIOMODE:
            msg.message = MSG_AUDIOMODE;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            tmp8 = msg.buf[0] = rx_data[PACKET_HEADER_SIZE];
            Uart_Response(&msg);

            switch (tmp8)
            {
            case AUIDO_MODE_MEDIA:
                // NAVI,Media
                dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;

                break;
            case AUIDO_MODE_RADIO:
                // RADIO
                dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_FM;
                if (dev->r_Tuner->t_mode == FM_MODE)
                {
                    IlPutTxIHU_RadioFrequanceMode(0x01);
                    // CanIfSet_gCanIf_RRM_2_AMRadioFrequanceValue_0(0x00);
                    // CanIfSet_gCanIf_RRM_2_AMRadioFrequanceValue_1(0x00);
                }
                else
                {
                    IlPutTxIHU_RadioFrequanceMode(0x02);
                    // CanIfSet_gCanIf_RRM_2_FMRadioFrequanceValue_0(0x00);
                    // CanIfSet_gCanIf_RRM_2_FMRadioFrequanceValue_1(0x00);
                }

                //
                break;
            case AUIDO_MODE_BT_PHONE:
                // BT PHONE
                dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_BT_HFP;

                break;
            case AUIDO_MODE_BCALL:
                // BT CALL
                dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_BCALL;

                break;
            default:

                break;
            }
            //			DPRINTF(DBG_MSG3, "%s() MSG_AUDIO_MODE : current:%d befor:%d receive:%d \r\n", __FUNCTION__,dev->r_Dsp->d_audo_mode,dev->r_Dsp->d_prev_audo_mode,tmp8);

            if (dev->r_Dsp->d_prev_audo_mode != dev->r_Dsp->d_audo_mode)
            {
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_CHANG_MODE_1, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&audio_mode_cmd_q, (uint8)dev->r_Dsp->d_audo_mode);
                q_en(&ak7739_cmd_q, (uint8)DSP_CHANG_MODE_1);
#endif
#ifdef SEPERATE_VOLUME_TABLE
                if ((dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL) || (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA))
                    mediachtype = CH_TYPE_MEDIA;
                else if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_FM)
                {
                    if (dev->r_Tuner->t_mode == FM_MODE)
                        mediachtype = CH_TYPE_FM;
                    else
                        mediachtype = CH_TYPE_AM;
                }
#endif
                DPRINTF(DBG_MSG3, "DSP_MODE_CHANG  source before: %d new:%d \r\n", dev->r_Dsp->d_prev_audo_mode, dev->r_Dsp->d_audo_mode);
                dev->r_Dsp->d_prev_audo_mode   = dev->r_Dsp->d_audo_mode;
                dev->r_Dsp->d_change_audo_mode = ON;
            }
            break;
        case MSG_VOLUME:
            msg.message = MSG_VOLUME;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 2;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

#if 1
            tmp8  = rx_data[PACKET_HEADER_SIZE];     // volume type
            value = rx_data[PACKET_HEADER_SIZE + 1]; // voume level

            switch (tmp8)
            {
            case VOL_TYPE_MEDIA:
                if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA)
                {
                    dev->r_Dsp->d_media_vol     = AK_MEDIA_VOL_DEFAULT;
                    e2p_save_data.E2P_MEDIA_VOL = value;
                }
                else if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL)
                {
                    dev->r_Dsp->d_media_vol     = AK_BCALL_VOL_DEFAULT;
                    e2p_save_data.E2P_MEDIA_VOL = value;
                }
                else
                    e2p_save_data.E2P_MEDIA_VOL = dev->r_Dsp->d_media_vol = value;
#ifdef CX62C_FUNCTION
// CanIfSet_gCanIf_RRM_2_KnobSts(value);
// SrvMcanSendReq(RRM_2_516_IDX);
#endif
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
#endif
#ifdef ENABLE_LOUDNESS_FOR_INTERAMP
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_LOUDNESS, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_LOUDNESS);
#endif
#endif

                break;
            case VOL_TYPE_RADAR:
                e2p_save_data.E2P_RADAR_WARN_VOL = dev->r_Dsp->d_radar_vol = value;
                //					DPRINTF(DBG_MSG1, "%s() VOL_TYPE_RADAR %d \n", __FUNCTION__,dev->r_Dsp->d_radar_vol);
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_RADAR_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_RADAR_VOL);
#endif
                break;
            case VOL_TYPE_INSTRUMENT:
                e2p_save_data.E2P_CLU_WARN_VOL = dev->r_Dsp->d_instrument_vol = value;
                //					DPRINTF(DBG_MSG1, "%s() VOL_TYPE_INSTRUMENT %d \n", __FUNCTION__,dev->r_Dsp->d_instrument_vol);
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_INSTRUMENT_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_INSTRUMENT_VOL);
#endif
                break;
            case VOL_TYPE_KEY_BUTTON: // soc is control
                // 0x0:mute, 0x1: low 0x2: middle 0x3: high
                e2p_save_data.E2P_KEY_VOL = dev->r_Dsp->d_key_vol = value;
                if (value == 0x01)
                    tmp8 = 0x1;
                else if (value == 0x03)
                    tmp8 = 0x2;
                else if (value == 0x00)
                    tmp8 = 0x3;
                // CanIfSet_gCanIf_RRM_10_SET_TouchSound(tmp8);
                // SrvMcanSendReq(RRM_10_53D_IDX);
                //					q_en(&ak7739_cmd_q, (uint8)DSP_KEY_VOL);
                break;
            case VOL_TYPE_PHONE:
                e2p_save_data.E2P_CALL_VOL = dev->r_Dsp->d_phone_vol = value;
                //					CanIfSet_gCanIf_RRM_10_SET_phonevolume(value);
                //					SrvMcanSendReq(RRM_10_53D_IDX);
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_CALL_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_CALL_VOL);
#endif
                break;
            case VOL_TYPE_NAVI: // soc is control
                e2p_save_data.E2P_NAVI_VOL = dev->r_Dsp->d_navi_vol = value;
                // CanIfSet_gCanIf_RRM_10_SET_NaviVolume(value);
                // SrvMcanSendReq(RRM_10_53D_IDX);
                break;
            case VOL_TYPE_VOICE: // soc is control
                e2p_save_data.E2P_VOICE_VOL = dev->r_Dsp->d_voice_vol = value;
                // CanIfSet_gCanIf_RRM_10_SET_TtsVolume(value);
                // SrvMcanSendReq(RRM_10_53D_IDX);
                //					q_en(&ak7739_cmd_q, (uint8)DSP_VOICE_VOL);
                break;
            case VOL_TYPE_BCALL:
                break;
            default:
                break;
            }
#else
            // value
            value                   = rx_data[PACKET_HEADER_SIZE];
            dev->r_Dsp->d_media_vol = main_vol_leve[value];
            dev->r_Dsp->d_state     = DSP_VOL;
#endif
            e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data); // JANG_220112
            Setting_Update           = ON;
            DPRINTF(DBG_MSG3, "%s() MSG_VOLUME  vol_mode:%d level %d audio_mode %d\n", __FUNCTION__, tmp8, value, dev->r_Dsp->d_audo_mode);
            break;
        case MSG_FADE_BAL:
            msg.message = MSG_FADE_BAL;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 2;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            // value
            value = rx_data[PACKET_HEADER_SIZE]; // DATA 0

            if (EQ_CONT_FADER == value)
            {
                e2p_save_data.E2P_EQ_FADER = rx_data[PACKET_HEADER_SIZE + 1];
                e2p_save_data.E2P_CHKSUM   = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_FADER_BALANCE_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_FADER_BALANCE_0);
#endif
            }
            else if (EQ_CONT_BALANCE == value)
            {
                e2p_save_data.E2P_EQ_BALANCE = rx_data[PACKET_HEADER_SIZE + 1];
                e2p_save_data.E2P_CHKSUM     = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_FADER_BALANCE_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_FADER_BALANCE_0);
#endif
            }
            Setting_Update = ON;
            DPRINTF(DBG_MSG3, "%s() MSG_FADE_BAL  type:%d balance,balance:%d\n", __FUNCTION__, value, rx_data[PACKET_HEADER_SIZE + 1]);
            break;
        case MSG_EQ:
            msg.message = MSG_EQ;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 3;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            tmp8 = rx_data[PACKET_HEADER_SIZE];

            switch (tmp8)
            {
            case EQ_MODE_STANDARD:

                e2p_save_data.E2P_EQ_TYPE = 0x0;
                e2p_save_data.E2P_CHKSUM  = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_STANDARD, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_STANDARD);
#endif

                break;
            case EQ_MODE_POP: // POP
                // CanIfSet_gCanIf_RRM_11_SET_SoundMode(EQ_MODE_POP);
                e2p_save_data.E2P_EQ_TYPE = EQ_MODE_POP;
                e2p_save_data.E2P_CHKSUM  = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_POP, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_POP);
#endif

                break;
            case EQ_MODE_ROCK: // ROCK
                // CanIfSet_gCanIf_RRM_11_SET_SoundMode(EQ_MODE_ROCK);
                e2p_save_data.E2P_EQ_TYPE = EQ_MODE_ROCK;
                e2p_save_data.E2P_CHKSUM  = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_ROCK, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_ROCK);
#endif

                break;
            case EQ_MODE_JAZZ: // JAZZ
                // CanIfSet_gCanIf_RRM_11_SET_SoundMode(0x06);
                e2p_save_data.E2P_EQ_TYPE = EQ_MODE_JAZZ;
                e2p_save_data.E2P_CHKSUM  = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_JAZZ, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_JAZZ);
#endif

                break;
            case EQ_MODE_CLASSIC: // CLASSIC
                // CanIfSet_gCanIf_RRM_11_SET_SoundMode(0x04);
                e2p_save_data.E2P_EQ_TYPE = EQ_MODE_CLASSIC;
                e2p_save_data.E2P_CHKSUM  = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_CLASSIC, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_CLASSIC);
#endif

                break;
            case EQ_MODE_HUMAN_VOICE: // SIR
                // CanIfSet_gCanIf_RRM_11_SET_SoundMode(0x05);
                e2p_save_data.E2P_EQ_TYPE = EQ_MODE_HUMAN_VOICE;
                e2p_save_data.E2P_CHKSUM  = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_HUMAN_VOICE, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_HUMAN_VOICE);
#endif

                break;
            case EQ_MODE_CUSTOM: // PERSONAL
                // CanIfSet_gCanIf_RRM_11_SET_SoundMode(0x01);
                e2p_save_data.E2P_EQ_TYPE = EQ_MODE_CUSTOM;
                if (rx_data[PACKET_HEADER_SIZE + 1] == EQ_TYPE_TREBLE)
                {
                    // treble
                    e2p_save_data.E2P_EQ_TREBLE = rx_data[PACKET_HEADER_SIZE + 2];
                    //						DPRINTF(DBG_MSG5_1, " treble %d\n", e2p_save_data.E2P_EQ_TREBLE);
                }
                else if (rx_data[PACKET_HEADER_SIZE + 1] == EQ_TYPE_MID)
                {
                    // mid
                    e2p_save_data.E2P_EQ_MID = rx_data[PACKET_HEADER_SIZE + 2];
                    //						DPRINTF(DBG_MSG5_1, " middle %d\n", e2p_save_data.E2P_EQ_MID);
                }
                else if (rx_data[PACKET_HEADER_SIZE + 1] == EQ_TYPE_BASS)
                {
                    // bass
                    e2p_save_data.E2P_EQ_BASS = rx_data[PACKET_HEADER_SIZE + 2];
                    //						DPRINTF(DBG_MSG5_1, " bass %d\n", e2p_save_data.E2P_EQ_BASS);
                }
                e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data); // JANG_220112
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_CUSTOM_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_EQ_CUSTOM_0);
#endif

                break;
            default:

                break;
            }
            Setting_Update = ON;
            DPRINTF(DBG_MSG3, "%s() E2P_EQ_TYPE %d save:%d\n", __FUNCTION__, tmp8, e2p_save_data.E2P_EQ_TYPE);
            break;
        case MSG_SOFTMUTE:
            msg.message = rx_data[PACKET_ID_SIZE];
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = 0x01;
            Uart_Response(&msg);

            if (rx_data[PACKET_HEADER_SIZE] == 0x01)
            {
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_SOFT_MUTE, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_SOFT_MUTE);
#endif
            }
            else
            {
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_SOFT_UNMUTE, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_SOFT_UNMUTE);
#endif
            }

            //			DPRINTF(DBG_MSG1, "%s() MSG_SOFTMUTE %d\n", __FUNCTION__,rx_data[PACKET_HEADER_SIZE]);
            break;
        case MSG_AMPMUTE:
            msg.message = rx_data[PACKET_ID_SIZE];
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = 0x01;
            Uart_Response(&msg);

            if (rx_data[PACKET_HEADER_SIZE] == 0x01)
            {
#ifdef EXTERNAL_AMP
// CanIfSet_gCanIf_RRM_10_SET_mute(AMP_MUTE);//amp mute
// SrvMcanSendReq(RRM_10_53D_IDX);
#endif
#ifdef INTERNAL_AMP
                MCU_AMP_MUTE(ON); // Audio mute
#endif
            }
            else
            {
#ifdef EXTERNAL_AMP
// CanIfSet_gCanIf_RRM_10_SET_mute(AMP_UNMUTE);//amp unmute
// SrvMcanSendReq(RRM_10_53D_IDX);
#endif
#ifdef INTERNAL_AMP
                MCU_AMP_MUTE(OFF); // Audio Unmute
#endif
            }
            DPRINTF(DBG_MSG1, "%s() MSG_AMPMUTE\n", __FUNCTION__);
            break;
        case MSG_NAVI_VOICE:
            msg.message = MSG_NAVI_VOICE;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = 0x01;
            Uart_Response(&msg);
            DPRINTF(DBG_MSG5_1, "%s() MSG_NAVI_VOICE %d \n  ", __FUNCTION__, rx_data[PACKET_HEADER_SIZE]);
            break;
        case MSG_AUIDO_MIXER:
            msg.message = MSG_AUIDO_MIXER;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 3;
            for (i = 0; i < msg.len; i++)
                msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];
            Uart_Response(&msg);

            tmp8   = rx_data[PACKET_HEADER_SIZE];
            tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];
            tmp8_2 = rx_data[PACKET_HEADER_SIZE + 2];

#if 0		
			if((dev->r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)||(dev->r_System->s_state == SYS_STANDBY_MODE_IDLE))
				return;
#endif
            switch (tmp8)
            {
            case MIX_AUIDO_SRC_MEDIA:
                if (tmp8_1 == MIX_AUIDO_SRC_NAVI)
                {
#ifdef DSP_DOUBLE_QUE
                    q2_en(&ak7739_cmd2_q, (uint8)DSP_MIXER_ENTER_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                    q_en(&ak7739_cmd_q, (uint8)DSP_MIXER_ENTER_0);
#endif
                }
                else if (tmp8_1 == MIX_AUIDO_SRC_IDLE)
                {
#ifdef DSP_DOUBLE_QUE
                    q2_en(&ak7739_cmd2_q, (uint8)DSP_MIXER_EXIT_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                    q_en(&ak7739_cmd_q, (uint8)DSP_MIXER_EXIT_0);
#endif
                }
                break;
            case MIX_AUIDO_SRC_RADIO:
                if (tmp8_1 == MIX_AUIDO_SRC_NAVI)
                {
#ifdef DSP_DOUBLE_QUE
                    q2_en(&ak7739_cmd2_q, (uint8)DSP_MIXER_ENTER_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                    q_en(&ak7739_cmd_q, (uint8)DSP_MIXER_ENTER_0);
#endif
                }
                else if (tmp8_1 == MIX_AUIDO_SRC_IDLE)
                {
#ifdef DSP_DOUBLE_QUE
                    q2_en(&ak7739_cmd2_q, (uint8)DSP_MIXER_EXIT_0, (uint8)dev->r_Dsp->d_audo_mode);
#else
                    q_en(&ak7739_cmd_q, (uint8)DSP_MIXER_EXIT_0);
#endif
                }
                break;
            case MIX_AUIDO_SRC_NAVI:
                if (tmp8_1 == MIX_AUIDO_SRC_RADIO)
                {
#ifdef DSP_DOUBLE_QUE
                    q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                    q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
#endif
                }
                else if (tmp8_1 == MIX_AUIDO_SRC_MEDIA)
                {
#ifdef DSP_DOUBLE_QUE
                    q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
                    q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
#endif
                }
                break;
            case MIX_AUIDO_SRC_PHONE:
            case MIX_AUIDO_SRC_BCALL:
                break;
            }
            DPRINTF(DBG_MSG5, "%s() MSG_AUIDO_MIXER %d %d %d\n", __FUNCTION__, tmp8, tmp8_1, tmp8_2);
            break;
        case MSG_EXTERNAL_AMP:
            msg.message = MSG_EXTERNAL_AMP;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = rx_data[PACKET_HEADER_SIZE];
            Uart_Response(&msg);

            tmp8 = rx_data[PACKET_HEADER_SIZE];

            switch (tmp8)
            {
            case SET_MUSICLOUDNESS:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];
                IlPutTxIHU_MusicLoudness_120HZ(tmp8_1 >> 4);
                IlPutTxIHU_MusicLoudness_250HZ(tmp8_1 & 0x0F);
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 2];
                IlPutTxIHU_MusicLoudness_500HZ(tmp8_1 >> 4);
                IlPutTxIHU_MusicLoudness_1000HZ(tmp8_1 & 0x0F);
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 3];
                IlPutTxIHU_MusicLoudness_1500HZ(tmp8_1 >> 4);
                IlPutTxIHU_MusicLoudness_2000HZ(tmp8_1 & 0x0F);
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 4];
                IlPutTxIHU_MusicLoudness_6000HZ(tmp8_1 & 0x0F);

                DPRINTF(DBG_MSG1, "SET_MUSICLOUDNESS 1:%x 2:%x 3:%x 4:%x\n", rx_data[PACKET_HEADER_SIZE + 1], rx_data[PACKET_HEADER_SIZE + 2],
                        rx_data[PACKET_HEADER_SIZE + 3], rx_data[PACKET_HEADER_SIZE + 4]);
                break;
            case SET_SVDC:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];
                // CanIfSet_gCanIf_RRM_10_SET_SVDC(tmp8_1);
                e2p_save_data.E2P_SVCD   = tmp8_1;
                e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data); // JANG_220112
                // SrvMcanSendReq(RRM_10_53D_IDX);
                //					DPRINTF(DBG_MSG1, "SET_SVDC %x \n", tmp8_1);
                break;
            case SET_ACOUSTIC_FIELD:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];
                // CanIfSet_gCanIf_RRM_11_SET_Acousticfieldmodel(tmp8_1);
                e2p_save_data.E2P_SOUND_FEILD_MODE = tmp8_1;
                e2p_save_data.E2P_CHKSUM           = checksum_setup_value(&e2p_save_data); // JANG_220112
                // SrvMcanSendReq(RRM_11_55D_IDX);
                //					DPRINTF(DBG_MSG1, "SET_ACOUSTIC_FIELD %x \n", tmp8_1);
                break;
            case SET_SOUND_WAVE_MODE:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];
                // CanIfSet_gCanIf_RRM_11_SET_SoundWaveMode(tmp8_1);
                e2p_save_data.E2P_SOUND_WAVE_MODE = tmp8_1;
                e2p_save_data.E2P_CHKSUM          = checksum_setup_value(&e2p_save_data); // JANG_220112
                // SrvMcanSendReq(RRM_11_55D_IDX);
                //					DPRINTF(DBG_MSG1, "SET_SOUND_WAVE_MODE %x \n", tmp8_1);
                break;
            case SET_DEFAULT_MAXVOL:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];

                e2p_save_data.E2P_DEFAULTMAX_VOL = tmp8_1;
                e2p_save_data.E2P_CHKSUM         = checksum_setup_value(&e2p_save_data); // JANG_220112
                //					DPRINTF(DBG_MSG1, "SET_DEFAULT_MAXVOL %x \n", tmp8_1);
                break;
            case SET_LOUDNESS:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];

                e2p_save_data.E2P_LONDNESS_ONOFF = tmp8_1;
                e2p_save_data.E2P_CHKSUM         = checksum_setup_value(&e2p_save_data); // JANG_220112

#ifdef ENABLE_LOUDNESS_FOR_INTERAMP
#ifdef INTERNAL_AMP
#ifdef DSP_DOUBLE_QUE
                q2_en(&ak7739_cmd2_q, (uint8)DSP_LOUDNESS, (uint8)dev->r_Dsp->d_audo_mode);
#else
                q_en(&ak7739_cmd_q, (uint8)DSP_LOUDNESS);
#endif
#endif
#endif
                //					DPRINTF(DBG_MSG1, "SET_LOUDNESS %x \n", tmp8_1);
                break;
            case SET_SURROUND_SOUND:
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];

                e2p_save_data.E2P_SURROUND_SOUND = tmp8_1;
                e2p_save_data.E2P_CHKSUM         = checksum_setup_value(&e2p_save_data); // JANG_220112

                //					DPRINTF(DBG_MSG1, "SET_SURROUND_SOUND %x \n", tmp8_1);
                break;
            default:
                break;
            }
            Setting_Update = ON;
            break;
        case MSG_AUDIO_FACTORY_RESET:
            msg.message = MSG_AUDIO_FACTORY_RESET;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 9;

            // default value
            e2p_save_data.E2P_SVCD             = 3;
            e2p_save_data.E2P_LONDNESS_ONOFF   = 1;
            e2p_save_data.E2P_SURROUND_SOUND   = 1;
            e2p_save_data.E2P_SOUND_FEILD_MODE = 2;
            e2p_save_data.E2P_SOUND_WAVE_MODE  = 1;
            e2p_save_data.E2P_EQ_TYPE          = EQ_MODE_CUSTOM;
            e2p_save_data.E2P_EQ_TREBLE        = 7;
            e2p_save_data.E2P_EQ_MID           = 7;
            e2p_save_data.E2P_EQ_BASS          = 7;
            e2p_save_data.E2P_EQ_FADER         = 7;
            e2p_save_data.E2P_EQ_BALANCE       = 7;
            e2p_save_data.E2P_KEY_VOL          = 1;
            e2p_save_data.E2P_VOICE_VOL        = 5;
            e2p_save_data.E2P_RADAR_WARN_VOL = dev->r_Dsp->d_radar_vol = 2;
            e2p_save_data.E2P_CLU_WARN_VOL = dev->r_Dsp->d_instrument_vol = 2;
            e2p_save_data.E2P_KEY_VOL                                     = 0x1;
#if 0
			e2p_save_data.E2P_MEDIA_VOL= 10;
#else
            if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA)
            {
                dev->r_Dsp->d_media_vol     = AK_MEDIA_VOL_DEFAULT;
                e2p_save_data.E2P_MEDIA_VOL = 10;
            }
            else if (dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL)
            {
                dev->r_Dsp->d_media_vol     = AK_BCALL_VOL_DEFAULT;
                e2p_save_data.E2P_MEDIA_VOL = 10;
            }
            else
                e2p_save_data.E2P_MEDIA_VOL = dev->r_Dsp->d_media_vol = 10;
#endif
            e2p_save_data.E2P_CALL_VOL = dev->r_Dsp->d_phone_vol = 10;
            e2p_save_data.E2P_NAVI_VOL                           = 5; // default 10->20
            e2p_save_data.E2P_DEFAULTMAX_VOL                     = 10;
            // keep original value of cluster setting
            // e2p_save_data.E2P_CLS_BRIGHTNESS = 10;
            // e2p_save_data.E2P_CLS_DRIVINGTIME = 0x0F;
            // e2p_save_data.E2P_CLS_OVERSPD = 0x3F;
            e2p_save_data.E2P_PSNGRAIRBAG_ONOFF = 2;

            e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data); // JANG_220112
            Setting_Update           = ON;

            msg.buf[0] = ((e2p_save_data.E2P_SVCD << 4) | (e2p_save_data.E2P_LONDNESS_ONOFF << 2) | e2p_save_data.E2P_SURROUND_SOUND);
            msg.buf[1] = ((e2p_save_data.E2P_SOUND_WAVE_MODE << 6) | (e2p_save_data.E2P_SOUND_FEILD_MODE << 3) | e2p_save_data.E2P_EQ_TYPE);
            msg.buf[2] = ((e2p_save_data.E2P_EQ_FADER << 4) | e2p_save_data.E2P_EQ_BALANCE);
            msg.buf[3] = ((e2p_save_data.E2P_EQ_TREBLE << 4) | e2p_save_data.E2P_EQ_MID);
            msg.buf[4] = ((e2p_save_data.E2P_EQ_BASS << 4) | e2p_save_data.E2P_VOICE_VOL);
            msg.buf[5] = ((e2p_save_data.E2P_RADAR_WARN_VOL << 4) | e2p_save_data.E2P_MEDIA_VOL);
            msg.buf[6] = ((e2p_save_data.E2P_CLU_WARN_VOL << 4) | e2p_save_data.E2P_CALL_VOL);
            msg.buf[7] = ((e2p_save_data.E2P_KEY_VOL << 4) | e2p_save_data.E2P_NAVI_VOL);
            msg.buf[8] = (e2p_save_data.E2P_DEFAULTMAX_VOL);

#if 1
            dev->r_Dsp->d_softmute_status = OFF;
            dev->r_Dsp->d_mix_status      = OFF;
#endif
            Uart_Response(&msg);
#ifdef EXTERNAL_AMP

#else
#ifdef DSP_DOUBLE_QUE
            q2_en(&ak7739_cmd2_q, (uint8)DSP_EQ_CUSTOM_0, (uint8)dev->r_Dsp->d_audo_mode);
            q2_en(&ak7739_cmd2_q, (uint8)DSP_FADER_BALANCE_0, (uint8)dev->r_Dsp->d_audo_mode);
            q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL, (uint8)dev->r_Dsp->d_audo_mode);
            q2_en(&ak7739_cmd2_q, (uint8)DSP_RADAR_VOL, (uint8)dev->r_Dsp->d_audo_mode);
            q2_en(&ak7739_cmd2_q, (uint8)DSP_INSTRUMENT_VOL, (uint8)dev->r_Dsp->d_audo_mode);
            q2_en(&ak7739_cmd2_q, (uint8)DSP_CALL_VOL, (uint8)dev->r_Dsp->d_audo_mode);
#else
            q_en(&ak7739_cmd_q, (uint8)DSP_EQ_CUSTOM_0);     // EQ
            q_en(&ak7739_cmd_q, (uint8)DSP_FADER_BALANCE_0); // FADER/BALANCE
            q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
            q_en(&ak7739_cmd_q, (uint8)DSP_RADAR_VOL);
            q_en(&ak7739_cmd_q, (uint8)DSP_INSTRUMENT_VOL);
            q_en(&ak7739_cmd_q, (uint8)DSP_CALL_VOL);
#endif
#endif
            DPRINTF(DBG_MSG3, "MSG_AUDIO_FACTORY_RESET:%x %x %x %x %x %x %x %x  %x \n", msg.buf[0], msg.buf[1], msg.buf[2], msg.buf[3], msg.buf[4],
                    msg.buf[5], msg.buf[6], msg.buf[7], msg.buf[8]);
            break;
        case MSG_ASTLIST_REQ:
            msg.message = MSG_ASTLIST_REQ;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = (dev->r_Tuner->ast_cnt * 4) + 2;
            msg.buf[0]  = rx_data[PACKET_HEADER_SIZE]; // Received
            msg.buf[1]  = rx_data[PACKET_HEADER_SIZE + 1];

            for (i = 0; i < dev->r_Tuner->ast_cnt; i++)
            {
                msg.buf[2 + (i * 4)] = i + 1; // preset
                tmp16                = dev->r_Tuner->ast_list[i];
                msg.buf[3 + (i * 4)] = 0x00;                  //
                msg.buf[4 + (i * 4)] = (uint8)(tmp16 >> 8);   //
                msg.buf[5 + (i * 4)] = (uint8)(tmp16 & 0xFF); //
            }
            Uart_Response(&msg);

            for (i = 0; i < msg.len; i++)
                DPRINTF(DBG_MSG5_1, " %x ", msg.buf[i]);
            DPRINTF(DBG_MSG5_1, "%s() MSG_ASTLIST_REQ\n", __FUNCTION__);
            break;

        case MSG_ENCODER: //设置CAN方控工作模式
            msg.message = MSG_ENCODER;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = rx_data[PACKET_HEADER_SIZE];
            Uart_Response(&msg);

            tmp8 = rx_data[PACKET_HEADER_SIZE];
            if (tmp8 == 0x02)
            {
                CanKeyWorkMode = 0x02; // CAN方控处于后视镜调节模式
            }
            else if (tmp8 == 0x05)
            {
                CanKeyWorkMode = 0x05; // CAN方控处于巡航模式
            }
            else
            {
                CanKeyWorkMode = 0x00; // CAN方控处于默认模式
            }
            break;

        case MSG_TEST_RADIO_SIG:
            fm_check_radio_packet.message = MSG_TEST_RADIO_SIG;
            fm_check_radio_packet.msg_cnt = check_msg_cnt;
            fm_check_radio_packet.len = msg.len = 4;
            for (i = 0; i < msg.len; i++)
                fm_check_radio_packet.buf[i] = msg.buf[i] = rx_data[PACKET_HEADER_SIZE + i];

            // performance
            tmp16 = (uint16)((rx_data[PACKET_HEADER_SIZE + 2] << 8) | rx_data[PACKET_HEADER_SIZE + 3]);
            if (dev->r_Tuner->t_mode == FM_MODE)
            {
                if ((FM_MIN_FREQ <= tmp16) && (tmp16 <= FM_MAX_FREQ))
                    dev->r_Tuner->fm_freq = tmp16;
            }
            else
                dev->r_Tuner->am_freq = tmp16;
            q_en(&tef6686_cmd_q, (uint8)TUNER_CHECK_SIGNAL_0);
            //			DPRINTF(DBG_MSG5_1, "%s() SIGNAL CHECK %d\n", __FUNCTION__,tmp16);
            break;

        case MSG_ENGINEER_MODE: // engineer mode
            msg.message = MSG_ENGINEER_MODE;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = 0x01;
            Uart_Response(&msg);

            tmp8   = rx_data[PACKET_HEADER_SIZE]; // check mode
            tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1];

            DPRINTF(DBG_MSG3, "%s() MSG_ENGINEER_MODE %x %x\n", __FUNCTION__, tmp8, tmp8_1);

            switch (tmp8)
            {
            case ENGINEER_MODE_SYSTEM:
                if (tmp8_1 & 0x40)
                {
                    // DOOR OPEN
                }
                else
                {
                }

                if (tmp8_1 & 0x80)
                {
                    // SYSTEM TEST MODE
                }
                else
                {
                }
                break;
            case ENGINEER_MODE_AUDIO:
                // audio
                // main volume offset
                if (100 < tmp8_1)
                    tmp8_1 = 100;
                // loudness offset
                tmp8_2 = rx_data[PACKET_HEADER_SIZE + 2];
                if (100 < tmp8_2)
                    tmp8_2 = 100;
                break;
            case ENGINEER_MODE_RADIO:
                // radio debug on/off
                // radio antenna
                // radio audio Flat
                if (tmp8_1 & 0x80)
                    debug_on = ON;
                else
                    debug_on = OFF;
                break;
            case ENGINEER_MODE_RADIO_LEVEL:
                // Level detector
                tmp8            = rx_data[PACKET_HEADER_SIZE + 1];
                check_ast_level = (tmp8 * 10);
                break;
            case ENGINEER_MODE_RADIO_FM_MULTIPATH:
                // FM multipath detector(wam)
                tmp8          = rx_data[PACKET_HEADER_SIZE + 1];
                check_ast_wan = (tmp8 * 10);
                break;
            case ENGINEER_MODE_RADIO_FM_USN:
                // FM Adjust cannel detector(usn)
                tmp8          = rx_data[PACKET_HEADER_SIZE + 1];
                check_ast_usn = (tmp8 * 10);
                break;
            case ENGINEER_MODE_RADIO_FM_OFFSET:
                // Offset
                tmp8             = rx_data[PACKET_HEADER_SIZE + 1];
                check_ast_offset = (tmp8 * 10);
                break;
            case ENGINEER_MODE_RADIO_FM_BANDWIDTH:
                // Bandwidth
                tmp8               = rx_data[PACKET_HEADER_SIZE + 1];
                set_bandwith_value = (tmp8 * 10 + 560);
                break;
            case ENGINEER_MODE_RADIO_FM_AGC:
                // AGC
                tmp8 = rx_data[PACKET_HEADER_SIZE + 1];
                FM_Set_RFAGC((840 + tmp8 * 10), 0);
                break;
            case ENGINEER_MODE_RADIO_FM_HIGHCUT_TIME:
                // RADIO HIGHCUT TIME
                tmp8   = rx_data[PACKET_HEADER_SIZE + 2];
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 4];
                FM_Set_HighCut_Time((rx_data[PACKET_HEADER_SIZE + 1] * 10 + 60), (tmp8 * 100 + 120), (rx_data[PACKET_HEADER_SIZE + 3] * 10 + 10), (tmp8_1 * 100 + 20));
                break;
            case ENGINEER_MODE_RADIO_FM_HIGHCUT_LEVEL:
                // RADIO HIGHCUT LEVEL
                tmp8   = rx_data[PACKET_HEADER_SIZE + 1];
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 2];
                FM_Set_HighCut_Level(3, (tmp8 * 10 + 200), (tmp8_1 * 10 + 60)); // set FM
                break;
            case ENGINEER_MODE_RADIO_FM_HIGHCUT_NOISE:
                // RADIO HIGHCUT NOISE
                tmp8   = rx_data[PACKET_HEADER_SIZE + 1];
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 2];
                FM_Set_HighCut_Noise(2, (tmp8 * 10), (tmp8_1 * 10 + 100));
                break;
            case ENGINEER_MODE_RADIO_FM_HIGHCUT_MPH:
                // RADIO HIGHCUT MPH
                tmp8   = rx_data[PACKET_HEADER_SIZE + 1];
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 2];
                FM_Set_HighCut_Mph(2, (tmp8 * 10), (tmp8_1 * 10 + 100));
                break;
            case ENGINEER_MODE_RADIO_FM_HIGHCUT_MAX:
                // RADIO HIGHCUT MAX
                tmp8   = rx_data[PACKET_HEADER_SIZE + 1];
                tmp8_1 = rx_data[PACKET_HEADER_SIZE + 2];
                FM_Set_HighCut_Max(tmp8, (tmp8_1 * 100));
                break;
            case ENGINEER_MODE_AGING:
                // start 1, end 2
                if (tmp8_1 == 1)
                    dev->r_System->eng_mode_on = ON;
                else if (tmp8_1 == 2)
                    dev->r_System->eng_mode_on = OFF;
                break;
            }
            break;

        case MSG_LAST_READ:
        {
            uint8 tmp[50] = {
                0,
            }; // 100->50

            // engineer mode
            Last_Memory.message = MSG_LAST_READ;
            Last_Memory.msg_cnt = check_msg_cnt;
            Last_Memory.len     = (uint16)((rx_data[PACKET_HEADER_SIZE + 2] << 8) | rx_data[PACKET_HEADER_SIZE + 3]);
            Last_Memory.buf[0]  = rx_data[PACKET_HEADER_SIZE];                              // addr
            Last_Memory.buf[1]  = rx_data[PACKET_HEADER_SIZE + 1];                          // addr
            Last_Memory.buf[2]  = rx_data[PACKET_HEADER_SIZE + 2];                          // length
            Last_Memory.buf[3]  = rx_data[PACKET_HEADER_SIZE + 3];                          // length
            tmp16               = (uint16)((Last_Memory.buf[0] << 8) | Last_Memory.buf[1]); // addr

            //			DPRINTF(DBG_MSG1,"%s MSG_LAST_READ %d %d %d\n\r",__FUNCTION__,e2p_size,(Last_Memory.len+tmp16-1),rx_data[3]);

            // no response
            if ((e2p_size < (Last_Memory.len + tmp16 - 1)) || (rx_data[3] != 0x0c))
                return;

            memcpy(&tmp[0], &e2p_save_data, sizeof(DATA_SAVE));

            if ((Last_Memory.len + tmp16 - 1) <= e2p_size)
            {
                // valid
                memcpy(&Last_Memory.buf[4], &tmp[tmp16], Last_Memory.len);
                DPRINTF(DBG_MSG1, " MSG_LAST_READ SEND %d\n\r", Last_Memory.len);
                dev->r_SendCPU->send_last_memory = ON;
            }
        }
        break;

        case MSG_BRIGHTNESS:
            msg.message = MSG_BRIGHTNESS;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 2;
            tmp8 = msg.buf[0] = rx_data[PACKET_HEADER_SIZE];
            tmp8_1 = msg.buf[1] = rx_data[PACKET_HEADER_SIZE + 1];

            dev->r_System->lcd_ok = ON;

            Uart_Response(&msg);
            //			DPRINTF(DBG_MSG3, "lcd received data : data0:%d  data1:%d %d\n",   tmp8,tmp8_1);

            // no response for lcd control
            //			if(dev->r_System->lcd_control.onoff == ON)
            //				return;
#if 0 // JANG_211203_1 ignore
			if(tmp8==0)
			{
				//JANG_211203
#if 1
				Control_LCD_Backlight_for_Mengbo(dev,OFF);
				return;
#else
				MCU_LCD1_2_EN_3P3(ON);//off
#endif
			}
			else
				MCU_LCD1_2_EN_3P3(OFF);
#endif
#if 0
			if(10<e2p_save_data.E2P_LCD_BRIGHTNESS)
				e2p_save_data.E2P_LCD_BRIGHTNESS = 10;
			else
				e2p_save_data.E2P_LCD_BRIGHTNESS = tmp8_1;

			tmp8 = Set_LCD_Brightness(e2p_save_data.E2P_LCD_BRIGHTNESS);
			FTM3_PWM_Duty(tmp8);	//duty
#else
            //			DPRINTF(DBG_MSG3, "MSG_BRIGHTNESS1 %d  %d\n", e2p_save_data.E2P_LCD_BRIGHTNESS,tmp8_1);
            if ((e2p_save_data.E2P_LCD_BRIGHTNESS == tmp8_1) || (10 < tmp8_1))
            {
                // don't excute
                DPRINTF(DBG_MSG3, "don't excute lcd  MSG_BRIGHTNESS\n");
            }
            else
            {
                tmp8   = Set_LCD_Brightness(tmp8_1);
                tmp8_2 = Set_LCD_Brightness(e2p_save_data.E2P_LCD_BRIGHTNESS);

                //				dev->r_System->lcd_control.enable = ON;
                dev->r_System->lcd_control.rev_ctl   = ON; // JANG_210901_1
                dev->r_System->lcd_control.gotovalue = Set_BL_Duty_Step(tmp8_2, tmp8);
                if (tmp8_2 < tmp8)
                    dev->r_System->lcd_control.direct = 1;
                else
                    dev->r_System->lcd_control.direct = 0;
                dev->r_System->lcd_control.startvalue = tmp8_2;
                dev->r_System->lcd_control.counter    = 0;
                e2p_save_data.E2P_LCD_BRIGHTNESS      = tmp8_1;
                e2p_save_data.E2P_CHKSUM              = checksum_setup_value(&e2p_save_data); // JANG_220112
                DPRINTF(DBG_MSG3, "%s() MSG_BRIGHTNESS2 before:%d  after:%d\n", __FUNCTION__, tmp8_2, tmp8);
            }
#endif
            break;

        case MSG_CPU_ALIVE_ENABLE:
            msg.message = MSG_CPU_ALIVE_ENABLE;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0] = tmp8 = rx_data[PACKET_HEADER_SIZE];
            if (tmp8 == ON)
            {
                dev->r_System->alivesignal = ON;
            }
            else
            {
                u8SOCAlive                 = 1;
                dev->r_System->alivesignal = OFF;
            }
            Uart_Response(&msg);
            DPRINTF(DBG_MSG3, "cpu alive %d\r\n", tmp8);
            break;

        case MSG_CPU_ALIVE:
            dev->r_System->alivecleartime = ON;
            u8SOCAlive                    = 1;
            break;

        case MSG_CPU_UPGRADE:
            tmp8 = rx_data[PACKET_HEADER_SIZE];
            if (tmp8 == 0x80)
            {
#ifdef INTERNAL_AMP
                MCU_AMP_MUTE(ON); // SOUND OFF
#endif
                DPRINTF(DBG_ERR, "MSG_CPU_UPGRADE REBOOT 0x80! \n\r");
                delay_ms(100);
                SystemSoftwareReset();
            }
            else if (tmp8 == 0x01)
            {
#ifdef INTERNAL_AMP
                MCU_AMP_MUTE(ON); // SOUND OFF
#endif
#if 1
                e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
                flash_write_func(FLASH_DATA0_ADDR, (const unsigned char *)&e2p_save_data, sizeof(DATA_SAVE));
                flash_write_func(FLASH_DATA1_ADDR, (const unsigned char *)&e2p_sys_data, sizeof(E2P_SYSTEM));
                delay_ms(100);
#ifdef WATCHDOG_ENABLE
                WDOG_DRV_Trigger(INST_WATCHDOG1);
#endif
#endif
                // enter upgrade
                Self_BootSectorStat.AppStatus     = APP_UPGRADE1;
                Self_BootSectorStat.BootSectorNum = BOOT_MARK_PREV;

                DPRINTF(DBG_ERR, "MSG_CPU_UPGRADE REBOOT 0x01! \n\r");

                /* �����Ҷ� ���� 5ȸ ��õ�? */
                tmp8 = 5;
                while (tmp8--)
                {
                    if (BootStatusFlashBlock_writeStatus(&Self_BootSectorStat) == STATUS_SUCCESS)
                    {
                        DPRINTF(DBG_MSG1, "FLASH WRITE OK \n\r");
                        break;
                    }
                }

                BootStatusFlashBlock_init();
                BootStatusFlashBlock_getStatus(&Self_BootSectorStat);

                DPRINTF(DBG_MSG1, "read vale : %x %x \n\r", Self_BootSectorStat.AppStatus, Self_BootSectorStat.BootSectorNum);
                dev->r_System->mcu_update = ON;
            }
            break;
        case MSG_DTC_INFORM_CMD:
            msg.message = MSG_DTC_INFORM_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            tmp8 = msg.buf[0] = rx_data[PACKET_HEADER_SIZE];
            Uart_Response(&msg);
            tmp8 = rx_data[PACKET_HEADER_SIZE];
            DPRINTF(DBG_MSG3, "MSG_DTC_INFORM_CMD : %d ", tmp8);
            //			MntFltDtcCpuSts(tmp8);
            break;
        case MSG_DTC_CMD:
            dtc_list_cmd.trycnt = 0;
            dtc_list_cmd.flag   = OFF;
            //			dtc_list_cmd.timeout = GetTickCount();
            break;
        case MSG_VIN_CMD:
            msg.message = MSG_VIN_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 17;
            DPRINTF(DBG_MSG3, "MSG_VIN_CMD :  ");
            for (i = 0; i < msg.len; i++)
            {
                msg.buf[i] = u8Vin[i];
                DPRINTF(DBG_MSG3, "%x ", u8Vin[i]);
            }
            DPRINTF(DBG_MSG3, " \n\r");
            Uart_Response(&msg);
            break;
        case MSG_SOFTCONFIG_CMD:
            msg.message = MSG_SOFTCONFIG_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 32u;
            DPRINTF(DBG_MSG3, "MSG_SOFTCONFIG_CMD :  ");
            for (i = 0; i < msg.len; i++)
            {
                if (i >= 8u)
                {
                    msg.buf[i] = softwareConfiguration[i];
                }
                else
                {
                    msg.buf[i] = u8VariantCoding[i];
                }
                DPRINTF(DBG_MSG3, "%x ", msg.buf[i]);
            }
            DPRINTF(DBG_MSG3, " \n\r");
            Uart_Response(&msg);
            break;
        case MSG_MUSIC_KEY_CMD:
            msg.message                         = MSG_MUSIC_KEY_CMD;
            msg.msg_cnt                         = check_msg_cnt;
            msg.len                             = 1;
            dev->r_Dsp->d_musical_rhythm_enable = msg.buf[0] = rx_data[PACKET_HEADER_SIZE];
            Uart_Response(&msg);
            DPRINTF(DBG_MSG5_1, "MSG_MUSIC_KEY_CMD :%d  \n\r", dev->r_Dsp->d_musical_rhythm_enable);
            break;
        case MSG_DVR_CONT_CMD:
            dvr_ctl_cmd.trycnt = 0;
            dvr_ctl_cmd.flag   = OFF;
            //			dvr_ctl_cmd.timeout = GetTickCount();
            break;
        case MSG_MANUFACTURE_CMD:
            tmp8 = rx_data[PACKET_HEADER_SIZE];
// check model version
#ifdef CX62C_FUNCTION
            if (tmp8 & 0x01)
                manufacture_maxim = ON;
            else
                manufacture_maxim = OFF;
#endif
            msg.message = MSG_MANUFACTURE_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 2;
            msg.buf[0]  = tmp8;
            msg.buf[1]  = 0x2; // sony effect on, internal amp
            Uart_Response(&msg);
            break;
#ifdef CX62C_FUNCTION
        case MSG_IVI_INFO_CMD:
            tmp8   = rx_data[PACKET_HEADER_SIZE];
            tmp8_1 = rx_data[PACKET_HEADER_SIZE + 1]; // length
            // response
            msg.message = MSG_IVI_INFO_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = tmp8;
            Uart_Response(&msg);

            // cluster do communiction after icm function value is received from cluster
            if (DVD_ICM_ONOFF != ON)
                return;

            switch (tmp8)
            {
            case RRM_DISPLAY_CEAR_INFO_CMD: // 1 byte clear
                displayclear_onoff = ON;
                gCanTpState        = TP_STATE_EVENT;
                break;
            case RRM_DISPLAY_ROAD_NAME_CMD: // n byte road name
// clear
#if 1 // JANG_211112
                Road_Name.len = tmp8_1;
                memcpy(&Road_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                if (check_msg_cnt_road_name != check_msg_cnt)
                {
                    Road_Name.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_ROAD_NAME_CMD);
                }
                check_msg_cnt_road_name = check_msg_cnt;
#else
                Road_Name.len = tmp8_1;
                memcpy(&Road_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                tmp8 = memcmp(&prev_road_name_buf[0], &Road_Name.buf[0], Road_Name.len); // JANG_210811
                if ((tmp8) || (1500 <= GetElapsedTime(tp_road_timer_tick)))
                {
                    //						DPRINTF(DBG_MSG3, "%s() RRM_DISPLAY_ROAD_NAME_CMD len %d\n ", __FUNCTION__, tmp8_1);
                    memcpy(&prev_road_name_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_ROAD_NAME_CMD);
                    Road_Name.cnt      = 0;
                    tp_road_timer_tick = GetTickCount();
                }
#endif
                break;
            case RRM_DISPLAY_NAVI_INFO_CMD: // 5 byte navi information
#if 1                                       // JANG_211112
                Navi_Infomation.len = 5;    // tmp8_1;
                memcpy(&Navi_Infomation.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], Navi_Infomation.len);
                if (check_msg_cnt_navi_info != check_msg_cnt)
                {
                    memcpy(&prev_navi_info_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], Navi_Infomation.len);
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_NAVI_INFO_CMD);
                    Navi_Infomation.cnt = 0;
                    tp_navi_timer_tick  = GetTickCount();
                }
                check_msg_cnt_navi_info = check_msg_cnt;
#else
                Navi_Infomation.len = 5; // tmp8_1;
                memcpy(&Navi_Infomation.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], 5);
                tmp8 = memcmp(&prev_navi_info_buf[0], &Navi_Infomation.buf[0], Navi_Infomation.len); // JANG_210811
                if ((tmp8) || (1500 <= GetElapsedTime(tp_navi_timer_tick)))
                {
                    //						DPRINTF(DBG_MSG3, "%s() RRM_DISPLAY_NAVI_INFO_CMD len %d\n ", __FUNCTION__, tmp8_1);
                    memcpy(&prev_navi_info_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], 5);
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_NAVI_INFO_CMD);
                    Navi_Infomation.cnt = 0;
                    tp_navi_timer_tick  = GetTickCount();
                }
#endif
                break;
            case RRM_DISPLAY_SONG_NAME_CMD: // n byte song name
#if 1                                       // JANG_211112
                Song_Name.len = tmp8_1;
                memcpy(&Song_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                if (check_msg_cnt_song_name != check_msg_cnt)
                {
                    Song_Name.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_SONG_NAME_CMD);
                }
                check_msg_cnt_song_name = check_msg_cnt;
#else
                Song_Name.len = tmp8_1;
                memcpy(&Song_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                tmp8 = memcmp(&prev_song_name_buf[0], &Song_Name.buf[0], Song_Name.len); // JANG_210811
                if ((tmp8) || (1500 <= GetElapsedTime(tp_song_timer_tick)))
                {
                    //						DPRINTF(DBG_MSG3, "%s() RRM_DISPLAY_SONG_NAME_CMD len %d\n ", __FUNCTION__, tmp8_1);
                    memcpy(&prev_song_name_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_SONG_NAME_CMD);
                    Song_Name.cnt      = 0;
                    tp_song_timer_tick = GetTickCount();
                }
#endif
                break;
            case RRM_DISPLAY_ARTIST_NAME_CMD: // n byte artist name
#if 1                                         // JANG_211112
                Artist_Name.len = tmp8_1;
                memcpy(&Artist_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                if (check_msg_cnt_artist_name != check_msg_cnt)
                {
                    Artist_Name.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_ARTIST_NAME_CMD);
                }
                check_msg_cnt_artist_name = check_msg_cnt;
#else
                Artist_Name.len = tmp8_1;
                memcpy(&Artist_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                tmp8 = memcmp(&prev_artist_name_buf[0], &Artist_Name.buf[0], Artist_Name.len); // JANG_210811
                if ((tmp8) || (1500 <= GetElapsedTime(tp_artist_timer_tick)))
                {
                    memcpy(&prev_artist_name_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                    //						DPRINTF(DBG_MSG3, "%s() RRM_DISPLAY_ARTIST_NAME_CMD len %d\n ", __FUNCTION__, tmp8_1);
                    Artist_Name.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_ARTIST_NAME_CMD);

                    tp_artist_timer_tick = GetTickCount();
                }
#endif
                break;
            case RRM_DISPLAY_PHONE_NUM_CMD: // n byte phone number
#if 1                                       // JANG_211112
                Phone_Number.len = tmp8_1;
                memcpy(&Phone_Number.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                if (check_msg_cnt_call_num != check_msg_cnt)
                {
                    Phone_Number.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_PHONE_NUM_CMD);
                    DPRINTF(DBG_MSG3, "CALL_NUM : len %d cnt:%d pre_cnt:%d \n", tmp8_1, check_msg_cnt, check_msg_cnt_call_num);
                }
                check_msg_cnt_call_num = check_msg_cnt;
#else
                Phone_Number.len = tmp8_1;
                memcpy(&Phone_Number.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                tmp8 = memcmp(&prev_phone_num_buf[0], &Phone_Number.buf[0], Phone_Number.len);
                DPRINTF(DBG_MSG3, "CALL_NUM : len %d cnt:%d time:%d\n ", tmp8_1, check_msg_cnt, tp_phone_number_timer_tick);
                if ((tmp8) || (1500 <= GetElapsedTime(tp_phone_number_timer_tick)))
                {
                    memcpy(&prev_phone_num_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                    Phone_Number.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_PHONE_NUM_CMD);
                    tp_phone_number_timer_tick = GetTickCount();
                    DPRINTF(DBG_MSG3, "CALL_NUM excute \n ");
                }
#endif
                break;
            case RRM_DISPLAY_CALL_NAME_CMD: // n byte call name
#if 1                                       // JANG_211112
                Call_Name.len = tmp8_1;
                memcpy(&Call_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                if (check_msg_cnt_call_name != check_msg_cnt)
                {
                    Call_Name.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_CALL_NAME_CMD);
                }
                DPRINTF(DBG_MSG3, "CALL_NAME : len %d cnt:%d pre_cnt:%d \n", tmp8_1, check_msg_cnt_call_name, check_msg_cnt);
                check_msg_cnt_call_name = check_msg_cnt;
#else
                Call_Name.len = tmp8_1;
                memcpy(&Call_Name.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                tmp8 = memcmp(&prev_call_name_buf[0], &Call_Name.buf[0], Call_Name.len); // JANG_210811
                DPRINTF(DBG_MSG3, "CALL_NAME : len %d cnt:%d time:%d\n ", tmp8_1, check_msg_cnt, tp_call_timer_tick);
                if ((tmp8) || (1500 <= GetElapsedTime(tp_call_timer_tick)))
                {
                    memcpy(&prev_call_name_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], tmp8_1);
                    Call_Name.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_CALL_NAME_CMD);
                    tp_call_timer_tick = GetTickCount();
                    DPRINTF(DBG_MSG3, "CALL_NAME excute \n ");
                }
#endif
                break;
            case RRM_DISPLAY_CALL_INFO_CMD: // 4 byte call information
#if 1                                       // JANG_211112
                Phone_Information.len = tmp8_1;
                memcpy(&Phone_Information.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], Phone_Information.len);
                if (check_msg_cnt_call_inform != check_msg_cnt)
                {
                    Phone_Information.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_CALL_INFO_CMD);
                }
                DPRINTF(DBG_MSG3, "CALL_INFO : len %d cnt:%d pre_cnt:%d \n", tmp8_1, check_msg_cnt_call_inform, check_msg_cnt);
                check_msg_cnt_call_inform = check_msg_cnt;
#else
                Phone_Information.len = 4;
                memcpy(&Phone_Information.buf[0], &rx_data[PACKET_HEADER_SIZE + 2], Phone_Information.len);
                tmp8 = memcmp(&prev_phone_info_buf[0], &Phone_Information.buf[0], Phone_Information.len); // JANG_210811
                DPRINTF(DBG_MSG3, "CALL_INFO : len %d cnt:%d time:%d\n ", __FUNCTION__, Phone_Information.len, check_msg_cnt, tp_phone_infor_timer_tick);
                if ((tmp8) || (1500 <= GetElapsedTime(tp_phone_infor_timer_tick)))
                {
                    memcpy(&prev_phone_info_buf[0], &rx_data[PACKET_HEADER_SIZE + 2], Phone_Information.len);
                    Phone_Information.cnt = 0;
                    q_en(&tpdvd_cmd_q, (uint8)RRM_DISPLAY_CALL_INFO_CMD);
                    tp_phone_infor_timer_tick = GetTickCount();
                    DPRINTF(DBG_MSG3, "CALL_INFO excute \n ");
                }
#endif
                break;
            }
#if 0		
			DPRINTF(DBG_MSG3, "%s() MSG_IVI_INFO_CMD %x %x %x %x %x %x %x %x \n ", __FUNCTION__, 
				rx_data[PACKET_HEADER_SIZE],rx_data[PACKET_HEADER_SIZE+1],rx_data[PACKET_HEADER_SIZE+2],rx_data[PACKET_HEADER_SIZE+3],
				rx_data[PACKET_HEADER_SIZE+4],rx_data[PACKET_HEADER_SIZE+5],rx_data[PACKET_HEADER_SIZE+6],rx_data[PACKET_HEADER_SIZE+7]);
#endif
            break;
        case MSG_CLU_STATUS_INFO_CMD:
            // mcu send id
            tmp8        = rx_data[PACKET_HEADER_SIZE];
            msg.message = MSG_CLU_STATUS_INFO_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = tmp8;
            Uart_Response(&msg);
            break;
        case MSG_CLU_STATUS_REQ_CMD:
            tmp8        = rx_data[PACKET_HEADER_SIZE];
            msg.message = MSG_CLU_STATUS_REQ_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 1;
            msg.buf[0]  = tmp8;
            Uart_Response(&msg);
            // parsing

            break;
#endif

        case MSG_DIAG_USB_CMD:
            diag_usb_device.flag   = OFF; // JANG_210920_4
            diag_usb_device.trycnt = 0;
            //			diag_usb_device.timeout = GetTickCount();
            break;

        case MSG_UART_TEST:
            msg.message = MSG_UART_TEST;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 2;
            msg.buf[0]  = rx_data[PACKET_HEADER_SIZE];
            msg.buf[1]  = rx_data[PACKET_HEADER_SIZE + 1];
            Uart_Response(&msg);
            break;

#ifdef CAN_RXTX_TEST_FUNC
        case MSG_RXTX_TEST_EXCUTE_CMD:
            // check recieved data  //no response
            dev->r_System->rxtx_test.rxcnt++;
            /* wl20220817 */
            bt_address[0] = rx_data[PACKET_HEADER_SIZE + 1];
            bt_address[1] = rx_data[PACKET_HEADER_SIZE + 2];
            bt_address[2] = rx_data[PACKET_HEADER_SIZE + 3];
            bt_address[3] = rx_data[PACKET_HEADER_SIZE + 4];
            bt_address[4] = rx_data[PACKET_HEADER_SIZE + 5];
            bt_address[5] = rx_data[PACKET_HEADER_SIZE + 6];
            break;
        case MSG_RXTX_TEST_CMD:
            tmp8 = rx_data[PACKET_HEADER_SIZE];
            if (tmp8 == 0x01)
            {
                // uart rxtx start
                dev->r_System->rxtx_test.state = tmp8;
                dev->r_System->rxtx_test.cnt   = 0;
                dev->r_System->rxtx_test.rxcnt = 0;
                dev->r_System->rxtx_test.txcnt = 0;
            }
            else if (tmp8 == 0x02)
            {
                // uart rxtx end
                dev->r_System->rxtx_test.state = tmp8;
            }
            break;
        case MSG_RXTX_TEST_RESULT_CMD:
            msg.message = MSG_RXTX_TEST_RESULT_CMD;
            msg.msg_cnt = check_msg_cnt;
            msg.len     = 2;
            msg.buf[0]  = (dev->r_System->rxtx_test.rxcnt >> 8);
            msg.buf[1]  = (dev->r_System->rxtx_test.rxcnt & 0xFF);
            Uart_Response(&msg);
            break;
#endif

        case MSG_VERSION:
            // msg.buf[3] = tmp8= rx_data[PACKET_HEADER_SIZE];//target version
            msg.message = MSG_VERSION;
            msg.msg_cnt = check_msg_cnt;
            msg.state   = MCU_RESPONSE;
            msg.len     = 5;

            if (tmp8 == 0x0)
            {
                // AMP Version  //HEX

                //				DPRINTF(DBG_MSG1,"MSG_VERSION Target %x %x %x\n\r",CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_H(),CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_M(),
                //					CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_L());
            }
            //			else if(tmp8==0x1)
            //			{
            //				//MCU Version //DEC
            //				#ifdef CX62C_FUNCTION
            msg.buf[0] = Version_1 >> 8; // version first
            msg.buf[1] = Version_1;      // version second
            msg.buf[2] = Version_2;
            msg.buf[3] = Version_3 >> 8; // version third
            msg.buf[4] = Version_3;
            // DPRINTF(DBG_INFO,"System is inialized V0%d.%d.%d\n\r", msg.buf[0],msg.buf[1],msg.buf[2]);
            //				#else
            //				msg.buf[0] = 63;	//version first
            //				msg.buf[1] = 00;	//version second
            //				msg.buf[2] = 7;	//version third
            //				#endif
            //			}
            Uart_Response(&msg);
            // UART_Send_Book(dev, &msg);
            break;
        default:
            break;
        }
    }
}

// MCU���ʹ��������SOC
void Uart_Send_Func(const SendMSGbuf *msg)
{
    uint8  uart_respnse_buf[300];
    uint16 length = 0, checksum = 0;
    uint16 i = 0;

    length = msg->len + PACKET_HEADER_SIZE + 1;

    uart_respnse_buf[0] = CPU_STX;     // start byte          0x02
    uart_respnse_buf[1] = MCU_COMMAND; // state byte         0x0C
    uart_respnse_buf[2] = (uint8)(length) >> 8;
    uart_respnse_buf[3] = (uint8)(length)&0xFF; // length      0x0013
    uart_respnse_buf[4] = msg->message;         // cmd id    0x84

    if (msg->state == MCU_COMMAND) // message counter
    {
        MCU_TX_CNT++;
        uart_respnse_buf[5] = (MCU_TX_CNT >> 8);
        uart_respnse_buf[6] = MCU_TX_CNT;
    }
    else
    {
        uart_respnse_buf[5] = (uint8)(msg->msg_cnt >> 8);
        uart_respnse_buf[6] = (uint8)msg->msg_cnt;
    }

    /*
    uart_respnse_buf[5] = (uint8)(msg->msg_cnt>>8);
    uart_respnse_buf[6] = (uint8)msg->msg_cnt;
    */

    memcpy(&uart_respnse_buf[PACKET_HEADER_SIZE], msg->buf, msg->len);

    for (i = 0; i < (length - 1); i++)
        checksum += uart_respnse_buf[i];
    uart_respnse_buf[length - 1] = checksum;

#if 1
    DPRINTF(DBG_MSG1, "%s :  ", __FUNCTION__);
    for (i = 0; i < length; i++)
        DPRINTF(DBG_MSG1, "%x ", uart_respnse_buf[i]);
    DPRINTF(DBG_MSG1, " \n\r");
#endif

    uputa0(&uart_respnse_buf[0], length);

    // DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ScrollUp \n");
}

void Uart_Send_Func1(const SendMSGbuf *msg)
{
    uint8  uart_respnse_buf[300];
    uint16 length = 0, checksum = 0;
    uint16 i = 0;

    length = msg->len + PACKET_HEADER_SIZE + 1;

    uart_respnse_buf[0] = CPU_STX;
    uart_respnse_buf[1] = MCU_COMMAND;
    uart_respnse_buf[2] = (uint8)(length) >> 8;
    uart_respnse_buf[3] = (uint8)(length)&0xFF;
    uart_respnse_buf[4] = msg->message;
    uart_respnse_buf[5] = (uint8)(msg->msg_cnt >> 8);
    uart_respnse_buf[6] = (uint8)msg->msg_cnt;

    memcpy(&uart_respnse_buf[PACKET_HEADER_SIZE], msg->buf, msg->len);

    for (i = 0; i < (length - 1); i++)
        checksum += uart_respnse_buf[i];

    uart_respnse_buf[length - 1] = checksum;

#if 1
    DPRINTF(DBG_MSG1, "%s :  ", __FUNCTION__);
    for (i = 0; i < length; i++)
        DPRINTF(DBG_MSG1, "%x ", uart_respnse_buf[i]);
    DPRINTF(DBG_MSG1, " \n\r");
#endif

    uputa0(&uart_respnse_buf[0], length);
}

void Uart_Response(const MSGbuf *msg)
{
    uint8  uart_respnse_buf[300];
    uint16 length = 0, checksum = 0;
    uint16 i = 0;

    length = msg->len + PACKET_HEADER_SIZE + 1;

    uart_respnse_buf[0] = CPU_STX;
    uart_respnse_buf[1] = MCU_RESPONSE;
    uart_respnse_buf[2] = (uint8)(length) >> 8;
    uart_respnse_buf[3] = (uint8)(length)&0xFF;
    uart_respnse_buf[4] = msg->message; // ID
    uart_respnse_buf[5] = (uint8)(msg->msg_cnt >> 8);
    uart_respnse_buf[6] = (uint8)msg->msg_cnt;
    uart_respnse_buf[7] = 0x01; // ACK

    memcpy(&uart_respnse_buf[PACKET_HEADER_SIZE], msg->buf, msg->len);

    for (i = 0; i < (length - 1); i++)
        checksum += uart_respnse_buf[i];

    uart_respnse_buf[length - 1] = checksum;
    uputa0(&uart_respnse_buf[0], length);

#if 0
	DPRINTF(DBG_MSG1,"Uart_Response :  ");
	for(i=0;i<length;i++)
		DPRINTF(DBG_MSG1,"%x ",uart_respnse_buf[i]);
	DPRINTF(DBG_MSG1," \n\r");
#endif

    //	uart_timer_tick = GetTickCount();
    //	DPRINTF(DBG_MSG1," %d \n\r",uart_timer_tick);
}

void UART_Send_Book(DEVICE *dev, const MSGbuf *msg)
{

    dev->X50_UART->u_length = X50_Uart_Resp_Cmd(msg, 0); // booking packet length check

    if ((UART_Packet_Info.buf_end + dev->X50_UART->u_length) < sizeof(uart0_tx_test_buf))
    {
        memcpy(&uart0_tx_test_buf[UART_Packet_Info.buf_end], &TX_MSG_BUF[0], dev->X50_UART->u_length);

        DPRINTF(DBG_INFO, "%s", "[Rx CAN] ");

        for (uint8_t FrameCnt = 7; FrameCnt < 18; FrameCnt++)
        {
            if (FrameCnt == 9)
            {
                FrameCnt++;
            }

            DPRINTF(DBG_INFO, "[%02x]", TX_MSG_BUF[FrameCnt]);
        }

        DPRINTF(DBG_INFO, "%s", "\r\n");

        UART_Packet_Info.buf_end += dev->X50_UART->u_length;
        dev->X50_UART->u_state = UART_TX_READY;
    }
    else
        DPRINTF(DBG_INFO, "FIFO BUF FULL...!!!\n\r");
}

uint16 X50_Uart_Resp_Cmd(const MSGbuf *msg, uint16 Busy_length)
{
    uint16 length   = 0;
    uint16 checksum = 0;
    uint16 i        = 0;

    if (Busy_length == 0)
    {
        length = msg->len + PACKET_HEADER_SIZE + 1;

        TX_MSG_BUF[0] = CPU_STX;
        TX_MSG_BUF[1] = msg->state;
        TX_MSG_BUF[2] = (uint8)(length) >> 8;
        TX_MSG_BUF[3] = (uint8)(length)&0xFF; // to make sure ???
        TX_MSG_BUF[4] = msg->message;         // ID

        if (msg->state == MCU_COMMAND)
        {
            MCU_TX_CNT++;
            TX_MSG_BUF[5] = (MCU_TX_CNT >> 8);
            TX_MSG_BUF[6] = MCU_TX_CNT;
        }
        else
        {
            TX_MSG_BUF[5] = (uint8)(msg->msg_cnt >> 8);
            TX_MSG_BUF[6] = (uint8)msg->msg_cnt;
        }

        memcpy(&TX_MSG_BUF[PACKET_HEADER_SIZE], msg->buf, msg->len);

        for (i = 0; i < (length - 1); i++)
        {
            checksum += TX_MSG_BUF[i];
        }

        TX_MSG_BUF[length - 1] = checksum;
    }
    else
    {
    }
    return length;
}

static void X50_UART_Rx_Service(DEVICE *dev)
{
    uint8        tmp8       = 0;
    uint8        packet_cnt = 0;
    static uint8 Length     = 0;
    static uint8 chksum     = 0;
    uint8        data       = 0;

    WDOG_DRV_Trigger(INST_WATCHDOG1); // start clear first

    if (front != rear)
    {
        if (UART_Packet_Info.STX == 0)
        {
            data = pull_que();

            if (CPU_STX == data)
            {
                UART_Packet_Info.STX = 1; // set STX received
                UART_Packet_Info.Data_cnt++;
                UART_Packet_Info.LEN = 0;
                rx_counter           = 0;
                Length               = 0;
                chksum               = 0;

                DPRINTF(DBG_INFO, "[0x%x]", data);

                uart_rx_buffer[rx_counter] = data;
                rx_counter++;
                chksum += data;
            }
            else
                return;
        }
        else
        {
            data = pull_que();

            UART_Packet_Info.Data_cnt++;

            uart_rx_buffer[rx_counter] = data;
            rx_counter++;

            DPRINTF(DBG_INFO, "[0x%x]", data);

            if (UART_Packet_Info.LEN == 1)
            {
                if (rx_counter != Length)
                {
                    chksum += data;
                }
                else
                {
                    if (chksum != data)
                    {
                        DPRINTF(DBG_ERR, "Packet CHKSUM ERR : [%x] \n\r", chksum);

                        UART_Packet_Info.STX      = 0;
                        UART_Packet_Info.Data_cnt = 0;
                    }
                    else
                    {
                        // DPRINTF(DBG_ERR, "Packet CHKSUM OK : %x \n\r", chksum);
                        DPRINTF(DBG_INFO, "\n\r");

                        X50_Uart_Recv_Data(dev, &uart_rx_buffer[0], rx_len); // packet investigate...rx_Len <-- not use

                        UART_Packet_Info.STX      = 0;
                        UART_Packet_Info.Data_cnt = 0;
                    }
                }
            }
            else
            {
                chksum += data;

                if (UART_Packet_Info.Data_cnt == 3)
                {
                    Length = (data << 8); // check delay
                }

                if (UART_Packet_Info.Data_cnt == 4)
                {
                    Length |= data;
                    UART_Packet_Info.LEN = 1;
                    // DPRINTF(DBG_INFO, "Length --> %x\n\r", Length);
                    return;
                }
            }
        }
    }
}

// MCU��ȡSOC���ڲ�����ʵ��CAN�źŸ�ֵ����
void CAN_Msg_Tx_Set_Process(DEVICE *dev, uint8 *rx_data, uint8 tmp8)
{
    uint32 UartRxID = 0;
    uint8  Databuf  = 0;

    UartRxID = rx_data[7] << 8;

    UartRxID |= rx_data[8];

    // DPRINTF(DBG_MSG1, "MSG_VEHICLE_SIG\n");
    for (int i = 9; i < 17; i++)
    {
        rx_data[i] = 0x54;
    }
    UartRxID = 0x4FC;

    switch (UartRxID) // CAN ID
    {
    case 0x537:
        // CanData[0]
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxNavSpeedLimitStatus(Databuf);

        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxNavSpeedLimit(Databuf);
        // CanData[3]
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxNavSpeedLimitUnits(Databuf);

        // CanData[3]
        Databuf = Uart_Data_Shft(rx_data[12], 4);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxNavCurrRoadType(Databuf);

        break;
    case 0x4FC:
        // CanData[2]
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_FCWSwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[11], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_FCWSnvtySet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[11], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_AEBSwtSet(Databuf);

        // CanData[3]
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_SCF_request_confirmed(Databuf);

        Databuf = Uart_Data_Shft(rx_data[12], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_SCFSwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[12], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_SLA_TSI_TLI_SwtSet(Databuf);

        // CanData[4]
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_LKASwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_HMASwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_IESSwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_DAISwtSet(Databuf);

        // CanData[5]
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_LDPSwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[14], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_WarnModSwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[14], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_LDWSwtSet(Databuf);

        // CanData[7]
        Databuf = Uart_Data_Shft(rx_data[15], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_ELKSwtSet(Databuf);

        Databuf = Uart_Data_Shft(rx_data[15], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_11_LDWLDPSnvtySetLDWLDP(Databuf);

        break;

    // IHU_DTC1
    case 0x3BE:
        // CanData[0]
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_AmbientLightBrightnessAdjust_Front(Databuf);

        // CanData[1]
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_AmbientLightBrightnessAdjust_Roof(Databuf);

        // CanData[2]
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_AmbientLightBrightnessAdjust_Rear(Databuf);

        // CanData[3]
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_AmbientLightColorSet_Front(Databuf);

        // CanData[4]
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_AmbientLightColorSet_Roof(Databuf);

        // CanData[5]
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_AmbientLightColorSet_Rear(Databuf);

        // CanData[6]
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_RegionColorSynchronizationReq(Databuf);

        Databuf = Uart_Data_Shft(rx_data[15], 4);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_AmbientLightModeSet(Databuf);

        // CanData[7]
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AmbientLightEnableSwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[16], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AmbientLightAreaDisplayEnableSwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[16], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AmbientLightBreatheEnableSwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[16], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AmbientLightBreatheWithVehSpdEnableSwSts(Databuf);
        break;

    case 0x3BD: //预约充电设置
                // CanData[0]
        // IHU_Chg_SOC_LimitPointSet   Bit0-Bit5      充电限制值设置
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_Chg_SOC_LimitPointSet(Databuf);

        // IHU_ChgModeSet  Bit6-Bit7  充电模式设置
        Databuf = Uart_Data_Shft(rx_data[9], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_ChgModeSet(Databuf);

        // CanData[1]   Bit8-Bit12    预约充电开始小时设置
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_BookChgStartTimeSet_Hour(Databuf);

        // CanData[2]   Bit16-Bit21   预约充电开始分钟设置
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_BookChgStartTimeSet_Minute(Databuf);

        // Bit22-Bit23  预约充电时间设置有效状态
        Databuf = Uart_Data_Shft(rx_data[11], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_BookChgTimeSet_VD(Databuf);

        // CanData[3]   Bit24-Bit28   预约充电停止小时设置
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_BookChgStopTimeSet_Hour(Databuf);

        // CanData[4]   Bit32-Bit37   预约充电停止分钟设置
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_BookchgStopTimeSet_Minute(Databuf);
        break;

    // IHU12  部分周期，部分周期事件型信号
    case 0x3BC: // IHU_12  氛围灯的控制及设置
        IHU12OperFlag   = TRUE;
        IHU12TimerCount = 0;

        // CanData[0]  Bit0-Bit4  音乐响度120HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_120HZ(Databuf);
        // Bit6
        Databuf = Uart_Data_Shft(rx_data[9], 6);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_VoiceAssistantActiveSts(Databuf);

        // CanData[1]  Bit8-Bit12  音乐响度250HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_250HZ(Databuf);

        // CanData[2]   Bit16-Bit20  音乐响度500HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_500HZ(Databuf);

        // CanData[3]   Bit21-Bit23  音乐响度1000HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_1000HZ(Databuf);

        // CanData[4]   Bit32-Bit36  音乐响度1500HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_1500HZ(Databuf);

        // CanData[5]   Bit40-Bit44  音乐响度2000HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_2000HZ(Databuf);

        // CanData[6]   Bit48-Bit52  音乐响度6000HZ等级设置   周期型信号
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_MusicLoudness_6000HZ(Databuf);
        break;

    case 0x3BB:
        // CanData[0] bit0-bit5
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_GPS_Time_year(Databuf);

        Databuf = Uart_Data_Shft(rx_data[9], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_GPS_TimeSwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_GPS_Time_Month(Databuf);

        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_GPS_Time_Day(Databuf);

        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_GPS_Time_Hour(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_GPS_Time_Minute(Databuf);

        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_GPS_Time_Second(Databuf);

        break;

    case 0x3BA:
        // CanData[0]  Bit0-Bit1  后向预警灯开关
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_RearWarnSystemEnableSwSts(Databuf);
        break;

    case 0x3B8:
        // CanData[0]   Bit5-Bit7     仪表主题设置
        Databuf = Uart_Data_Shft(rx_data[9], 5);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_ICM_ThemelSet(Databuf);

        // CanData[1]   Bit14-Bit15   仪表亮屏请求
        Databuf = Uart_Data_Shft(rx_data[10], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_ICM_ScreenReq(Databuf);

        // CanData[2]   Bit16-Bit21   超速报警设置
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 63);
        IlPutTxIHU_OverSpdValueSet(Databuf);
        u8Set1 = Databuf;
        if ((0u != Databuf) && (Databuf != e2p_save_data.E2P_CLS_OVERSPD))
        {
            e2p_save_data.E2P_CLS_OVERSPD = Databuf;
            e2p_save_data.E2P_CHKSUM      = checksum_setup_value(&e2p_save_data);
            Setting_Update                = ON;
        }
        // Bit22
        Databuf = Uart_Data_Shft(rx_data[11], 6);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_ICM_Menu_OK_ButtonSts(Databuf);
        // Bit23
        Databuf = Uart_Data_Shft(rx_data[11], 7);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_ICM_ModeButtonSts(Databuf);

        // CanData[3]   Bit24-Bit27    疲劳驾驶时间设置
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_FatigureDrivingTimeSet(Databuf);
        u8Set2 = Databuf;
        if ((0u != Databuf) && (Databuf != e2p_save_data.E2P_CLS_DRIVINGTIME))
        {
            e2p_save_data.E2P_CLS_DRIVINGTIME = Databuf;
            e2p_save_data.E2P_CHKSUM          = checksum_setup_value(&e2p_save_data);
            Setting_Update                    = ON;
        }
        // Bit28    仪表上
        Databuf = Uart_Data_Shft(rx_data[12], 4);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_ICM_UpButtonSts(Databuf);
        // Bit29    仪表下
        Databuf = Uart_Data_Shft(rx_data[12], 5);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_ICM_DownButtonSts(Databuf);
        // Bit30    仪表左
        Databuf = Uart_Data_Shft(rx_data[12], 6);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU__ICM_LeftButtonSts(Databuf);
        // Bit31    仪表右
        Databuf = Uart_Data_Shft(rx_data[12], 7);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU__ICM_RightButtonSts(Databuf);

        // CanData[4]   Bit32-Bit35   仪表亮度调节
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_BrightnessAdjustSet_ICM(Databuf);
        u8Set3 = Databuf;
        if ((0u != Databuf) && (Databuf != e2p_save_data.E2P_CLS_BRIGHTNESS))
        {
            e2p_save_data.E2P_CLS_BRIGHTNESS = Databuf;
            e2p_save_data.E2P_CHKSUM         = checksum_setup_value(&e2p_save_data);
            Setting_Update                   = ON;
        }

        // CanData[5]   Bit46   一键拍照开关
        Databuf = Uart_Data_Shft(rx_data[14], 6);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_DVR_QuickCameraSw(Databuf);
        DPRINTF(DBG_MSG3, "Cluster setting BRIGHTNESS=%x DRIVINGTIME=%x OVERSPD=%x\r\n", u8Set3, u8Set2, u8Set1);
        break;

    case 0x3B7:
        // CanData[0]   Bit0-Bit2
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SeatHeatLevelSetReq_RL(Databuf);
        // CanData[0]   Bit3-Bit5
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SeatHeatLevelSetReq_RR(Databuf);

        // CanData[2]   Bit18-Bit19
        Databuf = Uart_Data_Shft(rx_data[11], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_WirelessChargingEnableSwSts(Databuf);
        // CanData[3]   Bit27-Bit29
        Databuf = Uart_Data_Shft(rx_data[12], 3);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_AVAS_VolumeSet(Databuf);

        // CanData[4]   Bit32-Bit35   行人提醒音源设置
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_AVAS_AudioSourceSet(Databuf);
        // Bit38-Bit39   行人提醒开关
        Databuf = Uart_Data_Shft(rx_data[13], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AVAS_EnableSwSts(Databuf);
        u8Set1 = Databuf;
        if ((0u != Databuf) && (Databuf != e2p_save_data.E2P_AVAS_ONOFF))
        {
            e2p_save_data.E2P_AVAS_ONOFF = Databuf;
            e2p_save_data.E2P_CHKSUM     = checksum_setup_value(&e2p_save_data);
            Setting_Update               = ON;
        }

        // CanData[5]  Bit40-Bit41
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_SeatMemoryEnableSwSts(Databuf);
        // CanData[5]  Bit42-Bit44   座椅存储控制
        Databuf = Uart_Data_Shft(rx_data[14], 2);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SeatPositionStoreReq(Databuf);
        // Bit45-Bit47   座椅调用控制
        Databuf = Uart_Data_Shft(rx_data[14], 5);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SeatPositionCalloutReq(Databuf);

        // CanData[6]  Bit48-Bit50
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SeatHeatVentLevelSetReq_Drive(Databuf);
        // CanData[6]  Bit51-Bit53
        Databuf = Uart_Data_Shft(rx_data[15], 3);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SeatHeatVentLevelSetReq_Psngr(Databuf);
        // CanData[6]  Bit54-Bit55   人脸识别绑定座椅调节
        Databuf = Uart_Data_Shft(rx_data[15], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FaceRecSeatEnableSwSts(Databuf);

        // CanData[7]  Bit56-Bit57   人脸识别绑定后视镜调节
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FaceRecMirrorInclineEnableSwSts(Databuf);
        // Bit58-Bit59   人脸识别身份验证执行结果
        Databuf = Uart_Data_Shft(rx_data[16], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FaceRecAuthenticationResult(Databuf);
        // Bit60-Bit61   账户删除
        Databuf = Uart_Data_Shft(rx_data[16], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AccountDeletedReq(Databuf);
        // Bit62-Bit63   便携出入座椅控制开关  add 2022/07/07
        Databuf = Uart_Data_Shft(rx_data[16], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_EntryExitSeatCtrlEnableSwSts(Databuf);
        DPRINTF(DBG_MSG3, "AVAS setting ONOFF=%x \r\n", u8Set1);
        break;

    case 0x3B6: // IHU_6   ?????????????    100ms??20ms??
        IHU6OperFlag   = TRUE;
        IHU6TimerCount = 0;

        // CanData[0]   Bit0  Auto开关
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_AUTO_SwSts(Databuf);
        // Bit1   A/C开关
        Databuf = Uart_Data_Shft(rx_data[9], 1);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_AC_SwSts(Databuf);
        // Bit2   PTC开关
        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_PTC_SwSts(Databuf);
        // Bit3
        Databuf = Uart_Data_Shft(rx_data[9], 3);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_OFF_SwSts(Databuf);
        // Bit4   后除霜开关
        Databuf = Uart_Data_Shft(rx_data[9], 4);
        Databuf = UART_Data_Maching(Databuf, 1); // 2022/07/27
        IlPutTxIHU_RearDfstSwSts(Databuf);
        // Bit5-bit6
        Databuf = Uart_Data_Shft(rx_data[9], 5);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_CirculationModeSet(Databuf);
        // Bit7
        Databuf = Uart_Data_Shft(rx_data[9], 7);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_AutoDfstSwSts(Databuf);

        // CanData[1]  Bit8-Bit10   吹风模式设置
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_BlowModeSet(Databuf);
        // Bit11-Bit15   空调温度设置
        Databuf = Uart_Data_Shft(rx_data[10], 3);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_TempSet_Driver(Databuf);

        // CanData[2]   Bit16-Bit19   空调风量设置
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_BlowerlevelSet(Databuf);
        // Bit20
        Databuf = Uart_Data_Shft(rx_data[11], 4);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_AnionGeneratorSwSts(Databuf);
        // Bit22-biit23
        Databuf = Uart_Data_Shft(rx_data[11], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AQS_EnablaSw(Databuf);

        // CanData[3]   Bit24-Bit25   空调自通风开关
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AutoBlowEnablaSw(Databuf);
        // Bit28-Bit29   空调记忆开关
        Databuf = Uart_Data_Shft(rx_data[12], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AC_MemoryModeEnablaSw(Databuf);
        // Bit30-Bit31
        Databuf = Uart_Data_Shft(rx_data[12], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_DUAL_EnablaSw(Databuf);

        // CanData[4]    Bit32    前除霜开关
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_FrontDefrostSwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 1);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_VentilationSwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 2);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_ECO_SwSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[13], 3);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_TempSet_Psngr(Databuf);

        // CanData[6]    Bit48-Bit50   灯光控制开关
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_LightCtrlSwSts(Databuf);
        // bit51-bit53
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_FrontWiperSwSts(Databuf);
        // Bit54   后雾灯开关
        Databuf = Uart_Data_Shft(rx_data[15], 6);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_RearFogLightSwSts(Databuf);
        // Bit55   后视镜倾斜
        Databuf = Uart_Data_Shft(rx_data[15], 7);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_MirrorReversePositionStoreReq(Databuf);

        // CanData[7]   Bit56-Bit58    左后视镜调节
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 7); // Modify by 2022.07.07
        IlPutTxIHU_MirrorCtrlReq_L(Databuf);
        // Bit59-Bit61   右后视镜调节
        Databuf = Uart_Data_Shft(rx_data[16], 3);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_MirrorCtrlReq_R(Databuf);
        // Bit62
        Databuf = Uart_Data_Shft(rx_data[16], 6);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_RearWiperSwSts(Databuf);
        // Bit63
        Databuf = Uart_Data_Shft(rx_data[16], 7);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_RearWashSwSts(Databuf);
        break;

    case 0x3B5:
        // CanData[2]   Bit18-Bit19   逆变确认
        Databuf = Uart_Data_Shft(rx_data[11], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_InverterConfirmSts(Databuf);
        // Bit20-Bit21  逆变器开关
        Databuf = Uart_Data_Shft(rx_data[11], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_InverterEnableSwSts(Databuf);
        // Bit23-Bit24
        Databuf = Uart_Data_Shft(rx_data[11], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_CruiseTypeSetSts(Databuf);

        // CanData[3]   Bit24-Bit26   洗车拖车模式设置
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_VehWashModeSet(Databuf);
        // Bit27-Bit31  车速限制值设置
        Databuf = Uart_Data_Shft(rx_data[12], 3);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_SpeedLimitValueSet(Databuf);

        // CanData[4]   Bit32-Bit33   车速限制开关
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_SpeedLimitEnableSwSts(Databuf);
        // Bit34-Bit36  能量回收等级设置
        Databuf = Uart_Data_Shft(rx_data[13], 2);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_RegenerationLevelSet(Databuf);
        // Bit37-Bit39  驾驶模式设置
        Databuf = Uart_Data_Shft(rx_data[13], 5);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_DriveModeSet(Databuf);

        // CanData[5]   Bit40-Bit47   放电状态SOC限制值设置
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 255); // Modify  2022/07/28
        IlPutTxIHU_OutputLimitSOCSet(Databuf);

        // CanData[6]   Bit48-Bit49   换挡提示音开关
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_ShiftVoiceRemind_EnableSwSts(Databuf);
        // Bit50-Bit51   N档操作提示开关
        Databuf = Uart_Data_Shft(rx_data[15], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_ShiftErrorActionRemindEnableSwSts(Databuf);
        // bit52-53
        Databuf = Uart_Data_Shft(rx_data[15], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_SOC_ChargeLockEnableSwSts(Databuf);
        break;

    case 0x3B4:
        // CanData[0] Bit2-Bit3    自动落锁开关
        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AutoLockEnableSwSts(Databuf);
        // Bit4-Bit5    设防提示模式设置:主要包括声音提示，灯光提示，声音+灯光提示三种
        Databuf = Uart_Data_Shft(rx_data[9], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AntiTheftModeSetSwSts(Databuf);
        // Bit6-Bit7    方向盘加热
        Databuf = Uart_Data_Shft(rx_data[9], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_SteeringWheelHeatReq(Databuf);

        // CanData[1]   BIt8-Bit9   设防后视镜折叠开关
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 3); // Modify  2022/07/28
        IlPutTxIHU_MirrorFoldEnableSwSts__(Databuf);
        // Bit10-Bit11  伴我回家开关
        Databuf = Uart_Data_Shft(rx_data[10], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FollowHomeMeEnableSwSts(Databuf);
        // Bit12-Bit13
        Databuf = Uart_Data_Shft(rx_data[10], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_SunroofAutoCloseEnableSwSts(Databuf);
        // Bit14-Bit15  迎宾灯开关
        Databuf = Uart_Data_Shft(rx_data[10], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_WelcomeLightEnableSwSts(Databuf);

        // CanData[2]   Bit16-Bit18   舒享模式设置
        Databuf = Uart_Data_Shft(rx_data[11], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_ComfortModeSet(Databuf);
        // Bit19-Bit21   雨量传感器灵敏度设置
        Databuf = Uart_Data_Shft(rx_data[11], 3);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_WiperSensitivitySet(Databuf);
        // Bit22-Bit23   后视镜折叠开关
        Databuf = Uart_Data_Shft(rx_data[11], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MirrorFoldReq(Databuf);

        // CanData[3]    Bit24-Bit25
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AHB_EnableSwSts(Databuf);

        // CanData[3]    Bit26-Bit28
        Databuf = Uart_Data_Shft(rx_data[12], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_LowBeamAngleAdjust(Databuf);
        // Bit29
        Databuf = Uart_Data_Shft(rx_data[12], 5);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_FrontWiperMaintenanceModeReq(Databuf);
        // Bit30-Bit31   紧急下电提示确认
        Databuf = Uart_Data_Shft(rx_data[12], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_EmergencyPowerOffReqConfirm(Databuf);

        // CanData[4]    Bit32-Bit33   倒车后视镜倾斜开关
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MirrorReverseInclineEnableSwSts(Databuf);

        // Bit36-Bit37   伴我回家时间设置
        Databuf = Uart_Data_Shft(rx_data[13], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FollowMeHomeTimeSetSts(Databuf);

        // Bit38-Bit39
        Databuf = Uart_Data_Shft(rx_data[13], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_DoorWindowCtrlEnableSwSts(Databuf);

        // CanData[5]    Bit40-Bit41   锁车自动升窗开关
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_WindowAutoCloseEnableSwSts(Databuf);

        // bit42-bit43
        Databuf = Uart_Data_Shft(rx_data[14], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_RKE_SunRoofCtrlEnableSwSts(Databuf);
        // Bit46-Bit47   顶灯门控开关
        Databuf = Uart_Data_Shft(rx_data[14], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_DomeLightDoorCtrlSwitchSts(Databuf);

        // CanData[6]
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MaxPositionSet(Databuf);

        // CanData[7]    Bit56-Bit57
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FragranceEnableSwSts(Databuf);

        // bit58-bit59
        Databuf = Uart_Data_Shft(rx_data[16], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FragranceConcentrationSet(Databuf);
        // Bit60-Bit61   顶灯门控开关
        Databuf = Uart_Data_Shft(rx_data[16], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_FragrancePositionSet(Databuf);

        break;

    case 0x3B3:
        // CanData[0]   Bit0-Bit3
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_VoiceCtrl_SunroofReq(Databuf);

        // CanData[0]   Bit4-Bit5
        Databuf = Uart_Data_Shft(rx_data[10], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_VoiceCtrl_SunshadeReq(Databuf);

        // CanData[1]   Bit8-Bit10
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_VoiceCtrl_WindowReq_FL(Databuf);

        // CanData[1]   Bit11-Bit13
        Databuf = Uart_Data_Shft(rx_data[10], 3);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_VoiceCtrl_WindowReq_FR(Databuf);

        // CanData[2]   Bit16-Bit18
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_VoiceCtrl_WindowReq_RL(Databuf);

        // CanData[2]   Bit19-Bit21
        Databuf = Uart_Data_Shft(rx_data[10], 3);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_VoiceCtrl_WindowReq_RR(Databuf);

        // CanData[3]   Bit24-Bit25
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_VoiceCtrl_LowbeamReq(Databuf);

        // CanData[3]   Bit26-Bit27
        Databuf = Uart_Data_Shft(rx_data[10], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_VoiceCtrl_TrunkReq(Databuf);

        // Bit28-Bit29
        Databuf = Uart_Data_Shft(rx_data[10], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_SmokingModeReq(Databuf);

        break;

    case 0x3AD:
        // CanData[0]   Bit0-Bit1     1号账户创建
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AccountCreatSts_Num1(Databuf);
        // Bit2-Bit3     2号账户创建
        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AccountCreatSts_Num2(Databuf);
        // Bit4-Bit5     3号账户创建
        Databuf = Uart_Data_Shft(rx_data[9], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AccountCreatSts_Num3(Databuf);

        // CanData[1]    Bit8-Bit10   驾驶员身份识别
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_DriverFaceRecognitionSts(Databuf);

        break;

    case 0x3AE:
        // CanData[3]
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_ForwardBack_Driver(Databuf);
        // CanData[4]
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_UpDown_Driver(Databuf);
        // CanData[5]
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_SeatBack_Driver(Databuf);

        // CanData[6]	Bit48-Bit51
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_16_MsgAliveCounter(Databuf);
        // CanData[7]
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_LegSupport_Driver(Databuf);

        break;

    case 0x3AF:
        // CanData[0] bit0-bit1
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MassageSwithSet_Psngr(Databuf);
        // CanData[0] bit2-bit4
        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_MassageModeSet_Psngr(Databuf);
        // CanData[0] bit5-bit7
        Databuf = Uart_Data_Shft(rx_data[9], 5);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_MassageStrengthSet_Psngr(Databuf);

        // CanData[1]	Bit8-Bit10
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_LumbarSupportModeSet_Psngr(Databuf);
        // CanData[3]
        Databuf = Uart_Data_Shft(rx_data[12], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_ForwardBack_Psngr(Databuf);
        // CanData[5]
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_SeatBack_Psngr(Databuf);

        // CanData[6]
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_17_MsgAliveCounter(Databuf);

        // CanData[7]
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_MotoPositionSet_LegSupport_Psngr(Databuf);

        break;

    case 0x3AC:

        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_RadioFrequanceMode(Databuf);

        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_RadioResearchSts(Databuf);

        Databuf = Uart_Data_Shft(rx_data[9], 4);
        Databuf = UART_Data_Maching(Databuf, 7);
        IlPutTxIHU_SourceStationMode(Databuf);

        Databuf = Uart_Data_Shft(rx_data[9], 7);
        Databuf = UART_Data_Maching(Databuf, 1);
        IlPutTxIHU_PowerOnSts(Databuf);

        Databuf                                    = Uart_Data_Shft(rx_data[10], 0);
        Databuf                                    = UART_Data_Maching(Databuf, 7);
        IHU_14.IHU_14.IHU_FM_RadioFrequanceValue_0 = rx_data[10];

        Databuf                                    = Uart_Data_Shft(rx_data[11], 0);
        Databuf                                    = UART_Data_Maching(Databuf, 7);
        IHU_14.IHU_14.IHU_FM_RadioFrequanceValue_1 = rx_data[11];

        Databuf                                    = Uart_Data_Shft(rx_data[12], 0);
        Databuf                                    = UART_Data_Maching(Databuf, 7);
        IHU_14.IHU_14.IHU_AM_RadioFrequanceValue_0 = rx_data[12];

        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 31);
        IlPutTxIHU_RadioVolume(Databuf);

        Databuf                                    = Uart_Data_Shft(rx_data[13], 5);
        Databuf                                    = UART_Data_Maching(Databuf, 7);
        IHU_14.IHU_14.IHU_AM_RadioFrequanceValue_1 = Databuf;

        break;

    case 0x3AA: // IHU_1  辅助驾驶控制
        IHU1OperFlag   = TRUE;
        IHU1TimerCount = 0;

        // CanData[0]    Bit4-Bit5
        Databuf = Uart_Data_Shft(rx_data[9], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_EPB_SwSts(Databuf);

        // CanData[3]    Bit28-Bit29
        Databuf = Uart_Data_Shft(rx_data[12], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_BrakemodeSet(Databuf);

        // CanData[3]    Bit28-Bit29
        Databuf = Uart_Data_Shft(rx_data[12], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_EPS_ModeSet(Databuf);

        // CanData[4]    Bit32-Bit33
        Databuf = Uart_Data_Shft(rx_data[13], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_ESC_OFF_SwSts(Databuf);

        // CanData[4]    Bit34-Bit35
        Databuf = Uart_Data_Shft(rx_data[13], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_CST_FunctionEnableSwSts(Databuf);

        // CanData[4]    Bit36-Bit37
        Databuf = Uart_Data_Shft(rx_data[13], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_HDC_FunctionEnableSwSts(Databuf);

        // CanData[4]    Bit38-Bit39
        Databuf = Uart_Data_Shft(rx_data[13], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_AVH_FunctionEnableSwSts(Databuf);

        // CanData[5]    Bit40-Bit41
        Databuf = Uart_Data_Shft(rx_data[14], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_PDC_EnableSwSts(Databuf);
        break;

    case 0x293:
        // CanData[0]    Bit0-Bit1
        Databuf = Uart_Data_Shft(rx_data[9], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_CruiseCtrlEnableButtonSts(Databuf);
        // Bit2-Bit3
        Databuf = Uart_Data_Shft(rx_data[9], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_CruiseCancelButtonSts(Databuf);
        // Bit4-Bit5
        Databuf = Uart_Data_Shft(rx_data[9], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_CruiseSetButtonSts(Databuf);
        // Bit6-Bit7
        Databuf = Uart_Data_Shft(rx_data[9], 6);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_CruiseResumeButtonSts(Databuf);

        // CanData[1]    Bit8-Bit9
        Databuf = Uart_Data_Shft(rx_data[10], 0);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_SpdLimitButtonSts(Databuf);
        // Bit10-Bit11
        Databuf = Uart_Data_Shft(rx_data[10], 2);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_ACC_GapSB1(Databuf);
        // Bit12-Bit13
        Databuf = Uart_Data_Shft(rx_data[10], 4);
        Databuf = UART_Data_Maching(Databuf, 3);
        IlPutTxIHU_MFS_ACC_GapSB(Databuf);
        // Bit14-Bit15
//        Databuf = Uart_Data_Shft(rx_data[10], 6);
//        Databuf = UART_Data_Maching(Databuf, 3);
//        IlPutTxIHU_Reserved0(Databuf);
//        // CanData[2] - CanData[5]
//        Databuf                         = Uart_Data_Shft(rx_data[11], 0);
//        Databuf                         = UART_Data_Maching(Databuf, 255);
//        IHU_MFS.IHU_MFS.IHU_Reserved1_0 = rx_data[11];
//
//        Databuf                         = Uart_Data_Shft(rx_data[12], 0);
//        Databuf                         = UART_Data_Maching(Databuf, 255);
//        IHU_MFS.IHU_MFS.IHU_Reserved1_1 = rx_data[12];
//
//        Databuf                         = Uart_Data_Shft(rx_data[13], 0);
//        Databuf                         = UART_Data_Maching(Databuf, 255);
//        IHU_MFS.IHU_MFS.IHU_Reserved1_2 = rx_data[13];
//
//        Databuf                         = Uart_Data_Shft(rx_data[14], 0);
//        Databuf                         = UART_Data_Maching(Databuf, 255);
//        IHU_MFS.IHU_MFS.IHU_Reserved1_3 = rx_data[14];
        // CanData[6] bit48-bit51
        Databuf = Uart_Data_Shft(rx_data[15], 0);
        Databuf = UART_Data_Maching(Databuf, 15);
        IlPutTxIHU_MsgAliveCounter_MFS(Databuf);
        // bit52-bit55
        // Databuf = Uart_Data_Shft(rx_data[15], 4);
        // Databuf = UART_Data_Maching(Databuf, 15);
        // IlPutTxIHU_Reserved2(Databuf);
        // CanData[6]
        Databuf = Uart_Data_Shft(rx_data[16], 0);
        Databuf = UART_Data_Maching(Databuf, 255);
        IlPutTxIHU_Checksum_CRC_MFS(Databuf);
        break;

    default:
        break;
    }
}

uint8 Uart_Data_Shft(uint8 Data, uint8 ShftNum)
{
    uint8 result = 0;

    result = Data >> ShftNum;

    return result;
}

uint8 UART_Data_Maching(uint8 Data, uint8 Len)
{
    uint8 result = 0;

    result = Len & 0xFF;

    result = Data & result;

    return result;
}
BUTTON_TX_t *GetButtonTxData(void)
{
    return &button_tx_data;
}
