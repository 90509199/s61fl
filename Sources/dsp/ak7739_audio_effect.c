/*
 * ak7739_audio_effect.c  --  audio driver for ak7739
 *
 */




#include "includes.h"

// AKM_AudioEffect_Basic_UseCase_V01_2_1

/*
 * Function for Converting User data format to DSP data format
 *
 */
#define min(a, b) ((a<b)?a:b)
#define max(a, b) ((a>b)?a:b)
#define sat(x,_min,_max) (max(min(x,_max),_min))

#define M_PI 3.14159265358979323846


// Single Precision
static int fix32(
	double x,
	int shift)
{
	x = x * pow(2, 31-shift) + ((x<0) ? -0.5:0.5);

	return (int)(max(min(x, pow(2,31)-1), -pow(2,31)));
}

// MSB 32bit when Double Precision
static int fix32H(
	double x,
	int shift)
{
	x = floor( x * pow(2, 31-shift) );

	return (int)(max(min(x, pow(2,31)-1), -pow(2,31)));
}

// LSB 32bit when Double Precision
static int fix32L(
	double x,
	int shift)
{
	x = x * pow(2, 31 - shift) - fix32H(x, shift);

	return fix32(x, 0);
}

/*
* Function for Audio Effect User Interface
*
*/
//Cram data update
static void ae_userValue_to_cramWord(
		unsigned char *cramWord,
		int value)
{
	cramWord[0] = (unsigned char)((0xFF000000 & value) >> 24);
	cramWord[1] = (unsigned char)((0xFF0000 & value) >> 16);
	cramWord[2] = (unsigned char)((0xFF00 & value) >> 8);
	cramWord[3] = (unsigned char)(0xFF & value);
}

int ae_nodeToCramAdd(
		AE_NODETYPE node
)
{
	int add;


	akdbgprt("[AK7739] %s node[%d]\n", __FUNCTION__, node);

	if(node < AE_DCCutSW2 ||  node >= AE_Node_Max) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Node\n", __FUNCTION__,__LINE__);
		return 0xFFFF;
	}

	if(r_sonysound)
	{
		switch(node) 
		{
			case AE_DCCutSW2: add = CRAM_ADDR_IN1_DCCutSW2_sony; akdbgprt("node[AE_DCCutSW2]\n"); break;
			case AE_DeEmpSW2: add = CRAM_ADDR_IN1_DeEmpSW2_sony; akdbgprt("node[AE_DeEmpSW2]\n"); break;
			case AE_CompressorSW2: add = CRAM_ADDR_IN1_CompressorSW2_sony; akdbgprt("node[AE_CompressorSW2]\n"); break;
			case AE_BMTSW2: add = CRAM_ADDR_IN1_BMTSW2_sony; akdbgprt("node[AE_BMTSW2]\n"); break;
			case AE_SoundModeSW2: add = CRAM_ADDR_IN1_SoundModeSW2_sony; akdbgprt("node[AE_SoundModeSW2]\n"); break;
			case AE_LoudnessSW2: add = CRAM_ADDR_IN1_LoudnessSW2_sony; akdbgprt("node[AE_LoudnessSW2]\n"); break;
			case AE_SurroundSW2: add = CRAM_ADDR_IN1_SurroundSW2_sony; akdbgprt("node[AE_SurroundSW2]\n"); break;
			case AE_Out6SW2: add = CRAM_ADDR_IN1_Out6SW2_sony; akdbgprt("node[AE_Out6SW2]\n"); break;
			case AE_MainEffectSW2: add = CRAM_ADDR_IN1_MainEffectSW2_sony; akdbgprt("node[AE_MainEffectSW2]\n"); break;
			case AE_NaviGEQSW2: add = CRAM_ADDR_IN1_NaviGEQSW2_sony; akdbgprt("node[AE_NaviGEQSW2]\n"); break;
			case AE_PhoneGEQSW2: add = CRAM_ADDR_IN1_PhoneGEQSW2_sony; akdbgprt("node[AE_PhoneGEQSW2]\n"); break;
	//		case AE_RSEDCCutSW2: add = CRAM_ADDR_IN1_RSEDCCutSW2; akdbgprt("node[AE_RSEDCCutSW2]\n"); break;
	//		case AE_RSEDeEmpSW2: add = CRAM_ADDR_IN1_RSEDeEmpSW2; akdbgprt("node[AE_RSEDeEmpSW2]\n"); break;
	//		case AE_RSEBMTSW2: add = CRAM_ADDR_IN1_RSEBMTSW2; akdbgprt("node[AE_RSEBMTSW2]\n"); break;
			case AE_Out5SW4: add = CRAM_ADDR_IN1_Out5SW4_sony; akdbgprt("node[AE_Out5SW4]\n"); break;
			case AE_MediaInputSW4: add = CRAM_ADDR_IN1_MediaInputSW4_sony; akdbgprt("node[AE_MediaInputSW4]\n"); break;
			case AE_SoundModeSW4: add = CRAM_ADDR_IN1_SoundModeSW4_sony; akdbgprt("node[AE_SoundModeSW4]\n"); break;
			case AE_MediaInVolume: add = CRAM_ADDR_LEVEL_MediaInVolume_sony; akdbgprt("node[AE_MediaInVolume]\n"); break;
			#ifdef SEPERATE_VOLUME_TABLE
			case AE_FMVolume: add = CRAM_ADDR_LEVEL_MediaInVolume_sony; akdbgprt("node[AE_FMVolume]\n"); break;
			case AE_AMVolume: add = CRAM_ADDR_LEVEL_MediaInVolume_sony; akdbgprt("node[AE_AMVolume]\n"); break;
			#endif
			case AE_NaviVolume: add = CRAM_ADDR_LEVEL_NaviVolume_sony; akdbgprt("node[AE_NaviVolume]\n"); break;
			case AE_PhoneVolume: add = CRAM_ADDR_LEVEL_PhoneVolume_sony; akdbgprt("node[AE_PhoneVolume]\n"); break;
			case AE_MediaOutVolumeFLR: add = CRAM_ADDR_LEVEL_MediaOutVolumeFLR_sony; akdbgprt("node[AE_MediaOutVolumeFLR]\n"); break;
			case AE_MediaOutVolumeRLR: add = CRAM_ADDR_LEVEL_MediaOutVolumeRLR_sony; akdbgprt("node[AE_MediaOutVolumeRLR]\n"); break;
			case AE_OutVolumeFLR: add = CRAM_ADDR_LEVEL_OutVolumeFLR_sony; akdbgprt("node[AE_OutVolumeFLR]\n"); break;
			case AE_OutVolumeRLR: add = CRAM_ADDR_LEVEL_OutVolumeRLR_sony; akdbgprt("node[AE_OutVolumeRLR]\n"); break;
			case AE_OutSWVolumeL: add = CRAM_ADDR_LEVEL_OutSWVolumeL_sony; akdbgprt("node[AE_OutSWVolumeL]\n"); break;
			case AE_OutSWVolumeR: add = CRAM_ADDR_LEVEL_OutSWVolumeR_sony; akdbgprt("node[AE_OutSWVolumeR]\n"); break;
			case AE_OutSWVolumeLR: add = CRAM_ADDR_LEVEL_OutSWVolumeLR_sony; akdbgprt("node[AE_OutSWVolumeLR]\n"); break;
			case AE_OutCenterVolume: add = CRAM_ADDR_LEVEL_OutCenterVolume_sony; akdbgprt("node[AE_OutCenterVolume]\n"); break;
			case AE_BeepVolume: add = CRAM_ADDR_LEVEL_BeepVolume_sony; akdbgprt("node[AE_BeepVolume]\n"); break;
			case AE_ChimeVolume: add = CRAM_ADDR_LEVEL_ChimeVolume_sony; akdbgprt("node[AE_ChimeVolume]\n"); break;
			case AE_RSEOutVolumeL: add = CRAM_ADDR_LEVEL_RSEOutVolumeL_sony; akdbgprt("node[AE_RSEOutVolumeL]\n"); break;
			case AE_RSEOutVolumeR: add = CRAM_ADDR_LEVEL_RSEOutVolumeR_sony; akdbgprt("node[AE_RSEOutVolumeR]\n"); break;
			case AE_RSEOutVolumeLR: add = CRAM_ADDR_LEVEL_RSEOutVolumeLR_sony; akdbgprt("node[AE_RSEOutVolumeLR]\n"); break;
			case AE_Media1Gain: add = CRAM_ADDR_LEVEL_Media1Gain_sony; akdbgprt("node[AE_Media1Gain]\n"); break;
			case AE_Media2Gain: add = CRAM_ADDR_LEVEL_Media2Gain_sony; akdbgprt("node[AE_Media2Gain]\n"); break;
			case AE_Media3Gain: add = CRAM_ADDR_LEVEL_Media3Gain_sony; akdbgprt("node[AE_Media3Gain]\n"); break;
			case AE_SVCDFLRGain: add = CRAM_ADDR_LEVEL_SSVGainFLR_sony; akdbgprt("node[AE_SVCDFLRGain]\n"); break;
			case AE_SVCDRLRGain: add = CRAM_ADDR_LEVEL_SSVGainRLR_sony; akdbgprt("node[AE_SVCDRLRGain]\n"); break;
			case AE_RSEPersonalGain: add = CRAM_ADDR_LEVEL_PersonalGain_sony; akdbgprt("node[AE_RSEPersonalGain]\n"); break;
			case AE_OutMuteFLR: add = CRAM_ADDR_LEVEL_OutMuteFLR_sony; akdbgprt("node[AE_OutMuteFLR]\n"); break;
			case AE_OutMuteRLR: add = CRAM_ADDR_LEVEL_OutMuteRLR_sony; akdbgprt("node[AE_OutMuteRLR]\n"); break;
			case AE_OutMuteSW: add = CRAM_ADDR_LEVEL_OutMuteSW_sony; akdbgprt("node[AE_OutMuteSW]\n"); break;
			case AE_OutMuteCenter: add = CRAM_ADDR_LEVEL_OutMuteCenter_sony; akdbgprt("node[AE_OutMuteCenter]\n"); break;
			case AE_BeepMute: add = CRAM_ADDR_LEVEL_BeepMute_sony; akdbgprt("node[AE_BeepMute]\n"); break;
			case AE_ChimeMute: add = CRAM_ADDR_LEVEL_ChimeMute_sony; akdbgprt("node[AE_ChimeMute]\n"); break;
			case AE_NaviMute: add = CRAM_ADDR_LEVEL_NaviMute_sony; akdbgprt("node[AE_NaviMute]\n"); break;
			case AE_PhoneMute: add = CRAM_ADDR_LEVEL_PhoneMute_sony; akdbgprt("node[AE_PhoneMute]\n"); break;
			case AE_RSEOutMute: add = CRAM_ADDR_LEVEL_RSEOutMute_sony; akdbgprt("node[AE_RSEOutMute]\n"); break;
			case AE_OutMixFL: add = CRAM_ADDR_VOL_IN1_OutMixFL_sony; akdbgprt("node[AE_OutMixFL]\n"); break;
			case AE_OutMixFR: add = CRAM_ADDR_VOL_IN1_OutMixFR_sony; akdbgprt("node[AE_OutMixFR]\n"); break;
			case AE_OutMixRL: add = CRAM_ADDR_VOL_IN1_OutMixRL_sony; akdbgprt("node[AE_OutMixRL]\n"); break;
			case AE_OutMixRR: add = CRAM_ADDR_VOL_IN1_OutMixRR_sony; akdbgprt("node[AE_OutMixRR]\n"); break;
			//FIX_ME: need to add more geq
			case AE_BmtGeq_B1: add = CRAM_ADDR_BAND1_BMTGEQ_sony; akdbgprt("node[AE_BmtGeq_B1]\n"); break;
			case AE_BmtGeq_B2: add = CRAM_ADDR_BAND2_BMTGEQ_sony; akdbgprt("node[AE_BmtGeq_B2]\n"); break;
			case AE_BmtGeq_B3: add = CRAM_ADDR_BAND3_BMTGEQ_sony; akdbgprt("node[AE_BmtGeq_B3]\n"); break;
			case AE_PersonalGEQ_B1: add = CRAM_ADDR_BAND1_PersonalGEQ_sony; akdbgprt("node[AE_PersonalGEQ_B1]\n"); break;
			case AE_PersonalGEQ_B2: add = CRAM_ADDR_BAND2_PersonalGEQ_sony; akdbgprt("node[AE_PersonalGEQ_B2]\n"); break;
			case AE_PersonalGEQ_B3: add = CRAM_ADDR_BAND3_PersonalGEQ_sony; akdbgprt("node[AE_PersonalGEQ_B3]\n"); break;
			case AE_PersonalGEQ_B4: add = CRAM_ADDR_BAND4_PersonalGEQ_sony; akdbgprt("node[AE_PersonalGEQ_B4]\n"); break;
			case AE_PersonalGEQ_B5: add = CRAM_ADDR_BAND5_PersonalGEQ_sony; akdbgprt("node[AE_PersonalGEQ_B5]\n"); break;
			case AE_PersonalGEQ_B6: add = CRAM_ADDR_BAND6_PersonalGEQ_sony; akdbgprt("node[AE_PersonalGEQ_B6]\n"); break;
	//		case AE_RseBmtGeq_B1: add = CRAM_ADDR_BAND1_RSEBMTGEQ; akdbgprt("node[AE_RseBmtGeq_B1]\n"); break;
	//		case AE_RseBmtGeq_B2: add = CRAM_ADDR_BAND2_RSEBMTGEQ; akdbgprt("node[AE_RseBmtGeq_B2]\n"); break;
	//		case AE_RseBmtGeq_B3: add = CRAM_ADDR_BAND3_RSEBMTGEQ; akdbgprt("node[AE_RseBmtGeq_B3]\n"); break;
			case AE_Navi_B1: add = CRAM_ADDR_BAND1_NaviGEQ_sony; akdbgprt("node[AE_Navi_B1]\n"); break;
			case AE_Navi_B2: add = CRAM_ADDR_BAND2_NaviGEQ_sony; akdbgprt("node[AE_Navi_B2]\n"); break;
			case AE_Navi_B3: add = CRAM_ADDR_BAND3_NaviGEQ_sony; akdbgprt("node[AE_Navi_B3]\n"); break;
		default:
			akdbgprt_err("\t%s: node[%d] is invalid value!\n", __FUNCTION__, node);
			add = 0xFFFF;
			break;
		}
	}
	else
	{
		switch(node) 
		{
		case AE_DCCutSW2: add = CRAM_ADDR_IN1_DCCutSW2; akdbgprt("node[AE_DCCutSW2]\n"); break;
		case AE_DeEmpSW2: add = CRAM_ADDR_IN1_DeEmpSW2; akdbgprt("node[AE_DeEmpSW2]\n"); break;
		case AE_CompressorSW2: add = CRAM_ADDR_IN1_CompressorSW2; akdbgprt("node[AE_CompressorSW2]\n"); break;
		case AE_BMTSW2: add = CRAM_ADDR_IN1_BMTSW2; akdbgprt("node[AE_BMTSW2]\n"); break;
		case AE_SoundModeSW2: add = CRAM_ADDR_IN1_SoundModeSW2; akdbgprt("node[AE_SoundModeSW2]\n"); break;
		case AE_LoudnessSW2: add = CRAM_ADDR_IN1_LoudnessSW2; akdbgprt("node[AE_LoudnessSW2]\n"); break;
		case AE_SurroundSW2: add = CRAM_ADDR_IN1_SurroundSW2; akdbgprt("node[AE_SurroundSW2]\n"); break;
		case AE_Out6SW2: add = CRAM_ADDR_IN1_Out6SW2; akdbgprt("node[AE_Out6SW2]\n"); break;
		case AE_MainEffectSW2: add = CRAM_ADDR_IN1_MainEffectSW2; akdbgprt("node[AE_MainEffectSW2]\n"); break;
		case AE_NaviGEQSW2: add = CRAM_ADDR_IN1_NaviGEQSW2; akdbgprt("node[AE_NaviGEQSW2]\n"); break;
		case AE_PhoneGEQSW2: add = CRAM_ADDR_IN1_PhoneGEQSW2; akdbgprt("node[AE_PhoneGEQSW2]\n"); break;
		case AE_RSEDCCutSW2: add = CRAM_ADDR_IN1_RSEDCCutSW2; akdbgprt("node[AE_RSEDCCutSW2]\n"); break;
		case AE_RSEDeEmpSW2: add = CRAM_ADDR_IN1_RSEDeEmpSW2; akdbgprt("node[AE_RSEDeEmpSW2]\n"); break;
		case AE_RSEBMTSW2: add = CRAM_ADDR_IN1_RSEBMTSW2; akdbgprt("node[AE_RSEBMTSW2]\n"); break;
		case AE_Out5SW4: add = CRAM_ADDR_IN1_Out5SW4; akdbgprt("node[AE_Out5SW4]\n"); break;
		case AE_MediaInputSW4: add = CRAM_ADDR_IN1_MediaInputSW4; akdbgprt("node[AE_MediaInputSW4]\n"); break;
		case AE_SoundModeSW4: add = CRAM_ADDR_IN1_SoundModeSW4; akdbgprt("node[AE_SoundModeSW4]\n"); break;
		case AE_MediaInVolume: add = CRAM_ADDR_LEVEL_MediaInVolume; akdbgprt("node[AE_MediaInVolume]\n"); break;
		#ifdef SEPERATE_VOLUME_TABLE
		case AE_FMVolume: add = CRAM_ADDR_LEVEL_MediaInVolume; akdbgprt("node[AE_FMVolume]\n"); break;
		case AE_AMVolume: add = CRAM_ADDR_LEVEL_MediaInVolume; akdbgprt("node[AE_AMVolume]\n"); break;
		#endif
		case AE_NaviVolume: add = CRAM_ADDR_LEVEL_NaviVolume; akdbgprt("node[AE_NaviVolume]\n"); break;
		case AE_PhoneVolume: add = CRAM_ADDR_LEVEL_PhoneVolume; akdbgprt("node[AE_PhoneVolume]\n"); break;
		case AE_MediaOutVolumeFLR: add = CRAM_ADDR_LEVEL_MediaOutVolumeFLR; akdbgprt("node[AE_MediaOutVolumeFLR]\n"); break;
		case AE_MediaOutVolumeRLR: add = CRAM_ADDR_LEVEL_MediaOutVolumeRLR; akdbgprt("node[AE_MediaOutVolumeRLR]\n"); break;
		case AE_OutVolumeFLR: add = CRAM_ADDR_LEVEL_OutVolumeFLR; akdbgprt("node[AE_OutVolumeFLR]\n"); break;
		case AE_OutVolumeRLR: add = CRAM_ADDR_LEVEL_OutVolumeRLR; akdbgprt("node[AE_OutVolumeRLR]\n"); break;
		case AE_OutSWVolumeL: add = CRAM_ADDR_LEVEL_OutSWVolumeL; akdbgprt("node[AE_OutSWVolumeL]\n"); break;
		case AE_OutSWVolumeR: add = CRAM_ADDR_LEVEL_OutSWVolumeR; akdbgprt("node[AE_OutSWVolumeR]\n"); break;
		case AE_OutSWVolumeLR: add = CRAM_ADDR_LEVEL_OutSWVolumeLR; akdbgprt("node[AE_OutSWVolumeLR]\n"); break;
		case AE_OutCenterVolume: add = CRAM_ADDR_LEVEL_OutCenterVolume; akdbgprt("node[AE_OutCenterVolume]\n"); break;
		case AE_BeepVolume: add = CRAM_ADDR_LEVEL_BeepVolume; akdbgprt("node[AE_BeepVolume]\n"); break;
		case AE_ChimeVolume: add = CRAM_ADDR_LEVEL_ChimeVolume; akdbgprt("node[AE_ChimeVolume]\n"); break;
		case AE_RSEOutVolumeL: add = CRAM_ADDR_LEVEL_RSEOutVolumeL; akdbgprt("node[AE_RSEOutVolumeL]\n"); break;
		case AE_RSEOutVolumeR: add = CRAM_ADDR_LEVEL_RSEOutVolumeR; akdbgprt("node[AE_RSEOutVolumeR]\n"); break;
		case AE_RSEOutVolumeLR: add = CRAM_ADDR_LEVEL_RSEOutVolumeLR; akdbgprt("node[AE_RSEOutVolumeLR]\n"); break;
		case AE_Media1Gain: add = CRAM_ADDR_LEVEL_Media1Gain; akdbgprt("node[AE_Media1Gain]\n"); break;
		case AE_Media2Gain: add = CRAM_ADDR_LEVEL_Media2Gain; akdbgprt("node[AE_Media2Gain]\n"); break;
		case AE_Media3Gain: add = CRAM_ADDR_LEVEL_Media3Gain; akdbgprt("node[AE_Media3Gain]\n"); break;
		case AE_SVCDFLRGain: add = CRAM_ADDR_LEVEL_SSVGainFLR; akdbgprt("node[AE_SVCDFLRGain]\n"); break;
		case AE_SVCDRLRGain: add = CRAM_ADDR_LEVEL_SSVGainRLR; akdbgprt("node[AE_SVCDRLRGain]\n"); break;
		case AE_RSEPersonalGain: add = CRAM_ADDR_LEVEL_PersonalGain; akdbgprt("node[AE_RSEPersonalGain]\n"); break;
		case AE_OutMuteFLR: add = CRAM_ADDR_LEVEL_OutMuteFLR; akdbgprt("node[AE_OutMuteFLR]\n"); break;
		case AE_OutMuteRLR: add = CRAM_ADDR_LEVEL_OutMuteRLR; akdbgprt("node[AE_OutMuteRLR]\n"); break;
		case AE_OutMuteSW: add = CRAM_ADDR_LEVEL_OutMuteSW; akdbgprt("node[AE_OutMuteSW]\n"); break;
		case AE_OutMuteCenter: add = CRAM_ADDR_LEVEL_OutMuteCenter; akdbgprt("node[AE_OutMuteCenter]\n"); break;
		case AE_BeepMute: add = CRAM_ADDR_LEVEL_BeepMute; akdbgprt("node[AE_BeepMute]\n"); break;
		case AE_ChimeMute: add = CRAM_ADDR_LEVEL_ChimeMute; akdbgprt("node[AE_ChimeMute]\n"); break;
		case AE_NaviMute: add = CRAM_ADDR_LEVEL_NaviMute; akdbgprt("node[AE_NaviMute]\n"); break;
		case AE_PhoneMute: add = CRAM_ADDR_LEVEL_PhoneMute; akdbgprt("node[AE_PhoneMute]\n"); break;
		case AE_RSEOutMute: add = CRAM_ADDR_LEVEL_RSEOutMute; akdbgprt("node[AE_RSEOutMute]\n"); break;
		case AE_OutMixFL: add = CRAM_ADDR_VOL_IN1_OutMixFL; akdbgprt("node[AE_OutMixFL]\n"); break;
		case AE_OutMixFR: add = CRAM_ADDR_VOL_IN1_OutMixFR; akdbgprt("node[AE_OutMixFR]\n"); break;
		case AE_OutMixRL: add = CRAM_ADDR_VOL_IN1_OutMixRL; akdbgprt("node[AE_OutMixRL]\n"); break;
		case AE_OutMixRR: add = CRAM_ADDR_VOL_IN1_OutMixRR; akdbgprt("node[AE_OutMixRR]\n"); break;
		//FIX_ME: need to add more geq
		case AE_BmtGeq_B1: add = CRAM_ADDR_BAND1_BMTGEQ; akdbgprt("node[AE_BmtGeq_B1]\n"); break;
		case AE_BmtGeq_B2: add = CRAM_ADDR_BAND2_BMTGEQ; akdbgprt("node[AE_BmtGeq_B2]\n"); break;
		case AE_BmtGeq_B3: add = CRAM_ADDR_BAND3_BMTGEQ; akdbgprt("node[AE_BmtGeq_B3]\n"); break;
		case AE_PersonalGEQ_B1: add = CRAM_ADDR_BAND1_PersonalGEQ; akdbgprt("node[AE_PersonalGEQ_B1]\n"); break;
		case AE_PersonalGEQ_B2: add = CRAM_ADDR_BAND2_PersonalGEQ; akdbgprt("node[AE_PersonalGEQ_B2]\n"); break;
		case AE_PersonalGEQ_B3: add = CRAM_ADDR_BAND3_PersonalGEQ; akdbgprt("node[AE_PersonalGEQ_B3]\n"); break;
		case AE_PersonalGEQ_B4: add = CRAM_ADDR_BAND4_PersonalGEQ; akdbgprt("node[AE_PersonalGEQ_B4]\n"); break;
		case AE_PersonalGEQ_B5: add = CRAM_ADDR_BAND5_PersonalGEQ; akdbgprt("node[AE_PersonalGEQ_B5]\n"); break;
		case AE_PersonalGEQ_B6: add = CRAM_ADDR_BAND6_PersonalGEQ; akdbgprt("node[AE_PersonalGEQ_B6]\n"); break;
		case AE_RseBmtGeq_B1: add = CRAM_ADDR_BAND1_RSEBMTGEQ; akdbgprt("node[AE_RseBmtGeq_B1]\n"); break;
		case AE_RseBmtGeq_B2: add = CRAM_ADDR_BAND2_RSEBMTGEQ; akdbgprt("node[AE_RseBmtGeq_B2]\n"); break;
		case AE_RseBmtGeq_B3: add = CRAM_ADDR_BAND3_RSEBMTGEQ; akdbgprt("node[AE_RseBmtGeq_B3]\n"); break;
		case AE_Navi_B1: add = CRAM_ADDR_BAND1_NaviGEQ; akdbgprt("node[AE_Navi_B1]\n"); break;
		case AE_Navi_B2: add = CRAM_ADDR_BAND2_NaviGEQ; akdbgprt("node[AE_Navi_B2]\n"); break;
		case AE_Navi_B3: add = CRAM_ADDR_BAND3_NaviGEQ; akdbgprt("node[AE_Navi_B3]\n"); break;
		default:
			akdbgprt_err("\t%s: node[%d] is invalid value!\n", __FUNCTION__, node);
			add = 0xFFFF;
			break;
	}
	}

	akdbgprt("[AK7739] %s node[%d] add[0x%0X]\n", __FUNCTION__, node, add);
	return add;
}

int set_ae_sel2selector(
		ak7739_priv *pData,
		AE_NODETYPE node,
		AE_SEL2IN sel)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int dspno = DSPNUMTYPE_DSP2;
	int add;


	akdbgprt("[AK7739] %s node[%d] sel[%d]\n", __FUNCTION__, node, sel);

	if(node < AE_DCCutSW2 ||  node > AE_RSEBMTSW2) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Node\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	add = ae_nodeToCramAdd(node);

	if(sel == AE_SEL2IN_1)
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(AE_Cram_Sel2_Off), AE_Cram_Sel2_Off);
	else if(sel == AE_SEL2IN_2)
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(AE_Cram_Sel2_On), AE_Cram_Sel2_On);
	else {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	return ret;
}

int set_ae_sel4selector(
		ak7739_priv *pData,
		AE_NODETYPE node,
		AE_SEL4IN sel)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int dspno = DSPNUMTYPE_DSP2;
	int add;


	akdbgprt("[AK7739] %s node[%d] sel[%d]\n", __FUNCTION__, node, sel);

	if(node < AE_MediaInputSW4 ||  node > AE_Out5SW4) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Node\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	add = ae_nodeToCramAdd(node);

	if(sel == AE_SEL4IN_1)
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(AE_Cram_Sel4_1), AE_Cram_Sel4_1);
	else if(sel == AE_SEL4IN_2)
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(AE_Cram_Sel4_2), AE_Cram_Sel4_2);
	else if(sel == AE_SEL4IN_3)
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(AE_Cram_Sel4_3), AE_Cram_Sel4_3);
	else if(sel == AE_SEL4IN_4)
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(AE_Cram_Sel4_4), AE_Cram_Sel4_4);
	else {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
}

	return ret;
}

//0~31 control (31 = 0dB, 0 = -145dB)
//Fader 12dB ~ -145dB 
int set_ae_volume(
		ak7739_priv *pData,
		AE_NODETYPE node,
		unsigned int volIndex)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int i;
	int volumedB;
	int faderValue;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramFader[AE_FADER_CRAMSIZE] = {0x00};
	int add;


	akdbgprt("[AK7739] %s node[%d] Index[%d]\n", __FUNCTION__, node, volIndex);

	if(node < AE_MediaInVolume ||  node > AE_RSEPersonalGain) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid node\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}
	else if(volIndex > AE_VOL_INDEX_MAX) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	add = ae_nodeToCramAdd(node);

	if(volIndex == 0) {
		volumedB = 0;
		faderValue = 0x00000000;
	}
	else {
		switch(node) {
			case AE_MediaOutVolumeFLR: volumedB = mediaOutVolTable[volIndex]; break;
			case AE_MediaOutVolumeRLR: volumedB = mediaOutVolTable[volIndex]; break;
			#ifdef SEPERATE_VOLUME_TABLE
			case AE_MediaInVolume: volumedB = commonVolTable[volIndex]; break;
			case AE_FMVolume: volumedB = FMVolTable[volIndex]; break;
			case AE_AMVolume: volumedB = AMVolTable[volIndex]; break;
			#else
			case AE_MediaInVolume: volumedB = commonVolTable[volIndex]; break;
			#endif
			case AE_NaviVolume: volumedB = commonVolTable[volIndex]; break;
			case AE_PhoneVolume: volumedB = phoneVolTable[volIndex]; break;
			case AE_OutVolumeFLR: volumedB = commonVolTable[volIndex]; break;
			case AE_OutVolumeRLR: volumedB = commonVolTable[volIndex]; break;
			case AE_OutSWVolumeL: volumedB = commonVolTable[volIndex]; break;
			case AE_OutSWVolumeR: volumedB = commonVolTable[volIndex]; break;
			case AE_OutSWVolumeLR: volumedB = commonVolTable[volIndex]; break;
			case AE_OutCenterVolume: volumedB = commonVolTable[volIndex]; break;
			case AE_BeepVolume: volumedB = commonVolTable[volIndex]; break;
			case AE_ChimeVolume: volumedB = commonVolTable[volIndex]; break;
			case AE_RSEOutVolumeL: volumedB = commonVolTable[volIndex]; break;
			case AE_RSEOutVolumeR: volumedB = commonVolTable[volIndex]; break;
			case AE_RSEOutVolumeLR: volumedB = commonVolTable[volIndex]; break;
			case AE_Media1Gain: volumedB = commonVolTable[volIndex]; break;
			case AE_Media2Gain: volumedB = commonVolTable[volIndex]; break;
			case AE_Media3Gain: volumedB = commonVolTable[volIndex]; break;
			case AE_SVCDFLRGain: volumedB = SVCDVolTable[volIndex]; break;
			case AE_SVCDRLRGain: volumedB = SVCDVolTable[volIndex]; break;
			case AE_RSEPersonalGain: volumedB = commonVolTable[volIndex]; break;
			default:
				akdbgprt_err("\t%s: volumetype[%d] is invalid value!\n", __FUNCTION__, node);
				break;
		}
		faderValue = ceil((double)AE_FADER_MUTE * pow((double)10, (double)(volumedB+AE_FADER_0DB)/(double)20));//rounded up
	}
	akdbgprt("[AK7739] %s faderValue[0x%08X] volumedB[%d]\n", __FUNCTION__, faderValue, volumedB);

	for ( i = 0 ; i < AE_FADER_CRAMSIZE; i ++ )
		cramFader[i] = AE_Cram_Fader_0dB[i];

	ae_userValue_to_cramWord(cramFader, faderValue);
	ret = ak7739_write_cram(ak7739, dspno, add, sizeof(cramFader), cramFader);

	return ret;
}

int set_ae_mute(
		ak7739_priv *pData,
		AE_NODETYPE node,
		AE_MUTEONOFF mute)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int i;
	int volumedB;
	int faderValue;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramFader[AE_FADER_CRAMSIZE] = {0x00};
	int add;


	akdbgprt("[AK7739] %s mute[%d]\n", __FUNCTION__, mute);

	if(node < AE_OutMuteFLR ||  node > AE_RSEOutMute) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Node\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}
	else if(mute > AE_MUTE_OFF) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	add = ae_nodeToCramAdd(node);

	if(mute == AE_MUTE_ON) {
		//Mute
		faderValue = 0x00000000;
		volumedB = -145;
	}
	else if(mute == AE_MUTE_OFF) {
		//0dB
		faderValue = 0x20000000;
		volumedB = 0;
	}
	else {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	akdbgprt("[AK7739] %s faderValue[0x%08X] volumedB[%d]\n", __FUNCTION__, faderValue, volumedB);
//apply for sony sound
#if 1
	for ( i = 0 ; i < AE_FADER_CRAMSIZE; i ++ )
		cramFader[i] = AE_Cram_Mute_0dB[i];
#else
	for ( i = 0 ; i < AE_FADER_CRAMSIZE; i ++ )
		cramFader[i] = AE_Cram_Fader_0dB[i];
#endif
	ae_userValue_to_cramWord(cramFader, faderValue);
	ret = ak7739_write_cram(ak7739, dspno, add, sizeof(cramFader), cramFader);

	return ret;
}

int set_ae_bassMidTreble(
		ak7739_priv *pData,
		AE_BMT_TYPE bassIndex,
		AE_BMT_TYPE midIndex,
		AE_BMT_TYPE treIndex)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
#if 0
	int i;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramBassMidTreble[AE_BASSMIDTRABLE_CRAMSIZE] = {0x00};
#endif

	akdbgprt("[AK7739] %s bassIndex[%d] midIndex[%d] treIndexl[%d]\n", __FUNCTION__, bassIndex, midIndex, treIndex);

	if(bassIndex > AE_BMT_P7 || midIndex > AE_BMT_P7 || treIndex > AE_BMT_P7) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}
	//apply for sony sound
	if(r_sonysound)
	{
		ret = set_ae_1bandGeq(ak7739, AE_BmtGeq_B1, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, 500, (double)bassIndex-7, 0.7);
		if( ret >= 0 )
			ret = set_ae_1bandGeq(ak7739, AE_BmtGeq_B2, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, 1600, (double)midIndex-7, 1.5);
		if( ret >= 0 )
			ret = set_ae_1bandGeq(ak7739, AE_BmtGeq_B3, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, 5952, (double)treIndex-7, 0.7);
	}
	else
	{
	ret = set_ae_1bandGeq(ak7739, AE_BmtGeq_B1, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, 120, (double)bassIndex-7, 0.6);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_BmtGeq_B2, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, 880, (double)midIndex-7, 0.8);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_BmtGeq_B3, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, 8660, (double)treIndex-7, 0.6);
	}

	if( ret < 0 ) {
		akdbgprt_err("\t[AK7739] %s(%d) set treble fail\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	return ret;
}

int set_ae_personalGeqOn(ak7739_priv *pData)
	{
	struct ak7739_priv *ak7739 = pData;
	int ret;


	akdbgprt("[AK7739] %s turn on sound mode as personal\n", __FUNCTION__);

	ret = set_ae_volume(ak7739, AE_RSEPersonalGain, AE_VOL_INDEX_MAX); //0dB
	if( ret < 0 )
		akdbgprt_err("\t[AK7739] %s(%d) set personal EQ gain fail\n", __FUNCTION__,__LINE__);

	ret = set_ae_sel4selector(ak7739, AE_SoundModeSW4, AE_SEL4IN_1); //select personal mode
	if( ret >= 0 )
		ret = set_ae_sel2selector(ak7739, AE_SoundModeSW2, AE_SEL2IN_2); //enable sound mode
	if( ret < 0 ) {
		akdbgprt_err("\t[AK7739] %s(%d) enable sound mode fail\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_PersonalGEQ_B1, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, geq1.band_freq, geq1.gain, geq1.band_factor);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_PersonalGEQ_B2, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, geq2.band_freq, geq2.gain, geq2.band_factor);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_PersonalGEQ_B3, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, geq3.band_freq, geq3.gain, geq3.band_factor);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_PersonalGEQ_B4, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, geq4.band_freq, geq4.gain, geq4.band_factor);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_PersonalGEQ_B5, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, geq5.band_freq, geq5.gain, geq5.band_factor);
	if( ret >= 0 )
		ret = set_ae_1bandGeq(ak7739, AE_PersonalGEQ_B6, AE_GEQ_FILTER_PEQ, AE_PRECISION_S, geq6.band_freq, geq6.gain, geq6.band_factor);
	if( ret < 0 ) {
		akdbgprt_err("\t[AK7739] %s(%d) set sound mode fail\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	return ret;
	}

int set_ae_personalGeqOff(ak7739_priv *pData)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;

	akdbgprt("[AK7739] %s turn off sound mode as personal\n", __FUNCTION__);

	ret = set_ae_sel2selector(ak7739, AE_SoundModeSW2, AE_SEL2IN_1); //disable sound mode
	if( ret < 0 ) {
		akdbgprt_err("\t[AK7739] %s(%d) enable sound mode fail\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	return ret;
}

int set_ae_loudness(
		ak7739_priv *pData,
		AE_LD_TYPE b1Index,
		AE_LD_TYPE b2Index,
		AE_LD_TYPE b3Index,
		AE_LD_TYPE b4Index,
		AE_LD_TYPE b5Index,
		AE_LD_TYPE b6Index,
		AE_LD_TYPE b7Index)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int i;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramLoudness[AE_LOUDNESS_CRAMSIZE] = {0x00};
	int cramDivLen = 40; //bytes
	int cramDivAdd = 10; //Word


	akdbgprt("[AK7739] %s b1Idx[%d] b2Idx[%d] b3Idx[%d] b4Idx[%d] b5Idx[%d] b6Idx[%d] b7Idx[%d]\n",
		__FUNCTION__, b1Index, b2Index, b3Index, b4Index, b5Index, b6Index, b7Index);

	if(b1Index > AE_LD_16 || b2Index > AE_LD_16 || b3Index > AE_LD_16 || b4Index > AE_LD_16
		|| b5Index > AE_LD_16 || b6Index > AE_LD_16 || b7Index > AE_LD_16) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	for ( i = 0 ; i < AE_GEQ_1BAND_CRAMSIZE ; i ++ ) 
	{
		cramLoudness[i] = AE_Cram_LD_Band1[b1Index][i];
		cramLoudness[AE_GEQ_1BAND_CRAMSIZE+i] = AE_Cram_LD_Band2[b2Index][i];
		cramLoudness[AE_GEQ_1BAND_CRAMSIZE*2+i] = AE_Cram_LD_Band3[b3Index][i];
		cramLoudness[AE_GEQ_1BAND_CRAMSIZE*3+i] = AE_Cram_LD_Band4[b4Index][i];
		cramLoudness[AE_GEQ_1BAND_CRAMSIZE*4+i] = AE_Cram_LD_Band5[b5Index][i];
		cramLoudness[AE_GEQ_1BAND_CRAMSIZE*5+i] = AE_Cram_LD_Band6[b6Index][i];
		cramLoudness[AE_GEQ_1BAND_CRAMSIZE*6+i] = AE_Cram_LD_Band7[b7Index][i];
	}

	//AE_LOUDNESS_CRAMSIZE 140bytes //Cram run write cannot exceed more than 64bytes
	if(r_sonysound)
	{
		ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ_sony, cramDivLen, cramLoudness);
		ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ_sony+cramDivAdd, cramDivLen, &cramLoudness[cramDivLen]);
		ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ_sony+cramDivAdd*2, cramDivLen, &cramLoudness[cramDivLen*2]);
		ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ_sony+cramDivAdd*3, AE_LOUDNESS_CRAMSIZE - cramDivLen*3, &cramLoudness[cramDivLen*3]);
	}
	else
	{
	ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ, cramDivLen, cramLoudness);
	ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ+cramDivAdd, cramDivLen, &cramLoudness[cramDivLen]);
	ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ+cramDivAdd*2, cramDivLen, &cramLoudness[cramDivLen*2]);
	ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BAND1_LoudnessGEQ+cramDivAdd*3, AE_LOUDNESS_CRAMSIZE - cramDivLen*3, &cramLoudness[cramDivLen*3]);
	}

	return ret;
}

int set_ae_fadeNbalance(
		ak7739_priv *pData,
		AE_FADE_TYPE fadeIndex,
		AE_BALANCE_TYPE balIndex)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int i, n;
	int volumedB;
	int faderValue;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramFader[AE_FADER_CRAMSIZE] = {0x00};


	akdbgprt("[AK7739] %s fadeIndex[%d]  balIndex[%d]\n", __FUNCTION__, fadeIndex, balIndex);
	akdbgprt("[AK7739] AE_fadeNbalance[%d, %d, %d, %d]\n",
			AE_fadeNbalance[fadeIndex][balIndex][0], AE_fadeNbalance[fadeIndex][balIndex][1],
			AE_fadeNbalance[fadeIndex][balIndex][2], AE_fadeNbalance[fadeIndex][balIndex][3]);

	if(fadeIndex > AE_FADE_MAX || balIndex > AE_BALANCE_MAX) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	for(n=0; n<AE_FADEBAL_CH_MAX; n++) {
		volumedB = AE_fadeNbalance[fadeIndex][balIndex][n];
		faderValue = ceil((double)AE_FADER_MUTE * pow((double)10, (double)(volumedB+AE_FADER_0DB)/(double)20));//rounded up

		akdbgprt("[AK7739] %s faderValue[0x%08X] volumedB[%d]\n", __FUNCTION__, faderValue, volumedB);

		for ( i = 0 ; i < AE_FADER_CRAMSIZE; i ++ )
			cramFader[i] = AE_Cram_Fader_0dB[i];

		ae_userValue_to_cramWord(cramFader, faderValue);

		if(r_sonysound)
		{
			if(n == 0)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceFL_sony, sizeof(cramFader), cramFader);
			else if(n == 1)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceFR_sony, sizeof(cramFader), cramFader);
			else if(n == 2)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceRL_sony, sizeof(cramFader), cramFader);
			else if(n == 3)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceRR_sony, sizeof(cramFader), cramFader);
		}
		else
		{
		if(n == 0)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceFL, sizeof(cramFader), cramFader);
		else if(n == 1)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceFR, sizeof(cramFader), cramFader);
		else if(n == 2)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceRL, sizeof(cramFader), cramFader);
		else if(n == 3)
			ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_LEVEL_FadeBalanceRR, sizeof(cramFader), cramFader);
		}
		}
	
	return ret;
		}

int set_ae_outMix(
		ak7739_priv *pData,
		AE_NODETYPE node,
		int mediaGain,
		int naviGain,
		int phoneGain,
		int BeepGain,
		int signGain)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int i, n;
	int volumedB;
	int gainValue;
	int add;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramOutMix[AE_OUTMIX_CRAMSIZE] = {0x00};


	akdbgprt("[AK7739] %s mediaGain[%d] naviGain[%d] phoneGain[%d] BeepGain[%d] signGain[%d]\n", __FUNCTION__, mediaGain, naviGain, phoneGain, BeepGain, signGain);

	if(node < AE_OutMixFL ||  node > AE_OutMixRR) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Node\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	} else if((mediaGain > 0) || (naviGain > 0) || (phoneGain > 0) || (BeepGain > 0) || (signGain > 0)) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	} else if((mediaGain < -145) || (naviGain < -145) || (phoneGain < -145) || (BeepGain < -145) || (signGain < -145)) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
		}

	add = ae_nodeToCramAdd(node);

	n=0;
	for ( i = 0 ; i < 5; i ++ ) {
		if(i == 0) volumedB = mediaGain;
		else if(i == 1) volumedB = naviGain;
		else if(i == 2) volumedB = phoneGain;
		else if(i == 3) volumedB = BeepGain;
		else if(i == 4) volumedB = signGain;

		if(volumedB == -145)
			gainValue = 0x00000000;
		else
			gainValue = ceil((double)AE_FADER_MUTE * pow((double)10, (double)(volumedB+AE_FADER_0DB)/(double)20));//rounded up

		akdbgprt("[AK7739] %s gainValue[0x%08X] volumedB[%d]\n", __FUNCTION__, gainValue, volumedB);

		cramOutMix[n] = (unsigned char)((0xFF000000 & gainValue) >> 24);
		n++;
		cramOutMix[n] = (unsigned char)((0xFF0000 & gainValue) >> 16);
		n++;
		cramOutMix[n] = (unsigned char)((0xFF00 & gainValue) >> 8);
		n++;
		cramOutMix[n] = (unsigned char)(0xFF & gainValue);
		n++;
	}
	
	ret = ak7739_write_cram(ak7739, dspno, add, sizeof(cramOutMix), cramOutMix);

	return ret;
}

int set_ae_beepControl(
		ak7739_priv *pData,
		AE_BEEP_CONTROL_TYPE control)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int dspno = DSPNUMTYPE_DSP2;
	int controlValue;
	unsigned char cramBeepControl[AE_CRAM_WORD_SIZE] = {0x00};


	akdbgprt("[AK7739] %s control[%d]\n", __FUNCTION__, control);

	if(control < AE_BEEP_START ||  control > AE_BEEP_STOP) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Value\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	if(control == AE_BEEP_START)
		controlValue = AE_BEEP_CONTROL_START;
	else if(control == AE_BEEP_SOFT_STOP)
		controlValue = AE_BEEP_CONTROL_SOFT_STOP;
	else if(control == AE_BEEP_STOP)
		controlValue = AE_BEEP_CONTROL_STOP;

	ae_userValue_to_cramWord(cramBeepControl, controlValue);
	if(r_sonysound)
		ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BEEP_TRIGGER_Beep_sony, sizeof(cramBeepControl), cramBeepControl);
	else
	ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_BEEP_TRIGGER_Beep, sizeof(cramBeepControl), cramBeepControl);

	return ret;
}

int set_ae_beepOption(
		ak7739_priv *pData,
		unsigned int freq,
		unsigned int attackTimeMs,
		unsigned int decayTimeMs,
		unsigned int dutyCyclePer,
		unsigned int candenceMs,
		unsigned int repeats)
{
	ak7739_priv *ak7739 = pData;
	int ret;
	int n, i;
	double freqValue;
	int freqMSBValue;
	int freqLSBValue;
	int ats=0, dcs=0, ta=0, tst=0, tsc=0, reps=0;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramBeep[AE_BEEP_WORD_CNT][AE_CRAM_WORD_SIZE] = {0x00};


	akdbgprt("[AK7739] %s freq[%d] repeats[%d]\n", __FUNCTION__, freq, repeats);


	for ( n = 0 ; n < AE_BEEP_WORD_CNT; n ++ ) {
		for ( i = 0 ; i < AE_CRAM_WORD_SIZE; i ++ ) {
			cramBeep[n][i] = AE_Cram_Beep[n][i];
		}
	}

	//Freq
	if(freq < 20 ||  freq > 20000) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid freq Value, set default as 1kHz\n", __FUNCTION__,__LINE__);
		freq = 1000;
	}

	freqValue = cos(2 * M_PI * freq / ak7739->fs);
	freqMSBValue = fix32H(freqValue, 0);
	freqLSBValue = fix32L(freqValue, 0);
	akdbgprt("[AK7739] %s freqValue[%lf] freqMSBValue[0x%08X] freqLSBValue[0x%08X]\n", __FUNCTION__, freqValue, freqMSBValue, freqLSBValue);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_FREQ_MSB32][0], freqMSBValue);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_FREQ_LSB32][0], freqLSBValue);

	//Attack Step
	if(attackTimeMs <= 0 || attackTimeMs > (32767000 / ak7739->fs)) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid attackTimeMs Value, set default as 10ms\n", __FUNCTION__,__LINE__);
		attackTimeMs = 10;
	}

	ats = fix32((double)1000 / (double)(attackTimeMs * ak7739->fs), 0);
	akdbgprt("[AK7739] %s attackTimeMs[%d] ats[0x%08X]\n", __FUNCTION__, attackTimeMs, ats);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_ATTACK_STEP][0], ats);

	//Decay Step
	if(decayTimeMs <= 0 || decayTimeMs > (32767000 / ak7739->fs)) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid decayTimeMs Value, set default as 10ms\n", __FUNCTION__,__LINE__);
		decayTimeMs = 10;
	}

	dcs = fix32((double)-1000 / (double)(decayTimeMs * ak7739->fs), 0);
	akdbgprt("[AK7739] %s decayTimeMs[%d] dcs[0x%08X]\n", __FUNCTION__, decayTimeMs, dcs);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_DECAY_STEP][0], dcs);

	//Attack Time
	ta = ((attackTimeMs * ak7739->fs) / 1000)*256*256;
	akdbgprt("[AK7739] %s attackTimeMs[%d] ta[0x%08X]\n", __FUNCTION__, attackTimeMs, ta);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_ATTACK_TIME][0], ta);

	//Sustain Time
	if(candenceMs <= 0 || candenceMs > (65535000 / ak7739->fs)) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid candenceMs Value, set default as 100ms\n", __FUNCTION__,__LINE__);
		candenceMs = 100;
	}

	if(dutyCyclePer <= 0) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid dutyCyclePer Value, set default as 100\n", __FUNCTION__,__LINE__);
		dutyCyclePer = 100;
	}

	tst = (candenceMs * dutyCyclePer * ak7739->fs / 100000)*256*256;
	akdbgprt("[AK7739] %s dutyCyclePer[%d] dcs[0x%08X]\n", __FUNCTION__, candenceMs, tst);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_SUSTAIN_TIME][0], tst);

	//Sound Candence
	tsc= (candenceMs * ak7739->fs / 1000)*256*256;
	akdbgprt("[AK7739] %s candenceMs[%d] tsc[0x%08X]\n", __FUNCTION__, candenceMs, tsc);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_SOUND_CANDENCE][0], tsc);

	//Repeat Time
	if(repeats <= 0) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid repeats Value, set default as 1\n", __FUNCTION__,__LINE__);
		repeats = 1;
	}

	reps = (repeats)*256*256;
	akdbgprt("[AK7739] %s repeats[%d] reps[0x%08X]\n", __FUNCTION__, repeats, reps);
	ae_userValue_to_cramWord(&cramBeep[AE_BEEP_OPT_REPEAT_TIME][0], reps);

	//Beep Option Write to DSP
	if(r_sonysound)
		ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_Beep_sony, sizeof(cramBeep), &cramBeep[0][0]);
	else
	ret = ak7739_write_cram(ak7739, dspno, CRAM_ADDR_Beep, sizeof(cramBeep), &cramBeep[0][0]);

	return ret;
}

int set_ae_1bandGeq(
		ak7739_priv *pData,
		AE_NODETYPE node,
		AE_GEQ_FILTER_TYPE filter,
		AE_PRECISION_TYPE precision,
		unsigned int freq,
		double gain,
		double qFactor)
{
	ak7739_priv *ak7739 = pData;
	int ret;
	AE_GEQ_FILTER_TYPE filterValue = filter;
	AE_PRECISION_TYPE precisionValue = precision;
	unsigned int fc = freq;
	double gb = gain;
	double Q = qFactor;
	int shiftValue = 1;

	double w, sn, cs, alpha, A, beta;
	double p0, p1, a0, a1, a2, b1, b2;
	int c0, c1, c2, c3, c4, c5, c6, c7, c8, c9;

	int add;
	int dspno = DSPNUMTYPE_DSP2;
	unsigned char cramGeqSingle[AE_GEQ_S_PARAM_MAX][AE_CRAM_WORD_SIZE] = {0x00};
	unsigned char cramGeqDouble[AE_GEQ_D_PARAM_MAX][AE_CRAM_WORD_SIZE] = {0x00};


	akdbgprt("[AK7739] %s filter[%d] fc[%d] gain[%lf] qFactor[%lf]\n", __FUNCTION__, filter, fc, gain, qFactor);

	//FIX_ME: need to add more geq
	if(node < AE_BmtGeq_B1 ||  node > AE_Navi_B3) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid Node\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	if(precisionValue == AE_PRECISION_D) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid precision\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	//FC
	if(fc < 20 ||  fc > 20000) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid fc Value, set default as THRU\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	//qFactor
	if(Q < 0.1 || Q > 100) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid qFactor Value, set default as 0.7\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	//gain
	if(gb < -15 || gb > 15) {
		akdbgprt_err("\t[AK7739] %s(%d) Invalid gain Value, set default as 0\n", __FUNCTION__,__LINE__);
		return AK7739_ERROR_EINVAL;
	}

	add = ae_nodeToCramAdd(node);

	w = 2 * M_PI * fc / ak7739->fs;
	sn = sin(w);
	cs = cos(w);
	alpha = sn / (2 * Q);
	A = pow((double)10, (double)(gb)/(double)40);//rounded up
//	akdbgprt("[AK7739] %s JJH fc %d fs: %d \n", __FUNCTION__, fc, ak7739->fs);
//	akdbgprt("[AK7739] %s JJH CAL %d [%.3f] [%.3f] [%.3f] [%.3f] \n", __FUNCTION__, add,w, sn,cs,alpha,A);

	switch(filterValue) {
		case AE_GEQ_FILTER_THRU:
			a2 = 0;
			a1 = 0;
			b2 = 0;
			b1 = 0;
			a0 = 1;
			break;
		case AE_GEQ_FILTER_LPF:
			p0 = 1 + alpha;
			a2 = (1 - cs) / 2 / p0;
			a1 = (1 - cs) / p0;
			b2 = -(1 - alpha) / p0;
			b1 = 2 * cs / p0;
			a0 = (1 - cs) / 2 / p0;
			break;
		case AE_GEQ_FILTER_HPF:
			p0 = 1 + alpha;
			a2 = (1 + cs) / 2 / p0;
			a1 = -(1 + cs) / p0;
			b2 = -(1 - alpha) / p0;
			b1 = 2 * cs /p0;
			a0 = (1 + cs) / 2 / p0;
			break;
		case AE_GEQ_FILTER_BPF:
			p0 = 1 + alpha;
			a2 = -alpha / p0;
			a1 = 0;
			b2 = -(1 - alpha) / p0;
			b1 = 2 * cs / p0;
			a0 = alpha / p0;
			break;
		case AE_GEQ_FILTER_1STLPF:
			p1 = cs/(1+sn);
			a2 = 0.0;
			a1 = (1-p1)/2.0;
			b2 = 0.0;
			b1 = p1;
			a0 = a1;
			break;
		case AE_GEQ_FILTER_1STHPF:
			p1 = (1-sn)/cs;
			a2 = 0.0;
			a1 = -(1+p1)/2.0;
			b2 = 0.0;
			b1 = p1;
			a0 = -a1;
			break;
		case AE_GEQ_FILTER_NOTCH:
			p0 = 1 + alpha;
			a2 = 1 / p0;
			a1 = -2 * cs / p0;
			b2 = -(1 - alpha) / p0;
			b1 = 2 * cs / p0;
			a0 = 1 / p0;
			break;
		case AE_GEQ_FILTER_PEQ:
			p0 = 1 + alpha / A;
			a2 = (1 - alpha * A) / p0;
			a1 = -2 * cs / p0;
			b2 = -(1 - alpha / A) / p0;
			b1 = 2 * cs /p0;
			a0 = (1 + alpha * A) / p0;
			break;
		case AE_GEQ_FILTER_SHL:
			beta = sqrt(A / Q);
			p0 = (A+1)+(A-1)*cs+beta*sn;
			a2 =(A*((A+1)-(A-1)*cs-beta*sn)) / p0;
			a1 = (2*A*((A-1)-(A+1)*cs)) / p0;
			b2 = -((A+1)+(A-1)*cs-beta*sn) / p0;
			b1 =-(-2*((A-1)+(A+1)*cs)) / p0;
			a0 = (A*((A+1)-(A-1)*cs+beta*sn)) / p0;
			break;
		case AE_GEQ_FILTER_SHH:
			beta = sqrt(A / Q);
			p0 = (A+1)-(A-1)*cs+beta*sn;
			a2 = (A*((A+1)+(A-1)*cs-beta*sn)) / p0;
			a1 = (-2*A*((A-1)+(A+1)*cs)) / p0;
			b2 = -((A+1)-(A-1)*cs-beta*sn) / p0;
			b1 =-(2*((A-1)-(A+1)*cs)) / p0;
			a0 = (A*((A+1)+(A-1)*cs+beta*sn)) / p0;
			break;
		case AE_GEQ_FILTER_APF:
			p0 = 1 + alpha;
			a2 = 1;
			a1 = -2 * cs / p0;
			b2 = -(1 - alpha) / p0;
			b1 = 2 * cs / p0;
			a0 = (1 - alpha) / p0;
			break;
		//FIX_ME: need to implement below filter for user if.
		case AE_GEQ_FILTER_1STSHL:
		case AE_GEQ_FILTER_1STSHH:
		case AE_GEQ_FILTER_1STAPF:
		default:
			akdbgprt_err("\t%s: filter[%d] is invalid value!\n", __FUNCTION__, filter);
			return AK7739_ERROR_EINVAL;
			break;
	}

	if(precisionValue == AE_PRECISION_S) {
		c0 = fix32(a2, shiftValue);
		c1 = fix32(a1, shiftValue);
		c2 = fix32(b2, shiftValue);
		c3 = fix32(b1, shiftValue);
		c4 = fix32(a0, shiftValue);
		ae_userValue_to_cramWord(&cramGeqSingle[AE_GEQ_S_PARAM_C0][0], c0);
		ae_userValue_to_cramWord(&cramGeqSingle[AE_GEQ_S_PARAM_C1][0], c1);
		ae_userValue_to_cramWord(&cramGeqSingle[AE_GEQ_S_PARAM_C2][0], c2);
		ae_userValue_to_cramWord(&cramGeqSingle[AE_GEQ_S_PARAM_C3][0], c3);
		ae_userValue_to_cramWord(&cramGeqSingle[AE_GEQ_S_PARAM_C4][0], c4);
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(cramGeqSingle), &cramGeqSingle[0][0]);
	}
	else if(precisionValue == AE_PRECISION_D) {
		c0 = fix32H(a2, shiftValue);
		c1 = fix32L(a2, shiftValue);
		c2 = fix32H(a1, shiftValue);
		c3 = fix32L(a1, shiftValue);
		c4 = fix32H(b2, shiftValue);
		c5 = fix32L(b2, shiftValue);
		c6 = fix32H(b1, shiftValue);
		c7 = fix32L(b1, shiftValue);
		c8 = fix32H(a0, shiftValue);
		c9 = fix32L(a0, shiftValue);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C0][0], c0);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C1][0], c1);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C2][0], c2);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C3][0], c3);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C4][0], c4);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C5][0], c5);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C6][0], c6);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C7][0], c7);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C8][0], c8);
		ae_userValue_to_cramWord(&cramGeqDouble[AE_GEQ_D_PARAM_C9][0], c9);
		ret = ak7739_write_cram(ak7739, dspno, add, sizeof(cramGeqDouble), &cramGeqDouble[0][0]);
	}

	return ret;
}

int get_ae_spaectrumAnalysis(
		ak7739_priv *pData,
		int *band1dB, int *band2dB, int *band3dB, int *band4dB,
		int *band5dB, int *band6dB, int *band7dB, int *band8dB)
{
	struct ak7739_priv *ak7739 = pData;
	int ret;
	int readData[8] = {0x00};

	ret = ak7739_readDSP2SpectrumAnalyzer(ak7739, &readData[0], ARRAY_SIZE(readData));

	if(ret < 0) {
		akdbgprt_err("\t[AK7739] %s(%d) read spectrum failure\n", __FUNCTION__,__LINE__);

		*band1dB = -145;
		*band2dB = -145;
		*band3dB = -145;
		*band4dB = -145;
		*band5dB = -145;
		*band6dB = -145;
		*band7dB = -145;
		*band8dB = -145;
	}
	else {
		*band1dB = readData[0];
		*band2dB = readData[1];
		*band3dB = readData[2];
		*band4dB = readData[3];
		*band5dB = readData[4];
		*band6dB = readData[5];
		*band7dB = readData[6];
		*band8dB = readData[7];
	}

	//akdbgprt("AK7739 band1~8[%d][%d][%d][%d] [%d][%d][%d][%d] dB\n",
	//		*band1dB, *band2dB, *band3dB, *band4dB, *band5dB, *band6dB, *band7dB, *band8dB);

	return ret;
}


