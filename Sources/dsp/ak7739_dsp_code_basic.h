/*
 * ak7739_dsp_code_basic.h  --  audio driver for ak7739
 *
 */

#ifndef _AK7739_DSP_CODE_BASIC_H
#define _AK7739_DSP_CODE_BASIC_H

// PRAM file for DSP1
// PRAM download command = 0xB8
static const unsigned char ak7739_pram_dsp1_basic[]= {
	0xB8, 0x00, 0x00,		// command code and address
	0x0C, 0x94, 0xED, 0x1E, 0x74,
	0x04, 0xA9, 0x1C, 0x6D, 0x55,
	0x00, 0x44, 0x02, 0x11, 0x99,
	0x09, 0x58, 0x6F, 0x4D, 0xD0,
	0x01, 0x5A, 0x7E, 0xC9, 0x38,
	0x01, 0x35, 0xB3, 0x09, 0x8C,
	0x03, 0x4B, 0xFA, 0x24, 0x75,
	0x06, 0x78, 0xFB, 0xA0, 0xD6,
	0x0F, 0x82, 0xD3, 0xD3, 0x40,
	0x05, 0x79, 0x77, 0x03, 0x9D,
	0x07, 0xFE, 0x22, 0x43, 0x38,
	0x0E, 0xD9, 0xA1, 0xDE, 0x1F,
	0x00, 0x7B, 0xE2, 0xE2, 0x61,
	0x01, 0xDA, 0xBC, 0xF1, 0xF1,
	0x02, 0x38, 0xDA, 0xB0, 0xD8,
	0x08, 0x04, 0x23, 0x09, 0xCA,
	0x02, 0xDE, 0x9F, 0x97, 0x42,
	0x04, 0xFD, 0x92, 0x5B, 0xF2,
	0x0A, 0x36, 0x83, 0x19, 0x46,
	0x07, 0xF4, 0x58, 0xEB, 0x2C,
	0x01, 0xF7, 0x41, 0xA9, 0x9A,
	0x0D, 0xAF, 0x2A, 0x89, 0xCA,
	0x02, 0xEE, 0x07, 0x32, 0xCF,
	0x0D, 0x44, 0x86, 0x70, 0xBD,
	0x01, 0x43, 0xB8, 0x39, 0xE0,
	0x07, 0xC5, 0xCC, 0x82, 0x53,
	0x04, 0x29, 0x73, 0x62, 0xA4,
	0x01, 0xB5, 0x71, 0x31, 0x10,
	0x08, 0x46, 0x13, 0x91, 0x43,
	0x05, 0x37, 0xA2, 0x8D, 0xA9,
	0x0B, 0x24, 0xB7, 0xEC, 0x14,
	0x0C, 0x06, 0x32, 0x8D, 0x2F,
	0x0A, 0xB1, 0xD2, 0x5F, 0xE3,
	0x0E, 0x83, 0x5B, 0x76, 0x0B,
	0x0F, 0x05, 0x82, 0x95, 0xE5,
	0x0C, 0x0E, 0x74, 0x9E, 0xBA,
	0x09, 0x0C, 0xE1, 0x7F, 0x45,
	0x0F, 0x78, 0xFF, 0xC8, 0xEF,
	0x0B, 0x99, 0x04, 0xAE, 0x68,
	0x03, 0xE6, 0xC5, 0x48, 0xE3,
	0x08, 0xE2, 0x66, 0x26, 0x10,
	0x0C, 0x27, 0x2A, 0xC3, 0x7A,
	0x0E, 0x15, 0x8A, 0xD3, 0xF6,
	0x09, 0x6F, 0xC9, 0xA9, 0x98,
	0x0C, 0x65, 0x1A, 0x5B, 0xF5,
	0x0B, 0xAC, 0x33, 0xCE, 0x9D,
	0x06, 0xB6, 0xEC, 0x1F, 0xFE,
	0x0A, 0x05, 0x2B, 0xCB, 0xB8,
	0x0E, 0xE9, 0x39, 0x73, 0x12,
	0x09, 0xC2, 0xF6, 0xCD, 0x0E,
	0x00, 0xAF, 0x03, 0xDF, 0x17,
	0x02, 0x09, 0x4E, 0xD1, 0xE7,
	0x0D, 0x8A, 0x91, 0xC2, 0xF0,
	0x0C, 0xC4, 0xC0, 0x28, 0x98,
	0x0E, 0x55, 0x86, 0xFD, 0x5C,
	0x0A, 0x15, 0xA7, 0xEC, 0x92,
	0x0D, 0x93, 0x57, 0x36, 0x18,
	0x0A, 0x34, 0xBF, 0xA2, 0xC7,
	0x09, 0x37, 0x0F, 0xBA, 0x0D,
	0x0D, 0xD8, 0x2D, 0x7D, 0x54,
	0x0A, 0x57, 0x97, 0x74, 0x1F,
	0x0A, 0x7A, 0x6A, 0x2D, 0xF3,
	0x05, 0xED, 0x9A, 0x14, 0x21,
	0x0F, 0x07, 0xBE, 0x2E, 0x64,
	0x00, 0x9D, 0xA7, 0xC9, 0x9B,
	0x05, 0x23, 0x8D, 0xAB, 0x89,
	0x08, 0xD0, 0xC2, 0x30, 0x9C,
	0x0B, 0x0D, 0xE9, 0xB9, 0x14,
	0x0B, 0x4F, 0xD9, 0x21, 0x98,
	0x0E, 0xA6, 0xE0, 0x3B, 0x94,
	0x09, 0x7F, 0x45, 0x84, 0xB2,
	0x0F, 0x1F, 0x74, 0x1A, 0xDB,
	0x02, 0x5A, 0xFE, 0xAE, 0x14,
	0x0F, 0x2E, 0xE0, 0x73, 0xA4,
	0x05, 0x84, 0xC8, 0x67, 0x0B,
	0x0B, 0x34, 0x3B, 0xC3, 0xFE,
	0x0F, 0x7C, 0x5C, 0xCC, 0x0D,
	0x03, 0x47, 0x1F, 0x3C, 0x6A,
	0x07, 0x1B, 0x57, 0x19, 0x51,
	0x00, 0x84, 0x61, 0x39, 0x56,
	0x09, 0xD3, 0x76, 0x2E, 0x56,
	0x0F, 0xB2, 0x4B, 0x7E, 0x4D,
	0x0C, 0x90, 0xE3, 0x28, 0xD2,
	0x0E, 0x8B, 0x1D, 0x65, 0x9E,
	0x0E, 0xE8, 0x35, 0xB3, 0x49,
	0x0D, 0xF5, 0xD0, 0x23, 0xDE,
	0x0D, 0xC0, 0xE7, 0x43, 0x6B,
	0x08, 0x90, 0xCE, 0x17, 0xB6,
	0x0A, 0x77, 0x83, 0xFA, 0x1E,
	0x08, 0xB9, 0x90, 0x4A, 0x76,
	0x0F, 0x6E, 0xEC, 0x54, 0x8E,
	0x06, 0xAE, 0x26, 0x22, 0x01,
	0x08, 0xC2, 0x72, 0xA8, 0x1D,
	0x0E, 0xE4, 0xD0, 0xA7, 0xFF,
	0x04, 0x96, 0xFC, 0x90, 0x59,
	0x00, 0xC6, 0x51, 0xA5, 0xFD,
	0x04, 0x3A, 0xCF, 0x3A, 0x7D,
	0x00, 0x6B, 0x6E, 0xC1, 0x6B,
	0x0A, 0xF0, 0xD2, 0xBC, 0xBB,
	0x01, 0xCE, 0x93, 0xD7, 0x51,
	0x01, 0x9C, 0x2F, 0x68, 0xFB,
	0x00, 0x0F, 0xF8, 0x3D, 0xF1
};

// PRAM file for DSP2
// PRAM download command = 0xB9
static const unsigned char ak7739_pram_dsp2_basic[]= {
	0xB9, 0x00, 0x00,		// command code and address
	0x0C, 0x94, 0xED, 0x1E, 0x74,
	0x04, 0xA9, 0x1C, 0x6D, 0x55,
	0x00, 0x44, 0x02, 0x11, 0x99,
	0x09, 0x58, 0x6F, 0x4D, 0xD0,
	0x01, 0x5A, 0x7E, 0xC9, 0x38,
	0x01, 0x35, 0xB3, 0x09, 0x8C,
	0x03, 0x4B, 0xFA, 0x24, 0x75,
	0x06, 0x78, 0xFB, 0xA0, 0xD6,
	0x0F, 0x82, 0xD3, 0xD3, 0x40,
	0x05, 0x79, 0x77, 0x03, 0x9D,
	0x07, 0xFE, 0x22, 0x43, 0x38,
	0x0E, 0xD9, 0xA1, 0xDE, 0x1F,
	0x00, 0x7B, 0xE2, 0xE2, 0x61,
	0x01, 0xDA, 0xBC, 0xF1, 0xF1,
	0x02, 0x38, 0xDA, 0xB0, 0xD8,
	0x08, 0x04, 0x23, 0x09, 0xCA,
	0x02, 0xDE, 0x9F, 0x97, 0x42,
	0x04, 0xFD, 0x92, 0x5B, 0xF2,
	0x0A, 0x36, 0x83, 0x19, 0x46,
	0x07, 0xF4, 0x58, 0xEB, 0x2C,
	0x01, 0xF7, 0x41, 0xA9, 0x9A,
	0x0D, 0xAF, 0x2A, 0x89, 0xCA,
	0x02, 0xEE, 0x07, 0x32, 0xCF,
	0x0D, 0x44, 0x86, 0x70, 0xBD,
	0x01, 0x43, 0xB8, 0x39, 0xE0,
	0x07, 0xC5, 0xCC, 0x82, 0x53,
	0x04, 0x29, 0x73, 0x62, 0xA4,
	0x01, 0xB5, 0x71, 0x31, 0x10,
	0x08, 0x46, 0x13, 0x91, 0x43,
	0x05, 0x37, 0xA2, 0x8D, 0xA9,
	0x0B, 0x24, 0xB7, 0xEC, 0x14,
	0x0C, 0x06, 0x32, 0x8D, 0x2F,
	0x0A, 0xB1, 0xD2, 0x5F, 0xE3,
	0x0E, 0x83, 0x5B, 0x76, 0x0B,
	0x0F, 0x05, 0x82, 0x95, 0xE5,
	0x0C, 0x0E, 0x74, 0x9E, 0xBA,
	0x09, 0x0C, 0xE1, 0x7F, 0x45,
	0x0F, 0x78, 0xFF, 0xC8, 0xEF,
	0x0B, 0x99, 0x04, 0xAE, 0x68,
	0x03, 0xE6, 0xC5, 0x48, 0xE3,
	0x08, 0xE2, 0x66, 0x26, 0x10,
	0x0C, 0x27, 0x2A, 0xC3, 0x7A,
	0x0E, 0x15, 0x8A, 0xD3, 0xF6,
	0x09, 0x6F, 0xC9, 0xA9, 0x98,
	0x0C, 0x65, 0x1A, 0x5B, 0xF5,
	0x0B, 0xAC, 0x33, 0xCE, 0x9D,
	0x06, 0xB6, 0xEC, 0x1F, 0xFE,
	0x0A, 0x05, 0x2B, 0xCB, 0xB8,
	0x0E, 0xE9, 0x39, 0x73, 0x12,
	0x09, 0xC2, 0xF6, 0xCD, 0x0E,
	0x00, 0xAF, 0x03, 0xDF, 0x17,
	0x02, 0x09, 0x4E, 0xD1, 0xE7,
	0x0D, 0x8A, 0x91, 0xC2, 0xF0,
	0x0C, 0xC4, 0xC0, 0x28, 0x98,
	0x0E, 0x55, 0x86, 0xFD, 0x5C,
	0x0A, 0x15, 0xA7, 0xEC, 0x92,
	0x0D, 0x93, 0x57, 0x36, 0x18,
	0x0A, 0x34, 0xBF, 0xA2, 0xC7,
	0x09, 0x37, 0x0F, 0xBA, 0x0D,
	0x0D, 0xD8, 0x2D, 0x7D, 0x54,
	0x0A, 0x57, 0x97, 0x74, 0x1F,
	0x0A, 0x7A, 0x6A, 0x2D, 0xF3,
	0x05, 0xED, 0x9A, 0x14, 0x21,
	0x0F, 0x07, 0xBE, 0x2E, 0x64,
	0x00, 0x9D, 0xA7, 0xC9, 0x9B,
	0x05, 0x23, 0x8D, 0xAB, 0x89,
	0x08, 0xD0, 0xC2, 0x30, 0x9C,
	0x0B, 0x0D, 0xE9, 0xB9, 0x14,
	0x0B, 0x4F, 0xD9, 0x21, 0x98,
	0x0E, 0xA6, 0xE0, 0x3B, 0x94,
	0x09, 0x7F, 0x45, 0x84, 0xB2,
	0x0F, 0x1F, 0x74, 0x1A, 0xDB,
	0x02, 0x5A, 0xFE, 0xAE, 0x14,
	0x0F, 0x2E, 0xE0, 0x73, 0xA4,
	0x05, 0x84, 0xC8, 0x67, 0x0B,
	0x0B, 0x34, 0x3B, 0xC3, 0xFE,
	0x0F, 0x7C, 0x5C, 0xCC, 0x0D,
	0x03, 0x47, 0x1F, 0x3C, 0x6A,
	0x07, 0x1B, 0x57, 0x19, 0x51,
	0x00, 0x84, 0x61, 0x39, 0x56,
	0x09, 0xD3, 0x76, 0x2E, 0x56,
	0x0F, 0xB2, 0x4B, 0x7E, 0x4D,
	0x0C, 0x90, 0xE3, 0x28, 0xD2,
	0x0E, 0x8B, 0x1D, 0x65, 0x9E,
	0x0E, 0xE8, 0x35, 0xB3, 0x49,
	0x0D, 0xF5, 0xD0, 0x23, 0xDE,
	0x0D, 0xC0, 0xE7, 0x43, 0x6B,
	0x08, 0x90, 0xCE, 0x17, 0xB6,
	0x0A, 0x77, 0x83, 0xFA, 0x1E,
	0x08, 0xB9, 0x90, 0x4A, 0x76,
	0x0F, 0x6E, 0xEC, 0x54, 0x8E,
	0x06, 0xAE, 0x26, 0x22, 0x01,
	0x08, 0xC2, 0x72, 0xA8, 0x1D,
	0x0E, 0xE4, 0xD0, 0xA7, 0xFF,
	0x04, 0x96, 0xFC, 0x90, 0x59,
	0x00, 0xC6, 0x51, 0xA5, 0xFD,
	0x04, 0x3A, 0xCF, 0x3A, 0x7D,
	0x00, 0x6B, 0x6E, 0xC1, 0x6B,
	0x0A, 0xF0, 0xD2, 0xBC, 0xBB,
	0x01, 0xCE, 0x93, 0xD7, 0x51,
	0x01, 0x9C, 0x2F, 0x68, 0xFB,
	0x00, 0x0F, 0xF8, 0x3D, 0xF1
};

// PRAM file for DSP3
// PRAM download command = 0xBA
static const unsigned char ak7739_pram_dsp3_basic[]= {
	0xBA, 0x00, 0x00,		// command code and address
	0x0C, 0x94, 0xED, 0x1E, 0x74,
	0x04, 0xA9, 0x1C, 0x6D, 0x55,
	0x00, 0x44, 0x02, 0x11, 0x99,
	0x09, 0x58, 0x6F, 0x4D, 0xD0,
	0x01, 0x5A, 0x7E, 0xC9, 0x38,
	0x06, 0x35, 0x33, 0x01, 0x8C
};


// CRAM file for DSP1
// CRAM download command = 0xB4
static const unsigned char ak7739_cram_dsp1_basic[]= {
	0xB4, 0x00, 0x00,		// command code and address
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
};

// CRAM file for DSP2
// CRAM download command = 0xB5
static const unsigned char ak7739_cram_dsp2_basic[]= {
	0xB5, 0x00, 0x00,		// command code and address
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00,
};

// CRAM file for DSP3
// CRAM download command = 0xB6
static const unsigned char ak7739_cram_dsp3_basic[]= {
	0xB6, 0x00, 0x00,		// command code and address
	0x00, 0x00, 0x00, 0x00,
};


// OFFREG file for DSP1
// OFFREG download command = 0xB2
static const unsigned char ak7739_offram_dsp1_basic[]= {
	0xB2, 0x00, 0x00,  // OFFREG Write Command  
	0x00, 0x00, 0x00, 0x00,
};

// OFFREG file for DSP2
// OFFREG download command = 0xB3
static const unsigned char ak7739_offram_dsp2_basic[]= {
	0xB3, 0x00, 0x00,  // OFFREG Write Command  
	0x00, 0x00, 0x00, 0x00,
};

#endif

