/*
 * ak7739_audio_effect_addr.h  --  audio driver for ak7739
 *
 */

#ifndef AK7739_AUDIO_EFFECT_ADDR_SONY_H_
#define AK7739_AUDIO_EFFECT_ADDR_SONY_H_

// AKM_AudioEffect_Basic_UseCase_V01_3_0_addr
// Component: Media1Gain
#define 	CRAM_ADDR_LEVEL_Media1Gain_sony	0x0000
#define 	CRAM_ADDR_ATT_TIME_Media1Gain_sony	0x0001
#define 	CRAM_ADDR_REL_TIME_Media1Gain_sony	0x0002

// Component: Media2Gain
#define 	CRAM_ADDR_LEVEL_Media2Gain_sony	0x0003
#define 	CRAM_ADDR_ATT_TIME_Media2Gain_sony	0x0004
#define 	CRAM_ADDR_REL_TIME_Media2Gain_sony	0x0005

// Component: Media3Gain
#define 	CRAM_ADDR_LEVEL_Media3Gain_sony	0x0006
#define 	CRAM_ADDR_ATT_TIME_Media3Gain_sony	0x0007
#define 	CRAM_ADDR_REL_TIME_Media3Gain_sony	0x0008

// Component: Media23LMix
#define 	CRAM_ADDR_VOL_IN1_Media23LMix_sony	0x0009
#define 	CRAM_ADDR_VOL_IN2_Media23LMix_sony	0x000A

// Component: Media23RMix
#define 	CRAM_ADDR_VOL_IN1_Media23RMix_sony	0x000B
#define 	CRAM_ADDR_VOL_IN2_Media23RMix_sony	0x000C

// Component: Media23Gain
#define 	CRAM_ADDR_LEVEL_Media23Gain_sony	0x000D
#define 	CRAM_ADDR_ATT_TIME_Media23Gain_sony	0x000E
#define 	CRAM_ADDR_REL_TIME_Media23Gain_sony	0x000F

// Component: MediaInputSW4
#define 	CRAM_ADDR_IN1_MediaInputSW4_sony	0x0010
#define 	CRAM_ADDR_IN2_MediaInputSW4_sony	0x0011
#define 	CRAM_ADDR_IN3_MediaInputSW4_sony	0x0012
#define 	CRAM_ADDR_IN4_MediaInputSW4_sony	0x0013

// Component: MediaInVolume
#define 	CRAM_ADDR_LEVEL_MediaInVolume_sony	0x0014
#define 	CRAM_ADDR_ATT_TIME_MediaInVolume_sony	0x0015
#define 	CRAM_ADDR_REL_TIME_MediaInVolume_sony	0x0016

// Component: DCCut
#define 	CRAM_ADDR_BAND1_DCCut_sony	0x0017
#define 	CRAM_ADDR_BAND2_DCCut_sony	0x001C
#define 	CRAM_ADDR_BAND3_DCCut_sony	0x0021

// Component: DCCutSW2
#define 	CRAM_ADDR_IN1_DCCutSW2_sony	0x0026
#define 	CRAM_ADDR_IN2_DCCutSW2_sony	0x0027

// Component: DeEmphasis
#define 	CRAM_ADDR_BAND1_DeEmphasis_sony	0x0028

// Component: DeEmpSW2
#define 	CRAM_ADDR_IN1_DeEmpSW2_sony	0x002D
#define 	CRAM_ADDR_IN2_DeEmpSW2_sony	0x002E

// Component: Compressor
#define 	CRAM_ADDR_ATT_TIME_Compressor_sony	0x0030
#define 	CRAM_ADDR_REL_TIME_Compressor_sony	0x0031
#define 	CRAM_ADDR_P1_INFO_Compressor_sony	0x0036
#define 	CRAM_ADDR_P2_INFO_Compressor_sony	0x0039
#define 	CRAM_ADDR_P3_INFO_Compressor_sony	0x003C
#define 	CRAM_ADDR_P4_INFO_Compressor_sony	0x003F
#define 	CRAM_ADDR_P5_INFO_Compressor_sony	0x0042
#define 	CRAM_ADDR_BIASGAIN_Compressor_sony	0x0048

// Component: CompressorSW2
#define 	CRAM_ADDR_IN1_CompressorSW2_sony	0x0049
#define 	CRAM_ADDR_IN2_CompressorSW2_sony	0x004A

// Component: BMTGain
#define 	CRAM_ADDR_LEVEL_BMTGain_sony	0x004B
#define 	CRAM_ADDR_ATT_TIME_BMTGain_sony	0x004C
#define 	CRAM_ADDR_REL_TIME_BMTGain_sony	0x004D

// Component: BMTGEQ
#define 	CRAM_ADDR_BAND1_BMTGEQ_sony	0x004E
#define 	CRAM_ADDR_BAND2_BMTGEQ_sony	0x0053
#define 	CRAM_ADDR_BAND3_BMTGEQ_sony	0x0058
#define 	CRAM_ADDR_BAND4_BMTGEQ_sony	0x005D
#define 	CRAM_ADDR_BAND5_BMTGEQ_sony	0x0062
#define 	CRAM_ADDR_BAND6_BMTGEQ_sony	0x0067

// Component: BMTSW2
#define 	CRAM_ADDR_IN1_BMTSW2_sony	0x006C
#define 	CRAM_ADDR_IN2_BMTSW2_sony	0x006D

// Component: PersonalGain
#define 	CRAM_ADDR_LEVEL_PersonalGain_sony	0x006E
#define 	CRAM_ADDR_ATT_TIME_PersonalGain_sony	0x006F
#define 	CRAM_ADDR_REL_TIME_PersonalGain_sony	0x0070

// Component: PersonalGEQ
#define 	CRAM_ADDR_BAND1_PersonalGEQ_sony	0x0071
#define 	CRAM_ADDR_BAND2_PersonalGEQ_sony	0x0076
#define 	CRAM_ADDR_BAND3_PersonalGEQ_sony	0x007B
#define 	CRAM_ADDR_BAND4_PersonalGEQ_sony	0x0080
#define 	CRAM_ADDR_BAND5_PersonalGEQ_sony	0x0085
#define 	CRAM_ADDR_BAND6_PersonalGEQ_sony	0x008A

// Component: POPGain
#define 	CRAM_ADDR_LEVEL_POPGain_sony	0x008F
#define 	CRAM_ADDR_ATT_TIME_POPGain_sony	0x0090
#define 	CRAM_ADDR_REL_TIME_POPGain_sony	0x0091

// Component: POPGEQ
#define 	CRAM_ADDR_BAND1_POPGEQ_sony	0x0092
#define 	CRAM_ADDR_BAND2_POPGEQ_sony	0x0097
#define 	CRAM_ADDR_BAND3_POPGEQ_sony	0x009C
#define 	CRAM_ADDR_BAND4_POPGEQ_sony	0x00A1
#define 	CRAM_ADDR_BAND5_POPGEQ_sony	0x00A6
#define 	CRAM_ADDR_BAND6_POPGEQ_sony	0x00AB

// Component: RockGain
#define 	CRAM_ADDR_LEVEL_RockGain_sony	0x00B0
#define 	CRAM_ADDR_ATT_TIME_RockGain_sony	0x00B1
#define 	CRAM_ADDR_REL_TIME_RockGain_sony	0x00B2

// Component: RockGEQ
#define 	CRAM_ADDR_BAND1_RockGEQ_sony	0x00B3
#define 	CRAM_ADDR_BAND2_RockGEQ_sony	0x00B8
#define 	CRAM_ADDR_BAND3_RockGEQ_sony	0x00BD
#define 	CRAM_ADDR_BAND4_RockGEQ_sony	0x00C2
#define 	CRAM_ADDR_BAND5_RockGEQ_sony	0x00C7
#define 	CRAM_ADDR_BAND6_RockGEQ_sony	0x00CC

// Component: ClassicalGain
#define 	CRAM_ADDR_LEVEL_ClassicalGain_sony	0x00D1
#define 	CRAM_ADDR_ATT_TIME_ClassicalGain_sony	0x00D2
#define 	CRAM_ADDR_REL_TIME_ClassicalGain_sony	0x00D3

// Component: ClassicalGEQ
#define 	CRAM_ADDR_BAND1_ClassicalGEQ_sony	0x00D4
#define 	CRAM_ADDR_BAND2_ClassicalGEQ_sony	0x00D9
#define 	CRAM_ADDR_BAND3_ClassicalGEQ_sony	0x00DE
#define 	CRAM_ADDR_BAND4_ClassicalGEQ_sony	0x00E3
#define 	CRAM_ADDR_BAND5_ClassicalGEQ_sony	0x00E8
#define 	CRAM_ADDR_BAND6_ClassicalGEQ_sony	0x00ED

// Component: SoundModeSW4
#define 	CRAM_ADDR_IN1_SoundModeSW4_sony	0x00F2
#define 	CRAM_ADDR_IN2_SoundModeSW4_sony	0x00F3
#define 	CRAM_ADDR_IN3_SoundModeSW4_sony	0x00F4
#define 	CRAM_ADDR_IN4_SoundModeSW4_sony	0x00F5

// Component: SoundModeSW2
#define 	CRAM_ADDR_IN1_SoundModeSW2_sony	0x00F6
#define 	CRAM_ADDR_IN2_SoundModeSW2_sony	0x00F7

// Component: LoudnessGain
#define 	CRAM_ADDR_LEVEL_LoudnessGain_sony	0x00F8
#define 	CRAM_ADDR_ATT_TIME_LoudnessGain_sony	0x00F9
#define 	CRAM_ADDR_REL_TIME_LoudnessGain_sony	0x00FA

// Component: LoudnessGEQ
#define 	CRAM_ADDR_BAND1_LoudnessGEQ_sony	0x00FB
#define 	CRAM_ADDR_BAND2_LoudnessGEQ_sony	0x0100
#define 	CRAM_ADDR_BAND3_LoudnessGEQ_sony	0x0105
#define 	CRAM_ADDR_BAND4_LoudnessGEQ_sony	0x010A
#define 	CRAM_ADDR_BAND5_LoudnessGEQ_sony	0x010F
#define 	CRAM_ADDR_BAND6_LoudnessGEQ_sony	0x0114
#define 	CRAM_ADDR_BAND7_LoudnessGEQ_sony	0x0119

// Component: LoudnessSW2
#define 	CRAM_ADDR_IN1_LoudnessSW2_sony	0x011E
#define 	CRAM_ADDR_IN2_LoudnessSW2_sony	0x011F

// Component: EffectInGainL
#define 	CRAM_ADDR_LEVEL_EffectInGainL_sony	0x0120
#define 	CRAM_ADDR_ATT_TIME_EffectInGainL_sony	0x0121
#define 	CRAM_ADDR_REL_TIME_EffectInGainL_sony	0x0122

// Component: EffectInGainR
#define 	CRAM_ADDR_LEVEL_EffectInGainR_sony	0x0123
#define 	CRAM_ADDR_ATT_TIME_EffectInGainR_sony	0x0124
#define 	CRAM_ADDR_REL_TIME_EffectInGainR_sony	0x0125

// Component: SurroundSW2
#define 	CRAM_ADDR_IN1_SurroundSW2_sony	0x0126
#define 	CRAM_ADDR_IN2_SurroundSW2_sony	0x0127

// Component: MainEffectSW2
#define 	CRAM_ADDR_IN1_MainEffectSW2_sony	0x0128
#define 	CRAM_ADDR_IN2_MainEffectSW2_sony	0x0129

// Component: OutGainFL
#define 	CRAM_ADDR_LEVEL_OutGainFL_sony	0x012A
#define 	CRAM_ADDR_ATT_TIME_OutGainFL_sony	0x012B
#define 	CRAM_ADDR_REL_TIME_OutGainFL_sony	0x012C

// Component: OutGEQFL
#define 	CRAM_ADDR_BAND1_OutGEQFL_sony	0x012D
#define 	CRAM_ADDR_BAND2_OutGEQFL_sony	0x0132
#define 	CRAM_ADDR_BAND3_OutGEQFL_sony	0x0137
#define 	CRAM_ADDR_BAND4_OutGEQFL_sony	0x013C
#define 	CRAM_ADDR_BAND5_OutGEQFL_sony	0x0141
#define 	CRAM_ADDR_BAND6_OutGEQFL_sony	0x0146
#define 	CRAM_ADDR_BAND7_OutGEQFL_sony	0x014B
#define 	CRAM_ADDR_BAND8_OutGEQFL_sony	0x0150
#define 	CRAM_ADDR_BAND9_OutGEQFL_sony	0x0155

// Component: OutGainFR
#define 	CRAM_ADDR_LEVEL_OutGainFR_sony	0x015A
#define 	CRAM_ADDR_ATT_TIME_OutGainFR_sony	0x015B
#define 	CRAM_ADDR_REL_TIME_OutGainFR_sony	0x015C

// Component: OutGEQFR
#define 	CRAM_ADDR_BAND1_OutGEQFR_sony	0x015D
#define 	CRAM_ADDR_BAND2_OutGEQFR_sony	0x0162
#define 	CRAM_ADDR_BAND3_OutGEQFR_sony	0x0167
#define 	CRAM_ADDR_BAND4_OutGEQFR_sony	0x016C
#define 	CRAM_ADDR_BAND5_OutGEQFR_sony	0x0171
#define 	CRAM_ADDR_BAND6_OutGEQFR_sony	0x0176
#define 	CRAM_ADDR_BAND7_OutGEQFR_sony	0x017B
#define 	CRAM_ADDR_BAND8_OutGEQFR_sony	0x0180
#define 	CRAM_ADDR_BAND9_OutGEQFR_sony	0x0185

// Component: MediaOutVolumeFLR
#define 	CRAM_ADDR_LEVEL_MediaOutVolumeFLR_sony	0x018A
#define 	CRAM_ADDR_ATT_TIME_MediaOutVolumeFLR_sony	0x018B
#define 	CRAM_ADDR_REL_TIME_MediaOutVolumeFLR_sony	0x018C

// Component: NaviGain
#define 	CRAM_ADDR_LEVEL_NaviGain_sony	0x018D
#define 	CRAM_ADDR_ATT_TIME_NaviGain_sony	0x018E
#define 	CRAM_ADDR_REL_TIME_NaviGain_sony	0x018F

// Component: NaviGEQ
#define 	CRAM_ADDR_BAND1_NaviGEQ_sony	0x0190
#define 	CRAM_ADDR_BAND2_NaviGEQ_sony	0x0195
#define 	CRAM_ADDR_BAND3_NaviGEQ_sony	0x019A
#define 	CRAM_ADDR_BAND4_NaviGEQ_sony	0x019F
#define 	CRAM_ADDR_BAND5_NaviGEQ_sony	0x01A4
#define 	CRAM_ADDR_BAND6_NaviGEQ_sony	0x01A9

// Component: NaviGEQSW2
#define 	CRAM_ADDR_IN1_NaviGEQSW2_sony	0x01AE
#define 	CRAM_ADDR_IN2_NaviGEQSW2_sony	0x01AF

// Component: NaviVolume
#define 	CRAM_ADDR_LEVEL_NaviVolume_sony	0x01B0
#define 	CRAM_ADDR_ATT_TIME_NaviVolume_sony	0x01B1
#define 	CRAM_ADDR_REL_TIME_NaviVolume_sony	0x01B2

// Component: NaviMute
#define 	CRAM_ADDR_LEVEL_NaviMute_sony	0x01B3
#define 	CRAM_ADDR_ATT_TIME_NaviMute_sony	0x01B4
#define 	CRAM_ADDR_REL_TIME_NaviMute_sony	0x01B5

// Component: PhoneGain
#define 	CRAM_ADDR_LEVEL_PhoneGain_sony	0x01B6
#define 	CRAM_ADDR_ATT_TIME_PhoneGain_sony	0x01B7
#define 	CRAM_ADDR_REL_TIME_PhoneGain_sony	0x01B8

// Component: PhoneGEQ
#define 	CRAM_ADDR_BAND1_PhoneGEQ_sony	0x01B9
#define 	CRAM_ADDR_BAND2_PhoneGEQ_sony	0x01BE
#define 	CRAM_ADDR_BAND3_PhoneGEQ_sony	0x01C3
#define 	CRAM_ADDR_BAND4_PhoneGEQ_sony	0x01C8
#define 	CRAM_ADDR_BAND5_PhoneGEQ_sony	0x01CD
#define 	CRAM_ADDR_BAND6_PhoneGEQ_sony	0x01D2

// Component: PhoneGEQSW2
#define 	CRAM_ADDR_IN1_PhoneGEQSW2_sony	0x01D7
#define 	CRAM_ADDR_IN2_PhoneGEQSW2_sony	0x01D8

// Component: PhoneVolume
#define 	CRAM_ADDR_LEVEL_PhoneVolume_sony	0x01D9
#define 	CRAM_ADDR_ATT_TIME_PhoneVolume_sony	0x01DA
#define 	CRAM_ADDR_REL_TIME_PhoneVolume_sony	0x01DB

// Component: PhoneMute
#define 	CRAM_ADDR_LEVEL_PhoneMute_sony	0x01DC
#define 	CRAM_ADDR_ATT_TIME_PhoneMute_sony	0x01DD
#define 	CRAM_ADDR_REL_TIME_PhoneMute_sony	0x01DE

// Component: Beep
#define 	CRAM_ADDR_Beep_sony	0x01DF
#define 	ORAM_ADDR_Beep_sony	0x0000
#define 	DRAM0_ADDR_Beep_sony	0x016E
#define 	DRAM1_ADDR_Beep_sony	0x0000
#define 	CRAM_ADDR_BEEP_TRIGGER_Beep_sony	0x01E0
#define 	CRAM_ADDR_BEEP_FREQ_Beep_sony	0x01E2
#define 	CRAM_ADDR_BEEP_ATTACK_STEP_Beep_sony	0x01E4
#define 	CRAM_ADDR_BEEP_DECAY_STEP_Beep_sony	0x01E5
#define 	CRAM_ADDR_BEEP_ATTACK_TIME_Beep_sony	0x01E6
#define 	CRAM_ADDR_BEEP_SUSTAIN_TIME_Beep_sony	0x01E7
#define 	CRAM_ADDR_BEEP_SOUND_CADENCE_Beep_sony	0x01E8
#define 	CRAM_ADDR_BEEP_REPS_Beep_sony	0x01E9
#define 	CRAM_ADDR_BEEP_STATUS_Beep_sony	0x01F3
#define 	CRAM_ADDR_BEEP_REPSREMAIN_Beep_sony	0x01F4

// Component: BeepGain
#define 	CRAM_ADDR_LEVEL_BeepGain_sony	0x01F5
#define 	CRAM_ADDR_ATT_TIME_BeepGain_sony	0x01F6
#define 	CRAM_ADDR_REL_TIME_BeepGain_sony	0x01F7

// Component: BeepVolume
#define 	CRAM_ADDR_LEVEL_BeepVolume_sony	0x01F8
#define 	CRAM_ADDR_ATT_TIME_BeepVolume_sony	0x01F9
#define 	CRAM_ADDR_REL_TIME_BeepVolume_sony	0x01FA

// Component: BeepMute
#define 	CRAM_ADDR_LEVEL_BeepMute_sony	0x01FB
#define 	CRAM_ADDR_ATT_TIME_BeepMute_sony	0x01FC
#define 	CRAM_ADDR_REL_TIME_BeepMute_sony	0x01FD

// Component: RSEGain
#define 	CRAM_ADDR_LEVEL_RSEGain_sony	0x01FE
#define 	CRAM_ADDR_ATT_TIME_RSEGain_sony	0x01FF
#define 	CRAM_ADDR_REL_TIME_RSEGain_sony	0x0200

// Component: ChimeMix
#define 	CRAM_ADDR_VOL_IN1_ChimeMix_sony	0x0201
#define 	CRAM_ADDR_VOL_IN2_ChimeMix_sony	0x0202

// Component: ChimeVolume
#define 	CRAM_ADDR_LEVEL_ChimeVolume_sony	0x0203
#define 	CRAM_ADDR_ATT_TIME_ChimeVolume_sony	0x0204
#define 	CRAM_ADDR_REL_TIME_ChimeVolume_sony	0x0205

// Component: ChimeMute
#define 	CRAM_ADDR_LEVEL_ChimeMute_sony	0x0206
#define 	CRAM_ADDR_ATT_TIME_ChimeMute_sony	0x0207
#define 	CRAM_ADDR_REL_TIME_ChimeMute_sony	0x0208

// Component: OutMixFL
#define 	CRAM_ADDR_VOL_IN1_OutMixFL_sony	0x0209
#define 	CRAM_ADDR_VOL_IN2_OutMixFL_sony	0x020A
#define 	CRAM_ADDR_VOL_IN3_OutMixFL_sony	0x020B
#define 	CRAM_ADDR_VOL_IN4_OutMixFL_sony	0x020C
#define 	CRAM_ADDR_VOL_IN5_OutMixFL_sony	0x020D

// Component: OutDelayFL
#define 	ORAM_ADDR_WRITE_OutDelayFL_sony	0x0000
#define 	ORAM_ADDR_READ1_OutDelayFL_sony	0x0001

// Component: FadeBalanceFL
#define 	CRAM_ADDR_LEVEL_FadeBalanceFL_sony	0x020E
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceFL_sony	0x020F
#define 	CRAM_ADDR_REL_TIME_FadeBalanceFL_sony	0x0210

// Component: OutMixFR
#define 	CRAM_ADDR_VOL_IN1_OutMixFR_sony	0x0211
#define 	CRAM_ADDR_VOL_IN2_OutMixFR_sony	0x0212
#define 	CRAM_ADDR_VOL_IN3_OutMixFR_sony	0x0213
#define 	CRAM_ADDR_VOL_IN4_OutMixFR_sony	0x0214
#define 	CRAM_ADDR_VOL_IN5_OutMixFR_sony	0x0215

// Component: OutDelayFR
#define 	ORAM_ADDR_WRITE_OutDelayFR_sony	0x0002
#define 	ORAM_ADDR_READ1_OutDelayFR_sony	0x0003

// Component: FadeBalanceFR
#define 	CRAM_ADDR_LEVEL_FadeBalanceFR_sony	0x0216
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceFR_sony	0x0217
#define 	CRAM_ADDR_REL_TIME_FadeBalanceFR_sony	0x0218

// Component: SSVGainFLR
#define 	CRAM_ADDR_LEVEL_SSVGainFLR_sony	0x0219
#define 	CRAM_ADDR_ATT_TIME_SSVGainFLR_sony	0x021A
#define 	CRAM_ADDR_REL_TIME_SSVGainFLR_sony	0x021B

// Component: SSVGEQFLR
#define 	CRAM_ADDR_BAND1_SSVGEQFLR_sony	0x021C
#define 	CRAM_ADDR_BAND2_SSVGEQFLR_sony	0x0221
#define 	CRAM_ADDR_BAND3_SSVGEQFLR_sony	0x0226
#define 	CRAM_ADDR_BAND4_SSVGEQFLR_sony	0x022B
#define 	CRAM_ADDR_BAND5_SSVGEQFLR_sony	0x0230
#define 	CRAM_ADDR_BAND6_SSVGEQFLR_sony	0x0235

// Component: OutGainSW
#define 	CRAM_ADDR_LEVEL_OutGainSW_sony	0x023A
#define 	CRAM_ADDR_ATT_TIME_OutGainSW_sony	0x023B
#define 	CRAM_ADDR_REL_TIME_OutGainSW_sony	0x023C

// Component: COverGEQSW
#define 	CRAM_ADDR_BAND1_COverGEQSW_sony	0x023D
#define 	CRAM_ADDR_BAND2_COverGEQSW_sony	0x0242
#define 	CRAM_ADDR_BAND3_COverGEQSW_sony	0x0247

// Component: OutSWVolumeL
#define 	CRAM_ADDR_LEVEL_OutSWVolumeL_sony	0x024C
#define 	CRAM_ADDR_ATT_TIME_OutSWVolumeL_sony	0x024D
#define 	CRAM_ADDR_REL_TIME_OutSWVolumeL_sony	0x024E

// Component: OutSWVolumeR
#define 	CRAM_ADDR_LEVEL_OutSWVolumeR_sony	0x024F
#define 	CRAM_ADDR_ATT_TIME_OutSWVolumeR_sony	0x0250
#define 	CRAM_ADDR_REL_TIME_OutSWVolumeR_sony	0x0251

// Component: OutSWVolumeLR
#define 	CRAM_ADDR_LEVEL_OutSWVolumeLR_sony	0x0252
#define 	CRAM_ADDR_ATT_TIME_OutSWVolumeLR_sony	0x0253
#define 	CRAM_ADDR_REL_TIME_OutSWVolumeLR_sony	0x0254

// Component: OutMuteSW
#define 	CRAM_ADDR_LEVEL_OutMuteSW_sony	0x0255
#define 	CRAM_ADDR_ATT_TIME_OutMuteSW_sony	0x0256
#define 	CRAM_ADDR_REL_TIME_OutMuteSW_sony	0x0257

// Component: OutLimiterSWL
#define 	CRAM_ADDR_ATT_TIME_OutLimiterSWL_sony	0x0259
#define 	CRAM_ADDR_REL_TIME_OutLimiterSWL_sony	0x025A
#define 	CRAM_ADDR_THRESHOLD_OutLimiterSWL_sony	0x025D

// Component: OutLimiterSWR
#define 	CRAM_ADDR_ATT_TIME_OutLimiterSWR_sony	0x0261
#define 	CRAM_ADDR_REL_TIME_OutLimiterSWR_sony	0x0262
#define 	CRAM_ADDR_THRESHOLD_OutLimiterSWR_sony	0x0265

// Component: OutGainCenter
#define 	CRAM_ADDR_LEVEL_OutGainCenter_sony	0x0268
#define 	CRAM_ADDR_ATT_TIME_OutGainCenter_sony	0x0269
#define 	CRAM_ADDR_REL_TIME_OutGainCenter_sony	0x026A

// Component: COverGEQCenter
#define 	CRAM_ADDR_BAND1_COverGEQCenter_sony	0x026B
#define 	CRAM_ADDR_BAND2_COverGEQCenter_sony	0x0270
#define 	CRAM_ADDR_BAND3_COverGEQCenter_sony	0x0275

// Component: OutCenterMix
#define 	CRAM_ADDR_VOL_IN1_OutCenterMix_sony	0x027A
#define 	CRAM_ADDR_VOL_IN2_OutCenterMix_sony	0x027B

// Component: OutCenterVolume
#define 	CRAM_ADDR_LEVEL_OutCenterVolume_sony	0x027C
#define 	CRAM_ADDR_ATT_TIME_OutCenterVolume_sony	0x027D
#define 	CRAM_ADDR_REL_TIME_OutCenterVolume_sony	0x027E

// Component: OutVolumeCenter
#define 	CRAM_ADDR_LEVEL_OutVolumeCenter_sony	0x027F
#define 	CRAM_ADDR_ATT_TIME_OutVolumeCenter_sony	0x0280
#define 	CRAM_ADDR_REL_TIME_OutVolumeCenter_sony	0x0281

// Component: OutMuteCenter
#define 	CRAM_ADDR_LEVEL_OutMuteCenter_sony	0x0282
#define 	CRAM_ADDR_ATT_TIME_OutMuteCenter_sony	0x0283
#define 	CRAM_ADDR_REL_TIME_OutMuteCenter_sony	0x0284

// Component: OutLimiterCenter
#define 	CRAM_ADDR_ATT_TIME_OutLimiterCenter_sony	0x0286
#define 	CRAM_ADDR_REL_TIME_OutLimiterCenter_sony	0x0287
#define 	CRAM_ADDR_THRESHOLD_OutLimiterCenter_sony	0x028A

// Component: Out6SW2
#define 	CRAM_ADDR_IN1_Out6SW2_sony	0x028D
#define 	CRAM_ADDR_IN2_Out6SW2_sony	0x028E

// Component: RSEGEQ
#define 	CRAM_ADDR_BAND1_RSEGEQ_sony	0x028F
#define 	CRAM_ADDR_BAND2_RSEGEQ_sony	0x0294
#define 	CRAM_ADDR_BAND3_RSEGEQ_sony	0x0299
#define 	CRAM_ADDR_BAND4_RSEGEQ_sony	0x029E
#define 	CRAM_ADDR_BAND5_RSEGEQ_sony	0x02A3
#define 	CRAM_ADDR_BAND6_RSEGEQ_sony	0x02A8

// Component: RSEGEQSW2
#define 	CRAM_ADDR_IN1_RSEGEQSW2_sony	0x02AD
#define 	CRAM_ADDR_IN2_RSEGEQSW2_sony	0x02AE

// Component: RSEOutVolumeL
#define 	CRAM_ADDR_LEVEL_RSEOutVolumeL_sony	0x02AF
#define 	CRAM_ADDR_ATT_TIME_RSEOutVolumeL_sony	0x02B0
#define 	CRAM_ADDR_REL_TIME_RSEOutVolumeL_sony	0x02B1

// Component: RSEOutVolumeR
#define 	CRAM_ADDR_LEVEL_RSEOutVolumeR_sony	0x02B2
#define 	CRAM_ADDR_ATT_TIME_RSEOutVolumeR_sony	0x02B3
#define 	CRAM_ADDR_REL_TIME_RSEOutVolumeR_sony	0x02B4

// Component: RSEOutVolumeLR
#define 	CRAM_ADDR_LEVEL_RSEOutVolumeLR_sony	0x02B5
#define 	CRAM_ADDR_ATT_TIME_RSEOutVolumeLR_sony	0x02B6
#define 	CRAM_ADDR_REL_TIME_RSEOutVolumeLR_sony	0x02B7

// Component: RSEOutMute
#define 	CRAM_ADDR_LEVEL_RSEOutMute_sony	0x02B8
#define 	CRAM_ADDR_ATT_TIME_RSEOutMute_sony	0x02B9
#define 	CRAM_ADDR_REL_TIME_RSEOutMute_sony 0x02BA

// Component: COverGEQFLR
#define 	CRAM_ADDR_BAND1_COverGEQFLR_sony	0x02BB
#define 	CRAM_ADDR_BAND2_COverGEQFLR_sony	0x02C0
#define 	CRAM_ADDR_BAND3_COverGEQFLR_sony	0x02C5

// Component: OutVolumeFLR
#define 	CRAM_ADDR_LEVEL_OutVolumeFLR_sony	0x02CA
#define 	CRAM_ADDR_ATT_TIME_OutVolumeFLR_sony	0x02CB
#define 	CRAM_ADDR_REL_TIME_OutVolumeFLR_sony	0x02CC

// Component: OutMuteFLR
#define 	CRAM_ADDR_LEVEL_OutMuteFLR_sony	0x02CD
#define 	CRAM_ADDR_ATT_TIME_OutMuteFLR_sony	0x02CE
#define 	CRAM_ADDR_REL_TIME_OutMuteFLR_sony	0x02CF

// Component: OutLimiterFL
#define 	CRAM_ADDR_ATT_TIME_OutLimiterFL_sony	0x02D1
#define 	CRAM_ADDR_REL_TIME_OutLimiterFL_sony	0x02D2
#define 	CRAM_ADDR_THRESHOLD_OutLimiterFL_sony	0x02D5

// Component: OutGainRL
#define 	CRAM_ADDR_LEVEL_OutGainRL_sony	0x02D8
#define 	CRAM_ADDR_ATT_TIME_OutGainRL_sony	0x02D9
#define 	CRAM_ADDR_REL_TIME_OutGainRL_sony	0x02DA

// Component: OutGEQRL
#define 	CRAM_ADDR_BAND1_OutGEQRL_sony	0x02DB
#define 	CRAM_ADDR_BAND2_OutGEQRL_sony	0x02E0
#define 	CRAM_ADDR_BAND3_OutGEQRL_sony	0x02E5
#define 	CRAM_ADDR_BAND4_OutGEQRL_sony	0x02EA
#define 	CRAM_ADDR_BAND5_OutGEQRL_sony	0x02EF
#define 	CRAM_ADDR_BAND6_OutGEQRL_sony	0x02F4
#define 	CRAM_ADDR_BAND7_OutGEQRL_sony	0x02F9
#define 	CRAM_ADDR_BAND8_OutGEQRL_sony	0x02FE
#define 	CRAM_ADDR_BAND9_OutGEQRL_sony	0x0303

// Component: OutGainRR
#define 	CRAM_ADDR_LEVEL_OutGainRR_sony	0x0308
#define 	CRAM_ADDR_ATT_TIME_OutGainRR_sony	0x0309
#define 	CRAM_ADDR_REL_TIME_OutGainRR_sony	0x030A

// Component: OutGEQRR
#define 	CRAM_ADDR_BAND1_OutGEQRR_sony	0x030B
#define 	CRAM_ADDR_BAND2_OutGEQRR_sony	0x0310
#define 	CRAM_ADDR_BAND3_OutGEQRR_sony	0x0315
#define 	CRAM_ADDR_BAND4_OutGEQRR_sony	0x031A
#define 	CRAM_ADDR_BAND5_OutGEQRR_sony 	0x031F
#define 	CRAM_ADDR_BAND6_OutGEQRR_sony	0x0324
#define 	CRAM_ADDR_BAND7_OutGEQRR_sony	0x0329
#define 	CRAM_ADDR_BAND8_OutGEQRR_sony	0x032E
#define 	CRAM_ADDR_BAND9_OutGEQRR_sony	0x0333

// Component: MediaOutVolumeRLR
#define 	CRAM_ADDR_LEVEL_MediaOutVolumeRLR_sony	0x0338
#define 	CRAM_ADDR_ATT_TIME_MediaOutVolumeRLR_sony	0x0339
#define 	CRAM_ADDR_REL_TIME_MediaOutVolumeRLR	0x033A

// Component: OutMixRL
#define 	CRAM_ADDR_VOL_IN1_OutMixRL_sony	0x033B
#define 	CRAM_ADDR_VOL_IN2_OutMixRL_sony	0x033C
#define 	CRAM_ADDR_VOL_IN3_OutMixRL_sony	0x033D
#define 	CRAM_ADDR_VOL_IN4_OutMixRL_sony	0x033E
#define 	CRAM_ADDR_VOL_IN5_OutMixRL_sony	0x033F

// Component: OutDelayRL
#define 	ORAM_ADDR_WRITE_OutDelayRL_sony	0x0004
#define 	ORAM_ADDR_READ1_OutDelayRL_sony	0x0005

// Component: FadeBalanceRL
#define 	CRAM_ADDR_LEVEL_FadeBalanceRL_sony	0x0340
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceRL_sony	0x0341
#define 	CRAM_ADDR_REL_TIME_FadeBalanceRL_sony	0x0342

// Component: OutMixRR
#define 	CRAM_ADDR_VOL_IN1_OutMixRR_sony	0x0343
#define 	CRAM_ADDR_VOL_IN2_OutMixRR_sony	0x0344
#define 	CRAM_ADDR_VOL_IN3_OutMixRR_sony	0x0345
#define 	CRAM_ADDR_VOL_IN4_OutMixRR_sony	0x0346
#define 	CRAM_ADDR_VOL_IN5_OutMixRR_sony	0x0347

// Component: OutDelayRR
#define 	ORAM_ADDR_WRITE_OutDelayRR_sony	0x0006
#define 	ORAM_ADDR_READ1_OutDelayRR_sony	0x0007

// Component: FadeBalanceRR
#define 	CRAM_ADDR_LEVEL_FadeBalanceRR_sony	0x0348
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceRR_sony	0x0349
#define 	CRAM_ADDR_REL_TIME_FadeBalanceRR_sony	0x034A

// Component: SSVGainRLR
#define 	CRAM_ADDR_LEVEL_SSVGainRLR_sony	0x034B
#define 	CRAM_ADDR_ATT_TIME_SSVGainRLR_sony	0x034C
#define 	CRAM_ADDR_REL_TIME_SSVGainRLR_sony	0x034D

// Component: SSVGEQRLR
#define 	CRAM_ADDR_BAND1_SSVGEQRLR_sony	0x034E
#define 	CRAM_ADDR_BAND2_SSVGEQRLR_sony	0x0353
#define 	CRAM_ADDR_BAND3_SSVGEQRLR_sony	0x0358
#define 	CRAM_ADDR_BAND4_SSVGEQRLR_sony	0x035D
#define 	CRAM_ADDR_BAND5_SSVGEQRLR_sony	0x0362
#define 	CRAM_ADDR_BAND6_SSVGEQRLR_sony	0x0367

// Component: COverGEQRLR
#define 	CRAM_ADDR_BAND1_COverGEQRLR_sony	0x036C
#define 	CRAM_ADDR_BAND2_COverGEQRLR_sony	0x0371
#define 	CRAM_ADDR_BAND3_COverGEQRLR_sony	0x0376

// Component: OutVolumeRLR
#define 	CRAM_ADDR_LEVEL_OutVolumeRLR_sony	0x037B
#define 	CRAM_ADDR_ATT_TIME_OutVolumeRLR_sony	0x037C
#define 	CRAM_ADDR_REL_TIME_OutVolumeRLR_sony	0x037D

// Component: OutMuteRLR
#define 	CRAM_ADDR_LEVEL_OutMuteRLR_sony	0x037E
#define 	CRAM_ADDR_ATT_TIME_OutMuteRLR_sony	0x037F
#define 	CRAM_ADDR_REL_TIME_OutMuteRLR_sony	0x0380

// Component: OutLimiterRL
#define 	CRAM_ADDR_ATT_TIME_OutLimiterRL_sony	0x0382
#define 	CRAM_ADDR_REL_TIME_OutLimiterRL_sony	0x0383
#define 	CRAM_ADDR_THRESHOLD_OutLimiterRL_sony	0x0386

// Component: OutLimiterFR
#define 	CRAM_ADDR_ATT_TIME_OutLimiterFR_sony	0x038A
#define 	CRAM_ADDR_REL_TIME_OutLimiterFR_sony	0x038B
#define 	CRAM_ADDR_THRESHOLD_OutLimiterFR_sony	0x038E

// Component: OutLimiterRR
#define 	CRAM_ADDR_ATT_TIME_OutLimiterRR_sony	0x0392
#define 	CRAM_ADDR_REL_TIME_OutLimiterRR_sony	0x0393
#define 	CRAM_ADDR_THRESHOLD_OutLimiterRR_sony	0x0396

// Component: Out5SW4
#define 	CRAM_ADDR_IN1_Out5SW4_sony	0x0399
#define 	CRAM_ADDR_IN2_Out5SW4_sony	0x039A
#define 	CRAM_ADDR_IN3_Out5SW4_sony	0x039B
#define 	CRAM_ADDR_IN4_Out5SW4_sony	0x039C

// Component: SpeAnaMix
#define 	CRAM_ADDR_VOL_IN1_SpeAnaMix_sony	0x039D
#define 	CRAM_ADDR_VOL_IN2_SpeAnaMix_sony	0x039E

// Component: SpeAnaMedia
#define 	CRAM_ADDR_SpeAnaMedia_sony	0x039F
#define 	ORAM_ADDR_SpeAnaMedia_sony	0x0008
#define 	DRAM0_ADDR_SpeAnaMedia_sony	0x02DB
#define 	DRAM1_ADDR_SpeAnaMedia_sony	0x0000
#define 	CRAM_ADDR_FREQ_SINE_SpeAnaMedia_sony	0x039F
#define 	CRAM_ADDR_GAIN_LCH_SINE_SpeAnaMedia_sony	0x03A1
#define 	CRAM_ADDR_GAIN_RCH_SINE_SpeAnaMedia_sony	0x03A2

// Component: SpeAnaRamMon
#define 	CRAM_ADDR_CDMONITOR_SpeAnaRamMon_sony	0x0417

#endif /* AK7739_AUDIO_EFFECT_ADDR_H_ */

