/*
 * ak7739_audio_effect_addr.h  --  audio driver for ak7739
 *
 */

#ifndef AK7739_AUDIO_EFFECT_ADDR_H_
#define AK7739_AUDIO_EFFECT_ADDR_H_

// AKM_AudioEffect_Basic_UseCase_V01_3_0_addr
// Component: Media1Gain
#define 	CRAM_ADDR_LEVEL_Media1Gain	0x01F3
#define 	CRAM_ADDR_ATT_TIME_Media1Gain	0x01F4
#define 	CRAM_ADDR_REL_TIME_Media1Gain	0x01F5

// Component: Media2Gain
#define 	CRAM_ADDR_LEVEL_Media2Gain	0x01F6
#define 	CRAM_ADDR_ATT_TIME_Media2Gain	0x01F7
#define 	CRAM_ADDR_REL_TIME_Media2Gain	0x01F8

// Component: Media3Gain
#define 	CRAM_ADDR_LEVEL_Media3Gain	0x01F9
#define 	CRAM_ADDR_ATT_TIME_Media3Gain	0x01FA
#define 	CRAM_ADDR_REL_TIME_Media3Gain	0x01FB

// Component: Media23LMix
#define 	CRAM_ADDR_VOL_IN1_Media23LMix	0x01FC
#define 	CRAM_ADDR_VOL_IN2_Media23LMix	0x01FD

// Component: Media23RMix
#define 	CRAM_ADDR_VOL_IN1_Media23RMix	0x01FE
#define 	CRAM_ADDR_VOL_IN2_Media23RMix	0x01FF

// Component: Media23Gain
#define 	CRAM_ADDR_LEVEL_Media23Gain	0x0200
#define 	CRAM_ADDR_ATT_TIME_Media23Gain	0x0201
#define 	CRAM_ADDR_REL_TIME_Media23Gain	0x0202

// Component: MediaInputSW4
#define 	CRAM_ADDR_IN1_MediaInputSW4	0x0203
#define 	CRAM_ADDR_IN2_MediaInputSW4	0x0204
#define 	CRAM_ADDR_IN3_MediaInputSW4	0x0205
#define 	CRAM_ADDR_IN4_MediaInputSW4	0x0206

// Component: MediaInVolume
#define 	CRAM_ADDR_LEVEL_MediaInVolume	0x0207
#define 	CRAM_ADDR_ATT_TIME_MediaInVolume	0x0208
#define 	CRAM_ADDR_REL_TIME_MediaInVolume	0x0209

// Component: DCCut
#define 	CRAM_ADDR_BAND1_DCCut	0x020A
#define 	CRAM_ADDR_BAND2_DCCut	0x020F
#define 	CRAM_ADDR_BAND3_DCCut	0x0214

// Component: DCCutSW2
#define 	CRAM_ADDR_IN1_DCCutSW2	0x0219
#define 	CRAM_ADDR_IN2_DCCutSW2	0x021A

// Component: DeEmphasis
#define 	CRAM_ADDR_BAND1_DeEmphasis	0x021B

// Component: DeEmpSW2
#define 	CRAM_ADDR_IN1_DeEmpSW2	0x0220
#define 	CRAM_ADDR_IN2_DeEmpSW2	0x0221

// Component: Compressor
#define 	CRAM_ADDR_ATT_TIME_Compressor	0x0223
#define 	CRAM_ADDR_REL_TIME_Compressor	0x0224
#define 	CRAM_ADDR_P1_INFO_Compressor	0x0229
#define 	CRAM_ADDR_P2_INFO_Compressor	0x022C
#define 	CRAM_ADDR_P3_INFO_Compressor	0x022F
#define 	CRAM_ADDR_P4_INFO_Compressor	0x0232
#define 	CRAM_ADDR_P5_INFO_Compressor	0x0235
#define 	CRAM_ADDR_BIASGAIN_Compressor	0x023B

// Component: CompressorSW2
#define 	CRAM_ADDR_IN1_CompressorSW2	0x023C
#define 	CRAM_ADDR_IN2_CompressorSW2	0x023D

// Component: BMTGain
#define 	CRAM_ADDR_LEVEL_BMTGain	0x023E
#define 	CRAM_ADDR_ATT_TIME_BMTGain	0x023F
#define 	CRAM_ADDR_REL_TIME_BMTGain	0x0240

// Component: BMTGEQ
#define 	CRAM_ADDR_BAND1_BMTGEQ		0x0241
#define 	CRAM_ADDR_BAND2_BMTGEQ		0x0246
#define 	CRAM_ADDR_BAND3_BMTGEQ		0x024B
#define 	CRAM_ADDR_BAND4_BMTGEQ		0x0250
#define 	CRAM_ADDR_BAND5_BMTGEQ		0x0255
#define 	CRAM_ADDR_BAND6_BMTGEQ		0x025A

// Component: BMTSW2
#define 	CRAM_ADDR_IN1_BMTSW2	0x025F
#define 	CRAM_ADDR_IN2_BMTSW2	0x0260

// Component: PersonalGain
#define 	CRAM_ADDR_LEVEL_PersonalGain	0x0261
#define 	CRAM_ADDR_ATT_TIME_PersonalGain	0x0262
#define 	CRAM_ADDR_REL_TIME_PersonalGain	0x0263

// Component: PersonalGEQ
#define 	CRAM_ADDR_BAND1_PersonalGEQ	0x0264
#define 	CRAM_ADDR_BAND2_PersonalGEQ	0x0269
#define 	CRAM_ADDR_BAND3_PersonalGEQ	0x026E
#define 	CRAM_ADDR_BAND4_PersonalGEQ	0x0273
#define 	CRAM_ADDR_BAND5_PersonalGEQ	0x0278
#define 	CRAM_ADDR_BAND6_PersonalGEQ	0x027D

// Component: POPGain
#define 	CRAM_ADDR_LEVEL_POPGain	0x0282
#define 	CRAM_ADDR_ATT_TIME_POPGain	0x0283
#define 	CRAM_ADDR_REL_TIME_POPGain	0x0284

// Component: POPGEQ
#define 	CRAM_ADDR_BAND1_POPGEQ	0x0285
#define 	CRAM_ADDR_BAND2_POPGEQ	0x028A
#define 	CRAM_ADDR_BAND3_POPGEQ	0x028F
#define 	CRAM_ADDR_BAND4_POPGEQ	0x0294
#define 	CRAM_ADDR_BAND5_POPGEQ	0x0299
#define 	CRAM_ADDR_BAND6_POPGEQ	0x029E

// Component: RockGain
#define 	CRAM_ADDR_LEVEL_RockGain	0x02A3
#define 	CRAM_ADDR_ATT_TIME_RockGain	0x02A4
#define 	CRAM_ADDR_REL_TIME_RockGain	0x02A5

// Component: RockGEQ
#define 	CRAM_ADDR_BAND1_RockGEQ	0x02A6
#define 	CRAM_ADDR_BAND2_RockGEQ	0x02AB
#define 	CRAM_ADDR_BAND3_RockGEQ	0x02B0
#define 	CRAM_ADDR_BAND4_RockGEQ	0x02B5
#define 	CRAM_ADDR_BAND5_RockGEQ	0x02BA
#define 	CRAM_ADDR_BAND6_RockGEQ	0x02BF

// Component: ClassicalGain
#define 	CRAM_ADDR_LEVEL_ClassicalGain	0x02C4
#define 	CRAM_ADDR_ATT_TIME_ClassicalGain	0x02C5
#define 	CRAM_ADDR_REL_TIME_ClassicalGain	0x02C6

// Component: ClassicalGEQ
#define 	CRAM_ADDR_BAND1_ClassicalGEQ	0x02C7
#define 	CRAM_ADDR_BAND2_ClassicalGEQ	0x02CC
#define 	CRAM_ADDR_BAND3_ClassicalGEQ	0x02D1
#define 	CRAM_ADDR_BAND4_ClassicalGEQ	0x02D6
#define 	CRAM_ADDR_BAND5_ClassicalGEQ	0x02DB
#define 	CRAM_ADDR_BAND6_ClassicalGEQ	0x02E0

// Component: SoundModeSW4
#define 	CRAM_ADDR_IN1_SoundModeSW4	0x02E5
#define 	CRAM_ADDR_IN2_SoundModeSW4	0x02E6
#define 	CRAM_ADDR_IN3_SoundModeSW4	0x02E7
#define 	CRAM_ADDR_IN4_SoundModeSW4	0x02E8

// Component: SoundModeSW2
#define 	CRAM_ADDR_IN1_SoundModeSW2	0x02E9
#define 	CRAM_ADDR_IN2_SoundModeSW2	0x02EA

// Component: LoudnessGain
#define 	CRAM_ADDR_LEVEL_LoudnessGain	0x02EB
#define 	CRAM_ADDR_ATT_TIME_LoudnessGain	0x02EC
#define 	CRAM_ADDR_REL_TIME_LoudnessGain	0x02ED

// Component: LoudnessGEQ
#define 	CRAM_ADDR_BAND1_LoudnessGEQ	0x02EE
#define 	CRAM_ADDR_BAND2_LoudnessGEQ	0x02F3
#define 	CRAM_ADDR_BAND3_LoudnessGEQ	0x02F8
#define 	CRAM_ADDR_BAND4_LoudnessGEQ	0x02FD
#define 	CRAM_ADDR_BAND5_LoudnessGEQ	0x0302
#define 	CRAM_ADDR_BAND6_LoudnessGEQ	0x0307
#define 	CRAM_ADDR_BAND7_LoudnessGEQ	0x030C

// Component: LoudnessSW2
#define 	CRAM_ADDR_IN1_LoudnessSW2	0x0311
#define 	CRAM_ADDR_IN2_LoudnessSW2	0x0312

// Component: EffectInGainL
#define 	CRAM_ADDR_LEVEL_EffectInGainL	0x0313
#define 	CRAM_ADDR_ATT_TIME_EffectInGainL	0x0314
#define 	CRAM_ADDR_REL_TIME_EffectInGainL	0x0315

// Component: SurroundMixL
#define 	CRAM_ADDR_VOL_IN1_SurroundMixL	0x0316
#define 	CRAM_ADDR_VOL_IN2_SurroundMixL	0x0317

// Component: EffectInGainR
#define 	CRAM_ADDR_LEVEL_EffectInGainR	0x0318
#define 	CRAM_ADDR_ATT_TIME_EffectInGainR	0x0319
#define 	CRAM_ADDR_REL_TIME_EffectInGainR	0x031A

// Component: SurroundMixR
#define 	CRAM_ADDR_VOL_IN1_SurroundMixR	0x031B
#define 	CRAM_ADDR_VOL_IN2_SurroundMixR	0x031C

// Component: SurroundSW2
#define 	CRAM_ADDR_IN1_SurroundSW2	0x031D
#define 	CRAM_ADDR_IN2_SurroundSW2	0x031E

// Component: CallerRSS_01
#define 	CRAM_ADDR_CallerRSS_01	0x031F
#define 	ORAM_ADDR_CallerRSS_01	0x0010
#define 	DRAM0_ADDR_CallerRSS_01	0x036A
#define 	DRAM1_ADDR_CallerRSS_01	0x0000

// Component: MainEffectSW2
#define 	CRAM_ADDR_IN1_MainEffectSW2	0x031F
#define 	CRAM_ADDR_IN2_MainEffectSW2	0x0320

// Component: OutGainFL
#define 	CRAM_ADDR_LEVEL_OutGainFL	0x0321
#define 	CRAM_ADDR_ATT_TIME_OutGainFL	0x0322
#define 	CRAM_ADDR_REL_TIME_OutGainFL	0x0323

// Component: OutGEQFL
#define 	CRAM_ADDR_BAND1_OutGEQFL	0x0324
#define 	CRAM_ADDR_BAND2_OutGEQFL	0x0329
#define 	CRAM_ADDR_BAND3_OutGEQFL	0x032E

// Component: OutGainFR
#define 	CRAM_ADDR_LEVEL_OutGainFR	0x0333
#define 	CRAM_ADDR_ATT_TIME_OutGainFR	0x0334
#define 	CRAM_ADDR_REL_TIME_OutGainFR	0x0335

// Component: OutGEQFR
#define 	CRAM_ADDR_BAND1_OutGEQFR	0x0336
#define 	CRAM_ADDR_BAND2_OutGEQFR	0x033B
#define 	CRAM_ADDR_BAND3_OutGEQFR	0x0340

// Component: MediaOutVolumeFLR
#define 	CRAM_ADDR_LEVEL_MediaOutVolumeFLR	0x0345
#define 	CRAM_ADDR_ATT_TIME_MediaOutVolumeFLR	0x0346
#define 	CRAM_ADDR_REL_TIME_MediaOutVolumeFLR	0x0347

// Component: NaviGain
#define 	CRAM_ADDR_LEVEL_NaviGain	0x0348
#define 	CRAM_ADDR_ATT_TIME_NaviGain	0x0349
#define 	CRAM_ADDR_REL_TIME_NaviGain	0x034A

// Component: NaviGEQ
#define 	CRAM_ADDR_BAND1_NaviGEQ	0x034B
#define 	CRAM_ADDR_BAND2_NaviGEQ	0x0350
#define 	CRAM_ADDR_BAND3_NaviGEQ	0x0355
#define 	CRAM_ADDR_BAND4_NaviGEQ	0x035A
#define 	CRAM_ADDR_BAND5_NaviGEQ	0x035F
#define 	CRAM_ADDR_BAND6_NaviGEQ	0x0364

// Component: NaviGEQSW2
#define 	CRAM_ADDR_IN1_NaviGEQSW2	0x0369
#define 	CRAM_ADDR_IN2_NaviGEQSW2	0x036A

// Component: NaviVolume
#define 	CRAM_ADDR_LEVEL_NaviVolume	0x036B
#define 	CRAM_ADDR_ATT_TIME_NaviVolume	0x036C
#define 	CRAM_ADDR_REL_TIME_NaviVolume	0x036D

// Component: NaviMute
#define 	CRAM_ADDR_LEVEL_NaviMute	0x036E
#define 	CRAM_ADDR_ATT_TIME_NaviMute	0x036F
#define 	CRAM_ADDR_REL_TIME_NaviMute	0x0370

// Component: PhoneGain
#define 	CRAM_ADDR_LEVEL_PhoneGain	0x0371
#define 	CRAM_ADDR_ATT_TIME_PhoneGain	0x0372
#define 	CRAM_ADDR_REL_TIME_PhoneGain	0x0373

// Component: PhoneGEQ
#define 	CRAM_ADDR_BAND1_PhoneGEQ	0x0374
#define 	CRAM_ADDR_BAND2_PhoneGEQ	0x0379
#define 	CRAM_ADDR_BAND3_PhoneGEQ	0x037E
#define 	CRAM_ADDR_BAND4_PhoneGEQ	0x0383
#define 	CRAM_ADDR_BAND5_PhoneGEQ	0x0388
#define 	CRAM_ADDR_BAND6_PhoneGEQ	0x038D

// Component: PhoneGEQSW2
#define 	CRAM_ADDR_IN1_PhoneGEQSW2	0x0392
#define 	CRAM_ADDR_IN2_PhoneGEQSW2	0x0393

// Component: PhoneVolume
#define 	CRAM_ADDR_LEVEL_PhoneVolume	0x0394
#define 	CRAM_ADDR_ATT_TIME_PhoneVolume	0x0395
#define 	CRAM_ADDR_REL_TIME_PhoneVolume	0x0396

// Component: PhoneMute
#define 	CRAM_ADDR_LEVEL_PhoneMute	0x0397
#define 	CRAM_ADDR_ATT_TIME_PhoneMute	0x0398
#define 	CRAM_ADDR_REL_TIME_PhoneMute	0x0399

// Component: Beep
#define 	CRAM_ADDR_Beep	0x039A
#define 	ORAM_ADDR_Beep	0x0010
#define 	DRAM0_ADDR_Beep	0x03D0
#define 	DRAM1_ADDR_Beep	0x0000
#define 	CRAM_ADDR_BEEP_TRIGGER_Beep	0x039B
#define 	CRAM_ADDR_BEEP_FREQ_Beep	0x039D
#define 	CRAM_ADDR_BEEP_ATTACK_STEP_Beep	0x039F
#define 	CRAM_ADDR_BEEP_DECAY_STEP_Beep	0x03A0
#define 	CRAM_ADDR_BEEP_ATTACK_TIME_Beep	0x03A1
#define 	CRAM_ADDR_BEEP_SUSTAIN_TIME_Beep	0x03A2
#define 	CRAM_ADDR_BEEP_SOUND_CADENCE_Beep	0x03A3
#define 	CRAM_ADDR_BEEP_REPS_Beep	0x03A4
#define 	CRAM_ADDR_BEEP_STATUS_Beep	0x03AE
#define 	CRAM_ADDR_BEEP_REPSREMAIN_Beep	0x03AF

// Component: BeepGain
#define 	CRAM_ADDR_LEVEL_BeepGain	0x03B0
#define 	CRAM_ADDR_ATT_TIME_BeepGain	0x03B1
#define 	CRAM_ADDR_REL_TIME_BeepGain	0x03B2

// Component: BeepVolume
#define 	CRAM_ADDR_LEVEL_BeepVolume	0x03B3
#define 	CRAM_ADDR_ATT_TIME_BeepVolume	0x03B4
#define 	CRAM_ADDR_REL_TIME_BeepVolume	0x03B5

// Component: BeepMute
#define 	CRAM_ADDR_LEVEL_BeepMute	0x03B6
#define 	CRAM_ADDR_ATT_TIME_BeepMute	0x03B7
#define 	CRAM_ADDR_REL_TIME_BeepMute	0x03B8

// Component: RSEGain
#define 	CRAM_ADDR_LEVEL_RSEGain	0x03B9
#define 	CRAM_ADDR_ATT_TIME_RSEGain	0x03BA
#define 	CRAM_ADDR_REL_TIME_RSEGain	0x03BB

// Component: ChimeMix
#define 	CRAM_ADDR_VOL_IN1_ChimeMix	0x03BC
#define 	CRAM_ADDR_VOL_IN2_ChimeMix	0x03BD

// Component: ChimeVolume
#define 	CRAM_ADDR_LEVEL_ChimeVolume	0x03BE
#define 	CRAM_ADDR_ATT_TIME_ChimeVolume	0x03BF
#define 	CRAM_ADDR_REL_TIME_ChimeVolume	0x03C0

// Component: ChimeMute
#define 	CRAM_ADDR_LEVEL_ChimeMute	0x03C1
#define 	CRAM_ADDR_ATT_TIME_ChimeMute	0x03C2
#define 	CRAM_ADDR_REL_TIME_ChimeMute	0x03C3

// Component: OutMixFL
#define 	CRAM_ADDR_VOL_IN1_OutMixFL	0x03C4
#define 	CRAM_ADDR_VOL_IN2_OutMixFL	0x03C5
#define 	CRAM_ADDR_VOL_IN3_OutMixFL	0x03C6
#define 	CRAM_ADDR_VOL_IN4_OutMixFL	0x03C7
#define 	CRAM_ADDR_VOL_IN5_OutMixFL	0x03C8

// Component: OutDelayFL
#define 	ORAM_ADDR_WRITE_OutDelayFL	0x0010
#define 	ORAM_ADDR_READ1_OutDelayFL	0x0011

// Component: FadeBalanceFL
#define 	CRAM_ADDR_LEVEL_FadeBalanceFL	0x03C9
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceFL	0x03CA
#define 	CRAM_ADDR_REL_TIME_FadeBalanceFL	0x03CB

// Component: OutMixFR
#define 	CRAM_ADDR_VOL_IN1_OutMixFR	0x03CC
#define 	CRAM_ADDR_VOL_IN2_OutMixFR	0x03CD
#define 	CRAM_ADDR_VOL_IN3_OutMixFR	0x03CE
#define 	CRAM_ADDR_VOL_IN4_OutMixFR	0x03CF
#define 	CRAM_ADDR_VOL_IN5_OutMixFR	0x03D0

// Component: OutDelayFR
#define 	ORAM_ADDR_WRITE_OutDelayFR	0x0012
#define 	ORAM_ADDR_READ1_OutDelayFR	0x0013

// Component: FadeBalanceFR
#define 	CRAM_ADDR_LEVEL_FadeBalanceFR	0x03D1
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceFR	0x03D2
#define 	CRAM_ADDR_REL_TIME_FadeBalanceFR	0x03D3

// Component: SSVGainFLR
#define 	CRAM_ADDR_LEVEL_SSVGainFLR	0x03D4
#define 	CRAM_ADDR_ATT_TIME_SSVGainFLR	0x03D5
#define 	CRAM_ADDR_REL_TIME_SSVGainFLR	0x03D6

// Component: SSVGEQFLR
#define 	CRAM_ADDR_BAND1_SSVGEQFLR	0x03D7
#define 	CRAM_ADDR_BAND2_SSVGEQFLR	0x03DC
#define 	CRAM_ADDR_BAND3_SSVGEQFLR	0x03E1
#define 	CRAM_ADDR_BAND4_SSVGEQFLR	0x03E6
#define 	CRAM_ADDR_BAND5_SSVGEQFLR	0x03EB
#define 	CRAM_ADDR_BAND6_SSVGEQFLR	0x03F0

// Component: OutGainSW
#define 	CRAM_ADDR_LEVEL_OutGainSW	0x03F5
#define 	CRAM_ADDR_ATT_TIME_OutGainSW	0x03F6
#define 	CRAM_ADDR_REL_TIME_OutGainSW	0x03F7

// Component: COverGEQSW
#define 	CRAM_ADDR_BAND1_COverGEQSW	0x03F8
#define 	CRAM_ADDR_BAND2_COverGEQSW	0x03FD
#define 	CRAM_ADDR_BAND3_COverGEQSW	0x0402

// Component: OutSWVolumeL
#define 	CRAM_ADDR_LEVEL_OutSWVolumeL	0x0407
#define 	CRAM_ADDR_ATT_TIME_OutSWVolumeL	0x0408
#define 	CRAM_ADDR_REL_TIME_OutSWVolumeL	0x0409

// Component: OutSWVolumeR
#define 	CRAM_ADDR_LEVEL_OutSWVolumeR	0x040A
#define 	CRAM_ADDR_ATT_TIME_OutSWVolumeR	0x040B
#define 	CRAM_ADDR_REL_TIME_OutSWVolumeR	0x040C

// Component: OutSWVolumeLR
#define 	CRAM_ADDR_LEVEL_OutSWVolumeLR	0x040D
#define 	CRAM_ADDR_ATT_TIME_OutSWVolumeLR	0x040E
#define 	CRAM_ADDR_REL_TIME_OutSWVolumeLR	0x040F

// Component: OutMuteSW
#define 	CRAM_ADDR_LEVEL_OutMuteSW	0x0410
#define 	CRAM_ADDR_ATT_TIME_OutMuteSW	0x0411
#define 	CRAM_ADDR_REL_TIME_OutMuteSW	0x0412

// Component: OutLimiterSWL
#define 	CRAM_ADDR_ATT_TIME_OutLimiterSWL	0x0414
#define 	CRAM_ADDR_REL_TIME_OutLimiterSWL	0x0415
#define 	CRAM_ADDR_THRESHOLD_OutLimiterSWL	0x0418

// Component: OutLimiterSWR
#define 	CRAM_ADDR_ATT_TIME_OutLimiterSWR	0x041C
#define 	CRAM_ADDR_REL_TIME_OutLimiterSWR	0x041D
#define 	CRAM_ADDR_THRESHOLD_OutLimiterSWR	0x0420

// Component: OutGainCenter
#define 	CRAM_ADDR_LEVEL_OutGainCenter	0x0423
#define 	CRAM_ADDR_ATT_TIME_OutGainCenter	0x0424
#define 	CRAM_ADDR_REL_TIME_OutGainCenter	0x0425

// Component: COverGEQCenter
#define 	CRAM_ADDR_BAND1_COverGEQCenter	0x0426
#define 	CRAM_ADDR_BAND2_COverGEQCenter	0x042B
#define 	CRAM_ADDR_BAND3_COverGEQCenter	0x0430

// Component: OutCenterMix
#define 	CRAM_ADDR_VOL_IN1_OutCenterMix	0x0435
#define 	CRAM_ADDR_VOL_IN2_OutCenterMix	0x0436

// Component: OutCenterVolume
#define 	CRAM_ADDR_LEVEL_OutCenterVolume	0x0437
#define 	CRAM_ADDR_ATT_TIME_OutCenterVolume	0x0438
#define 	CRAM_ADDR_REL_TIME_OutCenterVolume	0x0439

// Component: OutVolumeCenter
#define 	CRAM_ADDR_LEVEL_OutVolumeCenter	0x043A
#define 	CRAM_ADDR_ATT_TIME_OutVolumeCenter	0x043B
#define 	CRAM_ADDR_REL_TIME_OutVolumeCenter	0x043C

// Component: OutMuteCenter
#define 	CRAM_ADDR_LEVEL_OutMuteCenter	0x043D
#define 	CRAM_ADDR_ATT_TIME_OutMuteCenter	0x043E
#define 	CRAM_ADDR_REL_TIME_OutMuteCenter	0x043F

// Component: OutLimiterCenter
#define 	CRAM_ADDR_ATT_TIME_OutLimiterCenter	0x0441
#define 	CRAM_ADDR_REL_TIME_OutLimiterCenter	0x0442
#define 	CRAM_ADDR_THRESHOLD_OutLimiterCenter	0x0445

// Component: Out6SW2
#define 	CRAM_ADDR_IN1_Out6SW2	0x0448
#define 	CRAM_ADDR_IN2_Out6SW2	0x0449

// Component: RSEDCCut
#define 	CRAM_ADDR_BAND1_RSEDCCut	0x044A
#define 	CRAM_ADDR_BAND2_RSEDCCut	0x044F
#define 	CRAM_ADDR_BAND3_RSEDCCut	0x0454

// Component: RSEDCCutSW2
#define 	CRAM_ADDR_IN1_RSEDCCutSW2	0x0459
#define 	CRAM_ADDR_IN2_RSEDCCutSW2	0x045A

// Component: RSEDeEmphasis
#define 	CRAM_ADDR_BAND1_RSEDeEmphasis	0x045B

// Component: RSEDeEmpSW2
#define 	CRAM_ADDR_IN1_RSEDeEmpSW2	0x0460
#define 	CRAM_ADDR_IN2_RSEDeEmpSW2	0x0461

// Component: RSEBMTGain
#define 	CRAM_ADDR_LEVEL_RSEBMTGain	0x0462
#define 	CRAM_ADDR_ATT_TIME_RSEBMTGain	0x0463
#define 	CRAM_ADDR_REL_TIME_RSEBMTGain	0x0464

// Component: RSEBMTGEQ
#define 	CRAM_ADDR_BAND1_RSEBMTGEQ	0x0465
#define 	CRAM_ADDR_BAND2_RSEBMTGEQ	0x046A
#define 	CRAM_ADDR_BAND3_RSEBMTGEQ	0x046F
#define 	CRAM_ADDR_BAND4_RSEBMTGEQ	0x0474
#define 	CRAM_ADDR_BAND5_RSEBMTGEQ	0x0479
#define 	CRAM_ADDR_BAND6_RSEBMTGEQ	0x047E

// Component: RSEBMTSW2
#define 	CRAM_ADDR_IN1_RSEBMTSW2	0x0483
#define 	CRAM_ADDR_IN2_RSEBMTSW2	0x0484

// Component: RSEOutVolumeL
#define 	CRAM_ADDR_LEVEL_RSEOutVolumeL	0x0485
#define 	CRAM_ADDR_ATT_TIME_RSEOutVolumeL	0x0486
#define 	CRAM_ADDR_REL_TIME_RSEOutVolumeL	0x0487

// Component: RSEOutVolumeR
#define 	CRAM_ADDR_LEVEL_RSEOutVolumeR	0x0488
#define 	CRAM_ADDR_ATT_TIME_RSEOutVolumeR	0x0489
#define 	CRAM_ADDR_REL_TIME_RSEOutVolumeR	0x048A

// Component: RSEOutVolumeLR
#define 	CRAM_ADDR_LEVEL_RSEOutVolumeLR	0x048B
#define 	CRAM_ADDR_ATT_TIME_RSEOutVolumeLR	0x048C
#define 	CRAM_ADDR_REL_TIME_RSEOutVolumeLR	0x048D

// Component: RSEOutMute
#define 	CRAM_ADDR_LEVEL_RSEOutMute	0x048E
#define 	CRAM_ADDR_ATT_TIME_RSEOutMute	0x048F
#define 	CRAM_ADDR_REL_TIME_RSEOutMute	0x0490

// Component: COverGEQFLR
#define 	CRAM_ADDR_BAND1_COverGEQFLR	0x0491
#define 	CRAM_ADDR_BAND2_COverGEQFLR	0x0496
#define 	CRAM_ADDR_BAND3_COverGEQFLR	0x049B

// Component: OutVolumeFLR
#define 	CRAM_ADDR_LEVEL_OutVolumeFLR	0x04A0
#define 	CRAM_ADDR_ATT_TIME_OutVolumeFLR	0x04A1
#define 	CRAM_ADDR_REL_TIME_OutVolumeFLR	0x04A2

// Component: OutMuteFLR
#define 	CRAM_ADDR_LEVEL_OutMuteFLR	0x04A3
#define 	CRAM_ADDR_ATT_TIME_OutMuteFLR	0x04A4
#define 	CRAM_ADDR_REL_TIME_OutMuteFLR	0x04A5

// Component: OutLimiterFL
#define 	CRAM_ADDR_ATT_TIME_OutLimiterFL	0x04A7
#define 	CRAM_ADDR_REL_TIME_OutLimiterFL	0x04A8
#define 	CRAM_ADDR_THRESHOLD_OutLimiterFL	0x04AB

// Component: OutGainRL
#define 	CRAM_ADDR_LEVEL_OutGainRL	0x04AE
#define 	CRAM_ADDR_ATT_TIME_OutGainRL	0x04AF
#define 	CRAM_ADDR_REL_TIME_OutGainRL	0x04B0

// Component: OutGEQRL
#define 	CRAM_ADDR_BAND1_OutGEQRL	0x04B1
#define 	CRAM_ADDR_BAND2_OutGEQRL	0x04B6
#define 	CRAM_ADDR_BAND3_OutGEQRL	0x04BB

// Component: OutGainRR
#define 	CRAM_ADDR_LEVEL_OutGainRR	0x04C0
#define 	CRAM_ADDR_ATT_TIME_OutGainRR	0x04C1
#define 	CRAM_ADDR_REL_TIME_OutGainRR	0x04C2

// Component: OutGEQRR
#define 	CRAM_ADDR_BAND1_OutGEQRR	0x04C3
#define 	CRAM_ADDR_BAND2_OutGEQRR	0x04C8
#define 	CRAM_ADDR_BAND3_OutGEQRR	0x04CD

// Component: MediaOutVolumeRLR
#define 	CRAM_ADDR_LEVEL_MediaOutVolumeRLR	0x04D2
#define 	CRAM_ADDR_ATT_TIME_MediaOutVolumeRLR	0x04D3
#define 	CRAM_ADDR_REL_TIME_MediaOutVolumeRLR	0x04D4

// Component: OutMixRL
#define 	CRAM_ADDR_VOL_IN1_OutMixRL	0x04D5
#define 	CRAM_ADDR_VOL_IN2_OutMixRL	0x04D6
#define 	CRAM_ADDR_VOL_IN3_OutMixRL	0x04D7
#define 	CRAM_ADDR_VOL_IN4_OutMixRL	0x04D8
#define 	CRAM_ADDR_VOL_IN5_OutMixRL	0x04D9

// Component: OutDelayRL
#define 	ORAM_ADDR_WRITE_OutDelayRL	0x0014
#define 	ORAM_ADDR_READ1_OutDelayRL	0x0015

// Component: FadeBalanceRL
#define 	CRAM_ADDR_LEVEL_FadeBalanceRL	0x04DA
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceRL	0x04DB
#define 	CRAM_ADDR_REL_TIME_FadeBalanceRL	0x04DC

// Component: OutMixRR
#define 	CRAM_ADDR_VOL_IN1_OutMixRR	0x04DD
#define 	CRAM_ADDR_VOL_IN2_OutMixRR	0x04DE
#define 	CRAM_ADDR_VOL_IN3_OutMixRR	0x04DF
#define 	CRAM_ADDR_VOL_IN4_OutMixRR	0x04E0
#define 	CRAM_ADDR_VOL_IN5_OutMixRR	0x04E1

// Component: OutDelayRR
#define 	ORAM_ADDR_WRITE_OutDelayRR	0x0016
#define 	ORAM_ADDR_READ1_OutDelayRR	0x0017

// Component: FadeBalanceRR
#define 	CRAM_ADDR_LEVEL_FadeBalanceRR	0x04E2
#define 	CRAM_ADDR_ATT_TIME_FadeBalanceRR	0x04E3
#define 	CRAM_ADDR_REL_TIME_FadeBalanceRR	0x04E4

// Component: SSVGainRLR
#define 	CRAM_ADDR_LEVEL_SSVGainRLR	0x04E5
#define 	CRAM_ADDR_ATT_TIME_SSVGainRLR	0x04E6
#define 	CRAM_ADDR_REL_TIME_SSVGainRLR	0x04E7

// Component: SSVGEQRLR
#define 	CRAM_ADDR_BAND1_SSVGEQRLR	0x04E8
#define 	CRAM_ADDR_BAND2_SSVGEQRLR	0x04ED
#define 	CRAM_ADDR_BAND3_SSVGEQRLR	0x04F2
#define 	CRAM_ADDR_BAND4_SSVGEQRLR	0x04F7
#define 	CRAM_ADDR_BAND5_SSVGEQRLR	0x04FC
#define 	CRAM_ADDR_BAND6_SSVGEQRLR	0x0501

// Component: COverGEQRLR
#define 	CRAM_ADDR_BAND1_COverGEQRLR	0x0506
#define 	CRAM_ADDR_BAND2_COverGEQRLR	0x050B
#define 	CRAM_ADDR_BAND3_COverGEQRLR	0x0510

// Component: OutVolumeRLR
#define 	CRAM_ADDR_LEVEL_OutVolumeRLR	0x0515
#define 	CRAM_ADDR_ATT_TIME_OutVolumeRLR	0x0516
#define 	CRAM_ADDR_REL_TIME_OutVolumeRLR	0x0517

// Component: OutMuteRLR
#define 	CRAM_ADDR_LEVEL_OutMuteRLR	0x0518
#define 	CRAM_ADDR_ATT_TIME_OutMuteRLR	0x0519
#define 	CRAM_ADDR_REL_TIME_OutMuteRLR	0x051A

// Component: OutLimiterRL
#define 	CRAM_ADDR_ATT_TIME_OutLimiterRL	0x051C
#define 	CRAM_ADDR_REL_TIME_OutLimiterRL	0x051D
#define 	CRAM_ADDR_THRESHOLD_OutLimiterRL	0x0520

// Component: OutLimiterFR
#define 	CRAM_ADDR_ATT_TIME_OutLimiterFR	0x0524
#define 	CRAM_ADDR_REL_TIME_OutLimiterFR	0x0525
#define 	CRAM_ADDR_THRESHOLD_OutLimiterFR	0x0528

// Component: OutLimiterRR
#define 	CRAM_ADDR_ATT_TIME_OutLimiterRR	0x052C
#define 	CRAM_ADDR_REL_TIME_OutLimiterRR	0x052D
#define 	CRAM_ADDR_THRESHOLD_OutLimiterRR	0x0530

// Component: Out5SW4
#define 	CRAM_ADDR_IN1_Out5SW4	0x0533
#define 	CRAM_ADDR_IN2_Out5SW4	0x0534
#define 	CRAM_ADDR_IN3_Out5SW4	0x0535
#define 	CRAM_ADDR_IN4_Out5SW4	0x0536

// Component: SpeAnaMix
#define 	CRAM_ADDR_VOL_IN1_SpeAnaMix	0x0537
#define 	CRAM_ADDR_VOL_IN2_SpeAnaMix	0x0538

// Component: SpeAnaMedia
#define 	CRAM_ADDR_SpeAnaMedia	0x0539
#define 	ORAM_ADDR_SpeAnaMedia	0x0018
#define 	DRAM0_ADDR_SpeAnaMedia	0x0543
#define 	DRAM1_ADDR_SpeAnaMedia	0x0000
#define 	CRAM_ADDR_FREQ_SINE_SpeAnaMedia	0x0539
#define 	CRAM_ADDR_GAIN_LCH_SINE_SpeAnaMedia	0x053B
#define 	CRAM_ADDR_GAIN_RCH_SINE_SpeAnaMedia	0x053C

// Component: SpeAnaRamMon
#define 	CRAM_ADDR_CDMONITOR_SpeAnaRamMon	0x05B1

#endif /* AK7739_AUDIO_EFFECT_ADDR_H_ */

