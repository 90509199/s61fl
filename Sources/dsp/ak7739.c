/*
 * ak7739.c  --  audio driver for ak7739
 *
 */

/*
 * Version:1.3
 *
 * V1.3
 * date:20200929
 * note:
 * 1. Update for AKM_AudioEffect_Basic_UseCase_V01_3_0
 * 2. add ak7739_readDSP2SpectrumAnalyzer
 * 3. fix ram read to read cram pram during run
 * 4. add MIR simulation code
 * Todo:
 * 1. Need to check the DSP2 Pram splited download function
 *
 * V1.2
 * date:20200804
 * note:
 * 1. Update for AKM_AudioEffect_Basic_UseCase_V01_2_4
 * 2. Function Name Fix, SW2, SW4, GEQ
 * 3. ADC Input setting as pseudo differential
 * 4. Use SRC1, 2 for 24kHz surround input output
 * Todo:
 * 1. Need to check the DSP2 Pram splited download function
 * 2. Need to make a splited Pram of DSP1, DSP3
 *
 * V1.1
 * date:20200731
 * note:
 * 1. Update for AKM_AudioEffect_Basic_UseCase_V01_2_1
 * 2. Add DSPOUT5,6 source selector for EXT AMP use case
 * 3. Implement the Split Pram, Cram API
 * 4. Implement the set_ae_1bandGeq for GEQ setting
 * Todo:
 * 1. Need to change the node name for GEQ
 * 2. Need to check the DSP2 Pram splited download function
 * 3. Need to make a splited Pram of DSP1, DSP3
 *
 * V1.0
 * date:20200708
 * note:
 * 1. Update for AKM_Automotive_AudioEffect_K_Basic_V1_2
 * 2. Fix the BMT parameter to 7 ~ -7
 *
 * V0.6
 * date:20200626
 * note:
 * 1. Add Beep control and option function
 * 2. Add Outmix function
 * 3. Fix test_read_ram function for debug
 *
 * V0.5
 * date:20200610
 * note:
 * 1. Add BassMidTreble, Loudness for Audio Effect User Interface
 * 2. Change the Cram write size
 * todo: Add OutMix and Personal GEQ interface for SoundMode
 *
 * V0.4
 * date:20200609
 * note:
 * 1. Add Volume, Mute, Switch, Fade Balance Function for Audio Effect User Interface
 * 2. Change the Cram data type as CONST type
 * todo: Fix BassMidTreble and Loudness function for Audio Effect User Interface
 *
 * V0.3
 * date:20200603
 * note:
 * 1. Add Ram read write simulation function  
 * 2. Add Audio Effect Pram, Cram data
 * 3. Add Function for Audio Effect User Interface
 * todo: Add more Function for Audio Effect User Interface
 *
 * V0.2
 * date:20200410
 * note:
 * 1. fix major bug as 20200409_Major_fix
 *    Add delay for the Power-up sequence, delay after PDN L->H, delay before Firmware write
 * 2. fix minor bug as 20200409_Minor_fix
 *
 * V0.1
 * date:20200402
 * note:
 * 1. 20200402 Add path setting for customer use case
 *
 * V0
 * Linux Driver to RTOS Convert Note:
 * bool -> enum BOOLTYPE, u8 -> unsigned char, u16 -> unsigned short, u32 -> unsigned int, u64 -> unsigned long
 *
 */


#include "includes.h"
#include "Cpu.h"

#include "A2B_I2C_Commandlist.h"
#define	ARRAY_SIZE(x)	( sizeof((x))/sizeof((x)[0]) )

/* ak7739 default register settings */
static const struct reg_default ak7739_reg[] = {
		/* AUDIOHUB,PERI block */
	  { 0x0000, 0x00 },  /* AK7739_C0_000_SYSCLOCK_SETTING1                */
	  { 0x0001, 0x00 },  /* AK7739_C0_001_SYSCLOCK_SETTING2                */
	  { 0x0002, 0x00 },  /* AK7739_C0_002_SYSCLOCK_SETTING3                */
	  { 0x0003, 0x00 },  /* AK7739_C0_003_SYSCLOCK_SETTING4                */
	  { 0x0004, 0x00 },  /* AK7739_C0_004_SYSCLOCK_SETTING5                */
	  { 0x0008, 0x00 },  /* AK7739_C0_008_AHCLK_SETTING5                   */
	  { 0x0009, 0x00 },  /* AK7739_C0_009_BUSCLOCK_SETTING                 */
	  { 0x000A, 0x00 },  /* AK7739_C0_00A_BUSCLOCK_SETTING2                */
	  { 0x000B, 0x00 },  /* AK7739_C0_00B_CLKO_SETTING1                    */
	  { 0x000C, 0x00 },  /* AK7739_C0_00C_CLKO_SETTING2                    */
	  { 0x000D, 0x02 },  /* AK7739_C0_00D_Masterspi_SCLK_SETTING1          */
	  { 0x000E, 0x01 },  /* AK7739_C0_00E_Masterspi_SCLK_SETTING2          */
	  { 0x0010, 0x00 },  /* AK7739_C0_010_SYNCDOMAIN1_SETTING1             */
	  { 0x0011, 0x00 },  /* AK7739_C0_011_SYNCDOMAIN1_SETTING2             */
	  { 0x0012, 0x00 },  /* AK7739_C0_012_SYNCDOMAIN1_SETTING3             */
	  { 0x0013, 0x00 },  /* AK7739_C0_013_SYNCDOMAIN1_SETTING4             */
	  { 0x0014, 0x00 },  /* AK7739_C0_014_SYNCDOMAIN2_SETTING1             */
	  { 0x0015, 0x00 },  /* AK7739_C0_015_SYNCDOMAIN2_SETTING2             */
	  { 0x0016, 0x00 },  /* AK7739_C0_016_SYNCDOMAIN2_SETTING3             */
	  { 0x0017, 0x00 },  /* AK7739_C0_017_SYNCDOMAIN2_SETTING4             */
	  { 0x0018, 0x00 },  /* AK7739_C0_018_SYNCDOMAIN3_SETTING1             */
	  { 0x0019, 0x00 },  /* AK7739_C0_019_SYNCDOMAIN3_SETTING2             */
	  { 0x001A, 0x00 },  /* AK7739_C0_01A_SYNCDOMAIN3_SETTING3             */
	  { 0x001B, 0x00 },  /* AK7739_C0_01B_SYNCDOMAIN3_SETTING4             */
	  { 0x001C, 0x00 },  /* AK7739_C0_01C_SYNCDOMAIN4_SETTING1             */
	  { 0x001D, 0x00 },  /* AK7739_C0_01D_SYNCDOMAIN4_SETTING2             */
	  { 0x001E, 0x00 },  /* AK7739_C0_01E_SYNCDOMAIN4_SETTING3             */
	  { 0x001F, 0x00 },  /* AK7739_C0_01F_SYNCDOMAIN4_SETTING4             */
	  { 0x0020, 0x00 },  /* AK7739_C0_020_SYNCDOMAIN5_SETTING1             */
	  { 0x0021, 0x00 },  /* AK7739_C0_021_SYNCDOMAIN5_SETTING2             */
	  { 0x0022, 0x00 },  /* AK7739_C0_022_SYNCDOMAIN5_SETTING3             */
	  { 0x0023, 0x00 },  /* AK7739_C0_023_SYNCDOMAIN5_SETTING4             */
	  { 0x0025, 0x00 },  /* AK7739_C0_025_SYNCDOMAIN6_SETTING2             */
	  { 0x0026, 0x00 },  /* AK7739_C0_026_SYNCDOMAIN6_SETTING3             */
	  { 0x0027, 0x00 },  /* AK7739_C0_027_SYNCDOMAIN6_SETTING4             */
	  { 0x0029, 0x00 },  /* AK7739_C0_029_SYNCDOMAIN7_SETTING2             */
	  { 0x002A, 0x00 },  /* AK7739_C0_02A_SYNCDOMAIN7_SETTING3             */
	  { 0x002B, 0x00 },  /* AK7739_C0_02B_SYNCDOMAIN7_SETTING4             */
	  { 0x0040, 0x00 },  /* AK7739_C0_040_BICK_FORMAT_SETTING1             */
	  { 0x0041, 0x00 },  /* AK7739_C0_041_BICK_SYNCDOMAIN_SELECT1          */
	  { 0x0042, 0x00 },  /* AK7739_C0_042_BICK_FORMAT_SETTING2             */
	  { 0x0043, 0x00 },  /* AK7739_C0_043_BICK_SYNCDOMAIN_SELECT2          */
	  { 0x0044, 0x00 },  /* AK7739_C0_044_BICK_FORMAT_SETTING3             */
	  { 0x0045, 0x00 },  /* AK7739_C0_045_BICK_SYNCDOMAIN_SELECT3          */
	  { 0x0046, 0x00 },  /* AK7739_C0_046_BICK_FORMAT_SETTING4             */
	  { 0x0047, 0x00 },  /* AK7739_C0_047_BICK_SYNCDOMAIN_SELECT4          */
	  { 0x0048, 0x00 },  /* AK7739_C0_048_BICK_FORMAT_SETTING5             */
	  { 0x0049, 0x00 },  /* AK7739_C0_049_BICK_SYNCDOMAIN_SELECT5          */
	  { 0x0050, 0x00 },  /* AK7739_C0_050_SDIN1_INPUT_FORMAT               */
	  { 0x0051, 0x00 },  /* AK7739_C0_051_SDIN1_SYNCDOMAIN_SELECT          */
	  { 0x0052, 0x00 },  /* AK7739_C0_052_SDIN2_INPUT_FORMAT               */
	  { 0x0053, 0x00 },  /* AK7739_C0_053_SDIN2_SYNCDOMAIN_SELECT          */
	  { 0x0054, 0x00 },  /* AK7739_C0_054_SDIN3_INPUT_FORMAT               */
	  { 0x0055, 0x00 },  /* AK7739_C0_055_SDIN3_SYNCDOMAIN_SELECT          */
	  { 0x0056, 0x00 },  /* AK7739_C0_056_SDIN4_INPUT_FORMAT               */
	  { 0x0057, 0x00 },  /* AK7739_C0_057_SDIN4_SYNCDOMAIN_SELECT          */
	  { 0x0058, 0x00 },  /* AK7739_C0_058_SDIN5_INPUT_FORMAT               */
	  { 0x0059, 0x00 },  /* AK7739_C0_059_SDIN5_SYNCDOMAIN_SELECT          */
	  { 0x005A, 0x00 },  /* AK7739_C0_05A_SDIN6_INPUT_FORMAT               */
	  { 0x005B, 0x00 },  /* AK7739_C0_05B_SDIN6_SYNCDOMAIN_SELECT          */
	  { 0x0060, 0x00 },  /* AK7739_C0_060_SDOUT1_OUTPUT_FORMAT             */
	  { 0x0061, 0x00 },  /* AK7739_C0_061_SDOUT1_SYNCDOMAIN_SELECT         */
	  { 0x0062, 0x00 },  /* AK7739_C0_062_SDOUT2_OUTPUT_FORMAT             */
	  { 0x0063, 0x00 },  /* AK7739_C0_063_SDOUT2_SYNCDOMAIN_SELECT         */
	  { 0x0064, 0x00 },  /* AK7739_C0_064_SDOUT3_OUTPUT_FORMAT             */
	  { 0x0065, 0x00 },  /* AK7739_C0_065_SDOUT3_SYNCDOMAIN_SELECT         */
	  { 0x0066, 0x00 },  /* AK7739_C0_066_SDOUT4_OUTPUT_FORMAT             */
	  { 0x0067, 0x00 },  /* AK7739_C0_067_SDOUT4_SYNCDOMAIN_SELECT         */
	  { 0x0068, 0x00 },  /* AK7739_C0_068_SDOUT5_OUTPUT_FORMAT             */
	  { 0x0069, 0x00 },  /* AK7739_C0_069_SDOUT5_SYNCDOMAIN_SELECT         */
	  { 0x006A, 0x00 },  /* AK7739_C0_06A_SDOUT6_OUTPUT_FORMAT             */
	  { 0x006B, 0x00 },  /* AK7739_C0_06B_SDOUT6_SYNCDOMAIN_SELECT         */
	  { 0x0087, 0x00 },  /* AK7739_C0_087_OSMEM_SYNCDOMAIN_SELECT          */
	  { 0x0088, 0x00 },  /* AK7739_C0_088_ADC1_SYNCDOMAIN_SELECT           */
	  { 0x0089, 0x00 },  /* AK7739_C0_089_CODEC_SYNCDOMAIN_SELECT          */
	  { 0x0098, 0x00 },  /* AK7739_C0_098_MIXER_CH1_INPUT_SELECT           */
	  { 0x0099, 0x00 },  /* AK7739_C0_099_MIXER_CH1_INPUT_SELECT           */
	  { 0x00A0, 0x00 },  /* AK7739_C0_0A0_SRC1_SYNCDOMAIN_SELECT           */
	  { 0x00A1, 0x00 },  /* AK7739_C0_0A1_SRC2_SYNCDOMAIN_SELECT           */
	  { 0x00A2, 0x00 },  /* AK7739_C0_0A2_SRC3_SYNCDOMAIN_SELECT           */
	  { 0x00A3, 0x00 },  /* AK7739_C0_0A3_SRC4_SYNCDOMAIN_SELECT           */
	  { 0x00A4, 0x00 },  /* AK7739_C0_0A4_SRC5_SYNCDOMAIN_SELECT           */
	  { 0x00A5, 0x00 },  /* AK7739_C0_0A5_SRC6_SYNCDOMAIN_SELECT           */
	  { 0x00A6, 0x00 },  /* AK7739_C0_0A6_SRC7_SYNCDOMAIN_SELECT           */
	  { 0x00A7, 0x00 },  /* AK7739_C0_0A7_SRC8_SYNCDOMAIN_SELECT           */
	  { 0x00B0, 0x00 },  /* AK7739_C0_0B0_DSP1_SYNCDOMAIN_SELECT           */
	  { 0x00B1, 0x00 },  /* AK7739_C0_0B1_DSP2_SYNCDOMAIN_SELECT           */
	  { 0x00C0, 0x00 },  /* AK7739_C0_0C0_DSP1_OUTPUT_SYNCDOMAIN_SELECT1   */
	  { 0x00C1, 0x00 },  /* AK7739_C0_0C1_DSP1_OUTPUT_SYNCDOMAIN_SELECT2   */
	  { 0x00C2, 0x00 },  /* AK7739_C0_0C2_DSP1_OUTPUT_SYNCDOMAIN_SELECT3   */
	  { 0x00C3, 0x00 },  /* AK7739_C0_0C3_DSP1_OUTPUT_SYNCDOMAIN_SELECT4   */
	  { 0x00C4, 0x00 },  /* AK7739_C0_0C4_DSP1_OUTPUT_SYNCDOMAIN_SELECT5   */
	  { 0x00C5, 0x00 },  /* AK7739_C0_0C5_DSP1_OUTPUT_SYNCDOMAIN_SELECT6   */
	  { 0x00C6, 0x00 },  /* AK7739_C0_0C6_DSP2_OUTPUT_SYNCDOMAIN_SELECT1   */
	  { 0x00C7, 0x00 },  /* AK7739_C0_0C7_DSP2_OUTPUT_SYNCDOMAIN_SELECT2   */
	  { 0x00C8, 0x00 },  /* AK7739_C0_0C8_DSP2_OUTPUT_SYNCDOMAIN_SELECT3   */
	  { 0x00C9, 0x00 },  /* AK7739_C0_0C9_DSP2_OUTPUT_SYNCDOMAIN_SELECT4   */
	  { 0x00CA, 0x00 },  /* AK7739_C0_0CA_DSP2_OUTPUT_SYNCDOMAIN_SELECT5   */
	  { 0x00CB, 0x00 },  /* AK7739_C0_0CB_DSP2_OUTPUT_SYNCDOMAIN_SELECT6   */
	  { 0x0106, 0x00 },  /* AK7739_C0_106_DIT_INPUT_DATA_SELECT            */
	  { 0x0108, 0x00 },  /* AK7739_C0_108_DAC1_INPUT_DATA_SELECT           */
	  { 0x0109, 0x00 },  /* AK7739_C0_109_DAC2_INPUT_DATA_SELECT           */
	  { 0x0118, 0x00 },  /* AK7739_C0_118_MIXER1_CHA_INPUT_DATA_SELECT     */
	  { 0x0119, 0x00 },  /* AK7739_C0_119_MIXER1_CHB_INPUT_DATA_SELECT     */
	  { 0x011A, 0x00 },  /* AK7739_C0_11A_MIXER2_CHA_INPUT_DATA_SELECT     */
	  { 0x011B, 0x00 },  /* AK7739_C0_11B_MIXER2_CHB_INPUT_DATA_SELECT     */
	  { 0x0120, 0x00 },  /* AK7739_C0_120_SRC1_INPUT_DATA_SELECT           */
	  { 0x0121, 0x00 },  /* AK7739_C0_121_SRC2_INPUT_DATA_SELECT           */
	  { 0x0122, 0x00 },  /* AK7739_C0_122_SRC3_INPUT_DATA_SELECT           */
	  { 0x0123, 0x00 },  /* AK7739_C0_123_SRC4_INPUT_DATA_SELECT           */
	  { 0x0124, 0x00 },  /* AK7739_C0_124_SRC5_INPUT_DATA_SELECT           */
	  { 0x0125, 0x00 },  /* AK7739_C0_125_SRC6_INPUT_DATA_SELECT           */
	  { 0x0126, 0x00 },  /* AK7739_C0_126_SRC7_INPUT_DATA_SELECT           */
	  { 0x0127, 0x00 },  /* AK7739_C0_127_SRC8_INPUT_DATA_SELECT           */
	  { 0x0140, 0x00 },  /* AK7739_C0_140_SDOUT1A_OUTPUT_DATA_SELECT       */
	  { 0x0141, 0x00 },  /* AK7739_C0_141_SDOUT1B_OUTPUT_DATA_SELECT       */
	  { 0x0142, 0x00 },  /* AK7739_C0_142_SDOUT1C_OUTPUT_DATA_SELECT       */
	  { 0x0143, 0x00 },  /* AK7739_C0_143_SDOUT1D_OUTPUT_DATA_SELECT       */
	  { 0x0144, 0x00 },  /* AK7739_C0_144_SDOUT1E_OUTPUT_DATA_SELECT       */
	  { 0x0145, 0x00 },  /* AK7739_C0_145_SDOUT1F_OUTPUT_DATA_SELECT       */
	  { 0x0146, 0x00 },  /* AK7739_C0_146_SDOUT1G_OUTPUT_DATA_SELECT       */
	  { 0x0147, 0x00 },  /* AK7739_C0_147_SDOUT1H_OUTPUT_DATA_SELECT       */
	  { 0x0148, 0x00 },  /* AK7739_C0_148_SDOUT2A_OUTPUT_DATA_SELECT       */
	  { 0x0149, 0x00 },  /* AK7739_C0_149_SDOUT2B_OUTPUT_DATA_SELECT       */
	  { 0x014A, 0x00 },  /* AK7739_C0_14A_SDOUT2C_OUTPUT_DATA_SELECT       */
	  { 0x014B, 0x00 },  /* AK7739_C0_14B_SDOUT2D_OUTPUT_DATA_SELECT       */
	  { 0x014C, 0x00 },  /* AK7739_C0_14C_SDOUT2E_OUTPUT_DATA_SELECT       */
	  { 0x014D, 0x00 },  /* AK7739_C0_14D_SDOUT2F_OUTPUT_DATA_SELECT       */
	  { 0x014E, 0x00 },  /* AK7739_C0_14E_SDOUT2G_OUTPUT_DATA_SELECT       */
	  { 0x014F, 0x00 },  /* AK7739_C0_14F_SDOUT2H_OUTPUT_DATA_SELECT       */
	  { 0x0150, 0x00 },  /* AK7739_C0_150_SDOUT3A_OUTPUT_DATA_SELECT       */
	  { 0x0151, 0x00 },  /* AK7739_C0_151_SDOUT3B_OUTPUT_DATA_SELECT       */
	  { 0x0152, 0x00 },  /* AK7739_C0_152_SDOUT3C_OUTPUT_DATA_SELECT       */
	  { 0x0153, 0x00 },  /* AK7739_C0_153_SDOUT3D_OUTPUT_DATA_SELECT       */
	  { 0x0154, 0x00 },  /* AK7739_C0_154_SDOUT3E_OUTPUT_DATA_SELECT       */
	  { 0x0155, 0x00 },  /* AK7739_C0_155_SDOUT3F_OUTPUT_DATA_SELECT       */
	  { 0x0156, 0x00 },  /* AK7739_C0_156_SDOUT3G_OUTPUT_DATA_SELECT       */
	  { 0x0157, 0x00 },  /* AK7739_C0_157_SDOUT3H_OUTPUT_DATA_SELECT       */
	  { 0x0158, 0x00 },  /* AK7739_C0_158_SDOUT4A_OUTPUT_DATA_SELECT       */
	  { 0x0159, 0x00 },  /* AK7739_C0_159_SDOUT4B_OUTPUT_DATA_SELECT       */
	  { 0x015A, 0x00 },  /* AK7739_C0_15A_SDOUT4C_OUTPUT_DATA_SELECT       */
	  { 0x015B, 0x00 },  /* AK7739_C0_15B_SDOUT4D_OUTPUT_DATA_SELECT       */
	  { 0x015C, 0x00 },  /* AK7739_C0_15C_SDOUT4E_OUTPUT_DATA_SELECT       */
	  { 0x015D, 0x00 },  /* AK7739_C0_15D_SDOUT4F_OUTPUT_DATA_SELECT       */
	  { 0x015E, 0x00 },  /* AK7739_C0_15E_SDOUT4G_OUTPUT_DATA_SELECT       */
	  { 0x015F, 0x00 },  /* AK7739_C0_15F_SDOUT4H_OUTPUT_DATA_SELECT       */
	  { 0x0160, 0x00 },  /* AK7739_C0_160_SDOUT5A_OUTPUT_DATA_SELECT       */
	  { 0x0161, 0x00 },  /* AK7739_C0_161_SDOUT5B_OUTPUT_DATA_SELECT       */
	  { 0x0162, 0x00 },  /* AK7739_C0_162_SDOUT5C_OUTPUT_DATA_SELECT       */
	  { 0x0163, 0x00 },  /* AK7739_C0_163_SDOUT5D_OUTPUT_DATA_SELECT       */
	  { 0x0164, 0x00 },  /* AK7739_C0_164_SDOUT5E_OUTPUT_DATA_SELECT       */
	  { 0x0165, 0x00 },  /* AK7739_C0_165_SDOUT5F_OUTPUT_DATA_SELECT       */
	  { 0x0166, 0x00 },  /* AK7739_C0_166_SDOUT5G_OUTPUT_DATA_SELECT       */
	  { 0x0167, 0x00 },  /* AK7739_C0_167_SDOUT5H_OUTPUT_DATA_SELECT       */
	  { 0x0168, 0x00 },  /* AK7739_C0_168_SDOUT6A_OUTPUT_DATA_SELECT       */
	  { 0x0169, 0x00 },  /* AK7739_C0_169_SDOUT6B_OUTPUT_DATA_SELECT       */
	  { 0x016A, 0x00 },  /* AK7739_C0_16A_SDOUT6C_OUTPUT_DATA_SELECT       */
	  { 0x016B, 0x00 },  /* AK7739_C0_16B_SDOUT6D_OUTPUT_DATA_SELECT       */
	  { 0x016C, 0x00 },  /* AK7739_C0_16C_SDOUT6E_OUTPUT_DATA_SELECT       */
	  { 0x016D, 0x00 },  /* AK7739_C0_16D_SDOUT6F_OUTPUT_DATA_SELECT       */
	  { 0x016E, 0x00 },  /* AK7739_C0_16E_SDOUT6G_OUTPUT_DATA_SELECT       */
	  { 0x016F, 0x00 },  /* AK7739_C0_16F_SDOUT6H_OUTPUT_DATA_SELECT       */
	  { 0x0180, 0x00 },  /* AK7739_C0_180_DSP1_DIN1_INPUT_DATA_SELECT      */
	  { 0x0181, 0x00 },  /* AK7739_C0_181_DSP1_DIN2_INPUT_DATA_SELECT      */
	  { 0x0182, 0x00 },  /* AK7739_C0_182_DSP1_DIN3_INPUT_DATA_SELECT      */
	  { 0x0183, 0x00 },  /* AK7739_C0_183_DSP1_DIN4_INPUT_DATA_SELECT      */
	  { 0x0184, 0x00 },  /* AK7739_C0_184_DSP1_DIN5_INPUT_DATA_SELECT      */
	  { 0x0185, 0x00 },  /* AK7739_C0_185_DSP1_DIN6_INPUT_DATA_SELECT      */
	  { 0x0186, 0x00 },  /* AK7739_C0_186_DSP2_DIN1_INPUT_DATA_SELECT      */
	  { 0x0187, 0x00 },  /* AK7739_C0_187_DSP2_DIN2_INPUT_DATA_SELECT      */
	  { 0x0188, 0x00 },  /* AK7739_C0_188_DSP2_DIN3_INPUT_DATA_SELECT      */
	  { 0x0189, 0x00 },  /* AK7739_C0_189_DSP2_DIN4_INPUT_DATA_SELECT      */
	  { 0x018A, 0x00 },  /* AK7739_C0_18A_DSP2_DIN5_INPUT_DATA_SELECT      */
	  { 0x018B, 0x00 },  /* AK7739_C0_18B_DSP2_DIN6_INPUT_DATA_SELECT      */
	  { 0x0200, 0x00 },  /* AK7739_C0_200_SDOUT_OUTPUT_DATA_SELECT         */
	  { 0x0201, 0x00 },  /* AK7739_C0_201_SDOUT_ENABLE_SETTING             */
	  { 0x0202, 0x00 },  /* AK7739_C0_202_SDOUT_OUTPUT_MODE_SETTING        */
	  { 0x0203, 0x00 },  /* AK7739_C0_203_SDIN_INPUT_DATA_SELECT           */
	  { 0x0204, 0x00 },  /* AK7739_C0_204_MASTER_spi_SELECT                */
	  { 0x0205, 0x00 },  /* AK7739_C0_205_STO_FLAG_SETTING                 */
	  { 0x0206, 0x00 },  /* AK7739_C0_206_LRCK4_5_OUTPUT_DATA_SELECT       */
	  { 0x0210, 0x00 },  /* AK7739_C0_210_MASTER_spi_TX00                  */
	  { 0x0211, 0x00 },  /* AK7739_C0_211_MASTER_spi_TX01                  */
	  { 0x0212, 0x00 },  /* AK7739_C0_212_MASTER_spi_TX02                  */
	  { 0x0213, 0x00 },  /* AK7739_C0_213_MASTER_spi_TX03                  */
	  { 0x0214, 0x00 },  /* AK7739_C0_214_MASTER_spi_TX04                  */
	  { 0x0215, 0x00 },  /* AK7739_C0_215_MASTER_spi_TX05                  */
	  { 0x0216, 0x00 },  /* AK7739_C0_216_MASTER_spi_TX06                  */
	  { 0x0217, 0x00 },  /* AK7739_C0_217_MASTER_spi_TX07                  */
	  { 0x0218, 0x00 },  /* AK7739_C0_218_MASTER_spi_TX08                  */
	  { 0x0219, 0x00 },  /* AK7739_C0_219_MASTER_spi_TX09                  */
	  { 0x021A, 0x00 },  /* AK7739_C0_21A_MASTER_spi_TX10                  */
	  { 0x021B, 0x00 },  /* AK7739_C0_21B_MASTER_spi_TX11                  */
	  { 0x021C, 0x00 },  /* AK7739_C0_21C_MASTER_spi_TX12                  */
	  { 0x021D, 0x00 },  /* AK7739_C0_21D_MASTER_spi_TX13                  */
	  { 0x021E, 0x00 },  /* AK7739_C0_21E_MASTER_spi_TX14                  */
	  { 0x021F, 0x00 },  /* AK7739_C0_21F_MASTER_spi_TX15                  */
	  { 0x0220, 0x00 },  /* AK7739_C0_220_MASTER_spi_TX16                  */
	  { 0x0221, 0x00 },  /* AK7739_C0_221_MASTER_spi_TX17                  */
	  { 0x0222, 0x00 },  /* AK7739_C0_222_MASTER_spi_TX18                  */
	  { 0x0223, 0x00 },  /* AK7739_C0_223_MASTER_spi_TX19                  */
	  { 0x0224, 0x00 },  /* AK7739_C0_224_MASTER_spi_TX20                  */
	  { 0x0225, 0x00 },  /* AK7739_C0_225_MASTER_spi_TX21                  */
	  { 0x0226, 0x00 },  /* AK7739_C0_226_MASTER_spi_TX22                  */
	  { 0x0227, 0x00 },  /* AK7739_C0_227_MASTER_spi_TX23                  */
	  { 0x0228, 0x00 },  /* AK7739_C0_228_MASTER_spi_TX24                  */
	  { 0x0229, 0x00 },  /* AK7739_C0_229_MASTER_spi_TX25                  */
	  { 0x022A, 0x00 },  /* AK7739_C0_22A_MASTER_spi_TX26                  */
	  { 0x022B, 0x00 },  /* AK7739_C0_22B_MASTER_spi_TX27                  */
	  { 0x022C, 0x00 },  /* AK7739_C0_22C_MASTER_spi_TX28                  */
	  { 0x022D, 0x00 },  /* AK7739_C0_22D_MASTER_spi_TX29                  */
	  { 0x022E, 0x00 },  /* AK7739_C0_22E_MASTER_spi_TX30                  */
	  { 0x022F, 0x00 },  /* AK7739_C0_22F_MASTER_spi_TX31                  */
	  { 0x0300, 0x39 },	 /* AK7739_C0_300_DEVICE_ID                        */
	  { 0x0301, 0x00 },	 /* AK7739_C0_301_REVISION_NUM                     */
	  { 0x0302, 0x00 },	 /* AK7739_C0_302_CRC_ERROR_STATUS                 */
	  { 0x0303, 0x01 },	 /* AK7739_C0_303_STO_READ_OUT                     */
	  { 0x030F, 0x00 },	 /* AK7739_C0_30F_MASTER_spi_STATUS                */
	  { 0x0310, 0x00 },	 /* AK7739_C0_310_MASTER_spi_RX00                  */
	  { 0x0311, 0x00 },	 /* AK7739_C0_311_MASTER_spi_RX01                  */
	  { 0x0312, 0x00 },	 /* AK7739_C0_312_MASTER_spi_RX02                  */
	  { 0x0313, 0x00 },  /* AK7739_C0_313_MASTER_spi_RX03                  */
	  { 0x0314, 0x00 },  /* AK7739_C0_314_MASTER_spi_RX04                  */
	  { 0x0315, 0x00 },  /* AK7739_C0_315_MASTER_spi_RX05                  */
	  { 0x0316, 0x00 },  /* AK7739_C0_316_MASTER_spi_RX06                  */
	  { 0x0317, 0x00 },  /* AK7739_C0_317_MASTER_spi_RX07                  */
	  { 0x0318, 0x00 },  /* AK7739_C0_318_MASTER_spi_RX08                  */
	  { 0x0319, 0x00 },  /* AK7739_C0_319_MASTER_spi_RX09                  */
	  { 0x031A, 0x00 },  /* AK7739_C0_31A_MASTER_spi_RX10                  */
	  { 0x031B, 0x00 },  /* AK7739_C0_31B_MASTER_spi_RX11                  */
	  { 0x031C, 0x00 },  /* AK7739_C0_31C_MASTER_spi_RX12                  */
	  { 0x031D, 0x00 },  /* AK7739_C0_31D_MASTER_spi_RX13                  */
	  { 0x031E, 0x00 },  /* AK7739_C0_31E_MASTER_spi_RX14                  */
	  { 0x031F, 0x00 },  /* AK7739_C0_31F_MASTER_spi_RX15                  */
	  { 0x0320, 0x00 },	 /* AK7739_C0_320_MASTER_spi_RX16                  */
	  { 0x0321, 0x00 },  /* AK7739_C0_321_MASTER_spi_RX17                  */
	  { 0x0322, 0x00 },	 /* AK7739_C0_322_MASTER_spi_RX18                  */
	  { 0x0323, 0x00 },  /* AK7739_C0_323_MASTER_spi_RX19                  */
	  { 0x0324, 0x00 },  /* AK7739_C0_324_MASTER_spi_RX20                  */
	  { 0x0325, 0x00 },  /* AK7739_C0_325_MASTER_spi_RX21                  */
	  { 0x0326, 0x00 },  /* AK7739_C0_326_MASTER_spi_RX22                  */
	  { 0x0327, 0x00 },  /* AK7739_C0_327_MASTER_spi_RX23                  */
	  { 0x0328, 0x00 },  /* AK7739_C0_328_MASTER_spi_RX24                  */
	  { 0x0329, 0x00 },  /* AK7739_C0_329_MASTER_spi_RX25                  */
	  { 0x032A, 0x00 },  /* AK7739_C0_32A_MASTER_spi_RX26                  */
	  { 0x032B, 0x00 },  /* AK7739_C0_32B_MASTER_spi_RX27                  */
	  { 0x032C, 0x00 },  /* AK7739_C0_32C_MASTER_spi_RX28                  */
	  { 0x032D, 0x00 },  /* AK7739_C0_32D_MASTER_spi_RX29                  */
	  { 0x032E, 0x00 },  /* AK7739_C0_32E_MASTER_spi_RX30                  */
	  { 0x032F, 0x00 },  /* AK7739_C0_32F_MASTER_spi_RX31                  */

	  /* DSP block */
	  { 0x1000, 0x00 },  /* AK7739_C1_000_DSP_RESET_CONTROL                */
	  { 0x1001, 0x00 },  /* AK7739_C1_001_DSP_CLOCK_SETTING                */
	  { 0x1002, 0x00 },  /* AK7739_C1_002_RAM_CLEAR_SETTING                */
	  { 0x1003, 0x00 },  /* AK7739_C1_003_DSP_WATCHDOG_TIMER_FLAG_SETTING  */
	  { 0x1004, 0x00 },  /* AK7739_C1_004_DSP_GPO_SETTING                  */
	  { 0x1005, 0x00 },  /* AK7739_C1_005_DSP1,2,3_GPO_STATUS              */
	  { 0x1008, 0x07 },  /* AK7739_C1_008_DSP_WATCHDOG_TIMER_ERROR_STATUS  */
	  { 0x1011, 0x00 },  /* AK7739_C1_011_DSP1_DRAM_SETTING                */
	  { 0x1012, 0x00 },  /* AK7739_C1_012_DSP2_DRAM_SETTING                */
	  { 0x1013, 0x00 },  /* AK7739_C1_013_DSP3_DRAM_SETTING                */
	  { 0x1020, 0x00 },  /* AK7739_C1_020_DSP1,2_DLYRAM_ASSIGNMENT         */
	  { 0x1021, 0x00 },  /* AK7739_C1_021_DSP1_DLYRAM_SETTING              */
	  { 0x1022, 0x00 },  /* AK7739_C1_022_DSP2_DLYRAM_SETTING              */
	  { 0x1031, 0x00 },  /* AK7739_C1_031_DSP1_CRAM&DLP0_SETTING           */
	  { 0x1032, 0x00 },  /* AK7739_C1_032_DSP2_CRAM&DLP0_SETTING           */
	  { 0x1033, 0x00 },  /* AK7739_C1_033_DSP3_CRAM_SETTING                */
	  { 0x1040, 0x00 },  /* AK7739_C1_040_DSP1_JX_SETTING                  */
	  { 0x1041, 0x00 },  /* AK7739_C1_041_DSP2_JX_SETTING                  */
	  { 0x1042, 0x00 },  /* AK7739_C1_042_DSP3_JX_SETTING                  */

	  /* SRC block */
	  { 0x2000, 0x00 },  /* AK7739_C2_000_SRC_POWER_MANAGEMENT             */
	  { 0x2001, 0x00 },  /* AK7739_C2_001_SRC_FILTER_SETTING1              */
	  { 0x2003, 0x00 },  /* AK7739_C2_003_SRC_PHASE_GROUP1                 */
	  { 0x2005, 0x00 },  /* AK7739_C2_005_SRC_MUTE_SETTING1                */
	  { 0x2006, 0x00 },  /* AK7739_C2_006_SRC_MUTE_SETTING2                */
	  { 0x2007, 0x00 },  /* AK7739_C2_007_SRC_STO_FLAG_SETTING             */
	  { 0x2010, 0x00 },  /* AK7739_C2_010_SRC_STATUS1                      */
	  { 0x2101, 0x00 },  /* AK7739_C2_101_MONO_SRC_POWER_MANAGEMENT        */
	  { 0x2102, 0x00 },  /* AK7739_C2_102_MONO_SRC_FILTER_SETTING          */
	  { 0x2103, 0x00 },  /* AK7739_C2_103_MONO_SRC_PHASE_GROUP             */
	  { 0x2104, 0x00 },  /* AK7739_C2_104_MONO_SRC_MUTE_SETTING            */
	  { 0x2105, 0x00 },  /* AK7739_C2_105_MONO_SRC_STO_FLAG_SETTING        */
	  { 0x2106, 0x00 },  /* AK7739_C2_106_MONO_SRC_PATH_SETTING            */
	  { 0x2110, 0x00 },  /* AK7739_C2_110_MONO_SRC_STATUS1                 */
	  { 0x2200, 0x00 },  /* AK7739_C2_200_DIT_POWER_MANAGEMENT             */
	  { 0x2201, 0x00 },  /* AK7739_C2_201_DIT_STATUS_BIT1                  */
	  { 0x2202, 0x04 },  /* AK7739_C2_202_DIT_STATUS_BIT2                  */
	  { 0x2203, 0x02 },  /* AK7739_C2_203_DIT_STATUS_BIT3                  */
	  { 0x2204, 0x00 },  /* AK7739_C2_204_DIT_STATUS_BIT4                  */
	  { 0x2210, 0x00 },  /* AK7739_C2_210_MIXER1_SETTING                   */
	  { 0x2211, 0x00 },  /* AK7739_C2_211_MIXER2_SETTING                   */

	  /* CODEC block */
	  { 0x3000, 0x00 },  /* AK7739_C3_000_POWER_MANAGEMENT                 */
	  { 0x3001, 0x00 },  /* AK7739_C3_001_MICBIAS_POWER_MANAGEMENT         */
	  { 0x3002, 0x00 },  /* AK7739_C3_002_RESET_CONTROL                    */
	  { 0x3003, 0x00 },  /* AK7739_C3_003_SYSTEM_CLOCK_SETTING             */
	  { 0x3004, 0x11 },  /* AK7739_C3_004_MIC_AMP_GAIN                     */
	  { 0x3005, 0x30 },  /* AK7739_C3_005_ADC1_LCH_DIGITAL_VOLUME          */
	  { 0x3006, 0x30 },  /* AK7739_C3_006_ADC1_RCH_DIGITAL_VOLUME          */
	  { 0x3007, 0x30 },  /* AK7739_C3_007_ADC2_LCH_DIGITAL_VOLUME          */
	  { 0x3008, 0x30 },  /* AK7739_C3_008_ADC2_RCH_DIGITAL_VOLUME          */
	  { 0x3009, 0x04 },  /* AK7739_C3_009_ADC_DIGITAL_FILTER_SETTING       */
	  { 0x300A, 0x00 },  /* AK7739_C3_00A_ADC_ANALOG_INPUT_SETTING         */
	  { 0x300B, 0x00 },  /* AK7739_C3_00B_ADC_MUTE&HPF_CONTROL             */
	  { 0x300C, 0x18 },  /* AK7739_C3_00C_DAC1_LCH_DIGITAL_VOLUME          */
	  { 0x300D, 0x18 },  /* AK7739_C3_00D_DAC1_RCH_DIGITAL_VOLUME          */
	  { 0x300E, 0x18 },  /* AK7739_C3_00E_DAC2_LCH_DIGITAL_VOLUME          */
	  { 0x300F, 0x18 },  /* AK7739_C3_00F_DAC2_RCH_DIGITAL_VOLUME          */
	  { 0x3013, 0x15 },  /* AK7739_C3_013_DAC_De-EMPHASIS_SETTING          */
	  { 0x3014, 0x02 },  /* AK7739_C3_014_DAC_MUTE&FILTER_SETTING          */
	  { 0x3015, 0x00 },  /* AK7739_C3_015_DIGITAL_MIC_CONTROL              */
};

/*
 * Functions for SPI read and write
 *
 */
#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
static unsigned char ak7739_hubreg_test_buffer[0x32F] = {0x00};
static unsigned char ak7739_dspreg_test_buffer[0x42] = {0x00};
static unsigned char ak7739_srcreg_test_buffer[0x211] = {0x00};
static unsigned char ak7739_codecreg_test_buffer[0x15] = {0x00};

static unsigned char ak7739_pram1_test_buffer[TOTAL_NUM_OF_PRAM1_MAX] = {0x00};
static unsigned char ak7739_cram1_test_buffer[TOTAL_NUM_OF_CRAM1_MAX] = {0x00};
static unsigned char ak7739_offram1_test_buffer[TOTAL_NUM_OF_OFREG1_MAX] = {0x00};

static unsigned char ak7739_pram2_test_buffer[TOTAL_NUM_OF_PRAM2_MAX] = {0x00};
static unsigned char ak7739_cram2_test_buffer[TOTAL_NUM_OF_CRAM2_MAX] = {0x00};
static unsigned char ak7739_offram2_test_buffer[TOTAL_NUM_OF_OFREG2_MAX] = {0x00};

static unsigned char ak7739_pram3_test_buffer[TOTAL_NUM_OF_PRAM3_MAX] = {0x00};
static unsigned char ak7739_cram3_test_buffer[TOTAL_NUM_OF_CRAM3_MAX] = {0x00};

static unsigned char ak7739_MIR_test_buffer[TOTAL_NUM_OF_MIR_MAX] = {
		0x00, 0x00, 0x0B, 0x1E, 0x18,
		0x00, 0x03, 0x13, 0x41, 0xB0,
		0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x03, 0xFF, 0xFF, 0xF8,

		0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00,
};
#endif

#if 0
#define AK_INIT_REG_0 0
#define AK_INIT_REG_1 1
#define AK_INIT_REG_2 2
#define AK_INIT_REG_3 3
#define AK_INIT_REG_4 4
#define AK_INIT_REG_5 5
#define AK_INIT_REG_6 6
#define AK_INIT_REG_7 7
#define AK_INIT_REG_8 8
#define AK_INIT_REG_9 9
#define AK_INIT_REG_10 10
#define AK_INIT_REG_11 11
#define AK_INIT_REG_12 12
#define AK_INIT_REG_13 13
#define AK_INIT_REG_14 14
#define AK_INIT_REG_15 15
#define AK_INIT_REG_16 16
#define AK_INIT_REG_17 17
#define AK_INIT_REG_18 18
#define AK_INIT_REG_19 19
#define AK_INIT_REG_20 20
#define AK_INIT_REG_21 21
#define AK_INIT_REG_22 22
#define AK_INIT_REG_23 23
#define AK_INIT_REG_24 24
#define AK_INIT_REG_25 25
#define AK_INIT_REG_26 26
#define AK_INIT_REG_27 27
#define AK_INIT_REG_28 28
#define AK_INIT_REG_29 29
#define AK_INIT_REG_30 30
#else
enum
{
	AK_INIT_REG_0 = 0,
	AK_INIT_REG_1,
	AK_INIT_REG_2,
	AK_INIT_REG_3,
	AK_INIT_REG_4,
	AK_INIT_REG_5,
	AK_INIT_REG_6,
	AK_INIT_REG_7,
	AK_INIT_REG_8,
	AK_INIT_REG_9,
	AK_INIT_REG_10,
	AK_INIT_REG_11,
	AK_INIT_REG_12,
	AK_INIT_REG_13,
	AK_INIT_REG_14,
	AK_INIT_REG_15,
	AK_INIT_REG_16,
	AK_INIT_REG_17,
	AK_INIT_REG_18,
	AK_INIT_REG_19,
	AK_INIT_REG_20,
	AK_INIT_REG_21,
	AK_INIT_REG_22,
	AK_INIT_REG_23,
	AK_INIT_REG_24,	
	AK_INIT_REG_25,
	AK_INIT_REG_26,
	AK_INIT_REG_27,
	AK_INIT_REG_28,
	AK_INIT_REG_29,
	AK_INIT_REG_30,
	AK_INIT_REG_31,
	AK_INIT_REG_32,
};

#endif



static volatile uint32 dsp_timer_tick1 = 0;

#ifdef RECOVER_DSP_RAM_WRITE_FAIL
extern uint16 uDspRamWriteFailCnt;
#endif


/*
 * Functions for SOC H/W control
 *
 */
static unsigned char gpio_set_value(unsigned int gpio, int value)
{
    int ret=0;

#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
    akdbgprt("[SOC] %s gpio[%d] value[%d]\n", __FUNCTION__, gpio, value);
    ret = 0;
#else
	DSP_PDN(value);
#endif

    return ret;
}

static void mdelay(int ms)
{
#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
    akdbgprt("[SOC] %s set mdelay ms[%d]\n", __FUNCTION__, ms);
#else
	delay_ms(ms);
#endif
}

static void msleep(int ms)
{
#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
    akdbgprt("[SOC] %s set msleep ms[%d]\n", __FUNCTION__, ms);
#else
	delay_ms(ms);	
#endif
}

/* SPI data reads*/
int spi_reads(
		const unsigned char *tx, int txlen,
		unsigned char *rx, int rxlen)
{
	int ret;	

#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
	unsigned int reg;
	int cmd, cmd2, dspNo, i, n;

	//reg = ((0x0000 | tx[1]) << 8) | tx[2]; //20200409_Minor_fix to set proper reg address from tx buffer
	reg  = ((0xFF00 & ((int)tx[1])  << 8)) | (0xFF & (int)tx[2]);

	cmd = (0xFC & tx[0]);
	cmd2 = tx[0];

   if(txlen == 3) {
    	if(tx[0] == COMMAND_READ_HUBREG)
    		rx[0] = ak7739_hubreg_test_buffer[reg];
    	else if(tx[0] == COMMAND_READ_DSPREG)
    		rx[0] = ak7739_dspreg_test_buffer[reg];
    	else if(tx[0] == COMMAND_READ_SRCREG)
    		rx[0] = ak7739_srcreg_test_buffer[reg];
    	else if(tx[0] == COMMAND_READ_CODECREG)
    		rx[0] = ak7739_codecreg_test_buffer[reg];
    	else if(tx[0] == COMMAND_READ_CODECREG) {
    		rx[0] = ak7739_codecreg_test_buffer[reg];
    	}
    	else if(cmd == COMMAND_READ_PRAM) {
    		dspNo = cmd2 - COMMAND_READ_PRAM;
			if(dspNo > 2) {
				akdbgprt_err("\t[SOC] %s PRAM read fail dspNo[%d]\n", __FUNCTION__, dspNo+1);
				return AK7739_ERROR_EIO;
			}

			n = reg * AK7739_PRAM_WORD_SIZE;
			for ( i = 0 ; i < rxlen; i ++ ) {
				if(dspNo == 0) {
					if(n >= TOTAL_NUM_OF_PRAM1_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_pram1_test_buffer[n];
				}
				else if(dspNo == 1) {
					if(n >= TOTAL_NUM_OF_PRAM2_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_pram2_test_buffer[n];
				}
				else if(dspNo == 2) {
					if(n >= TOTAL_NUM_OF_PRAM3_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_pram3_test_buffer[n];
				}
				n++;
			}
    	}
    	else if(cmd == COMMAND_READ_CRAM) {
    		dspNo = cmd2 - COMMAND_READ_CRAM;
			if(dspNo > 2) {
				akdbgprt_err("\t[SOC] %s CRAM read fail dspNo[%d]\n", __FUNCTION__, dspNo+1);
				return AK7739_ERROR_EIO;
			}

			n = reg * AK7739_CRAM_ORAM_WORD_SIZE;
			for ( i = 0 ; i < rxlen; i ++ ) {
				if(dspNo == 0) {
					if(n >= TOTAL_NUM_OF_CRAM1_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_cram1_test_buffer[n];
				}
				else if(dspNo == 1) {
					if(n >= TOTAL_NUM_OF_CRAM2_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_cram2_test_buffer[n];
				}
				else if(dspNo == 2) {
					if(n >= TOTAL_NUM_OF_CRAM3_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_cram3_test_buffer[n];
				}
				n++;
			}
    	}
    	else if( ( cmd2 == COMMAND_READ_OFREG ) || ( cmd2 == (COMMAND_READ_OFREG + 1) ) ) {
    		dspNo = cmd2 - COMMAND_READ_OFREG;
			if(dspNo > 1) {
				akdbgprt_err("\t[SOC] %s OFFRAM read fail dspNo[%d]\n", __FUNCTION__, dspNo+1);
				return AK7739_ERROR_EIO;
			}

			n = reg * AK7739_CRAM_ORAM_WORD_SIZE;
			for ( i = 0 ; i < rxlen; i ++ ) {
				if(dspNo == 0) {
					if(n >= TOTAL_NUM_OF_OFREG1_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_offram1_test_buffer[n];
				}
				else if(dspNo == 1) {
					if(n >= TOTAL_NUM_OF_OFREG2_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					rx[i] = ak7739_offram2_test_buffer[n];
				}
				n++;
			}
    	}
    	else if(cmd2 == COMMAND_MIR_READ+1) {
			/*FIX_ME: need to implement for DSP1,2,3*/
			n = reg * AK7739_MIR_WORD_SIZE;

			for ( i = 0 ; i < rxlen; i ++ ) {
				if(n >= TOTAL_NUM_OF_MIR_MAX) {
					akdbgprt_err("\t[SOC] %s Max size over\n", __FUNCTION__);
					return AK7739_ERROR_EIO;
				}
				rx[i] = ak7739_MIR_test_buffer[n];
				akdbgprt("[AK7739] %s rx(%d)[0x%02X]\n", __FUNCTION__, i, rx[i]);
				n++;
			}
    	}
	else {
    		akdbgprt("[SOC] %s ***** cmd[0x%02X] reg[0x%04X], not supported with test code \n", __FUNCTION__, tx[0], reg);
    	    return AK7739_ERROR_EIO;
    	}
#ifdef AK7739_DEBUG_REG_READ_WRITE
    	akdbgprt("[SOC] %s reg[0x%02X][0x%04X] rdata[0x%02X]\n", __FUNCTION__, tx[0]&0x0F, reg, rx[0]);
#endif
    	ret = rxlen;
    }
    else {
        akdbgprt_err("\t[AK7739] %s txlen=%d not supported with test code\n", __FUNCTION__, txlen);
        ret = AK7739_ERROR_EIO;
    }
#else
#if 1
	uint8 result=0;
	uint8 rx_data[2048]={0,};			//	1024->	2048
	uint8 echoBack = 3; //Dummy:Command:Dummy 3byte
	unsigned int max_rxlen=0,i;

	max_rxlen = txlen+rxlen;

	if(max_rxlen < echoBack+rxlen)
		max_rxlen = echoBack+rxlen;

	if(max_rxlen > sizeof(rx_data)-echoBack)
		max_rxlen = sizeof(rx_data) - echoBack;

#ifdef AK7739_DEBUG_REG_READ_WRITE
	akdbgprt("[SOC] %s SPI Read tx[0x%02X] tx[0x%02X] tx[0x%02X] txlen[%d] max_rxlen[%d]\n",
		__FUNCTION__, tx[0], tx[1], tx[2], txlen, max_rxlen);
#endif
	result = LPSPI_DRV_MasterTransferBlocking(LPSPICOM1,&tx[0],&rx_data[0],max_rxlen,1000);
#ifdef AK7739_DEBUG_REG_READ_WRITE
	akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
		__FUNCTION__, rx_data[0], rx_data[1], rx_data[2], rx_data[3], rx_data[4]);
	if(tx[0] == 0x77) {
		//For MIR Read
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[5], rx_data[6], rx_data[7], rx_data[8], rx_data[9]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[10], rx_data[11], rx_data[12], rx_data[13], rx_data[14]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[15], rx_data[16], rx_data[17], rx_data[18], rx_data[19]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[20], rx_data[21], rx_data[22], rx_data[23], rx_data[24]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[25], rx_data[26], rx_data[27], rx_data[28], rx_data[29]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[30], rx_data[31], rx_data[32], rx_data[33], rx_data[34]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[35], rx_data[36], rx_data[37], rx_data[38], rx_data[39]);
		akdbgprt("[SOC] %s SPI Read rx[0x%02X] rx[0x%02X] rx[0x%02X]\n",
			__FUNCTION__, rx_data[40], rx_data[41], rx_data[42]);
	}
#endif
	
	if(result != 0) {
		DPRINTF(DBG_ERR,"%s error \n\r",__FUNCTION__);
		ret = AK7739_ERROR_EIO;
    }
	else {
		for(i=0;i<rxlen;i++) {
			rx[i] =	rx_data[echoBack+i];
			
			#ifdef AK7739_DEBUG_REG_READ_WRITE
			if(i < 3)
				akdbgprt("[SOC] %s SPI Read rx[0x%02X] i[%d]\n", __FUNCTION__, rx[i], i);
			#endif
		}
		
    	ret = max_rxlen;
	}
#else
	uint8 rx_data[2048]={0,};			//	1024->	2048
	int i;

    if(txlen == 3) {
		LPSPI_DRV_MasterTransferBlocking(LPSPICOM1,&tx[0],&rx_data[0],txlen+rxlen,1000);
		for(i=0;i<rxlen;i++)
			rx[i] =	rx_data[txlen+i];

    	ret = rxlen;
    }
    else {
		LPSPI_DRV_MasterTransferBlocking(LPSPICOM1,&tx[0],&rx_data[0],txlen+rxlen,1000);
		for(i=0;i<rxlen;i++)
			rx[i] =	rx_data[txlen+i];

    	ret = rxlen;
    }
#endif
#endif

    return ret;
}

/* SPI data writes*/
static int spi_writes(
		const unsigned char *tx, int txlen)
{
    int ret;

#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
	unsigned int reg;
    int cmd, cmd2, dspNo, i, n;

    if(txlen == 4) {
	//reg = ((0x0000 | tx[1]) << 8) | tx[2]; //20200409_Minor_fix to set proper reg address from tx buffer
	reg  = ((0xFF00 & ((int)tx[1])  << 8)) | (0xFF & (int)tx[2]);

    	if(tx[0]==0xDE && tx[1]==0xAD && tx[2]==0xDA && tx[3]==0x7A) {
			akdbgprt("[SOC] %s tx[0x%02X] tx[0x%02X] tx[0x%02X] tx[0x%02X]\n", __FUNCTION__, tx[0], tx[1], tx[2], tx[3]);
			akdbgprt("[SOC] %s ***** SPI Enable CMD received\n", __FUNCTION__);		
            return txlen;
    	}
    	else if(tx[0] == COMMAND_WRITE_HUBREG)
    		ak7739_hubreg_test_buffer[reg] = tx[3];
    	else if(tx[0] == COMMAND_WRITE_DSPREG)
    		ak7739_dspreg_test_buffer[reg] = tx[3];
    	else if(tx[0] == COMMAND_WRITE_SRCREG)
    		ak7739_srcreg_test_buffer[reg] = tx[3];
    	else if(tx[0] == COMMAND_WRITE_CODECREG)
    		ak7739_codecreg_test_buffer[reg] = tx[3];
    	else {
    		akdbgprt_err("\t[SOC] %s REG write fail cmd[0x%02X] reg[0x%04X] value[0x%02X]\n", __FUNCTION__, tx[0], reg, tx[3]);
    	    return AK7739_ERROR_EIO;
    	}
#ifdef AK7739_DEBUG_REG_READ_WRITE
    	akdbgprt("[SOC] %s reg[0x%02X][0x%04X] value[0x%02X]\n", __FUNCTION__, tx[0]&0x0F, reg, tx[3]);
#endif
    	ret = txlen;
    }
    else {
		cmd = (0xFC & tx[0]);
		cmd2 = tx[0];
		reg  = ((0xFF00 & ((int)tx[1])  << 8)) | (0xFF & (int)tx[2]);

    	//Pram, Cram write
    	akdbgprt("[SOC] %s ***** parm cram write cmd[0x%02X] add[0x%02X%02X] firstdata[0x%02X] txlen[%d]\n", __FUNCTION__, tx[0], tx[1], tx[2], tx[3], txlen);

		if ( cmd == COMMAND_WRITE_PRAM ) {
			dspNo = cmd2 - COMMAND_WRITE_PRAM;
			if(dspNo > 2) {
				akdbgprt_err("\t[SOC] %s PRAM write fail dspNo[%d]\n", __FUNCTION__, dspNo+1);
				return AK7739_ERROR_EIO;
			}

			//akdbgprt_dump("\t[SOC] Pram Write dspNo%d\n", dspNo+1);
			//akdbgprt_dump("\t0x%02X, 0x%02X, 0x%02X,\n\n", tx[0], tx[1], tx[2]);
			n = reg * AK7739_PRAM_WORD_SIZE;
			for ( i = 3 ; i < txlen; i ++ ) {
				if(dspNo == 0) {
					if(n >= TOTAL_NUM_OF_PRAM1_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_pram1_test_buffer[n] = tx[i];
				}
				else if(dspNo == 1) {
					if(n >= TOTAL_NUM_OF_PRAM2_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_pram2_test_buffer[n] = tx[i];
				}
				else if(dspNo == 2) {
					if(n >= TOTAL_NUM_OF_PRAM3_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_pram3_test_buffer[n] = tx[i];
				}
				n++;
			}
		}
		else if ( cmd == COMMAND_WRITE_CRAM ) {
			dspNo = cmd2 - COMMAND_WRITE_CRAM;
			if(dspNo > 2) {
				akdbgprt_err("\t[SOC] %s CRAM write fail dspNo[%d]\n", __FUNCTION__, dspNo+1);
				return AK7739_ERROR_EIO;
			}

			//akdbgprt_dump("\t[SOC] Cram Write dspNo%d\n", dspNo+1);
			//akdbgprt_dump("\t0x%02X, 0x%02X, 0x%02X,\n\n", tx[0], tx[1], tx[2]);
			n = reg * AK7739_CRAM_ORAM_WORD_SIZE;
			for ( i = 3 ; i < txlen; i ++ ) {
				if(dspNo == 0) {
					if(n >= TOTAL_NUM_OF_CRAM1_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_cram1_test_buffer[n] = tx[i];
				}
				else if(dspNo == 1) {
					if(n >= TOTAL_NUM_OF_CRAM2_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_cram2_test_buffer[n] = tx[i];
				}
				else if(dspNo == 2) {
					if(n >= TOTAL_NUM_OF_CRAM3_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_cram3_test_buffer[n] = tx[i];
				}
				n++;
			}
		}
		else if ( ( cmd2 == COMMAND_WRITE_OFREG ) || ( cmd2 == (COMMAND_WRITE_OFREG + 1) ) ) {
			dspNo = cmd2 - COMMAND_WRITE_OFREG;

			if(dspNo > 1) {
				akdbgprt_err("\t[SOC] %s OFFRAM write fail dspNo[%d]\n", __FUNCTION__, dspNo+1);
				return AK7739_ERROR_EIO;
			}

			//akdbgprt_dump("\t[SOC] Offram Write dspNo%d\n", dspNo+1);
			//akdbgprt_dump("\t0x%02X, 0x%02X, 0x%02X,\n\n", tx[0], tx[1], tx[2]);
			n = reg * AK7739_CRAM_ORAM_WORD_SIZE;
			for ( i = 3 ; i < txlen; i ++ ) {
				if(dspNo == 0) {
					if(n >= TOTAL_NUM_OF_OFREG1_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_offram1_test_buffer[n] = tx[i];
				}
				else if(dspNo == 1) {
					if(n >= TOTAL_NUM_OF_OFREG2_MAX) {
						akdbgprt_err("\t[SOC] %s dspNo[%d] Max size over\n", __FUNCTION__, dspNo+1);
						return AK7739_ERROR_EIO;
					}
					ak7739_offram2_test_buffer[n] = tx[i];
				}
				n++;
			}
		}
		else {
			akdbgprt_err("\t[SOC] %s RAM write fail cmd[0x%02X] tx1n2[0x%02X%02X]\n", __FUNCTION__, tx[0], tx[1], tx[2]);
			return AK7739_ERROR_EIO;
		}

    	ret = txlen;
    }
#else
	// User implementation
	uint8 result=0;

#ifdef AK7739_DEBUG_REG_READ_WRITE
	akdbgprt("[SOC] %s SPI Write tx[0x%02X] tx[0x%02X] tx[0x%02X] tx[0x%02X] txlen[%d]\n", __FUNCTION__, tx[0], tx[1], tx[2], tx[3], txlen);
#endif

	result = LPSPI_DRV_MasterTransferBlocking(LPSPICOM1,&tx[0],NULL,txlen,1000);
	if(result != 0) {
		DPRINTF(DBG_ERR,"%s error \n\r",__FUNCTION__);
		ret = AK7739_ERROR_EIO;
    }
	else
    	ret = txlen;
#endif

    return ret;
}

/*
 * Functions for register read and write
 *
 */

static BOOLTYPE ak7739_readable(unsigned int reg)
{
	BOOLTYPE ret;
	int  n, ne;
	int  tabnum;

	tabnum = sizeof(ak7739_reg) / sizeof(ak7739_reg[0]);

	ne = 0x0;
	do {
		n = ne;
		ne += 0x10;
		if ( ne >= tabnum  ) {
			ne = tabnum;
			break;
		}
	} while (reg >= ak7739_reg[ne].reg);

	ret = AK7739_FALSE;

	do {
		if ( ak7739_reg[n].reg == reg ) {
			ret = AK7739_TRUE;
			break;
		}
		n++;

	} while ( n < ne );


	return(ret);

}

static BOOLTYPE ak7739_writeable(unsigned int reg)
{
	BOOLTYPE ret;
	int  n, ne;
	int  tabnum;

	tabnum = sizeof(ak7739_reg) / sizeof(ak7739_reg[0]);

	if ( ( 0xFF00 & reg ) == 0x300 ) {
		return(AK7739_FALSE);
	}

	ne = 0x0;
	do {
		n = ne;
		ne += 0x10;
		if ( ne >= tabnum  ) {
			ne = tabnum;
			break;
		}
	} while (reg >= ak7739_reg[ne].reg);


	ret = AK7739_FALSE;

	do {
		if ( ak7739_reg[n].reg == reg ) {
			ret = AK7739_TRUE;
			break;
		}
		n++;

	} while ( n < ne );


	return(ret);

}

unsigned int ak7739_reg_read(ak7739_priv *pData, unsigned int reg)
{
	unsigned char tx[3]={0,};
	unsigned char rx[1]={0,};
	int	wlen, rlen;
	int ret;
	unsigned int rdata;

	if (!ak7739_readable(reg)) {
#ifdef AK7739_DEBUG_REG_READ_WRITE
		akdbgprt("\tReg read failed value is not readable reg: 0x%04X\n", reg);
#endif
		return 0;
	}

	wlen = 3;
	rlen = 1;

	if ( ( 0xF000 & reg ) == 0 ) {
		tx[0] = (unsigned char)COMMAND_READ_HUBREG;
	}
	else if ( ( 0xF000 & reg ) == 0x1000 ) {
		tx[0] = (unsigned char)COMMAND_READ_DSPREG;
	}
	else if ( ( 0xF000 & reg ) == 0x2000 ) {
		tx[0] = (unsigned char)COMMAND_READ_SRCREG;
	}
	else {
		tx[0] = (unsigned char)COMMAND_READ_CODECREG;
	}
	tx[1] = (unsigned char)(0x0F & (reg >> 8));
	tx[2] = (unsigned char)(0xFF & reg);

#ifdef AK7739_DEBUG_REG_READ_WRITE
	akdbgprt("[AK7739] %s >> reg(0x%04X) tx[0x%02X] tx[0x%02X] tx[0x%02X]\n", __FUNCTION__, reg, tx[0], tx[1], tx[2]);
#endif
	ret = spi_reads(tx, wlen, rx, rlen);

	if (ret < 0) {
		akdbgprt_err("\t[AK7739] %s error ret = %d\n", __FUNCTION__, ret);
	}
	else {
		rdata = (unsigned int)rx[0];
#ifdef AK7739_DEBUG_REG_READ_WRITE
			akdbgprt("[AK7739] %s << reg(0x%04X) rdata(0x%02X)\n", __FUNCTION__, reg, rdata);
#endif
	}

	if (rdata > AK7738_REG_VALUE_MAX) {
		akdbgprt_err("\tReg read failed value is too big: 0x%02X\n", rdata);
		rdata = AK7739_ERROR_EIO;
	}

	return rdata;
}

static int ak7739_reg_write(ak7739_priv *pData,
		unsigned int reg,
		unsigned int value)
{
	unsigned char tx[4];
	int wlen;
	int ret;

	if (value > AK7738_REG_VALUE_MAX) {
		akdbgprt_err("\tReg write failed value is too big: 0x%02X\n", value);
		return(AK7739_ERROR_EINVAL);
	}

	if (!ak7739_writeable(reg)) {
		return(AK7739_ERROR_EINVAL);
	}

	if ( reg > AK7739_C3_015_DIGITAL_MIC_CONTROL ) {
		akdbgprt_err("\tReg is out of range, write to 0x%04X failed\n",reg);
		return(0);
	}

	wlen = 4;
	if ( ( 0xF000 & reg ) == 0 ) {
		tx[0] = (unsigned char)COMMAND_WRITE_HUBREG;
	}
	else if ( ( 0xF000 & reg ) == 0x1000 ) {
		tx[0] = (unsigned char)COMMAND_WRITE_DSPREG;
	}
	else if ( ( 0xF000 & reg ) == 0x2000 ) {
		tx[0] = (unsigned char)COMMAND_WRITE_SRCREG;
	}
	else {
		tx[0] = (unsigned char)COMMAND_WRITE_CODECREG;
	}

	tx[1] = (unsigned char)(0x0F & (reg >> 8));
	tx[2] = (unsigned char)(0xFF & reg);
	tx[3] = value;

#ifdef AK7739_DEBUG_REG_READ_WRITE
	akdbgprt("[AK7739] %s >> reg(0x%04X) value(0x%02X)\n", __FUNCTION__, reg, tx[3]);
#endif
	ret = spi_writes(tx, wlen);

#ifdef AK7739_DEBUG_REG_READ_WRITE
	if(value != ak7739_reg_read(pData, reg))
		akdbgprt_err("[AK7739] %s reg write fail!!! reg(0x%02X%02X) value(0x%02X)\n", __FUNCTION__, tx[1], tx[2], tx[3]);
#endif

	if (ret != wlen)
		akdbgprt_err("\tReg write to 0x%X failed: %d\n", reg, ret);

	return ret;

}

static int ak7739_reg_update_bits(
		ak7739_priv *pData,
		unsigned short reg,
		unsigned int mask,
		unsigned int value)
{
    unsigned char change;
    unsigned int old, new;
    int ret;
    ak7739_priv *ak7739 = pData;

    old = ak7739_reg_read(ak7739, reg);

    new = ((old & ~mask) & 0xFF) | (value & mask);
    //change = old != new; //20200409_Minor_fix for proper old and new value check
    change = (old != new);

    if (change) {
#ifdef AK7739_DEBUG_REG_READ_WRITE
    	akdbgprt("[AK7739] %s <<==>> reg(0x%X), mask(0x%02X), value(0x%02X)\n", __FUNCTION__, reg, mask, value);
#endif
        ret = ak7739_reg_write(ak7739, reg, new);
    }

    return ret;
}

static int ak7739_reads(ak7739_priv *pData,
		unsigned char *tx,
		size_t wlen,
		unsigned char *rx,
		size_t rlen)
{
	int ret;

	akdbgprt_debug("[AK7739] %s tx[0]=0x%02X, raddr=0x%02x%02X, wlen=%d, rlen=%d\n", __FUNCTION__, tx[0],tx[1],tx[2],(int)wlen,(int)rlen);
	ret = spi_reads(tx, wlen, rx, rlen);

	return ret;

}

static int ak7739_writes(ak7739_priv *pData,
		const unsigned char *tx,
		size_t wlen)
{
	int rc;

	akdbgprt("[AK7739] %s tx[0]=%02x tx[1]=%02x tx[2]=%02X, tx[3]=%02X, wlen=%d\n", __FUNCTION__, (int)tx[0], (int)tx[1], (int)tx[2], (int)tx[3], (int)wlen);
	rc = spi_writes(tx, wlen);

	if(rc != wlen) {
		akdbgprt_err("\t%s: comm error, rc %d, wlen %d\n", __FUNCTION__, rc, (int)wlen);
		DPRINTF(DBG_MSG1,"\t%s: comm error, rc %d, wlen %d\n", __FUNCTION__, rc, (int)wlen);
	}

	return rc;
}

static int crc_read(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int rc;

	unsigned char	tx[3], rx[2];

	tx[0] = COMMAND_CRC_READ;
	tx[1] = 0;
	tx[2] = 0;

	rc =  ak7739_reads(ak7739, tx, 3, rx, 2);

	return (rc < 0) ? rc : ((rx[0] << 8) + rx[1]);
}

static int ak7739_ClockReset(ak7739_priv *pData, PMEVENTTYPE event)
{
	ak7739_priv *ak7739 = pData;

	akdbgprt("[AK7739] %s(%d)\n", __FUNCTION__,__LINE__);

	switch (event) {
		case AK7739_PRE_PMU:
			akdbgprt("[AK7739] %s AK7739_PRE_PMU\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C0_000_SYSCLOCK_SETTING1, 0x02, 0x02);  // CKRESETN bit = 1
			mdelay(10);
			ak7739_reg_update_bits(ak7739, AK7739_C0_000_SYSCLOCK_SETTING1, 0x01, 0x01);  // HRESETN bit = 1
			break;

		case AK7739_POST_PMD:
			akdbgprt("[AK7739] %s AK7739_POST_PMD\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C0_000_SYSCLOCK_SETTING1, 0x03, 0x00);   // HRESETN bit = 0
			break;
	}
	return 0;
}

static int ak7758_DSP1PowerUp(ak7739_priv *pData, PMEVENTTYPE event)
{
	ak7739_priv *ak7739 = pData;

	switch (event) {
		case AK7739_PRE_PMU:
			akdbgprt("[AK7739] %s AK7739_PRE_PMU\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x01, 0x01);
			if ( ak7739->selDSP3 == 1 ) {
				ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x04, 0x04);
			}
			break;
		case AK7739_POST_PMD:
			akdbgprt("[AK7739] %s AK7739_POST_PMD\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x01, 0x00);
			if ( ak7739->selDSP3 == 1 ) {
				ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x04, 0x00);
			}
			break;
	}
	return 0;
}


static int ak7758_DSP2PowerUp(ak7739_priv *pData, PMEVENTTYPE event)
{
	ak7739_priv *ak7739 = pData;

	switch (event) {
		case AK7739_PRE_PMU:
			akdbgprt("[AK7739] %s AK7739_PRE_PMU\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x02, 0x02);
			if ( ak7739->selDSP3 == 2 ) {
				ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x04, 0x04);
			}
			break;
		case AK7739_POST_PMD:
			akdbgprt("[AK7739] %s AK7739_POST_PMD\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x02, 0x00);
			if ( ak7739->selDSP3 == 2 ) {
				ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, 0x04, 0x00);
			}
			break;
	}
	return 0;
}

int ak7739_set_status(ak7739_priv *pData, STATUSTYPE status)
{
	ak7739_priv *ak7739 = pData;
	int ckresetn;

	switch (status) {
		case STATUS_DOWNLOAD:
			akdbgprt("[AK7739] %s >>***** DOWNLOAD\n", __FUNCTION__);
			ckresetn = ak7739_reg_read(ak7739, AK7739_C0_000_SYSCLOCK_SETTING1);
			ckresetn &= 0x02;
			if ( ckresetn == 0 ) {
				ak7739_reg_update_bits(ak7739, AK7739_C0_000_SYSCLOCK_SETTING1, 0x80, 0x80);  // DLRDY = 1
			}
			mdelay(1);
			break;
		case STATUS_DOWNLOAD_FINISH:
			akdbgprt("[AK7739] %s <<***** DOWNLOAD_FINISH\n", __FUNCTION__);
			ak7739_reg_update_bits(ak7739, AK7739_C0_000_SYSCLOCK_SETTING1, 0x80, 0x00);  // DLRDY = 0
			break;
		case STATUS_STANDBY:
			akdbgprt("[AK7739] %s STANDBY\n", __FUNCTION__);
			#if 0
//			set_adc_soft_mute(ak7739, ADC_1, MUTE_ON);
//			set_adc_soft_mute(ak7739, ADC_2, MUTE_ON);
//			set_dac_soft_mute(ak7739, DAC_1, MUTE_ON);
//			set_dac_soft_mute(ak7739, DAC_2, MUTE_ON);
			#endif

			//CLOCK Reset
			ak7739_ClockReset(ak7739, AK7739_PRE_PMU);
			//DSP1, 3 Power on
			ak7758_DSP1PowerUp(ak7739, AK7739_PRE_PMU);
			//DSP2, 3 Power on
			ak7758_DSP2PowerUp(ak7739, AK7739_PRE_PMU);

			//Using Analog Input
			ak7739_reg_update_bits(ak7739, AK7739_C3_001_MICBIAS_POWER_MANAGEMENT, 0x20, 0x20);

			//CODEC Reset Release
			ak7739_reg_update_bits(ak7739, AK7739_C3_002_RESET_CONTROL, 0x01, 0x01);
			//ADC1,2 DAC1,2 Normal Operation
			ak7739_reg_update_bits(ak7739, AK7739_C3_000_POWER_MANAGEMENT, 0x36, 0x36);

            //SDOUT1,2,3,4,5,6 Enable
			ak7739_reg_update_bits(ak7739, AK7739_C0_201_SDOUT_ENABLE_SETTING, 0xFC, 0xFC);
			//SRC1,2,3,4 Normal Operation
			ak7739_reg_update_bits(ak7739, AK7739_C2_000_SRC_POWER_MANAGEMENT, 0xF0, 0xF0);
			//SRC5,6,7,8 Normal Operation
			ak7739_reg_update_bits(ak7739, AK7739_C2_101_MONO_SRC_POWER_MANAGEMENT, 0x0F, 0x0F);
			break;
		case STATUS_RUN:
			akdbgprt("[AK7739] %s RUN\n", __FUNCTION__);
			break;
		case STATUS_SUSPEND:
		case STATUS_POWERDOWN:
			if(status == STATUS_SUSPEND)
				akdbgprt("[AK7739] %s SUSPEND\n", __FUNCTION__);
			else if(status == STATUS_POWERDOWN)
				akdbgprt("[AK7739] %s POWERDOWN\n", __FUNCTION__);

			set_adc_soft_mute(ak7739, ADC_1, MUTE_ON);
			set_adc_soft_mute(ak7739, ADC_2, MUTE_ON);
			set_dac_soft_mute(ak7739, DAC_1, MUTE_ON);
			set_dac_soft_mute(ak7739, DAC_2, MUTE_ON);

			//DSP1, 3 Power off
			ak7758_DSP1PowerUp(ak7739, AK7739_POST_PMD);
			//DSP2, 3 Power off
			ak7758_DSP2PowerUp(ak7739, AK7739_POST_PMD);
			//CLOCK Reset
			ak7739_ClockReset(ak7739, AK7739_POST_PMD);

			//Using Analog Input
			ak7739_reg_update_bits(ak7739, AK7739_C3_001_MICBIAS_POWER_MANAGEMENT, 0x20, 0x00);

			//CODEC Reset
			ak7739_reg_update_bits(ak7739, AK7739_C3_002_RESET_CONTROL, 0x01, 0x00);
			//ADC1,2 DAC1,2 Power Save Mode
			ak7739_reg_update_bits(ak7739, AK7739_C3_000_POWER_MANAGEMENT, 0x36, 0x00);

            //SDOUT1,2,3,4,5,6 Disable
			ak7739_reg_update_bits(ak7739, AK7739_C0_201_SDOUT_ENABLE_SETTING, 0xFC, 0x00);
			//SRC1,2,3,4 Normal Operation
			ak7739_reg_update_bits(ak7739, AK7739_C2_000_SRC_POWER_MANAGEMENT, 0xF0, 0x00);
			//SRC5,6,7,8 Normal Operation
			ak7739_reg_update_bits(ak7739, AK7739_C2_101_MONO_SRC_POWER_MANAGEMENT, 0x0F, 0x00);
			break;
		default:
			akdbgprt_err("\t[AK7739] %s Unknown Status\n", __FUNCTION__);
			return AK7739_ERROR_EINVAL;
	}

	return 0;
}

int ak7739_write_cram(ak7739_priv *pData,
		DSPNUMTYPE dspno,
		int addr,
		int len,
		const unsigned char *cram_data)
{
	ak7739_priv *ak7739 = pData;
	int i, n, ret;
	int nDSPRun;
	unsigned char tx[255];

	akdbgprt("[AK7739] %s dspno=%d addr=0x%02X, len=%d\n", __FUNCTION__, dspno, addr, len);

	if ( len > (255-3) ) {
		akdbgprt("[AK7739] %s Length over!\n", __FUNCTION__);
		return(AK7739_ERROR_EINVAL);
	}


	nDSPRun = ak7739_reg_read(ak7739, AK7739_C1_000_DSP_RESET_CONTROL);

	switch(dspno) {
		case DSPNUMTYPE_DSP1:
			nDSPRun &= 0x1;
			break;
		case DSPNUMTYPE_DSP2:
			nDSPRun &= 0x2;
			break;
		case DSPNUMTYPE_DSP3:
			nDSPRun &= 0x4;
			break;
		default:
			akdbgprt("[AK7739] %s DSP No Error! \n", __FUNCTION__);
			return(AK7739_ERROR_EINVAL);
	}

#ifdef DRIVER_TEST_WITHOUT_PLATFORM_CODE
	nDSPRun = 0;
#endif

	if ( nDSPRun != 0 ) {
		tx[0] = COMMAND_WRITE_CRAM_RUN + (unsigned char)((len / 4) - 1); //AK7739 Cram one word is 4bytes(32bit)
		tx[1] = (unsigned char)(0xFF & (addr >> 8));
		tx[2] = (unsigned char)(0xFF & addr);
	}
	else {
		ak7739_set_status(ak7739, STATUS_DOWNLOAD);
		tx[0] = COMMAND_WRITE_CRAM + dspno;
		tx[1] = (unsigned char)(0xFF & (addr >> 8));
		tx[2] = (unsigned char)(0xFF & addr);
	}

	n = 3;
	for ( i = 0 ; i < len; i ++ ) {
		tx[n++] = cram_data[i];
	}

#ifdef AK7739_DEBUG_CRAM_READ_WRITE
	akdbgprt_info("****** AK7739 CRAM Write ******\n");
	akdbgprt_info("AK7739 CMD : 0x%02X \n", (int)tx[0]);
	akdbgprt_info("AK7739 CAddr : 0x%02X 0x%02X\n", (int)tx[1], (int)tx[2]);
	akdbgprt_info("AK7739 CData : 0x%02X, 0x%02X, 0x%02X, 0x%02X\n", (int)tx[3], (int)tx[4], (int)tx[5], (int)tx[6]);
#endif

	ret = ak7739_writes(ak7739, tx, n);

	if ( nDSPRun != 0 ) {
		tx[0] = COMMAND_WRITE_CRAM_EXEC + dspno;
		tx[1] = 0;
		tx[2] = 0;
		ret = ak7739_writes(ak7739, tx, 3);
		mdelay(1); //need for delay for sparate spi //JJH
	}
	else {
		ak7739_set_status(ak7739, STATUS_DOWNLOAD_FINISH);
	}

#ifdef AK7739_DEBUG_CRAM_READ_WRITE
	test_read_ram(ak7739, dspno, IMGTYPE_CRAM, addr, len);
	#endif

	return ret;
}

static unsigned long ak7739_readMIR(ak7739_priv *pData,
		int dspNo,
		int mirNo,
		unsigned long *dwMIRData)
{
	ak7739_priv *ak7739 = pData;
	unsigned char tx[3];
	unsigned char rx[40];
	int n, nRLen;

	if ((mirNo >= 8) || (dspNo < 1) || (dspNo > 3)) return(AK7739_ERROR_EINVAL);

	tx[0] = (unsigned char)COMMAND_MIR_READ + dspNo - 1;
	tx[1] = 0;
	tx[2] = 0;
	nRLen = 5 * (mirNo + 1);

	ak7739_reads(ak7739, tx, 3, rx, nRLen);

	n = 5 * mirNo;
	*dwMIRData = ((0xFF & (unsigned long)rx[n++]) << 20);
	*dwMIRData += ((0xFF & (unsigned long)rx[n++]) << 12);
    *dwMIRData += ((0xFF & (unsigned long)rx[n++]) << 4);
    *dwMIRData += ((0xFF & (unsigned long)rx[n++]) >> 4);

	return(0);

}

int ak7739_readDSP2SpectrumAnalyzer(ak7739_priv *pData, int *readData, int readNum)
{
	ak7739_priv *ak7739 = pData;
	unsigned char tx[3];
	unsigned char rx[40];
	int n, i, nRLen;
	int ret;
	int dspNo = 2;
	unsigned long dwMIRData;
	unsigned long spectrumLevel;
	int dB_Level;

	if ((readNum >= 9) || (dspNo < 1) || (dspNo > 3)) return(AK7739_ERROR_EINVAL);

	tx[0] = (unsigned char)COMMAND_MIR_READ + dspNo - 1;
	tx[1] = 0;
	tx[2] = 0;
	nRLen = 5 * readNum; //40-bit(5byte) x n

	ret = ak7739_reads(ak7739, tx, 3, rx, nRLen);
	
	for ( i = 0; i < readNum; i++ ) {
				n = 5 * i;
				akdbgprt("AK7739 rx[0x%02X][0x%02X][0x%02X][0x%02X][0x%02X]\n",rx[n], rx[n+1], rx[n+2], rx[n+3], rx[n+4]);
		}

	for ( i = 0; i < readNum; i++ ) {
		n = 5 * i;
		dwMIRData = ((0xFF & (unsigned long)rx[n++]) << 24);
		dwMIRData += ((0xFF & (unsigned long)rx[n++]) << 16);
		dwMIRData += ((0xFF & (unsigned long)rx[n++]) << 8);
	    dwMIRData += ((0xFF & (unsigned long)rx[n++]));
	    dwMIRData += ((0xFF & (unsigned long)rx[n++]) >> 8);
		spectrumLevel = 0xFFFFFFFF & dwMIRData;
		akdbgprt("[AK7739] %s 37bit: 0x%lX 32bit 0x%lX\n", __FUNCTION__, dwMIRData, spectrumLevel);

		if (spectrumLevel <= 0) {
			//akdbgprt("[AK7739] %s spectrumLevel <= 0 \n", __FUNCTION__);
			dB_Level = -180; //(00000000H ~ 7FFFFFFFH)
		}
		else if (spectrumLevel > 0x7FFFFFFF) {
			//akdbgprt("[AK7739] %s spectrumLevel > 0x7FFFFF \n", __FUNCTION__);
			dB_Level = 0;
		}
		else {
			dB_Level = 20 * log10( spectrumLevel / pow(2, 31));
		}

		if (dB_Level < -180)
			dB_Level = -180;

		readData[i] = dB_Level;
	}
	//akdbgprt("AK7739 readData0~7[%d][%d][%d][%d] [%d][%d][%d][%d]dB\n",
	//		readData[0], readData[1], readData[2], readData[3], readData[4], readData[5], readData[6], readData[7]);
	
	return ret;
}


static int ak7739_ram_download(ak7739_priv *pData,
		const unsigned char *tx_ram,
		unsigned long num,
		unsigned short crc)
{
	ak7739_priv *ak7739 = pData;
	int rc;
	unsigned short	read_crc;

	akdbgprt("[AK7739] %s num=%ld\n", __FUNCTION__, (long int)num);

	ak7739_set_status(ak7739, STATUS_DOWNLOAD);

	rc = ak7739_writes(ak7739, tx_ram, num);
	if (rc < 0) {
		ak7739_set_status(ak7739, STATUS_DOWNLOAD_FINISH);
		DPRINTF(DBG_MSG1, "%s: ak7739_writes return %d \r\n", __FUNCTION__, rc);
		return rc;
	}

	if ( ( crc != 0 ) && (rc >= 0) )  {
		read_crc = crc_read(ak7739);
#ifndef DRIVER_TEST_WITHOUT_PLATFORM_CODE
		akdbgprt("[AK7739] %s CRC Cal=0x%X Read=0x%X\n", __FUNCTION__, (int)crc,(int)read_crc);
		if ( read_crc == crc ) rc = 0;
		else
		{
			rc = AK7739_ERROR;
			DPRINTF(DBG_MSG1, "%s: read_crc!=crc %x %x \r\n", __FUNCTION__, read_crc,crc);
		}
#else
		akdbgprt("[AK7739] %s CRC Check Fail Cal=0x%X Read=0x%X, not supported with test code \n", __FUNCTION__, (int)crc,(int)read_crc);
		rc = 0;
#endif
	}

	ak7739_set_status(ak7739, STATUS_DOWNLOAD_FINISH);

	return rc;

}

static int calc_CRC(int length, const unsigned char *data )
{

#define CRC16_CCITT (0x1021)

	unsigned short crc = 0x0000;
	int i, j;

	for ( i = 0; i < length; i++ ) {
		crc ^= *data++ << 8;
		for ( j = 0; j < 8; j++) {
			if ( crc & 0x8000) {
				crc <<= 1;
				crc ^= CRC16_CCITT;
			}
			else {
				crc <<= 1;
			}
		}
	}

	akdbgprt("[AK7739] %s CRC=0x%X\n", __FUNCTION__, crc);

	return crc;
}

static int ak7739_write_ram(ak7739_priv *pData,
		const unsigned char 	*upRam,
		int	 nWSize)
{
	ak7739_priv *ak7739 = pData;
	int n, ret;
	int	wCRC;
	int nMaxSize;
	int devWord;
	int cmd;
	int cmd2;
	int dspNo;

	int returnEnd = FALSE;

	static int devLen;
	static int restLen;
	static int dspResetn;
	static int dspBitMask;
	static const unsigned char *ramdata;

	static uint8 firstEntrance = TRUE;
	static uint8 lastExit      = FALSE;
	static uint32 count = 0;

	cmd = (0xFC & (int)upRam[0]);
	cmd2 = (int)upRam[0];

	if (TRUE == firstEntrance)
	{
		akdbgprt("[AK7739] %s Cmd=0x%X, len=%d\n", __FUNCTION__, (int)upRam[0], nWSize);

		devWord = 0;
		devLen = nWSize;

		dspResetn = ak7739_reg_read(ak7739, AK7739_C1_000_DSP_RESET_CONTROL);

		if ( cmd == COMMAND_WRITE_PRAM ) {
			dspNo = (int)upRam[0] - COMMAND_WRITE_PRAM;
			switch(dspNo) {
				case 0:
					nMaxSize = TOTAL_NUM_OF_PRAM1_MAX;
					break;
				case 1:
					nMaxSize = TOTAL_NUM_OF_PRAM2_MAX;
					break;
				case 2:
					nMaxSize = TOTAL_NUM_OF_PRAM3_MAX;
					break;
				default:
					return(AK7739_ERROR_EINVAL);
			}
			if (  nWSize > nMaxSize ) {
				akdbgprt("%s: PRAM Write size is over! \n", __FUNCTION__);
				return(AK7739_ERROR_EINVAL);
			}
#ifdef AK7739_USE_SPLITED_PRAM // FIX_ME: need to use for all pram, test with DSP2 Pram
			//FIX_ME: old method should be fixed, it should not use add data for piece size
			if ( cmd == COMMAND_WRITE_PRAM ) 
			{
			if(dspNo == 1) {
				devWord = AK7739_DSP_PRAM_PIECE_SIZE;
				if ( devWord != 0 ) {
					devLen = AK7739_PRAM_WORD_SIZE * devWord + 3;
				}
				akdbgprt("[AK7739] %s ***** Pram Split PIECE down devWord=0x%X, devLen=%d, nWSize=%d\n", __FUNCTION__, devWord, devLen, nWSize);
			}
				else {
					devWord = ((int)upRam[1] << 8 ) + (int)upRam[2];
					if ( devWord != 0 ) {
						devLen = 5 * devWord + 3;
					}
				}
			}
			else {
				devWord = ((int)upRam[1] << 8 ) + (int)upRam[2];
				if ( devWord != 0 ) {
					devLen = 5 * devWord + 3;
				}
			}
#else
			devWord = ((int)upRam[1] << 8 ) + (int)upRam[2];
			if ( devWord != 0 ) {
				devLen = 5 * devWord + 3;
			}
#endif
			akdbgprt("[AK7739] %s dev Length=%d\n", __FUNCTION__, devLen);
		}
		else if ( cmd == COMMAND_WRITE_CRAM ) {
			dspNo = (int)upRam[0] - COMMAND_WRITE_CRAM;
			switch(dspNo) {
				case 0:
					nMaxSize = TOTAL_NUM_OF_CRAM1_MAX;
					break;
				case 1:
					nMaxSize = TOTAL_NUM_OF_CRAM2_MAX;
					break;
				case 2:
					nMaxSize = TOTAL_NUM_OF_CRAM3_MAX;
					break;
				default:
					return(AK7739_ERROR_EINVAL);
			}
		}
		else if ( ( cmd2 == COMMAND_WRITE_OFREG ) || ( cmd2 == (COMMAND_WRITE_OFREG + 1) ) ) {
			dspNo = (int)upRam[0] - COMMAND_WRITE_OFREG;
			switch(dspNo) {
				case 0:
					nMaxSize = TOTAL_NUM_OF_OFREG1_MAX;
					break;
				case 1:
					nMaxSize = TOTAL_NUM_OF_OFREG2_MAX;
					break;
				default:
					return(AK7739_ERROR_EINVAL);
			}
			if ( dspNo > 1 ) {
				return(AK7739_ERROR_EINVAL);
			}
		}
		else {
			return(AK7739_ERROR_EINVAL);
		}

		dspBitMask = ( 1 << dspNo );
		if ( ( dspResetn & dspBitMask ) != 0 ) {
			ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, dspBitMask, 0);
		}

		ramdata = upRam;
		restLen = nWSize;

#ifdef AK7739_USE_SPLITED_PRAM // FIX_ME: need to use for all pram, test with DSP2 Pram
		//FIX_ME: old method should be fixed, it should not use add data for piece size
		if ( cmd == COMMAND_WRITE_PRAM ) {
			if(dspNo == 1) {
				akdbgprt("[AK7739] %s ***** Pram Split PIECE down for dsp2 devWord=0x%X, devLen=%d, nWSize=%d\n", __FUNCTION__, devWord, devLen, nWSize);
			}
			else {
				if ( devWord != 0 ) {
					ramdata += 3;
					restLen -= 3;
				}
			}	
		}
		else {
			if ( devWord != 0 ) {
				ramdata += 3;
				restLen -= 3;
			}
		}
#else
		if ( devWord != 0 ) {
			ramdata += 3;
			restLen -= 3;
		}
#endif

		firstEntrance = FALSE;
		count = 0;
	}

//	do {
		WDOG_DRV_Trigger(INST_WATCHDOG1);

		if ( restLen < devLen ) devLen = restLen;
		wCRC = calc_CRC(devLen, ramdata);
		n = MAX_LOOP_TIMES;
		do {
			 int32 temp =0;
			 temp = GetTickCount();
			ret = ak7739_ram_download(ak7739, ramdata, devLen, wCRC);
//			dsp_timer_tick1 = GetTickCount();
//			DPRINTF(DBG_MSG1,"ak7739_ram_download	START %d STOP %d Elasped Time:[%d] \n\r",temp,dsp_timer_tick1,dsp_timer_tick1 - temp);			
			if ( ret >= 0 ) break;
			n--;
		} while ( n > 0 );

		if ( ret < 0 ) {
			akdbgprt_err("%s: RAM Write Error! RAM No =0x%X \n", __FUNCTION__, cmd);
			DPRINTF(DBG_INFO,"%s: RAM Write Error! RAM No =0x%X \n", __FUNCTION__, cmd);
			#ifdef RECOVER_DSP_RAM_WRITE_FAIL
			uDspRamWriteFailCnt++;
			#endif
			if ( ( dspResetn & dspBitMask ) != 0 ) {
				ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, dspBitMask, dspBitMask);
			}
			firstEntrance = TRUE;
			lastExit	  = FALSE;
			return(AK7739_ERROR_EINVAL);
		}
		restLen -= devLen;
		if ( restLen > 0 ) ramdata += devLen;

		count++;
//	}while( restLen > 0 );

		if (restLen <= 0) lastExit = TRUE;

	if (TRUE == lastExit)
	{
		if ( ( dspResetn & dspBitMask ) != 0 ) {
			ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, dspBitMask, dspBitMask);
		}

		firstEntrance = TRUE;
		lastExit      = FALSE;
		returnEnd     = TRUE;
	}

	return (returnEnd);
}

#ifdef AK7739_USE_SPLITED_CRAM
static int ak7739_write_ram_splitedCram(ak7739_priv *pData,
		const unsigned char 	*upRam,
		int	 nWSize)
{
	ak7739_priv *ak7739 = pData;
	int div, remain, add, j;
	int splitedSize = AK7739_CRAM_SPLIT_SIZE;
	int cmdAddSize = AK7739_CRAM_WRITE_CMD_ADD_SIZE;
	unsigned char splitedCram[AK7739_CRAM_SPLIT_SIZE + AK7739_CRAM_WRITE_CMD_ADD_SIZE] = {0x00};

	static int index = 0;
	int returnMid = FALSE;
	int returnEnd = FALSE;

	if(splitedSize < 1) {
		akdbgprt_err("\t[AK7739] %s(%d) splitedSize=%d Invalid Value\n", __FUNCTION__,__LINE__,splitedSize);
		return (AK7739_ERROR_EINVAL);
	}

	div = (nWSize - cmdAddSize) / splitedSize;
	remain = (nWSize - cmdAddSize) % splitedSize;

	akdbgprt("[AK7739] %s ***** nWSize=%d splitedSize=%d div=%d remain=%d\n", __FUNCTION__, nWSize, splitedSize, div, remain);

	if(div) 
	{
		if (index < div) 
		{
			splitedCram[0] = upRam[0];
			splitedCram[1] = 0x00;
			splitedCram[2] = 0x00;
			if(index > 0) {
				add = splitedSize*index/AK7739_CRAM_ORAM_WORD_SIZE;
				splitedCram[1] = 0xFF & add >> 8;
				splitedCram[2] = 0xFF & add;
			}
			//akdbgprt("[AK7739] %s ***** index=%d cmd=%X add=0x%02X%02X\n", __FUNCTION__, index, splitedCram[0], splitedCram[1], splitedCram[2]);

			for(j = 0; j < splitedSize; j++) {
				splitedCram[cmdAddSize+j] = upRam[splitedSize*index+cmdAddSize+j];
			}
			akdbgprt("[AK7739] %s *****>> divCnt=%d write splited ram start\n", __FUNCTION__, index);
			returnMid = ak7739_write_ram(ak7739, splitedCram, sizeof(splitedCram));
			akdbgprt("[AK7739] %s *****<< divCnt=%d write splited ram finish\n", __FUNCTION__, index);

			if (TRUE == returnMid) index++;
		}
		else
		if(remain) 
		{
			splitedCram[0] = upRam[0];
			splitedCram[1] = 0x00;
			splitedCram[2] = 0x00;
			if(index > 0) {
				add = splitedSize*index/AK7739_CRAM_ORAM_WORD_SIZE;
				splitedCram[1] = 0xFF & add >> 8;
				splitedCram[2] = 0xFF & add;
			}
			//akdbgprt("[AK7739] %s ***** index=%d cmd=%d add=0x%X%X\n", __FUNCTION__, index, splitedCram[0], splitedCram[1], splitedCram[2]);

			for(j = 0; j < remain; j++) {
				splitedCram[cmdAddSize+j] = upRam[splitedSize*index+cmdAddSize+j];
			}

			returnMid = ak7739_write_ram(ak7739, splitedCram, remain);
			
			index = 0;
			if (TRUE == returnMid) returnEnd = TRUE;
		}
		else
		{
			index = 0;
			returnEnd = TRUE;
		}
	}
	else 
	{
		akdbgprt("[AK7739] %s ***** nWSize=%d splitedSize=%d div=%d remain=%d write without split\n", __FUNCTION__, nWSize, splitedSize, div, remain);
		returnMid = ak7739_write_ram(ak7739, upRam, nWSize);
		
		if (TRUE == returnMid) returnEnd = TRUE;
	}

	return (returnEnd);
}
#endif

static int ak7739_firmware_write_ram(ak7739_priv *pData, DSPNUMTYPE dspno, IMGTYPE imgtype, RAMNUMTYPE ramnumtype)
{
	ak7739_priv *ak7739 = pData;
	int returnEnd = TRUE;
	int nRamSize;
	const unsigned char  *ram_basic;

	akdbgprt("[AK7739] %s ***** dspnumtype=%d IMGTYPE=%d, ramnumtype=%d\n", __FUNCTION__, dspno, imgtype, ramnumtype);

	if ( ramnumtype == 0 ) return(returnEnd);

	if ( ramnumtype > RAMNUM_AUDIOEFFECT ) {
		akdbgprt_err("\t%s: invalid ramnumtype %d\n", __FUNCTION__, ramnumtype);
		return( AK7739_ERROR_EINVAL);
	}

	switch(imgtype) {
		case IMGTYPE_PRAM:
			akdbgprt("[AK7739] %s ***** IMGTYPE_PRAM\n", __FUNCTION__);
			if ( dspno == DSPNUMTYPE_DSP1) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_pram_dsp1_basic;
					nRamSize = sizeof(ak7739_pram_dsp1_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
						ram_basic = ak7739_pram_dsp1_audioEffect_sony;
						nRamSize = sizeof(ak7739_pram_dsp1_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_pram_dsp1_audioEffect;
					nRamSize = sizeof(ak7739_pram_dsp1_audioEffect);
				}
			}
			}
			else if ( dspno == DSPNUMTYPE_DSP2) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_pram_dsp2_basic;
					nRamSize = sizeof(ak7739_pram_dsp2_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
						ram_basic = ak7739_pram_dsp2_audioEffect_sony;
						nRamSize = sizeof(ak7739_pram_dsp2_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_pram_dsp2_audioEffect;
					nRamSize = sizeof(ak7739_pram_dsp2_audioEffect);
				}
			}
			}
			else if ( dspno == DSPNUMTYPE_DSP3) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_pram_dsp3_basic;
					nRamSize = sizeof(ak7739_pram_dsp3_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
						ram_basic = ak7739_pram_dsp3_audioEffect_sony;
						nRamSize = sizeof(ak7739_pram_dsp3_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_pram_dsp3_audioEffect;
					nRamSize = sizeof(ak7739_pram_dsp3_audioEffect);
				}
			}
			}
				break;
		case IMGTYPE_CRAM:
			akdbgprt("[AK7739] %s ***** IMGTYPE_CRAM\n", __FUNCTION__);
			if ( dspno == DSPNUMTYPE_DSP1) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_cram_dsp1_basic;
					nRamSize = sizeof(ak7739_cram_dsp1_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
						ram_basic = ak7739_cram_dsp1_audioEffect_sony;
						nRamSize = sizeof(ak7739_cram_dsp1_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_cram_dsp1_audioEffect;
					nRamSize = sizeof(ak7739_cram_dsp1_audioEffect);
				}
			}
			}
			else if ( dspno == DSPNUMTYPE_DSP2) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_cram_dsp2_basic;
					nRamSize = sizeof(ak7739_cram_dsp2_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
					ram_basic = ak7739_cram_dsp2_audioEffect_sony;
					nRamSize = sizeof(ak7739_cram_dsp2_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_cram_dsp2_audioEffect;
					nRamSize = sizeof(ak7739_cram_dsp2_audioEffect);
				}
			}
			}
			else if ( dspno == DSPNUMTYPE_DSP3) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_cram_dsp3_basic;
					nRamSize = sizeof(ak7739_cram_dsp3_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
					ram_basic = ak7739_cram_dsp3_audioEffect_sony;
					nRamSize = sizeof(ak7739_cram_dsp3_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_cram_dsp3_audioEffect;
					nRamSize = sizeof(ak7739_cram_dsp3_audioEffect);
				}
				}
				}
				break;
		case IMGTYPE_ORAM:
			akdbgprt("[AK7739] %s ***** IMGTYPE_ORAM\n", __FUNCTION__);
			if ( dspno == DSPNUMTYPE_DSP1) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_offram_dsp1_basic;
					nRamSize = sizeof(ak7739_offram_dsp1_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
					ram_basic = ak7739_offram_dsp1_audioEffect_sony;
					nRamSize = sizeof(ak7739_offram_dsp1_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_offram_dsp1_audioEffect;
					nRamSize = sizeof(ak7739_offram_dsp1_audioEffect);
				}
			}
			}
			else if ( dspno == DSPNUMTYPE_DSP2) {
				if ( ramnumtype == RAMNUM_BASIC ) {
					ram_basic = ak7739_offram_dsp2_basic;
					nRamSize = sizeof(ak7739_offram_dsp2_basic);
				}
				else if ( ramnumtype == RAMNUM_AUDIOEFFECT ){
					if(r_sonysound)
					{
					ram_basic = ak7739_offram_dsp2_audioEffect_sony;
					nRamSize = sizeof(ak7739_offram_dsp2_audioEffect_sony);
					}
					else
					{
					ram_basic = ak7739_offram_dsp2_audioEffect;
					nRamSize = sizeof(ak7739_offram_dsp2_audioEffect);
				}
				}
				}
				break;
			default:
				return( AK7739_ERROR_EINVAL);
		}

#ifdef AK7739_USE_SPLITED_CRAM
	if(imgtype != IMGTYPE_PRAM)
		returnEnd = ak7739_write_ram_splitedCram(ak7739, ram_basic, nRamSize);
	else
		returnEnd = ak7739_write_ram(ak7739, ram_basic, nRamSize);
#else
	returnEnd = ak7739_write_ram(ak7739, ram_basic, nRamSize);
#endif

	return (returnEnd);
}

/*
 * Functions for debug
 *
 */
int test_read_ram(ak7739_priv *pData, DSPNUMTYPE dspno, IMGTYPE imgtype, int addr, int readlen)
{
#ifdef AK7739_DEBUG_RAMDUMP
	ak7739_priv *ak7739 = pData;
	unsigned char	tx[3], rx[TOTAL_NUM_OF_PRAM1_MAX];
	int i, n, plen, clen, olen;
	int dspResetn;
	int dspBitMask;

	akdbgprt("*****[AK7739] %s(%d) dspno[%d] imgtype[%d] addr[0x%X] readlen[%d]\n", __FUNCTION__,__LINE__, dspno+1, imgtype, addr, readlen);

	dspResetn = ak7739_reg_read(ak7739, AK7739_C1_000_DSP_RESET_CONTROL);
	
	dspBitMask = ( 1 << dspno );
	if ( ( dspResetn & dspBitMask ) != 0 ) {
		ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, dspBitMask, 0);
	}

	ak7739_set_status(ak7739, STATUS_DOWNLOAD);

	if ( imgtype == IMGTYPE_PRAM ) {
		plen = readlen;
		
		for ( n = 0 ; n < plen ; n++ ) rx[n] = 0;
		tx[0] = (COMMAND_WRITE_PRAM & 0x7F)+dspno;
		tx[1] = (unsigned char)(0xFF & (addr >> 8));
		tx[2] = (unsigned char)(0xFF & addr);
		ak7739_reads(ak7739, tx, 3, rx, plen);
		akdbgprt_dump("\t*****%s AK7739 PRAM dspno[%d] CMD=0x%X, LEN=%d PAddr=0x%02X%02X*******\n", __FUNCTION__, dspno+1, (int)tx[0], plen, tx[1], tx[2]);

		akdbgprt_dump("\t");
		for ( i = 0 ; i < plen ; i ++ ) {
			akdbgprt_dump("0x%02X, ", (int)rx[i]);
			if (((i+1) >= 5) && ((i+1) % 5 == 0))
				akdbgprt_dump("\n\t");
		}
		akdbgprt_dump("\n");
		akdbgprt("*****%s AK7739 dspno[%d] Pram [%d]bytes *******\n", __FUNCTION__, dspno+1, i);
	}
	else if ( imgtype == IMGTYPE_CRAM ){
		clen = readlen;

		for ( n = 0 ; n < clen ; n++ ) rx[n] = 0;
		tx[0] = (COMMAND_WRITE_CRAM & 0x7F)+dspno;
		tx[1] = (unsigned char)(0xFF & (addr >> 8));
		tx[2] = (unsigned char)(0xFF & addr);
		ak7739_reads(ak7739, tx, 3, rx, clen);
		akdbgprt_dump("\t*****%s AK7739 CRAM dspno[%d] CMD=0x%X, LEN=%d CAddr=0x%02X%02X*******\n", __FUNCTION__, dspno+1, (int)tx[0], clen, tx[1], tx[2]);

		akdbgprt_dump("\t");
		for ( i = 0 ; i < clen ; i ++ ) {
			akdbgprt_dump("0x%02X, ", (int)rx[i]);
			if (((i+1) >= 4) && ((i+1) % 4 == 0))
				akdbgprt_dump("\n\t");
		}
		akdbgprt_dump("\n");
		akdbgprt("*****%s AK7739 dspno[%d] Cram [%d]bytes *******\n", __FUNCTION__, dspno+1, i);
	}
	else if ( imgtype == IMGTYPE_ORAM ){
		olen = readlen;

		//for ( n = 0 ; n < (3 * olen) ; n++ ) rx[n] = 0; //OFREG 3byte is wrong
		for ( n = 0 ; n < olen ; n++ ) rx[n] = 0; //20200409_Minor_fix to init proper rx buffer size
		tx[0] = (COMMAND_WRITE_OFREG & 0x7F)+dspno;
		tx[1] = (unsigned char)(0xFF & (addr >> 8));
		tx[2] = (unsigned char)(0xFF & addr);
		ak7739_reads(ak7739, tx, 3, rx, olen); //OFREG 4byte
		akdbgprt_dump("\t*****%s AK7739 OFFRAM dspno[%d] CMD=0x%X, LEN = %d OAddr=0x%02X%02X*******\n", __FUNCTION__, dspno+1, (int)tx[0], olen, tx[1], tx[2]);

		akdbgprt_dump("\t");
		for ( i = 0 ; i < olen ; i ++ ) {
			akdbgprt_dump("0x%02X, ", (int)rx[i]);
			if (((i+1) >= 4) && ((i+1) % 4 == 0))
				akdbgprt_dump("\n\t");
		}
		akdbgprt_dump("\n");
		akdbgprt("*****%s AK7739 dspno[%d] Offram [%d]bytes *******\n", __FUNCTION__, dspno+1, i);
	}

	ak7739_set_status(ak7739, STATUS_DOWNLOAD_FINISH);

	if ( ( dspResetn & dspBitMask ) != 0 ) {
		ak7739_reg_update_bits(ak7739, AK7739_C1_000_DSP_RESET_CONTROL, dspBitMask, dspBitMask);
	}
#endif
	return(0);

}

int set_test_reg_read(ak7739_priv *pData, int setValue)
{
	ak7739_priv *ak7739 = pData;
	int    i, value;
	int	   regs, rege;


	if ( setValue < 7 ) {
		switch(setValue) {
			case 0: regs = 0x000; rege = 0x0CB; break;
			case 1: regs = 0x100; rege = 0x18B; break;
			case 2: regs = 0x200; rege = 0x22F; break;
			case 3: regs = 0x300; rege = 0x32F; break;
			case 4: regs = 0x1000; rege = 0x1042; break;
			case 5: regs = 0x2000; rege = 0x2211; break;
			case 6: regs = 0x3000; rege = 0x3015; break;
			default: regs = 0x000; rege = 0x0FF; break;
		}
		for ( i = regs ; i <= rege ; i++ )
		{
			if (!ak7739_readable(i)) continue;
			value = ak7739_reg_read(ak7739, i);
//			akdbgprt_dump("W:0x%02X,0x%04X,0x%02X\n", (0xC0 + ((0xF000 & i)>>12)), (0xFFF & i), value);
			DPRINTF(DBG_MSG3,"W:0x%02X,0x%04X,0x%02X\n", (0xC0 + ((0xF000 & i)>>12)), (0xFFF & i), value);
		}
	}

	return 0;

}

/*
 * Functions for device control
 *
 */
#define  DOWNLOAD_PLAY_ADDRESS_AUDIO_DATA   27  // 31 - 7 + 3
#define  DOWNLOAD_PLAY_ADDRESS_START        19  //
#define  DOWNLOAD_PLAY_ADDRESS_END          24  //

#define AK7739_VCO_FREQUENCY   147456000

static int XtiFsTab[] = {
	12288000,  18432000, 24576000
};

static const struct {
	int pllInputFs;
	int plmBit;
	int plnBit;
} ak7739PLLRefClock[] = {
   { 256000, 0, 0x47C },
   { 384000, 0, 0x2FC },
   { 512000, 0, 0x23C },
   { 768000, 0, 0x17C },
   { 1024000, 0, 0x11C },
   { 1152000, 0, 0xFC },
   { 1536000, 0, 0xBC },
   { 2048000, 0, 0x8C },
   { 2304000, 0, 0x7C },
   { 3072000, 0, 0x5C },
   { 4096000, 1, 0x8C },
   { 4608000, 1, 0x7C },
   { 6144000, 1, 0x5C },
   { 8192000, 3, 0x8C },
   { 9216000, 3, 0x7C },
   { 12288000, 3, 0x5C },
   { 18432000, 7, 0x7C },
   { 24576000, 7, 0x5C },
};

static int sdfstab[] = {
    8000, 12000, 16000, 24000,
    32000, 48000, 96000, 192000
};

static int sdbicktab[] = {
	64, 48, 32, 128, 256, 512
};

static int setPLLOut(
		ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int nPLLInFs;
	int value, nBfs, fs;
	int n, nMax;
	int addr;

	value = (0x7 & ak7739->PLLInput);
	ak7739_reg_update_bits(ak7739, AK7739_C0_001_SYSCLOCK_SETTING2, 0x07, value);

	if ( ak7739->PLLInput == PLLINPUT_TIELOW ) {
		return(0);
	}
	else if ( ak7739->PLLInput == PLLINPUT_XTI ) {
		nPLLInFs = XtiFsTab[ak7739->XtiFs];
	}
	else {
		nBfs = sdbicktab[ak7739->SDBick[ak7739->PLLInput-2]];
		fs = sdfstab[ak7739->SDfs[ak7739->PLLInput-2]];
		akdbgprt_info("[AK7739] %s nBfs=%d fs=%d\n", __FUNCTION__, nBfs, fs);
		nPLLInFs = nBfs * fs;
		if ( ( fs % 4000 ) != 0 ) {
			nPLLInFs *= 441;
			nPLLInFs /= 480;
		}
	}

	n = 0;
	nMax = sizeof(ak7739PLLRefClock) / sizeof(ak7739PLLRefClock[0]);

	do {
		akdbgprt_debug("[AK7739] %s n=%d PLL:%d %d\n", __FUNCTION__, n, nPLLInFs, ak7739PLLRefClock[n].pllInputFs);
		if ( nPLLInFs == ak7739PLLRefClock[n].pllInputFs ) break;
		n++;
	} while ( n < nMax);


	if ( n == nMax ) {
		akdbgprt_err("\t%s: PLL1 setting Error!\n", __FUNCTION__);
		return(AK7739_ERROR_EINVAL);
	}

	addr = AK7739_C0_002_SYSCLOCK_SETTING3;
	ak7739_reg_update_bits(ak7739, addr, 0xFF, ak7739PLLRefClock[n].plmBit);
	value = (0x00FF & ak7739PLLRefClock[n].plnBit);
	addr++;
	ak7739_reg_update_bits(ak7739, addr, 0xFF, value);
	value = ((0xFF00 & ak7739PLLRefClock[n].plnBit) >> 8);
	addr++;
	ak7739_reg_update_bits(ak7739, addr, 0xFF, value);

	return(0);

}

static int set_pllinput(
		ak7739_priv *pData,
		PLLINPUTTYPE setValue)
{
	ak7739_priv *ak7739 = pData;

	if (setValue < PLLINPUT_MAX){
		if ( ak7739->PLLInput != setValue ) {
			ak7739->PLLInput = setValue;
			setPLLOut(ak7739);
		}
	}
	else {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}
	return 0;
}

static int set_xtifs(
		ak7739_priv *pData,
		XTIFSTYPE setValue)
{
    ak7739_priv *ak7739 = pData;
	int    tabNum;

	tabNum = sizeof(XtiFsTab) / sizeof(XtiFsTab[0]);

	if (setValue < tabNum){
		if ( ak7739->XtiFs != setValue ) {
			ak7739->XtiFs = setValue;
			setPLLOut(ak7739);
		}
	}
	else {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}
	return 0;
}

static int setSDXClock(
		ak7739_priv *pData,
		int nSDNo)
{
	ak7739_priv *ak7739 = pData;
	int addr;
	int fs, bickfs;
	int sdv, bdv;
	int dev;

	if ( nSDNo >= AK7739_SYNCDOMAIN_NUM ) return(0);

	fs = sdfstab[ak7739->SDfs[nSDNo]];
	bickfs = sdbicktab[ak7739->SDBick[nSDNo]] * fs;

	addr = AK7739_C0_012_SYNCDOMAIN1_SETTING3 + ( 4 * nSDNo );

	bdv = AK7739_VCO_FREQUENCY / bickfs;
	dev = (0x3 & AK7739_AHDIV_BIT) + 1;
	bdv /= dev;

	sdv = ak7739->SDBick[nSDNo];
	akdbgprt("[AK7739] %s, BDV=%d, SDV=%d\n", __FUNCTION__, bdv, sdbicktab[sdv]);
	bdv--;
	if ( bdv > 255) {
		akdbgprt_err("\t%s: BDV Error! SD No = %d, bdv bit = %d\n", __FUNCTION__, nSDNo, bdv);
		bdv = 255;
	}

	akdbgprt("[AK7739] %s, bdv bits=0x%X\n", __FUNCTION__, bdv);
	ak7739_reg_update_bits(ak7739, addr, 0xFF, bdv); //BDIV[7:0]

	addr++;
	akdbgprt("[AK7739] %s, sdv bits=0x%X\n", __FUNCTION__, sdv);
	ak7739_reg_update_bits(ak7739, addr, 0x07, sdv);

	if ( ak7739->PLLInput == (nSDNo + 2) ) {
		setPLLOut(ak7739);
	}

	return(0);

}

static int setSDXMaster(
		ak7739_priv *pData,
		SYNCDOMAINTYPE nSDNo,
		int nMaster)
{
	ak7739_priv *ak7739 = pData;
	int addr;
	int value;

	akdbgprt_debug("[AK7739] %s nSDNo = %d nMaster = %d\n", __FUNCTION__, nSDNo, nMaster);

	if ( nSDNo >= AK7739_SYNCDOMAIN_NUM ) return(0);

	addr = AK7739_C0_010_SYNCDOMAIN1_SETTING1 + ( 4 * nSDNo );

	ak7739->Master[nSDNo] = nMaster;
	value = ((nMaster == 1) ? 0x80: 0);
	ak7739_reg_update_bits(ak7739, addr, 0x80, value);

	addr++;
	value = ak7739->SDCks[nSDNo];
	ak7739_reg_update_bits(ak7739, addr, 0x07, value);

	return(0);
}

static int set_sdX_ms(
		ak7739_priv *pData,
		SYNCDOMAINTYPE nSDNo,
		i2s_master_slave_type setValue)
{
	ak7739_priv *ak7739 = pData;


	if (setValue < 2){
		setSDXMaster(ak7739, nSDNo, setValue);
	}

    return 0;
}

static int set_sdX_fs(
		ak7739_priv *pData,
		SYNCDOMAINTYPE nSDNo,
		i2s_samplerate_type setValue)
{
    ak7739_priv *ak7739 = pData;


	if (setValue <= SAMPLERATE_192KHZ){
		if ( ak7739->SDfs[nSDNo] != setValue ) {
			ak7739->SDfs[nSDNo] = setValue;
			setSDXClock(ak7739, nSDNo);
			setSDXMaster(ak7739, nSDNo, ak7739->Master[nSDNo]);
		}
	}
	else {
		akdbgprt(" [AK7739] %s Invalid Value selected! Mode = %d\n", __FUNCTION__, setValue);
	}
    return 0;

}

static int set_sdX_bick(
		ak7739_priv *pData,
		SYNCDOMAINTYPE nSDNo,
		i2s_bickfs_type setValue)
{
    ak7739_priv *ak7739 = pData;


	if (setValue <= BICKFS_512FS){
		if ( ak7739->SDBick[nSDNo] != setValue ) {
			ak7739->SDBick[nSDNo] = setValue;
			setSDXClock(ak7739, nSDNo);
		}
	}
	else {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}
	return 0;
}

static int set_portX_tdm(
		ak7739_priv *pData,
		SYNCDOMAINTYPE nSDNo,
		i2s_tdmcontrol_type setValue)
{
	ak7739_priv *ak7739 = pData;


	if (setValue < 2){
		if ( ak7739->TDMMode[nSDNo] != setValue ) {
			ak7739->TDMMode[nSDNo] = setValue;
			ak7739->DIEDGEbit[nSDNo] = ak7739->TDMMode[nSDNo];
			ak7739->DOEDGEbit[nSDNo] = ak7739->TDMMode[nSDNo];
		}
	}
	else {
		akdbgprt(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}
    return 0;
}

static int set_portX_slot(
		ak7739_priv *pData,
		SYNCDOMAINTYPE nSDNo,
		i2s_ioslotsize_type setValue)
{
	ak7739_priv *ak7739 = pData;


	if (setValue < 4){
		if ( ak7739->DxSLbit[nSDNo] != setValue ) {
			ak7739->DxSLbit[nSDNo] = setValue;
		}
	}
	else {
		akdbgprt(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}
    return 0;
}

int set_cram_write_value(
		ak7739_priv *pData,
		DSPNUMTYPE dspno,
		unsigned int addr,
		unsigned int setValue)
{
	ak7739_priv *ak7739 = pData;
	unsigned char cram[4];
	int    maxAddr;
	int    ret;

	if ((dspno < DSPNUMTYPE_DSP1) || (dspno > DSPNUMTYPE_DSP3)) {
		akdbgprt_err("\tAK7739 CRAM Write Address DSP No Error Address=0x%X\n", addr);
		return(AK7739_ERROR_EINVAL);
	}

	switch(dspno) {
		case DSPNUMTYPE_DSP1:
			maxAddr = AK7739_CRAM1_MAX_ADDRESS;
			break;
		case DSPNUMTYPE_DSP2:
			maxAddr = AK7739_CRAM2_MAX_ADDRESS;
			break;
		case DSPNUMTYPE_DSP3:
			maxAddr = AK7739_CRAM3_MAX_ADDRESS;
			break;
		default:
			akdbgprt_err("\tAK7739 CRAM Write Address DSP No Error Address=0x%X\n", addr);
			return AK7739_ERROR_EINVAL;
	}

	if ( addr > maxAddr ) {
		akdbgprt_err("\tAK7739 CRAM%d Write Address Error:addr=0x%X\n", dspno, addr);
		return AK7739_ERROR_EINVAL;
	}
	cram[0] = (unsigned char)((0xFF000000 & setValue) >> 24);
	cram[1] = (unsigned char)((0xFF0000 & setValue) >> 16);
	cram[2] = (unsigned char)((0xFF00 & setValue) >> 8);
	cram[3] = (unsigned char)((0xFF & setValue));

	ret = ak7739_write_cram(ak7739, dspno, addr, 4, cram);

#ifdef AK7739_DEBUG_CRAM_READ_WRITE
	test_read_ram(ak7739, dspno, IMGTYPE_CRAM, addr, 4);
	#endif

	if ( ret < 0 ) return(ret);

	return(0);
}

static int set_mir_value(
		ak7739_priv *pData,
		int setValue)
{
	ak7739_priv *ak7739 = pData;
	unsigned long  uMIRData;
	int dspNo = 1;
	int n;

	n = dspNo - 1;
	ak7739->MIRNo[n] = setValue;

	ak7739_readMIR(ak7739, dspNo, (0xF & (ak7739->MIRNo[n])), &uMIRData);
	akdbgprt("AK7739 DSP%d MIR No%d : %lX\n", dspNo, ak7739->MIRNo[n], uMIRData);

	return(0);
}

static int set_mir2_value(ak7739_priv *pData, int setValue)
{
	ak7739_priv *ak7739 = pData;
	unsigned long  uMIRData;
	int dspNo = 2;
	int n;

	n = dspNo - 1;
	ak7739->MIRNo[n] = setValue;

	ak7739_readMIR(ak7739, dspNo, (0xF & (ak7739->MIRNo[n])), &uMIRData);
	akdbgprt("AK7739 DSP%d MIR No%d : %lX\n", dspNo, ak7739->MIRNo[n], uMIRData);

	return(0);
}

static int set_mir3_value(ak7739_priv *pData, int setValue)
{
	ak7739_priv *ak7739 = pData;
	unsigned long  uMIRData;
	int dspNo = 3;
	int n;

	n = dspNo - 1;
	ak7739->MIRNo[n] = setValue;

	ak7739_readMIR(ak7739, dspNo, (0xF & (ak7739->MIRNo[n])), &uMIRData);
	akdbgprt("AK7739 DSP%d MIR No%d : %lX\n", dspNo, ak7739->MIRNo[n], uMIRData);

	return(0);
}

static int ak7739_hw_params(ak7739_priv *pData,
		int sampleRate,
		PCMLENGTHTYPE dataSize,
		AIFPORTTYPE portId)
{
	ak7739_priv *ak7739 = pData;
	int nSDNo;
	int fsno, nmax;
	int DIODLbit, addr, value;

	akdbgprt_info("[AK7739] %s(%d)\n", __FUNCTION__,__LINE__);

	ak7739->fs = sampleRate;

	akdbgprt_info("[AK7739] %s fs=%d\n", __FUNCTION__, ak7739->fs );
	mdelay(10);

	DIODLbit = 2;

	switch (dataSize) {
		case AK7739_PCM_FORMAT_S16_LE:
			DIODLbit = 2;
			break;
		case AK7739_PCM_FORMAT_S24_LE:
			DIODLbit = 0;
			break;
		case AK7739_PCM_FORMAT_S32_LE:
			DIODLbit = 3;
			break;
		default:
			akdbgprt_err("\t%s: invalid Audio format %u\n", __FUNCTION__, dataSize);
			return AK7739_ERROR_EINVAL;
	}

	switch (portId) {
		case AK7739_AIF_PORT1: nSDNo = 0; break;
		case AK7739_AIF_PORT2: nSDNo = 1; break;
		case AK7739_AIF_PORT3: nSDNo = 2; break;
		case AK7739_AIF_PORT4: nSDNo = 3; break;
		case AK7739_AIF_PORT5: nSDNo = 4; break;
		default:
			akdbgprt_err("\t%s: Invalid dai portId %d\n", __FUNCTION__, portId);
			return AK7739_ERROR_EINVAL;
			break;
	}	

	fsno = 0;
	nmax = sizeof(sdfstab) / sizeof(sdfstab[0]);
	akdbgprt_info("[AK7739] %s nmax = %d\n", __FUNCTION__, nmax);

	do {
		if ( ak7739->fs <= sdfstab[fsno] ) break;
		fsno++;
	} while ( fsno < nmax );
	akdbgprt_info("[AK7739] %s fsno = %d\n", __FUNCTION__, fsno);

	if ( fsno == nmax ) {
		akdbgprt_err("\t%s: not support Sampling Frequency : %d\n", __FUNCTION__, ak7739->fs);
		return AK7739_ERROR_EINVAL;
	}

	akdbgprt_info("[AK7739] %s setSDClock\n", __FUNCTION__);
	mdelay(10);

	ak7739->SDfs[nSDNo] = fsno;
	setSDXClock(ak7739, nSDNo);

	/* set Word length */
	value = DIODLbit;

	addr = AK7739_C0_050_SDIN1_INPUT_FORMAT + 2 * nSDNo;
	ak7739_reg_update_bits(ak7739, addr, 0x03, value);

	addr = AK7739_C0_060_SDOUT1_OUTPUT_FORMAT + 2 * nSDNo;
	ak7739_reg_update_bits(ak7739, addr, 0x03, value);

	return 0;
}

static int ak7739_set_dai_fmt(ak7739_priv *pData,
		AIFPORTTYPE portId,
		DAIFMTSYNCTYPE daiSync,
		DAIFMTDATATYPE daiFmt
)
{
	ak7739_priv *ak7739 = pData;
	int format, diolsb, diedge, doedge, dislot, doslot;
	int msnbit;
	SYNCDOMAINTYPE nSDNo;
	int value;
	int addr;

	akdbgprt("[AK7739] %s(%d) portId = %d\n", __FUNCTION__,__LINE__,portId);

	switch (portId) {
		case AK7739_AIF_PORT1: nSDNo = 0; break;
		case AK7739_AIF_PORT2: nSDNo = 1; break;
		case AK7739_AIF_PORT3: nSDNo = 2; break;
		case AK7739_AIF_PORT4: nSDNo = 3; break;
		case AK7739_AIF_PORT5: nSDNo = 4; break;
		default:
			akdbgprt_err("\t%s: Invalid dai id %d\n", __FUNCTION__, portId);
			return AK7739_ERROR_EINVAL;
			break;
	}

	/* set master/slave audio interface */
    switch (daiSync) {
        case AK7739_DAIFMT_CBS_CFS:
			msnbit = 0;
			akdbgprt("[AK7739] %s(Slave_nSDNo=%d)\n", __FUNCTION__,nSDNo);
            break;
        case AK7739_DAIFMT_CBM_CFM:
			msnbit = 1;
			akdbgprt("[AK7739] %s(Master_nSDNo=%d)\n", __FUNCTION__,nSDNo);
            break;
        case AK7739_DAIFMT_CBS_CFM:
        case AK7739_DAIFMT_CBM_CFS:
        default:
            akdbgprt_err("\t[AK7739] %s(%d)\n", __FUNCTION__,__LINE__);
           return AK7739_ERROR_EINVAL;
   	}

	switch (daiFmt) {
		case AK7739_DAIFMT_I2S:
			format = AK7739_LRIF_I2S_MODE; // 0
			diolsb = 0;
			diedge = ak7739->DIEDGEbit[nSDNo];
			doedge = ak7739->DOEDGEbit[nSDNo];
			dislot = 3;
			doslot = 3;
			break;
		case AK7739_DAIFMT_LEFT_J:
			format = AK7739_LRIF_MSB_MODE; // 5
			diolsb = 0;
			diedge = ak7739->DIEDGEbit[nSDNo];
			doedge = ak7739->DOEDGEbit[nSDNo];
			dislot = 3;
			doslot = 3;
			break;
		case AK7739_DAIFMT_RIGHT_J:
			format = AK7739_LRIF_LSB_MODE; // 5
			diolsb = 1;
			diedge = ak7739->DIEDGEbit[nSDNo];
			doedge = ak7739->DOEDGEbit[nSDNo];
			dislot = 3;
			doslot = 3;
			break;
		case AK7739_DAIFMT_DSP_A:
			format = AK7739_LRIF_PCM_SHORT_MODE; // 6
			diolsb = 0;
			diedge = 1;
			doedge = 1;
			dislot = ak7739->DxSLbit[nSDNo];
			doslot = ak7739->DxSLbit[nSDNo];
			break;
		case AK7739_DAIFMT_DSP_B:
			format = AK7739_LRIF_PCM_LONG_MODE; // 7
			diolsb = 0;
			diedge = 1;
			doedge = 1;
			dislot = ak7739->DxSLbit[nSDNo];
			doslot = ak7739->DxSLbit[nSDNo];
			break;
		default:
			return AK7739_ERROR_EINVAL;
	}

	/* set format */
	setSDXMaster(ak7739, nSDNo, msnbit);
	addr = AK7739_C0_040_BICK_FORMAT_SETTING1 + 2 * nSDNo;
	ak7739_reg_update_bits(ak7739, addr, 0x87, format);

	/* set SDIO format */
	/* set Slot length */
	
	addr = AK7739_C0_050_SDIN1_INPUT_FORMAT + 2 * nSDNo;
	value = (diedge <<  7) + (diolsb <<  3) + (dislot <<  4);
	ak7739_reg_update_bits(ak7739, addr, 0xB8, value);
	if ( nSDNo == (AK7739_SDIO6_SYNC_DOMAIN - 1) ) {
		ak7739_reg_update_bits(ak7739, AK7739_C0_05A_SDIN6_INPUT_FORMAT, 0xB8, value);
	}
	
	addr = AK7739_C0_060_SDOUT1_OUTPUT_FORMAT + 2 * nSDNo;
	value = (doedge <<  7) + (diolsb <<  3) + (doslot <<  4);
	ak7739_reg_update_bits(ak7739, addr, 0xB8, value);
	if ( nSDNo == (AK7739_SDIO6_SYNC_DOMAIN - 1) ) {
		ak7739_reg_update_bits(ak7739, AK7739_C0_06A_SDOUT6_OUTPUT_FORMAT, 0xB8, value);
	}

	return(0);
}

static int set_mixer(
		ak7739_priv *pData,
		MIXERTYPE mixerType, MIXINPUTTYPE input, MIXADJTYPE adj, MIXSWAPTYPE swap)
{
		ak7739_priv *ak7739 = pData;
		unsigned int mask = 0xFF;
		unsigned int value = 0x00;

		akdbgprt("[AK7739] %s Mixer[%d] \n", __FUNCTION__, mixerType);

		if(mixerType > MIXER_2 || input > MIX_INPUT2 || adj > MIX_ADJ_MUTE || swap > MIX_SWAP_SWAP) {
			akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
			return AK7739_ERROR;
		}

		if (input == MIX_INPUT1) {
			mask = 0x33;
			value = adj<<4;
			value = value|swap;
		}
		else if (input == MIX_INPUT2) {
			mask = 0xCC;
			value = adj<<6;
			value = value|(swap<<2);
		}

		if (mixerType == MIXER_1) {
			ak7739_reg_update_bits(ak7739, AK7739_C2_210_MIXER1_SETTING, mask, value);
		}
		else if (mixerType == MIXER_2) {
			ak7739_reg_update_bits(ak7739, AK7739_C2_211_MIXER2_SETTING, mask, value);
		}

		return 0;
}

/* Control for user */

int set_mic_gain(
		ak7739_priv *pData,
		MICCHANNELTYPE mictype, MICGAINTYPE setValue)
{
	ak7739_priv *ak7739 = pData;

	if(setValue > MIC_GAIN_P30DB) {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
		return AK7739_ERROR;
	}

	if (mictype == MIC_LCH) {
		if(ak7739->micgianL != setValue) {
			ak7739->micgianL = setValue;
			ak7739_reg_update_bits(ak7739, AK7739_C0_0B1_DSP2_SYNCDOMAIN_SELECT, 0xF0, setValue<<4);
		}
	}
	else if(mictype == MIC_RCH) {
		if(ak7739->micgianR != setValue) {
			ak7739->micgianR = setValue;
			ak7739_reg_update_bits(ak7739, AK7739_C0_0B1_DSP2_SYNCDOMAIN_SELECT, 0x0F, setValue);
		}
	}
	else {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}
	return 0;
}

/*
 * ADC Volume Max 0(24dB), Min 255(Mute), 0.5dB Step
 *	AK7739 Datasheet Table 60. ADC Digital Volume Setting
 *	0x00 +24.0dB ... 0x2F +0.5dB
 *	: :
 *	0x30 (48) 0.0dB (default)
 *	0x31 -0.5dB
 *	: :
 *	0xFF Mute
 *
 * DAC Volume Max 0(12dB), Min 255(Mute), 0.5dB Step
 *	AK7739 Datasheet Table 66. DAC Digital Volume Setting
 *	0x00 +12.0dB
 *	: :
 *	0x17 +0.5dB
 *	0x18 (24) 0.0dB (default)
 *	0x19 -0.5dB
 *	: :
 *	0xFF Mute
 *
 */
int set_codec_digital_volume(
		ak7739_priv *pData,
		CODECTYPE codecType, unsigned int setValue)
{
	ak7739_priv *ak7739 = pData;

	if(setValue > 0xFF) {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
		return AK7739_ERROR;
	}

	if(codecType < DAC1_LCH) {
		int volume;
		volume = 24 - setValue*0.5;
		akdbgprt_info(" [AK7739] %s ADC setVal[%d][0x%02X] vol[%d]dB\n", __FUNCTION__, setValue, setValue, volume);
	}
	else {
		int volume;
		volume = 12 - setValue*0.5;
		akdbgprt_info(" [AK7739] %s DAC setVal[%d][0x%02X] vol[%d]dB\n", __FUNCTION__, setValue, setValue, volume);
	}

	if (codecType == ADC1_LCH) {
		if(ak7739->adc1DigitalL != setValue) {
			ak7739->adc1DigitalL = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_005_ADC1_LCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == ADC1_RCH) {
		if(ak7739->adc1DigitalR != setValue) {
			ak7739->adc1DigitalR = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_006_ADC1_RCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == ADC2_LCH) {
		if(ak7739->adc2DigitalL != setValue) {
			ak7739->adc2DigitalL = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_007_ADC2_LCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == ADC2_RCH) {
		if(ak7739->adc2DigitalR != setValue) {
			ak7739->adc2DigitalR = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_008_ADC2_RCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == DAC1_LCH) {
		if(ak7739->dac1DigitalL != setValue) {
			ak7739->dac1DigitalL = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_00C_DAC1_LCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == DAC1_RCH) {
		if(ak7739->dac1DigitalR != setValue) {
			ak7739->dac1DigitalR = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_00D_DAC1_RCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == DAC2_LCH) {
		if(ak7739->dac2DigitalL != setValue) {
			ak7739->dac2DigitalL = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_00E_DAC2_LCH_DIGITAL_VOLUME, setValue);
		}
	}
	else if (codecType == DAC2_RCH) {
		if(ak7739->dac2DigitalR != setValue) {
			ak7739->dac2DigitalR = setValue;
			ak7739_reg_write(ak7739, AK7739_C3_00F_DAC2_RCH_DIGITAL_VOLUME, setValue);
		}
	}
	else {
		akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
	}

	return 0;
}

int set_adc_soft_mute(
		ak7739_priv *pData,
		ADCTYPE adcType, MUTETYPE setValue)
{
		ak7739_priv *ak7739 = pData;

		akdbgprt("[AK7739] %s ADC[%d] \n", __FUNCTION__, adcType);

		if (adcType == ADC_1) {
			ak7739_reg_update_bits(ak7739, AK7739_C3_00B_ADC_MUTE_HPF_CONTROL, 0x40, setValue<<6);
		}
		else if (adcType == ADC_2) {
			ak7739_reg_update_bits(ak7739, AK7739_C3_00B_ADC_MUTE_HPF_CONTROL, 0x20, setValue<<5);
		}
		else {
			akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
		}

		return 0;
}

int set_dac_soft_mute(
		ak7739_priv *pData,
		DACTYPE dacType, MUTETYPE setValue)
{
		ak7739_priv *ak7739 = pData;

		akdbgprt("[AK7739] %s DAC[%d] mute[%d] \n", __FUNCTION__, dacType, setValue);

		if (dacType == DAC_1) {
			ak7739_reg_update_bits(ak7739, AK7739_C3_014_DAC_MUTE_FILTER_SETTING, 0x10, setValue<<4);
		}
		else if (dacType == DAC_2) {
			ak7739_reg_update_bits(ak7739, AK7739_C3_014_DAC_MUTE_FILTER_SETTING, 0x20, setValue<<5);
		}
		else {
			akdbgprt_info(" [AK7739] %s Invalid Value selected!\n", __FUNCTION__);
		}

		return 0;
}

#ifdef AK7739_USER_CONTROL_CODE
/*
 * Functions for user control
 *
 */
 
#ifdef AK7739_AUDIO_EFFECT_ENABLE
int ak7739_set_useCase(ak7739_priv *pData, AVNTUSECASETYPE useCase)
{
	ak7739_priv *ak7739 = pData;

	akdbgprt("[AK7739] %s >=============== Start\n", __FUNCTION__);
	akdbgprt_info("[AK7739] %s(%d) useCase = %d\n", __FUNCTION__, __LINE__, useCase);
	
		switch (useCase) {
		case AVNT_USECASE_AUDIOPATH_FM:
			akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_FM Use Case\n", __FUNCTION__);
			 #ifdef EXTERNAL_AMP
			//FM Select
			ak7739_reg_write(ak7739, AK7739_C0_121_SRC2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN2A]);

			//For EC/NR Ref A2B to EC/NR //SDIN3A-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);
			#else
			//For EC/NR Ref DSP2OUT1 to EC/NR //DSP2OUT1-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			set_ae_volume(ak7739, AE_Media1Gain, 30); //0db  //process for bt call 	//JANG_210901
			#endif
			//FM Select
			set_ae_sel4selector(ak7739, AE_MediaInputSW4, AE_SEL4IN_1); //FM Select //���� ����..
			break;
			
		case AVNT_USECASE_AUDIOPATH_MEDIA:			
		case AVNT_USECASE_AUDIOPATH_BT_A2DP:
		case AVNT_USECASE_AUDIOPATH_USB_HICAR_CARLIFE:
		case AVNT_USECASE_AUDIOPATH_VR_AECREF:
			if(useCase == AVNT_USECASE_AUDIOPATH_MEDIA)
				akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_BT_HFP Use Case\n", __FUNCTION__);
			else if(useCase == AVNT_USECASE_AUDIOPATH_BT_A2DP)
			akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_BT_A2DP Use Case\n", __FUNCTION__);
			else if(useCase == AVNT_USECASE_AUDIOPATH_USB_HICAR_CARLIFE)
				akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_USB_HICAR_CARLIFE Use Case\n", __FUNCTION__);
			else if(useCase == AVNT_USECASE_AUDIOPATH_VR_AECREF)
				akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_VR_AECREF Use Case\n", __FUNCTION__);
			#ifdef EXTERNAL_AMP
			//Media Select
			ak7739_reg_write(ak7739, AK7739_C0_121_SRC2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);

			//For EC/NR Ref A2B to EC/NR //SDIN3A-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);
			#else
			//For EC/NR Ref DSP2OUT1 to EC/NR //DSP2OUT1-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			#endif
			set_ae_sel4selector(ak7739, AE_MediaInputSW4, AE_SEL4IN_2); //���� ����..
			#if 0//test code for EC/NR Ref from EXT AMP
			ak7739_reg_write(ak7739, AK7739_C0_120_SRC1_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);

			ak7739_reg_write(ak7739, AK7739_C0_108_DAC1_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);
			ak7739_reg_write(ak7739, AK7739_C0_109_DAC2_INPUT_DATA_SELECT, ak7739_source_select_values[SRCO1]);
			#endif
			break;

		case AVNT_USECASE_AUDIOPATH_BCALL:
			akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_BCALL Use Case\n", __FUNCTION__);
			 #ifdef EXTERNAL_AMP
			//BCall Input Select
			ak7739_reg_write(ak7739, AK7739_C0_121_SRC2_INPUT_DATA_SELECT, ak7739_source_select_values[AK_ADC1]);

			//For EC/NR Ref A2B to EC/NR //SDIN3A-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN3A]);
			#else
			//BCall Input Select
			set_ae_sel4selector(ak7739, AE_MediaInputSW4, AE_SEL4IN_3);			//BCALL 

			//For EC/NR Ref DSP2OUT1 to EC/NR //DSP2OUT1-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			#endif
			break;

		case AVNT_USECASE_AUDIOPATH_BT_HFP:
			akdbgprt("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_BT_HFP Use Case\n", __FUNCTION__);
			//For EC/NR Ref SDIN1D to EC/NR //SDIN1D-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			set_ae_volume(ak7739, AE_Media1Gain, 0); //min //process for bt call 		//JANG_210901
			break;

		case AVNT_USECASE_AUDIOPATH_NAVI:
			akdbgprt_err("[AK7739] %s Set AVNT_USECASE_AUDIOPATH_NAVI Use Case\n", __FUNCTION__);
			//For EC/NR Ref SDIN1D to EC/NR //SDIN1D-> SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			break;
			
		default:
			akdbgprt_err("\t%s: invalid useCase\n", __FUNCTION__);
			return AK7739_ERROR_EINVAL;
	}

	if(useCase == AVNT_USECASE_AUDIOPATH_BT_HFP) 
	{
		//HFP Input to DSP2IN4
		ak7739_reg_write(ak7739, AK7739_C0_189_DSP2_DIN4_INPUT_DATA_SELECT, ak7739_source_select_values[SRCO7]);
//		ak7739_reg_write(ak7739, AK7739_C0_189_DSP2_DIN4_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
	}
	else 
	{
		//NAVI Input to DSP2IN4
		ak7739_reg_write(ak7739, AK7739_C0_189_DSP2_DIN4_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
	}

	ak7739_set_status(ak7739, STATUS_RUN);

	akdbgprt("[AK7739] %s <=============== End\n", __FUNCTION__);

	return 0;
}
#endif
#endif

int ak7739_write_spidmy(ak7739_priv *pData)
{
	unsigned char tx[4];
	int wlen;
	int rd;

	wlen = 4;
	tx[0] = (unsigned char)(0xDE);
	tx[1] = (unsigned char)(0xAD);
	tx[2] = (unsigned char)(0xDA);
	tx[3] = (unsigned char)(0x7A);

	akdbgprt("[AK7739] %s Send SPI Dummy code(0xDEADDA7A) to enable SPI\n", __FUNCTION__);

	rd = spi_writes(tx, wlen);

	return rd;
}

void ak7739_init_reset1(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	gpio_set_value(ak7739->pdn_gpio, 0);
}

void ak7739_init_reset2(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	gpio_set_value(ak7739->pdn_gpio, 1);
//	msleep(10); //20200409_Major_fix Wait 7ms after PDN High before Send SPI command, refer to the AK7739 Power-up Sequence
}

int ak7739_dummy_reg(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int i, devid;
	int dev, value;
	int busDev;

	akdbgprt("[AK7739] %s >=============== Start\n", __FUNCTION__);
	#if 0
	if ( ak7739->pdn_gpio > 0 )  {
//		akdbgprt("[AK7739] %s PDN pdn_gpio[%d] Low -> High\n", __FUNCTION__, ak7739->pdn_gpio);
		DPRINTF(DBG_MSG3," ak7739 reset  %d\n\r\n\r", ak7739->pdn_gpio);	
		gpio_set_value(ak7739->pdn_gpio, 0);
		msleep(1);
		gpio_set_value(ak7739->pdn_gpio, 1);
		msleep(7); //20200409_Major_fix Wait 7ms after PDN High before Send SPI command, refer to the AK7739 Power-up Sequence
	}
	#endif

	#if 0 //def DSP_BOOTING_SEQUENCE
	PORTB->PCR[0] = PORT_PCR_MUX(3);		//SPI
	PORTB->PCR[1] = PORT_PCR_MUX(3);		//SPI
	PORTB->PCR[2] = PORT_PCR_MUX(3);		//SPI
	PORTB->PCR[3] = PORT_PCR_MUX(3);		//SPI
	spi0_deinit();
	spi0_init();
	#endif

	ak7739_write_spidmy(ak7739); //Send SPI Dummy Code to enable SPI Mode

//	#ifndef DRIVER_TEST_WITHOUT_PLATFORM_CODE
	#if 1
	devid = ak7739_reg_read(ak7739, AK7739_C0_300_DEVICE_ID);
	akdbgprt("[AK7739] %s Device ID = 0x%X\n", __FUNCTION__, devid);
	
	DPRINTF(DBG_MSG3,"[AK7739] %s Device ID = 0x%X\n", __FUNCTION__, devid);
	if ( devid != 0x39 ) 
	{
		akdbgprt_err("\t[AK7739] %s Device ID read fail\n", __FUNCTION__);
		return 1;
	}
	#endif

	return 0;
}



uint32 check_test[10]={0,};
extern uint8 au8DspInfo[];
int ak7739_init_reg_cust(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int i;
	int dev, value;
	int busDev;
//	int ret;
	uint8 returnEnd = FALSE;	
	int   returnMid = TRUE;
	static unsigned int ak_init_reg_state = AK_INIT_REG_0;
	static unsigned int reg_write_cnt=0;
	
	switch(ak_init_reg_state)
	{
		case AK_INIT_REG_0:
			// set default value
			akdbgprt("[AK7739] %s set default reg value, size %d\n", __FUNCTION__, ARRAY_SIZE(ak7739_reg));
			#ifdef DSP_SEPERATE_DETAIL_210916
			if(ARRAY_SIZE(ak7739_reg)<=reg_write_cnt)
			{
				ak_init_reg_state = AK_INIT_REG_1;
				check_test[0] = GetTickCount();
				DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_0 %d  %d %d\n\r\n\r",check_test[0],ARRAY_SIZE(ak7739_reg) ,reg_write_cnt);
				reg_write_cnt =0;
			}
			else
			{
				ak7739_reg_write(ak7739, ak7739_reg[reg_write_cnt].reg, ak7739_reg[reg_write_cnt].def);
				reg_write_cnt++;
			}
			#else
			for ( i = 0 ; i < ARRAY_SIZE(ak7739_reg) ; i++ ) 
			{
				ak7739_reg_write(ak7739, ak7739_reg[i].reg, ak7739_reg[i].def);
			}
			ak_init_reg_state = AK_INIT_REG_1;
			check_test[0] = GetTickCount();
			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_0 %d  %d %d\n\r\n\r",check_test[0],ARRAY_SIZE(ak7739_reg) ,i);
			#endif
			break;
		case AK_INIT_REG_1:
			reg_write_cnt =0;
			ak7739_set_status(ak7739, STATUS_POWERDOWN); //20200601_set_status powerdown after set default reg
			setPLLOut(ak7739);

			for ( i = 0 ; i < AK7739_SYNCDOMAIN_NUM ; i ++ ) 
			{
				setSDXClock(ak7739, i);
				setSDXMaster(ak7739, i, 0); // All Slave
			}
			ak_init_reg_state = AK_INIT_REG_2;
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_1 %d\n\r\n\r",check_test[0]);
			break;
		case AK_INIT_REG_2:
			ak7739_reg_update_bits(ak7739, AK7739_C0_008_AHCLK_SETTING, 0x3, AK7739_AHDIV_BIT);
			ak7739_reg_write(ak7739, AK7739_C0_009_BUSCLOCK_SETTING, 1);

			if ( ak7739->BUSMclk == 1 ) 
			{
				busDev = 24;
			}
			else 
			{
				busDev = 12;
			}

			dev = AK7739_AHDIV_BIT + 1;
			switch(dev) 
			{
				case 1:
				case 2:
				case 3:
					value = busDev / dev;
					break;
				default:
					value = busDev;
					break;
			}

			value--;
			ak7739_reg_write(ak7739, AK7739_C0_00A_BUSCLOCK_SETTING2, value);
			ak_init_reg_state = AK_INIT_REG_3;
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_2 %d\n\r\n\r",check_test[0]);
			break;
		case AK_INIT_REG_3:	
			ak7739_reg_write(ak7739, AK7739_C0_041_BICK_SYNCDOMAIN_SELECT1, 0x01);		// BICK1:SD1
			ak7739_reg_write(ak7739, AK7739_C0_043_BICK_SYNCDOMAIN_SELECT2, 0x02);		// BICK2:SD2
			ak7739_reg_write(ak7739, AK7739_C0_045_BICK_SYNCDOMAIN_SELECT3, 0x03);		// BICK3:SD3
			ak7739_reg_write(ak7739, AK7739_C0_047_BICK_SYNCDOMAIN_SELECT4, 0x04);		// BICK4:SD4
			ak7739_reg_write(ak7739, AK7739_C0_049_BICK_SYNCDOMAIN_SELECT5, 0x05);		// BICK5:SD5

			ak7739_reg_write(ak7739, AK7739_C0_051_SDIN1_SYNCDOMAIN_SELECT, 0x01);		// SDIN1:BICK1
			ak7739_reg_write(ak7739, AK7739_C0_053_SDIN2_SYNCDOMAIN_SELECT, 0x02);		// SDIN2:BICK2
			ak7739_reg_write(ak7739, AK7739_C0_055_SDIN3_SYNCDOMAIN_SELECT, 0x03);		// SDIN3:BICK3
			ak7739_reg_write(ak7739, AK7739_C0_057_SDIN4_SYNCDOMAIN_SELECT, 0x04);		// SDIN4:BICK4
			ak7739_reg_write(ak7739, AK7739_C0_059_SDIN5_SYNCDOMAIN_SELECT, 0x05);		// SDIN5:BICK5
			ak7739_reg_write(ak7739, AK7739_C0_05B_SDIN6_SYNCDOMAIN_SELECT, AK7739_SDIO6_SYNC_DOMAIN);		// SDIN6

			ak7739_reg_write(ak7739, AK7739_C0_061_SDOUT1_SYNCDOMAIN_SELECT, 0x01); 	// SDOUT1:SD1
			ak7739_reg_write(ak7739, AK7739_C0_063_SDOUT2_SYNCDOMAIN_SELECT, 0x02); 	// SDOUT2:SD2
			ak7739_reg_write(ak7739, AK7739_C0_065_SDOUT3_SYNCDOMAIN_SELECT, 0x03); 	// SDOUT3:SD3
			ak7739_reg_write(ak7739, AK7739_C0_067_SDOUT4_SYNCDOMAIN_SELECT, 0x04); 	// SDOUT4:SD4
			ak7739_reg_write(ak7739, AK7739_C0_069_SDOUT5_SYNCDOMAIN_SELECT, 0x05); 	// SDOUT5:SD5
			ak7739_reg_write(ak7739, AK7739_C0_06B_SDOUT6_SYNCDOMAIN_SELECT, AK7739_SDIO6_SYNC_DOMAIN); 	// SDOUT6
			ak_init_reg_state = AK_INIT_REG_4;
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_3 %d\n\r\n\r",check_test[0]);
			break;
		case AK_INIT_REG_4:
			#ifdef AK7739_USER_CONTROL_CODE
			akdbgprt("[AK7739] %s >>===== Default Setting for Customer Use Case Start\n", __FUNCTION__);
		
			//PLL Setting
			set_pllinput(ak7739, PLLINPUT_XTI);
			set_xtifs(ak7739, XTIFS_12P288MHZ);
		
			//DSP Data Format Setting
			//DSP DRAM Setting
			ak7739_reg_write(ak7739, AK7739_C1_011_DSP1_DRAM_SETTING, 0x02);
			ak7739_reg_write(ak7739, AK7739_C1_012_DSP2_DRAM_SETTING, 0x01);
			ak7739_reg_write(ak7739, AK7739_C1_013_DSP3_DRAM_SETTING, 0x06);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_4 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_5;
			break;
		case AK_INIT_REG_5:
			#ifdef AK7739_USER_CONTROL_CODE
			//DSP DelayRAM Setting
			ak7739_reg_write(ak7739, AK7739_C1_020_DSP_DLYRAM_ASSIGNMENT, 0x01);
			ak7739_reg_write(ak7739, AK7739_C1_021_DSP1_DLYRAM_SETTING, 0x00);
			ak7739_reg_write(ak7739, AK7739_C1_022_DSP2_DLYRAM_SETTING, 0x00);
			//DSP CRAM Setting
			ak7739_reg_write(ak7739, AK7739_C1_031_DSP1_CRAM_DLP0_SETTING, 0x13);
			ak7739_reg_write(ak7739, AK7739_C1_032_DSP2_CRAM_DLP0_SETTING, 0x00);
			ak7739_reg_write(ak7739, AK7739_C1_033_DSP3_CRAM_SETTING, 0x03); 
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_5 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_6;
			break;
		case AK_INIT_REG_6:
			#ifdef AK7739_USER_CONTROL_CODE		
			//New Sync Domain Setting
			//Set the ADC, DAC, DSP12 Sync Domain Sync as SD2 for Customer(Digen AVNT) Use Case
			ak7739_reg_update_bits(ak7739, AK7739_C0_088_ADC1_SYNCDOMAIN_SELECT, 0x7, 0x01);	 // ADC1:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_089_CODEC_SYNCDOMAIN_SELECT, 0x7, 0x01); 	// CODEC:SD1
		
			ak7739_reg_update_bits(ak7739, AK7739_C0_0B0_DSP1_SYNCDOMAIN_SELECT, 0x7, 0x06);	 //DSP1:SD6
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C0_DSP1_OUTPUT_SYNCDOMAIN_SELECT1, 0x7, 0x01);	 //DSP1OUT1:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C1_DSP1_OUTPUT_SYNCDOMAIN_SELECT2, 0x7, 0x06);	 //DSP1OUT2:SD6
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C2_DSP1_OUTPUT_SYNCDOMAIN_SELECT3, 0x7, 0x06);	 //DSP1OUT3:SD6
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C3_DSP1_OUTPUT_SYNCDOMAIN_SELECT4, 0x7, 0x06);	 //DSP1OUT4:SD6
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C4_DSP1_OUTPUT_SYNCDOMAIN_SELECT5, 0x7, 0x06);	 //DSP1OUT5:SD6
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C5_DSP1_OUTPUT_SYNCDOMAIN_SELECT6, 0x7, 0x06);	 //DSP1OUT6:SD6
		
			ak7739_reg_update_bits(ak7739, AK7739_C0_0B1_DSP2_SYNCDOMAIN_SELECT, 0x7, 0x01);	 //DSP2:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C6_DSP2_OUTPUT_SYNCDOMAIN_SELECT1, 0x7, 0x01);	 //DSP2OUT1:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C7_DSP2_OUTPUT_SYNCDOMAIN_SELECT2, 0x7, 0x01);	 //DSP2OUT2:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C8_DSP2_OUTPUT_SYNCDOMAIN_SELECT3, 0x7, 0x01);	 //DSP2OUT3:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0C9_DSP2_OUTPUT_SYNCDOMAIN_SELECT4, 0x7, 0x01);	 //DSP2OUT4:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0CA_DSP2_OUTPUT_SYNCDOMAIN_SELECT5, 0x7, 0x01);	 //DSP2OUT5:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0CB_DSP2_OUTPUT_SYNCDOMAIN_SELECT6, 0x7, 0x06);	 //DSP2OUT6:SD6
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_6 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_7;
			break;
		case AK_INIT_REG_7:
			#ifdef AK7739_USER_CONTROL_CODE

			//Default Setting for every use case
			/*
			 * Clock and Data Format Requirements for Digen AVNT Use Case
			 * I2S Port1 Use Case
			 * TDM_SCK=12.28MHZ, Channel=8
			 * ENT=6Channels, 32Bit MSB Justified, Media Data
			 * ANN=1Channel, 32Bit MSB Justified, Navigation/Announcement Data
			 * TEL=1Channel, 32Bit MSB Justified, Telephone Data
			 */
			//Select SDOUT2,3,4,5 for SDOUT
			ak7739_reg_write(ak7739, AK7739_C0_200_SDOUT_OUTPUT_DATA_SELECT, 0x10);

			//Select SDOUT5 for SDOUT
			ak7739_reg_write(ak7739, AK7739_C0_204_MASTER_SPI_SELECT, 0x0F);

			//Select SDIN1,2,3,4,5 for SDIN
			ak7739_reg_write(ak7739, AK7739_C0_203_SDIN_INPUT_DATA_SELECT, 0x00);

			//Codec FS: 48kHz:48kHz
			//Sampling Frequency Setting of ADC2, DAC1, DAC2=48kHz, ADC1=48kHz)
			ak7739_reg_update_bits(ak7739, AK7739_C3_003_SYSTEM_CLOCK_SETTING, 0x1F, 0x04);

			//ADC1 Input Type: Pseudo Differential
			ak7739_reg_update_bits(ak7739, AK7739_C3_00A_ADC_ANALOG_INPUT_SETTING, 0x3C, 0x28);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_7 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_8;
			break;
		case AK_INIT_REG_8:
			#ifdef AK7739_USER_CONTROL_CODE	
			#if 0
			reg_write_cnt++;
			if(reg_write_cnt==1)
			{
				//SYNCDOMAIN1 Setting For SOC
				set_sdX_ms(ak7739, SYNCDOMAIN1, I2S_MASTER); //Master Slave Setting
			}
			else if(reg_write_cnt==2)
			{
				set_sdX_fs(ak7739, SYNCDOMAIN1, SAMPLERATE_48KHZ); //Sample Rate Setting
			}
			else if(reg_write_cnt==3)
			{
				set_sdX_bick(ak7739, SYNCDOMAIN1, BICKFS_256FS); //BICK Clock Setting
			}
			else if(reg_write_cnt==4)
			{
				set_portX_tdm(ak7739, SYNCDOMAIN1, TDM_ON); //TDM On, Off Setting
			}
			else if(reg_write_cnt==5)
			{
				set_portX_slot(ak7739, SYNCDOMAIN1, SLOTSIZE_32BIT); //PCM Data slot size setting
			}
			else if(reg_write_cnt==6)
			{
				ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT1, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_LEFT_J); //BICK, LRCK Master Slave Setting //PCM Data format setting
			}
			else if(reg_write_cnt==7)
			{
				ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT1); //PCM Data size setting
				check_test[0] = GetTickCount();
				DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_8 %d\n\r\n\r",check_test[0]);
				reg_write_cnt =0;
				ak_init_reg_state = AK_INIT_REG_9;
			}
			#else		
			//SYNCDOMAIN1 Setting For SOC
			set_sdX_ms(ak7739, SYNCDOMAIN1, I2S_MASTER); //Master Slave Setting
			set_sdX_fs(ak7739, SYNCDOMAIN1, SAMPLERATE_48KHZ); //Sample Rate Setting
			set_sdX_bick(ak7739, SYNCDOMAIN1, BICKFS_256FS); //BICK Clock Setting
			set_portX_tdm(ak7739, SYNCDOMAIN1, TDM_ON); //TDM On, Off Setting
			set_portX_slot(ak7739, SYNCDOMAIN1, SLOTSIZE_32BIT); //PCM Data slot size setting

			ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT1, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_LEFT_J); //BICK, LRCK Master Slave Setting //PCM Data format setting
			ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT1); //PCM Data size setting
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_8 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_9;
			#endif
			#endif
			break;
		case AK_INIT_REG_9:
			#ifdef AK7739_USER_CONTROL_CODE
			#if 0
			reg_write_cnt++;
			if(reg_write_cnt==1)
			{
				//SYNCDOMAIN2 Setting For RM RADIO
				set_sdX_ms(ak7739, SYNCDOMAIN2, I2S_MASTER);
			}
			else if(reg_write_cnt==2)
			{
				set_sdX_fs(ak7739, SYNCDOMAIN2, SAMPLERATE_48KHZ);
			}
			else if(reg_write_cnt==3)
			{
				set_sdX_bick(ak7739, SYNCDOMAIN2, BICKFS_64FS);
			}
			else if(reg_write_cnt==4)
			{
				set_portX_tdm(ak7739, SYNCDOMAIN2, TDM_OFF);
			}
			else if(reg_write_cnt==5)
			{
				set_portX_slot(ak7739, SYNCDOMAIN2, SLOTSIZE_32BIT);
			}
			else if(reg_write_cnt==6)
			{
				ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT2, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_I2S); //FM Radio is using I2S compatible
				ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT2);
				check_test[0] = GetTickCount();
				DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_9 %d\n\r\n\r",check_test[0]);
				ak_init_reg_state = AK_INIT_REG_10;
				reg_write_cnt=0;
			}
			
			#else
			//SYNCDOMAIN2 Setting For RM RADIO
			set_sdX_ms(ak7739, SYNCDOMAIN2, I2S_MASTER);
			set_sdX_fs(ak7739, SYNCDOMAIN2, SAMPLERATE_48KHZ);
			set_sdX_bick(ak7739, SYNCDOMAIN2, BICKFS_64FS);
			set_portX_tdm(ak7739, SYNCDOMAIN2, TDM_OFF);
			set_portX_slot(ak7739, SYNCDOMAIN2, SLOTSIZE_32BIT);

			ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT2, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_I2S); //FM Radio is using I2S compatible
			ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT2);
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_9 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_10;
			#endif
			#endif
			break;
		case AK_INIT_REG_10:
			#ifdef AK7739_USER_CONTROL_CODE
			//SYNCDOMAIN3 Setting For A2B, EXTERNAL AMP
			set_sdX_ms(ak7739, SYNCDOMAIN3, I2S_MASTER);
			set_sdX_fs(ak7739, SYNCDOMAIN3, SAMPLERATE_48KHZ);
			set_sdX_bick(ak7739, SYNCDOMAIN3, BICKFS_256FS);
			set_portX_tdm(ak7739, SYNCDOMAIN3, TDM_ON);
			set_portX_slot(ak7739, SYNCDOMAIN3, SLOTSIZE_32BIT);

			ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT3, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_DSP_A);  //for A2B Early Sync enabled
//			ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT3);
			ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S24_LE, AK7739_AIF_PORT3);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_10 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_11;
			break;
		case AK_INIT_REG_11:
			#ifdef AK7739_USER_CONTROL_CODE
			//SYNCDOMAIN4 Setting For A2B, EXTERNAL AMP
			set_sdX_ms(ak7739, SYNCDOMAIN4, I2S_MASTER);
			set_sdX_fs(ak7739, SYNCDOMAIN4, SAMPLERATE_48KHZ);
			set_sdX_bick(ak7739, SYNCDOMAIN4, BICKFS_256FS);
			set_portX_tdm(ak7739, SYNCDOMAIN4, TDM_ON);
			set_portX_slot(ak7739, SYNCDOMAIN4, SLOTSIZE_32BIT);

	 		ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT4, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_DSP_A);
			ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT4);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_11 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_12;
			break;
		case AK_INIT_REG_12:
			#ifdef AK7739_USER_CONTROL_CODE
			//SYNCDOMAIN5 Setting For EC/NR Ref. Signal
			set_sdX_ms(ak7739, SYNCDOMAIN5, I2S_MASTER);
			set_sdX_fs(ak7739, SYNCDOMAIN5, SAMPLERATE_48KHZ);
			set_sdX_bick(ak7739, SYNCDOMAIN5, BICKFS_64FS);
			set_portX_tdm(ak7739, SYNCDOMAIN5, TDM_OFF);
			set_portX_slot(ak7739, SYNCDOMAIN5, SLOTSIZE_32BIT);

			ak7739_set_dai_fmt(ak7739, AK7739_AIF_PORT5, AK7739_DAIFMT_CBM_CFM, AK7739_DAIFMT_I2S);
			ak7739_hw_params(ak7739, 48000, AK7739_PCM_FORMAT_S32_LE, AK7739_AIF_PORT5);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_12 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_13;
			break;
		case AK_INIT_REG_13:
			#ifdef AK7739_USER_CONTROL_CODE
			//SYNCDOMAIN6 Setting For DSP1 Surround Signal
			set_sdX_ms(ak7739, SYNCDOMAIN6, I2S_MASTER);
			set_sdX_fs(ak7739, SYNCDOMAIN6, SAMPLERATE_24KHZ);
			set_sdX_bick(ak7739, SYNCDOMAIN6, BICKFS_64FS);
			set_portX_tdm(ak7739, SYNCDOMAIN6, TDM_OFF);
			set_portX_slot(ak7739, SYNCDOMAIN6, SLOTSIZE_32BIT);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_13 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_14;
			break;
		case AK_INIT_REG_14:
			#ifdef AK7739_USER_CONTROL_CODE
			//Set Default Path
			ak7739_reg_update_bits(ak7739, AK7739_C0_098_MIXER1_SYNCDOMAIN_SELECT, 0x7, 0x01);	 // ADC1:SD1
			ak7739_reg_write(ak7739, AK7739_C0_118_MIXER1_CHA_INPUT_DATA_SELECT, ak7739_source_select_values[ALL0]);
			ak7739_reg_write(ak7739, AK7739_C0_119_MIXER1_CHB_INPUT_DATA_SELECT, ak7739_source_select_values[ALL0]);
			set_mixer(ak7739, MIXER_1, MIX_INPUT1, MIX_ADJ_0DB, MIX_SWAP_THROUGH);
			set_mixer(ak7739, MIXER_1, MIX_INPUT2, MIX_ADJ_0DB, MIX_SWAP_THROUGH);
			
			ak7739_reg_update_bits(ak7739, AK7739_C0_099_MIXER2_SYNCDOMAIN_SELECT, 0x7, 0x01);	 // ADC1:SD1
			ak7739_reg_write(ak7739, AK7739_C0_11A_MIXER2_CHA_INPUT_DATA_SELECT, ak7739_source_select_values[ALL0]);
			ak7739_reg_write(ak7739, AK7739_C0_11B_MIXER2_CHB_INPUT_DATA_SELECT, ak7739_source_select_values[ALL0]);
			set_mixer(ak7739, MIXER_2, MIX_INPUT1, MIX_ADJ_0DB, MIX_SWAP_THROUGH);
			set_mixer(ak7739, MIXER_2, MIX_INPUT2, MIX_ADJ_0DB, MIX_SWAP_THROUGH);

			ak7739_reg_update_bits(ak7739, AK7739_C0_0A0_SRC1_SYNCDOMAIN_SELECT, 0x7, 0x01);	//SRC1:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0A1_SRC2_SYNCDOMAIN_SELECT, 0x7, 0x03);	//SRC2:SD3
			ak7739_reg_update_bits(ak7739, AK7739_C0_0A2_SRC3_SYNCDOMAIN_SELECT, 0x7, 0x03);	//SRC3:SD3
			ak7739_reg_update_bits(ak7739, AK7739_C0_0A3_SRC4_SYNCDOMAIN_SELECT, 0x7, 0x03);	//SRC4:SD3

			ak7739_reg_update_bits(ak7739, AK7739_C0_0A4_SRC5_SYNCDOMAIN_SELECT, 0x7, 0x05);	//SRC5:SD5
			ak7739_reg_update_bits(ak7739, AK7739_C0_0A5_SRC6_SYNCDOMAIN_SELECT, 0x7, 0x05);	//SRC5:SD5
			ak7739_reg_update_bits(ak7739, AK7739_C0_0A6_SRC7_SYNCDOMAIN_SELECT, 0x7, 0x01);	//SRC5:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C0_0A7_SRC8_SYNCDOMAIN_SELECT, 0x7, 0x01);	//SRC5:SD1
			ak7739_reg_update_bits(ak7739, AK7739_C2_106_MONO_SRC_PATH_SETTING, 0xFA, 0x5A);	//SRC5,7 as stereo

			#if 1		//add voice filter	
			ak7739_reg_write(ak7739, AK7739_C2_001_SRC_FILTER_SETTING1, 0x55); //SRC1~4 for Audio Mode
			ak7739_reg_write(ak7739, AK7739_C2_102_MONO_SRC_FILTER_SETTING, 0x55); //SRC5~8 for Audio Mode
			#endif
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_14 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_15;
			break;
		case AK_INIT_REG_15:
			#ifdef AK7739_USER_CONTROL_CODE
			ak7739_reg_write(ak7739, AK7739_C0_120_SRC1_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN2A]);
			ak7739_reg_write(ak7739, AK7739_C0_121_SRC2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);
			ak7739_reg_write(ak7739, AK7739_C0_122_SRC3_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT6]);
			ak7739_reg_write(ak7739, AK7739_C0_123_SRC4_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT5]);
			ak7739_reg_write(ak7739, AK7739_C0_124_SRC5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			ak7739_reg_write(ak7739, AK7739_C0_125_SRC6_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			ak7739_reg_write(ak7739, AK7739_C0_126_SRC7_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN5A]);
			ak7739_reg_write(ak7739, AK7739_C0_127_SRC8_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN5A]);
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_15 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_16;
			break;
		case AK_INIT_REG_16:
			#ifdef AK7739_USER_CONTROL_CODE			
			//For FM SRC1 to DSP2 //SDIN2A-> SRC1-> DSP2_In1
			ak7739_reg_write(ak7739, AK7739_C0_186_DSP2_DIN1_INPUT_DATA_SELECT, ak7739_source_select_values[SRCO1]);
			//For Media to DSP2 //SDIN1A to DSP2_In2
			ak7739_reg_write(ak7739, AK7739_C0_187_DSP2_DIN2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);
			//For BCall to DSP2 //ADC1-> DSP2_In3
			ak7739_reg_write(ak7739, AK7739_C0_188_DSP2_DIN3_INPUT_DATA_SELECT, ak7739_source_select_values[AK_ADC1]);
			//For Navi, Phone to DSP2 //SDIN1D-> DSP2_In4
			ak7739_reg_write(ak7739, AK7739_C0_189_DSP2_DIN4_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
			//For Instrument(Chime) to DSP2 //SDIN1C-> DSP2_In5
			ak7739_reg_write(ak7739, AK7739_C0_18A_DSP2_DIN5_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1C]);
			//For Surround DSP2 to DSP1, DSP1 to DSP2 //DSP2_Out6 -> DSP1_In1 //DSP1_Out1 -> DSP2_In6
			ak7739_reg_write(ak7739, AK7739_C0_18B_DSP2_DIN6_INPUT_DATA_SELECT, ak7739_source_select_values[DSP1OUT1]);
			ak7739_reg_write(ak7739, AK7739_C0_180_DSP1_DIN1_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT6]);
			//For Surround Effect DSP1 connect with DSP3
			ak7739_reg_update_bits(ak7739, AK7739_C1_001_DSP_CLOCK_SETTING, 0x10, 0x00);   //DSP1 and DSP3 are connected
			#endif
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_16 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_17;
			break;
		case AK_INIT_REG_17:
			#ifdef AK7739_USER_CONTROL_CODE	
			//For EC/NR Ref DSP2OUT1 to EC/NR //SRC5-> SDOUT5
			ak7739_reg_write(ak7739, AK7739_C0_160_SDOUT5A_OUTPUT_DATA_SELECT, ak7739_source_select_values[SRCO5]);

			#ifdef EXTERNAL_AMP
			//For Media SRC2 to A2B //SDIN2A-> SRC2-> SDOUT3_A
			ak7739_reg_write(ak7739, AK7739_C0_150_SDOUT3A_OUTPUT_DATA_SELECT, ak7739_source_select_values[SRCO2]);
			//For Navi, Beep SRC3 to A2B //DSP2_out6-> SRC3-> SDOUT3_B
			ak7739_reg_write(ak7739, AK7739_C0_151_SDOUT3B_OUTPUT_DATA_SELECT, ak7739_source_select_values[SRCO3]);
			//For Phone, Chime SRC4 to A2B //DSP2_out6-> SRC4-> SDOUT3_C
			ak7739_reg_write(ak7739, AK7739_C0_152_SDOUT3C_OUTPUT_DATA_SELECT, ak7739_source_select_values[SRCO4]);
			#else
			//For Internal AMP Out //DSP2_Out1-> DAC1 //DAP2_Out2-> DAC2
			ak7739_reg_write(ak7739, AK7739_C0_108_DAC1_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT1]);
			ak7739_reg_write(ak7739, AK7739_C0_109_DAC2_INPUT_DATA_SELECT, ak7739_source_select_values[DSP2OUT2]);
			#endif //EXTERNAL_AMP
			#endif
			akdbgprt("[AK7739] %s <<===== Default Setting for Customer Use Case End\n", __FUNCTION__);
//			check_test[0] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_17 %d\n\r\n\r",check_test[0]);
			ak_init_reg_state = AK_INIT_REG_18;
			au8DspInfo[6]=r_sonysound;
			break;
		case AK_INIT_REG_18:
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP1, IMGTYPE_PRAM, RAMNUM_AUDIOEFFECT);
//			check_test[1] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_18  %d\n\r\n\r",check_test[1]);
			if (FALSE != returnMid)
			#endif
			{
				DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_18  %d\n\r\n\r",GetTickCount());
				ak_init_reg_state = AK_INIT_REG_19;
				uDspRamWriteFailCnt=0;
			}
			break;
		case AK_INIT_REG_19:
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP1, IMGTYPE_CRAM, RAMNUM_AUDIOEFFECT);
//			check_test[2]  = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_19  %d\n\r\n\r",check_test[2]);
			if (FALSE != returnMid)
			#endif
			{
				DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_19  %d\n\r\n\r",GetTickCount());
				ak_init_reg_state = AK_INIT_REG_20;
			}
			break;
		case AK_INIT_REG_20:			
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP1, IMGTYPE_ORAM, RAMNUM_AUDIOEFFECT);
//			check_test[3] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_20  %d\n\r\n\r",check_test[3]);
			if (FALSE != returnMid)
			#endif
			ak_init_reg_state = AK_INIT_REG_21;
			break;
		case AK_INIT_REG_21:		
			#ifdef AK7739_USER_CONTROL_CODE	
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP2, IMGTYPE_PRAM, RAMNUM_AUDIOEFFECT);
//			check_test[4] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_21  %d\n\r\n\r",check_test[4]);
			if (FALSE != returnMid)
			#endif
			ak_init_reg_state = AK_INIT_REG_22;
			break;
		case AK_INIT_REG_22:
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP2, IMGTYPE_CRAM, RAMNUM_AUDIOEFFECT);
//			check_test[5] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_5  %d\n\r\n\r",check_test[5]);
			if (FALSE != returnMid)
			#endif
			ak_init_reg_state = AK_INIT_REG_23;
			break;
		case AK_INIT_REG_23:
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP2, IMGTYPE_ORAM, RAMNUM_AUDIOEFFECT);
//			check_test[6] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_23  %d\n\r\n\r",check_test[6]);
			if (FALSE != returnMid)
			#endif
			ak_init_reg_state = AK_INIT_REG_24;
			break;
		case AK_INIT_REG_24:
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP3, IMGTYPE_PRAM, RAMNUM_AUDIOEFFECT);
//			check_test[7] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_24  %d\n\r\n\r",check_test[7]);
			if (FALSE != returnMid)
			#endif
			ak_init_reg_state = AK_INIT_REG_25;
			break;
		case AK_INIT_REG_25:
			#ifdef AK7739_USER_CONTROL_CODE
			returnMid = ak7739_firmware_write_ram(ak7739, DSPNUMTYPE_DSP3, IMGTYPE_CRAM, RAMNUM_AUDIOEFFECT);
//			check_test[8] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_25  %d\n\r\n\r",check_test[8]);
			if (FALSE != returnMid)
			#endif
			ak_init_reg_state = AK_INIT_REG_26;
			break;
		case AK_INIT_REG_26:
			akdbgprt_user(">> =========================== Cram API Call Start ak7739_init_reg_cust ===========================\n");
			#ifdef EXTERNAL_AMP
			set_ae_sel4selector(ak7739, AE_Out5SW4, AE_SEL4IN_2); //for Phone/Chime => EXT Amp
			set_ae_sel2selector(ak7739, AE_Out6SW2, AE_SEL2IN_2); //for Navi/Beep => EXT Amp
			#else
			if(r_sonysound)
			{
			}
			else
			{
			set_ae_sel2selector(ak7739, AE_DCCutSW2, AE_SEL2IN_1); //for DCCut ON
			set_ae_sel4selector(ak7739, AE_Out5SW4, AE_SEL4IN_1); //for Phone/Chime => EXT Amp
				set_ae_loudness(ak7739, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1); //min //flat
					//Ex) set_ae_loudness(ak7739, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16); //max
			}
			
			set_ae_sel2selector(ak7739, AE_Out6SW2, AE_SEL2IN_1); //for Navi/Beep => EXT Amp

			#if 1
			switch(e2p_save_data.E2P_EQ_TYPE )
			{	
				case 1://EQ_MODE_CUSTOM
					#ifdef NEW_APPLY_EQ
					set_ae_bassMidTreble(ak7739, e2p_save_data.E2P_EQ_BASS, e2p_save_data.E2P_EQ_MID,e2p_save_data.E2P_EQ_TREBLE); 
					#else
					set_ae_bassMidTreble(ak7739, (AE_BMT_N7-e2p_save_data.E2P_EQ_BASS), (AE_BMT_N7-e2p_save_data.E2P_EQ_MID),(AE_BMT_N7-e2p_save_data.E2P_EQ_TREBLE)); 
					#endif
					break;
				case 2://EQ_MODE_POP
					set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_N3, AE_BMT_P5);
					break;
				case 3://EQ_MODE_ROCK
					set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_N3, AE_BMT_P3);
					break;
				case 4://EQ_MODE_CLASSIC
					set_ae_bassMidTreble(ak7739, AE_BMT_P2, AE_BMT_P2, AE_BMT_N3);
					break;
				case 5://EQ_MODE_HUMAN_VOICE
					set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_P4, AE_BMT_N4); 
					break;
				case 6://EQ_MODE_JAZZ
					set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_P2, AE_BMT_N2); 
					break;
				default:
					#ifdef NEW_APPLY_EQ
					set_ae_bassMidTreble(ak7739, e2p_save_data.E2P_EQ_BASS, e2p_save_data.E2P_EQ_MID,e2p_save_data.E2P_EQ_TREBLE);
					#else
					set_ae_bassMidTreble(ak7739, (AE_BMT_N7-e2p_save_data.E2P_EQ_BASS), (AE_BMT_N7-e2p_save_data.E2P_EQ_MID),(AE_BMT_N7-e2p_save_data.E2P_EQ_TREBLE)); 
					#endif
					break;
			}
			ak_init_reg_state = AK_INIT_REG_27;
//			check_test[7] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_26  %d\n\r\n\r",check_test[7]);
			break;
		case AK_INIT_REG_27:
			set_ae_fadeNbalance(ak7739, (AE_REAR_7-e2p_save_data.E2P_EQ_FADER), e2p_save_data.E2P_EQ_BALANCE);
			#else
			set_ae_bassMidTreble(ak7739, AE_BMT_0, AE_BMT_0, AE_BMT_0); //mid //flat
			set_ae_fadeNbalance(ak7739, AE_FADER_0, AE_BALANCE_0); //mid //flat
			#endif
			
			if(r_sonysound)
			{
			}
			else
			{
				set_ae_1bandGeq(ak7739, AE_RseBmtGeq_B1, AE_GEQ_FILTER_LPF, AE_PRECISION_S, 100, 0, 1);
			}
			//Ex) set_ae_1bandGeq(ak7739, AE_RseBmtGeq_B1, AE_GEQ_FILTER_LPF, AE_PRECISION_S, 100, 0, 1);
			ak_init_reg_state = AK_INIT_REG_28;
			break;
	case AK_INIT_REG_28:
			//Ex) set_ae_outMix(ak7739, AE_OutMixFL, -145, -145, -145, -145, -145); //all mute
			set_ae_outMix(ak7739, AE_OutMixFL, 0, 0, 0, 0, 0); //all 0dB
			set_ae_outMix(ak7739, AE_OutMixFR, 0, 0, 0, 0, 0); //all 0dB
			set_ae_outMix(ak7739, AE_OutMixRL, 0, 0, -145, 0, 0); //all 0dB
			set_ae_outMix(ak7739, AE_OutMixRR, 0, 0, -145, 0, 0); //all 0dB
			#endif
//			check_test[7] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_27 %d\n\r\n\r",check_test[7]);
			ak_init_reg_state = AK_INIT_REG_29;
			break;
	case AK_INIT_REG_29:
			#if 1
			if(e2p_sys_data.E2P_AUDIO_PATH_DEFAULT == AVNT_USECASE_AUDIOPATH_MEDIA)
				set_ae_volume(ak7739, AE_MediaInVolume, AK_MEDIA_VOL_DEFAULT);
			else if(e2p_sys_data.E2P_AUDIO_PATH_DEFAULT == AVNT_USECASE_AUDIOPATH_BCALL)
				set_ae_volume(ak7739, AE_MediaInVolume, AK_BCALL_VOL_DEFAULT); 
			else 
			{
				#ifdef SEPERATE_VOLUME_TABLE
				if(mediachtype == CH_TYPE_FM)
					set_ae_volume(ak7739, AE_FMVolume, e2p_save_data.E2P_MEDIA_VOL); //max
				else if(mediachtype == CH_TYPE_AM)
					set_ae_volume(ak7739, AE_AMVolume, e2p_save_data.E2P_MEDIA_VOL); //max
				DPRINTF(DBG_MSG3,"%s  %d \n\r",__FUNCTION__,mediachtype); 
				#else
				set_ae_volume(ak7739, AE_MediaInVolume, e2p_save_data.E2P_MEDIA_VOL); //max
				#endif
			}	
			set_ae_volume(ak7739, AE_NaviVolume, 20);														// 20			e2p_save_data.E2P_NAVI_VOL
			set_ae_volume(ak7739, AE_PhoneVolume, e2p_save_data.E2P_CALL_VOL);		// 31			e2p_save_data.E2P_CALL_VOL
			ak_init_reg_state = AK_INIT_REG_30;
//			check_test[7] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust AK_INIT_REG_28  %d\n\r\n\r",check_test[7]);
			break;
		case AK_INIT_REG_30:
			if(e2p_save_data.E2P_RADAR_WARN_VOL ==VOL_LOW)
				set_ae_volume(ak7739, AE_BeepVolume, RADAR_LEVEL_LOW);
			else if(e2p_save_data.E2P_RADAR_WARN_VOL ==VOL_MIDDLE)
				set_ae_volume(ak7739, AE_BeepVolume, RADAR_LEVEL_MID);
			else
				set_ae_volume(ak7739, AE_BeepVolume, RADAR_LEVEL_HIGH);

			if(e2p_save_data.E2P_CLU_WARN_VOL ==VOL_LOW)
				set_ae_volume(ak7739, AE_ChimeVolume, INSTRUMENT_LEVEL_LOW);
			else if(e2p_save_data.E2P_CLU_WARN_VOL ==VOL_MIDDLE)
				set_ae_volume(ak7739, AE_ChimeVolume, INSTRUMENT_LEVEL_MID);
			else
				set_ae_volume(ak7739, AE_ChimeVolume, INSTRUMENT_LEVEL_HIGH);
			#endif

			if(r_sonysound)
			{
			}
			else
			{
				set_ae_volume(ak7739, AE_SVCDFLRGain, 17); // 0db
				set_ae_volume(ak7739, AE_SVCDRLRGain, 17); // 0db
				#ifdef NEW_APPLY_EQ
				set_ae_personalGeqOn(ak7739);//add
				#endif
			set_ae_sel2selector(ak7739, AE_SurroundSW2, AE_SEL2IN_1);//default   //surround off
			}
//			set_ae_beepOption(ak7739, 1000, 20, 20, 100, 200, 3); //1kHz, 20ms, 20ms, 100%, 200ms, 3		
			set_ae_beepOption(ak7739, 1000, 10, 10, 100, 180, 1); //1kHz, 10ms, 10ms, 100%,180ms, 3

			//Set Default Gain
			//Set Default ADC, DAC Gain as 0dB
			set_codec_digital_volume(ak7739, ADC1_LCH, AK7739_ADC_VOLUME_0DB);
			set_codec_digital_volume(ak7739, ADC1_RCH, AK7739_ADC_VOLUME_0DB);
			set_codec_digital_volume(ak7739, ADC2_LCH, AK7739_ADC_VOLUME_0DB);
			set_codec_digital_volume(ak7739, ADC2_RCH, AK7739_ADC_VOLUME_0DB);	
			set_codec_digital_volume(ak7739, DAC1_LCH, AK7739_DAC_VOLUME_0DB);
			set_codec_digital_volume(ak7739, DAC1_RCH, AK7739_DAC_VOLUME_0DB);
			set_codec_digital_volume(ak7739, DAC2_LCH, AK7739_DAC_VOLUME_0DB);
			set_codec_digital_volume(ak7739, DAC2_RCH, AK7739_DAC_VOLUME_0DB);

			#ifdef EXTERNAL_AMP
			set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);//NAVI  ON
			set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_OFF);//HPF ON
			set_ae_mute(ak7739, AE_BeepMute, AE_MUTE_OFF);//NAVI  ON
			set_ae_mute(ak7739, AE_ChimeMute, AE_MUTE_OFF);//HPF ON
			#else
			set_ae_mute(ak7739, AE_OutMuteFLR, AE_MUTE_OFF);//Out Front LR
			set_ae_mute(ak7739, AE_OutMuteRLR, AE_MUTE_OFF);//Out Rear LR
			set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);//NAVI  ON
			set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_OFF);//HPF ON
			set_ae_mute(ak7739, AE_BeepMute, AE_MUTE_OFF);//NAVI  ON
			set_ae_mute(ak7739, AE_ChimeMute, AE_MUTE_OFF);//HPF ON
			#endif
			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust volume  M:%d CALL:%d R:%d  CLU:%d \n\r"
				,e2p_save_data.E2P_MEDIA_VOL,e2p_save_data.E2P_CALL_VOL,e2p_save_data.E2P_RADAR_WARN_VOL,e2p_save_data.E2P_CLU_WARN_VOL); 

			akdbgprt("[AK7739] %s <=============== Cram API Call End ak7739_init_reg_cust\n", __FUNCTION__);
			check_test[9] = GetTickCount();
//			DPRINTF(DBG_MSG3,"ak7739_init_reg_cust check time  %d %d %d  %d %d %d  %d %d %d %d  \n\r"
//				,check_test[0],check_test[1],check_test[2],check_test[3],check_test[4],check_test[5],check_test[6],check_test[7],check_test[8],check_test[9]); 
			returnEnd = TRUE;	
			ak_init_reg_state = AK_INIT_REG_0;
			break;
	}
	au8DspInfo[5]=ak_init_reg_state; //0x13 AK_INIT_REG_19
	return (returnEnd);
}

int ak7739_init_reg(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int i;
//	int devid;
	int dev, value;
	int busDev;
//	int ret;
	
	akdbgprt("[AK7739] %s <===============  Cram API Call End ak7739_init_reg \n", __FUNCTION__);
	return 0;
}

int ak7739_power_on(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int ret = 0;
	int i;

	akdbgprt("[AK7739] %s >=============== Start\n", __FUNCTION__);
	#ifdef RECOVER_DSP_RAM_WRITE_FAIL
	uDspRamWriteFailCnt=0;
	#endif

	ak7739->pdn_gpio = AK7739_PDN_GPIO;

	if( ak7739->pdn_gpio <= 0){
		akdbgprt_err("\t%s pdn need to be defined pdn_gpio[%d]\n", __FUNCTION__, ak7739->pdn_gpio);
		ret = AK7739_ERROR;
		return ret;
	}

	ak7739->fs = 48000;
	ak7739->PLLInput = PLLINPUT_XTI;
	ak7739->XtiFs    = XTIFS_12P288MHZ;
	ak7739->BUSMclk  = AK7739_BUSCLOCK_NO;  // 1:24.576MHz,  0:12.288MHz

	for ( i = 0 ; i < AK7739_SYNCDOMAIN_NUM ; i ++ ) {
		ak7739->SDBick[i]  = 0; //64fs
		ak7739->SDfs[i] = 5;  // 48kHz
		ak7739->SDCks[i] = 1;  // AHCLK
		if ( i < 5 ) {
			ak7739->Master[i] = 0; // Slave
		}
		else {
			ak7739->Master[i] = 1; // Master
		}
	}

	for ( i = 0 ; i < AK7739_SDATA_NUM ; i ++ ) {
		ak7739->TDMMode[i] = 0;
		ak7739->DIEDGEbit[i] = 0;
		ak7739->DOEDGEbit[i] = 0;
		ak7739->DxSLbit[i] = 0;
	}

	ak7739->masterSPI = 1;
	ak7739->selDSP3 = 1; //For Surround Effect DSP1 connect with DSP3
	for ( i = 0 ; i < 3 ; i++ ) {
		ak7739->MIRNo[i] = 0;
	}

	ak7739->micgianL = MIC_GAIN_0DB;
	ak7739->micgianR = MIC_GAIN_0DB;
	ak7739->adc1DigitalL = 48; //0x30 0dB
	ak7739->adc1DigitalR = 48; //0x30 0dB
	ak7739->adc2DigitalL = 48; //0x30 0dB
	ak7739->adc2DigitalR = 48; //0x30 0dB
	ak7739->dac1DigitalL = 24; //0x18 0dB
	ak7739->dac1DigitalR = 24; //0x18 0dB
	ak7739->dac2DigitalL = 24; //0x18 0dB
	ak7739->dac2DigitalR = 24; //0x18 0dB

	//Cram Interface
	//Switch Off 0, On 1
	ak7739->DCCutSwitch = 0;
	ak7739->DeempSwitch = 0;
	ak7739->CompressorSwitch = 0;
	ak7739->BMTSwitch = 0;
	ak7739->SoundModeSwitch = 0;
	ak7739->LoudnessSwitch = 0;
	ak7739->SurroundSwitch = 0;
	ak7739->BypassSwitch = 0;
	ak7739->NaviGEQSwitch = 0;
	ak7739->PhoneGEQSwitch = 0;
	ak7739->RESDCCutSwitch = 0;
	ak7739->RESDeempSwitch = 0;
	ak7739->RESGEQSwitch = 0;
	ak7739->DebugSwitch = 0;

	//Selector to 1, 2, 3, 4
	ak7739->MediaInputSelector = 1;
	ak7739->SoundModeSelector = 1;
	ak7739->DebugInput = 1;
	ak7739->DebugAfterFunction = 1;
	ak7739->DebugAfterEffect = 1;
	ak7739->DebugOutput = 1;
	ak7739->DebugSelector = 1;

//	akdbgprt("[AK7739] %s call ak7739_init_reg\n", __FUNCTION__);
//remove		ak7739_init_reg(ak7739);

//remove		ak7739_set_status(ak7739, STATUS_STANDBY);

	akdbgprt("[AK7739] %s <=============== End\n", __FUNCTION__);
	return ret;
}

int ak7739_power_down(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;

	akdbgprt("[AK7739] %s(%d)\n",__FUNCTION__,__LINE__);

	ak7739_set_status(ak7739, STATUS_POWERDOWN);

	if ( ak7739->pdn_gpio > 0 ) {
		gpio_set_value(ak7739->pdn_gpio, 0);
		msleep(1);
	}

	return 0;
}

int ak7739_suspend(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;

	ak7739_set_status(ak7739, STATUS_SUSPEND);

	if ( ak7739->pdn_gpio > 0 ) 
	{
		gpio_set_value(ak7739->pdn_gpio, 0);
		msleep(1);
	}

	return 0;
}

int ak7739_resume(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;

	ak7739_init_reg(ak7739);

	ak7739_set_status(ak7739, STATUS_STANDBY);

	return 0;
}

int check_ak7739_reg(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	int    i, value;
	int	   regs, rege;

	regs = 0x000; rege = 0x003;
	for ( i = regs ; i <= rege ; i++ )
	{
		if (!ak7739_readable(i)) continue;
		value = ak7739_reg_read(ak7739, i);
		DPRINTF(DBG_INFO,"check_ak7739_reg W:0x%02X,0x%04X,0x%02X\n", (0xC0 + ((0xF000 & i)>>12)), (0xFFF & i), value);
		if(value==0)
			return 1;
	}

	return 0;
}

//media bypass
int ak7739_test(ak7739_priv *pData)
{
	
	ak7739_priv *ak7739 = pData;

	#ifdef EXTERNAL_AMP
	ak7739_reg_write(ak7739, AK7739_C0_187_DSP2_DIN2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);
	set_ae_sel4selector(ak7739, AE_MediaInputSW4, AE_SEL4IN_2); //FM Select
	#else
	ak7739_reg_write(ak7739, AK7739_C0_108_DAC1_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);
	ak7739_reg_write(ak7739, AK7739_C0_109_DAC2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1A]);
	#endif
}

// radio bypass
int ak7739_test1(ak7739_priv *pData)
{
	
	ak7739_priv *ak7739 = pData;

	#ifdef EXTERNAL_AMP
	ak7739_reg_write(ak7739, AK7739_C0_187_DSP2_DIN2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN2A]);
	set_ae_sel4selector(ak7739, AE_MediaInputSW4, AE_SEL4IN_2); //Radio select
	#else
	ak7739_reg_write(ak7739, AK7739_C0_108_DAC1_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN2A]);
	ak7739_reg_write(ak7739, AK7739_C0_109_DAC2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN2A]);
	#endif
}

// phone bypass
int ak7739_test2(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;

	#ifdef EXTERNAL_AMP
	ak7739_reg_write(ak7739, AK7739_C0_187_DSP2_DIN2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
	set_ae_sel4selector(ak7739, AE_MediaInputSW4, AE_SEL4IN_2); //phone select
	#else
	ak7739_reg_write(ak7739, AK7739_C0_108_DAC1_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
	ak7739_reg_write(ak7739, AK7739_C0_109_DAC2_INPUT_DATA_SELECT, ak7739_source_select_values[SDIN1D]);
	#endif
}

int ak7739_test3(ak7739_priv *pData)
{
	ak7739_priv *ak7739 = pData;
	static int kkk=20;

	set_ae_volume(ak7739, AE_SVCDFLRGain, kkk); //0db  //process for bt call
	set_ae_volume(ak7739, AE_SVCDRLRGain, kkk); //0db  //process for bt call
	DPRINTF(DBG_MSG3," ak7739_test3 %d\n\r",kkk);
	kkk+=1;
	if(kkk>32)
		kkk=20;
}

