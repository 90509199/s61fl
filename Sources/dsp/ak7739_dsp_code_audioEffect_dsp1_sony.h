/*
 * ak7739_dsp_code_audioEffect_dsp1.h  --  audio driver for ak7739
 *
 */

#ifndef _AK7739_DSP_CODE_AUDIOEFFECT_DSP1_SONY_H
#define _AK7739_DSP_CODE_AUDIOEFFECT_DSP1_SONY_H

#if 0
#define _AK77XX_DSP_PRAM_DSP1_WRITE_SIZE	33
#define _AK77XX_DSP_CRAM_DSP1_WRITE_SIZE	3
#define _AK77XX_DSP_OFFREG_DSP1_WRITE_SIZE	3
#endif

// PRAM file for DSP1
// PRAM download command = 0xB8
static const unsigned char ak7739_pram_dsp1_audioEffect_sony[]= {
    0xB8, 0x00, 0x00, // command code and address
	0x0C, 0x94, 0xED, 0x1E, 0x74,
	0x04, 0xA9, 0x1C, 0x6D, 0x55,
	0x00, 0x44, 0x02, 0x11, 0x99,
	0x09, 0x58, 0x6F, 0x4D, 0xD0,
	0x01, 0x5A, 0x7E, 0xC9, 0x38,
	0x06, 0x35, 0x33, 0x01, 0x8C
};

// CRAM file for DSP1
// CRAM download command = 0xB4
static const unsigned char ak7739_cram_dsp1_audioEffect_sony[]= {
    0xB4, 0x00, 0x00, // command code and address
};

// OFFREG file for DSP1
// OFFREG download command = 0xB2
static const unsigned char ak7739_offram_dsp1_audioEffect_sony[]= {
	0xB2, 0x00, 0x00, 		// command code and address  
};

#endif
