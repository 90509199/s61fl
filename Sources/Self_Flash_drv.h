

#ifndef SELF_FLASH_DRV_H_
#define SELF_FLASH_DRV_H_

#define		APP_ERASED					(0xCD55CD55u)
#define		APP_UPGRADE1							(0xAA04CD55u)
#define		APP_UPGRADE2							(0xAA040002u)
#define		APP_OK							(0xAA04AA04u)
#define		BOOT_MODE					(0x98009800u)
#define		APP_MODE						(0x76017601u)
#define		BOOT_MARK_PREV		(0x1849A5A5u)

#define 	BOOT_STAT_DEF_VAL			((BOOT_MODE << 32) | (APP_ERASED))

typedef struct
{
	uint32_t AppStatus;
	uint32_t BootSectorNum;
}SECTOR_STAT_T;

typedef struct
{
	uint32_t WaterMark;
	uint8_t UartNum;
}BOOT_PARAM_T;

//#define BootParam						((BOOT_PARAM_T *)FLEX_RAM_START_ADDR)

u32 SelfFlashInit(void);
//u32 SelfFlashBlockErase(u32 EraseBlock, u8 BootSector);
//u32 SelfFlashProgram(u32 Dest, u8 *pSrc, u32 length, u8 BootSector);
status_t SelfFlashBlockErase(u32 addr);
status_t SelfFlashProgram(u32 Dest, u8 *pSrc, u32 lenByte);

void BootStatusFlashBlock_init(void);
void BootStatusFlashBlock_getStatus(SECTOR_STAT_T *pGet_Stat);
status_t BootStatusFlashBlock_writeStatus(SECTOR_STAT_T *pWrite_Stat);
status_t BootStatusFlashBlock_reset(SECTOR_STAT_T *pWrite_Stat);
status_t BootStatusFlashBlock_checkBlock(void);

#endif

