/*
 * crc.h
 *
 *  Created on: 2018. 8. 22.
 *      Author: Digen
 */

#ifndef CRC_H_
#define CRC_H_


/**************************************************************************************************/
/*
 *
1)CCITT
Checksum len:16bit
계산식: X^16+X^12+X^5+1
제수 :0x1021
나머지 초기값:0xffff
최종 XOR값: 0x0000

2)CRC16
Checksum len:16bit
계산식: X^16+X^12+X^2+1
제수 :0x8005
나머지 초기값:0x0000
최종 XOR값: 0x0000

3)CRC32
Checksum len:32bit
계산식: X^32+X^26+X^23+ X^22+X^16+ X^12+X^11+ X^10+
X^8+: X^7+X^5+X^4+ X^2+ X^1+1
제수 :0x04C11DE7
나머지 초기값:0xffffffff
최종 XOR값:0xffffffff
 *
 *
 */
/**************************************************************************************************/

#define POLYNOMIAL				0x1021
#define INITIAL_REMAINDER		0xFFFF
#define FINAL_XOR_VALUE			0x0000

typedef unsigned short width;

#define WIDTH 	(8*sizeof(width))
#define TOPBIT (1<<(WIDTH -1))



#endif /* CRC_H_ */
