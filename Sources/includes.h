/*
 * include.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef INCLUDE_H_
#define INCLUDE_H_

#include "Cpu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#include "S32K144.h"
#include "config.h"
#include "data_def.h"
#include "debug.h"
#include "boot_def.h"

//init
#include "adc_init.h"
#include "can_init.h"
#include "sys_init.h"
#include "sysclk_init.h"
#include "uart_init.h"
#include "siu_init.h"
#include "nvic_init.h"
#include "spi_init.h"
#include "i2c_init.h"
#include "flexio_i2c_init.h"
#include "pwm_init.h"

//can Init
//#include "il_inc.h"
//#include "can_cfg.h"
//#include "can_par.h"
//#include "CanNm_Cfg.h"
//#include "std_types.h"
//#include "Platform_Types.h"
//#include "nmcbdwrp_cfg.h"
//#include "Compiler_Cfg.h"

#include "il_inc.h"
#include "can_cfg.h"
#include "can_par.h"
#include "Compiler.h"
#include "Platform_Types.h"
#include "CanNm_Cfg.h"
#include "Nm.h"
#include "CanNm.h"
#include "Std_types.h"
#include "Platform_Types.h"
#include "ccl_inc.h"

//sys
#include "isr.h"
#include "shelldrv.h"
#include "tick.h"
#include "que.h"
#include "timerdrv.h"

//func
#include "uart_func.h"
#include "power.h"
#include "gpio_func.h"
#include "timer_func.h"

//src
#include "common.h"
#include "can_comm.h"
#include "uart_comm.h"
#include "system_proc.h"
#include "change_mode.h"
#include "shellcmd.h"
#include "cust.h"
#include "test_mode.h"
#include "diag_proc.h"
#include "beep_proc.h"
#include "key_app.h"
#include "key_drv.h"
#include "TEF6686.h"
#include "service_proc.h"

#include "AMP_TB2952.h"
#include "encoder.h"
#include "flash_proc.h"
#include "Self_Flash_drv.h"

#include "dsp/ak7739.h"
#include "dsp/ak7739_audio_effect.h"
#include "dsp/ak7739_audio_effect_addr.h"
#include "dsp/ak7739_dsp_code_audioEffect_dsp1.h"
#include "dsp/ak7739_dsp_code_audioEffect_dsp2.h"
#include "dsp/ak7739_dsp_code_audioEffect_dsp3.h"
#include "dsp/ak7739_audio_effect_addr_sony.h"
#include "dsp/ak7739_dsp_code_audioEffect_dsp1_sony.h"
#include "dsp/ak7739_dsp_code_audioEffect_dsp2_sony.h"
#include "dsp/ak7739_dsp_code_audioEffect_dsp3_sony.h"

#include "dsp/ak7739_dsp_code_basic.h"
#include "ak7739_proc.h"




#include "A2B_I2C_Commandlist.h"



#endif /* INCLUDE_H_ */
