

#ifndef _BOOTDEF_H_
#define _BOOTDEF_H_


/*---------------------------*/
/* Rewrite information macro */
/*---------------------------*/
#define		FLASH_BLOCK_SIZE			( FEATURE_FLS_PF_BLOCK_SECTOR_SIZE )				//	4096 BYTE

#define		MICOM_FLASH_SIZE			( FEATURE_FLS_PF_BLOCK_SIZE )							//	512 KB
#define		BOOT_SECTOR_SIZE			( ( DWORD )( 0x0000F000u ) )									//	60 KB
#define		BOOT_STATUS_SECTOR_SIZE		( FLASH_BLOCK_SIZE )									//	4 KB
#define		APP_SECTOR_SIZE			( ( DWORD )( 0x00070000u ) )									//	448 KB


#define		BOOT_ROM_START_ADDR			( 0x00000000u )
#define		BOOT_STATUS_START_ADDR		( BOOT_ROM_START_ADDR + BOOT_SECTOR_SIZE )						//	60 KB
#define		APP_ROM_START_ADDR		( BOOT_STATUS_START_ADDR + BOOT_STATUS_SECTOR_SIZE)				//	64KB


#define		APP_START_ADDR				APP_ROM_START_ADDR		//	64 KB

//#define		BOOT_EXE_RAM_ADDR			( 0x20000000u )
#define		BOOT_EXE_RAM_ADDR			( 0x1FFFFC00u )

/* SRAM_L + SRAM_U 이지만 Preboot 영역이 사용하고 있는 RAM 영역은 손대면 안된다. */
#define		RAM_SIZE					( 0x00003C00u )									//	15 KB


#define		FLEX_RAM_START_ADDR			( 0x14000000u )




#endif

