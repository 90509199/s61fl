/*******************************************************************************
Copyright (c) 2020 - Analog Devices Inc. All Rights Reserved.
This software is proprietary & confidential to Analog Devices, Inc.
and its licensors.
******************************************************************************
* @brief: This file contains I2C command sequence to be followed for discovery 
*         and configuration of A2B nodes for an A2B schematic
* @version: $Revision$
* @date: Thursday, July 23, 2020-4:16:34 PM
* I2C Command File Version - 1.0.0
* A2B DLL version- 19.3.0
* A2B Stack DLL version- 19.3.0.0
* SigmaStudio version- 4.05.000.1779
* Developed by: Automotive Software and Systems team, Bangalore, India
* THIS IS A SIGMASTUDIO GENERATED FILE
*****************************************************************************/

/*! \addtogroup ADI_A2B_DISCOVERY_CONFIG ADI_A2B_DISCOVERY_CONFIG 
* @{
*/
#ifndef _ADI_A2B_I2C_LIST_H_ 
#define _ADI_A2B_I2C_LIST_H_ 

/*! \struct ADI_A2B_DISCOVERY_CONFIG 
A2B discovery config unit structure 
*/
extern unsigned char B9_ISR_Flag;

typedef struct 
 { 
/*!  Device address */
	unsigned char nDeviceAddr;

/*!  Operation code */
	unsigned char eOpCode;

/*! Reg Sub address */
	unsigned char nRegAddr;

/*! Config Data */
	unsigned char nData;


} ADI_A2B_DISCOVERY_CONFIG;

enum A2B_STATE{
	A2B_IDLE=0,
	A2B_INIT1,
	A2B_INIT2,
	A2B_ACTIVE,
};

#define WRITE   ((unsigned char) 0x00u)
#define READ    ((unsigned char) 0x01u)
#define DELAY   ((unsigned char) 0x02u)
#define INVALID ((unsigned char) 0xffu)

#define WR_ERR  1

extern uint8_t A2B_ISR_Flag;

extern flexio_device_state_t flexIODeviceState;
extern flexio_i2c_master_state_t i2cMasterState;

#define HDCNTERR         0x00
#define DDERR            0x01
#define CRCERR           0x02
#define DPERR            0x03
#define BECOVF           0x04
#define SRFERR           0x05
#define SRFCRCERR        0x06
#define PWRERR_9         0x09
#define PWRERR_10        0x0a
#define PWRERR_11        0x0b
#define PWRERR_12        0x0c
#define PWRERR_13        0x0d
#define PWRERR_14        0x0e
#define PWRERR_15        0x0f
#define IO0PND_0         0x10
#define IO0PND_1         0x11
#define IO0PND_2         0x12
#define IO0PND_3         0x13
#define IO0PND_4         0x14
#define IO0PND_5         0x15
#define IO0PND_6         0x16
#define IO0PND_7         0x17

#define I2CERR           0x19
#define ICRCERR          0x1a

#define PWRERR_41        0x29
#define PWRERR_42        0x2a
#define MAILBOX0_FULL    0x30
#define MAILBOX0_EMPTY   0x31
#define MAILBOX1_FULL    0x32
#define MAILBOX1_EMPTY   0x33
#define MESSAGING_ERROR  0x80
#define STARTUP_ERROR    0xfc
#define INTTYPE_READ_ERROR       0xfd
#define MASTER_RUNNING   0xff
#define DSCDONE          0x18

#define MASTER_AD2428_ADDR      0x68
#define SLAVE_AMP_ADDR          0x69

#define NODE_REG             0x29

#define INT_STATUS_REG         0x15
#define INT_SOURCE_REG         0x16
#define INT_TYPE_REG           0x17
#define INT_PENDING0_REG 	   0x18
#define INT_PENDING1_REG       0x19
#define INT_PENDING2_REG       0x1A
#define NODE_REG               0x29
#define RAISE_INT_REG          0x54
#endif /* _ADI_A2B_I2C_LIST_H_ */

uint8_t A2B_Write(DEVICE *dev,uint8_t device_addr,uint8_t register_address,uint8_t data);
uint8_t A2B_Read(DEVICE *dev,uint8_t device_addr,uint8_t register_address,uint8_t count);
void A2B_IRQ_Set();
void A2B_proc(DEVICE *dev);
uint8 A2B_Delay(DEVICE *dev,uint32 delay);
void A2B_Init_Data(DEVICE *dev);
void A2B_Sleep_Set(DEVICE *dev);
void A2B_Power_ON_Set(DEVICE *dev);
void A2B_Power_OFF_Set(DEVICE *dev);


