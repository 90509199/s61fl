/*
 * common.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#include "includes.h"

uint8 DriverDoorOpen(void)
{
	uint8 ret=0;


	ret = IlGetRxBCM_DoorAjarSts_FL();

	return ret; 
}

uint8 KeyFortification(void)
{
	uint8 ret=0;

	ret = IlGetRxBCM_KeySts();
	//ret = CanIfGet_gCanIf_CEM_BCM_2_AlarmMode();

	
	return (ret>1u);
}

uint8 R_Gear_Operate(void)
{
	uint8 ret=0;

	if(0x02==CanIfGet_gCanIf_CGW_TCU_G_GBPositoionDisplay())
		ret =1;
	else
		ret =0;

	return ret; 
}

uint8 Set_LCD_Brightness(uint8 data)
{
	uint8 ret =0;

//JANG_211203
	switch(data)
	{
		case 0x0:
		case 0x01:
			ret = LCD_BL_STEP1;
			break;
		case 0x02:
			ret = LCD_BL_STEP2;
			break;
		case 0x03:
			ret = LCD_BL_STEP3;
			break;
		case 0x04:
			ret = LCD_BL_STEP4;
			break;
		case 0x05:
			ret = LCD_BL_STEP5;
			break;
		case 0x06:
			ret = LCD_BL_STEP6;
			break;
		case 0x07:
			ret = LCD_BL_STEP7;
			break;
		case 0x08:
			ret = LCD_BL_STEP8;
			break;
		case 0x09:
			ret = LCD_BL_STEP9; //50->5
			break;
		case 0x0A:
			ret = LCD_BL_STEP10;
			break;
		default:
			ret = LCD_BL_DEFAULT;
			break;
	}
	
	return ret; 
}

//************** Glover Variable *******************************/


