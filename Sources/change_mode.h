/*
 * change_mode.h
 *
 *  Created on: 2018. 8. 27.
 *      Author: Digen
 */

#ifndef CHANGE_MODE_H_
#define CHANGE_MODE_H_

void Set_RUNmode (uint8_t value);
void Set_HSRUNmode (uint8_t value);
void Set_VLPRUNmode (uint8_t value);
void Go_To_Sleep (void);

#endif /* CHANGE_MODE_H_ */
