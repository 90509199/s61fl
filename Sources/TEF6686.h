/*
 * TEF6686.h
 *
 *  Created on: 2018. 10. 19.
 *      Author: Digen
 */

#ifndef TEF6686_DEV_H_
#define TEF6686_DEV_H_

//radio spec
#define TUNE_GET_TIME_STAMP						(350)					//(70)			//(32-1)  32ms -> apply for 35ms			

#define TUNE_GET_FM_LEVEL							(50)					// 40 -> 80 -> 100   //	20dB		->18db
#define TUNE_GET_FM_USN								(250)					//	25%이하 ->25%
#define TUNE_GET_FM_WAN								(250)					//	25%이하 ->25%
#define TUNE_GET_FM_OFFSET							(100)					// 10khz

#define TUNE_GET_AM_LEVEL							(200)//(400)					//	40dB		
#define TUNE_GET_AM_USN								(250)					//	25%이하 
#define TUNE_GET_AM_OFFSET						(10)						// 1khz

#define TUNE_DETECT_MINUS							0x1FFF			//0x8000

#define CHECK_AST_COUNTER							5			//3->5

//frequency  range
//AM 531~1630Khz 9khz step
//FM 87.50~108.25Mhz 100Khz step
#define AM_MIN_FREQ					(531)						//	(522)
#define AM_MAX_FREQ					(1620)						//	(1629)
#define FM_MIN_FREQ					(8750)
#define FM_MAX_FREQ					(10800)						//(10825)



//#define FM_LOW_VALID			(FM_MIN_FREQ<=FM_LOW_VALID) ? 0: 1
//#define FM_HIGH_VALID		(FM_HIGH_VALID<=FM_MAX_FREQ) ? 0: 1
//#define AM_LOW_VALID			(AM_MIN_FREQ<=FM_LOW_VALID) ? 0: 1
//#define AM_HIGH_VALID		(AM_HIGH_VALID<=AM_MAX_FREQ) ? 0: 1



#define TUNE_REF_CLOCK										0x400401
#define TUNE_ACTIVE												0x400501

#define TUNE_OPERATION_STATUS						0x408001
#define TUNE_IDENTIFICATION							0x408201

#define TUNE_FM_QUALITY_STATUS					0x208001
#define TUNE_FM_QUALITY_DATA						0x208101

#define FM_TUNE_TO												0x200101
#define FM_SET_CH_EQ											0x201601	
#define FM_SET_MPH_SUPPRESSION					0x201401	

#define FM_SET_SOFTMUTE_TIME						0x202801	
#define FM_SET_SOFTMUTE_LEVEL						0x202A01	
#define FM_SET_SOFTMUTE_NOISE						0x202B01
#define FM_SET_SOFTMUTE_MPH							0x202C01

#define FM_SET_HIGHCUT_MOD							0x203301	
#define FM_SET_HIGHCUT_LEVEL							0x203401	
#define FM_SET_HIGHCUT_TIME							0x203201	
#define FM_SET_HIGHCUT_MPH							0x203601	
#define FM_SET_HIGHCUT_NOISE						0x203501
#define FM_SET_HIGHCUT_MAX							0x203701
#define FM_SET_HIGHCUT_MIN							0x203801
#define FM_SET_HIGHCUT_OPTIONS					0x203B01

#define FM_SET_LOWCUT_MAX								0x203901	

#define FM_SET_STEREO_TIME								0x203C01
#define FM_SET_STEREO_MOD								0x203D01
#define FM_SET_STEREO_LEVEL							0x203E01
#define FM_SET_STEREO_MPH								0x204001	
#define FM_SET_STEREO_NOISE							0x203F01	
#define FM_SET_STEREO_MIN								0x204201	
#define FM_SET_STHIBLEND_MPH						0x204A01
#define FM_SET_STHIBLEND_NOISE						0x204901

#define FM_SET_MONO_IMPROVE						0x202301
#define FM_SET_BANDWIDTH								0x200A01
#define FM_SET_TUNE_OPTIONS							0x200201
#define FM_SET_DEEMPHASIS								0x201F01
#define FM_SET_STEREOIMPROVEMENT				0x202001
#define FM_SET_LEVELOFFSET								0x202701
#define FM_SET_NOISEBLANKER							0x201701

#define FM_SET_RDS												0x205101
#define FM_SET_RFAGC											0x200B01
#define FM_GET_AGC												0x208401	

#define AM_TUNE_TO												0x210101
#define TUNE_AM_QUALITY_STATUS					0x218001
#define TUNE_AM_QUALITY_DATA						0x218101
#define AM_COCH_DET											0x210E01

#define AM_SET_HIGHCUT_LEVEL							0x213401	
#define AM_SET_BANDWIDTH								0x210A01	
#define AM_SET_SOFTMUTE_MOD						0x212901
#define AM_SET_SOFTMUTE_LEVEL						0x212A01
#define AM_SET_SOFTMUTE_MAX							0x212D01
#define AM_SET_LEVELOFFSET								0x212701


#define AUDIO_SET_DIG_IO									0x301601
#define AUDIO_SET_MUTE										0x300B01
#define AUDIO_SET_VOL										0x300A01
#define AUDIO_SET_OUTPUT_SOURCE					0x300D01
#define APPL_SET_GPIO										0x400301

//init setting radio value 
#define INIT_FM_SET_CH_EQ											(0x01)
#define INIT_FM_SET_MPH_SUPPRESSION						(0x01)

#define INIT_FM_SET_SOFTMUTE_TMIE_1						120
#define INIT_FM_SET_SOFTMUTE_TMIE_2						500
#define INIT_FM_SET_SOFTMUTE_TMIE_3						10
#define INIT_FM_SET_SOFTMUTE_TMIE_4						20

#define INIT_FM_SET_SOFTMUTE_MPH_1						0
#define INIT_FM_SET_SOFTMUTE_MPH_2						200
#define INIT_FM_SET_SOFTMUTE_MPH_3						1000
#define INIT_FM_SET_SOFTMUTE_MPH_4						0
#define INIT_FM_SET_SOFTMUTE_MPH_5						60

#define INIT_FM_SET_SOFTMUTE_NOISE_1					0
#define INIT_FM_SET_SOFTMUTE_NOISE_2					200
#define INIT_FM_SET_SOFTMUTE_NOISE_3					1000
#define INIT_FM_SET_SOFTMUTE_NOISE_4					0
#define INIT_FM_SET_SOFTMUTE_NOISE_5					60

#if 0
#define INIT_FM_SET_HIGHCUT_MOD_1							0
#define INIT_FM_SET_HIGHCUT_MOD_2							250
#define INIT_FM_SET_HIGHCUT_MOD_3							130
#define INIT_FM_SET_HIGHCUT_MOD_4							500
#else
#define INIT_FM_SET_HIGHCUT_MOD_1							1
#define INIT_FM_SET_HIGHCUT_MOD_2							250
#define INIT_FM_SET_HIGHCUT_MOD_3							130
#define INIT_FM_SET_HIGHCUT_MOD_4							500
#endif

#if 0
#define INIT_FM_SET_HIGHCUT_LEVEL_1						3 //0
#define INIT_FM_SET_HIGHCUT_LEVEL_2						360
#define INIT_FM_SET_HIGHCUT_LEVEL_3						300
#else
#define INIT_FM_SET_HIGHCUT_LEVEL_1						3			// 3
#define INIT_FM_SET_HIGHCUT_LEVEL_2						440//440		// 360
#define INIT_FM_SET_HIGHCUT_LEVEL_3						220	//260		//300
#endif

#define INIT_FM_SET_HIGHCUT_TIME_1							1000//200
#define INIT_FM_SET_HIGHCUT_TIME_2							8000//2000
#define INIT_FM_SET_HIGHCUT_TIME_3							20//10
#define INIT_FM_SET_HIGHCUT_TIME_4							80

#if 0
#define INIT_FM_SET_HIGHCUT_MPH_1							3//0
#define INIT_FM_SET_HIGHCUT_MPH_2							300//120
#define INIT_FM_SET_HIGHCUT_MPH_3							250//160

#define INIT_FM_SET_HIGHCUT_NOISE_1						3 //0
#define INIT_FM_SET_HIGHCUT_NOISE_2						240//150
#define INIT_FM_SET_HIGHCUT_NOISE_3						110//200
#else
#define INIT_FM_SET_HIGHCUT_MPH_1							3//0
#define INIT_FM_SET_HIGHCUT_MPH_2							260//220//120
#define INIT_FM_SET_HIGHCUT_MPH_3							240//250//160

#define INIT_FM_SET_HIGHCUT_NOISE_1						3 //0
#define INIT_FM_SET_HIGHCUT_NOISE_2						230//220//150
#define INIT_FM_SET_HIGHCUT_NOISE_3						100//80//200
#endif

#define INIT_FM_SET_HIGHCUT_MAX_1							1
#if 0
#define INIT_FM_SET_HIGHCUT_MAX_2							2000//2400
#else
#define INIT_FM_SET_HIGHCUT_MAX_2							1500 //2000
#endif

#define INIT_FM_SET_LOWCUT_MAX_1							1
#define INIT_FM_SET_LOWCUT_MAX_2							100

#define INIT_FM_SET_STEREO_TIME_1							1000		//200
#define INIT_FM_SET_STEREO_TIME_2							8000		//4000
#define INIT_FM_SET_STEREO_TIME_3							300		//20
#define INIT_FM_SET_STEREO_TIME_4							800		//80

#if 0
#define INIT_FM_SET_STEREO_MPH_1							3
#define INIT_FM_SET_STEREO_MPH_2							100
#define INIT_FM_SET_STEREO_MPH_3							150
#else
#define INIT_FM_SET_STEREO_MPH_1							3
#define INIT_FM_SET_STEREO_MPH_2							240
#define INIT_FM_SET_STEREO_MPH_3							200
#endif

#define INIT_FM_SET_STEREO_NOISE_1						3
#define INIT_FM_SET_STEREO_NOISE_2						240	//120
#define INIT_FM_SET_STEREO_NOISE_3						200	//160

#define INIT_FM_SET_STHIBLEND_MPH_1						3
#define INIT_FM_SET_STHIBLEND_MPH_2						80	
#define INIT_FM_SET_STHIBLEND_MPH_3						140

#define INIT_FM_SET_STHIBLEND_NOISE_1					3
#define INIT_FM_SET_STHIBLEND_NOISE_2					80
#define INIT_FM_SET_STHIBLEND_NOISE_3					140

enum TEF6686_STATUS{
	TUNER_IDLE = 0,
	TUNER_ERROR_1,
	TUNER_ERROR_2,
	TUNER_ERROR_3,
	TUNER_INIT_1_1,				//initial tune sequence
	TUNER_INIT_1_2,
	TUNER_INIT_1_3,
	TUNER_INIT_1_4,
	TUNER_INIT_1_5,
	TUNER_INIT_1_6,
	TUNER_INIT_1_7,
	TUNER_INIT_1_8,
	TUNER_INIT_1_9,
	TUNER_INIT_1_10,
	TUNER_INIT_1_11,
	TUNER_INIT_1_12,
	TUNER_INIT_1_13,
	TUNER_INIT_2_1,				//set default value
	TUNER_INIT_2_2,
	TUNER_INIT_2_3,
	TUNER_INIT_2_4,
	TUNER_INIT_2_5,
	TUNER_INIT_2_6,
	TUNER_INIT_2_7,
	TUNER_INIT_2_8,
	TUNER_INIT_2_9,
	TUNER_INIT_2_10,
	TUNER_INIT_2_11,
	TUNER_INIT_2_12,
	TUNER_INIT_2_13,
	TUNER_INIT_2_14,
	TUNER_INIT_2_15,
	TUNER_INIT_2_16,
	TUNER_INIT_2_17,
	TUNER_INIT_2_18,
	TUNER_INIT_3_1,
	TUNER_INIT_3_2,
	TUNER_INIT_3_3,
	TUNER_INIT_3_4,
	TUNER_INIT_3_5,
	TUNER_INIT_3_6,
	TUNER_INIT_3_7,
	TUNER_INIT_3_8,
	TUNER_INIT_3_9,
	TUNER_INIT_4_1,
	TUNER_INIT_4_2,
	TUNER_INIT_4_3,
	TUNER_INIT_4_4,
	TUNER_INIT_4_5,
	TUNER_INIT_4_6,
	TUNER_INIT_4_7,
	TUNER_INIT_4_8,
	TUNER_INIT_5_1,
	TUNER_INIT_5_2,
	TUNER_INIT_5_3,
	TUNER_INIT_5_4,
	TUNER_INIT_5_5,
	TUNER_INIT_5_6,
	TUNER_INIT_5_7,
	TUNER_INIT_5_8,
	TUNER_INIT_6_1,
	TUNER_INIT_6_2,
	TUNER_INIT_6_3,
	TUNER_INIT_6_4,
	TUNER_INIT_AMP,
	TUNER_ACTIVE_0,
	TUNER_ACTIVE_1,
	TUNER_CHANG_TO_FM_0,
	TUNER_CHANG_TO_FM_1,
	TUNER_CHANG_TO_AM_0,
	TUNER_CHANG_TO_AM_1,
	TUNER_TUNE_0,
	TUNER_TUNE_1,
	TUNER_SEEK_PLUS_0,
	TUNER_SEEK_PLUS_1,
	TUNER_SEEK_MINUS_0,
	TUNER_SEEK_MINUS_1,
	TUNER_ATS_0,
	TUNER_ATS_1,
	TUNER_ATS_2,
	TUNER_CHECK_SIGNAL_0,
	TUNER_CHECK_SIGNAL_1,
	TUNER_CHECK_SIGNAL_2,
};

extern uint16 set_bandwith_value;
extern uint16 check_ast_level;
extern uint16 check_ast_usn;
extern uint16 check_ast_wan;
extern uint16 check_ast_offset;
extern uint16 check_amast_level;

extern uint8 debug_on;

extern  QUE_t tef6686_cmd_q;

void TEF6686_Init_Comm(void);
uint16 check_TEF6686_cmd(void);

void TEF6686_Proc(DEVICE *dev);
uint8 Get_Operation_Status(void);
uint8 Get_Identification(uint8 *rx_data,uint8 len);
uint8 Get_Quality_Status(uint8 *rx_data,uint8 len, uint8 mode);
uint8 Get_Quality_Data(uint8 *rx_data,uint8 len, uint8 mode);

uint8 Clear_Required_Data(void);
uint8 Set_Required_Data0(void);
uint8 Set_Required_Data1(void);
uint8 Start_Cmd(void);
uint8 Send_Patchdata0(uint16 cnt);
uint8 Send_Patchdata1(uint16 cnt,uint8 len);
uint8 APPL_Set_ReferenceClock(uint32 value,uint16 mode);
uint8 Activate(void);

uint8 FM_Tune_To(uint16 mode,uint16 frequency);			
uint8 FM_Set_ChannelEqualizer(uint8 data);
uint8 FM_Set_MphSuppression(uint8 data);
uint8 FM_Set_Softmute_Time(uint16 slow_attack,uint16 slow_decay,uint16 fast_attack,uint16 fast_decay);
uint8 FM_Set_Softmute_Level(uint16 mode, uint16 start, uint16 slop);
uint8 FM_Set_Softmute_Noise(uint16 mode,uint16 start,uint16 slop,uint16 limit_mode,uint16 limit);
uint8 FM_Set_Softmute_Mph(uint16 mode,uint16 start,uint16 slop,uint16 limit_mode,uint16 limit);

uint8 FM_Set_HighCut_Mod(uint16 mode, uint16 start, uint16 slop, uint16 shift);
uint8 FM_Set_HighCut_Level(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_HighCut_Time(uint16 slow_attack,uint16 slow_decay,uint16 fast_attack,uint16 fast_decay);
uint8 FM_Set_HighCut_Mph(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_HighCut_Noise(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_HighCut_Max(uint16 mode,uint16 limit );
uint8 FM_Set_HighCut_Min(uint16 mode,uint16 limit );
uint8 FM_Set_Highcut_Options(uint16 mode);

uint8 FM_Set_LowCut_Max(uint16 mode,uint16 limit );

uint8 FM_Set_Stereo_Time(uint16 slow_attack,uint16 slow_decay,uint16 fast_attack,uint16 fast_decay);
uint8 FM_Set_Stereo_Mod(uint16 mode,uint16 start,uint16 slop,uint16 shift);
uint8 FM_Set_Stereo_Level(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_Stereo_Mph(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_Stereo_Noise(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_Stereo_Min(uint16 mode, uint16 limit);

uint8 FM_Set_StHiBlend_Mph(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_StHiBlend_Noise(uint16 mode,uint16 start,uint16 slop);
uint8 FM_Set_RFAGC(uint16 AGC, uint16 extand);
uint8 FM_Get_RFAGC(uint8 *rx_data,uint8 len);
//uint8 FM_Set_Bandwidth(uint16 mode, uint16 bandwidth, uint16 control_sense, uint16 low_level_sense, uint16 min_band,uint16 nominal_band,uint16 control_attack);
uint8 FM_Set_Bandwidth(uint16 mode, uint16 bandwidth, uint16 control_sense, uint16 low_level_sense);
uint8 FM_Set_Tune_Options(uint16 mode, uint16 bandwidth, uint16 mute_time, uint16 sample_time);
uint8 FM_Set_Deemphasis(uint16 timeconstant);
uint8 FM_Set_StereoImprovement(uint16 mode);
uint8 FM_Set_LevelOffset(uint16 offset);
uint8 FM_Set_NoiseBlanker(uint16 mode, uint16 sensitivity, uint16 level_sensitivity, uint16 modulation, uint16 offset, uint16 attack, uint16 decay);
uint8 FM_Set_RDS(uint16 mode);

uint8 AM_Tune_To(uint16 mode,uint16 frequency);
uint8 AM_Set_CoChannelDet(uint16 mode,uint16 restart,uint16 sensitivity,uint8 cnt);
uint8 AM_Set_HighCut_Level(uint16 mode,uint16 start,uint16 slop);
uint8  AM_Set_Bandwidth(uint16 mode, uint16 bandwidth);
uint8 AM_Set_Softmute_Mod(uint16 mode, uint16 start, uint16 slope, uint16 shift);
uint8 AM_Set_Softmute_Level(uint16 mode, uint16 start, uint16 slope);
uint8 AM_Set_Softmute_Max(uint16 mode, uint16 limit);
uint8 AM_Set_LevelOffset(uint16 offset);

//not used
uint8 APPL_Set_GPIO(uint16 pin,uint16 module,uint16 feature);
uint8 AUDIO_Set_Dig_IO(uint16 signal,uint16 mode,uint16 format,uint16 operation,uint16 samplerate);
uint8 AUDIO_Set_Mute(uint16 mode);
uint8 AUDIO_Set_Volume(uint16 volume );
uint8 AUDIO_Set_Output_Source(uint16 signal, uint16 source);
uint8 FM_Set_MonoImprovement(uint16 mode );


#endif /* TEF6686_DEV_H_ */
