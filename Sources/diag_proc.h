/*
 * diag_proc.h
 *
 *  Created on: 2018. 12. 24.
 *      Author: Digen
 */

#ifndef DIAG_PROC_H_
#define DIAG_PROC_H_



typedef enum {
	DIA_CMD_IDLE,	
	DIA_CMD_SPEAKER_FL_CTRL,	
	DIA_CMD_SPEAKER_FR_CTRL,	
	DIA_CMD_SPEAKER_RL_CTRL,	
	DIA_CMD_SPEAKER_RR_CTRL,	
	DIA_CMD_DISPLAY_ONOFF,	
	DIA_CMD_VOLUME_CTRL,	
	DIA_CMD_RADIO_SEEK,	
	DIA_CMD_RADIO_ANIMATION_ONOFF,	
} DIA_CMD_t;


void Diag_Task(DEVICE *dev);
 

#endif /* DIAG_PROC_H_ */
