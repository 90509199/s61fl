/*
 * ak7739_proc.c
 *
 *  Created on: 2018. 12. 31.
 *      Author: Digen
 */
#include "includes.h"

#ifdef SI_LABS_TUNER
#include "sg_defines.h"
#include "main.h"
#endif
extern uint8 au8DspInfo[];
//#define AKPRINTF(lvl, fmt, args...)		DPRINTF(lvl, fmt, ##args)
#define AKPRINTF(lvl, fmt, args...)

static volatile uint32 dsp_timer_tick = 0;
static volatile uint32 spaectrum_timer_tick = 0;

#ifdef DSP_DOUBLE_QUE
DSPDATA ak7739_cmd2[30]; 		 //20->30
QUE2_t ak7739_cmd2_q;
#else
uint8 ak7739_cmd[30];			//20->30
QUE_t ak7739_cmd_q;

 uint8 audio_mode_cmd[30];			//30
 QUE_t audio_mode_cmd_q;
#endif

#define LOUDNESS_DEFAULT					15
#define LOUDNESS_BASE_VALUE			25

static void ak7739_set_beep_vol(ak7739_priv *ak7739, DEVICE *dev);

void ak7739_Init_Comm(void)
{
	#ifdef DSP_DOUBLE_QUE
	init_q2(&ak7739_cmd2_q, &ak7739_cmd2[0], sizeof(ak7739_cmd2)/sizeof(DSPDATA));
	#else
	init_q(&ak7739_cmd_q, &ak7739_cmd[0], sizeof(ak7739_cmd));
	init_q(&audio_mode_cmd_q, &audio_mode_cmd[0], sizeof(audio_mode_cmd));
	#endif
}

#ifdef DSP_DOUBLE_QUE
uint16 check_ak_cmd2(void)
{
	return q2_data_size(&ak7739_cmd2_q);
}
#else
uint16 check_ak_cmd(void)
{
	return q_data_size(&ak7739_cmd_q);
}

uint16 check_audio_cmd(void)
{
	return q_data_size(&audio_mode_cmd_q);
}
#endif

/*
-20   	15
-25	14
-30	13
-35	12
-40	11
-45	10
-50	9
-55	8
-60	7
-65	6
-70	5
-75	4
-80	3
-85	2
-90	1
*/

void ak7739_media_volume(ak7739_priv *ak7739,	uint8 volume)
{
	#ifdef INTERNAL_AMP
	#ifdef SEPERATE_VOLUME_TABLE
	if(mediachtype == CH_TYPE_FM)
		set_ae_volume(ak7739, AE_FMVolume, volume); 			//control value
	else if(mediachtype == CH_TYPE_AM)
		set_ae_volume(ak7739, AE_AMVolume, volume); 			//control value
	else
		set_ae_volume(ak7739, AE_MediaInVolume, volume); 			//control value
		//set_ae_volume(ak7739, AE_MediaOutVolumeFLR, volume); 			//control value
//	DPRINTF(DBG_MSG3,"%s  %d \n\r",__FUNCTION__,mediachtype); 
	#else
	set_ae_volume(ak7739, AE_MediaInVolume, volume); 			//control value
	#endif
	#endif

}

#ifdef ENABLE_LOUDNESS_FOR_INTERAMP
void ak7739_loudness_volume(ak7739_priv *ak7739,	uint8 volume, uint8 onoff)
{
	uint8	tmp=0;
	#ifdef INTERNAL_AMP
	if(onoff)
	{		
		//onoff
		if(25<=volume)
		{
			set_ae_loudness(ak7739, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1); //min
		}
		else if((10<volume)&&(volume<24))
		{
			tmp = AE_LD_1+LOUDNESS_BASE_VALUE -volume;
			set_ae_loudness(ak7739, tmp, tmp, tmp, tmp, tmp, tmp, tmp); //max
			AKPRINTF(DBG_MSG5,"apply for loudness %d\n\r",volume);
		}
		else
			set_ae_loudness(ak7739, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16, AE_LD_16); //max

		AKPRINTF(DBG_MSG5,"ak7739_loudness_volume %d\n\r",volume);
	}
	else
	{
		//onoff
		set_ae_loudness(ak7739, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1, AE_LD_1); //min
	}
	#endif
}
#endif


static uint8 calcurate_spaectrum(uint8 input, uint8 volume)
{
	uint8 ret = 0;
	uint8 tmp	=0;

	tmp=VOLUME_MAX - volume;

	if(input<=(125-tmp))
		ret=0;
	else if(input<=(130-tmp))
		ret=1;
	else if(input<=(132-tmp))
		ret=3;
	else if(input<=(138-tmp))
		ret=7;
	#if 0
	else if(input<=(143-tmp)){
		ret=11;
	}
	else{
		ret=15;
	}
	#else
	else
		ret=15;
	#endif
	
	return ret;
}


//default 140  //-40
static uint8 calcurate_spaectrum_band1(uint8 input, uint8 volume)
{
	uint8 ret = 0;
	uint8 tmp =0;

	tmp=VOLUME_MAX - volume;

	if(input<=(123-tmp))
		ret=0;
	else if(input<=(128-tmp))
		ret=1;
	else if(input<=(132-tmp))
		ret=3;
	else if(input<=(135-tmp))
		ret=7;
	#if 0
	else if(input<=(140-tmp)){
		ret=11;
	}
	else{
		ret=15;
	}
	#else
	else
		ret=15;
	#endif
	
	return ret;
}

//default  84~129  //-51
static uint8 calcurate_spaectrum_band2(uint8 input, uint8 volume)
{
	uint8 ret = 0;
	uint8 tmp =0;

	tmp=VOLUME_MAX - volume;

	if(input<=(126 -tmp))
		ret=0;
	else if(input<=(128-tmp))
		ret=1;
	else if(input<=(130-tmp))
		ret=3;
	else if(input<=(136-tmp))
		ret=7;
#if 0
	else if(input<=(140-tmp)){
		ret=11;
	}
	else{
		ret=15;
	}
#else
	else
		ret=15;
#endif
	
	return ret;
}


//default  0  	x<-65
//			1		-65<x<-56
//			2		-56<x<-52
//			3		-52<x<50
//			7		-50<x<47
//			15	-47<x
static uint8 calcurate_spaectrum_band3(uint8 input, uint8 volume)
{
	uint8 ret = 0;
	uint8 tmp =0;

	tmp=VOLUME_MAX - volume;

	if(input<=(115 -tmp))
		ret=0;
	else if(input<=(124 -tmp))
		ret=1;
	else if(input<=(128 -tmp))
		ret=2;
	else if(input<=(130-tmp))
		ret=3;
	else if(input<=(133-tmp))
		ret=7;
#if 0
	else if(input<=(140-tmp)){
		ret=11;
	}
	else{
		ret=15;
	}
#else
	else
		ret=15;
#endif
	
	return ret;
}

//default  84~130  //-50
static uint8 calcurate_spaectrum_band4(uint8 input)
{
	uint8 tmp=0,ret =0;

	tmp = (uint8)input;

	if(tmp<113)
		ret=0;
	else if(127<=tmp)
		ret =14;
	else
		ret = tmp -112;
	
	return ret;
}

//default  110~130  //-50
static uint8 calcurate_spaectrum_band5(uint8 input)
{
	uint8 ret =0;

	if(input<110)
		ret=0;
	else if(input<120)
		ret=1;
	else if(input<123)
		ret=3;
	else if(input<126)
		ret=5;
	else if(input<129)
		ret=7;
	else if(input<132)
		ret=11;
	else
		ret=14;
	
	return ret;
}

void ak7739_proc(DEVICE *dev, ak7739_priv *ak7739)
{
//	uint8 rx_data[10]={0,};
	uint8 ret=0;
	uint8 tmp= 0;

	//check need to dev->r_Dsp->d_boot_ok
	#ifdef DSP_DOUBLE_QUE
	if((check_ak_cmd2() !=0)&&(dev->r_Dsp->d_state_lock ==OFF)&&(dev->r_Dsp->d_boot_ok==ON))
	{
		dev->r_Dsp->d_state = q2_de(&ak7739_cmd2_q) ;
	}
	#else
	if((check_ak_cmd() !=0)&&(dev->r_Dsp->d_state_lock ==OFF)&&(dev->r_Dsp->d_boot_ok==ON))
	{
		dev->r_Dsp->d_state = q_de(&ak7739_cmd_q) ;
	}
	#endif
	
	switch(dev->r_Dsp->d_state)
	{
		case DSP_IDLE:
			dsp_timer_tick = GetTickCount();
			break;	
		case DSP_INIT_1://2, 10 ->300, 200 ng, 100,50 sometime ok, ng 
			if(2<=GetElapsedTime(dsp_timer_tick))
			{
				au8DspInfo[3]++; //0x0e
				dev->r_Dsp->d_state =DSP_INIT_2;
				//setting internal amp
				dsp_timer_tick = GetTickCount();		
				AKPRINTF(DBG_MSG3," DSP_INIT_1  %d \n\r",dsp_timer_tick);
			}
			break;
		case DSP_INIT_2:
			if(5<=GetElapsedTime(dsp_timer_tick))
			{
				low_batter_flag	=	ON;
				ak7739_power_on(ak7739);
				dev->r_Dsp->d_state = DSP_INIT_3;
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_act_cnt ++;
				AKPRINTF(DBG_MSG3," ak7739_power_on time: %d \n\r",dsp_timer_tick);
			}
			break;
		case DSP_INIT_3:
			if(2<=GetElapsedTime(dsp_timer_tick))
			{
				//this time is 2ms
				dev->r_Dsp->d_state = DSP_INIT_4;
				ak7739_init_reset1(ak7739);
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG3," ak7739_init_reset1 time: %d \n\r",dsp_timer_tick);
			}
			break;	
		case DSP_INIT_4:
			if(7<=GetElapsedTime(dsp_timer_tick))
			{
				//this time is 5ms
				dev->r_Dsp->d_state = DSP_INIT_5;
				ak7739_init_reset2(ak7739);
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG3," ak7739_init_reset2 %d \n\r",dsp_timer_tick);
			}
			break;	
		case DSP_INIT_5:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{
				//this time is 10ms
				if(ak7739_dummy_reg(ak7739)&&(dev->r_Dsp->d_act_cnt <=5))
				{
					AKPRINTF(DBG_MSG3," ak7739_dummy_reg  fail \n\r");
					dev->r_Dsp->d_state = DSP_INIT_1;
					delay_ms(100); 
				}
				else if(5<dev->r_Dsp->d_act_cnt)
				{
					dev->r_Dsp->d_act_cnt =0;
					AKPRINTF(DBG_ERR," ak7739 failed initalize \n\r");
					dev->r_Dsp->d_state = DSP_ACTIVE;
				}
				else
					dev->r_Dsp->d_state = DSP_INIT_6;
				
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG3," DSP_INIT_5 %d \n\r\n\r",dsp_timer_tick);
			}
			break;	
		case DSP_INIT_6:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{
				dev->r_Dsp->d_state = DSP_INIT_7;
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG3," DSP_INIT_6 %d\n\r\n\r",dsp_timer_tick);
			}
			break;
		case DSP_INIT_7:
//			if(1<=GetElapsedTime(dsp_timer_tick))
			if(dev->r_Power->p_high_proity==OFF)
			{
				if (ak7739_init_reg_cust(ak7739) == TRUE)				
				{
					ak7739_set_status(ak7739, STATUS_STANDBY);
					dev->r_Dsp->d_state = DSP_INIT_8;
					dsp_timer_tick = GetTickCount();	
				}					
			}
			break;	
		case DSP_INIT_8:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{
				set_adc_soft_mute(ak7739, ADC_1, MUTE_ON);	
				set_adc_soft_mute(ak7739, ADC_2, MUTE_ON);	
				dev->r_Dsp->d_state = DSP_INIT_9;
				//check loop which DSP is normal		//5 times retry
				if((check_ak7739_reg(ak7739))&&(dev->r_Dsp->d_act_cnt <=5))
				{
					dev->r_Dsp->d_state = DSP_INIT_2;
				}
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG3," DSP_INIT_8 %d\n\r\n\r",dsp_timer_tick);
			}
			break;	
		case DSP_INIT_9:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				dev->r_Dsp->d_act_cnt =0;
				set_dac_soft_mute(ak7739, DAC_1, MUTE_ON);	
				dev->r_Dsp->d_state = DSP_INIT_10;
				dsp_timer_tick = GetTickCount();
			}
			break;				
		case DSP_INIT_10:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				dev->r_Dsp->d_act_cnt =0;
				dev->r_Dsp->d_state = DSP_INIT_11;
				set_dac_soft_mute(ak7739, DAC_2, MUTE_ON);	
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG3," sotf mute on %d\n\r\n\r",dsp_timer_tick);
			}
			break;	
		case DSP_INIT_11:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				dev->r_Dsp->d_state = DSP_INIT_12;
				//dev->r_Dsp->d_media_vol = e2p_save_data.E2P_MEDIA_VOL;
				dev->r_Dsp->d_radar_vol = e2p_save_data.E2P_RADAR_WARN_VOL;
				dev->r_Dsp->d_instrument_vol = e2p_save_data.E2P_CLU_WARN_VOL;
				dev->r_Dsp->d_key_vol = e2p_save_data.E2P_KEY_VOL;
				dev->r_Dsp->d_phone_vol = e2p_save_data.E2P_CALL_VOL;
				dev->r_Dsp->d_navi_vol = e2p_save_data.E2P_NAVI_VOL;
				dev->r_Dsp->d_voice_vol = e2p_save_data.E2P_VOICE_VOL;	
				
				dsp_timer_tick = GetTickCount();
			}
			break;	
		case DSP_INIT_12:
			if(1<=GetElapsedTime(dsp_timer_tick))
			{
				#ifdef M_BOARD_CONTROL_SOUND
				ret = ak7739_set_useCase(ak7739, AVNT_USECASE_AUDIOPATH_FM);
				dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_FM;
				#else
				ret = ak7739_set_useCase(ak7739, AVNT_USECASE_AUDIOPATH_MEDIA);
				dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
				#ifdef SEPERATE_VOLUME_TABLE
				mediachtype = CH_TYPE_MEDIA;
				#endif
				#endif
				dsp_timer_tick = GetTickCount();
				
				if(ret < 0)        
					akdbgprt_err("Usecase setting fail\n");
				dev->r_Dsp->d_state =DSP_INIT_13;				
			}
			break;	
		case DSP_INIT_13:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{	
				//initailize first tuner
				#ifdef SI_LABS_TUNER
				dev->r_Tuner->t_state = TUNER_INIT_1_1;
				dev->r_Dsp->d_state =DSP_INIT_14;
				#else
				dev->r_Tuner->t_state = TUNER_INIT_1_1;
				dev->r_Dsp->d_state =DSP_IDLE;
				#endif
				dsp_timer_tick = GetTickCount();
				low_batter_flag = OFF;
				AKPRINTF(DBG_MSG3," chang Tuner state %d\n\r\n\r",dsp_timer_tick);
			}
			break;	
		case DSP_INIT_14:
			if(1<=GetElapsedTime(dsp_timer_tick))
			{
				//setting external amp
				#ifdef EXTERNAL_AMP
				dev->r_A2B->a_state = A2B_INIT1;
				dev->r_Dsp->d_state =DSP_IDLE;
				AKPRINTF(DBG_MSG3,"chang a2b state \n\r" );
				#else				
				dev->r_System->amp_state = INTERNAL_AMP_INIT_START;
				dev->r_Dsp->d_state =DSP_IDLE;
				#endif

				//check which flash erase
				if(flash_erase_on ==OFF)
				{
					flash_sector_erase(FLASH_DATA0_ADDR);
					AKPRINTF(DBG_MSG3,"excute flash_sector_erase %d\n\r", dsp_timer_tick);
					flash_erase_on= ON;
				}
				dsp_timer_tick = GetTickCount();
			}
			break;	
		case DSP_INIT_15:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{				
				au8DspInfo[4]++; //0x16
				AKPRINTF(DBG_MSG3,"DSP_INIT_15 %d\n\r\n\r",dsp_timer_tick);
				dev->r_Dsp->d_state =DSP_INIT_UNMUTE_1;
			}
			break;	
  		case DSP_INIT_UNMUTE_1:
			dev->r_Dsp->d_state_lock =ON;


			if(dev->r_System->s_state <= SYS_ACTIVE_MODE_CHECK)
			{
				if(dev->r_Dsp->d_goto_mute_status == OFF)
					set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);
				set_ae_volume(ak7739, AE_MediaInVolume, 0); //min
			}

			AKPRINTF(DBG_MSG3," INIT MEDIA MUTE \n\r");
			dev->r_Dsp->d_state =DSP_INIT_UNMUTE_2;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_INIT_UNMUTE_2:
			if(700<=GetElapsedTime(dsp_timer_tick))
			{  //pop noise		500->400->700->800->1000
				dsp_timer_tick = GetTickCount();
				//check a2b amp
				dev->r_Dsp->d_state =DSP_INIT_UNMUTE_3;
			}
			break;
		case DSP_INIT_UNMUTE_3:
			if(2<=GetElapsedTime(dsp_timer_tick))
			{  //pop noise	
				set_adc_soft_mute(ak7739, ADC_1, MUTE_OFF);	
				set_adc_soft_mute(ak7739, ADC_2, MUTE_OFF);				
				set_dac_soft_mute(ak7739, DAC_1, MUTE_OFF);	

				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_INIT_UNMUTE_4;
			}
			break;
		case DSP_INIT_UNMUTE_4:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				AKPRINTF(DBG_INFO," DSP_INIT_UNMUTE \n\r"); 	
				set_dac_soft_mute(ak7739, DAC_2, MUTE_OFF);
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_ACTIVE;
				dev->r_Dsp->d_state_lock =OFF;
				dev->r_Dsp->d_boot_ok=ON;
			}
			break;
		case DSP_ACTIVE:
			//start radar
			if(dev->r_Beep->beep_active != dev->r_Beep->beep_pre_active)
			{

				if(dev->r_Beep->beep_active==ON)
					q2_en(&ak7739_cmd2_q, (uint8)DSP_MIXER_VOL_CONT,(uint8) dev->r_Dsp->d_audo_mode);
				else
					q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL,(uint8) dev->r_Dsp->d_audo_mode);
				dev->r_Beep->beep_pre_active = dev->r_Beep->beep_active;
			}
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_CHANG_MODE_1:
			dev->r_Dsp->d_state_lock =ON;
			set_adc_soft_mute(ak7739, ADC_1, MUTE_ON);	
			set_adc_soft_mute(ak7739, ADC_2, MUTE_ON);	
			set_dac_soft_mute(ak7739, DAC_1, MUTE_ON);	
			set_dac_soft_mute(ak7739, DAC_2, MUTE_ON);
			dsp_timer_tick = GetTickCount();
			dev->r_Dsp->d_state= DSP_CHANG_MODE_2;
			break;
		case DSP_CHANG_MODE_2://25->20
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				ret = ak7739_set_useCase(ak7739, dev->r_Dsp->d_audo_mode ); 
				e2p_sys_data.E2P_AUDIO_PATH_DEFAULT = dev->r_Dsp->d_audo_mode;
				DPRINTF(DBG_MSG3,"DSP_CHANG_MODE_2 mode %d vol:%d mute%d\n\r\n\r",dev->r_Dsp->d_audo_mode,
				dev->r_Dsp->d_media_vol,dev->r_Dsp->d_softmute_status); 
				
				if(ret < 0)        
					akdbgprt_err("Usecase setting fail\n");
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state= DSP_CHANG_MODE_3;
			}
			break;
		case DSP_CHANG_MODE_3:
			if(5<=GetElapsedTime(dsp_timer_tick))
			{
				dev->r_Dsp->d_change_audo_mode = OFF;
				switch(dev->r_Dsp->d_audo_mode)
				{
					case AVNT_USECASE_AUDIOPATH_FM:			//부팅시... standby 모드 mute하기 위해..
						if(dev->r_System->s_state <= SYS_ACTIVE_MODE_CHECK)
						{
							//soft mute
							set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);//NAVI  MUTE
							ak7739_media_volume(ak7739, VOLUME_MIN);
						}
						else
						{
							set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_ON);//HPF MUTE
							if((dev->r_Dsp->d_softmute_status == OFF)
								/*&&((dev->r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)&&(dev->r_System->s_state != SYS_STANDBY_MODE_IDLE))*/)
							{
								if(dev->r_Dsp->d_goto_mute_status == OFF)
									set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);	//NAVI  UNMUTE
								else
									set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);		//NAVI  MUTE
									
								if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
									ak7739_media_volume(ak7739, (e2p_save_data.E2P_MEDIA_VOL/2));
								else
									ak7739_media_volume(ak7739, e2p_save_data.E2P_MEDIA_VOL);
							}
							else
							{
								set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);//NAVI  MUTE
								ak7739_media_volume(ak7739, VOLUME_MIN);
							}
						}		
						dev->r_Dsp->d_media_vol = e2p_save_data.E2P_MEDIA_VOL;
						break;
					case AVNT_USECASE_AUDIOPATH_MEDIA:
						if(dev->r_System->s_state <= SYS_ACTIVE_MODE_CHECK)
						{
							set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);//NAVI  OFF
							ak7739_media_volume(ak7739, VOLUME_MIN);
						}
						else
						{
							set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_ON);//HPF OFF
							if(dev->r_Dsp->d_softmute_status == OFF)
							{
								if(dev->r_Dsp->d_goto_mute_status == OFF)
									set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);//NAVI  ON
								else
									set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);		//NAVI  MUTE
									
								if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
									ak7739_media_volume(ak7739, (AK_MEDIA_VOL_DEFAULT/2));
								else
									ak7739_media_volume(ak7739, AK_MEDIA_VOL_DEFAULT);
							}
							else
							{
								set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);//NAVI  MUTE
								ak7739_media_volume(ak7739, VOLUME_MIN);
							}
						}
						dev->r_Dsp->d_media_vol = AK_MEDIA_VOL_DEFAULT;
						break;
					case AVNT_USECASE_AUDIOPATH_BCALL:
						ak7739_media_volume(ak7739, AK_BCALL_VOL_DEFAULT);
						set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);		//NAVI  OFF
						set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_ON);//HPF OFF
						dev->r_Dsp->d_media_vol = AK_BCALL_VOL_DEFAULT;
						break;
					case AVNT_USECASE_AUDIOPATH_BT_A2DP:
					case AVNT_USECASE_AUDIOPATH_USB_HICAR_CARLIFE:
					case AVNT_USECASE_AUDIOPATH_NAVI:
					case AVNT_USECASE_AUDIOPATH_VR_AECREF:
						if((dev->r_Dsp->d_goto_mute_status == OFF)&&(dev->r_Dsp->d_softmute_status == OFF))
							set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);//NAVI  ON
						set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_ON);//HPF OFF
						break;
					case AVNT_USECASE_AUDIOPATH_BT_HFP:
						set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);//NAVI  OFF
						set_ae_mute(ak7739, AE_PhoneMute, AE_MUTE_OFF);//HPF ON
//						set_ae_volume(ak7739, AE_PhoneVolume, dev->r_Dsp->d_phone_vol);  //send soc data
						AKPRINTF(DBG_MSG1,"AVNT_USECASE_AUDIOPATH_BT_HFP  volume %d,media volume:%d \n",dev->r_Dsp->d_phone_vol); 
						break;
				}
				
				dev->r_Dsp->d_state = DSP_CHANG_MODE_4;
				dsp_timer_tick = GetTickCount();			
			}
			break;
		case DSP_CHANG_MODE_4:			//500->600
			#ifdef SOS_CALL_APPLY_TBOX_MUTE
			if(dev->r_Dsp->d_audo_mode==AVNT_USECASE_AUDIOPATH_BCALL)
			{
				if(MCU_TBOX_MUTE()==ON)
				{	
					if(3000<=GetElapsedTime(dsp_timer_tick))
					{	
						set_adc_soft_mute(ak7739, ADC_1, MUTE_OFF);	
						set_adc_soft_mute(ak7739, ADC_2, MUTE_OFF);

						
						AKPRINTF(DBG_MSG3,"DSP_CHANG_MODE_4 mode %d \n\r",dev->r_Dsp->d_audo_mode); 
						dev->r_Dsp->d_state = DSP_CHANG_MODE_5;
						dsp_timer_tick = GetTickCount();
					}
				}
				else if(dev->r_Dsp->d_change_audo_mode == ON)
				{
					//cancel sos call
					set_adc_soft_mute(ak7739, ADC_1, MUTE_OFF);	
					set_adc_soft_mute(ak7739, ADC_2, MUTE_OFF);	
					
					AKPRINTF(DBG_MSG3,"DSP_CHANG_MODE_4 mode %d \n\r\n\r",dev->r_Dsp->d_audo_mode);
					dev->r_Dsp->d_state = DSP_CHANG_MODE_5;
					dsp_timer_tick = GetTickCount();
				}
				else
				{
					dsp_timer_tick = GetTickCount();	
				}
			}
			else
			{
			if(500<=GetElapsedTime(dsp_timer_tick))
			{	
				set_adc_soft_mute(ak7739, ADC_1, MUTE_OFF);	
				set_adc_soft_mute(ak7739, ADC_2, MUTE_OFF);	

				
				dev->r_Dsp->d_state = DSP_CHANG_MODE_5;
				dsp_timer_tick = GetTickCount();
			}
			}			

			break;
		case DSP_CHANG_MODE_5:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				set_dac_soft_mute(ak7739, DAC_1, MUTE_OFF);					
				set_dac_soft_mute(ak7739, DAC_2, MUTE_OFF);
				dev->r_Dsp->d_state_lock =OFF;
				dev->r_Dsp->d_state = DSP_ACTIVE;
//				DPRINTF(DBG_MSG3,"DSP_CHANG_MODE_5 mode %d %d\n\r\n\r",dev->r_Dsp->d_audo_mode,dev->r_Dsp->d_softmute_status); 
				dsp_timer_tick = GetTickCount();				
			}
			break;
		case DSP_MUTE_1:
			dev->r_Dsp->d_state_lock =ON;
			DPRINTF(DBG_MSG3," DSP_MUTE_1 \n\r"); 
			set_adc_soft_mute(ak7739, ADC_1, MUTE_ON);	
			set_adc_soft_mute(ak7739, ADC_2, MUTE_ON);	
			set_dac_soft_mute(ak7739, DAC_1, MUTE_ON);	
			dsp_timer_tick = GetTickCount();
			dev->r_Dsp->d_state =DSP_MUTE_2;
			break;
		case DSP_MUTE_2:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				set_dac_soft_mute(ak7739, DAC_2, MUTE_ON);
				dev->r_Dsp->d_state_lock =OFF;

				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;
		case DSP_UNMUTE_1:
			dev->r_Dsp->d_state_lock =ON;
			DPRINTF(DBG_MSG3," DSP_UNMUTE_1 \n\r"); 
			set_adc_soft_mute(ak7739, ADC_1, MUTE_OFF);	
			set_adc_soft_mute(ak7739, ADC_2, MUTE_OFF);	
			set_dac_soft_mute(ak7739, DAC_1, MUTE_OFF);	

			dsp_timer_tick = GetTickCount();
			dev->r_Dsp->d_state =DSP_UNMUTE_2;
			break;
		case DSP_UNMUTE_2:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				set_dac_soft_mute(ak7739, DAC_2, MUTE_OFF);
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state_lock =OFF;
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;
		case DSP_GOTO_MUTE_1:
			if(dev->r_Dsp->d_audo_mode != AVNT_USECASE_AUDIOPATH_BCALL)
			ak7739_media_volume(ak7739, VOLUME_MIN);
			set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);
			dev->r_Dsp->d_goto_mute_status = ON;
			dev->r_Dsp->d_state =DSP_ACTIVE;	
			dsp_timer_tick = GetTickCount();
			DPRINTF(DBG_INFO," DSP_GOTO_MUTE \n\r");
			break;
		case DSP_GOTO_MUTE_2:
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_GOTO_UNMUTE_1:
			dev->r_Dsp->d_state_lock =ON;
			DPRINTF(DBG_INFO," DSP_GOTO_UNMUTE_1 \n\r"); 
			if(dev->r_Dsp->d_softmute_status == OFF)
			{
				if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA)
				{
					if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
						ak7739_media_volume(ak7739, (AK_MEDIA_VOL_DEFAULT/2));
					else
						ak7739_media_volume(ak7739, AK_MEDIA_VOL_DEFAULT);
				}
				else if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL)
				{
						ak7739_media_volume(ak7739, AK_BCALL_VOL_DEFAULT);
				}
				else
				{
					if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
						ak7739_media_volume(ak7739, (e2p_save_data.E2P_MEDIA_VOL/2));
					else
						ak7739_media_volume(ak7739, e2p_save_data.E2P_MEDIA_VOL);
				}
				set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);
//				dev->r_Dsp->d_goto_mute_status = OFF;
			}	
			else
			{	//unmute
				if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
					ak7739_media_volume(ak7739, (e2p_save_data.E2P_MEDIA_VOL/2));
				else
					ak7739_media_volume(ak7739, e2p_save_data.E2P_MEDIA_VOL);
			}
			dev->r_Dsp->d_goto_mute_status = OFF;	
			dev->r_Dsp->d_state =DSP_GOTO_UNMUTE_2;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_GOTO_UNMUTE_2:
			if(5<=GetElapsedTime(dsp_timer_tick))
			{
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state_lock =OFF;
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;
		case DSP_SOFT_MUTE:	//navi, media mute
			if(dev->r_Dsp->d_audo_mode != AVNT_USECASE_AUDIOPATH_BCALL)
			ak7739_media_volume(ak7739, VOLUME_MIN);
			set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_ON);
			dev->r_Dsp->d_softmute_status = ON;
			dev->r_Dsp->d_state =DSP_ACTIVE;
			AKPRINTF(DBG_INFO," MSG_SOFT_MUTE \n\r"); 
			break;
		case DSP_SOFT_UNMUTE://navi, media unmute
			#if 1
			if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
			{
				if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA)
					ak7739_media_volume(ak7739, AK_MEDIA_VOL_DEFAULT/2);
				else if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL)
					ak7739_media_volume(ak7739, AK_BCALL_VOL_DEFAULT);
				else
					ak7739_media_volume(ak7739, e2p_save_data.E2P_MEDIA_VOL/2);
			}
			else
			{
				if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA)
					ak7739_media_volume(ak7739, AK_MEDIA_VOL_DEFAULT);
				else if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL)
					ak7739_media_volume(ak7739, AK_BCALL_VOL_DEFAULT);
				else
					ak7739_media_volume(ak7739, e2p_save_data.E2P_MEDIA_VOL);
			}			
			#endif
			if(dev->r_Dsp->d_audo_mode !=AVNT_USECASE_AUDIOPATH_BT_HFP)
				set_ae_mute(ak7739, AE_NaviMute, AE_MUTE_OFF);// don't use navi pass(bt_hfp) 
			dev->r_Dsp->d_softmute_status = OFF;
			dev->r_Dsp->d_state =DSP_ACTIVE;
			AKPRINTF(DBG_INFO," MSG_SOFT_UNMUTE \n\r"); 
			break;
		case DSP_MAIN_VOL:
			#ifndef M_BOARD_CONTROL_SOUND
			if(((SYS_ACTIVE_MODE_ENTER<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_SCREENOFF_1_MODE_IDLE))) //||((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_15MIN_MODE_IDLE)))  //KHI 15MIN CONDITION DELETE
			#endif
				{
					if(dev->r_Dsp->d_softmute_status == OFF)
					{
						if((dev->r_Beep->beep_active==ON)||(dev->r_Dsp->d_mix_status==ON))
						{
							if(dev->r_Dsp->d_media_vol<32)
								ak7739_media_volume(ak7739, (dev->r_Dsp->d_media_vol/2));
							else
								ak7739_media_volume(ak7739, VOLUME_MIDDLE);
						}
						else
						{
							if(dev->r_Dsp->d_media_vol<32)
								ak7739_media_volume(ak7739, dev->r_Dsp->d_media_vol);
							else
								ak7739_media_volume(ak7739, VOLUME_MIDDLE);
						}
					}
					AKPRINTF(DBG_MSG3," DSP_MAIN_VOL %d mute: %d\n\r",dev->r_Dsp->d_media_vol,dev->r_Dsp->d_softmute_status);
				}
			dsp_timer_tick = GetTickCount();
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_MIXER_VOL_CONT:
			if(((SYS_ACTIVE_MODE_ENTER<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_SCREENOFF_1_MODE_IDLE))) //||((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_15MIN_MODE_IDLE)))    //KHI 15MIN CONDITION DELETE
				{
					if(dev->r_Dsp->d_softmute_status == OFF)
					{
						if(dev->r_Dsp->d_media_vol<32)
							ak7739_media_volume(ak7739, (dev->r_Dsp->d_media_vol/2));
						else
							ak7739_media_volume(ak7739, VOLUME_MIDDLE);
					}
					AKPRINTF(DBG_MSG5_1," DSP_MIXER_VOL_CONT  %d mode %d\n\r",dev->r_Dsp->d_media_vol,dev->r_Dsp->d_audo_mode ); 
				}		
			dsp_timer_tick = GetTickCount();
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_MIXER_ENTER_0:
			if(((SYS_ACTIVE_MODE_ENTER<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_SCREENOFF_1_MODE_IDLE))) // ||((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_15MIN_MODE_IDLE)))   //KHI 15MIN CONDITION DELETE
				{
					if(dev->r_Dsp->d_softmute_status == OFF)
					{
						if(dev->r_Dsp->d_media_vol<32)
							ak7739_media_volume(ak7739, ((dev->r_Dsp->d_media_vol*3)/4));
						else
							ak7739_media_volume(ak7739, VOLUME_MIDDLE);
					}
					dev->r_Dsp->d_state_lock =ON;
					dev->r_Dsp->d_state =DSP_MIXER_ENTER_1;
				}
			else
				dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_MIXER_ENTER_1://300->250->100
			if(100<=GetElapsedTime(dsp_timer_tick))
			{
				if(dev->r_Dsp->d_softmute_status == OFF)
				{
					if(dev->r_Dsp->d_media_vol<32)
						ak7739_media_volume(ak7739, ((dev->r_Dsp->d_media_vol*2)/3));
					else
						ak7739_media_volume(ak7739, VOLUME_MIDDLE);
				}
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_MIXER_ENTER_2;
			}
			break;
		case DSP_MIXER_ENTER_2:
			if(100<=GetElapsedTime(dsp_timer_tick))
			{
				if(dev->r_Dsp->d_softmute_status == OFF)
				{
					if(dev->r_Dsp->d_media_vol<32)
						ak7739_media_volume(ak7739, (dev->r_Dsp->d_media_vol/2));
					else
						ak7739_media_volume(ak7739, VOLUME_MIDDLE);
				}
				AKPRINTF(DBG_MSG5," DSP_MIXER_ENTER_2  %d mode %d\n\r",dev->r_Dsp->d_media_vol,dev->r_Dsp->d_audo_mode ); 
				dev->r_Dsp->d_mix_status = ON;
				dev->r_Dsp->d_state_lock =OFF;
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;	
		case DSP_MIXER_EXIT_0:
			if(((SYS_ACTIVE_MODE_ENTER<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_SCREENOFF_1_MODE_IDLE))) // ||((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_15MIN_MODE_IDLE)))  //KHI 15MIN CONDITION DELETE
				{
					if(dev->r_Dsp->d_softmute_status == OFF)
					{
						if(dev->r_Dsp->d_media_vol<32)
							ak7739_media_volume(ak7739, ((dev->r_Dsp->d_media_vol*2)/3));
						else
							ak7739_media_volume(ak7739, VOLUME_MIDDLE);
					}
					dev->r_Dsp->d_state_lock =ON;
					
					dev->r_Dsp->d_state =DSP_MIXER_EXIT_1;
				}
			else
			{
				dev->r_Dsp->d_mix_status = OFF;
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_MIXER_EXIT_1://300->250->100
			if(100<=GetElapsedTime(dsp_timer_tick))
			{
				if(dev->r_Dsp->d_softmute_status == OFF)
				{
					if(dev->r_Dsp->d_media_vol<32)
						ak7739_media_volume(ak7739, ((dev->r_Dsp->d_media_vol*3)/4));
					else
						ak7739_media_volume(ak7739, VOLUME_MIDDLE);
				}
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_MIXER_EXIT_2;
			}
			break;
		case DSP_MIXER_EXIT_2:
			if(100<=GetElapsedTime(dsp_timer_tick))
			{
				if(dev->r_Dsp->d_softmute_status == OFF)
				{
					if(dev->r_Dsp->d_media_vol<32)
						ak7739_media_volume(ak7739, (dev->r_Dsp->d_media_vol));
					else
						ak7739_media_volume(ak7739, VOLUME_MIDDLE);
				}
				AKPRINTF(DBG_MSG5," DSP_MIXER_EXIT_2 %d\n\r",dev->r_Dsp->d_media_vol);
				dev->r_Dsp->d_mix_status = OFF;
				dev->r_Dsp->d_state_lock =OFF;
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;
		case DSP_FADER_BALANCE_0:
			dev->r_Dsp->d_state_lock =ON;

			set_ae_fadeNbalance(ak7739, (AE_REAR_7-e2p_save_data.E2P_EQ_FADER), e2p_save_data.E2P_EQ_BALANCE);
			#endif
			AKPRINTF(DBG_MSG5,"DSP_FADER_BALANCE %d %d\n\r", e2p_save_data.E2P_EQ_FADER, (14-e2p_save_data.E2P_EQ_BALANCE));

			dev->r_Dsp->d_state =DSP_FADER_BALANCE_1;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_FADER_BALANCE_1:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state_lock =OFF;
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;
		case 	DSP_BEEP_START:
			ak7739_set_beep_vol(ak7739,dev);
			set_ae_beepControl(ak7739, AE_BEEP_START);
//			AKPRINTF(DBG_MSG5,"DSP_BEEP_START\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case 	DSP_BEEP_STOP:
			AKPRINTF(DBG_MSG5,"DSP_BEEP_STOP\n\r" );
			 set_ae_beepControl(ak7739, AE_BEEP_SOFT_STOP); //cycle stop
			 dev->r_Dsp->d_state =DSP_ACTIVE;
			break; 
		case DSP_BEEP_DIAG_START:
//		set_ae_beepOption(ak7739, 1250, 10, 10, 80, 100, 1); //1kHz, 20ms, 20ms, 100%, 200ms, 3
			set_ae_beepOption(ak7739, 1250, 10, 10, 80, 500, 10); //1kHz, 20ms, 20ms, 100%, 200ms, 3
			ak7739_set_beep_vol(ak7739,dev);
			set_ae_beepControl(ak7739, AE_BEEP_START);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_RADAR_VOL:
			//radar vol control
			#if 1 //def INTERNAL_AMP
			if(dev->r_Dsp->d_radar_vol ==VOL_LOW)
			{
				tmp = RADAR_LEVEL_LOW;			//10->8->6
				set_ae_volume(ak7739, AE_BeepVolume, tmp);
			}
			else if(dev->r_Dsp->d_radar_vol ==VOL_MIDDLE)
			{
				tmp = RADAR_LEVEL_MID;			//18->14->10->7
				set_ae_volume(ak7739, AE_BeepVolume,tmp);
			}
			else if(dev->r_Dsp->d_radar_vol ==VOL_HIGH)
			{
				tmp = RADAR_LEVEL_HIGH;			//25 ->20->12->8
				set_ae_volume(ak7739, AE_BeepVolume, tmp);
			}
			#endif
			 dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case 	DSP_INSTRUMENT_VOL:
			//instument vol control
			#if 1 //def INTERNAL_AMP
			if(dev->r_Dsp->d_instrument_vol==VOL_LOW)
			{
				tmp = INSTRUMENT_LEVEL_LOW;			//10->8->7
				set_ae_volume(ak7739, AE_ChimeVolume, tmp);
			}
			else if(dev->r_Dsp->d_instrument_vol ==VOL_MIDDLE)
			{
				tmp = INSTRUMENT_LEVEL_MID;			//18->14->10->8
				set_ae_volume(ak7739, AE_ChimeVolume,tmp);
			}
			else if(dev->r_Dsp->d_instrument_vol ==VOL_HIGH)
			{
				tmp = INSTRUMENT_LEVEL_HIGH;			//25 ->20->12->9
				set_ae_volume(ak7739, AE_ChimeVolume, tmp);
			}
			#endif
			 dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_KEY_VOL:
			break;
		case	DSP_CALL_VOL:
			//phone vol control
			#if 1//def INTERNAL_AMP
			set_ae_volume(ak7739, AE_PhoneVolume, dev->r_Dsp->d_phone_vol);
			#endif
			AKPRINTF(DBG_MSG3,"DSP_CALL_VOL %d mute: %d\n\r",dev->r_Dsp->d_phone_vol,dev->r_Dsp->d_softmute_status);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case	DSP_NAVI_VOL:
			//nvai vol control
			#ifdef INTERNAL_AMP
//			set_ae_volume(ak7739, AE_NaviVolume, dev->r_Dsp->d_navi_vol);
			#endif
			 dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case	DSP_VOICE_VOL:
			break;		
		case	DSP_BCALL_VOL:
			if(((SYS_ACTIVE_MODE_ENTER<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_SCREENOFF_1_MODE_IDLE))) //|| ((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&( dev->r_System->s_state<= SYS_15MIN_MODE_IDLE)))  //KHI 15MIN CONDITION DELETE
				{
					//if(dev->r_Dsp->d_softmute_status == OFF)
					{
						if(dev->r_Dsp->d_media_vol<32)
							ak7739_media_volume(ak7739, dev->r_Dsp->d_bcall_vol);
						else
							ak7739_media_volume(ak7739, VOLUME_MIDDLE);
					}
					AKPRINTF(DBG_MSG3," DSP_BCALL_VOL %d mute: %d\n\r",dev->r_Dsp->d_bcall_vol,dev->r_Dsp->d_softmute_status);
				}
			dsp_timer_tick = GetTickCount();
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;	
		case DSP_EQ_STANDARD:			

			set_ae_bassMidTreble(ak7739, AE_BMT_0, AE_BMT_0, AE_BMT_0); // 	0 		0		0
			AKPRINTF(DBG_MSG3,"DSP_EQ_STANDARD\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;	
		case DSP_EQ_POP:						// 	4 		2		3			//11			9		10

			set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_N3, AE_BMT_P5);  // 	3 		-3		5
			AKPRINTF(DBG_MSG3,"DSP_EQ_POP\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_EQ_ROCK:					// 	6		0		4				//13			7		11

			set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_N3, AE_BMT_P3); // 	3 		-3		3 
			AKPRINTF(DBG_MSG3,"DSP_EQ_ROCK\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_EQ_JAZZ:						// 	3		0		2			//	10			7		9
			set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_P2, AE_BMT_N2); // 	3		2		-2
			AKPRINTF(DBG_MSG3,"DSP_EQ_JAZZ\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_EQ_CLASSIC:				// 	-3		-2		2			// 4		5		9	
			set_ae_bassMidTreble(ak7739, AE_BMT_P2, AE_BMT_P2, AE_BMT_N3);  // 	2 		2		-3
			AKPRINTF(DBG_MSG3,"DSP_EQ_CLASSIC\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_EQ_HUMAN_VOICE:		// 	7		3		2		//14		10		9
			set_ae_bassMidTreble(ak7739, AE_BMT_P3, AE_BMT_P4, AE_BMT_N4);  // 	3		4		-4
			AKPRINTF(DBG_MSG3,"DSP_EQ_HUMAN_VOICE\n\r" );
			dev->r_Dsp->d_state =DSP_ACTIVE;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_EQ_CUSTOM_0:		//		0		0		0
			dev->r_Dsp->d_state_lock =ON;
			set_ae_bassMidTreble(ak7739, e2p_save_data.E2P_EQ_BASS, e2p_save_data.E2P_EQ_MID,e2p_save_data.E2P_EQ_TREBLE); 
			AKPRINTF(DBG_MSG3,"DSP_EQ_COMMAND bass:%d mid:%d treble:%d\n\r", e2p_save_data.E2P_EQ_BASS, e2p_save_data.E2P_EQ_MID,e2p_save_data.E2P_EQ_TREBLE); 
			dev->r_Dsp->d_state =DSP_EQ_CUSTOM_1;
			dsp_timer_tick = GetTickCount();
			break;
		case DSP_EQ_CUSTOM_1:
			if(10<=GetElapsedTime(dsp_timer_tick)){
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_state_lock =OFF;
				dev->r_Dsp->d_state =DSP_ACTIVE;
			}
			break;
		case DSP_LOUDNESS:
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		#ifdef SVCD_ENABLE
		case DSP_SVCD0:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 17); // 0db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 17); // 0db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD1:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 18); // 1db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 18); // 1db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD2:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 19); // 2db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 19); // 2db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD3:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 20); // 3db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 20); // 3db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD4:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 21); // 4db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 21); // 4db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD5:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 22); // 5db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 22); // 5db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD6:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 23); // 6db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 23); // 6db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD7:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 24); // 7db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 24); // 7db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD8:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 25); // 8db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 25); // 8db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD9:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 26); // 9db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 26); // 9db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD10:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 27); // 10db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 27); // 10db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD11:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 28); // 11db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 28); // 11db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD12:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 29); // 12db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 29); // 12db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD13:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 30); // 13db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 30); // 13db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_SVCD14:
			#ifndef REMOVE_DSP_PRAM_FOR_CAN_TEST
			set_ae_volume(ak7739, AE_SVCDFLRGain, 31); // 14db
			set_ae_volume(ak7739, AE_SVCDRLRGain, 31); // 14db
			#endif
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		#endif
		case DSP_POWER_DOWN:
			ak7739_power_down(ak7739);
			AKPRINTF(DBG_MSG3,"DSP_POWER_DOWN \n\r"); 			
			dev->r_Dsp->d_state =DSP_IDLE;
			break;
		case DSP_TEST_1:	
			//RegDump for GUI
			AKPRINTF(DBG_MSG3,">>############### After Set USECASE AK7739_DEBUG_REGDUMP Start\n");
			//Before user reg set
			set_test_reg_read(ak7739, 0);
			set_test_reg_read(ak7739, 1);
			set_test_reg_read(ak7739, 2);
			set_test_reg_read(ak7739, 3);
			set_test_reg_read(ak7739, 4);
			set_test_reg_read(ak7739, 5);
			set_test_reg_read(ak7739, 6);
			AKPRINTF(DBG_MSG1,"<<############### After Set USECASE AK7739_DEBUG_REGDUMP End\n");
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_2:
			ak7739_test(ak7739);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_3:
			ak7739_test1(ak7739);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_4:
			ak7739_test2(ak7739);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_5:
			ak7739_test3(ak7739);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_6:
			AKPRINTF(DBG_MSG3," DSP_TEST_6\n\r");
			if(r_sonysound)
				test_read_ram(ak7739, DSPNUMTYPE_DSP2, IMGTYPE_CRAM, CRAM_ADDR_BAND1_LoudnessGEQ_sony, AE_LOUDNESS_CRAMSIZE);
			else
				test_read_ram(ak7739, DSPNUMTYPE_DSP2, IMGTYPE_CRAM, CRAM_ADDR_BAND1_LoudnessGEQ, AE_LOUDNESS_CRAMSIZE);
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_7:
			DPRINTF(DBG_MSG3," DSP_TEST_7\n\r");
			//apply for sony sound
			if(r_sonysound)
			{
			}
			else
			{
			if(shell_geq_onoff)
				set_ae_personalGeqOn(ak7739);
			else
				set_ae_personalGeqOff(ak7739);
			}
			dev->r_Dsp->d_state =DSP_ACTIVE;
			break;
		case DSP_TEST_INIT_1:
			{
				low_batter_flag	=	ON;
				dev->r_Dsp->d_state_lock =ON;
				ak7739_power_on(ak7739);
				dev->r_Dsp->d_state =DSP_TEST_INIT_2;
				dsp_timer_tick = GetTickCount();
				dev->r_Dsp->d_act_cnt ++;
				AKPRINTF(DBG_MSG3," DSP_TEST_INIT_1\n\r");
			}	
			break;
		case DSP_TEST_INIT_2:
			if(2<=GetElapsedTime(dsp_timer_tick))
			{
				//this time is 2ms
				dev->r_Dsp->d_state =DSP_TEST_INIT_3;
				ak7739_init_reset1(ak7739);
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_2\n\r");
			}
			break;
		case DSP_TEST_INIT_3:
			if(5<=GetElapsedTime(dsp_timer_tick))
			{
				//this time is 5ms
				dev->r_Dsp->d_state =DSP_TEST_INIT_4;
				ak7739_init_reset2(ak7739);
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_3\n\r");
			}
			
			break;
		case DSP_TEST_INIT_4:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{
				//this time is 10ms
				if(ak7739_dummy_reg(ak7739)){
					AKPRINTF(DBG_MSG1," ak7739_dummy_reg  fail  \n\r");
					dev->r_Dsp->d_state = DSP_ACTIVE;
				}
				dev->r_Dsp->d_state =DSP_TEST_INIT_5;
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_4\n\r");
			}
			break;
		case DSP_TEST_INIT_5:
			if(2<=GetElapsedTime(dsp_timer_tick))
			{
				if (ak7739_init_reg_cust(ak7739) == TRUE)				
				{
					ak7739_set_status(ak7739, STATUS_STANDBY);
					dev->r_Dsp->d_state =DSP_TEST_INIT_6;							
				}				
				dsp_timer_tick = GetTickCount();	
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_5\n\r");
			}
			break;
		case DSP_TEST_INIT_6:
			if(10<=GetElapsedTime(dsp_timer_tick))
			{
				set_adc_soft_mute(ak7739, ADC_1, MUTE_ON);	
				set_adc_soft_mute(ak7739, ADC_2, MUTE_ON);	
				set_dac_soft_mute(ak7739, DAC_1, MUTE_ON);
				dev->r_Dsp->d_state =DSP_TEST_INIT_7;
				//check loop which DSP is normal		//5 times retry
				if((check_ak7739_reg(ak7739))&&(dev->r_Dsp->d_act_cnt <=5))
				{
					dev->r_Dsp->d_state = DSP_TEST_INIT_1;
					AKPRINTF(DBG_MSG5_1,"DSP_TEST_FAIL \n\r");
				}
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_6\n\r");
				dsp_timer_tick = GetTickCount();
			}
			break;
		case DSP_TEST_INIT_7:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				dev->r_Dsp->d_act_cnt =0;
				set_dac_soft_mute(ak7739, DAC_2, MUTE_ON);	
				dev->r_Dsp->d_state =DSP_TEST_INIT_8;
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_7\n\r");
				dsp_timer_tick = GetTickCount();
			}
			break;
		case DSP_TEST_INIT_8:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{
				ret = ak7739_set_useCase(ak7739, AVNT_USECASE_AUDIOPATH_MEDIA);
				dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
				dsp_timer_tick = GetTickCount();
				AKPRINTF(DBG_MSG5_1,"DSP_TEST_INIT_8\n\r");
				
				if(ret < 0)        
					akdbgprt_err("Usecase setting fail \n");
				dev->r_Dsp->d_state =DSP_TEST_INIT_9;				
			}
			break;
		case DSP_TEST_INIT_9:
			if(20<=GetElapsedTime(dsp_timer_tick))
			{				
				dev->r_Dsp->d_state =DSP_INIT_UNMUTE_1;
				dev->r_Dsp->d_state_lock =OFF;
			}
			break;
	}
#if 1
	//DSP_ACTIVE -> DSP_INIT_14
	if((DSP_ACTIVE<=dev->r_Dsp->d_state)&&
		(((SYS_ACTIVE_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_SCREENOFF_2_MODE_IDLE)))) //||((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE))))    //KHI 15MIN CONDITION DELETE
		{
			if(dev->r_Dsp->d_musical_rhythm_enable ==ON)
			{
				if(99<=GetElapsedTime(spaectrum_timer_tick))
				{
					int specB1, specB2, specB3, specB4, specB5, specB6, specB7, specB8;		
					get_ae_spaectrumAnalysis(ak7739, &specB1, &specB2, &specB3, &specB4, &specB5, &specB6, &specB7, &specB8);	
					if(((specB1!=(-180))||(specB2!=(-180)))&&((dev->r_Dsp->d_audo_mode ==AVNT_USECASE_AUDIOPATH_BT_A2DP)||
						(dev->r_Dsp->d_audo_mode ==AVNT_USECASE_AUDIOPATH_MEDIA)))
					{
		//				AKPRINTF(DBG_MSG1,"band[%d][%d][%d][%d] [%d][%d][%d][%d]dB\n", specB1, specB2, specB3, specB4, specB5, specB6, specB7, specB8);
						tmp = calcurate_spaectrum((uint8)(180+specB1),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_120HZ(tmp&0x0F);
						tmp = calcurate_spaectrum_band1((uint8)(180+specB2),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_250HZ(tmp&0x0F);
						tmp = calcurate_spaectrum_band1((uint8)(180+specB3),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_500HZ(tmp&0x0F);
						tmp = calcurate_spaectrum_band1((uint8)(180+specB4),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_1000HZ(tmp&0x0F);
						tmp = calcurate_spaectrum_band1((uint8)(180+specB5),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_1500HZ(tmp&0x0F);
						tmp = calcurate_spaectrum_band1((uint8)(180+specB6),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_2000HZ(tmp&0x0F);
						tmp = calcurate_spaectrum_band1((uint8)(180+specB7),e2p_save_data.E2P_MEDIA_VOL);
						IlPutTxIHU_MusicLoudness_6000HZ(tmp&0x0F);

					}
					dev->r_Dsp->d_musical_rhythm_status = ON;
					spaectrum_timer_tick= GetTickCount();
				}
				
			}
			else
			{
				if(dev->r_Dsp->d_musical_rhythm_status)
				{
					if(99<=GetElapsedTime(spaectrum_timer_tick))
					{
						IlPutTxIHU_MusicLoudness_120HZ(0);
						IlPutTxIHU_MusicLoudness_250HZ(0);
						IlPutTxIHU_MusicLoudness_500HZ(0);
						IlPutTxIHU_MusicLoudness_1000HZ(0);
						IlPutTxIHU_MusicLoudness_1500HZ(0);
						IlPutTxIHU_MusicLoudness_2000HZ(0);
						IlPutTxIHU_MusicLoudness_6000HZ(0);
						//SrvMcanSendReq(RRM_6_540_IDX);
						dev->r_Dsp->d_musical_rhythm_status = OFF;
						spaectrum_timer_tick= GetTickCount();
					}
				}
			}
		}
#endif
}

static void ak7739_set_beep_vol(ak7739_priv *ak7739,DEVICE *dev)
{
	int tmp0=0,tmp1=0,tmp2=0,tmp3=0;

	if((dev->r_Beep->fl_beep!=dev->r_Beep->pre_fl_beep)||(dev->r_Beep->fr_beep!=dev->r_Beep->pre_fr_beep)||
		(dev->r_Beep->rl_beep!=dev->r_Beep->pre_rl_beep)||(dev->r_Beep->rr_beep!=dev->r_Beep->pre_rr_beep))
		{
			if(dev->r_Beep->fl_beep != ON)
				tmp0 = -145;
			if(dev->r_Beep->fr_beep != ON)
				tmp1 = -145;
			if(dev->r_Beep->rl_beep != ON)
				tmp2 = -145;
			if(dev->r_Beep->rr_beep != ON)
				tmp3 = -145;

			dev->r_Beep->pre_fl_beep = dev->r_Beep->fl_beep;
			dev->r_Beep->pre_fr_beep = dev->r_Beep->fr_beep;
			dev->r_Beep->pre_rl_beep = dev->r_Beep->rl_beep;
			dev->r_Beep->pre_rr_beep = dev->r_Beep->rr_beep;
			
			set_ae_outMix(ak7739, AE_OutMixFL, 0, 0, 0, tmp0, 0); //all 0dB
			set_ae_outMix(ak7739, AE_OutMixFR, 0, 0, 0, tmp1, 0); //all 0dB
			set_ae_outMix(ak7739, AE_OutMixRL, 0, 0, -145, tmp2, 0); //all 0dB
			set_ae_outMix(ak7739, AE_OutMixRR, 0, 0, -145, tmp3, 0); //all 0dB

//			AKPRINTF(DBG_MSG1,"fl:[%d]fr:[%d]rl:[%d]rr:[%d] [%d][%d][%d][%d] \n", dev->r_Beep->fl_beep, dev->r_Beep->fr_beep, dev->r_Beep->rl_beep, dev->r_Beep->rr_beep
//						,tmp0,tmp1,tmp2,tmp3);
		}
}

