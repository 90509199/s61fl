/*
 * beep_proc.h
 *
 *  Created on: 2018. 12. 31.
 *      Author: Digen
 */

#ifndef BEEP_PROC_H_
#define BEEP_PROC_H_

#define BEEP_ON_TIME							100
#define BEEP_4HZ_TIME						250
#define BEEP_2HZ_TIME						500
#define BEEP_1HZ_TIME						1000

#define BEEP_REMAIN_TIME					500


typedef enum {
	FRONT_RADAR_DETECT = 1,
	REAR_RADAR_DETECT,
}RADAR_DETECT;


typedef enum {
	BEEP_TYPE_OFF = 0,
	BEEP_TYPE_ON,
	BEEP_TYPE_4HZ,
	BEEP_TYPE_2HZ,
	BEEP_TYPE_1HZ,
}BEEP_TYPE;

void Beep_Proc(DEVICE *dev);


#endif /* BEEP_PROC_H_ */
