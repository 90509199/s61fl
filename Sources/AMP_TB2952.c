/*
 * AMP_TB2952.c
 *
 *  Created on: 2018. 10. 19.
 *      Author: Digen
 */

#include "includes.h"

#define AMP_ERR_RETRY			100

#define DETECT_STANDBY_ON			0x80
#define CH_TWEETER_OPEN				0x20
#define DETECT_LOW_VOLTAGE			0x04
uint32 u32AmpI2CErrCnt=0;
static volatile uint32 internal_amp_timer_tick = 0;
static volatile uint32 internal_amp_timer_tick1 = 0;

static volatile uint8 i2c_read_buffer[8];
static volatile uint8 i2c_write_buffer[2];

static uint8 amp_set_Init(DEVICE *dev);

uint8 AMP_Init(DEVICE *dev)
{
	uint8 tx_data[5] ={0,}; 
	uint8 result=I2C_BUSY;
	static uint8 error_cnt=0;
	
		//0xD8
	result = FlexIO_I2C_Write(0x6C,0x01,0x06);	//0x6C
	delay_ms(1);
	result = FlexIO_I2C_Write(0x6C,0x02,0x10);
	delay_ms(1);
	result = FlexIO_I2C_Write(0x6C,0x03,0x00);

	if(result != STATUS_SUCCESS)
	{
		DPRINTF(DBG_MSG1," %s error %d %d\n\r", __FUNCTION__,result,error_cnt);
		
		if(error_cnt<3)
		{
			error_cnt ++;
			MCU_AMP_ST_BY(OFF);
			MCU_AMP_MUTE(ON);	//add
			delay_ms(150);
			dev->r_Dsp->d_state = DSP_INIT_1;
			if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
			dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
		}
		else
			error_cnt =0;
	}
	else
	{
		error_cnt =0;
		DPRINTF(DBG_MSG1," %s ok %d\n\r", __FUNCTION__,result);
		//ok
	}

	return result;	
}

#ifdef INTERNAL_AMP
uint8 AMP_proc(DEVICE *dev)
{
	switch(dev->r_System->amp_state)
	{
		case INTERNAL_AMP_IDLE:
			internal_amp_timer_tick = GetTickCount();	
		break;
		case INTERNAL_AMP_INIT_START:
			u32AmpI2CErrCnt=0;
			if(2<=GetElapsedTime(internal_amp_timer_tick))
			{
				if(SYS_BATTERY_LOW_MODE_IDLE!=dev->r_System->s_state)
					amp_set_Init(dev);
				internal_amp_timer_tick = GetTickCount();		
			}
			break;
		case INTERNAL_AMP_INIT_FINISH:
			if(2<=GetElapsedTime(internal_amp_timer_tick))
			{
			dev->r_System->amp_state = INTERNAL_AMP_ACTIVE;
			dev->r_Dsp->d_state =DSP_INIT_15;
			MCU_AMP_MUTE(OFF);						//SOUND ON
			internal_amp_timer_tick = GetTickCount();	
			DPRINTF(DBG_MSG1,"goto amp_active \n\r");
			}
			break;	
		case INTERNAL_AMP_ACTIVE:
			#if 1
			//5000->3000  #if 1	//JANG_210819  monitoring amp status every 3s
			if(3000<=GetElapsedTime(internal_amp_timer_tick)&&
				((SYS_STANDBY_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE)))
				{
					if (STATUS_ERROR!=FlexIO_I2C_Read(0x6C,0x0,8))
					{
						if((!(i2c_read_buffer[2]&DETECT_STANDBY_ON))||(i2c_read_buffer[4]&DETECT_LOW_VOLTAGE)||(i2c_read_buffer[0]&CH_TWEETER_OPEN)
							||(i2c_read_buffer[1]&CH_TWEETER_OPEN)||(i2c_read_buffer[2]&CH_TWEETER_OPEN)||(i2c_read_buffer[3]&CH_TWEETER_OPEN))	//check standby and low voltage detect
						{
							dev->r_System->amp_state = INTERNAL_AMP_INIT_START;
						}
					}
					else
					{
						u32AmpI2CErrCnt++;
						if (u32AmpI2CErrCnt>20)
						{
							dev->r_System->amp_state = INTERNAL_AMP_INIT_START;
						}
					}
					internal_amp_timer_tick = GetTickCount();
				}
			#else
			internal_amp_timer_tick = GetTickCount();	
			#endif	
			break;	
	}
	
}
#endif

static uint8 amp_set_Init(DEVICE *dev)
{
	static uint16 try_cnt=0;
	static uint16 error_cnt = 0;
	static unsigned int amp_init_reg_state = AMP_INIT_REG_0;	
	uint8 tx_data[5] ={0,}; 
	uint8 result=I2C_BUSY;
	
	switch(amp_init_reg_state)
	{
		case AMP_INIT_REG_0:
			//check error counter
			if(10<=error_cnt)
			{
				MCU_AMP_ST_BY(OFF);
				MCU_AMP_MUTE(ON);
				amp_init_reg_state = AMP_INIT_REG_5;//error
				internal_amp_timer_tick1 = GetTickCount();
			}
			else
			{
				amp_init_reg_state = AMP_INIT_REG_1;//initialize
			}
			break;
		case AMP_INIT_REG_1:
			result = FlexIO_I2C_Write(0x6C,0x01,0x06);	//0x6C
			
			if(result != STATUS_SUCCESS)
			{
				error_cnt ++;
				if(AMP_ERR_RETRY<=try_cnt)
				{
					error_cnt =0;
					amp_init_reg_state = AMP_INIT_REG_4;
				}
				else
					amp_init_reg_state = AMP_INIT_REG_0;
				DPRINTF(DBG_MSG1," %s REG_1 error cnt %d \n\r", __FUNCTION__,error_cnt);
			}
			else
			{
				amp_init_reg_state = AMP_INIT_REG_2;
				DPRINTF(DBG_MSG1," %s 1 ok\n\r", __FUNCTION__);
			}
			break;
		case AMP_INIT_REG_2:
			result = FlexIO_I2C_Write(0x6C,0x02,0x10);
			
			if(result != STATUS_SUCCESS)
			{
				error_cnt ++;
				if(AMP_ERR_RETRY<=try_cnt)
				{
					error_cnt =0;
					amp_init_reg_state = AMP_INIT_REG_4;
				}
				else
					amp_init_reg_state = AMP_INIT_REG_0;
				DPRINTF(DBG_MSG1," %s REG_2 error cnt %d \n\r", __FUNCTION__,error_cnt);
			}
			else
			{
				amp_init_reg_state = AMP_INIT_REG_3;
				DPRINTF(DBG_MSG1," %s 2 ok\n\r", __FUNCTION__);
			}
			break;
		case AMP_INIT_REG_3:
			result = FlexIO_I2C_Write(0x6C,0x03,0x00);
			
			if(result != STATUS_SUCCESS)
			{
				error_cnt ++;
				if(AMP_ERR_RETRY<=try_cnt)
				{
					error_cnt =0;
					amp_init_reg_state = AMP_INIT_REG_4;
				}
				else
					amp_init_reg_state = AMP_INIT_REG_0;
				DPRINTF(DBG_MSG1," %s REG_3 error cnt %d \n\r", __FUNCTION__,error_cnt);
			}
			else
			{
				amp_init_reg_state = AMP_INIT_REG_4;
				DPRINTF(DBG_MSG1," %s 3 ok\n\r", __FUNCTION__);
			}
			break;
		case AMP_INIT_REG_4:
			if(error_cnt==0)
			{
				//clear
				try_cnt = 0;
				error_cnt = 0;
			amp_init_reg_state = AMP_INIT_REG_0;
			dev->r_System->amp_state = INTERNAL_AMP_INIT_FINISH;
				DPRINTF(DBG_MSG1," %s finish \n\r", __FUNCTION__);
			}
			else
			{
				//three more retry//clear
				amp_init_reg_state = AMP_INIT_REG_6;
				DPRINTF(DBG_MSG1," %s more retry amp\n\r", __FUNCTION__);
			}			
			break;
		case AMP_INIT_REG_5:
			if(20<=GetElapsedTime(internal_amp_timer_tick1))
			{
				MCU_AMP_ST_BY(ON);
				MCU_AMP_MUTE(OFF);
				try_cnt++;
				error_cnt =0;
				internal_amp_timer_tick1 = GetTickCount();	
				amp_init_reg_state = AMP_INIT_REG_6;
			}
			break;
		case AMP_INIT_REG_6:
			if(5000<=GetElapsedTime(internal_amp_timer_tick1))
			{
				try_cnt = 0;
				error_cnt =0;
				internal_amp_timer_tick1 = GetTickCount();	
				amp_init_reg_state = AMP_INIT_REG_0;
			}
			break;
	}

	return result;	
}

uint8_t FlexIO_I2C_Read(uint8_t device_addr,uint8_t register_address,uint8_t count)
{
	static uint8_t read_value=0;
	uint32_t result;
	
	result=FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, device_addr);
	if(result)
	{
		DPRINTF(DBG_MSG1,"raddr %02x %02x result:%04x\r\n",device_addr,register_address,result);
		return STATUS_ERROR;
	}

	result=FLEXIO_I2C_DRV_MasterReceiveDataBlocking(&i2cMasterState, i2c_read_buffer, count, true,1000);
	delay_ms(1);
	if(result)
	{
		DPRINTF(DBG_MSG1,"amp rrecv %02x %02x result:%04x errorcnt:%d\r\n",device_addr,register_address,result,u32AmpI2CErrCnt);
		return STATUS_ERROR;
	}
//	DPRINTF(DBG_MSG1,"FlexIO_I2C_Read %x %x  %x %x %x\r\n",i2c_read_buffer[0],i2c_read_buffer[1],i2c_read_buffer[2],i2c_read_buffer[3],i2c_read_buffer[4]);
	return STATUS_SUCCESS;
}

uint8_t FlexIO_I2C_Write(uint8_t device_addr,uint8_t register_address,uint8_t data)
{
	uint32_t result;
	i2c_write_buffer[0]=register_address;
	i2c_write_buffer[1]=data;

	result=FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, device_addr);
	if(result)
	{
		DPRINTF(DBG_MSG1,"waddr %02x %02x %02x result:%04x\r\n",device_addr,register_address,data,result);
		return STATUS_ERROR;
	}
	result=FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, i2c_write_buffer, 2, true,1000);

	if(result)
	{
		DPRINTF(DBG_MSG1,"wsend %02x %02x %02x result:%04x\r\n",device_addr,register_address,data,result);
		return STATUS_ERROR;
	}

	return STATUS_SUCCESS;
}


