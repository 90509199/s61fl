/*
*******************************************************************************
**
**  This device driver was created by Applilet2 for V850ES/Sx3
**  32-Bit Single-Chip Microcontrollers
**
**  Copyright(C) NEC Electronics Corporation 2002 - 2018
**  All rights reserved by NEC Electronics Corporation.
**
**  This program should be used on your own responsibility.
**  NEC Electronics Corporation assumes no responsibility for any losses
**  incurred by customers or third parties arising from the use of this file.
**
**  Filename :	user_define.h
**  Abstract :	This file includes user define.
**  APIlib :	Applilet2 for V850ES/Sx3 V2.41 [28 Apr. 2008]
**
**  Device :	uPD70F3367
**
**  Compiler :	CA850
**
**  Creation date:	2018-05-11
**  
*******************************************************************************
*/
#ifndef _MD_USER_DEF_
#define _MD_USER_DEF_
/*
*******************************************************************************
**  Macro define
*******************************************************************************
*/
/* Start user code for definition. Do not edit comment generated here */
#endif
