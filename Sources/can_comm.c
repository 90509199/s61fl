/*
 * can_comm.c
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#include "includes.h"

//#define CPRINTF(lvl, fmt, args...)		DPRINTF(lvl, fmt, ##args)
#define CPRINTF(lvl, fmt, args...)

#ifdef CHECK_TURNLIGHT_TYPE
uint8                  turnlight_type             = 0;
static volatile uint32 check_turnlight_timer_tick = 0;
#endif

// CAN锟斤拷锟截癸拷锟斤拷模式锟斤拷锟斤拷SOC通知
extern uint8  CanKeyWorkMode;
extern DEVICE r_Device;
extern uint8  u8VariantCoding[];

#if 1 // JANG_211116
uint16 prev_seat_status;
uint8  prev_gear_status;
uint32 prev_angle_status;
uint16 prev_speed_status;
uint16 prev_dte_status;
uint16 prev_fuellevel_status_0;
//	uint8	prev_fuellevel_status_1;
uint8  prev_dvr_status;
uint8  prev_seatbelt_status;
uint8  prev_bcm1_status;
uint32 prev_bcm2_status_0;
uint16 prev_bcm2_status_1;
uint8  prev_bcm3_status;
uint16 prev_bcm4_status;
uint8  prev_bsd1_status;
uint8  prev_turnlight_status;
uint32 prev_odo_meter;
uint8  prev_wpc;
uint32 prev_breath_status;
uint8  prev_tbox_alarm;
uint8  prev_drivemode;
uint32 prev_enginespeed;
uint8  prev_ctl;
uint8  prev_rollingcount;
uint8  prev_mfs_heart;
uint8  prev_chime;
uint8  prev_frm3;
uint32 prev_abs_eps5_0;
uint32 prev_abs_eps5_1;
uint32 prev_pm25_0;
uint16 prev_pm25_1;
uint16 prev_avm2;
uint16 prev_fcm2;
uint32 prev_cem_ipm_0;
uint8  prev_cem_ipm_1;
uint8  prev_icm4;
#ifdef CX62C_FUNCTION
uint32 prev_icm3_0;
uint32 prev_icm3_1;
uint32 prev_icm4_0;
#endif
uint16 prev_plg;
uint16 prev_cgw_abm;
uint32 prev_afu;
uint8  prev_avm_apa2;
uint8  prev_avm_apa3;
uint8  prev_cem_peps;
uint32 prev_amp1;
#endif

#ifdef CX62C_FUNCTION
uint8 tpdvd_cmd[30]; // 20->30
QUE_t tpdvd_cmd_q;

void CANTp_Init_data()
{
    init_q(&tpdvd_cmd_q, &tpdvd_cmd[0], sizeof(tpdvd_cmd));
}

uint16 check_tpdvd_cmd(void)
{
    return q_data_size(&tpdvd_cmd_q);
}
#endif

void CAN_Comm_Proc(DEVICE *dev)
{
    uint8  tmp8    = 0;
    uint16 tmp16   = 0;
    uint32 tmp32   = 0;
    uint32 tmp32_1 = 0;

    static uint8 active_press_watch         = MSG_KEY_RELEASE;
    static uint8 active_press_menu          = MSG_KEY_RELEASE;
    static uint8 active_press_up            = MSG_KEY_RELEASE;
    static uint8 active_press_dn            = MSG_KEY_RELEASE;
    static uint8 active_press_left          = MSG_KEY_RELEASE;
    static uint8 active_press_right         = MSG_KEY_RELEASE;
    static uint8 active_press_ok            = MSG_KEY_RELEASE;
    static uint8 active_press_mode          = MSG_KEY_RELEASE;
    static uint8 active_press_voice_active  = MSG_KEY_RELEASE;
    static uint8 active_press_voice_plus    = MSG_KEY_RELEASE;
    static uint8 active_press_voice_minus   = MSG_KEY_RELEASE;
    static uint8 active_press_hangup        = MSG_KEY_RELEASE;
    static uint8 active_press_answer        = MSG_KEY_RELEASE;
    static uint8 active_press_answer_hangup = MSG_KEY_RELEASE;
    static uint8 active_press_last_song     = MSG_KEY_RELEASE;
    static uint8 active_press_next_song     = MSG_KEY_RELEASE;

    uint8 idx       = 0;
    uint8 changeFlg = 0;

    //	if(dev->r_Tuner->t_state<TUNER_ACTIVE_0)
    //		return;

#ifdef CX62C_FUNCTION
    CANTp_Comm(dev);
#endif

#if 0
	for(idx  = 0 ; idx < RRM_RX_MAX ; idx++)
	{
		changeFlg = SrvMcanGetChangeFlg(idx);

		if(0 != changeFlg)
		{
			SrvMcanClrChangeFlg(idx);

			switch(idx)
			{
				case CGW_EMS_G_280_IDX:
//					SrvMcanTestCgwEmsG_280();
					//can problem
#if 1 // JANG_211231
					tmp8 = (CanIfGet_gCanIf_CGW_EMS_G_ISSSts()<<4)|(CanIfGet_gCanIf_CGW_EMS_G_TargetGearPosition()<<1)|CanIfGet_gCanIf_CGW_EMS_G_EngineSts();
#else
					tmp8 = (CanIfGet_gCanIf_CGW_EMS_G_TargetGearPosition()<<1)|CanIfGet_gCanIf_CGW_EMS_G_EngineSts();
#endif
					tmp32 = (uint32)tmp8<<16;
					tmp8 = CanIfGet_gCanIf_CGW_EMS_G_EngineSpeed_0();
					tmp32 |= (uint32)tmp8<<8;
					tmp8 = CanIfGet_gCanIf_CGW_EMS_G_EngineSpeed_1();
					tmp32 |= (uint32)tmp8;
					
					//can filter
					if(prev_enginespeed!=(tmp32/4))
					{
						CAR_SIG_ENGINESPEED.flag = ON;
						CAR_SIG_ENGINESPEED.trycnt = 0;
						CAR_SIG_ENGINESPEED.buf[1] = (uint8)(tmp32>>16);
						CAR_SIG_ENGINESPEED.buf[2] = (uint8)(tmp32>>8);
						CAR_SIG_ENGINESPEED.buf[3] = (uint8)tmp32;
						CPRINTF(DBG_MSG1,"280 CAR_SIG_ENGINESPEED %x\n\r",tmp32);
						prev_enginespeed = (tmp32/4);
					}
										
					tmp8 = CanIfGet_gCanIf_CGW_EMS_G_EngineCoolantTemperture();

					if(prev_ctl!=tmp8)
					{
						CAR_SIG_CLT.flag = ON;
						CAR_SIG_CLT.trycnt = 0;
						CAR_SIG_CLT.buf[1] = (uint8)tmp8;
						prev_ctl = tmp8;
					}
					
					tmp8 = CanIfGet_gCanIf_CGW_EMS_G_FuelRollingCounter();

					if(prev_rollingcount!=tmp8)
					{
						CAR_SIG_ROLLCOUNT.flag = ON;
						CAR_SIG_ROLLCOUNT.trycnt = 0;
						CAR_SIG_ROLLCOUNT.buf[1] = (uint8)tmp8;
						CPRINTF(DBG_MSG1,"280 CAR_SIG_ROLLCOUNT %x\n\r",tmp8);
						prev_rollingcount = tmp8;
					}
					break;
					
				case ABS_ESP_1_2E9_IDX:
//					SrvMcanTestABSESP1_2E9();
			
					if(CanIfGet_gCanIf_ABS_ESP_1_Checksum()!=SrvMcanGetCheckSum(ABS_ESP_1_2E9_IDX)){
//						CPRINTF(DBG_MSG1,"ABS_ESP_1_2E9_IDX checksum error %x %x\n\r",CanIfGet_gCanIf_ABS_ESP_1_Checksum(),SrvMcanGetCheckSum(ABS_ESP_1_2E9_IDX));
					}
					else
					{
						tmp8 = CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSig_0();		//speed
						tmp16 = (uint16)tmp8<<8;
						tmp8 = CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSig_1();
						tmp16 |= (uint16)tmp8;
			
						if((prev_speed_status !=tmp16)&&(CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSigValidData()==0))
						{
							CAR_SIG_SPEED.flag = ON;
							CAR_SIG_SPEED.trycnt = 0;
							CAR_SIG_SPEED.buf[1] = (uint8)(tmp16>>8);
							CAR_SIG_SPEED.buf[2] = (uint8)tmp16;
							CPRINTF(DBG_MSG1,"send speed %d %d\n\r",prev_speed_status,tmp16);
							prev_speed_status = tmp16;
						}
						//check speed
#ifdef INTERNAL_AMP
#ifdef SVCD_ENABLE
						if(CanIfGet_gCanIf_ABS_ESP_1_VehicleSpeedVSOSigValidData()==0)
						{
							dev->r_System->svcd_speed = tmp8 = (uint8)(tmp16/16);
						}
#endif
#endif
					}
					break;	
					
				case CGW_TCU_G_301_IDX:
//					SrvMcanTestCgwTcuG_301();
					tmp8  = (CanIfGet_gCanIf_CGW_TCU_G_DriverMode()<<6);					//drive mode
					tmp8 = CanIfGet_gCanIf_CGW_TCU_G_GBPositoionDisplay()|tmp8;		//change msg
					dev->r_System->gear_status = CanIfGet_gCanIf_CGW_TCU_G_GBPositoionDisplay();

					if(prev_gear_status != tmp8)
					{
						//add message
						CAR_SIG_GEAR.flag = ON;
						CAR_SIG_GEAR.trycnt = 0;
						CAR_SIG_GEAR.buf[1] = tmp8;
						CPRINTF(DBG_MSG1,"CGW_TCU_G_301_IDX information %x %x\n\r",tmp8,prev_gear_status);
						prev_gear_status = tmp8;
					}					
					break;
					
				case FRM_3_305_IDX:				//default value	 00 00 00 00 00 01 70 1F
//					SrvMcanTestFrm3_305();
					if(CanIfGet_gCanIf_FRM_3_CRC()!=SrvMcanGetRxMsgCrc(FRM_3_305_IDX))
					{
						CPRINTF(DBG_MSG1,"FRM_3_305_IDX crc error %x %x\n\r",CanIfGet_gCanIf_FRM_3_CRC(),SrvMcanGetRxMsgCrc(FRM_3_305_IDX));
					}
					else
					{
						tmp8 = (CanIfGet_gCanIf_FRM_3_TimeGapSet_DVD()<<6)|(CanIfGet_gCanIf_FRM_3_TimeGapLastSet_DVD()<<5)|
							(CanIfGet_gCanIf_FRM_3_FCW_ON_OFF_sts()<<4)|(CanIfGet_gCanIf_FRM_3_FCW_OPTION_sts()<<2)|
							(CanIfGet_gCanIf_FRM_3_AEB_ON_OFF_sts()<<1)|CanIfGet_gCanIf_FRM_3_DistanceWarning_on_off_sts();

						if(prev_frm3 !=tmp8)
						{
							CAR_SIG_FRM3.flag = ON;
							CAR_SIG_FRM3.trycnt = 0;
							CAR_SIG_FRM3.buf[1] = (uint8)tmp8;
							CPRINTF(DBG_MSG1,"FRM_3_305_IDX %x \n\r",tmp8);
							prev_frm3 = tmp8;
						}
					}
					break;
			
				case FCM_2_307_IDX:				//default value	 10 02 00 02 00 10 00 AC
//					SrvMcanTestFcm2_307();
					if(CanIfGet_gCanIf_FCM_2_CRCCheck()!=SrvMcanGetRxMsgCrc(FCM_2_307_IDX))
					{
						CPRINTF(DBG_MSG1,"FCM_2_307_IDX crc error %x %x\n\r",CanIfGet_gCanIf_FCM_2_CRCCheck(),SrvMcanGetRxMsgCrc(FCM_2_307_IDX));
					}
					else
					{
						tmp8 =(CanIfGet_gCanIf_FCM_2_LDW_LKA_LaneAssitfee()<<6)|(CanIfGet_gCanIf_FCM_2_HMAoNoFFSts()<<4)|
							(CanIfGet_gCanIf_FCM_2_LDW_LKA_Senaitivityfeedback()<<2)|CanIfGet_gCanIf_FCM_2_SLAOnOffsts();
						tmp16 = (uint16)tmp8<<8;
						tmp8 = (CanIfGet_gCanIf_FCM_2_TJA_ICA_ON_OFF_STS()<<2)|CanIfGet_gCanIf_FCM_2_LDWOnOffSts();
						tmp16 |=(uint16)tmp8;

						if(prev_fcm2 !=tmp16)
						{
							CAR_SIG_FCM2.flag = ON;	
							CAR_SIG_FCM2.trycnt= 0;	
							CAR_SIG_FCM2.buf[1] = (uint8)(tmp16>>8);
							CAR_SIG_FCM2.buf[2] = (uint8)tmp16;
							CPRINTF(DBG_MSG1,"FCM_2_307_IDX %x \n\r",tmp16);
							prev_fcm2 = tmp16;
						}
					}
					break;
					
				case CGW_ABM_G_320_IDX:
//					SrvMcanTestCGW_ABM_G_320();
					if(CanIfGet_gCanIf_CGW_ABM_G_Checksum()!=SrvMcanGetCheckSum(CGW_ABM_G_320_IDX))
					{
						CPRINTF(DBG_MSG1,"CGW_ABM_G_320_IDX checksum error %x %x\n\r",CanIfGet_gCanIf_CGW_ABM_G_Checksum(),SrvMcanGetCheckSum(CGW_ABM_G_320_IDX));
					}
					else
					{
						tmp8 = CanIfGet_gCanIf_CGW_ABM_G_PsngrSeatBeltWarning();	
						tmp16 = (uint16)tmp8<<8;
						tmp8 = CanIfGet_gCanIf_CGW_ABM_G_CrashOutputSts();
						tmp16 |= (uint16)tmp8;

						if(prev_cgw_abm!=tmp16)
						{
							CAR_SIG_CGW_ABM_G.flag = ON;
							CAR_SIG_CGW_ABM_G.trycnt = 0;
							CAR_SIG_CGW_ABM_G.buf[1] = (uint8)(tmp16>>8);
							CAR_SIG_CGW_ABM_G.buf[2] = (uint8)tmp16;
							CPRINTF(DBG_MSG1,"CGW_ABM_G_320_IDX %x \n\r",tmp16);
							prev_cgw_abm =tmp16;
						}
					}
					break;

#ifdef CHANG_TO_MFS_3
				case MFS_2_323_IDX:
#else
				case MFS_2_321_IDX:
#endif
		
					if((dev->r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)||(dev->r_System->s_state == SYS_STANDBY_MODE_IDLE))
						return;
					if(CanIfGet_gCanIf_MFS_2_MFS_heatingthesteeringwheel()==0x01)
					{
						//menu
						key_packet.flag  = ON;
						dev->r_System->key_active = ON;							
						key_packet.buf[0] = (uint8)(KEY_DEF_WECHAT>>8);
						key_packet.buf[1] = KEY_DEF_WECHAT;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_watch =	MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"Watch Key Press\n\r");
					}
					else
					{
						if(active_press_watch == MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;							
							key_packet.buf[0] = (uint8)(KEY_DEF_WECHAT>>8);
							key_packet.buf[1] = KEY_DEF_WECHAT;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_watch =	MSG_KEY_RELEASE;
							DPRINTF(DBG_MSG5,"Watch Key Release\n\r");				
						}
					}
					break;
					
				case MFS_4_325_IDX:
//					SrvMcanTestMFS_4_325();
					tmp8 =CanIfGet_gCanIf_MFS_4_MFS_Heartrate();

					if(prev_mfs_heart !=tmp8)
					{
						CAR_SIG_MFS_HEARTRATE.flag = ON;						
						CAR_SIG_MFS_HEARTRATE.trycnt = 0;
						CAR_SIG_MFS_HEARTRATE.buf[1] = tmp8;
						prev_mfs_heart = tmp8;
					}

					//prohibit wheel remote control at standby mode
					if(dev->r_System->s_state == SYS_STANDBY_MODE_IDLE)
						return;

					if(CanIfGet_gCanIf_MFS_4_MFS_Voice_Activation()==0x01)
					{
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = 2;
						key_packet.buf[1] = 0x46;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_voice_active =	MSG_KEY_PRESS;
						if(dev->r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)
							dev->r_System->voice_key_active = ON;
						DPRINTF(DBG_MSG5,"VOICE ACTIVE Key Press\n\r");
					}
					else
					{
						if(active_press_voice_active ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = 2;
							key_packet.buf[1] = 0x46;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_voice_active =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"VOICE ACTIVE Key Release\n\r");
						}
					}
					
					//prohibit wheel remote control at screen saver
					if(dev->r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)
						return;

					if(CanIfGet_gCanIf_MFS_4_MFS_Menu()==0x01)
					{
						//menu
						key_packet.flag  = ON;
						dev->r_System->key_active = ON;							
						key_packet.buf[0] = (uint8)(KEY_DEF_MENU>>8);
						key_packet.buf[1] = KEY_DEF_MENU;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_menu =	MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"MENU Key Press\n\r");
					}
					else
					{
						if(active_press_menu == MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;							
							key_packet.buf[0] = (uint8)(KEY_DEF_MENU>>8);
							key_packet.buf[1] = KEY_DEF_MENU;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_menu =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"MENU Key Release\n\r");				
						}
					}

					if(CanIfGet_gCanIf_MFS_4_MFS_RRM_Up_Zoomin()==0x01)
					{
						//up
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_UP>>8);
						key_packet.buf[1] = KEY_DEF_UP;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_up =	MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"UP Key Press\n\r");
					}
					else
					{
						if(active_press_up ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_UP>>8);
							key_packet.buf[1] = KEY_DEF_UP;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_up =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"UP Key Release\n\r");
						}
					}

					if(CanIfGet_gCanIf_MFS_4_MFS_RRM_Down_ZoomOut()==0x01)
					{
						//dn
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_DN>>8);
						key_packet.buf[1] = KEY_DEF_DN;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_dn = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"DN Key Press\n\r");
					}
					else
					{
						if(active_press_dn ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_DN>>8);
							key_packet.buf[1] = KEY_DEF_DN;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_dn =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"DN Key Release\n\r");
						}
					}

					if(CanIfGet_gCanIf_MFS_4_MFS_RRM_Left()==0x01)
					{
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_LEFT>>8);
						key_packet.buf[1] = KEY_DEF_LEFT;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_left = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"LEFT Key Press\n\r");
					}
					else
					{
						if(active_press_left ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_LEFT>>8);
							key_packet.buf[1] = KEY_DEF_LEFT;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_left =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"LEFT Key Release\n\r");
						}
					}
					
					if(CanIfGet_gCanIf_MFS_4_MFS_RRM_Right()==0x01)
					{
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_RIGHT>>8);
						key_packet.buf[1] = KEY_DEF_RIGHT;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_right = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"RIGHT Key Press\n\r");
					}
					else
					{
						if(active_press_right ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_RIGHT>>8);
							key_packet.buf[1] = KEY_DEF_RIGHT;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_right =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"RIGHT Key Release\n\r");
						}
					}
					
					if(CanIfGet_gCanIf_MFS_4_MFS_RRM_Confirm()==0x01)
					{
						//ok
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_OK>>8);
						key_packet.buf[1] = KEY_DEF_OK;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_ok =	MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"OK Key Press\n\r");
					}
					else
					{
						if(active_press_ok ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_OK>>8);
							key_packet.buf[1] = KEY_DEF_OK;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_ok =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"OK Key Release\n\r");
						}
					}
						
					if(CanIfGet_gCanIf_MFS_4_MFS_Mute()==0x01)
					{
						dev->r_System->key_active = ON;
						key_packet.flag = ON;
						key_packet.buf[0] = 1;
						key_packet.buf[1] = 0x3C;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_mode = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"MODE Key Press\n\r");
					}
					else
					{
						if(active_press_mode ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = 1;
							key_packet.buf[1] = 0x3C;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_mode =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"MODE Key Release\n\r");
						}
					}
								
					if(CanIfGet_gCanIf_MFS_4_MFS_Voice_ADD()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_VOL_UP>>8);
						key_packet.buf[1] = KEY_DEF_VOL_UP;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_voice_plus =	MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"VOICE PLUS Key Press\n\r");
					}
					else
					{
						if(active_press_voice_plus ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_VOL_UP>>8);
							key_packet.buf[1] = KEY_DEF_VOL_UP;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_voice_plus =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"VOICE PLUS Key Release\n\r");
						}
					}

					if(CanIfGet_gCanIf_MFS_4_MFS_Voice_Jian()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_VOL_DN>>8);
						key_packet.buf[1] = KEY_DEF_VOL_DN;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_voice_minus = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"VOICE MINUS Key Pressed\n\r");
					}
					else
					{
						if(active_press_voice_minus ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_VOL_DN>>8);
							key_packet.buf[1] = KEY_DEF_VOL_DN;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_voice_minus =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"VOICE MINUS Key Release\n\r");
						}
					}
					
					if(CanIfGet_gCanIf_MFS_4_MFS_HangUpPhone()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_HANGUP>>8);
						key_packet.buf[1] = KEY_DEF_PHONE_HANGUP;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_hangup = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"Hangup Key Press\n\r");
					}
					else
					{
						if(active_press_hangup ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_HANGUP>>8);
							key_packet.buf[1] = KEY_DEF_PHONE_HANGUP;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_hangup =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"Hangup Key Release\n\r");
						}
					}
					 
					if(CanIfGet_gCanIf_MFS_4_MFS_AnswerPhone()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_CALL>>8);
						key_packet.buf[1] = KEY_DEF_PHONE_CALL;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_answer = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"Phone Key Press\n\r");
					}
					else
					{
						if(active_press_answer ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_CALL>>8);
							key_packet.buf[1] = KEY_DEF_PHONE_CALL;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_answer =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"Phone Key Release\n\r");
						}
					}

					if(CanIfGet_gCanIf_MFS_4_MFS_AnswerHangupPhone()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_CALL_HANGUP>>8);
						key_packet.buf[1] = KEY_DEF_PHONE_CALL_HANGUP;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_answer_hangup = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"call-hangup Key Press\n\r");
					}
					else
					{
						if(active_press_answer_hangup ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_CALL_HANGUP>>8);
							key_packet.buf[1] = KEY_DEF_PHONE_CALL_HANGUP;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_answer_hangup =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"call-hangup Key Release\n\r");
						}
					}

					if(CanIfGet_gCanIf_MFS_4_MFS_Previous()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_LAST_SONG>>8);
						key_packet.buf[1] = KEY_DEF_LAST_SONG;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_last_song = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"Last Song Key Press\n\r");
					}
					else
					{
						if(active_press_last_song ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_LAST_SONG>>8);
							key_packet.buf[1] = KEY_DEF_LAST_SONG;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_last_song =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"Last Song Key Release\n\r");
						}
					}
						
					if(CanIfGet_gCanIf_MFS_4_MFS_Next()==0x01)
					{
						key_packet.flag = ON;
						dev->r_System->key_active = ON;
						key_packet.buf[0] = (uint8)(KEY_DEF_NEXT_SONG>>8);
						key_packet.buf[1] = KEY_DEF_NEXT_SONG;
						key_packet.buf[2] = MSG_KEY_PRESS;
						key_packet.buf[3] = MSG_KEY_STEERING;
						active_press_next_song = MSG_KEY_PRESS;
						DPRINTF(DBG_MSG5,"Next Song Key Press\n\r");
					}
					else
					{
						if(active_press_next_song ==MSG_KEY_PRESS)
						{
							//release packet
							key_packet.flag = ON;
							key_packet.buf[0] = (uint8)(KEY_DEF_NEXT_SONG>>8);
							key_packet.buf[1] = KEY_DEF_NEXT_SONG;
							key_packet.buf[2] = MSG_KEY_RELEASE;
							key_packet.buf[3] = MSG_KEY_STEERING;
							active_press_next_song =	MSG_KEY_RELEASE;
							CPRINTF(DBG_MSG5,"Next Song Key Release\n\r");
						}
					}											
					break;
			
				case CGW_SAM_G_340_IDX:
#if 1
//					SrvMcanTestCgwSamG_340();
					tmp8 = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_0();		//speed 
					tmp32 =(uint32)tmp8<<24;
					tmp8 = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_1();
					tmp32 |=(uint32)tmp8<<16;
					tmp32 =(tmp32/4);																			// CAN filter : apply for value divde 4
					tmp8 = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngleSpeed();
					tmp32 |=(uint32)tmp8<<8;
					tmp8 = (CanIfGet_gCanIf_CGW_SAM_G_SASCalibrated()<<1)|CanIfGet_gCanIf_CGW_SAM_G_AngleFailure(); //fixed anglefailure
					tmp32 |=(uint32)tmp8;

					if(prev_angle_status != tmp32)
					{
						CAR_SIG_STR_ANGLE.flag = ON;
						CAR_SIG_STR_ANGLE.trycnt = 0;
						CAR_SIG_STR_ANGLE.buf[1] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_0();
						CAR_SIG_STR_ANGLE.buf[2] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_1();
						CAR_SIG_STR_ANGLE.buf[3] = (uint8)(tmp32>>8);
						CAR_SIG_STR_ANGLE.buf[4] = (uint8)tmp32;
						CPRINTF(DBG_MSG1,"CGW_SAM_G_340_IDX %x \n\r",tmp32);
						prev_angle_status = tmp32;
					}
#endif
					break;
					
				case CGW_EPS_G_380_IDX:				//default value	 00 80 00 00		
//				SrvMcanTestCgwEpsG_380(); 
					 tmp8 = CanIfGet_gCanIf_CGW_EPS_G_EpsmodeSts();
					 if(prev_drivemode !=tmp8)
					 {
					 	CAR_SIG_CGW_EPS_G.flag = ON;						 
						CAR_SIG_CGW_EPS_G.trycnt = 0;
						CAR_SIG_CGW_EPS_G.buf[1] = tmp8;
						  prev_drivemode = tmp8;
					 }
					break;
					
				case CEM_BCM_1_391_IDX:
//					SrvMcanTestCemBcm1_391();
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_1_HazardLightSW()<<1)|CanIfGet_gCanIf_CEM_BCM_1_DriverDoorLockSts();

					if(prev_bcm1_status != tmp8)
					{						
						CAR_SIG_CEM_BCM1.flag= ON;
						CAR_SIG_CEM_BCM1.trycnt = 0;
						CAR_SIG_CEM_BCM1.buf[1] = tmp8;
						prev_bcm1_status = tmp8;
					}
					break;
				case CEM_BCM_2_392_IDX:				//default value	 01 00 00 00 10 00 85 80
//					SrvMcanTestCemBcm2_392();
					//can problem
#if 1
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeTimeSts()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeSts()<<5)|
						(CanIfGet_gCanIf_CEM_BCM_2_KeySts()<<3)|(CanIfGet_gCanIf_CEM_BCM_2_BlankchanginglaneSts()<<2);
					tmp32 =(uint32)tmp8<<24;

					tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_ParkTailLightSts()<<7)|(CanIfGet_gCanIf_CEM_BCM_2_HighBeamSts()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_LowBeamSts()<<5)|
						(CanIfGet_gCanIf_CEM_BCM_2_DRLSts()<<4)|(CanIfGet_gCanIf_CEM_BCM_2_BlankingnumberSts()<<2)|(CanIfGet_gCanIf_CEM_BCM_2_DriverDoorSts()<<1)|
						CanIfGet_gCanIf_CEM_BCM_2_PsngrDoorSts();
					tmp32 |=(uint32)tmp8<<16;
					
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_BonnetSts()<<7)|(CanIfGet_gCanIf_CEM_BCM_2_RHRDoorSts()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_LHRDoorSts()<<5)|
						(CanIfGet_gCanIf_CEM_BCM_2_Trunk_BackDoor_Sts()<<4)|(CanIfGet_gCanIf_CEM_BCM_2_MirrorFoldSts()<<3)|
						(CanIfGet_gCanIf_CEM_BCM_2_External_Welcome_LightSts()<<1)|CanIfGet_gCanIf_CEM_BCM_2_RearFogLightSts();
					tmp32 |=(uint32)tmp8<<8;	

					tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_ReverseGearSwitch()<<6)|(CanIfGet_gCanIf_CEM_BCM_2_LaserhighbeamassistStatus()<<4)|
						(CanIfGet_gCanIf_CEM_BCM_2_FrontFogLightSts()<<3)|(CanIfGet_gCanIf_CEM_BCM_2_AutoUnlockSts()<<2)|CanIfGet_gCanIf_CEM_BCM_2_RemoteLockFeedbackSts();
					tmp32 |=(uint32)tmp8;	

					tmp8 = /*(CanIfGet_gCanIf_CEM_BCM_2_UnlockingBreathStatus()<<5)|*/(CanIfGet_gCanIf_CEM_BCM_2_MirrorFlipSts()<<4)|
						(CanIfGet_gCanIf_CEM_BCM_2_AidTurningIlluminationSts()<<3)|(CanIfGet_gCanIf_CEM_BCM_2_HazardLampForUrgencyBrakeSts()<<2)|
						(CanIfGet_gCanIf_CEM_BCM_2_AutoLockSts()<<1)|CanIfGet_gCanIf_CEM_BCM_2_RemoteTrunkOnlySts();
					tmp16 =((uint16)tmp8<<8);
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_BrakePedalSts()<<4)|CanIfGet_gCanIf_CEM_BCM_2_AlarmMode();	
					tmp16 |=(uint16)tmp8;

//					e2p_save_data.E2P_DOOR_STATUS = CanIfGet_gCanIf_CEM_BCM_2_DriverDoorSts();
						
					if((prev_bcm2_status_0 != tmp32)||(prev_bcm2_status_1 != tmp16))
					{
						//add message
						CAR_SIG_CEM_BCM2.flag= ON;
						CAR_SIG_CEM_BCM2.trycnt = 0;
						CAR_SIG_CEM_BCM2.buf[1] = (uint8)(tmp32>>24);
						CAR_SIG_CEM_BCM2.buf[2] = (uint8)(tmp32>>16);
						CAR_SIG_CEM_BCM2.buf[3] = (uint8)(tmp32>>8);
						CAR_SIG_CEM_BCM2.buf[4] = (uint8)tmp32;
						CAR_SIG_CEM_BCM2.buf[5] = (uint8)(tmp16>>8);
						CAR_SIG_CEM_BCM2.buf[6] = (uint8)tmp16;
						CPRINTF(DBG_MSG1,"CEM_BCM_2_392_IDX %x %x\n\r",tmp32,tmp16);
						prev_bcm2_status_0 = tmp32;
						prev_bcm2_status_1 = tmp16;
					}
										
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_2_RHTurnlightSts()<<4)|CanIfGet_gCanIf_CEM_BCM_2_LHTurnlightSts();

					if(prev_turnlight_status != tmp8)
					{
						prev_turnlight_status = tmp8;
#ifdef CHECK_TURNLIGHT_TYPE
						if((CanIfGet_gCanIf_CEM_BCM_2_RHTurnlightSts()==ON)||(CanIfGet_gCanIf_CEM_BCM_2_LHTurnlightSts()==ON))
						{
							if(500<=GetElapsedTime(check_turnlight_timer_tick))
							{
								turnlight_type =0;
								check_turnlight_timer_tick = GetTickCount();
							}
							else
							{
								turnlight_type =1;
								check_turnlight_timer_tick = GetTickCount();
								tmp8 += 0x20;//refer to protocol
							}
						}
#endif
						
						CAR_SIG_TURNLIGHT.flag = ON;
						CAR_SIG_TURNLIGHT.trycnt = 0;
						CAR_SIG_TURNLIGHT.buf[1] = tmp8;
					}
#endif
					break;
					
				case AVM_APA_3_41A_IDX:
//					SrvMcanTestAvmApa3_41A();
					tmp8 =(CanIfGet_gCanIf_AVM_APA_3_AVM_APA_SystemSts()<<4)|(CanIfGet_gCanIf_AVM_APA_3_APA_Parking_Space_Search_Sts()<<3)
						|(CanIfGet_gCanIf_AVM_APA_3_AVMDisplaySts()<<2)|(CanIfGet_gCanIf_AVM_APA_3_AVMFeedbackSts_RemoteMode()<<1)
						|CanIfGet_gCanIf_AVM_APA_3_AVM_FactoryMode();

					if(prev_avm_apa3 !=tmp8)
					{						
						CAR_SIG_AVM_APA3.flag = ON;
						CAR_SIG_AVM_APA3.trycnt = 0;
						CAR_SIG_AVM_APA3.buf[1] = tmp8;
						DPRINTF(DBG_MSG1,"AVM_APA_3_41A_IDX : %x avm %d \n\r",tmp8,CanIfGet_gCanIf_AVM_APA_3_AVMDisplaySts());
						prev_avm_apa3 =tmp8;
					}
					break;
					
				case ICM_1_430_IDX:
//					 SrvMcanTestIcm1_430();
					 //can problem
#if 1
					tmp8 =	CanIfGet_gCanIf_ICM_1_FuelLevel();
					tmp16 =(uint16)tmp8<<8;
					tmp8 =	 CanIfGet_gCanIf_ICM_1_FuelLevelFailSts();
					tmp16 |=(uint16)tmp8;	

					if(prev_fuellevel_status_0 != tmp16)
					{
						//add message
						CAR_SIG_FUEL.flag = ON;
						CAR_SIG_FUEL.trycnt = 0;
						CAR_SIG_FUEL.buf[1] = CanIfGet_gCanIf_ICM_1_FuelLevel();
						CAR_SIG_FUEL.buf[2] = CanIfGet_gCanIf_ICM_1_FuelLevelFailSts();
						
						prev_fuellevel_status_0 = tmp16;
					}
					
					tmp8 = (CanIfGet_gCanIf_ICM_1_DriverSeatBeltWarningSts()<<4)|CanIfGet_gCanIf_ICM_1_PassengerSeatBeltWarningSts();

					if(prev_seatbelt_status != tmp8)
					{
						//add message
						CAR_SIG_SEATBELT.flag = ON;
						CAR_SIG_SEATBELT.trycnt = 0;
						CAR_SIG_SEATBELT.buf[1] = tmp8;
						prev_seatbelt_status = tmp8;
					}
#endif
					tmp32 = (((uint32)CanIfGet_gCanIf_ICM_1_TotalOdometer_km_0()<<16)|((uint32)CanIfGet_gCanIf_ICM_1_TotalOdometer_km_1()<<8)
							|CanIfGet_gCanIf_ICM_1_TotalOdometer_km_2());
					
					if(prev_odo_meter != tmp32)
					{
						CAR_SIG_ODOMETER.flag = ON;
						CAR_SIG_ODOMETER.trycnt = 0;
						CAR_SIG_ODOMETER.buf[1] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_0();
						CAR_SIG_ODOMETER.buf[2] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_1();
						CAR_SIG_ODOMETER.buf[3] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_2();
						prev_odo_meter = tmp32;
					}
					break;
			
				case PLG_436_IDX:				//default value	 00 00 00 00 00 5F 00 00
//					SrvMcanTestPLG_436();
#if 0			
//					if(CanIfGet_gCanIf_PLG_Checksum()!=SrvMcanGetCheckSum(PLG_436_IDX))
//					{
//						CPRINTF(DBG_MSG1,"PLG_436_IDX checksum error %x %x\n\r",CanIfGet_gCanIf_PLG_Checksum(),SrvMcanGetCheckSum(PLG_436_IDX));
//					}
//					else
#endif
					{
						tmp8 = CanIfGet_gCanIf_PLG_RearDoorStatus();
						tmp16 = (uint16)tmp8<<8;
						tmp8 = CanIfGet_gCanIf_PLG_PLGSASSts();
						tmp16 |= (uint16)tmp8;

						if(prev_plg !=tmp16)
						{
							CAR_SIG_PLG1.flag = ON;
							CAR_SIG_PLG1.trycnt = 0;
							CAR_SIG_PLG1.buf[1] = (uint8)(tmp16>>8);
							CAR_SIG_PLG1.buf[2] =(uint8)tmp16;
							CPRINTF(DBG_MSG1,"PLG_436_IDX %x \n\r",tmp16);
							prev_plg = tmp16;
						}
					}
					break;
					
				case RADAR_1_440_IDX:
//					SrvMcanTestRadar1_440();				
					CAR_SIG_CEM_RADAR1.buf[1] = CanIfGet_gCanIf_RADAR_1_RHRRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[2] = CanIfGet_gCanIf_RADAR_1_LHRRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[3] = CanIfGet_gCanIf_RADAR_1_RHMRRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[4] = CanIfGet_gCanIf_RADAR_1_LHMRRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[5] = CanIfGet_gCanIf_RADAR_1_LHFRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[6] = CanIfGet_gCanIf_RADAR_1_RHFRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[7] = CanIfGet_gCanIf_RADAR_1_RHMFRadarSensorDistance();
					CAR_SIG_CEM_RADAR1.buf[8] = CanIfGet_gCanIf_RADAR_1_LHMFRadarSensorDistance();
					tmp8 = (CanIfGet_gCanIf_RADAR_1_AudibleBeepRate()<<3)|(CanIfGet_gCanIf_RADAR_1_RadarDetectSts()<<1)|
						(CanIfGet_gCanIf_RADAR_1_RadarWorkSts());
			
					CAR_SIG_CEM_RADAR1.buf[9] = tmp8;
					
					if(CanIfGet_gCanIf_RADAR_1_RadarWorkSts()==ON)
					{
						CAR_SIG_CEM_RADAR1.flag = ON;
						CAR_SIG_CEM_RADAR1.trycnt = 0;
					}
					break;
					
				case BSD_1_449_IDX:				//default value	 04 09 00 00 00 F2 00 00
//					SrvMcanTestBSD1_449();
					if(CanIfGet_gCanIf_BSD_1_Checksum()!=SrvMcanGetCheckSum(BSD_1_449_IDX))
					{
						CPRINTF(DBG_MSG1,"BSD_1_449_IDX checksum error %x %x\n\r",CanIfGet_gCanIf_BSD_1_Checksum(),SrvMcanGetCheckSum(BSD_1_449_IDX));
					}
					else
					{
						//CanIfGet_gCanIf_BSD_1_BSD_1_MessageCounter(void)
						tmp8 = (CanIfGet_gCanIf_BSD_1_SystemState()<<6)|(CanIfGet_gCanIf_BSD_1_BSDState()<<5)|
						(CanIfGet_gCanIf_BSD_1_RCTAState()<<4)|(CanIfGet_gCanIf_BSD_1_RCTAWarningLeft()<<3)|
						(CanIfGet_gCanIf_BSD_1_RCTAWarningRight()<<2)|(CanIfGet_gCanIf_BSD_1_RCWSts()<<1)| CanIfGet_gCanIf_BSD_1_DOWSts();
						
						if(prev_bsd1_status != tmp8)
						{
							CAR_SIG_BSD1.flag = ON;
							CAR_SIG_BSD1.trycnt = 0;
							CAR_SIG_BSD1.buf[1] = tmp8;
							prev_bsd1_status = tmp8;
						}
					}
					break;
					
				case ICM_2_452_IDX:
//					SrvMcanTestICM2_452();
					tmp8 = CanIfGet_gCanIf_ICM_2_STAT_Chime();

					if((prev_chime !=tmp8 )/*&&
						((tmp8<0x05)||((0x10<tmp8)&&(tmp8<0x15))||((0x20<tmp8)&&(tmp8<0x2E))||((0x30<tmp8)&&(tmp8<0x37)))*/)
					{
						CAR_SIG_ICM2.flag = ON;
						CAR_SIG_ICM2.trycnt=0;
						CAR_SIG_ICM2.buf[1] = tmp8;
						prev_chime = tmp8;
					}
					break;
					
				case WPC_1_460_IDX:
//					SrvMcanTestWpc1_460();
					tmp8 = (CanIfGet_gCanIf_WPC_1_WPC_WirelessChargingStatus()<<1)|CanIfGet_gCanIf_WPC_1_NFCSts();
					if(prev_wpc !=tmp8)
					{
						CAR_SIG_WPC.flag = ON;
						CAR_SIG_WPC.trycnt = 0;
						CAR_SIG_WPC.buf[1] = tmp8;
						prev_wpc = tmp8;
					}					
					break;
					
				case AVM_APA_4_475_IDX:
//					SrvMcanTestAvmApa4_475();
					tmp8 =CanIfGet_gCanIf_AVM_APA_4_RHSRRadarSensorDistance();
					tmp32 = (uint32)tmp8<<24;
					tmp8 =CanIfGet_gCanIf_AVM_APA_4_LHSRRadarSensorDistance();
					tmp32 |= (uint32)tmp8<<16;
					tmp8 =CanIfGet_gCanIf_AVM_APA_4_RHSFRadarSensorDistance();
					tmp32 |= (uint32)tmp8<<8;
					tmp8 = CanIfGet_gCanIf_AVM_APA_4_LHSFRadarSensorDistance();

					tmp32 |= (uint32)tmp8;

					if(prev_avm_apa3!=tmp32)
					{
						CAR_SIG_AVM_APA4.flag = ON;
						CAR_SIG_AVM_APA4.trycnt = 0;
						CAR_SIG_AVM_APA4.buf[1] = CanIfGet_gCanIf_AVM_APA_4_RHSRRadarSensorDistance();
						CAR_SIG_AVM_APA4.buf[2] = CanIfGet_gCanIf_AVM_APA_4_LHSRRadarSensorDistance();
						CAR_SIG_AVM_APA4.buf[3] = CanIfGet_gCanIf_AVM_APA_4_RHSFRadarSensorDistance();
						CAR_SIG_AVM_APA4.buf[4] = CanIfGet_gCanIf_AVM_APA_4_LHSFRadarSensorDistance();
						prev_avm_apa3 = tmp32;
					}
					break;
					
				case CEM_PEPS_480_IDX:				//default value	 00 00 00 00 00 20 00 00
//					SrvMcanTestCemPeps_480();
#if 0
//					if(CanIfGet_gCanIf_CEM_PEPS_checksum()!=SrvMcanGetCheckSum(CEM_PEPS_480_IDX)){
//						CPRINTF(DBG_MSG1,"PEPS_480 checksum error %x %x\n\r",CanIfGet_gCanIf_CEM_PEPS_checksum(),SrvMcanGetCheckSum(CEM_PEPS_480_IDX));
//					}
//					else
#endif
					{
						tmp8 = CanIfGet_gCanIf_CEM_PEPS_Pollingsts();			//Polling sts 
						
						if(prev_cem_peps!=tmp8)
						{	
							CAR_SIG_CEM_PEPS.flag =	ON;
							CAR_SIG_CEM_PEPS.trycnt = 0;
							CAR_SIG_CEM_PEPS.buf[1] = tmp8;
							prev_cem_peps = tmp8;
						}

					}
					break;
					
				case CEM_ALM_1_490_IDX:				//default value	 00 0C 04 00 01 00 00 05
//					SrvMcanTestCemAlm1_490();
					tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_SeatSetPop_Up()<<4)|(CanIfGet_gCanIf_CEM_ALM_1_PersonalSeatEn()<<2)|	
						CanIfGet_gCanIf_CEM_ALM_1_PersonalBeamEn();
					tmp16 =(uint16)tmp8<<8;
					tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_SeatSetStatus()<<4)|CanIfGet_gCanIf_CEM_ALM_1_DeletePeronalSet_FB();
					tmp16 |=(uint16)tmp8;	
										
					if(prev_seat_status != tmp16)
					{
						CAR_SIG_SEATSTATUS.flag = ON;
						CAR_SIG_SEATSTATUS.trycnt = 0;
						CAR_SIG_SEATSTATUS.buf[1] = (uint8)(tmp16>>8);
						CAR_SIG_SEATSTATUS.buf[2] = (uint8)tmp16;
						prev_seat_status = tmp16;
					}

					tmp8 =(CanIfGet_gCanIf_CEM_ALM_1_PersonalSetSts()<<7)| (CanIfGet_gCanIf_CEM_ALM_1_STAT_ControlSwitch()<<5)|
						(CanIfGet_gCanIf_CEM_ALM_1_STAT_StaticEffect()<<2)|CanIfGet_gCanIf_CEM_ALM_1_STAT_Music();
					tmp32 = (uint32)tmp8<<16;
					tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_STAT_StaticColour()<<4)| CanIfGet_gCanIf_CEM_ALM_1_STAT_Apiluminance();
					tmp32 |= (uint32)tmp8<<8;
					tmp8 = (CanIfGet_gCanIf_CEM_ALM_1_STAT_AssociatedWithDriverMode()<<4)
						|(CanIfGet_gCanIf_CEM_ALM_1_STAT_WelcomeWaterLamp()<<2)| CanIfGet_gCanIf_CEM_ALM_1_Wash_Car_Status();

					tmp32 |= (uint32)tmp8;

					if(prev_breath_status != tmp32)
					{
						CAR_SIG_CEM_ALM1.flag = ON;
						CAR_SIG_CEM_ALM1.trycnt = 0;
						CAR_SIG_CEM_ALM1.buf[1] = (uint8)(tmp32>>16);
						CAR_SIG_CEM_ALM1.buf[2] = (uint8)(tmp32>>8);
						CAR_SIG_CEM_ALM1.buf[3] = (uint8)tmp32;
//						DPRINTF(DBG_MSG1,"CEM_ALM_1_490_IDX \n\r");
						prev_breath_status= tmp32;
					}
					break;
			
				case ABS_ESP_5_4E5_IDX:
	//				SrvMcanTestAbsEsp5_4E5();
					if(CanIfGet_gCanIf_ABS_ESP_5_Checksum()!=SrvMcanGetCheckSum(ABS_ESP_5_4E5_IDX))
					{
						CPRINTF(DBG_MSG1,"ABS_ESP_5_4E5_IDX checksum error %x %x\n\r",CanIfGet_gCanIf_ABS_ESP_5_Checksum(),SrvMcanGetCheckSum(ABS_ESP_5_4E5_IDX));
					}
					else
					{
						//add
						tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_LHRPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_LHRPulseCounter();
						tmp32 = (uint32)tmp16<<16;
						tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_RHRPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_RHRPulseCounter();
						tmp32 |= (uint32)tmp16;
						tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_LHFPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_LHFPulseCounter();
						tmp32_1 = (uint32)tmp16<<16;
						tmp16 = (((uint16)CanIfGet_gCanIf_ABS_ESP_5_RHFPulseCounterFailSts())<<13)|CanIfGet_gCanIf_ABS_ESP_5_RHFPulseCounter();
						tmp32_1 |= (uint32)tmp16;

						if((prev_abs_eps5_0 !=tmp32)||(prev_abs_eps5_1!=tmp32_1))
						{
							CAR_SIG_ABS_ESP5.flag = ON;
							CAR_SIG_ABS_ESP5.trycnt = 0;
							CAR_SIG_ABS_ESP5.buf[1] = (uint8)(tmp32>>24);
							CAR_SIG_ABS_ESP5.buf[2] = (uint8)(tmp32>>16);
							CAR_SIG_ABS_ESP5.buf[3] = (uint8)(tmp32>>8);
							CAR_SIG_ABS_ESP5.buf[4] = (uint8)tmp32;
							CAR_SIG_ABS_ESP5.buf[5] = (uint8)(tmp32_1>>24);
							CAR_SIG_ABS_ESP5.buf[6] = (uint8)(tmp32_1>>16);
							CAR_SIG_ABS_ESP5.buf[7] = (uint8)(tmp32_1>>8);
							CAR_SIG_ABS_ESP5.buf[8] = (uint8)tmp32_1;
							prev_abs_eps5_0 = tmp32;
							prev_abs_eps5_1 = tmp32_1;
						}
					}
					break;
					
				case AVM_APA_2_508_IDX:
//					SrvMcanTestAvmApa2_508();
					tmp8 = (CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraFront()<<4)|(CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraLeft()<<3)	
						|(CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraRear()<<2)|(CanIfGet_gCanIf_AVM_APA_2_AVMFaultStatusCameraRight()<<1)
						|CanIfGet_gCanIf_AVM_APA_2_AVMStatusFault();

					if(prev_avm_apa2 !=tmp8)
					{
						CAR_SIG_AVM_APA2.flag = ON;
						CAR_SIG_AVM_APA2.trycnt = 0;
						CAR_SIG_AVM_APA2.buf[1] = tmp8;
						prev_avm_apa2 = tmp8;
					}
					break;
			
				case TBOX_2_519_IDX:
//					SrvMcanTBOX2_519();					
					//check checksume 
					if(CanIfGet_gCanIf_TBOX_2_Checksum()!=SrvMcanGetCheckSum(TBOX_2_519_IDX)){
						CPRINTF(DBG_MSG1,"TBOX_2 checksum error %x %x\n\r",CanIfGet_gCanIf_TBOX_2_Checksum(),SrvMcanGetCheckSum(TBOX_2_519_IDX));
					}
					else
					{
						tmp8 = CanIfGet_gCanIf_TBOX_2_TriggerAlarmSts();

						if(prev_tbox_alarm !=tmp8){
							CAR_SIG_TBOX2.flag = ON;
							CAR_SIG_TBOX2.trycnt = 0;
							CAR_SIG_TBOX2.buf[1] = tmp8;
							prev_tbox_alarm = tmp8;
						}									
					}
					break;

				case CEM_IPM_1_51A_IDX:
//					SrvMcanTestCemIpm1_51A();
					tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_CEM_FLTempsts()<<2)|CanIfGet_gCanIf_CEM_IPM_1_CEM_RecyMode();		//speed 
					tmp32 = (uint32)tmp8<<24;
					tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_CEM_FRTempsts()<<2)|(CanIfGet_gCanIf_CEM_IPM_1_ACStatus()<<1)|	
						CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontOFFSts();
					tmp32 |= (uint32)tmp8<<16;
					tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontBlowSpdCtrlsts()<<4)|	CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontBlowModeSts();
					tmp32 |= (uint32)tmp8<<8;
					tmp8 =(CanIfGet_gCanIf_CEM_IPM_1_CEM_RearDefrosts()<<7)|(CanIfGet_gCanIf_CEM_IPM_1_CEM_RearAutoACSts()<<6)|
						(CanIfGet_gCanIf_CEM_IPM_1_CEM_FrontAutoACSts()<<5)|(CanIfGet_gCanIf_CEM_IPM_1_CEM_SyncSts()<<4)|
						(CanIfGet_gCanIf_CEM_IPM_1_CEM_PM25_Detect()<<3)|(CanIfGet_gCanIf_CEM_IPM_1_CEM_AnionPurify()<<2)|
						(CanIfGet_gCanIf_CEM_IPM_1_BTReduceWindSpeedSts()<<1)|	CanIfGet_gCanIf_CEM_IPM_1_IPMblowerdelaySts();
					tmp32 |= (uint32)tmp8;

					tmp8 = (CanIfGet_gCanIf_CEM_IPM_1_IPMFirstBlowingSts()<<6)|(CanIfGet_gCanIf_CEM_IPM_1_AC_DisplaySts()<<5)|
						CanIfGet_gCanIf_CEM_IPM_1_CEM_TempknobRollingCounter();

					if((prev_cem_ipm_0 !=tmp32)||(prev_cem_ipm_1!=tmp8))
					{							
						CAR_SIG_CEM_IPM.flag =ON;
						CAR_SIG_CEM_IPM.trycnt=0;
						CAR_SIG_CEM_IPM.buf[1] =(uint8)(tmp32>>24);
						CAR_SIG_CEM_IPM.buf[2] =(uint8)(tmp32>>16);
						CAR_SIG_CEM_IPM.buf[3] =(uint8)(tmp32>>8);
						CAR_SIG_CEM_IPM.buf[4] =(uint8)tmp32;
						CAR_SIG_CEM_IPM.buf[5] =tmp8;
						CPRINTF(DBG_MSG1,"CAR_SIG_CEM_IPM %x %x %x %x %x\n\r", CAR_SIG_CEM_IPM.buf[1], CAR_SIG_CEM_IPM.buf[2],	CAR_SIG_CEM_IPM.buf[3], 
							CAR_SIG_CEM_IPM.buf[4], CAR_SIG_CEM_IPM.buf[5]);
						prev_cem_ipm_0 = tmp32;
						prev_cem_ipm_1 = tmp8;	
					}
					break;
				case AVM_2_51C_IDX:
//					SrvMcanTestAvm2_51C();
						tmp8 = (CanIfGet_gCanIf_AVM_2_AVM_DisplaySts()<<6)|(CanIfGet_gCanIf_AVM_2_AVM_WorkModeSts()<<4)|
							(CanIfGet_gCanIf_AVM_2_AVM_LanguageSts()<<1)|CanIfGet_gCanIf_AVM_2_AVM_SteeringWheelSts();
						tmp16 = (uint16)tmp8<<8;
						tmp8 = (CanIfGet_gCanIf_AVM_2_AVM_RadarWariningSts()<<6)|(CanIfGet_gCanIf_AVM_2_AVM_LDW_FunSts()<<5)|
							(CanIfGet_gCanIf_AVM_2_AVM_BSD_FunSts()<<4)|(CanIfGet_gCanIf_AVM_2_AVM_DefDispMode()<<3)|CanIfGet_gCanIf_AVM_2_DVR_WorkState();
						tmp16 |= (uint16)tmp8;

						if(prev_avm2 !=tmp16)
						{
							CAR_SIG_AVM2.flag= ON;
							CAR_SIG_AVM2.trycnt=0;
							CAR_SIG_AVM2.buf[1] = (uint8)(tmp16>>8);
							CAR_SIG_AVM2.buf[2] = (uint8)tmp16;
							prev_avm2 = tmp16;
						}
			
					break;
					
				case ICM_3_52E_IDX:
//					SrvMcanTestIcm3_52E();
					break;

				case ICM_4_530_IDX:
//					SrvMcanTestIcm4_530();
					//can problem					
					tmp8 = CanIfGet_gCanIf_ICM_4_DistanceToEmpty_Km_0();		//speed 
					tmp16 = (uint16)tmp8;
					tmp8 = CanIfGet_gCanIf_ICM_4_DistanceToEmpty_Km_1();
					tmp16 = (uint16)(tmp16<<8)|tmp8;

					if(prev_dte_status != tmp16)
					{
						//add message
						CAR_SIG_DTE.flag = ON;
						CAR_SIG_DTE.trycnt = 0;
						CAR_SIG_DTE.buf[1] = CanIfGet_gCanIf_ICM_4_DistanceToEmpty_Km_0();
						CAR_SIG_DTE.buf[2] = CanIfGet_gCanIf_ICM_4_DistanceToEmpty_Km_1();
						prev_dte_status = tmp16;
					}

#ifdef CX62C_FUNCTION
					tmp8  = (CanIfGet_gCanIf_ICM_4_ThemeSys()<<2)|CanIfGet_gCanIf_ICM_4_ICMLanguageSet();

					tmp16 = ((uint16)CanIfGet_gCanIf_ICM_4_Brake_Fuel_Level()<<12)|((uint16)CanIfGet_gCanIf_ICM_4_AverageFuelConsume_0()<<8)|
						CanIfGet_gCanIf_ICM_4_AverageFuelConsume_1();

					tmp32 |= (uint32)tmp16<<16;
					tmp16 = ((uint16)CanIfGet_gCanIf_ICM_4_Engine_oil_Pressure()<<13)|((uint16)CanIfGet_gCanIf_ICM_4_AverageVehicleSpeed_0()<<8)|
						CanIfGet_gCanIf_ICM_4_AverageVehicleSpeed_1();
					tmp32 |= (uint32)tmp16;

					if((prev_icm4 != tmp8)||(prev_icm4_0!=tmp32))
					{
						CAR_SIG_ICM4.flag = ON;
						CAR_SIG_ICM4.trycnt = 0;
						CAR_SIG_ICM4.buf[1] = tmp8;	
						CAR_SIG_ICM4.buf[2] = (uint8)(tmp32>>24);		
						CAR_SIG_ICM4.buf[3] = (uint8)(tmp32>>16);
						CAR_SIG_ICM4.buf[4] = (uint8)(tmp32>>8);
						CAR_SIG_ICM4.buf[5] = (uint8)tmp32;

						prev_icm4 = tmp8;
						prev_icm4_0 = tmp32;
					}
#else
					tmp8  = (CanIfGet_gCanIf_ICM_4_ThemeSys()<<2)|CanIfGet_gCanIf_ICM_4_ICMLanguageSet();

					if(prev_icm4 != tmp8)
					{
						CAR_SIG_ICM4.flag = ON;
						CAR_SIG_ICM4.trycnt = 0;
						CAR_SIG_ICM4.buf[1] = tmp8;					
						prev_icm4 = tmp8;
					}
#endif
					break;
			
				case PM25_1_53A_IDX:
//					SrvMcanTestPM25_1_53A();
					tmp8 = CanIfGet_gCanIf_PM25_1_PM2_5Indensity_0();
					tmp32 = (uint32)tmp8<<24;
					tmp8 = CanIfGet_gCanIf_PM25_1_PM2_5Indensity_1();
					tmp32 |= (uint32)tmp8<<16;
					tmp8  = CanIfGet_gCanIf_PM25_1_PM2_5Outdensity_0();
					tmp32 |= (uint32)tmp8<<8;
					tmp8  = CanIfGet_gCanIf_PM25_1_PM2_5Outdensity_1();
					tmp32 |= (uint32)tmp8;

					tmp8  = (CanIfGet_gCanIf_PM25_1_AirInQLevel()<<4)|CanIfGet_gCanIf_PM25_1_AirOutQLevel();
					tmp16 |= (uint16)tmp8<<8;
					tmp8  = (CanIfGet_gCanIf_PM25_1_PM25Sts()<<2)|CanIfGet_gCanIf_PM25_1_PM25ErrSts();
					tmp16 |= (uint16)tmp8;

					if((prev_pm25_0 != tmp32)||(prev_pm25_1 != tmp16))
					{
						//add message
						CAR_SIG_PM25.flag = ON;
						CAR_SIG_PM25.trycnt = 0;
						CAR_SIG_PM25.buf[1] = (uint8)(tmp32>>24);
						CAR_SIG_PM25.buf[2] = (uint8)(tmp32>>16);
						CAR_SIG_PM25.buf[3] = (uint8)(tmp32>>8);
						CAR_SIG_PM25.buf[4] = (uint8)(tmp32);
						CAR_SIG_PM25.buf[5] = (uint8)(tmp16>>8);
						CAR_SIG_PM25.buf[6] = (uint8)(tmp16);
						prev_pm25_0 = tmp32;
						prev_pm25_1 = tmp16;
					}
					break;
					
				case AFU_1_560_IDX:				//default value	 00 00 00 40 00 00 00 00
//					SrvMcanTestAfu1_560();
					tmp8 = (CanIfGet_gCanIf_AFU_1_AirFragCh1Detn()<<7)|(CanIfGet_gCanIf_AFU_1_AirFragCh1Type()<<4)|
						(CanIfGet_gCanIf_AFU_1_AirFragCh2Detn()<<3)|CanIfGet_gCanIf_AFU_1_AirFragCh2Type();
//					CPRINTF(DBG_MSG5,"AFU 1 %d %d %x\r",CanIfGet_gCanIf_AFU_1_AirFragCh1Detn(),CanIfGet_gCanIf_AFU_1_AirFragCh1Type(),tmp8);
					tmp32 = (uint32)tmp8<<16;
					tmp8  = (CanIfGet_gCanIf_AFU_1_AirFragCh3Detn()<<7)|(CanIfGet_gCanIf_AFU_1_AirFragCh3Type()<<4)|
						(CanIfGet_gCanIf_AFU_1_AFU_ON_button()<<3)|CanIfGet_gCanIf_AFU_1_AirFragLvlRspn();
					tmp32 |= (uint32)tmp8<<8;
					tmp8 = (CanIfGet_gCanIf_AFU_1_AirFragChChgSts()<<6)|(CanIfGet_gCanIf_AFU_1_AirFragInitSts()<<4)|
						(CanIfGet_gCanIf_AFU_1_AirFragSwthSts()<<2)|CanIfGet_gCanIf_AFU_1_AirFragCh1Button();
					tmp32 |= (uint32)tmp8;
//					CPRINTF(DBG_MSG5,"AFU total %x\r",tmp32);
					if(prev_afu !=tmp32)
					{
						CAR_SIG_AFU.flag = ON;			
						CAR_SIG_AFU.trycnt = 0;
						CAR_SIG_AFU.buf[1] = (uint8)(tmp32>>16);
						CAR_SIG_AFU.buf[2] = (uint8)(tmp32>>8);
						CAR_SIG_AFU.buf[3] = (uint8)(tmp32);
						prev_afu = tmp32;
					}
					break;	
					
				case AMP_1_555_IDX:
//					SrvMcanTestAmp1_555();
#if 0
//					dev->r_SendCPU->sig_amp = ON;					
//					CAR_SIG_AMP.buf[1] = CanIfGet_gCanIf_AMP_1_AMP_TempHighVolAdiustLow();			
//					CAR_SIG_AMP.buf[2] =  CanIfGet_gCanIf_AMP_1_AMP_InitFlag();
#else
					tmp8 = CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_H();
					tmp32 |= (uint32)tmp8<<16;
					tmp8 = CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_M();
					tmp32 |= (uint32)tmp8<<8;
					tmp8 = CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_L();
					tmp32 |= (uint32)tmp8;
					if(prev_amp1!= tmp32)
					{
						prev_amp1 = tmp32;
						CPRINTF(DBG_MSG1,"CAN DATA %x %x %x\n\r",CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_H(),CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_M(),
						CanIfGet_gCanIf_AMP_1_AMP_AMP_SET_Ver_L());
					}
#endif
					break;

				case DVR_1_58A_IDX:
//					SrvMcanTestDvr1_58A();
					tmp8 = (CanIfGet_gCanIf_DVR_1_DVR_Sdcard_Sts()<<5)|(CanIfGet_gCanIf_DVR_1_DVR_SystemSts()<<4)
						|(CanIfGet_gCanIf_DVR_1_DVR_LocalPhotographResult()<<2)|CanIfGet_gCanIf_DVR_1_DVRLocalContinuousPhotograph();

					if(prev_dvr_status != tmp8)
					{
						//add message
						CAR_SIG_DVR1.flag = ON;
						CAR_SIG_DVR1.trycnt = 0;
						CAR_SIG_DVR1.buf[1] = tmp8;
//						CPRINTF(DBG_MSG1,"CAR_SIG_DVR1 : send message %d %d  ",prev_dvr_status,tmp8);
						prev_dvr_status = tmp8;						
					}
					break;	
					
				case CEM_BCM_4_58D_IDX:
//					SrvMcanTestCemBcm4_58D();
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_4_FL_WIN_Position()<<4)|CanIfGet_gCanIf_CEM_BCM_4_FR_WIN_Position();
					tmp16 = (uint16)tmp8;
					tmp16 = (uint16)(tmp16<<8)|(CanIfGet_gCanIf_CEM_BCM_4_RL_WIN_Position()<<4)|CanIfGet_gCanIf_CEM_BCM_4_RR_WIN_Position();
						
					if(prev_bcm4_status != tmp16)
					{
						CAR_SIG_CEM_BCM4.flag= ON;
						CAR_SIG_CEM_BCM4.trycnt = 0;
						CAR_SIG_CEM_BCM4.buf[1] =(uint8)(tmp16>>8);
						CAR_SIG_CEM_BCM4.buf[2] = (uint8)tmp16;
						prev_bcm4_status = tmp16;
					}				
					break;
					
				case CEM_BCM_3_5FF_IDX:
//					SrvMcanTestCemBcm3_5FF();
					tmp8 = (CanIfGet_gCanIf_CEM_BCM_3_EngineControlSts()<<2)|CanIfGet_gCanIf_CEM_BCM_3_DWM_Sts_Msg();
					if(prev_bcm3_status != tmp8)
					{
						CAR_SIG_CEM_BCM3.flag= ON;
						CAR_SIG_CEM_BCM3.trycnt = 0;
						CAR_SIG_CEM_BCM3.buf[1] =tmp8;
						prev_bcm3_status = tmp8;
					}
					break;
					
				default:
					break;
			}

		}
	}
#endif
}

//<<JANG_211116
void CAN_Init_Comm(void)
{
    prev_seat_status        = 0;
    prev_gear_status        = 0;
    prev_angle_status       = 0;
    prev_speed_status       = 0;
    prev_dte_status         = 0;
    prev_fuellevel_status_0 = 0;
    //	prev_fuellevel_status_1 = 0;
    prev_dvr_status       = 0;
    prev_seatbelt_status  = 0;
    prev_bcm1_status      = 0;
    prev_bcm2_status_0    = 0;
    prev_bcm2_status_1    = 0;
    prev_bcm3_status      = 0;
    prev_bcm4_status      = 0;
    prev_bsd1_status      = 0;
    prev_turnlight_status = 0;
    prev_odo_meter        = 0;
    prev_wpc              = 0;
    prev_breath_status    = 0;
    prev_tbox_alarm       = 0;
    prev_drivemode        = 0;
    prev_enginespeed      = 0;
    prev_ctl              = 0;
    prev_rollingcount     = 0;
    prev_mfs_heart        = 0;
    prev_chime            = 0;
    prev_frm3             = 0;
    prev_abs_eps5_0       = 0;
    prev_abs_eps5_1       = 0;
    prev_pm25_0           = 0;
    prev_pm25_1           = 0;
    prev_avm2             = 0;
    prev_fcm2             = 0;
    prev_cem_ipm_0        = 0;
    prev_cem_ipm_1        = 0;
    prev_icm4             = 0;
#ifdef CX62C_FUNCTION
    prev_icm3_0 = 0;
    prev_icm3_1 = 0;
    prev_icm4_0 = 0;
#endif
    prev_plg      = 0;
    prev_cgw_abm  = 0;
    prev_afu      = 0;
    prev_avm_apa2 = 0;
    prev_avm_apa3 = 0;
    prev_cem_peps = 0;
    prev_amp1     = 0;
}
//>>

TP_STATE_TYPE      gCanTpState = TP_STATE_IDLE; // Block during Rx Request processing
static TP_CMD_TYPE current_type;
uint8              DVD_ICM_ONOFF = 0;
static uint8       TP_CONFLOW    = 0;

static volatile uint32 tp_ivi_icm_timer_tick      = 0;
static volatile uint32 tp_flow_control_timer_tick = 0;

void CANTp_Comm(DEVICE *dev)
{
    uint8        tmp8   = 0;
    static uint8 tp_cnt = 0;

    // CAN TP COMM
    // 52E  -->ICM_3
}

// JANG_211116

//抓取CAN锟斤拷锟斤拷锟斤拷息:锟斤拷锟斤拷CAN锟斤拷锟斤拷锟斤拷抓取锟斤拷锟斤拷息锟斤拷通锟斤拷锟斤拷锟节凤拷式锟斤拷锟酵革拷SOC
void CHK_CAN_COMM_10MS_MSG(DEVICE *dev)
{
    // VCU_0_5:0x123
    CAN_COMM_10MS_123();
    if (CGW_VCU_P_5_123.flag == ON)
    {
        CGW_VCU_P_5_123.flag  = OFF; // for next difference check
        CGW_VCU_P_5_123.timer = 0;   // for slow send postponed

        // UART_Send_Book(dev, &CGW_VCU_P_5_123);
        Uart_Send_Func(&CGW_VCU_P_5_123);
    }
    else
        CGW_VCU_P_5_123.timer++; // slow send timer


    // MFS_1:0x294
    CAN_COMM_10MS_294();
    if (CGW_BCM_MFS_1_294.flag == ON)
    {
        CGW_BCM_MFS_1_294.flag  = OFF;
        CGW_BCM_MFS_1_294.timer = 0;

        // UART_Send_Book(dev, &CGW_BCM_MFS_1_294);
        Uart_Send_Func(&CGW_BCM_MFS_1_294);
    }
    else
        CGW_BCM_MFS_1_294.timer++;

} // 2

uint8 CHK_CAN_COMM_20MS_MSG(DEVICE *dev)
{
    // BCM1:0x245
    CAN_COMM_20MS_245();
    if (CGW_BCM_1_245.flag == ON)
    {
        CGW_BCM_1_245.flag  = OFF;
        CGW_BCM_1_245.timer = 0;

        // UART_Send_Book(dev, &CGW_BCM_1_245);
        Uart_Send_Func(&CGW_BCM_1_245);
    }
    else
        CGW_BCM_1_245.timer++;

    CAN_COMM_20MS_215();
    if (CGW_EGS_1_215.flag == ON)
    {
        CGW_EGS_1_215.flag  = OFF;
        CGW_EGS_1_215.timer = 0;
        // UART_Send_Book(dev, &CGW_EGS_1_215);
        Uart_Send_Func(&CGW_EGS_1_215);
    }
    else
        CGW_EGS_1_215.timer++;

    // EPB1:0x231
    CAN_COMM_20MS_231();
    if (CGW_EPB_1_231.flag == ON)
    {
        CGW_EPB_1_231.flag  = OFF;
        CGW_EPB_1_231.timer = 0;
        // UART_Send_Book(dev, &CGW_EPB_1_231);
        Uart_Send_Func(&CGW_EPB_1_231);
    }
    else
        CGW_EPB_1_231.timer++;

    // ICM1:0x271
    CAN_COMM_20MS_271();
    if (CGW_ICM_1_271.flag == ON)
    {
        CGW_ICM_1_271.flag  = OFF;
        CGW_ICM_1_271.timer = 0;
        // UART_Send_Book(dev, &CGW_ICM_1_271);
        Uart_Send_Func(&CGW_ICM_1_271);
    }
    else
        CGW_ICM_1_271.timer++;

    // BCS_C_1:0x225
    CAN_COMM_20MS_225();
    if (CGW_BCS_C_1_225.flag == ON)
    {
        CGW_BCS_C_1_225.flag  = OFF;
        CGW_BCS_C_1_225.timer = 0;
        // UART_Send_Book(dev, &CGW_BCS_C_1_225);
        Uart_Send_Func(&CGW_BCS_C_1_225);
    }
    else
        CGW_BCS_C_1_225.timer++;

    // BMS3:0x25A
    CAN_COMM_20MS_25A();
    if (CGW_BMS_3_25A.flag == ON)
    {
        CGW_BMS_3_25A.flag  = OFF;
        CGW_BMS_3_25A.timer = 0;
        // UART_Send_Book(dev, &CGW_BMS_3_25A);
        Uart_Send_Func(&CGW_BMS_3_25A);
    }
    else
        CGW_BMS_3_25A.timer++;

    // BMS3:0x25A
    CAN_COMM_20MS_238();
    if (CGW_EPS_1_238.flag == ON)
    {
        CGW_EPS_1_238.flag  = OFF;
        CGW_EPS_1_238.timer = 0;
        // UART_Send_Book(dev, &CGW_EPS_1_238);
        Uart_Send_Func(&CGW_EPS_1_238);
    }
    else
        CGW_EPS_1_238.timer++;

} // 4

uint8 CHK_CAN_COMM_50MS_MSG(DEVICE *dev)
{
    // PEPS:0x291
    CAN_COMM_50MS_291();
    if (CGW_BCM_PEPS_1_291.flag == ON)
    {
        CGW_BCM_PEPS_1_291.flag  = OFF;
        CGW_BCM_PEPS_1_291.timer = 0;
        // UART_Send_Book(dev, &CGW_BCM_PEPS_1_291);
        Uart_Send_Func(&CGW_BCM_PEPS_1_291);
    }
    else
        CGW_BCM_PEPS_1_291.timer++;

    // BSD1:0x298
    CAN_COMM_50MS_298();
    if (CGW_BSD_1_298.flag == ON)
    {
        CGW_BSD_1_298.flag  = OFF;
        CGW_BSD_1_298.timer = 0;
        // UART_Send_Book(dev, &CGW_BSD_1_298);
        Uart_Send_Func(&CGW_BSD_1_298);
    }
    else
        CGW_BSD_1_298.timer++;

    // VCU_3_4:0x28A
    CAN_COMM_50MS_28A();
    if (CGW_VCU_3_4_28A.flag == ON)
    {
        CGW_VCU_3_4_28A.flag  = OFF;
        CGW_VCU_3_4_28A.timer = 0;
        // UART_Send_Book(dev, &CGW_VCU_3_4_28A);
        Uart_Send_Func(&CGW_VCU_3_4_28A);
    }
    else
        CGW_VCU_3_4_28A.timer++;

    // FCM10:0x3FC
    CAN_COMM_50MS_3FC();
    if (CGW_FCM_10_3FC.flag == ON)
    {
        CGW_FCM_10_3FC.flag  = OFF;
        CGW_FCM_10_3FC.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_10_3FC);
        Uart_Send_Func(&CGW_FCM_10_3FC);
    }
    else
        CGW_FCM_10_3FC.timer++;

    // FCM2:0x307
    CAN_COMM_50MS_307();
    if (CGW_FCM_2_307.flag == ON)
    {
        CGW_FCM_2_307.flag  = OFF;
        CGW_FCM_2_307.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_2_307);
        Uart_Send_Func(&CGW_FCM_2_307);
    }
    else
        CGW_FCM_2_307.timer++;

    // FCM6:0x307
    CAN_COMM_50MS_3DC();
    if (CGW_FCM_6_3DC.flag == ON)
    {
        CGW_FCM_6_3DC.flag  = OFF;
        CGW_FCM_6_3DC.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_6_3DC);
        Uart_Send_Func(&CGW_FCM_6_3DC);
    }
    else
        CGW_FCM_6_3DC.timer++;

    // FCM6:0x307
    CAN_COMM_50MS_3DE();
    if (CGW_FCM_7_3DE.flag == ON)
    {
        CGW_FCM_7_3DE.flag  = OFF;
        CGW_FCM_7_3DE.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_7_3DE);
        Uart_Send_Func(&CGW_FCM_7_3DE);
    }
    else
        CGW_FCM_7_3DE.timer++;

    // FRM6:0x387
    CAN_COMM_50MS_387();
    if (CGW_FCM_FRM_6_387.flag == ON)
    {
        CGW_FCM_FRM_6_387.flag  = OFF;
        CGW_FCM_FRM_6_387.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_FRM_6_387);
        Uart_Send_Func(&CGW_FCM_FRM_6_387);
    }
    else
        CGW_FCM_FRM_6_387.timer++;

    // FRM8:0x3ED
    CAN_COMM_50MS_3ED();
    if (CGW_FCM_FRM_8_3ED.flag == ON)
    {
        CGW_FCM_FRM_8_3ED.flag  = OFF;
        CGW_FCM_FRM_8_3ED.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_FRM_8_3ED);
        Uart_Send_Func(&CGW_FCM_FRM_8_3ED);
    }
    else
        CGW_FCM_FRM_8_3ED.timer++;

    // FRM9:0x3FA
    CAN_COMM_50MS_3FA();
    if (CGW_FCM_FRM_9_3FA.flag == ON)
    {
        CGW_FCM_FRM_9_3FA.flag  = OFF;
        CGW_FCM_FRM_9_3FA.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_FRM_9_3FA);
        Uart_Send_Func(&CGW_FCM_FRM_9_3FA);
    }
    else
        CGW_FCM_FRM_9_3FA.timer++;
} // 3

// S52锟斤拷锟侥斤拷锟秸达拷锟斤拷
uint8 CHK_CAN_COMM_100MS_MSG(DEVICE *dev)
{
    // ACCM:0x3D2   锟秸碉拷模锟斤拷
    CAN_COMM_100MS_3D2();
    if (ACCM_1_3D2.flag == ON)
    {
        ACCM_1_3D2.flag  = OFF;
        ACCM_1_3D2.timer = 0;
        // UART_Send_Book(dev, &ACCM_1_3D2);
        Uart_Send_Func(&ACCM_1_3D2);
    }
    else
        ACCM_1_3D2.timer++;

    // BCM4:0x324
    CAN_COMM_100MS_324();
    if (CGW_BCM_4_324.flag == ON)
    {
        CGW_BCM_4_324.flag  = OFF;
        CGW_BCM_4_324.timer = 0;
        // UART_Send_Book(dev, &CGW_BCM_4_324);
        Uart_Send_Func(&CGW_BCM_4_324);
    }
    else
        CGW_BCM_4_324.timer++;
	// BCM4:0x3B1
    CAN_COMM_100MS_3B1();
    if (ICM_2_3B1.flag == ON)
    {
        ICM_2_3B1.flag  = OFF;
        ICM_2_3B1.timer = 0;
        // UART_Send_Book(dev, &ICM_2_3B1);
        Uart_Send_Func(&ICM_2_3B1);
    }
    else
        ICM_2_3B1.timer++;

    // BCM5:0x323
    CAN_COMM_100MS_323();
    if (CGW_BCM_5_323.flag == ON)
    {
        CGW_BCM_5_323.flag  = OFF;
        CGW_BCM_5_323.timer = 0;
        // UART_Send_Book(dev, &CGW_BCM_5_323);
        Uart_Send_Func(&CGW_BCM_5_323);
    }
    else
        CGW_BCM_5_323.timer++;

    // BCM6:0x457
    CAN_COMM_100MS_457();
    if (BCM_6_457.flag == ON)
    {
        BCM_6_457.flag  = OFF;
        BCM_6_457.timer = 0;
        // UART_Send_Book(dev, &BCM_6_457);
        Uart_Send_Func(&BCM_6_457);
    }
    else
        BCM_6_457.timer++;

    // TPMS1:0x326
    CAN_COMM_100MS_326();
    if (BCM_TPMS_1_326.flag == ON)
    {
        BCM_TPMS_1_326.flag  = OFF;
       BCM_TPMS_1_326.timer = 0;
        // UART_Send_Book(dev, &BCM_TPMS_1_326);
        Uart_Send_Func(&BCM_TPMS_1_326);
    }
    else
        BCM_TPMS_1_326.timer++;

    // ALM:0x45D
    CAN_COMM_100MS_45D();
    if (BCM_ALM_45D.flag == ON)
    {
        BCM_ALM_45D.flag  = OFF;
       BCM_ALM_45D.timer = 0;
        // UART_Send_Book(dev, &BCM_ALM_45D);
        Uart_Send_Func(&BCM_ALM_45D);
    }
    else
        BCM_ALM_45D.timer++;
    // SCM3:0x469
    CAN_COMM_100MS_45C();
    if (CGW_BCM_SCM_3_45C.flag == ON)
    {
        CGW_BCM_SCM_3_45C.flag  = OFF;
        CGW_BCM_SCM_3_45C.timer = 0;
        // UART_Send_Book(dev, &CGW_BCM_SCM_3_45C);
        Uart_Send_Func(&CGW_BCM_SCM_3_45C);
    }
    else
        CGW_BCM_SCM_3_45C.timer++;

    // BMS3:0x46B
    // CAN_COMM_100MS_46B();
    // if (CGW_BMS_3_46B.flag == ON)
    // {
    //     CGW_BMS_3_46B.flag  = OFF;
    //     CGW_BMS_3_46B.timer = 0;
    //     // UART_Send_Book(dev, &CGW_BMS_3_46B);
    //     Uart_Send_Func(&CGW_BMS_3_46B);
    // }
    // else
    //     CGW_BMS_3_46B.timer++;

    // SCM1:0x45A
    CAN_COMM_100MS_45A();
    if (CGW_BCM_SCM_1_45A.flag == ON)
    {
        CGW_BCM_SCM_1_45A.flag  = OFF;
        CGW_BCM_SCM_1_45A.timer = 0;
        // UART_Send_Book(dev, &CGW_BCM_SCM_1_45A);
        Uart_Send_Func(&CGW_BCM_SCM_1_45A);
    }
    else
        CGW_BCM_SCM_1_45A.timer++;

    // RCM1:0x328
    CAN_COMM_100MS_328();
    if (CGW_RCM_1_328.flag == ON)
    {
        CGW_RCM_1_328.flag  = OFF;
        CGW_RCM_1_328.timer = 0;
        // UART_Send_Book(dev, &CGW_RCM_1_328);
        Uart_Send_Func(&CGW_RCM_1_328);
    }
    else
        CGW_RCM_1_328.timer++;

    // EAC1:0x3D8
    CAN_COMM_100MS_3D8();
    if (CGW_BCM_EAC_1_3D8.flag == ON)
    {
        CGW_BCM_EAC_1_3D8.flag  = OFF;
        CGW_BCM_EAC_1_3D8.timer = 0;
        // UART_Send_Book(dev, &CGW_BCM_EAC_1_3D8);
        Uart_Send_Func(&CGW_BCM_EAC_1_3D8);
    }
    else
        CGW_BCM_EAC_1_3D8.timer++;

    // SAS2:0x504
    CAN_COMM_100MS_504();
    if (CGW_SAS_2_504.flag == ON)
    {
        CGW_SAS_2_504.flag  = OFF;
        CGW_SAS_2_504.timer = 0;
        // UART_Send_Book(dev, &CGW_SAS_2_504);
        Uart_Send_Func(&CGW_SAS_2_504);
    }
    else
        CGW_SAS_2_504.timer++;

    // VCU_3_6:0x313
    CAN_COMM_100MS_313();
    if (CGW_VCU_3_6_313.flag == ON)
    {
        CGW_VCU_3_6_313.flag  = OFF;
        CGW_VCU_3_6_313.timer = 0;
        // UART_Send_Book(dev, &CGW_VCU_3_6_313);
        Uart_Send_Func(&CGW_VCU_3_6_313);
    }
    else
        CGW_VCU_3_6_313.timer++;

    // VCU_3_7:0x314
    CAN_COMM_100MS_314();
    if (CGW_VCU_3_7_314.flag == ON)
    {
        CGW_VCU_3_7_314.flag  = OFF;
        CGW_VCU_3_7_314.timer = 0;
        // UART_Send_Book(dev, &CGW_VCU_3_7_314);
        Uart_Send_Func(&CGW_VCU_3_7_314);
    }
    else
        CGW_VCU_3_7_314.timer++;

    // VCU_3_8:0x315
    CAN_COMM_100MS_315();
    if (CGW_VCU_B_5_315.flag == ON)
    {
        CGW_VCU_B_5_315.flag  = OFF;
        CGW_VCU_B_5_315.timer = 0;
        // UART_Send_Book(dev, &CGW_VCU_B_5_315);
        Uart_Send_Func(&CGW_VCU_B_5_315);
    }
    else
        CGW_VCU_B_5_315.timer++;

    // BMS9:0x355
    CAN_COMM_100MS_355();
    if (CGW_BMS_9_355.flag == ON)
    {
        CGW_BMS_9_355.flag  = OFF;
        CGW_BMS_9_355.timer = 0;
        // UART_Send_Book(dev, &CGW_BMS_9_355);
        Uart_Send_Func(&CGW_BMS_9_355);
    }
    else
        CGW_BMS_9_355.timer++;

    // FCM5:0x4DD
    CAN_COMM_100MS_4DD();
    if (CGW_FCM_5_4DD.flag == ON)
    {
        CGW_FCM_5_4DD.flag  = OFF;
        CGW_FCM_5_4DD.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_5_4DD);
        Uart_Send_Func(&CGW_FCM_5_4DD);
    }
    else
        CGW_FCM_5_4DD.timer++;

    // FCM_FRM5:0x4E3
    CAN_COMM_100MS_4E3();
    if (CGW_FCM_FRM_5_4E3.flag == ON)
    {
        CGW_FCM_FRM_5_4E3.flag  = OFF;
        CGW_FCM_FRM_5_4E3.timer = 0;
        // UART_Send_Book(dev, &CGW_FCM_FRM_5_4E3);
        Uart_Send_Func(&CGW_FCM_FRM_5_4E3);
    }
    else
        CGW_FCM_FRM_5_4E3.timer++;

    // PLG1:0x32A
    CAN_COMM_100MS_32A();
    if (CGW_PLG_1_32A.flag == ON)
    {
        CGW_PLG_1_32A.flag  = OFF;
        CGW_PLG_1_32A.timer = 0;
        // UART_Send_Book(dev, &CGW_PLG_1_32A);
        Uart_Send_Func(&CGW_PLG_1_32A);
    }
    else
        CGW_PLG_1_32A.timer++;

} // 12

uint8 CHK_CAN_COMM_200MS_MSG(DEVICE *dev)
{
    // FM:0x458
    CAN_COMM_200MS_458();
    if (CGW_BCM_FM_458.flag == ON)
    {
        CGW_BCM_FM_458.flag  = OFF; // for next difference check
        CGW_BCM_FM_458.timer = 0;   // for slow send postponed

        // UART_Send_Book(dev, &CGW_BCM_FM_458);
        Uart_Send_Func(&CGW_BCM_FM_458);
    }
    else
        CGW_BCM_FM_458.timer++; // slow send timer

    // VCU_3_12:0x347
    CAN_COMM_200MS_347();
    if (CGW_VCU_3_12_347.flag == ON)
    {
        CGW_VCU_3_12_347.flag  = OFF;
        CGW_VCU_3_12_347.timer = 0;
        // UART_Send_Book(dev, &CGW_VCU_3_12_347);
        Uart_Send_Func(&CGW_VCU_3_12_347);
    }
    else
        CGW_VCU_3_12_347.timer++;
} // 2

uint8 CHK_CAN_COMM_500MS_MSG(DEVICE *dev)
{
    // TPMS2:0x455
    CAN_COMM_500MS_455();
    if (CGW_BCM_TPMS_2_455.flag == ON)
    {
        CGW_BCM_TPMS_2_455.flag  = OFF;
        CGW_BCM_TPMS_2_455.timer = 0;

        // UART_Send_Book(dev, &CGW_BCM_TPMS_2_455);
        Uart_Send_Func(&CGW_BCM_TPMS_2_455);
    }
    else
        CGW_BCM_TPMS_2_455.timer++;

    // TPMS2:0x3D4
    CAN_COMM_500MS_3D4();
    if (ACCM_3_3D4.flag == ON)
    {
        ACCM_3_3D4.flag  = OFF;
        ACCM_3_3D4.timer = 0;

        // UART_Send_Book(dev, &ACCM_3_3D4);
        Uart_Send_Func(&ACCM_3_3D4);
    }
    else
        ACCM_3_3D4.timer++;

    // SCM_L:0x45A

    // SCM_R:0x45B
    CAN_COMM_500MS_45B();
    if (CGW_SCM_R_45B.flag == ON)
    {
        CGW_SCM_R_45B.flag  = OFF;
        CGW_SCM_R_45B.timer = 0;
        // UART_Send_Book(dev, &CGW_SCM_R_45B);
        Uart_Send_Func(&CGW_SCM_R_45B);
    }
    else
        CGW_SCM_R_45B.timer++;

    // TBOX_4_7:0x4A8
    CAN_COMM_500MS_4A8();
    if (TBOX_4_7_4A8.flag == ON)
    {
        TBOX_4_7_4A8.flag  = OFF;
        TBOX_4_7_4A8.timer = 0;
        // UART_Send_Book(dev, &TBOX_4_7_4A8);
        Uart_Send_Func(&TBOX_4_7_4A8);
    }
    else
        TBOX_4_7_4A8.timer++;

    // OBC1:0x461
    CAN_COMM_500MS_461();
    if (CGW_OBC_1_461.flag == ON)
    {
        CGW_OBC_1_461.flag  = OFF;
        CGW_OBC_1_461.timer = 0;
        // UART_Send_Book(dev, &CGW_OBC_1_461);
        Uart_Send_Func(&CGW_OBC_1_461);
    }
    else
        CGW_OBC_1_461.timer++;
} // 3

uint8 CHK_CAN_COMM_1000MS_MSG(DEVICE *dev)
{
    // AVAS1:0x4B1
    CAN_COMM_1000MS_4B1();
    if (AVAS_1_4B1.flag == ON)
    {
        AVAS_1_4B1.flag  = OFF;
        AVAS_1_4B1.timer = 0;
        // UART_Send_Book(dev, &AVAS_1_4B1);
        Uart_Send_Func(&AVAS_1_4B1);
    }
    else
        AVAS_1_4B1.timer++;

    // WCM1:0x4BA
    CAN_COMM_1000MS_4BA();
    if (WCM_1_4BA.flag == ON)
    {
        WCM_1_4BA.flag  = OFF;
        WCM_1_4BA.timer = 0;
        // UART_Send_Book(dev, &WCM_1_4BA);
        Uart_Send_Func(&WCM_1_4BA);
    }
    else
        WCM_1_4BA.timer++;

} // 2

//锟斤拷锟斤拷DVR锟斤拷锟秸诧拷锟斤拷:(实时锟斤拷锟斤拷)
void CAN_COMM_10MS_294(void)
{
    CGW_BCM_MFS_1_294.buf[2] = 0x01; // need to change Node absent state
                             // Active = 1
                             // Absent = 0

    if ((CGW_BCM_MFS_1_294.buf[3] != CGW_BCM_MFS_1._c[0]) || (CGW_BCM_MFS_1_294.buf[4] != CGW_BCM_MFS_1._c[1]) || (CGW_BCM_MFS_1_294.buf[5] != CGW_BCM_MFS_1._c[2]) || (CGW_BCM_MFS_1_294.buf[6] != CGW_BCM_MFS_1._c[3]) ||
        (CGW_BCM_MFS_1_294.buf[7] != CGW_BCM_MFS_1._c[4]) || (CGW_BCM_MFS_1_294.buf[8] != CGW_BCM_MFS_1._c[5]) || (CGW_BCM_MFS_1_294.buf[9] != CGW_BCM_MFS_1._c[6]) || (CGW_BCM_MFS_1_294.buf[10] != CGW_BCM_MFS_1._c[7]))
    {
        CGW_BCM_MFS_1_294.flag = ON;

        CGW_BCM_MFS_1_294.buf[3]  = CGW_BCM_MFS_1._c[0];
        CGW_BCM_MFS_1_294.buf[4]  = CGW_BCM_MFS_1._c[1];
        CGW_BCM_MFS_1_294.buf[5]  = CGW_BCM_MFS_1._c[2];
        CGW_BCM_MFS_1_294.buf[6]  = CGW_BCM_MFS_1._c[3];
        CGW_BCM_MFS_1_294.buf[7]  = CGW_BCM_MFS_1._c[4];
        CGW_BCM_MFS_1_294.buf[8]  = CGW_BCM_MFS_1._c[5];
        CGW_BCM_MFS_1_294.buf[9]  = CGW_BCM_MFS_1._c[6];
        CGW_BCM_MFS_1_294.buf[10] = CGW_BCM_MFS_1._c[7];
    }
    else
    {
        CGW_BCM_MFS_1_294.flag = FALSE;
    }
}
void CAN_COMM_100MS_326(void)
{
    BCM_TPMS_1_326.buf[2] = 0x01; // need to change Node absent state
                             // Active = 1
                             // Absent = 0

    if ((BCM_TPMS_1_326.buf[3] != BCM_TPMS_1._c[0]) || (BCM_TPMS_1_326.buf[4] != BCM_TPMS_1._c[1]) || (BCM_TPMS_1_326.buf[5] != BCM_TPMS_1._c[2]) || (BCM_TPMS_1_326.buf[6] != BCM_TPMS_1._c[3]) ||
        (BCM_TPMS_1_326.buf[7] != BCM_TPMS_1._c[4]) || (BCM_TPMS_1_326.buf[8] != BCM_TPMS_1._c[5]) || (BCM_TPMS_1_326.buf[9] != BCM_TPMS_1._c[6]) || (BCM_TPMS_1_326.buf[10] != BCM_TPMS_1._c[7]))
    {
        BCM_TPMS_1_326.flag = ON;

        BCM_TPMS_1_326.buf[3]  = BCM_TPMS_1._c[0];
        BCM_TPMS_1_326.buf[4]  = BCM_TPMS_1._c[1];
        BCM_TPMS_1_326.buf[5]  = BCM_TPMS_1._c[2];
        BCM_TPMS_1_326.buf[6]  = BCM_TPMS_1._c[3];
        BCM_TPMS_1_326.buf[7]  = BCM_TPMS_1._c[4];
        BCM_TPMS_1_326.buf[8]  = BCM_TPMS_1._c[5];
        BCM_TPMS_1_326.buf[9]  = BCM_TPMS_1._c[6];
        BCM_TPMS_1_326.buf[10] = BCM_TPMS_1._c[7];
    }
    else
    {
        BCM_TPMS_1_326.flag = FALSE;
    }
}
void CAN_COMM_100MS_45D(void)
{
    BCM_ALM_45D.buf[2] = 0x01; // need to change Node absent state
                             // Active = 1
                             // Absent = 0

    if ((BCM_ALM_45D.buf[3] != BCM_ALM._c[0]) || (BCM_ALM_45D.buf[4] != BCM_ALM._c[1]) || (BCM_ALM_45D.buf[5] != BCM_ALM._c[2]) || (BCM_ALM_45D.buf[6] != BCM_ALM._c[3]) ||
        (BCM_ALM_45D.buf[7] != BCM_ALM._c[4]) || (BCM_ALM_45D.buf[8] != BCM_ALM._c[5]) || (BCM_ALM_45D.buf[9] != BCM_ALM._c[6]) || (BCM_ALM_45D.buf[10] != BCM_ALM._c[7]))
    {
        BCM_ALM_45D.flag = ON;

        BCM_ALM_45D.buf[3]  = BCM_ALM._c[0];
        BCM_ALM_45D.buf[4]  = BCM_ALM._c[1];
        BCM_ALM_45D.buf[5]  = BCM_ALM._c[2];
        BCM_ALM_45D.buf[6]  = BCM_ALM._c[3];
        BCM_ALM_45D.buf[7]  = BCM_ALM._c[4];
        BCM_ALM_45D.buf[8]  = BCM_ALM._c[5];
        BCM_ALM_45D.buf[9]  = BCM_ALM._c[6];
        BCM_ALM_45D.buf[10] = BCM_ALM._c[7];
    }
    else
    {
        BCM_ALM_45D.flag = FALSE;
    }
}
void CAN_COMM_10MS_123(void)
{
    int8  PreValue     = 0;
    uint8 Chksum_check = 0; // for checksum disable = 0...change to define later

    CGW_VCU_P_5_123.buf[2] = 0x01; // need to change Node absent state
                                   // Active = 1
                                   // Absent = 0

    if ((CGW_VCU_P_5_123.buf[3] != CGW_VCU_P_5._c[0]) || (CGW_VCU_P_5_123.buf[6] != CGW_VCU_P_5._c[3]) || (CGW_VCU_P_5_123.buf[7] != CGW_VCU_P_5._c[4]) ||
        (CGW_VCU_P_5_123.buf[8] != CGW_VCU_P_5._c[5]))
    {
        CGW_VCU_P_5_123.flag = ON;

        CGW_VCU_P_5_123.buf[3]  = CGW_VCU_P_5._c[0];
        CGW_VCU_P_5_123.buf[4]  = 0x00;
        CGW_VCU_P_5_123.buf[5]  = 0x00;
        CGW_VCU_P_5_123.buf[6]  = CGW_VCU_P_5._c[3];
        CGW_VCU_P_5_123.buf[7]  = CGW_VCU_P_5._c[4];
        CGW_VCU_P_5_123.buf[8]  = CGW_VCU_P_5._c[5];
        CGW_VCU_P_5_123.buf[9]  = 0x00;
        CGW_VCU_P_5_123.buf[10] = 0x00;

#ifdef INTERNAL_AMP
#ifdef SVCD_ENABLE
        if (IlGetRxVCU_VehSpd_VD() == 0)
        {
            r_Device.r_System->svcd_speed = (uint8)(IlGetRxVCU_VehSpd() / 16);
        }
#endif
#endif
    }
    else
    {
        CGW_VCU_P_5_123.flag = FALSE;
    }
}

/*
    CAN锟斤拷锟截达拷锟斤拷:
    1锟斤拷ACC锟斤拷锟斤拷应巡锟斤拷模式/TJA_ICA/锟斤拷锟斤拷巡锟斤拷>锟角憋拷锟斤拷锟斤拷模式>锟斤拷锟接撅拷锟斤拷锟斤拷模式>I_driver锟斤拷锟斤拷模式>默锟较碉拷锟斤拷模式锟斤拷
    2锟斤拷锟斤拷锟斤拷实锟街凤拷式锟斤拷为锟斤拷值锟斤拷锟斤拷锟酵憋拷锟斤拷转锟斤拷式锟斤拷锟斤拷锟斤拷实锟街猴拷锟斤拷锟斤拷式锟侥帮拷锟斤拷锟斤拷取锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟侥癸拷锟斤拷模式锟斤拷
    3锟斤拷锟斤拷值锟斤拷式锟侥帮拷锟斤拷锟斤拷锟借处锟斤拷锟斤拷通锟斤拷锟斤拷锟斤拷锟斤拷式转锟斤拷锟斤拷SOC锟斤拷锟叫达拷锟斤拷锟斤拷CAN锟斤拷锟斤拷锟斤拷式锟侥帮拷锟斤拷锟斤拷MCU锟斤拷锟斤拷锟襟，凤拷锟酵革拷模式锟侥匡拷锟狡憋拷锟侥ｏ拷锟斤拷锟借发锟酵达拷锟斤拷协锟斤拷锟絊OC锟斤拷
*/

// 取锟斤拷锟斤拷锟斤拷锟侥斤拷锟斤拷锟竭硷拷锟斤拷锟斤拷锟斤拷转锟斤拷锟斤拷值锟斤拷息锟斤拷SOC锟斤拷锟斤拷锟姐不同应锟矫筹拷锟斤拷锟斤拷锟斤拷锟斤拷
#if 0
void CAN_COMM_10MS_294(void)
{
	static uint16_t tmp = 0;

	static uint8_t  BlowerLevelValue = 0;           //锟秸碉拷锟斤拷锟斤拷值
	static uint8_t  TempSetValue = 0;               //锟秸碉拷锟铰讹拷值
	static uint8_t  HardKeyResetCnt = 0;            //锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷

    //锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
	static uint8_t  KeyOperStatus = 0;
	static uint8_t  KeyCount = 0;

	static uint8_t  RightToggleLeftReleaseStatus = 0;
	static uint8_t  RightToggleRightReleaseStatus = 0;

	/*
	static uint8_t L_button_prees_cnt = 0;
	static uint8_t R_button_prees_cnt = 0;
	static uint8_t Custom_button_prees_cnt = 0;
	static uint8_t Return_prees_cnt = 0;
	*/

	//buf0-buf3锟斤拷锟节存储CAN锟斤拷锟截憋拷锟斤拷
	static uint8_t buf0 = 0;
	static uint8_t buf1 = 0;
	static uint8_t buf2 = 0;
	static uint8_t buf3 = 0;

	/*
	static uint8_t Long_chk_flg_L = 0;
	static uint8_t Long_chk_flg_R = 0;
	static uint8_t Long_chk_flg_CT = 0;
	static uint8_t Long_chk_flg_RE = 0;
	*/

	static uint8_t  CustomPressStatus = 0;    //锟皆讹拷锟藉按锟斤拷锟斤拷始状态
	static uint8_t  RightWheelButtonPressStatus = 0;       //锟揭癸拷锟街帮拷锟斤拷锟斤拷始状态
	static uint8_t  BackButtonPressStatus = 0;      //锟斤拷锟截帮拷锟斤拷锟斤拷始状态
	static uint8_t  WeChatButttonStatus = 0;  //微锟脚帮拷锟斤拷锟斤拷始状态
	static uint8_t  LeftToggleLeftStatus = 0;    //锟斤拷锟斤拷蟀醇锟斤拷锟绞甲刺�
	static uint8_t  LeftToggleRightStatus = 0;    //锟斤拷锟斤拷野锟斤拷锟斤拷锟绞甲刺�

	static uint8_t  RightWheelScrollUpStatus = 0;      //锟揭诧拷锟斤拷锟较癸拷锟斤拷锟斤拷锟斤拷锟斤拷始状态
	static uint8_t  RightWheelScrollDownStatus = 0;    //锟揭诧拷锟斤拷锟铰癸拷锟斤拷锟斤拷锟斤拷锟斤拷始状态
	static uint8_t  RightToggleLeftStatus = 0;      //锟揭诧拷锟襟按硷拷锟斤拷始状态
	static uint8_t  RightToggleRightStatus = 0;     //锟揭诧拷锟揭帮拷锟斤拷锟斤拷始状态

// 	/* MCU锟斤拷锟斤拷CAN锟斤拷锟斤拷锟竭硷拷锟斤拷锟斤拷
	if(MFS_1._c[0] != buf0)
	{
		buf0 = MFS_1._c[0];

        //锟斤拷喟达拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟窖诧拷锟斤拷锟斤拷锟绞癸拷茫锟斤拷锟街寡诧拷锟侥Ｊ斤拷拢锟斤拷锟斤拷锟窖诧拷锟斤拷俣鹊锟斤拷锟绞憋拷锟斤拷锟斤拷止锟斤拷芨锟斤拷系锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟矫伙拷锟斤拷要锟侥癸拷锟斤拷
        if(EnterCruiseMode == 0x01)
		{
            //锟斤拷锟斤拷巡锟斤拷模式锟铰ｏ拷锟斤拷锟斤拷锟斤拷业锟斤拷诎锟斤拷锟斤拷锟饺伙拷锟斤拷锟斤拷盏锟斤拷锟斤拷锟斤拷锟斤拷诠锟斤拷锟�
            if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)   //锟斤拷锟斤拷-
		    {
                LeftToggleLeftStatus = 1;

				tmp = 31;
				CGW_BCM_MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "Cruise Mode LeftWheelButtonSts_ToggleLeft Key Press\n");
		    }
			else if(LeftToggleLeftStatus == 1)
			{
                if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 0)
				{
                     LeftToggleLeftStatus = 0;

				     tmp = 31;
			         MFS_1_294.flag = ON;
			         MFS_1_294.buf[0] = tmp>>8;
			         MFS_1_294.buf[1] = tmp;
			         MFS_1_294.buf[2] = 0;
			         MFS_1_294.buf[3] = 0;
			         //DPRINTF(DBG_INFO, "IlGetRxMFS_WeChatButtonSts Key216\n");
                }
			}

			if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)   //锟斤拷锟斤拷+
		    {
                LeftToggleRightStatus = 1;

				tmp = 32;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "Cruise Mode LeftWheelButtonSts_ToggleRight Key Press\n");
		    }
			else if(LeftToggleRightStatus == 1)
			{
                if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 0)   //锟斤拷锟斤拷+
		        {
                    LeftToggleRightStatus = 0;

				    tmp = 32;
				    MFS_1_294.flag = ON;
			        MFS_1_294.buf[0] = tmp>>8;
			        MFS_1_294.buf[1] = tmp;
				    MFS_1_294.buf[2] = 0;
			        MFS_1_294.buf[3] = 0;
			        //DPRINTF(DBG_INFO, "Cruise Mode LeftWheelButtonSts_ToggleRight Key Press\n");
		        }
			}

			//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷巡锟斤拷模式锟斤拷锟劫度碉拷锟斤拷
		    if((IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)||(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1))   //巡锟斤拷锟劫讹拷+
            {
                MFS_1_294.flag = OFF;
            }
        }

		//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷野锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷潜锟斤拷锟斤拷锟绞癸拷锟�
        else if(EnterIcmMode == 0x01)
        {
            MFS_1_294.flag = ON;

			if(IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)   //锟斤拷值占锟斤拷锟斤拷锟街斤拷
		    {
			    tmp = 120;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ScrollUp Key120\n");
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1)
		    {
			    tmp = 121;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ScrollDown Key121\n");
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)
		    {
			    tmp = 122;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ToggleLeft Key122\n");
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)
		    {
			    tmp = 123;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ToggleRight Key123\n");
		    }
			else if(IlGetRxMFS_LeftWheelButtonSts_Press() == 1)
			{
			    tmp = 124;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ToggleRight Key123\n");
		    }
		    else
		    {
			    //tmp = 0;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
                //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ScrollUp  Test002\n");
		    }
        }

        //锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷野锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟接撅拷锟斤拷锟斤拷使锟斤拷
        else if(EnterMirrorMode == 0x01)
        {
            MFS_1_294.flag = OFF;

			//DPRINTF(DBG_INFO, "Enter Left Rear Mirror Mode! \n");

			EnterIdriveMode = 0x00;

			if(IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)
            {
                IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(1);
            }
			else if(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(2);
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(3);
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(4);
		    }
        }

		//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷野锟斤拷锟斤拷锟斤拷锟斤拷锟侥拷习锟斤拷锟绞癸拷锟�,实锟街空碉拷锟斤拷锟斤拷
		else
        {
            if(IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)   //锟斤拷值占锟斤拷锟斤拷锟街斤拷  Temp+
		    {
			    tmp = 29;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ScrollUp Key29\n");
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1)   //Temp-
		    {
			    tmp = 30;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_LeftWheelButtonSts_ScrollDown Key30\n");
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)   //锟斤拷锟斤拷-
		    {
			    tmp = 31;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "Default Mode LeftWheelButtonSts_ToggleLeft Key Press\n");
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)   //锟斤拷锟斤拷+
		    {
			    tmp = 32;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "Default Mode LeftWheelButtonSts_ToggleRight Key Press\n");
		    }
		    else
		    {
			    //tmp = 0;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
                //DPRINTF(DBG_INFO, "Default Mode Key  Release\n");
		    }
        }
	}

	if(MFS_1._c[1] != buf1)
	{
		buf1 = MFS_1._c[1];

        //锟秸碉拷锟斤拷锟截硷拷
		if(IlGetRxMFS_LeftWheelButtonSts_Press() == 1)
	    {
		    IHU6OperFlag = TRUE;
		    IHU6TimerCount = 0;

			tmp = 0;

			MFS_1_294.flag = OFF;

		    IlPutTxIHU_System_ON_OFF_SwSts(1);
	    }

		//微锟脚硷拷
		if(IlGetRxMFS_WeChatButtonSts() == 1)
		{
            WeChatButttonStatus = 1;

			tmp = 216;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
			//DPRINTF(DBG_INFO, "IlGetRxMFS_WeChatButtonSts Key216\n");
		}
		else if(WeChatButttonStatus == 1)
		{
            if(IlGetRxMFS_WeChatButtonSts() == 0)
            {
                WeChatButttonStatus = 0;

				tmp = 216;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_WeChatButtonSts Key216\n");
            }
		}

		//锟皆讹拷锟斤拷锟�
		if(IlGetRxMFS_CustomButtonSts() == 1)     //锟皆讹拷锟藉按锟斤拷锟斤拷锟斤拷
	    {
            CustomPressStatus = 1;

			tmp = 316;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;

			//DPRINTF(DBG_INFO, "IlGetRxMFS_CustomButtonSts Key316  Press! \n");
		}
        else if(CustomPressStatus == 1)     //锟皆讹拷锟斤拷锟教э拷锟�
		{
            if(IlGetRxMFS_CustomButtonSts() == 0)
            {
                CustomPressStatus = 0;

			    tmp = 316;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;

				//DPRINTF(DBG_INFO, "IlGetRxMFS_CustomButtonSts Key316  Release! \n");
            }
		}
    }

	//锟揭诧拷锟斤拷锟斤拷锟斤拷锟揭帮拷锟斤拷
	if(MFS_1._c[2] != buf2)
	{
		buf2 = MFS_1._c[2];

        //Idrive锟斤拷锟斤拷状态锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟揭碉拷锟节癸拷锟斤拷
		if(EnterIdriveMode == 0x01)
		{
            if(IlGetRxMFS_RightWheelButtonSts_ScrollUp() == 1)
		    {
			    tmp = 103;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollUp Idrive  Key103\n");
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ScrollDown() == 1)
		    {
			    tmp = 108;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollDown Idrive  Key108\n");
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleLeft() == 1)
		    {
			    tmp = 105;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ToggleLeft Idrive Key105\n");
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleRight() == 1)
		    {
			    tmp = 106;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ToggleRight Idrive  Key106\n");
		    }
		    else
		    {
			    //tmp = 0;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
		    }
		}

		//锟揭侧按锟斤拷锟斤拷锟斤拷锟斤拷锟揭猴拷锟接撅拷锟斤拷锟斤拷
		else if(EnterMirrorMode == 0x01)
		{
            MFS_1_294.flag = OFF;

			//DPRINTF(DBG_INFO, "Enter Right Rear Mirror Mode! \n");

			EnterIdriveMode = 0x00;

			if(IlGetRxMFS_RightWheelButtonSts_ScrollUp() == 1)
            {
                IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(1);
            }
			else if(IlGetRxMFS_RightWheelButtonSts_ScrollDown() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(2);
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleLeft() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(3);
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleRight() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(4);
		    }
		}

		//锟揭侧按锟斤拷状态锟斤拷锟斤拷锟斤拷默锟较硷拷使锟矫ｏ拷实锟斤拷锟斤拷锟斤拷锟接硷拷锟斤拷锟斤拷锟斤拷锟斤拷锟叫伙拷
		else
		{
            MFS_1_294.flag = ON;

			if(IlGetRxMFS_RightWheelButtonSts_ScrollUp() == 1)
		    {
			    tmp = 115;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollUp Key115\n");
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ScrollDown() == 1)
		    {
			    tmp = 114;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollDown Key114\n");
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleLeft() == 1)
		    {
			    tmp = 163;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ToggleLeft Key163\n");
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleRight() == 1)
		    {
			    tmp = 165;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ToggleRight Key165\n");
		    }
		    else
		    {
			    //tmp = 0;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
		    }
		}
	}

	if(MFS_1._c[3] != buf3)
	{
		buf3 = MFS_1._c[3];

        //锟斤拷锟斤拷锟斤拷锟斤拷
		if(IlGetRxMFS_VoiceRecognitionButtonSts() == 1)      //锟斤拷锟斤拷锟斤拷锟斤拷
		{
			tmp = 582;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
			//DPRINTF(DBG_INFO, "IlGetRxMFS_VoiceRecognitionButtonSts Key582\n");
		}
		else if(IlGetRxMFS_VoiceRecognitionButtonSts() == 0)
		{
			//tmp = 0;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 0;
			MFS_1_294.buf[3] = 0;
		}

        //锟揭诧拷锟斤拷职锟斤拷锟�
		if(IlGetRxMFS_RightWheelButtonSts_Press() == 1)    //锟揭诧拷锟斤拷职锟斤拷锟斤拷锟斤拷锟�
		{
            RightWheelButtonPressStatus = 1;

			tmp = 28;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
		}
        else if(RightWheelButtonPressStatus == 1)    //锟揭诧拷锟斤拷职锟斤拷锟斤拷头锟�
		{
            if(IlGetRxMFS_RightWheelButtonSts_Press() == 0)
            {
                RightWheelButtonPressStatus = 0;

				tmp = 28;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
            }
		}

		//锟斤拷锟截硷拷
		if(IlGetRxMFS_ReturnButtonSts() == 1)    //锟斤拷锟截帮拷锟斤拷锟斤拷锟斤拷
		{
            BackButtonPressStatus = 1;

			tmp = 158;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
		}
        else if(BackButtonPressStatus == 1)     //锟斤拷锟截帮拷锟斤拷锟酵凤拷
		{
            if(IlGetRxMFS_ReturnButtonSts() == 0)
            {
                BackButtonPressStatus = 0;

				tmp = 158;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
            }
		}
	}

	//Idrive 锟竭硷拷锟斤拷锟斤拷
	//锟揭诧拷锟叫硷拷锟斤拷锟斤拷锟节短帮拷锟斤拷锟斤拷锟斤拷锟斤拷
	if(IlGetRxMFS_RightWheelButtonSts_Press() == 1)
	{
		if(R_button_prees_cnt < 200)
			R_button_prees_cnt++;

		if(R_button_prees_cnt > 5 && R_button_prees_cnt < 200)
		{
			R_button_prees_cnt = 0;
		    Long_chk_flg_R = 0;

		    EnterIdriveMode = 0x01;
		}
	}

	//锟斤拷锟截硷拷锟斤拷锟斤拷锟节短帮拷锟斤拷锟斤拷锟斤拷锟斤拷
	if(IlGetRxMFS_ReturnButtonSts() == 1)
	{
		if(Return_prees_cnt < 200)
			Return_prees_cnt++;

		//锟斤拷锟截硷拷锟教帮拷锟斤拷锟剿筹拷Idrive锟斤拷锟斤拷状态
		if(Return_prees_cnt > 5 && Return_prees_cnt < 200)
		{
			Return_prees_cnt = 0;
		    Long_chk_flg_RE = 0;

			if(EnterIdriveMode == 0x01)     //锟斤拷锟斤拷锟斤拷锟截硷拷锟剿筹拷Idrive模式锟斤拷锟斤拷确锟斤拷
		    {
                EnterIdriveMode = 0x00;
		    }
		}
    }

/*锟斤拷锟斤拷S52锟铰讹拷锟斤拷陌锟斤拷锟斤拷锟斤拷芙锟斤拷锟绞碉拷锟�(锟斤拷模式锟斤拷锟斤拷锟斤拷SOC通知锟斤拷状态为锟斤拷 */
//锟斤拷喟达拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟窖诧拷锟斤拷锟斤拷锟绞癸拷茫锟斤拷锟街寡诧拷锟侥Ｊ斤拷拢锟斤拷锟斤拷锟窖诧拷锟斤拷俣鹊锟斤拷锟绞憋拷锟斤拷锟斤拷止锟斤拷芨锟斤拷系锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟矫伙拷锟斤拷要锟侥癸拷锟斤拷
    if(CanKeyWorkMode == 0x05)     //巡锟斤拷模式锟铰ｏ拷锟斤拷锟斤拷应巡锟斤拷锟劫度碉拷锟节诧拷锟斤拷
    {
        //巡锟斤拷模式锟铰ｏ拷锟斤拷锟斤拷巡锟斤拷锟斤拷锟斤拷时锟斤拷锟斤拷锟街碉拷锟斤拷巡锟斤拷锟劫度ｏ拷锟侥憋拷锟剿空碉拷锟铰度碉拷锟斤拷锟斤拷
        if(MFS_1._c[0] != buf0)    //锟秸碉拷锟斤拷锟斤拷锟斤拷锟斤拷只锟斤拷锟节短帮拷锟斤拷锟斤拷
        {
            buf0 = MFS_1._c[0];

		    MFS_1_294.flag = OFF;

			BlowerLevelValue = IlGetRxACCM_BlowerlevelSetSts();

			if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)   //锟斤拷锟斤拷-
		    {
                if(BlowerLevelValue <= 0x01)   //锟斤拷锟斤拷锟窖达到锟斤拷小值锟斤拷锟睫凤拷锟斤拷锟斤拷锟斤拷
                {
                    BlowerLevelValue = 0x01;
                }
				else
				{
                    BlowerLevelValue = BlowerLevelValue - 1;
				}

				IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_BlowerlevelSet(BlowerLevelValue);
			}

			if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)   //锟斤拷锟斤拷+
			{
                if(BlowerLevelValue >= 0x07)   //锟斤拷锟斤拷锟窖达到锟斤拷锟街碉拷锟斤拷薹锟斤拷臃锟斤拷锟�
                {
                    BlowerLevelValue = 0x07;
                }
				else
				{
                    BlowerLevelValue = BlowerLevelValue + 1;
				}

				IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_BlowerlevelSet(BlowerLevelValue);
			}

			//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷巡锟斤拷模式锟斤拷锟劫度碉拷锟节ｏ拷锟斤拷止锟斤拷锟斤拷巡锟斤拷锟斤拷锟斤拷时锟斤拷影锟斤拷盏锟斤拷露锟�
		    if((IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)||(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1))   //巡锟斤拷锟劫讹拷+
            {
                MFS_1_294.flag = OFF;
            }
        }
    }

	//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷野锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟接撅拷锟斤拷锟斤拷使锟斤拷
    else if(CanKeyWorkMode == 0x02)    //锟斤拷锟接撅拷锟斤拷锟斤拷模式
    {
        if(MFS_1._c[0] != 0)
        {
			MFS_1_294.flag = OFF;

			//DPRINTF(DBG_INFO, "Enter Left Rear Mirror Mode! \n");

			if(IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)
            {
                IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(1);
            }
			else if(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(2);
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(3);
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_L(4);
		    }
        }
    }

	//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷野锟斤拷锟斤拷锟斤拷锟斤拷锟侥拷习锟斤拷锟绞癸拷锟�,实锟街空碉拷锟斤拷锟斤拷
	else
    {
        if(MFS_1._c[0] != buf0)
        {
            buf0 = MFS_1._c[0];

			//锟斤拷为MCU锟斤拷锟节凤拷锟斤拷锟斤拷锟铰度ｏ拷锟斤拷实锟斤拷锟斤拷SOC没锟叫癸拷锟斤拷之前锟斤拷通锟斤拷MCU实锟街空碉拷锟铰度和凤拷锟斤拷锟侥匡拷锟狡和碉拷锟斤拷
            MFS_1_294.flag = OFF;

            BlowerLevelValue = IlGetRxACCM_BlowerlevelSetSts();
			TempSetValue = IlGetRxACCM_TempSetSts();

			if(IlGetRxMFS_LeftWheelButtonSts_ScrollUp() == 1)   //锟铰讹拷+
		    {
			    if(TempSetValue >= 0x1D)   //锟铰讹拷锟窖达到锟斤拷锟街碉拷锟斤拷薹锟斤拷锟斤拷露锟�
			    {
                    TempSetValue = 0x1D;
			    }
				else
				{
                    TempSetValue = TempSetValue + 1;
				}

				IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_TempSet(TempSetValue);
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ScrollDown() == 1)   //锟铰讹拷-
		    {
			    if(TempSetValue <= 0x01)    //锟铰讹拷锟窖达到锟斤拷小值锟斤拷锟睫凤拷锟斤拷锟铰讹拷
			    {
                    TempSetValue = 0x01;
			    }
				else
				{
                    TempSetValue = TempSetValue - 1;
				}

				IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_TempSet(TempSetValue);
		    }
		    else if(IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() == 1)   //锟斤拷锟斤拷-
		    {
                if(BlowerLevelValue <= 0x01)    //锟斤拷锟斤拷锟窖达到锟斤拷小值锟斤拷锟睫凤拷锟斤拷锟斤拷锟斤拷
                {
                    BlowerLevelValue = 0x01;
                }
				else
				{
                    BlowerLevelValue = BlowerLevelValue - 1;
				}

				IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_BlowerlevelSet(BlowerLevelValue);
			}
            else if(IlGetRxMFS_LeftWheelButtonSts_ToggleRight() == 1)   //锟斤拷锟斤拷+
			{
                if(BlowerLevelValue >= 0x07)    //锟斤拷锟斤拷锟窖撅拷锟斤到锟斤拷锟街碉拷锟斤拷薹锟斤拷臃锟斤拷锟�
                {
                    BlowerLevelValue = 0x07;
                }
				else
				{
                    BlowerLevelValue = BlowerLevelValue + 1;
				}

				IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_BlowerlevelSet(BlowerLevelValue);
			}
        }
	}

    //CanData[1]
	if(MFS_1._c[1] != buf1)
	{
		buf1 = MFS_1._c[1];

        //锟秸碉拷锟斤拷锟截硷拷
		if(IlGetRxMFS_LeftWheelButtonSts_Press() == 1)
	    {
		    IHU6OperFlag = TRUE;
		    IHU6TimerCount = 0;

			tmp = 0;

			MFS_1_294.flag = OFF;

		    IlPutTxIHU_System_ON_OFF_SwSts(1);
	    }

		//微锟脚硷拷
		if(IlGetRxMFS_WeChatButtonSts() == 1)
		{
            WeChatButttonStatus = 1;

			tmp = 216;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
			//DPRINTF(DBG_INFO, "IlGetRxMFS_WeChatButtonSts Key216\n");
		}
		else if(WeChatButttonStatus == 1)
		{
            if(IlGetRxMFS_WeChatButtonSts() == 0)
            {
                WeChatButttonStatus = 0;

				tmp = 216;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_WeChatButtonSts Key216\n");
            }
		}

		//锟皆讹拷锟斤拷锟�
		if(IlGetRxMFS_CustomButtonSts() == 1)     //锟皆讹拷锟藉按锟斤拷锟斤拷锟斤拷
	    {
            CustomPressStatus = 1;

			tmp = 316;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;

			//DPRINTF(DBG_INFO, "IlGetRxMFS_CustomButtonSts Key316  Press! \n");
		}
        else if(CustomPressStatus == 1)     //锟皆讹拷锟斤拷锟教э拷锟�
		{
            if(IlGetRxMFS_CustomButtonSts() == 0)
            {
                CustomPressStatus = 0;

			    tmp = 316;
			    MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
			    MFS_1_294.buf[2] = 0;
			    MFS_1_294.buf[3] = 0;

				//DPRINTF(DBG_INFO, "IlGetRxMFS_CustomButtonSts Key316  Release! \n");
            }
		}
    }

	//锟揭诧拷锟斤拷锟斤拷锟斤拷锟揭帮拷锟斤拷
	if(CanKeyWorkMode == 0x02)
	{
	    if(MFS_1._c[2] != 0)  //实锟街筹拷锟斤拷锟斤拷锟节猴拷锟接撅拷锟侥癸拷锟斤拷
	    {
		    //锟揭侧按锟斤拷锟斤拷锟斤拷锟斤拷锟揭猴拷锟接撅拷锟斤拷锟斤拷
		    MFS_1_294.flag = OFF;

			//DPRINTF(DBG_INFO, "Enter Right Rear Mirror Mode! \n");

			if(IlGetRxMFS_RightWheelButtonSts_ScrollUp() == 1)
            {
                IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(1);
            }
			else if(IlGetRxMFS_RightWheelButtonSts_ScrollDown() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(2);
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleLeft() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(3);
		    }
		    else if(IlGetRxMFS_RightWheelButtonSts_ToggleRight() == 1)
		    {
			    IHU6OperFlag = TRUE;
				IHU6TimerCount = 0;

				IlPutTxIHU_MirrorCtrlReq_R(4);
		    }
		}
	}

	//锟揭侧按锟斤拷状态锟斤拷锟斤拷锟斤拷默锟较硷拷使锟矫ｏ拷实锟斤拷锟斤拷锟斤拷锟接硷拷锟斤拷锟斤拷锟斤拷锟斤拷锟叫伙拷
	else
	{
        if(MFS_1._c[2] != buf2)
        {
            KeyOperStatus = 1;
			KeyCount = 0;

			buf2 = MFS_1._c[2];

			//锟揭诧拷锟斤拷锟较癸拷锟斤拷锟斤拷锟斤拷
			if(IlGetRxMFS_RightWheelButtonSts_ScrollUp() == 1)
		    {
                RightWheelScrollUpStatus = 1;

				tmp = 115;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollUp Key115\n");
		    }
			else if(RightWheelScrollUpStatus == 1)
			{
                if(IlGetRxMFS_RightWheelButtonSts_ScrollUp() == 0)
		        {
			        RightWheelScrollUpStatus = 0;

				    tmp = 115;
					MFS_1_294.flag = ON;
			        MFS_1_294.buf[0] = tmp>>8;
			        MFS_1_294.buf[1] = tmp;
				    MFS_1_294.buf[2] = 0;
			        MFS_1_294.buf[3] = 0;
			        //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollUp Key115\n");
		        }
			}

			//锟揭诧拷锟斤拷锟铰癸拷锟斤拷锟斤拷锟斤拷
		    if(IlGetRxMFS_RightWheelButtonSts_ScrollDown() == 1)
		    {
                RightWheelScrollDownStatus = 1;

				tmp = 114;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollDown Key114\n");
		    }
			else if(RightWheelScrollDownStatus == 1)
			{
                if(IlGetRxMFS_RightWheelButtonSts_ScrollDown() == 0)
		        {
                    RightWheelScrollDownStatus = 0;

					tmp = 114;
					MFS_1_294.flag = ON;
			        MFS_1_294.buf[0] = tmp>>8;
			        MFS_1_294.buf[1] = tmp;
				    MFS_1_294.buf[2] = 0;
			        MFS_1_294.buf[3] = 0;
			        //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ScrollDown Key114\n");
		        }
			}

			//锟揭诧拷锟襟按硷拷
		    if(IlGetRxMFS_RightWheelButtonSts_ToggleLeft() == 1)
		    {
                RightToggleLeftStatus = 1;

				tmp = 163;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "RightToggleLeftReleaseStatus = 0\n");
		    }
			else if(RightToggleLeftStatus == 1)
			{
				if(IlGetRxMFS_RightWheelButtonSts_ToggleLeft() == 0)
		        {
                    RightToggleLeftStatus = 0;
					RightToggleLeftReleaseStatus = 1;

					/*
                    tmp = 163;
					MFS_1_294.flag = ON;
			        MFS_1_294.buf[0] = tmp>>8;
			        MFS_1_294.buf[1] = tmp;
				    MFS_1_294.buf[2] = 0;
			        MFS_1_294.buf[3] = 0;
			        //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ToggleLeft Key163\n");
			        */
			    }
		    }

			//锟揭诧拷锟揭帮拷锟斤拷
		    if(IlGetRxMFS_RightWheelButtonSts_ToggleRight() == 1)
		    {
                RightToggleRightStatus = 1;

				tmp = 165;
				MFS_1_294.flag = ON;
			    MFS_1_294.buf[0] = tmp>>8;
			    MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 1;     //锟斤拷应应锟矫碉拷0,锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
			    MFS_1_294.buf[3] = 0;
			    //DPRINTF(DBG_INFO, "RightToggleRightReleaseStatus = 0\n");
			}
		    else if(RightToggleRightStatus == 1)
		    {
				if(IlGetRxMFS_RightWheelButtonSts_ToggleRight() == 0)
		        {
                    RightToggleRightStatus = 0;
					RightToggleRightReleaseStatus = 1;

					/*
					tmp = 165;
					MFS_1_294.flag = ON;
			        MFS_1_294.buf[0] = tmp>>8;
			        MFS_1_294.buf[1] = tmp;
				    MFS_1_294.buf[2] = 0;    //锟斤拷应应锟矫碉拷1  锟斤拷锟斤拷锟斤拷锟斤拷锟酵凤拷
			        MFS_1_294.buf[3] = 0;
			        //DPRINTF(DBG_INFO, "IlGetRxMFS_RightWheelButtonSts_ToggleRight Key165-222\n");
			        */
                }
		    }
		}

        //锟斤拷锟接帮拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
		if((KeyOperStatus == 1)&&(RightToggleLeftReleaseStatus == 1)&&(++KeyCount >= 20))
		{
            KeyOperStatus = 0;
			KeyCount = 0;
			RightToggleLeftReleaseStatus = 0;

            tmp = 163;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
		    MFS_1_294.buf[2] = 0;    //锟斤拷应应锟矫碉拷1  锟斤拷锟斤拷锟斤拷锟斤拷锟酵凤拷
			MFS_1_294.buf[3] = 0;
		}

		if((KeyOperStatus == 1)&&(RightToggleRightReleaseStatus == 1)&&(++KeyCount >= 25))
		{
            KeyOperStatus = 0;
			KeyCount = 0;
			RightToggleRightReleaseStatus = 0;

            tmp = 165;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
		    MFS_1_294.buf[2] = 0;    //锟斤拷应应锟矫碉拷1  锟斤拷锟斤拷锟斤拷锟斤拷锟酵凤拷
			MFS_1_294.buf[3] = 0;
		}
	}

	//CanData[3]
	if(MFS_1._c[3] != buf3)
	{
		buf3 = MFS_1._c[3];

		//锟斤拷锟斤拷锟斤拷锟斤拷
		if(IlGetRxMFS_VoiceRecognitionButtonSts() == 1) 	 //锟斤拷锟斤拷锟斤拷锟斤拷
		{
			tmp = 582;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
			//DPRINTF(DBG_INFO, "IlGetRxMFS_VoiceRecognitionButtonSts Key582\n");
		}
		else if(IlGetRxMFS_VoiceRecognitionButtonSts() == 0)
		{
			tmp = 582;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 0;
			MFS_1_294.buf[3] = 0;
		}

		//锟揭诧拷锟斤拷职锟斤拷锟�
		if(IlGetRxMFS_RightWheelButtonSts_Press() == 1)    //锟揭诧拷锟斤拷职锟斤拷锟斤拷锟斤拷锟�
		{
			RightWheelButtonPressStatus = 1;

			tmp = 28;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
		}
		else if(RightWheelButtonPressStatus == 1)	 //锟揭诧拷锟斤拷职锟斤拷锟斤拷头锟�
		{
			if(IlGetRxMFS_RightWheelButtonSts_Press() == 0)
			{
				RightWheelButtonPressStatus = 0;

				tmp = 28;
				MFS_1_294.flag = ON;
				MFS_1_294.buf[0] = tmp>>8;
				MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 0;
				MFS_1_294.buf[3] = 0;
			}
		}

		//锟斤拷锟截硷拷
		if(IlGetRxMFS_ReturnButtonSts() == 1)	 //锟斤拷锟截帮拷锟斤拷锟斤拷锟斤拷
		{
			BackButtonPressStatus = 1;

			tmp = 158;
			MFS_1_294.flag = ON;
			MFS_1_294.buf[0] = tmp>>8;
			MFS_1_294.buf[1] = tmp;
			MFS_1_294.buf[2] = 1;
			MFS_1_294.buf[3] = 0;
		}
		else if(BackButtonPressStatus == 1) 	//锟斤拷锟截帮拷锟斤拷锟酵凤拷
		{
			if(IlGetRxMFS_ReturnButtonSts() == 0)
			{
				BackButtonPressStatus = 0;

				tmp = 158;
				MFS_1_294.flag = ON;
				MFS_1_294.buf[0] = tmp>>8;
				MFS_1_294.buf[1] = tmp;
				MFS_1_294.buf[2] = 0;
				MFS_1_294.buf[3] = 0;
			}
		}
	}

	//锟斤拷锟斤拷屑锟�+锟揭侧返锟截硷拷锟斤拷锟斤拷10S锟斤拷锟揭ｏ拷锟斤拷值锟斤拷锟侥憋拷锟�2锟斤拷锟斤拷耍锟轿拷锟斤拷锟斤拷锟斤拷锟绞э拷锟斤拷砂锟斤拷锟斤拷锟斤拷路锟绞斤拷锟斤拷锟�
	    if((IlGetRxMFS_LeftWheelButtonSts_Press() == 2)&&(IlGetRxMFS_ReturnButtonSts() == 2))     //锟脚伙拷锟斤拷锟斤拷锟斤拷锟斤拷
	    {
	         //MFS_1_294.flag = OFF;

			 /*

			 M_MAIN_PWR_EN(OFF);

			 DelayMs(500);

			 M_MAIN_PWR_EN(ON);

			 */

			 if(++HardKeyResetCnt >= 50)
			 {
	             HardKeyResetCnt = 0;
	             DPRINTF(DBG_ERR, "Manual reset 1\n");
#ifdef INTERNAL_AMP
			     MCU_AMP_MUTE(ON); 					//SOUND OFF
#endif
			     EraseRecord();
			     save_last_memory();
				 RecordReset(1,NULL);
			     delay_ms(100);

			     SystemSoftwareReset();
			 }
	    }

}
#endif
void CAN_COMM_20MS_245(void)
{

    // CGW_BCM_1_245
    CGW_BCM_1_245.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_BCM_1_245.buf[3] != CGW_BCM_1._c[0]) || (CGW_BCM_1_245.buf[6] != CGW_BCM_1._c[3]) || (CGW_BCM_1_245.buf[8] != CGW_BCM_1._c[5]) ||
        (CGW_BCM_1_245.buf[9] != CGW_BCM_1._c[6]) || (CGW_BCM_1_245.buf[10] != CGW_BCM_1._c[7])|| (CGW_BCM_1_245.buf[5] != CGW_BCM_1._c[2]) ||
		(CGW_BCM_1_245.buf[7] != CGW_BCM_1._c[4]) || (CGW_BCM_1_245.buf[4] != CGW_BCM_1._c[1]))
    {
        CGW_BCM_1_245.flag = ON;

        CGW_BCM_1_245.buf[3]  = CGW_BCM_1._c[0];
        CGW_BCM_1_245.buf[4]  = CGW_BCM_1._c[1];
        CGW_BCM_1_245.buf[5]  = CGW_BCM_1._c[2];
        CGW_BCM_1_245.buf[6]  = CGW_BCM_1._c[3];
        CGW_BCM_1_245.buf[7]  = CGW_BCM_1._c[4];
        CGW_BCM_1_245.buf[8]  = CGW_BCM_1._c[5];
        CGW_BCM_1_245.buf[9]  = CGW_BCM_1._c[6];
        CGW_BCM_1_245.buf[10] = CGW_BCM_1._c[7];
    }
    else
    {
        CGW_BCM_1_245.flag = FALSE;
    }
}

void CAN_COMM_20MS_215(void)
{

    // CGW_EGS_1_215
    CGW_EGS_1_215.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if (CGW_EGS_1_215.buf[4] != CGW_EGS_1._c[1])
    {
        CGW_EGS_1_215.flag = ON;

        CGW_EGS_1_215.buf[3]  = 0x00;
        CGW_EGS_1_215.buf[4]  = CGW_EGS_1._c[1];
        CGW_EGS_1_215.buf[5]  = 0x00;
        CGW_EGS_1_215.buf[6]  = 0x00;
        CGW_EGS_1_215.buf[7]  = 0x00;
        CGW_EGS_1_215.buf[8]  = 0x00;
        CGW_EGS_1_215.buf[9]  = 0x00;
        CGW_EGS_1_215.buf[10] = 0x00;
    }
    else
    {
        CGW_EGS_1_215.flag = FALSE;
    }
}

void CAN_COMM_20MS_231(void)
{

    // CGW_EPB_1_231
    CGW_EPB_1_231.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0
    if ((CGW_EPB_1_231.buf[5] != CGW_BCS_EPB_1._c[2]) || (CGW_EPB_1_231.buf[6] != CGW_BCS_EPB_1._c[3]) || (CGW_EPB_1_231.buf[9] != CGW_BCS_EPB_1._c[6]) ||
        (CGW_EPB_1_231.buf[10] != CGW_BCS_EPB_1._c[7]))
    {
        CGW_EPB_1_231.flag = ON;

        CGW_EPB_1_231.buf[3]  = 0x00;
        CGW_EPB_1_231.buf[4]  = 0x00;
        CGW_EPB_1_231.buf[5]  = CGW_BCS_EPB_1._c[2];
        CGW_EPB_1_231.buf[6]  = CGW_BCS_EPB_1._c[3];
        CGW_EPB_1_231.buf[7]  = 0x00;
        CGW_EPB_1_231.buf[8]  = 0x00;
        CGW_EPB_1_231.buf[9]  = CGW_BCS_EPB_1._c[6];
        CGW_EPB_1_231.buf[10] = CGW_BCS_EPB_1._c[7];
    }
    else
    {
        CGW_EPB_1_231.flag = FALSE;
    }
}

void CAN_COMM_20MS_271(void)
{

    // CGW_ICM_1_271
    CGW_ICM_1_271.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0
    if ((CGW_ICM_1_271.buf[3] != ICM_1._c[0]) || (CGW_ICM_1_271.buf[4] != ICM_1._c[1]) || (CGW_ICM_1_271.buf[5] != ICM_1._c[2]))
    {
        CGW_ICM_1_271.flag = ON;

        CGW_ICM_1_271.buf[3]  = ICM_1._c[0];
        CGW_ICM_1_271.buf[4]  = ICM_1._c[1];
        CGW_ICM_1_271.buf[5]  = ICM_1._c[2];
        CGW_ICM_1_271.buf[6]  = 0x00;
        CGW_ICM_1_271.buf[7]  = 0x00;
        CGW_ICM_1_271.buf[8]  = 0x00;
        CGW_ICM_1_271.buf[9]  = 0x00;
        CGW_ICM_1_271.buf[10] = 0x00;
    }
    else
    {
        CGW_ICM_1_271.flag = FALSE;
    }
}

void CAN_COMM_20MS_225(void)
{

    // CGW_BCS_C_1_225
    CGW_BCS_C_1_225.buf[2] = 0x01; // need to change Node absent state
                                   // Active = 1
                                   // Absent = 0
    if ((CGW_BCS_C_1_225.buf[4] != CGW_BCS_C_1._c[1]) || (CGW_BCS_C_1_225.buf[7] != CGW_BCS_C_1._c[4]) || (CGW_BCS_C_1_225.buf[9] != CGW_BCS_C_1._c[6]))
    {
        CGW_BCS_C_1_225.flag = ON;

        CGW_BCS_C_1_225.buf[3]  = 0x00;
        CGW_BCS_C_1_225.buf[4]  = CGW_BCS_C_1._c[1];
        CGW_BCS_C_1_225.buf[5]  = 0x00;
        CGW_BCS_C_1_225.buf[6]  = 0x00;
        CGW_BCS_C_1_225.buf[7]  = CGW_BCS_C_1._c[4];
        CGW_BCS_C_1_225.buf[8]  = 0x00;
        CGW_BCS_C_1_225.buf[9]  = CGW_BCS_C_1._c[6];
        CGW_BCS_C_1_225.buf[10] = 0x00;
    }
    else
    {
        CGW_BCS_C_1_225.flag = FALSE;
    }
}

void CAN_COMM_20MS_25A(void)
{

    // CGW_BMS_3_25A
    CGW_BMS_3_25A.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0
    if ((CGW_BMS_3_25A.buf[4] != CGW_BMS_3._c[1]) || (CGW_BMS_3_25A.buf[5] != CGW_BMS_3._c[2]) || (CGW_BMS_3_25A.buf[6] != CGW_BMS_3._c[3]))
    {
        CGW_BMS_3_25A.flag = ON;

        CGW_BMS_3_25A.buf[3]  = 0x00;
        CGW_BMS_3_25A.buf[4]  = CGW_BMS_3._c[1];
        CGW_BMS_3_25A.buf[5]  = CGW_BMS_3._c[2];
        CGW_BMS_3_25A.buf[6]  = CGW_BMS_3._c[3];
        CGW_BMS_3_25A.buf[7]  = 0x00;
        CGW_BMS_3_25A.buf[8]  = 0x00;
        CGW_BMS_3_25A.buf[9]  = 0x00;
        CGW_BMS_3_25A.buf[10] = 0x00;
    }
    else
    {
        CGW_BMS_3_25A.flag = FALSE;
    }
}

void CAN_COMM_20MS_238(void)
{

    // CGW_EPS_1_238
    CGW_EPS_1_238.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0
    if (CGW_EPS_1_238.buf[3] != CGW_EPS_1._c[0])
    {
        CGW_EPS_1_238.flag = ON;

        CGW_EPS_1_238.buf[3]  = CGW_EPS_1._c[0];
        CGW_EPS_1_238.buf[4]  = 0x00;
        CGW_EPS_1_238.buf[5]  = 0x00;
        CGW_EPS_1_238.buf[6]  = 0x00;
        CGW_EPS_1_238.buf[7]  = 0x00;
        CGW_EPS_1_238.buf[8]  = 0x00;
        CGW_EPS_1_238.buf[9]  = 0x00;
        CGW_EPS_1_238.buf[10] = 0x00;
    }
    else
    {
        CGW_EPS_1_238.flag = FALSE;
    }
}

void CAN_COMM_50MS_291(void)
{

    // CGW_BCM_PEPS_291
    CGW_BCM_PEPS_1_291.buf[2] = 0x01; // need to change Node absent state
                                      // Active = 1
                                      // Absent = 0
    if (CGW_BCM_PEPS_1_291.buf[7] != CGW_BCM_PEPS_1._c[4])
    {
        CGW_BCM_PEPS_1_291.flag = ON;

        CGW_BCM_PEPS_1_291.buf[3]  = 0x00;
        CGW_BCM_PEPS_1_291.buf[4]  = 0x00;
        CGW_BCM_PEPS_1_291.buf[5]  = 0x00;
        CGW_BCM_PEPS_1_291.buf[6]  = 0x00;
        CGW_BCM_PEPS_1_291.buf[7]  = CGW_BCM_PEPS_1._c[4];
        CGW_BCM_PEPS_1_291.buf[8]  = 0x00;
        CGW_BCM_PEPS_1_291.buf[9]  = 0x00;
        CGW_BCM_PEPS_1_291.buf[10] = 0x00;
    }
    else
    {
        CGW_BCM_PEPS_1_291.flag = FALSE;
    }
}

void CAN_COMM_50MS_298(void)
{

    // CGW_EMS_4_298
    CGW_BSD_1_298.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if (CGW_BSD_1_298.buf[3] != CGW_BSD_1._c[0])
    {
        CGW_BSD_1_298.flag = ON;

        CGW_BSD_1_298.buf[3]  = CGW_BSD_1._c[0];
        CGW_BSD_1_298.buf[4]  = 0x00;
        CGW_BSD_1_298.buf[5]  = 0x00;
        CGW_BSD_1_298.buf[6]  = 0x00;
        CGW_BSD_1_298.buf[7]  = 0x00;
        CGW_BSD_1_298.buf[8]  = 0x00;
        CGW_BSD_1_298.buf[9]  = 0x00;
        CGW_BSD_1_298.buf[10] = 0x00;
    }
    else
    {
        CGW_BSD_1_298.flag = FALSE;
    }
}

void CAN_COMM_50MS_28A(void)
{

    // CGW_VCU_3_4_28A
    CGW_VCU_3_4_28A.buf[2] = 0x01; // need to change Node absent state
                                   // Active = 1
                                   // Absent = 0

    if ((CGW_VCU_3_4_28A.buf[5] != CGW_VCU_B_2._c[2]) || (CGW_VCU_3_4_28A.buf[7] != CGW_VCU_B_2._c[4]) || (CGW_VCU_3_4_28A.buf[8] != CGW_VCU_B_2._c[5]))
    {
        CGW_VCU_3_4_28A.flag = ON;

        CGW_VCU_3_4_28A.buf[3]  = 0x00;
        CGW_VCU_3_4_28A.buf[4]  = 0x00;
        CGW_VCU_3_4_28A.buf[5]  = CGW_VCU_B_2._c[2];
        CGW_VCU_3_4_28A.buf[6]  = 0x00;
        CGW_VCU_3_4_28A.buf[7]  = CGW_VCU_B_2._c[4];
        CGW_VCU_3_4_28A.buf[8]  = CGW_VCU_B_2._c[5];
        CGW_VCU_3_4_28A.buf[9]  = 0x00;
        CGW_VCU_3_4_28A.buf[10] = 0x00;
    }
    else
    {
        CGW_VCU_3_4_28A.flag = FALSE;
    }
}

void CAN_COMM_50MS_3FC(void)
{

    // CGW_FCM_10_3FC
    CGW_FCM_10_3FC.buf[2] = 0x01; // need to change Node absent state
                                  // Active = 1
                                  // Absent = 0

    if ((CGW_FCM_10_3FC.buf[3] != CGW_FCM_10._c[0]) || (CGW_FCM_10_3FC.buf[4] != CGW_FCM_10._c[1]) || (CGW_FCM_10_3FC.buf[5] != CGW_FCM_10._c[2]) ||
        (CGW_FCM_10_3FC.buf[6] != CGW_FCM_10._c[3]) || (CGW_FCM_10_3FC.buf[7] != CGW_FCM_10._c[4]) || (CGW_FCM_10_3FC.buf[8] != CGW_FCM_10._c[5]) ||
        (CGW_FCM_10_3FC.buf[9] != CGW_FCM_10._c[6]))
    {
        CGW_FCM_10_3FC.flag = ON;

        CGW_FCM_10_3FC.buf[3]  = CGW_FCM_10._c[0];
        CGW_FCM_10_3FC.buf[4]  = CGW_FCM_10._c[1];
        CGW_FCM_10_3FC.buf[5]  = CGW_FCM_10._c[2];
        CGW_FCM_10_3FC.buf[6]  = CGW_FCM_10._c[3];
        CGW_FCM_10_3FC.buf[7]  = CGW_FCM_10._c[4];
        CGW_FCM_10_3FC.buf[8]  = CGW_FCM_10._c[5];
        CGW_FCM_10_3FC.buf[9]  = CGW_FCM_10._c[6];
        CGW_FCM_10_3FC.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_10_3FC.flag = FALSE;
    }
}

void CAN_COMM_50MS_307(void)
{

    // CGW_FCM_2_307
    CGW_FCM_2_307.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_FCM_2_307.buf[3] != CGW_FCM_2._c[0]) || (CGW_FCM_2_307.buf[4] != CGW_FCM_2._c[1]) || (CGW_FCM_2_307.buf[5] != CGW_FCM_2._c[2]) ||
        (CGW_FCM_2_307.buf[6] != CGW_FCM_2._c[3]) || (CGW_FCM_2_307.buf[7] != CGW_FCM_2._c[4]) || (CGW_FCM_2_307.buf[8] != CGW_FCM_2._c[5]) ||
        (CGW_FCM_2_307.buf[9] != CGW_FCM_2._c[6]))
    {
        CGW_FCM_2_307.flag = ON;

        CGW_FCM_2_307.buf[3]  = CGW_FCM_2._c[0];
        CGW_FCM_2_307.buf[4]  = CGW_FCM_2._c[1];
        CGW_FCM_2_307.buf[5]  = CGW_FCM_2._c[2];
        CGW_FCM_2_307.buf[6]  = CGW_FCM_2._c[3];
        CGW_FCM_2_307.buf[7]  = CGW_FCM_2._c[4];
        CGW_FCM_2_307.buf[8]  = CGW_FCM_2._c[5];
        CGW_FCM_2_307.buf[9]  = CGW_FCM_2._c[6];
        CGW_FCM_2_307.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_2_307.flag = FALSE;
    }
}

void CAN_COMM_50MS_3DC(void)
{

    // CGW_FCM_6_3DC
    CGW_FCM_6_3DC.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_FCM_6_3DC.buf[3] != CGW_FCM_6._c[0]) || (CGW_FCM_6_3DC.buf[4] != CGW_FCM_6._c[1]) || (CGW_FCM_6_3DC.buf[5] != CGW_FCM_6._c[2]) ||
        (CGW_FCM_6_3DC.buf[6] != CGW_FCM_6._c[3]) || (CGW_FCM_6_3DC.buf[7] != CGW_FCM_6._c[4]) || (CGW_FCM_6_3DC.buf[8] != CGW_FCM_6._c[5]))
    {
        CGW_FCM_6_3DC.flag = ON;

        CGW_FCM_6_3DC.buf[3]  = CGW_FCM_6._c[0];
        CGW_FCM_6_3DC.buf[4]  = CGW_FCM_6._c[1];
        CGW_FCM_6_3DC.buf[5]  = CGW_FCM_6._c[2];
        CGW_FCM_6_3DC.buf[6]  = CGW_FCM_6._c[3];
        CGW_FCM_6_3DC.buf[7]  = CGW_FCM_6._c[4];
        CGW_FCM_6_3DC.buf[8]  = CGW_FCM_6._c[5];
        CGW_FCM_6_3DC.buf[9]  = 0x00;
        CGW_FCM_6_3DC.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_6_3DC.flag = FALSE;
    }
}

void CAN_COMM_50MS_3DE(void)
{

    // CGW_FCM_7_3DE
    CGW_FCM_7_3DE.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_FCM_7_3DE.buf[3] != CGW_FCM_7._c[0]) || (CGW_FCM_7_3DE.buf[4] != CGW_FCM_7._c[1]) || (CGW_FCM_7_3DE.buf[5] != CGW_FCM_7._c[2]) ||
        (CGW_FCM_7_3DE.buf[6] != CGW_FCM_7._c[3]) || (CGW_FCM_7_3DE.buf[7] != CGW_FCM_7._c[4]) || (CGW_FCM_7_3DE.buf[8] != CGW_FCM_7._c[5]))
    {
        CGW_FCM_7_3DE.flag = ON;

        CGW_FCM_7_3DE.buf[3]  = CGW_FCM_7._c[0];
        CGW_FCM_7_3DE.buf[4]  = CGW_FCM_7._c[1];
        CGW_FCM_7_3DE.buf[5]  = CGW_FCM_7._c[2];
        CGW_FCM_7_3DE.buf[6]  = CGW_FCM_7._c[3];
        CGW_FCM_7_3DE.buf[7]  = CGW_FCM_7._c[4];
        CGW_FCM_7_3DE.buf[8]  = CGW_FCM_7._c[5];
        CGW_FCM_7_3DE.buf[9]  = 0x00;
        CGW_FCM_7_3DE.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_7_3DE.flag = FALSE;
    }
}

void CAN_COMM_50MS_387(void)
{

    // CGW_FCM_FRM_6_387
    CGW_FCM_FRM_6_387.buf[2] = 0x01; // need to change Node absent state
                                     // Active = 1
                                     // Absent = 0

    if ((CGW_FCM_FRM_6_387.buf[3] != CGW_FCM_FRM_6._c[0]) || (CGW_FCM_FRM_6_387.buf[4] != CGW_FCM_FRM_6._c[1]) || (CGW_FCM_FRM_6_387.buf[5] != CGW_FCM_FRM_6._c[2]) ||
        (CGW_FCM_FRM_6_387.buf[6] != CGW_FCM_FRM_6._c[3]) || (CGW_FCM_FRM_6_387.buf[7] != CGW_FCM_FRM_6._c[4]) || (CGW_FCM_FRM_6_387.buf[8] != CGW_FCM_FRM_6._c[5]) ||
        (CGW_FCM_FRM_6_387.buf[9] != CGW_FCM_FRM_6._c[6]))
    {
        CGW_FCM_FRM_6_387.flag = ON;

        CGW_FCM_FRM_6_387.buf[3]  = CGW_FCM_FRM_6._c[0];
        CGW_FCM_FRM_6_387.buf[4]  = CGW_FCM_FRM_6._c[1];
        CGW_FCM_FRM_6_387.buf[5]  = CGW_FCM_FRM_6._c[2];
        CGW_FCM_FRM_6_387.buf[6]  = CGW_FCM_FRM_6._c[3];
        CGW_FCM_FRM_6_387.buf[7]  = CGW_FCM_FRM_6._c[4];
        CGW_FCM_FRM_6_387.buf[8]  = CGW_FCM_FRM_6._c[5];
        CGW_FCM_FRM_6_387.buf[9]  = CGW_FCM_FRM_6._c[6];
        CGW_FCM_FRM_6_387.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_FRM_6_387.flag = FALSE;
    }
}

void CAN_COMM_50MS_3ED(void)
{

    // CGW_FCM_FRM_8_3ED
    CGW_FCM_FRM_8_3ED.buf[2] = 0x01; // need to change Node absent state
                                     // Active = 1
                                     // Absent = 0

    if ((CGW_FCM_FRM_8_3ED.buf[3] != CGW_FCM_FRM_8._c[0]) || (CGW_FCM_FRM_8_3ED.buf[4] != CGW_FCM_FRM_8._c[1]) || (CGW_FCM_FRM_8_3ED.buf[5] != CGW_FCM_FRM_8._c[2]) ||
        (CGW_FCM_FRM_8_3ED.buf[6] != CGW_FCM_FRM_8._c[3]) || (CGW_FCM_FRM_8_3ED.buf[7] != CGW_FCM_FRM_8._c[4]) || (CGW_FCM_FRM_8_3ED.buf[8] != CGW_FCM_FRM_8._c[5]) ||
        (CGW_FCM_FRM_8_3ED.buf[9] != CGW_FCM_FRM_8._c[6]))
    {
        CGW_FCM_FRM_8_3ED.flag = ON;

        CGW_FCM_FRM_8_3ED.buf[3]  = CGW_FCM_FRM_8._c[0];
        CGW_FCM_FRM_8_3ED.buf[4]  = CGW_FCM_FRM_8._c[1];
        CGW_FCM_FRM_8_3ED.buf[5]  = CGW_FCM_FRM_8._c[2];
        CGW_FCM_FRM_8_3ED.buf[6]  = CGW_FCM_FRM_8._c[3];
        CGW_FCM_FRM_8_3ED.buf[7]  = CGW_FCM_FRM_8._c[4];
        CGW_FCM_FRM_8_3ED.buf[8]  = CGW_FCM_FRM_8._c[5];
        CGW_FCM_FRM_8_3ED.buf[9]  = CGW_FCM_FRM_8._c[6];
        CGW_FCM_FRM_8_3ED.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_FRM_8_3ED.flag = FALSE;
    }
}

void CAN_COMM_50MS_3FA(void)
{

    // CGW_FCM_FRM_9_3FA
    CGW_FCM_FRM_9_3FA.buf[2] = 0x01; // need to change Node absent state
                                     // Active = 1
                                     // Absent = 0

    if ((CGW_FCM_FRM_9_3FA.buf[3] != CGW_FCM_FRM_9._c[0]) || (CGW_FCM_FRM_9_3FA.buf[4] != CGW_FCM_FRM_9._c[1]) || (CGW_FCM_FRM_9_3FA.buf[5] != CGW_FCM_FRM_9._c[2]) ||
        (CGW_FCM_FRM_9_3FA.buf[6] != CGW_FCM_FRM_9._c[3]) || (CGW_FCM_FRM_9_3FA.buf[7] != CGW_FCM_FRM_9._c[4]) || (CGW_FCM_FRM_9_3FA.buf[8] != CGW_FCM_FRM_9._c[5]) ||
        (CGW_FCM_FRM_9_3FA.buf[9] != CGW_FCM_FRM_9._c[6]))
    {
        CGW_FCM_FRM_9_3FA.flag = ON;

        CGW_FCM_FRM_9_3FA.buf[3]  = CGW_FCM_FRM_9._c[0];
        CGW_FCM_FRM_9_3FA.buf[4]  = CGW_FCM_FRM_9._c[1];
        CGW_FCM_FRM_9_3FA.buf[5]  = CGW_FCM_FRM_9._c[2];
        CGW_FCM_FRM_9_3FA.buf[6]  = CGW_FCM_FRM_9._c[3];
        CGW_FCM_FRM_9_3FA.buf[7]  = CGW_FCM_FRM_9._c[4];
        CGW_FCM_FRM_9_3FA.buf[8]  = CGW_FCM_FRM_9._c[5];
        CGW_FCM_FRM_9_3FA.buf[9]  = CGW_FCM_FRM_9._c[6];
        CGW_FCM_FRM_9_3FA.buf[10] = 0x00;
    }
    else
    {
        CGW_FCM_FRM_9_3FA.flag = FALSE;
    }
}

void CAN_COMM_100MS_3D2(void)
{

    // ACCM_1_3D2
    ACCM_1_3D2.buf[2] = 0x01; // need to change Node absent state
                              // Active = 1
                              // Absent = 0

    if ((ACCM_1_3D2.buf[3] != CGW_BCM_ACCM_1._c[0]) || (ACCM_1_3D2.buf[4] != CGW_BCM_ACCM_1._c[1]) || (ACCM_1_3D2.buf[5] != CGW_BCM_ACCM_1._c[2]) ||
        (ACCM_1_3D2.buf[6] != CGW_BCM_ACCM_1._c[3]) || (ACCM_1_3D2.buf[7] != CGW_BCM_ACCM_1._c[4]) || (ACCM_1_3D2.buf[8] != CGW_BCM_ACCM_1._c[5]) ||
        (ACCM_1_3D2.buf[9] != CGW_BCM_ACCM_1._c[6]) || (ACCM_1_3D2.buf[10] != CGW_BCM_ACCM_1._c[7]))
    {
        ACCM_1_3D2.flag    = ON;
        ACCM_1_3D2.buf[3]  = CGW_BCM_ACCM_1._c[0];
        ACCM_1_3D2.buf[4]  = CGW_BCM_ACCM_1._c[1];
        ACCM_1_3D2.buf[5]  = CGW_BCM_ACCM_1._c[2];
        ACCM_1_3D2.buf[6]  = CGW_BCM_ACCM_1._c[3];
        ACCM_1_3D2.buf[7]  = CGW_BCM_ACCM_1._c[4];
        ACCM_1_3D2.buf[8]  = CGW_BCM_ACCM_1._c[5];
        ACCM_1_3D2.buf[9]  = CGW_BCM_ACCM_1._c[6];
        ACCM_1_3D2.buf[10] = CGW_BCM_ACCM_1._c[7];

        // DPRINTF(DBG_INFO, "IlGetRxACCM_WorkState \n");
    }
    else
    {
        ACCM_1_3D2.flag = FALSE;
    }
}
void CAN_COMM_100MS_3B1(void)
{

    // ICM_2_3B1
    ICM_2_3B1.buf[2] = 0x01; // need to change Node absent state
                              // Active = 1
                              // Absent = 0

    if ((ICM_2_3B1.buf[4] != ICM_2._c[1]) || (ICM_2_3B1.buf[7] != ICM_2._c[4]))
    {
        ICM_2_3B1.flag    = ON;
        ICM_2_3B1.buf[3]  = 0x00;
        ICM_2_3B1.buf[4]  = ICM_2._c[1];
        ICM_2_3B1.buf[5]  = 0x00;
        ICM_2_3B1.buf[6]  = 0x00;
        ICM_2_3B1.buf[7]  = ICM_2._c[4];
        ICM_2_3B1.buf[8]  = 0x00;
        ICM_2_3B1.buf[9]  = 0x00;
        ICM_2_3B1.buf[10] = 0x00;

        // DPRINTF(DBG_INFO, "IlGetRxICM_2_3B1 \n");
    }
    else
    {
        ICM_2_3B1.flag = FALSE;
    }
}

void CAN_COMM_100MS_324(void)
{
    // CGW_BCM_4_324
    CGW_BCM_4_324.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_BCM_4_324.buf[3] != CGW_BCM_4._c[0]) || (CGW_BCM_4_324.buf[4] != CGW_BCM_4._c[1]) ||
        (CGW_BCM_4_324.buf[7] != CGW_BCM_4._c[4]) || (CGW_BCM_4_324.buf[10] != CGW_BCM_4._c[7]))
    {
        CGW_BCM_4_324.flag    = ON;
        CGW_BCM_4_324.buf[3]  = CGW_BCM_4._c[0];
        CGW_BCM_4_324.buf[4]  = CGW_BCM_4._c[1];
        CGW_BCM_4_324.buf[5]  = 0x00;
        CGW_BCM_4_324.buf[6]  = 0x00;
        CGW_BCM_4_324.buf[7]  = CGW_BCM_4._c[4];
        CGW_BCM_4_324.buf[8]  = 0x00;
        CGW_BCM_4_324.buf[9]  = 0x00;
        CGW_BCM_4_324.buf[10] = CGW_BCM_4._c[7];

        // DPRINTF(DBG_INFO, "IlGetRxBCM_5_WorkState \n");
    }
    else
    {
        CGW_BCM_4_324.flag = FALSE;
    }
}

void CAN_COMM_100MS_323(void)
{

    // CGW_BCM_5_323
    CGW_BCM_5_323.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_BCM_5_323.buf[3] != CGW_BCM_5._c[0]) || (CGW_BCM_5_323.buf[4] != CGW_BCM_5._c[1]) || (CGW_BCM_5_323.buf[5] != CGW_BCM_5._c[2]) ||
        (CGW_BCM_5_323.buf[6] != CGW_BCM_5._c[3]) || (CGW_BCM_5_323.buf[7] != CGW_BCM_5._c[4]) || (CGW_BCM_5_323.buf[8] != CGW_BCM_5._c[5]) ||
        (CGW_BCM_5_323.buf[9] != CGW_BCM_5._c[6]) || (CGW_BCM_5_323.buf[10] != CGW_BCM_5._c[7]))
    {
        CGW_BCM_5_323.flag = ON;

        CGW_BCM_5_323.buf[3]  = CGW_BCM_5._c[0];
        CGW_BCM_5_323.buf[4]  = CGW_BCM_5._c[1];
        CGW_BCM_5_323.buf[5]  = CGW_BCM_5._c[2];
        CGW_BCM_5_323.buf[6]  = CGW_BCM_5._c[3];
        CGW_BCM_5_323.buf[7]  = CGW_BCM_5._c[4];
        CGW_BCM_5_323.buf[8]  = CGW_BCM_5._c[5];
        CGW_BCM_5_323.buf[9]  = CGW_BCM_5._c[6];
        CGW_BCM_5_323.buf[10] = CGW_BCM_5._c[7];

        // DPRINTF(DBG_INFO, "IlGetRxBCM_5_WorkState \n");
    }
    else
    {
        CGW_BCM_5_323.flag = FALSE;
    }
}

void CAN_COMM_100MS_457(void)
{

    // BCM_6_457
    BCM_6_457.buf[2] = 0x01; // need to change Node absent state
                             // Active = 1
                             // Absent = 0

    if ((BCM_6_457.buf[3] != BCM_6._c[0]) || (BCM_6_457.buf[4] != BCM_6._c[1]) || (BCM_6_457.buf[5] != BCM_6._c[2]) ||
        (BCM_6_457.buf[6] != BCM_6._c[3]) || (BCM_6_457.buf[7] != BCM_6._c[4]) || (BCM_6_457.buf[8] != BCM_6._c[5]) ||
        (BCM_6_457.buf[9] != BCM_6._c[6]) || (BCM_6_457.buf[10] != BCM_6._c[7]))
    {
        BCM_6_457.flag = ON;

        BCM_6_457.buf[3]  = BCM_6._c[0];
        BCM_6_457.buf[4]  = BCM_6._c[1];
        BCM_6_457.buf[5]  = BCM_6._c[2];
        BCM_6_457.buf[6]  = BCM_6._c[3];
        BCM_6_457.buf[7]  = BCM_6._c[4];
        BCM_6_457.buf[8]  = BCM_6._c[5];
        BCM_6_457.buf[9]  = BCM_6._c[6];
        BCM_6_457.buf[10] = BCM_6._c[7];
    }
    else
    {
        BCM_6_457.flag = FALSE;
    }
}

void CAN_COMM_100MS_294(void)
{

    // CGW_BCM_MFS_1_294
    CGW_BCM_MFS_1_294.buf[2] = 0x01; // need to change Node absent state
                                      // Active = 1
                                      // Absent = 0

    if ((CGW_BCM_MFS_1_294.buf[3] != CGW_BCM_MFS_1._c[0]) || (CGW_BCM_MFS_1_294.buf[4] != CGW_BCM_MFS_1._c[1]) || (CGW_BCM_MFS_1_294.buf[5] != CGW_BCM_MFS_1._c[2]) ||
        (CGW_BCM_MFS_1_294.buf[6] != CGW_BCM_MFS_1._c[3]) || (CGW_BCM_MFS_1_294.buf[7] != CGW_BCM_MFS_1._c[4]) || (CGW_BCM_MFS_1_294.buf[8] != CGW_BCM_MFS_1._c[5]) ||
        (CGW_BCM_MFS_1_294.buf[9] != CGW_BCM_MFS_1._c[6]) || (CGW_BCM_MFS_1_294.buf[10] != CGW_BCM_MFS_1._c[7]))
    {
        CGW_BCM_MFS_1_294.flag = ON;

        CGW_BCM_MFS_1_294.buf[3]  = CGW_BCM_MFS_1._c[0];
        CGW_BCM_MFS_1_294.buf[4]  = CGW_BCM_MFS_1._c[1];
        CGW_BCM_MFS_1_294.buf[5]  = CGW_BCM_MFS_1._c[2];
        CGW_BCM_MFS_1_294.buf[6]  = CGW_BCM_MFS_1._c[3];
        CGW_BCM_MFS_1_294.buf[7]  = CGW_BCM_MFS_1._c[4];
        CGW_BCM_MFS_1_294.buf[8]  = CGW_BCM_MFS_1._c[5];
        CGW_BCM_MFS_1_294.buf[9]  = CGW_BCM_MFS_1._c[6];
        CGW_BCM_MFS_1_294.buf[10] = CGW_BCM_MFS_1._c[7];
    }
    else
    {
        CGW_BCM_MFS_1_294.flag = FALSE;
    }
}

void CAN_COMM_100MS_45C(void)
{

    // CGW_BCM_SCM_3_45C
    CGW_BCM_SCM_3_45C.buf[2] = 0x01; // need to change Node absent state
                                     // Active = 1
                                     // Absent = 0

    if ((CGW_BCM_SCM_3_45C.buf[5] != CGW_BCM_SCM_3._c[2]) || (CGW_BCM_SCM_3_45C.buf[6] != CGW_BCM_SCM_3._c[3]))
    {
        CGW_BCM_SCM_3_45C.flag = ON;

        CGW_BCM_SCM_3_45C.buf[3]  = 0x00;
        CGW_BCM_SCM_3_45C.buf[4]  = 0x00;
        CGW_BCM_SCM_3_45C.buf[5]  = CGW_BCM_SCM_3._c[2];
        CGW_BCM_SCM_3_45C.buf[6]  = CGW_BCM_SCM_3._c[3];
        CGW_BCM_SCM_3_45C.buf[7]  = 0x00;
        CGW_BCM_SCM_3_45C.buf[8]  = 0x00;
        CGW_BCM_SCM_3_45C.buf[9]  = 0x00;
        CGW_BCM_SCM_3_45C.buf[10] = 0x00;
    }
    else
    {
        CGW_BCM_SCM_3_45C.flag = FALSE;
    }
}

// void CAN_COMM_100MS_46B(void)
// {

//     // CGW_BMS_3_46B
//     CGW_BMS_3_46B.buf[2] = 0x01; // need to change Node absent state
//                                  // Active = 1
//                                  // Absent = 0

//     if ((CGW_BMS_3_46B.buf[3] != CGW_BMS_3._c[0]) || (CGW_BMS_3_46B.buf[6] != CGW_BMS_3._c[3]) || (CGW_BMS_3_46B.buf[8] != CGW_BMS_3._c[5]) ||
//         (CGW_BMS_3_46B.buf[9] != CGW_BMS_3._c[6]) || (CGW_BMS_3_46B.buf[10] != CGW_BMS_3._c[7]))
//     {
//         CGW_BMS_3_46B.flag = ON;

//         CGW_BMS_3_46B.buf[3]  = CGW_BMS_3._c[0];
//         CGW_BMS_3_46B.buf[4]  = 0x00;
//         CGW_BMS_3_46B.buf[5]  = 0x00;
//         CGW_BMS_3_46B.buf[6]  = CGW_BMS_3._c[3];
//         CGW_BMS_3_46B.buf[7]  = 0x00;
//         CGW_BMS_3_46B.buf[8]  = CGW_BMS_3._c[5];
//         CGW_BMS_3_46B.buf[9]  = CGW_BMS_3._c[6];
//         CGW_BMS_3_46B.buf[10] = CGW_BMS_3._c[7];
//     }
//     else
//     {
//         CGW_BMS_3_46B.flag = FALSE;
//     }
// }

void CAN_COMM_100MS_45A(void)
{

    // CGW_BCM_SCM_1_45A
    CGW_BCM_SCM_1_45A.buf[2] = 0x01; // need to change Node absent state
                                     // Active = 1
                                     // Absent = 0

    if ((CGW_BCM_SCM_1_45A.buf[3] != CGW_BCM_SCM_1._c[0]) || (CGW_BCM_SCM_1_45A.buf[6] != CGW_BCM_SCM_1._c[3]) || (CGW_BCM_SCM_1_45A.buf[7] != CGW_BCM_SCM_1._c[4]) ||
        (CGW_BCM_SCM_1_45A.buf[8] != CGW_BCM_SCM_1._c[5]) || (CGW_BCM_SCM_1_45A.buf[9] != CGW_BCM_SCM_1._c[6]) || (CGW_BCM_SCM_1_45A.buf[10] != CGW_BCM_SCM_1._c[7]))
    {
        CGW_BCM_SCM_1_45A.flag = ON;

        CGW_BCM_SCM_1_45A.buf[3]  = CGW_BCM_SCM_1._c[0];
        CGW_BCM_SCM_1_45A.buf[4]  = 0x00;
        CGW_BCM_SCM_1_45A.buf[5]  = 0x00;
        CGW_BCM_SCM_1_45A.buf[6]  = CGW_BCM_SCM_1._c[3];
        CGW_BCM_SCM_1_45A.buf[7]  = CGW_BCM_SCM_1._c[4];
        CGW_BCM_SCM_1_45A.buf[8]  = CGW_BCM_SCM_1._c[5];
        CGW_BCM_SCM_1_45A.buf[9]  = CGW_BCM_SCM_1._c[6];
        CGW_BCM_SCM_1_45A.buf[10] = CGW_BCM_SCM_1._c[7];
    }
    else
    {
        CGW_BCM_SCM_1_45A.flag = FALSE;
    }
}

void CAN_COMM_100MS_328(void)
{
    // uint8 u8RCM_AudibleBeepRate = 0u;

    // CGW_RCM_1_328
    CGW_RCM_1_328.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_RCM_1_328.buf[3] != RCM_1._c[0]) || (CGW_RCM_1_328.buf[4] != RCM_1._c[1]) || (CGW_RCM_1_328.buf[5] != RCM_1._c[2]) ||
        (CGW_RCM_1_328.buf[6] != RCM_1._c[3]) || (CGW_RCM_1_328.buf[7] != RCM_1._c[4]) || (CGW_RCM_1_328.buf[8] != RCM_1._c[5]) ||
        (CGW_RCM_1_328.buf[9] != RCM_1._c[6]) || (CGW_RCM_1_328.buf[10] != RCM_1._c[7]))
    {
        CGW_RCM_1_328.flag = ON;

        CGW_RCM_1_328.buf[3]  = RCM_1._c[0];
        CGW_RCM_1_328.buf[4]  = RCM_1._c[1];
        CGW_RCM_1_328.buf[5]  = RCM_1._c[2];
        CGW_RCM_1_328.buf[6]  = RCM_1._c[3];
        CGW_RCM_1_328.buf[7]  = RCM_1._c[4];
        CGW_RCM_1_328.buf[8]  = RCM_1._c[5];
        CGW_RCM_1_328.buf[9]  = RCM_1._c[6];
        CGW_RCM_1_328.buf[10] = RCM_1._c[7];

        // u8RCM_AudibleBeepRate = (RCM_1._c[4] >> 5u) & 0x7u;

        // if ((u8RCM_AudibleBeepRate > 0) && (u8RCM_AudibleBeepRate < 5))
        // {
        //     r_Device.r_Beep->beep_active = ON;
        // }
        // else
        // {
        //     r_Device.r_Beep->beep_active = OFF;
        // }
    }
    else
    {
        CGW_RCM_1_328.flag = FALSE;
    }
}

void CAN_COMM_100MS_3D8(void)
{
    // uint8 u8RCM_AudibleBeepRate = 0u;

    // CGW_BCM_EAC_1_3D8
    // CGW_BCM_EAC_1_3D8.buf[2] = 0x01; // need to change Node absent state
    //                                  // Active = 1
    //                                  // Absent = 0

    // if (CGW_BCM_EAC_1_3D8.buf[8] != CGW_BCM_EAC_1._c[5])
    // {
    //     CGW_BCM_EAC_1_3D8.flag = ON;

    //     CGW_BCM_EAC_1_3D8.buf[3]  = 0x00;
    //     CGW_BCM_EAC_1_3D8.buf[4]  = 0x00;
    //     CGW_BCM_EAC_1_3D8.buf[5]  = 0x00;
    //     CGW_BCM_EAC_1_3D8.buf[6]  = 0x00;
    //     CGW_BCM_EAC_1_3D8.buf[7]  = 0x00;
    //     CGW_BCM_EAC_1_3D8.buf[8]  = CGW_BCM_EAC_1._c[5];
    //     CGW_BCM_EAC_1_3D8.buf[9]  = 0x00;
    //     CGW_BCM_EAC_1_3D8.buf[10] = 0x00;
    // }
    // else
    // {
    //     CGW_BCM_EAC_1_3D8.flag = FALSE;
    // }
}

void CAN_COMM_100MS_504(void)
{

    // CGW_SAS_2_504
    CGW_SAS_2_504.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_SAS_2_504.buf[3] != CGW_SAS_2._c[0]) || (CGW_SAS_2_504.buf[4] != CGW_SAS_2._c[1]) || (CGW_SAS_2_504.buf[5] != CGW_SAS_2._c[2]) ||
        (CGW_SAS_2_504.buf[6] != CGW_SAS_2._c[3]))
    {
        CGW_SAS_2_504.flag = ON;

        CGW_SAS_2_504.buf[3]  = CGW_SAS_2._c[0];
        CGW_SAS_2_504.buf[4]  = CGW_SAS_2._c[1];
        CGW_SAS_2_504.buf[5]  = CGW_SAS_2._c[2];
        CGW_SAS_2_504.buf[6]  = CGW_SAS_2._c[3];
        CGW_SAS_2_504.buf[7]  = 0x00;
        CGW_SAS_2_504.buf[8]  = 0x00;
        CGW_SAS_2_504.buf[9]  = 0x00;
        CGW_SAS_2_504.buf[10] = 0x00;
    }
    else
    {
        CGW_SAS_2_504.flag = FALSE;
    }
}

void CAN_COMM_100MS_313(void)
{

    // CGW_VCU_3_6_313
    CGW_VCU_3_6_313.buf[2] = 0x01; // need to change Node absent state
                                   // Active = 1
                                   // Absent = 0

    if ((CGW_VCU_3_6_313.buf[4] != CGW_VCU_B_3._c[1]) || (CGW_VCU_3_6_313.buf[5] != CGW_VCU_B_3._c[2]) ||
        (CGW_VCU_3_6_313.buf[6] != CGW_VCU_B_3._c[3]) || (CGW_VCU_3_6_313.buf[7] != CGW_VCU_B_3._c[4]) || (CGW_VCU_3_6_313.buf[8] != CGW_VCU_B_3._c[5]) ||
        (CGW_VCU_3_6_313.buf[9] != CGW_VCU_B_3._c[6]) || (CGW_VCU_3_6_313.buf[10] != CGW_VCU_B_3._c[7]))
    {
        CGW_VCU_3_6_313.flag    = ON;
        CGW_VCU_3_6_313.buf[3]  = 0x00;
        CGW_VCU_3_6_313.buf[4]  = CGW_VCU_B_3._c[1];
        CGW_VCU_3_6_313.buf[5]  = CGW_VCU_B_3._c[2];
        CGW_VCU_3_6_313.buf[6]  = CGW_VCU_B_3._c[3];
        CGW_VCU_3_6_313.buf[7]  = CGW_VCU_B_3._c[4];
        CGW_VCU_3_6_313.buf[8]  = CGW_VCU_B_3._c[5];
        CGW_VCU_3_6_313.buf[9]  = CGW_VCU_B_3._c[6];
        CGW_VCU_3_6_313.buf[10] = CGW_VCU_B_3._c[7];

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_6__WorkState \n");
    }
    else
    {
        CGW_VCU_3_6_313.flag = FALSE;
    }
}

void CAN_COMM_100MS_314(void)
{
    // CGW_VCU_3_7:0x314
    CGW_VCU_3_7_314.buf[2] = 0x01; // need to change Node absent state
                                   // Active = 1
                                   // Absent = 0

    if ((CGW_VCU_3_7_314.buf[3] != CGW_VCU_B_4._c[0]) || (CGW_VCU_3_7_314.buf[4] != CGW_VCU_B_4._c[1]) || (CGW_VCU_3_7_314.buf[5] != CGW_VCU_B_4._c[2]) ||
        (CGW_VCU_3_7_314.buf[7] != CGW_VCU_B_4._c[4]) || (CGW_VCU_3_7_314.buf[8] != CGW_VCU_B_4._c[5]) || (CGW_VCU_3_7_314.buf[9] != CGW_VCU_B_4._c[6]))
    {
        CGW_VCU_3_7_314.flag    = ON;
        CGW_VCU_3_7_314.buf[3]  = CGW_VCU_B_4._c[0];
        CGW_VCU_3_7_314.buf[4]  = CGW_VCU_B_4._c[1];
        CGW_VCU_3_7_314.buf[5]  = CGW_VCU_B_4._c[2];
        CGW_VCU_3_7_314.buf[6]  = 0x00;
        CGW_VCU_3_7_314.buf[7]  = CGW_VCU_B_4._c[4];
        CGW_VCU_3_7_314.buf[8]  = CGW_VCU_B_4._c[5];
        CGW_VCU_3_7_314.buf[9]  = CGW_VCU_B_4._c[6];
        CGW_VCU_3_7_314.buf[10] = 0x00;

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_7__WorkState \n");
    }
    else
    {
        CGW_VCU_3_7_314.flag = FALSE;
    }
}

void CAN_COMM_100MS_315(void)
{

    // CGW_VCU_B_5_315
    CGW_VCU_B_5_315.buf[2] = 0x01; // need to change Node absent state
                                   // Active = 1
                                   // Absent = 0

    if ((CGW_VCU_B_5_315.buf[3] !=CGW_VCU_B_5._c[0]) || (CGW_VCU_B_5_315.buf[5] !=CGW_VCU_B_5._c[2]) || (CGW_VCU_B_5_315.buf[6] != CGW_VCU_B_5._c[3]) ||
        (CGW_VCU_B_5_315.buf[9] !=CGW_VCU_B_5._c[6]) || (CGW_VCU_B_5_315.buf[4] != CGW_VCU_B_5._c[1]))
    {
        CGW_VCU_B_5_315.flag    = ON;
        CGW_VCU_B_5_315.buf[3]  = CGW_VCU_B_5._c[0];
        CGW_VCU_B_5_315.buf[4]  = CGW_VCU_B_5._c[1];
        CGW_VCU_B_5_315.buf[5]  = CGW_VCU_B_5._c[2];
        CGW_VCU_B_5_315.buf[6]  = CGW_VCU_B_5._c[3];
        CGW_VCU_B_5_315.buf[7]  = 0x00;
        CGW_VCU_B_5_315.buf[8]  = 0x00;
        CGW_VCU_B_5_315.buf[9]  = CGW_VCU_B_5._c[6];
        CGW_VCU_B_5_315.buf[10] = 0x00;

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_8__WorkState \n");
    }
    else
    {
        CGW_VCU_B_5_315.flag = FALSE;
    }
}

void CAN_COMM_100MS_355(void)
{

    // CGW_BMS_9_355
    CGW_BMS_9_355.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_BMS_9_355.buf[3] != CGW_FCM_5._c[0]) || (CGW_BMS_9_355.buf[4] != CGW_FCM_5._c[1]) || (CGW_BMS_9_355.buf[5] != CGW_FCM_5._c[2]))
    {
        CGW_BMS_9_355.flag    = ON;
        CGW_BMS_9_355.buf[3]  = CGW_FCM_5._c[0];
        CGW_BMS_9_355.buf[4]  = CGW_FCM_5._c[1];
        CGW_BMS_9_355.buf[5]  = CGW_FCM_5._c[2];
        CGW_BMS_9_355.buf[6]  = 0x00;
        CGW_BMS_9_355.buf[7]  = 0x00;
        CGW_BMS_9_355.buf[8]  = 0x00;
        CGW_BMS_9_355.buf[9]  = 0x00;
        CGW_BMS_9_355.buf[10] = 0x00;

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_8__WorkState \n");
    }
    else
    {
        CGW_BMS_9_355.flag = FALSE;
    }
}

void CAN_COMM_100MS_4DD(void)
{

    // CGW_FCM_5_4DD
    CGW_FCM_5_4DD.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_FCM_5_4DD.buf[4] != CGW_FCM_5._c[1]) || (CGW_FCM_5_4DD.buf[5] != CGW_FCM_5._c[2])||(CGW_FCM_5_4DD.buf[3] != CGW_FCM_5._c[0])||
	(CGW_FCM_5_4DD.buf[9] != CGW_FCM_5._c[6]) || (CGW_FCM_5_4DD.buf[10] != CGW_FCM_5._c[7]))
    {
        CGW_FCM_5_4DD.flag    = ON;
        CGW_FCM_5_4DD.buf[3]  = CGW_FCM_5._c[0];
        CGW_FCM_5_4DD.buf[4]  = CGW_FCM_5._c[1];
        CGW_FCM_5_4DD.buf[5]  = CGW_FCM_5._c[2];
        CGW_FCM_5_4DD.buf[6]  = 0x00;
        CGW_FCM_5_4DD.buf[7]  = 0x00;
        CGW_FCM_5_4DD.buf[8]  = 0x00;
        CGW_FCM_5_4DD.buf[9]  = CGW_FCM_5._c[6];
        CGW_FCM_5_4DD.buf[10] = CGW_FCM_5._c[7];

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_8__WorkState \n");
    }
    else
    {
        CGW_FCM_5_4DD.flag = FALSE;
    }
}

void CAN_COMM_100MS_4E3(void)
{

    // CGW_FCM_FRM_5_4E3
    CGW_FCM_FRM_5_4E3.buf[2] = 0x01; // need to change Node absent state
                                     // Active = 1
                                     // Absent = 0

    if ((CGW_FCM_FRM_5_4E3.buf[3] != CGW_FCM_FRM_5._c[0]) || (CGW_FCM_FRM_5_4E3.buf[4] != CGW_FCM_FRM_5._c[1]) || (CGW_FCM_FRM_5_4E3.buf[9] != CGW_FCM_FRM_5._c[6]))
    {
        CGW_FCM_FRM_5_4E3.flag    = ON;
        CGW_FCM_FRM_5_4E3.buf[3]  = CGW_FCM_FRM_5._c[0];
        CGW_FCM_FRM_5_4E3.buf[4]  = CGW_FCM_FRM_5._c[1];
        CGW_FCM_FRM_5_4E3.buf[5]  = 0x00;
        CGW_FCM_FRM_5_4E3.buf[6]  = 0x00;
        CGW_FCM_FRM_5_4E3.buf[7]  = CGW_FCM_FRM_5._c[4];
        CGW_FCM_FRM_5_4E3.buf[8]  = 0x00;
        CGW_FCM_FRM_5_4E3.buf[9]  = CGW_FCM_FRM_5._c[6];
        CGW_FCM_FRM_5_4E3.buf[10] = 0x00;

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_8__WorkState \n");
    }
    else
    {
        CGW_FCM_FRM_5_4E3.flag = FALSE;
    }
}

void CAN_COMM_100MS_32A(void)
{

    // CGW_PLG_1_32A
    CGW_PLG_1_32A.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if ((CGW_PLG_1_32A.buf[7] != CGW_PLG_1._c[4])||(CGW_PLG_1_32A.buf[4]  != CGW_PLG_1._c[1]))
    {
        CGW_PLG_1_32A.flag    = ON;
        CGW_PLG_1_32A.buf[3]  = 0x00;
        CGW_PLG_1_32A.buf[4]  = CGW_PLG_1._c[1];
        CGW_PLG_1_32A.buf[5]  = 0x00;
        CGW_PLG_1_32A.buf[6]  = 0x00;
        CGW_PLG_1_32A.buf[7]  = CGW_PLG_1._c[4];
        CGW_PLG_1_32A.buf[8]  = 0x00;
        CGW_PLG_1_32A.buf[9]  = 0x00;
        CGW_PLG_1_32A.buf[10] = 0x00;

        // DPRINTF(DBG_INFO, "IlGetRxVCU_3_8__WorkState \n");
    }
    else
    {
        CGW_PLG_1_32A.flag = FALSE;
    }
}

void CAN_COMM_200MS_458(void)
{

    // CGW_BCM_FM_458
    CGW_BCM_FM_458.buf[2] = 0x01; // need to change Node absent state
                                  // Active = 1
                                  // Absent = 0

    if ((CGW_BCM_FM_458.buf[3] != CGW_BCM_FM._c[0]) || (CGW_BCM_FM_458.buf[4] != CGW_BCM_FM._c[1]) || (CGW_BCM_FM_458.buf[5] != CGW_BCM_FM._c[2]) ||
        (CGW_BCM_FM_458.buf[6] != CGW_BCM_FM._c[3]))
    {
        CGW_BCM_FM_458.flag = ON;

        CGW_BCM_FM_458.buf[3]  = CGW_BCM_FM._c[0];
        CGW_BCM_FM_458.buf[4]  = CGW_BCM_FM._c[1];
        CGW_BCM_FM_458.buf[5]  = CGW_BCM_FM._c[2];
        CGW_BCM_FM_458.buf[6]  = CGW_BCM_FM._c[3];
        CGW_BCM_FM_458.buf[7]  = 0x00;
        CGW_BCM_FM_458.buf[8]  = 0x00;
        CGW_BCM_FM_458.buf[9]  = 0x00;
        CGW_BCM_FM_458.buf[10] = 0x00;
    }
    else
    {
        CGW_BCM_FM_458.flag = FALSE;
    }
}

void CAN_COMM_200MS_347(void)
{

    // CGW_VCU_3_12_347
    CGW_VCU_3_12_347.buf[2] = 0x01; // need to change Node absent state
                                    // Active = 1
                                    // Absent = 0

    if ((CGW_VCU_3_12_347.buf[3] != CGW_VCU_E_3._c[0]) || (CGW_VCU_3_12_347.buf[4] != CGW_VCU_E_3._c[1]) || (CGW_VCU_3_12_347.buf[5] != CGW_VCU_E_3._c[2]) ||
        (CGW_VCU_3_12_347.buf[6] != CGW_VCU_E_3._c[3]) || (CGW_VCU_3_12_347.buf[7] != CGW_VCU_E_3._c[4]))
    {
        CGW_VCU_3_12_347.flag    = ON;
        CGW_VCU_3_12_347.buf[3]  = CGW_VCU_E_3._c[0];
        CGW_VCU_3_12_347.buf[4]  = CGW_VCU_E_3._c[1];
        CGW_VCU_3_12_347.buf[5]  = CGW_VCU_E_3._c[2];
        CGW_VCU_3_12_347.buf[6]  = CGW_VCU_E_3._c[3];
        CGW_VCU_3_12_347.buf[7]  = CGW_VCU_E_3._c[4];
        CGW_VCU_3_12_347.buf[8]  = 0x00;
        CGW_VCU_3_12_347.buf[9]  = 0x00;
        CGW_VCU_3_12_347.buf[10] = 0x00;
    }
    else
    {
        CGW_VCU_3_12_347.flag = FALSE;
    }
}

void CAN_COMM_500MS_455(void)
{

    // CGW_BCM_TPMS_2_455
    CGW_BCM_TPMS_2_455.buf[2] = 0x01; // need to change Node absent state
                                      // Active = 1
                                      // Absent = 0

    if ((CGW_BCM_TPMS_2_455.buf[3] != CGW_BCM_TPMS_2._c[0]) || (CGW_BCM_TPMS_2_455.buf[4] != CGW_BCM_TPMS_2._c[1]) || (CGW_BCM_TPMS_2_455.buf[5] != CGW_BCM_TPMS_2._c[2]) ||
        (CGW_BCM_TPMS_2_455.buf[6] != CGW_BCM_TPMS_2._c[3]) || (CGW_BCM_TPMS_2_455.buf[7] != CGW_BCM_TPMS_2._c[4]) || (CGW_BCM_TPMS_2_455.buf[8] != CGW_BCM_TPMS_2._c[5]) ||
        (CGW_BCM_TPMS_2_455.buf[9] != CGW_BCM_TPMS_2._c[6]) || (CGW_BCM_TPMS_2_455.buf[10] != CGW_BCM_TPMS_2._c[7]))
    {
        CGW_BCM_TPMS_2_455.flag    = ON;
        CGW_BCM_TPMS_2_455.buf[3]  = CGW_BCM_TPMS_2._c[0];
        CGW_BCM_TPMS_2_455.buf[4]  = CGW_BCM_TPMS_2._c[1];
        CGW_BCM_TPMS_2_455.buf[5]  = CGW_BCM_TPMS_2._c[2];
        CGW_BCM_TPMS_2_455.buf[6]  = CGW_BCM_TPMS_2._c[3];
        CGW_BCM_TPMS_2_455.buf[7]  = CGW_BCM_TPMS_2._c[4];
        CGW_BCM_TPMS_2_455.buf[8]  = CGW_BCM_TPMS_2._c[5];
        CGW_BCM_TPMS_2_455.buf[9]  = CGW_BCM_TPMS_2._c[6];
        CGW_BCM_TPMS_2_455.buf[10] = CGW_BCM_TPMS_2._c[7];
    }
    else
    {
        CGW_BCM_TPMS_2_455.flag = FALSE;
    }
}

void CAN_COMM_500MS_45B(void)
{

    // CGW_SCM_R_45B
    CGW_SCM_R_45B.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0
    if ((CGW_SCM_R_45B.buf[3] != CGW_BCM_SCM_2._c[0]) || (CGW_SCM_R_45B.buf[6] != CGW_BCM_SCM_2._c[3]) || (CGW_SCM_R_45B.buf[8] != CGW_BCM_SCM_2._c[5]) ||
        (CGW_SCM_R_45B.buf[9] != CGW_BCM_SCM_2._c[6]) || (CGW_SCM_R_45B.buf[10] != CGW_BCM_SCM_2._c[7]))
    {
        CGW_SCM_R_45B.flag = ON;

        CGW_SCM_R_45B.buf[3]  = CGW_BCM_SCM_2._c[0];
        CGW_SCM_R_45B.buf[4]  = 0x00;
        CGW_SCM_R_45B.buf[5]  = 0x00;
        CGW_SCM_R_45B.buf[6]  = CGW_BCM_SCM_2._c[3];
        CGW_SCM_R_45B.buf[7]  = 0x00;
        CGW_SCM_R_45B.buf[8]  = CGW_BCM_SCM_2._c[5];
        CGW_SCM_R_45B.buf[9]  = CGW_BCM_SCM_2._c[6];
        CGW_SCM_R_45B.buf[10] = CGW_BCM_SCM_2._c[7];
    }
    else
    {
        CGW_SCM_R_45B.flag = FALSE;
    }
}

void CAN_COMM_500MS_3D4(void)
{

    // ACCM_3_3D4
    ACCM_3_3D4.buf[2] = 0x01; // need to change Node absent state
                              // Active = 1
                              // Absent = 0

    if ((ACCM_3_3D4.buf[3] != CGW_BCM_ACCM_3._c[0]) || (ACCM_3_3D4.buf[4] != CGW_BCM_ACCM_3._c[1]) || (ACCM_3_3D4.buf[5] != CGW_BCM_ACCM_3._c[2]) ||
        (ACCM_3_3D4.buf[6] != CGW_BCM_ACCM_3._c[3]) || (ACCM_3_3D4.buf[7] != CGW_BCM_ACCM_3._c[4]) || (ACCM_3_3D4.buf[8] != CGW_BCM_ACCM_3._c[5]) ||
        (ACCM_3_3D4.buf[9] != CGW_BCM_ACCM_3._c[6]) || (ACCM_3_3D4.buf[10] != CGW_BCM_ACCM_3._c[7]))
    {
        ACCM_3_3D4.flag    = ON;
        ACCM_3_3D4.buf[3]  = CGW_BCM_ACCM_3._c[0];
        ACCM_3_3D4.buf[4]  = CGW_BCM_ACCM_3._c[1];
        ACCM_3_3D4.buf[5]  = CGW_BCM_ACCM_3._c[2];
        ACCM_3_3D4.buf[6]  = CGW_BCM_ACCM_3._c[3];
        ACCM_3_3D4.buf[7]  = CGW_BCM_ACCM_3._c[4];
        ACCM_3_3D4.buf[8]  = CGW_BCM_ACCM_3._c[5];
        ACCM_3_3D4.buf[9]  = CGW_BCM_ACCM_3._c[6];
        ACCM_3_3D4.buf[10] = CGW_BCM_ACCM_3._c[7];
    }
    else
    {
        CGW_BCM_TPMS_2_455.flag = FALSE;
    }
}

void CAN_COMM_500MS_4A8(void)
{

    // TBOX_4_7_4A8
    TBOX_4_7_4A8.buf[2] = 0x01; // need to change Node absent state
                                // Active = 1
                                // Absent = 0

    if ((TBOX_4_7_4A8.buf[3] != TBOX_7._c[0]) || (TBOX_4_7_4A8.buf[4] != TBOX_7._c[1]) || (TBOX_4_7_4A8.buf[5] != TBOX_7._c[2]) ||
        (TBOX_4_7_4A8.buf[6] != TBOX_7._c[3]) || (TBOX_4_7_4A8.buf[7] != TBOX_7._c[4]) || (TBOX_4_7_4A8.buf[8] != TBOX_7._c[5]) ||
        (TBOX_4_7_4A8.buf[9] != TBOX_7._c[6]) || (TBOX_4_7_4A8.buf[10] != TBOX_7._c[7]))
    {
        TBOX_4_7_4A8.flag    = ON;
        TBOX_4_7_4A8.buf[3]  = TBOX_7._c[0];
        TBOX_4_7_4A8.buf[4]  = TBOX_7._c[1];
        TBOX_4_7_4A8.buf[5]  = TBOX_7._c[2];
        TBOX_4_7_4A8.buf[6]  = TBOX_7._c[3];
        TBOX_4_7_4A8.buf[7]  = TBOX_7._c[4];
        TBOX_4_7_4A8.buf[8]  = TBOX_7._c[5];
        TBOX_4_7_4A8.buf[9]  = TBOX_7._c[6];
        TBOX_4_7_4A8.buf[10] = TBOX_7._c[7];
    }
    else
    {
        TBOX_4_7_4A8.flag = FALSE;
    }
}

void CAN_COMM_500MS_461(void)
{

    // CGW_OBC_1_461
    CGW_OBC_1_461.buf[2] = 0x01; // need to change Node absent state
                                 // Active = 1
                                 // Absent = 0

    if (CGW_OBC_1_461.buf[9] != CGW_OBC_1._c[6])
    {
        CGW_OBC_1_461.flag    = ON;
        CGW_OBC_1_461.buf[3]  = 0x00;
        CGW_OBC_1_461.buf[4]  = 0x00;
        CGW_OBC_1_461.buf[5]  = 0x00;
        CGW_OBC_1_461.buf[6]  = 0x00;
        CGW_OBC_1_461.buf[7]  = 0x00;
        CGW_OBC_1_461.buf[8]  = 0x00;
        CGW_OBC_1_461.buf[9]  = CGW_OBC_1._c[6];
        CGW_OBC_1_461.buf[10] = 0x00;
    }
    else
    {
        CGW_OBC_1_461.flag = FALSE;
    }
}

void CAN_COMM_1000MS_4B1(void)
{

    // AVAS_1_4B1
    AVAS_1_4B1.buf[2] = 0x01; // need to change Node absent state
                              // Active = 1
                              // Absent = 0

    if (AVAS_1_4B1.buf[3] != AVAS_1._c[0])
    {
        AVAS_1_4B1.flag = ON;

        AVAS_1_4B1.buf[3]  = AVAS_1._c[0];
        AVAS_1_4B1.buf[4]  = 0x00;
        AVAS_1_4B1.buf[5]  = 0x00;
        AVAS_1_4B1.buf[6]  = 0x00;
        AVAS_1_4B1.buf[7]  = 0x00;
        AVAS_1_4B1.buf[8]  = 0x00;
        AVAS_1_4B1.buf[9]  = 0x00;
        AVAS_1_4B1.buf[10] = 0x00;
    }
    else
    {
        AVAS_1_4B1.flag = FALSE;
    }
}

void CAN_COMM_1000MS_4BA(void)
{

    // WCM_1_4BA
    WCM_1_4BA.buf[2] = 0x01; // need to change Node absent state
                             // Active = 1
                             // Absent = 0
    if ((WCM_1_4BA.buf[3] != WCM_1._c[0]) || (WCM_1_4BA.buf[4] != WCM_1._c[1]) || (WCM_1_4BA.buf[9] != WCM_1._c[6]))
    {
        WCM_1_4BA.flag = ON;

        WCM_1_4BA.buf[3]  = WCM_1._c[0];
        WCM_1_4BA.buf[4]  = WCM_1._c[1];
        WCM_1_4BA.buf[5]  = 0x00;
        WCM_1_4BA.buf[6]  = 0x00;
        WCM_1_4BA.buf[7]  = 0x00;
        WCM_1_4BA.buf[8]  = 0x00;
        WCM_1_4BA.buf[9]  = WCM_1._c[6];
        WCM_1_4BA.buf[10] = 0x00;
    }
    else
    {
        WCM_1_4BA.flag = FALSE;
    }
}

// CAN锟斤拷锟斤拷转锟斤拷
void S52_CAN_Comm_Proc(DEVICE *dev)
{
    static uint8 Msg_SW = 0;
    static uint8 test   = 0;

    switch (Msg_SW)
    {
    case 0:
        CHK_CAN_COMM_10MS_MSG(dev);
        Msg_SW++;
        break;

    case 1:
        CHK_CAN_COMM_20MS_MSG(dev);
        Msg_SW++;
        break;

    case 2:
        CHK_CAN_COMM_50MS_MSG(dev);
        Msg_SW++;
        break;

    case 3:
        CHK_CAN_COMM_100MS_MSG(dev);
        Msg_SW++;
        break;

    case 4:
        CHK_CAN_COMM_200MS_MSG(dev);
        Msg_SW++;
        break;

    case 5:
        CHK_CAN_COMM_500MS_MSG(dev);
        Msg_SW++;
        break;

    case 6:
        CHK_CAN_COMM_1000MS_MSG(dev);
        Msg_SW = 0;
        break;

    default:
        break;
    }
}
