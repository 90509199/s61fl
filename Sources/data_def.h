/*
 * common.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef DATA_TYPE_H_
#define DATA_TYPE_H_

#define PRESET_MAX				(30)
#define AST_LIST_MAX 			(250)

#define OFF									0
#define ON									1 

#define RET_FAIL						-1
#define RET_OK							1

#define FM_MODE						0
#define AM_MODE						1 

#define AMP_MUTE						1
#define AMP_UNMUTE					2 

#ifdef RADIO_VOLUME_4DB_PLUS
#ifdef CX62C_FUNCTION
#define	AK_MEDIA_VOL_DEFAULT						22 //22->29->22							//JANG_221124
#else
#define	AK_MEDIA_VOL_DEFAULT						22 //22
#endif
#else
#define	AK_MEDIA_VOL_DEFAULT						18  //25 		//15								//25
#endif
#define	AK_BCALL_VOL_DEFAULT						25


/* Start user code for definition. Do not edit comment generated here */
#ifndef NULL
#define NULL					(0x00u)
#endif

#ifndef NULL_P
#define NULL_P				((void *)(0))
#endif


#ifndef TRUE
#define TRUE					(0x01u)
#endif

#ifndef FALSE
#define FALSE					(0x00u)
#endif

#ifndef ON
#define ON						(0x01u)
#endif

#ifndef OFF
#define OFF						(0x00u)
#endif

#ifndef HIGH
#define HIGH 					(0x01u)
#endif

#ifndef LOW
#define LOW					(0x00u)
#endif

#ifndef ACTIVE
//#define ACTIVE				(0x01u)
#endif

#ifndef INACTIVE
#define INACTIVE			(0x00u)
#endif


#ifndef UNUSED_V
#define UNUSED_V(v)   (void)(v)
#endif

#define BIT0_BYTE       (1u << 0)
#define BIT1_BYTE       (1u << 1)
#define BIT2_BYTE       (1u << 2)
#define BIT3_BYTE       (1u << 3)
#define BIT4_BYTE       (1u << 4)
#define BIT5_BYTE       (1u << 5)
#define BIT6_BYTE       (1u << 6)
#define BIT7_BYTE       (1u << 7)


#define CHECK_STATUS_STEP_0		0
#define CHECK_STATUS_STEP_1		1
#define CHECK_STATUS_STEP_2 		2
#define CHECK_STATUS_STEP_3 		3
#define CHECK_STATUS_STEP_4  		4
#define CHECK_STATUS_STEP_5  		5
#define CHECK_STATUS_STEP_6  		6
#define CHECK_STATUS_STEP_7  		7
#define CHECK_STATUS_STEP_8  		8
#define CHECK_STATUS_STEP_9  		9

#define SET_BIT(x, y) 									((x) |= (1U << (y))) 		// set bit(port, number)
#define CLEAR_BIT(x, y) 								((x) &= ~(1U << (y)))		// clear bit(port, number)
#define COMPARE_BIT(x, y)								(((x) >> (y)) & 0x01U)

#define SET_BIT_SHIFT0(x) 								((x) |= (0x01U)) 			// set bit(port, number)
#define CLEAR_BIT_SHIFT0(x) 							((x) &= ~(0x01U))			// clear bit(port, number)
#define COMPARE_BIT_SHIFT0(x)							((x) & 0x01U)

#define FLASH_DATA0_ADDR							( 0x00000000u )
#define FLASH_DATA1_ADDR							(FLASH_DATA0_ADDR+(0x80))

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/
//typedef char    sint8;
typedef short sint16;
typedef long    sint32;
typedef unsigned long	uint32;
typedef long long  sint64;


typedef char int8;
typedef short int16;
typedef int int32;
typedef long long int64;

typedef unsigned char uint8;
typedef unsigned short uint16;
//typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef unsigned char  vuint8;
typedef signed char    vsint8;
typedef signed short vsint16;
//typedef signed int vsint32;

//typedef unsigned int     vbittype;
#define canbittype       vbittype


typedef unsigned char       u8;    // 1 byte unsigned; prefix: ub 
typedef signed char         s8;    // 1 byte signed;   prefix: sb 
typedef unsigned short      u16;    // 2 byte unsigned; prefix: uw 
typedef signed short        s16;    // 2 byte signed;   prefix: sw 
typedef unsigned long       u32;    // 4 byte unsigned; prefix: ul 
typedef signed long         s32;    // 4 byte signed;   prefix: sl 
typedef unsigned long long	u64;	// 8 byte unsigned;   prefix: ull
typedef signed long long	s64;	// 8 byte signed;   prefix: sll

typedef unsigned char BYTE;

typedef	unsigned char	    BOOL;

typedef	unsigned long	    U32;
typedef	unsigned long	    DWORD;
typedef	unsigned short	    U16;
typedef	unsigned short	    WORD;
typedef	volatile WORD	    VWORD;
typedef	unsigned char	    U8;
typedef	unsigned char	    BYTE;
typedef	volatile BYTE 	    VBYTE;
typedef	unsigned int	    jbittype;
typedef	unsigned int	    jbit;

#ifdef SVCD_ENABLE

#define	SVCD_SET_LOW_1		35
#define	SVCD_SET_LOW_2		74
#define	SVCD_SET_LOW_3		113
#define	SVCD_SET_LOW_4		153
#define	SVCD_SET_LOW_5		192
#define	SVCD_SET_LOW_6		232

#define	SVCD_SET_MID_1		22
#define	SVCD_SET_MID_2		37
#define	SVCD_SET_MID_3		52
#define	SVCD_SET_MID_4		67
#define	SVCD_SET_MID_5		82
#define	SVCD_SET_MID_6		97
#define	SVCD_SET_MID_7		111
#define	SVCD_SET_MID_8		126
#define	SVCD_SET_MID_9		141
#define	SVCD_SET_MID_10	156
#define	SVCD_SET_MID_11	171
#define	SVCD_SET_MID_12	186
#define	SVCD_SET_MID_13	201
#define	SVCD_SET_MID_14	216

#define	SVCD_SET_HIGH_1		20
#define	SVCD_SET_HIGH_2		29
#define	SVCD_SET_HIGH_3		38
#define	SVCD_SET_HIGH_4		47
#define	SVCD_SET_HIGH_5		56
#define	SVCD_SET_HIGH_6		66
#define	SVCD_SET_HIGH_7		74
#define	SVCD_SET_HIGH_8		84
#define	SVCD_SET_HIGH_9		93
#define	SVCD_SET_HIGH_10		102
#define	SVCD_SET_HIGH_11		111
#define	SVCD_SET_HIGH_12		121
#define	SVCD_SET_HIGH_13		129
#define	SVCD_SET_HIGH_14		138

typedef enum 
{
	SVCD_STEP_IDEL = 0x0,
	SVCD_STEP_1,
	SVCD_STEP_2,
	SVCD_STEP_3,
	SVCD_STEP_4,
	SVCD_STEP_5,
	SVCD_STEP_6,
	SVCD_STEP_7,
	SVCD_STEP_8,
	SVCD_STEP_9,
	SVCD_STEP_10,
	SVCD_STEP_11,
	SVCD_STEP_12,
	SVCD_STEP_13,
	SVCD_STEP_14,
}SVCD_STEP_TYPE;
#endif

typedef enum 
{
	TUNER_TYPE_IDLE=0,
	TUNER_TYPE_NXP,
	TUNER_TYPE_SILABS ,
}TUNER_TYPE;

typedef struct tagGPS_TIME
{
	uint8 enable;
	uint8 year;	
	uint8 month;
	uint8 day;
	uint8 hour;
	uint8 min;
	uint8 sec;
	uint32 counter;
}GPS_TIME;


typedef struct tagLCD_CTR
{
	uint8 enable;
	uint8 direct;	
	uint8 counter;	
	uint8 startvalue;
	uint8 gotovalue;
	uint8 onoff;
	uint8 rev_ctl;
}LCD_CTR;

typedef struct tagACCIGN_STATE
{
	uint8 prev_state;
	uint8 state;	
	uint8 cnt;	
}ACCIGN_STATE;

typedef struct tagUART_RXTX_TEST
{
	uint8 state;
	uint32 cnt;
	uint16 txcnt;
	uint16 rxcnt;
}UART_RXTX_TEST;


typedef struct tagPOWER
{
	uint8 p_state;
	uint8 p_high_proity;			// Fuck what is the proity...???
	uint32 p_batt_Voltage;
	uint8 p_SOC_state;
	uint16 p_SOC_PMIC_CTL_TIME;
}POWER;


typedef struct tagSYSTEM
{
	uint8 s_state;
	uint8 amp_state;
	uint8 s_state_backup;
	uint8 s_state_audio_state;
	uint8 s_state_lcd_state;
	uint8 s_state_prev_lcd_state;
	uint8 last_mode;			//last mode
	uint8 screenoff_enable;
	uint8 screensaver_enable;
	uint8 low_batter_enter;
	uint8 low_batter_status;
	uint8 system_init;
	uint8 soc_monitor;
	uint8 power_key_check;
	uint8 power_key_status;
	uint8 gear_status;
	#ifdef SVCD_ENABLE
	SVCD_STEP_TYPE svcd_step;
	SVCD_STEP_TYPE pev_svcd_step;
	uint8 svcd_speed;
	uint8 prev_svcd_speed;
	uint8 prev_svcd_e2p;
	#endif
	//no response counter
	uint8 no_response_active;
//	uint8 no_response_standby;
	uint8 no_response_screenoff;
	uint8 no_response_15min;
	uint8 no_response_screensaver;
//	uint8 no_response_sleep;

	LCD_CTR lcd_control;
	//time
	GPS_TIME gps_time;
	ACCIGN_STATE accign_state;

	#ifdef CAN_RXTX_TEST_FUNC
	UART_RXTX_TEST rxtx_test;
	#endif
	#ifdef VECTOR_CCL_TEST
	uint8 ccl_active;
	uint8 prev_ccl_active;
	uint8 usrdata1;
	uint8 wakup_userdata;
	uint8 can_wakeup;
	#endif

	uint8 lcd_ok;
	uint32 init_ok:1;
	uint32 boot_seq1:1;
	uint32 power_on:1;
	uint32 uart_ok:1;
	uint32 check_high_voltage_dtc:1;
	uint32 check_low_voltage_dtc:1;
	uint32 alivesignal:1;
	uint32 alivecleartime:1;
	uint32 pshold:1;
	uint32 soc_reboot:1;

	uint32 uart_send_on:1;
	uint32 incomecalling:1;
	uint32 rvcavm_active:1;
	uint32 voice_key_active:1;
	uint32 r_gear_active:1;
	uint32 touch_active:1;
	uint32 key_active:1;
	uint32 ask_screen_save_mode:2;
	uint32 ask_screen_off_mode:2;
	//mode ack protocol
	uint32 send_sleep_command:1;
	uint32 send_standby_command:1;
	uint32 send_active_command:1;
	
	uint32 send_screen_save_command:1;
	uint32 send_screenoff_1_command:1;
	uint32 send_screenoff_2_command:1;
	uint32 send_15min_command:1;
	//update
	uint32 mcu_update:1;
	uint32 soc_update:1;
	#ifdef QFI_MODE_SEQ_CHANGE
	uint32 soc_update_excute:1;
	#endif
	uint32 soc_update_powerkey:1;
	uint32 soc_update_setkey:1;
	uint32 soc_update_downkey:1;
	//engineer mode
	uint32 eng_mode_on:1;
	uint32 canmn_set_standby:1;
	uint32 canmn_start_flag:1;
	#ifdef CHANG_BL_TIMING
	uint32 blpwmenable:1;//JANG_211025
	#endif
}SYSTEM;
typedef struct tagUART_JYI_TEST
{
	uint8 u_state;
	uint16 u_length;
	uint16 u_ptr;
	uint16 u_buf_end;

}UART_JYI_TEST;

typedef struct tagTEF6686
{
	uint8	t_state;						//status
	uint8	t_act_cnt;					//start counter
	uint8	t_mode;						//mode
	uint16 fm_freq;						//FM Freq
	uint16 am_freq;					//AM Setting
	uint16 start_freq;					//AST
	uint16 ast_cnt;
	uint16 ast_list_tmp[AST_LIST_MAX];	//check buffer
	uint16 ast_list[AST_LIST_MAX];				//check buffer
	
	uint8 band;
	uint8 band_bk;
	
	///////////////////////////////
	//
	// 라디오 변수들
}TEF6686;

typedef struct tagAK7739
{
	uint8	d_state;						//status
	uint8	d_state_lock;				//lock
	uint8	d_audo_mode;			//audio mode
	uint8	d_prev_audo_mode;			//audio mode
	uint8	d_change_audo_mode;		//audio mode
	uint8	d_act_cnt;					//start counter
	uint8	d_boot_ok;					

	uint8	d_media_vol;
	uint8	d_radar_vol;
	uint8	d_instrument_vol;
	uint8	d_key_vol;
	uint8	d_phone_vol;
	uint8	d_navi_vol;
	uint8	d_voice_vol;
	uint8	d_bcall_vol;
	uint8	d_softmute_status;
	uint8	d_goto_mute_status;
	uint8	d_mix_status;
	
	uint8	d_musical_rhythm_enable;
	uint8	d_musical_rhythm_status;
}AK7739;

typedef struct tagCPU_MSG
{
	uint32 send_radio_finish_ast: 1;
	uint32 send_radio_valid_signal: 1;
	uint32 send_last_memory: 1;
}CPU_MSG;


typedef struct tagENCODER
{
	uint8 encoder;
	uint8 oldstatus;
	uint8 direction;
	uint8 laststatus;
//	uint8 code;
	uint8 needstable;
	uint32 enable:1;
	uint32 event:1;
	uint32 txed:1;
	uint32 ack:1;
	uint32 retry:4;
	vsint8 click;
}ENCODER;

typedef struct tagBEEP
{
	uint8 beep_active;	
	uint8 beep_pre_active;
	uint32 fl_beep:1;
	uint32 fr_beep:1;
	uint32 rl_beep:1;
	uint32 rr_beep:1;
	uint32 pre_fl_beep:1;
	uint32 pre_fr_beep:1;
	uint32 pre_rl_beep:1;
	uint32 pre_rr_beep:1;
}BEEP;


typedef struct tagA2B
{
	uint8 a_state;					//A2B status: IDLE/INIT1/INIT2/ACTIVE/
	uint32 a_init_cnt;				//to record how many times tried to init A2B bus
	uint8 init_trigger_flag;      //A2B reinitialize flag
	uint8 discovery_success;  	//A2B discovery success flag
	uint8 init_success;      		//A2B para init success flag
	uint8 error_retry_count;      //A2B RD/WR error count
	uint8 int_type;               //A2B interrupt type
	uint8 a2b_need_recover;
	uint8 a2b_power_status;       //A2B power on/off status(apply a_init_cnt > 0)
	uint32 series_init_err_count; //A2B series init err count
	
}A2B;


#ifdef CX62C_FUNCTION
typedef struct tagTpNamebuf
{
	uint8_t *buf;
	uint8_t len;
	uint8_t cnt;
	uint8_t lock;
}TpNamebuf;

typedef enum 
{
	RRM_DISPLAY_CEAR_INFO_CMD = 0x3,
	RRM_DISPLAY_ROAD_NAME_CMD = 0x30,
	RRM_DISPLAY_NAVI_INFO_CMD,
	RRM_DISPLAY_SONG_NAME_CMD = 0x50,
	RRM_DISPLAY_ARTIST_NAME_CMD,
	RRM_DISPLAY_PHONE_NUM_CMD = 0x70,
	RRM_DISPLAY_CALL_NAME_CMD,
	RRM_DISPLAY_CALL_INFO_CMD,
}TP_CMD_TYPE;

typedef enum 
{
	TP_STATE_IDLE = 0x0,
	TP_STATE_CHECKIGN_START,
	TP_STATE_CHECKIGN_END,
	TP_STATE_INIT,
	TP_STATE_COMM_START,
	TP_STATE_COMM_PROCESS,
	TP_STATE_COMM_END,
	TP_STATE_EVENT,
	TP_STATE_MAX,
}TP_STATE_TYPE;
#endif

typedef enum 
{
	SOURCE_STATION_IDLE = 0x0,
	SOURCE_STATION_RADIO,
	SOURCE_STATION_USB,
	SOURCE_STATION_ONLINE_MUSIC,
	SOURCE_STATION_ONLINE_RADIO,
	SOURCE_STATION_BLUETOOTH,
	SOURCE_STATION_IPOD,
	SOURCE_STATION_LOCALSTORAGE,
}SOURCE_STATION_TYPE;

typedef struct tagDEVICE
{
	SYSTEM *r_System;			//system
	POWER *r_Power;					//power
	TEF6686 *r_Tuner;				// tuner
	AK7739 *r_Dsp;						//dsp
	CPU_MSG *r_SendCPU;	//cpu 
	ENCODER *r_LeftEncoder;
	BEEP *r_Beep;
	A2B *r_A2B;
	BYTE ShareMemory[255];
	UART_JYI_TEST *X50_UART;		// only for test
}DEVICE;


extern DEVICE r_Device;
extern ENCODER r_Encoder;
extern uint8 r_sonysound;

#ifdef CX62C_FUNCTION
extern TP_STATE_TYPE gCanTpState;
extern uint8 DVD_ICM_ONOFF;

extern uint8 manufacture_maxim;
#endif

extern volatile uint8 nm_wakeup_flag;
extern volatile uint8 ignacc_wakeup_flag;
extern volatile uint8 diag_wakeup_flag;
extern volatile uint8 nm_firstframe_cnt;
extern volatile uint8 low_batter_flag;			//polling syste slow when dsp initialize, and i used this flag for 순단 테스트 
extern volatile uint8 flash_erase_on;

extern uint32 shell_value_0;
extern uint32 shell_value_1;
#ifdef SEPERATE_VOLUME_TABLE
enum
{
	CH_TYPE_MEDIA,
	CH_TYPE_FM,
	CH_TYPE_AM
};

extern uint8 mediachtype;
#endif
#if 1//def CX62C_FUNCTION
typedef enum  {
	QFI_MODE_IDLE=0,
	QFI_MODE_PROCESS_STEP1,
	QFI_MODE_PROCESS_STEP2,
	QFI_MODE_PROCESS_END,
	QFI_MODE_ENTER,
}QFIModeType;

extern QFIModeType QFIModeState;
#endif

#define SHAREMEM_0_INIT 			(0)			//System init ok, enter the power_proc
#define SHAREMEM_1_BATT				(1)			//BATT_Value Check
#define SHAREMEM_2_ACC_IGN			(2)			//ACC_IGN Status
#define SHAREMEM_3_EEPROM_EN		(3)			//EXT_3V3_EN
#define SHAREMEM_4_Ver1 			(4)			//IVI MCU Version_1
#define SHAREMEM_5_Ver2 			(5)			//IVI MCU Version_2
#define SHAREMEM_6_Ver3				(6)			//IVI MCU Version_3
#define SHAREMEM_7_ICM_BL_Status	(7)			//ICM_BackLight Status
#define SHAREMEM_8_ICM_BL_Control	(8)			//ICM_BackLight ONOFF
#define SHAREMEM_9_SOC_PW			(9)			//SOC_POWER ONOFF
#define SHAREMEM_10_ICM_Duty		(10)		//ICM_Brightness
#define SHAREMEM_11_ICM_Duty_state	(11)		//ICM_Brightness_state

#define SHAREMEM_500_Test1_LaneAssistreq	(500)		//ICM_LaneAssistreq
#define SHAREMEM_501_Test2_ClusterEEPRom_Fault	(501)		//ICM_DTC_EEPRom_Fault_test
#define SHAREMEM_500_SiLab_Flash_Ver0		(502)		//SiLab_Tuner_Flash_version_0
#define SHAREMEM_500_SiLab_Flash_Ver1		(503)		//SiLab_Tuner_Flash_version_1
#define SHAREMEM_500_SiLab_Flash_Ver2		(504)		//SiLab_Tuner_Flash_version_2

#endif /* DATA_TYPE_H_ */
