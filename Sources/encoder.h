/*
 *  encoder.h
 *
 *  Created on: 2019. 1. 4.
 *      Author: Digen
 */

#ifndef KEY_ENCODER_H_
#define KEY_ENCODER_H_

#define J_ENCODER_STOP (0x55)
#define J_ENCODER_MOVE (0xAA)
#define J_ENCODER_CW (0xFF)
#define J_ENCODER_CCW (0x0F)


void EncoderProc1(ENCODER *encoder,volatile uint8 port);
void ReadEncoder(DEVICE *dev);

#endif /* KEY_ENCODER_H_ */
