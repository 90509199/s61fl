/*
 * abs_timer.h
 *
 *  Created on: 2020. 3. 4.
 *      Author: Digen
 */

#ifndef ABSTRACTION_ABS_TIMER_H_
#define ABSTRACTION_ABS_TIMER_H_

/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include "../includes.h"

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/
typedef enum{
	ABS_REV0_TIMER = 0,
	ABS_REV1_TIMER,
	ABS_MAIN_TIMER,
	ABS_TIMER_MAX
}eAbsTimerIdx;

typedef void (*fpAbsTimerIsrCb)(void);

/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
#define ABSTIMER_GET_US_TIME()		AbsTimerGetTimerUs(ABS_MAIN_TIMER);

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/
extern void AbsTimerInit(void);
extern void AbsTimerDeInit(void);
extern void AbsTimerRegCbFunc(fpAbsTimerIsrCb func);
extern void AbsTimerStart(eAbsTimerIdx idx);
extern void AbsTimerStop(eAbsTimerIdx idx);
extern uint64 AbsTimerGetTimerUs(eAbsTimerIdx idx);
#endif /* ABSTRACTION_ABS_TIMER_H_ */
