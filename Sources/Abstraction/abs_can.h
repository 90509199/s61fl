/*
 * abs_can.h
 *
 *  Created on: 2020. 3. 5.
 *      Author: Digen
 */

#ifndef ABSTRACTION_ABS_CAN_H_
#define ABSTRACTION_ABS_CAN_H_

/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include "../includes.h"
#include "flexcan_driver.h"
#include "flexcan_hw_access.h"
#include "canCom1.h"
/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/
typedef void (*tAbsCanHsRxIndiationCbType)(uint32 buffIdx, uint32 cs, uint32 msgId, uint8* data, uint8 dataLen);
typedef void (*tAbsCanHsTxConfirmCbType)(uint32 msgId);
typedef void (*tAbsCanErrIndCbType)(void);
/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
#define ABSCANHS0_CONFIG_TXMB(mailBoxIdx, msgDescriptor, msgId)							FLEXCAN_DRV_ConfigTxMb(INST_CANCOM1, (mailBoxIdx), (msgDescriptor), (msgId));
#define ABSCANHS0_CONFIG_RXMB(mailBoxIdx, msgDescriptor, msgId)							FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, (mailBoxIdx), (msgDescriptor), (msgId));

#if 1
#define ABSCANHS0_SEND_MSG(mailBoxIdx, msgDescriptor, msgId, msg)						FLEXCAN_DRV_SendBlocking(INST_CANCOM1,(mailBoxIdx), (msgDescriptor), (msgId), (msg), 10u);
#else
#define ABSCANHS0_SEND_MSG(mailBoxIdx, msgDescriptor, msgId, msg)						FLEXCAN_DRV_Send(INST_CANCOM1,(mailBoxIdx), (msgDescriptor), (msgId), (msg));
#endif

#define ABSCANHS0_GET_TRANFER_STATUS(mbIdx)				FLEXCAN_DRV_GetTransferStatus(INST_CANCOM1, (mbIdx));

#define ABSCANHS0_START_RECEIVE(idx) 					AbsCanHs0StartReceive((idx))
#define ABSCANHS0_START_RXFIFO()						AbsCanHs0StartRxFifo()

#define ABSCANHS0_DISABLE_MB_IRQ()						INT_SYS_DisableIRQ(CAN0_ORed_0_15_MB_IRQn);\
														INT_SYS_DisableIRQ(CAN0_ORed_16_31_MB_IRQn)
														
#define ABSCANHS0_ENABLE_MB_IRQ()						INT_SYS_EnableIRQ(CAN0_ORed_0_15_MB_IRQn);\
														INT_SYS_EnableIRQ(CAN0_ORed_16_31_MB_IRQn)

#define ABSCANHS0_ABORT_TRANSFER(mailboxIdx)			FLEXCAN_DRV_AbortTransfer(INST_CANCOM1, (mailboxIdx));

#define ABSCANHS0_CONFIG_RX_FIFO(idFormat, table)		FLEXCAN_DRV_ConfigRxFifo(INST_CANCOM1, (idFormat), (table))

#define ABSCANHS0_SET_RXFIFO_GLOBAL_MASK(idType, mask)					FLEXCAN_DRV_SetRxFifoGlobalMask(INST_CANCOM1, (idType), (mask))
#define ABSCANHS0_SET_RXMASKTYPE_INDIVIDUAL()							FLEXCAN_DRV_SetRxMaskType(INST_CANCOM1, FLEXCAN_RX_MASK_INDIVIDUAL)
#define ABSCANHS0_SET_RXFIFO_INDIVIDUAL_MASK(idType, idCounter, mask)		FLEXCAN_DRV_SetRxIndividualMask(INST_CANCOM1, (idType), (idCounter), (mask))

#define ABSCANHS0_GET_ERROR_STS()											FLEXCAN_DRV_GetErrorStatus(INST_CANCOM1);

#define ABSCANHS0_GET_WAKE_MB(msgBuff)										FLEXCAN_DRV_GetWMB(INST_CANCOM1, 0, msgBuff);
/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/
extern void AbsCanHs0Init(void);
extern void AbsCanHs0DeInit(void);
extern void AbsCanHs0ReInit(void);
extern void AbsCanHs0RegRxIndCb(tAbsCanHsRxIndiationCbType istRxInd);
extern void AbsCanHs0RegTxConfCb(tAbsCanHsTxConfirmCbType istTxInd);
extern void AbsCanHs0RegErrIndCb(tAbsCanErrIndCbType istErrInd);
extern void AbsCanHs0StartReceive(uint8 iU8Idx);
extern void AbsCanHs0StartRxFifo(void);

#endif /* ABSTRACTION_ABS_CAN_H_ */
