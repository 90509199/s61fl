/*
 * abs_can.c
 *
 *  Created on: 2020. 3. 5.
 *      Author: Digen
 */
#include "../includes.h"
#include "abs_can.h"

/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/

/*****************************************************************************/
/* Global variable declarations ('extern', definition in C source)           */
/*****************************************************************************/
static flexcan_msgbuff_t gStAbsCanHs0RxDataBuff;
static tAbsCanHsRxIndiationCbType gStAbsCanHs0RxIndCbFunc = NULL;
static tAbsCanHsTxConfirmCbType gStAbsCanHs0TxConfCbFunc = NULL;
static tAbsCanErrIndCbType gStAbsCanHs0ErrIndCbFunc = NULL;

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/
static void AbsCanHs0Isr(uint8_t instance, flexcan_event_type_t iStEventType, uint32_t iU32BuffIdx, flexcan_state_t *ipStCanSts);
static void AbsCanHs0ErrorIsr(uint8_t instance, flexcan_event_type_t eventType, flexcan_state_t *flexcanState);
static void AbsCanHs0BuffInit(void);
/*****************************************************************************/
/*  Function															*/
/*****************************************************************************/

/*****************************************************************************/
/*  DRV CAN FD initialization													 */
/*****************************************************************************/
void AbsCanHs0Init(void)
{
	AbsCanHs0BuffInit();
	
	// FLEXCAN_DRV_Init(INST_CANCOM1, &canCom1_State, &canCom1_InitConfig0);
	// FLEXCAN_DRV_InstallEventCallback(INST_CANCOM1, AbsCanHs0Isr, NULL);
	// FLEXCAN_DRV_InstallErrorCallback(INST_CANCOM1, AbsCanHs0ErrorIsr, NULL);
}

/*****************************************************************************/
/*  DAbsCanHs0DeInit													 */
/*****************************************************************************/
void AbsCanHs0DeInit(void)
{
	FLEXCAN_DRV_Deinit(INST_CANCOM1);
}

/*****************************************************************************/
/*  AbsCanHs0RegRxIndCb													 */
/*****************************************************************************/
void AbsCanHs0RegRxIndCb(tAbsCanHsRxIndiationCbType istRxInd)
{
	if(NULL != istRxInd)
	{
		gStAbsCanHs0RxIndCbFunc = istRxInd;
	}
}

/*****************************************************************************/
/*  AbsCanHs0RegTxConfCb													 */
/*****************************************************************************/
void AbsCanHs0RegTxConfCb(tAbsCanHsTxConfirmCbType istTxInd)
{
	if(NULL != istTxInd)
	{
		gStAbsCanHs0TxConfCbFunc = istTxInd;
	}
}

/*****************************************************************************/
/*  AbsCanHs0RegTxConfCb													 */
/*****************************************************************************/
void AbsCanHs0RegErrIndCb(tAbsCanErrIndCbType istErrInd)
{
	if(NULL != istErrInd)
	{
		gStAbsCanHs0ErrIndCbFunc = istErrInd;
	}
}

/*****************************************************************************/
/*  AbsCanHs0StartReceive													 */
/*****************************************************************************/
void AbsCanHs0StartReceive(uint8 iU8Idx)
{
	FLEXCAN_DRV_Receive(INST_CANCOM1, iU8Idx, &gStAbsCanHs0RxDataBuff);
}

/*****************************************************************************/
/*  AbsCanHs0StartReceive													 */
/*****************************************************************************/
void AbsCanHs0StartRxFifo(void)
{
	FLEXCAN_DRV_RxFifo(INST_CANCOM1, &gStAbsCanHs0RxDataBuff);
}

/*****************************************************************************/
/*  AbsCanHs0Isr													 */
/*****************************************************************************/
static void AbsCanHs0Isr(uint8_t instance, flexcan_event_type_t iStEventType, uint32_t iU32BuffIdx, flexcan_state_t *ipStCanSts)
{
	(void)ipStCanSts;
	(void)instance;

	switch(iStEventType)
	{
		case FLEXCAN_EVENT_RXFIFO_COMPLETE:
			if(NULL != gStAbsCanHs0RxIndCbFunc)
			{
				gStAbsCanHs0RxIndCbFunc(iU32BuffIdx, gStAbsCanHs0RxDataBuff.cs, gStAbsCanHs0RxDataBuff.msgId, gStAbsCanHs0RxDataBuff.data, gStAbsCanHs0RxDataBuff.dataLen);
			}
			else
			{
				;
			}

			AbsCanHs0BuffInit();
			FLEXCAN_DRV_RxFifo(INST_CANCOM1, &gStAbsCanHs0RxDataBuff);

			break;

		case FLEXCAN_EVENT_TX_COMPLETE:
			if( NULL != gStAbsCanHs0TxConfCbFunc )
			{
				gStAbsCanHs0TxConfCbFunc(iU32BuffIdx);
			}
			break;

		case FLEXCAN_EVENT_ERROR:
			break;

		default:
			break;

	}
}

/*****************************************************************************/
/*  AbsCanHs0ErrorIsr													 */
/*****************************************************************************/
static void AbsCanHs0ErrorIsr(uint8_t instance, flexcan_event_type_t eventType, flexcan_state_t *flexcanState)
{
	gStAbsCanHs0ErrIndCbFunc();
}

/*****************************************************************************/
/*  AbsCanHs0BuffInit													 */
/*****************************************************************************/
static void AbsCanHs0BuffInit(void)
{
	uint8 idx = 0;
	
	gStAbsCanHs0RxDataBuff.cs = 0;
	gStAbsCanHs0RxDataBuff.dataLen = 0;
	gStAbsCanHs0RxDataBuff.msgId = 0;

	for(idx = 0 ; idx < 64 ; idx++)
	{
		gStAbsCanHs0RxDataBuff.data[idx] = 0;
	}
}
