/*
 * abs_timer.c
 *
 *  Created on: 2020. 3. 4.
 *      Author: Digen
 */

#include "abs_timer.h"
#include "interrupt_manager.h"
/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
#define ABSTIMER_REV0_TIMER_MASK	0x00000001u
#define ABSTIMER_REV1_TIMER_MASK	0x00000002u
#define ABSTIMER_MAIN_TIMER_MASK	0x00000004u

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/

/*****************************************************************************/
/* Global variable declarations ('extern', definition in C source)           */
/*****************************************************************************/
fpAbsTimerIsrCb gFpAbsTimerIsrCbFunc;
/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/

/*****************************************************************************/
/*  Function															*/
/*****************************************************************************/
void AbsTimerIsr(void);

/*****************************************************************************/
/*  ABS TIMER initialization													 */
/*****************************************************************************/
void AbsTimerInit(void)
{
	#if 0
	INT_SYS_InstallHandler(LPIT0_Ch2_IRQn, AbsTimerIsr, (isr_t *)0);

	LPIT_DRV_Init(INST_LPIT1, &lpit1_InitConfig);
	LPIT_DRV_InitChannel(INST_LPIT1, ABS_MAIN_TIMER, &lpit1_ChnConfig2);
	#endif
}

/*****************************************************************************/
/*  ABS CLOCK de-initialization													 */
/*****************************************************************************/
void AbsTimerDeInit(void)
{
	//LPIT_DRV_Deinit(INST_LPIT1);
}

/*****************************************************************************/
/*  AbsTimerStart     													 */
/*****************************************************************************/
void AbsTimerStart(eAbsTimerIdx idx)
{
	switch(idx)
	{
		case ABS_MAIN_TIMER:
//			LPIT_DRV_StartTimerChannels(INST_LPIT1, ABSTIMER_MAIN_TIMER_MASK);
			break;
		default:
			break;
	}

}

/*****************************************************************************/
/*  AbsTimerStop     													 */
/*****************************************************************************/
void AbsTimerStop(eAbsTimerIdx idx)
{
	switch(idx)
	{
		case ABS_MAIN_TIMER:
//			LPIT_DRV_StopTimerChannels(INST_LPIT1, ABSTIMER_MAIN_TIMER_MASK);
			break;
		default:
			break;
	}
}

/*****************************************************************************/
/*  AbsTimerGetTimerUs     													 */
/*****************************************************************************/
uint64 AbsTimerGetTimerUs(eAbsTimerIdx idx)
{
	uint64 lU8UsDuration = 0;
#if 0
	lU8UsDuration = LPIT_DRV_GetCurrentTimerUs(INST_LPIT1, idx);
#else
	lU8UsDuration = (uint64)GetTickCount();
#endif
	return lU8UsDuration;
}

/*****************************************************************************/
/*  AbsTimerRegCbFunc													 */
/*****************************************************************************/
void AbsTimerRegCbFunc(fpAbsTimerIsrCb func)
{
	if(NULL != func)
	{
		gFpAbsTimerIsrCbFunc = func;
	}
}

/*****************************************************************************/
/*  AbsTimerIsr													 */
/*****************************************************************************/
void AbsTimerIsr(void)
{
	if(NULL != gFpAbsTimerIsrCbFunc)
	{
		gFpAbsTimerIsrCbFunc();
	}
//	LPIT_DRV_ClearInterruptFlagTimerChannels(INST_LPIT1, ABSTIMER_MAIN_TIMER_MASK);
}

