/*
 * service_proc.c
 *
 *  Created on: 2018. 12. 21.
 *      Author: Digen
 */

#include "includes.h"

#define T1day (3600U*24U)
#define T1hour (3600)
#define T1min (60)

#define BOOT_SIGNAL_TIME							100000 		// (100)*1000//(69)*1000//(55)*1000
#define ALIVE_SIGNAL_TIME						30000		//(30)*1000

static volatile uint32 service_1ms_tick = 0;
static volatile uint32 service_2ms_tick = 0;
static volatile uint32 service_5ms_tick = 0;
static volatile uint32 service_10ms_tick = 0;
static volatile uint32 service_100ms_tick = 0;
static volatile uint32 service_500ms_tick = 0;
static volatile uint32 service_1000ms_tick = 0;

static volatile uint32 key_adc_tick = 0;
static volatile uint32 key_encoder_tick = 0;
static volatile uint32 alive_timer_tick = 0;
static volatile uint32 pshold_timer_tick = 0;
static volatile uint32 lowbatter_CAN_timer_tick = 0;
static volatile uint32 check_encoder_timer_tick = 0;

#ifdef QFI_MODE_SEQ_CHANGE
static volatile uint32 check_update_excute_tick = 0;
#endif

uint8 rrm_tx_cmd[12];
QUE_t rrm_tx_cmd_q;

static void CheckKeyFunc(DEVICE *dev);
static void CheckEncoderFunc(DEVICE *dev);
static void CheckSOCFunc(DEVICE *dev);
static void CheckQFIModeFunc(DEVICE *dev);
static void CheckTimeCountFunc(DEVICE *dev);
static void RRM_Tx_Service(DEVICE *dev);
static void ExcuteBackLightCtl(DEVICE *dev);

//Cycle Event Message
uint8  IHU1OperFlag,IHU6OperFlag,IHU12OperFlag;
uint8  IHU1TimerCount,IHU2TimerCount,IHU6TimerCount,IHU12TimerCount;
uint8  u8SOCAlive = 1;
//Event Message
//IHU3,IHU4,IHU5,IHU7,IHU8,IHU10,IHU13
//Cycle Message
//IHU11,IHU14,IHU15
uint8  EnterSocFlag;
uint8  TimerCount;
uint8 au8DspInfo[8]={0};
extern uint8  StartDefaultSet;

#ifdef RECOVER_DSP_RAM_WRITE_FAIL
uint16 uDspRamWriteFailCnt=0;
#endif

void rrm_tx_cmd_Init(void)
{
	u8SOCAlive=1;
	init_q(&rrm_tx_cmd_q, &rrm_tx_cmd[0], sizeof(rrm_tx_cmd));
}

uint16_t check_rrm_tx_cmd(void)
{
	return q_data_size(&rrm_tx_cmd_q);
}

void Serivce_Proc(DEVICE *dev)
{
	//prohibit service at soc_update mode
	//static uint8_t a=0;
	//static uint8_t b=0;
	#if 1
	if(dev->r_System->soc_update == ON)
		return;
	#endif
	
	if(1<=GetElapsedTime(service_1ms_tick))
	{
		Serivce_1ms_Func(dev);
		S52_CAN_Comm_Proc(dev);   //MCU���ʹ��������S0C
		service_1ms_tick  = GetTickCount();
	}

	if(2<=GetElapsedTime(service_2ms_tick))
	{
		Serivce_2ms_Func(dev);
		service_2ms_tick  = GetTickCount();
	}

	if(5<=GetElapsedTime(service_5ms_tick))
	{
		Serivce_5ms_Func(dev);
		service_5ms_tick  = GetTickCount();
	}

	if(10<=GetElapsedTime(service_10ms_tick))
	{
		Serivce_10ms_Func(dev);
		service_10ms_tick  = GetTickCount();
	}

	if(100<=GetElapsedTime(service_100ms_tick))
	{
		Serivce_100ms_Func(dev);
		service_100ms_tick  = GetTickCount();
	}

	if(1000<=GetElapsedTime(service_1000ms_tick))
	{
		Serivce_1000ms_Func(dev);
		service_1000ms_tick  = GetTickCount();
	}	
			
}

void Serivce_1ms_Func(DEVICE *dev)
{
	CAN_Comm_Proc(dev);// CAN COMM
	Check_AccIgn_State(dev);
//move to 2ms	CheckEncoderFunc(dev);
}

void Serivce_2ms_Func(DEVICE *dev)
{
	//Key_Task();
	CheckEncoderFunc(dev);
	
	#ifdef EXTERNAL_AMP
	A2B_proc(dev);
	#endif

	#ifdef NEW_SCAN_ADC_KEY
	CheckKeyFunc(dev);
	#endif
	//	if((SYS_SLEEP_MODE_IDLE<dev->r_System->s_state)&&(dev->r_System->s_state!=SYS_BATTERY_LOW_MODE_ENTER))
	if(SYS_STANDBY_MODE_CHECK<=dev->r_System->s_state)
	{
		#ifdef CHECK_ALIVE_SIGNAL
		CheckSOCFunc(dev);  //kkm 20220714
		#endif
	}
}

void Serivce_5ms_Func(DEVICE *dev)
{
	//check acc/ign
	Diag_Task(dev);
//	Beep_Proc(dev);   5->10ms move
	#ifndef NEW_SCAN_ADC_KEY
	CheckKeyFunc(dev);
	#endif
}

void Serivce_10ms_Func(DEVICE *dev)
{
	static uint8 cnt=0;
	
	#ifdef  SHELL_MODE
	Shell_Task();										//SHELL TASK
	#endif
	Test_Task(dev);									// TEST TASK
//	Beep_Proc(dev);

	switch(cnt)
	{
		case 0:
			cnt = 1;
			break;
		case 1:
			cnt = 0; //20ms
			ExcuteBackLightCtl(dev);		  
			break;
	}
}

void Serivce_100ms_Func(DEVICE *dev)
{
	static uint8 prev_fault_dvr=0;
	static uint8 StartDefaultSetCount = 0;
	
	CheckQFIModeFunc(dev);
	RRM_Tx_Service(dev);

	//DPRINTF(DBG_MSG3,"Timer 100ms Test  \n\r");

    /*
	if(MCU_ACC_DET() == OFF)
	{
        Control_LCD_Backlight(OFF); 
	}

	if(MCU_ACC_DET() == ON)
	{
        Control_LCD_Backlight(ON); 
	}
	*/

	if(EnterSocFlag == TRUE)
	{
        TimerCount++;

		if(TimerCount == 1)
		{
            Uart_Send_Func(&CGW_VCU_P_5_123);
			//DPRINTF(DBG_MSG3,"Timer1 100ms Test  \n\r");
		}
        if(TimerCount == 2)
		{
		    Uart_Send_Func(&CGW_BCM_1_245);
			//DPRINTF(DBG_MSG3,"Timer2 100ms Test  \n\r");
		}
		if(TimerCount == 3)
		{
	        Uart_Send_Func(&CGW_EGS_1_215);
		}
		if(TimerCount == 4)
		{
	        Uart_Send_Func(&CGW_EPB_1_231);
		}
		if(TimerCount == 5)
		{
	        Uart_Send_Func(&CGW_ICM_1_271);
		}
		if(TimerCount == 6)
		{
		    Uart_Send_Func(&CGW_BCM_PEPS_1_291);
		}
		if(TimerCount == 7)
		{
		    Uart_Send_Func(&CGW_BSD_1_298);
		}
		if(TimerCount == 8)
		{
		    Uart_Send_Func(&CGW_VCU_3_4_28A);
			//DPRINTF(DBG_MSG3,"Timer8 100ms Test  \n\r");
		}
		if(TimerCount == 9)
		{
		    Uart_Send_Func(&ACCM_1_3D2);
			//DPRINTF(DBG_MSG3,"Timer9 100ms Test  \n\r");
		}
		if(TimerCount == 10)
		{
		    Uart_Send_Func(&CGW_BCM_4_324);
		}
		if(TimerCount == 11)
		{
		    Uart_Send_Func(&CGW_BCM_5_323);
		}
		if(TimerCount == 12)
		{
	        Uart_Send_Func(&BCM_6_457);
		}
		if(TimerCount == 13)
		{
            Uart_Send_Func(&BCM_TPMS_1_326);
		}
		if(TimerCount == 14)
		{
            Uart_Send_Func(&CGW_BCM_SCM_3_45C);
		}
		if(TimerCount == 15)
		{
		    Uart_Send_Func(&CGW_BMS_3_46B);
		}
		if(TimerCount == 16)
		{
            Uart_Send_Func(&CGW_BCM_SCM_1_45A);
		}
		if(TimerCount == 17)
		{
		    Uart_Send_Func(&CGW_RCM_1_328);
		}
		if(TimerCount == 18)
		{
		    Uart_Send_Func(&CGW_SAS_2_504);
		}
		if(TimerCount == 19)
		{
		    Uart_Send_Func(&CGW_VCU_3_6_313);
		}
		if(TimerCount == 20)
		{
		    Uart_Send_Func(&CGW_VCU_3_7_314);
		}
		if(TimerCount == 21)
		{
		    Uart_Send_Func(&CGW_VCU_B_5_315);
		}
		if(TimerCount == 22)
		{
		    Uart_Send_Func(&CGW_BCM_FM_458);
		}
		if(TimerCount == 23)
		{
		    Uart_Send_Func(&CGW_VCU_3_12_347);
		}
		if(TimerCount == 24)
		{
		    Uart_Send_Func(&CGW_BCM_TPMS_2_455);
		}
		if(TimerCount == 25)
		{
		    Uart_Send_Func(&CGW_SCM_R_45B);
		}
		if(TimerCount == 26)
		{
		    Uart_Send_Func(&TBOX_4_7_4A8);
		}
		if(TimerCount == 27)
		{
            Uart_Send_Func(&AVAS_1_4B1);
		}
		if(TimerCount == 28)
		{
		    Uart_Send_Func(&WCM_1_4BA);
		}
		if(TimerCount == 29)
		{
		    Uart_Send_Func(&ACCM_3_3D4);
		}
		if(TimerCount == 30)
		{
		    Uart_Send_Func(&CGW_BCM_EAC_1_3D8);
		}
		if(TimerCount == 31)
		{
		    Uart_Send_Func(&CGW_BCS_C_1_225);
		}
		if(TimerCount == 32)
		{
		    Uart_Send_Func(&CGW_BMS_3_25A);
		}
		if(TimerCount == 33)
		{
		    Uart_Send_Func(&CGW_BMS_9_355);
		}
		if(TimerCount == 34)
		{
		    Uart_Send_Func(&CGW_EPS_1_238);
		}
		if(TimerCount == 35)
		{
		    Uart_Send_Func(&CGW_FCM_10_3FC);
		}
		if(TimerCount == 36)
		{
		    Uart_Send_Func(&CGW_FCM_2_307);
		}
		if(TimerCount == 37)
		{
		    Uart_Send_Func(&CGW_FCM_5_4DD);
		}
		if(TimerCount == 38)
		{
		    Uart_Send_Func(&CGW_FCM_6_3DC);
		}
		if(TimerCount == 39)
		{
		    Uart_Send_Func(&CGW_FCM_7_3DE);
		}
		if(TimerCount == 40)
		{
		    Uart_Send_Func(&CGW_FCM_FRM_5_4E3);
		}
		if(TimerCount == 41)
		{
		    Uart_Send_Func(&CGW_FCM_FRM_6_387);
		}
		if(TimerCount == 42)
		{
		    Uart_Send_Func(&CGW_FCM_FRM_8_3ED);
		}
		if(TimerCount == 43)
		{
		    Uart_Send_Func(&CGW_FCM_FRM_9_3FA);
		}
		if(TimerCount == 44)
		{
		    Uart_Send_Func(&CGW_OBC_1_461);
		}
		if(TimerCount == 45)
		{
		    Uart_Send_Func(&CGW_PLG_1_32A);
		}
		if(TimerCount == 46)
		{
		    Uart_Send_Func(&ICM_2_3B1);
		}
		if(TimerCount == 47)
		{
		    Uart_Send_Func(&BCM_ALM_45D);
		}
		if(TimerCount == 48)
		{
		    Uart_Send_Func(&CGW_BCM_MFS_1_294);
		}		
		if(TimerCount >= 50)
		{
            TimerCount = 0;
		    EnterSocFlag = FALSE;
			//DPRINTF(DBG_MSG3,"Timer10 100ms Test  \n\r");
		}
	}

	// if(IHU1OperFlag == TRUE)
	// {
    //     ++IHU1TimerCount;
	// 	if(IHU1TimerCount >= 3)
	// 	{
	// 	    //IlPutTxIHU_EPB_ApplyReq(0);
    //         //IlPutTxIHU_EPB_ApplyReq_VD(0);
    //         IlPutTxIHU_LDW_EnableSwSts(0);
    //         IlPutTxIHU_LDW_WarningSoundEnableSwSts(0);
    //         IlPutTxIHU_TSR_EnableSwSts(0);
    //         IlPutTxIHU_FCW_EnableSts(0);
    //         IlPutTxIHU_LDW_SensitivitySet(0);
    //         IlPutTxIHU_FCW_SensitivitySet(0);
    //         IlPutTxIHU_PsngrAirbagDisableSwSts(0);
    //         IlPutTxIHU_PDC_EnableSwSts(0);

    //         IHU1TimerCount = 0;
	// 		IHU1OperFlag = FALSE;
    //     }
	// }
	//����Ĭ�����ü��书��
    //StartDefaultSet = 1;
	
	if(StartDefaultSet == 1)
	{
         if(++StartDefaultSetCount >= 10)
         {
             StartDefaultSetCount = 0;
             StartDefaultSet = 0;

		     IHU6OperFlag = TRUE;
		     IHU6TimerCount = 0;
             IlPutTxIHU_LightCtrlSwSts(2);    //����Ĭ��Ϊ��

			 IHU12OperFlag = TRUE;
			 IHU12TimerCount = 0;
			 IlPutTxIHU_AmbientLightEnableSwSts(2);    //��Χ��Ĭ��Ϊ��

		     //DPRINTF(DBG_MSG3," Set the IHU_LightCtrlSwSts Default Value! \n\r");
         }
	}
	//���Ӵ����������Ĭ�Ϲ���
	if(u8SOCAlive == 0)     //SOC�������ʧ��
	{
		 IHU6OperFlag = TRUE;
		 IHU6TimerCount = 0;
		 IlPutTxIHU_LightCtrlSwSts(2);    //����Ĭ��Ϊ��

		 IHU12OperFlag = TRUE;
		 IHU12TimerCount = 0;
		 IlPutTxIHU_AmbientLightEnableSwSts(2);    //��Χ��Ĭ��Ϊ��
	}
	
	// if(IHU6OperFlag == TRUE)
	// {
    //     ++IHU6TimerCount;
	// 	if(IHU6TimerCount >= 3)
	//     {
    //         IlPutTxIHU_AUTO_SwSts(0);
	//         IlPutTxIHU_AC_SwSts(0);
    //         IlPutTxIHU_PTC_SwSts(0);
    //         IlPutTxIHU_System_ON_OFF_SwSts(0);
    //         IlPutTxIHU_RearDfstSwSts(0);
    //         IlPutTxIHU_CirculationSwSts(0);
    //         IlPutTxIHU_BlowModeSet(0);
    //         IlPutTxIHU_TempSet(0);
    //         IlPutTxIHU_BlowerlevelSet(0);
    //         IlPutTxIHU_AutoBlowEnablaSw(0);
    //         IlPutTxIHU_AutoCleanEnablaSw(0);
    //         IlPutTxIHU_AC_MemoryModeEnablaSw(0);
    //         IlPutTxIHU_FrontDefrostSwSts(0);
    //         IlPutTxIHU_LightCtrlSwSts(0);
    //         IlPutTxIHU_RearFogLightSwSts(0);
    //         IlPutTxIHU_MirrorReversePositionStoreReq(0);
    //         IlPutTxIHU_MirrorCtrlReq_L(0);
    //         IlPutTxIHU_MirrorCtrlReq_R(0);
			
    //         IHU6TimerCount = 0;
	// 	    IHU6OperFlag = FALSE;
	//     }
	// }

	if(IHU12OperFlag == TRUE)
	{
        ++IHU12TimerCount;
		if(IHU12TimerCount >= 3)
        {
		    //IlPutTxIHU_MusicLoudness_120HZ(0);      Cycle message
            // IlPutTxIHU_AmbientLightColorSet(0);
            //IlPutTxIHU_MusicLoudness_250HZ(0);      Cycle message
            // IlPutTxIHU_AmbientLightBrightnessAdjust(0);
            //IlPutTxIHU_MusicLoudness_500HZ(0);      Cycle message
            //IlPutTxIHU_MusicLoudness_1000HZ(0);     Cycle message
            //IlPutTxIHU_MusicLoudness_1500HZ(0);     Cycle message
            //IlPutTxIHU_MusicLoudness_2000HZ(0);     Cycle message
            //IlPutTxIHU_MusicLoudness_6000HZ(0);     Cycle message
            // IlPutTxIHU_AmbientLightEnableSwSts(0);
            // IlPutTxIHU_AssociatedWithDriverModeSwSts(0);
            // IlPutTxIHU_AssociatedWithMusicModeSwSts(0);

            IHU12TimerCount = 0;
			IHU12OperFlag = FALSE;
        }
	}
	
//	if((MntFltGetLosComFalut_DVR()!=prev_fault_dvr)&&(dev->r_System->uart_ok == ON))
//	{
//		dvr_ctl_cmd.flag = ON;
//		dvr_ctl_cmd.trycnt = 0;
//		dvr_ctl_cmd.buf[0] = MntFltGetLosComFalut_DVR()+1;
//		prev_fault_dvr = MntFltGetLosComFalut_DVR();		// 1 is time out , 0 is normal
//		//		DPRINTF(DBG_MSG1,"dvr_ctl_cmd	%d \n\r", prev_fault_dvr);
//	}

	#ifdef QFI_MODE_SEQ_CHANGE
	if(dev->r_System->soc_update_excute == ON)
	{	
		if(5000<=GetElapsedTime(check_update_excute_tick ))
		{
			MCU_UPGRADE_SOC(OFF);
			DPRINTF(DBG_MSG3," gpio for soc update  \n\r");
			dev->r_System->soc_update_excute =	OFF;
		}
	}
	else
	{
		check_update_excute_tick = GetTickCount();
	}
	#endif

	//excute long key, system is reset
	if(SYS_SLEEP_MODE_IDLE<dev->r_System->s_state)
	{
		//power key is repeat mode when QFM mode is entered
		if(dev->r_System->power_key_status==POWER_KEY_REPEAT)
		{
			DPRINTF(DBG_ERR," SystemSoftwareReset 6 by key, dsp: %x\n\r",dev->r_System->power_key_status);
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			dev->r_System->power_key_status = POWER_KEY_IDLE;
			e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
			save_last_memory();
			RecordReset(6,NULL);
			delay_ms(100);
			SystemSoftwareReset();
		}
		#ifdef RECOVER_DSP_RAM_WRITE_FAIL
		if(100<=uDspRamWriteFailCnt)						//JANG_210828_1				10->25->100->150
		{
			DPRINTF(DBG_ERR," DSP_INIT_1 by dsp\n\r");
			au8DspInfo[0]=(uint8)uDspRamWriteFailCnt; //0x9C
			au8DspInfo[1]=0; //0x36
			au8DspInfo[1]|=(dev->r_System->can_wakeup ? 0x01 : 0);
			au8DspInfo[1]|=(dev->r_System->ccl_active ? 0x02 : 0);//1
			au8DspInfo[1]|=(dev->r_System->accign_state.state ? 0x04 : 0);//1
			au8DspInfo[1]|=(dev->r_System->lcd_ok     ? 0x08 : 0);
			au8DspInfo[1]|=(dev->r_System->blpwmenable     ? 0x10 : 0); //1
			au8DspInfo[1]|=(dev->r_System->pshold     ? 0x20 : 0);  //1
			au8DspInfo[1]|=(dev->r_System->check_high_voltage_dtc     ? 0x40 : 0);
			au8DspInfo[1]|=(dev->r_System->check_low_voltage_dtc     ? 0x80 : 0);
			au8DspInfo[2]=r_sonysound;//dev->r_Dsp->d_act_cnt;//0x01
			RecordReset(7,&au8DspInfo[0]);
			//delay_ms(100);
			//SystemSoftwareReset();
			uDspRamWriteFailCnt=0;
			dev->r_Dsp->d_state=DSP_INIT_1;
		}
		#endif
	}

	//LCD check 
	if((PWR_ON_14<=dev->r_Power->p_state)&&((HU_BL_REQ()==ON)||(dev->r_System->lcd_ok)))
	{
		//check lcd
		if(dev->r_System->s_state_lcd_state  !=dev->r_System->s_state_prev_lcd_state)
		{
			switch(dev->r_System->s_state_lcd_state)
			{
				case SYS_ACTIVE_MODE_IDLE:
					DPRINTF(DBG_ERR,"lcd control 	SYS_ACTIVE_MODE_IDLE %d\n\r",dev->r_System->s_state_prev_lcd_state);
					#ifdef LCD_REQUSET_FOR_MENGBO
					if((dev->r_System->s_state_prev_lcd_state != SYS_SCREEN_SAVE_MODE_IDLE)&&
						(dev->r_System->s_state_prev_lcd_state != SYS_15MIN_MODE_IDLE)&&(dev->r_System->s_state_prev_lcd_state != SYS_ACTIVE_MODE_IDLE))
						Control_LCD_Backlight_for_Mengbo(dev,ON);
					#else
						Control_LCD_Backlight(ON);			//LCD ON
					#endif
					dev->r_System->s_state_prev_lcd_state = dev->r_System->s_state_lcd_state;
					break;
				case SYS_SCREEN_SAVE_MODE_IDLE:
					DPRINTF(DBG_ERR,"lcd control 	SYS_SCREEN_SAVE_MODE_IDLE\n\r");
					#ifdef LCD_REQUSET_FOR_MENGBO
					if((dev->r_System->s_state_prev_lcd_state != SYS_SCREEN_SAVE_MODE_IDLE)&&
						(dev->r_System->s_state_prev_lcd_state != SYS_15MIN_MODE_IDLE)&&(dev->r_System->s_state_prev_lcd_state != SYS_ACTIVE_MODE_IDLE))
						Control_LCD_Backlight_for_Mengbo(dev,ON);
					#else
					Control_LCD_Backlight(ON);			//LCD ON
					#endif
					dev->r_System->s_state_prev_lcd_state = dev->r_System->s_state_lcd_state;
					break;
				case SYS_15MIN_MODE_IDLE:
					DPRINTF(DBG_ERR,"lcd control 	SYS_15MIN_MODE_IDLE\n\r");
					#ifdef LCD_REQUSET_FOR_MENGBO
					if((dev->r_System->s_state_prev_lcd_state != SYS_SCREEN_SAVE_MODE_IDLE)&&
						(dev->r_System->s_state_prev_lcd_state != SYS_15MIN_MODE_IDLE)&&(dev->r_System->s_state_prev_lcd_state != SYS_ACTIVE_MODE_IDLE))
						Control_LCD_Backlight_for_Mengbo(dev,ON);
					#else 
					Control_LCD_Backlight(ON);			//LCD ON
					#endif
					dev->r_System->s_state_prev_lcd_state = dev->r_System->s_state_lcd_state;
					break;
				case SYS_SCREENOFF_1_MODE_IDLE:
				case SYS_SCREENOFF_2_MODE_IDLE:
				case SYS_STANDBY_MODE_IDLE:
				case SYS_BATTERY_HIGH_MODE_IDLE:
				case SYS_BATTERY_6V_UNDER_MODE_IDLE:	
				case SYS_BATTERY_9V_UNDER_MODE_IDLE:
				case SYS_BATTERY_16V_OVER_MODE_IDLE:
					Control_LCD_Backlight(OFF); 		//LCD OFF
					FTM3_PWM_Duty(99);
					dev->r_System->s_state_prev_lcd_state = dev->r_System->s_state_lcd_state;
					break;
				case SYS_BATTERY_LOW_MODE_IDLE:
				default:
					break;
			}
		}
	}

}

void Serivce_1000ms_Func(DEVICE *dev)
{
	uint8 value=0;
	uint8 direct=0;
	static uint8 check_a2b = CHECK_STATUS_STEP_0;
		
	CheckTimeCountFunc(dev);
//	DPRINTF(DBG_ERR,"Serivce_1000ms_Func 	%d\n\r",Check_Batt_Value());

	if(dev->r_System->low_batter_status ==ON)
	{
		//5000sec is ok  ������ �������, disable�ϱ����� 5�� �ʿ�...
		#if 1
		if(5000<=GetElapsedTime(lowbatter_CAN_timer_tick))
		{
			dev->r_System->low_batter_status = OFF;
			#ifndef EMC_FOR_CAN_UNDER_6V
			dev->r_System->ccl_active = OFF;
			//SrvMcanComCtrl(SRVMCAN_RX_ALL_DISABLE_IDX);
			//SrvMcanComCtrl(SRVMCAN_TX_ALL_DISABLE_IDX);
			#endif
		}
		#endif
	}
	else
		lowbatter_CAN_timer_tick = GetTickCount();

	#ifdef INTERNAL_AMP
	#ifdef SVCD_ENABLE
	if((dev->r_System->svcd_speed !=dev->r_System->prev_svcd_speed)||(e2p_save_data.E2P_SVCD != dev->r_System->prev_svcd_e2p))
	{
		if(dev->r_System->svcd_speed<dev->r_System->prev_svcd_speed)
			direct = ON;  
		dev->r_System->svcd_step = Check_SVCD_Func(dev->r_System->svcd_speed, direct);
		dev->r_System->prev_svcd_speed =dev->r_System->svcd_speed;
		dev->r_System->prev_svcd_e2p = e2p_save_data.E2P_SVCD;
	}
	
	if(dev->r_System->svcd_step!= dev->r_System->pev_svcd_step)
	{
		#ifdef DSP_DOUBLE_QUE
		Excute_SVCD_Func(dev);
		#else
		Excute_SVCD_Func(dev->r_System->svcd_step);
		#endif
		dev->r_System->pev_svcd_step =dev->r_System->svcd_step;
	}
	#endif
	#endif
}


void RESET_MCU(DEVICE *dev)
{
	dev->r_System->power_key_status = POWER_KEY_IDLE;
	e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
	e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120
	e2p_sys_data.E2P_SONY= r_sonysound;
	save_last_memory();
	delay_ms(100);
	SystemSoftwareReset();
}

static void CheckKeyFunc(DEVICE *dev)
{
	if(dev->r_System->soc_update == ON)
		return;
	
	if(SYS_SLEEP_MODE_IDLE<dev->r_System->s_state)
	{
		Adc_Key_Parser0((uint32)POWER_KEY());	//  timer_callback

		#if 0  // operate for avm sw button
		if(dev->r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)
		{
			return;
		}
		#endif
		Process_ADC_key();
	}
}

static void CheckEncoderFunc(DEVICE *dev)
{
	uint8 encoder =0;
	#ifdef ENABLE_ENCODER_15MIN_MODE
	static uint8 pre_acc_det=0;
	#endif

	if(dev->r_System->soc_update == ON)
		return;

	#ifdef ENABLE_ENCODER_15MIN_MODE
	if(((SYS_ACTIVE_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_SCREENOFF_2_MODE_IDLE)))
		// ||((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE)))   //KHI 15MIN CONDITION DELETE
	{
		if(MCU_KNOB_B()==1) {encoder|=0x01;}
		if(MCU_KNOB_A()==1) {encoder|=0x02;}
	
		//add acc off->on encoder value clear 
		if((dev->r_System->accign_state.state&0x02)!=pre_acc_det)
		{
			DPRINTF(DBG_MSG1," CheckEncoderFunc IGN_DET %d\n\r",(dev->r_System->accign_state.state&0x02));
			pre_acc_det = (dev->r_System->accign_state.state&0x02);
			check_encoder_timer_tick = GetTickCount();
		}
		
		// when mode is changed and wait 1 sec
		if(GetElapsedTime(check_encoder_timer_tick)<=2000)
		{
//			DPRINTF(DBG_MSG1," encoder clear %d\n\r",GetElapsedTime(check_encoder_timer_tick));
			dev->r_LeftEncoder->encoder= 0x00;
			dev->r_LeftEncoder->oldstatus = 0x00;
			dev->r_LeftEncoder->laststatus = J_ENCODER_STOP;
			dev->r_LeftEncoder->direction=	J_ENCODER_STOP;
			dev->r_LeftEncoder->enable=1;
			dev->r_LeftEncoder->txed=FALSE;
			dev->r_LeftEncoder->ack=FALSE;
			dev->r_LeftEncoder->retry=0;
			dev->r_LeftEncoder->click=0;
		}

		EncoderProc1(&r_Encoder,encoder);
		ReadEncoder(dev);
	}
	else if((SYS_SCREEN_SAVE_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_SCREEN_SAVE_MODE_IDLE))
	{
	}
	else
		check_encoder_timer_tick = GetTickCount();
	#else
	if((SYS_ACTIVE_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state <= SYS_ACTIVE_MODE_IDLE))
	{
		if(MCU_KNOB_B()==1) {encoder|=0x01;}
		if(MCU_KNOB_A()==1) {encoder|=0x02;}

		EncoderProc1(&r_Encoder,encoder);
		ReadEncoder(dev);
	}
	#endif
}

static void CheckSOCFunc(DEVICE *dev)
{
	 static int ps_cnt=0;
	 static uint8 onoff=0;
	//check PS_hold
	// 1) PS_HOLD		MAIN_PWR-> 1sec high  2->4sec reset
	// 2) 100�� alive check 
	// 3) 100�� GPIO Control

	//enter update mdoe 
	if((dev->r_System->soc_update == ON)||(dev->r_System->alivesignal==OFF))
		return;
	
	switch(dev->r_System->soc_monitor)
	{
		case BOOT_SOC_BOOT_PS_HOLD:
			#if 1
			if(dev->r_System->pshold)			//ps hold
			{
				if(1<=GetElapsedTime(pshold_timer_tick ))
				{
					pshold_timer_tick = GetTickCount();
					alive_timer_tick = GetTickCount();
					if(CHECK_BOOT_PS_HOLD==ps_cnt)
					{
						ps_cnt++;

						if(PS_HOLD()==OFF)
						{  //check PS_HOLD
							DPRINTF(DBG_INFO,"reset  first pshold %dms\n\r",pshold_timer_tick);
							dev->r_Power->p_state = PWR_RESET_1;
							dev->r_System->pshold =OFF;
							dev->r_System->alivecleartime = OFF;
							dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
							if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)//JANG_210829
								dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
							ps_cnt=0;
							u8SOCAlive=0;
						}
						else
						{
							DPRINTF(DBG_INFO," first check  pshold ok %dms GPIO=%d PS_HOLD=%d\n\r",pshold_timer_tick,GPIO_CONTROL(),PS_HOLD());
						}
					}
					else if(CHECK_BOOT_PS_HOLD<ps_cnt)
					{
						if(PS_HOLD()==OFF)
						{  //check PS_HOLD
							DPRINTF(DBG_INFO,"reset  second pshold\n\r");
							dev->r_Power->p_state = PWR_RESET_1;
							dev->r_System->alivecleartime = OFF;
							dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
							if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)//JANG_210829
								dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
							u8SOCAlive=0;
						}
						else
						{
							DPRINTF(DBG_INFO,"second pshold ok %dms GPIO=%d PS_HOLD=%d\n\r",pshold_timer_tick,GPIO_CONTROL(),PS_HOLD());
							dev->r_System->soc_monitor = BOOT_SOC_BOOT_MONITOR;
						}
						dev->r_System->pshold =OFF;
						
						ps_cnt=0;					
					}
					else
						ps_cnt++;
										
				}
			}
			#else
			dev->r_System->soc_monitor = BOOT_SOC_BOOT_MONITOR;
			#endif
			break;

		case BOOT_SOC_BOOT_MONITOR:
		#if 1
			//// 100sec ->70sec-> 55sec
			if(BOOT_SIGNAL_TIME<=GetElapsedTime(alive_timer_tick ))
			{
				alive_timer_tick = GetTickCount();
				#if 1
				if(dev->r_System->alivecleartime==OFF)
				{
					//GPIO_03_FROM_MCU
					if(GPIO_CONTROL()==OFF)
					{
						//wait
						DPRINTF(DBG_INFO,"enter update mode %dms\n\r",alive_timer_tick);
					}
					else
					#if 0
					{
						DPRINTF(DBG_INFO,"boot ok   alive signal %d  GPIO_CONTROL %d \n\r",dev->r_System->alivecleartime,GPIO_CONTROL());
						dev->r_System->soc_monitor =BOOT_SOC_ALIVE_MONITOR;
					}
					#else
					{
						DPRINTF(DBG_INFO,"boot ok   alive signal %d  GPIO_CONTROL %d \n\r",dev->r_System->alivecleartime,GPIO_CONTROL());
						dev->r_Power->p_state = PWR_RESET_1;
						dev->r_System->pshold =OFF;
						dev->r_System->alivecleartime = OFF;
						if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)//JANG_210829
							dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
						dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
						#if 1
						#ifdef DSP_DOUBLE_QUE
						dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
						q2_en(&ak7739_cmd2_q, (uint8)DSP_CHANG_MODE_1,(uint8) dev->r_Dsp->d_audo_mode);
						#else
						q_en(&audio_mode_cmd_q, (uint8)AVNT_USECASE_AUDIOPATH_MEDIA);
						q_en(&ak7739_cmd_q, (uint8)DSP_CHANG_MODE_1);
						#endif
						#ifdef SEPERATE_VOLUME_TABLE
						mediachtype = CH_TYPE_MEDIA;
						#endif
						#ifdef CX62C_FUNCTION

						#endif

						#endif
						u8SOCAlive=0;
					}
					#endif
				}
				else
				{
					if(GPIO_CONTROL()==OFF)
					{
						DPRINTF(DBG_INFO,"reset  GPIO_CONTROL fail %d \n\r",GPIO_CONTROL());
						dev->r_Power->p_state = PWR_RESET_1;
						dev->r_System->pshold =OFF;
						dev->r_System->alivecleartime = OFF;
						if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)//JANG_210829
							dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
						dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
						#if 1
						#ifdef DSP_DOUBLE_QUE
						dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
						q2_en(&ak7739_cmd2_q, (uint8)DSP_CHANG_MODE_1,(uint8) dev->r_Dsp->d_audo_mode);
						#else
						q_en(&audio_mode_cmd_q, (uint8)AVNT_USECASE_AUDIOPATH_MEDIA);
						q_en(&ak7739_cmd_q, (uint8)DSP_CHANG_MODE_1);
						#endif
						#ifdef SEPERATE_VOLUME_TABLE
						mediachtype = CH_TYPE_MEDIA;
						#endif
						#ifdef CX62C_FUNCTION
//						CanIfSet_gCanIf_RRM_2_SourceStationMode(SOURCE_STATION_USB);
//						CanIfSet_gCanIf_RRM_2_RadioFrequanceMode(0x00);		//FM 0x01	, AM 0x02
						#endif

						#endif
						u8SOCAlive=0;
					}
					else
					{
						DPRINTF(DBG_INFO,"boot ok   alive signal %d  GPIO_CONTROL %d \n\r",dev->r_System->alivecleartime,GPIO_CONTROL());
						dev->r_System->soc_monitor =BOOT_SOC_ALIVE_MONITOR;
					}
				}
				#else
				dev->r_System->soc_monitor = BOOT_SOC_ALIVE_MONITOR;
				dev->r_System->alivecleartime = ON;		//alive clear
				#endif
			}

			//ps_hold is low for 700ms, system is reboot	//usb update is 20ms
			if((SYS_STANDBY_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE))
			{
				if(CHECK_PS_HOLD<=GetElapsedTime(pshold_timer_tick ))
				{
					if(PS_HOLD()==OFF)
					{
						if(onoff==OFF)
						{
							#ifdef DSP_DOUBLE_QUE
							dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
							q2_en(&ak7739_cmd2_q, (uint8)DSP_CHANG_MODE_1,(uint8) dev->r_Dsp->d_audo_mode);
							#else
							q_en(&audio_mode_cmd_q, (uint8)AVNT_USECASE_AUDIOPATH_MEDIA);
							q_en(&ak7739_cmd_q, (uint8)DSP_CHANG_MODE_1);
							#endif
							#ifdef SEPERATE_VOLUME_TABLE
							mediachtype = CH_TYPE_MEDIA;
							#endif
							#ifdef DSP_DOUBLE_QUE
							q2_en(&ak7739_cmd2_q, (uint8)DSP_MUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
							#else
							q_en(&ak7739_cmd_q, (uint8)DSP_MUTE_1);
							#endif
							#ifdef INTERNAL_AMP
							MCU_AMP_MUTE(ON); 					//SOUND OFF
							#endif
							onoff = ON;
						}
						dev->r_System->pshold =ON;
						DPRINTF(DBG_INFO,"PS_HOLD low in BOOT_SOC_BOOT_MONITOR \n\r");
					}
					else
					{
						//normal
						if(dev->r_System->pshold)
						{
							DPRINTF(DBG_INFO,"return BOOT_SOC_BOOT_PS_HOLD in BOOT_SOC_BOOT_MONITOR \n\r");
							dev->r_System->soc_monitor =BOOT_SOC_BOOT_PS_HOLD;
							dev->r_System->alivecleartime = OFF;		//alive clear
							dev->r_System->alivesignal = OFF;
							dev->r_System->soc_reboot =ON;
						}
						onoff = OFF;
					}
					pshold_timer_tick = GetTickCount();
				}
			}
			#else
			dev->r_System->soc_monitor =BOOT_SOC_ALIVE_MONITOR;
			#endif
			break;
		case BOOT_SOC_ALIVE_MONITOR:
			#if 1
			if((SYS_STANDBY_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE))
			{
				if(dev->r_System->alivecleartime)
				{
					dev->r_System->alivecleartime = OFF;		//alive clear
					alive_timer_tick = GetTickCount();
	//				DPRINTF(DBG_MSG1,"alive  clear %d\n\r",alive_timer_tick);
				}
				
				if((ALIVE_SIGNAL_TIME<=GetElapsedTime(alive_timer_tick ))&&(dev->r_System->alivesignal == ON))
				{
					#if 1
					DPRINTF(DBG_ERR,"don't receive alive command and reset system\n\r");
					dev->r_Power->p_state = PWR_RESET_1;
					dev->r_System->pshold =OFF;
					dev->r_System->alivecleartime = OFF;
					if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)//JANG_210829
						dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
					dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
					#if 1
					#ifdef DSP_DOUBLE_QUE
					dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
					q2_en(&ak7739_cmd2_q, (uint8)DSP_CHANG_MODE_1,(uint8) dev->r_Dsp->d_audo_mode);
					#else
					q_en(&audio_mode_cmd_q, (uint8)AVNT_USECASE_AUDIOPATH_MEDIA);
					q_en(&ak7739_cmd_q, (uint8)DSP_CHANG_MODE_1);
					#endif
					#ifdef SEPERATE_VOLUME_TABLE
					mediachtype = CH_TYPE_MEDIA;
					#endif
					#ifdef CX62C_FUNCTION
//					CanIfSet_gCanIf_RRM_2_SourceStationMode(SOURCE_STATION_USB);
//					CanIfSet_gCanIf_RRM_2_RadioFrequanceMode(0x00); 	//FM 0x01 , AM 0x02
					#endif

					#endif
					#else
					DPRINTF(DBG_MSG1,"don't receive alive command and reset system\n\r");
					delay_ms(2);
					SystemSoftwareReset();
					#endif
					alive_timer_tick = GetTickCount();
					u8SOCAlive=0;
				}
				
				//ps_hold is low for 700ms, system is reboot
				//usb update is 20ms
				if(CHECK_PS_HOLD<=GetElapsedTime(pshold_timer_tick ))
				{
					if(PS_HOLD()==OFF)
					{
						if(onoff==OFF)
						{
							#ifdef DSP_DOUBLE_QUE
							dev->r_Dsp->d_audo_mode = AVNT_USECASE_AUDIOPATH_MEDIA;
							q2_en(&ak7739_cmd2_q, (uint8)DSP_CHANG_MODE_1,(uint8) dev->r_Dsp->d_audo_mode);
							#else
							q_en(&audio_mode_cmd_q, (uint8)AVNT_USECASE_AUDIOPATH_MEDIA);
							q_en(&ak7739_cmd_q, (uint8)DSP_CHANG_MODE_1);
							#endif
							#ifdef SEPERATE_VOLUME_TABLE
							mediachtype = CH_TYPE_MEDIA;
							#endif
							#ifdef DSP_DOUBLE_QUE
							q2_en(&ak7739_cmd2_q, (uint8)DSP_MUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
							#else
							q_en(&ak7739_cmd_q, (uint8)DSP_MUTE_1);
							#endif
							#ifdef INTERNAL_AMP
							MCU_AMP_MUTE(ON); 					//SOUND OFF
							#endif
							onoff = ON;
						}
						dev->r_System->pshold =ON;
						DPRINTF(DBG_INFO,"PS_HOLD low In activmode \n\r");
					}
					else
					{
						//normal
						if(dev->r_System->pshold)
						{
							DPRINTF(DBG_INFO,"return BOOT_SOC_BOOT_PS_HOLD \n\r");
							dev->r_System->soc_monitor =BOOT_SOC_BOOT_PS_HOLD;
							dev->r_System->alivecleartime = OFF;		//alive clear
							dev->r_System->alivesignal = OFF;
							dev->r_System->soc_reboot =ON;
						}
						onoff = OFF;
					}
					pshold_timer_tick = GetTickCount();
				}
			}
			#endif
			break;
	}

}

#if 1 //def CX62C_FUNCTION
QFIModeType QFIModeState = QFI_MODE_IDLE;
#endif

static void CheckQFIModeFunc(DEVICE *dev)
{
	 static int check_mode_cnt=0;
	 static int prev_check_mode_cnt=0;

	//enter update mdoe &  check parking 
	if((dev->r_System->soc_update == ON)||(dev->r_System->gear_status !=1))
		return;

	switch(QFIModeState)
	{
		case QFI_MODE_IDLE:// 2) 3 button press
			//check enter update mode
			#ifdef CX62C_FUNCTION
			if(dev->r_System->soc_update_powerkey&&dev->r_System->soc_update_setkey&&(dev->r_System->accign_state.state&0x01))
			#else
			if(dev->r_System->soc_update_downkey&&dev->r_System->soc_update_powerkey&&dev->r_System->soc_update_setkey&&
				(dev->r_System->accign_state.state&0x01))
			#endif
				{
					QFIModeState = QFI_MODE_PROCESS_STEP1;
					DPRINTF(DBG_INFO,"enter QFI_MODE_PROCESS_STEP1\n\r");
				}
			break;
		case QFI_MODE_PROCESS_STEP1:	// change mode when acc is off
			if((dev->r_System->accign_state.state&0x01)==OFF)
			{
				check_mode_cnt=0;
				prev_check_mode_cnt=0;
				QFIModeState = QFI_MODE_PROCESS_STEP2;
				DPRINTF(DBG_INFO,"enter QFI_MODE_PROCESS_STEP2\n\r");
			}
			else
			{
				#ifdef CX62C_FUNCTION
				if((dev->r_System->soc_update_powerkey==OFF)||(dev->r_System->soc_update_setkey==OFF))
				#else
				if((dev->r_System->soc_update_powerkey==OFF)||(dev->r_System->soc_update_setkey==OFF)||
					(dev->r_System->soc_update_downkey==OFF))
				#endif
				{
					QFIModeState = QFI_MODE_IDLE;
				}
			}
			break;
		case QFI_MODE_PROCESS_STEP2:
			if((dev->r_System->accign_state.state&0x01)==OFF)
			{
				//increase counter
				check_mode_cnt++;
				prev_check_mode_cnt++;
			}
			else
			{
				//finish counter
				check_mode_cnt =0;
				QFIModeState = QFI_MODE_PROCESS_END;
				DPRINTF(DBG_INFO,"enter QFI_MODE_PROCESS_END\n\r");
			}

			//when button is release
			#ifdef CX62C_FUNCTION
			if((dev->r_System->soc_update_powerkey==OFF)||(dev->r_System->soc_update_setkey==OFF))
			#else
			if((dev->r_System->soc_update_powerkey==OFF)||(dev->r_System->soc_update_setkey==OFF)||
				(dev->r_System->soc_update_downkey==OFF))
			#endif
			{
				prev_check_mode_cnt =check_mode_cnt = 0;
				QFIModeState = QFI_MODE_IDLE;
			}
			break;
		case QFI_MODE_PROCESS_END:
			if(check_mode_cnt !=prev_check_mode_cnt)
			{
				//check compare  500ms~3000ms
				if((5<=(prev_check_mode_cnt -check_mode_cnt))&&((prev_check_mode_cnt -check_mode_cnt)<=30))
				{
					QFIModeState = QFI_MODE_ENTER;
				}
				else
				{
					//exit update mode
					prev_check_mode_cnt =check_mode_cnt = 0;
					QFIModeState = QFI_MODE_IDLE;
					DPRINTF(DBG_INFO,"exit QFI_MODE\n\r");
				}
			}
			else
			{
				//exit update mode
					prev_check_mode_cnt =check_mode_cnt = 0;
					QFIModeState = QFI_MODE_IDLE;
					DPRINTF(DBG_INFO,"exit QFI_MODE\n\r");
			}
			break;
		case QFI_MODE_ENTER:
			//enter update mdoe
			//reboot power
			//deinit can
			// no action
			dev->r_System->soc_update = ON; 
			//WL0714
			CclRel_Com_Request0();	//start ccl
			dev->r_System->ccl_active = OFF;
			dev->r_Tuner->t_state = TUNER_IDLE;
			dev->r_Dsp->d_state = DSP_IDLE;
			prev_check_mode_cnt =check_mode_cnt = 0;
			DPRINTF(DBG_INFO,"enter soc update mode\n\r");
			dev->r_Power->p_state = PWR_RESET_1;
			dev->r_System->pshold =OFF;
			dev->r_System->alivecleartime = OFF;
			dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
			if(dev->r_System->s_state != SYS_BATTERY_LOW_MODE_IDLE)
			dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
			MCU_UPGRADE_SOC(ON);
			delay_ms(100);	
			QFIModeState = QFI_MODE_IDLE;	
			break;
	}

}

static void CheckTimeCountFunc(DEVICE *dev)
{
	uint8 monthday = 0;
	uint32 tmp32 = 0;
	uint32 tmp32_1 = 0;
	uint32 tmp32_2 = 0;
	
	if(dev->r_System->gps_time.enable)
	{
		tmp32_1 = dev->r_System->gps_time.counter ++;
		tmp32 = (tmp32_1%T1min);// sec
//		CanIfSet_gCanIf_RRM_7_CurrentTimeSecond(tmp32);
		tmp32 = (dev->r_System->gps_time.min+tmp32_1/T1min)%60;
		tmp32_2 = (dev->r_System->gps_time.min+tmp32_1/T1min);			// total min
//		tmp32 = (dev->r_System->gps_time.hour+(tmp32_2/60))%24;
		tmp32_2 = (dev->r_System->gps_time.hour+(tmp32_2/60));			//total hour
//		CanIfSet_gCanIf_RRM_7_CurrentTimeHour(tmp32);
		// 	1 month 	: 	31
		//	2 month	:	29,28
		// 	3 month	: 	31
		// 	4 month 	: 	30
		//	5 month	:	31
		// 	6 month	: 	30
		// 	7 month 	: 	31
		//	8 month	:	31
		// 	9 month	: 	30
		// 	10 month 	: 	31
		//	11 month	:	30
		// 	12 month	: 	31
		if((dev->r_System->gps_time.month==1)||(dev->r_System->gps_time.month==3)||(dev->r_System->gps_time.month==5)||
			(dev->r_System->gps_time.month==7)||(dev->r_System->gps_time.month==8)||(dev->r_System->gps_time.month==10)||
			(dev->r_System->gps_time.month==12))
		{
				tmp32 = (dev->r_System->gps_time.day+(tmp32_2/24))%32;
				tmp32_2 = (dev->r_System->gps_time.day+(tmp32_2/24));			//total day
	//			CanIfSet_gCanIf_RRM_7_CurrentTimeDay(tmp32);
				tmp32 = (dev->r_System->gps_time.month+(tmp32_2/32))%13;
				tmp32_2 =(dev->r_System->gps_time.month+(tmp32_2/32));		//total month
	//			CanIfSet_gCanIf_RRM_7_CurrentTimeMonth(tmp32);
				tmp32 = (dev->r_System->gps_time.year+(tmp32_2/13));
//				CanIfSet_gCanIf_RRM_7_CurrentTimeYear(tmp32);						//year
		}
		else if((dev->r_System->gps_time.month==4)||(dev->r_System->gps_time.month==6)||
			(dev->r_System->gps_time.month==9)||(dev->r_System->gps_time.month==11))
		{
			tmp32 = (dev->r_System->gps_time.day+(tmp32_2/24))%31;
			tmp32_2 = (dev->r_System->gps_time.day+(tmp32_2/24));			//total day
	//		CanIfSet_gCanIf_RRM_7_CurrentTimeDay(tmp32);
			tmp32 = (dev->r_System->gps_time.month+(tmp32_2/31))%13;
			tmp32_2 =(dev->r_System->gps_time.month+(tmp32_2/31));		//total month
//			CanIfSet_gCanIf_RRM_7_CurrentTimeMonth(tmp32);
			tmp32 = (dev->r_System->gps_time.year+(tmp32_2/13));
//			CanIfSet_gCanIf_RRM_7_CurrentTimeYear(tmp32);						//year
		}
		else if(dev->r_System->gps_time.month==2)
		{
			if (((dev->r_System->gps_time.year % 4 == 0) && (dev->r_System->gps_time.year % 100 != 0)) || (dev->r_System->gps_time.year % 400 == 0))
				monthday = 29;
			else
				monthday =28;
			tmp32 = (dev->r_System->gps_time.day+(tmp32_2/24))%(monthday+1);
			tmp32_2 = (dev->r_System->gps_time.day+(tmp32_2/24));			//total day
	//		CanIfSet_gCanIf_RRM_7_CurrentTimeDay(tmp32);
			tmp32 = (dev->r_System->gps_time.month+(tmp32_2/(monthday+1)))%13;
			tmp32_2 =(dev->r_System->gps_time.month+(tmp32_2/(monthday+1)));		//total month
	//		CanIfSet_gCanIf_RRM_7_CurrentTimeMonth(tmp32);
			tmp32 = (dev->r_System->gps_time.year+(tmp32_2/13));
	//		CanIfSet_gCanIf_RRM_7_CurrentTimeYear(tmp32);						//year
		}
	}
}

static void RRM_Tx_Service(DEVICE *dev)
{
 	static uint8 tx_status=CHECK_STATUS_STEP_0;
	uint8 tmp=0;
	
	if((check_rrm_tx_cmd() !=0)&&(CHECK_STATUS_STEP_0==tx_status))
	{
		tx_status = CHECK_STATUS_STEP_1;
		tmp= q_de(&rrm_tx_cmd_q) ;
	}

	switch(tx_status)
	{
		case CHECK_STATUS_STEP_1:
			tx_status = CHECK_STATUS_STEP_2;
			break;
		case CHECK_STATUS_STEP_2:
			tx_status = CHECK_STATUS_STEP_3;
			break;
		case CHECK_STATUS_STEP_3:
			tx_status = CHECK_STATUS_STEP_4;
			break;
		case CHECK_STATUS_STEP_4:
			tx_status = CHECK_STATUS_STEP_5;
			break;
		case CHECK_STATUS_STEP_5:
			tx_status = CHECK_STATUS_STEP_6;
			break;
		case CHECK_STATUS_STEP_6:
			tx_status = CHECK_STATUS_STEP_0;
			break;
		default:
			break;
	}

}

static void ExcuteBackLightCtl(DEVICE *dev)
{
	uint8 tmp=0;
	uint8 tmp1=0;
	uint8 step=0;
	
	if(dev->r_System->lcd_control.enable==ON)
	{
		tmp = (dev->r_System->lcd_control.gotovalue /25);
		dev->r_System->lcd_control.counter++;

		if(tmp ==0)
			step=1;
		else
			step= tmp+1;
//JANG_211203
		if(dev->r_System->lcd_control.direct==1)
		{
			if(LCD_BL_STEP1<=(dev->r_System->lcd_control.startvalue+(step*dev->r_System->lcd_control.counter)))
			{
				tmp1 =LCD_BL_STEP1;
			}
			else
				tmp1 =dev->r_System->lcd_control.startvalue+(step*dev->r_System->lcd_control.counter);
			
			FTM3_PWM_Duty(tmp1);
			if((tmp1==LCD_BL_STEP1)&&(dev->r_System->lcd_control.onoff))
			{
				Control_LCD_Backlight(OFF);
				FTM3_PWM_Duty(99);//JANG_211203
				dev->r_System->lcd_control.onoff = OFF;
			}
		}
		else if(dev->r_System->lcd_control.direct==2)
		{
			FTM3_PWM_Duty(dev->r_System->lcd_control.startvalue);
		}
		else
		{
			FTM3_PWM_Duty(dev->r_System->lcd_control.startvalue-(step*dev->r_System->lcd_control.counter));
		}

		if((dev->r_System->lcd_control.counter == 25)||((dev->r_System->lcd_control.gotovalue)-(step*dev->r_System->lcd_control.counter)<=0))
		{
			/* wl20220826 when BL = 3 and tmp1 = 75 or BL = 8 and tmp1 = 25,screen flickering */
            if((tmp1 == 75u)
              ||(((dev->r_System->lcd_control.startvalue -(step*dev->r_System->lcd_control.counter)) == 75u) && (dev->r_System->lcd_control.direct == 0u))
              ||(((dev->r_System->lcd_control.startvalue +(step*dev->r_System->lcd_control.counter)) == 75u) && (dev->r_System->lcd_control.direct == 1u)))
            {
                    /* Set value 77 to avoid screen flickering */
                    FTM3_PWM_Duty(77u);
            }
            else if((tmp1 == 25u)
                 || (((dev->r_System->lcd_control.startvalue -(step*dev->r_System->lcd_control.counter)) == 25u) && (dev->r_System->lcd_control.direct == 0u))
                 || (((dev->r_System->lcd_control.startvalue +(step*dev->r_System->lcd_control.counter)) == 25u) && (dev->r_System->lcd_control.direct == 1u)))
            {
                    /* Set value 27 to avoid screen flickering */
                    FTM3_PWM_Duty(27u);
            }
            else
            {
                    /* Do nothing */
            }
			dev->r_System->lcd_control.counter=0;
			dev->r_System->lcd_control.enable =OFF;
			dev->r_System->lcd_control.onoff = OFF;
//					DPRINTF(DBG_MSG3,"%s LCD clear %d %d\n\r",__FUNCTION__,(dev->r_System->lcd_control.gotovalue),(step*dev->r_System->lcd_control.counter));
		}
	}
	else
	{
		//<<JANG_210901_1
		if(dev->r_System->lcd_control.rev_ctl)
		{
			dev->r_System->lcd_control.rev_ctl = OFF;
			dev->r_System->lcd_control.enable = ON;
		}
		//>>
	}

}

#ifdef SVCD_ENABLE
SVCD_STEP_TYPE Check_SVCD_Func(uint8 speed, uint8 direct)
{
	uint8 ret=0,tmp=0;
	
	//decrease 
	if(direct==ON)
		tmp=5;
		
	#ifdef CX62C_FUNCTION
	switch(e2p_save_data.E2P_SVCD)
	{
		case 1: 		//OFF
			ret = SVCD_STEP_IDEL;
			break;
		case 2:			//LOW
			if(speed<(SVCD_SET_LOW_1-tmp))
				ret = SVCD_STEP_IDEL;
			else if(speed<(SVCD_SET_LOW_2-tmp))
				ret = SVCD_STEP_1;
			else if(speed<(SVCD_SET_LOW_3-tmp))
				ret = SVCD_STEP_2;
			else if(speed<(SVCD_SET_LOW_4-tmp))
				ret = SVCD_STEP_3;
			else if(speed<(SVCD_SET_LOW_5-tmp))
				ret = SVCD_STEP_4;
			else if(speed<(SVCD_SET_LOW_6-tmp))
				ret = SVCD_STEP_5;
			else
				ret = SVCD_STEP_6;
			break;
		case 3:			//MIDDLE
			if(speed<(SVCD_SET_MID_1-tmp))
				ret = SVCD_STEP_IDEL;
			else if(speed<(SVCD_SET_MID_2-tmp))
				ret = SVCD_STEP_1;
			else if(speed<(SVCD_SET_MID_3-tmp))
				ret = SVCD_STEP_2;
			else if(speed<(SVCD_SET_MID_4-tmp))
				ret = SVCD_STEP_3;
			else if(speed<(SVCD_SET_MID_5-tmp))
				ret = SVCD_STEP_4;
			else if(speed<(SVCD_SET_MID_6-tmp))
				ret = SVCD_STEP_5;
			else if(speed<(SVCD_SET_MID_7-tmp))
				ret = SVCD_STEP_6;
			else if(speed<(SVCD_SET_MID_8-tmp))
				ret = SVCD_STEP_7;
			else if(speed<(SVCD_SET_MID_9-tmp))
				ret = SVCD_STEP_8;
			else if(speed<(SVCD_SET_MID_10-tmp))
				ret = SVCD_STEP_9;
			else if(speed<(SVCD_SET_MID_11-tmp))
				ret = SVCD_STEP_10;
			else if(speed<(SVCD_SET_MID_12-tmp))
				ret = SVCD_STEP_11;
			else if(speed<(SVCD_SET_MID_13-tmp))
				ret = SVCD_STEP_12;
			else if(speed<(SVCD_SET_MID_14-tmp))
				ret = SVCD_STEP_13;
			else
				ret = SVCD_STEP_14;
			break;
		case 4:			//HIGH
			if(speed<(SVCD_SET_HIGH_1-tmp))
				ret = SVCD_STEP_IDEL;
			else if(speed<(SVCD_SET_HIGH_2-tmp))
				ret = SVCD_STEP_1;
			else if(speed<(SVCD_SET_HIGH_3-tmp))
				ret = SVCD_STEP_2;
			else if(speed<(SVCD_SET_HIGH_4-tmp))
				ret = SVCD_STEP_3;
			else if(speed<(SVCD_SET_HIGH_5-tmp))
				ret = SVCD_STEP_4;
			else if(speed<(SVCD_SET_HIGH_6-tmp))
				ret = SVCD_STEP_5;
			else if(speed<(SVCD_SET_HIGH_7-tmp))
				ret = SVCD_STEP_6;
			else if(speed<(SVCD_SET_HIGH_8-tmp))
				ret = SVCD_STEP_7;
			else if(speed<(SVCD_SET_HIGH_9-tmp))
				ret = SVCD_STEP_8;
			else if(speed<(SVCD_SET_HIGH_10-tmp))
				ret = SVCD_STEP_9;
			else if(speed<(SVCD_SET_HIGH_11-tmp))
				ret = SVCD_STEP_10;
			else if(speed<(SVCD_SET_HIGH_12-tmp))
				ret = SVCD_STEP_11;
			else if(speed<(SVCD_SET_HIGH_13-tmp))
				ret = SVCD_STEP_12;
			else if(speed<(SVCD_SET_HIGH_14-tmp))
				ret = SVCD_STEP_13;
			else
				ret = SVCD_STEP_14;
			break;

			break;
		default:
			ret = SVCD_STEP_IDEL;
			break;
	}
	#else
	switch(e2p_save_data.E2P_SVCD)
	{
		case 1: 		//OFF
			ret = SVCD_STEP_IDEL;
			break;
		case 2:			//LOW
			if(speed<50)
				ret = SVCD_STEP_IDEL;
			else if(speed<93)
				ret = SVCD_STEP_1;
			else if(speed<136)
				ret = SVCD_STEP_2;
			else if(speed<177)
				ret = SVCD_STEP_3;
			else
				ret = SVCD_STEP_4;
			break;
		case 3:			//MIDDLE
			if(speed<50)
				ret = SVCD_STEP_IDEL;
			else if(speed<78)
				ret = SVCD_STEP_1;
			else if(speed<106)
				ret = SVCD_STEP_2;
			else if(speed<136)
				ret = SVCD_STEP_3;
			else if(speed<164)
				ret = SVCD_STEP_4;
			else if(speed<192)
				ret = SVCD_STEP_5;
			else
				ret = SVCD_STEP_6;
			break;
		case 4:			//HIGH
			if(speed<50)
				ret = SVCD_STEP_IDEL;
			else if(speed<65)
				ret = SVCD_STEP_1;
			else if(speed<78)
				ret = SVCD_STEP_2;
			else if(speed<93)
				ret = SVCD_STEP_3;
			else if(speed<106)
				ret = SVCD_STEP_4;
			else if(speed<121)
				ret = SVCD_STEP_5;
			else if(speed<136)
				ret = SVCD_STEP_6;
			else if(speed<149)
				ret = SVCD_STEP_7;
			else if(speed<164)
				ret = SVCD_STEP_8;
			else if(speed<177)
				ret = SVCD_STEP_9;
			else if(speed<192)
				ret = SVCD_STEP_10;
			else
				ret = SVCD_STEP_11;
			break;
		default:
			ret = SVCD_STEP_IDEL;
			break;
	}
	#endif
	
	return (SVCD_STEP_TYPE)ret;
}

#ifdef DSP_DOUBLE_QUE
void Excute_SVCD_Func(DEVICE *dev)
{
	uint8 step = dev->r_System->svcd_step;
	
	DPRINTF(DBG_MSG1,"%s step:%d \n\r",__FUNCTION__, step);
	switch(step)
	{
		case SVCD_STEP_IDEL:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD0,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_1: //SVCD
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD1,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_2:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD2,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_3:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD3,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_4:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD4,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_5:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD6,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_6:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD6,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_7:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD7,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_8:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD8,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_9:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD9,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_10:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD10,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_11:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD11,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_12:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD12,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_13:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD13,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		case SVCD_STEP_14:
			q2_en(&ak7739_cmd2_q, (uint8)DSP_SVCD14,(uint8) dev->r_Dsp->d_audo_mode);
			break;
		default:
			break;
	}
}

#else

void Excute_SVCD_Func(uint8 step)
{
	DPRINTF(DBG_MSG1,"%s step:%d \n\r",__FUNCTION__, step);
	switch(step)
	{
		case SVCD_STEP_IDEL:	
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD0);
			break;
		case SVCD_STEP_1:	//SVCD
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD1);
			break;
		case SVCD_STEP_2:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD2);
			break;
		case SVCD_STEP_3:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD3);
			break;
		case SVCD_STEP_4:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD4);
			break;
		case SVCD_STEP_5:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD5);
			break;
		case SVCD_STEP_6:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD6);
			break;
		case SVCD_STEP_7:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD7);
			break;
		case SVCD_STEP_8:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD8);
			break;
		case SVCD_STEP_9:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD9);
			break;
		case SVCD_STEP_10:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD10);
			break;
		case SVCD_STEP_11:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD11);
			break;
		case SVCD_STEP_12:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD12);
			break;
		case SVCD_STEP_13:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD13);
			break;
		case SVCD_STEP_14:
			q_en(&ak7739_cmd_q, (uint8)DSP_SVCD14);
			break;
		default:
			break;
	}
}
#endif

#endif


