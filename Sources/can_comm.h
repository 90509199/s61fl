/*
 * can_comm.h
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#ifndef CAN_COMM_H_
#define CAN_COMM_H_

#define	TP_SINGLE_FRAME										(0x00u)
#define	TP_FIRST_FRAME											(0x01u)
#define TP_CONTINUE_FRAME									(0x02u)
#define TP_FLOW_CONTROL_FRAME						(0x03u)


#define TP_PCI_TYPE_0												(0x00u)
#define TP_PCI_TYPE_3												(0x03u)

#define CLU_CONNECT_REQ										(0x01u)
#define CLU_CLEAR_DISPLAY									(0x03u)


#define	TP_CLU_ON													(0x01u)
#define	TP_CLU_OFF													(0x02u)


#if 1	//JANG_211116
extern uint16	prev_seat_status;
extern uint8	prev_gear_status;
extern uint32	prev_angle_status;
extern uint16	prev_speed_status;
extern uint16	prev_dte_status;
extern uint16	prev_fuellevel_status_0;
//	extern uint8	prev_fuellevel_status_1;
extern uint8	prev_dvr_status;
extern uint8	prev_seatbelt_status;
extern uint8	prev_bcm1_status;
extern uint32	prev_bcm2_status_0;
extern uint16	prev_bcm2_status_1;
extern uint8	prev_bcm3_status;
extern uint16	prev_bcm4_status;
extern uint8	prev_bsd1_status;
extern uint8	prev_turnlight_status;
extern uint32	prev_odo_meter;
extern uint8	prev_wpc;
extern uint32	prev_breath_status;
extern uint8	prev_tbox_alarm;
extern uint8	prev_drivemode;
extern uint32	prev_enginespeed;
extern uint8	prev_ctl;
extern uint8	prev_rollingcount;
extern uint8	prev_mfs_heart;
extern uint8	prev_chime;
extern uint8	prev_frm3;
extern uint32	prev_abs_eps5_0;
extern uint32	prev_abs_eps5_1;
extern uint32	prev_pm25_0;
extern uint16	prev_pm25_1;
extern uint16	prev_avm2;
extern uint16	prev_fcm2;
extern uint32	prev_cem_ipm_0;
extern uint8	prev_cem_ipm_1;
extern uint8	prev_icm4;
#ifdef CX62C_FUNCTION
extern uint32	prev_icm3_0;
extern uint32	prev_icm3_1;
extern uint32	prev_icm4_0;
#endif
extern uint16	prev_plg;
extern uint16	prev_cgw_abm;
extern uint32	prev_afu;
extern uint8 	prev_avm_apa2;
extern uint8 	prev_avm_apa3;
extern uint8 	prev_cem_peps;
extern uint32	prev_amp1;
#endif


void CAN_Comm_Proc(DEVICE *dev);
void CAN_Init_Comm(void);	//JANG_211116

#ifdef CX62C_FUNCTION
extern QUE_t tpdvd_cmd_q;

#if 0
typedef enum{
	IHU_DTC_2_5AD_IDX = 0,   /*  0  */
	IHU_DTC_1_5AC_IDX,
	IHU_DVR_1_3CA_IDX,
	IHU_13_3BD_IDX,
	IHU_12_3BC_IDX,
	IHU_11_3BB_IDX,       /*  5  */
	IHU_10_3BA_IDX,
	IHU_8_3B8_IDX,
	IHU_7_3B7_IDX,
	IHU_6_3B6_IDX,
	IHU_5_3B5_IDX,      /*  10  */
	IHU_4_3B4_IDX,
	IHU_3_3B3_IDX,
	IHU_15_3AD_IDX,
	IHU_14_3AC_IDX,
	IHU_2_3AB_IDX,   /*  15  */
	IHU_1_3AA_IDX,
	IHU_ICM_1_2E5_IDX,
}eSrvMcanTxMsg;

typedef enum{
	CGW_ASA_2_504_IDX = 0, /*  0  */
	WCM_1_4BA_IDX,
	AVAS_1_4B1_IDX,
	TBOX_4_7_4A8_IDX,
	CGW_BMS_3_46B_IDX,
	CGW_BMS_1_469_IDX,     /*  5  */
	CGW_SCM_R_45B_IDX,
	SCM_L_45A_IDX,
	BCM_6_457_IDX,
	CGW_BCM_TPMS_2_455_IDX,
	ACCM_1_3D2_IDX,     /*  10  */
	DVR_1_3A8_IDX,
	CGW_VCU_3_12_347_IDX,
	CGW_FCM_4_331_IDX,
	CGW_RCM_1_328_IDX,
	CGW_BCM_MFS_1_326_IDX,         /*  15  */
	CGW_BCM_4_324_IDX,
	CGW_BCM_5_323_IDX,
	CGW_VCU_B_5_315_IDX,
	CGW_VCU_3_7_314_IDX,
	CGW_VCU_3_6_313_IDX,    /*  20  */
	CGW_ICM_IHU_1_2E6_IDX,
	CGW_BSD_1_298_IDX,
	MFS_1_294_IDX,
	CGW_BCM_PEPS_1_291_IDX,
	CGW_VCU_3_4_28A_IDX,        /*  25  */
	CGW_ICM_1_271_IDX,
	CGW_BCM_1_245_IDX,
	CGW_ABM_1_234_IDX,
	CGW_EPB_1_231_IDX,
	CGW_EGS_1_215_IDX,      /*  30  */
	CGW_VCU_P_5_123_IDX,
}eSrvMcanRxMsg;

typedef enum{
	SRVMCAN_RX_ALL_ENABLE_IDX = 0,
	SRVMCAN_RX_NM_ENABLE_IDX,
	SRVMCAN_RX_NORMAL_ENABLE_IDX,
	SRVMCAN_RX_ALL_DISABLE_IDX,
	SRVMCAN_RX_NM_DISABLE_IDX,
	SRVMCAN_RX_NORMAL_DISABLE_IDX,
	SRVMCAN_TX_ALL_ENABLE_IDX,
	SRVMCAN_TX_NM_ENABLE_IDX,
	SRVMCAN_TX_NORMAL_ENABLE_IDX,
	SRVMCAN_TX_ALL_DISABLE_IDX,
	SRVMCAN_TX_NM_DISABLE_IDX,
	SRVMCAN_TX_NORMAL_DISABLE_IDX,
	SRVMCAN_COMCTRL_MAX
}eSrvMcanComCtrl;

typedef enum{
    CAN_ERROR_ACTIVE_STS,
    CAN_ERROR_PASSIVE_STS,
    CAN_BUS_OFF_STS,
    CAN_NOT_USED,
    CAN_ERROR_STS_MAX
}eCanControllerSts;

typedef enum{
    CAN_TRANS_STANDBY_STS = 0,
    CAN_TRANS_NORMAL_STS,
    CAN_TRANS_STS_MAX
}eCanTransCtrlSts;



typedef struct{
	uint32 FLAG_123_IDX:1;
	uint32 FLAG_215_IDX:1;
	uint32 FLAG_231_IDX:1;
	uint32 FLAG_234_IDX:1;
	uint32 FLAG_245_IDX:1;

	uint32 FLAG_271_IDX:1;
	uint32 FLAG_28A_IDX:1;
	uint32 FLAG_291_IDX:1;
	uint32 FLAG_294_IDX:1;
	uint32 FLAG_298_IDX:1;

	uint32 FLAG_2E6_IDX:1;
	uint32 FLAG_313_IDX:1;
	uint32 FLAG_314_IDX:1;
	uint32 FLAG_315_IDX:1;
	uint32 FLAG_323_IDX:1;

	uint32 FLAG_324_IDX:1;
	uint32 FLAG_326_IDX:1;
	uint32 FLAG_328_IDX:1;
	uint32 FLAG_331_IDX:1;
	uint32 FLAG_347_IDX:1;

	uint32 FLAG_3A8_IDX:1;
	uint32 FLAG_3D2_IDX:1;
	uint32 FLAG_455_IDX:1;
	uint32 FLAG_457_IDX:1;
	uint32 FLAG_45A_IDX:1;

	uint32 FLAG_45B_IDX:1;
	uint32 FLAG_469_IDX:1;
	uint32 FLAG_46B_IDX:1;
	uint32 FLAG_4A8_IDX:1;
	uint32 FLAG_4B1_IDX:1;

	uint32 FLAG_4BA_IDX:1;
	uint32 FLAG_504_IDX:1;
}tCAN_RX_FLAG;
#endif

void CAN_COMM_10MS_123(void);
// void CAN_COMM_10MS_294(void);
void CAN_COMM_10MS_294(void);

void CAN_COMM_20MS_245(void);
void CAN_COMM_20MS_215(void);
void CAN_COMM_20MS_231(void);
void CAN_COMM_20MS_271(void);
void CAN_COMM_20MS_225(void);
void CAN_COMM_20MS_25A(void);
void CAN_COMM_20MS_238(void);

void CAN_COMM_50MS_291(void);
void CAN_COMM_50MS_298(void);
void CAN_COMM_50MS_28A(void);
void CAN_COMM_50MS_3FC(void);
void CAN_COMM_50MS_307(void);
void CAN_COMM_50MS_3DC(void);
void CAN_COMM_50MS_3DE(void);
void CAN_COMM_50MS_387(void);
void CAN_COMM_50MS_3ED(void);
void CAN_COMM_50MS_3FA(void);


void CAN_COMM_100MS_3D2(void);
void CAN_COMM_100MS_3B1(void);
void CAN_COMM_100MS_324(void);
void CAN_COMM_100MS_323(void);
void CAN_COMM_100MS_457(void);
void CAN_COMM_100MS_326(void);
void CAN_COMM_100MS_45C(void);
// void CAN_COMM_100MS_46B(void);
void CAN_COMM_100MS_45A(void);
void CAN_COMM_100MS_328(void);
void CAN_COMM_100MS_3D8(void);
void CAN_COMM_100MS_504(void);
void CAN_COMM_100MS_313(void);
void CAN_COMM_100MS_314(void);
void CAN_COMM_100MS_315(void);
void CAN_COMM_100MS_45D(void);
void CAN_COMM_100MS_355(void);
void CAN_COMM_100MS_4DD(void);
void CAN_COMM_100MS_4E3(void);
void CAN_COMM_100MS_32A(void);

void CAN_COMM_200MS_458(void);
void CAN_COMM_200MS_347(void);

void CAN_COMM_500MS_455(void);
void CAN_COMM_500MS_3D4(void);
void CAN_COMM_500MS_45B(void);
void CAN_COMM_500MS_4A8(void);
void CAN_COMM_500MS_461(void);

void CAN_COMM_1000MS_4B1(void);
void CAN_COMM_1000MS_4BA(void);

extern void S52_CAN_Comm_Proc(DEVICE *dev);

void CAN_Comm_Proc_test(DEVICE *dev);

void CHK_CAN_COMM_10MS_MSG(DEVICE *dev);
uint8 CHK_CAN_COMM_20MS_MSG(DEVICE *dev);
uint8 CHK_CAN_COMM_50MS_MSG(DEVICE *dev);
uint8 CHK_CAN_COMM_100MS_MSG(DEVICE *dev);
uint8 CHK_CAN_COMM_200MS_MSG(DEVICE *dev);
uint8 CHK_CAN_COMM_500MS_MSG(DEVICE *dev);
uint8 CHK_CAN_COMM_1000MS_MSG(DEVICE *dev);


void CANTp_Init_data(void);
uint16 check_tpdvd_cmd(void);
void CANTp_Comm(DEVICE *dev);
void CANTp_Control_Func(DEVICE *dev);
void CANTp_Send_Func(TpNamebuf* tpdata,TP_CMD_TYPE type);
void CANTp_Set_RRM_4(uint8* data);
#endif
void CAN_Init_CanIfSetMsg_gCanIf(void);	//JANG_211116
void CAN_Init_RRM_7_Time(void);
void CAN_Init_RRM_10_AMP(void);
void CAN_Init_RRM_11_AMP(void);

#endif /* CAN_COMM_H_ */
