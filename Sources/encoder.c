/*
 * encoder.c
 *
 *  Created on: 2019. 1. 4.
 *      Author: Digen
 */

#include "includes.h"


static volatile uint32 right_direction_tick = 0;
static volatile uint32 left_direction_tick = 0;
static volatile uint32 encoder_read_tick = 0;

#if 1
void EncoderProc1(ENCODER *encoder,volatile uint8 port)
{
	uint8 currentstatus;

	currentstatus = port;
	
	if(encoder->encoder!=currentstatus)
	{
		encoder->encoder=currentstatus;
		right_direction_tick = GetTickCount();
		left_direction_tick = GetTickCount();

		switch(encoder->oldstatus)
		{
		case 0x00:		// OFF
			switch(currentstatus)
			{
			case 0x01:
				encoder->direction=J_ENCODER_CCW;
				encoder->oldstatus=currentstatus;
				break;
			case 0x02:
				encoder->direction=J_ENCODER_CW;
				encoder->oldstatus=currentstatus;
				break;
			default:
				encoder->direction=J_ENCODER_STOP;
				encoder->laststatus=J_ENCODER_STOP;
				encoder->oldstatus=currentstatus;
				break;
			}
			break;
		case 0x01:		//MCU_KNOB_B ON
			switch(currentstatus)
			{
			case 0x00:
				switch(encoder->direction)
				{
				case J_ENCODER_CW:
					if(encoder->click<100)
					{
						encoder->click++;
					}
					encoder->oldstatus=currentstatus;
					break;
				default:
					encoder->direction=J_ENCODER_STOP;
					encoder->laststatus=J_ENCODER_STOP;
					encoder->oldstatus=currentstatus;
					break;
				}
				break;
			case 0x03:
				switch(encoder->direction)
				{
				case J_ENCODER_CCW:
					if(encoder->click>-100)
					{
						encoder->click--;
					}
					encoder->oldstatus=currentstatus;
					break;
				default:
					encoder->direction=J_ENCODER_STOP;
					encoder->laststatus=J_ENCODER_STOP;
					encoder->oldstatus=currentstatus;
					break;
				}
				break;
			case 0x02:
				encoder->direction=J_ENCODER_STOP;
				encoder->laststatus=J_ENCODER_STOP;
				break;
			default:
				encoder->direction=J_ENCODER_STOP;
				encoder->laststatus=J_ENCODER_STOP;
				encoder->oldstatus=currentstatus;
				break;
			}
			break;
		case 0x02:	//MCU_KNOB_A ON
			switch(currentstatus)
			{
			case 0x00:
				switch(encoder->direction)
				{
				case J_ENCODER_CCW:
					if(encoder->click>-100)
					{
						encoder->click--;
					}
					encoder->oldstatus=currentstatus;
					break;
				default:
					encoder->direction=J_ENCODER_STOP;
					encoder->laststatus=J_ENCODER_STOP;
					encoder->oldstatus=currentstatus;
					break;
				}
				break;
			case 0x03:
				switch(encoder->direction)
				{
				case J_ENCODER_CW:
					if(encoder->click<100)
					{
						encoder->click++;
					}
					encoder->oldstatus=currentstatus;
					break;
				default:
					encoder->direction=J_ENCODER_STOP;
					encoder->laststatus=J_ENCODER_STOP;
					encoder->oldstatus=currentstatus;
					break;
				}
				break;
			case 0x01:
				encoder->direction=J_ENCODER_STOP;
				encoder->laststatus=J_ENCODER_STOP;
				break;
			default:
				encoder->direction=J_ENCODER_STOP;
				encoder->laststatus=J_ENCODER_STOP;
				encoder->oldstatus=currentstatus;
				break;
			}
			break;
		case 0x03:	// off
			switch(currentstatus)
			{
			case 0x01:
				encoder->direction=J_ENCODER_CW;
				encoder->oldstatus=currentstatus;
				break;
			case 0x02:
				encoder->direction=J_ENCODER_CCW;
				encoder->oldstatus=currentstatus;
				break;
			default:
				encoder->direction=J_ENCODER_STOP;
				encoder->laststatus=J_ENCODER_STOP;
				encoder->oldstatus=currentstatus;
				break;
			}
			break;
	default:
		break;
		}
	}
}

void ReadEncoder(DEVICE *dev)
{
	if(GetElapsedTime(encoder_read_tick )<50) {return;}
		encoder_read_tick =	GetTickCount();
		
	#ifndef ENABLE_ENCODER_15MIN_MODE
	if(MCU_ACC_DET()==OFF)
	{
		dev->r_LeftEncoder->click=0;
		return;
	}	
	#endif

	if(dev->r_LeftEncoder->click!=0)
	{	
		dev->r_LeftEncoder->laststatus=J_ENCODER_STOP;
		dev->r_LeftEncoder->txed=TRUE;
		dev->r_LeftEncoder->ack=FALSE;

		if(dev->r_LeftEncoder->click>0)
		{
			if(dev->r_LeftEncoder->click>10)
			{
				dev->r_LeftEncoder->click/=4;
			}
			else
			{
				dev->r_LeftEncoder->click--;
			}

			if(50<GetElapsedTime(right_direction_tick ))
			{
				right_direction_tick = GetTickCount();
				dev->r_LeftEncoder->click=0;
				DPRINTF(DBG_MSG1,"right_direction click clear \n\r");
			}
			DPRINTF(DBG_MSG1,"right encoder %d\n\r",dev->r_LeftEncoder->click);
			key_packet.flag = ON;
			r_Device.r_System->key_active = ON;

			key_packet.buf[0] = (uint8)(KEY_DEF_ENCODER_RIGHT>>8);
			key_packet.buf[1] = (uint8)KEY_DEF_ENCODER_RIGHT;
			key_packet.buf[2] = MSG_STEER_RIGHT;
			key_packet.buf[3] = MSG_KEY_IDRIVE;
		}

		if(dev->r_LeftEncoder->click<0)
		{
			if(dev->r_LeftEncoder->click<-10)
			{
				dev->r_LeftEncoder->click/=4;
			}
			else
			{
				dev->r_LeftEncoder->click++;
			}
			if(50<GetElapsedTime(left_direction_tick ))
			{
				
				left_direction_tick =	GetTickCount();
				dev->r_LeftEncoder->click=0;
				DPRINTF(DBG_MSG1,"left_direction click clear \n\r");
			}
			DPRINTF(DBG_MSG1,"left encoder %d\n\r",dev->r_LeftEncoder->click);
			key_packet.flag = ON;
			r_Device.r_System->key_active = ON;

			key_packet.buf[0] = (uint8)(KEY_DEF_ENCODER_LEFT>>8);
			key_packet.buf[1] = (uint8)KEY_DEF_ENCODER_LEFT;
			key_packet.buf[2] = MSG_STEER_LEFT;
			key_packet.buf[3] = MSG_KEY_IDRIVE;
		}	
	}
}
#endif

