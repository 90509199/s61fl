/*
 * AMP_TB2952.h
 *
 *  Created on: 2018. 10. 19.
 *      Author: Digen
 */

#ifndef AMPTB2952_DEV_H_
#define AMPTB2952_DEV_H_

enum
{
	INTERNAL_AMP_IDLE=0,
	INTERNAL_AMP_INIT_START,
	INTERNAL_AMP_INIT_FINISH,
	INTERNAL_AMP_ACTIVE,
};

enum
{
	AMP_INIT_REG_0=0,
	AMP_INIT_REG_1,
	AMP_INIT_REG_2,
	AMP_INIT_REG_3,
	AMP_INIT_REG_4,
	AMP_INIT_REG_5,
	AMP_INIT_REG_6,
};

uint8 AMP_Init(DEVICE *dev);
uint8_t FlexIO_I2C_Read(uint8_t device_addr,uint8_t register_address,uint8_t count);
uint8_t FlexIO_I2C_Write(uint8_t device_addr,uint8_t register_address,uint8_t data);

#endif /* AMPTB2952_DEV_H_ */
