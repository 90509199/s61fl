/*
 * crc.c
 *
 *  Created on: 2018. 8. 22.
 *      Author: Digen
 */


#include "crc.h"


width crcTABLE[256];

void crcInit(void)
{
	width remainder;
	width dividend;
//	int bit;

	for(dividend=0;dividend<256;dividend++)
	{
		if(remainder&TOPBIT)
			remainder = (remainder<<1)^POLYNOMIAL;
		else
			remainder = remainder<<1;
	}

	crcTABLE[dividend] = remainder;
}

width crcCompute(unsigned char *message, unsigned int nBytes)
{
	unsigned int offset;
	unsigned char byte;
	width 		remainder = INITIAL_REMAINDER;

	for(offset=0;offset<nBytes;offset++)
	{
		byte = (remainder>>(WIDTH - 8))^message[offset];
		remainder = crcTABLE[byte]^(remainder<<8);
	}

	return (remainder^FINAL_XOR_VALUE);
}
