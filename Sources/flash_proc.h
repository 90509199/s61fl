/*
 * flash_proc.h
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#ifndef FLASH_PROC_H_
#define FLASH_PROC_H_

#define IS25LP_WRITE_ENABLE							0x06
#define IS25LP_WRITE_DISABLE							0x04	
#define IS25LP_NORMAL_READ_MODE				0x03
#define IS25LP_INPUT_PAGE_PROGRAM			0x02
#define IS25LP_SECTOR_ERASE							0xD7//0x20
#define IS25LP_READ_STATUS								0x05
#define IS25LP_WRITE_STATUS							0x01

typedef struct tagDATA_SAVE
{
	uint8 E2P_ID1;											//ADDR	0
	uint8 E2P_CHKSUM;
	uint8 E2P_KEY_VOL;
	uint8 E2P_SVCD;
	uint8 E2P_CLU_WARN_VOL;
	uint8 E2P_RADAR_WARN_VOL;
	uint8 E2P_MEDIA_VOL;
	uint8 E2P_CALL_VOL;
	uint8 E2P_NAVI_VOL;
	uint8 E2P_VOICE_VOL;
	uint8 E2P_LONDNESS_ONOFF;					//ADDR 10
	uint8 E2P_EQ_FADER;
	uint8 E2P_EQ_BALANCE;
	uint8 E2P_EQ_TREBLE;	
	uint8 E2P_EQ_MID;
	uint8 E2P_EQ_BASS;
	uint8 E2P_EQ_TYPE;//sound mode
	uint8 E2P_SOUND_WAVE_MODE;
	uint8 E2P_SOUND_FEILD_MODE;
	uint8 E2P_SURROUND_SOUND;
	uint8 E2P_LCD_BRIGHTNESS;					//ADDR 20
	uint8 E2P_DEFAULTMAX_VOL;					//ADDR 21
	uint8 E2P_CLS_BRIGHTNESS;
	uint8 E2P_CLS_DRIVINGTIME;
	uint8 E2P_CLS_OVERSPD;
	uint8 E2P_PSNGRAIRBAG_ONOFF;
	uint8 E2P_AVAS_ONOFF;
} DATA_SAVE;

typedef struct tagE2P_SYSTEM
{
	uint8 E2P_AUDIO_PATH_DEFAULT;			//ADDR 30
	uint8 E2P_LAST_MODE;
	uint8 E2P_RADIO_MODE;
	uint16 E2P_RADIO_FREQUENCY;
	uint16 E2P_TUNNING_FM_LEVEL;
	uint16 E2P_TUNNING_FM_WAM;
	uint16 E2P_TUNNING_FM_USN;
	uint16 E2P_TUNNING_FM_OFFSET;
	uint16 E2P_TUNNING_AM_LEVEL;
	uint16 E2P_TUNNING_AM_OFFSET;
	uint8 E2P_ENGINEER_MODE;
	uint8 E2P_DOOR_OPEN;
	uint8 E2P_TEST_MODE;
	uint8 E2P_DEBUG_ONOFF;
	uint8 E2P_TUNER_TYPE;
	uint8 E2P_SONY;
} E2P_SYSTEM;


extern DATA_SAVE e2p_save_data;
extern E2P_SYSTEM e2p_sys_data;

extern const size_t e2p_size;

void INIT_FLASH_DATA(void);

uint8 reg_read_status(void);
uint8 flash_sector_erase(uint32 addr);

uint8 flash_read_func(uint32 addr, unsigned char *rxbuf, int rxlen);
uint8 flash_write_func(uint32 addr, const unsigned char *txbuf, int txlen);

void defualt_setup_value(DATA_SAVE *data);
void defualt_system_value(E2P_SYSTEM *data);
uint8 checksum_setup_value(DATA_SAVE *data);
void save_last_memory(void);

#endif /* FLASH_PROC_H_ */
