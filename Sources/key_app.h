/*
 * key_app.h
 *
 *  Created on: 2019. 1. 4.
 *      Author: Digen
 */

#ifndef KEY_APP_H_
#define KEY_APP_H_

//KEY
#define KEY_BACK										1	
#define KEY_SET											2	
#define KEY_HOME										3	
#define KEY_ENTER										4	
#define KEY_UP											5
#define KEY_DOWN										6			
#define KEY_LEFT										7			
#define KEY_RIGHT										8
#define KEY_PWR										9		
#define KEY_AVM_ONOFF							10
#ifdef CX62C_FUNCTION
#define KEY_NEXT 										11
#define KEY_LAST										12
#define KEY_VOICE_PLUS							13
#define KEY_VOICE_MINUS 						14
#define KEY_MODE 										15
#define KEY_VOICE_ACTIVATE					16
#define KEY_WECHAT									17
#define KEY_PHONE_CALL							18
#define KEY_MAX										19
#else
#define KEY_MAX										11
#endif

#define KEY_DEF_OK										28
#define KEY_DEF_PHONE_CALL					61	
#define KEY_DEF_PHONE_HANGUP				62
#define KEY_DEF_UP										29
#define KEY_DEF_LEFT									31
#define KEY_DEF_RIGHT									32
#define KEY_DEF_DN										30
#define KEY_DEF_VOL_DN								108
#define KEY_DEF_VOL_UP								103
#define KEY_DEF_MENU									172//127
#define KEY_DEF_SETUP								141	
#define KEY_DEF_RETURN								158	
#define KEY_DEF_LAST_SONG						105
#define KEY_DEF_NEXT_SONG						106
#define KEY_DEF_HOME2								172

#define KEY_DEF_HOME									587
#define KEY_DEF_WECHAT								216		
#define KEY_DEF_PWR									116		
#define KEY_DEF_MODE									316
#define KEY_DEF_VOICE								582	
#define KEY_DEF_ENCODER_LEFT					585
#define KEY_DEF_ENCODER_RIGHT				586
#define KEY_DEF_PHONE_CALL_HANGUP	588
#define KEY_DEF_AVM_ON_OFF					589



typedef enum  {
	POWER_KEY_IDLE=0,
	POWER_KEY_SHORT,
	POWER_KEY_LONG,
	POWER_KEY_REPEAT,
}POWER_KEY_TYPE;


void InitDrvKey(void);

void Adc_Key_Parser0(uint32_t key_value);
void Adc_Key_Parser1(uint32_t key_value);
void Adc_Key_Parser2(uint32_t key_value);
#ifdef CX62C_FUNCTION
void Cluster_Key_Parser1(uint32_t key_value);
void Cluster_Key_Parser2(uint32_t key_value);
#endif

void Key_Task(void);

#endif /* KEY_APP_H_ */
