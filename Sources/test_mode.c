/*
 * test_mode.c
 *
 *  Created on: 2018. 12. 24.
 *      Author: Digen
 */

#include "includes.h" 
#include "Cpu.h"

#ifdef SI_LABS_TUNER
#include "sg_defines.h"
#include "main.h"
#endif

static volatile uint32 test_timer_tick = 0;


DSP_GEQ geq1;
DSP_GEQ geq2;
DSP_GEQ geq3;
DSP_GEQ geq4;
DSP_GEQ geq5;
DSP_GEQ geq6;

#if 0
extern const uint8 vehicleManufactSparePartNum[DIAGDBI_VEHICLE_MANUFACTURER_SPARE_PART_NUM_LEN + 1];
extern const uint8 systemSupplierId[DIAGDBI_SYSTEM_SUPPLIER_ID_LEN + 1];
extern const uint8 vehicleManufactECUHwNum[DIAGDBI_VEHICLE_MANUFACTURER_ECU_HW_NUM_LEN + 1]; // {'E', 'C', 'U', '-', 'H', 'W', 'N', 'U', 'M', 'B'}
extern const uint8 systemSupplierECUSWNum [DIAGDBI_VEHICLE_MANUFACTURER_ECU_SW_NUM_LEN + 1]; // {'E', 'C', 'U', '-', 'S', 'W', 'N', 'U', 'M', 'B'}
#else
extern const uint8 vehicleManufactSparePartNum[14];
extern const uint8 vehicleManufactSparePartNum1[14];
extern const uint8 systemSupplierId[4];
extern const uint8 vehicleManufactECUHwNum[11]; // {'E', 'C', 'U', '-', 'H', 'W', 'N', 'U', 'M', 'B'}
extern const uint8 systemSupplierECUSWNum [11]; // {'E', 'C', 'U', '-', 'S', 'W', 'N', 'U', 'M', 'B'}
#endif

void DSP_Init_GEQ(void)
{
	geq1.gain = -6;
	geq1.band_factor = 0.7;
	geq1.band_freq = 50;

	geq2.gain = -7;
	geq2.band_factor = 3.0;
	geq2.band_freq = 275;

	geq3.gain = 5;
	geq3.band_factor = 2.0;
	geq3.band_freq = 660;

	geq4.gain = -8;
	geq4.band_factor = 2.0;
	geq4.band_freq = 1100;

	geq5.gain = -2;
	geq5.band_factor = 2.0;
	geq5.band_freq = 2700;

	geq6.gain = 3;
	geq6.band_factor = 3.0;
	geq6.band_freq = 140;
}


void Test_Task(DEVICE *dev)
{
	uint8 tx_data[255]={0,};
	uint8 rx_data[255] ={0,};
	uint8 result=I2C_BUSY;
	uint16 i=0;
	uint16 tmp16=0;

	if(shell_radio_freq)
	{
		if(dev->r_Tuner->t_mode==FM_MODE)
		{
			if((FM_MIN_FREQ<=shell_radio_freq)&&(shell_radio_freq<=FM_MAX_FREQ))
			{	
				dev->r_Tuner->fm_freq = shell_radio_freq;
				#ifdef SI_LABS_TUNER
				dev->r_Tuner->t_state = TUNER_TUNE_0;
				#else
				FM_Tune_To(1,shell_radio_freq);
				#endif
			}
		}
		else
		{
			if((AM_MIN_FREQ<=shell_radio_freq)&&(shell_radio_freq<=AM_MAX_FREQ))
			{				
				dev->r_Tuner->am_freq = shell_radio_freq;
				#ifdef SI_LABS_TUNER
				dev->r_Tuner->t_state = TUNER_TUNE_0;
				#else
				AM_Tune_To(1,shell_radio_freq);
				#endif
			}
		}
		shell_radio_freq =0;
	}

	if(shell_audio_src!=0)
	{
		dev->r_Dsp->d_audo_mode = shell_audio_src;
		dev->r_Dsp->d_state = DSP_CHANG_MODE_1;
		#ifdef SEPERATE_VOLUME_TABLE
		if((dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_BCALL)||(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_MEDIA))
			mediachtype = CH_TYPE_MEDIA;
		else if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_FM)
		{
			if(dev->r_Tuner->t_mode==FM_MODE)
				mediachtype = CH_TYPE_FM;
			else
				mediachtype = CH_TYPE_AM;
		}
		#endif
		shell_audio_src=0;
	}

	//Volume CMD
	switch(shell_volume)
	{
		case 0:
			break;
		case 0xFE:	//unmute
			dev->r_Dsp->d_state =DSP_UNMUTE_1;
			shell_volume =0;
			break;
		case 0xFF:		//mute
			dev->r_Dsp->d_state =DSP_MUTE_1;

			shell_volume =0;
			break;
		default:
			if(shell_volume==32)
				dev->r_Dsp->d_media_vol = 0;
			else
				dev->r_Dsp->d_media_vol = shell_volume;			
			dev->r_Dsp->d_state =DSP_MAIN_VOL;
			shell_volume =0;
			DPRINTF(DBG_INFO,"\n\r vol control ok : %x\n\r",dev->r_Dsp->d_media_vol);
			break;
	}

	switch(shell_value_0)
	{
		//normal test function 1~19
		case 1:		
//			Control_LCD_Backlight_for_Mengbo(dev,OFF);
			Control_LCD_Backlight(OFF);
			DPRINTF(DBG_INFO,"\n\r BL OFF \n\r");
			shell_value_0=0;
			break;
		case 2:
			Control_LCD_Backlight(ON);//LCD ON
//			Control_LCD_Backlight_for_Mengbo(dev,ON);
			DPRINTF(DBG_INFO,"\n\r BL ON \n\r");
			shell_value_0=0;
			break;
		case 3:
//			dev->r_Dsp->d_state =DSP_TEST_5;
			#ifdef SI_LABS_TUNER
			if((stTuner.task == TUNER_TASK_NONE)&&(stTuner.prc == TUNER_PRC_NONE))
			{
				stTuner.prc =TUNER_PRC_INIT;
				stTuner.task =TUNER_TASK_PRC_INIT_START;
				DPRINTF(DBG_INFO,"\n\r TUNER INIT\n\r");
			}	
			#endif
			shell_value_0=0;
			break;
		case 4:
//			dev->r_Dsp->d_state =DSP_TEST_6;
			#if 0 //def INTERNAL_AMP
			FlexIO_I2C_Write(0x6C,0x01,0x06); //0x6C
			delay_ms(10);
			FlexIO_I2C_Write(0x6C,0x02,0x10);
			delay_ms(10);
			FlexIO_I2C_Write(0x6C,0x03,0x00);
			#endif
			#ifdef SI_LABS_TUNER
			if((stTuner.task == TUNER_TASK_NONE)&&(stTuner.prc == TUNER_PRC_NONE))
			{
				if(ucBand == B_FM1){
					ucBand = B_AM1;
					lFreq =AM_MIN_FREQ;
				}
				else{
					ucBand = B_FM1;
					lFreq =10170;
				}
				stTuner.prc =TUNER_PRC_BAND_CHANGE;
				stTuner.task =TUNER_TASK_PRC_BAND_CHANGE_START;
				DPRINTF(DBG_INFO,"\n\r BAND_CHANGE\n\r");
			}
			#endif
			shell_value_0=0;
			break;
		case 5:		
			#ifdef SI_LABS_TUNER
			if((stTuner.task == TUNER_TASK_NONE)&&(stTuner.prc == TUNER_PRC_NONE))
			{
				stTuner.prc =TUNER_PRC_AUTO_STORE;
				stTuner.task =TUNER_TASK_PRC_AUTO_STORE_START;
				DPRINTF(DBG_INFO,"\n\r AUTO_STORE\n\r");
			}
			#else
			#ifdef INTERNAL_AMP
			FlexIO_I2C_Read(0x6C,0x0,5);
			#endif
			#endif
			shell_value_0=0;
			break;
		case 6:
			#if 0 //def SI_LABS_TUNER
			DPRINTF(DBG_INFO,"\n stTuner task:%d prc:%d \n\r",stTuner.task ,stTuner.prc);
			if((stTuner.task == TUNER_TASK_NONE)&&(stTuner.prc == TUNER_PRC_NONE))
			{
				stTuner.prc = TUNER_PRC_CHECK_RSQ; 
				stTuner.task = TUNER_TASK_PRC_CHECK_RSQ_START;
				DPRINTF(DBG_INFO,"\n\r TUNER_PRC_CHECK_RSQ\n\r");
			}
			#endif
			#ifdef CX62C_FUNCTION
			DPRINTF(DBG_INFO,"\n sys_mode:%d dsp mode:%d tuner_mode:%d\n\r",dev->r_System->s_state,dev->r_Dsp->d_state,dev->r_Tuner->t_state);
			DPRINTF(DBG_INFO,"\n sony:%d \n\r", r_sonysound);
			DPRINTF(DBG_INFO,"\n soc update :%d %d\n\r", dev->r_System->soc_update,dev->r_System->gear_status);
			#else
			DPRINTF(DBG_INFO,"\n sys_mode:%d dsp mode:%d tuner_mode:%d\n\r",dev->r_System->s_state,dev->r_Dsp->d_state,dev->r_Tuner->t_state);
			DPRINTF(DBG_INFO,"\n soc update :%d %d\n\r", dev->r_System->soc_update,dev->r_System->gear_status);
			#endif
			shell_value_0=0;
			break;
		case 7:	
			#if 1
			uart0_deinit();
			uart1_deinit();
			init_dev_uart();
			DPRINTF(DBG_INFO,"\n\r reinitalize uart\n\r");
			#else
			//uart rxtx start
			dev->r_System->rxtx_test.state =1;
			dev->r_System->rxtx_test.cnt =0;
			dev->r_System->rxtx_test.rxcnt =0;
			dev->r_System->rxtx_test.txcnt =0;
			#endif
			shell_value_0=0;	
			break;
		case 8:	
			#if 0
			gCanTpState = TP_STATE_COMM_START;
			for(i=0;i<10;i++)
				tx_data[i] =i;
			
			Road_Name.len = 10;
			Road_Name.lock = ON;
			Road_Name.cnt = 0;
			memcpy(&Road_Name.buf[0], &tx_data[0], 10);
			#else
 //			dev->r_System->rxtx_test.state =2;

			#endif
			#ifdef SI_LABS_TUNER
			DPRINTF(DBG_INFO,"\n task:%d proc:%d tuner_mode:%d\n\r",stTuner.task,stTuner.prc ,dev->r_Tuner->t_state);
			#endif
			shell_value_0=0;	
			break;
		case 9:
			dev->r_Power->p_state = PWR_RESET_1;
			dev->r_System->pshold =OFF;
			dev->r_System->alivecleartime = OFF;
			dev->r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
			dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
			DPRINTF(DBG_INFO,"\n\r SOC reboot \n\r");
			shell_value_0=0;	
			break;
		case 10:

			shell_value_0=0;	
			break;
		//dsp test function 20~29
		case 20:	//register dump
			dev->r_Dsp->d_state =DSP_TEST_1;
			DPRINTF(DBG_INFO,"\n\r dsp register dump \n\r");
			shell_value_0=0;	
			break;
		case 21:	//only dsp init
			dev->r_Dsp->d_state = DSP_TEST_INIT_1;
			DPRINTF(DBG_INFO,"\n\r dsp init \n\r");
			shell_value_0=0;	
			break;
		case 22:	//a2b init
			#ifdef INTERNAL_AMP
			dev->r_System->amp_state = INTERNAL_AMP_INIT_START;
			MCU_AMP_MUTE(ON);
			DPRINTF(DBG_INFO,"\n\r amp init \n\r");
			#else
			dev->r_A2B->a_state = A2B_INIT1;
			DPRINTF(DBG_INFO,"\n\r a2b init \n\r");
			#endif
			shell_value_0=0;	
			break;
		case 23:	//media bypass
			dev->r_Dsp->d_state =DSP_TEST_2;
			DPRINTF(DBG_INFO,"\n\r media bypass \n\r");
			shell_value_0=0;	
			break;
		case 24:	//radio bypass
			dev->r_Dsp->d_state =DSP_TEST_3;
			DPRINTF(DBG_INFO,"\n\r radio bypass \n\r");
			shell_value_0=0;	
			break;	
		case 25:	//phone bypass
			dev->r_Dsp->d_state =DSP_TEST_4;
			DPRINTF(DBG_INFO,"\n\r phone bypass \n\r");
			shell_value_0=0;	
			break;	
		case 26:	//dsp all init
			dev->r_Dsp->d_state = DSP_INIT_1;
			DPRINTF(DBG_INFO,"\n\r dsp all init \n\r");
			shell_value_0=0;	
			break;
		case 27:	//beep test
			dev->r_Dsp->d_state =DSP_BEEP_START;
			DPRINTF(DBG_INFO,"\n\r beep start \n\r");
			shell_value_0=0;	
			break;
		case 28:	//dsp reset
			DPRINTF(DBG_INFO,"\n\r dsp reset and reset system\n\r");
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			delay_ms(10);
			SystemSoftwareReset();
			shell_value_0=0;	
			break;	
		case 29:
			dev->r_Dsp->d_state =DSP_TEST_7;
			DPRINTF(DBG_INFO,"\n\r cmd geq \n\r");
			shell_value_0=0;	
			break;	
		//FM function 30~39	
		case 30:
			dev->r_Tuner->t_state = TUNER_SEEK_PLUS_0;
			DPRINTF(DBG_INFO,"\n\r seek up \n\r");
			shell_value_0=0;	
			break;
		case 31:
			dev->r_Tuner->t_state = TUNER_SEEK_MINUS_0;
			DPRINTF(DBG_INFO,"\n\r seek dn \n\r");
			shell_value_0=0;	
			break;
		case 32:
			dev->r_Tuner->t_state = TUNER_ATS_0;
			DPRINTF(DBG_INFO,"\n\r seek ast \n\r");
			shell_value_0=0;	
			break;
		case 33:
			if(dev->r_Tuner->t_mode==FM_MODE)
			{	
				dev->r_Tuner->am_freq = 540;
				dev->r_Tuner->t_mode= AM_MODE;
				dev->r_Tuner->t_state = TUNER_CHANG_TO_AM_0;				
				#ifdef SEPERATE_VOLUME_TABLE
				if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_FM)
				{
					if(dev->r_Tuner->t_mode==FM_MODE)
						mediachtype = CH_TYPE_FM;
					else
						mediachtype = CH_TYPE_AM;
				}
				#endif
				DPRINTF(DBG_INFO,"\n\r enter am mode \n\r");
			}
			else
			{
				dev->r_Tuner->fm_freq = 8910;
				dev->r_Tuner->t_mode= FM_MODE;
				dev->r_Tuner->t_state = TUNER_CHANG_TO_FM_0;
				#ifdef SEPERATE_VOLUME_TABLE
				if(dev->r_Dsp->d_audo_mode == AVNT_USECASE_AUDIOPATH_FM)
				{
					if(dev->r_Tuner->t_mode==FM_MODE)
						mediachtype = CH_TYPE_FM;
					else
						mediachtype = CH_TYPE_AM;
				}
				#endif
				DPRINTF(DBG_INFO,"\n\r enter fm mode \n\r");
			}
			shell_value_0=0;	
			break;
		case 34:
			dev->r_Tuner->fm_freq+=10;
			if(FM_MAX_FREQ<dev->r_Tuner->fm_freq)
				dev->r_Tuner->fm_freq = FM_MIN_FREQ;	
			dev->r_Tuner->t_state = TUNER_TUNE_0;
			DPRINTF(DBG_INFO,"\n\r tune up %d\n\r",dev->r_Tuner->fm_freq);
			shell_value_0=0;
			break;
		case 35:
			dev->r_Tuner->fm_freq-=10;
			if(dev->r_Tuner->fm_freq<FM_MIN_FREQ)
				dev->r_Tuner->fm_freq = FM_MAX_FREQ;
			dev->r_Tuner->t_state = TUNER_TUNE_0;
			DPRINTF(DBG_INFO,"\n\r tune down %d\n\r",dev->r_Tuner->fm_freq);
			shell_value_0=0;
			break;
		case 36:
//			dev->r_Tuner->t_state= TUNER_IDLE;
//			i2c_deinit();
			DPRINTF(DBG_INFO,"\n\r fm level 2db \n\r");
			shell_value_0=0;
			break;
		case 37:
//			i2c_init();
//			dev->r_Tuner->t_state= TUNER_ACTIVE_0;
			DPRINTF(DBG_INFO,"\n\r fm level -20db \n\r");			
			shell_value_0=0;
			break;
		case 38:
//			check_amast_level = TUNE_GET_AM_LEVEL+200;
			DPRINTF(DBG_INFO,"\n\r am level 0db \n\r");
			shell_value_0=0;
			break;
		case 39:
//			check_amast_level = TUNE_GET_AM_LEVEL;
			DPRINTF(DBG_INFO,"\n\r am level -20db \n\r");		
			shell_value_0=0;	
			break;
		case 40:			// bt addr
			DPRINTF(DBG_INFO,"\n\r bt addr [ %x:%x:%x:%x:%x:%x]\n\r",bt_address[0],bt_address[1],bt_address[2],bt_address[3],bt_address[4],bt_address[5]);
			shell_value_0=0;	
			break;	
		case 41:			// wifi addr
			DPRINTF(DBG_INFO,"\n\r wifi addr [ %x:%x:%x:%x:%x:%x]\n\r",wifi_address[0],wifi_address[1],wifi_address[2],wifi_address[3],wifi_address[4],wifi_address[5]);
			shell_value_0=0;	
			break;	
		case 42:			// vehicleManufactSparePartNum

			shell_value_0=0;	
			break;
		case 43:			// systemSupplierId

			shell_value_0=0;	
			break;
		case 44:			// vehicleManufactECUHwNum

			shell_value_0=0;	
			break;
		case 45:			// systemSupplierECUSWNum

			shell_value_0=0;	
			break;
		case 50:			//EQ
			dev->r_Dsp->d_state =DSP_TEST_5;
			shell_value_0=0;	
			break;	
		case 51:			//EQ
			dev->r_Dsp->d_state =DSP_TEST_6;
			shell_value_0=0;	
			break;	
		case 60:			//sony eq
			//dsp restart
			dev->r_Dsp->d_state =DSP_INIT_1;
			e2p_sys_data.E2P_SONY =r_sonysound ;
			shell_value_0=0;	
			break;	
	}
}

 

