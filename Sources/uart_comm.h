/*
 * uart_comm.h
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#ifndef UART_COMM_H_
#define UART_COMM_H_

extern uint16 MCU_TX_CNT;
extern uint16 Version_1;
extern uint16 Version_2;
extern uint16 Version_3;

extern uint8  IHU1OperFlag,IHU2OperFlag,IHU6OperFlag,IHU12OperFlag;
extern uint8  IHU1TimerCount,IHU2TimerCount,IHU6TimerCount,IHU12TimerCount;
extern uint8  EnterSocFlag,TimerCount;

#define MAX_UART_COMMAND 			(1024)//(1560)

//////////////////////////////////////////////
#define PACKET_ID_SIZE 					(4)
#define PACKET_CNT_SIZE 				(5)
#define PACKET_HEADER_SIZE 		(7)//(5)

#define CPU_STX 								(0x02)
#define CPU_ETX 								(0x03)

#define CPU_COMMAND 						(0x0A)
#define CPU_RESPONSE 					(0x0D)
#define MCU_RESPONSE 					(0x0B)
#define MCU_COMMAND						(0x0C)

//////////////////////////////////////////////
//
//	Command ID 
//
//////////////////////////////////////////////
#define MSG_REMOTE									(0x7f)

#define MSG_ACC_IGN_STATUS				(0x80)
#define MSG_UART_OK								(0x81)
#define MSG_SYS_MODE_REQ 					(0x82)
#define MSG_SYS_MODE_REV					(0x83)
#define MSG_CARSIGNAL 							(0x84)
#define MSG_VEHICLE_SIG						(0x86)
#define MSG_RADIO_MIX							(0xD1)
#define MSG_RADIO_NOISE_FILTER	 	(0x87)
#define MSG_DABPATH								(0x88)
#define MSG_RADIO_PWR							(0x89)
#define MSG_PRESET									(0x8A)
#define MSG_SEEK 										(0x8B)
#define MSG_AST										(0x8C)
#define MSG_BAND 									(0x8D)
#define MSG_RADIO_STATE						(0x8E)
#define MSG_RDS										(0x8F)

//TEST
#define MSG_TEST_RADIO_SIG				(0xD0)

#define MSG_AUDIOMODE							(0x90)
#define MSG_VOLUME									(0x91)
#define MSG_FADE_BAL								(0x92)
#define MSG_EQ											(0x93)
#define MSG_SOFTMUTE								(0x94)
#define MSG_AMPMUTE								(0x95)
#define MSG_AUDIO_FACTORY_RESET	(0x96)
#define MSG_NAVI_VOICE							(0x97)
#define MSG_AUIDO_MIXER						(0x98)
#define MSG_EXTERNAL_AMP 					(0x99)
#define MSG_ASTLIST_REQ						(0x9A)
#define MSG_UI_BUTTON							(0x9B)
#define MSG_CARPLAYDUCKING 				(0x9C)
#define MSG_RESERVED_9D						(0x9D)
#define MSG_RESERVED_9E						(0x9E)
#define MSG_RESERVED_9F						(0x9F)


#define MSG_HARDKEY								(0xA0)
#define MSG_ENCODER								(0xA1)
#define MSG_STEERINGKEY						(0xA2)
#define MSG_CLIMATE								(0xA3)
#define MSG_CPU_UPGRADE						(0xA4)
#define MSG_MCU_UPGRADE_START		(0xA5)
#define MSG_MCU_UPGRADE						(0xA6)
#define MSG_LAST_WRITE							(0xA7)
#define MSG_LAST_READ							(0xA8)
#define MSG_CPU_ALIVE_ENABLE 			(0xA9)
#define MSG_CPU_ALIVE							(0xAA)
#define MSG_VERSION								(0xAB)
#define MSG_BRIGHTNESS 						(0xAC)
#define MSG_CLOCK_SET							(0xAD)
#define MSG_DTC_INFORM_CMD				(0xAE)
#define MSG_DTC_CMD								(0xAF)

#define MSG_VIN_CMD								(0xB0)
#define MSG_SOFTCONFIG_CMD				(0xB1)
#define MSG_MUSIC_KEY_CMD					(0xB2)
#define MSG_ENGINEER_MODE					(0xB3)
#define MSG_DVR_CONT_CMD					(0xB4)
#define MSG_MANUFACTURE_CMD			(0xB5)
#define MSG_ACCIGN_REQ_CMD				(0xB6)

#ifdef CX62C_FUNCTION
#define MSG_IVI_INFO_CMD						(0xB7)
#define MSG_CLU_STATUS_INFO_CMD	(0xB8)
#define MSG_CLU_STATUS_REQ_CMD		(0xB9)
#endif

#define MSG_DIAG_USB_CMD					(0xBA)
#define MSG_RXTX_TEST_EXCUTE_CMD	(0xBB)
#define MSG_RXTX_TEST_CMD					(0xBC)
#define MSG_RXTX_TEST_RESULT_CMD	(0xBD)


#define MSG_UART_TEST							(0xFF)

#define MSG_KEY_PRESS							(0x01)
#define MSG_KEY_RELEASE						(0x00)

#define MSG_STEER_RIGHT						(0x01)
#define MSG_STEER_LEFT							(0x02)

#define MSG_KEY_STEERING						(0x01)
#define MSG_KEY_IDRIVE							(0x02)


#define SETUP_LIST_DATA_LEN1				(2)
#define SETUP_LIST_DATA_LEN2				(4)
#define SETUP_LIST_DATA_LEN3				(8)
#define SETUP_LIST_DATA_LEN4				(16)
#define SETUP_LIST_DATA_LEN5				(32)



#define CPU_SEND_RETRY_TIME				(30)
#define CPU_SEND_20_TIME						(20)
#define CPU_SEND_50_TIME						(50)
#define CPU_SEND_100_TIME					(100)
#define CPU_SEND_200_TIME					(200)
#define CPU_SEND_500_TIME					(500)
#define CPU_SEND_1000_TIME					(1000)


#define CPU_SEND_CNT								(1)
#define CPU_SEND_RETRY_CNT				(2)



/*************************************************************************************************/
/*************************************************************************************************/

typedef enum
{
	TX_KEY_CLEAR				= 0x00,
	TX_BACK_KEY				= 0x01,
	TX_SET_KEY					= 0x02,
	TX_HOME_KEY				= 0x03,
	TX_ENTER_KEY				= 0x04,
	TX_UP_KEY						= 0x05,
	TX_DOWN_KEY				= 0x06,
	TX_LEFT_KEY					= 0x07,
	TX_RIGHT_KEY				= 0x08,
	TX_PWR_KEY					= 0x09,
	TX_AVM_KEY					= 0x0A,
	#ifdef CX62C_FUNCTION
	TX_NEXT_KEY					= 0x0B,
	TX_LAST_KEY					= 0x0C,
	TX_VOICE_P_KEY			= 0x0D,
	TX_VOICE_M_KEY			= 0x0E,
	TX_MODE_KEY				= 0x0F,
	TX_VOICE_ACT_KEY		= 0x10,
	TX_WECHAT_KEY			= 0x11,
	TX_PHONE_KEY				= 0x12,
	#endif
	TX_ENTER_LONGKEY		= 0x80,
	TX_PWR_LONGKEY		= 0x81,
	#ifdef CX62C_FUNCTION
	TX_PHONECALL_LONGKEY	= 0x82,
	TX_WECHAT_LONGKEY	= 0x83,
	#endif
	TX_KEY_DUMMY_			= 0xFF,
} BUTTON_TX_t;


enum
{
	RADIO_NORMAL							= 0x00,
	RADIO_SEEK_PROGRESS				= 0x01,
	RADIO_SEEK_FINISH					= 0x02,
	RADIO_AST_PROGRESS				= 0x03,
	RADIO_AST_FINISH						= 0x04,
	RADIO_DUMMY_							= 0xFF,
};

enum
{
	CAR_SIG_TX_GEAR_CMD = 1,
	CAR_SIG_TX_CGW_SAM_G_CMD,
	CAR_SIG_TX_SPEED_CMD,
	CAR_SIG_TX_SEAT_CMD,
	CAR_SIG_TX_FUELLEVEL_CMD,
	CAR_SIG_TX_DTE_CMD,
	CAR_SIG_TX_DVR1_CMD,
	CAR_SIG_TX_SEATBELT_CMD,
	CAR_SIG_TX_CEM_BCM1_CMD,
	CAR_SIG_TX_CEM_BCM2_CMD,						//10
	CAR_SIG_TX_CEM_BCM3_CMD,
	CAR_SIG_TX_CEM_BCM4_CMD,
	CAR_SIG_TX_ABS_ESP5_CMD,
	CAR_SIG_TX_PM25_1_CMD,
	CAR_SIG_TX_BSD1_CMD,
	CAR_SIG_TX_CEM_RADAR1_CMD,
	CAR_SIG_TX_TURNSIGNAL_CMD,
	CAR_SIG_TX_ODO_CMD,
	CAR_SIG_TX_AVM_APA4_CMD,
	CAR_SIG_TX_AVM_APA2_CMD,						//20
	CAR_SIG_TX_AVM_APA3_CMD,
	CAR_SIG_TX_AVM2_CMD,
	CAR_SIG_TX_FCM2_CMD,
	CAR_SIG_TX_FRM3_CMD,
	CAR_SIG_TX_WPC_CMD,
	CAR_SIG_TX_CEM_ALM1_CMD,
	CAR_SIG_TX_TBOX2_CMD,
	CAR_SIG_TX_ENGINESPEED_CMD,
	CAR_SIG_TX_CLT_CMD,
	CAR_SIG_TX_ROLLINGCOUNTER_CMD,			//30
	CAR_SIG_TX_MFS_HEARTRATE_CMD,
	CAR_SIG_TX_AFU_CMD,
	CAR_SIG_TX_AMP_CMD,
	CAR_SIG_TX_CEM_IPM_CMD,
	CAR_SIG_TX_ICM2_CMD,
	CAR_SIG_TX_ICM4_CMD,
	CAR_SIG_TX_CEM_PEPS_CMD,
	CAR_SIG_TX_PLG1_CMD,
	CAR_SIG_TX_CGW_EPS_G_CMD,
	CAR_SIG_TX_CGW_ABM_G_CMD,
};

enum
{
	CAR_SIG_AUTO_LOCK = 1,
	CAR_SIG_STALLING_UNLOCK,
	CAR_SIG_SETUP_DEFENCES,
	CAR_SIG_OUTSIDE_REARVIEW_MIRROR,
	CAR_SIG_TRUNK_DOOR,
	CAR_SIG_ACCOMPANY_ME_HOME_TIME,
	CAR_SIG_ACCOMPANY_ME_HOME,
	CAR_SIG_TURN_FLASH_LIGHT,
	CAR_SIG_BRAKE_FLASH_LIGHT,
	CAR_SIG_EXTERIOR_MIRROR,		//10
	CAR_SIG_LASER_BEAM,
	CAR_SIG_DVR_CONFIG,
	CAR_SIG_DOOR_OPENING_WARN,
	CAR_SIG_RCTA,
	CAR_SIG_BAM,
	CAR_SIG_LWDS_SENSITIVITY,
	CAR_SIG_ASSIST_OPTION,
	CAR_SIG_SLA,
	CAR_SIG_HMA,
	CAR_SIG_AEB,	//20
	CAR_SIG_FRONT_COLLISION_WARN,
	CAR_SIG_DISTANCE_ALERT,
	CAR_SIG_TJA_ICA,
	CAR_SIG_STEERING_FORCE_MODE,
	CAR_SIG_STEERING_MODE,
	CAR_SIG_MOODLAMP,
	CAR_SIG_MOODLAMP_BRIGHT,
	CAR_SIG_MOODLAMP_COLOR,
	CAR_SIG_BREATHING_LAMP,
	CAR_SIG_MUSICAL_RHYTHM,		//30
	CAR_SIG_DRIVE_MODE,
	CAR_SIG_WELCOME_WATER_LAMP,
	CAR_SIG_FRAGRANCE_SW,
	CAR_SIG_FRAGRANCE_SELECT,
	CAR_SIG_FRAGRANCE_LEVEL,
	CAR_SIG_AC_REQUSET,
	CAR_SIG_BLOWSPEED_LEVEL,
	CAR_SIG_SET_TEMPERATURE,
	CAR_SIG_CIRCULATION_MODE,
	CAR_SIG_MODEADJUST,			///40
	CAR_SIG_REARDEFROST_MODE ,
	CAR_SIG_ANIONPURIFY,
	CAR_SIG_SEAT_MEMO,
	CAR_SIG_UNLOCK_BREATHING,
	CAR_SIG_LOCK_BREATHING,
	CAR_SIG_LIGHT_SHOW,
	CAR_SIG_FATC_DELAY,
	CAR_SIG_FATC_BT_BLOW,
	CAR_SIG_FATC_FIRST_BLOW,
	CAR_SIG_DVR_REC,		//50
	CAR_SIG_DMS_DETECT_USERID_RESULT,
	CAR_SIG_DMS_PERSONALSETSEAT,
	CAR_SIG_DMS_PERSONALSETBEAM,
	CAR_SIG_AVMOPVIEW,
	CAR_SIG_DETECTBABYNEEDNIGHT,			//55
	CAR_SIG_TRIGGER_ALARM,
	CAR_SIG_VEHICHLE_WINDOW_COM,
	CAR_SIG_WIFI_NAME_COM,
	CAR_SIG_WIFI_PASSWORD_COM,
	CAR_SIG_NFC_BT_ADDR_COM,				//60
	CAR_SIG_BT_INCOMECALLING_COM,
	CAR_SIG_MONITER_TOUNCH_COM,
	CAR_SIG_R_GEAR_STATUS,
	CAR_SIG_RVC_AVM_STATUS,
	CAR_SIG_SCREEN_SAVE_MODE,
	CAR_SIG_SCREEN_OFF_MODE,
	CAR_SIG_SUN_ROOF,
	CAR_SIG_RESET_FACE_ID,
	CAR_SIG_AIDTURNINGILLUMINATION,
	CAR_SIG_SET_TIME_TYPE,						//70
	CAR_SIG_SET_WASH_CAR,
	CAR_SIG_SET_POLLING,
	CAR_SIG_CALIBRATIONREQUEST,
	CAR_SIG_LDW_SWITCH,
	CAR_SIG_SET_STREERINGWHEEL,
	CAR_SIG_AVM_SET_LANG,
	CAR_SIG_SET_RADARWARNING,
	CAR_SIG_DEFDISPMODE,
	CAR_SIG_OP_TP_X,
	CAR_SIG_OP_TP_Y,									//80
	CAR_SIG_SET_WORK_MODE,
	CAR_SIG_SET_SCREEN,
	CAR_SIG_SET_DEFAULT,
	CAR_SIG_FRONTOFF_REQ,
	CAR_SIG_TEMPKNOB_ROLLINGCOUNTER_REQ,
	CAR_SIG_CONTROL_MODE_REQ,
	CAR_SIG_ENGINE_CONTROL_REQ,
	CAR_SIG_SYNC_SW_REQ,
	CAR_SIG_SET_MUSIC_LOUDNESS,
	CAR_SIG_TIMEGAPSET_REQ,					//90
	CAR_SIG_FCW_ON_OFF,
	CAR_SIG_FCW_OPTION,
	CAR_SIG_TIMEGAPLAST_SET_REQ,
	CAR_SIG_CURRENT_TIME,
	CAR_SIG_NAVI_1_COMMAND,
	CAR_SIG_NAVI_2_COMMAND,
	CAR_SIG_NAVI_3_COMMAND,
	CAR_SIG_RADIO_1_COMMAND,
	CAR_SIG_RADIO_2_COMMAND,
	CAR_SIG_RADIO_3_COMMAND,				//100
	CAR_SIG_RADIO_4_COMMAND,
	CAR_SIG_RADIO_5_COMMAND,
	CAR_SIG_AUTOSTATE,
	CAR_SIG_AUDIO_FACTORY_RESET,
	CAR_SIG_WIFI_ADDR,		//105
	CAR_SIG_DVD_APA_SW,
	CAR_SIG_BT_PHONE_Sts,
	CAR_SIG_AIR_CONDITIONER=115,
	CAR_SIG_ISS_SW,
};

enum
{
	AUIDO_MODE_POWR_OFF = 0,
	AUIDO_MODE_MEDIA,
	AUIDO_MODE_RADIO,
	AUIDO_MODE_BT_PHONE,
	AUIDO_MODE_BCALL,
};

enum
{
	RADIO_SEEK_UP = 1,
	RADIO_SEEK_DN = 2,
	RADIO_FUNC_CANCEAL =0x80,
};

enum
{
	EQ_MODE_IDLE = 0,
	EQ_MODE_STANDARD,
	EQ_MODE_POP,
	EQ_MODE_ROCK,
	EQ_MODE_JAZZ,
	EQ_MODE_CLASSIC,
	EQ_MODE_HUMAN_VOICE,
	EQ_MODE_CUSTOM,
};

enum
{
	EQ_TYPE_TREBLE =1,
	EQ_TYPE_MID,
	EQ_TYPE_BASS,
};

enum
{
	VOL_TYPE_MEDIA = 1,
	VOL_TYPE_RADAR,
	VOL_TYPE_INSTRUMENT,
	VOL_TYPE_KEY_BUTTON,
	VOL_TYPE_PHONE,				//phone, b-call
	VOL_TYPE_NAVI,
	VOL_TYPE_VOICE,
	VOL_TYPE_BCALL,
};

enum
{
	EQ_CONT_FADER=0,
	EQ_CONT_BALANCE,
};

enum
{
	MIX_AUIDO_SRC_IDLE = 0,
	MIX_AUIDO_SRC_MEDIA,
	MIX_AUIDO_SRC_RADIO,
	MIX_AUIDO_SRC_NAVI,
	MIX_AUIDO_SRC_PHONE,
	MIX_AUIDO_SRC_BCALL,
};

enum
{
	SET_MUSICLOUDNESS = 1,
	SET_SVDC,
	SET_ACOUSTIC_FIELD,
	SET_DEFAULT_MAXVOL,
	SET_SOUND_WAVE_MODE,
	SET_LOUDNESS,
	SET_SURROUND_SOUND,
};

enum
{
	VOL_KEEP_ON = 0,
	VOL_CUR_AUIDO_HALF,
	VOL_NEW_AUDIO_HALF,
};

enum
{
	ENGINEER_MODE_SYSTEM				= 0x10,
	ENGINEER_MODE_AUDIO					= 0x20,
	ENGINEER_MODE_RADIO					= 0x30,
	ENGINEER_MODE_RADIO_LEVEL,
	ENGINEER_MODE_RADIO_FM_MULTIPATH,
	ENGINEER_MODE_RADIO_FM_USN,
	ENGINEER_MODE_RADIO_FM_OFFSET,
	ENGINEER_MODE_RADIO_FM_BANDWIDTH,
	ENGINEER_MODE_RADIO_FM_AGC,
	ENGINEER_MODE_RADIO_FM_HIGHCUT_TIME,
	ENGINEER_MODE_RADIO_FM_HIGHCUT_LEVEL,
	ENGINEER_MODE_RADIO_FM_HIGHCUT_NOISE,
	ENGINEER_MODE_RADIO_FM_HIGHCUT_MPH,
	ENGINEER_MODE_RADIO_FM_HIGHCUT_MAX,
	ENGINEER_MODE_AGING					= 0x40,
};


#define chksum(a)	 a&0xff

#define BusyWait(_if, nLoop) { \
    auto unsigned long _i; \
    for(_i=0; _i < nLoop; _i++) { \
        if(_if) break; \
    } \
}

typedef struct tagSendMSGbuf
{
	uint8 state;
	uint8 message;
	uint16 msg_cnt;
	uint8 *buf;
	uint16 len;
	uint8 trycnt;
	uint8 flag;
	uint32 timeout;
	uint32 timer;
}SendMSGbuf;

typedef struct tagMSGbuf
{
	uint8 state;
	uint8 message;
	uint16 msg_cnt;
	uint8 *buf;
	uint16 len;
}MSGbuf;

typedef struct tagMSGPacketInfo
{
	uint8 STX;
	uint16 LEN;
	uint16 Data_cnt;
	uint8 flag;
	uint32 Packet_cnt;
	uint8 State;
	uint16 ptr;
	uint8 utCnt;
	uint8 temp;
	uint8 temp2;
	uint16 buf_end;
}MSGPacketInfo;

typedef union tagMSGKeyInfo
{
	uint32 KeyInfoBuff;

	struct
	{
		uint8 SB_L_UP:2;
		uint8 SB_L_DW:2;
		uint8 SB_L_LEFT:2;
		uint8 SB_L_RIGHT:2;

		uint8 SB_L_OK:2;
		uint8 SB_L_WECHAT:2;
		uint8 SB_L_USER:2;
		uint8 SB_R_UP:2;

		uint8 SB_R_DW:2;
		uint8 SB_R_LEFT:2;
		uint8 SB_R_RIGHT:2;
		uint8 SB_R_OK:2;

		uint8 SB_R_RETURN:2;
		uint8 SB_R_SPEECH:2;
		uint8 SB_SAND:2;
		uint8 Reserved0:2;
	};
}MSGKeyInfo;
uint8 uart0_tx_test_buf[2048];		// for test
extern uint8 uart0_tx_test_buf[2048];		// for test

uint8 CAN_10MS_BUF[200];
uint8 CAN_20MS_BUF[200];
uint8 CAN_50MS_BUF[200];
uint8 CAN_200MS_BUF[200];
uint8 CAN_100MS_BUF[200];
uint8 CAN_500MS_BUF[200];
uint8 CAN_1000MS_BUF[200];

extern uint8 CAN_10MS_BUF[200];
extern uint8 CAN_20MS_BUF[200];
extern uint8 CAN_50MS_BUF[200];
extern uint8 CAN_200MS_BUF[200];
extern uint8 CAN_100MS_BUF[200];
extern uint8 CAN_500MS_BUF[200];
extern uint8 CAN_1000MS_BUF[200];

#ifdef NEW_URAT_RX0
extern  QUE_t uart0_rx_q;
#endif
#ifdef NEW_URAT_RX1
extern  QUE_t uart1_rx_q;
#endif
#ifdef NEW_URAT_TX
extern  QUE_t uart0_tx_q;
extern  QUE_t uart1_tx_q;
#endif

extern SendMSGbuf acc_packet;
extern SendMSGbuf radio_packet;
extern SendMSGbuf key_packet;
extern SendMSGbuf fm_check_radio_packet;
extern SendMSGbuf dtc_list_cmd;
extern SendMSGbuf dvr_ctl_cmd;
extern SendMSGbuf acc_ign_status;
extern SendMSGbuf radio_signal_status;


extern SendMSGbuf CAR_SIG_GEAR;		// 1
extern SendMSGbuf CAR_SIG_STR_ANGLE;
extern SendMSGbuf CAR_SIG_SPEED;
extern SendMSGbuf CAR_SIG_SEATSTATUS;
extern SendMSGbuf CAR_SIG_FUEL;		// 5
extern SendMSGbuf CAR_SIG_DTE;	
extern SendMSGbuf CAR_SIG_DVR1;		
extern SendMSGbuf CAR_SIG_SEATBELT;
extern SendMSGbuf CAR_SIG_CEM_BCM1;
extern SendMSGbuf CAR_SIG_CEM_BCM2;
extern SendMSGbuf CAR_SIG_CEM_BCM3;
extern SendMSGbuf CAR_SIG_CEM_BCM4;
extern SendMSGbuf CAR_SIG_ABS_ESP5;
extern SendMSGbuf CAR_SIG_PM25;
extern SendMSGbuf CAR_SIG_BSD1;
extern SendMSGbuf CAR_SIG_CEM_RADAR1;
extern SendMSGbuf CAR_SIG_TURNLIGHT;
extern SendMSGbuf CAR_SIG_ODOMETER;
extern SendMSGbuf CAR_SIG_AVM_APA4;
extern SendMSGbuf CAR_SIG_AVM_APA2;
extern SendMSGbuf CAR_SIG_AVM_APA3;
extern SendMSGbuf CAR_SIG_AVM2;
extern SendMSGbuf CAR_SIG_FCM2;
extern SendMSGbuf CAR_SIG_FRM3;
extern SendMSGbuf CAR_SIG_WPC;
extern SendMSGbuf CAR_SIG_CEM_ALM1;
extern SendMSGbuf CAR_SIG_TBOX2;
extern SendMSGbuf CAR_SIG_ENGINESPEED;
extern SendMSGbuf CAR_SIG_CLT;
extern SendMSGbuf CAR_SIG_ROLLCOUNT;
extern SendMSGbuf CAR_SIG_MFS_HEARTRATE;
extern SendMSGbuf CAR_SIG_AFU;
extern SendMSGbuf CAR_SIG_AMP;
extern SendMSGbuf CAR_SIG_CEM_IPM;
extern SendMSGbuf CAR_SIG_ICM2;
extern SendMSGbuf CAR_SIG_ICM4;
extern SendMSGbuf CAR_SIG_CEM_PEPS;
extern SendMSGbuf CAR_SIG_PLG1;
extern SendMSGbuf CAR_SIG_CGW_EPS_G;
extern SendMSGbuf CAR_SIG_CGW_ABM_G;

extern SendMSGbuf Last_Memory;

extern SendMSGbuf CGW_VCU_P_5_123;	// Cycle : 10ms
extern SendMSGbuf CGW_BCM_MFS_1_294;

extern SendMSGbuf CGW_BCM_1_245;	// Cycle :20ms
extern SendMSGbuf CGW_EGS_1_215;
extern SendMSGbuf CGW_EPB_1_231;
extern SendMSGbuf CGW_ICM_1_271;
extern SendMSGbuf CGW_BCS_C_1_225;
extern SendMSGbuf CGW_BMS_3_25A;
extern SendMSGbuf CGW_EPS_1_238;

extern SendMSGbuf CGW_BCM_PEPS_1_291;	// Cycle :50ms
extern SendMSGbuf CGW_BSD_1_298;
extern SendMSGbuf CGW_VCU_3_4_28A;
extern SendMSGbuf CGW_FCM_10_3FC;
extern SendMSGbuf CGW_FCM_2_307;
extern SendMSGbuf CGW_FCM_6_3DC;
extern SendMSGbuf CGW_FCM_7_3DE;
extern SendMSGbuf CGW_FCM_FRM_6_387;
extern SendMSGbuf CGW_FCM_FRM_8_3ED;
extern SendMSGbuf CGW_FCM_FRM_9_3FA;

extern SendMSGbuf ACCM_1_3D2;			// Cycle :100ms
extern SendMSGbuf CGW_BCM_4_324;
extern SendMSGbuf CGW_BCM_5_323;
extern SendMSGbuf BCM_6_457;
extern SendMSGbuf BCM_TPMS_1_326;
extern SendMSGbuf ICM_2_3B1;
extern SendMSGbuf BCM_ALM_45D;

extern SendMSGbuf CGW_BCM_SCM_3_45C;
extern SendMSGbuf CGW_BMS_3_46B;
extern SendMSGbuf CGW_BCM_SCM_1_45A;
extern SendMSGbuf CGW_RCM_1_328;
extern SendMSGbuf CGW_BCM_EAC_1_3D8;
extern SendMSGbuf CGW_SAS_2_504;

extern SendMSGbuf CGW_VCU_3_6_313;
extern SendMSGbuf CGW_VCU_3_7_314;
extern SendMSGbuf CGW_VCU_B_5_315;
extern SendMSGbuf CGW_BMS_9_355;
extern SendMSGbuf CGW_FCM_5_4DD;
extern SendMSGbuf CGW_FCM_FRM_5_4E3;
extern SendMSGbuf CGW_PLG_1_32A;

extern SendMSGbuf CGW_BCM_FM_458;	// Cycle :200ms
extern SendMSGbuf CGW_VCU_3_12_347;

extern SendMSGbuf CGW_BCM_TPMS_2_455;	// Cycle :500ms
extern SendMSGbuf CGW_SCM_R_45B;
extern SendMSGbuf TBOX_4_7_4A8;
extern SendMSGbuf CGW_OBC_1_461;
extern SendMSGbuf ACCM_3_3D4;

extern SendMSGbuf AVAS_1_4B1;		// Cycle :1000ms
extern SendMSGbuf WCM_1_4BA;

extern SendMSGbuf DVR_1_3A8;      // DVR·´À¡


#ifdef CX62C_FUNCTION
extern TpNamebuf Road_Name;
extern TpNamebuf Navi_Infomation;
extern TpNamebuf Artist_Name;
extern TpNamebuf Song_Name;
extern TpNamebuf Phone_Number;
extern TpNamebuf Call_Name;
extern TpNamebuf Phone_Information;

extern uint8 displayclear_onoff;
#endif
extern SendMSGbuf diag_usb_device;
#ifdef CAN_RXTX_TEST_FUNC
extern SendMSGbuf uart_rxtx_test[10];
#endif
extern uint8 wifi_address[6];
extern uint8 bt_address[6];

extern MSGPacketInfo UART_Packet_Info;
extern MSGKeyInfo KeyInfo_323;

void Uart_Init_Comm(void);
void UART_Comm_Proc(DEVICE *dev);
void Uart_Send_Func(const SendMSGbuf *msg);
void Uart_Send_Func1(const SendMSGbuf *msg);
void Uart_Response(const MSGbuf *msg);
void Uart_Rx_Set_Default_Data(void);
void Uart_Rx_Check_Set_Init_Data(void);

BUTTON_TX_t* GetButtonTxData(void);

void CAN_Msg_Tx_Set_Process(DEVICE *dev, uint8* rx_data, uint8 tmp8);

uint16 X50_Uart_Resp_Cmd(const MSGbuf *msg, uint16 Busy_length);

#endif /* UART_COMM_H_ */
