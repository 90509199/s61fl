/*
 * shellcmd.c
 *
 *  Created on: 2018. 8. 29.
 *      Author: Digen
 */

#include "includes.h"
#include "Diag/dtc_mgr.h"

uint8 shell_system_mode = OFF;
uint8	shell_volume= 0;
uint8	shell_audio_src =0;
uint8 shell_lound =0;
uint8 shell_geq_onoff =0;

uint16 shell_radio_freq = 0;
uint32 shell_value_0 = 0;
uint32 shell_value_1 =0;
uint8  u8TestSleepCmd = 0u;

static void ResetCmd(uint8_t _argc, uint8_t* _argv[]);
static void Dim_CtrlCmd(uint8_t _argc, uint8_t* _argv[]);
static void PWM_FreqCtrlCmd(uint8_t _argc, uint8_t* _argv[]);
static void Alive_CtrlCmd(uint8_t _argc, uint8_t* _argv[]);
static void Data_ClaerCmd(uint8_t _argc, uint8_t* _argv[]);

//MCU TEST CMD
static void Debug_CtrlCmd(uint8_t _argc, uint8_t* _argv[]);
static void FM_Set_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void FM_Seek_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void FM_Tune_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Radio_Test_Cmd(uint8_t _argc, uint8_t* _argv[]);

static void Voluem_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Sony_Eq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Audio_Src_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Sound_Debug_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Dev_addr_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Test_System_Mode(uint8_t _argc, uint8_t* _argv[]);

static void Geq1_Gain_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq1_Freq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq1_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq2_Gain_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq2_Freq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq2_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq3_Gain_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq3_Freq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq3_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq4_Gain_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq4_Freq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq4_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq5_Gain_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq5_Freq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq5_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq6_Gain_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq6_Freq_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Geq6_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void GeqOnOff_Cmd(uint8_t _argc, uint8_t* _argv[]);

static void loundness_Cmd(uint8_t _argc, uint8_t* _argv[]);

static void Test_Cmd(uint8_t _argc, uint8_t* _argv[]);
static void Test_Cmd1(uint8_t _argc, uint8_t* _argv[]);

static void Test_CPU_UPGRADE(uint8_t _argc, uint8_t* _argv[]);//yun220303

static void Fram_Cmd(uint8_t _argc, uint8_t* _argv[]);

static const SHELL_CMD_t user_cmd[] =
{
	{
		(int8_t*)"reset", ResetCmd,
		(int8_t*)"mcu reset\n",
		(int8_t*)"usage : MCU> reset\n"
	},
	{
		(int8_t*)"duty", Dim_CtrlCmd,
		(int8_t*)"dim cmd\n",
		(int8_t*)"usage : MCU> duty up/dn/num\n"
				 "example : MCU> duty up\n"
				 "          MCU> duty dn\n"
				 "          MCU> duty 100\n"
	},
	{
		(int8_t*)"freq", PWM_FreqCtrlCmd,
		(int8_t*)"feq cmd\n",
		(int8_t*)"usage : MCU> freq <FREQ VALUE>\n"
				 "example : MCU> freq 10k\n"
				 "example : MCU> freq 500\n\N"
				 "<VALID FREQ VALUE>\n"
				 "100 ; 200 ; 300 ; 400 ; 500 ; 600 ; 700 ; 800; 900 ; \n"
				 "1k ; 2k ; 3k ; 4k ; 5k ; 6k ; 7k ~ 20k;\n"
	},
	{
	(int8_t*)"alive", Alive_CtrlCmd,
	(int8_t*)"alive Signal cmd\n",
	(int8_t*)"usage : MCU> alive enable/disable\n"
	},
	{
		(int8_t*)"clear", Data_ClaerCmd,
		(int8_t*)"eep_clear\n",
		(int8_t*)"example : MCU> eep_clear\n"
	},
	{
		(int8_t*)"debug", Debug_CtrlCmd,
		(int8_t*)"debug no,all <arg1>\n",
		(int8_t*)"example : MCU> debug value\n"
	},
	{
		(int8_t*)"sony", Sony_Eq_Cmd,
		(int8_t*)"sony cmd <arg1>\n",
		(int8_t*)"usage : MCU> sony on/off\n"
	},
	{
		(int8_t*)"radio", FM_Set_Cmd,
		(int8_t*)"radio cmd <arg1>\n",
		(int8_t*)"usage : MCU> radio <period[ms]>\n"
				 "example : MCU> radio 2000\n"
				 "example : MCU> radio 0 => (periodic cmd stop)\n"
	},
	{
		(int8_t*)"seek", FM_Seek_Cmd,
		(int8_t*)"seek cmd <arg1>\n",
		(int8_t*)"usage : MCU> seek up/dn\n"
	},
	{
		(int8_t*)"tune", FM_Tune_Cmd,
		(int8_t*)"tune cmd <arg1>\n",
		(int8_t*)"usage : MCU> tune up/dn\n"
	},
	{
		(int8_t*)"rset", Radio_Test_Cmd,
		(int8_t*)"rset cmd <arg1>\n",
		(int8_t*)"usage : MCU> rset fmon/fmoff/amon/amoff\n"
	},
	{
		(int8_t*)"vol", Voluem_Cmd,
		(int8_t*)"vol cmd <arg1>\n",
		(int8_t*)"usage : MCU> vol mute/unmute\n"
				 "example : MCU> vol value\n"
	},
	{
		(int8_t*)"audio", Audio_Src_Cmd,
		(int8_t*)"audio r/vr/hfp/m/a2dp \n",
		(int8_t*)"usage : MCU> audio  r/vr/hfp/m/a2dp\n"
	},
	{
		(int8_t*)"dsp", Sound_Debug_Cmd,
		(int8_t*)"dsp reg/init/a2b/reset/ \n",
		(int8_t*)"usage : MCU> dsp  reg/init/a2b/reset/\n"
	},
	{
		(int8_t*)"g1gain", Geq1_Gain_Cmd,
		(int8_t*)"g1gain value\n",
		(int8_t*)"usage : MCU> g1gain  value \n"
	},
	{
		(int8_t*)"g1freq", Geq1_Freq_Cmd,
		(int8_t*)"g1freq value\n",
		(int8_t*)"usage : MCU>g1freq  value \n"
	},
	{
		(int8_t*)"g1q", Geq1_Qfactor_Cmd,
		(int8_t*)"g1q value \n",
		(int8_t*)"usage : MCU>g1q  value \n"
	},
	{
		(int8_t*)"g2gain", Geq2_Gain_Cmd,
		(int8_t*)"g2gain value\n",
		(int8_t*)"usage : MCU>g2gain  value \n"
	},
	{
		(int8_t*)"g2freq", Geq2_Freq_Cmd,
		(int8_t*)"g2freq value\n",
		(int8_t*)"usage : MCU>g2freq  value \n"
	},
	{
		(int8_t*)"g2q", Geq2_Qfactor_Cmd,
		(int8_t*)"g2q value \n",
		(int8_t*)"usage : MCU>g2q  value \n"
	},
	{
		(int8_t*)"g3gain", Geq3_Gain_Cmd,
		(int8_t*)"g3gain value\n",
		(int8_t*)"usage : MCU>g3gain  value \n"
	},
	{
		(int8_t*)"g3freq", Geq3_Freq_Cmd,
		(int8_t*)"g3freq value\n",
		(int8_t*)"usage : MCU>g3freq  value \n"
	},
	{
		(int8_t*)"g3q", Geq3_Qfactor_Cmd,
		(int8_t*)"g3q value \n",
		(int8_t*)"usage : MCU>g3q  value \n"
	},
	{
		(int8_t*)"g4gain", Geq4_Gain_Cmd,
		(int8_t*)"g4gain value\n",
		(int8_t*)"usage : MCU> g4gain  value \n"
	},
	{
		(int8_t*)"g4freq", Geq4_Freq_Cmd,
		(int8_t*)"g4freq value\n",
		(int8_t*)"usage : MCU>g4freq  value \n"
	},
	{
		(int8_t*)"g4q", Geq4_Qfactor_Cmd,
		(int8_t*)"g4q value \n",
		(int8_t*)"usage : MCU>g4q  value \n"
	},
	{
		(int8_t*)"g5gain", Geq5_Gain_Cmd,
		(int8_t*)"g5gain value\n",
		(int8_t*)"usage : MCU>g5gain  value \n"
	},
	{
		(int8_t*)"g5freq", Geq5_Freq_Cmd,
		(int8_t*)"g5freq value\n",
		(int8_t*)"usage : MCU>g5freq  value \n"
	},
	{
		(int8_t*)"g5q", Geq5_Qfactor_Cmd,
		(int8_t*)"g5q value \n",
		(int8_t*)"usage : MCU>g5q  value \n"
	},
	{
		(int8_t*)"g6gain", Geq6_Gain_Cmd,
		(int8_t*)"g6gain value\n",
		(int8_t*)"usage : MCU>g6gain  value \n"
	},
	{
		(int8_t*)"g6freq", Geq6_Freq_Cmd,
		(int8_t*)"g6freq value\n",
		(int8_t*)"usage : MCU>g6freq  value \n"
	},
	{
		(int8_t*)"g6q", Geq6_Qfactor_Cmd,
		(int8_t*)"g6q value \n",
		(int8_t*)"usage : MCU>g6q  value \n"
	},
	{
		(int8_t*)"geq", GeqOnOff_Cmd,
		(int8_t*)"geq on/off \n",
		(int8_t*)"usage : MCU>geq  value \n"
	},
	{
		(int8_t*)"lound", loundness_Cmd,
		(int8_t*)"lound value \n",
		(int8_t*)"usage : MCU>lound  value \n"
	},
	{
		(int8_t*)"tmod", Test_System_Mode,
		(int8_t*)"tmode on/off \n",
		(int8_t*)"usage : MCU> tmod  on/off \n"
	},
	{
		(int8_t*)"test", Test_Cmd,
		(int8_t*)"test value \n",
		(int8_t*)"usage : MCU> test value\n"
	},
	{
		(int8_t*)"test1", Test_Cmd1,
		(int8_t*)"test1 value \n",
		(int8_t*)"usage : MCU> test1 value\n"
	},
	{
		(int8_t*)"addr", Dev_addr_Cmd,
		(int8_t*)"addr bt/wifi \n",
		(int8_t*)"usage : MCU> addr bt/wifi\n"
	},
		{
		(int8_t*)"upgrade", Test_CPU_UPGRADE,
		(int8_t*)"upgrade cpu\n\r",
		(int8_t*)"usage : MCU> upgrade cpu\n\r"			//yun220303 : Changed the command as the cause of the screen off at bootup issue. 
	},
	{
		(int8_t*)"fram", Fram_Cmd,
		(int8_t*)"fram init \n",
		(int8_t*)"usage : MCU> erase fram\n"
	},
};

SHELLDRV_OBJECT_t user_shellObj;

void InitDrvShell()
{

#if DEBUT_UART_PORT == 0
	user_shellObj.GetCharFunc = (GetCharFuncType)ugetc0;
	user_shellObj.PutCharFunc = (PutCharFuncType)uputc0;
	user_shellObj.KbhitFunc = kbhit0;
#elif DEBUT_UART_PORT == 1
	user_shellObj.GetCharFunc = (GetCharFuncType)ugetc1;
	user_shellObj.PutCharFunc = (PutCharFuncType)uputc1;
	user_shellObj.KbhitFunc = kbhit1;
#elif DEBUT_UART_PORT == 2
	user_shellObj.GetCharFunc = (GetCharFuncType)ugetc2;
	user_shellObj.PutCharFunc = (PutCharFuncType)uputc2;
	user_shellObj.KbhitFunc = kbhit2;
#endif

	user_shellObj.cmdList = (SHELL_CMD_t*)user_cmd;
	user_shellObj.cmdListCnt = sizeof(user_cmd) / sizeof(SHELL_CMD_t);

	InitShellDrv(&user_shellObj);
}

void Shell_Task()
{
	ShellDrvTask(&user_shellObj);
}

static void ResetCmd(uint8_t _argc, uint8_t* _argv[])
{
	DPRINTF(DBG_ERR,"SystemSoftwareReset	13\n\r");
	save_last_memory();
	delay_ms(100);
	SystemSoftwareReset();
}

static void Dim_CtrlCmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t duty;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	duty = (uint8_t)atoi((char*)_argv[0]);

	if (strcmp((char*)_argv[0], "up") == 0)
	{
	}
	else if (strcmp((char*)_argv[0], "dn") == 0)
	{
	}
	else if (strcmp((char*)_argv[0], "0") == 0)
	{
	}
	else if ((duty > 0) && (duty <= 100))
	{
//		SetBrightness(duty);
		FTM3_PWM_Duty(duty);
//		dim_cnt ^=1;
//		if(dim_cnt)
//			FTM3_stop_counter();
//		else
//			FTM3_start_counter();
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!\n");
		return;
	}

}

static void PWM_FreqCtrlCmd(uint8_t _argc, uint8_t* _argv[])
{
}

static void Alive_CtrlCmd(uint8_t _argc, uint8_t* _argv[])
{
}

static void Data_ClaerCmd(uint8_t _argc, uint8_t* _argv[])
{
	DPRINTF(DBG_INFO, "%s\n",__FUNCTION__);
}

static void Debug_CtrlCmd(uint8_t _argc, uint8_t* _argv[])
{
	#if 1

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}
	if (strcmp((char*)_argv[0], "no") == 0)
		debugLevel = DBG_NO_MSG;
	else if (strcmp((char*)_argv[0], "err") == 0)
		debugLevel = DBG_ERR;
	else if (strcmp((char*)_argv[0], "warn") == 0)
		debugLevel = DBG_WARN;
	else if (strcmp((char*)_argv[0], "info") == 0)
		debugLevel = DBG_INFO;
	else if (strcmp((char*)_argv[0], "m1") == 0)
		debugLevel = DBG_MSG1;
	else if (strcmp((char*)_argv[0], "m2") == 0)
		debugLevel = DBG_MSG2;
	else if (strcmp((char*)_argv[0], "m3") == 0)
		debugLevel = DBG_MSG3;
	else if (strcmp((char*)_argv[0], "m4") == 0)
		debugLevel = DBG_MSG4;
	else if (strcmp((char*)_argv[0], "m5") == 0)
		debugLevel = DBG_MSG5;
	else if (strcmp((char*)_argv[0], "m6") == 0)
		debugLevel = DBG_MSG6;
	else if (strcmp((char*)_argv[0], "m7") == 0)
		debugLevel = DBG_MSG7;
	else if (strcmp((char*)_argv[0], "all") == 0)
		debugLevel = DBG_ALL_MSG;
	else if (strcmp((char*)_argv[0], "on") == 0)
	{
		debugenable = ON;
		debug_on = ON;
	}
	else if (strcmp((char*)_argv[0], "off") == 0)
	{
		debugenable = OFF;
		debug_on = OFF;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!\n");
		return;
	}

	DPRINTF(DBG_INFO, "debug select : %d \n", debugLevel);
	#else
	if( _argc<1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "on") == 0)
	{
		debugenable = ON;
	}
	else if (strcmp((char*)_argv[0], "off") == 0)
	{
		debugenable = OFF;
	}
	else if (strcmp((char*)_argv[0], "can") == 0)
	{
//		debugLevel = DBG_MSG7;
	}
	else{
		DPRINTF(DBG_ERR, "Invalid Argument!\n");
		return;
	}
	#endif
}

static void FM_Set_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;
	uint8_t tx_packet[5]={1,2,3,4,5};

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	shell_radio_freq = dummy;
	DPRINTF(DBG_INFO, "\n radio freq: %d \n ",  shell_radio_freq);

}

static void FM_Seek_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t seek;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

//	seek = (uint8_t)atoi((char*)_argv[0]);

	if (strcmp((char*)_argv[0], "up") == 0)
	{
		shell_value_0 = 30;
	}
	else if (strcmp((char*)_argv[0], "dn") == 0)
	{
		shell_value_0 = 31;
	}
	else if (strcmp((char*)_argv[0], "all") == 0)
	{
		shell_value_0 = 32;
	}
	else if (strcmp((char*)_argv[0], "mode") == 0)
	{
		shell_value_0 = 33;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!up/dn/all \n");
		return;
	}

}

static void FM_Tune_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t seek;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "up") == 0)
	{
		shell_value_0 = 34;
	}
	else if (strcmp((char*)_argv[0], "dn") == 0)
	{
		shell_value_0 = 35;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!up/dn/all \n");
		return;
	}

}

static void Radio_Test_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t seek;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "fmon") == 0)
	{
		shell_value_0 = 36;
	}
	else if (strcmp((char*)_argv[0], "fmoff") == 0)
	{
		shell_value_0 = 37;
	}
	else if (strcmp((char*)_argv[0], "amon") == 0)
	{
		shell_value_0 = 38;
	}
	else if (strcmp((char*)_argv[0], "amoff") == 0)
	{
		shell_value_0 = 39;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!up/dn/all \n");
		return;
	}

}



static void Voluem_Cmd(uint8_t _argc, uint8_t* _argv[])
	{
	uint8_t value;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	value = (uint8_t)atoi((char*)_argv[0]);

	if (strcmp((char*)_argv[0], "mute") == 0)
	{
		shell_volume = 0xFF;
	}
	else if (strcmp((char*)_argv[0], "unmute") == 0)
	{
		shell_volume = 0xFE;
	}
	else if (( 0<=value) && (value <= 31))
	{
		if(value==0)
			shell_volume = 32;
		else
			shell_volume = value;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument! value:0-40/mute/unmute\n");
		return;
	}

}

static void Sony_Eq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t seek;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "on") == 0)
	{
		 r_sonysound=1;
		shell_value_0 = 60;
		DPRINTF(DBG_INFO, "sony eq on\n");
	}
	else if (strcmp((char*)_argv[0], "off") == 0)
	{
		 r_sonysound=0;
		shell_value_0 = 60;
		DPRINTF(DBG_INFO, "sony eq off\n");
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!up/dn/all \n");
		return;
	}

}

static void Audio_Src_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "r") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_FM;
		DPRINTF(DBG_ALL_MSG, "%s value : Radio \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "bcall") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_BCALL;
		DPRINTF(DBG_ALL_MSG, "%s value : bcall \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "a2dp") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_BT_A2DP;
		DPRINTF(DBG_ALL_MSG, "%s value : a2dp \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "hfp") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_BT_HFP;
		DPRINTF(DBG_ALL_MSG, "%s value : hfp \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "vr") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_VR_AECREF;
		DPRINTF(DBG_ALL_MSG, "%s value : vr \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "m") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_MEDIA;
		DPRINTF(DBG_ALL_MSG, "%s value : media \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "navi") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_NAVI;
		DPRINTF(DBG_ALL_MSG, "%s value : navi \n", __FUNCTION__);
	}
	else if (strcmp((char*)_argv[0], "hi") == 0){
		shell_audio_src = AVNT_USECASE_AUDIOPATH_USB_HICAR_CARLIFE;
		DPRINTF(DBG_ALL_MSG, "%s value : carlife \n", __FUNCTION__);
	}
	else
	{
		shell_audio_src = 0x00;
		DPRINTF(DBG_ERR, "Invalid Argument!\n");
		return;
	}
}

static void Sound_Debug_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "reg") == 0)
	{
		shell_value_0 = 20;
	}
	else if (strcmp((char*)_argv[0], "init") == 0)
	{
		shell_value_0 = 21;
	}
	else if (strcmp((char*)_argv[0], "a2b") == 0)
	{
		shell_value_0 = 22;
	}
	else if (strcmp((char*)_argv[0], "mpass") == 0)
	{
		shell_value_0 = 23;
	}
	else if (strcmp((char*)_argv[0], "rpass") == 0)
	{
		shell_value_0 = 24;
	}
	else if (strcmp((char*)_argv[0], "ppass") == 0)
	{
		shell_value_0 = 25;
	}
	else if (strcmp((char*)_argv[0], "all") == 0)
	{
		shell_value_0 = 26;
	}
	else if (strcmp((char*)_argv[0], "beep") == 0)
	{
		shell_value_0 = 27;
	}
	else if (strcmp((char*)_argv[0], "reset") == 0)
	{
		shell_value_0 = 28;
	}
	else{
		shell_value_0 = 0;
		DPRINTF(DBG_ERR, "Invalid Argument!\n");
	}

}

static void Dev_addr_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "bt") == 0)
	{
		shell_value_0 = 40;
	}
	else if (strcmp((char*)_argv[0], "wifi") == 0)
	{
		shell_value_0 = 41;
	}
	else if (strcmp((char*)_argv[0], "part") == 0)
	{
		shell_value_0 = 42;
	}
	else if (strcmp((char*)_argv[0], "id") == 0)
	{
		shell_value_0 = 43;
	}
	else if (strcmp((char*)_argv[0], "hw") == 0)
	{
		shell_value_0 = 44;
	}
	else if (strcmp((char*)_argv[0], "sw") == 0)
	{
		shell_value_0 = 45;
	}
	else{
		shell_value_0 = 0;
		DPRINTF(DBG_ERR, "Invalid Argument!\n");
	}
}

static void Test_System_Mode(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t seek;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	seek = (uint8_t)atoi((char*)_argv[0]);

	if (strcmp((char*)_argv[0], "on") == 0)
	{
		shell_system_mode = ON;
	}
	else if (strcmp((char*)_argv[0], "off") == 0)
	{
		shell_system_mode = OFF;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!up/dn/all \n");
		return;
	}

}

static void Geq1_Gain_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint8_t)atoi((char*)_argv[0]);

	if(128<dummy)
		geq1.gain = -(int)(0xFF-dummy);		// minus
	else
		geq1.gain =(int)dummy;						

	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq1.gain);

}

static void Geq1_Freq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq1.band_freq= dummy;
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq1.band_freq);

}

static void Geq1_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq1.band_factor = (double)dummy/10;			//uint 0.1
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %2.1f \n ",  __FUNCTION__,geq1.band_factor);

}

static void Geq2_Gain_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint8_t)atoi((char*)_argv[0]);

	if(128<dummy)
		geq2.gain = -(int)(255-dummy);
	else
		geq2.gain =(int)dummy;

	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq2.gain);

}

static void Geq2_Freq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq2.band_freq = dummy;
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq2.band_freq);

}

static void Geq2_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq2.band_factor= (double)dummy/10;			//uint 0.1
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %2.1f \n ",  __FUNCTION__,geq2.band_factor);

}

static void Geq3_Gain_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint8_t)atoi((char*)_argv[0]);

	if(128<dummy)
		geq3.gain = -(int)(255-dummy);
	else
		geq3.gain =(int)dummy;

	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq3.gain);

}


static void Geq3_Freq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq3.band_freq = dummy;
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq3.band_freq);

}

static void Geq3_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq3.band_factor = (double)dummy/10;			//uint 0.1
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %2.1f \n ",  __FUNCTION__,geq3.band_factor );

}


static void Geq4_Gain_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint8_t)atoi((char*)_argv[0]);

	if(128<dummy)
		geq4.gain  = -(int)(255-dummy);
	else
		geq4.gain =(int)dummy;

	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq4.gain);

}


static void Geq4_Freq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq4.band_freq = dummy;
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq4.band_freq);

}

static void Geq4_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq4.band_factor = (double)dummy/10;			//uint 0.1
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %2.1f \n ",  __FUNCTION__,geq4.band_factor);

}


static void Geq5_Gain_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint8_t)atoi((char*)_argv[0]);

	if(128<dummy)
		geq5.gain = -(int)(255-dummy);
	else
		geq5.gain =(int)dummy;

	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq5.gain);

}


static void Geq5_Freq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq5.band_freq = dummy;
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq5.band_freq);

}

static void Geq5_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq5.band_factor = (double)dummy/10;			//uint 0.1
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %2.1f \n ",  __FUNCTION__,geq5.band_factor);

}


static void Geq6_Gain_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint8_t)atoi((char*)_argv[0]);

	if(128<dummy)
		geq6.gain = -(int)(255-dummy);
	else
		geq6.gain =(int)dummy;

	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq6.gain);

}

static void Geq6_Freq_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq6.band_freq = dummy;
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,geq6.band_freq);

}

static void Geq6_Qfactor_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	geq6.band_factor= (double)dummy/10;			//uint 0.1
	shell_value_0 = 50;
	DPRINTF(DBG_INFO, "\n %s : %2.1f \n ",  __FUNCTION__,geq6.band_factor);

}

static void GeqOnOff_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint8_t seek;
	static uint8_t dim_cnt=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	seek = (uint8_t)atoi((char*)_argv[0]);

	if (strcmp((char*)_argv[0], "on") == 0)
	{
		shell_geq_onoff = ON;
	}
	else if (strcmp((char*)_argv[0], "off") == 0)
	{
		shell_geq_onoff = OFF;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid Argument!up/dn/all \n");
		return;
	}
	shell_value_0 = 29;

}


static void loundness_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "\n No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	if(dummy<=14)
	{
		shell_lound =dummy;
		shell_value_0 = 51;
		DPRINTF(DBG_INFO, "\n %s : %d \n ",  __FUNCTION__,shell_lound);
	}
	else
	{
		shell_lound = 0;
		DPRINTF(DBG_ERR, "\n Invalid value!\n");
	}
}

static void Test_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "sleep") == 0)
	{
		if((r_Device.r_System->accign_state.state&0x01) != 1u)
		{
			u8TestSleepCmd = 1u;
			r_Device.r_System->ccl_active = OFF;
			DPRINTF(DBG_INFO, "Start entering into sleep,please wait for 1s -> 5s!\n");
		}
		else
		{
			u8TestSleepCmd = 0u;
			DPRINTF(DBG_INFO, "IG/ACC ON, Cannot enter into sleep!\n");
		}
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	shell_value_0 = dummy;
//	DPRINTF(DBG_ALL_MSG, "%s value : %d \n", __FUNCTION__, shell_value_0);

}

static void Test_Cmd1(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;
	uint8_t tx_packet[5]={1,2,3,4,5};

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (!(*_argv[0] >= '0' && *_argv[0] <= '9'))
	{
		DPRINTF(DBG_ERR, "Invalid value!\n");
		return;
	}

	dummy = (uint32_t)atoi((char*)_argv[0]);

	shell_value_1 = dummy;
//	DPRINTF(DBG_ALL_MSG, "%s value : %d \n", __FUNCTION__, shell_value_1);

}
static void Test_CPU_UPGRADE(uint8_t _argc, uint8_t* _argv[])//yun220303
{
	//SrvMcanHsDeInit();//disable CAN
	//r_Device.r_Tuner->t_state = TUNER_IDLE;
	r_Device.r_Dsp->d_state = DSP_IDLE;
	r_Device.r_System->s_state = SYS_IDLE_MODE;
	DPRINTF(DBG_INFO,"enter soc update mode\n\r");
	r_Device.r_Power->p_state = PWR_RESET_1;
	r_Device.r_System->pshold =OFF;
	r_Device.r_System->alivecleartime = OFF;
	r_Device.r_System->soc_monitor = BOOT_SOC_BOOT_PS_HOLD;
	r_Device.r_System->s_state = SYS_STANDBY_MODE_ENTER;
	MCU_UPGRADE_SOC(ON);
	delay_ms(100);			
	//reboot power
	//deinit can
	// no action
}

static void Fram_Cmd(uint8_t _argc, uint8_t* _argv[])
{
	uint32_t dummy=0;

	if (_argc < 1)
	{
		DPRINTF(DBG_ERR, "No Argument!\n");
		return;
	}

	if (strcmp((char*)_argv[0], "init") == 0)
	{
		if((r_Device.r_System->accign_state.state&0x01) == 1u)
		{
			DPRINTF(DBG_INFO, "Start erasing fram!\n");
			EraseFRAM();
		}
		else
		{
			DPRINTF(DBG_INFO, "IG/ACC OFF, Cannot erase fram!\n");
		}
		return;
	}
	else
	{
		DPRINTF(DBG_ERR, "Invalid cmd!\n");
		return;
	}

//	DPRINTF(DBG_ALL_MSG, "%s value : %d \n", __FUNCTION__, shell_value_0);

}
