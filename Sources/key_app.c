/*
 * key_app.c
 *
 *  Created on: 2019. 1. 4.
 *      Author: Digen
 */

#include "includes.h"

static KEYDRV_DEV_t app_key[KEY_MAX];
static volatile uint8_t key_push_info0;
static volatile uint8_t key_push_info1;
static volatile uint8_t key_push_info2;
#ifdef CX62C_FUNCTION
static volatile uint8_t key_push_info3;
static volatile uint8_t key_push_info4;
#endif

int up_key_cnt = 0;
int down_key_cnt = 0;
int mode_key_cnt = 0;

#ifdef NEW_SCAN_ADC_KEY
#define KEY_SCAN_CNT				25
#define KEY_SCAN_SAMPLE_CNT		15
#define KEY_SCAN_REMOVE_CNT		((KEY_SCAN_CNT-KEY_SCAN_SAMPLE_CNT)/2)		
#endif
#define KPRINTF(lvl, fmt, args...)		DPRINTF(lvl, fmt, ##args)
//#define KPRINTF(lvl, fmt, args...)

static uint8_t GetKey_Power_Stat(void);
static uint8_t GetKey_Back_Stat(void);
static uint8_t GetKey_Set_Stat(void);
static uint8_t GetKey_Home_Stat(void);
static uint8_t GetKey_Enter_Stat(void);
static uint8_t GetKey_AVM_Stat(void);

static uint8_t GetKey_Up_Stat(void);
static uint8_t GetKey_Down_Stat(void);
static uint8_t GetKey_Left_Stat(void);
static uint8_t GetKey_Right_Stat(void);

#ifdef CX62C_FUNCTION
static uint8_t GetKey_Next_Stat(void);
static uint8_t GetKey_Last_Stat(void);
static uint8_t GetKey_Voice_Plus_Stat(void);
static uint8_t GetKey_Voice_Minus_Stat(void);
static uint8_t GetKey_Mode_Stat(void);
static uint8_t GetKey_Voice_Activate_Stat(void);
static uint8_t GetKey_Wechat_Stat(void);
static uint8_t GetKey_PhoneCall_Stat(void);
#endif

static void Key_Power_Push(void);
static void Key_Back_Push(void);
static void Key_Set_Push(void);
static void Key_Home_Push(void);
static void Key_Enter_Push(void);
static void Key_AVM_Push(void);

static void Key_Up_Push(void);
static void Key_Down_Push(void);
static void Key_Left_Push(void);
static void Key_Right_Push(void);

#ifdef CX62C_FUNCTION
static void Key_Next_Push(void);
static void Key_Last_Push(void);
static void Key_Voice_Plus_Push(void);
static void Key_Voice_Minus_Push(void);
static void Key_Mode_Push(void);
static void Key_Voice_Activate_Push(void);
static void Key_Wechat_Push(void);
static void Key_PhoneCall_Push(void);
#endif

static void Key_Power_Release(void);
static void Key_Power_Longkey(void);
static void Key_Power_Repeat(void);

static void Key_Back_Release(void);
static void Key_Set_Release(void);
static void Key_Home_Release(void);
static void Key_Enter_Release(void);
static void Key_Enter_Longkey(void);
static void Key_AVM_Release(void);

static void Key_Up_Release(void);
static void Key_Down_Release(void);
static void Key_Left_Release(void);
static void Key_Right_Release(void);

#ifdef CX62C_FUNCTION
static void Key_Next_Release(void);
static void Key_Last_Release(void);
static void Key_Voice_Plus_Release(void);
static void Key_Voice_Minus_Release(void);
static void Key_Mode_Release(void);
static void Key_Voice_Activate_Release(void);
static void Key_Wechat_Release(void);
static void Key_Wechat_Longkey(void);
static void Key_PhoneCall_Release(void);
static void Key_PhoneCall_Longkey(void);
#endif

#ifdef NEW_SCAN_ADC_KEY
static uint32_t Key_data_Sorting(uint16_t *sortdata,uint8_t len, uint8_t offset);
#endif

void InitDrvKey(void)
{
	uint8_t i;

	for(i=1 ; i<KEY_MAX ; i++)
	{
		memset(&app_key[i], 0x00, sizeof(KEYDRV_DEV_t));
		InitKeyStat(&app_key[i]);
	}

	app_key[KEY_PWR].GetKeyPinStat = GetKey_Power_Stat;
	app_key[KEY_PWR].GetTickCount = GetTickCount;
	app_key[KEY_PWR].Release = Key_Power_Release;
	app_key[KEY_PWR].Push = Key_Power_Push;
	app_key[KEY_PWR].LongKey = Key_Power_Longkey;
	app_key[KEY_PWR].Repeat = Key_Power_Repeat;
	app_key[KEY_PWR].longkey_time = 2000;
	app_key[KEY_PWR].repeat_time = 10000;

	app_key[KEY_BACK].GetKeyPinStat = GetKey_Back_Stat;
	app_key[KEY_BACK].GetTickCount = GetTickCount;
	app_key[KEY_BACK].Release = Key_Back_Release;
	app_key[KEY_BACK].Push = Key_Back_Push;

	app_key[KEY_SET].GetKeyPinStat = GetKey_Set_Stat;
	app_key[KEY_SET].GetTickCount = GetTickCount;
	app_key[KEY_SET].Release = Key_Set_Release;
	app_key[KEY_SET].Push = Key_Set_Push;

	app_key[KEY_HOME].GetKeyPinStat = GetKey_Home_Stat;
	app_key[KEY_HOME].GetTickCount = GetTickCount;
	app_key[KEY_HOME].Release = Key_Home_Release;
	app_key[KEY_HOME].Push = Key_Home_Push;

	app_key[KEY_ENTER].GetKeyPinStat = GetKey_Enter_Stat;
	app_key[KEY_ENTER].GetTickCount = GetTickCount;
	app_key[KEY_ENTER].Release = Key_Enter_Release;
	app_key[KEY_ENTER].Push = Key_Enter_Push;
//	app_key[KEY_ENTER].LongKey = Key_Enter_Longkey;
//	app_key[KEY_ENTER].longkey_time = 2000;

	app_key[KEY_AVM_ONOFF].GetKeyPinStat = GetKey_AVM_Stat;
	app_key[KEY_AVM_ONOFF].GetTickCount = GetTickCount;
	app_key[KEY_AVM_ONOFF].Release = Key_AVM_Release;
	app_key[KEY_AVM_ONOFF].Push = Key_AVM_Push;	

	app_key[KEY_UP].GetKeyPinStat = GetKey_Up_Stat;
	app_key[KEY_UP].GetTickCount = GetTickCount;
	app_key[KEY_UP].Release = Key_Up_Release;
	app_key[KEY_UP].Push = Key_Up_Push;
	
	app_key[KEY_DOWN].GetKeyPinStat = GetKey_Down_Stat;
	app_key[KEY_DOWN].GetTickCount = GetTickCount;
	app_key[KEY_DOWN].Release = Key_Down_Release;
	app_key[KEY_DOWN].Push = Key_Down_Push;

	app_key[KEY_LEFT].GetKeyPinStat = GetKey_Left_Stat;
	app_key[KEY_LEFT].GetTickCount = GetTickCount;
	app_key[KEY_LEFT].Release = Key_Left_Release;
	app_key[KEY_LEFT].Push = Key_Left_Push;

	app_key[KEY_RIGHT].GetKeyPinStat = GetKey_Right_Stat;
	app_key[KEY_RIGHT].GetTickCount = GetTickCount;
	app_key[KEY_RIGHT].Release = Key_Right_Release;
	app_key[KEY_RIGHT].Push = Key_Right_Push;
	
	#ifdef CX62C_FUNCTION
	app_key[KEY_NEXT].GetKeyPinStat = GetKey_Next_Stat;
	app_key[KEY_NEXT].GetTickCount = GetTickCount;
	app_key[KEY_NEXT].Release = Key_Next_Release;
	app_key[KEY_NEXT].Push = Key_Next_Push;

	app_key[KEY_LAST].GetKeyPinStat = GetKey_Last_Stat;
	app_key[KEY_LAST].GetTickCount = GetTickCount;
	app_key[KEY_LAST].Release = Key_Last_Release;
	app_key[KEY_LAST].Push = Key_Last_Push;

	app_key[KEY_VOICE_PLUS].GetKeyPinStat = GetKey_Voice_Plus_Stat;
	app_key[KEY_VOICE_PLUS].GetTickCount = GetTickCount;
	app_key[KEY_VOICE_PLUS].Release = Key_Voice_Plus_Release;
	app_key[KEY_VOICE_PLUS].Push = Key_Voice_Plus_Push;

	app_key[KEY_VOICE_MINUS].GetKeyPinStat = GetKey_Voice_Minus_Stat;
	app_key[KEY_VOICE_MINUS].GetTickCount = GetTickCount;
	app_key[KEY_VOICE_MINUS].Release = Key_Voice_Minus_Release;
	app_key[KEY_VOICE_MINUS].Push = Key_Voice_Minus_Push;

	app_key[KEY_MODE].GetKeyPinStat = GetKey_Mode_Stat;
	app_key[KEY_MODE].GetTickCount = GetTickCount;
	app_key[KEY_MODE].Release = Key_Mode_Release;
	app_key[KEY_MODE].Push = Key_Mode_Push;

	app_key[KEY_VOICE_ACTIVATE].GetKeyPinStat = GetKey_Voice_Activate_Stat;
	app_key[KEY_VOICE_ACTIVATE].GetTickCount = GetTickCount;
	app_key[KEY_VOICE_ACTIVATE].Release = Key_Voice_Activate_Release;
	app_key[KEY_VOICE_ACTIVATE].Push = Key_Voice_Activate_Push;

	app_key[KEY_WECHAT].GetKeyPinStat = GetKey_Wechat_Stat;
	app_key[KEY_WECHAT].GetTickCount = GetTickCount;
	app_key[KEY_WECHAT].Release = Key_Wechat_Release;
	app_key[KEY_WECHAT].Push = Key_Wechat_Push;
	app_key[KEY_WECHAT].LongKey = Key_Wechat_Longkey;
	app_key[KEY_WECHAT].longkey_time = 2000;

	app_key[KEY_PHONE_CALL].GetKeyPinStat = GetKey_PhoneCall_Stat;
	app_key[KEY_PHONE_CALL].GetTickCount = GetTickCount;
	app_key[KEY_PHONE_CALL].Release = Key_PhoneCall_Release;
	app_key[KEY_PHONE_CALL].Push = Key_PhoneCall_Push;
	app_key[KEY_PHONE_CALL].LongKey = Key_PhoneCall_Longkey;
  	app_key[KEY_PHONE_CALL].longkey_time = 2000;
	#endif
}

static uint8_t GetKey_Power_Stat(void)
{
	if (key_push_info0 & 0x01)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Back_Stat(void)
{
	if (key_push_info1 & 0x01)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Set_Stat(void)
{
	if (key_push_info1 & 0x02)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Home_Stat(void)
{
	if (key_push_info1 & 0x04)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Enter_Stat(void)
{
	if (key_push_info1 & 0x08)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_AVM_Stat(void)
{
	if (key_push_info1 & 0x10)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Up_Stat(void)
{
	if (key_push_info2 & 0x01)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Down_Stat(void)
{
	if (key_push_info2 & 0x02)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Left_Stat(void)
{
	if (key_push_info2 & 0x04)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Right_Stat(void)
{
	if (key_push_info2 & 0x08)
		return PUSH;
	else
		return RELEASE;
}

#ifdef CX62C_FUNCTION
static uint8_t GetKey_Next_Stat(void)
{
	if (key_push_info3 & 0x01)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Last_Stat(void)
{
	if(key_push_info3 & 0x02)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Voice_Plus_Stat(void)
{
	if(key_push_info3 & 0x04)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Voice_Minus_Stat(void)
{
	if(key_push_info3 & 0x08)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Mode_Stat(void)
{
	if(key_push_info3 & 0x10)
		return PUSH;
	else
		return RELEASE;
}


static uint8_t GetKey_Voice_Activate_Stat(void)
{
	if(key_push_info4 & 0x01)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_Wechat_Stat(void)
{
	if(key_push_info4 & 0x02)
		return PUSH;
	else
		return RELEASE;
}

static uint8_t GetKey_PhoneCall_Stat(void)
{
	if(key_push_info4 & 0x04)
		return PUSH;
	else
		return RELEASE;
}
#endif

static void Key_Power_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	
	*p_button_tx_data = TX_PWR_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_PWR>>8);
	key_packet.buf[1] = KEY_DEF_PWR;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag  = ON;
	r_Device.r_System->soc_update_powerkey = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Back_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	
	*p_button_tx_data = TX_BACK_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_RETURN>>8);
	key_packet.buf[1] = KEY_DEF_RETURN;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Set_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	

	*p_button_tx_data = TX_SET_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_SETUP>>8);
	key_packet.buf[1] = KEY_DEF_SETUP;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->soc_update_setkey = ON;
	
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Home_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_HOME_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_HOME>>8);
	key_packet.buf[1] = (uint8)KEY_DEF_HOME;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Enter_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_ENTER_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_OK>>8);
	key_packet.buf[1] = (uint8)KEY_DEF_OK;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_AVM_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_AVM_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_AVM_ON_OFF>>8);
	key_packet.buf[1] = (uint8)KEY_DEF_AVM_ON_OFF;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Up_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_UP_KEY;
	
	key_packet.buf[0] = (uint8)(KEY_DEF_UP>>8);
	key_packet.buf[1] = KEY_DEF_UP;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Down_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	*p_button_tx_data = TX_DOWN_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_DN>>8);
	key_packet.buf[1] = KEY_DEF_DN;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->soc_update_downkey = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
	
}

static void Key_Left_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_LEFT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_LEFT>>8);
	key_packet.buf[1] = KEY_DEF_LEFT;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Right_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_RIGHT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_RIGHT>>8);
	key_packet.buf[1] = KEY_DEF_RIGHT;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

#ifdef CX62C_FUNCTION
static void Key_Next_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_NEXT_KEY;
	
	key_packet.buf[0] = (uint8)(KEY_DEF_NEXT_SONG>>8);
	key_packet.buf[1] = KEY_DEF_NEXT_SONG;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Last_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_LAST_KEY;

	key_packet.buf[0] = (uint8)( KEY_DEF_LAST_SONG>>8);
	key_packet.buf[1] =  KEY_DEF_LAST_SONG;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Voice_Plus_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_VOICE_P_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_VOL_UP>>8);
	key_packet.buf[1] = KEY_DEF_VOL_UP;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Voice_Minus_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_VOICE_M_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_VOL_DN>>8);
	key_packet.buf[1] = KEY_DEF_VOL_DN;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Mode_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_MODE_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_MODE>>8);
	key_packet.buf[1] = KEY_DEF_MODE;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Voice_Activate_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_VOICE_ACT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_VOICE>>8);
	key_packet.buf[1] = KEY_DEF_VOICE;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Wechat_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_WECHAT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_WECHAT>>8);
	key_packet.buf[1] = KEY_DEF_WECHAT;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_PhoneCall_Push(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_PHONE_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_HANGUP>>8);
	key_packet.buf[1] = KEY_DEF_PHONE_HANGUP;
	key_packet.buf[2] = MSG_KEY_PRESS;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}
#endif


static void Key_Power_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	
	*p_button_tx_data = TX_PWR_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_PWR>>8);
	key_packet.buf[1] = KEY_DEF_PWR;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	r_Device.r_System->key_active = ON;
	key_packet.flag = ON;	
	r_Device.r_System->soc_update_powerkey = OFF;
	if(r_Device.r_System->power_key_check ==POWER_KEY_LONG){
		r_Device.r_System->power_key_status = POWER_KEY_LONG;
	}
	else if(r_Device.r_System->power_key_check==POWER_KEY_REPEAT){
		r_Device.r_System->power_key_status = POWER_KEY_REPEAT;
	}
	else
		r_Device.r_System->power_key_status = POWER_KEY_SHORT;

	r_Device.r_System->power_key_check = POWER_KEY_IDLE;
		
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Power_Longkey(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_PWR_LONGKEY;
	r_Device.r_System->power_key_check= POWER_KEY_LONG;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Power_Repeat(void)
{
	BUTTON_TX_t* p_button_tx_data;
	r_Device.r_System->power_key_check= POWER_KEY_REPEAT;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Back_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	
	*p_button_tx_data = TX_BACK_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_RETURN>>8);
	key_packet.buf[1] = KEY_DEF_RETURN;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Set_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	
	*p_button_tx_data = TX_SET_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_SETUP>>8);
	key_packet.buf[1] = KEY_DEF_SETUP;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;
	r_Device.r_System->soc_update_setkey = OFF;
	
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_Home_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_HOME_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_HOME>>8);
	key_packet.buf[1] = (uint8)KEY_DEF_HOME;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Enter_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_ENTER_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_OK>>8);
	key_packet.buf[1] = (uint8)KEY_DEF_OK;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Enter_Longkey(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_ENTER_LONGKEY;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_AVM_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_AVM_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_AVM_ON_OFF>>8);
	key_packet.buf[1] = (uint8)KEY_DEF_AVM_ON_OFF;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Up_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_UP_KEY;
	
	key_packet.buf[0] = (uint8)(KEY_DEF_UP>>8);
	key_packet.buf[1] = KEY_DEF_UP;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Down_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();
	*p_button_tx_data = TX_DOWN_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_DN>>8);
	key_packet.buf[1] = KEY_DEF_DN;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;
	r_Device.r_System->soc_update_downkey = OFF;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Left_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_LEFT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_LEFT>>8);
	key_packet.buf[1] = KEY_DEF_LEFT;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Right_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_RIGHT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_RIGHT>>8);
	key_packet.buf[1] = KEY_DEF_RIGHT;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_IDRIVE;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

#ifdef CX62C_FUNCTION
static void Key_Next_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_NEXT_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_NEXT_SONG>>8);
	key_packet.buf[1] = KEY_DEF_NEXT_SONG;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Last_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_LAST_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_LAST_SONG>>8);
	key_packet.buf[1] = KEY_DEF_LAST_SONG;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Voice_Plus_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_VOICE_P_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_VOL_UP>>8);
	key_packet.buf[1] = KEY_DEF_VOL_UP;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Voice_Minus_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_VOICE_M_KEY;

	key_packet.buf[0] = (uint8)( KEY_DEF_VOL_DN>>8);
	key_packet.buf[1] =  KEY_DEF_VOL_DN;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Mode_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_MODE_KEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_MODE>>8);
	key_packet.buf[1] = KEY_DEF_MODE;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Voice_Activate_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_VOICE_ACT_KEY	;

	key_packet.buf[0] = (uint8)(KEY_DEF_VOICE>>8);
	key_packet.buf[1] = KEY_DEF_VOICE;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Wechat_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_WECHAT_KEY	;

	key_packet.buf[0] = (uint8)(KEY_DEF_WECHAT>>8);
	key_packet.buf[1] = KEY_DEF_WECHAT;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_Wechat_Longkey(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_WECHAT_LONGKEY;

	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}


static void Key_PhoneCall_Release(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_PHONE_KEY	;

	key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_HANGUP>>8);
	key_packet.buf[1] = KEY_DEF_PHONE_HANGUP;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}

static void Key_PhoneCall_Longkey(void)
{
	BUTTON_TX_t* p_button_tx_data;
	p_button_tx_data = GetButtonTxData();

	*p_button_tx_data = TX_PHONECALL_LONGKEY;

	key_packet.buf[0] = (uint8)(KEY_DEF_PHONE_CALL>>8);
	key_packet.buf[1] = KEY_DEF_PHONE_CALL;
	key_packet.buf[2] = MSG_KEY_RELEASE;
	key_packet.buf[3] = MSG_KEY_STEERING;
	key_packet.flag = ON;
	r_Device.r_System->key_active = ON;
	KPRINTF(DBG_MSG1, "%s  \n",__FUNCTION__);
}
#endif

void Adc_Key_Parser0(uint32_t key_value)	// 5ms timer_callback
{
	#if 0				//JANG_220111 apply to key scan
	if(key_value){
		key_push_info0 = 0x01;	// POWER KEY
		}
	else
		key_push_info0 = 0x00;	/// INVALID VALUE
	#else
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;

	avg_adc_val1 += key_value;
	avg_adc_cnt++;

	if(key_value==0)
	{
		// no press button			// low or high
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info0 = 0x00;	// INVALID VALUE
	}

	if (avg_adc_cnt > 9)
	{
		avg_adc_cnt = 0;
		avg_adc_val1/= 10;

		if(avg_adc_val1)
		{
			key_push_info0 = 0x01;	// POWER KEY
		}
		else
			key_push_info0 = 0x00;	/// INVALID VALUE
	}
	#endif
}

// 3.3V reference
/*
ADC1			����
0.81				0.65<x<0.95		
1.20       		0.99<x< 1.38      //1.05<x<1.35 		
1.64   			1.45<x<1.78		//1.5<x<1.7
2.40   			2.17<x<2.63		//2.2<x<2.6
*/
void Adc_Key_Parser1(uint32_t key_value)	// 5ms timer_callback
{
	#ifdef NEW_SCAN_ADC_KEY
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;
	static uint16_t avg_adc[KEY_SCAN_CNT] = {0,};

	if((4000< key_value)){
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info1 = 0x00;	/// INVALID VALUE
		return;
	}

	avg_adc_cnt++;

	if(avg_adc_cnt != 0)
	{
		 avg_adc[avg_adc_cnt-1] = key_value;
	}

	if (avg_adc_cnt > KEY_SCAN_CNT)
	{
		avg_adc_val1 = Key_data_Sorting(&avg_adc[0], KEY_SCAN_CNT, KEY_SCAN_REMOVE_CNT);
		
		avg_adc_cnt = 0;
		memset(&avg_adc[0],0x0,sizeof(avg_adc));

		if((1000< avg_adc_val1) && (avg_adc_val1 <= 1450))							//1228
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x01;	// BACK KEY
		}
		else if((1500 < avg_adc_val1) && (avg_adc_val1 <= 2100))				// 1818
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x02;	// SET KEY
		}
		else if((2200< avg_adc_val1) && (avg_adc_val1 <= 2700))					//2485
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x04;	// HOME KEY
		}
		else if((3300 < avg_adc_val1) && (avg_adc_val1 <= 4000))				//3638
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x08;	// ENTER KEY
		}
		else if((avg_adc_val1 <= 500))						//0V
		{
			key_push_info1 = 0x10;	// AVM KEY
		}
		else
			key_push_info1 = 0x00;	/// INVALID VALUE

		//	DPRINTF(DBG_MSG5, "adc_val : %d\n", avg_adc_val1);		
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#else
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;

	if((4000< key_value)){
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info1 = 0x00;	/// INVALID VALUE
		return;
	}

	if(avg_adc_cnt != 0)
	{
		 avg_adc_val1 += key_value;
	}
	else //if (avg_adc_cnt == 0)
	{
		 avg_adc_val1 = 0;
		 avg_adc_val1 += key_value;
	}

	avg_adc_cnt++;

	if (avg_adc_cnt > 9)
	{
		avg_adc_cnt = 0;
		avg_adc_val1/= 10;

		if((1000< avg_adc_val1) && (avg_adc_val1 <= 1450))							//1228
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x01;	// BACK KEY
		}
		else if((1500 < avg_adc_val1) && (avg_adc_val1 <= 2100))				// 1818
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x02;	// SET KEY
		}
		else if((2200< avg_adc_val1) && (avg_adc_val1 <= 2700))					//2485
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x04;	// HOME KEY
		}
		else if((3300 < avg_adc_val1) && (avg_adc_val1 <= 4000))				//3638
		{
			if(r_Device.r_System->s_state != SYS_SCREEN_SAVE_MODE_IDLE)
			key_push_info1 = 0x08;	// ENTER KEY
		}
		else if((avg_adc_val1 <= 500))						//0V
		{
			key_push_info1 = 0x10;	// AVM KEY
		}
		else
			key_push_info1 = 0x00;	/// INVALID VALUE

//			DPRINTF(DBG_MSG5, "adc_val : %d\n", key_value);		
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#endif
}


void Adc_Key_Parser2(uint32_t key_value)	// 5ms timer_callback
{
	#ifdef NEW_SCAN_ADC_KEY
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;
	static uint16_t avg_adc[KEY_SCAN_CNT] = {0,};

	if((4000< key_value)||(r_Device.r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE))
	{
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info2 = 0x00;	/// INVALID VALUE
		return;
	}

	avg_adc_cnt++;

	if(avg_adc_cnt != 0)
	{
		 avg_adc[avg_adc_cnt-1] = key_value;
	}

	if (avg_adc_cnt > KEY_SCAN_CNT)
	{
		avg_adc_val1 = Key_data_Sorting(&avg_adc[0], KEY_SCAN_CNT, KEY_SCAN_REMOVE_CNT);
		avg_adc_cnt = 0;
		memset(&avg_adc[0],0x0,sizeof(avg_adc));

		if((1000< avg_adc_val1) && (avg_adc_val1 <= 1450))							//1228         //810
		{
			key_push_info2 = 0x01;	// UP KEY
		}
		else if((1500 < avg_adc_val1) && (avg_adc_val1 <= 2100))				// 1818			//1200
		{
			key_push_info2 = 0x02;	// DN KEY
		}
		else if((2200< avg_adc_val1) && (avg_adc_val1 <= 2700))					//2485			//1640
		{
			key_push_info2 = 0x04;	// LEFT KEY
		}
		else if((3300 < avg_adc_val1) && (avg_adc_val1 <= 4000))				//3638			//2400
		{
			key_push_info2 = 0x08;	// RIGHT KEY
		}
		else
			key_push_info2 = 0x00;	/// INVALID VALUE


		//DPRINTF(DBG_MSG5_1, "adc_val : %d\n", key_value);
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#else
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;

	if((4000< key_value)||(r_Device.r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE))
	{
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info2 = 0x00;	/// INVALID VALUE
		return;
	}

	if(avg_adc_cnt != 0)
	{
		 avg_adc_val1 += key_value;
	}
	else //if (avg_adc_cnt == 0)
	{
		 avg_adc_val1 = 0;
		 avg_adc_val1 += key_value;
	}

	avg_adc_cnt++;

	if (avg_adc_cnt > 9)
	{
		avg_adc_cnt = 0;
		avg_adc_val1/= 10;

		if((1000< avg_adc_val1) && (avg_adc_val1 <= 1450))							//1228         //810
		{
			key_push_info2 = 0x01;	// UP KEY
		}
		else if((1500 < avg_adc_val1) && (avg_adc_val1 <= 2100))				// 1818			//1200
		{
			key_push_info2 = 0x02;	// DN KEY
		}
		else if((2200< avg_adc_val1) && (avg_adc_val1 <= 2700))					//2485			//1640
		{
			key_push_info2 = 0x04;	// LEFT KEY
		}
		else if((3300 < avg_adc_val1) && (avg_adc_val1 <= 4000))				//3638			//2400
		{
			key_push_info2 = 0x08;	// RIGHT KEY
		}
		else
			key_push_info2 = 0x00;	/// INVALID VALUE


		//DPRINTF(DBG_MSG5_1, "adc_val : %d\n", key_value);
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#endif
}

#ifdef CX62C_FUNCTION
void Cluster_Key_Parser1(uint32_t key_value)
{
	#ifdef NEW_SCAN_ADC_KEY
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;
	static uint16_t avg_adc[KEY_SCAN_CNT] = {0,};

	if((4600< key_value))	//JANG_210828
	{
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info3 = 0x00;	/// INVALID VALUE
		return;
	}

	avg_adc_cnt++;
	
	if(avg_adc_cnt != 0)
	{
		avg_adc[avg_adc_cnt-1] = key_value;
	}

	if (avg_adc_cnt > KEY_SCAN_CNT)
	{
		
		avg_adc_val1 = Key_data_Sorting(&avg_adc[0], KEY_SCAN_CNT, KEY_SCAN_REMOVE_CNT);
		avg_adc_cnt = 0;
		memset(&avg_adc[0],0x0,sizeof(avg_adc));

		if(avg_adc_val1 <= 300)							//0
		{
			#if 0			//JANG_211102
			key_push_info3 = 0x01;	// NEXT KEY
			#else
			key_push_info3 = 0x02;	// LAST KEY
			#endif
		}
		else if((1000< avg_adc_val1) && (avg_adc_val1 <= 1450))							//1219      //804   
		{	
			#if 0			//JANG_211102
		 	key_push_info3 = 0x02;	// LAST KEY
			#else
			key_push_info3 = 0x01;	// NEXT KEY
			#endif
		}
		else if((2200 < avg_adc_val1) && (avg_adc_val1 <= 2650))						// 2476    	//1623
		{
			key_push_info3 = 0x04;	// VOICE PLUS KEY
		}
		else if((3400< avg_adc_val1) && (avg_adc_val1 <= 3850)) 						//3687		//2433
		{
			key_push_info3 = 0x08;	// VOICE MINUS KEY
		}
		else if((4300 < avg_adc_val1) && (avg_adc_val1 <= 4600))						//4470		//2950
		{
				key_push_info3 = 0x10;	// MODE KEY
		}
		else
			key_push_info3 = 0x00;	/// INVALID VALUE

	//		DPRINTF(DBG_MSG5, "cluster_key1 : %d\n", key_value); 	
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#else
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;
//	uint16_t adc_val;

	if((4600< key_value))
	{
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info3 = 0x00;	/// INVALID VALUE
		return;
	}

	if(avg_adc_cnt != 0)
	{
		 avg_adc_val1 += key_value;
	}
	else
	{
		 avg_adc_val1 = 0;
		 avg_adc_val1 += key_value;
	}

	avg_adc_cnt++;

	if (avg_adc_cnt > 9)
	{
		avg_adc_cnt = 0;
		avg_adc_val1/= 10;

		if(avg_adc_val1 <= 300)							//0
		{
			#if 0
			key_push_info3 = 0x01;	// NEXT KEY
			#else
			key_push_info3 = 0x02;	// LAST KEY
			#endif
		}
		else if((1000< avg_adc_val1) && (avg_adc_val1 <= 1450))							//1219      //804   
		{
		 	#if 0
		 	key_push_info3 = 0x02;	// LAST KEY
			#else
			key_push_info3 = 0x01;	// NEXT KEY
			#endif
		}
		else if((2200 < avg_adc_val1) && (avg_adc_val1 <= 2650))						// 2476    	//1623
		{
			key_push_info3 = 0x04;	// VOICE PLUS KEY
		}
		else if((3400< avg_adc_val1) && (avg_adc_val1 <= 3850)) 						//3687		//2433
		{
			key_push_info3 = 0x08;	// VOICE MINUS KEY
		}
		else if((4300 < avg_adc_val1) && (avg_adc_val1 <= 4600))						//4470		//2950
		{
				key_push_info3 = 0x10;	// MODE KEY
		}
		else
			key_push_info3 = 0x00;	/// INVALID VALUE

	//		DPRINTF(DBG_MSG5, "cluster_key1 : %d\n", key_value); 	
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#endif

}


void Cluster_Key_Parser2(uint32_t key_value)
{
	#ifdef NEW_SCAN_ADC_KEY
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;
	static uint16_t avg_adc[KEY_SCAN_CNT] = {0,};

	if((4000< key_value)||(r_Device.r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)){
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info4 = 0x00;	/// INVALID VALUE
		return;
	}

	avg_adc_cnt++;
	
	if(avg_adc_cnt != 0)
	{
		 avg_adc[avg_adc_cnt-1] = key_value;
	}

	if (avg_adc_cnt > KEY_SCAN_CNT)
	{
		avg_adc_val1 = Key_data_Sorting(&avg_adc[0], KEY_SCAN_CNT, KEY_SCAN_REMOVE_CNT);
		avg_adc_cnt = 0;
		memset(&avg_adc[0],0x0,sizeof(avg_adc));
		
		if(avg_adc_val1 <= 300)	
		{
			key_push_info4 = 0x01;	// VOICE ACTIVATE KEY
		}
		else if((2300 < avg_adc_val1) && (avg_adc_val1 <= 2700))				// 2479			//1636
		{
			key_push_info4 = 0x02;	// WATCH KEY
		}
		else if((3400< avg_adc_val1) && (avg_adc_val1 <= 3800)) 				//3622			//
		{
			key_push_info4 = 0x04;	// CALL HANGUP KEY
		}
		else
			key_push_info4 = 0x00;	/// INVALID VALUE

		//DPRINTF(DBG_MSG5, "cluster_key2 : %d\n", key_value);
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#else
	static uint32_t avg_adc_val1 = 0;
	static uint8_t avg_adc_cnt = 0;

	if((4000< key_value)||(r_Device.r_System->s_state == SYS_SCREEN_SAVE_MODE_IDLE)){
		// no press button			// 5000
		avg_adc_cnt =0;
		avg_adc_val1 = 0;
		key_push_info4 = 0x00;	/// INVALID VALUE
		return;
	}

	if(avg_adc_cnt != 0)
	{
		 avg_adc_val1 += key_value;
	}
	else
	{
		 avg_adc_val1 = 0;
		 avg_adc_val1 += key_value;
	}

	avg_adc_cnt++;

	if (avg_adc_cnt > 9)
	{
		avg_adc_cnt = 0;
		avg_adc_val1/= 10;
		
		if(avg_adc_val1 <= 300)	
		{
			key_push_info4 = 0x01;	// VOICE ACTIVATE KEY
		}
		else if((2300 < avg_adc_val1) && (avg_adc_val1 <= 2700))				// 2479			//1636
		{
			key_push_info4 = 0x02;	// WATCH KEY
		}
		else if((3400< avg_adc_val1) && (avg_adc_val1 <= 3800)) 				//3622			//
		{
			key_push_info4 = 0x04;	// CALL HANGUP KEY
		}
		else
			key_push_info4 = 0x00;	/// INVALID VALUE

		//DPRINTF(DBG_MSG5, "cluster_key2 : %d\n", key_value);
		//if (key_push_info != 0x00) DPRINTF(DBG_INFO, "key_push : 0x%02x\n", key_push_info);
	}
	#endif

}

#endif
#ifdef NEW_SCAN_ADC_KEY
static uint32_t Key_data_Sorting(uint16_t *sortdata,uint8_t len, uint8_t offset)
{
	int i=0,j=0;
	uint8_t postion=0;
	uint16_t swap=0;
	uint32_t ret_val=0;
	uint16_t sort_packet[50];

	memcpy(&sort_packet[0], sortdata,len*sizeof(uint16_t));

	for(i=0;i<len;i++){
		postion = i;
		for(j=0;j<len-i-1;j++){
			if(sort_packet[j]>sort_packet[j+1]){
				swap = sort_packet[j];
				sort_packet[j] = sort_packet[j+1];
				sort_packet[j+1] = swap;}
		}
	}

	for(i=offset;i<len-offset;i++)
		{ret_val +=(uint32_t)sort_packet[i];}
	
	#if 0
	{
			for(i=0;i<len;i++)
				DPRINTF(DBG_MSG5, "%d  ", sort_packet[i]);	
			DPRINTF(DBG_MSG5, " : key %d \n", (ret_val/(len-(2*offset))));	
	}
	#endif
	return (ret_val/(len-(2*offset)));
}
#endif

void Key_Task(void)
{
	KeyTask(&app_key[KEY_PWR]);
	KeyTask(&app_key[KEY_BACK]);
	KeyTask(&app_key[KEY_SET]);
	KeyTask(&app_key[KEY_HOME]);
	KeyTask(&app_key[KEY_ENTER]);
	KeyTask(&app_key[KEY_UP]);
	KeyTask(&app_key[KEY_DOWN]);
	KeyTask(&app_key[KEY_LEFT]);
	KeyTask(&app_key[KEY_RIGHT]);
	KeyTask(&app_key[KEY_AVM_ONOFF]);
	#ifdef CX62C_FUNCTION
	KeyTask(&app_key[KEY_NEXT]);
	KeyTask(&app_key[KEY_LAST]);
	KeyTask(&app_key[KEY_VOICE_PLUS]);
	KeyTask(&app_key[KEY_VOICE_MINUS]);
	KeyTask(&app_key[KEY_MODE]);
	KeyTask(&app_key[KEY_VOICE_ACTIVATE]);
	KeyTask(&app_key[KEY_WECHAT]);
	KeyTask(&app_key[KEY_PHONE_CALL]);
	#endif
}
