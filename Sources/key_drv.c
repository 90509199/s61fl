/*
 * key_drv.c
 *
 *  Created on: 2019. 1. 4.
 *      Author: Digen
 */

#include "includes.h"

void InitKeyStat(struct KEYDRV_DEV* keydrv)
{
	keydrv->stat = KEYDRV_IDLE;
}

void KeyTask(struct KEYDRV_DEV* keydrv)
{
	switch (keydrv->stat)
	{
	case KEYDRV_IDLE:
		if (keydrv->GetKeyPinStat() == PUSH)
		{
			#if 0
			if (keydrv->LongKey != 0)
			{
				keydrv->stat = KEYDRV_LONGKEY;
				keydrv->tick_cnt = keydrv->GetTickCount();
			}
			else
				keydrv->stat = KEYDRV_RELEASE;
			#else
				
			keydrv->stat = KEYDRV_PUSH;
			#endif
		}
		break;
	case KEYDRV_LONGKEY:
		if (keydrv->GetKeyPinStat() == RELEASE)
		{
			keydrv->stat = KEYDRV_RELEASE;
//			keydrv->Release();
//			keydrv->stat = KEYDRV_IDLE;
		}
		else if (keydrv->longkey_time < Keydrv_GetElapsedTime(keydrv))
		{
			keydrv->LongKey();
			keydrv->tick_cnt = keydrv->GetTickCount();
			keydrv->stat = KEYDRV_REPEAT;
		}
		break;
	case KEYDRV_REPEAT:
		if (keydrv->GetKeyPinStat() == RELEASE)
		{
			keydrv->stat = KEYDRV_RELEASE;
//			if (keydrv->LongKeyRelease != 0) keydrv->LongKeyRelease();
//			keydrv->stat = KEYDRV_IDLE;
		}
		else if ((keydrv->Repeat != 0) && (keydrv->repeat_time < Keydrv_GetElapsedTime(keydrv)))
		{
			keydrv->Repeat();
			keydrv->tick_cnt = keydrv->GetTickCount();
			keydrv->stat = KEYDRV_REPEAT;
		}
		break;
	case KEYDRV_RELEASE:
		if (keydrv->GetKeyPinStat() == RELEASE)
		{
			keydrv->Release();
			keydrv->stat = KEYDRV_IDLE;
		}
		break;
	case KEYDRV_PUSH:
		if (keydrv->GetKeyPinStat() == PUSH)
		{
			keydrv->Push();
			if (keydrv->LongKey != 0)
			{
				keydrv->stat = KEYDRV_LONGKEY;
				keydrv->tick_cnt = keydrv->GetTickCount();
			}
			else
				keydrv->stat = KEYDRV_RELEASE;
		}
		break;
	}
}

static uint32_t Keydrv_GetElapsedTime(struct KEYDRV_DEV* _keydrv)
{
	uint32_t start;
	uint32_t current;
	uint32_t elapsed;

	start = _keydrv->tick_cnt;
	current = _keydrv->GetTickCount();

	if (current == start) return 0;

	if (current > start) elapsed = current - start;
	else elapsed = 0xFFFFFFFF - start + current;

	return elapsed;
}

