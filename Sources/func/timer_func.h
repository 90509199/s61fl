/*
 * timer_func.h
 *
 *  Created on: 2019. 1. 16.
 *      Author: Digen
 */

#ifndef FUNC_TIMER_FUNC_H_
#define FUNC_TIMER_FUNC_H_


void Init_TimerDrv(void);
void Timer_Task(void);
void ADCkey_Enable(uint8_t OnOff);

uint32 Check_AD1_Key(void);
uint32 Check_AD2_Key(void);

#ifdef CX62C_FUNCTION
uint32 Check_Cluster_P_Key(void);
uint32 Check_Cluster_M_Key(void);
#endif

uint32 Check_Batt_Value(void);
uint32 Check_HW_PCB_Ver(void);

void Process_ADC_key(void);

#endif /* FUNC_TIMER_FUNC_H_ */
