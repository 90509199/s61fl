;/*
 * GPIO_FUNC.h
 *
 *  Created on: 2018. 10. 10.
 *      Author: Digen
 */

#ifndef GPIO_FUNC_H_
#define GPIO_FUNC_H_
//OUTPUT

//TUNER_PW_EN, DSP_PW_EN
void DSP_PW_EN(uint8 OnOff);
void MCU_AMP_ST_BY(uint8 OnOff);
void MCU_AMP_MUTE(uint8 OnOff);
void ATB_3V3_EN(uint8 OnOff);
void Tuner_CS_EN(uint8 OnOff);
#if 0 //def SI_LABS_TUNER
void SiLabs_Tuner_RST(uint8 OnOff);
#endif
void M_5V_FRQ_SEL(uint8 OnOff);
void BAT_ADC_ON(uint8 OnOff);

void TFT_PW_EN(uint8 OnOff);
void MIC_PWR_EN_1(uint8 OnOff);
void GPS_MD_PW_EN(uint8 OnOff);

#ifdef B1_MOTHER_BOARD_SAMPLE
void USB_PW_EN(uint8 OnOff);
#else
void USB_5V_FRQ_SEL(uint8 OnOff);
#endif

void DSP_PDN(uint8 OnOff);
void M_5V_EN(uint8 OnOff);
#ifdef B1_MOTHER_BOARD_SAMPLE
void MCU_HU_LCD_EN(uint8 OnOff);
#endif
void CAN0_WAKE_N(uint8 OnOff);
void MIC_PWR_EN_2(uint8 OnOff);
#ifndef SI_LABS_TUNER
void IRLED_CTL(uint8 OnOff);
#endif
void MCU_UPGRADE_SOC(uint8 OnOff);


void MCU_LCD1_2_EN_3P3(uint8 OnOff);
void MCU_CONTROL_PMIC_ONOFF(uint8 OnOff);
void MCU_TFT2_PWR_EN(uint8 OnOff);
void M_MAIN_PWR_EN(uint8 OnOff);
void M_GPIO_TEST(uint8 OnOff);

//need to check //cpu reset
void MCU_CAN0_EN(uint8 OnOff);
void MCU_CAN0_STB(uint8 OnOff);
void PMIC_RESET_IN(uint8 OnOff);

//INPUT
uint8 MCU_ACC_DET(void);
//need to check
uint8 MCU_LOW_BAT(void);
uint8 PS_HOLD(void);
uint8 HU_BL_REQ(void);

uint8 GPIO_CONTROL(void);
uint8 POWER_KEY(void);
//need to check
uint8 MCU_TBOX_MUTE(void);
uint8 MCU_IGN_DET(void);
uint8 TFT1_PWR_OCP_MCU(void);

uint8 MCU_HIGH_BAT(void);
uint8 MCU_KNOB_B(void);
uint8 MCU_KNOB_A(void);


#endif /* GPIO_FUNC_H_ */
