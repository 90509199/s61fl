/*
 * uart_func.h
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#ifndef UART_FUNC_H_
#define UART_FUNC_H_

//#define MAX_USART_TX_BUF		128
#define MAX_USART_RX_BUF		1024	//1024


struct USART_DEV
{
	uint8_t rx_buf[MAX_USART_RX_BUF];
	volatile uint16_t rx_buf_wr;
	volatile uint16_t rx_buf_rd;
	volatile uint16_t rx_buf_cnt;
};
typedef struct USART_DEV USART_DEV_t;


struct USART_TX
{
	uint8_t tx_buf[MAX_USART_RX_BUF];
	uint16_t tx_buf_wr;
	uint16_t tx_buf_rd;
	volatile uint16_t tx_buf_cnt;
};
typedef struct USART_TX USART_TX_t;

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
extern USART_DEV_t usart0_dev;
extern USART_DEV_t usart1_dev;

void init_uart0(void);
void init_uart1(void);

uint16_t kbhit0(void);
uint8_t uputc0(char c);
uint8_t uputs0(char* str);
uint8_t ugetc0(void);
void uputa0(uint8_t* tx_data, uint8_t packet_size);

uint16_t kbhit1(void);
uint8_t uputc1(char c);
uint8_t uputs1(char* str);
int8_t ugetc1(void);
void uputa1(uint8_t* tx_data, uint8_t packet_size);

#endif /* UART_FUNC_H_ */
