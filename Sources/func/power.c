/*
 * power.c
 *
 *  Created on: 2018. 10. 10.
 *      Author: Digen
 */

#include "includes.h"
#define USE_FAST_CLOSE_ILLUMINATION

extern uint8  u8TestSleepCmd;

static volatile uint32 power_tick = 0;
#ifdef USE_FAST_CLOSE_ILLUMINATION
static uint8 KeyState=0u,LastKeyState=0u,DoorFLState=0u,DoorFRState=0u,ForceIllumOff=0u;
#endif

#define PPRINTF(lvl, fmt, args...)		DPRINTF(lvl, fmt, ##args)
//#define PPRINTF(lvl, fmt, args...)

void Power_Proc(DEVICE *dev)
{
	uint8 tmp8;
	
	switch(dev->r_Power->p_state)
	{
	case PWR_IDLE:
	case PWR_ON_ACTIVE:
		power_tick = GetTickCount();	
		#ifdef USE_FAST_CLOSE_ILLUMINATION
		DoorFLState=IlGetRxBCM_DoorAjarSts_FL();
		DoorFRState=IlGetRxBCM_DoorAjarSts_FR();
		KeyState=IlGetRxBCM_KeySts();
		if ((ForceIllumOff==1u)&&((KeyState>1u)||(DoorFLState!=0u)||(DoorFRState!=0u)))
		{
			ForceIllumOff=0u;
			Control_LCD_Backlight_for_Mengbo(dev,ON);
		}
		if (LastKeyState!=KeyState)
		{
			/*ON->OFF && Door Closed*/
			if ((LastKeyState==2u)&&(KeyState==0u)&&(DoorFLState==0u)&&(DoorFRState==0u))
			{
				ForceIllumOff=1u;
				Control_LCD_Backlight_for_Mengbo(dev,OFF);
			}
			LastKeyState=KeyState;
		}
		#endif
		break;
	case PWR_ON_1://5->50->10
		#ifdef USE_FAST_CLOSE_ILLUMINATION
		ForceIllumOff=0u;
		#endif
		if(1<=GetElapsedTime(power_tick))
		{
			M_5V_EN(ON);
			//ATB_3V3_EN(ON);
			BAT_ADC_ON(ON);
			// CAN0_WAKE_N(ON);  // break can drive issue when can port is high
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_1 %d ms\n\r",power_tick);
			dev->r_Power->p_state = PWR_ON_2;
		}
		break;
	case PWR_ON_2:
		if(5<=GetElapsedTime(power_tick))
		{
			MCU_AMP_MUTE(ON);
			MCU_AMP_ST_BY(ON);
			//RRM 7 invalid data

			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_2  %d ms\n\r",power_tick);
			dev->r_Power->p_state = PWR_ON_3;
		}
		break;
	case PWR_ON_3:
		if(5<=GetElapsedTime(power_tick))
		{
			DSP_PW_EN(ON);//DSP_PW_EN
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_3 %d ms\n\r",power_tick);
			dev->r_Power->p_state = PWR_ON_4;
		}
		break;
	case PWR_ON_4:
		if(2<=GetElapsedTime(power_tick))
		{
			//DSP_PDN(ON);					//600ns after pw_en is on
			//dev->r_Dsp->d_state = DSP_INIT_1;	
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_4 %d ms\n\r",power_tick);
			dev->r_Power->p_state = PWR_ON_5;
		}
		break;
	case PWR_ON_5:
		if(5<=GetElapsedTime(power_tick))
		{
			GPS_MD_PW_EN(ON);
//not used			MIC_PWR_EN_1(ON);
			MIC_PWR_EN_2(ON);
			//read flash
			#if 1
			PPRINTF(DBG_MSG3,"E2P ID:%x %x\n\r",e2p_save_data.E2P_ID1,e2p_save_data.E2P_CHKSUM);//JANG_211120
//			PPRINTF(DBG_MSG3,"E2P SET_MidPicth : %x\n\r",e2p_save_data.E2P_EQ_MID);
			PPRINTF(DBG_MSG3,"E2P SET_Treble : %x\n\r",e2p_save_data.E2P_EQ_TREBLE); 
			PPRINTF(DBG_MSG3,"E2P SET_Fader :%x \n\r",e2p_save_data.E2P_EQ_FADER);
			PPRINTF(DBG_MSG3,"E2P SET_Balance :%x \n\r",e2p_save_data.E2P_EQ_BALANCE);
			PPRINTF(DBG_MSG3,"E2P E2P_EQ_TYPE :%x \n\r",e2p_save_data.E2P_EQ_TYPE);
			DPRINTF(DBG_MSG3,"E2P E2P_LCD_BRIGHTNESS :%x \n\r",e2p_save_data.E2P_LCD_BRIGHTNESS);
			#endif
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_5 %d ms\n\r",power_tick);
			dev->r_Power->p_state = PWR_ON_6;
		}
		break;
	case PWR_ON_6:			
		if(4<=GetElapsedTime(power_tick))
		{
			//JANG_211120
			#ifdef X95_FEATURE
			if((e2p_save_data.E2P_ID1==0x90)&&(e2p_save_data.E2P_CHKSUM==checksum_setup_value(&e2p_save_data)))
			#else
			if((e2p_save_data.E2P_ID1==0x70)&&(e2p_save_data.E2P_CHKSUM==checksum_setup_value(&e2p_save_data)))
			#endif
			{
				PPRINTF(DBG_MSG3,"E2P data is loaded ok\n\r");
			}
			else
			{
				defualt_setup_value(&e2p_save_data);
				defualt_system_value(&e2p_sys_data);
				PPRINTF(DBG_MSG3,"E2P set default value\n\r");
			}

			//check valid
#ifdef CX62C_FUNCTION
			if((e2p_sys_data.E2P_SONY == 0x01)||(e2p_sys_data.E2P_SONY ==0x00))
				r_sonysound = e2p_sys_data.E2P_SONY;
#endif
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_6 %d ms\n\r",power_tick);		//535
			dev->r_System->s_state_backup = e2p_sys_data.E2P_LAST_MODE;
			dev->r_Power->p_state = PWR_ON_7;
		}			
		break;
	case PWR_ON_7:		
		if(4<=GetElapsedTime(power_tick))
		{
			PMIC_RESET_IN(ON);
			MCU_CONTROL_PMIC_ONOFF(ON);		//SOC POWER
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_7 %d ms\n\r" ,power_tick);		//539
			dev->r_Power->p_state = PWR_ON_8;	
		}			
		break;		
	case PWR_ON_8:
		if(50<=GetElapsedTime(power_tick))
		{	
			M_MAIN_PWR_EN(ON);	
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_8  %d ms\n\r",power_tick);	
			dev->r_Power->p_state = PWR_ON_9;	
		}
		break;
	case PWR_ON_9:			//100->99
		if(100<=GetElapsedTime(power_tick))
		{
			MCU_CONTROL_PMIC_ONOFF(OFF);
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_9 %d ms\n\r",power_tick);		//838
			dev->r_Power->p_state = PWR_ON_10;
		}
		break;
	case PWR_ON_10:		
		if(200<=GetElapsedTime(power_tick))
		{
			MCU_CONTROL_PMIC_ONOFF(ON);
			//PMIC_RESET_IN(ON);
			dev->r_System->pshold =ON;

			#ifdef B1_MOTHER_BOARD_SAMPLE
			USB_PW_EN(ON);
			#endif
			//set amp candata
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_10  pshold start %d ms\n\r",power_tick);		//1038 ms
			dev->r_Power->p_state = PWR_ON_11;
		}
		break;
	case PWR_ON_11:
		if(1<=GetElapsedTime(power_tick))
		{
			// check ps_hold or after 3 sec 
			if((PS_HOLD()==ON)||(3000<=GetElapsedTime(power_tick)))
			{
				#ifndef TFT_PW_FROM_GPIO_TO_PWM
				TFT_PW_EN(ON);				//TFT PWR ON
				dev->r_Power->p_state = PWR_ON_12;
				#else
				dev->r_Power->p_state = PWR_PWM_1;
				dev->r_Power->p_high_proity =ON;
				#endif
				power_tick = GetTickCount();
				PPRINTF(DBG_MSG3,"PWR_ON_11 %d ms\n\r",power_tick);
			}
		}
		break;
	#ifdef TFT_PW_FROM_GPIO_TO_PWM
		case PWR_PWM_1:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_init();
				FTM0_PWM_init(4);
				TFT_PW_EN(ON);				//TFT PWR ON
				FTM0_CH4_PWM_Duty(50);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_2;
			} 
			break;
		case PWR_PWM_2:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(60);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_3;
			} 
			break;
		case PWR_PWM_3:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(70);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_4;
			} 
			break;
		case PWR_PWM_4:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(80);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_5;
			} 
			break;
		case PWR_PWM_5:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(90);
				power_tick = GetTickCount();
	//			PPRINTF(DBG_MSG3,"PWR_PWM_5 %d ms\n\r",power_tick);
				dev->r_Power->p_state = PWR_PWM_6;
			} 
			break;
		case PWR_PWM_6:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(95);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_7;
			} 
			break;
		case PWR_PWM_7:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(100);
				dev->r_Power->p_high_proity =OFF;
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_8;
			} 
			break;
		case PWR_PWM_8:
			if(1<=GetElapsedTime(power_tick))
			{
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_ON_12;
			} 		
			break;
	#endif
	case PWR_ON_12:
		if(1<=GetElapsedTime(power_tick))
		{
			#ifdef CHECK_HU_BL_REQ
			if((HU_BL_REQ()==ON)||(dev->r_System->lcd_ok))			//JANG_210818
			#endif
			{
				#ifdef CHANG_BL_TIMING
				PWM_init();
				dev->r_System->blpwmenable =ON; //JANG_211025 
				#endif
				#ifdef B1_MOTHER_BOARD_SAMPLE
				FTM3_PWM_init(MCU_HU_LCD_PWM_3P3);
				FTM3_PWM_init(MCU_CAM_PWM_3P3);
				#else
				FTM3_PWM_init(MCU_CLUSTERLCD_PWM_3P3);
				#endif
				FTM3_start_counter(); 		//�����׽�Ʈ ���� �ʿ���..
				PPRINTF(DBG_MSG3,"MCU_HU_LCD_PWM_3P3:%d brightness:%d %d ms\n\r",TFT1_PWR_OCP_MCU(),e2p_save_data.E2P_LCD_BRIGHTNESS,power_tick);
				dev->r_Power->p_state = PWR_ON_13;
			}
			power_tick = GetTickCount();
		}
		break;
	case PWR_ON_13:
		if(1<=GetElapsedTime(power_tick))
		{ 
			FTM3_PWM_init(MCU_LCD2_PWM_3P3);
			FTM3_PWM_init(MCU_LCD1_PWM_3P3);
			FTM3_start_counter();			//�����׽�Ʈ ���� �ʿ���..
			
			#ifndef NEW_LCD_BACKLIGHT
			if(dev->r_System->accign_state.state)
			{
				//LCD ON
				MCU_LCD1_2_EN_3P3(OFF); //LCD ON
			}

			if((SYS_BATTERY_LOW_MODE_ENTER<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_BATTERY_16V_OVER_MODE_IDLE))
			{
				//LCD OFF
				MCU_LCD1_2_EN_3P3(ON); //LCD OFF
				PPRINTF(DBG_MSG3,"MCU_LCD2_PWM_3P3: OFF \n\r");
			}
			#endif	
			#if 0 //JANG_211203
			FTM3_PWM_Duty(Set_LCD_Brightness(e2p_save_data.E2P_LCD_BRIGHTNESS));	//default is darkness
			#else
			FTM3_PWM_Duty(99);	//default is darkness
			#endif
			delay_ms(1);	// HW request
			dev->r_System->lcd_control.enable=OFF;
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"MCU_LCD2_PWM_3P3:%d brightness:%d %dms\n\r",TFT1_PWR_OCP_MCU(),e2p_save_data.E2P_LCD_BRIGHTNESS,power_tick);
			dev->r_Power->p_state = PWR_ON_14;
		}
		break;
	case PWR_ON_14:
		if(3000<=GetElapsedTime(power_tick))
		{
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"PWR_ON_14 %d ms\n\r",power_tick);
			dev->r_Dsp->d_state = DSP_INIT_1;				//dsp start //soc power on after 11ms
			dev->r_Power->p_state = PWR_ON_ACTIVE;
		}							
		break;
	case PWR_RESET_1:
		#if 1
		dev->r_System->init_ok = OFF;
		dev->r_System->uart_ok = OFF;
//		dev->r_System->power_on = OFF;		//only sleep wakeup or enter battery 
		dev->r_Dsp->d_softmute_status = OFF;
		dev->r_Dsp->d_goto_mute_status = OFF;
		dev->r_Dsp->d_mix_status = OFF;
		#endif
		


		MCU_AMP_MUTE(ON);

		#ifdef B1_MOTHER_BOARD_SAMPLE
		USB_PW_EN(OFF);
		PPRINTF(DBG_MSG3,"USB_PW_EN:OFF\n\r");
		#endif
		M_MAIN_PWR_EN(OFF);
		dev->r_System->lcd_ok = 0;
		power_tick = GetTickCount();
		//LCD OFF
		#ifdef CHANG_BL_TIMING
		if(dev->r_System->blpwmenable)//JANG_211025 
		#endif
		FTM3_stop_counter();			//�����׽�Ʈ ���� �ʿ���..
		TFT_PW_EN(OFF);				//TFT PWR ON
		PPRINTF(DBG_MSG3,"PWR_RESET_1\n\r");
		dev->r_Power->p_state = PWR_RESET_2;
		break;
	case PWR_RESET_2:
		//1000->500
		if(500<=GetElapsedTime(power_tick))
		{
			MCU_CONTROL_PMIC_ONOFF(ON);
			PPRINTF(DBG_MSG3,"PWR_RESET_2\n\r");
			dev->r_Power->p_state = PWR_RESET_3;
			power_tick = GetTickCount();	
		}
		break;
	case PWR_RESET_3:
		if(50<=GetElapsedTime(power_tick))
		{
			M_MAIN_PWR_EN(ON);
			PPRINTF(DBG_MSG3,"PWR_RESET_3\n\r");
			power_tick = GetTickCount();
			dev->r_Power->p_state = PWR_RESET_4;						
		}
		break;
	case PWR_RESET_4:
		if(100<=GetElapsedTime(power_tick))
		{
			MCU_CONTROL_PMIC_ONOFF(OFF);
			PPRINTF(DBG_MSG3,"PWR_RESET_4\n\r");
			dev->r_Power->p_state = PWR_RESET_5;
			power_tick = GetTickCount();	
		}
		break;
	case PWR_RESET_5:
		if(200<=GetElapsedTime(power_tick))
		{
			dev->r_System->pshold =ON;
			MCU_CONTROL_PMIC_ONOFF(ON);
			power_tick = GetTickCount();
			dev->r_Power->p_state = PWR_RESET_6;
		}
		break;
	case PWR_RESET_6:
		if(1<=GetElapsedTime(power_tick))
		{
			// check ps_hold or after 3 sec 
			if((PS_HOLD()==ON)||(3000<=GetElapsedTime(power_tick)))
			{

				MCU_AMP_MUTE(OFF);

				#ifdef TFT_PW_FROM_GPIO_TO_PWM
				dev->r_Power->p_state = PWR_PWM_R1;
				#else
				TFT_PW_EN(ON);				//TFT PWR ON
				dev->r_Power->p_state = PWR_RESET_7;
				#endif
				power_tick = GetTickCount();
				PPRINTF(DBG_MSG3,"PWR_RESET_6 %d ms\n\r",power_tick);
			}
		}
		break;		
	
#ifdef TFT_PW_FROM_GPIO_TO_PWM
		case PWR_PWM_R1:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_init();
				FTM0_PWM_init(4);
				TFT_PW_EN(ON);				//TFT PWR ON
				FTM0_CH4_PWM_Duty(50);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_R2;
			} 
			break;
		case PWR_PWM_R2:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(60);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_R3;
			} 
			break;
		case PWR_PWM_R3:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(70);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_R4;
			} 
			break;
		case PWR_PWM_R4:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(80);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_R5;
			} 
			break;
		case PWR_PWM_R5:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(90);
				power_tick = GetTickCount();
	//			PPRINTF(DBG_MSG3,"PWR_PWM_5 %d ms\n\r",power_tick);
				dev->r_Power->p_state = PWR_PWM_R6;
			} 
			break;
		case PWR_PWM_R6:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(95);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_R7;
			} 
			break;
		case PWR_PWM_R7:
			if(1<=GetElapsedTime(power_tick))
			{
				FTM0_CH4_PWM_Duty(100);
				power_tick = GetTickCount();
				dev->r_Power->p_state = PWR_PWM_R8;
			} 
			break;
		case PWR_PWM_R8:
			if(1<=GetElapsedTime(power_tick))
			{
				power_tick = GetTickCount();
				#ifdef QFI_MODE_SEQ_CHANGE
				if(dev->r_System->soc_update == ON)
					dev->r_System->soc_update_excute =	ON;
				#endif
				dev->r_Power->p_state = PWR_RESET_7;
			} 		
			break;
#endif
	case PWR_RESET_7:
		#ifdef CHECK_HU_BL_REQ
		if((HU_BL_REQ()==ON) ||(dev->r_System->lcd_ok))		//timeout is 12sec, normal is 9sec
		#endif
		{
			#ifdef B1_MOTHER_BOARD_SAMPLE
			USB_PW_EN(ON);
			PPRINTF(DBG_MSG3,"USB_PW_EN:ON\n\r");
			#endif
			#ifdef CHANG_BL_TIMING
			if(dev->r_System->blpwmenable==OFF)//JANG_211025 
			{
				PWM_init();
				dev->r_System->blpwmenable = ON;
			}
			#endif
			#ifdef B1_MOTHER_BOARD_SAMPLE
			FTM3_PWM_init(MCU_HU_LCD_PWM_3P3);
			FTM3_PWM_init(MCU_CAM_PWM_3P3);
			#else
			FTM3_PWM_init(MCU_CLUSTERLCD_PWM_3P3);
			#endif
			FTM3_start_counter(); 		//�����׽�Ʈ ���� �ʿ���..
			#if 0	//jjh reset
			#ifdef B1_MOTHER_BOARD_SAMPLE
			if(dev->r_System->accign_state.state)
				MCU_HU_LCD_EN(OFF);
			#endif
			#endif
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"MCU_HU_LCD_PWM_3P3:%d brightness:%d\n\r",TFT1_PWR_OCP_MCU(),e2p_save_data.E2P_LCD_BRIGHTNESS);
			dev->r_Power->p_state = PWR_RESET_8;
		}
		break;
	case PWR_RESET_8:
		if(1<=GetElapsedTime(power_tick))
		{ 
			FTM3_PWM_init(MCU_LCD1_PWM_3P3);
			FTM3_PWM_init(MCU_LCD2_PWM_3P3);
			FTM3_start_counter();			//�����׽�Ʈ ���� �ʿ���..
			#if 0	//jjh reset
			if(dev->r_System->accign_state.state)
				MCU_LCD1_2_EN_3P3(OFF);
			#endif
			
//			FTM3_PWM_Duty(99);	//default is darkness				
			delay_ms(1);	// HW request
			dev->r_System->lcd_control.enable=OFF;
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3,"MCU_LCD2_PWM_3P3:%d brightness:%d\n\r",TFT1_PWR_OCP_MCU(),e2p_save_data.E2P_LCD_BRIGHTNESS);
			dev->r_Power->p_state = PWR_RESET_9;
		}
		break;

	case PWR_RESET_9:
		if(1<=GetElapsedTime(power_tick))
		{
			#ifndef NEW_LCD_BACKLIGHT
			if((SYS_ACTIVE_MODE_ENTER == dev->r_System->s_state)||(SYS_ACTIVE_MODE_IDLE==dev->r_System->s_state)||
				((SYS_SCREEN_SAVE_MODE_ENTER<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE)))
			#ifdef LCD_REQUSET_FOR_MENGBO
			Control_LCD_Backlight_for_Mengbo(dev,ON);
			#else
			Control_LCD_Backlight(ON);			//LCD ON
			#endif
			#endif
			power_tick = GetTickCount();
			#ifndef QFI_MODE_SEQ_CHANGE
			if(dev->r_System->soc_update == ON) 
				dev->r_Power->p_state = PWR_RESET_10;
			else
			#endif
				dev->r_Power->p_state = PWR_ON_ACTIVE;
		}
		break;
	case PWR_RESET_10:
		if(5000<=GetElapsedTime(power_tick))
		{
			MCU_UPGRADE_SOC(OFF);
			PPRINTF(DBG_MSG3,"PWR_RESET_10 low gpio for soc update \n\r");
			power_tick = GetTickCount();
			dev->r_Power->p_state = PWR_ON_ACTIVE;
		}
		break;
	case PWR_PRE_OFF_1:
		MCU_CONTROL_PMIC_ONOFF(OFF);
		power_tick = GetTickCount();
		PPRINTF(DBG_MSG3, " PWR_PRE_OFF_1 %d ms\n\r", power_tick);
		dev->r_Power->p_state = PWR_PRE_OFF_2;
		break;
	case PWR_PRE_OFF_2:// power sequence is 1sec
		if(1000<=GetElapsedTime(power_tick))
		{
			MCU_CONTROL_PMIC_ONOFF(ON);
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG3, " PWR_PRE_OFF_2 %d ms\n\r", power_tick);
			dev->r_Power->p_state = PWR_ON_ACTIVE;
		}
		break;
	case PWR_OFF_1:
		DSP_PDN(OFF);
		MCU_AMP_ST_BY(OFF);
		MCU_AMP_MUTE(ON);
		#ifdef B1_MOTHER_BOARD_SAMPLE
		USB_PW_EN(OFF);
		MCU_HU_LCD_EN(ON);
		#endif
		MCU_LCD1_2_EN_3P3(ON);
		power_tick = GetTickCount();
		PPRINTF(DBG_MSG1," PWR_OFF_1  USB_PW_EN : OFF %d ms \n\r",power_tick);
		dev->r_Power->p_state = PWR_OFF_2;
		break;
	case PWR_OFF_2:
		if ((PS_HOLD() == OFF) || ((PS_HOLD() == ON) && (30000 <= GetElapsedTime(power_tick))) 
			|| (u8TestSleepCmd == 1u))
		{
			MCU_CONTROL_PMIC_ONOFF(OFF);
			M_MAIN_PWR_EN(OFF);
			PMIC_RESET_IN(OFF);
			TFT_PW_EN(OFF);	
			MCU_TFT2_PWR_EN(OFF);//MCU_TFT2_PWR_EN
//not used			MIC_PWR_EN_1(OFF);  //JANG_211025_1
			MIC_PWR_EN_2(OFF);
			ATB_3V3_EN(OFF);
			M_5V_FRQ_SEL(OFF);
			GPS_MD_PW_EN(OFF);
			MCU_UPGRADE_SOC(OFF);//JANG_211025_1
			#if 0 //def SI_LABS_TUNER
			SiLabs_Tuner_RST(OFF);
			//#else
			IRLED_CTL(OFF);
			#endif
			power_tick = GetTickCount();
			PPRINTF(DBG_MSG1," PWR_OFF_2  %d ms \n\r",power_tick);
			dev->r_Power->p_state = PWR_OFF_3;
		}
		break;
	case PWR_OFF_3:
		DSP_PW_EN(OFF);//DSP_PW_EN
		BAT_ADC_ON(OFF);//BAT_ADC_ON
		power_tick = GetTickCount();
		PPRINTF(DBG_MSG1,"PWR_OFF_3 %d ms \n\r",power_tick);
		dev->r_Power->p_state = PWR_OFF_4;
		break;
	case PWR_OFF_4:
		power_tick = GetTickCount();
		PPRINTF(DBG_MSG1,"PWR_OFF_4 %d ms \n\r",power_tick);
		dev->r_Power->p_state = PWR_OFF_5;
		break;
	case PWR_OFF_5:
		M_5V_EN(OFF);
		power_tick = GetTickCount();
		PPRINTF(DBG_MSG1,"PWR_OFF_5 %d ms \n\r",power_tick);
		dev->r_Power->p_state = PWR_OFF_6;
		break;
	case PWR_OFF_6:
		if(10<=GetElapsedTime(power_tick))
		{
			//MCU_AMP_MUTE(ON);
			PPRINTF(DBG_MSG1,"PWR_OFF_6 %d ms \n\r",power_tick);
			dev->r_Power->p_state = PWR_OFF_7;
		}
		break;
	case PWR_OFF_7:
		#ifdef B1_MOTHER_BOARD_SAMPLE
		MCU_HU_LCD_EN(OFF);
		MCU_LCD1_2_EN_3P3(OFF);
		#endif
		dev->r_Power->p_state = PWR_IDLE;
		break;
	case PWR_OFF_ALL:
		DSP_PDN(OFF);
		MCU_AMP_ST_BY(OFF);
		MCU_AMP_MUTE(ON);	//ADD
		#ifdef B1_MOTHER_BOARD_SAMPLE
		USB_PW_EN(OFF);
		PPRINTF(DBG_MSG3,"USB_PW_EN:OFF\n\r");
		MCU_HU_LCD_EN(ON);
		#endif
		MCU_LCD1_2_EN_3P3(ON);
		MCU_CONTROL_PMIC_ONOFF(OFF);
		M_MAIN_PWR_EN(OFF);
		PMIC_RESET_IN(OFF);
		TFT_PW_EN(OFF);	
		MCU_TFT2_PWR_EN(OFF);//MCU_TFT2_PWR_EN
//not used		MIC_PWR_EN_1(OFF);
		MIC_PWR_EN_2(OFF);
		ATB_3V3_EN(OFF);
		M_5V_FRQ_SEL(OFF);
		GPS_MD_PW_EN(OFF);
		MCU_UPGRADE_SOC(OFF);//JANG_211025_1
		#if 0 //def SI_LABS_TUNER
		SiLabs_Tuner_RST(OFF); //JANG_211025_1
		//#else
		IRLED_CTL(OFF);
		#endif
		DSP_PW_EN(OFF);//DSP_PW_EN
		BAT_ADC_ON(OFF);//BAT_ADC_ON
		M_5V_EN(OFF);		
		PPRINTF(DBG_INFO,"PWR_OFF_ALL\n\r");
		dev->r_Power->p_state = PWR_IDLE;
		break;
	}
}

