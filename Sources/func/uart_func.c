/*
 * uart_func.c
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#include "includes.h"


USART_DEV_t usart0_dev;
USART_DEV_t usart1_dev;

void init_uart0(void)
{
	memset(&usart0_dev, 0, sizeof(USART_DEV_t));
}

void init_uart1(void)
{
	memset(&usart1_dev, 0, sizeof(USART_DEV_t));
}

//uart 0
uint16_t kbhit0(void)
{		
#ifdef NEW_URAT_RX0
	return q_data_size(&uart0_rx_q);
#else
	return (!empty_que());
#endif
}

uint8_t uputc0(char c)
{
	#ifdef POLLING_URAT_TX0
	while((LPUART0->STAT & LPUART_STAT_TDRE_MASK)>>LPUART_STAT_TDRE_SHIFT==0){};
  	LPUART0->DATA = c;  
//	while((LPUART0->STAT & LPUART_STAT_TC_MASK)>>LPUART_STAT_TC_SHIFT==0){};
	#else
	q_en(&uart0_tx_q, (uint8)c);
	SET_BIT(LPUART0->CTRL, LPUART_CTRL_TIE_SHIFT);
	#endif
}

uint8_t uputs0(char* str)
{
	while (*str)uputc0(*str++);
	#ifndef POLLING_URAT_TX0
	SET_BIT(LPUART0->CTRL, LPUART_CTRL_TIE_SHIFT);
	#endif	
	return 0;
}

uint8_t ugetc0(void)
{
	uint8_t d;
	
	#ifdef NEW_URAT_RX0
	d = q_de(&uart0_rx_q) ;
	#else	
	d = pull_que();
	#endif
	
	return d;
}

void uputa0(uint8_t* tx_data, uint8_t packet_size)
{
	uint8_t i;
	
	for(i=0 ; i<packet_size ; i++)
	{
		uputc0(*(tx_data+i));
	}
	#ifndef POLLING_URAT_TX0
	SET_BIT(LPUART0->CTRL, LPUART_CTRL_TIE_SHIFT);
	#endif
}

uint16_t kbhit1(void)
{
#ifdef NEW_URAT_RX1
		return q_data_size(&uart1_rx_q);
#else
	return usart1_dev.rx_buf_cnt;
#endif
}

uint8_t uputc1(char c)
{
	#ifdef POLLING_URAT_TX1
	uint16_t timeout = 0;
	
	while((LPUART1->STAT & LPUART_STAT_TDRE_MASK)>>LPUART_STAT_TDRE_SHIFT==0){};
  	LPUART1->DATA = c;  
//	while((LPUART1->STAT & LPUART_STAT_TC_MASK)>>LPUART_STAT_TC_SHIFT==0){};
	#else
	q_en(&uart1_tx_q, (uint8)c);
	SET_BIT(LPUART1->CTRL, LPUART_CTRL_TIE_SHIFT);
	#endif
}

uint8_t uputs1(char* str)
{
	while (*str)uputc1(*str++);
	#ifndef POLLING_URAT_TX1
	SET_BIT(LPUART1->CTRL, LPUART_CTRL_TIE_SHIFT);
	#endif
	
	return 0;
}

int8_t ugetc1(void)
{
	char d;
	#ifdef NEW_URAT_RX1
	d = q_de(&uart1_rx_q) ;
	#else
	while (!usart1_dev.rx_buf_cnt) {}
	d = usart1_dev.rx_buf[usart1_dev.rx_buf_rd];
	usart1_dev.rx_buf_rd++;
	usart1_dev.rx_buf_rd &= (MAX_USART_RX_BUF - 1);
	usart1_dev.rx_buf_cnt--;
//	LPUART1->CTRL |=LPUART_CTRL_RE(1); //add
	#endif
	
	return (int8_t)d;
}

void uputa1(uint8_t* tx_data, uint8_t packet_size)
{
	uint8_t i;
	
	for(i=0 ; i<packet_size ; i++)
	{
		uputc1(*(tx_data+i));
	}
	#ifndef POLLING_URAT_TX1
	SET_BIT(LPUART1->CTRL, LPUART_CTRL_TIE_SHIFT);
	#endif
}

