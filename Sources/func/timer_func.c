/*
 * timer_func.c
 *
 *  Created on: 2019. 1. 16.
 *      Author: Digen
 */


#include "includes.h"

static TimerDrv_t adckey;
static uint32 key_adc_valule;

//ADC0
#define ADC0_SW_AD1 										8
#define ADC0_SW_AD2 										9

#ifdef CX62C_FUNCTION
static uint32 key_cluster_valule;

#define ADC0_CLUSTER_AD1P							3
#define ADC0_CLUSTER_AD1M							14
#endif
//ADC1
#define BAT_ADC												7
#define HW_PCB_VER											10

static void InitADCkey(void);

void Init_TimerDrv(void)
{
	InitADCkey();
}

void Timer_Task(void)
{
	TimerDrv_Task(&adckey);
}

static void InitADCkey(void)
{
	memset(&adckey, 0x00, sizeof(TimerDrv_t));
	adckey.FunCmd = Process_ADC_key;
	adckey.GetTickCount = GetTickCount;
	adckey.elapsed =5;
	adckey.start =0;
	adckey.active =0;
}

void ADCkey_Enable(uint8_t OnOff)
{
	adckey.active = OnOff;
}


uint32 Check_AD1_Key(void)
{
	uint32 adc_value=0;

	Enable_ADC0_Ch(ADC0_SW_AD1);
	while(Wait_Check_ADC0()==0){}
	adc_value = Read_ADC0_CH();
	Disable_ADC0_Ch();
	
	return adc_value;
}

uint32 Check_AD2_Key(void)
{
	uint32 adc_value=0;

	Enable_ADC0_Ch(ADC0_SW_AD2);
	while(Wait_Check_ADC0()==0){}
	adc_value = Read_ADC0_CH();
	Disable_ADC0_Ch();
	
	return adc_value;
}

#ifdef CX62C_FUNCTION
uint32 Check_Cluster_P_Key(void)
{
	uint32 adc_value=0;

	Enable_ADC0_Ch(ADC0_CLUSTER_AD1P);
	while(Wait_Check_ADC0()==0){}
	adc_value = Read_ADC0_CH();
	Disable_ADC0_Ch();
	
	return adc_value;
}

uint32 Check_Cluster_M_Key(void)
{
	uint32 adc_value=0;

	Enable_ADC0_Ch(ADC0_CLUSTER_AD1M);
	while(Wait_Check_ADC0()==0){}
	adc_value = Read_ADC0_CH();
	Disable_ADC0_Ch();
	
	return adc_value;
}
#endif
uint32 Check_Batt_Value(void)
{
	uint32 adc_value=0;

	Enable_ADC1_Ch(BAT_ADC);
	while(Wait_Check_ADC1()==0){}
	adc_value = Read_ADC1_CH();
	Disable_ADC1_Ch();
	
	return adc_value;
}

uint32 Check_HW_PCB_Ver(void)
{
	uint32 adc_value=0;

	Enable_ADC1_Ch(HW_PCB_VER);
	while(Wait_Check_ADC1()==0){}
	adc_value = Read_ADC1_CH();
	Disable_ADC1_Ch();
	
	return adc_value;
}


//test value
void Process_ADC_key(void)
{
//	Adc_Key_Parser0((uint32_t)POWER_KEY());	// 5ms timer_callback

	key_adc_valule = Check_AD1_Key();
	Adc_Key_Parser1(key_adc_valule);

	key_adc_valule = Check_AD2_Key();
	Adc_Key_Parser2(key_adc_valule);
	#ifdef CX62C_FUNCTION
	key_cluster_valule = Check_Cluster_P_Key();
	Cluster_Key_Parser1(key_cluster_valule);
	key_cluster_valule = Check_Cluster_M_Key();
	Cluster_Key_Parser2(key_cluster_valule);
	#endif
}




