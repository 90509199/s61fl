/*
 * GPIO_FUNC.c
 *
 *  Created on: 2018. 10. 10.
 *      Author: Digen
 */

#include "includes.h"
//OUTPUT

//TUNER_PW_EN, DSP_PW_EN
void DSP_PW_EN(uint8 OnOff)
{
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTB->PSOR |= (1<<11);
	else 
		PTB->PCOR |= (1<<11);
}

void MCU_AMP_ST_BY(uint8 OnOff)
{
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTA->PSOR |= (1<<12);
	else 
		PTA->PCOR |= (1<<12);
}

void MCU_AMP_MUTE(uint8 OnOff)
{
	//HIGH ACTIVE			
	if(OnOff == ON)
		PTA->PSOR |= (1<<13);		//mute
	else 
		PTA->PCOR |= (1<<13);		//no mute
}

void ATB_3V3_EN(uint8 OnOff)
{
//HIGH ACTIVE			
	if(OnOff == ON)
		PTA->PSOR |= (1<<6);		//ON
	else 
		PTA->PCOR |= (1<<6);		//OFF
}

void Tuner_CS_EN(uint8 OnOff)
{
//HIGH ACTIVE
	if(OnOff == ON)
		PTA->PSOR |= (1<<9);		//ON
	else
		PTA->PCOR |= (1<<9);		//OFF
}

#if 0 //def SI_LABS_TUNER
void SiLabs_Tuner_RST(uint8 OnOff)
{
	if(OnOff == ON)
		PTC->PSOR |= (1<<11);		//ON
	else 
		PTC->PCOR |= (1<<11);		//OFF
}
#endif

//need to check
void M_5V_FRQ_SEL(uint8 OnOff)
{	
	if(OnOff == ON)
		PTA->PSOR |= (1<<15);
	else 
		PTA->PCOR |= (1<<15);
}
//need to check
void BAT_ADC_ON(uint8 OnOff)
{		
	if(OnOff == ON)
		PTA->PSOR |= (1<<17);
	else 
		PTA->PCOR |= (1<<17);
}

void TFT_PW_EN(uint8 OnOff)
{		
	#ifdef TFT_PW_FROM_GPIO_TO_PWM
	if(OnOff==ON)
		FTM0_start_counter();
	else
	{
		FTM0_stop_counter();
		FTM0->CNTIN =0x00000000;
		FTM0->SC = 0x00000000; /* disable PWM channel 2,5 output*/
		FTM0->MODE |=FTM_MODE_INIT_MASK;
		FTM0->MODE &= ~FTM_MODE_WPDIS_MASK; /* Write protect to registers disabled (default) */
		DPRINTF(DBG_MSG3,"TFT_PW_EN:OFF\n\r");
	}
	#else
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTB->PSOR |= (1<<4);
	else 
		PTB->PCOR |= (1<<4);
	#endif
}
//need to check
void GPS_MD_PW_EN(uint8 OnOff)
{		
	if(OnOff == ON)
		PTB->PSOR |= (1<<5);
	else 
		PTB->PCOR |= (1<<5);
}

void MIC_PWR_EN_1(uint8 OnOff)
{		
	if(OnOff == ON)
		PTB->PSOR |= (1<<8);
	else 
		PTB->PCOR |= (1<<8);
}

#ifdef B1_MOTHER_BOARD_SAMPLE
void USB_PW_EN(uint8 OnOff)
{		
	if(OnOff == ON)
		PTB->PSOR |= (1<<13);
	else 
		PTB->PCOR |= (1<<13);
}
#else
void USB_5V_FRQ_SEL(uint8 OnOff)
{		
	if(OnOff == ON)
		PTB->PSOR |= (1<<13);
	else 
		PTB->PCOR |= (1<<13);
}
#endif

void DSP_PDN(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTC->PSOR |= (1<<6);
	else 
		PTC->PCOR |= (1<<6);
}

#ifndef SI_LABS_TUNER
void IRLED_CTL(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTC->PSOR |= (1<<11);
	else 
		PTC->PCOR |= (1<<11);
}
#endif

void MCU_UPGRADE_SOC(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTC->PSOR |= (1<<17);
	else 
		PTC->PCOR |= (1<<17);
}

void M_5V_EN(uint8 OnOff)
{
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<1);
	else 
		PTD->PCOR |= (1<<1);
}

#ifdef B1_MOTHER_BOARD_SAMPLE
void MCU_HU_LCD_EN(uint8 OnOff)
{
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<4);
	else 
		PTD->PCOR |= (1<<4);
}
#endif

void CAN0_WAKE_N(uint8 OnOff)
{
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<6);
	else 
		PTD->PCOR |= (1<<6);
}

void MIC_PWR_EN_2(uint8 OnOff)
{
	#ifdef B1_MOTHER_BOARD_SAMPLE
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<5);
	else 
		PTD->PCOR |= (1<<5);
	#else
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<7);
	else 
		PTD->PCOR |= (1<<7);
	#endif
}

void MCU_LCD1_2_EN_3P3(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<9);
	else 
		PTD->PCOR |= (1<<9);
}

void MCU_CONTROL_PMIC_ONOFF(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<10);
	else 
		PTD->PCOR |= (1<<10);
}

void MCU_TFT2_PWR_EN(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTD->PSOR |= (1<<11);
	else 
		PTD->PCOR |= (1<<11);
}

void M_GPIO_TEST(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTE->PSOR |= (1<<0);
	else 
		PTE->PCOR |= (1<<0);
}


void M_MAIN_PWR_EN(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTE->PSOR |= (1<<3);
	else 
		PTE->PCOR |= (1<<3);
}
	
void MCU_CAN0_EN(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTE->PSOR |= (1<<10);
	else 
		PTE->PCOR |= (1<<10);
}

void MCU_CAN0_STB(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTE->PSOR |= (1<<11);
	else 
		PTE->PCOR |= (1<<11);
}
	
//need to check //cpu reset
void PMIC_RESET_IN(uint8 OnOff)
{		
	//HIGH ACTIVE	
	if(OnOff == ON)
		PTE->PSOR |= (1<<14);
	else 
		PTE->PCOR |= (1<<14);
}

//INPUT
uint8 MCU_ACC_DET(void)
{	
	uint8 ret=0;
	
	//default high, low is active
	if(PTA->PDIR&(1<<14))
		ret = OFF;
	else
		ret = ON;
	
	return ret;
}

//need to check
uint8 MCU_LOW_BAT(void)
{	
	uint8 ret=0;
	
	if(PTA->PDIR&(1<<16))
		ret = OFF;
	else
		ret = ON;
	
	return ret;
}

uint8 PS_HOLD(void)
{	

	uint8 ret=0;

	if(PTC->PDIR&(1<<8))
		ret = ON;
	else
		ret = OFF;
	
	return ret;
}

uint8 HU_BL_REQ(void)
{ 

	uint8 ret=0;

	if(PTC->PDIR&(1<<9))
		ret = ON;
	else
		ret = OFF;
	
	return ret;
}


uint8 POWER_KEY(void)
{	
	uint8 ret=0;

	if(PTC->PDIR&(1<<13))
		ret = OFF;
	else
		ret = ON;

	return ret;
}

uint8 MCU_TBOX_MUTE(void)
{	
	uint8 ret=0;

	if(PTD->PDIR&(1<<8))
		ret = ON;
	else
		ret = OFF;
	
	return ret;
}

//need to check
uint8 MCU_IGN_DET(void)
{	
	uint8 ret=0;

	//default high, low is active
	if(PTD->PDIR&(1<<12))
		ret = OFF;
	else
		ret = ON;
	
	return ret;
}

uint8 GPIO_CONTROL(void)
{	

	uint8 ret=0;

	if(PTE->PDIR&(1<<1))
		ret = ON;
	else
		ret = OFF;
	
	return ret;
}

uint8 TFT1_PWR_OCP_MCU(void)
{		
	uint8 ret=0;

	if(PTE->PDIR&(1<<8))
		ret = ON;
	else
		ret = OFF;
	
	return ret;
}

uint8 MCU_HIGH_BAT(void)
{	
	uint8 ret=0;

	if(PTE->PDIR&(1<<9))
		ret = OFF;
	else
		ret = ON;
	
	return ret;
}
	
uint8 MCU_KNOB_B(void)
{	
	uint8 ret=0;

	if(PTE->PDIR&(1<<12))
		ret = OFF;
	else
		ret = ON;
	
	return ret;
}

uint8 MCU_KNOB_A(void)
{	
	uint8 ret=0;

	if(PTE->PDIR&(1<<13))
		ret = OFF;
	else
		ret = ON;
	
	return ret;
}


