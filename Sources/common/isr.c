/*
 * isr.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */
#include "includes.h"
#include "Service/srv_mcan.h"


volatile uint8 nm_wakeup_flag=0;
volatile uint8 ignacc_wakeup_flag=0;
volatile uint8 low_batter_flag=0;
#ifdef SI_LABS_TUNER
extern U16 TmrTunerTaskSeq;						/* 1ms base timer */
#endif
extern void BusoffDTCDetect(void);
extern uint8 GetCANTxConfirmation(void);

void LPIT0_Ch0_IRQHandler (void) 
{
	LPIT0->MSR |= LPIT_MSR_TIF0_MASK; /* Clear LPIT0 timer flag 0 */
	/* Perform read-after-write to ensure flag clears before ISR exit */
//	PTC->PTOR |= 1<<11;   	//TEST CLK

	timertick_1ms ++;
	StdTimerTick();
	#ifdef SI_LABS_TUNER
	if(TmrTunerTaskSeq>0)
		TmrTunerTaskSeq --;
	#endif
	CheckDTCStartCondition();
	if(GetDTCStartProcessState() == 1u)
	{
		/* Not in busoff state, allow to detect CommDTC */
		if(GetCANTxConfirmation() == 1u)
		{
			CommDTCDetect();
		}
		else
		{
			/* Do nothing */
		}
		BusoffDTCDetect();
	}
	else
	{
		/* Do nothing */
	}
}

void PORTA_IRQHandler(void)
{
	//check to port14  //ACC
	if(PORTA->PCR[14]&(1<<24))
	{
		PORTA->PCR[14] |= PORT_PCR_ISF(0);
		ignacc_wakeup_flag = ON;
	}

		//check to port16
	if(PORTA->PCR[16]&(1<<24))
	{
		PORTA->PCR[16] |= PORT_PCR_ISF(0);
		if(low_batter_flag==ON)
		{
			//RecordReset(5,NULL);
			//SystemSoftwareReset();
		}
	}
}


void PORTC_IRQHandler(void)
{
	//check to port7
	if(PORTC->PCR[7]&(1<<24))
	{
		PORTC->PCR[7] |= PORT_PCR_ISF(0);
		nm_wakeup_flag = ON;// need to check this function.
	}

	//check to port13
	if(PORTC->PCR[13]&(1<<24))
	{
		PORTC->PCR[13] |= PORT_PCR_ISF(0);
	}
}

/*         // 220518 KHI
void PORTC_IRQHandler(void)
{
	//check to port7
	if(PORTC->PCR[7]&(1<<24))
	{
		PORTC->PCR[7] |= PORT_PCR_ISF(0);
		nm_wakeup_flag = ON;// need to check this function.
	}

	//check to port13
	if(PORTC->PCR[13]&(1<<24))
	{
		PORTC->PCR[13] |= PORT_PCR_ISF(0);
	}
}
*/
void PORTD_IRQHandler(void)
{

	//check to port12 IGN
	if(PORTD->PCR[12]&(1<<24))
	{
		PORTD->PCR[12] |= PORT_PCR_ISF(0);
		ignacc_wakeup_flag = ON;
	}
}


