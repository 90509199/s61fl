/*
 * shelldrv.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef SYS_SHELLDRV_H_
#define SYS_SHELLDRV_H_

#include <inttypes.h>

#define MAX_COUNT_CMD			32
#define MAX_COUNT_CMD_PARAM 		16
#define MAX_SIZE_PROMPT 		16
#define MAX_SIZE_MESSAGE		128
#define MAX_SIZE_CMD			16
#define MAX_SIZE_CMD_PARAM		16
#define MAX_SIZE_BANNER 		50

#define SHELL_TRUE				1
#define SHELL_FALSE 			-1

#define SHELL_NULL				0x00
#define SHELL_CR				0x0D
#define SHELL_LF				0x0A
#define SHELL_SPACE 			0x20
#define SHELL_BACKSPACE 		0x7F

#define EMPTY_USAGE_HELP_STR	"notapplicable (N/A)\n"


#ifndef NULL
# define NULL ((void*)0)
#endif


enum
{
	SH_ERR_NONE,
	SH_ERR_GETCHAR_NULL,
	SH_ERR_PUTCHAR_NULL,
	SH_ERR_KBHIT_NULL
};

enum
{
	INVALID_CMD,
	OVER_INPUT_BUF,
	OVER_COUNT_CMD_PARAM,
	CMD_FUNCTION_NULL
};

struct SHELL_INBUF
{
	int8_t		msgBuf[MAX_SIZE_MESSAGE];
	uint8_t 	idx;
	int8_t		isCmdMode;
};
typedef struct SHELL_INBUF SHELL_INBUF_t;

struct SHELL_PARSER_CMDATA
{
	uint8_t*	cmdName;
	uint8_t 	optCount;
	uint8_t*	optVal[MAX_COUNT_CMD_PARAM];
};
typedef struct SHELL_PARSER_CMDATA SHELL_PARSER_CMDATA_t;

struct SHELL_CMD
{
	int8_t* 	cmdName;
	void		(*CmdExecFunc)(uint8_t _argc, uint8_t* _argv[]);
	int8_t* 	cmdUsage;
	int8_t* 	cmdHelp;
};
typedef struct SHELL_CMD SHELL_CMD_t;

struct SHELLDRV_OBJECT
{
	uint8_t 		(*GetCharFunc)(void);
	int8_t			(*PutCharFunc)(uint8_t);
	uint16_t		(*KbhitFunc)(void);
	SHELL_CMD_t*	cmdList;
	SHELL_INBUF_t	shellBufData;
	int8_t			shellErr;
	uint8_t 		cmdListCnt;
	uint8_t 		cmdMaxLen;
};
typedef struct SHELLDRV_OBJECT SHELLDRV_OBJECT_t;

void ShellShowReadyBanner(SHELLDRV_OBJECT_t* _sObj);
void InitShellDrv(SHELLDRV_OBJECT_t* _sObj);
void ShellDrvTask(SHELLDRV_OBJECT_t* _sObj);



#endif /* SYS_SHELLDRV_H_ */
