/*
 * tick.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef SYS_TICK_H_
#define SYS_TICK_H_

extern volatile uint32_t timertick_1ms;

uint32_t GetTickCount(void);
void SetTickCount(uint32 value);
uint32_t GetElapsedTime(uint32_t start);

void delay_us(uint32_t us);
void delay_ms(uint32_t ms);


#endif /* SYS_TICK_H_ */
