/*
 * tick.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#include "includes.h"

volatile uint32_t timertick_1ms = 0;

uint32_t GetTickCount(void)
{
	return timertick_1ms;
}

void SetTickCount(uint32 value)
{
	timertick_1ms = value;
}


uint32_t GetElapsedTime(uint32_t start)
{
	uint32_t current;
	uint32_t elapsed;

	current = GetTickCount();

	if (current == start) return 0;

	if (current > start) elapsed = current - start;
	else elapsed = (0xffffffff - start) + current;

	return elapsed;
}


#define SYS_CLK_FREQ 80000000UL

void delay_us(uint32_t us)
{
	uint16_t clk_cnt;
	uint16_t clk_1us;
	uint32_t us_cnt = 0;

//	clk_1us = (uint16_t)(SYS_CLK_FREQ /80000000UL);

	while (us_cnt < us)
	{
//		for(clk_cnt=0 ; clk_cnt < clk_1us ; clk_cnt++)
//		{
//			asm (" nop");
//		}
		us_cnt++;
	}
}

void delay_ms(uint32_t ms)
{
	uint32_t start;
	uint32_t current;
	uint32_t elapsed = 0;

	start = timertick_1ms;

	while(elapsed < ms)
	{
		current = timertick_1ms;

		if (current >= start)
			elapsed = current - start;
		else
			elapsed = 0xffffffff - start + current;
	}
}

