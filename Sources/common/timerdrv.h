/*
 * timerdrv.h
 *
 *  Created on: 2019. 1. 16.
 *      Author: Digen
 */

#ifndef SYS_TIMERDRV_H_
#define SYS_TIMERDRV_H_

typedef struct{
	void(*FunCmd)();
	uint32_t(*GetTickCount)();
	uint32_t elapsed;
	uint32_t start;
	uint8_t active;
}TimerDrv_t;

void TimerDrv_Init(TimerDrv_t* timer);
void TimerDrv_Task(TimerDrv_t* timer);

#endif /* SYS_TIMERDRV_H_ */
