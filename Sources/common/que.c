/*
 * que.c
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#include "includes.h"


void init_q(QUE_t* q, void* buf, uint16_t len)
{
	memset((void*)q, 0, sizeof(QUE_t));
	q->buf  = buf;
	q->size = len;

	if ((q == NULL) || (buf == NULL) || (len == 0))
	{
		DPRINTF(DBG_WARN, "Must be initialsize first prior to use \n");
	}
	//	q_debug( q );

	memset((void*)q->buf, 0, (size_t)q->size);
}

// usage : q_en( &Qenc1, data ) ;
void q_en(QUE_t* q, uint8_t d)
{
	q->buf [ q->head++ ] = d;
	q->head %= q->size;
}


// usage : ret = q_de( &Qenc1 ) ;
uint8_t q_de(QUE_t* q)
{
	uint8_t ret;

	ret = q->buf[ q->tail++ ];
	q->tail %= q->size;

	return ret;
}


// usage: if ( q_data_size( &Qenc1 ) >=1 )  ret= q_de( &Qenc1 );
uint16_t q_data_size(QUE_t* q)
{
	uint16_t ret;

	ret = ((q->size + q->head) - q->tail) % q->size;
	return ret;
}


// don't backup the tail pointer,
void q_clear(QUE_t* q)
{
	q->tail = q->head;
}


//  this function do not touch tail pointer, then you must call q_clear( &q) prior to use.
uint8_t q_get_last(QUE_t* q)
{
	uint8_t ret;

	ret = q->buf[(q->head + q->size - 1) % q->size  ];
	return ret;
}

// 1 means last_data, 5 means last-5 data */
uint8_t q_buf_check_index_data(QUE_t* q, uint16_t index)
{
	uint8_t ret;

	ret = q->buf[ (q->head + q->size - index) % q->size ];
	return ret;
}

void q_copy(QUE_t* q, void* buf, uint16_t len, uint16_t index)
{
	uint16_t i;
	uint8_t* pbuf;

	pbuf = (uint8_t*)buf;

	q->tail = (q->head + q->size - index) % q->size;

	//DPRINTF(ERROR,"q_copy:");
	for (i = 0; i < len ; i++)
	{
		pbuf[i] = q_de(q) ;
		//DPRINTF(ERROR,"[%02x] ",pbuf[i]);
	}
	//DPRINTF(ERROR,"\n");
}

void q_debug(QUE_t* q)
{
	uint16_t i;

	DPRINTF(DBG_WARN, "\t====size== %d \n", q->size);
	DPRINTF(DBG_WARN, "\tq_head = %d\n", q->head);
	DPRINTF(DBG_WARN, "\tq_tail = %d\n", q->tail);
	for (i = 0; i < q->size ; i++)
	{
		DPRINTF(DBG_WARN, "\tq[%d] = %x\n", i, q->buf[i]);
	}
	DPRINTF(DBG_WARN, "\t===========\n", q->head);
}

#if 1  //JANG_211121
#define MAX 1024
#else
#define MAX 255
#endif
int front=-1;
int rear=-1;
uint8 queue[MAX];

int empty_que(void)
{
    if(front==rear)//front와 rear가 같으면 큐는 비어있는 상태 
        return 1;
    else 
		return 0;
}

int full_que(void)
{
    int tmp=(rear+1)%MAX; //원형 큐에서 rear+1을 MAX로 나눈 나머지값이
    if(tmp==front)//front와 같으면 큐는 가득차 있는 상태 
        return 1;
    else
        return 0;
}

void push_que(uint8 value)
{
    if(full_que())
		DPRINTF(DBG_WARN, "Queue is Full.\n");
    else
	{
         rear = (rear+1)%MAX;
         queue[rear]=value;
	}
}

int pull_que()
{
    if(empty_que())
		DPRINTF(DBG_WARN, "Queue is Empty.\n");
    else
	{
        front = (front+1)%MAX;
        return queue[front];
    }
}

#ifdef DSP_DOUBLE_QUE
void init_q2(QUE2_t* q, void* buf, uint16_t len)
{
	memset((void*)q, 0, sizeof(QUE2_t));
	q->buf	= buf;
	q->size = len;

	if ((q == NULL) || (buf == NULL) || (len == 0))
	{
		DPRINTF(DBG_WARN, "Must be initialsize first prior to use \n");
	}

	memset((void*)q->buf, 0, (size_t)q->size);
}

void q2_en(QUE2_t* q, uint8_t state,uint8_t data)
{
	uint16_t  tmp = q->head++ ;
	
	q->buf[tmp].q_state = state;
	q->buf[tmp].q_data = data;
	q->head %= q->size;
}

uint8_t q2_de(QUE2_t* q)
{
	uint8_t ret;
	uint16_t  tmp = q->tail++ ;

	ret = q->buf[ tmp ].q_state;
	q->buf[ tmp].q_data;
	q->tail %= q->size;

	return ret;
}

uint16_t q2_data_size(QUE2_t* q)
{
	uint16_t ret;

	ret = ((q->size + q->head) - q->tail) % q->size;
	return ret;
}

void q2_clear(QUE2_t* q)
{
	q->tail = q->head;
}
#endif

