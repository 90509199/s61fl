/*
 * shelldrv.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#include "includes.h"

static void 	ShellInitRecvBuf(SHELL_INBUF_t* _sobjBuf);
static void 	ShellSetCmdMaxLen(SHELLDRV_OBJECT_t* __sObj);
static void 	ShellOutputTask(SHELLDRV_OBJECT_t* __sObj, int8_t* _outStr, uint16_t len);
static int8_t	ShellParserRecv(SHELLDRV_OBJECT_t* __sObj);
static void 	ShellShowTotalCmdHelp(SHELLDRV_OBJECT_t* __sObj);
static void 	ShellShowCmdHelpUsage(SHELLDRV_OBJECT_t* __sObj, int8_t* _helpUsageStr);
static void 	ShellExeCommand(SHELLDRV_OBJECT_t* _sObj, SHELL_PARSER_CMDATA_t* _cmdata);
static void 	ShellBackSpaceRecv(SHELL_INBUF_t* _sobjBuf);
static int8_t	ShellSaveRecv(SHELL_INBUF_t* _sobjBuf, int8_t _c);
static int8_t	ShellFindUserCmd(uint8_t* _inCmdName, SHELLDRV_OBJECT_t* __sObj);
static void 	ShellDispPrompt(SHELLDRV_OBJECT_t* __sObj);
static void 	ShellDispError(SHELLDRV_OBJECT_t* __sObj, uint8_t _errno, uint8_t* _res1);

/**
@brief		: Print the shell banner.
@remarks
@param		: SHELLDRV_OBJECT_t* _sobj
@return 	: void
*/
void ShellShowReadyBanner(SHELLDRV_OBJECT_t* _sObj)
{
	uint8_t cnt;
	int8_t	banner[][MAX_SIZE_BANNER] =
	{
		"\n----------------------------------------------\n",
		"||  ..Ready to execute shell driver.. ^-^*  ||\n",
		"||   if you want to start the shell mode,   ||\n",
		"||      please push the your enter Key.     ||\n",
		"----------------------------------------------\n"
	};

	for(cnt = 0; cnt < 5; ++cnt)
	{
		ShellOutputTask(_sObj, banner[cnt], (uint16_t)strlen((char*)banner[cnt]));
	}

}

/**
@brief		: Initialize the data Received.
@remarks
@param		: SHELL_INBUF_t* _sobjBuf
@return 	: void
*/
void ShellInitRecvBuf(SHELL_INBUF_t* _sobjBuf)
{
	memset((char*)_sobjBuf->msgBuf, 0x00, sizeof(SHELL_INBUF_t));
	_sobjBuf->isCmdMode = SHELL_FALSE;
	_sobjBuf->idx = 0;
}

/**
@brief		: Set the length of longest command name.
@remarks
@param		: SHELLDRV_OBJECT_t* __sObj
@return 	: void
*/
void ShellSetCmdMaxLen(SHELLDRV_OBJECT_t* __sObj)
{
	uint8_t cnt;
	uint8_t len = 0;

	for (cnt = 0; cnt < __sObj->cmdListCnt; ++cnt)
	{
		if (len < (strlen((char*)__sObj->cmdList[cnt].cmdName)))
		{
			len = (uint8_t)strlen((char*)__sObj->cmdList[cnt].cmdName);
		}
	}

	__sObj->cmdMaxLen = len;
}

/**
@brief		: Initialize the shell Data.
@remarks
@param		: SHELLDRV_OBJECT_t* _sObj
@return 	: void
*/
void InitShellDrv(SHELLDRV_OBJECT_t* _sObj)
{
	_sObj->shellErr = SH_ERR_NONE;

	if(_sObj->GetCharFunc == NULL)
		_sObj->shellErr = SH_ERR_GETCHAR_NULL;

	if(_sObj->PutCharFunc == NULL)
		_sObj->shellErr = SH_ERR_PUTCHAR_NULL;

	if(_sObj->KbhitFunc == NULL)
		_sObj->shellErr = SH_ERR_KBHIT_NULL;

	if(_sObj->shellErr != SH_ERR_NONE)
		return;

	ShellSetCmdMaxLen(_sObj);
	ShellInitRecvBuf(&(_sObj->shellBufData));

	ShellShowReadyBanner(_sObj);
}

/**
@brief		: Write the string by outputFunc set by user.
@remarks
@param		: SHELLDRV_OBJECT_t* __sObj,
			  char* _outStr,
			  uint16_t len
@return 	: void
*/
void ShellOutputTask(SHELLDRV_OBJECT_t* __sObj, int8_t* _outStr, uint16_t len)
{
	uint16_t	cnt = 0;

	for (cnt = 0; cnt < len; ++cnt)
	{
		__sObj->PutCharFunc((uint8_t)_outStr[cnt]);
	}
}

/**
@brief		: Execute the shell task to help commands.
@remarks
@param		: SHELLDRV_OBJECT_t* _sObj
@return 	: void
*/
void ShellDrvTask(SHELLDRV_OBJECT_t* _sObj)
{
	int8_t			ret;
	int8_t			c;
	SHELL_INBUF_t*	sobjBuf;

	if(_sObj->shellErr != SH_ERR_NONE)
		return;

	sobjBuf = &(_sObj->shellBufData);

	if (_sObj->KbhitFunc())
	{
		c = (int8_t)_sObj->GetCharFunc();
		if(c == SHELL_LF)
			return;

		if (sobjBuf->isCmdMode == SHELL_TRUE)
		{
			ShellOutputTask(_sObj, &c, (uint16_t)sizeof(char));

			switch (c)
			{
			case SHELL_CR :
				if (sobjBuf->idx != 0)
				{
					ShellParserRecv(_sObj);
					ShellDispPrompt(_sObj);
				}
				else
				{
					ShellDispPrompt(_sObj);
				}
				break;
			case SHELL_BACKSPACE :
				ShellBackSpaceRecv(sobjBuf);
				break;
			default :
				ret = ShellSaveRecv(sobjBuf, c);
				if (ret == SHELL_FALSE)
				{
					ShellDispError(_sObj, OVER_INPUT_BUF, NULL);
					ShellInitRecvBuf(sobjBuf);
				}
				break;
			}
		}
		else
		{
			if (c == SHELL_CR)
			{
				ShellDispPrompt(_sObj);
			}
		}
	}
}

/**
@brief		: Parsing the data received.
@remarks
@param		: SHELLDRV_OBJECT_t* __sObj
@return 	: int8_t
			  [Success] return SHELL_TRUE
			  [Fail]	return SHELL_FASLE
*/
int8_t ShellParserRecv(SHELLDRV_OBJECT_t* __sObj)
{
	int8_t		ret = SHELL_TRUE;
	uint8_t 	tempMsgBuf[MAX_SIZE_MESSAGE];
	uint8_t 	cnt, len;
	uint8_t*	pos;
	SHELL_PARSER_CMDATA_t	sCmdata;

	memset((char*)&sCmdata, 	0x00, sizeof(SHELL_PARSER_CMDATA_t));
	memset((char*)tempMsgBuf, 0x00, MAX_SIZE_MESSAGE);
	strcpy((char*)tempMsgBuf, (char*)__sObj->shellBufData.msgBuf);

	len = (uint8_t)strlen((char*)tempMsgBuf);
	pos = tempMsgBuf;
	sCmdata.cmdName = pos;

	for (cnt = 0; cnt < len; ++cnt)
	{
		if (tempMsgBuf[cnt] == SHELL_SPACE)
		{
			tempMsgBuf[cnt] = '\0';
			pos = &tempMsgBuf[cnt + 1];

			if (sCmdata.optCount >= MAX_COUNT_CMD_PARAM)
			{
				ShellDispError(__sObj, OVER_COUNT_CMD_PARAM, sCmdata.cmdName);
				ret = SHELL_FALSE;
				break;
			}
			else
			{
				switch (tempMsgBuf[cnt + 1])
				{
				case SHELL_SPACE:
				case SHELL_NULL:
					break;
				default :
					sCmdata.optVal[sCmdata.optCount] = pos;
					sCmdata.optCount++;
					break;
				}
			}
		}
	}

	if (sCmdata.cmdName == NULL)
	{
		ret = SHELL_FALSE;
	}
	else
	{
		if (ret == SHELL_TRUE)
			ShellExeCommand(__sObj, &sCmdata);
	}

	ShellInitRecvBuf(&(__sObj->shellBufData));

	return ret;
}

/**
@brief		: Display the Data about total Commands, Usage.
@remarks
@param		: SHELLDRV_OBJECT_t* __sObj
@return 	: void
*/
void ShellShowTotalCmdHelp(SHELLDRV_OBJECT_t* __sObj)
{
	uint8_t 	cnt;
	uint8_t 	size;

	int8_t		makeStr[32];
	int8_t* 	makeStr_end;
	SHELL_CMD_t* pos;

	ShellOutputTask(__sObj, (int8_t*)"\n", (uint16_t)strlen("\n"));

	size = __sObj->cmdMaxLen + 5;

	pos  = __sObj->cmdList;
	makeStr_end = &makeStr[size - 1];

	for (cnt = 0; cnt < (__sObj->cmdListCnt); ++cnt)
	{
		*makeStr_end = '\0';
		memset(&makeStr[0], SHELL_SPACE, sizeof(size - 1));
		strncpy((char*)makeStr, (char*)pos[cnt].cmdName, strlen((char*)pos[cnt].cmdName));

		ShellOutputTask(__sObj, (int8_t*)makeStr, (uint16_t)strlen((char*)makeStr));
		ShellShowCmdHelpUsage(__sObj, pos[cnt].cmdUsage);
	}
}

/**
@brief		: Display the Data(ex.help|usage) about command.
@remarks
@param		: SHELLDRV_OBJECT_t* _sObj,
			  SHELL_PARSER_CMDATA_t* _cmdata
@return 	: void
*/
void ShellShowCmdHelpUsage(SHELLDRV_OBJECT_t* __sObj, int8_t* _helpUsageStr)
{
	if (_helpUsageStr == NULL)
	{
		ShellOutputTask(__sObj, (int8_t*)EMPTY_USAGE_HELP_STR, (uint16_t)strlen(EMPTY_USAGE_HELP_STR));
	}
	else
	{
		ShellOutputTask(__sObj, _helpUsageStr, (uint16_t)strlen((char*)_helpUsageStr));
	}
}

/**
@brief		: if command is valid, call the user-defined function with parameter.
@remarks
@param		: SHELLDRV_OBJECT_t* _sObj,
			  SHELL_PARSER_CMDATA_t* _cmdata
@return 	: void
*/
void ShellExeCommand(SHELLDRV_OBJECT_t* _sObj, SHELL_PARSER_CMDATA_t* _cmdata)
{
	int8_t	idx, flag = 0;

	idx = ShellFindUserCmd(_cmdata->cmdName, _sObj);
	if (idx == SHELL_FALSE)
	{
		// totalHelp or Unknown
		if (strcmp((char*)_cmdata->cmdName, (char*)"help") != 0)
		{
			ShellDispError(_sObj, INVALID_CMD, _cmdata->cmdName);
		}
		ShellShowTotalCmdHelp(_sObj);
	}
	else
	{
		// command usage or help or exe
		ShellOutputTask(_sObj, (int8_t*)"\n", (uint16_t)strlen("\n"));
		if(_cmdata->optCount != 0)
		{
			if (strcmp((char*)_cmdata->optVal[0], "usage") == 0)
			{
				ShellShowCmdHelpUsage(_sObj, _sObj->cmdList[idx].cmdUsage);
				flag = 1;
			}
			else
			{
				if (strcmp((char*)_cmdata->optVal[0], "help") == 0)
				{
					ShellShowCmdHelpUsage(_sObj, _sObj->cmdList[idx].cmdHelp);
					flag = 1;
				}
			}
		}

		if(flag != 1)
		{
			if (_sObj->cmdList[idx].CmdExecFunc == NULL)
			{
				ShellDispError(_sObj, CMD_FUNCTION_NULL, _cmdata->cmdName);
			}
			else
			{
				_sObj->cmdList[idx].CmdExecFunc(_cmdata->optCount, _cmdata->optVal);
			}
		}
	}

	ShellInitRecvBuf(&(_sObj->shellBufData));
}

/**
@brief		: process about backspace key.
@remarks
@param		: SHELL_INBUF_t* _sobjBuf
@return 	: void
*/
void ShellBackSpaceRecv(SHELL_INBUF_t* _sobjBuf)
{
	if (_sobjBuf->idx > 0)
	{
		_sobjBuf->idx--;
		_sobjBuf->msgBuf[_sobjBuf->idx] = '\0';
	}
}

/**
@brief		: Save the received character to recvBuffer
@remarks
@param		: SHELL_INBUF_t* _sobjBuf,
			  uint8_t _c
@return 	: int8_t
			  [Success] return SHELL_TRUE
			  [Fail]	return SHELL_FASLE
*/
int8_t ShellSaveRecv(SHELL_INBUF_t* _sobjBuf, int8_t _c)
{
	if (_sobjBuf->idx < MAX_SIZE_MESSAGE)
	{
		_sobjBuf->msgBuf[_sobjBuf->idx++] = _c;
		return SHELL_TRUE;
	}

	return SHELL_FALSE;
}

/**
@brief		: Check whether or not the received command is valid.
@remarks
@param		: uint8_t* _inCmdName,
			  SHELLDRV_OBJECT_t* __sobjCmdList
@return 	: int8_t
			  [Success] return the index of array.
			  [Fail]	return SHELL_FASLE
*/
int8_t ShellFindUserCmd(uint8_t* _inCmdName, SHELLDRV_OBJECT_t* __sObj)
{
	uint8_t 	cnt;
	SHELL_CMD_t*	pos;

	pos = (SHELL_CMD_t*)__sObj->cmdList;

	for (cnt = 0; cnt < __sObj->cmdListCnt ; ++cnt)
	{
		if (strcmp((char*)_inCmdName, (char*)pos->cmdName) != 0)
		{
			pos++;
			continue;
		}

		return (int8_t)cnt;
	}

	return SHELL_FALSE;
}

/**
@brief		: Display the prompt string set by user.
@remarks
@param		: SHELLDRV_OBJECT_t* __sObj
@return 	: void
*/
void ShellDispPrompt(SHELLDRV_OBJECT_t* __sObj)
{
	int8_t sendBuf[MAX_SIZE_MESSAGE];

	ShellInitRecvBuf(&(__sObj->shellBufData));

	memset(sendBuf, 0x00, MAX_SIZE_MESSAGE);

//	sprintf((char*)sendBuf, "\n%sMCU>%s ",ANSI_COLOR_CYAN, ANSI_COLOR_DEFAULT);
       sprintf((char*)sendBuf, "\nMCU>");
	ShellOutputTask(__sObj, sendBuf, (uint16_t)strlen((char*)sendBuf));

	__sObj->shellBufData.isCmdMode = SHELL_TRUE;
}

/**
@brief		: Display the error contents.
@remarks
@param		: SHELLDRV_OBJECT_t* __sobjIO,
			  uint8_t _errno,
			  uint8_t* _res1
@return 	: void
*/
void ShellDispError(SHELLDRV_OBJECT_t* __sObj, uint8_t _errno, uint8_t* _res1)
{
	int8_t sendBuf[MAX_SIZE_MESSAGE];

	memset(sendBuf, 0x00, MAX_SIZE_MESSAGE);

	switch (_errno)
	{
	case INVALID_CMD :
//		sprintf((char*)sendBuf, "\n%s\'%s\' is Unknown Command! Please view 'help'%s\n", ANSI_COLOR_RED, _res1, ANSI_COLOR_DEFAULT);
		break;
	case OVER_INPUT_BUF :
//		sprintf((char*)sendBuf, "\n%sMessage is Long( max : %d )%s\n", ANSI_COLOR_RED, MAX_SIZE_MESSAGE, ANSI_COLOR_DEFAULT);
		break;
	case OVER_COUNT_CMD_PARAM :
//		sprintf((char*)sendBuf, "\n%sCount of CMD's Option is exceeded.( cmd : %s, max : %d )%s\n", ANSI_COLOR_RED, _res1, MAX_COUNT_CMD_PARAM, ANSI_COLOR_DEFAULT);
		break;
	case CMD_FUNCTION_NULL :
//		sprintf((char*)sendBuf, "\n%sFunction of CMD is NULL.( cmd : %s )%s\n", ANSI_COLOR_RED, _res1, ANSI_COLOR_DEFAULT);
		break;
	}

	ShellOutputTask(__sObj, sendBuf, (uint16_t)strlen((char*)sendBuf));
}
