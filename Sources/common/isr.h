/*
 * isr.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef SYS_ISR_H_
#define SYS_ISR_H_


void LPIT0_Ch0_IRQHandler (void);
void PORTA_IRQHandler(void);
void PORTC_IRQHandler(void);
void PORTD_IRQHandler(void);

#endif /* SYS_ISR_H_ */
