/*
 * timerdrv.c
 *
 *  Created on: 2019. 1. 16.
 *      Author: Digen
 */

#include "includes.h"

void TimerDrv_Init(TimerDrv_t* timer)
{
	timer->start	=	0;
}

void TimerDrv_Task(TimerDrv_t* timer)
{
	uint32_t current=0;
	uint32_t elapsed=0;

	//<<check active
	if(timer->active == 0)
		return;
	//>>

	if(timer->start == 0) 
		timer->start = timer->GetTickCount();

	current = timer->GetTickCount();

	if (current == timer->start) return;

	if (current > timer->start) 
		elapsed = current - timer->start;
	else 
		elapsed = 0xffffffff - timer->start + current;

	if (elapsed > timer->elapsed)
	{
		timer->FunCmd();
		timer->start = timer->GetTickCount();
	}
}




