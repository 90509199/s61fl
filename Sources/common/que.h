/*
 * que.h
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */

#ifndef SYS_QUE_H_
#define SYS_QUE_H_

typedef struct QUE
{
	uint8_t* buf;
	uint16_t head;
	uint16_t tail;
	uint16_t size;
} QUE_t;

void init_q(QUE_t* q, void* buf, uint16_t len);
void q_en(QUE_t* q, uint8_t d);
uint8_t q_de(QUE_t* q);
uint16_t q_data_size(QUE_t* q);
void q_clear(QUE_t* q);
uint8_t q_get_last(QUE_t* q);
void q_debug(QUE_t* q);
uint8_t q_buf_check_index_data(QUE_t* q, uint16_t index);
void q_copy(QUE_t* q, void* buf, uint16_t len, uint16_t index);

int empty_que(void);
int full_que(void);
void push_que(uint8 value);
int pull_que();

#ifdef DSP_DOUBLE_QUE
typedef struct tagDSPDATA
{
	uint8 q_state;
	uint8 q_data;
}DSPDATA;

typedef struct QUE2
{
	DSPDATA* buf;
	uint16_t head;
	uint16_t tail;
	uint16_t size;
} QUE2_t;

void init_q2(QUE2_t* q, void* buf, uint16_t len);
void q2_en(QUE2_t* q, uint8_t state,uint8_t data);
uint8_t q2_de(QUE2_t* q);
uint16_t q2_data_size(QUE2_t* q);
void q2_clear(QUE2_t* q);
#endif

extern int front;
extern int rear;

#endif /* SYS_QUE_H_ */
