/*
 * debug.h
 *
 *  Created on: 2018. 5. 17.
 *      Author: Digen
 */

#ifndef SYS_DEBUG_H_
#define SYS_DEBUG_H_

#define DEBUT_UART_PORT		1
//#define USB_UPDATE_PORT		0


#define MAX_USART_STR 	512

#if 1
#if DEBUT_UART_PORT == 0
#define DPRINTF(lvl, fmt, args...)		dprintf0(lvl, fmt, ##args)
#define JPRINTF(fmt, args...)		jprintf0(fmt, ##args)
#elif DEBUT_UART_PORT == 1
#define DPRINTF(lvl, fmt, args...) 	dprintf1(lvl, fmt, ##args)
#define JPRINTF(fmt, args...) 	jprintf1(fmt, ##args)
#elif DEBUT_UART_PORT == 3
#endif
#else
#define DPRINTF(lvl, fmt, args...) 	dprintf1(lvl, fmt, ##args)
#endif
#if 0
enum
{
    DBG_ALL_MSG = 0,
    DBG_MSG1,
    DBG_MSG2,
    DBG_MSG3,
    DBG_MSG4,
    DBG_MSG5,
    DBG_MSG6,
    DBG_MSG7,
    DBG_INFO,
    DBG_WARN,
    DBG_ERR,
    DBG_NO_MSG
};
#else
enum
{
	DBG_NO_MSG = 0,
    DBG_ERR,
    DBG_WARN,
    DBG_INFO,
    DBG_MSG1,
    DBG_MSG2,
    DBG_MSG3,
    DBG_MSG4,
    DBG_MSG5,
    DBG_MSG5_1,
    DBG_MSG6,
    DBG_MSG7,
    DBG_ALL_MSG 
};

#endif

extern uint8_t debugLevel;
extern uint8_t debugenable;

void dprintf0(uint8_t lvl, const char* fmt, ...);
void dprintf1(uint8_t lvl, const char* fmt, ...);
//void dprintf2(uint8_t lvl, const char* fmt, ...);

void jprintf0(const char* fmt, ...);
void jprintf1(const char* fmt, ...);
//void jprintf2(const char* fmt, ...);


#endif /* SYS_DEBUG_H_ */
