/*
 * debug.c
 *
 *  Created on: 2018. 5. 17.
 *      Author: Digen
 */

#include "includes.h"

uint8_t debugLevel = DBG_MSG5;
uint8_t debugenable = ON;

void dprintf0(uint8_t lvl, const char* fmt, ...)
{
	va_list ap;
	char buf[MAX_USART_STR];

//	if((lvl >= debugLevel)&&debugenable)
	{
    	va_start(ap, fmt);
    	vsprintf(buf, fmt, ap);
	 	uputs0(buf);
    	va_end(ap);
	}
}

void dprintf1(uint8_t lvl, const char* fmt, ...)
{
	va_list ap;
	char buf[MAX_USART_STR];

	if((lvl <= debugLevel)&&debugenable)
//	if(lvl <=debugLevel)
	{
    	va_start(ap, fmt);
    	vsprintf(buf, fmt, ap);
	 	uputs1(buf);
    	va_end(ap);
	}
}
#if 0
void dprintf2(uint8_t lvl, const char* fmt, ...)
{
	va_list ap;
	char buf[MAX_USART_STR];

//	if((lvl >= debugLevel)&&debugenable)
	{
    	va_start(ap, fmt);
    	vsprintf(buf, fmt, ap);
	 	uputs2(buf);
    	va_end(ap);
	}
}
#endif
void jprintf0(const char* fmt, ...)
{
	va_list ap;
	char buf[MAX_USART_STR];

  	va_start(ap, fmt);
  	vsprintf(buf, fmt, ap);
 	uputs0(buf);
  	va_end(ap);
}

void jprintf1(const char* fmt, ...)
{
	va_list ap;
	char buf[MAX_USART_STR];

	if(debugenable)
	{
	  	va_start(ap, fmt);
	  	vsprintf(buf, fmt, ap);
	 	uputs1(buf);
	  	va_end(ap);
	}
}
#if 0
void jprintf2(const char* fmt, ...)
{
	va_list ap;
	char buf[MAX_USART_STR];

  	va_start(ap, fmt);
  	vsprintf(buf, fmt, ap);
 	uputs2(buf);
  	va_end(ap);
}
#endif
