
/*
 * nvic_init.c
 *
 *  Created on: 2018. 5. 21.
 *      Author: Digen
 */

#include "includes.h"
static void init_IRQ(uint8_t num);
static void init_IRQ_Prority(uint8_t num, uint8_t prority);

void nvic_init_fnc(void)
{
	INT_SYS_EnableIRQ(PORTA_IRQn);
	INT_SYS_EnableIRQ(PORTC_IRQn);
	//INT_SYS_EnableIRQ(PORTD_IRQn);
	INT_SYS_SetPriority( PORTA_IRQn, 2 );
	INT_SYS_SetPriority( PORTC_IRQn, 2 );
	//INT_SYS_SetPriority( PORTD_IRQn, 2 );
}

static void init_IRQ(uint8_t num)
{
	S32_NVIC->ICPR[num/32] = 1 << (num % 32);  /*  clr any pending IRQ*/
  	S32_NVIC->ISER[num/32] = 1 << (num % 32);  /* enable IRQ */
}

static void init_IRQ_Prority(uint8_t num, uint8_t prority)
{
	S32_NVIC->IP[num] = prority;             /* IRQ48-LPIT0 ch0: priority 10 of 0-15*/
}


//PIT init
void init_LPIT0(void) 
{
	PCC->PCCn[PCC_LPIT_INDEX] = PCC_PCCn_PCS(6);    //6	SPLL..
	// 1�̸�, SOSCDIV2_CLK			// 2�̸�, SIRCDIV2_CLK
	// 3�̸�, FIRCDIV2_CLK			// 6�̸�, SPLLDIV2_CLK
	PCC->PCCn[PCC_LPIT_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable clk to LPIT0 regs */
																								
	LPIT0->MCR = 0x00000001;    	/* DBG_EN-0: Timer chans stop in Debug mode */
	                            						/* DOZE_EN=0: Timer chans are stopped in DOZE mode */
	                            						/* SW_RST=0: SW reset does not reset timer chans, regs */
	                         							/* M_CEN=1: enable module clk (allows writing other LPIT0 regs)*/
	LPIT0->MIER = 0x00000001;   /* TIE0=1: Timer Interrupt Enabled fot Chan 0 */

	LPIT0->TMR[0].TVAL = 160000;   /* Chan 0 Timeout period: 160M clocks */
	LPIT0->TMR[0].TCTRL = 0x00000001; /* T_EN=1: Timer channel is enabled */
	                            /* CHAIN=0: channel chaining is disabled */
	                            /* MODE=0: 32 periodic counter mode */
	                            /* TSOT=0: Timer decrements immediately based on restart */
	                            /* TSOI=0: Timer does not stop after timeout */
	                            /* TROT=0 Timer will not reload on trigger */
	                            /* TRG_SRC=0: External trigger soruce */
	                            /* TRG_SEL=0: Timer chan 0 trigger source is selected*/
	INT_SYS_EnableIRQ(LPIT0_Ch0_IRQn);
//	INT_SYS_SetPriority( LPIT0_Ch0_IRQn, 0 );													
}

void deinit_LPIT0(void) 
{
//	LPIT0->MIER = 0x00000000;   /* TIE0=1: Timer Interrupt Enabled fot Chan 0 */
//	LPIT0->MCR = 0x00000000;    	/* DBG_EN-0: Timer chans stop in Debug mode */	
	INT_SYS_DisableIRQ(LPIT0_Ch0_IRQn);
}