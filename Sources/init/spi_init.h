/*
 * spi_init.h
 *
 *  Created on: 2018. 7. 23.
 *      Author: Digen
 */

#ifndef INIT_SPI_INIT_H_
#define INIT_SPI_INIT_H_

//#define SW_SPI

void spi0_init(void);
void spi1_init(void);
void spi0_deinit(void);
void spi1_deinit(void);


#ifdef SW_SPI
void SPI_WriteByte(volatile unsigned char Dat);
unsigned char SPI_ReadByte(void);

// Send port data to tlc6c5816
int SPI_Write(unsigned char* srcptr, int length);

// receive port data to tlc6c5816
void SPI_Read(unsigned char* srcptr, int length); 

void SPI_SEL_LOW(void);
void SPI_SEL_HIGH(void);
void SPI_MOSI_HIGH(void);
void SPI_MOSI_LOW(void);
void SPI_Init(void);
#endif

#endif /* INIT_SPI_INIT_H_ */
