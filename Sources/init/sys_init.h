/*
 * sys_init.h
 *
 *  Created on: 2018. 5. 17.
 *      Author: Digen
 */

#ifndef INIT_SYS_INIT_H_
#define INIT_SYS_INIT_H_

void sys_init_fnc (void);
void sys_deinit_fnc (void);


#endif /* INIT_SYS_INIT_H_ */
