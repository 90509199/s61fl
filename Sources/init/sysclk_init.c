/*
 * sysclk_init.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */
#include "includes.h"

static void mode_entry_init_fnc(void);
static void sysclk_init_fnc(void);

static void init_SOSC(void); //8MHz
static void init_SPLL(void); //SPLL 160Mhz
static void init_SIRC(void); //8MHz

void sysclk_module_init_fnc(void)
{
	sysclk_init_fnc();
        /* System Clock Initialization Function */
}

static void sysclk_init_fnc(void)
{
//	init_SOSC(); 
//	init_SIRC();
//	init_SPLL(); 
	Set_RUNmode(80); 
}

/*******************************************************************************
Function Name : init_SOSC
Notes         : SOSCDIV2 divide by 1 (8MHz)
                SOSCDIV1 divide by 1 (8MHz)
 *******************************************************************************/
static void init_SOSC(void)
{
	#if 1
	  SCG->SOSCDIV=0x00000101;  /* SOSCDIV1 & SOSCDIV2 =1: divide by 1 */
	  SCG->SOSCCFG=0x00000024;  /* Range=2: Medium freq (SOSC betw 1MHz-8MHz)*/
	                            /* HGO=0:   Config xtal osc for low power */
	                            /* EREFS=1: Input is external XTAL */
	  while(SCG->SOSCCSR & SCG_SOSCCSR_LK_MASK); /* Ensure SOSCCSR unlocked */
	  SCG->SOSCCSR=0x00000001;  /* LK=0:          SOSCCSR can be written */
	                            /* SOSCCMRE=0:    OSC CLK monitor IRQ if enabled */
	                            /* SOSCCM=0:      OSC CLK monitor disabled */
	                            /* SOSCERCLKEN=0: Sys OSC 3V ERCLK output clk disabled */
	                            /* SOSCLPEN=0:    Sys OSC disabled in VLP modes */
	                            /* SOSCSTEN=0:    Sys OSC disabled in Stop modes */
	                            /* SOSCEN=1:      Enable oscillator */
	  while(!(SCG->SOSCCSR & SCG_SOSCCSR_SOSCVLD_MASK)); /* Wait for sys OSC clk valid */
	#else
	SCG->SOSCDIV=SCG_SOSCDIV_SOSCDIV1(1)
		|SCG_SOSCDIV_SOSCDIV2(1);							  /* SOSCDIV1 & SOSCDIV2 =1: divide by 1 */
	SCG->SOSCCFG= SCG_SOSCCFG_RANGE(2)			/* Range=2: Medium freq (SOSC betw 1MHz-8MHz)*/
		|SCG_SOSCCFG_EREFS(1);  									/* EREFS=1: Input is external XTAL */
                            
	while(SCG->SOSCCSR & SCG_SOSCCSR_LK_MASK); /* Ensure SOSCCSR unlocked */
	//under line 
	SCG->SOSCCSR= SCG_SOSCCSR_SOSCEN(1);  /* LK=0:          SOSCCSR can be written */
                            /* SOSCCMRE=0:    OSC CLK monitor IRQ if enabled */
                            /* SOSCCM=0:      OSC CLK monitor disabled */
                            /* SOSCERCLKEN=0: Sys OSC 3V ERCLK output clk disabled */
                            /* SOSCLPEN=0:    Sys OSC disabled in VLP modes */
                            /* SOSCSTEN=0:    Sys OSC disabled in Stop modes */
                            /* SOSCEN=1:      Enable oscillator */
	while(!(SCG->SOSCCSR & SCG_SOSCCSR_SOSCVLD_MASK)); /* Wait for sys OSC clk valid */
	#endif
}

/*******************************************************************************
Function Name : init_SPLL
Notes         : VCO_CLK is (320MHz)
                SPLL_CLK is (160MHz)
 *******************************************************************************/
static void init_SPLL(void)
{
#if 1
	//160Mhz
  while(SCG->SPLLCSR & SCG_SPLLCSR_LK_MASK); /* Ensure SPLLCSR unlocked */
  SCG->SPLLCSR = SCG_SPLLCSR_SPLLEN(0);  	/* SPLLEN=0: SPLL is disabled (default) */
  SCG->SPLLDIV = SCG_SPLLDIV_SPLLDIV2(3)|SCG_SPLLDIV_SPLLDIV1(2);  /* SPLLDIV1 divide by 2; SPLLDIV2 divide by 4 */
  SCG->SPLLCFG = SCG_SPLLCFG_PREDIV(0)|SCG_SPLLCFG_MULT(24);	
//  SCG->SPLLCFG = 0x00180000;  /* PREDIV=0: Divide SOSC_CLK by 0+1=1 */
                              /* MULT=24:  Multiply sys pll by 16+24=40 */
                              /* SPLL_CLK = (8MHz / 1) * (40 / 2) = 160 MHz */
  /*The SPLL_CLK = (VCO_CLK)/2, The VCO_CLK = SPLL_SOURCE/(PREDIV + 1) X (MULT + 16)*/

  while(SCG->SPLLCSR & SCG_SPLLCSR_LK_MASK); /* Ensure SPLLCSR unlocked */
  SCG->SPLLCSR = SCG_SPLLCSR_SPLLEN(1); /* LK=0:        SPLLCSR can be written */
                             /* SPLLCMRE=0:  SPLL CLK monitor IRQ if enabled */
                             /* SPLLCM=0:    SPLL CLK monitor disabled */
                             /* SPLLSTEN=0:  SPLL disabled in Stop modes */
                             /* SPLLEN=1:    Enable SPLL */
  while(!(SCG->SPLLCSR & SCG_SPLLCSR_SPLLVLD_MASK)); /* Wait for SPLL valid */
#else
//48Mhz
  while(SCG->SPLLCSR & SCG_SPLLCSR_LK_MASK); /* Ensure SPLLCSR unlocked */
  SCG->SPLLCSR = SCG_SPLLCSR_SPLLEN(0);  	/* SPLLEN=0: SPLL is disabled (default) */
  SCG->SPLLDIV = SCG_SPLLDIV_SPLLDIV2(3)
		|SCG_SPLLDIV_SPLLDIV1(2);  		/* SPLLDIV1 divide by 2; SPLLDIV2 divide by 4 */
	
  SCG->SPLLCFG = SCG_SPLLCFG_PREDIV(1)
		|SCG_SPLLCFG_MULT(8); /* PREDIV=0: Divide SOSC_CLK by 1+1=2 */
                              /* MULT=8:  Multiply sys pll by 16+8=24 */
                              /* SPLL_CLK = (8MHz / 2) * (24 / 2) = 48 MHz */
  /*The SPLL_CLK = (VCO_CLK)/2, The VCO_CLK = SPLL_SOURCE/(PREDIV + 1) X (MULT + 16)*/

  while(SCG->SPLLCSR & SCG_SPLLCSR_LK_MASK); /* Ensure SPLLCSR unlocked */
  SCG->SPLLCSR = SCG_SPLLCSR_SPLLEN(1); /* LK=0:        SPLLCSR can be written */
                             /* SPLLCMRE=0:  SPLL CLK monitor IRQ if enabled */
                             /* SPLLCM=0:    SPLL CLK monitor disabled */
                             /* SPLLSTEN=0:  SPLL disabled in Stop modes */
                             /* SPLLEN=1:    Enable SPLL */
  while(!(SCG->SPLLCSR & SCG_SPLLCSR_SPLLVLD_MASK)); /* Wait for SPLL valid */
#endif
}

/*******************************************************************************
Function Name : init_SIRC is 8Mhz
Notes         : SIRCDIV2 divide by 1 (8MHz)
                SIRCDIV1 divide by 1 (8MHz)
 *******************************************************************************/
void init_SIRC(void)
{
    // Slow IRC Control Status Register
    SCG->SIRCCSR &= ~ (1 << 24);
    // [24] LK = 0 Control Status Register can be written.

    SCG->SIRCCSR &= ~ (1 << 0);
    // [0] SIRCEN = 0 Disable Slow IRC

    // Slow IRC Divide Register
    SCG->SIRCDIV |= 0x0101;
    // [10-8] SIRCDIV2 0b001 Divide by 1 (8MHz)
    // [2-0]  SIRCDIV1 0b001 Divide by 1 (8MHz)

    SCG->SIRCCSR |= (1 << 0);
    // [0] SIRCEN = 1 Enable Slow IRC

//<<
	SCG->SIRCCSR |= (1 << 1);
    // [1] SIRCSTEN = 1 Slow IRC is enabled in supported Stop modes

	SCG->SIRCCSR |= (1 << 2);
    // [2] SIRCLPEN = 1 Slow IRC is enabled in VLP modes
//>>
    while((SCG->SIRCCSR & (1 << 24)) == 0); // wait until clock is valid
    // [24] SIRCVLD = 1 Slow IRC is enabled and output clock is valid
}


