/*
 * nvic_init.h
 *
 *  Created on: 2018. 5. 21.
 *      Author: Digen
 */

#ifndef INIT_NVIC_INIT_H_
#define INIT_NVIC_INIT_H_


#define NVIC_ID_DMA_CH0							0
#define NVIC_ID_DMA_CH1							1
#define NVIC_ID_DMA_CH2							2
#define NVIC_ID_DMA_CH3							3
#define NVIC_ID_DMA_CH4							4
#define NVIC_ID_DMA_CH5							5
#define NVIC_ID_DMA_CH6							6
#define NVIC_ID_DMA_CH7							7
#define NVIC_ID_DMA_CH8							8
#define NVIC_ID_DMA_CH9							9
#define NVIC_ID_DMA_CH10							10
#define NVIC_ID_DMA_CH11							11
#define NVIC_ID_DMA_CH12							12
#define NVIC_ID_DMA_CH13							13
#define NVIC_ID_DMA_CH14							14
#define NVIC_ID_DMA_CH15							15
#define NVIC_ID_DMA_ERROR						16
#define NVIC_ID_MCM										17
#define NVIC_ID_WDOG									22
#define NVIC_ID_RCM										23
#define NVIC_ID_LPI2C0_MA						24
#define NVIC_ID_LPI2C0_SL							25
#define NVIC_ID_LPSPI0								26
#define NVIC_ID_LPSPI1								27
#define NVIC_ID_LPSPI2								28
#define NVIC_ID_LPI2C1_MA						29
#define NVIC_ID_LPI2C1_SL							30
#define NVIC_ID_LPUART0								31
#define NVIC_ID_LPUART1								33
#define NVIC_ID_LPUART2								35
#define NVIC_ID_ADC0									39
#define NVIC_ID_ADC1									40
#define NVIC_ID_ANLCMP								41
#define NVIC_ID_RTC0									46
#define NVIC_ID_RTC1									47
#define NVIC_ID_LPIT0_Ch0							48
#define NVIC_ID_LPIT0_Ch1							49
#define NVIC_ID_LPIT0_Ch2							50
#define NVIC_ID_LPIT0_Ch3							51
#define NVIC_ID_LPTIMER								58
#define NVIC_ID_PORTA									59
#define NVIC_ID_PORTB									60
#define NVIC_ID_PORTC									61
#define NVIC_ID_PORTD									62
#define NVIC_ID_PORTE									63
#define NVIC_ID_QUADSPI							65
#define NVIC_ID_FLEXIO								69
#define NVIC_ID_CAN0_ORED						78
#define NVIC_ID_CAN0_ERROR						79
#define NVIC_ID_CAN0_WAKEUP					80
#define NVIC_ID_CAN0_ORED_0_15			81
#define NVIC_ID_CAN0_ORED_16_31			82
#define NVIC_ID_CAN1_ORED						85
#define NVIC_ID_CAN1_ERROR						86
#define NVIC_ID_CAN1_WAKEUP					87
#define NVIC_ID_CAN1_ORED_0_15			88
#define NVIC_ID_CAN1_ORED_16_31			89
#define NVIC_ID_CAN2_ORED						92
#define NVIC_ID_CAN2_ERROR						93
#define NVIC_ID_CAN2_WAKEUP					94
#define NVIC_ID_CAN2_ORED_0_15			95
#define NVIC_ID_CAN2_ORED_16_31			96


void nvic_init_fnc(void);

//init pit
void init_LPIT0(void);
void deinit_LPIT0(void) ;
#endif /* INIT_NVIC_INIT_H_ */
