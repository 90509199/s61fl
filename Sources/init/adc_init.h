/*
 * adc_init.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef INIT_ADC_INIT_H_
#define INIT_ADC_INIT_H_

void ADC_Init(void);
void Init_ADC0(void);
void Init_ADC1(void);

void Enable_ADC0_Ch(uint8_t ch);
void Disable_ADC0_Ch(void);
uint8_t Wait_Check_ADC0(void);
uint32_t Read_ADC0_CH(void);

void Enable_ADC1_Ch(uint16_t ch);
void Disable_ADC1_Ch(void);
uint8_t Wait_Check_ADC1(void); 
uint32_t Read_ADC1_CH(void); 

#endif /* INIT_ADC_INIT_H_ */
