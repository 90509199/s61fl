/*
 * flexio_i2c_init.c
 *
 *  Created on: 2018. 7. 23.
 *      Author: Digen
 */

#include "includes.h"
#include "Cpu.h"
#include "flexio.h"
#ifdef FLEXIO_I2C
 flexio_device_state_t flexIODeviceState;
 flexio_i2c_master_state_t i2cMasterState;
#endif

#if 1
static volatile uint32_t flexio_i2c_tick = 0;

uint8 flexio_i2c_result;


#define EVB

#ifdef EVB
    #define LED_R       15      /* RGB RED LED - pin PTD[15] */
    #define LED_G       16      /* RGB GREEN LED - pin PTD[15] */
    #define GPIO_LED    PTD     /* RGB LED base pointer */
#endif

/* Declare transfer size */
#define TRANSFER_SIZE 6U

#define ADDRESS_SLAVE 0x11

#define ADDRESS_SEC 0x00
#define ADDRESS_MIN 0x01
#define ADDRESS_HOUR 0x02
#define ADDRESS_WEEK 0x03
#define ADDRESS_DAY 0x04
#define ADDRESS_MONTH 0x05
#define ADDRESS_YEAR 0x06
#define ADDRESS_ALARM_MIN 0x07

#define ADDRESS_WRITE 1
#define ADDRESS_READ 0
/* Define master and slave buffers */
uint8_t slaveBuffer[TRANSFER_SIZE];
uint8_t masterBuffer[TRANSFER_SIZE];

volatile uint32_t tick =0;


void flexio_i2c_init(void)
{
	  /* Allocate memory for LPI2C driver internal structure */
    lpi2c_slave_state_t lpi2cSlaveState;
    /* Allocate the memory necessary for the FlexIO state structures */
//    flexio_i2c_master_state_t i2cMasterState;
 // flexio_device_state_t flexIODeviceState;
    /* Variable that is used to initialize the buffers */
    uint8_t cnt;
    volatile bool isTransferOk[2] = {true, true};
		
	 /* Use as callback parameter the LPI2C instance number */
 //   lpi2c1_SlaveConfig0.callbackParam = (void *)INST_LPI2C1;
    /* Initialize LPI2C instance as bus slave */
 //  LPI2C_DRV_SlaveInit(INST_LPI2C1, &lpi2c1_SlaveConfig0, &lpi2cSlaveState);
		
	/* Init FlexIO device */
    FLEXIO_DRV_InitDevice(INST_FLEXIO_I2C1, &flexIODeviceState);
    /* Initialize FlexIO I2C driver as bus master */
    FLEXIO_I2C_DRV_MasterInit(INST_FLEXIO_I2C1, &flexio_i2c1_MasterConfig0, &i2cMasterState);

		
		/* Initialize master and slave buffers for master send sequence */
		for(cnt = 0; cnt < sizeof(masterBuffer); cnt++)
		{
			masterBuffer[cnt] = cnt;
			slaveBuffer[cnt]	= 0U;
		}

}

void flexio_i2c_deinit(void)
{
	FLEXIO_I2C_DRV_MasterDeinit(&i2cMasterState);
}



/*!
 *  @brief LPI2C Slave Callback
 *
 *  @param [in] instance   LPI2C instance number
 *  @param [in] slaveEvent Event received on the I2C bus
 *  @param [in] userData   User defined data that is passed to the callback
 *  @return None
 *
 *  @details This function will be called by LPI2C interrupt handler and it
 *  will assign the buffer for TX or RX events.
 *  If an error event occurs, it will abort the current transfer.
 */
void lpi2c1_SlaveCallback0(i2c_slave_event_t slaveEvent,void *userData)
{
    /* Casting userData to void in order to avoid warning as the parameter is not used */
    uint32_t instance;
    instance = (uint32_t)userData;

    /* Depending on the event received, set the buffers or abort the transfer */
    switch(slaveEvent)
    {
        case I2C_SLAVE_EVENT_RX_REQ:
            /*
             * If the bus master requests data, then set the destination RX buffer
             * and accepted transfer size
             */
            LPI2C_DRV_SlaveSetRxBuffer(instance, slaveBuffer, TRANSFER_SIZE);
            break;
        case I2C_SLAVE_EVENT_TX_REQ:
            /*
             * If the bus master sends data, then set the source TX buffer
             * and accepted transfer size
             */
            LPI2C_DRV_SlaveSetTxBuffer(instance, slaveBuffer, TRANSFER_SIZE);
            break;
        case I2C_SLAVE_EVENT_TX_EMPTY:
        case I2C_SLAVE_EVENT_RX_FULL:
            /*
             * If the TX buffer is empty or the RX buffer is full, abort the current
             * transfer.
             */
            LPI2C_DRV_SlaveAbortTransferData(instance);
            break;
        case I2C_SLAVE_EVENT_STOP:
            /*
             * This case is used when a stop condition is on the bus. Because
             * the example does not handle this case there is no action taken.
             */
            break;
    }
}

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - __start (startup asm routine)
 * - __init_hardware()
 * - main()
 *   - PE_low_level_init()
 *     - Common_Init()
 *     - Peripherals_Init()
*/

#if 0
uint8 flexio_i2c_check_timeout(uint8 limit)
{
	if(GetElapsedTime(flexio_i2c_tick)<limit)
	{
		return 0;// smaller than limit
	}
	else
	{
		//i2c timeout	//1ms 
		flexio_i2c_deinit();	// larger than limit
		flexio_i2c_init();	// larger than limit
		flexio_i2c_tick = GetTickCount();
		
		return 1;
	}
}

uint8 flexio_i2c_check_busfree(void)
{
	uint8 result,status;
	result=0;
	status=LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL);

	//if ((status==STATUS_SUCCESS) && (r_I2cMaster.i2cIdle == true))
	if(status==STATUS_SUCCESS)
	{
		result=1;
	}
	return result;
}
#endif

//uint8 flexio_i2c_write(uint32 cmd, uint8 *tx_buf, uint8 cnt)
//{
//	uint8 len=0;
//	uint8 result=I2C_BUSY;
//	static uint8 data_buf[256] ={0,};
//
//	if((cmd&0x00FF0000)!=0x00)
//	{
//		data_buf[0]=(uint8)(cmd>>16);
//		data_buf[1]=(uint8)(cmd>>8);
//		data_buf[2]=(uint8)cmd;
//		memcpy(&data_buf[3],tx_buf,cnt);
//		len=cnt+3;
//	}
//	else
//	{
//		data_buf[0]=(uint8)(cmd);
//		memcpy(&data_buf[1],tx_buf,cnt);
//		len=cnt+1;
//	}
//
//	flexio_i2c_tick = GetTickCount();
//
//	FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, (ADDRESS_SLAVE | ADDRESS_READ));
//	FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, masterBuffer, sizeof(masterBuffer), true, 3UL);
//
//	if(result != STATUS_SUCCESS)
//	{
//		DPRINTF(DBG_MSG1," %s error %d\n\r", __FUNCTION__,result);
//	}
//
//
//	return result;
//}
//
//
//uint8 flexio_i2c_read(uint32 cmd, uint8 *des, uint8 cnt)
//{
//	uint8 data_buf[3]={0,};
//
//	flexio_i2c_result=I2C_BUSY;
//
//	if((cmd&0x00FF0000)!=0x00)
//	{
//		data_buf[0]=(uint8)(cmd>>16);
//		data_buf[1]=(uint8)(cmd>>8);
//		data_buf[2]=(uint8)cmd;
//	}
//	else
//	{
//		data_buf[0]=(uint8)(cmd);
//	}
//
//	flexio_i2c_tick = GetTickCount();
//
//
//	// Data READ
//		/* Send data to the slave  Address + Command Address*/
//        FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, (ADDRESS_SLAVE | ADDRESS_READ));
//        masterBuffer[0] = ADDRESS_MIN;
//
//		FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, masterBuffer, sizeof(masterBuffer), false, 1UL);
//
//		/* Send data to the slave  Address + Command Address + Write*/
//		/* 4byte Read to Stop bit Send */
//		FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, (ADDRESS_SLAVE | ADDRESS_WRITE));
//		FLEXIO_I2C_DRV_MasterReceiveDataBlocking(&i2cMasterState, masterBuffer, sizeof(masterBuffer), true, 4UL);
//
//}

#endif

uint8 TB2952AHQ_Write(uint8 cmd, uint8 *des, uint8 cnt)
{
	uint8 write_buf[256]={0,};
	uint8 write_cnt;
	uint8 result  =I2C_BUSY;

	write_buf[0]=(uint8)cmd;
	memcpy(&write_buf[1],des,cnt);
	write_cnt=cnt+1;

	#ifdef FLEXIO_I2C
//	result = FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, &write_buf[0], write_cnt, true, 1000UL);
	result = FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, write_buf, write_cnt, true, 1000UL);
	#endif
return result;
}



