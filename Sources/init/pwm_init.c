/*
 * pwm_init.c
 *
 *  Created on: 2018. 8. 14.
 *      Author: Digen
 */
#include "includes.h"

//FTM3_CH5				//PTD3		MCU_LCD1_PWM_3P3
//FTM3_CH	6				//PTE2
//FTM3_CH2 				//PTB10		MCU_LCD2_PWM_3P3
//FTM0_CH4       //PTB4

void PWM_init(void)
{
	#ifdef CHANG_BL_TIMING
	PORTD->PCR[2] =  PORT_PCR_MUX(2);//MUX_SEL					//PTD2			PWM O				//LCD2 PWM
	PORTD->PCR[3] =  PORT_PCR_MUX(2);//MUX_SEL					//PTD3			PWM O				//LCD1 PWM
	#endif
	FTM3_init();
	#if 0
	FTM3_PWM_init(MCU_LCD2_PWM_3P3);
	FTM3_PWM_init(MCU_LCD1_PWM_3P3);
	#ifdef B1_MOTHER_BOARD_SAMPLE
	FTM3_PWM_init(MCU_HU_LCD_PWM_3P3);
	FTM3_PWM_init(MCU_CAM_PWM_3P3);
	#else
	FTM3_PWM_init(MCU_CLUSTERLCD_PWM_3P3);
	#endif
	FTM3_start_counter();			//전압테스트 인해 필요함..
	#endif
}

//FTM2_CH0         //PTC5

void FTM0_init(void)
{
	PCC->PCCn[PCC_FTM0_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
	PCC->PCCn[PCC_FTM0_INDEX] |= PCC_PCCn_PCS(1)/* Clock Src=1, 8 MHz SOSCDIV1_CLK */
	| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */

	FTM0->MODE |= FTM_MODE_WPDIS_MASK; /* Write protect to registers disabled (default) */
	FTM0->COMBINE = 0x00000000;/* FTM mode settings used: DECAPENx, MCOMBINEx, COMBINEx=0 */
	FTM0->POL = 0x00000000; /* Polarity for all channels is active high (default) */

	//1khz
	FTM0->SC = 0x00100007; 	/* Enable PWM channel 4 output*/
													/*Divide by 128*/
	FTM0->MOD = 62 -1; /* FTM1 counter final value (used for PWM mode) */						//
	//FTM0->MOD = 31 -1 ; /* FTM1 counter final value (used for PWM mode) */			// 2KH
	//	FTM0->MOD = 62 -1 ; /* FTM1 counter final value (used for PWM mode) */			// 1kh
}

void FTM0_PWM_init(uint8_t ch)
{
	  FTM0->CONTROLS[4].CnSC = 0x00000028;  /* FTM0 ch5: edge-aligned PWM, low true pulses */
                                        /* CHIE (Chan Interrupt Ena) = 0 (default) */
                                        /* MSB:MSA (chan Mode Select)=0b10, Edge Align PWM*/
                                        /* ELSB:ELSA (chan Edge/Level Select)=0b10, low true */
//	FTM0->CONTROLS[4].CnV =  5;       /* FTM0 ch1 compare value (~75% duty cycle) */
}

void FTM0_start_counter(void)
{
	// External clock->system clock
	FTM0->SC |= FTM_SC_CLKS(1);	//FTM_CLK  0x01: system clock, 0x02: RTC 0x03: External clock
}

void FTM0_stop_counter(void)
{
	// External clock->system clock
	FTM0->SC &= ~(FTM_SC_CLKS(1));//FTM_CLK  0x01: system clock, 0x02: RTC 0x03: External clock	
}

void FTM0_CH4_PWM_Duty(uint8_t value) {
	uint16_t temp=0;

	temp = (62*value)/100;
	FTM0->CONTROLS[4].CnV =  temp;       /* FTM0 ch1 compare value (~75% duty cycle) */
}

void FTM1_init(void)
{
	PCC->PCCn[PCC_FTM1_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
	PCC->PCCn[PCC_FTM1_INDEX] |= PCC_PCCn_PCS(1)/* Clock Src=1, 8 MHz SOSCDIV1_CLK */
	| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */

	FTM1->MODE |= FTM_MODE_WPDIS_MASK; /* Write protect to registers disabled (default) */
	FTM1->COMBINE = 0x00000000;/* FTM mode settings used: DECAPENx, MCOMBINEx, COMBINEx=0 */
	FTM1->POL = 0x00000000; /* Polarity for all channels is active high (default) */
	#if 1
	//200hz
	FTM1->SC = 0x00040007; /* Enable PWM channel 2 output*/
	FTM1->MOD = 312 -1 ; /* FTM1 counter final value (used for PWM mode) */			//200hz
	//	FTM1->MOD = 62500 -1 ; /* FTM1 counter final value (used for PWM mode) */  //max value is 65535
	#else
	//500khz
	FTM1->SC = 0x00080002; /* Enable PWM channel 2 output*/		//PWM prescale calcurate
	FTM1->MOD = 4 -1 ; /* FTM1 counter final value (used for PWM mode) */			//500khz
	#endif

	/* FTM1 Period = MOD-CNTIN+0x0001 ~= 62500 ctr clks */
	/* 8MHz /128 = 62.5kHz -> ticks -> 1Hz */
}

void FTM1_PWM_init(uint8_t value){
  FTM1->CONTROLS[2].CnSC = 0x00000028;  /* FTM0 ch1: edge-aligned PWM, low true pulses */
                                        /* CHIE (Chan Interrupt Ena) = 0 (default) */
                                        /* MSB:MSA (chan Mode Select)=0b10, Edge Align PWM*/
                                        /* ELSB:ELSA (chan Edge/Level Select)=0b10, low true */
//  FTM1->CONTROLS[3].CnV =  200;       /* FTM0 ch1 compare value (~75% duty cycle) */
}

void FTM1_start_counter (void) {
	FTM1->SC |= FTM_SC_CLKS(3);
	/* Start FTM0 counter with clk source = external clock (SOSCDIV1_CLK)*/
}


void FTM1_CH3_PWM_Duty(uint8_t value) {
	uint16_t temp=0;

	temp = (312*value)/100;
	FTM1->CONTROLS[2].CnV =  temp;       /* FTM0 ch1 compare value (~75% duty cycle) */
}
   
void FTM3_init(void)
{
	PCC->PCCn[PCC_FTM3_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
	PCC->PCCn[PCC_FTM3_INDEX] |= PCC_PCCn_PCS(1)/* Clock Src=1, 8 MHz SOSCDIV1_CLK */
	| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */

	FTM3->MODE |= FTM_MODE_WPDIS_MASK; /* Write protect to registers disabled (default) */
	FTM3->COMBINE = 0x00000000;/* FTM mode settings used: DECAPENx, MCOMBINEx, COMBINEx=0 */
	FTM3->POL = 0x00000000; /* Polarity for all channels is active high (default) */

	//1khz
	#ifdef B1_MOTHER_BOARD_SAMPLE
	FTM3->SC = 0x00B40007; /* Enable PWM channel 2,5,4,7 output*/
	#else
	FTM3->SC = 0x00640007; /* Enable PWM channel 2,5,6 output*/
	#endif
//	FTM3->SC = 0x00240007; /* Enable PWM channel 2,5 output*/

#ifdef PWM_CHANGE_FREQ
//	FTM3->MOD = 6000 ; /* FTM1 counter final value (used for PWM mode) */			// 2KH
//	FTM3->MOD = 312 -1 ; /* FTM1 counter final value (used for PWM mode) */			//200hz
//	FTM3->MOD = 124 -1; 		//500hz
//	FTM3->MOD = 190 -1;			//328hz
//		FTM3->MOD = 200 -1;			//312hz
		FTM3->MOD = 209 -1;					//299hz	
//		FTM3->MOD = 210 -1;			//297hz
//	FTM3->MOD = 256 -1;			//250hz
#else
	FTM3->MOD = 62 -1;			//1khz
#endif

}

void FTM3_PWM_init(uint8_t ch)
{
	FTM3->CONTROLS[ch].CnSC = 0x00000028;  /* FTM0 : edge-aligned PWM, low true pulses */
                                        /* CHIE (Chan Interrupt Ena) = 0 (default) */
                                        /* MSB:MSA (chan Mode Select)=0b10, Edge Align PWM*/
                                        /* ELSB:ELSA (chan Edge/Level Select)=0b10, low true */
	//  FTM0->CONTROLS[6].CnV =  3;       /* FTM0 ch1 compare value (~75% duty cycle) */
	#ifdef PWM_CHANGE_FREQ
	FTM3->CONTROLS[ch].CnV =  100;       /* FTM0 ch1 compare value (~75% duty cycle) */
	#else
	FTM3->CONTROLS[ch].CnV =  30;       /* FTM0 ch1 compare value (~75% duty cycle) */
	#endif
}

void FTM3_PWM_Duty(uint8_t value)
{
	uint16_t temp=0;

//	DPRINTF(DBG_MSG1, "%s()  %d\n", __FUNCTION__,value);

	#ifdef PWM_CHANGE_FREQ
	temp = (208*value)/100;
	#else
	temp = (62*value)/100;
	#endif
	#ifdef B1_MOTHER_BOARD_SAMPLE
	FTM3->CONTROLS[2].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	FTM3->CONTROLS[4].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	FTM3->CONTROLS[5].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	FTM3->CONTROLS[7].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	#else
	FTM3->CONTROLS[2].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	FTM3->CONTROLS[5].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	FTM3->CONTROLS[6].CnV =  temp;       /* FTM3 ch2 compare value (~75% duty cycle) */
	#endif
}

void FTM3_start_counter (void)
{
	FTM3->SC |= FTM_SC_CLKS(3);
}
void FTM3_stop_counter (void)
{
	FTM3->SC &= ~(FTM_SC_CLKS(3));
}

void FTM3_deinit(void)
{
	FTM3_stop_counter();
	FTM3->SC = 0x00000000; /* disable PWM channel 2,5 output*/
}

uint8 Set_BL_Duty_Step(uint8 data, uint8 new_data)
{
	uint8 ret=0;
	if(data<=new_data)
	{
		//data <new_data
		ret=(new_data-data);
	}
	else
	{
		ret=(data-new_data);
	}

	return ret;
}
