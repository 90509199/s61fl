/*
 * pwm_init.h
 *
 *  Created on: 2018. 8. 14.
 *      Author: Digen
 */

#ifndef INIT_PWM_INIT_H_
#define INIT_PWM_INIT_H_


void PWM_init(void);

void FTM0_init(void); 
void FTM0_PWM_init(uint8_t ch);
void FTM0_start_counter (void);
void FTM0_stop_counter (void);
void FTM0_CH4_PWM_Duty(uint8_t value);

void FTM1_init(void); 
void FTM1_PWM_init(uint8_t ch);
void FTM1_CH3_PWM_Duty(uint8_t value);
void FTM1_start_counter (void);

void FTM3_init(void); 
void FTM3_deinit(void);
void FTM3_PWM_init(uint8_t ch);
void FTM3_PWM_Duty(uint8_t value);
void FTM3_start_counter (void);
void FTM3_stop_counter (void);

uint8 Set_BL_Duty_Step(uint8 data, uint8 new_data);
#endif /* INIT_PWM_INIT_H_ */
