/*
 * sysclk_init.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef INIT_SYSCLK_INIT_H_
#define INIT_SYSCLK_INIT_H_


void sysclk_module_init_fnc(void);


#endif /* INIT_SYSCLK_INIT_H_ */
