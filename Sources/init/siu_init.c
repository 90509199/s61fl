/*
 * siu_init.c
 *
 *  Created on: 2018. 5. 17.
 *      Author: Digen
 */


#include "includes.h"

static void siu_portA_init_fnc(void);
static void siu_portB_init_fnc(void);
static void siu_portC_init_fnc(void);
static void siu_portD_init_fnc(void);
static void siu_portE_init_fnc(void);
static void siu_portA_deinit_fnc(void);
static void siu_portB_deinit_fnc(void);
static void siu_portC_deinit_fnc(void);
static void siu_portD_deinit_fnc(void);
static void siu_portE_deinit_fnc(void);
static void peri_clock_deinit_fnc(void);

void siu_init_fnc(void)
{
	siu_portA_init_fnc();
	siu_portB_init_fnc();
	siu_portC_init_fnc();
	siu_portD_init_fnc();
	siu_portE_init_fnc();
	siu_general_init_fnc();			//default port
}

void siu_deinit_fnc(void)
{
	siu_portA_deinit_fnc();
	siu_portB_deinit_fnc();
	siu_portC_deinit_fnc();
	siu_portD_deinit_fnc();
	siu_portE_deinit_fnc();
	peri_clock_deinit_fnc();
}

static void siu_portA_init_fnc(void)
{	
	PCC->PCCn[PCC_PORTA_INDEX ] |= PCC_PCCn_CGC_MASK; /* Enable clock for PORTA */				

	#if 1
	PORTA->PCR[0] |=PORT_PCR_MUX(4);    		  				//flex io i2c
	PORTA->PCR[1] |=PORT_PCR_MUX(4);							//flex io i2c
	PORTA->PCR[2] |=PORT_PCR_MUX(5);							//flex io i2c
	PORTA->PCR[3] |=PORT_PCR_MUX(5);							//flex io i2c
	#endif
	
	PORTA->PCR[4] |=PORT_PCR_MUX(7);							//DEBUG_MCK		//DEBUG_MCK
	PORTA->PCR[5] |=PORT_PCR_MUX(7);	          				//RESET  		
//	PORTA->PCR[6] |=PORT_PCR_MUX(1);     					//GPIO O					//ATB_3V3_EN
//PTA7					// SW_AD1+
	#if 0 //def SI_LABS_TUNER           													/* PortA7:  Data Direction= output 		*/
	PORTA->PCR[8]	|=PORT_PCR_MUX(3);  //SPI2 				//LPSPI2_SOUT
	PORTA->PCR[9]	|=PORT_PCR_MUX(1);  //SPI2 				//LPSPI2_PCS0
	PTA->PDDR |= 1<<9;
	#endif
	PORTA->PCR[10] |=PORT_PCR_MUX(7);	 					//JTEG_TDO
	PORTA->PCR[11] |=PORT_PCR_MUX(1);           			//GPIO O				//GPS_MD_PW_EN,...TUNER_PW_EN
	PORTA->PCR[12] |=PORT_PCR_MUX(1);						//GPIO O				//MCU_AMP_ST_BY
	PORTA->PCR[13] |=PORT_PCR_MUX(1);           			//GPIO O				//MCU_AMP_MUTE	
	PORTA->PCR[14] |= PORT_PCR_MUX(1)|PORT_PCR_IRQC(10); //falling edge  					//GPIO I				//MCU_ACC_DET
	PORTA->PCR[15] |=PORT_PCR_MUX(1); 						//GPIO O				//M_5V_FRQ_SEL
	PORTA->PCR[16] |=PORT_PCR_MUX(1)|PORT_PCR_IRQC(9);	//rising edge 					//GPIO I				//MCU_LOW_BAT		//need to check it.
	PORTA->PCR[17] |=PORT_PCR_MUX(1); 						//GPIO O				//BAT_ADC_ON
	
	PTA->PDDR |= 1<<6;            					/* PortA6:  Data Direction= output 		*/
	PTA->PDDR |= 1<<11;            				/* PortA11:  Data Direction= output */
	PTA->PDDR |= 1<<12;            				/* PortA12:  Data Direction= output 		*/
	PTA->PDDR |= 1<<13;            				/* PortA13:  Data Direction= output 		*/
	PTA->PDDR |= 1<<15;            				/* PortA15:  Data Direction= output */
	PTA->PDDR |= 1<<17;            				/* PortA17:  Data Direction= output */
}

static void siu_portB_init_fnc(void)
{
	 PCC->PCCn[PCC_PORTB_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable clock for PORTB */

	#ifdef DSP_BOOTING_SEQUENCE
	PORTB->PCR[0] =PORT_PCR_MUX(3); 	//부팅시 port High를 위해 input 적용..  	CS
	PORTB->PCR[1] =PORT_PCR_MUX(3); 	//부팅시 port low를 위해 input 적용..  	SELE/SI
	PORTB->PCR[2] =PORT_PCR_MUX(3); 	//부팅시 port low를 위해 input 적용..  	SELE/SI
	PORTB->PCR[3] =PORT_PCR_MUX(3); 	//부팅시 port low를 위해 input 적용..  	SELE/SI
//	PTB->PDDR |= 1<<0;            						/* PortB0:  Data Direction= output 		*/
//	PTB->PDDR |= 1<<1;            						/* PortB1:  Data Direction= output 		*/
//	PTB->PDDR |= 1<<2;            						/* PortB2:  Data Direction= output 		*/
//	PTB->PDDR |= 1<<3;            						/* PortB3:  Data Direction= output 		*/
//	PTB->PCOR |= (1<<0);
//	PTB->PCOR |= (1<<1);
//	PTB->PCOR |= (1<<2);
//	PTB->PCOR |= (1<<3);
	#else
	PORTB->PCR[0]=PORT_PCR_MUX(3);		//SPI
	PORTB->PCR[1]=PORT_PCR_MUX(3);
	PORTB->PCR[2]=PORT_PCR_MUX(3);		//SPI
	PORTB->PCR[3]=PORT_PCR_MUX(3);		//SPI
	#endif
	#ifdef TFT_PW_FROM_GPIO_TO_PWM
	PORTB->PCR[4] = PORT_PCR_MUX(2);            		//PTB4					PWM			//TFT_PW_EN
	#else
	PORTB->PCR[4] |=PORT_PCR_MUX(1);            		//PTB4					GPIO O				//TFT_PW_EN
	PTB->PDDR |= 1<<4;            						/* PortB4:  Data Direction= output 		*/
	#endif
	PORTB->PCR[5] |=PORT_PCR_MUX(1);            		//PTB5					GPIO O				//GPS_MD_PW_EN
	//PTB6, PTB7					//XTAL 
	#if 0
	PORTB->PCR[8] = PORT_PCR_MUX(1);						//PTB8					GPIO O			//NC
	//#else
	PORTB->PCR[8] = PORT_PCR_MUX(1)|PORT_PCR_PS(0)|PORT_PCR_PE(1);						//PTB8					GPIO O			//NC
	//set pulldown
	#endif
	PORTB->PCR[9] = PORT_PCR_MUX(1);            		//PTB9					GPIO I				//ATB ATB_IRQ
	PORTB->PCR[10] |= PORT_PCR_MUX(2);           	//PTB10				PWM					//MCU_LCD2_PWM_3P3

	PORTB->PCR[11] |=  PORT_PCR_MUX(1);           	//PTB11				GPIO I				//DSP_3V3D_PG
	PTB->PDDR |= 1<<11;
	//PTB12						//ADC 			//MCU_VBAT_ADC
	PORTB->PCR[13] |=  PORT_PCR_MUX(1);         		//PTB13				GPIO O				//
#if 1
	PORTB->PCR[14] |= PORT_PCR_MUX(3);         		//PTB14				SPI1					//LPSPI1_SCK
	PORTB->PCR[15] |= PORT_PCR_MUX(3);         		//PTB15				SPI1					//LPSPI1_SIN
	PORTB->PCR[16] |= PORT_PCR_MUX(3);         		//PTB16				SPI1					//LPSPI1_SOUT
	PORTB->PCR[17] |= PORT_PCR_MUX(3);				//PTB17				SPI1					//LPSPI1_SCS
#endif

	PTB->PDDR |= 1<<5;            						/* PortB5:  Data Direction= output 		*/
	PTB->PDDR |= 1<<13;            					/* PortB13:  Data Direction= output 		*/
 
}

static void siu_portC_init_fnc(void)
{
	PCC->PCCn[PCC_PORTC_INDEX ] |= PCC_PCCn_CGC_MASK; /* Enable clock for PORTC */  

	//PTC0						//ADC							//KNOB_SW_1
	//PTC1						//ADC							//KNOB_SW_2
	PORTC->PCR[2]|=PORT_PCR_MUX(0b100);           /* Port C2: MUX = ALT4,UART0 RX */			//CPU COMM
	PORTC->PCR[3]|=PORT_PCR_MUX(0b100);           /* Port C3: MUX = ALT4,UART0 TX */			//CPU COMM
	PORTC->PCR[4]|=PORT_PCR_MUX(0b111); 					//JTAG
	PORTC->PCR[5]|=PORT_PCR_MUX(0b111); 					//JTAG
	PORTC->PCR[6]|=PORT_PCR_MUX(0b001);        //PTC6					GPIO O		//DSP_PDN
//	PORTC->PCR[7]|=PORT_PCR_MUX(0b001)|PORT_PCR_IRQC(0b1010);        //PTC7					GPIO I		//MCU_CAN0_ERR
	PORTC->PCR[7] =PORT_PCR_MUX(1);        	//PTC7					GPIO I		//MCU_CAN0_ERR
	PORTC->PCR[8]|=PORT_PCR_MUX(0b001);								//PTC8				GPIO I	
	PORTC->PCR[9]|=PORT_PCR_MUX(0b001);								//PTC9				GPIO I	
	PORTC->PCR[10]|=PORT_PCR_MUX(0b001);    						//PTC10				GPIO I		//TFT2_PWR_OCP_MCU 
	#if 1
//	PORTC->PCR[11]|=PORT_PCR_MUX(0b001);							//PTC11				GPIO O		//OMS_IRLED+_CTL
//	PTC->PDDR |= 1<<11;            				/* PortC11:  Data Direction= output */
	#else
	PORTC->PCR[11] = PORT_PCR_MUX(1)|PORT_PCR_PS(1)|PORT_PCR_PE(1);						//PTC11					GPIO I
	//PS 0 pull-down, 1 pull-up     //PE 1 enable
	#endif
	//PTC12						//NC	
	PORTC->PCR[13]|=PORT_PCR_MUX(0b001);     		    			//PTC13				GPIO I		//MCU_KNOB_SW_POW
//	PORTC->PCR[14] = PORT_PCR_MUX(0b001);         					//PTC14				GPIO I		//DSP_1V2D_PG
	PORTC->PCR[15]|=PORT_PCR_MUX(0b001);         					//PTC15				GPIO I				//MCU_SPI_CORE_INT
//	PORTC->PCR[16]|=PORT_PCR_MUX(0b001);          				//PTC16				// SW_AD1-
	PORTC->PCR[17]|=PORT_PCR_MUX(0b001);           					//PTC17				GPIO O				//MCU_UPGRADE_SOC

	PTC->PDDR |= 1<<6;            					/* PortC6:  Data Direction= output */
	PTC->PDDR |= 1<<17;            				/* PortC17:  Data Direction= output */
}

static void siu_portD_init_fnc(void)
{
	PCC->PCCn[PCC_PORTD_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable clock for PORTD */
	#if 1
//	PORTD->PCR[0] |=  PORT_PCR_MUX(0b001);   			//PWM	INPUT		//MCU_ILL_DET
	#else
	PORTD->PCR[0] |=  PORT_PCR_MUX(0b001);   			//for mengbo board
	#endif
	PORTD->PCR[1] |=  PORT_PCR_MUX(0b001); 					//PTD1			GPIO O			  	//M_5V_EN
	#ifdef B1_MOTHER_BOARD_SAMPLE
	#ifdef CHANG_BL_TIMING 
	PORTD->PCR[2] =  PORT_PCR_MUX(0b001); 																// MCU_HU_LCD_PWM
	PTD->PDDR |= 1<<2;																										/* PortD1:  Data Direction= output */	
	PTD->PSOR |= (1<<2);
	#else
	PORTD->PCR[2] |=  PORT_PCR_MUX(0b010); 																// MCU_HU_LCD_PWM
	#endif
	PORTD->PCR[4] |=  PORT_PCR_MUX(0b001); 																// MCU_HU_LCD_EN
	#endif
	#ifdef CHANG_BL_TIMING
//	PORTD->PCR[3]  =  PORT_PCR_MUX(0b001);					//PTD3			PWM O				//LCD1 PWM
//	PTD->PDDR |= 1<<3;								/* PortD1:  Data Direction= output */
//	PTD->PSOR |= (1<<3);
	#else
	PORTD->PCR[3] |=  PORT_PCR_MUX(0b010);					//PTD3			PWM O				//LCD1 PWM
	#endif
	//PTD4			ADC				//SW_AD2+
	
	//PTD5						//NC
	PORTD->PCR[6] |=  PORT_PCR_MUX(0b001); 									// GPIO O CAN_WAKE_N
	#ifdef B1_MOTHER_BOARD_SAMPLE
	PORTD->PCR[5] |=  PORT_PCR_MUX(0b001);  								//GPIO O MIC_PWR_EN_2
	#else
	PORTD->PCR[7] |=  PORT_PCR_MUX(0b001);  								//GPIO O MIC_PWR_EN_2
	#endif
	PORTD->PCR[8] |=  PORT_PCR_MUX(0b001);       							//PTD8 		GPIO I				//MCU_TBOX_MUTE
	PORTD->PCR[9] |=  PORT_PCR_MUX(0b001);        							//PTD9 		MCU_LCD1_2_EN_3P3
	PORTD->PCR[10] |=  PORT_PCR_MUX(0b001);         						//PTD10 		GPIO O				//MCU_CONTROL_PMIC_ONOFF
	PORTD->PCR[11] |=  PORT_PCR_MUX(0b001);         						//PTD11 		GPIO O				//MCU_TFT2_PWR_EN		
	PORTD->PCR[12] |=  PORT_PCR_MUX(0b001)|PORT_PCR_PE(0b1)|PORT_PCR_PS(0b1);//|PORT_PCR_IRQC(0b1010); //falling edge         						//PTD12 		GPIO I				//MCU_IGN_DET
	PORTD->PCR[13] |=  PORT_PCR_MUX(0b011);			//PTD13						//UART_DEBUG 					//NC
	PORTD->PCR[14] |=  PORT_PCR_MUX(0b011);			//PTD14						//UART_DEBUG 					//NC		//
//	PORTD->PCR[15] |=  PORT_PCR_MUX(0b001);         						//PTD15		GPIO I				//GPS_ANT_SHORT
//	PORTD->PCR[16] |=  PORT_PCR_MUX(0b001);         						//PTD16		GPIO I				//GPS_ANT_OPEN
//	PORTD->PCR[17] |=  PORT_PCR_MUX(0b001);          					//PTD17		GPIO O				//GPIO_01_FROM_MCU

	#if 0	//for mengbo board
	PTD->PDDR |= 1<<0;								/* PortD1:  Data Direction= output */	
	#endif
	PTD->PDDR |= 1<<1;								/* PortD1:  Data Direction= output */	
	PTD->PDDR |= 1<<6;								/* PortD6:  Data Direction= output */
	#ifdef B1_MOTHER_BOARD_SAMPLE
	PTD->PDDR |= 1<<4;								/* PortD4:  Data Direction= output */
	PTD->PDDR |= 1<<5;								/* PortD5:  Data Direction= output */
	#else
	PTD->PDDR |= 1<<7;								/* PortD7:  Data Direction= output */
	#endif
	PTD->PDDR |= 1<<9;								/* PortD9:  Data Direction= output */	
	PTD->PDDR |= 1<<10;								/* PortD10:  Data Direction= output */	
	PTD->PDDR |= 1<<11;            				/* PortD11:  Data Direction= output */

//	PTD->PDDR |= 1<<17;            				/* PortD17:  Data Direction= output */
}

static void siu_portE_init_fnc(void)
{
	PCC->PCCn[PCC_PORTE_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable clock for PORTE */
	
	PORTE->PCR[0] |=  PORT_PCR_MUX(0b001);           	//PTE0					//GPIO2 		TEST
	PORTE->PCR[1] |=  PORT_PCR_MUX(0b001);           	//PTE1					//GPIO I 
//	PORTE->PCR[2] |=  PORT_PCR_MUX(0b100);           	//PTE2					//PCB_VER
	PORTE->PCR[3] |=  PORT_PCR_MUX(0b001);          	//PTE3			GPIO O     			//M_MAIN_PWR_EN
	PORTE->PCR[4] |= PORT_PCR_MUX(0b101); /* Port E4: MUX = ALT5, CAN0_RX */
	PORTE->PCR[5] |= PORT_PCR_MUX(0b101); /* Port E5: MUX = ALT5, CAN0_TX */
	#ifdef B1_MOTHER_BOARD_SAMPLE
	PORTE->PCR[6] |=  PORT_PCR_MUX(0b100);           	//PTE6					//PWM
	#endif
//	PORTE->PCR[6] |=  PORT_PCR_MUX(0b000);           	//PTE6					//NC
//	PORTE->PCR[7] |=  PORT_PCR_MUX(0b000);           	//PTE7					//NC
	PORTE->PCR[8] |=  PORT_PCR_MUX(0b001);       	 	//PTE8			GPIO I   			//TFT1_PWR_OCP_MCU
	PORTE->PCR[9] |=  PORT_PCR_MUX(0b001);           	//PTE9									//MCU_HIGHT_BAT

	PORTE->PCR[10] |=  PORT_PCR_MUX(0b001);        	//PTE10 		GPIO O				//MCU_CAN0_EN
	PORTE->PCR[11] |=  PORT_PCR_MUX(0b001);       	//PTE11 		GPIO O				//MCU_CAN0_STB
	PORTE->PCR[12] |=  PORT_PCR_MUX(0b001);         	//PTE12 		GPIO I					//KNOB_B
	PORTE->PCR[13] |=  PORT_PCR_MUX(0b001);         	//PTE13		GPIO I					//KNOB_A
	PORTE->PCR[14] |=  PORT_PCR_MUX(0b001);        	//PTE14		GPIO O					//PMIC_RESET_IN
	#if 0 //def SI_LABS_TUNER	
	PORTE->PCR[15] = PORT_PCR_MUX(0b011);         	//PTE15		SPI2				//LPSPI2_SCK
	PORTE->PCR[16] = PORT_PCR_MUX(0b011);         	//PTE16		SPI2				//LPSPI2_SIN
	#endif

	PTE->PDDR |= 1<<0;            					/* PortE0:  Data Direction= output */
	PTE->PDDR |= 1<<3;            					/* PortE3:  Data Direction= output */
//	PTE->PDDR |= 1<<8;            					/* PortE8:  Data Direction= output */	
	PTE->PDDR |= 1<<10;            				/* PortE10:  Data Direction= output */
	PTE->PDDR |= 1<<11;            				/* PortE11:  Data Direction= output */
	PTE->PDDR |= 1<<14;            				/* PortE14:  Data Direction= output */
}


#if 0
static void siu_portA_deinit_fnc(void)
{	
//	PCC->PCCn[PCC_PORTA_INDEX ] = PCC_PCCn_CGC_MASK; /* Enable clock for PORTA */	

	PORTA->PCR[0] = 0; 
	PORTA->PCR[1] = 0; 
	PORTA->PCR[2] = 0;
	PORTA->PCR[3] = 0; 
//	PORTA->PCR[6] = 0; 	//INPUT
//	PORTA->PCR[7] = 0;
	PORTA->PCR[8] = 0;
	PORTA->PCR[9] = 0;
//	PORTA->PCR[11] = 0;
//	PORTA->PCR[12] = 0;
//	PORTA->PCR[13] = 0;
//	PORTA->PCR[14] =  0; 		//ACC wake up
//	PORTA->PCR[15] = 0;
//	PORTA->PCR[16] = 0;
	//PORTA->PCR[17] = 0;
}

static void siu_portB_deinit_fnc(void)
{
//	PCC->PCCn[PCC_PORTB_INDEX] &=~ PCC_PCCn_CGC_MASK; /* disable clock for PORTB */
	 
	PORTB->PCR[0] = 0; 
	PORTB->PCR[1] = 0; 
	PORTB->PCR[2] = 0;
	PORTB->PCR[3] = 0; 
//	PORTB->PCR[4] = 0; 
//	PORTB->PCR[5] = 0; 
	//PTB6, PTB7					//XTAL 
	PORTB->PCR[8] = 0; 
//	PORTB->PCR[9] = 0; 
	PORTB->PCR[10] = 0; 

//	PORTB->PCR[11] = 0; 
	PORTB->PCR[12] = 0; 
//	PORTB->PCR[13] = 0; 
	PORTB->PCR[14] = 0; 
	PORTB->PCR[15] = 0; 
	PORTB->PCR[16] = 0;
	PORTB->PCR[17] = 0; 
}



static void siu_portD_deinit_fnc(void)
{
//	PCC->PCCn[PCC_PORTD_INDEX] &=~ PCC_PCCn_CGC_MASK; /* disable clock for PORTD */
	
	PORTD->PCR[0] = 0; 
//	PORTD->PCR[1] = 0; 
	PORTD->PCR[2] = 0;
	PORTD->PCR[3] = 0; 
	//PTD5						//NC
//	PORTD->PCR[6] |=  PORT_PCR_MUX(1); 									// GPIO O CAN_WAKE_N
//	PORTD->PCR[7] = 0; 
//	PORTD->PCR[8] = 0; 
//	PORTD->PCR[9] = 0; 
//	PORTD->PCR[10] = 0; 
//	PORTD->PCR[11] = 0; 	
//	PORTD->PCR[12] = 0; 
//	PORTD->PCR[13] = 0; 
//	PORTD->PCR[14] = 0; 
//	PORTD->PCR[15] = 0; 
//	PORTD->PCR[16] = 0; 
	PORTD->PCR[17] = 0; 

}

static void siu_portE_deinit_fnc(void)
{
//	PCC->PCCn[PCC_PORTE_INDEX] &=~ PCC_PCCn_CGC_MASK; /* disable clock for PORTE */

	
	PORTE->PCR[0] = 0; 
	PORTE->PCR[1] = 0; 
	PORTE->PCR[2] = 0; 
//	PORTE->PCR[3] = 0; 
	PORTE->PCR[4] = 0;  /* Port E4: MUX = ALT5, CAN0_RX */
	PORTE->PCR[5] = 0;  /* Port E5: MUX = ALT5, CAN0_TX */

//	PORTE->PCR[8] = 0; 
//	PORTE->PCR[10] |=  PORT_PCR_MUX(1);        	//PTE10 		GPIO O				//MCU_CAN0_EN
//	PORTE->PCR[11] |=  PORT_PCR_MUX(1);       	//PTE11 		GPIO O				//MCU_CAN0_STB
	PORTE->PCR[12] = 0; 
	PORTE->PCR[13] = 0; 
//	PORTE->PCR[14] = 0; 
	
	PORTE->PCR[15] = 0; 
	PORTE->PCR[16] = 0; 

}
#else
static void siu_portA_deinit_fnc(void)
{	
	#if 0 //상관없음.
	PORTA->PCR[0] = 0; 
	PORTA->PCR[1] = 0; 
	PORTA->PCR[2] = 0;
	PORTA->PCR[3] = 0; 
	#endif
	#ifdef DISABLE_JTEG_IN_SLEEP_MODE
	PORTA->PCR[4] = 0;	//add		//JTEG_MCK
	#endif
	PORTA->PCR[5] = 0; 	//add
//low	PORTA->PCR[6] = 0; 
	PORTA->PCR[7] = 0;
	#if 0 //def SI_LABS_TUNER
	PORTA->PCR[8] = 0;//SPI2 				//LPSPI2_SOUT
	PORTA->PCR[9] = 0;//SPI2 				//LPSPI2_SOUT
	#endif
	#ifdef DISABLE_JTEG_IN_SLEEP_MODE
	PORTA->PCR[10] = 0; 	//add   //JTEG_TDO
	#endif
//low	PORTA->PCR[11] = 0;
//low	PORTA->PCR[12] = 0;
//low	PORTA->PCR[13] = 0;
//	PORTA->PCR[14] =  0; 		//ACC wake up
//low	PORTA->PCR[15] = 0;
	PORTA->PCR[16] = 0;
//low	PORTA->PCR[17] = 0;
}

static void siu_portB_deinit_fnc(void)
{ 
	#if 1				//이부분 제거하면 6mA정도 먹음.
	PORTB->PCR[0] = 0; 
	PORTB->PCR[1] = 0; 
	PORTB->PCR[2] = 0;
	PORTB->PCR[3] = 0; 
	#endif
//low	PORTB->PCR[4] = 0; 
//low	PORTB->PCR[5] = 0; 
	//PTB6, PTB7					//XTAL 
    PORTB->PCR[8] = 0; 
	PORTB->PCR[9] = 0; 
	PORTB->PCR[10] = 0; 

	PORTB->PCR[11] = 0; 
	PORTB->PCR[12] = 0; 
//	PORTB->PCR[13] = 0; 
	PORTB->PCR[14] = 0; 
	PORTB->PCR[15] = 0; 
	PORTB->PCR[16] = 0;
	PORTB->PCR[17] = 0; 
}

static void siu_portC_deinit_fnc(void)
{
	PORTC->PCR[0] = 0; 
	PORTC->PCR[1] = 0; 
	PORTC->PCR[2] = 0;
	PORTC->PCR[3] = 0; 
	#ifdef DISABLE_JTEG_IN_SLEEP_MODE
	PORTC->PCR[4] = 0;//add
	PORTC->PCR[5] = 0; //add
	#endif
	//PTC4						//JTAG
	//PTC5						//JTAG
//low	PORTC->PCR[6] = 0; 
//	PORTC->PCR[7] =PORT_PCR_MUX(0b001)|PORT_PCR_IRQC(0b1010);        //PTC7					GPIO I		//MCU_CAN0_ERR
	PORTC->PCR[7] = PORT_PCR_MUX(1)|PORT_PCR_IRQC(10);        //PTC7					GPIO I		//MCU_CAN0_ERR
	//PTC8						//NC
	//PTC9						//NC	
	PORTC->PCR[8] = 0; 
	PORTC->PCR[9] = 0; 
	PORTC->PCR[10] = 0; 
//low	PORTC->PCR[11] = 0; 
	PORTC->PCR[12] = 0; 
	//PTC12						//NC	
	PORTC->PCR[13]	=PORT_PCR_MUX(0b001)|PORT_PCR_IRQC(0b1001); 
	PORTC->PCR[14] = 0; 
	PORTC->PCR[15] = 0; 
	PORTC->PCR[16] = 0; 
	PORTC->PCR[17] = 0; 
}

static void siu_portD_deinit_fnc(void)
{
	PORTD->PCR[0] = 0; 
//low	PORTD->PCR[1] = 0;
	#if 1
	PORTD->PCR[2] = 0;
	#else
	PORTD->PCR[2] =	PORT_PCR_MUX(0b001); 
	PTD->PDDR |= 1<<2;
	PTD->PCOR |= (1<<2);
	#endif
	PORTD->PCR[3] = 0; 
// low	PORTD->PCR[4] = 0;
	PORTD->PCR[5] = 0;
	//PTD5						//NC
//low	PORTD->PCR[6] |=  PORT_PCR_MUX(0b001); 									// GPIO O CAN_WAKE_N
//low	PORTD->PCR[7] = 0; 
	PORTD->PCR[8] = 0; 
//low	PORTD->PCR[9] = 0; 
//low	PORTD->PCR[10] = 0; 
//low	PORTD->PCR[11] = 0; 	
//	PORTD->PCR[12] = 0;  		//IGN wake up
	PORTD->PCR[13] = 0; 
	PORTD->PCR[14] = 0; 
	PORTD->PCR[15] = 0; 
	PORTD->PCR[16] = 0; 

}

static void siu_portE_deinit_fnc(void)
{
	PORTE->PCR[0] = 0; 
	PORTE->PCR[1] = 0; 
    PORTE->PCR[2] = 0; 
//low	PORTE->PCR[3] = 0; 
//	PORTE->PCR[4] |= PORT_PCR_MUX(0b101); /* Port E4: MUX = ALT5, CAN0_RX */
//	PORTE->PCR[5] |= PORT_PCR_MUX(0b101); /* Port E5: MUX = ALT5, CAN0_TX */
	PORTE->PCR[6] = 0;
	PORTE->PCR[7] = 0;

//low	PORTE->PCR[8] = 0; 
//	PORTE->PCR[10] |=  PORT_PCR_MUX(0b001);        	//PTE10 		GPIO O				//MCU_CAN0_EN
//	PORTE->PCR[11] |=  PORT_PCR_MUX(0b001);       	//PTE11 		GPIO O				//MCU_CAN0_STB
	PORTE->PCR[12] = 0; 
	PORTE->PCR[13] = 0; 
//low	PORTE->PCR[14] = 0; 
#if 0 //def SI_LABS_TUNER
	PORTE->PCR[15] = 0; //SPI2 				//LPSPI2_SOUT
	PORTE->PCR[16] = 0; //SPI2 				//LPSPI2_SOUT
#endif
	PORTD->PCR[17] = 0; 
}
#endif

static void peri_clock_deinit_fnc(void)
{
	uint8 idx=0;
/////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////// Peri Clock Off
       for(idx=0;idx<2;idx++)
       {
	       PCC->PCCn[PCC_FlexCAN0_INDEX] = 0x80000000;
	       PCC->PCCn[PCC_FlexCAN1_INDEX] = 0x80000000;
	       PCC->PCCn[PCC_FlexCAN2_INDEX] = 0x80000000;

          PCC->PCCn[PCC_LPSPI0_INDEX] = 0x80000000;
          PCC->PCCn[PCC_LPSPI1_INDEX] = 0x80000000;
          PCC->PCCn[PCC_LPSPI2_INDEX] = 0x80000000;

          PCC->PCCn[PCC_FTM0_INDEX] = 0x80000000;
          PCC->PCCn[PCC_FTM1_INDEX] = 0x80000000;
          PCC->PCCn[PCC_FTM2_INDEX] = 0x80000000;
          PCC->PCCn[PCC_FTM3_INDEX] = 0x80000000;

          PCC->PCCn[PCC_ADC0_INDEX] = 0x80000000;
          PCC->PCCn[PCC_ADC1_INDEX] = 0x80000000;

//          PCC->PCCn[PCC_PDB0_INDEX] = 0x80000000;
//         PCC->PCCn[PCC_PDB1_INDEX] = 0x80000000;                   

          //PCC->PCCn[PCC_CMU0_INDEX] &= ~PCC_PCCn_CGC_MASK;
          //PCC->PCCn[PCC_CMU1_INDEX] &= ~PCC_PCCn_CGC_MASK;
          PCC->PCCn[PCC_LPTMR0_INDEX] = 0x80000000;
          PCC->PCCn[PCC_FlexIO_INDEX] = 0x80000000;
          PCC->PCCn[PCC_LPI2C0_INDEX] = 0x80000000;
          PCC->PCCn[PCC_LPUART0_INDEX] = 0x80000000;
          PCC->PCCn[PCC_LPUART1_INDEX] = 0x80000000;
//          PCC->PCCn[PCC_LPUART2_INDEX] = 0x80000000;

          /* PCC index offsets */

//        	PCC->PCCn[PCC_PORTA_INDEX] = 0x80000000;    /* Disable clock for PORTA */
          PCC->PCCn[PCC_PORTB_INDEX] = 0x80000000;    /* Disable clock for PORTB */
//       	PCC->PCCn[PCC_PORTC_INDEX] = 0x80000000;    /* Disable clock for PORTC */
//          PCC->PCCn[PCC_PORTD_INDEX] = 0x80000000;    /* Disable clock for PORTD */
          PCC->PCCn[PCC_PORTE_INDEX] = 0x80000000;    /* Disable clock for PORTE */
 //         PCC->PCCn[PCC_RTC_INDEX] = 0x80000000;    /* Disable clock for PORTE */             
          PCC->PCCn[PCC_LPIT_INDEX] = 0x80000000;
//          PCC->PCCn[PCC_EWM_INDEX] = 0x80000000;                   

          //PCC->PCCn[PCC_CRC_INDEX] &= ~PCC_PCCn_CGC_MASK;
          //PCC->PCCn[PCC_DMAMUX_INDEX] &= ~PCC_PCCn_CGC_MASK;
          //PCC->PCCn[PCC_CMP0_INDEX] &= ~PCC_PCCn_CGC_MASK;
          //PCC->PCCn[PCC_FTFC_INDEX] &= ~PCC_PCCn_CGC_MASK
       	}

}

void siu_general_init_fnc(void)
{
	//All port is low
	//PORTA
	MCU_AMP_ST_BY(OFF);
	MCU_AMP_MUTE(ON);
	//ATB_3V3_EN(OFF);
	DSP_PW_EN(OFF);
	M_5V_FRQ_SEL(OFF);//M_5V_FRQ_SEL
	BAT_ADC_ON(OFF);//BAT_ADC_ON

	//PORTB
	#ifndef TFT_PW_FROM_GPIO_TO_PWM
	TFT_PW_EN(OFF);
	#endif
	GPS_MD_PW_EN(OFF);
	MIC_PWR_EN_1(OFF);
	#ifdef B1_MOTHER_BOARD_SAMPLE
	USB_PW_EN(OFF);
	MCU_HU_LCD_EN(ON);
	#else
	USB_5V_FRQ_SEL(OFF);
	#endif
	MCU_LCD1_2_EN_3P3(ON);
	
	//PORTC
	DSP_PDN(OFF);		//PTC->PCOR |= (1<<6);	//DSP_PDN
	#if 0 //def SI_LABS_TUNER
	SiLabs_Tuner_RST(OFF);   //JANG_211025_1
	//#else
	IRLED_CTL(OFF);
	#endif
	
	//PORTD
	#if 0 ////for mengbo board
	PTD->PSOR |= (1<<0);		//mute
	#endif
	M_5V_EN(OFF);
	CAN0_WAKE_N(OFF);
	MIC_PWR_EN_2(OFF);
	MCU_CONTROL_PMIC_ONOFF(OFF);
	MCU_TFT2_PWR_EN(OFF);//MCU_TFT2_PWR_EN

	//PORTE
	M_MAIN_PWR_EN(OFF);
	MCU_CAN0_EN(OFF);
	MCU_CAN0_STB(OFF);
	PMIC_RESET_IN(OFF);
}

