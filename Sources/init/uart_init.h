/*
 * uart_init.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef INIT_UART_INIT_H_
#define INIT_UART_INIT_H_


#define OSC_CLK_FREQ					8000000

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

void init_dev_uart(void);

void uart0_deinit(void);
void uart1_deinit(void);
void UART_reset(uint32_t uart_num);


#endif /* INIT_UART_INIT_H_ */
