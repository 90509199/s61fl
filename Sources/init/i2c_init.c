/*
 * i2c_init.c
 *
 *  Created on: 2018. 7. 23.
 *      Author: Digen
 */

#include "includes.h"
//#include "Cpu.h"

lpi2c_master_state_t lpi2c1MasterState;

static volatile uint32_t hwi2c_tick = 0;

static uint8 r_ReadSaf7730Result;
static uint8 r_ReadSaf7730Error;

static uint8 r_WriteSaf7730State;
static uint8 r_WriteSaf7730Result;
static uint8 r_WriteSaf7730Error;

void i2c_init(void)
{
	LPI2C_DRV_MasterInit(INST_LPI2C1, &lpi2c1_MasterConfig0, &lpi2c1MasterState);

}

void i2c_deinit(void)
{
	LPI2C_DRV_MasterDeinit(INST_LPI2C1);
}

uint8 i2c_check_timeout(uint8 limit)
{
	if(GetElapsedTime(hwi2c_tick)<limit)
	{
		return 0;// smaller than limit
	}
	else
	{
		//i2c timeout	//1ms 
//		i2c_deinit();	// larger than limit
//		delay_ms(2);
//		i2c_init();	// larger than limit
		hwi2c_tick = GetTickCount();
		
		return 1;
	}
}

uint8 i2c_check_busfree(void)
{
	uint8 result,status;
	result=0;
	status=LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL);

	//if ((status==STATUS_SUCCESS) && (r_I2cMaster.i2cIdle == true))
	if(status==STATUS_SUCCESS)
	{
		result=1;
	}
	return result;
}

uint8 i2c_tef6686_step_write(uint32 subaddr, BYTE *src, BYTE cnt)
{
	static uint8 WriteTef6638_buf[256];
	static uint8 WriteTef6638_cnt;

	switch(r_WriteSaf7730State)
	{
	case 0:
//		timer->eeprom=0;

		r_WriteSaf7730State=1;
		r_WriteSaf7730Result=0x01;//I2C_BUSY

		if((subaddr&0x00FF0000)!=0x00)
		{
			WriteTef6638_buf[0]=(uint8)(subaddr>>16);
			WriteTef6638_buf[1]=(uint8)(subaddr>>8);
			WriteTef6638_buf[2]=(uint8)subaddr;
			memcpy(&WriteTef6638_buf[3],src,cnt);
			WriteTef6638_cnt=cnt+3;
		}
		else
		{
			WriteTef6638_buf[0]=(uint8)(subaddr);
			memcpy(&WriteTef6638_buf[1],src,cnt);
			WriteTef6638_cnt=cnt+1;
		}
		hwi2c_tick = GetTickCount();
		break;
	case 1:
		if(i2c_check_busfree()==1)
		{
			LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C1, TUNER_CHIP_ADDR >> 1, false);
			LPI2C_DRV_MasterSendData(INST_LPI2C1,WriteTef6638_buf,WriteTef6638_cnt,true);
			r_WriteSaf7730State=2;
			hwi2c_tick = GetTickCount();
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				r_WriteSaf7730Result=I2C_ERROR;//I2C_ERR
				r_WriteSaf7730State=0;
				r_WriteSaf7730Error=0;
				hwi2c_tick = GetTickCount();
			}
		}
		break;
	case 2:
//		timer->eeprom=0;	
		if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_I2C_RECEIVED_NACK)
		{
			r_WriteSaf7730State=0;
//			F_I2C_ERROR=0;
			r_WriteSaf7730Error++;
			if(r_WriteSaf7730Error>=5) 
			{
				r_WriteSaf7730Error=0;
				r_WriteSaf7730Result=I2C_ERROR;
			}
		}
		else if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_SUCCESS)
		{
//			F_I2C_COMPLETE=0;
			r_WriteSaf7730Error=0;
			r_WriteSaf7730Result=I2C_OK;
			r_WriteSaf7730State=0;
//			I2c_SlaveMode();
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				r_WriteSaf7730Result=I2C_ERROR;
				r_WriteSaf7730State=0;
				r_WriteSaf7730Error=0;
			}
		}
		break;
	default:
		break;				
	}
	return r_WriteSaf7730Result;
}


//uint8 i2c_tef6686_step_read(uint32 subaddr, uint8 *des, uint8 cnt)
//{
//	static uint8	step =	 HWI2C_IDLE;
//	static uint8	result =	 I2C_BUSY;
//	static uint8 i2c_error = 0;
//	static uint8 temp_subaddr[3] ={0,};
//	static uint8 subaddr_len=0;
//
//	switch(step)
//	{
//	case HWI2C_IDLE:
//
//		if((subaddr&0x00FF0000)!=0x00)
//		{
//			temp_subaddr[0]=(uint8)(subaddr>>16);
//			temp_subaddr[1]=(uint8)(subaddr>>8);
//			temp_subaddr[2]=(uint8)subaddr;
//			subaddr_len=3;
//		}
//		else if((subaddr&0x00FF00)!=0x00){
//			temp_subaddr[0]=(uint8)(subaddr>>8);
//			temp_subaddr[1]=(uint8)subaddr;
//			subaddr_len=2;
//		}
//		else
//		{
//			temp_subaddr[0]=(uint8)(subaddr);
//			subaddr_len=1;
//		}
//
//		result=I2C_BUSY;
//		step = HWI2C_STEP1;
//		hwi2c_tick = GetTickCount();
//		break;
//	case HWI2C_STEP1:
////		if(timer->eeprom<T2ms) break;
////		timer->eeprom=0;
//		if(i2c_check_busfree()==1)
//		{
//			LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C1, TUNER_CHIP_ADDR >> 1, false);
//			LPI2C_DRV_MasterSendData(INST_LPI2C1,temp_subaddr,subaddr_len,false);
////			IIC0_MasterStart(r_Device.dirana->chipaddress,subaddr,3,des,cnt,I2C_READ|I2C_SUB);
//			step=HWI2C_STEP2;
//			hwi2c_tick = GetTickCount();
//		}
//		else
//		{
//			if(i2c_check_timeout(2)==1)
//			{
//				result=I2C_ERROR;
//				step=HWI2C_IDLE;
//				hwi2c_tick = GetTickCount();
//				i2c_error=0;
//			}
//		}
//		break;
//	case HWI2C_STEP2:
////		timer->eeprom=0;
//		if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_I2C_RECEIVED_NACK)
//		{
//			step=HWI2C_IDLE;
//			i2c_error++;
//			if(i2c_error>=5)
//			{
//				result=I2C_ERROR;
//				i2c_error=0;
//			}
//		}
//		else if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_SUCCESS)
//		{
//			i2c_error=0;
//			result=I2C_BUSY;
//			hwi2c_tick = GetTickCount();
//			step=HWI2C_STEP3;
//		}
//		else
//		{
//			if(i2c_check_timeout(2)==1)
//			{
//				result=I2C_ERROR;
//				step=HWI2C_IDLE;
//				i2c_error=0;
//			}
//		}
//		break;
//	case HWI2C_STEP3:
////		timer->eeprom=0;
//		if(i2c_check_busfree()==1)
//		{
//			LPI2C_DRV_MasterReceiveData(INST_LPI2C1,des,cnt,true);
//			step=HWI2C_STEP4;
//		}
//		else
//		{
//			if(i2c_check_timeout(2)==1)
//			{
//				result=I2C_ERROR;
//				step=HWI2C_IDLE;
//				i2c_error=0;
//			}
//		}
//		break;
//	case HWI2C_STEP4:
////		timer->eeprom=0;
//		if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_I2C_RECEIVED_NACK)
//		{
//			step=HWI2C_IDLE;
//			i2c_error++;
//			if(i2c_error>=5)
//			{
//				i2c_error=0;
//				result=I2C_ERROR;
//			}
//		}
//		else if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_SUCCESS)
//		{
//			i2c_error=0;
//			result=I2C_OK;
//			step=HWI2C_IDLE;
//		}
//		else
//		{
//			if(i2c_check_timeout(2)==1)
//			{
//				result=I2C_ERROR;
//				step=HWI2C_IDLE;
//				i2c_error=0;
//			}
//		}
//		break;
//	default:
//		break;
//	}
//	return result;
//}
//
//
uint8 tuner_i2c_write(uint32 subaddr, uint8 *src, uint8 cnt)
{
	static uint8	step =	 HWI2C_IDLE;
	static uint8	result =	 I2C_BUSY;
	static uint8 i2c_error = 0;
	static uint8 temp_subaddr[256] ={0,};
	static uint8 subaddr_len=0;
	static uint8 err_cnt=0;


	result=I2C_BUSY;

	if((subaddr&0x00FF0000)!=0x00)
	{
		temp_subaddr[0]=(uint8)(subaddr>>16);
		temp_subaddr[1]=(uint8)(subaddr>>8);
		temp_subaddr[2]=(uint8)subaddr;
		memcpy(&temp_subaddr[3],src,cnt);
		subaddr_len=cnt+3;
	}
	else
	{
		temp_subaddr[0]=(uint8)(subaddr);
		memcpy(&temp_subaddr[1],src,cnt);
		subaddr_len=cnt+1;
	}

	hwi2c_tick = GetTickCount();
	err_cnt =0;
	retry:
	while(1)
	{
		if(i2c_check_busfree()==1)
		{
			LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C1, TUNER_CHIP_ADDR >> 1, false);
			LPI2C_DRV_MasterSendData(INST_LPI2C1,temp_subaddr,subaddr_len,true);
			//임시 추가사항...
			delay_ms(subaddr_len/10);
			break;
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				DPRINTF(DBG_MSG1,"%s timeout fail 1\n\r", __FUNCTION__);
				result=I2C_ERROR;//I2C_ERR
				return  result;
			}
		}
	}
	hwi2c_tick = GetTickCount();

	while(1)
	{
		if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_I2C_RECEIVED_NACK)
		{
			err_cnt++;
			if(err_cnt<=5)
				goto retry;
			err_cnt=0;
			#if 1
			hwi2c_tick = GetTickCount();
			#endif
			DPRINTF(DBG_MSG1,"%s nack fail \n\r", __FUNCTION__);
			result=I2C_ERROR;
			break;
		}
		else if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_SUCCESS)
		{
			result=I2C_OK;
			break;
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				DPRINTF(DBG_MSG1,"%s timeout fail 2\n\r", __FUNCTION__);
				result=I2C_ERROR;
				break;
			}
		}
	}

return result;
}


uint8 tuner_i2c_read(uint32 subaddr, uint8 *des, uint8 cnt)
{
	static uint8 ReadTef6638_subaddr[3];
	static uint8 ReadTef6638_cnt;

	r_ReadSaf7730Result=I2C_BUSY;

	if((subaddr&0x00FF0000)!=0x00)
	{
		ReadTef6638_subaddr[0]=(uint8)(subaddr>>16);
		ReadTef6638_subaddr[1]=(uint8)(subaddr>>8);
		ReadTef6638_subaddr[2]=(uint8)subaddr;
		ReadTef6638_cnt=3;
	}
	else
	{
		ReadTef6638_subaddr[0]=(uint8)(subaddr);
		ReadTef6638_cnt=1;
	}

	hwi2c_tick = GetTickCount();

	while(1){
		if(i2c_check_busfree()==1)
		{
			LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C1, TUNER_CHIP_ADDR >> 1, false);
			LPI2C_DRV_MasterSendData(INST_LPI2C1,ReadTef6638_subaddr,ReadTef6638_cnt,false);
			break;
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				DPRINTF(DBG_MSG1,"%s timeout fail 1\n\r", __FUNCTION__);
				r_ReadSaf7730Result=I2C_ERROR;
				return r_ReadSaf7730Result;
			}
		}
	}
	hwi2c_tick = GetTickCount();

	while(1)
	{
		if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_I2C_RECEIVED_NACK)
		{
			#if 1
			DPRINTF(DBG_MSG1,"%s nack fail 1\n\r", __FUNCTION__);
			#endif
			r_ReadSaf7730Result=I2C_ERROR;
			return r_ReadSaf7730Result;
		}
		else if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_SUCCESS)
		{
			r_ReadSaf7730Result=I2C_BUSY;
			break;
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				DPRINTF(DBG_MSG1,"%s timeout fail 2\n\r", __FUNCTION__);
				r_ReadSaf7730Result=I2C_ERROR;
				return r_ReadSaf7730Result;
			}
		}
	}
	hwi2c_tick = GetTickCount();

	while(1)
	{
		if(i2c_check_busfree()==1)
		{
			LPI2C_DRV_MasterReceiveData(INST_LPI2C1,des,cnt,true);
			break;
		}
		else
		{
			if(i2c_check_timeout(2)==1)
			{
				DPRINTF(DBG_MSG1,"%s timeout fail 3\n\r", __FUNCTION__);
				r_ReadSaf7730Result=I2C_ERROR;
				return r_ReadSaf7730Result;
			}
		}
	}
	hwi2c_tick = GetTickCount();

	while(1)
	{
		if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_I2C_RECEIVED_NACK)
		{
			#if 1
			DPRINTF(DBG_MSG1,"%s nack fail 2\n\r", __FUNCTION__);
			#endif
			r_ReadSaf7730Result=I2C_ERROR;
			break;
		}
		else if(LPI2C_DRV_MasterGetTransferStatus(INST_LPI2C1,NULL)==STATUS_SUCCESS)
		{
			r_ReadSaf7730Result=I2C_OK;
			break;
		}
		else
		{
			if(i2c_check_timeout(5)==1)
			{
				DPRINTF(DBG_MSG1,"%s timeout fail 4\n\r", __FUNCTION__);
				r_ReadSaf7730Result=I2C_ERROR;
				break;
			}
		}
	}

	return r_ReadSaf7730Result;
}




