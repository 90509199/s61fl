/*
 * flexio_i2c_init.h
 *
 *  Created on: 2018. 7. 23.
 *      Author: Digen
 */

#ifndef FLEXIO_I2C_INIT_H_
#define FLEXIO_I2C_INIT_H_

#if 0
#define TUNER_CHIP_ADDR	(0xC8)
#define AMP_CHIP_ADDR		(0xD8)


#define I2C_OK 						(0x00)
#define I2C_BUSY					(0x01)
#define I2C_ERROR 					(0x02)
#define I2C_COMPLETE 			(0x08)


enum err
{
    OK,
    BUSY,
    NO_DATA_RECEIVED,
    NO_STOP,
    NDF,
    ALF,
    FEF,
    PLTF
};


#define BUSY_TIMEOUT        		1000
#define READING_TIMEOUT    	5000 // 2000
#define STOP_TIMEOUT        		3000


#endif

/*******************************************************************************
* Function prototypes
*******************************************************************************/
void flexio_i2c_init(void);
void flexio_i2c_deinit(void);
//uint8 flexio_i2c_check_timeout(uint8 limit);
//uint8 flexio_i2c_check_busfree(void);

uint8 flexio_i2c_write(uint32 subaddr, uint8 *src, uint8 cnt);
uint8 flexio_i2c_read(uint32 subaddr, uint8 *des, uint8 cnt);
uint8 TB2952AHQ_Write(uint8 subaddr, uint8 *des, uint8 cnt);



#endif /* FLEXIO_I2C_INIT_H_ */
