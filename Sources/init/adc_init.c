/*
 * adc_init.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#include "includes.h"

//PTC0		//KNOB_SW_1 						//ADC0_SE8					//SC1[8]
//PTC1		//KNOB_SW_2 						//ADC0_SE9					//SC1[9]

//PTB12	//MCU_VBAT_ADC 					//ADC1_SE7					//SC1[7]
//PTE2		//PCB_ADC_VER 						//ADC1_SE10					//SC1[10]

//PTA7			//SW_AD1+						//ADC0_SE3					//SC1[3]
//PTC16		//SW_AD1-						//ADC0_SE14					//SC1[14]

void ADC_Init(void)
{
	Init_ADC0();
	Init_ADC1();
}

void Init_ADC0(void)
{
	PCC->PCCn[PCC_ADC0_INDEX] &=~ PCC_PCCn_CGC_MASK; /* Disable clock to change PCS */
	PCC->PCCn[PCC_ADC0_INDEX] |= PCC_PCCn_PCS(1); /* PCS=1: Select SOSCDIV2 */
	PCC->PCCn[PCC_ADC0_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable bus clock in ADC */
	
	ADC0->SC1[0] =0x00001F; /* ADCH=1F: Module is disabled for conversions*/
	
	/* AIEN=0: Interrupts are disabled */
	ADC0->CFG1 = 0x000000004; /* ADICLK=0: Input clk=ALTCLK1=SOSCDIV2 */
	/* ADIV=0: Prescaler=1 */
	/* MODE=1: 12-bit conversion */
	ADC0->CFG2 = 0x00000000C; /* SMPLTS=12(default): sample time is 13 ADC clks */
	ADC0->SC2 = 0x00000000; /* ADTRG=0: SW trigger */
	/* ACFE,ACFGT,ACREN=0: Compare func disabled */
	/* DMAEN=0: DMA disabled */
	/* REFSEL=0: Voltage reference pins= VREFH, VREEFL */
	ADC0->SC3 = 0x00000000; /* CAL=0: Do not start calibration sequence */
	/* ADCO=0: One conversion performed */
	/* AVGE,AVGS=0: HW average function disabled */
}

void Init_ADC1(void)
{
	PCC->PCCn[PCC_ADC1_INDEX] &=~ PCC_PCCn_CGC_MASK; /* Disable clock to change PCS */
	PCC->PCCn[PCC_ADC1_INDEX] |= PCC_PCCn_PCS(1); /* PCS=1: Select SOSCDIV2 */
	PCC->PCCn[PCC_ADC1_INDEX] |= PCC_PCCn_CGC_MASK; /* Enable bus clock in ADC */

	ADC1->SC1[0] =0x00001F; /* ADCH=1F: Module is disabled for conversions*/
	/* AIEN=0: Interrupts are disabled */
	ADC1->CFG1 = 0x000000004; /* ADICLK=0: Input clk=ALTCLK1=SOSCDIV2 */
	/* ADIV=0: Prescaler=1 */
	/* MODE=1: 12-bit conversion */
	ADC1->CFG2 = 0x00000000C; /* SMPLTS=12(default): sample time is 13 ADC clks */
	ADC1->SC2 = 0x00000000; /* ADTRG=0: SW trigger */
	/* ACFE,ACFGT,ACREN=0: Compare func disabled */
	/* DMAEN=0: DMA disabled */
	/* REFSEL=0: Voltage reference pins= VREFH, VREEFL */
	ADC1->SC3 = 0x00000000; /* CAL=0: Do not start calibration sequence */
	/* ADCO=0: One conversion performed */
	/* AVGE,AVGS=0: HW average function disabled */
}

void Enable_ADC0_Ch(uint8_t ch)
{
	/* For SW trigger mode, SC1[0] is used */
	ADC0->SC1[0]&=~ADC_SC1_ADCH_MASK; /* Clear prior ADCH bits */
	ADC0->SC1[0] = ADC_SC1_ADCH(ch); /* Initiate Conversion*/
}

void Disable_ADC0_Ch(void)
{
	ADC0->SC1[0] =0x00001F; /* ADCH=1F: Module is disabled for conversions*/
}

uint8_t Wait_Check_ADC0(void){
	return ((ADC0->SC1[0] & ADC_SC1_COCO_MASK)>>ADC_SC1_COCO_SHIFT); /* Wait for completion */
}

uint32_t Read_ADC0_CH(void)
{
	uint16_t adc_result=0;
	
	adc_result=ADC0->R[0]; /* For SW trigger mode, R[0] is used */
	#ifdef ADC0_3P3_CHANGE
	return (uint32_t) ((3300*adc_result)/0xFFF); /* Convert result to mv for 0-3.3V range */
	#else
	return (uint32_t) ((5000*adc_result)/0xFFF); /* Convert result to mv for 0-5V range */
	#endif
}

void Enable_ADC1_Ch(uint16_t ch)
{
	/* For SW trigger mode, SC1[0] is used */
	ADC1->SC1[0]&=~ADC_SC1_ADCH_MASK; /* Clear prior ADCH bits */
	ADC1->SC1[0] = ADC_SC1_ADCH(ch); /* Initiate Conversion*/
}

void Disable_ADC1_Ch(void)
{
	ADC1->SC1[0] =0x00001F; /* ADCH=1F: Module is disabled for conversions*/
}

uint8_t Wait_Check_ADC1(void) 
{
	return ((ADC1->SC1[0] & ADC_SC1_COCO_MASK)>>ADC_SC1_COCO_SHIFT); /* Wait for completion */
}

uint32_t Read_ADC1_CH(void) 
{
	uint16_t adc_result=0;
	
	adc_result=ADC1->R[0]; /* For SW trigger mode, R[0] is used */
 
	#ifdef ADC1_3P3_CHANGE
	return (uint32_t) ((3300*adc_result)/0xFFF); /* Convert result to mv for 0-3.3V range */
	#else
	return (uint32_t) ((5000*adc_result)/0xFFF); /* Convert result to mv for 0-5V range */
	#endif
}

