/*
 * siu_init.h
 *
 *  Created on: 2018. 5. 17.
 *      Author: Digen
 */

#ifndef INIT_SIU_INIT_H_
#define INIT_SIU_INIT_H_


void siu_init_fnc(void);
void siu_deinit_fnc(void);
void siu_general_init_fnc(void);

#endif /* INIT_SIU_INIT_H_ */
