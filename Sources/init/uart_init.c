/*
 * uart_init.c
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#include "includes.h"
#include "lpuart_hw_access.h"


#define    SOSC_FREQ				8000000u

#if 1
static void uart0_init(uint32_t BaudRate);
static void uart1_init(uint32_t BaudRate);
static void uart2_init(uint32_t BaudRate);

//START_FUNCTION_DECLARATION_RAMSECTION
static void UART0_TXRX_IRQ_Handle(void);
//END_FUNCTION_DECLARATION_RAMSECTION

//START_FUNCTION_DECLARATION_RAMSECTION
static void UART1_TXRX_IRQ_Handle(void);
//END_FUNCTION_DECLARATION_RAMSECTION


static status_t LPUART_SetBaudRate(uint32_t instance, uint32_t desiredBaudRate);

void (*pUART_IRQ_Handle[LPUART_INSTANCE_COUNT])(void) = {UART0_TXRX_IRQ_Handle, UART1_TXRX_IRQ_Handle};
static LPUART_Type * const s_lpuartBase[LPUART_INSTANCE_COUNT] = LPUART_BASE_PTRS;
/* Table to save LPUART enum numbers defined in CMSIS files. */
static const IRQn_Type s_lpuartRxTxIrqId[LPUART_INSTANCE_COUNT] = LPUART_RX_TX_IRQS;



void UART_init(uint32_t uart_num, uint32_t baud)
{
	
	if (uart_num < LPUART_INSTANCE_COUNT) 
	{
			
		/* initialize the LPUART instance */
	    LPUART_Init(s_lpuartBase[uart_num]);
	    /* initialize the parameters of the LPUART config structure with desired data */
	    LPUART_SetBaudRate(uart_num, baud);

		s_lpuartBase[uart_num]->FIFO |=0xCE;	//TX FIFO 32 words		RX FIFO 128 words

		#if 0
	    LPUART_SetBitCountPerChar(s_lpuartBase[uart_num], LPUART_8_BITS_PER_CHAR, false);
	    LPUART_SetParityMode(s_lpuartBase[uart_num], LPUART_PARITY_DISABLED);
	    LPUART_SetStopBitCount(s_lpuartBase[uart_num], LPUART_ONE_STOP_BIT);
		#else
		LPUART_SetBitCountPerChar(s_lpuartBase[uart_num], 0x0);
	    LPUART_SetParityMode(s_lpuartBase[uart_num], 0x0);
	    LPUART_SetStopBitCount(s_lpuartBase[uart_num], 0x0);
		#endif


	    /* finally, enable the LPUART transmitter and receiver */
	    LPUART_SetTransmitterCmd(s_lpuartBase[uart_num], true);
	    LPUART_SetReceiverCmd(s_lpuartBase[uart_num], true);

	    /* Install LPUART irq handler */
	    INT_SYS_InstallHandler(s_lpuartRxTxIrqId[uart_num], pUART_IRQ_Handle[uart_num], (isr_t*) 0);

	    /* Enable LPUART interrupt. */
	    INT_SYS_EnableIRQ(s_lpuartRxTxIrqId[uart_num]);

	    /* Enable the receive data overrun interrupt */
	    LPUART_SetIntMode(s_lpuartBase[uart_num], LPUART_INT_RX_OVERRUN, true);
	    /* Enable receive data full interrupt */
	    LPUART_SetIntMode(s_lpuartBase[uart_num], LPUART_INT_RX_DATA_REG_FULL, true);

	    /* enable transmission complete interrupt */
	    //LPUART_SetIntMode(base, LPUART_INT_TX_DATA_REG_EMPTY, true);
    }
}

void UART_reset(uint32_t uart_num)
{
	if (uart_num < LPUART_INSTANCE_COUNT) 
	{
		/* Wait until the data is completely shifted out of shift register */
	    while (!LPUART_GetStatusFlag(s_lpuartBase[uart_num], LPUART_TX_COMPLETE)) {}

    	/* Disable LPUART interrupt. */
	    INT_SYS_DisableIRQ(s_lpuartRxTxIrqId[uart_num]);

		/* Set to after reset state an disable UART Tx/Rx */
		s_lpuartBase[uart_num]->CTRL = 0x00000000u;
		s_lpuartBase[uart_num]->BAUD = 0x00000000u;

		/* Disable clock to UART */
		//pPccBase->PCCn[(uint32_t)PCC_LPUART0_INDEX+uart_num] &= ~PCC_PCCn_CGC_MASK;     /* Enable clock for LPUARTx regs */
	}
}

void init_dev_uart(void)
{
	#if 1
	uart0_init(115200);
	uart1_init(115200);
	init_uart0();
	init_uart1();
	#else
	UART_init(0, 115200);
	UART_init(1, 115200);
	#endif
}

static void uart0_init(uint32_t BaudRate)
{
	PCC->PCCn[PCC_LPUART0_INDEX] &= ~PCC_PCCn_CGC_MASK;    /* Ensure clk disabled for config */
	PCC->PCCn[PCC_LPUART0_INDEX] |= PCC_PCCn_PCS(1)|  PCC_PCCn_CGC_MASK;    /* Clock Src= 1 (SOSCDIV2_CLK) */
	//0x0400000E->0x04000008

#if 1
	LPUART0->GLOBAL=0x00000002;//reset
	LPUART0->GLOBAL=0x00000000;//on
#endif
	LPUART0->BAUD = ((uint32_t)((FEATURE_LPUART_DEFAULT_OSR << LPUART_BAUD_OSR_SHIFT) | \
                 (FEATURE_LPUART_DEFAULT_SBR << LPUART_BAUD_SBR_SHIFT)));
    /* Clear the error/interrupt flags */
    LPUART0->STAT = FEATURE_LPUART_STAT_REG_FLAGS_MASK;
    /* Reset all features/interrupts by default */
    LPUART0->CTRL = 0x00000000u;
    /* Reset match addresses */
    LPUART0->MATCH = 0x00000000u;
    /* Reset IrDA modem features */
    LPUART0->MODIR = 0x00000000u;
    /* Reset FIFO feature */
	LPUART0->FIFO = FEATURE_LPUART_FIFO_RESET_MASK|0xFF;	//TX FIFO 256 words		RX FIFO 256 words
    /* Reset FIFO Watermark values */
    LPUART0->WATER = 0x00000000u;
	LPUART_SetBaudRate(0, 115200);
//	LPUART_SetBaudRate(0, 230400);// 230400

//	LPUART0->CTRL |= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK);
		/* Install LPUART irq handler */
  
//	INT_SYS_SetPriority( s_lpuartRxTxIrqId[0], 1 );
	LPUART0->CTRL|= (LPUART_CTRL_TE_MASK|LPUART_CTRL_RE_MASK|LPUART_CTRL_ORIE_MASK | LPUART_CTRL_RIE_MASK);
															 /* Enable transmitter & receiver, no parity, 8 bit char: */
                               /* RE=1: Receiver enabled */
                               /* TE=1: Transmitter enabled */
                               /* PE,PT=0: No hw parity generation or checking */
                               /* M7,M,R8T9,R9T8=0: 8-bit data characters*/
                               /* DOZEEN=0: LPUART enabled in Doze mode */
                               /* ORIE,NEIE,FEIE,PEIE,TIE,TCIE,RIE,ILIE,MA1IE,MA2IE=0: no IRQ*/
                               /* TxDIR=0: TxD pin is input if in single-wire mode */
                               /* TXINV=0: TRansmit data not inverted */
                               /* RWU,WAKE=0: normal operation; rcvr not in statndby */
                               /* IDLCFG=0: one idle character */
                               /* ILT=0: Idle char bit count starts after start bit */
                               /* SBK=0: Normal transmitter operation - no break char */
                               /* LOOPS,RSRC=0: no loop back */

	 INT_SYS_InstallHandler(s_lpuartRxTxIrqId[0], pUART_IRQ_Handle[0], (isr_t*) 0);
	 
	 /* Enable LPUART interrupt. */
	 INT_SYS_EnableIRQ(s_lpuartRxTxIrqId[0]);

}

static void uart1_init(uint32_t BaudRate)  /* Init. summary: 9600 baud, 1 stop bit, 8 bit format, no parity */
{
	uint32_t temp=0;

	PCC->PCCn[PCC_LPUART1_INDEX] &= ~PCC_PCCn_CGC_MASK;    /* Ensure clk disabled for config */
	PCC->PCCn[PCC_LPUART1_INDEX] |= PCC_PCCn_PCS(1)   //PCC_PCCn_PCS(0b001)   /* Clock Src= 1 (SOSCDIV2_CLK) */
	                             |  PCC_PCCn_CGC_MASK;     /* Enable clock for LPUART1 regs */
#if 1
	LPUART1->GLOBAL=0x00000002;//reset
	LPUART1->GLOBAL=0x00000000;//on
#endif
	LPUART1->BAUD = ((uint32_t)((FEATURE_LPUART_DEFAULT_OSR << LPUART_BAUD_OSR_SHIFT) | \
	             (FEATURE_LPUART_DEFAULT_SBR << LPUART_BAUD_SBR_SHIFT)));
	/* Clear the error/interrupt flags */
	LPUART1->STAT = FEATURE_LPUART_STAT_REG_FLAGS_MASK;
	/* Reset all features/interrupts by default */
	LPUART1->CTRL = 0x00000000u;
	/* Reset match addresses */
	LPUART1->MATCH = 0x00000000u;
	/* Reset IrDA modem features */
	LPUART1->MODIR = 0x00000000u;
	/* Reset FIFO feature */
	LPUART1->FIFO = FEATURE_LPUART_FIFO_RESET_MASK|0xFF;	//TX FIFO 32 words		(0xCC-> 0xFF)	RX FIFO 32 words->256b words
	/* Reset FIFO Watermark values */
	LPUART1->WATER = 0x00000000u;
	LPUART_SetBaudRate(1, 115200);// 230400
	//	LPUART_SetBaudRate(1, 115200);// 230400
//	LPUART_SetBaudRate(1, 230400);// 230400
//	LPUART_SetBaudRate(1, 460800);// 230400

//	LPUART1->CTRL |= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK);


	LPUART1->CTRL|= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK|LPUART_CTRL_ORIE_MASK | LPUART_CTRL_RIE_MASK);
															 /* Enable transmitter & receiver, no parity, 8 bit char: */
                       /* RE=1: Receiver enabled */
                               /* TE=1: Transmitter enabled */
                               /* PE,PT=0: No hw parity generation or checking */
                               /* M7,M,R8T9,R9T8=0: 8-bit data characters*/
                               /* DOZEEN=0: LPUART enabled in Doze mode */
                               /* ORIE,NEIE,FEIE,PEIE,TIE,TCIE,RIE,ILIE,MA1IE,MA2IE=0: no IRQ*/
                               /* TxDIR=0: TxD pin is input if in single-wire mode */
                               /* TXINV=0: TRansmit data not inverted */
                               /* RWU,WAKE=0: normal operation; rcvr not in statndby */
                               /* IDLCFG=0: one idle character */
                               /* ILT=0: Idle char bit count starts after start bit */
                               /* SBK=0: Normal transmitter operation - no break char */
                               /* LOOPS,RSRC=0: no loop back */
	 INT_SYS_InstallHandler(s_lpuartRxTxIrqId[1], pUART_IRQ_Handle[1], (isr_t*) 0);
	 
	 /* Enable LPUART interrupt. */
	 INT_SYS_EnableIRQ(s_lpuartRxTxIrqId[1]);
	 INT_SYS_SetPriority( s_lpuartRxTxIrqId[1], 1 );


}

static void uart2_init(uint32_t BaudRate)
{
  PCC->PCCn[PCC_LPUART2_INDEX] &= ~PCC_PCCn_CGC_MASK;    /* Ensure clk disabled for config */
  PCC->PCCn[PCC_LPUART2_INDEX] |= PCC_PCCn_PCS(1)|  PCC_PCCn_CGC_MASK;    /* Clock Src= 1 (SOSCDIV2_CLK) */

  LPUART2->GLOBAL=0x00000002;//reset
  LPUART2->GLOBAL=0x00000000;//on

  LPUART2->BAUD = 0x0400000E;  /* Initialize for 115200 baud, 1 stop: */
                               /* SBR=52 (0x34): baud divisor = 8M/9600/16 = ~52 */
                               /* OSR=15: Over sampling ratio = 15+1=16 */
                               /* SBNS=0: One stop bit */
                               /* BOTHEDGE=0: receiver samples only on rising edge */
                               /* M10=0: Rx and Tx use 7 to 9 bit data characters */
                               /* RESYNCDIS=0: Resync during rec'd data word supported */
                               /* LBKDIE, RXEDGIE=0: interrupts disable */
                               /* TDMAE, RDMAE, TDMAE=0: DMA requests disabled */
                               /* MAEN1, MAEN2,  MATCFG=0: Match disabled */

  LPUART2->CTRL=0x002C0000;    /* Enable transmitter & receiver, no parity, 8 bit char: */
                               /* RE=1: Receiver enabled */
                               /* TE=1: Transmitter enabled */
                               /* PE,PT=0: No hw parity generation or checking */
                               /* M7,M,R8T9,R9T8=0: 8-bit data characters*/
                               /* DOZEEN=0: LPUART enabled in Doze mode */
                               /* ORIE,NEIE,FEIE,PEIE,TIE,TCIE,RIE,ILIE,MA1IE,MA2IE=0: no IRQ*/
                               /* TxDIR=0: TxD pin is input if in single-wire mode */
                               /* TXINV=0: TRansmit data not inverted */
                               /* RWU,WAKE=0: normal operation; rcvr not in statndby */
                               /* IDLCFG=0: one idle character */
                               /* ILT=0: Idle char bit count starts after start bit */
                               /* SBK=0: Normal transmitter operation - no break char */
                               /* LOOPS,RSRC=0: no loop back */
}


void uart0_deinit(void)
{
	#if 1
	while((LPUART0->STAT & LPUART_STAT_TDRE_MASK)>>LPUART_STAT_TDRE_SHIFT==0){};
	INT_SYS_DisableIRQ(s_lpuartRxTxIrqId[0]);
	LPUART0->CTRL=0x00000000;
	LPUART0->BAUD = 0x00000000; 
	#else
	UART_reset(0);
	#endif
}

void uart1_deinit(void)
{
	#if 1
	while((LPUART1->STAT & LPUART_STAT_TDRE_MASK)>>LPUART_STAT_TDRE_SHIFT==0){};
	INT_SYS_DisableIRQ(s_lpuartRxTxIrqId[1]);
	LPUART1->CTRL=0x00000000;
	LPUART1->BAUD = 0x00000000; 
	#else
	UART_reset(1);
	#endif
}

static status_t LPUART_SetBaudRate(uint32_t instance, uint32_t desiredBaudRate)
{
    DEV_ASSERT(instance < LPUART_INSTANCE_COUNT);

    uint16_t sbr, sbrTemp, i;
    uint32_t osr, tempDiff, calculatedBaud, baudDiff;
    uint32_t lpuartSourceClock;
//    clock_names_t instanceClkName = s_lpuartClkNames[instance];
    LPUART_Type * base = s_lpuartBase[instance];

    /* Get the LPUART clock as configured in the clock manager */
    //(void)CLOCK_SYS_GetFreq(instanceClkName, &lpuartSourceClock);
    lpuartSourceClock = SOSC_FREQ;

    /* Check if current instance is clock gated off. */
    DEV_ASSERT(lpuartSourceClock > 0U);
    /* Check if the desired baud rate can be configured with the current protocol clock. */
    DEV_ASSERT(lpuartSourceClock >= (desiredBaudRate * 4U));

    /* This lpuart instantiation uses a slightly different baud rate calculation
     * The idea is to use the best OSR (over-sampling rate) possible
     * Note, osr is typically hard-set to 16 in other lpuart instantiations
     * First calculate the baud rate using the minimum OSR possible (4) */
    osr = 4;
    sbr = (uint16_t)(lpuartSourceClock / (desiredBaudRate * osr));
    calculatedBaud = (lpuartSourceClock / (osr * sbr));

    if (calculatedBaud > desiredBaudRate)
    {
        baudDiff = calculatedBaud - desiredBaudRate;
    }
    else
    {
        baudDiff = desiredBaudRate - calculatedBaud;
    }

    /* loop to find the best osr value possible, one that generates minimum baudDiff
     * iterate through the rest of the supported values of osr */
    for (i = 5U; i <= 32U; i++)
    {
        /* calculate the temporary sbr value   */
        sbrTemp = (uint16_t)(lpuartSourceClock / (desiredBaudRate * i));
        /* calculate the baud rate based on the temporary osr and sbr values */
        calculatedBaud = (lpuartSourceClock / (i * sbrTemp));

        if (calculatedBaud > desiredBaudRate)
        {
            tempDiff = calculatedBaud - desiredBaudRate;
        }
        else
        {
            tempDiff = desiredBaudRate - calculatedBaud;
        }

        if (tempDiff <= baudDiff)
        {
            baudDiff = tempDiff;
            osr = i;  /* update and store the best osr value calculated */
            sbr = sbrTemp;  /* update store the best sbr value calculated */
        }
    }

    /* Check if osr is between 4x and 7x oversampling.
     * If so, then "BOTHEDGE" sampling must be turned on */
    if ((osr > 3U) && (osr < 8U))
    {
        LPUART_SetBothEdgeSamplingCmd(base, true);
    }

    /* program the osr value (bit value is one less than actual value) */
    LPUART_SetOversamplingRatio(base, (osr - 1U));

    /* write the sbr value to the BAUD registers */
    LPUART_SetBaudRateDivisor(base, sbr);

    return STATUS_SUCCESS;
}

static void UART0_TXRX_IRQ_Handle(void)
{
//	uint8 c=0;
	/* Handle receive data full interrupt */
	uint16 data = 0;
	uint32 tmp = 0;

	/* Handle receive data full interrupt */
    if ((LPUART0->CTRL & LPUART_CTRL_RIE_MASK) == LPUART_CTRL_RIE_MASK)		// check
    {
        if ((LPUART0->STAT & LPUART_STAT_RDRF_MASK) == LPUART_STAT_RDRF_MASK)	// internal receiver buffer full
        {
			tmp = (LPUART0->STAT >> LPUART_STAT_RDRF_SHIFT);
			data = (uint8)LPUART0->DATA;
			push_que(data);
        }
    }

	if(((LPUART0->CTRL & LPUART_CTRL_TIE_MASK) == LPUART_CTRL_TIE_MASK)
		&& ((LPUART0->STAT & LPUART_STAT_TDRE_MASK) == LPUART_STAT_TDRE_MASK))
    {
			if(UART_Packet_Info.ptr < UART_Packet_Info.buf_end)
			{
				UART_Packet_Info.temp = uart0_tx_test_buf[UART_Packet_Info.ptr];
				LPUART0->DATA = UART_Packet_Info.temp;
				UART_Packet_Info.ptr++;
			}
			else
			{
				UART_Packet_Info.State = 3;
				UART_Packet_Info.buf_end = 0;
				UART_Packet_Info.ptr = 0;
				CLEAR_BIT(LPUART0->CTRL, LPUART_CTRL_TIE_SHIFT);
			}
    }

    /* Handle receive overrun interrupt */
    if ((LPUART0->STAT & LPUART_STAT_OR_MASK) == LPUART_STAT_OR_MASK)
    {
        //lpuartState->receiveStatus = STATUS_UART_RX_OVERRUN;
        /* Clear the flag, OR the rxDataRegFull will not be set any more */
        //(void)LPUART_ClearStatusFlag(pBase, LPUART_RX_OVERRUN);
        LPUART0->STAT = (LPUART0->STAT & (~FEATURE_LPUART_STAT_REG_FLAGS_MASK)) | LPUART_STAT_OR_MASK;
    }

#if 0
	if ((LPUART0->CTRL & LPUART_CTRL_RIE_MASK) == LPUART_CTRL_RIE_MASK)
    {
        if ((LPUART0->STAT & LPUART_STAT_RDRF_MASK) == LPUART_STAT_RDRF_MASK)
        {
        	
			#ifdef NEW_URAT_RX0
			q_en(&uart0_rx_q, (uint8)LPUART0->DATA);
			#else
			push_que((uint8)LPUART0->DATA);
			#endif
        }
	}

    /* Handle receive overrun interrupt */
    if ((LPUART0->STAT & LPUART_STAT_OR_MASK) == LPUART_STAT_OR_MASK)
    {
        //lpuartState->receiveStatus = STATUS_UART_RX_OVERRUN;
        /* Clear the flag, OR the rxDataRegFull will not be set any more */
        //(void)LPUART_ClearStatusFlag(pBase, LPUART_RX_OVERRUN);
        LPUART0->STAT = (LPUART0->STAT & (~FEATURE_LPUART_STAT_REG_FLAGS_MASK)) | LPUART_STAT_OR_MASK;
    }
#endif
}

static void UART1_TXRX_IRQ_Handle(void)
{

/* Handle receive data full interrupt */
	//if (LPUART_GetIntMode(pBase, LPUART_INT_RX_DATA_REG_FULL))
	if ((LPUART1->CTRL & LPUART_CTRL_RIE_MASK) == LPUART_CTRL_RIE_MASK)
	{
			//if (LPUART_GetStatusFlag(pBase, LPUART_RX_DATA_REG_FULL))
			if ((LPUART1->STAT & LPUART_STAT_RDRF_MASK) == LPUART_STAT_RDRF_MASK)
			{
				#ifdef NEW_URAT_RX1
				q_en(&uart1_rx_q, (uint8)LPUART1->DATA);
				#else
				usart1_dev.rx_buf[usart1_dev.rx_buf_wr] =(uint8)LPUART1->DATA;	
				usart1_dev.rx_buf_cnt++;
				usart1_dev.rx_buf_wr++;
				usart1_dev.rx_buf_wr &= (MAX_USART_RX_BUF - 1);
//				usart1_dev.rx_buf_cnt1++;
				#endif
			}
	}

	/* Handle receive overrun interrupt */
	//if (LPUART_GetStatusFlag(pBase, LPUART_RX_OVERRUN))
	if ((LPUART1->STAT & LPUART_STAT_OR_MASK) == LPUART_STAT_OR_MASK)
	{
			//lpuartState->receiveStatus = STATUS_UART_RX_OVERRUN;
			/* Clear the flag, OR the rxDataRegFull will not be set any more */
			//(void)LPUART_ClearStatusFlag(pBase, LPUART_RX_OVERRUN);
			LPUART1->STAT = (LPUART1->STAT & (~FEATURE_LPUART_STAT_REG_FLAGS_MASK)) | LPUART_STAT_OR_MASK;
	}

}

#else
//START_FUNCTION_DECLARATION_RAMSECTION
static void UART0_TXRX_IRQ_Handle(void);
//END_FUNCTION_DECLARATION_RAMSECTION

//START_FUNCTION_DECLARATION_RAMSECTION
static void UART1_TXRX_IRQ_Handle(void);
//END_FUNCTION_DECLARATION_RAMSECTION


static status_t LPUART_SetBaudRate(uint32_t instance, uint32_t desiredBaudRate);

void (*pUART_IRQ_Handle[LPUART_INSTANCE_COUNT])(void) = {UART0_TXRX_IRQ_Handle, UART1_TXRX_IRQ_Handle};

static LPUART_Type * const s_lpuartBase[LPUART_INSTANCE_COUNT] = LPUART_BASE_PTRS;
/* Table to save LPUART enum numbers defined in CMSIS files. */
static const IRQn_Type s_lpuartRxTxIrqId[LPUART_INSTANCE_COUNT] = LPUART_RX_TX_IRQS;


void init_dev_uart(void)
{
	uart0_init(115200);
	uart1_init(115200);
	init_uart0();
	init_uart1();
}

static void uart0_init(uint32_t BaudRate)
{
	PCC->PCCn[PCC_LPUART0_INDEX] &= ~PCC_PCCn_CGC_MASK;    /* Ensure clk disabled for config */
	PCC->PCCn[PCC_LPUART0_INDEX] |= PCC_PCCn_PCS(1)|  PCC_PCCn_CGC_MASK;    /* Clock Src= 1 (SOSCDIV2_CLK) */
	//0x0400000E->0x04000008

#if 0
	LPUART0->GLOBAL=0x00000002;//reset
	LPUART0->GLOBAL=0x00000000;//on
#endif
	LPUART0->BAUD = ((uint32_t)((FEATURE_LPUART_DEFAULT_OSR << LPUART_BAUD_OSR_SHIFT) | \
                 (FEATURE_LPUART_DEFAULT_SBR << LPUART_BAUD_SBR_SHIFT)));
    /* Clear the error/interrupt flags */
    LPUART0->STAT = FEATURE_LPUART_STAT_REG_FLAGS_MASK;
    /* Reset all features/interrupts by default */
    LPUART0->CTRL = 0x00000000u;
    /* Reset match addresses */
    LPUART0->MATCH = 0x00000000u;
    /* Reset IrDA modem features */
    LPUART0->MODIR = 0x00000000u;
    /* Reset FIFO feature */
	LPUART0->FIFO = FEATURE_LPUART_FIFO_RESET_MASK|0xCE;	//TX FIFO 32 words		RX FIFO 128 words
    /* Reset FIFO Watermark values */
    LPUART0->WATER = 0x00000000u;
	LPUART_SetBaudRate(0, 115200);
//	LPUART_SetBaudRate(0, 230400);// 230400

//	LPUART0->CTRL |= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK);
		/* Install LPUART irq handler */
  
//	INT_SYS_SetPriority( s_lpuartRxTxIrqId[0], 1 );
	LPUART0->CTRL|= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK|LPUART_CTRL_ORIE_MASK | LPUART_CTRL_RIE_MASK);
															 /* Enable transmitter & receiver, no parity, 8 bit char: */
                               /* RE=1: Receiver enabled */
                               /* TE=1: Transmitter enabled */
                               /* PE,PT=0: No hw parity generation or checking */
                               /* M7,M,R8T9,R9T8=0: 8-bit data characters*/
                               /* DOZEEN=0: LPUART enabled in Doze mode */
                               /* ORIE,NEIE,FEIE,PEIE,TIE,TCIE,RIE,ILIE,MA1IE,MA2IE=0: no IRQ*/
                               /* TxDIR=0: TxD pin is input if in single-wire mode */
                               /* TXINV=0: TRansmit data not inverted */
                               /* RWU,WAKE=0: normal operation; rcvr not in statndby */
                               /* IDLCFG=0: one idle character */
                               /* ILT=0: Idle char bit count starts after start bit */
                               /* SBK=0: Normal transmitter operation - no break char */
                               /* LOOPS,RSRC=0: no loop back */

	 INT_SYS_InstallHandler(s_lpuartRxTxIrqId[0], pUART_IRQ_Handle[0], (isr_t*) 0);
	 
	 /* Enable LPUART interrupt. */
	 INT_SYS_EnableIRQ(s_lpuartRxTxIrqId[0]);

}

static void uart1_init(uint32_t BaudRate)  /* Init. summary: 9600 baud, 1 stop bit, 8 bit format, no parity */
{
	uint32_t temp=0;

	PCC->PCCn[PCC_LPUART1_INDEX] &= ~PCC_PCCn_CGC_MASK;    /* Ensure clk disabled for config */
	PCC->PCCn[PCC_LPUART1_INDEX] |= PCC_PCCn_PCS(1)   //PCC_PCCn_PCS(0b001)   /* Clock Src= 1 (SOSCDIV2_CLK) */
	                             |  PCC_PCCn_CGC_MASK;     /* Enable clock for LPUART1 regs */
#if 0
	LPUART1->GLOBAL=0x00000002;//reset
	LPUART1->GLOBAL=0x00000000;//on
#endif
	LPUART1->BAUD = ((uint32_t)((FEATURE_LPUART_DEFAULT_OSR << LPUART_BAUD_OSR_SHIFT) | \
	             (FEATURE_LPUART_DEFAULT_SBR << LPUART_BAUD_SBR_SHIFT)));
	/* Clear the error/interrupt flags */
	LPUART1->STAT = FEATURE_LPUART_STAT_REG_FLAGS_MASK;
	/* Reset all features/interrupts by default */
	LPUART1->CTRL = 0x00000000u;
	/* Reset match addresses */
	LPUART1->MATCH = 0x00000000u;
	/* Reset IrDA modem features */
	LPUART1->MODIR = 0x00000000u;
	/* Reset FIFO feature */
	LPUART1->FIFO = FEATURE_LPUART_FIFO_RESET_MASK|0xCC;	//TX FIFO 32 words		RX FIFO 32 words
	/* Reset FIFO Watermark values */
	LPUART1->WATER = 0x00000000u;
	LPUART_SetBaudRate(1, 115200);// 230400
	//	LPUART_SetBaudRate(1, 115200);// 230400
//	LPUART_SetBaudRate(1, 230400);// 230400
//	LPUART_SetBaudRate(1, 460800);// 230400

//	LPUART1->CTRL |= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK);


	LPUART1->CTRL|= (LPUART_CTRL_TE_MASK | LPUART_CTRL_RE_MASK|LPUART_CTRL_ORIE_MASK | LPUART_CTRL_RIE_MASK);
															 /* Enable transmitter & receiver, no parity, 8 bit char: */
                       /* RE=1: Receiver enabled */
                               /* TE=1: Transmitter enabled */
                               /* PE,PT=0: No hw parity generation or checking */
                               /* M7,M,R8T9,R9T8=0: 8-bit data characters*/
                               /* DOZEEN=0: LPUART enabled in Doze mode */
                               /* ORIE,NEIE,FEIE,PEIE,TIE,TCIE,RIE,ILIE,MA1IE,MA2IE=0: no IRQ*/
                               /* TxDIR=0: TxD pin is input if in single-wire mode */
                               /* TXINV=0: TRansmit data not inverted */
                               /* RWU,WAKE=0: normal operation; rcvr not in statndby */
                               /* IDLCFG=0: one idle character */
                               /* ILT=0: Idle char bit count starts after start bit */
                               /* SBK=0: Normal transmitter operation - no break char */
                               /* LOOPS,RSRC=0: no loop back */
	 INT_SYS_InstallHandler(s_lpuartRxTxIrqId[1], pUART_IRQ_Handle[1], (isr_t*) 0);
	 
	 /* Enable LPUART interrupt. */
	 INT_SYS_EnableIRQ(s_lpuartRxTxIrqId[1]);
	 INT_SYS_SetPriority( s_lpuartRxTxIrqId[1], 1 );


}

static void uart2_init(uint32_t BaudRate)
{
  PCC->PCCn[PCC_LPUART2_INDEX] &= ~PCC_PCCn_CGC_MASK;    /* Ensure clk disabled for config */
  PCC->PCCn[PCC_LPUART2_INDEX] |= PCC_PCCn_PCS(1)|  PCC_PCCn_CGC_MASK;    /* Clock Src= 1 (SOSCDIV2_CLK) */

  LPUART2->GLOBAL=0x00000002;//reset
  LPUART2->GLOBAL=0x00000000;//on

  LPUART2->BAUD = 0x0400000E;  /* Initialize for 115200 baud, 1 stop: */
                               /* SBR=52 (0x34): baud divisor = 8M/9600/16 = ~52 */
                               /* OSR=15: Over sampling ratio = 15+1=16 */
                               /* SBNS=0: One stop bit */
                               /* BOTHEDGE=0: receiver samples only on rising edge */
                               /* M10=0: Rx and Tx use 7 to 9 bit data characters */
                               /* RESYNCDIS=0: Resync during rec'd data word supported */
                               /* LBKDIE, RXEDGIE=0: interrupts disable */
                               /* TDMAE, RDMAE, TDMAE=0: DMA requests disabled */
                               /* MAEN1, MAEN2,  MATCFG=0: Match disabled */

  LPUART2->CTRL=0x002C0000;    /* Enable transmitter & receiver, no parity, 8 bit char: */
                               /* RE=1: Receiver enabled */
                               /* TE=1: Transmitter enabled */
                               /* PE,PT=0: No hw parity generation or checking */
                               /* M7,M,R8T9,R9T8=0: 8-bit data characters*/
                               /* DOZEEN=0: LPUART enabled in Doze mode */
                               /* ORIE,NEIE,FEIE,PEIE,TIE,TCIE,RIE,ILIE,MA1IE,MA2IE=0: no IRQ*/
                               /* TxDIR=0: TxD pin is input if in single-wire mode */
                               /* TXINV=0: TRansmit data not inverted */
                               /* RWU,WAKE=0: normal operation; rcvr not in statndby */
                               /* IDLCFG=0: one idle character */
                               /* ILT=0: Idle char bit count starts after start bit */
                               /* SBK=0: Normal transmitter operation - no break char */
                               /* LOOPS,RSRC=0: no loop back */
}


void uart0_deinit(void)
{
	INT_SYS_DisableIRQ(s_lpuartRxTxIrqId[0]);
	LPUART0->CTRL=0x00000000;
	LPUART0->BAUD = 0x00000000; 
}

void uart1_deinit(void)
{
	INT_SYS_DisableIRQ(s_lpuartRxTxIrqId[1]);
	LPUART1->CTRL=0x00000000;
	LPUART1->BAUD = 0x00000000; 
}

static status_t LPUART_SetBaudRate(uint32_t instance, uint32_t desiredBaudRate)
{
    DEV_ASSERT(instance < LPUART_INSTANCE_COUNT);

    uint16_t sbr, sbrTemp, i;
    uint32_t osr, tempDiff, calculatedBaud, baudDiff;
    uint32_t lpuartSourceClock;
//    clock_names_t instanceClkName = s_lpuartClkNames[instance];
    LPUART_Type * base = s_lpuartBase[instance];

    /* Get the LPUART clock as configured in the clock manager */
    //(void)CLOCK_SYS_GetFreq(instanceClkName, &lpuartSourceClock);
    lpuartSourceClock = SOSC_FREQ;

    /* Check if current instance is clock gated off. */
    DEV_ASSERT(lpuartSourceClock > 0U);
    /* Check if the desired baud rate can be configured with the current protocol clock. */
    DEV_ASSERT(lpuartSourceClock >= (desiredBaudRate * 4U));

    /* This lpuart instantiation uses a slightly different baud rate calculation
     * The idea is to use the best OSR (over-sampling rate) possible
     * Note, osr is typically hard-set to 16 in other lpuart instantiations
     * First calculate the baud rate using the minimum OSR possible (4) */
    osr = 4;
    sbr = (uint16_t)(lpuartSourceClock / (desiredBaudRate * osr));
    calculatedBaud = (lpuartSourceClock / (osr * sbr));

    if (calculatedBaud > desiredBaudRate)
    {
        baudDiff = calculatedBaud - desiredBaudRate;
    }
    else
    {
        baudDiff = desiredBaudRate - calculatedBaud;
    }

    /* loop to find the best osr value possible, one that generates minimum baudDiff
     * iterate through the rest of the supported values of osr */
    for (i = 5U; i <= 32U; i++)
    {
        /* calculate the temporary sbr value   */
        sbrTemp = (uint16_t)(lpuartSourceClock / (desiredBaudRate * i));
        /* calculate the baud rate based on the temporary osr and sbr values */
        calculatedBaud = (lpuartSourceClock / (i * sbrTemp));

        if (calculatedBaud > desiredBaudRate)
        {
            tempDiff = calculatedBaud - desiredBaudRate;
        }
        else
        {
            tempDiff = desiredBaudRate - calculatedBaud;
        }

        if (tempDiff <= baudDiff)
        {
            baudDiff = tempDiff;
            osr = i;  /* update and store the best osr value calculated */
            sbr = sbrTemp;  /* update store the best sbr value calculated */
        }
    }

    /* Check if osr is between 4x and 7x oversampling.
     * If so, then "BOTHEDGE" sampling must be turned on */
    if ((osr > 3U) && (osr < 8U))
    {
        LPUART_SetBothEdgeSamplingCmd(base, true);
    }

    /* program the osr value (bit value is one less than actual value) */
    LPUART_SetOversamplingRatio(base, (osr - 1U));

    /* write the sbr value to the BAUD registers */
    LPUART_SetBaudRateDivisor(base, sbr);

    return STATUS_SUCCESS;
}

static void UART0_TXRX_IRQ_Handle(void)
{
//	uint8 c=0;
	/* Handle receive data full interrupt */
    if ((LPUART0->CTRL & LPUART_CTRL_RIE_MASK) == LPUART_CTRL_RIE_MASK)
    {
        if ((LPUART0->STAT & LPUART_STAT_RDRF_MASK) == LPUART_STAT_RDRF_MASK)
        {
        	
			#ifdef NEW_URAT_RX0
//			c =(uint8)LPUART0->DATA;
			q_en(&uart0_rx_q, (uint8)LPUART0->DATA);
			#else
			push_que((uint8)LPUART0->DATA);
			#endif
        }
	}

	#ifndef POLLING_URAT_TX0
	if ((LPUART0->CTRL & LPUART_CTRL_TIE_MASK) == LPUART_CTRL_TIE_MASK)
    {
        //if (LPUART_GetStatusFlag(pBase, LPUART_TX_DATA_REG_EMPTY))
        if ((LPUART0->STAT & LPUART_STAT_TDRE_MASK) == LPUART_STAT_TDRE_MASK)
        {
        	uint8_t d;
        	
        	if (q_data_size(&uart0_tx_q))
        	{
//        		volatile uint8_t * dataRegBytes = (volatile uint8_t *)(&(pBase->DATA));
//			    dataRegBytes[0] = Getdata[0];
				d = q_de(&uart0_tx_q) ;
//				LPUART_Putchar(d);
			    LPUART0->DATA = d;
        	}
        	else
        	{
        		/* Disable transmission complete interrupt */
			    //LPUART_SetIntMode(pBase, LPUART_INT_TX_DATA_REG_EMPTY, false);
//			    if((LPUART0->STAT & LPUART_STAT_TC_MASK)==LPUART_STAT_TC_MASK)
			    	CLEAR_BIT(LPUART0->CTRL, LPUART_CTRL_TIE_SHIFT);
        	}
        }
    }
	#endif

    /* Handle receive overrun interrupt */
    if ((LPUART0->STAT & LPUART_STAT_OR_MASK) == LPUART_STAT_OR_MASK)
    {
        //lpuartState->receiveStatus = STATUS_UART_RX_OVERRUN;
        /* Clear the flag, OR the rxDataRegFull will not be set any more */
        //(void)LPUART_ClearStatusFlag(pBase, LPUART_RX_OVERRUN);
        LPUART0->STAT = (LPUART0->STAT & (~FEATURE_LPUART_STAT_REG_FLAGS_MASK)) | LPUART_STAT_OR_MASK;
    }
}

static void UART1_TXRX_IRQ_Handle(void)
{

/* Handle receive data full interrupt */
	//if (LPUART_GetIntMode(pBase, LPUART_INT_RX_DATA_REG_FULL))
	if ((LPUART1->CTRL & LPUART_CTRL_RIE_MASK) == LPUART_CTRL_RIE_MASK)
	{
			//if (LPUART_GetStatusFlag(pBase, LPUART_RX_DATA_REG_FULL))
			if ((LPUART1->STAT & LPUART_STAT_RDRF_MASK) == LPUART_STAT_RDRF_MASK)
			{
				#ifdef NEW_URAT_RX1
				q_en(&uart1_rx_q, (uint8)LPUART1->DATA);
				#else
				usart1_dev.rx_buf[usart1_dev.rx_buf_wr] =(uint8)LPUART1->DATA;	
				usart1_dev.rx_buf_cnt++;
				usart1_dev.rx_buf_wr++;
				usart1_dev.rx_buf_wr &= (MAX_USART_RX_BUF - 1);
//				usart1_dev.rx_buf_cnt1++;
				#endif
			}
	}
	#ifndef POLLING_URAT_TX1
	 if ((LPUART1->CTRL & LPUART_CTRL_TIE_MASK) == LPUART_CTRL_TIE_MASK)
    {
        //if (LPUART_GetStatusFlag(pBase, LPUART_TX_DATA_REG_EMPTY))
        if ((LPUART1->STAT & LPUART_STAT_TDRE_MASK) == LPUART_STAT_TDRE_MASK)
        {
        	char d;
        	
        	if (q_data_size(&uart1_tx_q))
        	{
//        		volatile uint8_t * dataRegBytes = (volatile uint8_t *)(&(pBase->DATA));
//			    dataRegBytes[0] = Getdata[0];
				d = q_de(&uart1_tx_q) ;

			    LPUART1->DATA = d; 
        	}
        	else
        	{
        		/* Disable transmission complete interrupt */
			    //LPUART_SetIntMode(pBase, LPUART_INT_TX_DATA_REG_EMPTY, false);
			    CLEAR_BIT(LPUART1->CTRL, LPUART_CTRL_TIE_SHIFT);
        	}
        }
    }
	 #endif

	/* Handle receive overrun interrupt */
	//if (LPUART_GetStatusFlag(pBase, LPUART_RX_OVERRUN))
	if ((LPUART1->STAT & LPUART_STAT_OR_MASK) == LPUART_STAT_OR_MASK)
	{
			//lpuartState->receiveStatus = STATUS_UART_RX_OVERRUN;
			/* Clear the flag, OR the rxDataRegFull will not be set any more */
			//(void)LPUART_ClearStatusFlag(pBase, LPUART_RX_OVERRUN);
			LPUART1->STAT = (LPUART1->STAT & (~FEATURE_LPUART_STAT_REG_FLAGS_MASK)) | LPUART_STAT_OR_MASK;
	}
}

void UART_reset(uint32_t uart_num)
{
	if (uart_num < LPUART_INSTANCE_COUNT) 
	{
		/* Wait until the data is completely shifted out of shift register */
	    while (!LPUART_GetStatusFlag(s_lpuartBase[uart_num], LPUART_TX_COMPLETE)) {}

    	/* Disable LPUART interrupt. */
	    INT_SYS_DisableIRQ(s_lpuartRxTxIrqId[uart_num]);

		/* Set to after reset state an disable UART Tx/Rx */
		s_lpuartBase[uart_num]->CTRL = 0x00000000u;
		s_lpuartBase[uart_num]->BAUD = 0x00000000u;

		/* Disable clock to UART */
		//pPccBase->PCCn[(uint32_t)PCC_LPUART0_INDEX+uart_num] &= ~PCC_PCCn_CGC_MASK;     /* Enable clock for LPUARTx regs */
	}
}

#endif

