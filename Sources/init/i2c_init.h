/*
 * i2c_init.h
 *
 *  Created on: 2018. 7. 23.
 *      Author: Digen
 */

#ifndef INIT_I2C_INIT_H_
#define INIT_I2C_INIT_H_

#define TUNER_CHIP_ADDR	(0xC8)
#define AMP_CHIP_ADDR	(0xD8)


#define I2C_OK 						(0x00)
#define I2C_BUSY					((BYTE)(0x01))
#define I2C_ERROR 				(0x02)
#define I2C_COMPLETE 			(0x08)


enum{
	HWI2C_IDLE=0,
	HWI2C_STEP1,
	HWI2C_STEP2,
	HWI2C_STEP3,
	HWI2C_STEP4,
	HWI2C_STEP5
};


#define BUSY_TIMEOUT        1000
#define READING_TIMEOUT    5000 // 2000
#define STOP_TIMEOUT        3000




/*******************************************************************************
* Function prototypes
*******************************************************************************/

void i2c_init(void);
void i2c_deinit(void);
uint8 i2c_check_timeout(uint8 limit);
uint8 i2c_check_busfree(void);

//TEF6686 
uint8 i2c_tef6686_step_write(uint32 subaddr, uint8 *src, uint8 cnt);
uint8 i2c_tef6686_step_read(uint32 subaddr, uint8 *des, uint8 cnt);
uint8 tuner_i2c_write(uint32 subaddr, uint8 *src, uint8 cnt);
uint8 tuner_i2c_read(uint32 subaddr, uint8 *des, uint8 cnt);


#endif /* INIT_I2C_INIT_H_ */
