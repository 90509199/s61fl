/*
 * sys_init.c
 *
 *  Created on: 2018. 5. 17.
 *      Author: Digen
 */


#include "includes.h"
#include "SystemScheduler.h"

void sys_init_fnc (void)
{
/* ----------------------------------------------------------- */
/*	        Initialize the System Clock, Mode Entry (ME) & CMU             */
/* ----------------------------------------------------------- */
   sysclk_module_init_fnc();

/* ----------------------------------------------------------- */
/*	        Initialize the System Integration Unit (SIU)             */
/* ----------------------------------------------------------- */
	siu_init_fnc();

/* ----------------------------------------------------------- */
/*	        Initialize the Nested Vector Interrupt Controller (NVIC)             */
/* ----------------------------------------------------------- */
//	nvic_init_fnc();

/* ----------------------------------------------------------- */
/*	        LPIT setting (init_LPIT0)             */
/* ----------------------------------------------------------- */
	init_LPIT0();

/* ----------------------------------------------------------- */
/*	        Initialize the I2C             */
/* ----------------------------------------------------------- */
	i2c_init();
 	flexio_i2c_init();

/* ----------------------------------------------------------- */
/*	        Initialize the SPI             */
/* ----------------------------------------------------------- */
	//#ifndef DSP_BOOTING_SEQUENCE
	spi0_deinit();
	spi0_init();
	//#endif
	spi1_deinit();
	spi1_init();
	#if 0 //def SI_LABS_TUNER
	spi2_deinit();
	spi2_init();
	#endif
/* ----------------------------------------------------------- */
/*	        Initialize the UART             */
/* ----------------------------------------------------------- */
	init_dev_uart();

/* ----------------------------------------------------------- */
/*	        Initialize the ADC             */
/* ----------------------------------------------------------- */
	ADC_Init();

/* ----------------------------------------------------------- */
/*	        Initialize the PWM             */
/* ----------------------------------------------------------- */
#ifndef CHANG_BL_TIMING
	PWM_init();
#endif

/* ----------------------------------------------------------- */
/*	        Initialize the FlexCAN             */
/* ----------------------------------------------------------- */
	SYS_INIT();	//CAN
	MCU_CAN0_EN(ON);
	MCU_CAN0_STB(ON);

	/* wl20220815 add main 5v */
	M_5V_EN(ON);

/* ----------------------------------------------------------- */
/*	        APP Key             */
/* ----------------------------------------------------------- */
	Init_TimerDrv();
	ADCkey_Enable(ON);
	InitDrvKey();

	#ifdef SHELL_MODE
	InitDrvShell();
  	#endif
}

void sys_deinit_fnc (void)
{
	uart0_deinit();
	uart1_deinit();
	Disable_ADC0_Ch();
	Disable_ADC1_Ch();
	spi0_deinit();
	spi1_deinit();
	#if 0 //def SI_LABS_TUNER
	spi2_deinit();
	#endif
	i2c_deinit();
	EDMA_DRV_Deinit();					//not use
	flexio_i2c_deinit();
	deinit_LPIT0();
	siu_deinit_fnc();
}
