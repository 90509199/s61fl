 /*
 * spi_init.c
 *
 *  Created on: 2018. 7. 23.
 *      Author: Digen
 */

#include "includes.h"
#include "Cpu.h"


//SPI mode0 : CPOL=0, CPHA=0     clock's default is low data / check is first rising edge 
//SPI mode1 : CPOL=0, CPHA=1		clock's default is low data / check is first falling edge
//SPI mode2 : CPOL=1, CPHA=0		clock's default is high data / check is first falling edge 
//SPI mode3 : CPOL=1, CPHA=1		clock's default is high data / check is first rising edge

void spi0_init(void){
	LPSPI_DRV_MasterInit(LPSPICOM1,&lpspiCom1State,&lpspiCom1_MasterConfig0);
	LPSPI_DRV_MasterSetDelay(LPSPICOM1,10,10,10);
}

void spi1_init(void)
{	
	LPSPI_DRV_MasterInit(LPSPICOM2,&lpspiCom2State,&lpspiCom2_MasterConfig0);
	LPSPI_DRV_MasterSetDelay(LPSPICOM2,10,10,10);
}
#if 0 //def SI_LABS_TUNER
void spi2_init(void)
{	
	LPSPI_DRV_MasterInit(LPSPICOM3,&lpspiCom3State,&lpspiCom3_MasterConfig0);
	LPSPI_DRV_MasterSetDelay(LPSPICOM3,10,10,10);
}
#endif
void spi0_deinit(void)
{
	LPSPI_DRV_MasterDeinit(LPSPICOM1);
}

void spi1_deinit(void)
{
	LPSPI_DRV_MasterDeinit(LPSPICOM2);
}
#if 0 //def SI_LABS_TUNER
void spi2_deinit(void)
{	
	LPSPI_DRV_MasterDeinit(LPSPICOM3);
}
#endif

#ifdef SW_SPI

/*	GPIO ASSIGNMENTS
 *
 *	-----------------------------------------
 *  	Pin#		Pin_Name  		SPI_Name	<<direction>>
 *  -----------------------------------------
 *		12		PTA8				MOSI				OUT
 *		15		PTE16				MISO				IN
 *		13 		PTE15				CLK					OUT
 *		11		PTA9				CS					OUT
 *
 */	
 
//SW SPI

/* CS, SPI CHIP_SELECT */
/* Macro to make the SPI Slave select pin to HIGH state */
#define SPI_CS_HIGH()					//PTA->PSOR |=(1<<9);	
/* Macro to make the SPI Slave select pin to LOW state */
#define SPI_CS_LOW()					//PTE->PCOR |=(1<<9);	


/* CLK, SPI CLOCK */
#define SPI_CLK_SET()					//PTE->PSOR |=(1<<15);	
#define SPI_CLK_CLEAR()				//PTE->PCOR |=(1<<15);	
	
/* MOSI */
#define SPI_MOSI_SET()					//PTA->PSOR |=(1<<8);	
#define SPI_MOSI_CLEAR()			//PTA->PCOR |=(1<<8);	

/* MISO	*/
#define SPI_MISO_READ()				//(PTE->PDIR & (1<<15))


//Mode 3  CPHA=1 CPOL=1
void SPI_WriteByte(volatile unsigned char Dat) 
{
	int i;

	for(i=0;i<8;i++) 
	{
		SPI_CLK_CLEAR();
    	delay_us(1);
		if((Dat&0x80) == 0)
		{
			SPI_MOSI_CLEAR();
		}
		else
		{
			SPI_MOSI_SET();
		}
		Dat = Dat << 1;
		delay_us(1);
		SPI_CLK_SET();
    	delay_us(2);			//10->22	
			
	}
//	delay_us(110);//add			//100 is ok
}

unsigned char SPI_ReadByte(void)
{
	unsigned char Dat = 0;
	int i;
	for(i=0;i<8;i++) 
	{
		SPI_CLK_CLEAR();
    	delay_us(1);	
		Dat = Dat << 1;
		if(SPI_MISO_READ())  Dat |= 0x01;
		delay_us(1);	
		SPI_CLK_SET();
    	delay_us(2);	
	}
	return Dat;
}

// Send port data to tlc6c5816
int SPI_Write(unsigned char* srcptr, int length) 
{
	int i;

	delay_us(2);
	for(i = 0; i < length; i++) {
		SPI_WriteByte(srcptr[i]);
	}
	delay_us(2);
	
	return length; 
}

// Receive port data to tlc6c5816
void SPI_Read(unsigned char* srcptr, int length) 
{
	int i;
	
	delay_us(2);
	for(i = 0; i < length; i++) {
		srcptr[i] = SPI_ReadByte();
	}
	delay_us(2);

}

void SPI_SEL_LOW(void)
{
	SPI_CS_LOW();
	delay_us(10);
}

void SPI_SEL_HIGH(void)
{
	delay_us(10);
	SPI_CS_HIGH();
	delay_us(10);
}

void SPI_MOSI_HIGH(void)
{
	SPI_MOSI_SET()	;
}

void SPI_MOSI_LOW(void)
{
	SPI_MOSI_CLEAR();
}

	//PTE10 		GPIO O				//TELLTALE_EN
	//PTE14		GPIO O				//TELLTALE_CLR
	//PTE16		GPIO I				//TELLTALE_ERR 
// Initialize SPI ports 
void SPI_Init(void) 
{
	// Initialize SPI pins
	SPI_SEL_HIGH();
	SPI_CLK_SET()	;
	SPI_MOSI_SET();
	delay_us(10);	
}
#endif
