/*
 * beep_proc.c
 *
 *  Created on: 2018. 12. 31.
 *      Author: Digen
 */
#include "includes.h"

static volatile uint32 beep_time_tick = 0;
static volatile uint32 beep_output_time_tick = 0;

static void Beep_Freq_Type(DEVICE *dev, uint8 type);

void Beep_Proc(DEVICE *dev)
{
	uint8 tmp8=0;
	uint8 tmp8_1=0;
	uint8 tmp8_2=0;
	
	//start beep after dsp initialize
	if(dev->r_Dsp->d_boot_ok==OFF)
		return;

}


static void Beep_Freq_Type(DEVICE *dev, uint8 type)
{
	if((BEEP_1HZ_TIME<=GetElapsedTime(beep_time_tick))&&(type==BEEP_TYPE_1HZ))
	{
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_START,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_START);
		#endif
		beep_time_tick = GetTickCount();
	}

	if((BEEP_2HZ_TIME<=GetElapsedTime(beep_time_tick))&&(type==BEEP_TYPE_2HZ))
	{
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_START,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_START);
		#endif
		beep_time_tick = GetTickCount();
	}

	if((BEEP_4HZ_TIME<=GetElapsedTime(beep_time_tick))&&(type==BEEP_TYPE_4HZ))
	{
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_START,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_START);
		#endif
		beep_time_tick = GetTickCount();
	}

	if((BEEP_ON_TIME<=GetElapsedTime(beep_time_tick))&&(type==BEEP_TYPE_ON))
	{
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_START,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_START);
		#endif
		beep_time_tick = GetTickCount();
	}	
}


