/*
 * diag_proc.c
 *
 *  Created on: 2018. 12. 24.
 *      Author: Digen
 */

#include "includes.h" 
#include "Cpu.h"
#include "Diag/diag_dbi_if.h"

extern void DiagSetSessionDefault(void);
uint8  gDiagSessionDefaultByBussOffFlag = FALSE;
extern DiagIoCtrlCmd rDiagCmd;
static volatile uint32 diag_time_tick_1sec = 0;

void Diag_Task(DEVICE *dev)
{
	switch(rDiagCmd.diag_cmd)
	{
		case DIA_CMD_IDLE:
            break;
			
		case DIA_CMD_SPEAKER_FL_CTRL:
			if(rDiagCmd.data == 0x01)
			{
				//active
				dev->r_Beep->fl_beep =ON;
				dev->r_Beep->fr_beep =OFF;
				dev->r_Beep->rl_beep =OFF;
				dev->r_Beep->rr_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_DIAG_START,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_DIAG_START);
				#endif
			}
			else if(rDiagCmd.data == 0x02)
			{
				//inactive
				dev->r_Beep->fl_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_STOP,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_STOP);
				#endif
			}
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
		case DIA_CMD_SPEAKER_FR_CTRL:	
			if(rDiagCmd.data == 0x01)
			{
				//active
				dev->r_Beep->fl_beep =OFF;
				dev->r_Beep->fr_beep =ON;
				dev->r_Beep->rl_beep =OFF;
				dev->r_Beep->rr_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_DIAG_START,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_DIAG_START);
				#endif
			}
			else if(rDiagCmd.data==0x02)
			{
				//inactive
				dev->r_Beep->fr_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_STOP,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_STOP);
				#endif
			}
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
		#if 0
		case DIA_CMD_SPEAKER_RL_CTRL:	
			if(0)
			{
				//active
				dev->r_Beep->fl_beep =OFF;
				dev->r_Beep->fr_beep =OFF;
				dev->r_Beep->rl_beep =ON;
				dev->r_Beep->rr_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_DIAG_START,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_DIAG_START);
				#endif
			}
			else if(rDiagCmd.data==0x02)
			{
				//inactive
				dev->r_Beep->rl_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_STOP,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_STOP);
				#endif
			}
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
	 	case DIA_CMD_SPEAKER_RR_CTRL:
			if(0)
			{
				//active
				dev->r_Beep->fl_beep =OFF;
				dev->r_Beep->fr_beep =OFF;
				dev->r_Beep->rl_beep =OFF;
				dev->r_Beep->rr_beep =ON;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_DIAG_START,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_DIAG_START);
				#endif
			}
			else if(rDiagCmd.data==0x02)
			{
				//inactive
				dev->r_Beep->rr_beep =OFF;
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_BEEP_STOP,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_BEEP_STOP);
				#endif
			}
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
		
		case DIA_CMD_DISPLAY_ONOFF:
			if(0)
			{
				//on
				#ifdef B1_MOTHER_BOARD_SAMPLE
				MCU_HU_LCD_EN(OFF);	//port off
				#endif
				MCU_LCD1_2_EN_3P3(OFF);	//port off
			}
			else if(rDiagCmd.data==0x02)
			{
				//off
				#ifdef B1_MOTHER_BOARD_SAMPLE
				MCU_HU_LCD_EN(ON);	//port off
				#endif
				MCU_LCD1_2_EN_3P3(ON);	//port off
			}
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
		#endif
		case DIA_CMD_VOLUME_CTRL:
			#ifdef DSP_DOUBLE_QUE
			q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL,(uint8) dev->r_Dsp->d_audo_mode);
			#else
			q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);
			#endif
			e2p_save_data.E2P_MEDIA_VOL  = dev->r_Dsp->d_media_vol = rDiagCmd.data;
			e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
		case DIA_CMD_RADIO_SEEK:
			if(rDiagCmd.data==0)
			{
				//seek up
				q_en(&tef6686_cmd_q, (uint8)TUNER_SEEK_PLUS_0);
			}
			else if(rDiagCmd.data==1)
			{
				//seek dn
				q_en(&tef6686_cmd_q, (uint8)TUNER_SEEK_MINUS_0);
			}
			else if(rDiagCmd.data==3)
			{
				//seek all
				q_en(&tef6686_cmd_q, (uint8)TUNER_ATS_0);
			}			
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
		case DIA_CMD_RADIO_ANIMATION_ONOFF:
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;

		default:
			rDiagCmd.diag_cmd =DIA_CMD_IDLE;
			break;
	}
}

 

