#include "includes.h"
#include "Cpu.h"
#include "A2B_I2C_Commandlist.h"
#include "flexio_i2c_init.h"
#include "flexio_i2c_driver.h"
#include "debug.h"
#include "stdbool.h"
#include "pins_driver.h"
#include "flexio_common.h"

static volatile uint32 A2B_init_tick = 0;
static volatile uint32 a2b_recovery_tick = 0;

static volatile uint32 A2B_I2C_delay_tick = 0;

static volatile uint8_t A2B_read_buffer;
static volatile uint8_t A2B_Write_Buffer[2];
static int nIndex=0;

uint8_t A2B_ISR_Flag=0;

//#define LOOPBACK_TEST1
//#define LOOPBACK_SLAVE_TEST
//#define TESTMODE
#define A2B_POWER_ACT_LIMIT   //if define,then A2B power control times have limit.if no define, A2B power control times no limits.

ADI_A2B_DISCOVERY_CONFIG A2B_list[]={

		{0x68u,	WRITE,	0x12u,	0x84u},	/* CONTROL */
		{0x00u,	DELAY,	0x00u,	0x19u},	/* A2B_Delay */

		{0x68u,	WRITE,	0x1Bu,	0xFFu},	/* INTMSK0 *///77
		{0x68u,	WRITE,	0x1Cu,	0xFFu},	/* INTMSK1 *///78
		{0x68u,	WRITE,	0x1Du,	0x0Fu},	/* INTMSK2 */
		{0x68u,	WRITE,	0x0Fu,	0x9Eu},	/* RESPCYCS */
		{0x68u,	WRITE,	0x12u,	0x01u},	/* CONTROL */
		{0x68u,	WRITE,	0x41u,	0xA2u},	/* I2SGCFG */
		{0x68u,	WRITE,	0x09u,	0x01u},	/* SWCTL */
		{0x68u,	WRITE,	0x13u,	0x9Eu},	/* DISCVRY */
		{0x00u,	DELAY,	0x00u,	0x32u},	/* A2B_Delay */
		{0x68u,	WRITE,	0x1Au,	0x01u},	/* INTPND2 */
		{0x68u,	READ,	0x1Au,	0x00u},
		{0x68u,	WRITE,	0x01u,	0x00u},	/* NODEADR */
		{0x68u,	WRITE,	0x09u,	0x21u},	/* SWCTL */
		{0x68u,	WRITE,	0x01u,	0x00u},	/* NODEADR */
		{0x69u,	WRITE,	0x0Au,	0x00u},	/* BCDNSLOTS */ //Nothing passed to Side-B of Slave

#ifdef LOOPBACK_SLAVE_TEST
		{0x69u,	WRITE,	0x0Bu,	0x08u},	/* LDNSLOTS */ // Slots Master2Slave
		{0x69u,	WRITE,	0x0Cu,	0x08u},	/* LUPSLOTS */ // Slots Slave2Master
#else
		{0x69u, WRITE,	0x0Bu,	0x80u}, /* LDNSLOTS */ // Slots Master2Slave
		{0x69u, WRITE,	0x0Cu,	0x02u}, /* LUPSLOTS */ // Slots Slave2Master//0X02
#endif
		{0x69u,	WRITE,	0x3Fu,	0x00u},	/* I2CCFG */
		{0x69u,	WRITE,	0x41u,	0x42u}, /* I2SGCFG */
		{0x69u,	WRITE,	0x42u,	0x93u}, /* I2SCFG */
		{0x69u,	WRITE,	0x47u,	0x18u},	/* PDMCTL */
		{0x69u,	WRITE,	0x59u,	0x00u},	/* CLK1CFG */
		{0x69u,	WRITE,	0x5Au,	0xC1u},	/* CLK2CFG */
		{0x69u,	WRITE,	0x65u,	0xFFu},	/* DNMASK0 */
		{0x69u,	WRITE,	0x58u,	0x01u},	/* I2SRRSOFFS */
		{0x69u,	WRITE,	0x57u,	0x00u},	/* I2SRRCTL */
		{0x69u,	WRITE,	0x1Bu,	0xFFu},	/* INTMSK0 *///77
		{0x69u,	WRITE,	0x1Cu,	0xFFu},	/* INTMSK1 *///7F
		{0x69u,	WRITE,	0x1Eu,	0xEFu},	/* BECCTL *///EF
		
#ifdef LOOPBACK_SLAVE_TEST
/* if write to these 2 Reg, there is not BCLK output on Slave Side */
//		{0x69u,	WRITE,	0x0Du,	0x08u},	/* DNSLOTS */ // Slots Slave2Master
//		{0x69u,	WRITE,	0x0Eu,	0x02u},	/* UPSLOTS */ // Slots Master2Slave
#endif
		{0x68u,	WRITE,	0x3Fu,	0x00u},	/* I2CCFG */
		{0x68u,	WRITE,	0x42u,	0x11u},	/* I2SCFG */
#ifdef LOOPBACK_TEST1
		{0x68u,	WRITE,	0x53u,	0x06u},  //01_loopback Master2Host; 06_loopback Host2Master
#else
		{0x68u,	WRITE,	0x53u,	0x00u},
#endif
		{0x68u,	WRITE,	0x1Eu,	0xEFu},	/* BECCTL *///EF
		{0x68u,	WRITE,	0x0Du,	0x08u},	/* DNSLOTS */ // Slots Master2Slave
		{0x68u,	WRITE,	0x0Eu,	0x02u},	/* UPSLOTS */ // Slots Slave2Master
		{0x68u,	WRITE,	0x09u,	0x01u},	/* SWCTL */
		{0x68u,	WRITE,	0x40u,	0x00u},	/* PLLCTL */
		{0x68u,	WRITE,	0x01u,	0x80u},	/* NODEADR */
		{0x69u,	WRITE,	0x40u,	0x00u},	/* PLLCTL */
		{0x68u,	WRITE,	0x01u,	0x00u},	/* NODEADR */
		{0x68u,	WRITE,	0x10u,	0x66u},	/* SLOTFMT */
#ifdef TESTMODE
		{0x68u, WRITE,	0x20u,	0x03u}, /* TESTMODE */
		{0x00u, DELAY,	0x00u,	0xFFu},	/* A2B_Delay */
		{0x68u, READ,	0x21u,	0x00u},
		{0x68u, READ,	0x22u,	0x00u},
		{0x68u, READ,	0x23u,	0x00u},
		{0x68u, READ,	0x24u,	0x00u},
		{0x68u, WRITE,	0x20u,	0x00u}, /* TESTMODE */
#endif

#ifdef LOOPBACK_SLAVE_TEST
		{0x69u, WRITE,	0x53u,	0x01u},
#endif

#ifdef LOOPBACK_TEST1
		{0x68u,	WRITE,	0x11u,	0x00u},	/* DATCTL */ //1
#else
		{0x68u,	WRITE,	0x11u,	0x03u}, /* DATCTL */ //upstream & downstream enabled
#endif
		{0x68u,	WRITE,	0x56u,	0x00u},	/* I2SRRATE */
		{0x68u,	WRITE,	0x12u,	0x01u},	/* CONTROL */
};

uint8_t A2B_Read(DEVICE *dev,uint8_t device_addr,uint8_t register_address,uint8_t count)
{
	uint32_t result;
	
	result=FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, device_addr);
	if(result)
	{
		DPRINTF(DBG_MSG1,"raddr %02x %02x result:%04x\r\n",device_addr,register_address,result);
		return WR_ERR;
	}
	result=FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, &register_address, 1, true,1000);
	if(result)
	{
		DPRINTF(DBG_MSG1,"rreg %02x %02x result:%04x\r\n",device_addr,register_address,result);
		return WR_ERR;
	}

	result=FLEXIO_I2C_DRV_MasterReceiveDataBlocking(&i2cMasterState, &A2B_read_buffer, count, true,1000);
	if(result)
	{
		DPRINTF(DBG_MSG1,"rrecv %02x %02x result:%04x\r\n",device_addr,register_address,result);
		return WR_ERR;
	}
	return 0;
}

uint8_t A2B_Write(DEVICE *dev,uint8_t device_addr,uint8_t register_address,uint8_t data)
{
	uint32_t result;
	A2B_Write_Buffer[0]=register_address;
	A2B_Write_Buffer[1]=data;

	result=FLEXIO_I2C_DRV_MasterSetSlaveAddr(&i2cMasterState, device_addr);
	if(result)
	{
		DPRINTF(DBG_MSG1,"waddr %02x %02x %02x result:%04x\r\n",device_addr,register_address,data,result);
		return WR_ERR;
	}
	result=FLEXIO_I2C_DRV_MasterSendDataBlocking(&i2cMasterState, A2B_Write_Buffer, 2, true,1000);

	if(result)
	{
		DPRINTF(DBG_MSG1,"wsend %02x %02x %02x result:%04x\r\n",device_addr,register_address,data,result);
		return WR_ERR;
	}

	return 0;
}

uint8 A2B_Delay(DEVICE *dev,uint32 delay)
{
	uint8 delay_done=1;
	if(delay<=GetElapsedTime(A2B_I2C_delay_tick)){
		A2B_I2C_delay_tick = GetTickCount();
		delay_done = 0;
	}
	return delay_done;
}

void A2B_Power_ON_Set(DEVICE *dev)
{
		//DSP POWER ON
//		dev->r_Dsp->d_state = DSP_TEST_INIT_1;
		
//		DSP_PDN(ON);
//		DSP_PW_EN(ON);//DSP_PW_EN
		ATB_3V3_EN(ON); //A2B power off after A2B init failed
		DPRINTF(DBG_MSG1,"%s\r\n",__FUNCTION__);
}

void A2B_Power_OFF_Set(DEVICE *dev)
{
#ifdef A2B_POWER_ACT_LIMIT
	/*0~10 series_init_err_count,every time do A2B power control.
	 *11~200 series_init_err_count,every 10 times do A2B power control.
	 *over 200, do not do A2B power control.
	 */
	 #if 1
	 if((dev->r_A2B->series_init_err_count >= 0 && dev->r_A2B->series_init_err_count <= 10)||
		(dev->r_A2B->series_init_err_count > 10 && dev->r_A2B->series_init_err_count <= 200 && dev->r_A2B->series_init_err_count % 10 == 0))
	 #else
	if(/*(dev->r_A2B->series_init_err_count >= 0 && dev->r_A2B->series_init_err_count <= 10)|| */
		(dev->r_A2B->series_init_err_count > 10 && dev->r_A2B->series_init_err_count <= 200 && dev->r_A2B->series_init_err_count % 10 == 0))
	#endif
#endif
	{
		#ifndef EMC_FOR_EXTERNAL_AMP
		ATB_3V3_EN(OFF); //A2B power off after A2B init failed
		#endif
//		dev->r_Dsp->d_state = DSP_IDLE;
		//DSP POWER OFF
//		DSP_PDN(OFF);
//		DSP_PW_EN(OFF);//DSP_PW_EN
//		
#if 0
		flexio_i2c_deinit();//disable flexIO	
		PORTA->PCR[0] |=PORT_PCR_MUX(1);     					//GPIO O	
		PORTA->PCR[1] |=PORT_PCR_MUX(1);     					//GPIO O	
		PTA->PDDR |= 1<<0; 
		PTA->PDDR |= 1<<1; 
#else
//		flexio_i2c_deinit();//disable flexIO	
#endif
//		delay_ms(100);
		dev->r_A2B->a2b_need_recover = ON;

//		A2B_Sleep_Set(dev);
		a2b_recovery_tick= GetTickCount();
		DPRINTF(DBG_MSG1,"%s\r\n",__FUNCTION__);
		dev->r_A2B->a2b_power_status=0;
	}
}

void vPort_B_ISRHandler(void)
{
	A2B_ISR_Flag=1;
	PINS_DRV_ClearPortIntFlagCmd(PORTB);
}

void A2B_IRQ_Set(void)
{
	PINS_DRV_SetPullSel(PORTB, 9U, PORT_INTERNAL_PULL_DOWN_ENABLED);
	PINS_DRV_SetPinIntSel(PORTB, 9U, PORT_INT_RISING_EDGE);
	INT_SYS_InstallHandler(PORTB_IRQn, vPort_B_ISRHandler, (isr_t *)0);
	INT_SYS_EnableIRQ(PORTB_IRQn);
	INT_SYS_SetPriority( PORTB_IRQn, 0 );
}

void A2B_Init_Data(DEVICE *dev)
{
	A2B_ISR_Flag=0;
	nIndex =0;
	dev->r_A2B->a_state = A2B_INIT2;
	A2B_init_tick = GetTickCount();
	DPRINTF(DBG_MSG1,"A2B_init_cnt:%d\r\n",dev->r_A2B->a_init_cnt);
}

void A2B_Init_Trigger(DEVICE *dev)
{
	dev->r_A2B->init_trigger_flag = 1;
	nIndex = 0;
	dev->r_A2B->discovery_success=0;
	A2B_Power_OFF_Set(dev);
	A2B_init_tick = GetTickCount();
}

void A2B_Sleep_Set(DEVICE *dev)
{
	dev->r_A2B->a_state =A2B_IDLE;
	dev->r_A2B->init_trigger_flag = 0;
	dev->r_A2B->a_init_cnt=0;
	dev->r_A2B->series_init_err_count=0;
	A2B_ISR_Flag=0;
	INT_SYS_DisableIRQ(PORTB_IRQn);
}
/*
 * Execute the process of A2B discovery & initialization
 */

uint8_t A2B_Init(DEVICE *dev)
{
	ADI_A2B_DISCOVERY_CONFIG* pOPUnit;
	uint8_t init_onestep_result=0;
	uint8_t init_done=0;
	pOPUnit = &A2B_list[nIndex];

	switch (pOPUnit->eOpCode)
	{
		/* Write */
		case WRITE:
			init_onestep_result = A2B_Write(dev,pOPUnit->nDeviceAddr,pOPUnit->nRegAddr,pOPUnit->nData);
			A2B_I2C_delay_tick = GetTickCount();
			break;
			/* Read */
		case READ:
			init_onestep_result = A2B_Read(dev,pOPUnit->nDeviceAddr,pOPUnit->nRegAddr,1);
			A2B_I2C_delay_tick = GetTickCount();
			break;
			/* Delay */
		case DELAY:
			init_onestep_result = A2B_Delay(dev,pOPUnit->nData);
			break;

		default:
			break;
	}
	/*if init_onestep_result=0,then RD/WR success or delay finish
	 *if init_onestep_result=1,then RD/WR fail or delay not finished.
	 */
	if(!init_onestep_result)
	{
		nIndex++;
		dev->r_A2B->error_retry_count=0;
	}

	else
	{
		if(pOPUnit->eOpCode != DELAY)//It is executed when the register is read or written
		{
			dev->r_A2B->error_retry_count++;

			if(dev->r_A2B->error_retry_count>2)
			{
				nIndex = sizeof(A2B_list)/4;
				DPRINTF(DBG_MSG1,"over count err\n\r");
			}
			else
			{
				DPRINTF(DBG_MSG1,"retry %d\n\r",dev->r_A2B->error_retry_count);
			}
		}
	}
	if(nIndex == (sizeof(A2B_list)/4)){

		nIndex =0;
		init_done =1;
		if(dev->r_A2B->error_retry_count>2)
		{
			dev->r_A2B->init_success = 0;
			DPRINTF(DBG_MSG1,"init error\n\r");
		}
		else
		{
			dev->r_A2B->init_success = 1;
			DPRINTF(DBG_MSG1,"init finish\n\r");
		}
		dev->r_A2B->error_retry_count=0;
	}

	return init_done;

}

void A2B_ISR(DEVICE *dev)
{
	if(!A2B_Read(dev,MASTER_AD2428_ADDR,INT_TYPE_REG,1))//read success
	{
		DPRINTF(DBG_MSG1,"A2B_Int: %02x\n\r", A2B_read_buffer);
		switch(A2B_read_buffer)
		{
			case MASTER_RUNNING:
				break;
			case DSCDONE:
				dev->r_A2B->discovery_success=1;
				break;
			case I2CERR://I2C error
			case ICRCERR://Interrupt CRC error
			case PWRERR_9://PWRERR - Positive terminal BP shorted to GND
			case PWRERR_10://PWRERR - Negative terminal BN shorted to VBAT
			case PWRERR_11://PWRERR - BP shorted to BN
			case PWRERR_13://PWRERR - Cable is reverse connected or wrong port
			case PWRERR_15://PWRERR - Undetermined fault
			case PWRERR_41://PWRERR - Non-localized negative terminal BN short to GND
			case PWRERR_42://PWRERR - Non-localized positive terminal BP short to VBAT
			case PWRERR_12://PWRERR - Cable disconnected or open circuit or wrong port
			#ifndef EMC_FOR_EXTERNAL_AMP
			case SRFERR://SRF miss error
			#endif
			case BECOVF://Bit error counter overflow error
			case MESSAGING_ERROR://Interrupt messaging error
			case DDERR://Data decoding error
			#ifndef EMC_FOR_EXTERNAL_AMP
			case CRCERR://CRC error
			case SRFCRCERR://SRF CRC error
			#endif
			case INTTYPE_READ_ERROR://Slave INTTYPE read error
			case HDCNTERR://Header count error
			case DPERR://Data parity error
			case IO0PND_0://GP input IO0 interrupt
			case IO0PND_1://GP input IO1 interrupt
			case IO0PND_2://GP input IO2 interrupt
			case IO0PND_3://GP input IO3 interrupt
			case IO0PND_4://GP input IO4 interrupt
			case IO0PND_5://GP input IO5 interrupt
			case IO0PND_6://GP input IO6 interrupt
			case IO0PND_7://GP input IO7 interrupt
			case STARTUP_ERROR://Startup error - Return to factory
			case MAILBOX0_FULL://Mailbox 0 full
			case MAILBOX0_EMPTY://Mailbox 0 empty
			case MAILBOX1_FULL://Mailbox 1 full
			case MAILBOX1_EMPTY://Mailbox 1 empty
				if(dev->r_A2B->discovery_success)
					A2B_Init_Trigger(dev);
				break;
			default:
				break;
		}
		dev->r_A2B->int_type=A2B_read_buffer;
	}
	else
	{
		DPRINTF(DBG_MSG1,"A2B_Int read err\n\r");
	}
}

/*
 * A2B Process
 * called every 2ms,
 * Check the Interrupt from AD2428
 * Process the A2B discovery
 */
void A2B_proc(DEVICE *dev)
{
	if(dev->r_A2B->a2b_need_recover==ON)
	{
		if(100<=GetElapsedTime(a2b_recovery_tick))
		{
//			PORTA->PCR[0] |=PORT_PCR_MUX(4);    		  				//flex io i2c
//			PORTA->PCR[1] |=PORT_PCR_MUX(4);							//flex io i2c
//			flexio_i2c_init();

			DPRINTF(DBG_MSG1,"\nrecovery flexio_i2c 1\r\n");
			dev->r_A2B->a2b_need_recover =OFF;
			a2b_recovery_tick= GetTickCount();
		}
		return;
	}
	else
		a2b_recovery_tick= GetTickCount();
	

	if(dev->r_A2B->init_trigger_flag)
	{
		if(5000<=GetElapsedTime(A2B_init_tick))
		{
			if(!dev->r_A2B->a2b_power_status)
			{
				A2B_Power_ON_Set(dev);
				dev->r_A2B->a2b_power_status = 1;
			}

			if(5100<=GetElapsedTime(A2B_init_tick)) //delay 100ms implement A2B init after A2B power on
			{
			dev->r_A2B->a_state = A2B_INIT2;
			dev->r_A2B->a_init_cnt++;
			DPRINTF(DBG_MSG1,"A2B_init_cnt:%d\r\n",dev->r_A2B->a_init_cnt);
			A2B_init_tick=GetTickCount();	//update the init timer tick
		}
	}
	}

	if(A2B_ISR_Flag==1)
	{
		if(dev->r_A2B->a2b_need_recover==ON)
		{
//			flexio_i2c_init();
			DPRINTF(DBG_MSG1,"\n recovery flexio_i2c  2\r\n");
			dev->r_A2B->a2b_need_recover =OFF;
			a2b_recovery_tick= GetTickCount();
		}
			
		DPRINTF(DBG_MSG1,"enter ISR\r\n");
		A2B_ISR(dev);
		A2B_ISR_Flag = 0;
	}

	//Check the A2B top status
	switch(dev->r_A2B->a_state)
	{
		case A2B_IDLE:
		case A2B_ACTIVE:
			break;
		case A2B_INIT1:
			DPRINTF(DBG_MSG1,"A2B_INIT1\r\n");
			A2B_IRQ_Set();
			A2B_Init_Data(dev);
			dev->r_A2B->series_init_err_count=0;  //first booting
			break;

		case A2B_INIT2:
			if(A2B_Init(dev))
			{
				if(dev->r_A2B->discovery_success && dev->r_A2B->init_success)
				{
					dev->r_A2B->init_trigger_flag=0;
					DPRINTF(DBG_MSG1,"A2B_Init_OK\r\n");
					dev->r_A2B->a_state  =A2B_ACTIVE;		//Set the A2B state to ACTIVE
					dev->r_A2B->series_init_err_count=0;  //if init success,then series_init_err_count=0
				}
				else
				{
					dev->r_A2B->discovery_success=0;
					dev->r_A2B->init_trigger_flag=1;
					dev->r_A2B->a_state  =A2B_IDLE;			//Set the A2B state to IDLE if init failed, will trig the slave node monitor
					dev->r_A2B->series_init_err_count++;    //if series init failed ,then series_init_err_count add 1
					A2B_Power_OFF_Set(dev);
					DPRINTF(DBG_MSG1,"series_init_err=%d\r\n",dev->r_A2B->series_init_err_count);
					DPRINTF(DBG_MSG1,"A2B_Init_Fail\r\n");
				}

				if(dev->r_A2B->a_init_cnt==0)
					dev->r_Dsp->d_state =DSP_INIT_15;
			}
			break;
	}

}

