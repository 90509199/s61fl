/*
 * shellcmd.h
 *
 *  Created on: 2018. 8. 29.
 *      Author: Digen
 */

#ifndef SHELLCMD_H_
#define SHELLCMD_H_

extern uint8 shell_system_mode;
extern uint8	shell_volume;
extern uint8	shell_audio_src;
extern uint8 shell_lound;
extern uint8 shell_geq_onoff;

extern uint16 shell_radio_freq;

typedef uint8_t	(*GetCharFuncType)(void);
typedef int8_t	(*PutCharFuncType)(uint8_t);

void InitDrvShell();
void Shell_Task();
#endif
