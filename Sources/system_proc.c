/*
 * sys_comm.c
 *
 *  Created on: 2018. 12. 21.
 *      Author: Digen
 */

#include "includes.h"
#include "Diag/dtc_mgr.h"
#include "drv_par.h"

#define ACC_IGN_SCAN_TIME	50				//50ms
#define CCL_RELASE_TIME			(3*(1000))		//(10*(1000))

extern vuint8 IHU_12IlTxDefaultInitValue[8];
extern vuint8 IHU_1IlTxDefaultInitValue[8];
extern bool IsCustomMatched(void);

uint8 u8cansendrequest=0;
uint32 u32cansendrequest_tick = 0;
volatile uint8 flash_erase_on = 0;
volatile uint8 nm_firstframe_cnt = 0;
volatile uint8 diag_firstframe_cnt = 0;

 volatile uint32 system_boot_tick = 0;
static volatile uint32 system_timer_tick = 0;
static volatile uint32 system_timer_tick1 = 0;
static volatile uint32 standby_timer_tick = 0;

static volatile uint32 sleep_timer_tick = 0;
static volatile uint32 min15_timer_tick = 0;
static volatile uint32 lowbatt_timer_tick = 0;
static volatile uint32 lowbatt_timer_tick1 = 0;
static volatile uint32 highbatt_timer_tick = 0;
static volatile uint32 highbatt_can_timer_tick = 0;
static volatile uint32 batter_adc_timer_tick = 0;
static volatile uint32 batter_adc_timer_tick1 = 0;
static volatile uint32 batter_adc_can_timer_tick = 0;
static volatile uint32 voltage_dtc_timer_tick = 0;

volatile uint32 check_diag_timer_tick = 0;
volatile uint32 check_diag_timer_tick1 = 0;
volatile uint32 test_timer_tick1 = 0;
volatile uint32 nm_start_timer_tick = 0;
volatile uint32 ig_start_timer_tick = 0;

#ifdef CHERY_REQUSET_SYSTEM_TEST
volatile uint32 chery_request_timer_tick = 0;
#endif
volatile uint32 check_monitor_stanbymode_tick = 0;
extern uint8  u8TestSleepCmd;


static void PreCheckSleepMode(DEVICE *dev);
static void CheckSleepMode(DEVICE *dev);
static void MoveSleepMode(DEVICE *dev);
static void StaySleepMode(DEVICE *dev);

static void CheckStandbyMode(DEVICE *dev);
static void MoveStandbyMode(DEVICE *dev);
static void StayStandbyMode(DEVICE *dev);

static void CheckActiveMode(DEVICE *dev);
static void MoveActiveMode(DEVICE *dev);
static void StayActiveMode(DEVICE *dev);

static void CheckScreenOff1Mode(DEVICE *dev);
static void MoveScreenOff1Mode(DEVICE *dev);
static void StayScreenOff1Mode(DEVICE *dev);

static void CheckScreenOff2Mode(DEVICE *dev);
static void MoveScreenOff2Mode(DEVICE *dev);
static void StayScreenOff2Mode(DEVICE *dev);

static void CheckScreenSaverMode(DEVICE *dev);
static void MoveScreenSaverMode(DEVICE *dev);
static void StayScreenSaverMode(DEVICE *dev);

static void Check15MinMode(DEVICE *dev);
static void Move15MinMode(DEVICE *dev);
static void Stay15MinMode(DEVICE *dev);

static void MoveToLowBatteryMode(DEVICE *dev);
static void StayLowBatteryMode(DEVICE *dev);

static void MoveToHighBatteryMode(DEVICE *dev);
static void StayHighBatteryMode(DEVICE *dev);

static void MoveToLow6VMode(DEVICE *dev);
static void StayLow6VMode(DEVICE *dev);

static void MoveToLow9VMode(DEVICE *dev);
static void StayLow9VMode(DEVICE *dev);

static void MoveToHigh16VMode(DEVICE *dev);
static void StayHigh16VMode(DEVICE *dev);

static void CheckBatFunc(DEVICE *dev);

#if 0
uint8 DET_ACC_IGN(void)
{
	static uint8 acc_det=0;
	static uint8 ign_det=0;
	uint8 tmp=0;

	tmp = MCU_ACC_DET()|(MCU_IGN_DET()<<1);
	
#if 1
	if(MCU_ACC_DET() !=acc_det)
	{
		acc_det = MCU_ACC_DET();
		acc_ign_status.buf[0] = tmp;
		acc_ign_status.trycnt = 0;
		acc_ign_status.flag = ON;
		DPRINTF(DBG_MSG1,"MCU_ACC_DET %d \n\r",acc_det);
	}
	if(MCU_IGN_DET() !=ign_det)
	{
		ign_det = MCU_IGN_DET();
		acc_ign_status.buf[0] = tmp;
		acc_ign_status.trycnt = 0;
		acc_ign_status.flag = ON;
		DPRINTF(DBG_MSG1,"MCU_IGN_DET %d \n\r",ign_det);
	}
#endif

	return tmp;
}
#endif

void CanChangeDefValAfterReset(void)
{
	// IHU_12IlTxDefaultInitValue[0]=0u;
	// IHU_12IlTxDefaultInitValue[1]=0u;
}

void CanChangeDefValBeforeSleep(void)
{
	// IHU_1IlTxDefaultInitValue[3]=((IHU_1IlTxDefaultInitValue[3]&0xF3u)|(e2p_save_data.E2P_PSNGRAIRBAG_ONOFF<<2));
}

void CanRestoreDefVal(void)
{
	CanChangeDefValAfterReset();
	CanChangeDefValBeforeSleep();
	u8cansendrequest=3;
}

void CanSendInitValue(void)
{
	switch (u8cansendrequest)
	{
		case 3u:
			u8cansendrequest=2u;
			u32cansendrequest_tick=GetTickCount();
			IlPutTxIHU_OverSpdValueSet(0);
			IlPutTxIHU_FatigureDrivingTimeSet(0);
			IlPutTxIHU_BrightnessAdjustSet_ICM(0);
			// IlPutTxIHU_PsngrAirbagDisableSwSts(e2p_save_data.E2P_PSNGRAIRBAG_ONOFF);
			break;
		case 2u:
			if (300<GetElapsedTime(u32cansendrequest_tick))
			{
				u8cansendrequest=1u;
				u32cansendrequest_tick=GetTickCount();
				IlPutTxIHU_OverSpdValueSet(e2p_save_data.E2P_CLS_OVERSPD);
				IlPutTxIHU_FatigureDrivingTimeSet(e2p_save_data.E2P_CLS_DRIVINGTIME);
				IlPutTxIHU_BrightnessAdjustSet_ICM(e2p_save_data.E2P_CLS_BRIGHTNESS);
			}
			break;
		case 1u:
			if (1700<GetElapsedTime(u32cansendrequest_tick))
			{
				u8cansendrequest=0u;
				u32cansendrequest_tick=GetTickCount();
				if (IsCustomMatched())
				{
					IlPutTxIHU_AVAS_EnableSwSts(e2p_save_data.E2P_AVAS_ONOFF);
				}
			}
			break;
		default:break;
	}
}

uint8 Set_Acc_Status(uint8 data)
{
	uint8 tmp=0;

	if(data&0x01)
		tmp = 0x1;
	else
		tmp = 0x2;

	return tmp;
}

uint8 Check_AccIgn_State(DEVICE *dev)
{
	uint8 tmp=0;
	static int8 cnt=0;
	
	//tmp = MCU_ACC_DET()|(MCU_IGN_DET()<<1);

	tmp = MCU_ACC_DET();
	tmp = tmp|(tmp<<1);
	if(dev->r_System->accign_state.cnt<=ACC_IGN_SCAN_TIME)
		dev->r_System->accign_state.cnt++;

	if(dev->r_System->accign_state.prev_state != tmp)
	{
//		DPRINTF(DBG_MSG1,"%s  clear %d\n\r",__FUNCTION__,tmp);
		dev->r_System->accign_state.cnt =0;
		dev->r_System->accign_state.prev_state = tmp;
	}

	//scan 50ms
	if(ACC_IGN_SCAN_TIME==dev->r_System->accign_state.cnt)
	{	
		#if 0
		if((tmp==0)&&(dev->r_System->accign_state.state !=0))
			gCanTpState =TP_STATE_CHECKIGN_START;
		else if((tmp!=0)&&(dev->r_System->accign_state.state ==0))
			gCanTpState =TP_STATE_CHECKIGN_START;
		#else
		#ifdef CX62C_FUNCTION
		gCanTpState =TP_STATE_CHECKIGN_START;
		#endif
		#endif
		
		dev->r_System->accign_state.state = tmp;
		acc_ign_status.buf[0] = tmp;
		acc_ign_status.trycnt = 0;
		acc_ign_status.flag = ON;
		DPRINTF(DBG_MSG1,"%s  acc_ign status %d\n\r",__FUNCTION__,tmp);
	}

	return tmp;
}

void Control_LCD_Backlight(uint8 OnOff)
{
	if(OnOff==ON)
	{
		#ifdef B1_MOTHER_BOARD_SAMPLE
		MCU_HU_LCD_EN(OFF);
		#endif
		MCU_LCD1_2_EN_3P3(OFF);
		DPRINTF(DBG_MSG1,"%s LCD ON\n\r",__FUNCTION__);
	}
	else
	{
		#ifdef B1_MOTHER_BOARD_SAMPLE
		MCU_HU_LCD_EN(ON);
		#endif
		MCU_LCD1_2_EN_3P3(ON);
		DPRINTF(DBG_MSG1,"%s LCD OFF\n\r",__FUNCTION__);
	}
}

void Control_LCD_Backlight_for_Mengbo(DEVICE *dev, uint8 OnOff)
{
	uint8 tmp8, temp8_1;

	if(OnOff)
	{
		Control_LCD_Backlight(ON);
		tmp8 = Set_LCD_Brightness(1);
		temp8_1 = Set_LCD_Brightness(e2p_save_data.E2P_LCD_BRIGHTNESS);
	}
	else
	{
		tmp8 = Set_LCD_Brightness(e2p_save_data.E2P_LCD_BRIGHTNESS);
		temp8_1 = Set_LCD_Brightness(1);
	}

	dev->r_System->lcd_control.enable = ON;
	dev->r_System->lcd_control.gotovalue = Set_BL_Duty_Step(tmp8, temp8_1);
	if(tmp8<temp8_1)
		dev->r_System->lcd_control.direct =	1;
	else if(tmp8==temp8_1)
		dev->r_System->lcd_control.direct =	2;			// set value is same
	else
		dev->r_System->lcd_control.direct =	0;
	
	dev->r_System->lcd_control.startvalue = tmp8;
	dev->r_System->lcd_control.counter = 0;
	dev->r_System->lcd_control.onoff = ON;
}

void System_Proc(DEVICE *dev)
{
	switch(dev->r_System->s_state)
	{
		case SYS_IDLE_MODE:
			system_timer_tick = GetTickCount();
			sleep_timer_tick = GetTickCount();
			break;
		case SYS_SLEEP_MODE_PRE_CHECK:
			PreCheckSleepMode(dev);
			break;
		case SYS_SLEEP_MODE_CHECK:
			CheckSleepMode(dev);
			break;
		case SYS_SLEEP_MODE_ENTER:
			MoveSleepMode(dev);
			break;
		case SYS_SLEEP_MODE_IDLE:
			StaySleepMode(dev);
			break;
		case SYS_STANDBY_MODE_CHECK:
			CheckStandbyMode(dev);
			break;
		case SYS_STANDBY_MODE_ENTER:
			MoveStandbyMode(dev);
			break;
		case SYS_STANDBY_MODE_IDLE:
			StayStandbyMode(dev);
			break;	
		case SYS_ACTIVE_MODE_CHECK:
			CheckActiveMode(dev);
			break;
		case SYS_ACTIVE_MODE_ENTER:
			MoveActiveMode(dev);
			break;
		case SYS_ACTIVE_MODE_IDLE:
			StayActiveMode(dev);
			break;
		case SYS_SCREENOFF_1_MODE_CHECK:
			CheckScreenOff1Mode(dev);
			break;	
		case SYS_SCREENOFF_1_MODE_ENTER:
			MoveScreenOff1Mode(dev);
			break;
		case SYS_SCREENOFF_1_MODE_IDLE:
			StayScreenOff1Mode(dev);
			break;
		case SYS_SCREENOFF_2_MODE_CHECK:
			CheckScreenOff2Mode(dev);
			break;
		case SYS_SCREENOFF_2_MODE_ENTER:
			MoveScreenOff2Mode(dev);
			break;
		case SYS_SCREENOFF_2_MODE_IDLE:
			StayScreenOff2Mode(dev);
			break;
		case SYS_SCREEN_SAVE_MODE_CHECK:
			CheckScreenSaverMode(dev);
			break;
		case SYS_SCREEN_SAVE_MODE_ENTER:
			MoveScreenSaverMode(dev);
			break;
		case SYS_SCREEN_SAVE_MODE_IDLE:
			StayScreenSaverMode(dev);
			break;	
		case SYS_15MIN_MODE_CHECK:
			Check15MinMode(dev);
			break;
		case SYS_15MIN_MODE_ENTER:
			Move15MinMode(dev);
			break;
		case SYS_15MIN_MODE_IDLE:
			Stay15MinMode(dev);
			break;
		case SYS_BATTERY_LOW_MODE_ENTER:
			MoveToLowBatteryMode(dev);
			break;
		case SYS_BATTERY_LOW_MODE_IDLE:
			StayLowBatteryMode(dev);
			break;
		case SYS_BATTERY_HIGH_MODE_ENTER:
			MoveToHighBatteryMode(dev);
			break;
		case SYS_BATTERY_HIGH_MODE_IDLE:
			StayHighBatteryMode(dev);
			break;
		case SYS_BATTERY_6V_UNDER_MODE_ENTER:
			MoveToLow6VMode(dev);
			break;
		case SYS_BATTERY_6V_UNDER_MODE_IDLE:
			StayLow6VMode(dev);
			break;
		case SYS_BATTERY_9V_UNDER_MODE_ENTER:
			MoveToLow9VMode(dev);
			break;
		case SYS_BATTERY_9V_UNDER_MODE_IDLE:
			StayLow9VMode(dev);
			break;
		case SYS_BATTERY_16V_OVER_MODE_ENTER:
			MoveToHigh16VMode(dev);
			break;	
		case SYS_BATTERY_16V_OVER_MODE_IDLE:
			StayHigh16VMode(dev);
			break;
		case SYS_CHECK_DELAY_0:			//1000ms->500ms
			/* 20220808 */
			//if(500<=GetElapsedTime(system_timer_tick))
			if(200<=GetElapsedTime(system_timer_tick))
			{
				DPRINTF(DBG_MSG1,"SYS_CHECK_DELAY->SYS_STANDBY_MODE_ENTER\n\r");
				dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
				dev->r_System->ccl_active = ON; //first can operate when battery enter
				system_timer_tick = GetTickCount();
			}
			break;
		case SYS_CHECK_DELAY_1:		//200ms
			if(200<=GetElapsedTime(system_timer_tick))
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
				system_timer_tick = GetTickCount();
			}
			break;
		case SYS_CHECK_DELAY_2://20, 15ms ok
			if(70<=GetElapsedTime(system_timer_tick))
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
				system_timer_tick = GetTickCount();
			}
			break;
		case SYS_DELAY_100MS_FOR_CAN:
			if(100<=GetElapsedTime(system_timer_tick))
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
				system_timer_tick = GetTickCount();
			}
			break;
		case SYS_DELAY_200MS_FOR_CAN:
			if(200<=GetElapsedTime(system_timer_tick))
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
				system_timer_tick = GetTickCount();
			}
			break;
		default:
			dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
			break;
	}
#ifdef VECTOR_CCL_TEST


if(dev->r_System->ccl_active !=dev->r_System->prev_ccl_active)
{
	if(dev->r_System->ccl_active)
	{
		MCU_CAN0_EN(ON);
		MCU_CAN0_STB(ON);
		//WL0714
		CclSet_Com_Request0();	//start ccl
		DPRINTF(DBG_MSG1,"CclSet_Com_Request1\n\r");
	}
	else
	{
		//WL0714
		CclRel_Com_Request0();	//start ccl
		DPRINTF(DBG_MSG1,"CclRel_Com_Request1\n\r");
	}

	dev->r_System->prev_ccl_active = dev->r_System->ccl_active;
}

{
	static uint8 nm_state=0;
	static uint16_t res_nm=0;
	//int rx=0;
	//DPRINTF(DBG_MSG1,"\n SWITCH : %d \n\r",ApplGet_NM_state());
	switch(ApplGet_NM_state())
	{
		case NM_STATE_UNINIT:
			if(NM_STATE_UNINIT!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_UNINIT %d\n\r",test_timer_tick1);
			}

			nm_state = NM_STATE_UNINIT;
			break;
		case NM_STATE_BUS_SLEEP:
			CanChangeDefValBeforeSleep();
			if(NM_STATE_BUS_SLEEP!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_BUS_SLEEP %d\n\r",test_timer_tick1);

				res_nm=0;
			}
			/* Test sleep cmd */
			if(u8TestSleepCmd == 1)
			{
				dev->r_System->s_state = SYS_SLEEP_MODE_IDLE;
				if((dev->r_Power->p_state >= PWR_IDLE) && (dev->r_Power->p_state <= PWR_OFF_7))
				{
					/* Do nothing */
				}
				else
				{
					dev->r_Power->p_state = PWR_OFF_1;
				}
			}

			res_nm++;
			if(res_nm >= 10000)
			{
				CclInitPowerOn();
				//CclSet_Com_Request0();
				VectorIsrInit();
				res_nm=0;
				//DPRINTF(DBG_MSG1,"\n SWITCH : %d \n\r",ApplGet_NM_state());
			}
			nm_state = NM_STATE_BUS_SLEEP;
			break;
		case NM_STATE_PREPARE_BUS_SLEEP:
			if(NM_STATE_PREPARE_BUS_SLEEP!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				/* wl20220729 save DTC data before Bus Sleep */
        		// SYS_NVM_UPDATE_DTC_DATA(GetDtcMgrDataPtr(), GetDtcMgrDataSize());
				/* wl20220819 */
        		WriteDTCData();
				DPRINTF(DBG_MSG1,"NM_STATE_PREPARE_BUS_SLEEP %d\n\r",test_timer_tick1);
			}
			res_nm=0;
			nm_state = NM_STATE_PREPARE_BUS_SLEEP;
			break;
		case NM_STATE_READY_SLEEP:
			if(NM_STATE_READY_SLEEP!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_READY_SLEEP %d\n\r",test_timer_tick1);
			}
			nm_state = NM_STATE_READY_SLEEP;
			break;
		case NM_STATE_NORMAL_OPERATION:
			if(NM_STATE_NORMAL_OPERATION!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				system_boot_tick = GetTickCount(); 			//need to check nm state 4sec
				//wx20220906 deal with extra NOS frame when nm state changed from repeat message to normal
				//It's checked only once when state changed
				if (r_Device.r_System->accign_state.cnt > 50 && r_Device.r_System->accign_state.state == OFF && nm_state == NM_STATE_REPEAT_MESSAGE)
				{
					r_Device.r_System->ccl_active = OFF;
				}
				DPRINTF(DBG_MSG1,"NM_STATE_NORMAL_OPERATION %d %d\n\r",test_timer_tick1,(dev->r_System->usrdata1&ECUSPEC_WAKEUP));
			}
			//
			nm_state = NM_STATE_NORMAL_OPERATION;
			CanSendInitValue();
			break;
		case NM_STATE_REPEAT_MESSAGE:
			if(NM_STATE_REPEAT_MESSAGE!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				system_boot_tick = GetTickCount(); 			//need to check nm state
				DPRINTF(DBG_MSG1,"NM_STATE_REPEAT_MESSAGE %d\n\r",test_timer_tick1);
			}

			nm_state = NM_STATE_REPEAT_MESSAGE;
			break;
		case NM_STATE_SYNCHRONIZE:
			if(NM_STATE_SYNCHRONIZE!=nm_state)
			{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_SYNCHRONIZE %d\n\r",test_timer_tick1);
				}
			nm_state = NM_STATE_SYNCHRONIZE;
			break;
		case NM_STATE_WAIT_CHECK_ACTIVATION:
			if(NM_STATE_WAIT_CHECK_ACTIVATION!=nm_state)
				{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_WAIT_CHECK_ACTIVATION %d\n\r",test_timer_tick1);
				}
			nm_state = NM_STATE_WAIT_CHECK_ACTIVATION;
			break;
		case NM_STATE_WAIT_NETWORK_STARTUP:
			if(NM_STATE_WAIT_NETWORK_STARTUP!=nm_state)
				{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_WAIT_NETWORK_STARTUP %d\n\r",test_timer_tick1);
				}
			nm_state = NM_STATE_WAIT_NETWORK_STARTUP;
			break;
		case NM_STATE_BUS_OFF:
			if(NM_STATE_BUS_OFF!=nm_state)
				{
				test_timer_tick1 = GetTickCount();
				DPRINTF(DBG_MSG1,"NM_STATE_BUS_OFF %d\n\r",test_timer_tick1);
				}
			nm_state = NM_STATE_BUS_OFF;
			break;
					}
				}
//set usredata
			{
			#if 1
	// 3�ʵ��� ���� �޼��� ���� Ȯ��
			if(3000<=GetElapsedTime(check_diag_timer_tick))
			{
				check_diag_timer_tick = GetTickCount();
			}
	//1000->100
	if((200<=GetElapsedTime(check_diag_timer_tick1)&&(SYS_SLEEP_MODE_IDLE<dev->r_System->s_state)))
				{
//				if(SrvMcanGetChangeFlg(DIAG_RX_74A_IDX))
//				{
//					if(dev->r_System->accign_state.state)
//						dev->r_System->usrdata1 |=(DIGNOSTIC_AWAKE|ECUSPEC_AWAKE|IGN_AWAKE);
//					else
//					{
//							dev->r_System->usrdata1 |=(DIGNOSTIC_AWAKE|ECUSPEC_AWAKE);
//						dev->r_System->usrdata1 &=(~IGN_AWAKE);
//					}
//				}

				{
					if(dev->r_System->accign_state.state)
					{
//				 	if((SYS_ACTIVE_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_SCREEN_SAVE_MODE_IDLE))
				if((SYS_ACTIVE_MODE_IDLE==dev->r_System->s_state)||(SYS_SCREENOFF_1_MODE_IDLE==dev->r_System->s_state)
						||(SYS_SCREEN_SAVE_MODE_IDLE==dev->r_System->s_state))
				{
						dev->r_System->usrdata1 &=(~(DIGNOSTIC_AWAKE|ECUSPEC_AWAKE));
//						DPRINTF(DBG_MSG1,"userdata1 %d\n\r",dev->r_System->usrdata1);
					}
//					else if((SYS_15MIN_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE))
				else if(dev->r_System->s_state==SYS_15MIN_MODE_IDLE)
					{
					dev->r_System->usrdata1 &=(~DIGNOSTIC_AWAKE);
							dev->r_System->usrdata1 |=ECUSPEC_AWAKE;
//						DPRINTF(DBG_MSG1,"userdata2 %d\n\r",dev->r_System->usrdata1);

				}
				dev->r_System->usrdata1 |=IGN_AWAKE;
			}
			else
			{
//				 	if((SYS_STANDBY_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_ACTIVE_MODE_IDLE))
				if((SYS_STANDBY_MODE_IDLE==dev->r_System->s_state)/*||(dev->r_System->s_state==SYS_ACTIVE_MODE_IDLE)*/)
			{
					dev->r_System->usrdata1 &=(~(DIGNOSTIC_AWAKE|ECUSPEC_AWAKE));
//						DPRINTF(DBG_MSG1,"userdata3 %d\n\r",dev->r_System->usrdata1);
			}
//					else if((SYS_SCREENOFF_1_MODE_CHECK<=dev->r_System->s_state)&&(dev->r_System->s_state<=SYS_15MIN_MODE_IDLE))
				else if((SYS_SCREENOFF_1_MODE_IDLE==dev->r_System->s_state)||(dev->r_System->s_state==SYS_SCREEN_SAVE_MODE_IDLE)||
					(dev->r_System->s_state==SYS_15MIN_MODE_IDLE))
			{
					dev->r_System->usrdata1 &=(~DIGNOSTIC_AWAKE);
					dev->r_System->usrdata1 |=ECUSPEC_AWAKE;
//						DPRINTF(DBG_MSG1,"userdata4 %d\n\r",dev->r_System->usrdata1);
			}
				dev->r_System->usrdata1 &=(~(IGN_AWAKE));
			}
			}
		//TX_RRM_NM_USR_DATA1(dev->r_System->usrdata1);
		check_diag_timer_tick1 = GetTickCount();
	}
	#endif
}
#endif
	CheckBatFunc(dev);
}

static void PreCheckSleepMode(DEVICE *dev)
{
	static uint8 precheckstatus=CHECK_STATUS_STEP_0;

	//1. ACC DET ON -> standby mode
	if(1<=GetElapsedTime(system_timer_tick ))
	{ 
		if(precheckstatus==CHECK_STATUS_STEP_0)
		{
			dev->r_System->canmn_start_flag =OFF;
			precheckstatus = CHECK_STATUS_STEP_1;
			
			DPRINTF(DBG_MSG1,"PreCheckSleepMode CanNmAppLocalFucActReq %d \n\r",system_timer_tick);
		}
		system_timer_tick = GetTickCount();
	}

	//2. check can status 
	if(NM_STATE_BUS_SLEEP == ApplGet_NM_state()) // CHERY SPEC (NM)
	{
		//send ACC OFF command at once
		if(dev->r_System->power_on)
		{ 
			acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
			acc_packet.buf[1] =SYSTEM_MODE_SLEEP;
			acc_packet.buf[2] = 0u;
			acc_packet.buf[3] = 0u;
			acc_packet.trycnt =0;
			acc_packet.flag =ON;
			dev->r_System->s_state = SYS_SLEEP_MODE_CHECK;
			
			system_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s send sleep command %d\n\r",__FUNCTION__,system_timer_tick);
		 }
		 else
		 {
		 	system_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s direct sleep mode when soc turn off %d \n\r",__FUNCTION__,system_timer_tick);
		 	dev->r_System->s_state = SYS_SLEEP_MODE_ENTER;
		 }
		 e2p_sys_data.E2P_SONY= r_sonysound;
		 e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211216_1
		#ifdef CHERY_REQUSET_SYSTEM_TEST
		chery_request_timer_tick = GetTickCount();
		#endif
	}
	else
	{
		//wx20220812
		/*if(1000<=GetElapsedTime(system_timer_tick))
		{
			#ifdef VECTOR_CCL_TEST
			dev->r_System->ccl_active = OFF;
			//WL0714
			CclRel_Com_Request0();	//for adjust sync ccl off
			#endif
			system_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s ccl_active off %d\n\r",__FUNCTION__,system_timer_tick);
		}*/
	}

	//3. ACC DET ON -> standby mode
	if(dev->r_System->accign_state.state)
	{
		if(10<=GetElapsedTime(system_timer_tick1))
		{
			DPRINTF(DBG_MSG1,"%s	-> SYS_STANDBY_MODE_ENTER 1 \n\r",__FUNCTION__);
			dev->r_System->wakup_userdata = 0;
			dev->r_System->s_state =	SYS_STANDBY_MODE_ENTER;
			system_boot_tick = GetTickCount();
			system_timer_tick1 = GetTickCount();
			min15_timer_tick = GetTickCount();
			standby_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s	-> SYS_STANDBY_MODE_ENTER \n\r",__FUNCTION__);
		}
	}
	else
		system_timer_tick1= GetTickCount();

}

static void CheckSleepMode(DEVICE *dev)
{
	//1. forcy power off
	if(ACC_OFF_WAIT<=GetElapsedTime(system_timer_tick ))
	{ 
		dev->r_System->s_state = SYS_SLEEP_MODE_ENTER;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s	-> SYS_SLEEP_MODE_ENTER by forcy power off %d  \n\r",__FUNCTION__ ,system_timer_tick);
	}

	//2. ACC DET ON -> standby mode
	if((dev->r_System->accign_state.state))
	{
		if(10<=GetElapsedTime(system_timer_tick1))
		{
			dev->r_System->send_sleep_command = OFF;
			dev->r_System->s_state =	SYS_STANDBY_MODE_ENTER;
			system_boot_tick = GetTickCount();
			system_timer_tick1 = GetTickCount();
			min15_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s	-> SYS_STANDBY_MODE_ENTER \n\r",__FUNCTION__);
		}
	}
	else
		system_timer_tick1= GetTickCount();
	
	chery_request_timer_tick = GetTickCount();//add
}

static void MoveSleepMode(DEVICE *dev)
{	
	static uint32 cnt=0;
	//1. USB power off , CPU����
	//2. process soc power off
	if(dev->r_System->power_on)
	{
		//soc power on
		if(10<=GetElapsedTime(system_timer_tick))
		{		
			#ifdef CHERY_REQUSET_SYSTEM_TEST
			if((ACC_OFF_WAIT<=GetElapsedTime(chery_request_timer_tick))||shell_system_mode)
			cnt++;
			#else
			cnt++;
			#endif
			if(cnt==1)
			{
				dev->r_Power->p_state = PWR_PRE_OFF_1;
				#ifdef INTERNAL_AMP
				dev->r_Dsp->d_softmute_status = ON;
				MCU_AMP_MUTE(ON);	//Audio mute 
				#else
				//colin request 
				A2B_Sleep_Set(dev);
				#endif
				dev->r_Tuner->t_state = TUNER_IDLE;
				dev->r_Dsp->d_state = DSP_IDLE;
				DPRINTF(DBG_MSG1,"Enter MoveSleepMode \n\r");
			}
			//check ph_hold
			if(PS_HOLD()==OFF)
			{
				if(dev->r_Power->p_state == PWR_ON_ACTIVE)
				{
					//power off
					#ifdef B1_MOTHER_BOARD_SAMPLE
					USB_PW_EN(OFF);
					#endif
					dev->r_System->send_sleep_command = OFF;		
					dev->r_Power->p_state = PWR_OFF_1;
					dev->r_Tuner->t_state = TUNER_IDLE;
					dev->r_Dsp->d_state = DSP_IDLE;
				}
				else if(dev->r_Power->p_state == PWR_IDLE)
				{
					dev->r_System->s_state = SYS_SLEEP_MODE_IDLE;
					e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120
					cnt =0;
				}
			}
			else
			{
				if(dev->r_Power->p_state == PWR_ON_ACTIVE)
				{
					//wait 30sec
					if(ENTER_SLEEP_TIME<=cnt)
					{
						#ifdef B1_MOTHER_BOARD_SAMPLE
						USB_PW_EN(OFF);
						#endif
						dev->r_System->send_sleep_command = OFF;		
						dev->r_Power->p_state = PWR_OFF_1;
						dev->r_Tuner->t_state = TUNER_IDLE;
						dev->r_Dsp->d_state = DSP_IDLE;
					}
				}
				else if(dev->r_Power->p_state == PWR_IDLE)
				{
					dev->r_System->s_state = SYS_SLEEP_MODE_IDLE;
					e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120
					cnt =0;
				}
			}

			//exception DETECT PIN 
			if((dev->r_System->accign_state.state))
			{	
				DPRINTF(DBG_MSG1,"%s	-> SYS_STANDBY_MODE_ENTER ACC\n\r",__FUNCTION__);
				//PORTA
				DSP_PDN(OFF);
				MCU_AMP_ST_BY(OFF);
				MCU_AMP_MUTE(ON);
				#ifdef B1_MOTHER_BOARD_SAMPLE
				USB_PW_EN(OFF);
				#endif
				Control_LCD_Backlight(OFF);
				MCU_CONTROL_PMIC_ONOFF(OFF);
				M_MAIN_PWR_EN(OFF);
				PMIC_RESET_IN(OFF);
				TFT_PW_EN(OFF);	
				MCU_TFT2_PWR_EN(OFF);//MCU_TFT2_PWR_EN
//not used				MIC_PWR_EN_1(OFF);
				MIC_PWR_EN_2(OFF);
				//ATB_3V3_EN(OFF);
				M_5V_FRQ_SEL(OFF);
				GPS_MD_PW_EN(OFF);
				#if 0 //def SI_LABS_TUNER
				SiLabs_Tuner_RST(OFF);
				//#else
				IRLED_CTL(OFF);
				#endif
				DSP_PW_EN(OFF);//DSP_PW_EN
				BAT_ADC_ON(OFF);//BAT_ADC_ON
				M_5V_EN(OFF);
				#ifdef CHANG_BL_TIMING
				if(dev->r_System->blpwmenable)//JANG_211025 
				{
					FTM3_deinit();
					dev->r_System->blpwmenable =OFF;
				}
				#else
				FTM3_deinit();
				#endif
				sys_deinit_fnc();
				#ifdef WATCHDOG_ENABLE
				WDOG_DRV_Deinit(INST_WATCHDOG1);
				#endif
				SetTickCount(0);//tick_clear
				INIT_SYSTEM();
				INIT_GR_DATA();
				dev->r_Tuner->t_state = TUNER_IDLE;
				dev->r_Dsp->d_state = DSP_IDLE;
				dev->r_System->s_state = SYS_CHECK_DELAY_2;
				dev->r_System->init_ok = OFF;
				dev->r_System->power_on = OFF;
			//	dev->r_System->send_sleep_command = OFF;		
				dev->r_Dsp->d_softmute_status = OFF;
				dev->r_Dsp->d_goto_mute_status = OFF;
				
				DPRINTF(DBG_MSG1,"abnormal wakeup USB_PW_EN : OFF \n\r");
				
				dev->r_System->uart_ok = OFF;
				shell_system_mode = OFF;
				cnt =0;
			}
			else if((NM_STATE_NORMAL_OPERATION == ApplGet_NM_state())||(NM_STATE_REPEAT_MESSAGE == ApplGet_NM_state()))
			{	
				DPRINTF(DBG_MSG1,"%s	-> SYS_STANDBY_MODE_ENTER NM\n\r",__FUNCTION__);
				//PORTA
				DSP_PDN(OFF);
				MCU_AMP_ST_BY(OFF);
				MCU_AMP_MUTE(ON);
				#ifdef B1_MOTHER_BOARD_SAMPLE
				USB_PW_EN(OFF);
				#endif
				Control_LCD_Backlight(OFF);
				MCU_CONTROL_PMIC_ONOFF(OFF);
				M_MAIN_PWR_EN(OFF);
				PMIC_RESET_IN(OFF);
				TFT_PW_EN(OFF);	
				MCU_TFT2_PWR_EN(OFF);//MCU_TFT2_PWR_EN
				MIC_PWR_EN_2(OFF);
				//ATB_3V3_EN(OFF);
				M_5V_FRQ_SEL(OFF);
				GPS_MD_PW_EN(OFF);
				#if 0 //ndef SI_LABS_TUNER
				IRLED_CTL(OFF);
				#endif
				DSP_PW_EN(OFF);//DSP_PW_EN
				BAT_ADC_ON(OFF);//BAT_ADC_ON
				M_5V_EN(OFF);
				#ifdef CHANG_BL_TIMING
				if(dev->r_System->blpwmenable)//JANG_211025
				{
					FTM3_deinit();
					dev->r_System->blpwmenable =OFF;
				}
				#else
				FTM3_deinit();
				#endif
				sys_deinit_fnc();
				#ifdef WATCHDOG_ENABLE
				WDOG_DRV_Deinit(INST_WATCHDOG1);
				#endif
				INIT_SYSTEM();
				INIT_GR_DATA();
				dev->r_Tuner->t_state = TUNER_IDLE;
				dev->r_Dsp->d_state = DSP_IDLE;
				dev->r_System->wakup_userdata = 0;
				dev->r_System->s_state = SYS_CHECK_DELAY_2;
				dev->r_System->init_ok = OFF;
				dev->r_System->power_on = OFF;
				dev->r_Dsp->d_softmute_status = OFF;
				dev->r_Dsp->d_goto_mute_status = OFF;
				
				DPRINTF(DBG_MSG1,"abnormal can wakeup \n\r");
				
				dev->r_System->uart_ok = OFF;
				shell_system_mode = OFF;
				cnt =0;
			}

			system_timer_tick = GetTickCount();
		}	
	}
	else
	{
		if((NM_STATE_BUS_SLEEP == ApplGet_NM_state()))
		{
		#if 1
		if(dev->r_Power->p_state != PWR_IDLE)
			dev->r_Power->p_state = PWR_OFF_1;
		#endif
			DPRINTF(DBG_MSG1,"NM_STATE_BUS_SLEEP \n\r");
			dev->r_System->s_state = SYS_SLEEP_MODE_IDLE;
			e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120
		}
		else
		{
			dev->r_System->wakup_userdata = 0;
			dev->r_System->s_state = SYS_CHECK_DELAY_2;
		}
	}

}

static void StaySleepMode(DEVICE *dev)
{
	bool boWakeup=false;
	if((dev->r_Power->p_state==PWR_IDLE)&&(NM_STATE_BUS_SLEEP == ApplGet_NM_state())) // CHERY SPEC (NM))
	{
		u8TestSleepCmd = 0u;
		#ifdef VECTOR_CCL_TEST
		dev->r_System->usrdata1 &=(~(NETWORK_WAKEUP|IGN_WAKEUP));	//clear flag when go to sleep mode
		//TX_RRM_NM_USR_DATA1(dev->r_System->usrdata1);
		#endif
		#if 1
		MCU_CAN0_EN(OFF);
		MCU_CAN0_STB(OFF);
		INT_SYS_EnableIRQ(PORTC_IRQn);
		nm_firstframe_cnt = 0;
		diag_firstframe_cnt = 0;
		#endif

		#ifdef B1_MOTHER_BOARD_SAMPLE
		MCU_HU_LCD_EN(OFF);
		#endif
		MCU_LCD1_2_EN_3P3(OFF);
		#ifdef CHANG_BL_TIMING
		if(dev->r_System->blpwmenable)//JANG_211025 
		{
			FTM3_deinit();
			dev->r_System->blpwmenable =OFF;
		}
		#else
		FTM3_deinit();
		#endif
		sys_deinit_fnc();
		#ifdef WATCHDOG_ENABLE
		WDOG_DRV_Deinit(INST_WATCHDOG1);
		#endif
		//if(MCU_ACC_DET()||MCU_IGN_DET())
		if(MCU_ACC_DET())
		{
			boWakeup=true;
		}
		else
		{
			boWakeup=false;
			MCU_CAN0_EN(ON);
			Go_To_Sleep();
		}
		//exit sleepmode
		SetTickCount(0);//tick_clear
		INIT_SYSTEM();
		INIT_GR_DATA();
		if (boWakeup)
		{
			DPRINTF(DBG_MSG1,"hold wakeup\n\r");
		}
		else
		{
			u8cansendrequest=3u;
			DPRINTF(DBG_MSG1,"normal wakeup\n\r");
		}
		dev->r_System->init_ok = OFF;
		dev->r_System->uart_ok = OFF;
		dev->r_System->power_on = OFF;
		dev->r_Dsp->d_softmute_status = OFF;
		dev->r_Dsp->d_goto_mute_status = OFF;
		#ifndef VECTOR_CCL_TEST
		dev->r_System->s_state = SYS_CHECK_DELAY_1;			//200ms
		#else
		//TX_RRM_NM_USR_DATA1(IGN_WAKEUP|IGN_AWAKE);//ign default value
		dev->r_System->s_state = SYS_CHECK_DELAY_2;
		#endif
		shell_system_mode = OFF;

		system_boot_tick = GetTickCount();
		system_timer_tick = GetTickCount();
		system_timer_tick1 = GetTickCount();
		standby_timer_tick = GetTickCount();
		sleep_timer_tick = GetTickCount();
		min15_timer_tick = GetTickCount();

		lowbatt_timer_tick = GetTickCount();
		lowbatt_timer_tick1 = GetTickCount();
		highbatt_timer_tick = GetTickCount();
		highbatt_can_timer_tick = GetTickCount();
		batter_adc_timer_tick = GetTickCount();
		batter_adc_timer_tick1 = GetTickCount();
		batter_adc_can_timer_tick = GetTickCount();
		voltage_dtc_timer_tick= GetTickCount();
		system_timer_tick = GetTickCount();
		/* wl20220809 */
		nm_start_timer_tick = GetTickCount();
		/* wl20220810 */
		ig_start_timer_tick = GetTickCount();
	}
	else if((dev->r_Power->p_state!= PWR_ON_ACTIVE)&&(PWR_OFF_7<dev->r_Power->p_state))
	{
		//excute power off when it isn't power sequece mode
		dev->r_Power->p_state = PWR_OFF_1;
		system_timer_tick = GetTickCount();
	}
	else if(NM_STATE_BUS_SLEEP != ApplGet_NM_state())
	{
		//rebooting
		SetTickCount(0);//tick_clear
		INIT_SYSTEM();
		INIT_GR_DATA();
		DPRINTF(DBG_MSG1,"NM_STATE_BUS_SLEEP wakeup  %d\n\r", ApplGet_NM_state());
		dev->r_System->init_ok = OFF;
		dev->r_System->uart_ok = OFF;
		dev->r_System->power_on = OFF;
		dev->r_Dsp->d_softmute_status = OFF;
		dev->r_Dsp->d_goto_mute_status = OFF;
		dev->r_System->s_state = SYS_CHECK_DELAY_2;
		#ifdef VECTOR_CCL_TEST
//		r_Device.r_System->ccl_active = ON;
		r_Device.r_System->prev_ccl_active = OFF;
		#endif
		shell_system_mode = OFF;
		system_timer_tick = GetTickCount();
		system_timer_tick1 = GetTickCount();
		standby_timer_tick = GetTickCount();
		sleep_timer_tick = GetTickCount();
		min15_timer_tick = GetTickCount();

		lowbatt_timer_tick = GetTickCount();
		lowbatt_timer_tick1 = GetTickCount();
		highbatt_timer_tick = GetTickCount();
		highbatt_can_timer_tick = GetTickCount();
		batter_adc_timer_tick = GetTickCount();
		batter_adc_timer_tick1 = GetTickCount();
		batter_adc_can_timer_tick = GetTickCount();
		voltage_dtc_timer_tick= GetTickCount();
		system_timer_tick = GetTickCount();
		system_timer_tick = GetTickCount();
	}
	else
		system_timer_tick = GetTickCount();
		
}

static void CheckStandbyMode(DEVICE *dev)
{
	// 1.MCU��SOC ON
	//Screen OFF��Audio Mute
	//USB power ON��Sleep mode���� Stand by mode�� ���� ��� �ܿ��� USB Powr OFF)
	
	if(5<=GetElapsedTime(system_timer_tick))
	{
		system_timer_tick = GetTickCount();
		if((dev->r_System->uart_ok)&&(dev->r_System->send_standby_command == OFF))
	 	{
	 		acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
			acc_packet.buf[1] = SYSTEM_MODE_STANDBY;	//
			acc_packet.buf[2] = 0u;
			acc_packet.buf[3] = 0u;
			acc_packet.trycnt = 0;
			acc_packet.flag = ON;
			dev->r_System->send_standby_command = ON;
			system_timer_tick1 = GetTickCount();
			DPRINTF(DBG_MSG1,"%s send acc standby command %d\n\r",__FUNCTION__,system_timer_tick);
		}

		//no response -> standby move
		if(ACC_NO_RESPONSE<=GetElapsedTime(system_timer_tick1))
		{
			system_timer_tick1 = GetTickCount();
			DPRINTF(DBG_MSG1,"%s no response %d\n\r",__FUNCTION__,system_timer_tick);
			dev->r_System->send_standby_command = OFF;
			dev->r_System->s_state = SYS_STANDBY_MODE_ENTER;
		}
	}

}

static void MoveStandbyMode(DEVICE *dev)
{
	// 1.MCU��SOC ON
	//Screen OFF��Audio Mute
	//USB power ON��Sleep mode���� Stand by mode�� ���� ��� �ܿ��� USB Powr OFF)
	if(dev->r_System->init_ok== OFF)
	{
		 //RRM1, RRM 7 invalid data
//		Set_Init_RRM_1();
//		Set_Init_RRM_7();
//		Set_Init_RRM_8();
		#ifdef VECTOR_CCL_TEST
		if(dev->r_System->wakup_userdata==0)
		{
			system_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"init user data setting  %d %d \n\r",system_timer_tick, dev->r_System->can_wakeup);
			//check nm signal
			if(dev->r_System->can_wakeup)
			{

					dev->r_System->usrdata1 |=NETWORK_WAKEUP;
					dev->r_System->usrdata1 &=(~(IGN_WAKEUP|ECUSPEC_WAKEUP));

				dev->r_System->s_state = SYS_DELAY_200MS_FOR_CAN;
			}
			else
			{
				dev->r_System->usrdata1 |=IGN_WAKEUP;
				dev->r_System->usrdata1 &=(~(NETWORK_WAKEUP|ECUSPEC_WAKEUP));
				dev->r_System->s_state_lcd_state = SYS_STANDBY_MODE_IDLE;
				dev->r_System->s_state = SYS_DELAY_100MS_FOR_CAN;;
			}
			//TX_RRM_NM_USR_DATA1(dev->r_System->usrdata1);
				//DPRINTF(DBG_MSG1,"%s  TX_RRM_NM_USR_DATA1, USB_PW_EN : ON \n\r",__FUNCTION__);
			dev->r_System->can_wakeup =0;
			dev->r_System->wakup_userdata = 1;
		}
		else
			dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
		#else
		dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
		#endif
	}
	else
	{
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_GOTO_MUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_GOTO_MUTE_1);			//SOUND mute
		#endif
		DPRINTF(DBG_MSG1,"%s  AUDIO mute, USB_PW_EN : ON \n\r",__FUNCTION__);	
	}
	dev->r_System->s_state_lcd_state = SYS_STANDBY_MODE_IDLE;
	dev->r_System->send_standby_command = OFF;
	dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
	system_timer_tick = GetTickCount();
	standby_timer_tick = GetTickCount();
	system_boot_tick = GetTickCount(); 
		

	DPRINTF(DBG_MSG1,"%s %d\n\r",__FUNCTION__,system_timer_tick);
}

static void StayStandbyMode(DEVICE *dev)
{
//	uint8 tmp8=0;	
	//wait SOC POWER ON
	//DET_ACC_IGN()  	//on/off
	if(dev->r_System->init_ok ==OFF)
	{
		//first boot  //100ms
		if(dev->r_System->accign_state.state)
		{
			if(PWR_ON_14<=dev->r_Power->p_state)	//POWER ON
			{	
				system_timer_tick = GetTickCount();
				min15_timer_tick = GetTickCount();
				dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
				dev->r_System->s_state_lcd_state = SYS_ACTIVE_MODE_IDLE;
				DPRINTF(DBG_MSG1,"%s	-> SYS_ACTIVE_MODE_CHECK \n\r",__FUNCTION__);
			//CannmAppClrSleepFlg(); // CHERY SPEC (NM)
			}
			else if(dev->r_Power->p_state == PWR_IDLE) 
			{	
				#ifdef CHERY_REQUSET_SYSTEM_TEST
				dev->r_System->power_on = ON;
				#endif			
				dev->r_Power->p_state = PWR_ON_1;
				system_timer_tick = GetTickCount();
				DPRINTF(DBG_MSG1,"active mode power on %d\n\r",system_timer_tick);
			}
			#ifdef VECTOR_CCL_TEST
			/* 20220808 */
			//dev->r_System->ccl_active = ON;
			#endif

		}
		else
		{
			//wx20220812
			/*if(CCL_RELASE_TIME<=GetElapsedTime(system_boot_tick))
			{
				system_boot_tick = GetTickCount(); 
				DPRINTF(DBG_MSG1,"state %d CCL_RELASE_TIME1 %d\n\r",dev->r_System->ccl_active,system_boot_tick);
				#ifdef VECTOR_CCL_TEST
				dev->r_System->ccl_active = OFF;
				#endif
			}
			else*/ if(INIT_BOOT_TIME<=GetElapsedTime(system_boot_tick))
			{
				if(dev->r_Power->p_state == PWR_IDLE) 
				{
					dev->r_Power->p_state = PWR_ON_1;
					#ifdef CHERY_REQUSET_SYSTEM_TEST
					dev->r_System->power_on = ON;				// standby mode soc power on
					#endif
//					DPRINTF(DBG_MSG1,"standby mode power on %d\n\r",system_timer_tick);
				}
				else if(dev->r_Power->p_state==PWR_ON_14)
				{
					#ifndef CHERY_REQUSET_SYSTEM_TEST
					dev->r_System->power_on = ON;				// standby mode soc power on
					#endif
				}
			}
			
			//normal standby mod
			if(shell_system_mode)
			{
				#if 0
				if(STADNBY_MODE_TIME1<=GetElapsedTime(standby_timer_tick))
				{
					if(NM_STATE_BUS_SLEEP == ApplGet_NM_state())
					{
						dev->r_System->s_state = SYS_SLEEP_MODE_PRE_CHECK;
						standby_timer_tick = GetTickCount();
						DPRINTF(DBG_MSG1,"%s -> SYS_SLEEP_MODE_PRE_CHECK \n\r",__FUNCTION__);
					}
					//check release ccl
					if(dev->r_System->ccl_active)
						dev->r_System->ccl_active =OFF;
				}
				#else
				//wx20220831
				if(STADNBY_MODE_TIME1<=GetElapsedTime(standby_timer_tick)||
					#if 0
					(NM_STATE_BUS_SLEEP == ApplGet_NM_state())
					#else
					(INIT_BOOT_TIME<=GetElapsedTime(system_boot_tick)&&(NM_STATE_BUS_SLEEP == ApplGet_NM_state()))
					#endif
					) // CHERY SPEC (NM)
				{
					dev->r_System->s_state = SYS_SLEEP_MODE_PRE_CHECK;
					standby_timer_tick = GetTickCount();
					DPRINTF(DBG_MSG1,"%s  -> SYS_SLEEP_MODE_PRE_CHECK %d\n\r",__FUNCTION__,standby_timer_tick);
				}
				#endif
			}
			else
			{
				#if 0
				if(STADNBY_MODE_TIME<=GetElapsedTime(standby_timer_tick))
				{
					if(NM_STATE_BUS_SLEEP == ApplGet_NM_state())
					{
						dev->r_System->s_state = SYS_SLEEP_MODE_PRE_CHECK;
						standby_timer_tick = GetTickCount();
						DPRINTF(DBG_MSG1,"%s  -> SYS_SLEEP_MODE_PRE_CHECK \n\r",__FUNCTION__);
					}
					//check release ccl
					if(dev->r_System->ccl_active)
						dev->r_System->ccl_active =OFF;
				}
				#else
				//wx20220831
				if(STADNBY_MODE_TIME<=GetElapsedTime(standby_timer_tick)||
					#if 0
					(NM_STATE_BUS_SLEEP == ApplGet_NM_state())
					#else
					(INIT_BOOT_TIME<=GetElapsedTime(system_boot_tick)&&(NM_STATE_BUS_SLEEP == ApplGet_NM_state()))
					#endif
					) // CHERY SPEC (NM)
				{
					dev->r_System->s_state = SYS_SLEEP_MODE_PRE_CHECK;
					standby_timer_tick = GetTickCount();
					DPRINTF(DBG_MSG1,"%s  -> SYS_SLEEP_MODE_PRE_CHECK \n\r",__FUNCTION__);
				}
				#endif
			}	
			system_timer_tick = GetTickCount();
		}
	}
	else
	{
		if(dev->r_System->accign_state.state)
		{
			if(dev->r_Power->p_state == PWR_ON_ACTIVE) 	//POWER ON
			{
				//check after 3ms , to check noise //move to mode
				if(3<=GetElapsedTime(system_timer_tick))
				{
					dev->r_System->canmn_start_flag =ON;
					//goto screensaver mode	
					if(dev->r_System->s_state_backup == SYS_SCREEN_SAVE_MODE_IDLE)
					{
						dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_CHECK;
						DPRINTF(DBG_MSG1,"%s  -> SYS_SCREEN_SAVE_MODE_CHECK \n\r",__FUNCTION__);
					}
					else
					{
						//goto active mode
						dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
						DPRINTF(DBG_MSG1,"%s  -> SYS_ACTIVE_MODE_CHECK \n\r",__FUNCTION__);
					}
					min15_timer_tick = GetTickCount();
					system_timer_tick = GetTickCount();
				}
			}
			#ifdef VECTOR_CCL_TEST
			/* 20220808 */
			// dev->r_System->ccl_active = ON;
			#endif
		}
		else
		{
			//wx20220812
			/*if(CCL_RELASE_TIME<=GetElapsedTime(system_boot_tick))
			{
				#ifdef VECTOR_CCL_TEST
				dev->r_System->ccl_active = OFF;
				#endif
				system_boot_tick = GetTickCount(); 
				DPRINTF(DBG_MSG1,"state %d CCL_RELASE_TIME2 %d\n\r",dev->r_System->ccl_active,system_boot_tick);
			}
			else*/ if(INIT_BOOT_TIME<=GetElapsedTime(system_boot_tick))
			{
				if(dev->r_Power->p_state == PWR_IDLE) 
				{
					dev->r_Power->p_state = PWR_ON_1;
					DPRINTF(DBG_MSG1,"standby mode power on %d\n\r",system_timer_tick);
					#ifdef CHERY_REQUSET_SYSTEM_TEST
					dev->r_System->power_on = ON; 			// standby mode soc power on
					#endif

				}
				else if(dev->r_Power->p_state==PWR_ON_14)
				{
					#ifndef CHERY_REQUSET_SYSTEM_TEST
					dev->r_System->power_on = ON; 			// standby mode soc power on
					#endif
				}
			}
			
			//normal standby mod
			if(shell_system_mode)
			{
				if(STADNBY_MODE_TIME1<=GetElapsedTime(standby_timer_tick) || (NM_STATE_BUS_SLEEP == ApplGet_NM_state())) // CHERY SPEC (NM)
				{
					dev->r_System->s_state = SYS_SLEEP_MODE_PRE_CHECK;
					standby_timer_tick = GetTickCount();
					DPRINTF(DBG_MSG1,"%s  -> SYS_SLEEP_MODE_PRE_CHECK %d\n\r",__FUNCTION__,standby_timer_tick);
				}
			}
			else
			{
				if(STADNBY_MODE_TIME<=GetElapsedTime(standby_timer_tick) || (NM_STATE_BUS_SLEEP == ApplGet_NM_state())) // CHERY SPEC (NM)
				{
					dev->r_System->s_state = SYS_SLEEP_MODE_PRE_CHECK;
					standby_timer_tick = GetTickCount();
					DPRINTF(DBG_MSG1,"%s  -> SYS_SLEEP_MODE_PRE_CHECK %d\n\r",__FUNCTION__,standby_timer_tick);
				}
			}
			system_timer_tick = GetTickCount();
		}
	}
}

static void CheckActiveMode(DEVICE *dev)
{	
	#if 0
	if((GPIO_CONTROL()==OFF)&&(dev->r_Power->p_state<PWR_ON_14))		//finish power seqence and uart ok 
		return;
	#else
	if(dev->r_System->uart_ok==OFF)
	{
		//soc is gpio high
		if(((GPIO_CONTROL()==ON)||(PWR_ON_14<dev->r_Power->p_state))&&(dev->r_System->boot_seq1==OFF))
		{
			DPRINTF(DBG_MSG1,"system init lcd on \n\r");
			#ifdef CHANG_BL_TIMING			// if it dosen't prepare pwm init, it need to pwm init 
			if(dev->r_System->blpwmenable==OFF)//JANG_211025 
			{
				PWM_init();
				dev->r_System->blpwmenable =ON;
			}
			#endif
			Control_LCD_Backlight_for_Mengbo(dev,ON);		//LCD ON
			
			dev->r_System->boot_seq1= ON;
		}
		return;
	}
	else
		dev->r_System->boot_seq1=OFF;
	#endif
				
	if(dev->r_System->send_active_command == OFF)
	{
		acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
		acc_packet.buf[1] = SYSTEM_MODE_ACTIVE;
		acc_packet.buf[2] = 0u;
		acc_packet.buf[3] = 0u;
		acc_packet.trycnt = 0;
		acc_packet.flag = ON;
//		IlPutTxRRMOnSts(1);

		// clear
		dev->r_System->power_key_status = POWER_KEY_IDLE;
		dev->r_System->ask_screen_off_mode= OFF;
		dev->r_System->ask_screen_save_mode= OFF;
		
		dev->r_System->send_active_command =ON;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s send acc active command %d\n\r",__FUNCTION__,system_timer_tick);
	}

	//no response
	if(ACC_NO_RESPONSE<=GetElapsedTime(system_timer_tick))
	{
		dev->r_System->send_active_command = OFF;
		#if 1		//this is  for communiction error
		if(dev->r_System->init_ok == OFF)
			dev->r_System->s_state = SYS_ACTIVE_MODE_ENTER;  //first booting ->goto active mode
		else 
		#endif
		if(dev->r_System->s_state_backup == SYS_SCREENOFF_1_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_SCREENOFF_1_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_active++;
			#endif
		}
		else if(dev->r_System->s_state_backup == SYS_SCREEN_SAVE_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_active++;
			#endif
		}
		else if(dev->r_System->s_state_backup == SYS_15MIN_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_15MIN_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_active++;
			#endif
		}
		else 
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_active++;
			#endif
		}
		//check no response counter
		#ifdef CHECK_NO_RESPONSE
		if(12<dev->r_System->no_response_active)
		{
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			DPRINTF(DBG_ERR,"no response SystemSoftwareReset 12\n\r");
			RecordReset(12,NULL);
			RESET_MCU(dev);
		}
		else if(10<dev->r_System->no_response_active)
		{
			uart0_deinit();
			uart1_deinit();
			init_dev_uart();
			DPRINTF(DBG_ERR,"no response uart reset \n\r");
		}
		#endif
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s no response %d\n\r",__FUNCTION__,system_timer_tick);
	}
}

static void MoveActiveMode(DEVICE *dev)
{	
	if(dev->r_System->s_state_lcd_state != SYS_SCREENOFF_1_MODE_IDLE)
	{
		if(dev->r_Dsp->d_mix_status==OFF)
			#ifdef DSP_DOUBLE_QUE
			q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL,(uint8) dev->r_Dsp->d_audo_mode);
			#else
			q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL);			//volume recovery
			#endif
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_GOTO_UNMUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_GOTO_UNMUTE_1);
		#endif
	}
	
	#if 1
	Check_Communcte_CAN_status();
	#endif
	#ifdef CHECK_NO_RESPONSE
	dev->r_System->no_response_active=0;
	#endif
	dev->r_System->init_ok = ON;
	dev->r_System->s_state = SYS_ACTIVE_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_ACTIVE_MODE_IDLE;
	dev->r_System->key_active = OFF;
	dev->r_System->send_active_command = OFF;	
	dev->r_System->ask_screen_off_mode = OFF;
	DPRINTF(DBG_MSG1,"%s LCD ON, AUIDO ON \n\r",__FUNCTION__);

}

static void StayActiveMode(DEVICE *dev)
{
	//goto screen off mode
	//goto standby mode
	//goto screensaver mode	
	//goto 15min mode
	if(dev->r_System->accign_state.state==OFF)
	{
		//check after 3ms , to check noise //goto standby mode
		if(500<=GetElapsedTime(system_timer_tick))
		{
			if((DriverDoorOpen()== ON)||KeyFortification()==ON)
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
				dev->r_System->s_state_backup = SYS_ACTIVE_MODE_IDLE;
				DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			}
			else if(DriverDoorOpen()== OFF)
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
				dev->r_System->s_state_backup = SYS_ACTIVE_MODE_IDLE;
				DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			}
			system_timer_tick = GetTickCount();
		}
	}
	else
	{
		system_timer_tick = GetTickCount();
		min15_timer_tick = GetTickCount();
		
		//change 201223
#if 1
		if(dev->r_System->incomecalling||dev->r_System->rvcavm_active/*R_Gear_Operate()*/)
#else
#endif
		{
			if((dev->r_System->power_key_status==POWER_KEY_SHORT)||(dev->r_System->power_key_status==POWER_KEY_LONG))
				dev->r_System->power_key_status = POWER_KEY_IDLE;
			if(dev->r_System->ask_screen_off_mode)
				dev->r_System->ask_screen_off_mode = OFF;
			return;
		}
		
		if(dev->r_System->power_key_status==POWER_KEY_SHORT)
		{
			dev->r_System->power_key_status = POWER_KEY_IDLE;
			dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_ACTIVE_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_SCREEN_SAVE_MODE_CHECK by POWER_KEY_SHORT  \n\r",__FUNCTION__);
		}
		else if(dev->r_System->power_key_status==POWER_KEY_LONG)
		{
			dev->r_System->power_key_status = POWER_KEY_IDLE;
			dev->r_System->s_state = SYS_SCREENOFF_1_MODE_CHECK;	
			dev->r_System->s_state_backup = SYS_ACTIVE_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_SCREENOFF_1_MODE_CHECK by POWER_KEY_LONG  \n\r",__FUNCTION__);
		}

		if(dev->r_System->ask_screen_save_mode==ON)
		{
			dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_CHECK;		
			dev->r_System->s_state_backup = SYS_ACTIVE_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_SCREEN_SAVE_MODE_CHECK by ask_screen_save  \n\r",__FUNCTION__);
		}		

		if(dev->r_System->ask_screen_off_mode==ON)
		{
			dev->r_System->s_state = SYS_SCREENOFF_1_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_ACTIVE_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_SCREENOFF_1_MODE_CHECK by ask_screen_off  \n\r",__FUNCTION__);
		}	
	}	
}

static void CheckScreenOff1Mode(DEVICE *dev)
{ 
	if(dev->r_System->send_screenoff_1_command == OFF)
	{
		acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
		acc_packet.buf[1] = 0u;
		acc_packet.buf[2] = 0u;
		acc_packet.buf[3] = 1u;
		acc_packet.trycnt =0;
		acc_packet.flag =ON;

		//clear
		dev->r_System->ask_screen_off_mode= OFF;
	
		dev->r_System->send_screenoff_1_command =ON;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s send acc screen off command %d\n\r",__FUNCTION__,system_timer_tick);
	}

	//no response
	if(ACC_NO_RESPONSE<=GetElapsedTime(system_timer_tick))
	{
		dev->r_System->send_screenoff_1_command = OFF;
		if(dev->r_System->s_state_backup == SYS_ACTIVE_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_screenoff++;
			#endif
		}
		else if(dev->r_System->s_state_backup == SYS_15MIN_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_15MIN_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_screenoff++;
			#endif
		}
		system_timer_tick = GetTickCount();
		//check no response counter
		#ifdef CHECK_NO_RESPONSE
		if(12<dev->r_System->no_response_screenoff)
		{
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			DPRINTF(DBG_ERR,"no response SystemSoftwareReset 3\n\r");
			e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
			save_last_memory();
			RecordReset(3,NULL);
			delay_ms(100);
			SystemSoftwareReset();
		}
		else if(10<dev->r_System->no_response_screenoff)
		{
			uart0_deinit();
			uart1_deinit();
			init_dev_uart();
			DPRINTF(DBG_ERR,"no response uart reset \n\r");
		}
		else
			DPRINTF(DBG_MSG1,"%s no response  return mode:%d\n\r",__FUNCTION__,dev->r_System->s_state_backup);
		#endif
	}
}

static uint8 gCheckIgnAccState =0;
static void MoveScreenOff1Mode(DEVICE *dev)
{
	if(dev->r_Power->p_state == PWR_ON_ACTIVE)
	{
		#ifdef CHECK_NO_RESPONSE
		dev->r_System->no_response_screenoff =0;
		#endif
		dev->r_System->s_state = SYS_SCREENOFF_1_MODE_IDLE;
		dev->r_System->key_active = OFF;
		dev->r_System->ask_screen_off_mode = OFF;
		dev->r_System->touch_active = OFF;
		dev->r_System->send_screenoff_1_command = OFF;		//send command off
		dev->r_System->s_state_lcd_state = SYS_SCREENOFF_1_MODE_IDLE;

		DPRINTF(DBG_MSG1,"%s LCD OFF \n\r",__FUNCTION__);
	}
	gCheckIgnAccState = dev->r_System->accign_state.state;
}

static void StayScreenOff1Mode(DEVICE *dev)
{
	//goto active mode
	//change 201223
	#if 1
	if(dev->r_System->incomecalling||dev->r_System->rvcavm_active/*R_Gear_Operate()*/||dev->r_System->touch_active||dev->r_System->key_active)
	#else
	#endif
	{
		dev->r_System->touch_active = OFF;
		
		//reset key
		if(dev->r_System->power_key_status==POWER_KEY_REPEAT)
		{
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			DPRINTF(DBG_ERR,"SystemSoftwareReset	9\n\r");
			dev->r_System->power_key_status = POWER_KEY_IDLE;
			e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
			save_last_memory();
			RecordReset(9,NULL);
			delay_ms(100);
			SystemSoftwareReset();
		}

		dev->r_System->power_key_status= POWER_KEY_IDLE;
		
		if(dev->r_System->accign_state.state)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
			
			DPRINTF(DBG_MSG1,"%s -> SYS_ACTIVE_MODE_CHECK \n\r",__FUNCTION__);
		}
		else
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;

			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
		}
		
		dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
		system_timer_tick = GetTickCount();
	}
	
	//goto standby mode
//	if(DET_ACC_IGN()==OFF)
	if(dev->r_System->accign_state.state ==OFF)
	{
		if(shell_system_mode)
		{
			if(((MIN15_MODE_TIME1<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
			/*&&(dev->r_System->incomecalling ==OFF)*/)
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
				dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
				min15_timer_tick = GetTickCount();
			}
		}
		else
		{
			if(((MIN15_MODE_TIME<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
			/*&&(dev->r_System->incomecalling ==OFF)*/)
			{
				dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
				dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
				min15_timer_tick = GetTickCount();
			}
		}
		
	}
	else
	{
		system_timer_tick = GetTickCount();
		min15_timer_tick = GetTickCount();
	}

	if(dev->r_System->ask_screen_off_mode==0x02)
	{
		if(dev->r_System->accign_state.state)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
			DPRINTF(DBG_MSG1,"%s -> SYS_ACTIVE_MODE_CHECK by ask_screen_off \n\r",__FUNCTION__);
		}
		else
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
		}
		
		dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
		system_timer_tick = GetTickCount();
	}

	//check to IGN //ACC
	if(gCheckIgnAccState != dev->r_System->accign_state.state)
	{
		switch((gCheckIgnAccState&0x3))
		{
			case 0://ign off,off
				if(dev->r_System->accign_state.state)
				{
					dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
					dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;

					system_timer_tick = GetTickCount();
					DPRINTF(DBG_MSG1,"%s ->change IGN//ACC %d\n\r",__FUNCTION__,(gCheckIgnAccState&0x3));
				}
				break;
			case 1://acc on		
				if(dev->r_System->accign_state.state&0x02)//IGN ON
				{
					dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
					dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				}
				else if((dev->r_System->accign_state.state&0x01)==OFF)		//ACC OFF
				{
					dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
					dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				}
				system_timer_tick = GetTickCount();
				DPRINTF(DBG_MSG1,"%s ->change IGN//ACC %d\n\r",__FUNCTION__,(gCheckIgnAccState&0x3));
				break;
			case 2://ign on
				if(dev->r_System->accign_state.state&0x01)		//ACC ON
				{
					dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
					dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				}
				else if((dev->r_System->accign_state.state&0x02)==OFF)//IGN OFF
				{
					dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
					dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				}
				system_timer_tick = GetTickCount();
				DPRINTF(DBG_MSG1,"%s ->change IGN//ACC %d\n\r",__FUNCTION__,(gCheckIgnAccState&0x3));
				break;
			case 3://ign on, acc on
				if(dev->r_System->accign_state.state==OFF)		//ACC OFF, IGN OFF
				{
					dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
					dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
				}
				break;
		}

		gCheckIgnAccState = dev->r_System->accign_state.state;
	}
	
	#if 0
	//goto screenoff_2
	if(dev->r_System->accign_state.state==OFF)
	{
		//check after 3ms , to check noise //goto standby mode
		if(3<=GetElapsedTime(system_timer_tick)){
			dev->r_System->s_state = SYS_SCREENOFF_2_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_SCREENOFF_1_MODE_IDLE;
			dev->r_System->s_state_blstate = SYS_SCREENOFF_1_MODE_IDLE;
			system_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s -> SYS_SCREENOFF_2_MODE_CHECK \n\r",__FUNCTION__);
		}
	}
	else 
		system_timer_tick = GetTickCount();
	#endif

}

static void CheckScreenOff2Mode(DEVICE *dev)
{ 
	if(dev->r_System->send_screenoff_2_command == OFF)
	{
		acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
		acc_packet.buf[1] = 0u;
		acc_packet.buf[2] = 0u;
		acc_packet.buf[3] = 1u;
		acc_packet.trycnt =0;
		acc_packet.flag =ON;
//		CanIfSet_gCanIf_RRM_2_RRMOnSts(1);

		//clear
		dev->r_System->ask_screen_off_mode = OFF;
		dev->r_System->send_screenoff_2_command =ON;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s send acc screen off command %d\n\r",__FUNCTION__,system_timer_tick);
	}

	//no response
	if(ACC_NO_RESPONSE<=GetElapsedTime(system_timer_tick))
	{
		dev->r_System->send_screenoff_2_command = OFF;
		if(dev->r_System->s_state_backup == SYS_SCREENOFF_1_MODE_IDLE)
			dev->r_System->s_state = SYS_SCREENOFF_1_MODE_IDLE;
		else if(dev->r_System->s_state_backup == SYS_15MIN_MODE_IDLE)
			dev->r_System->s_state = SYS_15MIN_MODE_IDLE;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s no response %d\n\r",__FUNCTION__,system_timer_tick);
		
	}
}

static void MoveScreenOff2Mode(DEVICE *dev)
{
	if(dev->r_Power->p_state == PWR_ON_ACTIVE)
	{
		dev->r_System->s_state = SYS_SCREENOFF_2_MODE_IDLE;
		dev->r_System->key_active = OFF;	
		dev->r_System->send_screenoff_2_command = OFF;		//send command off
		dev->r_System->s_state_lcd_state = SYS_SCREENOFF_2_MODE_IDLE;
		DPRINTF(DBG_MSG1,"%s  LCD OFF \n\r",__FUNCTION__);
	}
}

static void StayScreenOff2Mode(DEVICE *dev)
{	
	//goto standby mode
	if(shell_system_mode)
	{
		if(((MIN15_MODE_TIME1<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON)))
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_SCREENOFF_2_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			min15_timer_tick = GetTickCount();
		}
	}
	else
	{
		if(((MIN15_MODE_TIME<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
		/*&&(dev->r_System->incomecalling ==OFF)*/)
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_SCREENOFF_2_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			min15_timer_tick = GetTickCount();
		}
	}

		//goto 15min mode
	if(dev->r_System->power_key_status == POWER_KEY_LONG||dev->r_System->incomecalling)
	{
		dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
		dev->r_System->s_state_backup = SYS_SCREENOFF_2_MODE_IDLE;

		dev->r_System->power_key_status= POWER_KEY_IDLE;
		DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
	}

		//goto screenoff_2
	if(dev->r_System->accign_state.state)
	{
		//check after 3ms , to check noise //goto standby mode
		if(3<=GetElapsedTime(system_timer_tick))
		{
			dev->r_System->s_state = SYS_SCREENOFF_1_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_SCREENOFF_2_MODE_IDLE;

			system_timer_tick = GetTickCount();
			min15_timer_tick = GetTickCount();
			DPRINTF(DBG_MSG1,"%s -> SYS_SCREENOFF_1_MODE_CHECK \n\r",__FUNCTION__);
		}
	}
	else 
		system_timer_tick = GetTickCount();
	
}	

static void CheckScreenSaverMode(DEVICE *dev)
{ 
	if(dev->r_System->send_screen_save_command == OFF)
	{
		acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
		acc_packet.buf[1] = 0u;
		acc_packet.buf[2] = 1u;
		acc_packet.buf[3] = 0u;
		acc_packet.trycnt =0;
		acc_packet.flag =ON;

		//clear
		dev->r_System->ask_screen_save_mode= OFF;
	
		dev->r_System->send_screen_save_command =ON;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s send acc screen save command %d\n\r",__FUNCTION__,system_timer_tick);
	}
	
	//no response
	if(ACC_NO_RESPONSE<=GetElapsedTime(system_timer_tick)){
		dev->r_System->send_screen_save_command = OFF;
		if(dev->r_System->s_state_backup == SYS_ACTIVE_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_screensaver++;
			#endif
		}
		else
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_screensaver++;
			#endif
		}
		system_timer_tick = GetTickCount();
		#ifdef CHECK_NO_RESPONSE
		if(12<dev->r_System->no_response_screensaver)
		{
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			DPRINTF(DBG_ERR,"no response SystemSoftwareReset 4\n\r");
			e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
			save_last_memory();
			RecordReset(4,NULL);
			delay_ms(100);
			SystemSoftwareReset();
		}
		else if(10<dev->r_System->no_response_screensaver)
		{
			uart0_deinit();
			uart1_deinit();
			init_dev_uart();
			DPRINTF(DBG_ERR,"no response uart reset \n\r");
		}
		else
			DPRINTF(DBG_MSG1,"%s no response %d\n\r",__FUNCTION__,system_timer_tick);	
		#endif		
	}
}

static void MoveScreenSaverMode(DEVICE *dev)
{
	#ifdef CHECK_NO_RESPONSE
	dev->r_System->no_response_screensaver=0;
	#endif
	#ifdef DSP_DOUBLE_QUE
	q2_en(&ak7739_cmd2_q, (uint8)DSP_GOTO_MUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
	#else
	q_en(&ak7739_cmd_q, (uint8)DSP_GOTO_MUTE_1);
	#endif
	system_timer_tick = GetTickCount();
	
	dev->r_System->send_screen_save_command = OFF;
	dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_SCREEN_SAVE_MODE_IDLE;
	DPRINTF(DBG_MSG1,"%s \n\r",__FUNCTION__);


}


static void StayScreenSaverMode(DEVICE *dev)
{
	#if 0
	//change 201223
	#if 1
	if(dev->r_System->incomecalling||dev->r_System->rvcavm_active)
		#endif
	#else
	if(dev->r_System->incomecalling||dev->r_System->voice_key_active)
	#endif
	{
		if(dev->r_System->accign_state.state)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
			dev->r_System->voice_key_active = OFF;
		}
		else
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->voice_key_active = OFF;
		}
		dev->r_System->s_state_backup = SYS_SCREEN_SAVE_MODE_IDLE;
		dev->r_System->power_key_status = POWER_KEY_IDLE;
		DPRINTF(DBG_MSG1,"%s -> SYS_ACTIVE_MODE_CHECK by  R, CAM\n\r",__FUNCTION__);
	}

	if(dev->r_System->power_key_status == POWER_KEY_SHORT)
	{
//		if(DET_ACC_IGN())
		if(dev->r_System->accign_state.state)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
		}
		else
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
		}
		dev->r_System->s_state_backup = SYS_SCREEN_SAVE_MODE_IDLE;
		dev->r_System->power_key_status = POWER_KEY_IDLE;
		DPRINTF(DBG_MSG1,"%s -> SYS_ACTIVE_MODE_CHECK by  POWER_KEY_SHORT\n\r",__FUNCTION__);
	}

	if(dev->r_System->ask_screen_save_mode==0x02)
	{
		dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
		dev->r_System->s_state_backup = SYS_SCREEN_SAVE_MODE_IDLE;
		DPRINTF(DBG_MSG1,"%s -> SYS_ACTIVE_MODE_CHECK by  ask_screen_save \n\r",__FUNCTION__);
	}

		//goto standby mode
	if(shell_system_mode)
	{
		if(((MIN15_MODE_TIME1<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
		&&(dev->r_System->accign_state.state==OFF)/*&&(dev->r_System->incomecalling ==OFF)*/)
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->last_mode = LAST_MODE_SCREENSAVER;
			dev->r_System->s_state_backup = SYS_SCREEN_SAVE_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			min15_timer_tick = GetTickCount();
		}
		else if(dev->r_System->accign_state.state)
			min15_timer_tick = GetTickCount();
	}
	else
	{
		if(((MIN15_MODE_TIME<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
		&&(dev->r_System->accign_state.state==OFF)/*&&(dev->r_System->incomecalling ==OFF)*/)
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->last_mode = LAST_MODE_SCREENSAVER;
			dev->r_System->s_state_backup = SYS_SCREEN_SAVE_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			min15_timer_tick = GetTickCount();
		}
		else if(dev->r_System->accign_state.state)
		{
			min15_timer_tick = GetTickCount();
		}
	}
}

static void Check15MinMode(DEVICE *dev)
{ 
	if(dev->r_System->send_15min_command == OFF)
	{
		acc_packet.buf[0] = Set_Acc_Status((dev->r_System->accign_state.state&0x01));
		acc_packet.buf[1] = SYSTEM_MODE_15MIN;
		acc_packet.buf[2] = 0u;
		acc_packet.buf[3] = 0u;
		acc_packet.trycnt = 0;
		acc_packet.flag = ON;

		//clear
		dev->r_System->power_key_status = POWER_KEY_IDLE;
		dev->r_System->send_15min_command =ON;
		system_timer_tick = GetTickCount();
		DPRINTF(DBG_MSG1,"%s send acc 15min command %d\n\r",__FUNCTION__,system_timer_tick);
	}

	//no response
	if(ACC_NO_RESPONSE<=GetElapsedTime(system_timer_tick))
	{
		dev->r_System->send_15min_command = OFF;
		if(dev->r_System->s_state_backup == SYS_ACTIVE_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_15min++;
			#endif
		}
		else if(dev->r_System->s_state_backup == SYS_SCREENOFF_1_MODE_IDLE)
		{
			dev->r_System->s_state = SYS_SCREENOFF_1_MODE_IDLE;
			#ifdef CHECK_NO_RESPONSE
			dev->r_System->no_response_15min++;
			#endif
		}
		system_timer_tick = GetTickCount();
		
		#ifdef CHECK_NO_RESPONSE
		if(12<dev->r_System->no_response_15min)
		{
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			DPRINTF(DBG_ERR,"no response SystemSoftwareReset 2\n\r");
			e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
			save_last_memory();
			RecordReset(2,NULL);
			delay_ms(100);
			SystemSoftwareReset();
		}
		else if(10<dev->r_System->no_response_15min)
		{
			uart0_deinit();
			uart1_deinit();
			init_dev_uart();
			DPRINTF(DBG_ERR,"no response uart reset \n\r");
		}
		else
			DPRINTF(DBG_MSG1,"%s no response %d\n\r",__FUNCTION__,system_timer_tick);
		#endif
	}
}

static void Move15MinMode(DEVICE *dev)
{
	#ifdef CHECK_NO_RESPONSE
	dev->r_System->no_response_15min=0;
	#endif

	if(dev->r_System->s_state_lcd_state == SYS_SCREEN_SAVE_MODE_IDLE)
	{
		if(dev->r_Dsp->d_mix_status==OFF)
			#ifdef DSP_DOUBLE_QUE
			q2_en(&ak7739_cmd2_q, (uint8)DSP_MAIN_VOL,(uint8) dev->r_Dsp->d_audo_mode);
			#else
			q_en(&ak7739_cmd_q, (uint8)DSP_MAIN_VOL); 		//volume recovery
			#endif
		#ifdef DSP_DOUBLE_QUE
		q2_en(&ak7739_cmd2_q, (uint8)DSP_GOTO_UNMUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
		#else
		q_en(&ak7739_cmd_q, (uint8)DSP_GOTO_UNMUTE_1);
		#endif
	}
	dev->r_System->send_15min_command = OFF;
	dev->r_System->s_state = SYS_15MIN_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_15MIN_MODE_IDLE;

	DPRINTF(DBG_MSG1,"%s MCU_HU_LCD_EN LCD ON \n\r",__FUNCTION__);
}

static void  Stay15MinMode(DEVICE *dev)
{
	//goto standby mode
	if(shell_system_mode)
	{
		if(((MIN15_MODE_TIME1<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
		/*&&(dev->r_System->incomecalling ==OFF)*/)
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_15MIN_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			min15_timer_tick = GetTickCount();
		}
	}
	else
	{
		if(((MIN15_MODE_TIME<=GetElapsedTime(min15_timer_tick))||(DriverDoorOpen()== ON)||(KeyFortification()==ON))
		/*&&(dev->r_System->incomecalling ==OFF)*/)
		{
			dev->r_System->s_state = SYS_STANDBY_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_15MIN_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_STANDBY_MODE_CHECK \n\r",__FUNCTION__);
			min15_timer_tick = GetTickCount();
		}
	}

	//goto active mode
	if(dev->r_System->accign_state.state)
	{
		if(3<=GetElapsedTime(system_timer_tick))
		{
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;
			dev->r_System->s_state_backup = SYS_15MIN_MODE_IDLE;
			DPRINTF(DBG_MSG1,"%s -> SYS_ACTIVE_MODE_CHECK \n\r",__FUNCTION__);
			system_timer_tick = GetTickCount();
			min15_timer_tick = GetTickCount();
		}
	}
	else 
		system_timer_tick = GetTickCount();
//[V1.0.33] 15min mode> Screen saver mode> incoming call> call connection> Entering the screen saver mode briefly when pressing i-drvier power button
	if(dev->r_System->incomecalling)
	{
		if((dev->r_System->power_key_status==POWER_KEY_SHORT)||(dev->r_System->power_key_status==POWER_KEY_LONG))
			dev->r_System->power_key_status = POWER_KEY_IDLE;
		if(dev->r_System->ask_screen_off_mode)
			dev->r_System->ask_screen_off_mode = OFF;
		return;
	}

	if((dev->r_System->power_key_status == POWER_KEY_SHORT)||(dev->r_System->ask_screen_save_mode==ON))
	{
		//goto screensaver mode
		dev->r_System->power_key_status = POWER_KEY_IDLE;
		dev->r_System->s_state = SYS_SCREEN_SAVE_MODE_CHECK;
		dev->r_System->s_state_backup = SYS_15MIN_MODE_IDLE;
		DPRINTF(DBG_MSG1,"%s -> SYS_SCREEN_SAVE_MODE_CHECK by POWER_KEY_SHORT\n\r",__FUNCTION__);
	}

	if((dev->r_System->power_key_status == POWER_KEY_LONG)||(dev->r_System->ask_screen_off_mode==ON))
	{
		//goto screenoff mode
		dev->r_System->power_key_status = POWER_KEY_IDLE;
		dev->r_System->s_state = SYS_SCREENOFF_1_MODE_CHECK;
		dev->r_System->s_state_backup = SYS_15MIN_MODE_IDLE;
		DPRINTF(DBG_MSG1,"%s -> SYS_SCREENOFF_1_MODE_CHECK by POWER_KEY_LONG\n\r",__FUNCTION__);
	}
}

static void MoveToLowBatteryMode(DEVICE *dev)
{
	//backup mode
	dev->r_System->low_batter_enter = ON;			//low battery mode
	dev->r_System->low_batter_status = ON;			//low battery mode
	lowbatt_timer_tick=GetTickCount();
	#ifdef LOW_PWR_DETECT
	Control_LCD_Backlight(OFF);
	#endif
	DPRINTF(DBG_MSG1,"%s low battery mode %d %d\n\r",__FUNCTION__,lowbatt_timer_tick,MCU_LOW_BAT());
	// ���͸��� ���� �ϸ� ������ ������ �Ǳ⿡ 1sec üũ�ϵ��� ��.
	if((1000<lowbatt_timer_tick)&&flash_erase_on)
	{
		//check 1sec to protect flash information when it shuts down quickly
		e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
		e2p_sys_data.E2P_SONY = r_sonysound;
		e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120		
		flash_write_func(FLASH_DATA0_ADDR, (const unsigned char *)&e2p_save_data, sizeof(DATA_SAVE));
		flash_write_func(FLASH_DATA1_ADDR, (const unsigned char *)&e2p_sys_data, sizeof(E2P_SYSTEM));
		lowbatt_timer_tick=GetTickCount();
		flash_erase_on = OFF;
		DPRINTF(DBG_MSG1,"%s flash backup %d %d\n\r",__FUNCTION__ ,lowbatt_timer_tick,e2p_sys_data.E2P_LAST_MODE);
		delay_ms(100);
	}
	#if 0		//soc off , pmic and pwr_en is off
//	MCU_CONTROL_PMIC_ONOFF(OFF);
//	M_MAIN_PWR_EN(OFF);
	#endif
	
	Uart_Init_Comm();
	dev->r_System->s_state = SYS_BATTERY_LOW_MODE_IDLE;
//	dev->r_Power->p_state = PWR_OFF_1;
	dev->r_Tuner->t_state = TUNER_IDLE;
	dev->r_Dsp->d_state = DSP_IDLE;
	dev->r_System->s_state_lcd_state = SYS_BATTERY_LOW_MODE_IDLE;

	system_timer_tick = GetTickCount();
	lowbatt_timer_tick1  =GetTickCount();
}

static void StayLowBatteryMode(DEVICE *dev)
{
	static uint8 OnOff=0;
	static uint8 OnOff1=0;   // EMC TEST
	
	if(MCU_LOW_BAT())
	{
		lowbatt_timer_tick1  =GetTickCount();
		if((100<=GetElapsedTime(system_timer_tick))&&(OnOff==0))
		{
			OnOff= ON;
			dev->r_System->check_low_voltage_dtc	= ON;
		}
		//enter goto sleep mode under 6v
		if(((Check_Batt_Value())<=BAT_ADC_5V_LEVEL)&&((Check_Batt_Value())!=0))
		{	
			if(OnOff1==OFF)
			{
				OnOff1 =ON;
				DPRINTF(DBG_MSG1,"enter StayLowBatteryMode 6V under \n\r");
				//PORTA
				DSP_PDN(OFF);
				MCU_AMP_ST_BY(OFF);
				MCU_AMP_MUTE(ON);
				#ifdef B1_MOTHER_BOARD_SAMPLE
				USB_PW_EN(OFF);
				#endif
				#ifndef LOW_PWR_DETECT
				Control_LCD_Backlight(OFF);
				#endif
				TFT_PW_EN(OFF);	
				MCU_CONTROL_PMIC_ONOFF(OFF);
				M_MAIN_PWR_EN(OFF);
				PMIC_RESET_IN(OFF);
				MCU_TFT2_PWR_EN(OFF);//MCU_TFT2_PWR_EN
//not used				MIC_PWR_EN_1(OFF);
				MIC_PWR_EN_2(OFF);
				//ATB_3V3_EN(OFF);
				M_5V_FRQ_SEL(OFF);
				GPS_MD_PW_EN(OFF);
				#if 0 //def SI_LABS_TUNER
				SiLabs_Tuner_RST(OFF);
				//#else
				IRLED_CTL(OFF);
				#endif
				DSP_PW_EN(OFF);//DSP_PW_EN
				BAT_ADC_ON(OFF);//BAT_ADC_ON
				M_5V_EN(OFF);
				#ifdef CHANG_BL_TIMING
				if(dev->r_System->blpwmenable)//JANG_211025 
				{
					FTM3_deinit();
					dev->r_System->blpwmenable =OFF;
				}
				#else
				FTM3_deinit();
				#endif					
// EMC TEST				sys_deinit_fnc();
				#ifdef VECTOR_CCL_TEST 
				dev->r_System->usrdata1 &=(~(NETWORK_WAKEUP|IGN_WAKEUP|ECUSPEC_WAKEUP));	//clear flag when go to sleep mode
				//TX_RRM_NM_USR_DATA1(dev->r_System->usrdata1);
				#endif
				#ifdef WATCHDOG_ENABLE
				WDOG_DRV_Deinit(INST_WATCHDOG1);
				#endif
// EMC TEST				Go_To_Sleep();
				SetTickCount(0);//tick_clear
// EMC TEST				INIT_SYSTEM();
// EMC TEST				INIT_GR_DATA();	
				dev->r_System->init_ok = OFF;
				dev->r_System->uart_ok = OFF;
				dev->r_System->power_on = OFF;
				dev->r_System->send_sleep_command = OFF;		
				dev->r_Dsp->d_softmute_status = OFF;
				dev->r_Dsp->d_goto_mute_status = OFF;
// EMC TEST				dev->r_System->s_state = SYS_BATTERY_LOW_MODE_IDLE;		//need to reset   (sleep mode exit and reset)
				DPRINTF(DBG_MSG1,"exit StayLowBatteryMode 6V under \n\r");
				
				shell_system_mode = OFF;
				}
			}
	}
	else
	{
		//1000->100->500  // EMC TEST
		if(500<=GetElapsedTime(lowbatt_timer_tick1))
		{
			#ifdef INTERNAL_AMP
			MCU_AMP_MUTE(ON); 					//SOUND OFF
			#endif
			OnOff=OFF;
			OnOff1 =OFF;
			RecordReset(8,NULL);
			SystemSoftwareReset();
		}
	}
}

static void MoveToHighBatteryMode(DEVICE *dev)
{	

	#ifdef INTERNAL_AMP
	MCU_AMP_MUTE(ON);					//SOUND OFF
	#ifdef DSP_DOUBLE_QUE
	q2_en(&ak7739_cmd2_q, (uint8)DSP_MUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
	#else
	q_en(&ak7739_cmd_q, (uint8)DSP_MUTE_1);
	#endif
	#endif

	highbatt_timer_tick  =GetTickCount();
	highbatt_can_timer_tick =GetTickCount();
	DPRINTF(DBG_MSG1,"%s LCD OFF,AMP OFF, CAN DISBLE %d\n\r",__FUNCTION__,highbatt_timer_tick);

	dev->r_System->s_state = SYS_BATTERY_HIGH_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_BATTERY_HIGH_MODE_IDLE;
}

static void StayHighBatteryMode(DEVICE *dev)
{
	static uint8 onoff= 0;
	static uint8 onoff1= 0;
	if(MCU_HIGH_BAT())
	{
		highbatt_timer_tick  =GetTickCount();
		
		//high DTC
		if((500<=GetElapsedTime(highbatt_can_timer_tick ))&&(onoff1==0))
		{
			onoff1 =ON;
//			MntFltSetResultProcess(DTC_POWER_HIGH, DTC_FAILED);
			dev->r_System->check_high_voltage_dtc	= ON;
		}

		//can control
		if(MIN_TIME_DEF<=GetElapsedTime(highbatt_can_timer_tick ))
		{
			onoff =ON;
			//SrvMcanComCtrl(SRVMCAN_RX_ALL_DISABLE_IDX);
			//SrvMcanComCtrl(SRVMCAN_TX_ALL_DISABLE_IDX);
			highbatt_can_timer_tick =GetTickCount();
		}
	}
	else
	{
		if(5<=GetElapsedTime(highbatt_timer_tick ))
		{	
			highbatt_timer_tick  =GetTickCount();
			if(onoff==ON)
			{
				onoff =OFF;
			//	SrvMcanComCtrl(SRVMCAN_RX_ALL_ENABLE_IDX);
			//	SrvMcanComCtrl(SRVMCAN_TX_ALL_ENABLE_IDX);
				DPRINTF(DBG_MSG1,"%s CAN ENABLE %d\n\r",__FUNCTION__,highbatt_timer_tick);
			}
			onoff1 =OFF;
			
			if(BAT_ADC_16V_LEVEL<(Check_Batt_Value()))
			{
				#ifdef INTERNAL_AMP
				#ifdef DSP_DOUBLE_QUE
				q2_en(&ak7739_cmd2_q, (uint8)DSP_UNMUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
				#else
				q_en(&ak7739_cmd_q, (uint8)DSP_UNMUTE_1);
				#endif
				#endif
				dev->r_System->s_state =  SYS_BATTERY_16V_OVER_MODE_ENTER;
				DPRINTF(DBG_MSG1,"highBat-> high 16v mode\n\r");
			}
			else
			{

				dev->r_System->amp_state = INTERNAL_AMP_INIT_START;

				q2_en(&ak7739_cmd2_q, (uint8)DSP_UNMUTE_1,(uint8) dev->r_Dsp->d_audo_mode);


				DPRINTF(DBG_MSG1,"%s AMP ON  MCU_HU_LCD_EN ON\n\r",__FUNCTION__);
				DPRINTF(DBG_MSG1,"highBat-> active mode\n\r");				

				dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;

			}
		}
	}
}

static void MoveToLow6VMode(DEVICE *dev)
{
	//AMP/LCD OFF
	if(dev->r_System->init_ok )
	{

		MCU_AMP_MUTE(ON);						//SOUND OFF
		
		DPRINTF(DBG_MSG1,"%s LCD OFF AMP OFF \n\r",__FUNCTION__);
	}
	DPRINTF(DBG_MSG1,"%s\n\r",__FUNCTION__);
	dev->r_System->s_state = SYS_BATTERY_6V_UNDER_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_BATTERY_6V_UNDER_MODE_IDLE;
}
static void StayLow6VMode(DEVICE *dev)
{
	if((Check_Batt_Value())<(BAT_ADC_6V_LEVEL+20))
		batter_adc_timer_tick1  =GetTickCount();
	else
	{	
		if(5<=GetElapsedTime(batter_adc_timer_tick1 ))
		{
			if(dev->r_System->init_ok )
			{
				DPRINTF(DBG_MSG1,"MCU_HU_LCD_EN LCD  ON 6\n\r");

				MCU_AMP_MUTE(OFF);						//SOUND ON

				MCU_CONTROL_PMIC_ONOFF(OFF);
				M_MAIN_PWR_EN(OFF);
				PMIC_RESET_IN(OFF);

				DPRINTF(DBG_MSG1,"%s V:%d LCD ON, AMP ON \n\r",__FUNCTION__,Check_Batt_Value());
			}
			dev->r_System->s_state = SYS_ACTIVE_MODE_ENTER;
		}
	}
}

static void MoveToLow9VMode(DEVICE *dev)
{
	//AMP/LCD OFF	

	MCU_AMP_MUTE(ON);					//SOUND OFF

	batter_adc_timer_tick1  =GetTickCount();
	dev->r_System->s_state = SYS_BATTERY_9V_UNDER_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_BATTERY_9V_UNDER_MODE_IDLE;
	DPRINTF(DBG_MSG1,"%s LCD OFF ,AMP OFF \n\r",__FUNCTION__);
}
static void StayLow9VMode(DEVICE *dev)
{
	//50 is ok
	if(Check_Batt_Value()<=(BAT_ADC_9V_LEVEL+BAT_ADC_9V_LEVEL_OFFSET))
		batter_adc_timer_tick1  =GetTickCount();
	else
	{
		if(20<=GetElapsedTime(batter_adc_timer_tick1 ))
		{	

			dev->r_System->amp_state = INTERNAL_AMP_INIT_START;
			batter_adc_timer_tick1  =GetTickCount();
			DPRINTF(DBG_MSG1,"%s V:%d LCD ON, AMP ON \n\r",__FUNCTION__,Check_Batt_Value());
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;

		}
	}
}

static void MoveToHigh16VMode(DEVICE *dev)
{

	MCU_AMP_MUTE(ON);			//SOUND OFF

	q2_en(&ak7739_cmd2_q, (uint8)DSP_MUTE_1,(uint8) dev->r_Dsp->d_audo_mode);
	batter_adc_timer_tick1  = GetTickCount();
	batter_adc_can_timer_tick  = GetTickCount();
	dev->r_System->s_state = SYS_BATTERY_16V_OVER_MODE_IDLE;
	dev->r_System->s_state_lcd_state = SYS_BATTERY_16V_OVER_MODE_IDLE;

	DPRINTF(DBG_MSG1,"%s LCD OFF, AMP OFF CAN DISABLE \n\r",__FUNCTION__);

}
static void StayHigh16VMode(DEVICE *dev)
{
	static uint8 onoff=0;

	if(BAT_ADC_16V_LEVEL<(Check_Batt_Value()))	
//	if((BAT_ADC_16V_LEVEL-5)<(Check_Batt_Value()))
	{
		batter_adc_timer_tick1  =GetTickCount();
		if((MIN_TIME_DEF*60)<=GetElapsedTime(batter_adc_can_timer_tick ))
		{

			onoff =ON;
			batter_adc_can_timer_tick  = GetTickCount();
		}
	}
	else
	{
		if(20<=GetElapsedTime(batter_adc_timer_tick1 )){

			dev->r_System->amp_state = INTERNAL_AMP_INIT_START;

			q2_en(&ak7739_cmd2_q, (uint8)DSP_UNMUTE_1,(uint8) dev->r_Dsp->d_audo_mode);


			if(onoff==ON)
			{
			//	SrvMcanComCtrl(SRVMCAN_RX_ALL_ENABLE_IDX);
			//	SrvMcanComCtrl(SRVMCAN_TX_ALL_ENABLE_IDX);
				onoff=OFF;
			}
			DPRINTF(DBG_MSG1,"%s V:%d LCD ON, AMP ON \n\r",__FUNCTION__,Check_Batt_Value());
			
			dev->r_System->s_state = SYS_ACTIVE_MODE_CHECK;

		}
	}
}

static void CheckBatFunc(DEVICE *dev)
{
	uint16 tmp16=0;

//	if((dev->r_System->s_state !=SYS_BATTERY_LOW_MODE_ENTER)&&(SYS_SLEEP_MODE_IDLE<dev->r_System->s_state))
	if((dev->r_System->s_state !=SYS_BATTERY_LOW_MODE_ENTER)&&(SYS_SLEEP_MODE_IDLE !=dev->r_System->s_state)&&
		(dev->r_System->s_state !=SYS_BATTERY_LOW_MODE_IDLE))
	{
		if(MCU_LOW_BAT())
		{
			if(1<=GetElapsedTime(lowbatt_timer_tick ))
			{
				//3->100->50		 
				lowbatt_timer_tick=GetTickCount();	
				DPRINTF(DBG_MSG1,"enter low batt %d %d\n\r",dev->r_System->s_state,lowbatt_timer_tick);
				dev->r_System->s_state = SYS_BATTERY_LOW_MODE_ENTER;
			}
		}
		else
			lowbatt_timer_tick	=GetTickCount();
	}
	else
		lowbatt_timer_tick	=GetTickCount();

//	if((dev->r_System->s_state !=SYS_BATTERY_16V_OVER_MODE_ENTER)&&(dev->r_System->s_state !=SYS_BATTERY_16V_OVER_MODE_IDLE))
	if((dev->r_System->s_state !=SYS_BATTERY_HIGH_MODE_ENTER)&&(dev->r_System->s_state !=SYS_BATTERY_HIGH_MODE_IDLE))
	{
		if(MCU_HIGH_BAT())
		{
			if(100<=GetElapsedTime(highbatt_timer_tick ))
			{
				dev->r_System->s_state = SYS_BATTERY_HIGH_MODE_ENTER;
				
				DPRINTF(DBG_MSG1,"enter high batt \n\r");
			}
		}
		else
			highbatt_timer_tick  =GetTickCount();
	}

	if((SYS_SLEEP_MODE_IDLE<dev->r_System->s_state)&&(dev->r_System->s_state<SYS_BATTERY_16V_OVER_MODE_IDLE))
	{
		tmp16 =Check_Batt_Value();
		
		if(tmp16<= BAT_ADC_6V_LEVEL)
		{
			if(100<=GetElapsedTime(batter_adc_timer_tick ))
			{
//				DPRINTF(DBG_MSG1,"under battery 6V %d mode:%d\r",tmp16,dev->r_System->s_state);
				batter_adc_timer_tick  =GetTickCount();
			}
		}
		else if((tmp16<=	BAT_ADC_9V_LEVEL)&&((dev->r_System->s_state !=SYS_BATTERY_9V_UNDER_MODE_IDLE)&&
		(dev->r_System->s_state !=SYS_BATTERY_LOW_MODE_ENTER)&&(dev->r_System->s_state !=SYS_BATTERY_LOW_MODE_IDLE)))
		{
			if(100<=GetElapsedTime(batter_adc_timer_tick ))
			{
				dev->r_System->s_state = SYS_BATTERY_9V_UNDER_MODE_ENTER;
				DPRINTF(DBG_MSG1,"under battery 9V %d mode:%d\n\r",tmp16,dev->r_System->s_state);
				batter_adc_timer_tick  =GetTickCount();
			}
		}
		else if((BAT_ADC_16V_LEVEL<tmp16)&&((dev->r_System->s_state !=SYS_BATTERY_HIGH_MODE_ENTER)&&(dev->r_System->s_state !=SYS_BATTERY_HIGH_MODE_IDLE)))
		{
			if(100<=GetElapsedTime(batter_adc_timer_tick ))
			{
				dev->r_System->s_state = SYS_BATTERY_16V_OVER_MODE_ENTER;
				DPRINTF(DBG_MSG1,"over battery 16V %d mode:%d\n\r",tmp16,dev->r_System->s_state);	
				batter_adc_timer_tick  =GetTickCount();
			}
		}
		else
		{
			if(((dev->r_System->check_high_voltage_dtc==ON)||(dev->r_System->check_low_voltage_dtc==ON))
				&&((dev->r_System->uart_ok == ON)))
			{
				if(500<=GetElapsedTime(voltage_dtc_timer_tick )){
//					DPRINTF(DBG_MSG1,"check DTC voltage: \n\r");	
					dev->r_System->check_high_voltage_dtc = OFF;
					dev->r_System->check_low_voltage_dtc = OFF;
					voltage_dtc_timer_tick = GetTickCount();
				}
			}
			else
				voltage_dtc_timer_tick = GetTickCount();
			batter_adc_timer_tick  =GetTickCount();
		}
		
	}
	else
	{
		voltage_dtc_timer_tick = GetTickCount();
		batter_adc_timer_tick  =GetTickCount();
	}

}

void Check_Communcte_CAN_status(void)
{
}

//check system function
