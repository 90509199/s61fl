/*
 * test_mode.h
 *
 *  Created on: 2018. 12. 24.
 *      Author: Digen
 */

#ifndef TEST_MODE_H_
#define TEST_MODE_H_


typedef struct tagDSP_GEQ
{
	int gain;	
	double band_factor;
	uint32 band_freq;
}DSP_GEQ;

extern uint8 main_vol_leve[41]; 

extern DSP_GEQ geq1;
extern DSP_GEQ geq2;
extern DSP_GEQ geq3;
extern DSP_GEQ geq4;
extern DSP_GEQ geq5;
extern DSP_GEQ geq6;

void DSP_Init_GEQ(void);
void Test_Task(DEVICE *dev);
 

#endif /* TEST_MODE_H_ */
