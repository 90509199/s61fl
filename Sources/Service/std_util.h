#ifndef UTIL_H
#define UTIL_H

/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include "../includes.h"
/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
#define UTL_Q_INIT(q, buff, size)			UtlQueueInit((q), (buff), (size))
#define UTL_Q_IS_EMPTY(q)					UtlQisEmpty(q)
#define UTL_Q_IS_FULL(q)					UtlQisFull(q)
#define UTL_EN_Q(q, data)					UtlEnQueue((q), (data))
#define UTL_DE_Q(q, data)					UtlDeQueue((q), (data))

#define UTL_COPY_DATA(dst, src, size)        UtlCopyData(dst, src, size)
#define UTL_SET_DATA(dst, data, size)        UtlSetData(dst, data, size)
#define UTL_COMPARE_DATA(comp1, comp2, size) UtlCompData(comp1, comp2, size)

#define UTL_MASK_SET(v, m)      ( (v) |= (m) )
#define UTL_MASK_CLR(v, m)      ( (v) &= ~(m) )
#define UTL_MASK_IS(v, m)       ( 0u != ((v) & (m)) )

#define UTL_SHIFT_R(d)          ((d) >> 1)
#define UTL_SHIFT_L(d)          ((d) << 1)

#define UTL_CONVERT_8BIT_TO_32BIT(d)		( (d[0] << 24) | (d[1] << 16) | (d[2] << 8) | d[3] )

#define UTL_CONVERT_8BIT_TO_16BIT(d)		( (d[0] << 8) | d[1] )

#define UTL_CONVERT_32BIT_TO_8BIT(d32, d8, i)	d8[(i)] = ( ((d32) >> 24) & 0xFF );(i) += 1;\
												d8[(i)] = ( ((d32) >> 16) & 0xFF );(i) += 1;\
												d8[(i)] = ( ((d32) >> 8) & 0xFF );(i) += 1;\
												d8[(i)] = ((d32) & 0xFF);(i) += 1;

#define UTL_CONVERT_16BIT_TO_8BIT(d16, d8, i)	d8[(i)] = ( ((d16) >> 8) & 0xFF );(i) += 1;\
												d8[(i)] = ((d16) & 0xFF);(i) += 1;

#define UTL_SET_SEP_VALUE_NUM3(arr0, arr1, shift, compVal, setVal)	arr0 = (setVal >> shift) & 0xFF;\
																	arr1 = (setVal & compVal);

#define UTL_GET_SEP_VALUE_NUM2(arr0, arr1, shift)			( ((arr0 << shift) | arr1) & 0xFFu )
#define UTL_SET_SEP_VALUE_NUM2(arr0, arr1, shift, compVal, setVal)	arr0 = (setVal >> shift) & 0xFF;\
																	arr1 = (setVal & compVal);

#define UTL_GET_SEP_VALUE_OVER_1BYTE(arr0, arr1, shift)			( ((arr0 << shift) | arr1) & 0xFFFFu )

#define UTL_GET_SEP_VALUE_NUM3_OVER_1BYTE(arr0, arr1, arr2, arr1Size, arr2Size)			( ((arr0 << (arr1Size + arr2Size)) | ( arr1 << arr2Size) | arr2 ) & 0xFFFFu )
#define UTL_SET_SEP_VALUE_NUM3_OVER_1BYTE(arr0, arr1, arr2, arr1Size, arr2Size, compVal1, compVal2, compVal3, setVal)	arr0 = (setVal >> (arr1Size + arr2Size)) & compVal1;\
																														arr1 = ((setVal >> arr2Size) & compVal2);\
																														arr2 = (setVal & compVal3);																													
#define STD_GET_1BYTE(d, s, i)		(d) = ((s)[(i)]);							(i)++
#define STD_PUT_1BYTE(d, s, i)		(d)[(i)] = (s);								(i)++


#define STD_GET_2BYTE(d, s, i)		(d) =  ((uint16)((s)[(i)]) << 8);			(i)++; \
									(d) |=  ((uint16)(s)[(i)]);					(i)++

#define STD_PUT_2BYTE(d, s, i)		(d)[(i)] = (uint8)(((s) >> 8) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)((s) & 0xFFu);			(i)++


#define STD_GET_3BYTE(d, s, i)		(d) =  ((uint32)((s)[(i)]) << 16);			(i)++; \
									(d) |=  ((uint32)((s)[(i)]) << 8);			(i)++; \
									(d) |=  ((uint32)(s)[(i)]);					(i)++

#define STD_PUT_3BYTE(d, s, i)		(d)[(i)] = (uint8)(((s) >> 16) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)(((s) >> 8) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)((s) & 0xFFu);			(i)++


#define STD_GET_4BYTE(d, s, i)		(d) =  ((uint32)((s)[(i)]) << 24);			(i)++; \
									(d) |=  ((uint32)((s)[(i)]) << 16);		(i)++; \
									(d) |=  ((uint32)((s)[(i)]) << 8);			(i)++; \
									(d) |=  ((uint32)(s)[(i)]);					(i)++

#define STD_PUT_4BYTE(d, s, i)		(d)[(i)] = (uint8)(((s) >> 24) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)(((s) >> 16) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)(((s) >> 8) & 0xFFu); 	(i)++; \
									(d)[(i)] = (uint8)((s) & 0xFFu);			(i)++


#define STD_GET_16BIT_CNT(cnt8bit)  ( (cnt8bit) / (2u) )

#define STD_SHIFT_R(d)					((d) >> 1)
#define STD_SHIFT_L(d)					((d) << 1)

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/
typedef struct {
	sint8 *queueBuff;
	uint8 rear;
	uint8 front;
	uint16 buffSize;
}stUtlQueueType;

/*****************************************************************************/
/* Global variable declarations ('extern', definition in C source)           */
/*****************************************************************************/

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/
extern void UtlQueueInit(stUtlQueueType* iStQ, sint8* iU8Buff, uint16 iU8BuffSize);
extern uint8 UtlQisEmpty(stUtlQueueType* iStQ);
extern uint8 UtlQisFull(stUtlQueueType* iStQ);
extern uint8 UtlEnQueue(stUtlQueueType* iStQ, uint8 iU8Data);
extern uint8 UtlDeQueue(stUtlQueueType* iStQ, uint8* oU8Data);

extern void UtlCopyData(uint8* dst, const uint8* src, uint32 size);
extern void UtlSetData(uint8* dst, uint8 data, uint32 size);
extern uint8 UtlCompData(const uint8* comp1, const uint8* comp2, uint32 size);

extern uint16 UtlAddU8U8_U16(const uint8 iU8Num1, const uint8 iU8Num2);

#endif /* UTIL_H */
