#ifndef STD_TIMER_H
#define STD_TIMER_H

/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include <Abstraction/abs_timer.h>
#include "../includes.h"
/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
#define TIMER_STATE_STOP    (0u)
#define TIMER_STATE_RUN     (1u)
#define TIMER_STATE_TIMEOUT (2u)

#define TIMER_INIT()    StdTimerInit()
#define TIMER_MGR()     StdTimerMgr()
#define TIMER_TICK()    StdTimerTick()
#define GET_TIME()      StdTimerGetTime()

#define START_TIMER(tmr, duration)      StdTimerStart(&(tmr), (duration))
#define RESTART_TIMER(tmr)              StdTimerRestart(&(tmr))
#define STOP_TIMER(tmr)                 StdTiemrStop(&(tmr))
#define IS_TIMER(tmr)                   StdTimerGetState(&(tmr))
#define GET_ELAPSED_TIME(tmr)			StdTimerGetElapsedTime(&(tmr))
#define GET_TIME_DURATION(tmr)			StdTimerGetDuration(&(tmr))

#define START_ALARM(idx, duration, cbFunc, isCyclic)    StdTimerStartAlarm( (idx), (duration), (cbFunc), (isCyclic) )
#define STOP_ALARM(idx)                                 StdTimerStopAlarm( (idx) )

#define GET_US_TIME()					StdTimerGetUsTime();
/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/

typedef void (* fpAlarmCbFuncType)(void);

typedef enum{
	NMAPP_SLP_DLY_TIMER_IDX,		
	NM_WAKE_UP_MSG_RESEND_TIMER_IDX,				
    ALARM_MAX
}enAlarmIdxType;

typedef struct {
    uint32 u32Duration;    /* duration time */
    uint32 u32Start;       /* start time */
    uint32 u32Target;      /* target time */
    uint8 u8Status;       /* status */
}stTimerType;

typedef struct {
    stTimerType stTimer;
    fpAlarmCbFuncType fpCbFunc;    /* call back function */
    uint8 u8IsCyclic;
}stAlarmType;

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/

extern void StdTimerInit(void);
extern void StdTimerMgr(void);
extern void StdTimerTick(void);
extern uint32 StdTimerGetTime(void);
extern void StdTimerStart(stTimerType* opSTTmr, uint32 iU32Duration);
extern void StdTimerRestart(stTimerType* opSTTmr);
extern void StdTiemrStop(stTimerType* opSTTmr);
extern uint8 StdTimerGetState(stTimerType* iopSTtmr);
extern uint32 StdTimerGetElapsedTime(stTimerType* iopSTtmr);
extern uint32 StdTimerGetDuration(stTimerType* iopSTtmr);
extern void StdTimerStartAlarm(enAlarmIdxType iENIdx, uint32 iU32Duration, fpAlarmCbFuncType oFPCbFunc, uint8 iU8IsCyclic);
extern void StdTimerStopAlarm(enAlarmIdxType iENIdx);
extern uint64 StdTimerGetUsTime(void);

#endif /* STD_TIMER_H */

