/**************************************************************************
 *  PROJECT:        CSP
 **************************************************************************/
/**********************************************************************************************************************
  INCLUDE
**********************************************************************************************************************/
#include "string.h"
#include "std_timer.h"
#include "std_util.h"
#include "Abstraction/abs_timer.h"
/**********************************************************************************************************************
  DEFINE
**********************************************************************************************************************/
#define STDTIMER_TIME_MAX  (0xFFFFFFFFu)
#define STDTIMER_MAX_TIMETICK	(1000u)
/**********************************************************************************************************************
  STRUCTURE
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLABAL VARIABLE
**********************************************************************************************************************/
volatile static uint32 gU32Tmr;

static uint8 gU8TimeTickSig = FALSE;

static stAlarmType gSTimerAlarm[ALARM_MAX];

/**********************************************************************************************************************
  FUNCTION PROTOTYPE
**********************************************************************************************************************/
void StdTimerCheckAlarm(void);

/**************************************************************************
 * FUNCTION NAME
 **************************************************************************/
/**************************************************************************
 * StdTimerInit
 **************************************************************************/
void
StdTimerInit(void)
{
#if 0
	AbsTimerRegCbFunc(StdTimerTick);
	AbsTimerInit();
	AbsTimerStart(ABS_MAIN_TIMER);
#endif
}

/**************************************************************************
 * StdTimerMgr
 **************************************************************************/
void
StdTimerMgr(void)
{
    if(TRUE == gU8TimeTickSig)/* every 1ms */
    {
        gU8TimeTickSig = FALSE;
        StdTimerCheckAlarm();
    }
}

/**************************************************************************
 * StdTimerTick
 **************************************************************************/
void 
StdTimerTick(void)
{
    /* 1ms */
    gU32Tmr += 1u;
    gU8TimeTickSig = TRUE;
}

/**************************************************************************
 * StdTimerGetTime
 **************************************************************************/
uint32 
StdTimerGetTime(void)
{
    return gU32Tmr;
}

/**************************************************************************
 * StdTimerStart
 **************************************************************************/
void 
StdTimerStart(stTimerType* opSTTmr, uint32 iU32Duration)
{
    if(NULL != opSTTmr)
    {
        opSTTmr->u8Status = TIMER_STATE_RUN;

        opSTTmr->u32Start = gU32Tmr;
        opSTTmr->u32Duration = iU32Duration;
        opSTTmr->u32Target = (opSTTmr->u32Start + opSTTmr->u32Duration);
    }
}

/**************************************************************************
 * StdTimerRestart
 **************************************************************************/
void 
StdTimerRestart(stTimerType* opSTTmr)
{
    if( (NULL != opSTTmr) && (0u != opSTTmr->u32Duration) )
    {
        opSTTmr->u8Status = TIMER_STATE_RUN;
        opSTTmr->u32Start = gU32Tmr;
        opSTTmr->u32Target = (opSTTmr->u32Start + opSTTmr->u32Duration);
    }
}

/**************************************************************************
 * StdTiemrStop
 **************************************************************************/
void 
StdTiemrStop(stTimerType* opSTTmr)
{
    if( NULL != opSTTmr )
    {
        opSTTmr->u8Status = TIMER_STATE_STOP;
        opSTTmr->u32Duration =  0u;
        opSTTmr->u32Target = 0u;
    }
}

/**************************************************************************
 * StdTimerGetState
 **************************************************************************/
uint8 
StdTimerGetState(stTimerType* iopSTtmr)
{
    uint8 lU8Status = TIMER_STATE_STOP;
    uint32 lU32TmValue = 0;

    if(NULL != iopSTtmr)
    {
        if(TIMER_STATE_RUN == iopSTtmr->u8Status)
        {
            lU32TmValue = gU32Tmr;

            if(lU32TmValue >= iopSTtmr->u32Target)
            {

                if(lU32TmValue < iopSTtmr->u32Start)
                {
                    /* over tick */
                    lU32TmValue += (STDTIMER_TIME_MAX - iopSTtmr->u32Start);
                }
                else
                {
                    /* normal tick */
                    lU32TmValue -= iopSTtmr->u32Start;
                }

                if(lU32TmValue >= iopSTtmr->u32Duration)
                {/* time out */
                    iopSTtmr->u8Status = TIMER_STATE_TIMEOUT;
                }
            }                
        }

        lU8Status = iopSTtmr->u8Status;
    }

    return lU8Status;
}


/**************************************************************************
 * StdTimerGetDuration
 **************************************************************************/
uint32
StdTimerGetElapsedTime(stTimerType* iopSTtmr)
{
    uint32 lU32TmValue = 0;

    if(NULL != iopSTtmr)
    {
        if(TIMER_STATE_RUN == iopSTtmr->u8Status)
        {
			lU32TmValue = gU32Tmr;

			if(lU32TmValue < iopSTtmr->u32Start)
			{
				/* over tick */
				lU32TmValue += (STDTIMER_TIME_MAX - iopSTtmr->u32Start);
			}
			else
			{
				/* normal tick */
				lU32TmValue -= iopSTtmr->u32Start;
			}
        }
    }

    return lU32TmValue;
}

/**************************************************************************
 * StdTimerGetDuration
 **************************************************************************/
uint32
StdTimerGetDuration(stTimerType* iopSTtmr)
{
	uint32 duration;

	if(NULL != iopSTtmr)
	{
		duration = iopSTtmr->u32Duration;
	}

	return duration;
}

/**************************************************************************
 * StdTimerStartAlarm
 **************************************************************************/
void 
StdTimerStartAlarm(enAlarmIdxType iENIdx, uint32 iU32Duration, fpAlarmCbFuncType oFPCbFunc, uint8 iU8IsCyclic)
{
    StdTimerStart(&gSTimerAlarm[iENIdx].stTimer, iU32Duration);
    gSTimerAlarm[iENIdx].u8IsCyclic = iU8IsCyclic;
    
    if(NULL != oFPCbFunc)
    {
        gSTimerAlarm[iENIdx].fpCbFunc = oFPCbFunc;
    }
}

/**************************************************************************
 * StdTimerStopAlarm
 **************************************************************************/
void 
StdTimerStopAlarm(enAlarmIdxType iENIdx)
{
    StdTiemrStop(&gSTimerAlarm[iENIdx].stTimer);
}


/**************************************************************************
 * StdTimerGetUsTime
 **************************************************************************/
uint64
StdTimerGetUsTime(void)
{
	uint64 lU64Timeus = 0;

	lU64Timeus = AbsTimerGetTimerUs(ABS_MAIN_TIMER);

	lU64Timeus = ( (lU64Timeus > STDTIMER_MAX_TIMETICK) ? 0 : (uint64)(STDTIMER_MAX_TIMETICK - lU64Timeus) );

	lU64Timeus += (gU32Tmr * 1000);

	return lU64Timeus;
}

/**************************************************************************
 * Timer100usTick
 **************************************************************************/
void StdTimerCheckAlarm(void)
{
    uint8 lU8Idx = 0u;
    uint8 lU8State = TIMER_STATE_STOP;

    for(lU8Idx = 0u ; lU8Idx < ALARM_MAX ; lU8Idx++)
    {
        lU8State = StdTimerGetState(&gSTimerAlarm[lU8Idx].stTimer);

        if( (TIMER_STATE_TIMEOUT == lU8State) && (NULL != gSTimerAlarm[lU8Idx].fpCbFunc) )
        {
            gSTimerAlarm[lU8Idx].fpCbFunc();
            gSTimerAlarm[lU8Idx].stTimer.u8Status = TIMER_STATE_STOP;

            if(FALSE != gSTimerAlarm[lU8Idx].u8IsCyclic)
            {
                StdTimerRestart(&gSTimerAlarm[lU8Idx].stTimer);
            }
        }
    }
}





