/*
 * srv_mcan.h
 *
 *  Created on: 2020. 3. 6.
 *      Author: Digen
 */

#ifndef SERVICE_SRV_MCAN_H_
#define SERVICE_SRV_MCAN_H_

/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include "../includes.h"

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/
typedef enum{
	RRM_7_453_IDX = 0,   /*  0  */
	RRM_1_515_IDX,
	RRM_2_516_IDX,
	RRM_3_517_IDX,
	RRM_8_51D_IDX,
	RRM_4_52D_IDX,       /*  5  */ 
	RRM_5_52F_IDX,
	RRM_9_537_IDX,
	RRM_10_53D_IDX,
	RRM_6_540_IDX,
	RRM_11_55D_IDX,      /*  10  */
	RRM_WPC_56D_IDX,
	RRM_DVR_1_57A_IDX,			
	RRM_DVR_2_57B_IDX,				
	RRM_DVR_3_57C_IDX,
	RRM_DMS_1_59A_IDX,   /*  15  */
	RRM_BCM_15_385_IDX,   //JANG_211231
	NWM_RRM_618_IDX,
	DIAG_TX_75A_IDX,
	#ifdef CAN_TX_392_ADD
	BCM_4_392_IDX,	
	#endif
	RRM_TX_MAX
}eSrvMcanTxMsg;

typedef enum{
	CGW_EMS_G_280_IDX = 0, /*  0  */
	ABS_ESP_1_2E9_IDX,
	CGW_TCU_G_301_IDX,
	FRM_3_305_IDX,
	FCM_2_307_IDX,
	CGW_ABM_G_320_IDX,     /*  5  */
	#ifdef CHANG_TO_MFS_3
	MFS_2_323_IDX,			// CHERY SPEC (CAN TRANS)
	#else
	MFS_2_321_IDX,			// CHERY SPEC (CAN TRANS)
	#endif
	MFS_4_325_IDX,
	CGW_SAM_G_340_IDX,
	CGW_EPS_G_380_IDX,
	CEM_BCM_1_391_IDX,
	CEM_BCM_2_392_IDX,     /*  10  */
	AVM_APA_3_41A_IDX,	
	ICM_1_430_IDX,
	PLG_436_IDX,
	RADAR_1_440_IDX,
	BSD_1_449_IDX,         /*  15  */
	ICM_2_452_IDX,
	WPC_1_460_IDX,
	AVM_APA_4_475_IDX,		
	CEM_PEPS_480_IDX,
	CEM_ALM_1_490_IDX,    /*  20  */
	ABS_ESP_5_4E5_IDX,	
	AVM_APA_2_508_IDX,      
	TBOX_2_519_IDX,
	CEM_IPM_1_51A_IDX,		
	AVM_2_51C_IDX,        /*  25  */
	ICM_3_52E_IDX,		
	ICM_4_530_IDX,          
	PM25_1_53A_IDX,
	AFU_1_560_IDX,
	AMP_1_555_IDX,      /*  30  */
	DVR_1_58A_IDX,
	CEM_BCM_4_58D_IDX,       
	CEM_BCM_3_5FF_IDX,
	NWM_CGW_600_IDX,
	DIAG_RX_74A_IDX,	/* 35 */
	DIAG_RX_7DF_IDX,	/*  */	
	TEST_RX_7FF_IDX,	/*  */	
	RRM_RX_MAX		
}eSrvMcanRxMsg;

typedef enum{
	SRVMCAN_RX_ALL_ENABLE_IDX = 0,
	SRVMCAN_RX_NM_ENABLE_IDX,
	SRVMCAN_RX_NORMAL_ENABLE_IDX,	
	SRVMCAN_RX_ALL_DISABLE_IDX,
	SRVMCAN_RX_NM_DISABLE_IDX,
	SRVMCAN_RX_NORMAL_DISABLE_IDX,	
	SRVMCAN_TX_ALL_ENABLE_IDX,
	SRVMCAN_TX_NM_ENABLE_IDX,	
	SRVMCAN_TX_NORMAL_ENABLE_IDX,		
	SRVMCAN_TX_ALL_DISABLE_IDX,		
	SRVMCAN_TX_NM_DISABLE_IDX,		
	SRVMCAN_TX_NORMAL_DISABLE_IDX,			
	SRVMCAN_COMCTRL_MAX	
}eSrvMcanComCtrl;

typedef enum{
    CAN_ERROR_ACTIVE_STS,
    CAN_ERROR_PASSIVE_STS,
    CAN_BUS_OFF_STS,
    CAN_NOT_USED,
    CAN_ERROR_STS_MAX
}eCanControllerSts;

typedef enum{
    CAN_TRANS_STANDBY_STS = 0,
    CAN_TRANS_NORMAL_STS,
    CAN_TRANS_STS_MAX
}eCanTransCtrlSts;

typedef enum{
	ID0x313,
	ID0x469,
	ID0x245,
	ID0x271,
	ID0x504,
	ID0x3D2,
	ID0x45B,
	ID0x4B1,
	ID0x298,
	ID0x331,
	ID0x4A8,
	ID0x294,
	ID0x234,
	ID0x4BA,	
	ID_MAX	
}eCANIDType;



typedef struct{
	uint32 FLAG_280_IDX:1;
	uint32 FLAG_2E9_IDX:1;
	uint32 FLAG_301_IDX:1;
	uint32 FLAG_305_IDX:1;
	uint32 FLAG_307_IDX:1;
	uint32 FLAG_320_IDX:1;
	uint32 FLAG_321_IDX:1;
	uint32 FLAG_325_IDX:1;
	
	uint32 FLAG_340_IDX:1;
	uint32 FLAG_380_IDX:1;
	uint32 FLAG_391_IDX:1;
	uint32 FLAG_392_IDX:1;
	uint32 FLAG_41A_IDX:1;
	uint32 FLAG_430_IDX:1;
	uint32 FLAG_436_IDX:1;
	uint32 FLAG_440_IDX:1;
	
	uint32 FLAG_449_IDX:1;
	uint32 FLAG_452_IDX:1;
	uint32 FLAG_460_IDX:1;
	uint32 FLAG_475_IDX:1;
	uint32 FLAG_480_IDX:1;
	uint32 FLAG_490_IDX:1;
	uint32 FLAG_4E5_IDX:1;
	uint32 FLAG_508_IDX:1;
	
	uint32 FLAG_519_IDX:1;
	uint32 FLAG_51A_IDX:1;
	uint32 FLAG_51C_IDX:1;
	uint32 FLAG_52E_IDX:1;
	uint32 FLAG_530_IDX:1;
	uint32 FLAG_53A_IDX:1;
	uint32 FLAG_555_IDX:1;
	uint32 FLAG_560_IDX:1;
	
	uint32 FLAG_58A_IDX:1;
	uint32 FLAG_58D_IDX:1;
	uint32 FLAG_5FF_IDX:1;
}tCAN_RX_FLAG;

/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
extern tCAN_RX_FLAG gRxInitFlag;

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/
extern void SrvMcanHsInit(void);
extern void SrvMcanHsDeInit(void);
extern void SrvMcanHsMgr(void);
extern uint8 SrvMcanSendReq(eSrvMcanTxMsg idx);
//extern uint8 SrvMcanSendReqInvalid(eSrvMcanTxMsg idx);
extern uint8 SrvMcanSendReqWithData(eSrvMcanTxMsg idx, uint8* data);
extern eSrvMcanComCtrl SrvMcanComCtrl(eSrvMcanComCtrl contrlIdx);
extern eCanControllerSts SrvMcanGetErrSts(void);
extern void SrvMcanSetTransMode(eCanTransCtrlSts sts);
extern void SrvMcanBusWakeUpProc(void);
extern uint8 SrvMcanGetChangeFlg(eSrvMcanRxMsg msgIdx);
extern void SrvMcanClrChangeFlg(eSrvMcanRxMsg msgIdx);
extern uint8 SrvMcanGetCheckSum(eSrvMcanRxMsg msgIdx);
extern uint8 SrvMcanGetRxMsgCrc(eSrvMcanRxMsg msgIdx);
extern uint8 SrvMcanGetTxCheckSum(eSrvMcanTxMsg msgIdx);
extern uint8 SrvMcanCalCrc(uint8* data, uint8 len, uint8 skipPos);
extern uint32 SrvMcanGetElapsedTime(eSrvMcanRxMsg msgIdx);

extern void SrvMcanTestCgwEmsG_280(void);
extern void SrvMcanTestABSESP1_2E9(void);
extern void SrvMcanTestCgwTcuG_301(void);
extern void SrvMcanTestFrm3_305(void);
extern void SrvMcanTestFcm2_307(void);
extern void SrvMcanTestCGW_ABM_G_320(void);
extern void SrvMcanTestMFS_4_325(void);
extern void SrvMcanTestCgwSamG_340(void);
extern void SrvMcanTestCgwEpsG_380(void);
extern void SrvMcanTestCemBcm1_391(void);
extern void SrvMcanTestCemBcm2_392(void);
extern void SrvMcanTestAvmApa3_41A(void);
extern void SrvMcanTestIcm1_430(void);
extern void SrvMcanTestPLG_436(void);
extern void SrvMcanTestRadar1_440(void);
extern void SrvMcanTestBSD1_449(void);
extern void SrvMcanTestICM2_452(void);
extern void SrvMcanTestWpc1_460(void);
extern void SrvMcanTestAvmApa4_475(void);
extern void SrvMcanTestCemPeps_480(void);
extern void SrvMcanTestCemAlm1_490(void);
extern void SrvMcanTestAbsEsp5_4E5(void);
extern void SrvMcanTestAvmApa2_508(void);
extern void SrvMcanTBOX2_519(void);
extern void SrvMcanTestCemIpm1_51A(void);
extern void SrvMcanTestAvm2_51C(void);
extern void SrvMcanTestIcm3_52E(void);
extern void SrvMcanTestIcm4_530(void);
extern void SrvMcanTestPM25_1_53A(void);
extern void SrvMcanTestAmp1_555(void);
extern void SrvMcanTestAfu1_560(void);
extern void SrvMcanTestDvr1_58A(void);
extern void SrvMcanTestCemBcm4_58D(void);
extern void SrvMcanTestCemBcm3_5FF(void);

extern uint8 Can_GetNMSendState(void);
extern uint8 Can_GetNMRecvState(void);
extern uint8 Can_GetComSendState(void);
extern uint8 Can_GetComRecvState(void);
extern uint8 Can_GetComRecvState(void);
extern void  Can_SetNMSendState(uint8 u8State);
extern void  Can_SetNMRecvState(uint8 u8State);
extern void  Can_SetComSendState(uint8 u8State);
extern void  Can_SetComRecvState(uint8 u8State);
extern void  Can_SetPowerState(uint8 u8State);

extern void CommDTCDetect(void);
extern uint16 Getu16BattValue(void);
extern void CheckDTCStartCondition(void);

#endif /* SERVICE_SRV_MCAN_H_ */
