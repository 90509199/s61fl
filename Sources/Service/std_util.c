/**************************************************************************
 *  PROJECT:        CSP
 *****************************************************************************/
#include "std_util.h"

/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/

/*****************************************************************************/
/* Global variable declarations ('extern', definition in C source)           */
/*****************************************************************************/

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/


/*****************************************************************************/
/*  Function                                                                 */
/*****************************************************************************/
/*****************************************************************************/
/*  UtlQueueInit                                                             */
/*****************************************************************************/
void UtlQueueInit(stUtlQueueType* iStQ, sint8* iU8Buff, uint16 iU8BuffSize)
{
	iStQ->front = 0;
	iStQ->rear = 0;
	iStQ->queueBuff = iU8Buff;
	iStQ->buffSize = iU8BuffSize;
}

/*****************************************************************************/
/*  UtlQisEmpty                                                             */
/*****************************************************************************/
uint8 UtlQisEmpty(stUtlQueueType* iStQ)
{
	return (iStQ->front == iStQ->rear);
}

/*****************************************************************************/
/*  UtlQisFull                                                             */
/*****************************************************************************/
uint8 UtlQisFull(stUtlQueueType* iStQ)
{
	return ( ((iStQ->rear + 1) % iStQ->buffSize) == iStQ->front );
}

/*****************************************************************************/
/*  StdLogEnQueue                                                             */
/*****************************************************************************/
uint8 UtlEnQueue(stUtlQueueType* iStQ, uint8 iU8Data)
{
	uint8 lU8IsFull = FALSE;

	lU8IsFull = UtlQisFull(iStQ);
	
	if(FALSE == lU8IsFull)
	{
		iStQ->rear = ( (iStQ->rear + 1) % iStQ->buffSize );
		iStQ->queueBuff[iStQ->rear] = iU8Data;
	}

	return lU8IsFull;
	
}

/*****************************************************************************/
/*  StdLogEnQueue                                                             */
/*****************************************************************************/
uint8 UtlDeQueue(stUtlQueueType* iStQ, uint8* oU8Data)
{
	uint8 lU8IsEmpty = FALSE;

	lU8IsEmpty = UtlQisEmpty(iStQ);
	
	if(FALSE == lU8IsEmpty)
	{
		iStQ->front = ( (iStQ->front + 1) % iStQ->buffSize );
		*oU8Data = iStQ->queueBuff[iStQ->front];
	}

	return lU8IsEmpty;
}


/*****************************************************************************/
/*  UtlCpyData                                                               */
/*****************************************************************************/
void UtlCopyData(uint8* dst, const uint8* src, uint32 size)
{
    uint32 cnt = 0;

    cnt = size;
    while(0u != cnt)
    {
        cnt -= 1u;
        dst[cnt] = src[cnt];
    }
}

/*****************************************************************************/
/*  UtlSetData                                                               */
/*****************************************************************************/
void UtlSetData(uint8* dst, uint8 data, uint32 size)
{
    uint32 cnt = 0;

    cnt = size;

    while(0u != cnt)
    {
       cnt -= 1u;
        dst[cnt] = data;
    }
}

/*****************************************************************************/
/*  UtlCompData                                                              */
/*****************************************************************************/
uint8 UtlCompData(const uint8* comp1, const uint8* comp2, uint32 size)
{
    uint8 isEqual = TRUE;
    uint32 cnt = 0;

    cnt = size;

    while(0u != cnt)
    {
        cnt -= 1u;

        if(comp1[cnt] != comp2[cnt])
        {
            isEqual = FALSE;
            cnt = 0u;
        }
    }

    return isEqual;
}


/*****************************************************************************/
/*  UtlAddU8U8_U16                                                           */
/*****************************************************************************/
uint16 UtlAddU8U8_U16(const uint8 iU8Num1, const uint8 iU8Num2)
{
    return (uint16)(iU8Num1 + iU8Num2);
}
