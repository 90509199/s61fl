/*
 * srv_mcan.c
 *
 *  Created on: 2020. 3. 6.
 *      Author: Digen
 */
#include "../includes.h"
#include "srv_mcan.h"
#include "std_timer.h"
#include "std_util.h"
#include "Abstraction/abs_can.h"
#include "CanIl/canil_MCan_if.h"
//#include "CanNm/cannm_nm.h"
//#include "CanNm/cannm_app.h"
#include "CanTp/srv_cantp.h"

#include "diag/mnt_flt.h"

#include "drv_par.h"
#include "can_par.h"
#include "il_par.h"
#include "ComStack_Types.h"
#include "Diag/dtc_mgr.h"

extern uint8 u8VariantCoding[];
/*****************************************************************************/
/* Global pre-processor symbols/macros ('#define')                           */
/*****************************************************************************/
#define SRVMCAN_TX_DURATION_0MS		(0u)
#define SRVMCAN_TX_DURATION_10MS	(10u)
#define SRVMCAN_TX_DURATION_20MS	(20u)
#define SRVMCAN_TX_DURATION_50MS	(50u)
#define SRVMCAN_TX_DURATION_100MS	(100u)
#define SRVMCAN_TX_DURATION_200MS	(200u)
#define SRVMCAN_TX_DURATION_300MS	(300u)
#define SRVMCAN_TX_DURATION_400MS	(400u)
#define SRVMCAN_TX_DURATION_500MS	(500u)
#define SRVMCAN_TX_DURATION_1000MS	(1000u)
#define SRVMCAN_TX_DURATION_2000MS	(2000u)
#define SRVMCAN_TX_DURATION_4000MS	(4000u)

#define SRVMCAN_TX_MAILBOX_NM	(26u)
#define SRVMCAN_TX_MAILBOX_DIAG	(27u)
#define SRVMCAN_TX_MAILBOX_EVNT	(28u)
#define SRVMCAN_TX_MAILBOX_CYCL	(29u)
#define SRVMCAN_TX_MAILBOX_CYCL_EVENT	(30u)

#define SRVMCAN_COMCTRL_COM_NORMAL_RXENABLE				(0x01u)
#define SRVMCAN_COMCTRL_COM_NM_RXENABLE					(0x02u)
#define SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE				(0x10u)
#define SRVMCAN_COMCTRL_COM_NM_TXENABLE					(0x20u)

#define SRVMCAN_RETRANS_MAX				(3u)
#define SRVMCAN_INVALID_MAX			(6u)

#define SRVMCAN_SET_STANBY(value)									MCU_CAN0_STB(value)
#define SRVMCAN_SET_EN(value)											MCU_CAN0_EN(value)

#define TIMERPERIOD                                     (1u)
#define NM_VOLTAGE_CHECK_3000MS			(6000u)
#define NM_VOLTAGE_NORMAL				(0u)
#define NM_VOLTAGE_LOW					(1u)
#define NM_VOLTAGE_HIGH					(2u)


/* 20221014 Add DTC config macros */
#define DTC_CONFIG_ESC					(1u)
#define DTC_CONFIG_BMS					(1u)
#define DTC_CONFIG_BCM					(1u)
#define DTC_CONFIG_ICM					(1u)
#define DTC_CONFIG_SAS					(1u)
#define DTC_CONFIG_ACCM					(u8VariantCoding[3u] & 0x60u)
#define DTC_CONFIG_SCM_R				(1u)
#define DTC_CONFIG_AVAS					(1u)
#define DTC_CONFIG_BSD					(u8VariantCoding[2u] & 0x40u)
#define DTC_CONFIG_FCM					(u8VariantCoding[2u] & 0x04u)
#define DTC_CONFIG_TBOX					(1u)
#define DTC_CONFIG_MFS					(1u)
#define DTC_CONFIG_ABM					(1u)
#define DTC_CONFIG_WCM					(u8VariantCoding[0u] & 0x01u)

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/
typedef uint8 (*tSrvCanHsSetMsgCbType)(uint8* data);
typedef uint8* (*tSrvCanHsGetMsgCbType)(void);
typedef void (*tSrvCanHsSetCrcSigType)(uint8 value);

typedef struct{
	uint8 needChecksum;
	uint8 checkSumPos;
	uint8 calChecksumVal;
}tSrvMcanChecksumInfoType;

typedef struct{
	uint8 needCrc;
	uint8 crcPos;
	uint8 calCrcVal;
}tSrvMcanCrcInfoType;

typedef struct{
uint32 id;
uint8 mailBoxIdx;
flexcan_data_info_t dataInfo;
stTimerType cycleTimer;
uint16 cycleTime;
stTimerType retransTimer;
uint16 retransTime;
tSrvCanHsSetMsgCbType setMsgCbFunc;
tSrvCanHsGetMsgCbType getMsgCbFunc;
tSrvCanHsGetMsgCbType getMsgCbFuncInvalid;
uint8 retransCount;
tSrvMcanChecksumInfoType checkSumInfo;
tSrvCanHsSetCrcSigType setCrcSigFunc;
}tSrvMcanTxMsgInfoType;

typedef struct{
flexcan_id_table_t idTable;
uint8 length;
stTimerType timer;
uint16 cycleTime;
tSrvCanHsSetMsgCbType setMsgCbFunc;
tSrvCanHsGetMsgCbType getMsgCbFunc;
uint8 changeFlg;
tSrvMcanChecksumInfoType checksumInfo;
tSrvMcanCrcInfoType crcInfo;
}tSrvMcanRxMsgInfoType;

typedef enum{
	SRVMCAN_TX_MB_NM_IDX = 0,
	SRVMCAN_TX_MB_DIAG_IDX = 1,
	SRVMCAN_TX_MB_EVENT_IDX = 2,
	SRVMCAN_TX_MB_CYCL_IDX = 3,	
	SRVMCAN_TX_MB_CYCL_EVENT_IDX = 4,		
	SRVMCAN_TX_MB_MAX	
}eSrvMcanTxMailBoxType;

typedef struct{
	uint16 u16Counter[ID_MAX];
}tCANTimeoutType;

tCANTimeoutType StCANTimeout = {0u};
/*****************************************************************************/
/* Global variable declarations ('extern', definition in C source)           */
/*****************************************************************************/
static tSrvMcanTxMsgInfoType gStSrvMcanTxDataTable[RRM_TX_MAX] = {
	/* RRM_7_453_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
		/* CAN ID */
		0x453u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_500MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_500MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_0MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_7,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_7, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

	/* RRM_1_515_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
		/* CAN ID */
		0x515u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_EVNT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_1,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_1, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_1_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL		
	},

	/* RRM_2_516_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
		/* CAN ID */
		0x516u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_500MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_500MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_2,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_2, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

	/* RRM_3_517_IDX - DLC : 8, Cyclic Time : 1000ms - */
	{
		/* CAN ID */
		0x517u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_1000MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_10MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_10MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_3,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_3, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

	/* RRM_8_51D_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
		/* CAN ID */
		0x51Du,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_EVNT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_8,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_8, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_8_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},
#ifdef CX62C_FUNCTION
//JANG_211112   SRVMCAN_TX_DURATION_500MS->SRVMCAN_TX_DURATION_10MS
	/* RRM_4_52D_IDX - DLC : 8, Cyclic Time : 0ms -*/
	{
		/* CAN ID */
		0x52Du,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_EVNT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_10MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_10MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_4,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_4, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

#else
	/* RRM_4_52D_IDX - DLC : 8, Cyclic Time : 0ms -*/
	{
		/* CAN ID */
		0x52Du,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_EVNT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_4,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_4, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_4_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

#endif
	/* RRM_5_52F_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
		/* CAN ID */
		0x52Fu,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_EVNT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_5,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_5, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_5_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

	/* RRM_9_537_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
		/* CAN ID */
		0x537u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_100MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_0MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_9,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_9, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL		
	},

	/* RRM_10_53D_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
		/* CAN ID */
		0x53Du,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_500MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_500MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_10,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_10, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

	/* RRM_6_540_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
		/* CAN ID */
		0x540u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_100MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_6,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_6, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_6_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},	

	/* RRM_11_55D_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
		/* CAN ID */
		0x55Du,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_500MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_500MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_11,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_11, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},	

	/* RRM_WPC_56D_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
		/* CAN ID */
		0x56Du,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_EVNT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_WPC,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_WPC, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_WPC_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},	 

	/* RRM_WPC_57A_IDX - DLC : 8, Cyclic Time : 1000ms - */
	{
		/* CAN ID */
		0x57Au,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_1000MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_DVR_1,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_DVR_1, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},	

	/* RRM_DVR_2_57B_IDX - DLC : 8, Cyclic Time : 1000ms - */
	{
		/* CAN ID */
		0x57Bu,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_1000MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_DVR_2,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_DVR_2, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},
	
	/* RRM_DVR_3_57C_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
		/* CAN ID */
		0x57Cu,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_100MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_DVR_3,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_DVR_3, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_DVR_3_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ TRUE,				7,					0	},
		/* Set CRC Signal Function */
		CanIfSet_gCanIf_RRM_DVR_3_CRCCheck
	},
	
	/* RRM_DMS_1_59A_IDX  - DLC : 8, Cyclic Time : 0ms - */
	{
		/* CAN ID */
		0x59Au,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_1000MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_100MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_100MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_DMS_1,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_DMS_1, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_DMS_1_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},	

	/* RRM_BCM_15_385_IDX - DLC : 8, Cyclic Time : 50ms - */
	{
		/* CAN ID */
		0x385u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL_EVENT,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_50MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_50MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_50MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_50MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_RRM_BCM_15,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_BCM_15, 
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_RRM_BCM_15_Invalid, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},

	/* NWM_RRM_618_IDX */
	{
		/* CAN ID */
		0x618u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_NM,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	2u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration							Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 			0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_0MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_NWM_RRM,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_NWM_RRM, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},
	
	/* DIAG_TX_75A_IDX */
	{
		/* CAN ID */
		0x753u,//0x75Au,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_DIAG,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration						Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_0MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_Diag_Resp,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_Diag_Resp,
		/* Get Message Function */		
		NULL,
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},
	#ifdef CAN_TX_392_ADD 
	/* BCM_4_392_IDX */
	{
		/* CAN ID */
		0x392u,
		/* Mail Box Idx */
		SRVMCAN_TX_MAILBOX_CYCL,
		/* message ID type    	data length 	fd enable	fd padding		enable brs		is remote   */
		{FLEXCAN_MSG_ID_STD, 	8u,  			FALSE,	 	0u, 			FALSE, 			FALSE},		/* CAN DATA INFO */
		/* Duration						Start		Target		Status  */	
		{ SRVMCAN_TX_DURATION_20MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_20MS, 
		/* Duration							Start		Target		Status	*/	
		{ SRVMCAN_TX_DURATION_0MS,			0,			0,			TIMER_STATE_STOP }, 	/* timer */
		/* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
		/* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_BCM_2,   
		/* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_BCM_2, 
		/* Get Message Function */		
		NULL, 
		/* Retrans Count */
		0,
		/* need CheckSum			CheckSumPos				CheckSumCrcVal */
		{ FALSE,				0,					0	},
		/* Set CRC Signal Function */
		NULL
	},
	#endif

};

static tSrvMcanRxMsgInfoType gStSrvMcanRxDataTable[RRM_RX_MAX] = {
	/* CGW_EMS_G_280_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x280u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration	100ms					Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CGW_EMS_G,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CGW_EMS_G,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}
	},

	/* ABS_ESP_1_2E9_IDX - DLC : 8, Cyclic Time : 10ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x2E9u },		/* ID table */
     /* data length */		
		8u, 		
	 /* Duration		10ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_200MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_200MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_ABS_ESP_1,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_ABS_ESP_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,					5,					0	},	
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}
	},

	/* CGW_TCU_G_301_IDX - DLC : 2, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x301u },		/* ID table */
     /* data length */			
		2u, 		
	 /* Duration		100ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CGW_TCU_G,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CGW_TCU_G,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* FRM_3_305_IDX - DLC : 8, Cyclic Time : 50ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x305u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		50ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_1000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_FRM_3,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_FRM_3,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},		
     /* need CRC			CrcPos				calCrcVal */
	    { TRUE,					7,					0	}		
	},	

	/* FCM_2_307_IDX - DLC : 8, Cyclic Time : 50ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x307u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		50ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_1000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_FCM_2,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_FCM_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { TRUE,					7,					0	}		
	},	

	/* CGW_ABM_G_320_IDX - DLC : 4, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x320u },		/* ID table */
     /* data length */			
		4u, 		
	 /* Duration		500ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CGW_ABM_G,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CGW_ABM_G,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,					2,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},		
	#ifdef CHANG_TO_MFS_3
	/* MFS_2_323_IDX - DLC : 6, Cyclic Time : 20ms - */ // CHERY SPEC (CAN TRANS)
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x323u },		/* ID table */
     /* data length */			
		6u, 		
	 /* Duration		20ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_400MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_400MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_MFS_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_MFS_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},
	#else
	/* MFS_2_321_IDX - DLC : 6, Cyclic Time : 20ms - */ // CHERY SPEC (CAN TRANS)
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x321u },		/* ID table */
     /* data length */			
		6u, 		
	 /* Duration		20ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_400MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_400MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_MFS_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_MFS_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	
	#endif

	/* MFS_4_325_IDX - DLC : 6, Cyclic Time : 20ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x325u },		/* ID table */
     /* data length */			
		6u, 		
	 /* Duration		20ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_400MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_400MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_MFS_4,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_MFS_4,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},

	/* CGW_SAM_G_340_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x340u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		100ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CGW_SAM_G,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CGW_SAM_G,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { TRUE,					7,					0	}			
	},

	/* CGW_EPS_G_380_IDX - DLC : 3, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x380u },		/* ID table */
     /* data length */			
		3u, 		
	 /* Duration		100ms					Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CGW_EPS_G,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CGW_EPS_G,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},

	/* CEM_BCM_1_391_IDX - DLC : 8, Cyclic Time : 20ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x391u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		20ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_400MS, 		0, 			0, 			TIMER_STATE_RUN },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_400MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_BCM_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_BCM_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,					1,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}			
	},

	/* CEM_BCM_2_392_IDX - DLC : 8, Cyclic Time : 20ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x392u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		20ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_400MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_400MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_BCM_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_BCM_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}				
	},	

	/* AVM_APA_3_41A_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x41Au },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		100ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_RUN },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AVM_APA_3,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AVM_APA_3,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}				
	},	

	/* ICM_1_430_IDX - DLC : 8, Cyclic Time : 20ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x430u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			20ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_400MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_400MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_ICM_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_ICM_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}				
	},		

	/* PLG_436_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x436u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		100ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_PLG,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_PLG,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,				7,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}					
	},	

	/* RADAR_1_440_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x440u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration		100ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_RADAR_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_RADAR_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}						
	},	

	/* BSD_1_449_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x449u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_BSD_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_BSD_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,					5,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}							
	},	

	/* ICM_2_452_IDX - DLC : 4, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x452u },		/* ID table */
     /* data length */			
		4u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_RUN },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_ICM_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_ICM_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}								
	},	

	/* WPC_1_460_IDX - DLC : 4, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x460u },		/* ID table */
     /* data length */			
		4u, 		
	 /* Duration		500ms				Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_WPC_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_WPC_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}								
	},		

	/* AVM_APA_4_475_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x475u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AVM_APA_4,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AVM_APA_4,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}								
	},	

	/* CEM_PEPS_480_IDX - DLC : 8, Cyclic Time : 50ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x480u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			50ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_1000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_1000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_PEPS,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_PEPS,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,				7,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}									
	},	 

	/* CEM_ALM_1_490_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x490u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_ALM_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_ALM_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}										
	},	 

	/* ABS_ESP_5_4E5_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x4E5u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_ABS_ESP_5,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_ABS_ESP_5,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,					7,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}											
	},	 

	/* AVM_APA_2_508_IDX - DLC : 2, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x508u },		/* ID table */
     /* data length */			
		2u, 		
	 /* Duration				500ms		Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AVM_APA_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AVM_APA_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}												
	},	 

	/* TBOX_2_519_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x519u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration							Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_TBOX_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_TBOX_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { TRUE,					7,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}													
	},

	/* CEM_IPM_1_51A_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x51Au },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_2000MS, 		0, 			0, 			TIMER_STATE_RUN },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_2000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_IPM_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_IPM_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* AVM_2_51C_IDX - DLC : 3, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x51Cu },		/* ID table */
     /* data length */			
		3u, 		
	 /* Duration				500ms		Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AVM_2,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AVM_2,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* ICM_3_52E_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x52Eu },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration							Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_ICM_3,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_ICM_3,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* ICM_4_530_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x530u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			500ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_ICM_4,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_ICM_4,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* PM25_1_53A_IDX - DLC : 8, Cyclic Time : 1000ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x53Au },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			1000ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_PM25_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_PM25_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* AFU_1_560_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x560u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			500ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AFU_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AFU_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* AMP_1_555_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x555u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			500ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AMP_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AMP_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* DVR_1_58A_IDX - DLC : 8, Cyclic Time : 100ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x58Au },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			100ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_500MS, 		0, 			0, 			TIMER_STATE_RUN },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_500MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_DVR_1,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_DVR_1,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},

	/* CEM_BCM_4_58D_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x58Du },		/* ID table */
     /* data length */		
		8u, 		
	 /* Duration			500ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_BCM_4,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_BCM_4,   			
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},	

	/* CEM_BCM_3_5FF_IDX - DLC : 8, Cyclic Time : 500ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x5FFu },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration			500ms			Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_4000MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_4000MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_CEM_BCM_3,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_CEM_BCM_3,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},
	
	/* NWM_CGW_600_IDX - DLC : 8, Cyclic Time : 0ms - */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x600u },		/* ID table */
     /* data length */			
		8u, 		
	 /* Duration							Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_NWM_CGW,  
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_NWM_CGW,
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}		
	},		
	
	/* DIAG_RX_74A_IDX */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x743u },		/* ID table */
     /* data length */		
		8u, 		
	 /* Duration						Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_Diag_Req,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_Diag_Req,   			
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}			
	},

	/* DIAG_RX_7DF_IDX */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x7DFu },		/* ID table */
     /* data length */		
		8u, 		
	 /* Duration						Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_Diag_Req,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_Diag_Req,   			
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}			
	},

	/* TEST_RX_7FF_IDX */
	{
	 /* Remote Frame    	Extended Frame		CAN ID	*/
		{ FALSE, 			FALSE, 				0x7FFu },		/* ID table */
     /* data length */		
		8u, 		
	 /* Duration						Start		Target		Status  */	
     	{ SRVMCAN_TX_DURATION_0MS, 		0, 			0, 			TIMER_STATE_STOP },		/* timer */
	 /* Cycle Time */				
		SRVMCAN_TX_DURATION_0MS, 
     /* Set Message Function */			
		CanIfSetMsg_gCanIf_AgingTest,   
	 /* Get Message Function */		
		CanIfGetMsg_gCanIf_AgingTest,   			
     /* Change Flg */	
        FALSE,		
     /* need Checksum		checksumPos			calChecksumVal */
	    { FALSE,				0,					0	},
     /* need CRC			CrcPos				calCrcVal */
	    { FALSE,				0,					0	}			
	}
	
};

static flexcan_id_table_t gStSrvMcanRxIdTable[RRM_RX_MAX];

static uint8 gU8SrvMcanComCtrlMask;
static uint8 gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_MAX];

static uint8 gU8SrvMcanDebugFlg[RRM_RX_MAX];

tCAN_RX_FLAG gRxInitFlag;

/* wl20220725 */
static uint8 u8NMSendEnable   = 1;
static uint8 u8NMRecvEnable   = 1;
static uint8 u8ComSendEnable  = 1;
static uint8 u8ComRecvEnable  = 1;
static uint8 u8PowerState     = 1;
static uint16 u16VoltageHighCnt = 0;
static uint16 u16VoltageLowCnt  = 0;
static uint8 u8VoltageFlag      = 0;
uint8 Can_GetNMSendState(void)
{
	return u8NMSendEnable;
}

uint8 Can_GetNMRecvState(void)
{
	return u8NMRecvEnable;
}

uint8 Can_GetComSendState(void)
{
	return u8ComSendEnable;
}

uint8 Can_GetComRecvState(void)
{
	return u8ComSendEnable;
}

uint8 Can_GetPowerState(void)
{
	return u8PowerState;
}

void Can_SetNMSendState(uint8 u8State)
{
	u8NMSendEnable = u8State;
}

void Can_SetNMRecvState(uint8 u8State)
{
	u8NMRecvEnable = u8State;
}

void Can_SetComSendState(uint8 u8State)
{
	u8ComSendEnable = u8State;
}

void Can_SetComRecvState(uint8 u8State)
{
	u8ComSendEnable = u8State;
}

void Can_SetPowerState(uint8 u8State)
{
	u8PowerState = u8State;
}

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/
/*static*/ void SrvMcanRxInd(uint32 buffIdx, uint32 cs, uint32 msgId, uint8* data, uint8 dataLen);
static void SrvMcanTxConf(uint32 msgId);
static void SrvMcanErrorInd(void);
static void SrvMcanCalCheckSum(eSrvMcanRxMsg msgIdx);
static void SrvMcanCalTxCheckSum(eSrvMcanTxMsg msgIdx);
static void SrvMcanReTransProc(void);
static uint8 SrvMcanSendPriodicReq(eSrvMcanTxMsg idx);
static void SrvMcanTxProc(void);
static void SrvMcanNormalMsgTxStartProc(void);
static void SrvMcanNormalMsgTxStopProc(void);
static void SrvMcanMntLosComFlt(void);
static void VoltageDTCDetect(void);
static void OtherDTCDetect(void);
void CheckDTCStartCondition(void);
void GetBattValuePeriod(void);
uint16 Getu16BattValue(void);
uint8 GetDTCStartProcessState(void);
void CommLostReset(uint8 u8ID);
void CommConnectIncrease(uint8 u8ID, uint16 u16Ms, uint16 u16Delt);
uint16 GetCANTimeoutCounter(uint8 u8ID);

/*****************************************************************************/
/*  Function															*/
/*****************************************************************************/

/*****************************************************************************/
/*  SRV CAN initialization													 */
/*****************************************************************************/
void SrvMcanHsInit(void)
{
	uint8 idx = 0;

	#ifdef CAN_TX_392_ADD 
	uint8 tempData[8] = {0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

	CanIfSetMsg_gCanIf_CEM_BCM_2(tempData);
	#endif

	//set CAN structure 
	AbsCanHs0RegRxIndCb(SrvMcanRxInd);
	AbsCanHs0RegTxConfCb(SrvMcanTxConf);
	AbsCanHs0RegErrIndCb(SrvMcanErrorInd);

	ABSCANHS0_DISABLE_MB_IRQ();

	AbsCanHs0Init();//WL0713

	ABSCANHS0_CONFIG_TXMB(SRVMCAN_TX_MAILBOX_NM, &gStSrvMcanTxDataTable[NWM_RRM_618_IDX].dataInfo, 0);	
	ABSCANHS0_CONFIG_TXMB(SRVMCAN_TX_MAILBOX_EVNT, &gStSrvMcanTxDataTable[RRM_1_515_IDX].dataInfo, 0);		
	ABSCANHS0_CONFIG_TXMB(SRVMCAN_TX_MAILBOX_CYCL, &gStSrvMcanTxDataTable[RRM_7_453_IDX].dataInfo, 0);

	ABSCANHS0_CONFIG_TXMB(SRVMCAN_TX_MAILBOX_DIAG, &gStSrvMcanTxDataTable[DIAG_TX_75A_IDX].dataInfo, 0);


	for(idx = 0 ; idx < RRM_RX_MAX ; idx++)
	{
		gStSrvMcanRxIdTable[idx].id = gStSrvMcanRxDataTable[idx].idTable.id;
		gStSrvMcanRxIdTable[idx].isExtendedFrame = gStSrvMcanRxDataTable[idx].idTable.isExtendedFrame;
		gStSrvMcanRxIdTable[idx].isRemoteFrame = gStSrvMcanRxDataTable[idx].idTable.isRemoteFrame;
	}

	ABSCANHS0_CONFIG_RX_FIFO(FLEXCAN_RX_FIFO_ID_FORMAT_A, gStSrvMcanRxIdTable);
	ABSCANHS0_SET_RXMASKTYPE_INDIVIDUAL();

	for(idx = 0 ; idx < RRM_RX_MAX ; idx++)
	{
		ABSCANHS0_SET_RXFIFO_INDIVIDUAL_MASK(FLEXCAN_MSG_ID_STD, idx, (0xC0000000|0x7FF));
	}

	ABSCANHS0_START_RXFIFO();

	SRVMCAN_SET_STANBY(HIGH);
	SRVMCAN_SET_EN(HIGH);	

	ABSCANHS0_ENABLE_MB_IRQ();
	#if 1		//temporary
	INT_SYS_SetPriority( CAN0_ORed_0_15_MB_IRQn, 0 );
	INT_SYS_SetPriority( CAN0_ORed_16_31_MB_IRQn, 0 );
	#else
	INT_SYS_SetPriority( CAN0_ORed_0_15_MB_IRQn, 1 );
	INT_SYS_SetPriority( CAN0_ORed_16_31_MB_IRQn, 1 );
	#endif

	memset(&gRxInitFlag, 0x00, sizeof(gRxInitFlag));	//clear can flag
}

/*****************************************************************************/
/*  SRV CAN de-initialization													 */
/*****************************************************************************/
void SrvMcanHsDeInit(void)
{
	AbsCanHs0DeInit();
	SRVMCAN_SET_STANBY(LOW);
}

/*****************************************************************************/
/*  SRV CAN Task Manager                                                    */
/*****************************************************************************/
void SrvMcanHsMgr(void)
{
	static u32Tick = 0;

	SrvMcanTxProc();
	
// CHERY SPEC (CAN TRANS)	#if 0
// CHERY SPEC (CAN TRANS)	if(TRUE == gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_CYCL_EVENT_IDX])
	{
// CHERY SPEC (CAN TRANS)		gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_CYCL_EVENT_IDX] = FALSE;
		SrvMcanReTransProc();
	}
// CHERY SPEC (CAN TRANS)	#endif

// CHERY SPEC (CAN TRANS)	if(TRUE == gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_EVENT_IDX])
	{
// CHERY SPEC (CAN TRANS)		gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_EVENT_IDX] = FALSE;
// CHERY SPEC (CAN TRANS)		SrvMcanReTransProc();
	}
	// if(GetDTCStartProcessState() == 1u)
	// {
		// SrvMcanMntLosComFlt();
	// }
	// else
	// {
	// 	/* Do nothing */
	// }
	/* wl 20220730 */
	if(2<=GetElapsedTime(u32Tick))
	{
		u32Tick = GetTickCount();
		GetBattValuePeriod();
		VoltageDTCDetect();
		OtherDTCDetect();
	}
	
	
}

/*****************************************************************************/
/*  SrvMcanSendMsg                                                    */
/*****************************************************************************/
uint8 SrvMcanSendReq(eSrvMcanTxMsg idx)
{
	uint8* lpU8TxData = NULL;
	status_t status = STATUS_SUCCESS;

	if((gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id == gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_TXENABLE))
	{/* NM MESSAGE */
		lpU8TxData = gStSrvMcanTxDataTable[NWM_RRM_618_IDX].getMsgCbFunc();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[NWM_RRM_618_IDX].mailBoxIdx);

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[NWM_RRM_618_IDX].mailBoxIdx, &gStSrvMcanTxDataTable[NWM_RRM_618_IDX].dataInfo, gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}
	}
	#ifdef CX62C_FUNCTION
	else if((gStSrvMcanTxDataTable[RRM_4_52D_IDX].id == gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE))
	{
		/* TP  MESSAGE */
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFunc();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
	}
	#endif
	else if((gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id != gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE))
	{/* NORMAL MESSAGE */
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFunc();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (FALSE != gStSrvMcanTxDataTable[idx].checkSumInfo.needChecksum) && (NULL != gStSrvMcanTxDataTable[idx].setCrcSigFunc))
		{
			SrvMcanCalTxCheckSum(idx);
		}

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}

		if( (STATUS_SUCCESS == status) 
			&& ((SRVMCAN_TX_MAILBOX_CYCL_EVENT == gStSrvMcanTxDataTable[idx].mailBoxIdx) || (SRVMCAN_TX_MAILBOX_EVNT == gStSrvMcanTxDataTable[idx].mailBoxIdx)) )
		{
			if (SRVMCAN_TX_DURATION_0MS != gStSrvMcanTxDataTable[idx].cycleTime)
			{
				/* Stop periodic transmission */
				STOP_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer); // CHERY SPEC (CAN TRANS)
			}

			/* Start event transmission */
			gStSrvMcanTxDataTable[idx].retransCount  = 0; // CHERY SPEC (CAN TRANS)
			gStSrvMcanTxDataTable[idx].retransCount += 1;
			START_TIMER(gStSrvMcanTxDataTable[idx].retransTimer, gStSrvMcanTxDataTable[idx].retransTime); // CHERY SPEC (CAN TRANS)
		}

	}
	else
	{
		status = STATUS_UNSUPPORTED;
	}
	
	return (uint8)status;
}

uint8 SrvMcanSendReqRetrans(eSrvMcanTxMsg idx) // CHERY SPEC (CAN TRANS)
{
	uint8* lpU8TxData = NULL;
	status_t status = STATUS_SUCCESS;

	if((gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id != gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE))
	{/* NORMAL MESSAGE */
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFunc();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (FALSE != gStSrvMcanTxDataTable[idx].checkSumInfo.needChecksum) && (NULL != gStSrvMcanTxDataTable[idx].setCrcSigFunc))
		{
			SrvMcanCalTxCheckSum(idx);
		}

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}

		if( (STATUS_SUCCESS == status) 
			&& ((SRVMCAN_TX_MAILBOX_CYCL_EVENT == gStSrvMcanTxDataTable[idx].mailBoxIdx) || (SRVMCAN_TX_MAILBOX_EVNT == gStSrvMcanTxDataTable[idx].mailBoxIdx)) )
		{
			if (SRVMCAN_TX_DURATION_0MS != gStSrvMcanTxDataTable[idx].cycleTime)
			{
				/* Stop periodic transmission */
				STOP_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer); // CHERY SPEC (CAN TRANS)
			}
			
			/* Start event transmission */
			gStSrvMcanTxDataTable[idx].retransCount += 1;
			START_TIMER(gStSrvMcanTxDataTable[idx].retransTimer, gStSrvMcanTxDataTable[idx].retransTime);
		}

	}
	else
	{
		status = STATUS_UNSUPPORTED;
	}
	
	return (uint8)status;
}

uint8 SrvMcanSendReqInvalid(eSrvMcanTxMsg idx) // CHERY SPEC (CAN TRANS)
{
	uint8* lpU8TxData = NULL;
	status_t status = STATUS_SUCCESS;

	if((gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id != gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE))
	{/* NORMAL MESSAGE */
		if (NULL == gStSrvMcanTxDataTable[idx].getMsgCbFuncInvalid)
		{
			STOP_TIMER(gStSrvMcanTxDataTable[idx].retransTimer);
			gStSrvMcanTxDataTable[idx].retransCount = 0;
			
			status = STATUS_UNSUPPORTED;
			return (uint8)status;
		}
		
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFuncInvalid();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (FALSE != gStSrvMcanTxDataTable[idx].checkSumInfo.needChecksum) && (NULL != gStSrvMcanTxDataTable[idx].setCrcSigFunc))
		{
			SrvMcanCalTxCheckSum(idx);
		}

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}

		if( (STATUS_SUCCESS == status) 
			&& ( (SRVMCAN_TX_MAILBOX_EVNT == gStSrvMcanTxDataTable[idx].mailBoxIdx)) )
		{
			gStSrvMcanTxDataTable[idx].retransCount += 1;
			START_TIMER(gStSrvMcanTxDataTable[idx].retransTimer, gStSrvMcanTxDataTable[idx].retransTime);
		}

	}
	else
	{
		status = STATUS_UNSUPPORTED;
	}
	
	return (uint8)status;
}

/*****************************************************************************/
/*  SrvMcanSendReqWithData                                                    */
/*****************************************************************************/
uint8 SrvMcanSendReqWithData(eSrvMcanTxMsg idx, uint8* data)
{
	uint8* lpU8TxData = NULL;
	status_t status = STATUS_SUCCESS;
	VAR( PduInfoType, AUTOMATIC ) nmPduInfo;
	VAR( Std_ReturnType, AUTOMATIC ) retVal;
#if 0
	if(NULL != data)
	{
		gStSrvMcanTxDataTable[idx].setMsgCbFunc(data);
	}

	if( (gStSrvMcanTxDataTable[DIAG_TX_75A_IDX].id == gStSrvMcanTxDataTable[idx].id) || UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE) )
	{/* NORMAL MESSAGE */
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFunc();

		if( (FALSE != gStSrvMcanTxDataTable[idx].checkSumInfo.needChecksum) && (NULL != gStSrvMcanTxDataTable[idx].setCrcSigFunc))
		{
			SrvMcanCalTxCheckSum(idx);
		}
		
		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}

		if( (STATUS_SUCCESS == status) 
			&& ((SRVMCAN_TX_MAILBOX_CYCL_EVENT == gStSrvMcanTxDataTable[idx].mailBoxIdx) || (SRVMCAN_TX_MAILBOX_EVNT == gStSrvMcanTxDataTable[idx].mailBoxIdx)) )
		{
			gStSrvMcanTxDataTable[idx].retransCount += 1;
		}
	}
	else
	{
		status = STATUS_UNSUPPORTED;
	}
#endif	
	
	nmPduInfo.SduDataPtr = data;
	nmPduInfo.SduLength  = 8u;
	/* wl20220720 Transmit a Diag message. */
    // retVal = CanIf_Transmit( IlTxMsgHndIHU_DTC_2, &nmPduInfo );

	// if( retVal == E_OK )
  	// {
	// 	/* Do nothing */
	// }
	// else
	// {
	// 	status = STATUS_ERROR;
	// }

	return (uint8)status;
}

/*****************************************************************************/
/*  SrvMcanComCtrl                                                    */
/*****************************************************************************/
eSrvMcanComCtrl SrvMcanComCtrl(eSrvMcanComCtrl contrlIdx)
{
	switch(contrlIdx)
	{
		/*  RX */
		case SRVMCAN_RX_ALL_ENABLE_IDX:
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_RXENABLE);
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_RXENABLE);			
			break;

		case SRVMCAN_RX_NM_ENABLE_IDX:
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_RXENABLE);
			break;

		case SRVMCAN_RX_NORMAL_ENABLE_IDX:
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_RXENABLE);
			break;

		case SRVMCAN_RX_ALL_DISABLE_IDX:
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_RXENABLE);
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_RXENABLE);			

		case SRVMCAN_RX_NM_DISABLE_IDX:
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_RXENABLE);
			break;

		case SRVMCAN_RX_NORMAL_DISABLE_IDX:
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_RXENABLE);			
			break;

		/* TX */
		case SRVMCAN_TX_ALL_ENABLE_IDX:
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_TXENABLE);
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE);
			SrvMcanNormalMsgTxStartProc();			
			break;

		case SRVMCAN_TX_NM_ENABLE_IDX:
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_TXENABLE);
			break;

		case SRVMCAN_TX_NORMAL_ENABLE_IDX:
			UTL_MASK_SET(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE);
			SrvMcanNormalMsgTxStartProc();
			break;

		case SRVMCAN_TX_ALL_DISABLE_IDX:
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_TXENABLE);
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE);
			SrvMcanNormalMsgTxStopProc();			
			break;

		case SRVMCAN_TX_NM_DISABLE_IDX:
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_TXENABLE);
			break;

		case SRVMCAN_TX_NORMAL_DISABLE_IDX:
			UTL_MASK_CLR(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE);
			SrvMcanNormalMsgTxStopProc();
			break;
			
		default :
			break;
	}

	return gU8SrvMcanComCtrlMask;
}

/*****************************************************************************/
/*  SrvMcanTxProc                                                       */
/*****************************************************************************/
eCanControllerSts SrvMcanGetErrSts(void)
{
	eCanControllerSts errSts = CAN_ERROR_ACTIVE_STS;
	uint32 err = 0u;
	uint8 fltConf = 0u;
	uint8 busOff = 0u;

	err = ABSCANHS0_GET_ERROR_STS();

	fltConf = ((err & 0x30) >> 4);
	busOff = (fltConf & 0x10U);

	if(FALSE != busOff)
	{
		errSts = CAN_ERROR_PASSIVE_STS;
	}
	else if(0x01u == fltConf)
	{
		errSts = CAN_BUS_OFF_STS;
	}
	else
	{
		;
	}

	return errSts;
}

/*****************************************************************************/
/*  SrvMcanTxProc                                                       */
/*****************************************************************************/
void SrvMcanSetTransMode(eCanTransCtrlSts sts)
{
	if(CAN_TRANS_STANDBY_STS == sts)
	{
		ABSCANHS0_DISABLE_MB_IRQ();
		
		ABSCANHS0_ABORT_TRANSFER(SRVMCAN_TX_MAILBOX_NM);
		ABSCANHS0_ABORT_TRANSFER(SRVMCAN_TX_MAILBOX_DIAG);
		ABSCANHS0_ABORT_TRANSFER(SRVMCAN_TX_MAILBOX_EVNT);
		ABSCANHS0_ABORT_TRANSFER(SRVMCAN_TX_MAILBOX_CYCL);
		ABSCANHS0_ABORT_TRANSFER(SRVMCAN_TX_MAILBOX_CYCL_EVENT);

		AbsCanHs0DeInit();

		SRVMCAN_SET_STANBY(LOW);
		SRVMCAN_SET_EN(LOW);			
	}
	else if(CAN_TRANS_NORMAL_STS == sts)
	{
		SrvMcanHsInit();
	}
	else
	{
		;
	}
}

/*****************************************************************************/
/*  SrvMcanBusWakeUpProc                                                       */
/*****************************************************************************/
void SrvMcanBusWakeUpProc(void)
{
	CannmBusWakeUpInd();
	CanNmAppWakeupProc();
}

/*****************************************************************************/
/*  SrvMcanGetChangeFlg                                                       */
/*****************************************************************************/
uint8 SrvMcanGetChangeFlg(eSrvMcanRxMsg msgIdx)
{
	return gStSrvMcanRxDataTable[msgIdx].changeFlg;
}

/*****************************************************************************/
/*  SrvMcanClrChangeFlg                                                       */
/*****************************************************************************/
void SrvMcanClrChangeFlg(eSrvMcanRxMsg msgIdx)
{
	gStSrvMcanRxDataTable[msgIdx].changeFlg = FALSE;
}

/*****************************************************************************/
/*  SrvMcanClrChangeFlg                                                       */
/*****************************************************************************/
uint8 SrvMcanGetCheckSum(eSrvMcanRxMsg msgIdx)
{
	DPRINTF(DBG_MSG6, "[SRV_MCAN] Get Checksum : %X\n", gStSrvMcanRxDataTable[msgIdx].checksumInfo.calChecksumVal);
	
	return gStSrvMcanRxDataTable[msgIdx].checksumInfo.calChecksumVal;
}

/*****************************************************************************/
/*  SrvMcanCalCheckSum                                                       */
/*****************************************************************************/
static void SrvMcanCalCheckSum(eSrvMcanRxMsg msgIdx)
{
	uint8 idx = 0;
	uint8* data = NULL;
	uint8 checksum = 0;
	
	DPRINTF(DBG_MSG7, "[SRV_MCAN] Cal checkSum [ID : %X] [Skip Pos : %X] [LEN : %d]\n", gStSrvMcanRxDataTable[msgIdx].idTable.id, gStSrvMcanRxDataTable[msgIdx].checksumInfo.checkSumPos, gStSrvMcanRxDataTable[msgIdx].length);

	data = gStSrvMcanRxDataTable[msgIdx].getMsgCbFunc();

	for(idx = 0 ; idx < gStSrvMcanRxDataTable[msgIdx].length ; idx ++)
	{
		if(idx != gStSrvMcanRxDataTable[msgIdx].checksumInfo.checkSumPos)
		{
			checksum += data[idx];
		}
		else
		{
			DPRINTF(DBG_MSG7, "[SRV_MCAN]  %d Rx Checksum value [%X]\n", idx, data[idx]);
		}
	}

	gStSrvMcanRxDataTable[msgIdx].checksumInfo.calChecksumVal = ( (checksum & 0xFF) ^ 0xFF );
	
//	DPRINTF(DBG_MSG7, "[SRV_MCAN] Calculation Checksum : %X\n", gStSrvMcanRxDataTable[msgIdx].checksumInfo.calChecksumVal);

}

/*****************************************************************************/
/*  SrvMcanClrChangeFlg                                                       */
/*****************************************************************************/
uint8 SrvMcanGetTxCheckSum(eSrvMcanTxMsg msgIdx)
{
	return gStSrvMcanTxDataTable[msgIdx].checkSumInfo.calChecksumVal;
}

/*****************************************************************************/
/*  SrvMcanCalCheckSum                                                       */
/*****************************************************************************/
static void SrvMcanCalTxCheckSum(eSrvMcanTxMsg msgIdx)
{
	uint8 idx = 0;
	uint8* data = NULL;
	uint8 checksum = 0;
	
	data = gStSrvMcanTxDataTable[msgIdx].getMsgCbFunc();

	for(idx = 0 ; idx < gStSrvMcanTxDataTable[msgIdx].dataInfo.data_length ; idx ++)
	{
		if(idx != gStSrvMcanTxDataTable[msgIdx].checkSumInfo.checkSumPos)
		{
			checksum += data[idx];
		}
	}
	gStSrvMcanTxDataTable[msgIdx].checkSumInfo.calChecksumVal = ( (checksum & 0xFF) ^ 0xFF );
	
//	DPRINTF(DBG_MSG7, "[SRV_MCAN] Calculation Checksum : %X\n", gStSrvMcanTxDataTable[msgIdx].checkSumInfo.calChecksumVal);

	if(NULL != gStSrvMcanTxDataTable[msgIdx].setCrcSigFunc)
	{
		gStSrvMcanTxDataTable[msgIdx].setCrcSigFunc(gStSrvMcanTxDataTable[msgIdx].checkSumInfo.calChecksumVal);
	}
	
}

/*****************************************************************************/
/*  SrvMcanClrChangeFlg                                                       */
/*****************************************************************************/
uint8 SrvMcanGetRxMsgCrc(eSrvMcanRxMsg msgIdx)
{
	DPRINTF(DBG_MSG7, "[SRV_MCAN] Get CRC : %X\n", gStSrvMcanRxDataTable[msgIdx].crcInfo.calCrcVal);
	
	return gStSrvMcanRxDataTable[msgIdx].crcInfo.calCrcVal;
}

/*****************************************************************************/
/*  SrvMcanCalCrc */
/*****************************************************************************/
uint8 SrvMcanCalCrc(uint8* data, uint8 len, uint8 skipPos)
{
	uint8 i = 0, j = 0;
	uint8 crc8 = 0;
	uint8 poly = 0;

	crc8 = 0xFF;
	poly = 0x1D;

	for(i = 0 ; i < len ; i++)
	{
		if(skipPos != i)
		{
			crc8 ^= data[i];

			for(j = 0 ; j < 8 ; j++)
			{
				if(crc8 & 0x80)
				{
					crc8 = (crc8 << 1) ^ poly;
				}
				else
				{
					crc8 <<= 1;
				}
			}
		}
	}

	crc8 ^= (uint8)0xFF;

	DPRINTF(DBG_MSG7, "[SRV_MCAN] CRC : 0x%X\n", crc8);

	return crc8;
}

/*****************************************************************************/
/*  SrvMcanReTransProc                                                       */
/*****************************************************************************/
static void SrvMcanReTransProc(void) // CHERY SPEC (CAN TRANS)
{
	uint8 idx = 0;
	uint8 state = TIMER_STATE_STOP;

	for(idx = 0 ; idx < RRM_TX_MAX; idx++)
	{
		if (SRVMCAN_TX_MAILBOX_CYCL_EVENT == gStSrvMcanTxDataTable[idx].mailBoxIdx)
		{
			if (SRVMCAN_TX_DURATION_0MS != gStSrvMcanTxDataTable[idx].retransTime)
			{
				state = IS_TIMER(gStSrvMcanTxDataTable[idx].retransTimer);
				
				if(TIMER_STATE_TIMEOUT == state)
				{
					if(SRVMCAN_RETRANS_MAX <= gStSrvMcanTxDataTable[idx].retransCount)
					{
						/* Stop event transmission */
						STOP_TIMER(gStSrvMcanTxDataTable[idx].retransTimer);
						gStSrvMcanTxDataTable[idx].retransCount = 0;

						/* Start periodic transmission */
						START_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer, gStSrvMcanTxDataTable[idx].cycleTime - gStSrvMcanTxDataTable[idx].retransTime); // CHERY SPEC (CAN TRANS)
						SrvMcanSendPriodicReq(idx); // CHERY SPEC (CAN TRANS)
					}
					else 
					if (0 < gStSrvMcanTxDataTable[idx].retransCount)
					{
						/* Send event message 3 times */
						SrvMcanSendReqRetrans(idx); // CHERY SPEC (CAN TRANS) SrvMcanSendReq(idx);
					}
				}
			}
		}
		else
		if (SRVMCAN_TX_MAILBOX_EVNT == gStSrvMcanTxDataTable[idx].mailBoxIdx)
		{
			if (SRVMCAN_TX_DURATION_0MS != gStSrvMcanTxDataTable[idx].retransTime)
			{
				state = IS_TIMER(gStSrvMcanTxDataTable[idx].retransTimer);

				if(TIMER_STATE_TIMEOUT == state)
				{
					if (SRVMCAN_INVALID_MAX <= gStSrvMcanTxDataTable[idx].retransCount)
					{
						/* Stop event transmission */
						STOP_TIMER(gStSrvMcanTxDataTable[idx].retransTimer);
						gStSrvMcanTxDataTable[idx].retransCount = 0;
					}
					else
					if (SRVMCAN_RETRANS_MAX <= gStSrvMcanTxDataTable[idx].retransCount)
					{
						/* Send invalid message 3 times */
						SrvMcanSendReqInvalid(idx);
					}
					else
					if (0 < gStSrvMcanTxDataTable[idx].retransCount)
					{
						/* Send event message 3 times */
						SrvMcanSendReqRetrans(idx); // CHERY SPEC (CAN TRANS) SrvMcanSendReq(idx);
					}
				}
			}
		}
	}
}

/*****************************************************************************/
/*  SrvMcanSendPriodicReq                                                    */
/*****************************************************************************/
static uint8 SrvMcanSendPriodicReq(eSrvMcanTxMsg idx)
{
	uint8* lpU8TxData = NULL;
	status_t status = STATUS_SUCCESS;

	if((gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id == gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_TXENABLE))
	{
		lpU8TxData = gStSrvMcanTxDataTable[NWM_RRM_618_IDX].getMsgCbFunc();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[NWM_RRM_618_IDX].mailBoxIdx);

		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[NWM_RRM_618_IDX].mailBoxIdx, &gStSrvMcanTxDataTable[NWM_RRM_618_IDX].dataInfo, gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}
	}
	#if 1
	// CHERY SPEC (CAN TRANS)
	else 
	if((
		#if 1	// 3 times JANG_210113
		(gStSrvMcanTxDataTable[RRM_BCM_15_385_IDX].id == gStSrvMcanTxDataTable[idx].id)||
		#endif
		(gStSrvMcanTxDataTable[RRM_6_540_IDX].id == gStSrvMcanTxDataTable[idx].id) || (gStSrvMcanTxDataTable[RRM_DVR_3_57C_IDX].id == gStSrvMcanTxDataTable[idx].id)) && 
		UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE))
	{
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFuncInvalid();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (FALSE != gStSrvMcanTxDataTable[idx].checkSumInfo.needChecksum) && (NULL != gStSrvMcanTxDataTable[idx].setCrcSigFunc))
		{
			SrvMcanCalTxCheckSum(idx);
		}
		
		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}		
	}
	#endif
	else 
	if((gStSrvMcanTxDataTable[NWM_RRM_618_IDX].id != gStSrvMcanTxDataTable[idx].id) && UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_TXENABLE))
	{
		lpU8TxData = gStSrvMcanTxDataTable[idx].getMsgCbFunc();

		status = ABSCANHS0_GET_TRANFER_STATUS(gStSrvMcanTxDataTable[idx].mailBoxIdx);

		if( (FALSE != gStSrvMcanTxDataTable[idx].checkSumInfo.needChecksum) && (NULL != gStSrvMcanTxDataTable[idx].setCrcSigFunc))
		{
			SrvMcanCalTxCheckSum(idx);
		}
		
		if( (NULL != lpU8TxData) && (STATUS_SUCCESS == status) )
		{
			status = ABSCANHS0_SEND_MSG(gStSrvMcanTxDataTable[idx].mailBoxIdx, &gStSrvMcanTxDataTable[idx].dataInfo, gStSrvMcanTxDataTable[idx].id, lpU8TxData);
		}
		else
		{
			status  = STATUS_BUSY;
		}		
	}
	else
	{
		status = STATUS_UNSUPPORTED;
	}
	
	return (uint8)status;
}

/*****************************************************************************/
/*  SrvMcanTxProc                                                       */
/*****************************************************************************/
static void SrvMcanTxProc(void)
{
	static uint8 idx = 0;
	uint8 state = TIMER_STATE_STOP;
	uint8 toNext = FALSE;
	status_t status = STATUS_BUSY;
	
	if(SRVMCAN_TX_DURATION_0MS != gStSrvMcanTxDataTable[idx].cycleTime)
	{
		state = IS_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer);
		
		if(TIMER_STATE_TIMEOUT == state)
		{
			START_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer, gStSrvMcanTxDataTable[idx].cycleTime); // CHERY SPEC (CAN TRANS)
			status = SrvMcanSendPriodicReq(idx);
		}
		else
		{
			toNext = TRUE;
		}
	}
	else
	{
		toNext = TRUE;
	}

	if( (FALSE != toNext) || (STATUS_SUCCESS == status) )
	{
		idx = ((idx + 1) % (uint8)RRM_TX_MAX);
	}

}

/*****************************************************************************/
/*  SrvMcanNormalMsgTxStartProc                                                       */
/*****************************************************************************/
static void SrvMcanNormalMsgTxStartProc(void)
{
	uint8 idx = 0;
	uint8 state = TIMER_STATE_STOP;
	
	for(idx = 0 ; idx < RRM_TX_MAX ; idx++)
	{
		if(SRVMCAN_TX_DURATION_0MS != gStSrvMcanTxDataTable[idx].cycleTime)
		{
			state = IS_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer);

			if(TIMER_STATE_RUN != state)
			{
				START_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer, gStSrvMcanTxDataTable[idx].cycleTime);
			}
		}
	}
}

/*****************************************************************************/
/*  SrvMcanNormalMsgTxStopProc                                                       */
/*****************************************************************************/
static void SrvMcanNormalMsgTxStopProc(void)
{
	uint8 idx = 0;
		
	for(idx = 0 ; idx < RRM_TX_MAX ; idx++)
	{
		STOP_TIMER(gStSrvMcanTxDataTable[idx].cycleTimer);
	}
	
}

/*****************************************************************************/
/*  SrvMcanNormalMsgTxStopProc                                                       */
/*****************************************************************************/
static void VoltageDTCDetect(void)
{
	static uint16 u16Delay = 0;
	static uint32 u32DTCTaskTick[ID_MAX] = {0u};
	static uint8  u8VoltAbove = 0u;
	static uint8  u8VoltBelow = 0u;
	static uint16 u16VoltErrCount = 0u;

	if(u16Delay >= 3000u)
	{
		u16Delay = 3000u;

		/* DTC_VOLT_BELOW */
		if(Getu16BattValue() < (BAT_ADC_9V_LEVEL))
		{
			u8VoltBelow = 1;
			MntFltSetResultProcess(DTC_VOLT_BELOW, DTC_FAILED);
		}
		else if((Getu16BattValue() > (BAT_ADC_9V_LEVEL+BAT_ADC_9V_LEVEL_OFFSET)) 
		     && (Getu16BattValue() < (BAT_ADC_16V_LEVEL+BAT_ADC_16V_LEVEL_OFFSET))
			 && (u8VoltBelow == 1u))
		{
			u8VoltBelow = 0u;
			MntFltSetResultProcess(DTC_VOLT_BELOW, DTC_PASSED);
		}
		else if((Getu16BattValue() >= (BAT_ADC_9V_LEVEL))
			 && (Getu16BattValue() <= (BAT_ADC_16V_LEVEL))
			 && (u8VoltBelow == 0u))
		{
			MntFltSetResultProcess(DTC_VOLT_BELOW, DTC_PASSED);
		}
		else
		{
			/* Do nothing */
		}

		/* DTC_VOLT_ABOVE */
		if(Getu16BattValue() > (BAT_ADC_16V_LEVEL+BAT_ADC_16V_LEVEL_OFFSET))
		{
			u8VoltAbove = 1u;
			MntFltSetResultProcess(DTC_VOLT_ABOVE, DTC_FAILED);
		}
		else if((Getu16BattValue() < (BAT_ADC_15_7V_LEVEL))
			 && (Getu16BattValue() > (BAT_ADC_9V_LEVEL-BAT_ADC_9V_LEVEL_OFFSET))
			 && (u8VoltAbove == 1u))
		{
			u8VoltAbove = 0u;
			MntFltSetResultProcess(DTC_VOLT_ABOVE, DTC_PASSED);
		}
		else if((Getu16BattValue() <= (BAT_ADC_16V_LEVEL))
			 && (Getu16BattValue() >= (BAT_ADC_9V_LEVEL))
			 && (u8VoltAbove == 0u))
		{
			MntFltSetResultProcess(DTC_VOLT_ABOVE, DTC_PASSED);
		}
		else
		{
			/* Do nothing */
		}

		//wx20220822
		/* Voltage high > 18.0v */
		if (Getu16BattValue() > BAT_ADC_18V_LEVEL)
		{
			u16VoltageLowCnt = 0;
			u16VoltageHighCnt += 2u;
			if (u16VoltageHighCnt >= NM_VOLTAGE_CHECK_3000MS)
			{
				u16VoltageHighCnt = NM_VOLTAGE_CHECK_3000MS;
				u8VoltageFlag = NM_VOLTAGE_HIGH;
			}
			else
			{
				u8VoltageFlag = NM_VOLTAGE_NORMAL;
			}
		}
		else
		{
			u16VoltageHighCnt = 0;
			/* Voltage low < 7.0v */
			if (Getu16BattValue() < BAT_ADC_7V_LEVEL)
			{
				u16VoltageLowCnt += 2u;
				if (u16VoltageLowCnt >= NM_VOLTAGE_CHECK_3000MS)
				{
					u16VoltageHighCnt = NM_VOLTAGE_CHECK_3000MS;
					u8VoltageFlag = NM_VOLTAGE_LOW;
				}
				else
				{
					u8VoltageFlag = NM_VOLTAGE_NORMAL;
				}
			}
			else
			{
				u16VoltageLowCnt = 0;
				u8VoltageFlag = NM_VOLTAGE_NORMAL;
			}
		}

		if (u8VoltageFlag != NM_VOLTAGE_NORMAL)
		{
			Can_SetPowerState(0u);
		}
		else
		{
			Can_SetPowerState(1u);
		}
	}
	else
	{
		u16Delay += 2u;
	}
	
	
}

static void OtherDTCDetect(void)
{
	static uint16 u16Delay = 0;
	static uint32 u32DTCTaskTick[ID_MAX] = {0u};
	static uint8  u8VoltAbove = 0u;
	static uint8  u8VoltBelow = 0u;

	if(u16Delay >= 3000u)
	{
		u16Delay = 3000u;

		/* DTC_SPK_CNECT_FAIL */
		MntFltSetResultProcess(DTC_SPK_CNECT_FAIL, DTC_PASSED);

		/* DTC_AMP_FAIL */
		MntFltSetResultProcess(DTC_AMP_FAIL, DTC_PASSED);

		/* DTC_Tuner_IC_FAIL */
		MntFltSetResultProcess(DTC_Tuner_IC_FAIL, DTC_PASSED);

		/* DTC_Voice_IC_FAIL */
		MntFltSetResultProcess(DTC_Voice_IC_FAIL, DTC_PASSED);

		/* DTC_MCU_SOC_COM_FAIL */
		MntFltSetResultProcess(DTC_MCU_SOC_COM_FAIL, DTC_PASSED);

		/* DTC_SOC_OVERTEMP */
		MntFltSetResultProcess(DTC_SOC_OVERTEMP, DTC_PASSED);

		/* DTC_USB1_CURRENT_ABOVE */
		MntFltSetResultProcess(DTC_USB1_CURRENT_ABOVE, DTC_PASSED);

		/* DTC_USB2_CURRENT_ABOVE */
		MntFltSetResultProcess(DTC_USB2_CURRENT_ABOVE, DTC_PASSED);

		/* DTC_SOC_DES_COM_FAIL */
		MntFltSetResultProcess(DTC_SOC_DES_COM_FAIL, DTC_PASSED);

		/* DTC_SOC_TFT_COM_FAIL */
		MntFltSetResultProcess(DTC_SOC_TFT_COM_FAIL, DTC_PASSED);

		/* DTC_SOFT_CONFIG_Error */
		MntFltSetResultProcess(DTC_SOFT_CONFIG_Error, DTC_PASSED);
	}
	else
	{
		u16Delay += 2u;
	}
	
	
}

/*****************************************************************************/
/*  SrvCanHsRxInd                                                             */
/*****************************************************************************/
/*static*/ void SrvMcanRxInd(uint32 buffIdx, uint32 cs, uint32 msgId, uint8* data, uint8 dataLen)
{
	uint8 idx = 0;
	uint8* bufAddr = NULL;

	if(gStSrvMcanRxDataTable[DIAG_RX_74A_IDX].idTable.id == msgId)
	{
		gStSrvMcanRxDataTable[DIAG_RX_74A_IDX].setMsgCbFunc(data);
		bufAddr = gStSrvMcanRxDataTable[DIAG_RX_74A_IDX].getMsgCbFunc();
		SrvCanTpRxEnQueue(gStSrvMcanRxDataTable[DIAG_RX_74A_IDX].idTable.id, bufAddr, dataLen);
	}
	else if(gStSrvMcanRxDataTable[DIAG_RX_7DF_IDX].idTable.id == msgId)
	{
		gStSrvMcanRxDataTable[DIAG_RX_7DF_IDX].setMsgCbFunc(data);
		bufAddr = gStSrvMcanRxDataTable[DIAG_RX_7DF_IDX].getMsgCbFunc();
		SrvCanTpRxEnQueue(gStSrvMcanRxDataTable[DIAG_RX_7DF_IDX].idTable.id, bufAddr, dataLen);
	}
	else if( (UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NORMAL_RXENABLE)) && (msgId != gStSrvMcanRxDataTable[NWM_CGW_600_IDX].idTable.id))
	{
		for(idx = 0 ; idx < RRM_RX_MAX ; idx ++)
		{
			if( (msgId == gStSrvMcanRxDataTable[idx].idTable.id) && ((dataLen >= gStSrvMcanRxDataTable[idx].length) && (9 > gStSrvMcanRxDataTable[idx].length))  )
			{	
				RESTART_TIMER(gStSrvMcanRxDataTable[idx].timer);
				
				gStSrvMcanRxDataTable[idx].changeFlg = gStSrvMcanRxDataTable[idx].setMsgCbFunc(data);
				gU8SrvMcanDebugFlg[idx] = gStSrvMcanRxDataTable[idx].changeFlg;

				if(FALSE != gStSrvMcanRxDataTable[idx].changeFlg)
				{
//					DPRINTF(DBG_MSG6,"SrvMcanRxInd Data Changed %d\r",idx); //�߰�
					if(FALSE != gStSrvMcanRxDataTable[idx].checksumInfo.needChecksum)
					{
						SrvMcanCalCheckSum(idx);
					}					
					if(FALSE != gStSrvMcanRxDataTable[idx].crcInfo.needCrc)
					{
						bufAddr = gStSrvMcanRxDataTable[idx].getMsgCbFunc();
						gStSrvMcanRxDataTable[idx].crcInfo.calCrcVal = SrvMcanCalCrc(bufAddr, gStSrvMcanRxDataTable[idx].length, gStSrvMcanRxDataTable[idx].crcInfo.crcPos);
					}					

				}
				idx = RRM_RX_MAX;
			}
			else
			{
				if( (msgId == gStSrvMcanRxDataTable[idx].idTable.id) && (dataLen != gStSrvMcanRxDataTable[idx].length) )
				{
					DPRINTF(DBG_MSG7, "[ID : %X] length is wrong [%d : %d]\n", msgId, gStSrvMcanRxDataTable[idx].length, dataLen);
				}
			}
		}
	}
	else if( (UTL_MASK_IS(gU8SrvMcanComCtrlMask, SRVMCAN_COMCTRL_COM_NM_RXENABLE)) && (msgId == gStSrvMcanRxDataTable[NWM_CGW_600_IDX].idTable.id))
	{

		if( (msgId == gStSrvMcanRxDataTable[NWM_CGW_600_IDX].idTable.id) && (dataLen == gStSrvMcanRxDataTable[NWM_CGW_600_IDX].length) )
		{	
			RESTART_TIMER(gStSrvMcanRxDataTable[idx].timer);
			
			gStSrvMcanRxDataTable[NWM_CGW_600_IDX].changeFlg = gStSrvMcanRxDataTable[NWM_CGW_600_IDX].setMsgCbFunc(data);

			if(gStSrvMcanRxDataTable[NWM_CGW_600_IDX].idTable.id == msgId)
			{
				//CannmSetRxFlg();//0713WL
			}
		}
	}
	else
	{
		if(msgId == gStSrvMcanRxDataTable[NWM_CGW_600_IDX].idTable.id)
		{
//			DPRINTF(DBG_MSG5, "[SRV_MCAN] NM RX Blocked!!! \n");
		}
		else
		{
//			DPRINTF(DBG_MSG5, "[SRV_MCAN] Normal RX Blocked!!! \n");
		}
	}
}

/*****************************************************************************/
/*  SrvCanHsTxConf                                                            */
/*****************************************************************************/
static void SrvMcanTxConf(uint32 buffIdx)
{

	CannmClearBusOffProc();

	switch(buffIdx)
	{
		case SRVMCAN_TX_MAILBOX_NM:

			break;

		case SRVMCAN_TX_MAILBOX_DIAG:
			SrvCanTpTxConfirm();
			break;

		case SRVMCAN_TX_MAILBOX_EVNT:
			gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_EVENT_IDX] = TRUE;
			break;

		case SRVMCAN_TX_MAILBOX_CYCL:

			break;
			
		case SRVMCAN_TX_MAILBOX_CYCL_EVENT:
			gU8SrvMcanTxConfirmFlg[SRVMCAN_TX_MB_CYCL_EVENT_IDX] = TRUE;
			break;		
		default :
			break;
	}
	
}

/*****************************************************************************/
/*  SrvMcanError                                                            */
/*****************************************************************************/
static void SrvMcanErrorInd(void)
{
	static eCanControllerSts preErrSts = CAN_ERROR_ACTIVE_STS;
	eCanControllerSts errSts = CAN_ERROR_ACTIVE_STS;
	
	errSts = SrvMcanGetErrSts();
	
	if((errSts != preErrSts) && (CAN_BUS_OFF_STS == errSts) )
	{
		CannmBusOffErrInd();
	}

	preErrSts = errSts;
}


/*****************************************************************************/
/*  SrvMcanTestCgwEmsG													 */
/*****************************************************************************/
void SrvMcanTestCgwEmsG_280(void)
{
	uint8 value[6] = {0, };
	tCGW_EMS_G_0x280Type val;
	uint8 *pVal = NULL;

	if(0 != gU8SrvMcanDebugFlg[CGW_EMS_G_280_IDX])
	{
		gU8SrvMcanDebugFlg[CGW_EMS_G_280_IDX] = FALSE;
	
		pVal = CanIfGetMsg_gCanIf_CGW_EMS_G();
		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCGW_EMS_G_0x280Type));

		value[5] = CanIfGet_gCanIf_CGW_EMS_G_EngineCoolantTemperture();					
		value[4] = CanIfGet_gCanIf_CGW_EMS_G_FuelRollingCounter();			
		value[2] = CanIfGet_gCanIf_CGW_EMS_G_EngineSpeed_0();	
		value[3] = CanIfGet_gCanIf_CGW_EMS_G_EngineSpeed_1();		
		value[1] = CanIfGet_gCanIf_CGW_EMS_G_TargetGearPosition();
		value[0] = CanIfGet_gCanIf_CGW_EMS_G_EngineSts();

		DPRINTF(DBG_MSG6, "[MSG_TEST] IEMSTs : %X\n", val.IEMSts);
		DPRINTF(DBG_MSG6, "[MSG_TEST] ISSSts : %X\n", val.ISSSts);
		DPRINTF(DBG_MSG6, "[MSG_TEST] CruiseControlStsForDisplay : %X\n", val.CruiseControlStsForDisplay);
		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineCoolantTempertureFailSts : %X\n", val.EngineCoolantTempertureFailSts);
		DPRINTF(DBG_MSG6, "[MSG_TEST] MILSts : %X\n", val.MILSts);
		DPRINTF(DBG_MSG6, "[MSG_TEST] SpeedLimitReleaseSts : %X\n", val.SpeedLimitReleaseSts);
		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineCoolantTemperture : %X\n", value[5]);
		DPRINTF(DBG_MSG6, "[MSG_TEST] FuelRollingCounter : %X\n", value[4]);
		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineSpeed : %X\n", ((value[2] << 3) | value[3]) );
		DPRINTF(DBG_MSG6, "[MSG_TEST] ISS_Sts_GS : %X\n", val.ISS_Sts_GS);
		DPRINTF(DBG_MSG6, "[MSG_TEST] TargetGearPosition : %X \n", value[1] );
		DPRINTF(DBG_MSG6, "[MSG_TEST] SSMStatus : %X\n", val.SSMStatus);
		DPRINTF(DBG_MSG6, "[MSG_TEST] EPCSts : %X\n", val.EPCSts);
		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineSpeedValidData : %X\n", val.EngineSpeedValidData);
		DPRINTF(DBG_MSG6, "[MSG_TEST] RollingCounterValidData : %X\n", val.RollingCounterValidData);
		DPRINTF(DBG_MSG6, "[MSG_TEST] GearRaiseIndication : %X\n", val.GearRaiseIndication);
		DPRINTF(DBG_MSG6, "[MSG_TEST] GearReduceIndication : %X\n", val.GearReduceIndication);
		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineSts : %X\n", value[0]);
		DPRINTF(DBG_MSG6, "[MSG_TEST] IMMOCodeWarningLightSts : %X\n", val.IMMOCodeWarningLightSts);
	}	
}

// /*****************************************************************************/
// /*  SrvMcanTestABSESP1		 */
// /*****************************************************************************/
// void SrvMcanTestABSESP1_2E9(void)
// {
// 	tABS_ESP_1_0x2E9Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[ABS_ESP_1_2E9_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[ABS_ESP_1_2E9_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_ABS_ESP_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tABS_ESP_1_0x2E9Type));

// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] TCS_VCDShiftInterferenceValide : %X\n", val.TCS_VCDShiftInterferenceValide);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] EBDFailSts: %X\n", val.EBDFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ESP_APA_Target_Gear_Request: %X\n", val.ESP_APA_Target_Gear_Request);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] ABSFailSts : %X\n", val.ABSFailSts);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] ABSActive : %X\n", val.ABSActive);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] PressureMasterCylinder : %X\n", UTL_GET_SEP_VALUE_OVER_1BYTE(val.PressureMasterCylinder_0, val.PressureMasterCylinder_1, 8));
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] VDCFailSts : %X\n", val.VDCFailSts);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] VDCActive : %X\n", val.VDCActive);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] TSCFailSts : %X\n", val.TSCFailSts);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] TCSActive : %X\n", val.TCSActive);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] BrakeSystemType : %X\n", val.BrakeSystemType);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] VehicleSpeedVSOSig : %X\n", UTL_GET_SEP_VALUE_OVER_1BYTE(val.VehicleSpeedVSOSig_0, val.VehicleSpeedVSOSig_1, 8));
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] PressureMasterCylinderValidData : %X\n", val.PressureMasterCylinderValidData);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] VehicleSpeedVSOSigValidData : %X\n", val.VehicleSpeedVSOSigValidData);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] EBDActive : %X\n", val.EBDActive);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] Checksum : %X\n", val.Checksum);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] HHCFailSts : %X\n", val.HHCFailSts);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] tcs_VDCShiftInterference : %X\n", val.tcs_VDCShiftInterference);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] BLRequestController : %X\n", val.BLRequestController);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] HHCActive : %X\n", val.HHCActive);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] ESCOffSwitchSts : %X\n", val.ESCOffSwitchSts);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] ESCOffStsValidData : %X\n", val.ESCOffStsValidData);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] SwitchOffCruiseControl : %X\n", val.SwitchOffCruiseControl);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCgwEmsG													 */
// /*****************************************************************************/
// void SrvMcanTestCgwTcuG_301(void)
// {
// 	uint8 value[1] = {0, };
// 	tCGW_TCU_G_0x301Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CGW_TCU_G_301_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CGW_TCU_G_301_IDX] = FALSE;
	
// 		pVal = CanIfGetMsg_gCanIf_CGW_TCU_G();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCGW_TCU_G_0x301Type));
			
// 		value[0] = CanIfGet_gCanIf_CGW_TCU_G_GBPositoionDisplay();

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverMode : %X\n", val.DriverMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ClutchTemperature : %X\n", val.ClutchTemperature);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BuzzerRequest : %X\n", val.BuzzerRequest);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] GBFaultstatus : %X\n", val.GBFaultstatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] GBPositoionDisplay : %X\n", value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ReverseGearInfo : %X\n", val.ReverseGearInfo);
// 	}	
// }


// /*****************************************************************************/
// /*  SrvMcanTestFrm3												 */
// /*****************************************************************************/
// void SrvMcanTestFrm3_305(void)
// {
// 	uint8 value[8] = {0, };
// 	tFRM_3_0x305Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[FRM_3_305_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[FRM_3_305_IDX] = FALSE;
	
// 		pVal = CanIfGetMsg_gCanIf_FRM_3();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCGW_TCU_G_0x301Type));
			
// 		value[0] = CanIfGet_gCanIf_FRM_3_TimeGapSet_DVD();
// 		value[1] = CanIfGet_gCanIf_FRM_3_TimeGapLastSet_DVD();
// 		value[2] = CanIfGet_gCanIf_FRM_3_FCW_ON_OFF_sts();
// 		value[3] = CanIfGet_gCanIf_FRM_3_FCW_OPTION_sts();
// 		value[4] = CanIfGet_gCanIf_FRM_3_AEB_ON_OFF_sts();
// 		value[5] = CanIfGet_gCanIf_FRM_3_DistanceWarning_on_off_sts();
// 		value[6] = CanIfGet_gCanIf_FRM_3_MessageCounter();
// 		value[7] = CanIfGet_gCanIf_FRM_3_CRC();

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] vSetDis : %X\n", ((val.vSetDis_0 << 1) | val.vSetDis_1));
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ObjValid : %X\n", val.ObjValid);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] dxTarObj : %X\n", val.dxTarObj);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TimeGapSet_ICM : %X\n", val.TimeGapSet_ICM);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TakeOverReq : %X\n", val.TakeOverReq);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Textinfo : %X\n", val.Textinfo);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TimeGapSet_DVD : %X\n", value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FCWMode : %X\n", ((val.FCWMode_0 << 1) | val.FCWMode_1));
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ACCMode : %X\n", val.ACCMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FCW_ON_OFF_sts : %X\n", value[2]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TimeGapLastSet_DVD : %X\n", value[1]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FCW_preWarning : %X\n", val.FCW_preWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DistanceWarning : %X\n", val.DistanceWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AEBMode : %X\n", val.AEBMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", value[6]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DistanceWarning_on_off_sts : %X\n", value[5]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AEB_ON_OFF_sts : %X\n", value[4]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FCW_OPTION_sts : %X\n", value[3]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CRC_T : %X\n", value[7]);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestFcm2											 */
// /*****************************************************************************/
// void SrvMcanTestFcm2_307(void)
// {
// 	uint8 value[4] = {0, };
// 	tFCM_2_0x307Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[FCM_2_307_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[FCM_2_307_IDX] = FALSE;
	
// 		pVal = CanIfGetMsg_gCanIf_FCM_2();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tFCM_2_0x307Type));
			
// 		value[0] = CanIfGet_gCanIf_FCM_2_HMAoNoFFSts();
// 		value[1] = CanIfGet_gCanIf_FCM_2_LDW_LKA_Senaitivityfeedback();
// 		value[2] = CanIfGet_gCanIf_FCM_2_TJA_ICA_ON_OFF_STS();
// 		value[3] = CanIfGet_gCanIf_FCM_2_LDWOnOffSts();

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HMA_Status : %X\n", val.HMA_Status);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HMA_HighbeamReq : %X\n", val.HMA_HighbeamReq);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HMAoNoFFSts : %X\n", value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDW_LKA_LeftVisualization : %X\n", val.LDW_LKA_LeftVisualization);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDW_LKA_Senaitivityfeedback : %X\n", value[1]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDW_LKA_LaneAssitfee : %X\n", val.LDW_LKA_LaneAssitfee);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDW_LKA_Status : %X\n", val.LDW_LKA_Status);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDW_LKA_RightVisualization : %X\n", val.LDW_LKA_RightVisualization);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SLASpdlimit : %X\n", (((val.SLASpdlimit_0 << 5) | val.SLASpdlimit_1) & 0xFF));
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SLAState : %X\n", val.SLAState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SLASpdlimitWarning : %X\n", val.SLASpdlimitWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SLAOnOffsts : %X\n", val.SLAOnOffsts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TJA_ICA_Textinfo : %X\n", val.TJA_ICA_Textinfo);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ADAS_TakeoverReq : %X\n", val.ADAS_TakeoverReq);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Camera_texinfo : %X\n", val.Camera_texinfo);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDWOnOffSts : %X\n", value[3]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TJA_ICA_ON_OFF_STS : %X\n", value[2]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TJA_ICA_mode : %X\n", val.TJA_ICA_mode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CRCCheck : %X\n", val.CRCCheck);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCGW_ABM_G									 */
// /*****************************************************************************/
// void SrvMcanTestCGW_ABM_G_320(void)
// {
// 	uint8 value[2] = {0, };
// 	tCGW_ABM_G_0x320Type val;
// 	uint8 *pVal = NULL;
	
// 	if(0 != gU8SrvMcanDebugFlg[CGW_ABM_G_320_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CGW_ABM_G_320_IDX] = FALSE;
	
// 		pVal = CanIfGetMsg_gCanIf_CGW_ABM_G();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCGW_ABM_G_0x320Type));
			
// 		value[0] = CanIfGet_gCanIf_CGW_ABM_G_PsngrSeatBeltWarning();
// 		value[1] = CanIfGet_gCanIf_CGW_ABM_G_CrashOutputSts();

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PsngrSeatBeltWarning : %X\n", value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PsngrBagSts : %X\n", val.PsngrBagSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirBagFailSts : %X\n", val.AirBagFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Checksum : %X\n", val.Checksum);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CrashOutputSts : %X\n", value[1]);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestMFS_4									 */
// /*****************************************************************************/
// void SrvMcanTestMFS_4_325(void)
// {
// 	uint8 value[16] = {0,};
// 	tMFS_4_0x325Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[MFS_4_325_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[MFS_4_325_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_MFS_4();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tMFS_4_0x325Type));
		
// 		value[0] = CanIfGet_gCanIf_MFS_4_MFS_Heartrate();
// 		value[1] = CanIfGet_gCanIf_MFS_4_MFS_HangUpPhone();
// 		value[2] = CanIfGet_gCanIf_MFS_4_MFS_AnswerPhone();
// 		value[3] = CanIfGet_gCanIf_MFS_4_MFS_AnswerHangupPhone();
// 		value[4] = CanIfGet_gCanIf_MFS_4_MFS_Voice_ADD();
// 		value[5] = CanIfGet_gCanIf_MFS_4_MFS_Voice_Activation();
// 		value[6] = CanIfGet_gCanIf_MFS_4_MFS_Mute();
// 		value[7] = CanIfGet_gCanIf_MFS_4_MFS_RRM_Confirm();
// 		value[8] = CanIfGet_gCanIf_MFS_4_MFS_RRM_Right();
// 		value[9] = CanIfGet_gCanIf_MFS_4_MFS_RRM_Left();
// 		value[10] = CanIfGet_gCanIf_MFS_4_MFS_RRM_Down_ZoomOut();
// 		value[11] = CanIfGet_gCanIf_MFS_4_MFS_RRM_Up_Zoomin();
// 		value[12] = CanIfGet_gCanIf_MFS_4_MFS_Menu();		
// 		value[13] = CanIfGet_gCanIf_MFS_4_MFS_Previous();				
// 		value[14] = CanIfGet_gCanIf_MFS_4_MFS_Next();				
// 		value[15] = CanIfGet_gCanIf_MFS_4_MFS_Voice_Jian();						
		
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_heatingthesteeringwheel : %X\n", val.MFS_heatingthesteeringwheel);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Menu : %X\n", value[12]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_RRM_Up_Zoomin : %X\n", value[11]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_RRM_Down_ZoomOut : %X\n", value[10]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_RRM_Left : %X\n", value[9]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_RRM_Right : %X\n", value[8]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_RRM_Confirm : %X\n", value[7]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Mute : %X\n", value[6]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Voice_Activation : %X\n", value[5]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_AnswerPhone : %X\n", value[2]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_HangUpPhone : %X\n", value[1]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Voice_ADD : %X\n", value[4]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Voice_Jian : %X\n", value[15]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_AnswerHangupPhone : %X\n", value[3]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Previous : %X\n", value[13]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Next : %X\n", value[14]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MFS_Heartrate : %X\n", value[0]);
		
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCgwSamG									 */
// /*****************************************************************************/
// void SrvMcanTestCgwSamG_340(void)
// {
// 	uint8 value[7] = {0,};
// 	tCGW_SAM_G_0x340Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CGW_SAM_G_340_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CGW_SAM_G_340_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CGW_SAM_G();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCGW_SAM_G_0x340Type));
		
// 		value[0] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_1();
// 		value[1] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngle_0();
// 		value[2] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngleSpeed();
// 		value[3] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngleValidData();
// 		value[4] = CanIfGet_gCanIf_CGW_SAM_G_SteeringAngleSpeedValidData();
// 		value[5] = CanIfGet_gCanIf_CGW_SAM_G_SASCalibrated();
// 		value[6] = CanIfGet_gCanIf_CGW_SAM_G_CRCCheck();		

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SteeringAngle : %X\n", (value[1] << 8) | value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SteeringAngleSpeed : %X\n", value[2]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SteeringAngleSpeedValidData : %X\n", value[4]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SteeringAngleValidData : %X\n", value[3]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SASCalibrated : %X\n", value[5]);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] SASInit : %X\n", val.SASInit);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] CANWarning : %X\n", val.CANWarning);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] InternalFailure : %X\n", val.InternalFailure);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] BatteryWarning : %X\n", val.BatteryWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AngleFailure : %X\n", val.AngleFailure);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] InternalFaultcodeBits : %X\n", val.InternalFaultcodeBits);
// 		//DPRINTF(DBG_MSG5, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CRCCheck : %X\n", value[6]);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCgwEpsG									 */
// /*****************************************************************************/
// void SrvMcanTestCgwEpsG_380(void)
// {
// 	uint8 value[1] = {0,};
// 	tCGW_EPS_G_0x380Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CGW_EPS_G_380_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CGW_EPS_G_380_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CGW_EPS_G();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCGW_EPS_G_0x380Type));
		
// 		value[0] = CanIfGet_gCanIf_CGW_EPS_G_EpsmodeSts();

// 		//DPRINTF(DBG_MSG7, "[MSG_TEST] EPSSts : %X\n", val.EPSSts);
// 		//DPRINTF(DBG_MSG7, "[MSG_TEST] EPSSteeringAngleCalib : %X\n", val.EPSSteeringAngleCalib);
// 		//DPRINTF(DBG_MSG7, "[MSG_TEST] EPSFailSts : %X\n", val.EPSFailSts);
// 		//DPRINTF(DBG_MSG7, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG7, "[MSG_TEST] EpsmodeSts : %X\n", value[0]);
// 		//DPRINTF(DBG_MSG7, "[MSG_TEST] RecoverableFault : %X\n", val.RecoverableFault);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCemBcm1									 */
// /*****************************************************************************/
// void SrvMcanTestCemBcm1_391(void)
// {
// 	uint8 value[9] = {0,};
// 	tCEM_BCM_1_0x391Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_BCM_1_391_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_BCM_1_391_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_BCM_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_BCM_1_0x391Type));
		
// 		value[0] = CanIfGet_gCanIf_CEM_BCM_1_RHTurnLightSts();
// 		value[1] = CanIfGet_gCanIf_CEM_BCM_1_LHTurnLightSts();
// 		value[2] = CanIfGet_gCanIf_CEM_BCM_1_HighBeamSts();
// 		value[3] = CanIfGet_gCanIf_CEM_BCM_1_KeySts();
// 		value[4] = CanIfGet_gCanIf_CEM_BCM_1_LowBeamSts();
// 		value[5] = CanIfGet_gCanIf_CEM_BCM_1_HazardLightSW();
// 		value[6] = CanIfGet_gCanIf_CEM_BCM_1_DriverDoorLockSts();
// 		value[7] = CanIfGet_gCanIf_CEM_BCM_1_DriverDoorSts();
// 		value[8] = CanIfGet_gCanIf_CEM_BCM_1_PsngrDoorSts();		

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHTurnLightSts : %X\n",value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHTurnLightSts : %X\n", value[1]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HighBeamSts : %X\n", value[2]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LowBeamSts : %X\n", value[4]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DRLSts : %X\n", val.DRLSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] KeySts : %X\n", value[3]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ParkLightOnWarning : %X\n", val.ParkLightOnWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Checksum : %X\n", val.Checksum);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriectionLndRight : %X\n", val.DriectionLndRight);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriectionLndLeft : %X\n", val.DriectionLndLeft);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HazardLightSW : %X\n", value[5]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AutoLightSW : %X\n", val.AutoLightSW);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TrunkDoorLockSts : %X\n", val.TrunkDoorLockSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverSW : %X\n", val.DriverSW);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverDoorLockSts : %X\n", value[6]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverDoorSts : %X\n", value[7]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PsngrDoorSts : %X\n", value[8]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HoodSts : %X\n", val.HoodSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ParkTailLightSts : %X\n", val.ParkTailLightSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHRdOORSts : %X\n", val.RHRdOORSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHRDoorSts : %X\n", val.LHRDoorSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TrunkSts : %X\n", val.TrunkSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverDoorstsQbit : %X\n", val.DriverDoorstsQbit);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HMASW : %X\n", val.HMASW);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearFogLightSts : %X\n", val.RearFogLightSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ReverseGearSts : %X\n", val.ReverseGearSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FrontFogLightSts : %X\n", val.FrontFogLightSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HightBeamSWSts : %X\n", val.HightBeamSWSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FlaseSWSts : %X\n", val.FlaseSWSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RARDfstCmd : %X\n", val.RARDfstCmd);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WiperSts : %X\n", val.WiperSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AutoHighSpeedRequest : %X\n", val.AutoHighSpeedRequest);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RainDetected : %X\n", val.RainDetected);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] IIIuminationControl : %X\n", val.IIIuminationControl);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] EventSourceCausingLock_Unlock : %X\n", val.EventSourceCausingLock_Unlock);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCemBcm2									 */
// /*****************************************************************************/
// void SrvMcanTestCemBcm2_392(void)
// {
// 	uint8 value[29] = {0,};
// 	tCEM_BCM_2_0x392Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_BCM_2_392_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_BCM_2_392_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_BCM_2();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_BCM_2_0x392Type));
		
// 		value[0] = CanIfGet_gCanIf_CEM_BCM_2_FrontFogLightSts();
// 		value[1] = CanIfGet_gCanIf_CEM_BCM_2_KeySts();
// 		value[2] = CanIfGet_gCanIf_CEM_BCM_2_HighBeamSts();
// 		value[3] = CanIfGet_gCanIf_CEM_BCM_2_LHTurnlightSts();
// 		value[4] = CanIfGet_gCanIf_CEM_BCM_2_LowBeamSts();
// 		value[5] = CanIfGet_gCanIf_CEM_BCM_2_ParkTailLightSts();
// 		value[6] = CanIfGet_gCanIf_CEM_BCM_2_RHTurnlightSts();
// 		value[7] = CanIfGet_gCanIf_CEM_BCM_2_BonnetSts();
// 		value[8] = CanIfGet_gCanIf_CEM_BCM_2_DriverDoorSts();		
// 		value[9] = CanIfGet_gCanIf_CEM_BCM_2_DRLSts();				
// 		value[10] = CanIfGet_gCanIf_CEM_BCM_2_LHRDoorSts();				
// 		value[11] = CanIfGet_gCanIf_CEM_BCM_2_PsngrDoorSts();				
// 		value[12] = CanIfGet_gCanIf_CEM_BCM_2_RHRDoorSts();				
// 		value[13] = CanIfGet_gCanIf_CEM_BCM_2_Trunk_BackDoor_Sts();				
// 		value[14] = CanIfGet_gCanIf_CEM_BCM_2_FollowMeTimeSts();				
// 		value[15] = CanIfGet_gCanIf_CEM_BCM_2_RearFogLightSts();				
// 		value[16] = CanIfGet_gCanIf_CEM_BCM_2_ReverseGearSwitch();
// 		value[17] = CanIfGet_gCanIf_CEM_BCM_2_AutoUnlockSts();
// 		value[18] = CanIfGet_gCanIf_CEM_BCM_2_RemoteLockFeedbackSts();
// 		value[19] = CanIfGet_gCanIf_CEM_BCM_2_AidTurningIlluminationSts();
// 		value[20] = CanIfGet_gCanIf_CEM_BCM_2_HazardLampForUrgencyBrakeSts();
// 		value[21] = CanIfGet_gCanIf_CEM_BCM_2_RemoteTrunkOnlySts();
// 		value[22] = CanIfGet_gCanIf_CEM_BCM_2_AlarmMode();
// 		value[23] = CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeTimeSts();
// 		value[24] = CanIfGet_gCanIf_CEM_BCM_2_FollowMeHomeSts();
// 		value[25] = CanIfGet_gCanIf_CEM_BCM_2_BlankchanginglaneSts();
// 		value[26] = CanIfGet_gCanIf_CEM_BCM_2_BlankingnumberSts();
// 		value[27] = CanIfGet_gCanIf_CEM_BCM_2_MirrorFoldSts();
// 		value[28] = CanIfGet_gCanIf_CEM_BCM_2_AutoLockSts();

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FollowMeHomeTimeSts : %X\n", value[23]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ParkLightOnWarning : %X\n", val.ParkLightOnWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] KeyRemindWarning : %X\n", val.KeyRemindWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FollowMeHomeSts : %X\n", value[24]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] KeySts : %X\n", value[1]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BlankchanginglaneSts : %X\n", value[25]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHTurnlightSts : %X\n", value[6]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHTurnlightSts : %X\n", value[3]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BrakePedalSts : %X\n", val.BrakePedalSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ParkTailLightSts : %X\n", value[5]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HighBeamSts : %X\n", value[2]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LaserlightSts : %X\n", val.LaserlightSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LowBeamSts : %X\n", value[4]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DRLSts : %X\n", value[9]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DirectionIndRight : %X\n", val.DirectionIndRight);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DirectionIndLeft : %X\n", val.DirectionIndLeft);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BlankingnumberSts : %X\n", value[26]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FollowMeTimeSts : %X\n", value[14]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverDoorSts : %X\n", value[8]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PsngrDoorSts : %X\n", value[11]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BonnetSts : %X\n", value[7]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHRDoorSts : %X\n", value[12]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHRDoorSts : %X\n", value[10]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Trunk_BackDoor_Sts : %X\n", value[13]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MirrorFoldSts : %X\n", value[27]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] External_Welcome_LightSts : %X\n", val.External_Welcome_LightSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearFogLightSts : %X\n", value[15]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ReverseGearSwitch : %X\n", value[16]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LaserhighbeamassistStatus : %X\n", val.LaserhighbeamassistStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FrontFogLightSts : %X\n", value[0]);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] RARDfstCmd : %X\n", val.RARDfstCmd);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] WiperSensitivitySWSts : %X\n", val.WiperSensitivitySWSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RainDetected : %X\n", val.RainDetected);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WiperSts : %X\n", val.WiperSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HazardLampForUrgencyBrakeSts : %X\n", value[20]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AidTurningIlluminationSts : %X\n", value[19]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MirrorFlipSts : %X\n", val.MirrorFlipSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] UnlockingBreathStatus : %X\n", val.UnlockingBreathStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RemoteLockFeedbackSts : %X\n", value[18]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AutoUnlockSts : %X\n", value[17]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AlarmMode : %X\n", value[22]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RemoteTrunkOnlySts : %X\n", value[21]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AutoLockSts : %X\n", value[28]);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAvmApa3					 */
// /*****************************************************************************/
// void SrvMcanTestAvmApa3_41A(void)
// {
// 	tAVM_APA_3_0x41AType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[AVM_APA_3_41A_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[AVM_APA_3_41A_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_AVM_APA_3();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tAVM_APA_3_0x41AType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMDisplaySts : %X\n", val.AVMDisplaySts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMFeedbackSts_RemoteMode : %X\n", val.AVMFeedbackSts_RemoteMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_FactoryMode : %X\n", val.AVM_FactoryMode);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestIcm1									 */
// /*****************************************************************************/
// void SrvMcanTestIcm1_430(void)
// {
// 	uint8 value[6] = {0,};
// 	tICM_1_0x430Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[ICM_1_430_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[ICM_1_430_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_ICM_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tICM_1_0x430Type));
		
// 		value[0] = CanIfGet_gCanIf_ICM_1_DriverSeatBeltWarningSts();
// 		value[1] = CanIfGet_gCanIf_ICM_1_PassengerSeatBeltWarningSts();
// 		value[3] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_0();
// 		value[4] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_1();
// 		value[5] = CanIfGet_gCanIf_ICM_1_TotalOdometer_km_2();

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DisplayVehiceSpeed : %X\n", (val.DisplayVehiceSpeed_0 << 8) | val.DisplayVehiceSpeed_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RFSeatBeltWarningSts : %X\n", val.RFSeatBeltWarningSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RRSeatBeltWarningSts : %X\n", val.RRSeatBeltWarningSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RMSeatBeltWarningSts : %X\n", val.RMSeatBeltWarningSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BatteryVoltageLevel : %X\n", val.BatteryVoltageLevel);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ECOModeSWSts : %X\n", val.ECOModeSWSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DriverSeatBeltWarningSts : %X\n", value[0]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PassengerSeatBeltWarningSts : %X\n", value[1]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FuelLevelFailSts : %X\n", val.FuelLevelFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FuelLevel : %X\n", val.FuelLevel);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TotalOdometer_km : %X\n", (value[3] << 16) | (value[4] << 8) | value[5]);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDWLKA_LaneAssitTypeReq : %X\n", val.LDWLKA_LaneAssitTypeReq);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] HandBrakeSts : %X\n", val.HandBrakeSts);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestPLG		 */
// /*****************************************************************************/
// void SrvMcanTestPLG_436(void)
// {
// 	tPLG_0x436Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[PLG_436_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[PLG_436_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_PLG();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tPLG_0x436Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearDoorStatus : %X\n", val.RearDoorStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearDoorStatusValidData: %X\n", val.RearDoorStatusValidData);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FootkickSWITCH : %X\n", val.FootkickSWITCH);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PLGLockSts : %X\n", val.PLGLockSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearDoorOpenDegree : %X\n", val.RearDoorOpenDegree);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearDoorOpenFailCause : %X\n", val.RearDoorOpenFailCause);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearDoorCloseFailCause : %X\n", val.RearDoorCloseFailCause);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RearDoorMoveDir : %X\n", val.RearDoorMoveDir);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PLGSASSts : %X\n", val.PLGSASSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RolingCounter : %X\n", val.RolingCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PLGUnlockinformation : %X\n", val.PLGUnlockinformation);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Checksum : %X\n", val.Checksum);

// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestRadar1									 */
// /*****************************************************************************/
// void SrvMcanTestRadar1_440(void)
// {
// 	tRADAR_1_0x440Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[RADAR_1_440_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[RADAR_1_440_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_RADAR_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tRADAR_1_0x440Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHRRadarSensorDistance : %X\n", val.LHRRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHMRRadarSensorDistance : %X\n", val.RHMRRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHMRRadarSensorDistance : %X\n", val.LHMRRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHRRadarSensorDistance : %X\n", val.RHRRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHFRadarSensorDistance : %X\n", val.LHFRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHFRadarSensorDistance : %X\n", val.RHFRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PasSwitch : %X\n", val.PasSwitch);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHMFRadarSensorDistance : %X\n", val.RHMFRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AudibleBeepRate : %X\n", val.AudibleBeepRate);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHMFRadarSensorDistance : %X\n", val.LHMFRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RadarWorkSts : %X\n", val.RadarWorkSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RadarDetectSts : %X\n", val.RadarDetectSts);
// 	}	
// }


// /*****************************************************************************/
// /*  SrvMcanTestBSD1		 */
// /*****************************************************************************/
// void SrvMcanTestBSD1_449(void)
// {
// 	tBSD_1_0x449Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[BSD_1_449_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[BSD_1_449_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_BSD_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tBSD_1_0x449Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SystemState : %X\n", val.SystemState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BSDState: %X\n", val.BSDState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RCTAState : %X\n", val.RCTAState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BSDWarningLeft : %X\n", val.BSDWarningLeft);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BSDWarningRight : %X\n", val.BSDWarningRight);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DOWSts : %X\n", val.DOWSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RCWWarning : %X\n", val.RCWWarning);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RCWSts : %X\n", val.RCWSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RCTAWarningRight : %X\n", val.RCTAWarningRight);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RCTAWarningLeft : %X\n", val.RCTAWarningLeft);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DOWWarningLeft : %X\n", val.DOWWarningLeft);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DOWWarningLeft : %X\n", val.DOWWarningRight);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BSD_1_MessageCounter : %X\n", val.BSD_1_MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Checksum : %X\n", val.Checksum);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestICM2_452		 */
// /*****************************************************************************/
// void SrvMcanTestICM2_452(void)
// {
// 	tICM_2_0x452Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[ICM_2_452_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[ICM_2_452_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_ICM_2();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tICM_2_0x452Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_Chime : %X\n", val.STAT_Chime);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirBagLampFailSts : %X\n", val.AirBagLampFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DiverModeSWsts : %X\n", val.DiverModeSWsts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] QDashACCFail : %X\n", val.QDashACCFail);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DISFail : %X\n", val.DISFail);
// 	}
// }

// /*****************************************************************************/
// /*  SrvMcanTestWpc1							 */
// /*****************************************************************************/
// void SrvMcanTestWpc1_460(void)
// {
// 	tWPC_1_0x460Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[WPC_1_460_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[WPC_1_460_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_WPC_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tWPC_1_0x460Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WPC_WirelessChargingStatus : %X\n", val.WPC_WirelessChargingStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] NFCSts : %X\n", val.NFCSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WPC_Voltage_IN : %X\n", val.WPC_Voltage_IN);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WPC_Send_Power : %X\n", val.WPC_Send_Power);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WPC_Recv_Power : %X\n", val.WPC_Recv_Power);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAvmApa4					 */
// /*****************************************************************************/
// void SrvMcanTestAvmApa4_475(void)
// {
// 	tAVM_APA_4_0x475Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[AVM_APA_4_475_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[AVM_APA_4_475_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_AVM_APA_4();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tAVM_APA_4_0x475Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHSRRadarSensorDistance : %X\n", val.RHSRRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHSRRadarSensorDistance : %X\n", val.LHSRRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHSFRadarSensorDistance : %X\n", val.RHSFRadarSensorDistance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHSFRadarSensorDistance : %X\n", val.LHSFRadarSensorDistance);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAvmApa4					 */
// /*****************************************************************************/
// void SrvMcanTestCemPeps_480(void)
// {
// 	tCEM_PEPS_0x480Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_PEPS_480_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_PEPS_480_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_PEPS();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_PEPS_0x480Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] P_N_SwitchSts : %X\n", val.P_N_SwitchSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_3_0 : %X\n", val.SmartSystemWarning_3_0);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_3_1 : %X\n", val.SmartSystemWarning_3_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_3_2 : %X\n", val.SmartSystemWarning_3_2);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_4_2 : %X\n", val.SmartSystemWarning_4_2);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_4_1 : %X\n", val.SmartSystemWarning_4_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Trunk_backDoorExternalSwitch : %X\n", val.Trunk_backDoorExternalSwitch);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_2_0 : %X\n", val.SmartSystemWarning_2_0);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_2_1 : %X\n", val.SmartSystemWarning_2_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_2_2 : %X\n", val.SmartSystemWarning_2_2);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_2_3 : %X\n", val.SmartSystemWarning_2_3);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_1_0 : %X\n", val.SmartSystemWarning_1_0);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_1_1 : %X\n", val.SmartSystemWarning_1_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_1_2 : %X\n", val.SmartSystemWarning_1_2);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_1_3 : %X\n", val.SmartSystemWarning_1_3);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_3_3 : %X\n", val.SmartSystemWarning_3_3);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_4_3 : %X\n", val.SmartSystemWarning_4_3);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_4_0 : %X\n", val.SmartSystemWarning_4_0);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ExteriorIdentificationResultSig : %X\n", val.ExteriorIdentificationResultSig);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] StarterSwitchFlag : %X\n", val.StarterSwitchFlag);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] OrderInformation : %X\n", val.OrderInformation);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_5_0 : %X\n", val.SmartSystemWarning_5_0);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] InformationSource : %X\n", val.InformationSource);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] IDInformation : %X\n", val.IDInformation);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SmartSystemWarning_5_1 : %X\n", val.SmartSystemWarning_5_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PEPS_UnLockReq : %X\n", val.PEPS_UnLockReq);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] WPCSts : %X\n", val.WPCSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Pollingsts : %X\n", val.Pollingsts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CrankingRequest : %X\n", val.CrankingRequest);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] MessageCounter : %X\n", val.MessageCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] checksum : %X\n", val.checksum);
// 	}
// }

// /*****************************************************************************/
// /*  SrvMcanTestAlm1					 */
// /*****************************************************************************/
// void SrvMcanTestCemAlm1_490(void)
// {
// 	tCEM_ALM_1_0x490Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_ALM_1_490_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_ALM_1_490_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_ALM_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_ALM_1_0x490Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_ControlSwitch : %X\n", val.STAT_ControlSwitch);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_StaticEffect : %X\n", val.STAT_StaticEffect);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_Apiluminance : %X\n", val.STAT_Apiluminance);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_StaticColour : %X\n", val.STAT_StaticColour);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_Music : %X\n", val.STAT_Music);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_AssociatedWithDriverMode : %X\n", val.STAT_AssociatedWithDriverMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] STAT_WelcomeWaterLamp : %X\n", val.STAT_WelcomeWaterLamp);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PersonalSetSts : %X\n", val.PersonalSetSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Wash_Car_Status : %X\n", val.Wash_Car_Status);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SeatSetPop_Up : %X\n", val.SeatSetPop_Up);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] SeatSetStatus : %X\n", val.SeatSetStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PersonalSeatEn : %X\n", val.PersonalSeatEn);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PersonalBeamEn : %X\n", val.PersonalBeamEn);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DeletePeronalSet_FB : %X\n", val.DeletePeronalSet_FB);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAlm1					 */
// /*****************************************************************************/
// void SrvMcanTestAbsEsp5_4E5(void)
// {
// 	tABS_ESP_5_0x4E5Type val;
// 	uint8 *pVal = NULL;
	
// 	uint16 lhrPulseCounter = 0;
// 	uint16 rhfPulseCounter = 0;
// 	uint16 rhrPulseCounter = 0;
// 	uint16 lhfPulseCounter = 0;	

// 	if(0 != gU8SrvMcanDebugFlg[ABS_ESP_5_4E5_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[ABS_ESP_5_4E5_IDX] = FALSE;
		
// 		lhrPulseCounter = CanIfGet_gCanIf_ABS_ESP_5_LHRPulseCounter();
// 		rhfPulseCounter = CanIfGet_gCanIf_ABS_ESP_5_RHFPulseCounter();
// 		rhrPulseCounter = CanIfGet_gCanIf_ABS_ESP_5_RHRPulseCounter();
// 		lhfPulseCounter = CanIfGet_gCanIf_ABS_ESP_5_LHFPulseCounter();
		
// 		pVal = CanIfGetMsg_gCanIf_ABS_ESP_5();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tABS_ESP_5_0x4E5Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHRPulseCounter : %X\n", lhrPulseCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHFPulseCounterFailSts : %X\n", val.RHFPulseCounterFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHFPulseCounterFailSts : %X\n", val.LHFPulseCounterFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHRPulseCounterFailSts : %X\n", val.LHRPulseCounterFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHRPulseCounter : %X\n", rhrPulseCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHFPulseCounter : %X\n", rhfPulseCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RHRPulseCounterFailSts : %X\n", val.RHRPulseCounterFailSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LHFPulseCounter : %X\n", lhfPulseCounter);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Checksum : %X\n", val.Checksum);
// 	}	
// }


// /*****************************************************************************/
// /*  SrvMcanTestAlm1					 */
// /*****************************************************************************/
// void SrvMcanTestAvmApa2_508(void)
// {
// 	tAVM_APA_2_0x508Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[AVM_APA_2_508_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[AVM_APA_2_508_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_AVM_APA_2();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tAVM_APA_2_0x508Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMFaultStatusCameraFront : %X\n", val.AVMFaultStatusCameraFront);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMFaultStatusCameraLeft : %X\n", val.AVMFaultStatusCameraLeft);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMFaultStatusCameraRear : %X\n", val.AVMFaultStatusCameraRear);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMFaultStatusCameraRight : %X\n", val.AVMFaultStatusCameraRight);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVMStatusFault : %X\n", val.AVMStatusFault);
// 	}	
// }


// /*****************************************************************************/
// /*  SrvMcanTBOX2					 */
// /*****************************************************************************/
// void SrvMcanTBOX2_519(void)
// {
// 	tTBOX_2_0x519Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[TBOX_2_519_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[TBOX_2_519_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_TBOX_2();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tTBOX_2_0x519Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ModeAdjust_Req : %X\n", val.ModeAdjust_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Fan_Work_AppCmd : %X\n", val.Fan_Work_AppCmd);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BlowSpeedLevel_Req : %X\n", val.BlowSpeedLevel_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CirculationMode_Req : %X\n", val.CirculationMode_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Working_Req : %X\n", val.Working_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TriggerAlarmSts : %X\n", val.TriggerAlarmSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AutoState : %X\n", val.AutoState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] L_Set_Temperature : %X\n", val.L_Set_Temperature);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] R_Set_Temperature : %X\n", val.R_Set_Temperature);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ControlMode_Req : %X\n", val.ControlMode_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TBOX_RemotePhotograph : %X\n", val.TBOX_RemotePhotograph);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TBOX_SetShowMode : %X\n", val.TBOX_SetShowMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Checksum : %X\n", val.Checksum);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestCemIpm1			 */
// /*****************************************************************************/
// void SrvMcanTestCemIpm1_51A(void)
// {
// 	tCEM_IPM_1_0x51AType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_IPM_1_51A_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_IPM_1_51A_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_IPM_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_IPM_1_0x51AType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FLTempsts : %X\n", UTL_GET_SEP_VALUE_NUM2(val.CEM_FLTempsts_0, val.CEM_FLTempsts_1, 4));
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FRTempsts : %X\n", val.CEM_FRTempsts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FrontBlowSpdCtrlsts : %X\n", val.CEM_FrontBlowSpdCtrlsts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FrontOFFSts : %X\n", val.CEM_FrontOFFSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FrontBlowModeSts : %X\n", val.CEM_FrontBlowModeSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ACStatus : %X\n", val.ACStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_RecyMode : %X\n", val.CEM_RecyMode);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_RearTempsts : %X\n", val.CEM_RearTempsts);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_RearDefrosts : %X\n", val.CEM_RearDefrosts);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_RearOFFsts : %X\n", val.CEM_RearOFFsts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_TempknobRollingCounter : %X\n", UTL_GET_SEP_VALUE_NUM2(val.CEM_TempknobRollingCounter_0, val.CEM_TempknobRollingCounter_1, 1));
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_RearBlowSpdCtrlSts : %X\n", val.CEM_RearBlowSpdCtrlSts);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] CEM__RearAutoACSts : %X\n", val.CEM_RearAutoACSts);
// 		//DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_RearBlowModeSts : %X\n", val.CEM_RearBlowModeSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FrontAutoACSts : %X\n", val.CEM_FrontAutoACSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_SyncSts : %X\n", val.CEM_SyncSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AC_DisplaySts : %X\n", val.AC_DisplaySts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BTReduceWindSpeedSts : %X\n", val.BTReduceWindSpeedSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] IPMblowerdelaySts : %X\n", val.IPMblowerdelaySts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] IPMFirstBlowingSts : %X\n", val.IPMFirstBlowingSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_PM25_Detect : %X\n", val.CEM_PM25_Detect);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FLSeatHeatVentSwSts : %X\n", val.CEM_FLSeatHeatVentSwSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_FRSeatHeatVentSwSts : %X\n", val.CEM_FRSeatHeatVentSwSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CEM_AnionPurify : %X\n", val.CEM_AnionPurify);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAvm2			 */
// /*****************************************************************************/
// void SrvMcanTestAvm2_51C(void)
// {
// 	tAVM_2_0x51CType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[AVM_2_51C_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[AVM_2_51C_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_AVM_2();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tAVM_2_0x51CType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_DisplaySts : %X\n", val.AVM_DisplaySts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_WorkModeSts : %X\n", val.AVM_WorkModeSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_LanguageSts : %X\n", val.AVM_LanguageSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_SteeringWheelSts : %X\n", val.AVM_SteeringWheelSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_RadarWariningSts : %X\n", val.AVM_RadarWariningSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_LDW_FunSts : %X\n", val.AVM_LDW_FunSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_BSD_FunSts : %X\n", val.AVM_BSD_FunSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AVM_DefDispMode : %X\n", val.AVM_DefDispMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_WorkState : %X\n", val.DVR_WorkState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] LDW_WarningState : %X\n", val.LDW_WarningState);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BSD_WarningState : %X\n", val.BSD_WarningState);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestIcm3		 */
// /*****************************************************************************/
// void SrvMcanTestIcm3_52E(void)
// {
// 	tICM_3_0x52EType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[ICM_3_52E_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[ICM_3_52E_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_ICM_3();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tICM_3_0x52EType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] F_DL : %X\n", val.F_DL);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PCItype : %X\n", val.PCItype);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA1 : %X\n", val.DATA1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA2 : %X\n", val.DATA2);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA3 : %X\n", val.DATA3);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA4 : %X\n", val.DATA4);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA5 : %X\n", val.DATA5);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA6 : %X\n", val.DATA6);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DATA7 : %X\n", val.DATA7);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestIcm4					 */
// /*****************************************************************************/
// void SrvMcanTestIcm4_530(void)
// {
// 	tICM_4_0x530Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[ICM_4_530_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[ICM_4_530_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_ICM_4();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tICM_4_0x530Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AverageFuelConsume : %X\n", (val.AverageFuelConsume_0 << 8)|val.AverageFuelConsume_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Brake_Fuel_Level : %X\n", val.Brake_Fuel_Level);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] Engine_oil_Pressure : %X\n", val.Engine_oil_Pressure);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ICMLanguageSet : %X\n", val.ICMLanguageSet);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DistanceToEmpty_Km : %X\n", (val.DistanceToEmpty_Km_0<< 8)|val.DistanceToEmpty_Km_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AverageVehicleSpeed : %X\n", (val.AverageVehicleSpeed_0 << 8)|val.AverageVehicleSpeed_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] ThemeSys : %X\n", val.ThemeSys);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestPM25_1		 */
// /*****************************************************************************/
// void SrvMcanTestPM25_1_53A(void)
// {
// 	tPM25_1_0x53AType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[PM25_1_53A_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[PM25_1_53A_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_PM25_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tPM25_1_0x53AType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PM2_5Indensity : %X\n", (val.PM2_5Indensity_0 << 8) | val.PM2_5Indensity_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PM2_5Outdensity : %X\n", (val.PM2_5Outdensity_0 << 8) | val.PM2_5Outdensity_1);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirInQLevel : %X\n", val.AirInQLevel);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirOutQLevel : %X\n", val.AirOutQLevel);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PM25Sts : %X\n", val.PM25Sts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] PM25ErrSts : %X\n", val.PM25ErrSts);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAmp1					 */
// /*****************************************************************************/
// void SrvMcanTestAmp1_555(void)
// {
// 	tAMP_1_0x555Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[AMP_1_555_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[AMP_1_555_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_AMP_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tAMP_1_0x555Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AMP_InitFlag : %X\n", val.AMP_InitFlag);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AMP_TempHighVolAdiustLow : %X\n", val.AMP_TempHighVolAdiustLow);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestAfu1					 */
// /*****************************************************************************/
// void SrvMcanTestAfu1_560(void)
// {
// 	tAFU_1_0x560Type val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[AFU_1_560_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[AFU_1_560_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_AFU_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tAFU_1_0x560Type));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh1Type : %X\n", val.AirFragCh1Type);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh2Type : %X\n", val.AirFragCh2Type);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh3Type : %X\n", val.AirFragCh3Type);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh1Detn : %X\n", val.AirFragCh1Detn);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh2Detn : %X\n", val.AirFragCh2Detn);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh3Detn : %X\n", val.AirFragCh3Detn);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragChChgSts : %X\n", val.AirFragChChgSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragLvlRspn : %X\n", val.AirFragLvlRspn);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragInitSts : %X\n",val.AirFragInitSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragSwthSts : %X\n",val.AirFragSwthSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AFU_ON_button : %X\n", val.AFU_ON_button);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] AirFragCh1Button : %X\n", val.AirFragCh1Button);
// 	}	
// }

// /*****************************************************************************/
// /*  SrvMcanTestDvr1		 */
// /*****************************************************************************/
// void SrvMcanTestDvr1_58A(void)
// {
// 	tDVR_1_0x58AType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[DVR_1_58A_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[DVR_1_58A_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_DVR_1();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tDVR_1_0x58AType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_Sdcard_Sts : %X\n", val.DVR_Sdcard_Sts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_DrivingRecord : %X\n", val.DVR_DrivingRecord);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_VideoResolution : %X\n", val.DVR_VideoResolution);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_VideoTimeSts : %X\n", val.DVR_VideoTimeSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_ShutdownDelay : %X\n", val.DVR_ShutdownDelay);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_VideoCoverageSW : %X\n", val.DVR_VideoCoverageSW);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_GSensorSensy : %X\n", val.DVR_GSensorSensy);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_ParkMonito : %X\n", val.DVR_ParkMonito);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_TapeSwitch : %X\n", val.DVR_TapeSwitch);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_HDRSwitch : %X\n", val.DVR_HDRSwitch);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_DriveInfoOverlay : %X\n", val.DVR_DriveInfoOverlay);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_SdcardFormateSts : %X\n", val.DVR_SdcardFormateSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_FactoryDefaultSts : %X\n", val.DVR_FactoryDefaultSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_SystemSts : %X\n", val.DVR_SystemSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_FactoryDefault : %X\n", val.DVR_FactoryDefault);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_GsensorCrashStatus : %X\n", val.DVR_GsensorCrashStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_requestWiFiopen : %X\n", val.DVR_requestWiFiopen);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_Network_Set : %X\n", val.DVR_Network_Set);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_LocalPhotographResult : %X\n", val.DVR_LocalPhotographResult);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVRLocalContinuousPhotograph : %X\n", val.DVRLocalContinuousPhotograph);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_LocalRecordResult : %X\n", val.DVR_LocalRecordResult);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_PlayRate : %X\n", val.DVR_PlayRate);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_Frameinterval : %X\n", val.DVR_Frameinterval);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVR_FrameCount : %X\n", UTL_GET_SEP_VALUE_OVER_1BYTE(val.DVR_FrameCount_0, val.DVR_FrameCount_1, 3));
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DVRLocalTimelapseRecord : %X\n", val.DVRLocalTimelapseRecord);
		
// 	}	
// }


// /*****************************************************************************/
// /*  SrvMcanTestCemBcm4					 */
// /*****************************************************************************/
// void SrvMcanTestCemBcm4_58D(void)
// {
// 	tCEM_BCM_4_0x58DType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_BCM_4_58D_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_BCM_4_58D_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_BCM_4();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_BCM_4_0x58DType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FL_WIN_Position : %X\n", val.FL_WIN_Position);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FR_WIN_Position : %X\n", val.FR_WIN_Position);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RL_WIN_Position : %X\n", val.RL_WIN_Position);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RR_WIN_Position : %X\n", val.RR_WIN_Position);
// 	}	
// }


// /*****************************************************************************/
// /*  SrvMcanTestIBCM		 */
// /*****************************************************************************/
// void SrvMcanTestCemBcm3_5FF(void)
// {
// 	tCEM_BCM_3_0x5FFType val;
// 	uint8 *pVal = NULL;

// 	if(0 != gU8SrvMcanDebugFlg[CEM_BCM_3_5FF_IDX])
// 	{
// 		gU8SrvMcanDebugFlg[CEM_BCM_3_5FF_IDX] = FALSE;
		
// 		pVal = CanIfGetMsg_gCanIf_CEM_BCM_3();
// 		UTL_COPY_DATA((uint8 *)&val, pVal, sizeof(tCEM_BCM_3_0x5FFType));

// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TBOX_AuthenticationStatus : %X\n", val.TBOX_AuthenticationStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] BM_AuthenticationStatus: %X\n", val.BM_AuthenticationStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] CurrentTelematcsMode : %X\n", val.CurrentTelematcsMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] TelematicsPowerMode : %X\n", val.TelematicsPowerMode);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineForbidStatus : %X\n", val.EngineForbidStatus);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] EngineControlSts : %X\n", val.EngineControlSts);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FLWindowCon_Req : %X\n", val.FLWindowCon_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RLWindowCon_Req : %X\n", val.RLWindowCon_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] DWM_Sts_Msg : %X\n", val.DWM_Sts_Msg);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FRWindowCon_Req : %X\n", val.FRWindowCon_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] RRWindowCon_Req : %X\n", val.RRWindowCon_Req);
// 		DPRINTF(DBG_MSG6, "[MSG_TEST] FindCarSts : %X\n", val.FindCarSts);
// 	}	
// }
extern DEVICE r_Device;
uint8 u8DTCStartProcess = 0u;
uint16 u16BattValue = 0u;
void GetBattValuePeriod(void)
{
	static uint8 u8Counter = 0u;

	if(u8Counter < 20u)
	{
		u8Counter += 1u;
	}
	else
	{
		u8Counter = 0u;
		u16BattValue = Check_Batt_Value();
	}
}

uint16 Getu16BattValue(void)
{
	return u16BattValue;
}
void CheckDTCStartCondition(void)
{
	static uint16 u16IGACCCounter = 0u;
	static uint16 u16BatCounter = 0u;
	
	/* 5s time check */
	if((r_Device.r_System->accign_state.state&0x01) == 1u)
	{
		if(u16IGACCCounter < 5000u)
		{
			u16IGACCCounter += 1u;
		}
		else
		{
			/* Do nothing */
		}
	}
	else
	{
		/* Clear counter to restart count */
		u16IGACCCounter = 0;
		/* End this DTC detect cycle */
		DtcMgrAgingProcess();
	}

	/* 9-16v 3s voltage check */
	if((u16BattValue > (BAT_ADC_9V_LEVEL+BAT_ADC_9V_LEVEL_OFFSET))
	&& (u16BattValue < BAT_ADC_16V_LEVEL))
	{
		if(u16BatCounter < 3000u)
		{
			u16BatCounter += 1u;
		}
		else
		{
			/* Do nothing */
		}
	}
	else
	{
		/* Clear counter to restart count */
		u16BatCounter = 0u;
		u16IGACCCounter = 0u;
		u16BatCounter = 0u;
	}

	/* check IGACC and voltage condition */
	if((u16IGACCCounter >= 5000u) && (u16BatCounter >= 3000u))
	{
		u8DTCStartProcess = 1;
	}
	else
	{
		u8DTCStartProcess = 0; 
	}
}

uint8 GetDTCStartProcessState(void)
{
	return u8DTCStartProcess;
}

void CommLostReset(uint8 u8ID)
{
	if(u8ID >= ID_MAX)
	{
		/* Do nothing */
	}
	else
	{
		StCANTimeout.u16Counter[u8ID] = 0u;
	}	
}

void CommConnectIncrease(uint8 u8ID, uint16 u16Ms, uint16 u16Delt)
{
	if(u8ID >= ID_MAX)
	{
		/* Do nothing */
	}
	else
	{
		if(StCANTimeout.u16Counter[u8ID] < u16Ms)
		{
			StCANTimeout.u16Counter[u8ID] += u16Delt;
		}
		else
		{
			StCANTimeout.u16Counter[u8ID] = u16Ms;
		}
		
	}	
}

uint16 GetCANTimeoutCounter(uint8 u8ID)
{
	uint16 u16Counter = 0;
	if(u8ID >= ID_MAX)
	{
		/* Do nothing */
	}
	else
	{
		u16Counter = StCANTimeout.u16Counter[u8ID];
	}
	return u16Counter;
}

void CommDTCDetect(void)
{
	static uint8 idx = 0;
	static uint32 u32DTCTaskTick[ID_MAX] = {0u};
	uint8 tmrSts = TIMER_STATE_TIMEOUT;
	tLosComListType losComFltIdx = LOS_COM_INDEX_MAX;
	
#if 0
	/* ESC lost */
	if(!DTC_CONFIG_ESC)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_VCU, DTC_PASSED);
	}
	else if(IlGetVCU_cESC_EnableStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_VCU, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x313) >= (3000u/TIMERPERIOD)) && (IlGetVCU_cESC_EnableStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_VCU, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x313]))
		// {
		// 	u32DTCTaskTick[ID0x313] = GetTickCount();
		// 	CommConnectIncrease(ID0x313, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x313, (3000u/TIMERPERIOD), 1u);
	}

	/* BMS lost */
	if(!DTC_CONFIG_BMS)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BMS, DTC_PASSED);
	}
	else if(IlGetBMS_ChgWireConnectStsDispRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BMS, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x469) >= (3000u/TIMERPERIOD)) && (IlGetBMS_ChgWireConnectStsDispRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BMS, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x469]))
		// {
		// 	u32DTCTaskTick[ID0x469] = GetTickCount();
		// 	CommConnectIncrease(ID0x469, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x469, (3000u/TIMERPERIOD), 1u);
	}

	/* BCM lost */
	if(!DTC_CONFIG_BCM)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BCM, DTC_PASSED);
	}
	else if(IlGetBCM_KeyStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BCM, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x245) >= (3000u/TIMERPERIOD)) && (IlGetBCM_KeyStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BCM, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x245]))
		// {
		// 	u32DTCTaskTick[ID0x245] = GetTickCount();
		// 	CommConnectIncrease(ID0x245, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x245, (3000u/TIMERPERIOD), 1u);
	}

	/* ICM lost */
	if(!DTC_CONFIG_ICM)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ICM, DTC_PASSED);
	}
	else if(IlGetICM_LowSOC_LampStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ICM, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x271) >= (3000u/TIMERPERIOD)) && (IlGetICM_LowSOC_LampStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ICM, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x271]))
		// {
		// 	u32DTCTaskTick[ID0x271] = GetTickCount();
		// 	CommConnectIncrease(ID0x271, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x271, (3000u/TIMERPERIOD), 1u);
	}

	/* SAS lost */
	if(!DTC_CONFIG_SAS)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_SAS, DTC_PASSED);
	}
	else if(IlGetSAS_SteeringAngleSpdVDRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_SAS, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x504) >= (3000u/TIMERPERIOD)) && (IlGetSAS_SteeringAngleSpdVDRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_SAS, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x504]))
		// {
		// 	u32DTCTaskTick[ID0x504] = GetTickCount();
		// 	CommConnectIncrease(ID0x504, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x504, (3000u/TIMERPERIOD), 1u);
	}

	/* ACCM lost */
	if((DTC_CONFIG_ACCM != 0x00u) && (DTC_CONFIG_ACCM != 0x20u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ACCM, DTC_PASSED);
	}
	else if(IlGetACCM_AUTO_StsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ACCM, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x3D2) >= (3000u/TIMERPERIOD)) && (IlGetACCM_AUTO_StsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ACCM, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x3D2]))
		// {
		// 	u32DTCTaskTick[ID0x3D2] = GetTickCount();
		// 	CommConnectIncrease(ID0x3D2, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x3D2, (3000u/TIMERPERIOD), 1u);
	}

	/* SCM_R lost */
	if(!DTC_CONFIG_SCM_R)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_SCM_R, DTC_PASSED);
	}
	else if(IlGetSCM_HMI_ReqRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_SCM_R, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x45B) >= (3000u/TIMERPERIOD)) && (IlGetSCM_HMI_ReqRxTimeout() == 0U))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_SCM_R, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x45B]))
		// {
		// 	u32DTCTaskTick[ID0x45B] = GetTickCount();
		// 	CommConnectIncrease(ID0x45B, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x45B, (3000u/TIMERPERIOD), 1);
	}

	/* AVAS lost */
	if(!DTC_CONFIG_AVAS)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_AVAS, DTC_PASSED);
	}
	else if(IlGetAVAS_VolumeStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_AVAS, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x4B1) >= (5500u/TIMERPERIOD)) && (IlGetAVAS_VolumeStsRxTimeout() == 0U))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_AVAS, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x4B1]))
		// {
		// 	u32DTCTaskTick[ID0x4B1] = GetTickCount();
		// 	CommConnectIncrease(ID0x4B1, (5100u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x4B1, (5500u/TIMERPERIOD), 1u);
	}

	/* BSD lost */
	if(!DTC_CONFIG_BSD)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BSD, DTC_PASSED);
	}
	else if(IlGetBSD_RearWarnSystemEnableStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BSD, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x298) >= (3000u/TIMERPERIOD)) && (IlGetBSD_RearWarnSystemEnableStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_BSD, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x298]))
		// {
		// 	u32DTCTaskTick[ID0x298] = GetTickCount();
		// 	CommConnectIncrease(ID0x298, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x298, (3000u/TIMERPERIOD), 1u);
	}

	/* FCM lost */
	if(!DTC_CONFIG_FCM)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_FCM, DTC_PASSED);
	}
	else if(IlGetFCM_LDW_EnableStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_FCM, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x331) >= (3000u/TIMERPERIOD)) && (IlGetFCM_LDW_EnableStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_FCM, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x331]))
		// {
		// 	u32DTCTaskTick[ID0x331] = GetTickCount();
		// 	CommConnectIncrease(ID0x331, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x331, (3000u/TIMERPERIOD), 1u);
	}

	/* TBOX lost */
	if(!DTC_CONFIG_TBOX)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_TBOX, DTC_PASSED);
	}
	else if(IlGetTBOX_GPS_Time_yearRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_TBOX, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x4A8) >= (3500u/TIMERPERIOD)) && (IlGetTBOX_GPS_Time_yearRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_TBOX, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x4A8]))
		// {
		// 	u32DTCTaskTick[ID0x4A8] = GetTickCount();
		// 	CommConnectIncrease(ID0x4A8, (3100u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x4A8, (3500u/TIMERPERIOD), 1u);
	}

	/* MFS lost */
	if(!DTC_CONFIG_MFS)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_MFS, DTC_PASSED);
	}
	else if(IlGetMFS_LeftWheelButtonSts_ScrollUpRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_MFS, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x294) >= (3000u/TIMERPERIOD)) && (IlGetMFS_LeftWheelButtonSts_ScrollUpRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_MFS, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x294]))
		// {
		// 	u32DTCTaskTick[ID0x294] = GetTickCount();
		// 	CommConnectIncrease(ID0x294, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x294, (3000u/TIMERPERIOD), 1u);
	}

	/* ABM lost */
	if(!DTC_CONFIG_ABM)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ABM, DTC_PASSED);
	}
	else if(IlGetABM_PassengerAirbagDisableStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ABM, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x234) >= (3000u/TIMERPERIOD)) && (IlGetABM_PassengerAirbagDisableStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_ABM, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x234]))
		// {
		// 	u32DTCTaskTick[ID0x234] = GetTickCount();
		// 	CommConnectIncrease(ID0x234, (3000u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x234, (3000u/TIMERPERIOD), 1u);
	}

	/* WCM lost */
	if(!DTC_CONFIG_WCM)
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_WCM, DTC_PASSED);
	}
	else if(IlGetWCM_WirelessChargingEnableStsRxTimeout())
	{
		/* Set lost state */
		MntFltSetResultProcess(DTC_LOS_COM_WCM, DTC_FAILED);
	}
	else if((GetCANTimeoutCounter(ID0x4BA) >= (3100u/TIMERPERIOD)) && (IlGetWCM_WirelessChargingEnableStsRxTimeout() == 0u))
	{
		/* Clear lost state */
		MntFltSetResultProcess(DTC_LOS_COM_WCM, DTC_PASSED);
	}
	else
	{
		// if(2<=GetElapsedTime(u32DTCTaskTick[ID0x4BA]))
		// {
		// 	u32DTCTaskTick[ID0x4BA] = GetTickCount();
		// 	CommConnectIncrease(ID0x4BA, (3100u/TIMERPERIOD), 2u);
		// }
		CommConnectIncrease(ID0x4BA, (3100u/TIMERPERIOD), 1u);
	}

#endif
}
