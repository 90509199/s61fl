/*
 * flash_proc.c
 *
 *  Created on: 2018. 6. 27.
 *      Author: Digen
 */
#include "includes.h"
#include "CPU.h"

#define ISSI_FLASH


DATA_SAVE e2p_save_data;
E2P_SYSTEM e2p_sys_data;

const size_t e2p_size = sizeof(e2p_save_data);

void INIT_FLASH_DATA(void)
{
	memset(&e2p_save_data, 	0x00, sizeof(DATA_SAVE));
	memset(&e2p_sys_data, 	0x00, sizeof(E2P_SYSTEM));
}

void defualt_setup_value(DATA_SAVE *data)
{
	#ifdef X95_FEATURE
	data->E2P_ID1 = 0x90;
	#else
	data->E2P_ID1 = 0x70;
	#endif
	data->E2P_CHKSUM = checksum_setup_value(data); //JANG_211120
	data->E2P_SVCD = 3;
	data->E2P_LONDNESS_ONOFF = 1;
	data->E2P_SURROUND_SOUND = 1;	
	data->E2P_SOUND_WAVE_MODE = 1;	
	data->E2P_SOUND_FEILD_MODE = 2;
	data->E2P_EQ_TYPE = 7;
	data->E2P_EQ_FADER = 7;	
	data->E2P_EQ_BALANCE = 7;	
	data->E2P_EQ_TREBLE = 7;
	data->E2P_EQ_MID = 7;	
	data->E2P_EQ_BASS = 7;		
	data->E2P_VOICE_VOL = 5;
	data->E2P_RADAR_WARN_VOL= 2;
	data->E2P_CLU_WARN_VOL = 2; 
	data->E2P_KEY_VOL= 1;
	data->E2P_MEDIA_VOL= AK_MEDIA_VOL_DEFAULT;	
	data->E2P_CALL_VOL= 15;	
	data->E2P_NAVI_VOL = 5;	//default 10->20	
	data->E2P_DEFAULTMAX_VOL = 10;
	#ifdef X95_FEATURE
	data->E2P_LCD_BRIGHTNESS =7;
	#else
	data->E2P_LCD_BRIGHTNESS =10;
	#endif
	data->E2P_CLS_BRIGHTNESS = 10;
	data->E2P_CLS_DRIVINGTIME = 0x0F;
	data->E2P_CLS_OVERSPD = 0x3F;
	data->E2P_PSNGRAIRBAG_ONOFF = 2;
	data->E2P_AVAS_ONOFF = 2;
}

void defualt_system_value(E2P_SYSTEM *data)
{
	data->E2P_AUDIO_PATH_DEFAULT =AVNT_USECASE_AUDIOPATH_MEDIA;
	data->E2P_LAST_MODE = SYS_ACTIVE_MODE_IDLE;
	data->E2P_RADIO_MODE =0x0;
	data->E2P_RADIO_FREQUENCY =FM_MIN_FREQ;
	data->E2P_TUNNING_FM_LEVEL =TUNE_GET_FM_LEVEL;
	data->E2P_TUNNING_FM_WAM =TUNE_GET_FM_USN;
	data->E2P_TUNNING_FM_USN =TUNE_GET_FM_WAN;
	data->E2P_TUNNING_FM_OFFSET =TUNE_GET_FM_OFFSET;
	data->E2P_TUNNING_AM_LEVEL =TUNE_GET_AM_LEVEL;
	data->E2P_TUNNING_AM_OFFSET =TUNE_GET_AM_OFFSET;
	data->E2P_ENGINEER_MODE = OFF;
	data->E2P_DOOR_OPEN = OFF;
	data->E2P_TEST_MODE = OFF;
	data->E2P_DEBUG_ONOFF = debugenable;
	data->E2P_TUNER_TYPE = TUNER_TYPE_NXP;
	data->E2P_SONY = 0; //normal
}

//JANG_211120
uint8 checksum_setup_value(DATA_SAVE *data)
{
	uint8 i=0;
	uint8 chksum =0;
	uint16 tmp16 =0;  
	uint8 tmp[22]={0,};
	
	memcpy(&tmp[0],data,22);

	for(i=0;i<20;i++){
		tmp16 += tmp[i+2];
	}
	
	chksum = ~((uint8)tmp16&0xff);

	return chksum;
}

uint8 reg_read_status(void)
{
	uint8 tx_data[2]={0,};		//1024
	uint8 rx_data[2]={0,};		//1024

	tx_data[0]= IS25LP_READ_STATUS;
	tx_data[1] = 0x01;

	LPSPI_DRV_MasterTransferBlocking(LPSPICOM2,&tx_data[0],&rx_data[0],2,1000);
	
//	DPRINTF(DBG_MSG1,"%s %x\n\r",__FUNCTION__,rx_data[1]);

	return rx_data[1];
}

uint8 flash_sector_erase(uint32 addr)
{
	int ret=0;
	uint8 write_cmd = IS25LP_WRITE_ENABLE;
	uint8 tx_data[4]={0,};		//1024

	tx_data[0]	= IS25LP_SECTOR_ERASE;
	tx_data[1] =(uint8)(addr>>16);
	tx_data[2] =(uint8)(addr>> 8);
	tx_data[3] =(uint8)addr;

	//enable write
	if(reg_read_status()!=0x2)
		LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &write_cmd, NULL, 1, 1000);

	LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &tx_data[0], NULL, 4, 1000);

	return ret;
}

uint8 flash_read_func(uint32 addr, unsigned char *rxbuf, int rxlen)
{
	int ret=0, i=0;
	uint8 tx_data[1024]={0,};		//1024
	uint8 rx_data[1024]={0,};		//1024

	#ifdef ISSI_FLASH 
	tx_data[0]	= IS25LP_NORMAL_READ_MODE;
	tx_data[1] =(uint8)(addr>> 8);
	tx_data[2] =(uint8)addr;

	LPSPI_DRV_MasterTransferBlocking(LPSPICOM2,&tx_data[0],&rx_data[0],3+rxlen,1000);
	for(i=0;i<rxlen;i++)
		rxbuf[i] =	rx_data[3+i];

	ret = rxlen;
	#else
	#endif

	return ret;
}

uint8 flash_write_func(uint32 addr, const unsigned char *txbuf, int txlen)
{
	int ret;
	uint8 write_cmd = IS25LP_WRITE_ENABLE;
	uint8 tx_data[1024]={0,};		//1024
	#ifdef ISSI_FLASH 

	memcpy(&tx_data[3],txbuf,txlen);
	tx_data[0]	= IS25LP_INPUT_PAGE_PROGRAM;
	tx_data[1] =(uint8)(addr>>8);
	tx_data[2] =(uint8)addr;
	
	txlen +=3;
	
	//enable write
	if(reg_read_status()!=0x2)
		LPSPI_DRV_MasterTransferBlocking(LPSPICOM2,&write_cmd,NULL,1,1000);

	LPSPI_DRV_MasterTransferBlocking(LPSPICOM2,&tx_data[0],NULL,txlen,1000);
	ret = txlen;
	#else
	#endif
	
	return ret;
}

void save_last_memory(void)
{
	#if 1 //JANG_211217	
	e2p_sys_data.E2P_SONY = r_sonysound;
	e2p_save_data.E2P_CHKSUM = checksum_setup_value(&e2p_save_data);		//JANG_211120	
//	e2p_sys_data.E2P_LAST_MODE = dev->r_System->s_state_backup;
	#endif
	flash_write_func(FLASH_DATA0_ADDR, (const unsigned char *)&e2p_save_data, sizeof(DATA_SAVE));
	delay_ms(1);
	flash_write_func(FLASH_DATA1_ADDR, (const unsigned char *)&e2p_sys_data, sizeof(E2P_SYSTEM));
}


