

#include "includes.h"



#define FLASH_ERR()				if (ret != STATUS_SUCCESS) {return STATUS_ERROR;}



flash_ssd_config_t flashSSDConfig;
u32 BootStatusFlashBlockAddr;

/* call form uart init */
u32 SelfFlashInit(void)
{
	status_t ret = FLASH_DRV_Init(&Flash1_InitConfig0, &flashSSDConfig);
	FLASH_ERR();

	BootStatusFlashBlock_init();
	return 0;
}


status_t SelfFlashBlockErase(u32 addr)
{
	status_t ret = STATUS_ERROR;
	
	/* Disable interrupts */
    INT_SYS_DisableIRQGlobal();
	ret = FLASH_DRV_EraseSector(&flashSSDConfig, addr, FLASH_BLOCK_SIZE);
	/* Enable interrupts */
    INT_SYS_EnableIRQGlobal();
	FLASH_ERR();

	/* Disable interrupts */
    INT_SYS_DisableIRQGlobal();
	ret = FLASH_DRV_VerifySection(&flashSSDConfig, addr, FLASH_BLOCK_SIZE / FTFx_DPHRASE_SIZE, 1u);
	/* Enable interrupts */
    INT_SYS_EnableIRQGlobal();
	FLASH_ERR();

	return ret;
}
status_t SelfFlashProgram(u32 Dest, u8 *pSrc, u32 lenByte)
{
	status_t ret = STATUS_ERROR;
	u32 failAddr;

	/* Disable interrupts */
    INT_SYS_DisableIRQGlobal();
	ret = FLASH_DRV_Program(&flashSSDConfig, Dest, lenByte, pSrc);
	/* Enable interrupts */
    INT_SYS_EnableIRQGlobal();
	FLASH_ERR();

	/* Disable interrupts */
    INT_SYS_DisableIRQGlobal();
	/* Verify the program operation at margin level value of 1, user margin */
    ret = FLASH_DRV_ProgramCheck(&flashSSDConfig, Dest, lenByte, pSrc, &failAddr, 1u);
    /* Enable interrupts */
    INT_SYS_EnableIRQGlobal();
    FLASH_ERR();

	return ret;
}


void BootStatusFlashBlock_init(void)
{
	u32 addr;

	// 마지막 빈 주소공간 검색
	for(addr = BOOT_STATUS_START_ADDR; addr < (BOOT_STATUS_START_ADDR + BOOT_STATUS_SECTOR_SIZE); addr += sizeof(u64))
	{
		if(*(__IO u64 *)addr == 0xffffffffffffffff)
		{
			break;
		}
	}
	BootStatusFlashBlockAddr = addr;
}

void BootStatusFlashBlock_getStatus(SECTOR_STAT_T *pGet_Stat)
{
	*pGet_Stat = *(__IO SECTOR_STAT_T *)(BootStatusFlashBlockAddr - sizeof(SECTOR_STAT_T));
	//return (u32 *)(BootStatusFlashBlockAddr - sizeof(u32));
}

status_t BootStatusFlashBlock_writeStatus(SECTOR_STAT_T *pWrite_Stat)
{
	status_t ret = STATUS_ERROR;
	u32 addr = BootStatusFlashBlockAddr;
	ret = BootStatusFlashBlock_checkBlock();
	FLASH_ERR();

	ret = SelfFlashProgram(addr, (u8 *)pWrite_Stat, sizeof(SECTOR_STAT_T));			// Data를 Write시킨다.
	FLASH_ERR();
	
	BootStatusFlashBlockAddr = addr+sizeof(SECTOR_STAT_T);						// 현재 주소를 증가시킨다.

	return ret;
}

status_t BootStatusFlashBlock_reset(SECTOR_STAT_T *pWrite_Stat)
{
	status_t ret = STATUS_ERROR;
	ret = SelfFlashBlockErase(BOOT_STATUS_START_ADDR);
	FLASH_ERR();

	ret = SelfFlashProgram(BOOT_STATUS_START_ADDR, (u8 *)pWrite_Stat, sizeof(SECTOR_STAT_T));			// Data를 Write시킨다.
	FLASH_ERR();
	
	BootStatusFlashBlockAddr = BOOT_STATUS_START_ADDR+sizeof(SECTOR_STAT_T);						// 현재 주소를 증가시킨다.

	return ret;
}

status_t BootStatusFlashBlock_checkBlock(void)
{
	status_t ret = STATUS_SUCCESS;
	
	SECTOR_STAT_T data = {APP_ERASED, BOOT_MODE};
	
	//검출된 값이 마지막번지인경우(꽉 찬 경우)
	if(BootStatusFlashBlockAddr >= (BOOT_STATUS_START_ADDR + BOOT_STATUS_SECTOR_SIZE))
	{
		BootStatusFlashBlock_getStatus(&data);		// 마지막 데이터를 백업

		// 유효하지 않은 data인경우 
		//if((data != AppInstallCheckInstallVal)||(data != AppInstallCheckUninstallVal))
		//	data = AppInstallCheckInstallVal;

		//ret = FlashBlockErase(BOOT_STATUS_START_ADDR);								
		//addrTemp = BOOT_STATUS_START_ADDR;
		ret = BootStatusFlashBlock_reset(&data);	// 페이지를 지우고 첫번째 주소로 변경
	}
	
	return ret;
}



#if 1
/* Test code */
#define FLASH_START_ADDR					0x20000			// 128KB 부터
#define FLASH_END_ADDR						0x80000			// 512KB 까지


static u8 sourceBuffer[256];

static uint32_t Flash_not_EraseCount(void)
{
	/* flash 상태 검사 */
	uint32_t i;
	uint32_t TempCNT = 0;
	for (i = FLASH_START_ADDR; i < FLASH_END_ADDR; i++)
	{
		if (*(const uint8_t *)i != 0xff)
		{
			TempCNT++;
		}
	}

	return TempCNT;
}

static uint32_t Flash_EraseCount(void)
{
	/* flash 상태 검사 */
	uint32_t i;
	uint32_t TempCNT = 0;
	for (i = FLASH_START_ADDR; i < FLASH_END_ADDR; i++)
	{
		if (*(const uint8_t *)i == 0xff)
		{
			TempCNT++;
		}
	}

	return TempCNT;
}

static void Flash_RW_Test(void)
{
	/* 0 ~ 256을 차례로 채움 */
	uint32_t i;
	for (i = 0; i < sizeof(sourceBuffer); i++)
	{
		sourceBuffer[i] = i;
	}
	
	status_t ret;
    /* Disable cache to ensure that all flash operations will take effect instantly,
     * this is device dependent */
#if 0     
#ifdef S32K144_SERIES
    MSCM->OCMDR[0u] |= MSCM_OCMDR_OCM0(0xFu) | MSCM_OCMDR_OCM1(0xFu) | MSCM_OCMDR_OCM2(0xFu);
    MSCM->OCMDR[1u] |= MSCM_OCMDR_OCM0(0xFu) | MSCM_OCMDR_OCM1(0xFu) | MSCM_OCMDR_OCM2(0xFu);
    MSCM->OCMDR[2u] |= MSCM_OCMDR_OCM0(0xFu) | MSCM_OCMDR_OCM1(0xFu) | MSCM_OCMDR_OCM2(0xFu);
    MSCM->OCMDR[3u] |= MSCM_OCMDR_OCM0(0xFu) | MSCM_OCMDR_OCM1(0xFu) | MSCM_OCMDR_OCM2(0xFu);
#endif /* S32K144_SERIES */
#endif

    /* Install interrupt for Flash Command Complete event */
    //INT_SYS_InstallHandler(FTFC_IRQn, CCIF_Handler, (isr_t*) 0);
    //INT_SYS_EnableIRQ(FTFC_IRQn);

    /* Enable global interrupt */
    //INT_SYS_EnableIRQGlobal();

    /* Always initialize the driver before calling other functions */
    ret = FLASH_DRV_Init(&Flash1_InitConfig0, &flashSSDConfig);
    //DEV_ASSERT(STATUS_SUCCESS == ret);

	i = Flash_not_EraseCount();
	dprintf("1st check not erase count : %d\r\n", i);

	/****8***** 인터럽트를 사용하고 싶으면 isr을 ram이나 다른 flash 영역에서 실행하야 한다. ***********/
	/* Disable interrupts */
    //INT_SYS_DisableIRQGlobal();
    ret = FLASH_DRV_EraseSector(&flashSSDConfig, FLASH_START_ADDR, (FLASH_END_ADDR - FLASH_START_ADDR));
    ret = FLASH_DRV_VerifySection(&flashSSDConfig, FLASH_START_ADDR, (FLASH_END_ADDR - FLASH_START_ADDR) / FTFx_DPHRASE_SIZE, 1u);
    /* Enable interrupts */
    //INT_SYS_EnableIRQGlobal();
    
    i = Flash_not_EraseCount();
	dprintf("2nd check not erase count : %d\r\n", i);

    /* Verify the erase operation at margin level value of 1, user read */
    
    //DEV_ASSERT(STATUS_SUCCESS == ret);
    uint32_t failAddr;
    
	for (i = FLASH_START_ADDR; i < FLASH_END_ADDR; i += sizeof(sourceBuffer))
	{
		ret = FLASH_DRV_Program(&flashSSDConfig, i, sizeof(sourceBuffer), sourceBuffer);
		ret = FLASH_DRV_ProgramCheck(&flashSSDConfig, i, sizeof(sourceBuffer), sourceBuffer, &failAddr, 1u);
	}
    
	/* Verify the program operation at margin level value of 1, user margin */
    
    //DEV_ASSERT(STATUS_SUCCESS == ret);

	i = Flash_EraseCount();
    //UART1_printf("3rd check erase count : %d\r\n", i);
}

#endif

