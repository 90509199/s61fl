/*
 * change_mode.c
 *
 *  Created on: 2018. 8. 27.
 *      Author: Digen
 */


#include "includes.h"


void Set_RUNmode (uint8_t value){  /* Change to normal RUN mode with 8MHz SOSC, 80 MHz PLL*/

	SMC->PMCTRL &= (0b00 << 5); 		// RUNM is 0b00 by default after restart	// [6-5] = 0b00 (Run Mode Control)	//Add
	//run mode
	//VCO_CLK = 320 MHz, SPLL_CLK = 160 MH
	// SCG_RCCR[SCS] = 0110b	SCG_RCCR[DIVCORE] = 0001b	SCG_RCCR[DIVBUS] = 0001b	SCG_RCCR[DIVSLOW] = 0010b
	 SCG->RCCR=SCG_RCCR_SCS(6)     	 	/* PLL as clock source*/
    |SCG_RCCR_DIVCORE(0b01)      			/* DIVCORE=1, div. by 2: Core clock = 160/2 MHz = 80 MHz*/
    |SCG_RCCR_DIVBUS(0b01)       			/* DIVBUS=1, div. by 2: bus clock = 40 MHz*/
    |SCG_RCCR_DIVSLOW(0b10);     			/* DIVSLOW=2, div. by 2: SCG slow, flash clock= 26 2/3 MHz*/

	if ((SCG->CSR & SCG_CSR_SCS_MASK >> SCG_CSR_SCS_SHIFT ) != 6) {}/* Wait for sys clk src = SPLL */
}

void Set_HSRUNmode (uint8_t value){  /* Change to normal RUN mode with 8MHz SOSC, 80 MHz PLL*/
	//HSRUN mode
	//VCO_CLK = 320 MHz, SPLL_CLK = 160 MH			//80Mhz
	// SCG_HCCR[SCS] = 0110b	SCGHCCR[DIVCORE] = 0001b	SCG_HCCR[DIVBUS] = 0001b	SCG_HCCR[DIVSLOW] = 0010b
	 SCG->RCCR=SCG_HCCR_SCS(0b0110)     	 	/* PLL as clock source*/
    |SCG_HCCR_DIVCORE(0b01)      			/* DIVCORE=1, div. by 2: Core clock = 160/2 MHz = 80 MHz*/
    |SCG_HCCR_DIVBUS(0b01)       			/* DIVBUS=1, div. by 2: bus clock = 40 MHz*/
    |SCG_HCCR_DIVSLOW(0b10);     			/* DIVSLOW=2, div. by 2: SCG slow, flash clock= 26 2/3 MHz*/

	if ((SCG->CSR & SCG_CSR_SCS_MASK >> SCG_CSR_SCS_SHIFT ) != 6) {} /* Wait for sys clk src = SPLL */
}

void Set_VLPRUNmode (uint8_t value){
	//VLPRUN mode
	//VCO_CLK = 320 MHz, SPLL_CLK = 160 MH			//80Mhz
	// SCG_HCCR[SCS] = 0110b	SCGHCCR[DIVCORE] = 0001b	SCG_HCCR[DIVBUS] = 0001b	SCG_HCCR[DIVSLOW] = 0010b
	 SCG->VCCR=SCG_VCCR_SCS(0b0010)     	 	/* PLL as clock source*/
    |SCG_VCCR_DIVCORE(0b01)      			/* DIVCORE=1, div. by 2: Core clock = 160/2 MHz = 80 MHz*/
    |SCG_VCCR_DIVBUS(0b00)       			/* DIVBUS=1, div. by 2: bus clock = 40 MHz*/
    |SCG_VCCR_DIVSLOW(0b11);     			/* DIVSLOW=2, div. by 2: SCG slow, flash clock= 26 2/3 MHz*/

	if ((SCG->CSR & SCG_CSR_SCS_MASK >> SCG_CSR_SCS_SHIFT ) != 6) {}/* Wait for sys clk src = SPLL */
}

#define NormalStop          0b000
#define VeryLowPowerStop    0b010
#define SleepOnExit         1U
#define SleepNow            0U
#define STOP1               0b01
#define STOP2               0b10

void Go_To_Sleep (void)
{
#if 0
	/* Enable SLEEPDEEP bit in the Core
	* (Allow deep sleep modes) */
	S32_SCB ->SCR|=S32_SCB_SCR_SLEEPDEEP_MASK;
	/* Select Stop Mode */
	SMC->PMCTRL=SMC_PMCTRL_STOPM(0b00);
	/* Select which STOP mode (Stop1 or Stop2)
	* is desired (Stop1 - 0b01, Stop2 - 0b10) */
//	SMC->STOPCTRL=SMC_STOPCTRL_STOPO(0b01);
SMC->STOPCTRL=SMC_STOPCTRL_STOPO(0b10);
	/* Check if current mode is RUN mode */
	if(SMC->PMSTAT == 0x01)
	{
		/* Go to deep sleep mode */
		asm("WFI");
	}
	#else
	SMC->PMPROT |= (1 << 5);								//VLPR and VLPS are allowed.
	S32_SCB->SCR |= S32_SCB_SCR_SLEEPDEEP_MASK;		/* Enable Stop Modes in the Core */
	SMC->PMCTRL |= (0b010);							//VeryLowPowerStop
	SMC->STOPCTRL=SMC_STOPCTRL_STOPO(STOP1);	// 00->���� ����...  01����..
	
	/* Check if current mode is RUN mode */
	if(SMC->PMSTAT == 0x01)
	{
		/* Go to deep sleep mode */
		asm("WFI");
	}
	#endif
}


