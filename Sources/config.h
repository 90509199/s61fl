/*
 * config.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define SHELL_MODE
//#define CPU_COMM_TEST_MODE
#define FLEXIO_I2C
#define HW_REV_VER_03
//#define DISABLE_JTEG_IN_SLEEP_MODE
#define DSP_BOOTING_SEQUENCE

//#define ADC0_3P3_CHANGE     //5V is used at key value
#define ADC1_3P3_CHANGE

#define  B1_MOTHER_BOARD_SAMPLE
//#define OLD_CPU_BOARD

//#define CAN_TX_392_ADD
//#define PWM_CHANGE_FREQ

//#define NEW_URAT_RX0   =>disble  for use ring buffer
#define NEW_URAT_RX1
#define NEW_URAT_TX

//must be use polling, interrupt is response lately 
#define POLLING_URAT_TX0
#define POLLING_URAT_TX1

#define INAMP
#define INTERNAL_APA
#ifdef INAMP
#define INTERNAL_AMP
#else
#define EXTERNAL_AMP
#endif

#define CHECK_ALIVE_SIGNAL
#define CHECK_HU_BL_REQ
#define CHANG_BL_TIMING

#define  LCD_REQUSET_FOR_MENGBO
#define  NEW_LCD_BACKLIGHT

#define CHANG_TO_MFS_3

#define ENABLE_ENCODER_15MIN_MODE
//#define ENABLE_LOUDNESS_FOR_INTERAMP

#define RADIO_INFO_SEND_DATA
#define SVCD_ENABLE												//JANG_210810			//modify SVCD feature

#define CHECK_NO_RESPONSE
#define NEW_APPLY_EQ
#define SOS_CALL_APPLY_TBOX_MUTE					//JANG_210920_1 		//fixed no sound to stop sos call mode when media mode is changed to sos call mode

#define CX62C_FUNCTION											//JANG_210703				// x70 low cost feature
//it need to remove DTC_AVMCOM for CX6C model   //DTC_LIST_AVMCOM //option which AVM communicate DTC
#ifndef CX62C_FUNCTION
#define DTC_LIST_AVMCOM     									//JANG_211013 	remove AVM_LOST_COMM DIST list at internal AVM model
#endif

#define WATCHDOG_ENABLE

#define SI_LABS_TUNER
//#define M_BOARD_CONTROL_SOUND
#define RADIO_VOLUME_4DB_PLUS
#define SEPERATE_VOLUME_TABLE

#define	DSP_REBOOT_FOR_ADB_REBOOT			//why?? x95 please check
#define	CHERY_REQUSET_SYSTEM_TEST

#define	PROHIBIT_REBOOT_TUNER_FAIL
#define CHECK_TURNLIGHT_TYPE							//JANG_210822 //add feature check which 2 turnlight type is normal mode or not emergency (CHECK_TURNLIGHT_TYPE)

#define VECTOR_PASSIVE_WAKEUP
#define VECTOR_CCL_TEST
#define	DIAG_DCT_CHANGE_TEST
//no sound
#define RECOVER_DSP_RAM_WRITE_FAIL 			//JANG_210920		// fixed no sound issue( add dsp recovery feature)
#define INTERNAL_AMP_MONITOR						

//this config is removed for soc 8.0   //from v31.10.41
//#define TFT_PW_FROM_GPIO_TO_PWM				//JANG_211204 //Change TFT_EN control type from PWM to GPIO

//#define EMC_FOR_CAN_UNDER_6V
//#define EMC_FOR_EXTERNAL_AMP

#define DSP_DOUBLE_QUE
#define LOW_PWR_DETECT

#define QFI_MODE_SEQ_CHANGE    // patched code is applied for QFI mode Sequence spec

#define NEW_SCAN_ADC_KEY

#define CAN_RXTX_TEST_FUNC     //Add CAN_TEST protocol //JANG_210920_2
//JANG_210810  //fixed  screen off issue  //reproduce step: screenoff mode -> highbattery  mode->active mode 
//fixed  excute diagnostic reset service  after QFI Mode enter

//JANG_210811		//Improve the issue of not being able to communicate TP when transferring BT phones to the same number.
//JANG_210818		// add check HU_REQ is high and backlight turn on
//JANG_210819		// fixed no sound issue (add no sound checking service)
//JANG_210819_1	//apply for sony effect by softwareconfig
//JANG_210823		//add qfi function
//JANG_210828		//release adc key value is changed (4000->4600)
//JANG_210828_1	//Change DSP CRAM write fail counter 10 to 25 times
//JANG_210829		//fixed the issue of black screen when failure occurs during SOC normal boot check after it enter low battery mode  during boot.
//JANG_210830		//fixed issue not to enter QFI mode in car 
//JANG_210901		//fixed issue mixed radio sound and tts sound when bt phone is changed at radio mode
//JANG_210901_1	//fixed that lcd duty doesn't sync to change auto_parkinglightsts sometimes 
//JANG_210920_1	//add AIR CONDITIONER Command
//JANG_210920_3	//RRM_Settheme postion is changed 
//JANG_210920_4	//Add 0xF101(usb device mode)
//JANG_210927 		//change HW information (6.0.0 ->7.0.0)
//JANG_211013_1	//fixed SourceStationMode information
//JANG_211013_2		//applied to second radio tunning for silabs
//JANG_211025  	//called pwm_deinit function to check which pwm_init is called or not when sleep mode is entered
//JANG_211025_1 	// set port invalid when it is entered in sleep mode
//JANG_211102		//exchange next key, prev key(idrive)
//JANG_211112		//Fixed bt issue display phone information(sometimes don't display phone number) 
//JANG_211114		//change uart default when system is booting
//JANG_211116		//intialize can value when system is booting //synchronization can compare value when default value is setting
//JANG_211120		//add flash checksum data
//JANG_211121		//uart_rx ringbuffer change
//JANG_221124		//media default volume is changed(29->22) : sometimes sound is broken
//JANG_211203		//change PWM duty for TYW monitor 
//JANG_211203_1 //lcd on/off function ignore
//JANG_211217		//fixed last memory occur to sometimes 
//JANG_211231		//Add CAN signal ISS_Switch(0x385),ISS_Sts(0x280)
//JANG_220111 		//apply to power key scan(20ms)
//JANG_220112		//fixed last memory occur to sometimes (add fixed)  
//JANG_220113   	// change SNR 10->3 for silabs tuner 
//JANG_220308		// systemSupplierECUSWNum is change from 01.06.00 to 01.07.00


#endif /* CONFIG_H_ */
