/*---------------------------------------------------------------------------- 
 * License information:
 *   -    Serialnumber:       CBD2100118
 *   - Date of license:       N/A
  ----------------------------------------------------------------------------
 * Software is licensed for:
 * Wuhu Mengbo Technology
 * Package: CBD_Vector_SLP2
 * Micro: Freescale S32K144
 * Compiler: Iar 8.50.9
  ---------------------------------------------------------------------------*/
 
#ifndef  _SIP_VERSION_CHECK_
#define  _SIP_VERSION_CHECK_


/* ROM CATEGORY 4 START*/
V_MEMROM0 extern V_MEMROM1 vuint8 V_MEMROM2 kSipMainVersion;
V_MEMROM0 extern V_MEMROM1 vuint8 V_MEMROM2 kSipSubVersion;
V_MEMROM0 extern V_MEMROM1 vuint8 V_MEMROM2 kSipBugFixVersion;
/* ROM CATEGORY 4 END*/


#endif /* _SIP_VERSIONCHECK_ */

