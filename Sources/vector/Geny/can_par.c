/* -----------------------------------------------------------------------------
  Filename:    can_par.c
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: DrvCan__base
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\Documents and Settings\90506667\桌面\sh52\新建文件夹\S61EV_FLV1.4_2023_03_04.dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#define C_DRV_INTERNAL
#include "can_inc.h"
#include "can_par.h"
#include "v_inc.h"

/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 tCanTxId0 V_MEMROM2 CanTxId0[kCanNumberOfTxObjects] = 
{
  MK_STDID0(0x618u) /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  MK_STDID0(0x537u) /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  MK_STDID0(0x4FCu) /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  MK_STDID0(0x3BEu) /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  MK_STDID0(0x3BDu) /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  MK_STDID0(0x3BCu) /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  MK_STDID0(0x3BBu) /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  MK_STDID0(0x3BAu) /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  MK_STDID0(0x3B8u) /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  MK_STDID0(0x3B7u) /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  MK_STDID0(0x3B6u) /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  MK_STDID0(0x3B5u) /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  MK_STDID0(0x3B4u) /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  MK_STDID0(0x3B3u) /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  MK_STDID0(0x3AFu) /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  MK_STDID0(0x3AEu) /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  MK_STDID0(0x3ADu) /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  MK_STDID0(0x3ACu) /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  MK_STDID0(0x3AAu) /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  MK_STDID0(0x293u) /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */


/* -----------------------------------------------------------------------------
    &&&~ CanTxDLC
 ----------------------------------------------------------------------------- */

/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 tCanDlc V_MEMROM2 CanTxDLC[kCanNumberOfTxObjects] = 
{
  MK_TX_DLC(8u) /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  MK_TX_DLC(8u) /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */



/* -----------------------------------------------------------------------------
    &&&~ CanTxMessageLength
 ----------------------------------------------------------------------------- */

#ifdef C_ENABLE_CAN_FD_FULL
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanTxMessageLength[kCanNumberOfTxObjects] = 
{
  8 /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  8 /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  8 /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  8 /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  8 /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  8 /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  8 /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  8 /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  8 /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  8 /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  8 /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  8 /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  8 /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  8 /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  8 /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  8 /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  8 /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  8 /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  8 /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  8 /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */

#endif




#ifdef C_ENABLE_COPY_TX_DATA
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 TxDataPtr V_MEMROM2 CanTxDataPtr[kCanNumberOfTxObjects] = 
{
  (TxDataPtr) NM_IHU._c /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  (TxDataPtr) IHU_ADAS_10._c /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  (TxDataPtr) IHU_ADAS_11._c /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  (TxDataPtr) IHU_18._c /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  (TxDataPtr) IHU_13._c /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  (TxDataPtr) IHU_12._c /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  (TxDataPtr) IHU_11._c /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  (TxDataPtr) IHU_10._c /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  (TxDataPtr) IHU_8._c /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  (TxDataPtr) IHU_7._c /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  (TxDataPtr) IHU_6._c /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  (TxDataPtr) IHU_5._c /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  (TxDataPtr) IHU_4._c /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  (TxDataPtr) IHU_3._c /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  (TxDataPtr) IHU_17._c /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  (TxDataPtr) IHU_16._c /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  (TxDataPtr) IHU_15._c /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  (TxDataPtr) IHU_14._c /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  (TxDataPtr) IHU_1._c /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  (TxDataPtr) IHU_MFS._c /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */

#endif


#ifdef C_ENABLE_PRETRANSMIT_FCT
/* ROM CATEGORY 1 START */
/* PRQA  S 1334 QAC_Can_1334 */ /* MD_Can_1334 */
V_MEMROM0 V_MEMROM1 ApplPreTransmitFct V_MEMROM2 CanTxApplPreTransmitPtr[kCanNumberOfTxObjects] = 
{
  V_NULL /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  V_NULL /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  V_NULL /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  V_NULL /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  V_NULL /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  V_NULL /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  V_NULL /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  V_NULL /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  V_NULL /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  V_NULL /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  V_NULL /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  V_NULL /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  V_NULL /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  V_NULL /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  V_NULL /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  V_NULL /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  V_NULL /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  V_NULL /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  V_NULL /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  V_NULL /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* PRQA  L:QAC_Can_1334 */

/* ROM CATEGORY 1 END */

#endif


#ifdef C_ENABLE_CONFIRMATION_FCT
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 ApplConfirmationFct V_MEMROM2 CanTxApplConfirmationPtr[kCanNumberOfTxObjects] = 
{
  CanNm_NmMsgConfirmation /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  V_NULL /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  V_NULL /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  V_NULL /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  V_NULL /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  V_NULL /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  V_NULL /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  V_NULL /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  V_NULL /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  V_NULL /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  V_NULL /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  V_NULL /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  V_NULL /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  V_NULL /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  V_NULL /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  V_NULL /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  V_NULL /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  V_NULL /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  V_NULL /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  V_NULL /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */

#endif


#ifdef C_ENABLE_PART_OFFLINE
/* ROM CATEGORY 2 START */

V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanTxSendMask[kCanNumberOfTxObjects] = 
{
  (vuint8) C_SEND_GRP_NONE /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  (vuint8) C_SEND_GRP_NONE /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};

/* ROM CATEGORY 2 END */

#endif


#ifdef C_ENABLE_CONFIRMATION_FLAG
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanConfirmationOffset[kCanNumberOfTxObjects] = 
{
  0 /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  0 /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  0 /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  0 /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  0 /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  0 /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  0 /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  0 /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  0 /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  1 /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  1 /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  1 /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  1 /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  1 /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  1 /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  1 /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  1 /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  2 /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  2 /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  2 /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */


/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanConfirmationMask[kCanNumberOfTxObjects] = 
{
  0x00u /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  0x01u /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  0x02u /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  0x04u /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  0x08u /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  0x10u /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  0x20u /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  0x40u /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  0x80u /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  0x01u /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  0x02u /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  0x04u /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  0x08u /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  0x10u /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  0x20u /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  0x40u /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  0x80u /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  0x01u /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  0x02u /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  0x04u /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 1 END */


#endif




/* Id table depending on search algorithm */
/* Linear search */
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 tCanRxId0 V_MEMROM2 CanRxId0[kCanNumberOfRxObjects] = 
{
  MK_STDID0(0x4E3u) /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  MK_STDID0(0x461u) /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  MK_STDID0(0x3D2u) /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  MK_STDID0(0x3B1u) /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  MK_STDID0(0x387u) /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  MK_STDID0(0x355u) /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  MK_STDID0(0x347u) /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  MK_STDID0(0x32Au) /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  MK_STDID0(0x328u) /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  MK_STDID0(0x326u) /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  MK_STDID0(0x324u) /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  MK_STDID0(0x323u) /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  MK_STDID0(0x315u) /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  MK_STDID0(0x307u) /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  MK_STDID0(0x298u) /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  MK_STDID0(0x294u) /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  MK_STDID0(0x291u) /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  MK_STDID0(0x28Au) /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  MK_STDID0(0x271u) /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  MK_STDID0(0x245u) /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  MK_STDID0(0x238u) /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  MK_STDID0(0x225u) /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  MK_STDID0(0x215u) /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  MK_STDID0(0x504u) /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  MK_STDID0(0x4DDu) /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  MK_STDID0(0x4BAu) /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  MK_STDID0(0x4B1u) /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  MK_STDID0(0x4A8u) /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  MK_STDID0(0x45Du) /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  MK_STDID0(0x45Cu) /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  MK_STDID0(0x45Bu) /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  MK_STDID0(0x45Au) /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  MK_STDID0(0x458u) /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  MK_STDID0(0x457u) /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  MK_STDID0(0x455u) /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  MK_STDID0(0x3FCu) /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  MK_STDID0(0x3FAu) /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  MK_STDID0(0x3EDu) /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  MK_STDID0(0x3DEu) /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  MK_STDID0(0x3DCu) /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  MK_STDID0(0x3D4u) /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  MK_STDID0(0x314u) /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  MK_STDID0(0x313u) /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  MK_STDID0(0x25Au) /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  MK_STDID0(0x231u) /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  MK_STDID0(0x123u) /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */


/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 CanReceiveHandle V_MEMROM2 CanRxMsgIndirection[kCanNumberOfRxObjects] = 
{
  1 /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 */, 
  6 /* ID: 0x00000461, Handle: 6, CGW_OBC_1 */, 
  20 /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 */, 
  21 /* ID: 0x000003b1, Handle: 21, ICM_2 */, 
  22 /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 */, 
  23 /* ID: 0x00000355, Handle: 23, CGW_BMS_9 */, 
  24 /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 */, 
  25 /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 */, 
  26 /* ID: 0x00000328, Handle: 26, RCM_1 */, 
  27 /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 */, 
  28 /* ID: 0x00000324, Handle: 28, CGW_BCM_4 */, 
  29 /* ID: 0x00000323, Handle: 29, CGW_BCM_5 */, 
  30 /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 */, 
  33 /* ID: 0x00000307, Handle: 33, CGW_FCM_2 */, 
  34 /* ID: 0x00000298, Handle: 34, CGW_BSD_1 */, 
  35 /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 */, 
  36 /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 */, 
  37 /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 */, 
  38 /* ID: 0x00000271, Handle: 38, ICM_1 */, 
  40 /* ID: 0x00000245, Handle: 40, CGW_BCM_1 */, 
  41 /* ID: 0x00000238, Handle: 41, CGW_EPS_1 */, 
  43 /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 */, 
  44 /* ID: 0x00000215, Handle: 44, CGW_EGS_1 */, 
  0 /* ID: 0x00000504, Handle: 0, CGW_SAS_2 */, 
  2 /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 */, 
  3 /* ID: 0x000004ba, Handle: 3, WCM_1 */, 
  4 /* ID: 0x000004b1, Handle: 4, AVAS_1 */, 
  5 /* ID: 0x000004a8, Handle: 5, TBOX_7 */, 
  7 /* ID: 0x0000045d, Handle: 7, BCM_ALM */, 
  8 /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 */, 
  9 /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 */, 
  10 /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 */, 
  11 /* ID: 0x00000458, Handle: 11, CGW_BCM_FM */, 
  12 /* ID: 0x00000457, Handle: 12, BCM_6 */, 
  13 /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 */, 
  14 /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 */, 
  15 /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 */, 
  16 /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 */, 
  17 /* ID: 0x000003de, Handle: 17, CGW_FCM_7 */, 
  18 /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 */, 
  19 /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 */, 
  31 /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 */, 
  32 /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 */, 
  39 /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 */, 
  42 /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 */, 
  45 /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 */
};
/* ROM CATEGORY 1 END */


/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanRxDataLen[kCanNumberOfRxObjects] = 
{
  4 /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  8 /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  3 /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  7 /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  1 /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  6 /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  7 /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  8 /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  4 /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  8 /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  8 /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  4 /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  8 /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  8 /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  7 /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  8 /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  8 /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  6 /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  6 /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  6 /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  7 /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  5 /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  8 /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  7 /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  5 /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  5 /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  8 /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  5 /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  8 /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  8 /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  3 /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  7 /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  8 /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  6 /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  1 /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  8 /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  5 /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  6 /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  4 /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  3 /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  6 /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  1 /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  8 /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  7 /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  3 /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  6 /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */


#ifdef C_ENABLE_COPY_RX_DATA
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 RxDataPtr V_MEMROM2 CanRxDataPtr[kCanNumberOfRxObjects] = 
{
  (RxDataPtr) CGW_SAS_2._c /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  (RxDataPtr) CGW_FCM_FRM_5._c /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  (RxDataPtr) CGW_FCM_5._c /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  (RxDataPtr) WCM_1._c /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  (RxDataPtr) AVAS_1._c /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  (RxDataPtr) TBOX_7._c /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  (RxDataPtr) CGW_OBC_1._c /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  (RxDataPtr) BCM_ALM._c /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  (RxDataPtr) CGW_BCM_SCM_3._c /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  (RxDataPtr) CGW_BCM_SCM_2._c /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  (RxDataPtr) CGW_BCM_SCM_1._c /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  (RxDataPtr) CGW_BCM_FM._c /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  (RxDataPtr) BCM_6._c /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  (RxDataPtr) CGW_BCM_TPMS_2._c /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  (RxDataPtr) CGW_FCM_10._c /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  (RxDataPtr) CGW_FCM_FRM_9._c /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  (RxDataPtr) CGW_FCM_FRM_8._c /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  (RxDataPtr) CGW_FCM_7._c /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  (RxDataPtr) CGW_FCM_6._c /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  (RxDataPtr) CGW_BCM_ACCM_3._c /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  (RxDataPtr) CGW_BCM_ACCM_1._c /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  (RxDataPtr) ICM_2._c /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  (RxDataPtr) CGW_FCM_FRM_6._c /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  (RxDataPtr) CGW_BMS_9._c /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  (RxDataPtr) CGW_VCU_E_3._c /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  (RxDataPtr) CGW_PLG_1._c /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  (RxDataPtr) RCM_1._c /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  (RxDataPtr) BCM_TPMS_1._c /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  (RxDataPtr) CGW_BCM_4._c /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  (RxDataPtr) CGW_BCM_5._c /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  (RxDataPtr) CGW_VCU_B_5._c /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  (RxDataPtr) CGW_VCU_B_4._c /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  (RxDataPtr) CGW_VCU_B_3._c /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  (RxDataPtr) CGW_FCM_2._c /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  (RxDataPtr) CGW_BSD_1._c /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  (RxDataPtr) CGW_BCM_MFS_1._c /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  (RxDataPtr) CGW_BCM_PEPS_1._c /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  (RxDataPtr) CGW_VCU_B_2._c /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  (RxDataPtr) ICM_1._c /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  (RxDataPtr) CGW_BMS_3._c /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  (RxDataPtr) CGW_BCM_1._c /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  (RxDataPtr) CGW_EPS_1._c /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  (RxDataPtr) CGW_BCS_EPB_1._c /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  (RxDataPtr) CGW_BCS_C_1._c /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  (RxDataPtr) CGW_EGS_1._c /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  (RxDataPtr) CGW_VCU_P_5._c /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */

#endif


#ifdef C_ENABLE_PRECOPY_FCT
/* CODE CATEGORY 1 START */
/* CODE CATEGORY 1 END */

/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 ApplPrecopyFct V_MEMROM2 CanRxApplPrecopyPtr[kCanNumberOfRxObjects] = 
{
  V_NULL /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  V_NULL /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  V_NULL /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  V_NULL /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  V_NULL /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  V_NULL /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  V_NULL /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  V_NULL /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  V_NULL /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  V_NULL /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  V_NULL /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  V_NULL /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  V_NULL /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  V_NULL /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  V_NULL /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  V_NULL /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  V_NULL /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  V_NULL /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  V_NULL /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  V_NULL /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  V_NULL /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  V_NULL /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  V_NULL /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  V_NULL /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  V_NULL /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  V_NULL /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  V_NULL /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  V_NULL /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  V_NULL /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  V_NULL /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  V_NULL /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  V_NULL /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  V_NULL /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  V_NULL /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  V_NULL /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  V_NULL /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  V_NULL /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  V_NULL /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  V_NULL /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  V_NULL /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  V_NULL /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  V_NULL /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  V_NULL /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  V_NULL /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  V_NULL /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  V_NULL /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */

#endif


#ifdef C_ENABLE_INDICATION_FCT
/* ROM CATEGORY 1 START */
/* PRQA  S 1334 QAC_Can_1334 */ /* MD_Can_1334 */
V_MEMROM0 V_MEMROM1 ApplIndicationFct V_MEMROM2 CanRxApplIndicationPtr[kCanNumberOfRxObjects] = 
{
  V_NULL /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  V_NULL /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  V_NULL /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  V_NULL /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  V_NULL /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  V_NULL /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  V_NULL /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  V_NULL /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  V_NULL /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  V_NULL /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  V_NULL /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  V_NULL /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  V_NULL /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  V_NULL /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  V_NULL /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  V_NULL /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  V_NULL /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  V_NULL /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  V_NULL /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  V_NULL /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  V_NULL /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  V_NULL /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  V_NULL /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  V_NULL /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  V_NULL /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  V_NULL /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  V_NULL /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  V_NULL /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  V_NULL /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  V_NULL /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  V_NULL /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  V_NULL /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  V_NULL /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  V_NULL /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  V_NULL /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  V_NULL /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  V_NULL /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  V_NULL /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  V_NULL /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  V_NULL /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  V_NULL /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  V_NULL /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  V_NULL /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  V_NULL /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  V_NULL /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  V_NULL /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* PRQA  L:QAC_Can_1334 */

/* ROM CATEGORY 1 END */

#endif


#ifdef C_ENABLE_INDICATION_FLAG
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanIndicationOffset[kCanNumberOfRxObjects] = 
{
  0 /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  0 /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  0 /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  0 /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  0 /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  0 /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  0 /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  0 /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  0 /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  0 /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  0 /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  0 /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  0 /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  0 /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  0 /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  0 /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  0 /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  0 /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  0 /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  0 /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  0 /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  0 /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  0 /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  0 /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  0 /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  0 /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  0 /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  0 /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  0 /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  0 /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  0 /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  0 /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  0 /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  0 /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  0 /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  0 /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  0 /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  0 /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  0 /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  0 /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  0 /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  0 /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  0 /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  0 /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  0 /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  0 /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */


/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanIndicationMask[kCanNumberOfRxObjects] = 
{
  0x00u /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  0x00u /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  0x00u /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  0x00u /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  0x00u /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  0x00u /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  0x00u /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  0x00u /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  0x00u /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  0x00u /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  0x00u /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  0x00u /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  0x00u /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  0x00u /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  0x00u /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  0x00u /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  0x00u /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  0x00u /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  0x00u /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  0x00u /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  0x00u /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  0x00u /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  0x00u /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  0x00u /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  0x00u /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  0x00u /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  0x00u /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  0x00u /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  0x00u /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  0x00u /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  0x00u /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  0x00u /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  0x00u /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  0x00u /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  0x00u /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  0x00u /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  0x00u /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  0x00u /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  0x00u /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  0x00u /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  0x00u /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  0x00u /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  0x00u /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  0x00u /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  0x00u /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  0x00u /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */


#endif




/* -----------------------------------------------------------------------------
    &&&~ Init structures
 ----------------------------------------------------------------------------- */

/* ROM CATEGORY 4 START */
/* PRQA S 3408 1 */ /* MD_Can_3408_extLinkage */

/* ROM CATEGORY 4 END */

/* ROM CATEGORY 4 START */
V_MEMROM0 V_MEMROM1 tCanInitObject V_MEMROM2 CanInitObject[1] = 
{
  
  {
    (vuint32) 0x01CB0000u /* Control 1 register */
  }
};

V_MEMROM0 V_MEMROM1 tCanInitBasicCan V_MEMROM2 CanInitBasicCan[8] = 
{
  
  {
    (vuint32) 0x1FFFFFFFu /* Mask Register */, 
    (vuint32) 0x0F4BFFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x1FFFFFFFu /* Mask Register */, 
    (vuint32) 0x0EC7FFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x1FFFFFFFu /* Mask Register */, 
    (vuint32) 0x0A63FFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x1FFFFFFFu /* Mask Register */, 
    (vuint32) 0x0A53FFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x1FFFFFFFu /* Mask Register */, 
    (vuint32) 0x0A47FFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x1FFFFFFFu /* Mask Register */, 
    (vuint32) 0x0A2BFFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x0A03FFFFu /* Mask Register */, 
    (vuint32) 0x0803FFFFu /* Code Register */
  }, 
  
  {
    (vuint32) 0x0063FFFFu /* Mask Register */, 
    (vuint32) 0x0003FFFFu /* Code Register */
  }
};

V_MEMROM0 V_MEMROM1 vuint16 V_MEMROM2 CanInitBasicCanIndex[1] = 
{
  (vuint16) 0x0000u
};

/* ROM CATEGORY 4 END */



#if defined(C_ENABLE_MULTI_ECU_CONFIG)
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanPhysToLogChannel[kVNumberOfIdentities][kCanNumberOfPhysChannels] = 
{
  
  {
    0
  }
};
/* ROM CATEGORY 1 END */

#endif

#if defined(C_ENABLE_MULTI_ECU_CONFIG)
/* ROM CATEGORY 2 START */

V_MEMROM0 V_MEMROM1 tVIdentityMsk V_MEMROM2 CanChannelIdentityAssignment[kCanNumberOfChannels] = 
{
  0x01u
};

/* ROM CATEGORY 2 END */

#endif

#if defined(C_ENABLE_MULTI_ECU_PHYS)
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 tVIdentityMsk V_MEMROM2 CanRxIdentityAssignment[kCanNumberOfRxObjects] = 
{
  0x01u /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  0x01u /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  0x01u /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  0x01u /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  0x01u /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  0x01u /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  0x01u /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  0x01u /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  0x01u /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  0x01u /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  0x01u /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  0x01u /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  0x01u /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  0x01u /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  0x01u /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  0x01u /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  0x01u /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  0x01u /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  0x01u /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  0x01u /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  0x01u /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  0x01u /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  0x01u /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  0x01u /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  0x01u /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  0x01u /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  0x01u /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  0x01u /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  0x01u /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  0x01u /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  0x01u /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  0x01u /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  0x01u /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  0x01u /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  0x01u /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  0x01u /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  0x01u /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  0x01u /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  0x01u /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  0x01u /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  0x01u /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  0x01u /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  0x01u /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  0x01u /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  0x01u /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  0x01u /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
/* ROM CATEGORY 1 END */

/* ROM CATEGORY 4 START */
V_MEMROM0 V_MEMROM1 tVIdentityMsk V_MEMROM2 CanTxIdentityAssignment[kCanNumberOfTxObjects] = 
{
  0x01u /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  0x01u /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  0x01u /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  0x01u /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  0x01u /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  0x01u /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  0x01u /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  0x01u /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  0x01u /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  0x01u /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  0x01u /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  0x01u /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  0x01u /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  0x01u /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  0x01u /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  0x01u /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  0x01u /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  0x01u /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  0x01u /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  0x01u /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 4 END */

#endif


#ifdef C_ENABLE_TX_FULLCAN_OBJECTS
/* ROM CATEGORY 2 START */
V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanTxMailbox[kCanNumberOfTxObjects] = 
{
  0x18u /* ID: 0x00000618, Handle: 0, NM_IHU [BC] */, 
  0x18u /* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */, 
  0x18u /* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */, 
  0x18u /* ID: 0x000003be, Handle: 3, IHU_18 [BC] */, 
  0x18u /* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */, 
  0x18u /* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */, 
  0x18u /* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */, 
  0x18u /* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */, 
  0x18u /* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */, 
  0x18u /* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */, 
  0x18u /* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */, 
  0x18u /* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */, 
  0x18u /* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */, 
  0x18u /* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */, 
  0x18u /* ID: 0x000003af, Handle: 14, IHU_17 [BC] */, 
  0x18u /* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */, 
  0x18u /* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */, 
  0x18u /* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */, 
  0x18u /* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */, 
  0x18u /* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
};
/* ROM CATEGORY 2 END */

#endif



/* -----------------------------------------------------------------------------
    &&&~ Can_Mailbox table
 ----------------------------------------------------------------------------- */

/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 tCanMailbox V_MEMROM2 Can_Mailbox[kCanNumberOfMailboxes] = 
{
  { 0, 1 } /* MailboxHandle: 0, Channel: Channel0, RxBasicCan */, 
  { 8, 1 } /* MailboxHandle: 1, Channel: Channel0, CGW_SAS_2 */, 
  { 9, 1 } /* MailboxHandle: 2, Channel: Channel0, CGW_FCM_5 */, 
  { 10, 1 } /* MailboxHandle: 3, Channel: Channel0, WCM_1 */, 
  { 11, 1 } /* MailboxHandle: 4, Channel: Channel0, AVAS_1 */, 
  { 12, 1 } /* MailboxHandle: 5, Channel: Channel0, TBOX_7 */, 
  { 13, 1 } /* MailboxHandle: 6, Channel: Channel0, BCM_ALM */, 
  { 14, 1 } /* MailboxHandle: 7, Channel: Channel0, CGW_BCM_SCM_3 */, 
  { 15, 1 } /* MailboxHandle: 8, Channel: Channel0, CGW_BCM_SCM_2 */, 
  { 16, 1 } /* MailboxHandle: 9, Channel: Channel0, CGW_BCM_SCM_1 */, 
  { 17, 1 } /* MailboxHandle: 10, Channel: Channel0, CGW_BCM_FM */, 
  { 18, 1 } /* MailboxHandle: 11, Channel: Channel0, BCM_6 */, 
  { 19, 1 } /* MailboxHandle: 12, Channel: Channel0, CGW_BCM_TPMS_2 */, 
  { 20, 1 } /* MailboxHandle: 13, Channel: Channel0, CGW_FCM_10 */, 
  { 21, 1 } /* MailboxHandle: 14, Channel: Channel0, CGW_FCM_FRM_9 */, 
  { 22, 1 } /* MailboxHandle: 15, Channel: Channel0, CGW_FCM_FRM_8 */, 
  { 23, 1 } /* MailboxHandle: 16, Channel: Channel0, CGW_FCM_7 */, 
  { 24, 1 } /* MailboxHandle: 17, Channel: Channel0, CGW_FCM_6 */, 
  { 25, 1 } /* MailboxHandle: 18, Channel: Channel0, CGW_BCM_ACCM_3 */, 
  { 26, 1 } /* MailboxHandle: 19, Channel: Channel0, CGW_VCU_B_4 */, 
  { 27, 1 } /* MailboxHandle: 20, Channel: Channel0, CGW_VCU_B_3 */, 
  { 28, 1 } /* MailboxHandle: 21, Channel: Channel0, CGW_BMS_3 */, 
  { 29, 1 } /* MailboxHandle: 22, Channel: Channel0, CGW_BCS_EPB_1 */, 
  { 30, 1 } /* MailboxHandle: 23, Channel: Channel0, CGW_VCU_P_5 */, 
  { 31, 1 } /* MailboxHandle: 24, Channel: Channel0, TxNormal */
} /* HwObjHandle, HwObjCount */;
/* ROM CATEGORY 1 END */



/* -----------------------------------------------------------------------------
    &&&~ Can_HwObjToCanMailboxIndirectionTable
 ----------------------------------------------------------------------------- */

/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 Can_HwObjToMailboxIndirection[kCanNumberOfHwObjToMailboxIndirections] = 
{
  0 /* Index: 0, Channel: Channel0, HwObjHandle: 0 */, 
  kCanMailboxNotUsed /* Index: 1, Channel: Channel0, HwObjHandle: 1 */, 
  kCanMailboxNotUsed /* Index: 2, Channel: Channel0, HwObjHandle: 2 */, 
  kCanMailboxNotUsed /* Index: 3, Channel: Channel0, HwObjHandle: 3 */, 
  kCanMailboxNotUsed /* Index: 4, Channel: Channel0, HwObjHandle: 4 */, 
  kCanMailboxNotUsed /* Index: 5, Channel: Channel0, HwObjHandle: 5 */, 
  kCanMailboxNotUsed /* Index: 6, Channel: Channel0, HwObjHandle: 6 */, 
  kCanMailboxNotUsed /* Index: 7, Channel: Channel0, HwObjHandle: 7 */, 
  1 /* Index: 8, Channel: Channel0, HwObjHandle: 8 */, 
  2 /* Index: 9, Channel: Channel0, HwObjHandle: 9 */, 
  3 /* Index: 10, Channel: Channel0, HwObjHandle: 10 */, 
  4 /* Index: 11, Channel: Channel0, HwObjHandle: 11 */, 
  5 /* Index: 12, Channel: Channel0, HwObjHandle: 12 */, 
  6 /* Index: 13, Channel: Channel0, HwObjHandle: 13 */, 
  7 /* Index: 14, Channel: Channel0, HwObjHandle: 14 */, 
  8 /* Index: 15, Channel: Channel0, HwObjHandle: 15 */, 
  9 /* Index: 16, Channel: Channel0, HwObjHandle: 16 */, 
  10 /* Index: 17, Channel: Channel0, HwObjHandle: 17 */, 
  11 /* Index: 18, Channel: Channel0, HwObjHandle: 18 */, 
  12 /* Index: 19, Channel: Channel0, HwObjHandle: 19 */, 
  13 /* Index: 20, Channel: Channel0, HwObjHandle: 20 */, 
  14 /* Index: 21, Channel: Channel0, HwObjHandle: 21 */, 
  15 /* Index: 22, Channel: Channel0, HwObjHandle: 22 */, 
  16 /* Index: 23, Channel: Channel0, HwObjHandle: 23 */, 
  17 /* Index: 24, Channel: Channel0, HwObjHandle: 24 */, 
  18 /* Index: 25, Channel: Channel0, HwObjHandle: 25 */, 
  19 /* Index: 26, Channel: Channel0, HwObjHandle: 26 */, 
  20 /* Index: 27, Channel: Channel0, HwObjHandle: 27 */, 
  21 /* Index: 28, Channel: Channel0, HwObjHandle: 28 */, 
  22 /* Index: 29, Channel: Channel0, HwObjHandle: 29 */, 
  23 /* Index: 30, Channel: Channel0, HwObjHandle: 30 */, 
  24 /* Index: 31, Channel: Channel0, HwObjHandle: 31 */
} /* MailboxHandle */;
/* ROM CATEGORY 1 END */






