/* -----------------------------------------------------------------------------
  Filename:    v_par.h
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: GenTool_GenyVcfgNameDecorator
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\WXWork\1688854678362950\Cache\File\2023-02\S61EV_FL(3).dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__V_PAR_H__)
#define __V_PAR_H__

/* -----------------------------------------------------------------------------
    &&&~ BaseEnv_PHF_Includes
 ----------------------------------------------------------------------------- */

#include "v_cfg.h"
#include "v_def.h"


/* -----------------------------------------------------------------------------
    &&&~ GENy Version Information
 ----------------------------------------------------------------------------- */

#define VGEN_DELIVERY_VERSION_BYTE_0         0x02
#define VGEN_DELIVERY_VERSION_BYTE_1         0x03
#define VGEN_DELIVERY_VERSION_BYTE_2         0x34
#define VGEN_DELIVERY_VERSION_BYTE_3         0x02
#define VGEN_DELIVERY_VERSION_BYTE_4         0x10
#define VGEN_DELIVERY_VERSION_BYTE_5         0x01
#define VGEN_DELIVERY_VERSION_BYTE_6         0x18
#define VGEN_DELIVERY_VERSION_BYTE_7         0x00
#define VGEN_DELIVERY_VERSION_BYTE_8         0x00
#define VGEN_DELIVERY_VERSION_BYTE_9         0x00
#define kGENyVersionNumberOfBytes            10
/* ROM CATEGORY 4 START */
V_MEMROM0 extern  V_MEMROM1 vuint8 V_MEMROM2 kGENyVersion[kGENyVersionNumberOfBytes];
/* ROM CATEGORY 4 END */




#endif /* __V_PAR_H__ */
