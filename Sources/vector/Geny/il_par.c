/* -----------------------------------------------------------------------------
  Filename:    il_par.c
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: Il_Vector
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\Documents and Settings\90506667\桌面\sh52\新建文件夹\S61EV_FLV1.4_2023_03_04.dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
    &&&~ Misra  justifications
 ----------------------------------------------------------------------------- */

/* PRQA S 3458 EOF *//* MD_CBD_19.4 */
/* PRQA S 3408 EOF *//* MD_Il_8.8 */
/* PRQA S 3460 EOF *//* MD_CBD_19.4 */
/* PRQA S 3412 EOF *//* MD_CBD_19.4 */
/* PRQA S 3453 EOF *//* MD_CBD_19.7 */
/* PRQA S 2006 EOF *//* MD_CBD_14.7 */
/* PRQA S 0777 EOF *//* MD_Il_0777 */
/* PRQA S 0778 EOF *//* MD_Il_0778 */
/* PRQA S 0779 EOF *//* MD_Il_0779 */
/* PRQA S 3673 EOF *//* MD_Il_3673 */
/* PRQA S 0310 EOF *//* MD_Il_0310 */
/* PRQA S 0312 EOF *//* MD_Il_0312 */
/* PRQA S 0635 EOF *//* MD_Il_0635 */
/* PRQA S 0781 EOF *//* MD_Il_0781 */
/* PRQA S 3410 EOF *//* MD_Il_3410 */
/* PRQA S 1330 EOF *//* MD_Il_1330 */
/* PRQA S 0342 EOF *//* MD_Il_0342 */
/* PRQA S 0857 EOF *//* MD_CBD_1.1 */
/* PRQA S 3109 EOF *//* MD_CBD_14.3 */
/* PRQA S 0883 EOF *//* */


#include "il_inc.h"

/* -----------------------------------------------------------------------------
    &&&~ local variables
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_CYCLIC_EVENT)
V_MEMRAM0 V_MEMRAM1_NEAR IlTIfActiveFlag V_MEMRAM2_NEAR ilIfActiveFlags;
#endif



/* -----------------------------------------------------------------------------
    &&&~ Local function prototypes
 ----------------------------------------------------------------------------- */



/* -----------------------------------------------------------------------------
    &&&~ Internal Get Tx Signal Access for signals smaller or equal 8bit
 ----------------------------------------------------------------------------- */

/* Handle:   82,Name:                 IHU_AUTO_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AUTO_SwSts()       (IHU_6.IHU_6.IHU_AUTO_SwSts)
#endif

/* Handle:   83,Name:                   IHU_AC_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AC_SwSts()         (IHU_6.IHU_6.IHU_AC_SwSts)
#endif

/* Handle:   84,Name:                  IHU_PTC_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_PTC_SwSts()        (IHU_6.IHU_6.IHU_PTC_SwSts)
#endif

/* Handle:   85,Name:                  IHU_OFF_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_OFF_SwSts()        (IHU_6.IHU_6.IHU_OFF_SwSts)
#endif

/* Handle:   86,Name:              IHU_RearDfstSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_RearDfstSwSts()    (IHU_6.IHU_6.IHU_RearDfstSwSts)
#endif

/* Handle:   87,Name:         IHU_CirculationModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_CirculationModeSet() (IHU_6.IHU_6.IHU_CirculationModeSet)
#endif

/* Handle:   88,Name:              IHU_AutoDfstSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AutoDfstSwSts()    (IHU_6.IHU_6.IHU_AutoDfstSwSts)
#endif

/* Handle:   89,Name:                IHU_BlowModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_BlowModeSet()      (IHU_6.IHU_6.IHU_BlowModeSet)
#endif

/* Handle:   90,Name:             IHU_TempSet_Driver,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_TempSet_Driver()   (IHU_6.IHU_6.IHU_TempSet_Driver)
#endif

/* Handle:   91,Name:             IHU_BlowerlevelSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_BlowerlevelSet()   (IHU_6.IHU_6.IHU_BlowerlevelSet)
#endif

/* Handle:   92,Name:        IHU_AnionGeneratorSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AnionGeneratorSwSts() (IHU_6.IHU_6.IHU_AnionGeneratorSwSts)
#endif

/* Handle:   93,Name:               IHU_AQS_EnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AQS_EnablaSw()     (IHU_6.IHU_6.IHU_AQS_EnablaSw)
#endif

/* Handle:   94,Name:           IHU_AutoBlowEnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AutoBlowEnablaSw() (IHU_6.IHU_6.IHU_AutoBlowEnablaSw)
#endif

/* Handle:   95,Name:      IHU_AC_MemoryModeEnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_AC_MemoryModeEnablaSw() (IHU_6.IHU_6.IHU_AC_MemoryModeEnablaSw)
#endif

/* Handle:   96,Name:              IHU_DUAL_EnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_DUAL_EnablaSw()    (IHU_6.IHU_6.IHU_DUAL_EnablaSw)
#endif

/* Handle:   97,Name:          IHU_FrontDefrostSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_FrontDefrostSwSts() (IHU_6.IHU_6.IHU_FrontDefrostSwSts)
#endif

/* Handle:   98,Name:           IHU_VentilationSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_VentilationSwSts() (IHU_6.IHU_6.IHU_VentilationSwSts)
#endif

/* Handle:   99,Name:                  IHU_ECO_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_ECO_SwSts()        (IHU_6.IHU_6.IHU_ECO_SwSts)
#endif

/* Handle:  100,Name:              IHU_TempSet_Psngr,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_TempSet_Psngr()    (IHU_6.IHU_6.IHU_TempSet_Psngr)
#endif

/* Handle:  101,Name:             IHU_LightCtrlSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_LightCtrlSwSts()   (IHU_6.IHU_6.IHU_LightCtrlSwSts)
#endif

/* Handle:  102,Name:            IHU_FrontWiperSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_FrontWiperSwSts()  (IHU_6.IHU_6.IHU_FrontWiperSwSts)
#endif

/* Handle:  103,Name:          IHU_RearFogLightSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_RearFogLightSwSts() (IHU_6.IHU_6.IHU_RearFogLightSwSts)
#endif

/* Handle:  104,Name: IHU_MirrorReversePositionStoreReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_MirrorReversePositionStoreReq() (IHU_6.IHU_6.IHU_MirrorReversePositionStoreReq)
#endif

/* Handle:  107,Name:             IHU_RearWiperSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_RearWiperSwSts()   (IHU_6.IHU_6.IHU_RearWiperSwSts)
#endif

/* Handle:  108,Name:              IHU_RearWashSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPrivateGetTxIHU_RearWashSwSts()    (IHU_6.IHU_6.IHU_RearWashSwSts)
#endif



#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_TIMEOUT)
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 IlTxTimeoutIndirection[kIlNumberOfTxObjects] = 
{
  kIlNoTxToutIndirection /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  kIlNoTxToutIndirection /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_FAST_ON_START)
V_MEMROM0 V_MEMROM1 IltTxCounter V_MEMROM2 IlTxFastOnStartDuration[kIlNumberOfTxObjects] = 
{
  kIlNoFastOnStartDuration /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  kIlNoFastOnStartDuration /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_FAST_ON_START) && defined(IL_ENABLE_TX_MODE_SIGNALS)
V_MEMROM0 V_MEMROM1 IltTxCounter V_MEMROM2 IlTxFastOnStartMuxDelay[kIlNumberOfTxObjects] = 
{
  0 /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  0 /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  0 /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  0 /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  0 /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  0 /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  0 /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  0 /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  0 /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  0 /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  0 /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  0 /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  0 /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  0 /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  0 /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  0 /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  0 /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  0 /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  0 /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX)
V_MEMROM0 V_MEMROM1 IltTxCounter V_MEMROM2 IlTxStartCycles[kIlNumberOfTxObjects] = 
{
  kIlNoCycleTime /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  kIlNoCycleTime /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  kIlNoCycleTime /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX)
V_MEMROM0 V_MEMROM1 IltTxUpdateCounter V_MEMROM2 IlTxUpdateCycles[kIlNumberOfTxObjects] = 
{
  kIlNoDelayTime /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  kIlNoDelayTime /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  kIlNoDelayTime /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  kIlNoDelayTime /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX)
V_MEMROM0 V_MEMROM1 IltTxCounter V_MEMROM2 IlTxCyclicCycles[kIlNumberOfTxObjects] = 
{
  2 /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  kIlNoCycleTime /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  10 /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  50 /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  10 /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  kIlNoCycleTime /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  10 /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  50 /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  10 /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  5 /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX) && (defined(IL_ENABLE_TX_CYCLIC_EVENT) || defined(IL_ENABLE_TX_SECURE_EVENT) || defined(IL_ENABLE_TX_FAST_ON_START))
V_MEMROM0 V_MEMROM1 IltTxCounter V_MEMROM2 IlTxEventCycles[kIlNumberOfTxObjects] = 
{
  kIlNoCycleTimeFast /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  10 /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  2 /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  10 /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  kIlNoCycleTimeFast /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  kIlNoCycleTimeFast /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  10 /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  10 /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  10 /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  2 /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  10 /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  10 /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  10 /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  10 /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  10 /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  kIlNoCycleTimeFast /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  kIlNoCycleTimeFast /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  2 /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  kIlNoCycleTimeFast /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_POLLING)
V_MEMROM0 V_MEMROM1 IlConfirmationFct V_MEMROM2 IlTxConfirmationFctPtr[kIlNumberOfTxObjects] = 
{
  V_NULL /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  V_NULL /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  V_NULL /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  V_NULL /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  V_NULL /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  V_NULL /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  V_NULL /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  V_NULL /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  V_NULL /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  V_NULL /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  V_NULL /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  V_NULL /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  V_NULL /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  V_NULL /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  V_NULL /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  V_NULL /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  V_NULL /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  V_NULL /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  V_NULL /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_TIMEOUT) && defined(C_MULTIPLE_RECEIVE_CHANNEL) && defined(IL_ENABLE_TX_VARYING_TIMEOUT) && (kIlNumberOfChannels > 1)
V_MEMROM0 V_MEMROM1 IltTxTimeoutCounter V_MEMROM2 IlTxTimeout[kIlNumberOfChannels] = 
{
  0
};
#endif


#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_SECURE_EVENT) && defined(IL_ENABLE_TX_VARYING_REPETITION) && (kIlNumberOfTxObjects > 1)
V_MEMROM0 V_MEMROM1 IltTxRepetitionCounter V_MEMROM2 IlTxRepetitionCounters[kIlNumberOfTxObjects] = 
{
  0 /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  3 /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  3 /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  3 /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  0 /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  0 /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  3 /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  3 /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  3 /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  3 /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  3 /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  3 /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  3 /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  3 /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  3 /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  0 /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  0 /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  3 /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  0 /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


#if defined(IL_ENABLE_RX) && defined(IL_ENABLE_RX_POLLING)
V_MEMROM0 V_MEMROM1 IlIndicationFct V_MEMROM2 IlCanRxIndicationFctPtr[kIlCanNumberOfRxObjects] = 
{
  V_NULL /* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */, 
  V_NULL /* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */, 
  V_NULL /* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */, 
  V_NULL /* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */, 
  V_NULL /* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */, 
  V_NULL /* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */, 
  V_NULL /* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */, 
  V_NULL /* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */, 
  V_NULL /* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */, 
  V_NULL /* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */, 
  V_NULL /* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */, 
  V_NULL /* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */, 
  V_NULL /* ID: 0x00000457, Handle: 12, BCM_6 [FC] */, 
  V_NULL /* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */, 
  V_NULL /* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */, 
  V_NULL /* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */, 
  V_NULL /* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */, 
  V_NULL /* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */, 
  V_NULL /* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */, 
  V_NULL /* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */, 
  V_NULL /* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */, 
  V_NULL /* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */, 
  V_NULL /* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */, 
  V_NULL /* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */, 
  V_NULL /* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */, 
  V_NULL /* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */, 
  V_NULL /* ID: 0x00000328, Handle: 26, RCM_1 [BC] */, 
  V_NULL /* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */, 
  V_NULL /* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */, 
  V_NULL /* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */, 
  V_NULL /* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */, 
  V_NULL /* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */, 
  V_NULL /* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */, 
  V_NULL /* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */, 
  V_NULL /* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */, 
  V_NULL /* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */, 
  V_NULL /* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */, 
  V_NULL /* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */, 
  V_NULL /* ID: 0x00000271, Handle: 38, ICM_1 [BC] */, 
  V_NULL /* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */, 
  V_NULL /* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */, 
  V_NULL /* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */, 
  V_NULL /* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */, 
  V_NULL /* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */, 
  V_NULL /* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */, 
  V_NULL /* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
};
#endif


/* -----------------------------------------------------------------------------
    &&&~ TxDefaultInitValue Message Tables
 ----------------------------------------------------------------------------- */

V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 IHU_6IlTxDefaultInitValue[8] = 
{
  0x00, 
  0x08, 
  0x00, 
  0x00, 
  0x08, 
  0x00, 
  0x00, 
  0x00
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 IHU_5IlTxDefaultInitValue[8] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x08, 
  0x00, 
  0x00, 
  0x00, 
  0x00
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 IHU_14IlTxDefaultInitValue[8] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x42, 
  0x40, 
  0x00, 
  0x00, 
  0x00
};


/* -----------------------------------------------------------------------------
    &&&~ RxDefaultInitValue Message Tables
 ----------------------------------------------------------------------------- */

V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_SAS_2IlRxDefaultInitValue[4] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x00
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_BCM_TPMS_2IlRxDefaultInitValue[8] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x28, 
  0x28, 
  0x28, 
  0x28
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_FCM_10IlRxDefaultInitValue[7] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x10, 
  0x00
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_FCM_FRM_9IlRxDefaultInitValue[8] = 
{
  0x00, 
  0x03, 
  0xE6, 
  0x00, 
  0x03, 
  0xE6, 
  0x00, 
  0x00
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_FCM_FRM_8IlRxDefaultInitValue[8] = 
{
  0x00, 
  0x03, 
  0xE6, 
  0x00, 
  0x03, 
  0xE6, 
  0x00, 
  0x00
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_FCM_7IlRxDefaultInitValue[6] = 
{
  0x03, 
  0xE5, 
  0x03, 
  0x03, 
  0xE5, 
  0x03
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_FCM_6IlRxDefaultInitValue[6] = 
{
  0x03, 
  0xE5, 
  0x03, 
  0x03, 
  0xE5, 
  0x03
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_BCM_ACCM_1IlRxDefaultInitValue[7] = 
{
  0x00, 
  0x08, 
  0x00, 
  0x00, 
  0x28, 
  0x50, 
  0x08
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_VCU_B_4IlRxDefaultInitValue[7] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x03, 
  0xE7
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_VCU_B_2IlRxDefaultInitValue[6] = 
{
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x00, 
  0x01
};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CGW_BMS_3IlRxDefaultInitValue[3] = 
{
  0x00, 
  0x1F, 
  0x3F
};


/* -----------------------------------------------------------------------------
    &&&~ TxDefaultInitValue
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_DEFAULTVALUE)
V_MEMROM0 IL_MEMROM1 vuint8 IL_MEMROM2 IL_MEMROM3 *V_MEMROM1 V_MEMROM2  IlTxDefaultInitValue[kIlNumberOfTxObjects] =
{
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  IHU_6IlTxDefaultInitValue,
  IHU_5IlTxDefaultInitValue,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  V_NULL,
  IHU_14IlTxDefaultInitValue,
  V_NULL,
  V_NULL
};
#endif



/* -----------------------------------------------------------------------------
    &&&~ RxDefaultInitValue
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_RX) && defined(IL_ENABLE_RX_DEFAULTVALUE)
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 V_MEMROM3* V_MEMROM1 V_MEMROM2 IlRxDefaultInitValue[kIlNumberOfRxObjects] = 
{
  CGW_SAS_2IlRxDefaultInitValue, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  CGW_BCM_TPMS_2IlRxDefaultInitValue, 
  CGW_FCM_10IlRxDefaultInitValue, 
  CGW_FCM_FRM_9IlRxDefaultInitValue, 
  CGW_FCM_FRM_8IlRxDefaultInitValue, 
  CGW_FCM_7IlRxDefaultInitValue, 
  CGW_FCM_6IlRxDefaultInitValue, 
  V_NULL, 
  CGW_BCM_ACCM_1IlRxDefaultInitValue, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  CGW_VCU_B_4IlRxDefaultInitValue, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  CGW_VCU_B_2IlRxDefaultInitValue, 
  V_NULL, 
  CGW_BMS_3IlRxDefaultInitValue, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL, 
  V_NULL
};
#endif



/* -----------------------------------------------------------------------------
    &&&~ Declaration checking macro for signals IfActive or IfActiveWithRepetition with size < 32bit
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX)
#define IHU_11_FCWSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_FCWSnvtySetIsActive(sigData)  ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_AEBSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_SCF_request_confirmedIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_SCFSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_SLA_TSI_TLI_SwtSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_LKASwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_HMASwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_IESSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_DAISwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_LDPSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_WarnModSwtSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_LDWSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_ELKSwtSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_11_LDWLDPSnvtySetLDWLDPIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightBrightnessAdjust_FrontIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightBrightnessAdjust_RoofIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightBrightnessAdjust_RearIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightColorSet_FrontIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightColorSet_RoofIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightColorSet_RearIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RegionColorSynchronizationReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightModeSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightAreaDisplayEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightBreatheEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AmbientLightBreatheWithVehSpdEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_Chg_SOC_LimitPointSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ChgModeSetIsActive(sigData)      ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BookChgStartTimeSet_HourIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BookChgStartTimeSet_MinuteIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BookChgTimeSet_VDIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BookChgStopTimeSet_HourIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BookchgStopTimeSet_MinuteIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RearWarnSystemEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ICM_ThemelSetIsActive(sigData)   ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ICM_ScreenReqIsActive(sigData)   ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_OverSpdValueSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ICM_Menu_OK_ButtonStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ICM_ModeButtonStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FatigureDrivingTimeSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ICM_UpButtonStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ICM_DownButtonStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU__ICM_LeftButtonStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU__ICM_RightButtonStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BrightnessAdjustSet_ICMIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_DVR_QuickCameraSwIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatHeatLevelSetReq_RLIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatHeatLevelSetReq_RRIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_WirelessChargingEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AVAS_VolumeSetIsActive(sigData)  ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AVAS_AudioSourceSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AVAS_EnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatMemoryEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatPositionStoreReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatPositionCalloutReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatHeatVentLevelSetReq_DriveIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SeatHeatVentLevelSetReq_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FaceRecSeatEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FaceRecMirrorInclineEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FaceRecAuthenticationResultIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AccountDeletedReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_EntryExitSeatCtrlEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_InverterConfirmStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_InverterEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_CruiseTypeSetStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VehWashModeSetIsActive(sigData)  ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SpeedLimitValueSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SpeedLimitEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RegenerationLevelSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_DriveModeSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_OutputLimitSOCSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ShiftVoiceRemind_EnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ShiftErrorActionRemindEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SOC_ChargeLockEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AutoLockEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AntiTheftModeSetSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SteeringWheelHeatReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MirrorFoldEnableSwSts__IsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FollowHomeMeEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SunroofAutoCloseEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_WelcomeLightEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ComfortModeSetIsActive(sigData)  ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_WiperSensitivitySetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MirrorFoldReqIsActive(sigData)   ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AHB_EnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_LowBeamAngleAdjustIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FrontWiperMaintenanceModeReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_EmergencyPowerOffReqConfirmIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MirrorReverseInclineEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FollowMeHomeTimeSetStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_DoorWindowCtrlEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_WindowAutoCloseEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RKE_SunRoofCtrlEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_DomeLightDoorCtrlSwitchStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MaxPositionSetIsActive(sigData)  ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FragranceEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FragranceConcentrationSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FragrancePositionSetIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_SunroofReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_SunshadeReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_WindowReq_FLIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_WindowReq_FRIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_WindowReq_RLIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_WindowReq_RRIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_LowbeamReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VoiceCtrl_TrunkReqIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_SmokingModeReqIsActive(sigData)  ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MassageSwithSet_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MassageModeSet_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MassageStrengthSet_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_LumbarSupportModeSet_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_ForwardBack_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_SeatBack_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_17_MsgAliveCounterIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_LegSupport_PsngrIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_ForwardBack_DriverIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_UpDown_DriverIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_SeatBack_DriverIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_16_MsgAliveCounterIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MotoPositionSet_LegSupport_DriverIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BrakemodeSetIsActive(sigData)    ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_EPS_ModeSetIsActive(sigData)     ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ESC_OFF_SwStsIsActive(sigData)   ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_CST_FunctionEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_HDC_FunctionEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AVH_FunctionEnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_PDC_EnableSwStsIsActive(sigData) ((vuint8) ((sigData) != (vuint8) 0))
#endif



/* -----------------------------------------------------------------------------
    &&&~ Declaration checking macro for signals OnChange or OnChangeWithRepetition with size < 32bit
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX)
#define IHU_AUTO_SwStsValueChanged(sigData)  (((sigData) != (IlPrivateGetTxIHU_AUTO_SwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AC_SwStsValueChanged(sigData)    (((sigData) != (IlPrivateGetTxIHU_AC_SwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_PTC_SwStsValueChanged(sigData)   (((sigData) != (IlPrivateGetTxIHU_PTC_SwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_OFF_SwStsValueChanged(sigData)   (((sigData) != (IlPrivateGetTxIHU_OFF_SwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RearDfstSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_RearDfstSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_CirculationModeSetValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_CirculationModeSet())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AutoDfstSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_AutoDfstSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BlowModeSetValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_BlowModeSet())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_TempSet_DriverValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_TempSet_Driver())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_BlowerlevelSetValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_BlowerlevelSet())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AnionGeneratorSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_AnionGeneratorSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AQS_EnablaSwValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_AQS_EnablaSw())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AutoBlowEnablaSwValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_AutoBlowEnablaSw())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_AC_MemoryModeEnablaSwValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_AC_MemoryModeEnablaSw())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_DUAL_EnablaSwValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_DUAL_EnablaSw())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FrontDefrostSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_FrontDefrostSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_VentilationSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_VentilationSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_ECO_SwStsValueChanged(sigData)   (((sigData) != (IlPrivateGetTxIHU_ECO_SwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_TempSet_PsngrValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_TempSet_Psngr())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_LightCtrlSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_LightCtrlSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_FrontWiperSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_FrontWiperSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RearFogLightSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_RearFogLightSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_MirrorReversePositionStoreReqValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_MirrorReversePositionStoreReq())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RearWiperSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_RearWiperSwSts())))
#endif

#if defined(IL_ENABLE_TX)
#define IHU_RearWashSwStsValueChanged(sigData) (((sigData) != (IlPrivateGetTxIHU_RearWashSwSts())))
#endif



/* -----------------------------------------------------------------------------
    &&&~ Implementation Init function for IfActive flags
 ----------------------------------------------------------------------------- */

void InitIfActiveFlags(void )
{
  {
    VStdMemNearClr( & ilIfActiveFlags, sizeof(ilIfActiveFlags));
  }
}




/* -----------------------------------------------------------------------------
    &&&~ Implementation of a function to check IfActive flags
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_SYS_TX_SIGNALS_ARE_ACTIVE_FCT)
Il_Boolean IlTxSignalsAreActive(void )
{
  {
    vuint16 nCnt = sizeof(ilIfActiveFlags);
    while(nCnt > (vuint16) 0x00)
    {
       -- nCnt;
      if(((vuint8*) ( & ilIfActiveFlags))[nCnt] != (vuint8) 0x00)
      {
        return IL_TRUE;
      }
    }
  }
  return IL_FALSE;
}


#endif



/* -----------------------------------------------------------------------------
    &&&~ Implementation function to reset indication flags
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_RX) && defined(IL_ENABLE_RX_POLLING)
void IlResetCanIndicationFlags(void )
{
  CanGlobalInterruptDisable();
  CanGlobalInterruptRestore();
}


#endif



/* -----------------------------------------------------------------------------
    &&&~ Implementation function to reset confirmation flags
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX)
void IlResetCanConfirmationFlags(void )
{
  CanGlobalInterruptDisable();
  IlGetTxConfirmationFlags(0) &= (vuint8) 0x00;
  IlGetTxConfirmationFlags(1) &= (vuint8) 0x00;
  IlGetTxConfirmationFlags(2) &= (vuint8) 0xF8;
  CanGlobalInterruptRestore();
}


#endif



/* -----------------------------------------------------------------------------
    &&&~ Get Rx Signal Access for signals greater 8bit and smaller or equal 32bit
 ----------------------------------------------------------------------------- */

/* Handle:    2,Name:              SAS_SteeringAngle,Size: 16,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxSAS_SteeringAngle(void)
{
  vuint16 rc;
  IlEnterCriticalSAS_SteeringAngle();
  rc = ((vuint16) CGW_SAS_2.CGW_SAS_2.SAS_SteeringAngle_0);
  rc |= ((vuint16) CGW_SAS_2.CGW_SAS_2.SAS_SteeringAngle_1) << 8;
  IlLeaveCriticalSAS_SteeringAngle();
  return rc;
}


#endif

/* Handle:  113,Name:      FCM_10_EgoLeftLineHeatgAg,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_10_EgoLeftLineHeatgAg(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_10_EgoLeftLineHeatgAg();
  rc = ((vuint16) CGW_FCM_10.CGW_FCM_10.FCM_10_EgoLeftLineHeatgAg_0);
  rc |= ((vuint16) CGW_FCM_10.CGW_FCM_10.FCM_10_EgoLeftLineHeatgAg_1) << 4;
  IlLeaveCriticalFCM_10_EgoLeftLineHeatgAg();
  return rc;
}


#endif

/* Handle:  115,Name:        FCM_FRM_9_ACCObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint8 IlGetRxFCM_FRM_9_ACCObjLgtDstX(void)
{
  vuint8 rc;
  IlEnterCriticalFCM_FRM_9_ACCObjLgtDstX();
  rc = ((vuint8) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_ACCObjLgtDstX_0);
  rc |= ((vuint8) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_ACCObjLgtDstX_1) << 5;
  IlLeaveCriticalFCM_FRM_9_ACCObjLgtDstX();
  return rc;
}


#endif

/* Handle:  118,Name:        FCM_FRM_9_ACCObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_FRM_9_ACCObjHozDstY(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_FRM_9_ACCObjHozDstY();
  rc = ((vuint16) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_ACCObjHozDstY_0);
  rc |= ((vuint16) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_ACCObjHozDstY_1) << 7;
  IlLeaveCriticalFCM_FRM_9_ACCObjHozDstY();
  return rc;
}


#endif

/* Handle:  119,Name:    FCM_FRM_9_FrntFarObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint8 IlGetRxFCM_FRM_9_FrntFarObjLgtDstX(void)
{
  vuint8 rc;
  IlEnterCriticalFCM_FRM_9_FrntFarObjLgtDstX();
  rc = ((vuint8) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_FrntFarObjLgtDstX_0);
  rc |= ((vuint8) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_FrntFarObjLgtDstX_1) << 5;
  IlLeaveCriticalFCM_FRM_9_FrntFarObjLgtDstX();
  return rc;
}


#endif

/* Handle:  122,Name:    FCM_FRM_9_FrntFarObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_FRM_9_FrntFarObjHozDstY(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_FRM_9_FrntFarObjHozDstY();
  rc = ((vuint16) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_FrntFarObjHozDstY_0);
  rc |= ((vuint16) CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_FrntFarObjHozDstY_1) << 7;
  IlLeaveCriticalFCM_FRM_9_FrntFarObjHozDstY();
  return rc;
}


#endif

/* Handle:  125,Name:        FCM_FRM_8_ACCObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint8 IlGetRxFCM_FRM_8_ACCObjLgtDstX(void)
{
  vuint8 rc;
  IlEnterCriticalFCM_FRM_8_ACCObjLgtDstX();
  rc = ((vuint8) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_ACCObjLgtDstX_0);
  rc |= ((vuint8) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_ACCObjLgtDstX_1) << 5;
  IlLeaveCriticalFCM_FRM_8_ACCObjLgtDstX();
  return rc;
}


#endif

/* Handle:  128,Name:        FCM_FRM_8_ACCObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_FRM_8_ACCObjHozDstY(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_FRM_8_ACCObjHozDstY();
  rc = ((vuint16) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_ACCObjHozDstY_0);
  rc |= ((vuint16) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_ACCObjHozDstY_1) << 7;
  IlLeaveCriticalFCM_FRM_8_ACCObjHozDstY();
  return rc;
}


#endif

/* Handle:  129,Name:    FCM_FRM_8_FrntFarObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint8 IlGetRxFCM_FRM_8_FrntFarObjLgtDstX(void)
{
  vuint8 rc;
  IlEnterCriticalFCM_FRM_8_FrntFarObjLgtDstX();
  rc = ((vuint8) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_FrntFarObjLgtDstX_0);
  rc |= ((vuint8) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_FrntFarObjLgtDstX_1) << 5;
  IlLeaveCriticalFCM_FRM_8_FrntFarObjLgtDstX();
  return rc;
}


#endif

/* Handle:  132,Name:    FCM_FRM_8_FrntFarObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_FRM_8_FrntFarObjHozDstY(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_FRM_8_FrntFarObjHozDstY();
  rc = ((vuint16) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_FrntFarObjHozDstY_0);
  rc |= ((vuint16) CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_FrntFarObjHozDstY_1) << 7;
  IlLeaveCriticalFCM_FRM_8_FrntFarObjHozDstY();
  return rc;
}


#endif

/* Handle:  135,Name:       FCM_7_NeborLeLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_7_NeborLeLineHozlDst(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_7_NeborLeLineHozlDst();
  rc = ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineHozlDst_0);
  rc |= ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineHozlDst_1) << 6;
  IlLeaveCriticalFCM_7_NeborLeLineHozlDst();
  return rc;
}


#endif

/* Handle:  139,Name:          FCM_7_NeborLeLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_7_NeborLeLineCrvt(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_7_NeborLeLineCrvt();
  rc = ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineCrvt_0);
  rc |= ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineCrvt_1) << 8;
  IlLeaveCriticalFCM_7_NeborLeLineCrvt();
  return rc;
}


#endif

/* Handle:  140,Name:       FCM_7_NeborRiLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_7_NeborRiLineHozlDst(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_7_NeborRiLineHozlDst();
  rc = ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineHozlDst_0);
  rc |= ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineHozlDst_1) << 6;
  IlLeaveCriticalFCM_7_NeborRiLineHozlDst();
  return rc;
}


#endif

/* Handle:  144,Name:          FCM_7_NeborRiLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_7_NeborRiLineCrvt(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_7_NeborRiLineCrvt();
  rc = ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineCrvt_0);
  rc |= ((vuint16) CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineCrvt_1) << 8;
  IlLeaveCriticalFCM_7_NeborRiLineCrvt();
  return rc;
}


#endif

/* Handle:  145,Name:         FCM_6_EgoLeLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_6_EgoLeLineHozlDst(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_6_EgoLeLineHozlDst();
  rc = ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineHozlDst_0);
  rc |= ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineHozlDst_1) << 6;
  IlLeaveCriticalFCM_6_EgoLeLineHozlDst();
  return rc;
}


#endif

/* Handle:  149,Name:            FCM_6_EgoLeLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_6_EgoLeLineCrvt(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_6_EgoLeLineCrvt();
  rc = ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineCrvt_0);
  rc |= ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineCrvt_1) << 8;
  IlLeaveCriticalFCM_6_EgoLeLineCrvt();
  return rc;
}


#endif

/* Handle:  150,Name:         FCM_6_EgoRiLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_6_EgoRiLineHozlDst(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_6_EgoRiLineHozlDst();
  rc = ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineHozlDst_0);
  rc |= ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineHozlDst_1) << 6;
  IlLeaveCriticalFCM_6_EgoRiLineHozlDst();
  return rc;
}


#endif

/* Handle:  154,Name:            FCM_6_EgoRiLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_6_EgoRiLineCrvt(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_6_EgoRiLineCrvt();
  rc = ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineCrvt_0);
  rc |= ((vuint16) CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineCrvt_1) << 8;
  IlLeaveCriticalFCM_6_EgoRiLineCrvt();
  return rc;
}


#endif

/* Handle:  156,Name:       ACCM_PM2_5_InternalValue,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxACCM_PM2_5_InternalValue(void)
{
  vuint16 rc;
  IlEnterCriticalACCM_PM2_5_InternalValue();
  rc = ((vuint16) CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_PM2_5_InternalValue_0);
  rc |= ((vuint16) CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_PM2_5_InternalValue_1) << 2;
  IlLeaveCriticalACCM_PM2_5_InternalValue();
  return rc;
}


#endif

/* Handle:  157,Name:       ACCM_PM2_5_ExternalValue,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxACCM_PM2_5_ExternalValue(void)
{
  vuint16 rc;
  IlEnterCriticalACCM_PM2_5_ExternalValue();
  rc = ((vuint16) CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_PM2_5_ExternalValue_0);
  rc |= ((vuint16) CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_PM2_5_ExternalValue_1) << 8;
  IlLeaveCriticalACCM_PM2_5_ExternalValue();
  return rc;
}


#endif

/* Handle:  196,Name:              FCM_FRM_6_VSetDis,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxFCM_FRM_6_VSetDis(void)
{
  vuint16 rc;
  IlEnterCriticalFCM_FRM_6_VSetDis();
  rc = ((vuint16) CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_VSetDis_0);
  rc |= ((vuint16) CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_VSetDis_1) << 4;
  IlLeaveCriticalFCM_FRM_6_VSetDis();
  return rc;
}


#endif

/* Handle:  200,Name:                   BMS_SOC_Disp,Size: 14,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxBMS_SOC_Disp(void)
{
  vuint16 rc;
  IlEnterCriticalBMS_SOC_Disp();
  rc = ((vuint16) CGW_BMS_9.CGW_BMS_9.BMS_SOC_Disp_0);
  rc |= ((vuint16) CGW_BMS_9.CGW_BMS_9.BMS_SOC_Disp_1) << 6;
  IlLeaveCriticalBMS_SOC_Disp();
  return rc;
}


#endif

/* Handle:  279,Name:           VCU_ResidualOdometer,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxVCU_ResidualOdometer(void)
{
  vuint16 rc;
  IlEnterCriticalVCU_ResidualOdometer();
  rc = ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_ResidualOdometer_0);
  rc |= ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_ResidualOdometer_1) << 2;
  IlLeaveCriticalVCU_ResidualOdometer();
  return rc;
}


#endif

/* Handle:  280,Name:            VCU_InstPowerConsum,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxVCU_InstPowerConsum(void)
{
  vuint16 rc;
  IlEnterCriticalVCU_InstPowerConsum();
  rc = ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_InstPowerConsum_0);
  rc |= ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_InstPowerConsum_1) << 4;
  IlLeaveCriticalVCU_InstPowerConsum();
  return rc;
}


#endif

/* Handle:  281,Name:             VCU_TotalPowerDisp,Size: 12,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxVCU_TotalPowerDisp(void)
{
  vuint16 rc;
  IlEnterCriticalVCU_TotalPowerDisp();
  rc = ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_TotalPowerDisp_0);
  rc |= ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_TotalPowerDisp_1) << 4;
  IlLeaveCriticalVCU_TotalPowerDisp();
  return rc;
}


#endif

/* Handle:  282,Name:              VCU_InstPowerDisp,Size: 12,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxVCU_InstPowerDisp(void)
{
  vuint16 rc;
  IlEnterCriticalVCU_InstPowerDisp();
  rc = ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_InstPowerDisp_0);
  rc |= ((vuint16) CGW_VCU_B_4.CGW_VCU_B_4.VCU_InstPowerDisp_1) << 8;
  IlLeaveCriticalVCU_InstPowerDisp();
  return rc;
}


#endif

/* Handle:  301,Name:                    SLASpdlimit,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint8 IlGetRxSLASpdlimit(void)
{
  vuint8 rc;
  IlEnterCriticalSLASpdlimit();
  rc = ((vuint8) CGW_FCM_2.CGW_FCM_2.SLASpdlimit_0);
  rc |= ((vuint8) CGW_FCM_2.CGW_FCM_2.SLASpdlimit_1) << 5;
  IlLeaveCriticalSLASpdlimit();
  return rc;
}


#endif

/* Handle:  331,Name:              ICM_TotalOdometer,Size: 20,UsedBytes:  3,SingleSignal */
#ifdef IL_ENABLE_RX
vuint32 IlGetRxICM_TotalOdometer(void)
{
  vuint32 rc;
  IlEnterCriticalICM_TotalOdometer();
  rc = ((vuint32) ICM_1.ICM_1.ICM_TotalOdometer_0);
  rc |= ((vuint32) ICM_1.ICM_1.ICM_TotalOdometer_1) << 8;
  rc |= ((vuint32) ICM_1.ICM_1.ICM_TotalOdometer_2) << 16;
  IlLeaveCriticalICM_TotalOdometer();
  return rc;
}


#endif

/* Handle:  333,Name:                BMS_HV_BattVolt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxBMS_HV_BattVolt(void)
{
  vuint16 rc;
  IlEnterCriticalBMS_HV_BattVolt();
  rc = ((vuint16) CGW_BMS_3.CGW_BMS_3.BMS_HV_BattVolt_0);
  rc |= ((vuint16) CGW_BMS_3.CGW_BMS_3.BMS_HV_BattVolt_1) << 2;
  IlLeaveCriticalBMS_HV_BattVolt();
  return rc;
}


#endif

/* Handle:  334,Name:                BMS_HV_BattCurr,Size: 14,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxBMS_HV_BattCurr(void)
{
  vuint16 rc;
  IlEnterCriticalBMS_HV_BattCurr();
  rc = ((vuint16) CGW_BMS_3.CGW_BMS_3.BMS_HV_BattCurr_0);
  rc |= ((vuint16) CGW_BMS_3.CGW_BMS_3.BMS_HV_BattCurr_1) << 8;
  IlLeaveCriticalBMS_HV_BattCurr();
  return rc;
}


#endif

/* Handle:  365,Name:                     VCU_VehSpd,Size: 13,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
vuint16 IlGetRxVCU_VehSpd(void)
{
  vuint16 rc;
  IlEnterCriticalVCU_VehSpd();
  rc = ((vuint16) CGW_VCU_P_5.CGW_VCU_P_5.VCU_VehSpd_0);
  rc |= ((vuint16) CGW_VCU_P_5.CGW_VCU_P_5.VCU_VehSpd_1) << 5;
  IlLeaveCriticalVCU_VehSpd();
  return rc;
}


#endif



/* -----------------------------------------------------------------------------
    &&&~ Set Tx Signal Access 
 ----------------------------------------------------------------------------- */

/* Handle:    4,Name:               IHU_11_FCWSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_FCWSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_FCWSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_FCWSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_FCWSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_FCWSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_FCWSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_FCWSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_FCWSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_FCWSwtSet();
}


#endif

/* Handle:    5,Name:             IHU_11_FCWSnvtySet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_FCWSnvtySet(vuint8 sigData)
{
  vuint8 active = IHU_11_FCWSnvtySetIsActive(sigData);
  IlEnterCriticalIHU_11_FCWSnvtySet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_FCWSnvtySet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSnvtySet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_FCWSnvtySet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSnvtySet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSnvtySet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_FCWSnvtySet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_FCWSnvtySet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_FCWSnvtySet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_FCWSnvtySet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_FCWSnvtySet();
}


#endif

/* Handle:    6,Name:               IHU_11_AEBSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_AEBSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_AEBSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_AEBSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_AEBSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_AEBSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_AEBSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_AEBSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_AEBSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_AEBSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_AEBSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_AEBSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_AEBSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_AEBSwtSet();
}


#endif

/* Handle:    7,Name:   IHU_11_SCF_request_confirmed,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_SCF_request_confirmed(vuint8 sigData)
{
  vuint8 active = IHU_11_SCF_request_confirmedIsActive(sigData);
  IlEnterCriticalIHU_11_SCF_request_confirmed();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_SCF_request_confirmed = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCF_request_confirmed == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_SCF_request_confirmed);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCF_request_confirmed = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCF_request_confirmed != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCF_request_confirmed = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_SCF_request_confirmed] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_SCF_request_confirmed);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_SCF_request_confirmed);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_SCF_request_confirmed();
}


#endif

/* Handle:    8,Name:               IHU_11_SCFSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_SCFSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_SCFSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_SCFSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_SCFSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCFSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_SCFSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCFSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCFSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_SCFSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_SCFSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_SCFSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_SCFSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_SCFSwtSet();
}


#endif

/* Handle:    9,Name:      IHU_11_SLA_TSI_TLI_SwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_SLA_TSI_TLI_SwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_SLA_TSI_TLI_SwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_SLA_TSI_TLI_SwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_SLA_TSI_TLI_SwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_SLA_TSI_TLI_SwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_SLA_TSI_TLI_SwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_SLA_TSI_TLI_SwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_SLA_TSI_TLI_SwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_SLA_TSI_TLI_SwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_SLA_TSI_TLI_SwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_SLA_TSI_TLI_SwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_SLA_TSI_TLI_SwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_SLA_TSI_TLI_SwtSet();
}


#endif

/* Handle:   10,Name:               IHU_11_LKASwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_LKASwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_LKASwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_LKASwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_LKASwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LKASwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_LKASwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LKASwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LKASwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LKASwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_LKASwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_LKASwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_LKASwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_LKASwtSet();
}


#endif

/* Handle:   11,Name:               IHU_11_HMASwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_HMASwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_HMASwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_HMASwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_HMASwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_HMASwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_HMASwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_HMASwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_HMASwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_HMASwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_HMASwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_HMASwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_HMASwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_HMASwtSet();
}


#endif

/* Handle:   12,Name:               IHU_11_IESSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_IESSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_IESSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_IESSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_IESSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_IESSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_IESSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_IESSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_IESSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_IESSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_IESSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_IESSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_IESSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_IESSwtSet();
}


#endif

/* Handle:   13,Name:               IHU_11_DAISwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_DAISwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_DAISwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_DAISwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_DAISwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_DAISwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_DAISwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_DAISwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_DAISwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_DAISwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_DAISwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_DAISwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_DAISwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_DAISwtSet();
}


#endif

/* Handle:   14,Name:               IHU_11_LDPSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_LDPSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_LDPSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_LDPSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_LDPSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDPSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_LDPSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDPSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDPSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDPSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_LDPSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_LDPSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_LDPSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_LDPSwtSet();
}


#endif

/* Handle:   15,Name:           IHU_11_WarnModSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_WarnModSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_WarnModSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_WarnModSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_WarnModSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_WarnModSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_WarnModSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_WarnModSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_WarnModSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_WarnModSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_WarnModSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_WarnModSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_WarnModSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_WarnModSwtSet();
}


#endif

/* Handle:   16,Name:               IHU_11_LDWSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_LDWSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_LDWSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_LDWSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_LDWSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_LDWSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_LDWSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_LDWSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_LDWSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_LDWSwtSet();
}


#endif

/* Handle:   17,Name:               IHU_11_ELKSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_ELKSwtSet(vuint8 sigData)
{
  vuint8 active = IHU_11_ELKSwtSetIsActive(sigData);
  IlEnterCriticalIHU_11_ELKSwtSet();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_ELKSwtSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_ELKSwtSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_ELKSwtSet);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_ELKSwtSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_ELKSwtSet != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_ELKSwtSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_ELKSwtSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_ELKSwtSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_ELKSwtSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_ELKSwtSet();
}


#endif

/* Handle:   18,Name:    IHU_11_LDWLDPSnvtySetLDWLDP,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_11_LDWLDPSnvtySetLDWLDP(vuint8 sigData)
{
  vuint8 active = IHU_11_LDWLDPSnvtySetLDWLDPIsActive(sigData);
  IlEnterCriticalIHU_11_LDWLDPSnvtySetLDWLDP();
  IHU_ADAS_11.IHU_ADAS_11.IHU_11_LDWLDPSnvtySetLDWLDP = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWLDPSnvtySetLDWLDP == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_11_LDWLDPSnvtySetLDWLDP);
        }
      }
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWLDPSnvtySetLDWLDP = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWLDPSnvtySetLDWLDP != 0)
    {
      ilIfActiveFlags.IHU_ADAS_11.IHU_11_LDWLDPSnvtySetLDWLDP = 0;
      ilTxNSendCounter[IlTxSigHndIHU_11_LDWLDPSnvtySetLDWLDP] = IlGetTxRepetitionCounter(IlTxSigHndIHU_11_LDWLDPSnvtySetLDWLDP);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_ADAS_11) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_11_LDWLDPSnvtySetLDWLDP);
        }
      }
    }
  }
  IlLeaveCriticalIHU_11_LDWLDPSnvtySetLDWLDP();
}


#endif

/* Handle:   19,Name: IHU_AmbientLightBrightnessAdjust_Front,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightBrightnessAdjust_Front(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightBrightnessAdjust_FrontIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightBrightnessAdjust_Front();
  IHU_18.IHU_18.IHU_AmbientLightBrightnessAdjust_Front = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Front == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Front);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Front = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Front != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Front = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightBrightnessAdjust_Front] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Front);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Front);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightBrightnessAdjust_Front();
}


#endif

/* Handle:   20,Name: IHU_AmbientLightBrightnessAdjust_Roof,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightBrightnessAdjust_Roof(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightBrightnessAdjust_RoofIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightBrightnessAdjust_Roof();
  IHU_18.IHU_18.IHU_AmbientLightBrightnessAdjust_Roof = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Roof == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Roof);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Roof = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Roof != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Roof = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightBrightnessAdjust_Roof] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Roof);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Roof);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightBrightnessAdjust_Roof();
}


#endif

/* Handle:   21,Name: IHU_AmbientLightBrightnessAdjust_Rear,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightBrightnessAdjust_Rear(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightBrightnessAdjust_RearIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightBrightnessAdjust_Rear();
  IHU_18.IHU_18.IHU_AmbientLightBrightnessAdjust_Rear = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Rear == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Rear);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Rear = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Rear != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBrightnessAdjust_Rear = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightBrightnessAdjust_Rear] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Rear);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightBrightnessAdjust_Rear);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightBrightnessAdjust_Rear();
}


#endif

/* Handle:   22,Name: IHU_AmbientLightColorSet_Front,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightColorSet_Front(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightColorSet_FrontIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightColorSet_Front();
  IHU_18.IHU_18.IHU_AmbientLightColorSet_Front = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Front == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightColorSet_Front);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Front = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Front != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Front = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightColorSet_Front] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightColorSet_Front);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightColorSet_Front);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightColorSet_Front();
}


#endif

/* Handle:   23,Name:  IHU_AmbientLightColorSet_Roof,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightColorSet_Roof(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightColorSet_RoofIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightColorSet_Roof();
  IHU_18.IHU_18.IHU_AmbientLightColorSet_Roof = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Roof == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightColorSet_Roof);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Roof = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Roof != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Roof = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightColorSet_Roof] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightColorSet_Roof);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightColorSet_Roof);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightColorSet_Roof();
}


#endif

/* Handle:   24,Name:  IHU_AmbientLightColorSet_Rear,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightColorSet_Rear(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightColorSet_RearIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightColorSet_Rear();
  IHU_18.IHU_18.IHU_AmbientLightColorSet_Rear = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Rear == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightColorSet_Rear);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Rear = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Rear != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightColorSet_Rear = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightColorSet_Rear] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightColorSet_Rear);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightColorSet_Rear);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightColorSet_Rear();
}


#endif

/* Handle:   25,Name: IHU_RegionColorSynchronizationReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RegionColorSynchronizationReq(vuint8 sigData)
{
  vuint8 active = IHU_RegionColorSynchronizationReqIsActive(sigData);
  IlEnterCriticalIHU_RegionColorSynchronizationReq();
  IHU_18.IHU_18.IHU_RegionColorSynchronizationReq = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_RegionColorSynchronizationReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_RegionColorSynchronizationReq);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_RegionColorSynchronizationReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_RegionColorSynchronizationReq != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_RegionColorSynchronizationReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_RegionColorSynchronizationReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_RegionColorSynchronizationReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_RegionColorSynchronizationReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_RegionColorSynchronizationReq();
}


#endif

/* Handle:   26,Name:        IHU_AmbientLightModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightModeSet(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightModeSetIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightModeSet();
  IHU_18.IHU_18.IHU_AmbientLightModeSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightModeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightModeSet);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightModeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightModeSet != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightModeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightModeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightModeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightModeSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightModeSet();
}


#endif

/* Handle:   27,Name:    IHU_AmbientLightEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightEnableSwSts();
  IHU_18.IHU_18.IHU_AmbientLightEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightEnableSwSts();
}


#endif

/* Handle:   28,Name: IHU_AmbientLightAreaDisplayEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightAreaDisplayEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightAreaDisplayEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightAreaDisplayEnableSwSts();
  IHU_18.IHU_18.IHU_AmbientLightAreaDisplayEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightAreaDisplayEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightAreaDisplayEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightAreaDisplayEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightAreaDisplayEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightAreaDisplayEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightAreaDisplayEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightAreaDisplayEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightAreaDisplayEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightAreaDisplayEnableSwSts();
}


#endif

/* Handle:   29,Name: IHU_AmbientLightBreatheEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightBreatheEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightBreatheEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightBreatheEnableSwSts();
  IHU_18.IHU_18.IHU_AmbientLightBreatheEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightBreatheEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightBreatheEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightBreatheEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightBreatheEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightBreatheEnableSwSts();
}


#endif

/* Handle:   30,Name: IHU_AmbientLightBreatheWithVehSpdEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AmbientLightBreatheWithVehSpdEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AmbientLightBreatheWithVehSpdEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AmbientLightBreatheWithVehSpdEnableSwSts();
  IHU_18.IHU_18.IHU_AmbientLightBreatheWithVehSpdEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheWithVehSpdEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AmbientLightBreatheWithVehSpdEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheWithVehSpdEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheWithVehSpdEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_18.IHU_AmbientLightBreatheWithVehSpdEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AmbientLightBreatheWithVehSpdEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AmbientLightBreatheWithVehSpdEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_18) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_18) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AmbientLightBreatheWithVehSpdEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AmbientLightBreatheWithVehSpdEnableSwSts();
}


#endif

/* Handle:   31,Name:      IHU_Chg_SOC_LimitPointSet,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_Chg_SOC_LimitPointSet(vuint8 sigData)
{
  vuint8 active = IHU_Chg_SOC_LimitPointSetIsActive(sigData);
  IlEnterCriticalIHU_Chg_SOC_LimitPointSet();
  IHU_13.IHU_13.IHU_Chg_SOC_LimitPointSet = ((vuint8) (sigData & (vuint8) 0x3F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_Chg_SOC_LimitPointSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_Chg_SOC_LimitPointSet);
      }
      ilIfActiveFlags.IHU_13.IHU_Chg_SOC_LimitPointSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_Chg_SOC_LimitPointSet != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_Chg_SOC_LimitPointSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_Chg_SOC_LimitPointSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_Chg_SOC_LimitPointSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_Chg_SOC_LimitPointSet);
      }
    }
  }
  IlLeaveCriticalIHU_Chg_SOC_LimitPointSet();
}


#endif

/* Handle:   32,Name:                 IHU_ChgModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ChgModeSet(vuint8 sigData)
{
  vuint8 active = IHU_ChgModeSetIsActive(sigData);
  IlEnterCriticalIHU_ChgModeSet();
  IHU_13.IHU_13.IHU_ChgModeSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_ChgModeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_ChgModeSet);
      }
      ilIfActiveFlags.IHU_13.IHU_ChgModeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_ChgModeSet != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_ChgModeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ChgModeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ChgModeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_ChgModeSet);
      }
    }
  }
  IlLeaveCriticalIHU_ChgModeSet();
}


#endif

/* Handle:   33,Name:   IHU_BookChgStartTimeSet_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BookChgStartTimeSet_Hour(vuint8 sigData)
{
  vuint8 active = IHU_BookChgStartTimeSet_HourIsActive(sigData);
  IlEnterCriticalIHU_BookChgStartTimeSet_Hour();
  IHU_13.IHU_13.IHU_BookChgStartTimeSet_Hour = ((vuint8) (sigData & (vuint8) 0x1F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Hour == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_BookChgStartTimeSet_Hour);
      }
      ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Hour = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Hour != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Hour = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BookChgStartTimeSet_Hour] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BookChgStartTimeSet_Hour);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_BookChgStartTimeSet_Hour);
      }
    }
  }
  IlLeaveCriticalIHU_BookChgStartTimeSet_Hour();
}


#endif

/* Handle:   34,Name: IHU_BookChgStartTimeSet_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BookChgStartTimeSet_Minute(vuint8 sigData)
{
  vuint8 active = IHU_BookChgStartTimeSet_MinuteIsActive(sigData);
  IlEnterCriticalIHU_BookChgStartTimeSet_Minute();
  IHU_13.IHU_13.IHU_BookChgStartTimeSet_Minute = ((vuint8) (sigData & (vuint8) 0x3F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Minute == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_BookChgStartTimeSet_Minute);
      }
      ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Minute = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Minute != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_BookChgStartTimeSet_Minute = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BookChgStartTimeSet_Minute] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BookChgStartTimeSet_Minute);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_BookChgStartTimeSet_Minute);
      }
    }
  }
  IlLeaveCriticalIHU_BookChgStartTimeSet_Minute();
}


#endif

/* Handle:   35,Name:          IHU_BookChgTimeSet_VD,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BookChgTimeSet_VD(vuint8 sigData)
{
  vuint8 active = IHU_BookChgTimeSet_VDIsActive(sigData);
  IlEnterCriticalIHU_BookChgTimeSet_VD();
  IHU_13.IHU_13.IHU_BookChgTimeSet_VD = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgTimeSet_VD == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_BookChgTimeSet_VD);
      }
      ilIfActiveFlags.IHU_13.IHU_BookChgTimeSet_VD = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgTimeSet_VD != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_BookChgTimeSet_VD = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BookChgTimeSet_VD] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BookChgTimeSet_VD);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_BookChgTimeSet_VD);
      }
    }
  }
  IlLeaveCriticalIHU_BookChgTimeSet_VD();
}


#endif

/* Handle:   36,Name:    IHU_BookChgStopTimeSet_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BookChgStopTimeSet_Hour(vuint8 sigData)
{
  vuint8 active = IHU_BookChgStopTimeSet_HourIsActive(sigData);
  IlEnterCriticalIHU_BookChgStopTimeSet_Hour();
  IHU_13.IHU_13.IHU_BookChgStopTimeSet_Hour = ((vuint8) (sigData & (vuint8) 0x1F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgStopTimeSet_Hour == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_BookChgStopTimeSet_Hour);
      }
      ilIfActiveFlags.IHU_13.IHU_BookChgStopTimeSet_Hour = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookChgStopTimeSet_Hour != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_BookChgStopTimeSet_Hour = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BookChgStopTimeSet_Hour] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BookChgStopTimeSet_Hour);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_BookChgStopTimeSet_Hour);
      }
    }
  }
  IlLeaveCriticalIHU_BookChgStopTimeSet_Hour();
}


#endif

/* Handle:   37,Name:  IHU_BookchgStopTimeSet_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BookchgStopTimeSet_Minute(vuint8 sigData)
{
  vuint8 active = IHU_BookchgStopTimeSet_MinuteIsActive(sigData);
  IlEnterCriticalIHU_BookchgStopTimeSet_Minute();
  IHU_13.IHU_13.IHU_BookchgStopTimeSet_Minute = ((vuint8) (sigData & (vuint8) 0x3F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookchgStopTimeSet_Minute == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_BookchgStopTimeSet_Minute);
      }
      ilIfActiveFlags.IHU_13.IHU_BookchgStopTimeSet_Minute = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_13.IHU_BookchgStopTimeSet_Minute != 0)
    {
      ilIfActiveFlags.IHU_13.IHU_BookchgStopTimeSet_Minute = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BookchgStopTimeSet_Minute] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BookchgStopTimeSet_Minute);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_13) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_BookchgStopTimeSet_Minute);
      }
    }
  }
  IlLeaveCriticalIHU_BookchgStopTimeSet_Minute();
}


#endif

/* Handle:   53,Name:  IHU_RearWarnSystemEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RearWarnSystemEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_RearWarnSystemEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_RearWarnSystemEnableSwSts();
  IHU_10.IHU_10.IHU_RearWarnSystemEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_10.IHU_RearWarnSystemEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_10) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_RearWarnSystemEnableSwSts);
      }
      ilIfActiveFlags.IHU_10.IHU_RearWarnSystemEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_10.IHU_RearWarnSystemEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_10.IHU_RearWarnSystemEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_RearWarnSystemEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_RearWarnSystemEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_10) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_RearWarnSystemEnableSwSts);
      }
    }
  }
  IlLeaveCriticalIHU_RearWarnSystemEnableSwSts();
}


#endif

/* Handle:   54,Name:              IHU_ICM_ThemelSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ICM_ThemelSet(vuint8 sigData)
{
  vuint8 active = IHU_ICM_ThemelSetIsActive(sigData);
  IlEnterCriticalIHU_ICM_ThemelSet();
  IHU_8.IHU_8.IHU_ICM_ThemelSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_ThemelSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ICM_ThemelSet);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_ICM_ThemelSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_ThemelSet != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_ICM_ThemelSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ICM_ThemelSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ICM_ThemelSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ICM_ThemelSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ICM_ThemelSet();
}


#endif

/* Handle:   55,Name:              IHU_ICM_ScreenReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ICM_ScreenReq(vuint8 sigData)
{
  vuint8 active = IHU_ICM_ScreenReqIsActive(sigData);
  IlEnterCriticalIHU_ICM_ScreenReq();
  IHU_8.IHU_8.IHU_ICM_ScreenReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_ScreenReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ICM_ScreenReq);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_ICM_ScreenReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_ScreenReq != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_ICM_ScreenReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ICM_ScreenReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ICM_ScreenReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ICM_ScreenReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ICM_ScreenReq();
}


#endif

/* Handle:   56,Name:            IHU_OverSpdValueSet,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_OverSpdValueSet(vuint8 sigData)
{
  vuint8 active = IHU_OverSpdValueSetIsActive(sigData);
  IlEnterCriticalIHU_OverSpdValueSet();
  IHU_8.IHU_8.IHU_OverSpdValueSet = ((vuint8) (sigData & (vuint8) 0x3F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_OverSpdValueSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_OverSpdValueSet);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_OverSpdValueSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_OverSpdValueSet != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_OverSpdValueSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_OverSpdValueSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_OverSpdValueSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_OverSpdValueSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_OverSpdValueSet();
}


#endif

/* Handle:   57,Name:      IHU_ICM_Menu_OK_ButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ICM_Menu_OK_ButtonSts(vuint8 sigData)
{
  vuint8 active = IHU_ICM_Menu_OK_ButtonStsIsActive(sigData);
  IlEnterCriticalIHU_ICM_Menu_OK_ButtonSts();
  IHU_8.IHU_8.IHU_ICM_Menu_OK_ButtonSts = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_Menu_OK_ButtonSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ICM_Menu_OK_ButtonSts);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_ICM_Menu_OK_ButtonSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_Menu_OK_ButtonSts != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_ICM_Menu_OK_ButtonSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ICM_Menu_OK_ButtonSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ICM_Menu_OK_ButtonSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ICM_Menu_OK_ButtonSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ICM_Menu_OK_ButtonSts();
}


#endif

/* Handle:   58,Name:          IHU_ICM_ModeButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ICM_ModeButtonSts(vuint8 sigData)
{
  vuint8 active = IHU_ICM_ModeButtonStsIsActive(sigData);
  IlEnterCriticalIHU_ICM_ModeButtonSts();
  IHU_8.IHU_8.IHU_ICM_ModeButtonSts = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_ModeButtonSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ICM_ModeButtonSts);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_ICM_ModeButtonSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_ModeButtonSts != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_ICM_ModeButtonSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ICM_ModeButtonSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ICM_ModeButtonSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ICM_ModeButtonSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ICM_ModeButtonSts();
}


#endif

/* Handle:   59,Name:     IHU_FatigureDrivingTimeSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FatigureDrivingTimeSet(vuint8 sigData)
{
  vuint8 active = IHU_FatigureDrivingTimeSetIsActive(sigData);
  IlEnterCriticalIHU_FatigureDrivingTimeSet();
  IHU_8.IHU_8.IHU_FatigureDrivingTimeSet = ((vuint8) (sigData & (vuint8) 0x0F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_FatigureDrivingTimeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_FatigureDrivingTimeSet);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_FatigureDrivingTimeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_FatigureDrivingTimeSet != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_FatigureDrivingTimeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FatigureDrivingTimeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FatigureDrivingTimeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_FatigureDrivingTimeSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_FatigureDrivingTimeSet();
}


#endif

/* Handle:   60,Name:            IHU_ICM_UpButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ICM_UpButtonSts(vuint8 sigData)
{
  vuint8 active = IHU_ICM_UpButtonStsIsActive(sigData);
  IlEnterCriticalIHU_ICM_UpButtonSts();
  IHU_8.IHU_8.IHU_ICM_UpButtonSts = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_UpButtonSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ICM_UpButtonSts);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_ICM_UpButtonSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_UpButtonSts != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_ICM_UpButtonSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ICM_UpButtonSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ICM_UpButtonSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ICM_UpButtonSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ICM_UpButtonSts();
}


#endif

/* Handle:   61,Name:          IHU_ICM_DownButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ICM_DownButtonSts(vuint8 sigData)
{
  vuint8 active = IHU_ICM_DownButtonStsIsActive(sigData);
  IlEnterCriticalIHU_ICM_DownButtonSts();
  IHU_8.IHU_8.IHU_ICM_DownButtonSts = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_DownButtonSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ICM_DownButtonSts);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_ICM_DownButtonSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_ICM_DownButtonSts != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_ICM_DownButtonSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ICM_DownButtonSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ICM_DownButtonSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ICM_DownButtonSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ICM_DownButtonSts();
}


#endif

/* Handle:   62,Name:         IHU__ICM_LeftButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU__ICM_LeftButtonSts(vuint8 sigData)
{
  vuint8 active = IHU__ICM_LeftButtonStsIsActive(sigData);
  IlEnterCriticalIHU__ICM_LeftButtonSts();
  IHU_8.IHU_8.IHU__ICM_LeftButtonSts = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU__ICM_LeftButtonSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU__ICM_LeftButtonSts);
        }
      }
      ilIfActiveFlags.IHU_8.IHU__ICM_LeftButtonSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU__ICM_LeftButtonSts != 0)
    {
      ilIfActiveFlags.IHU_8.IHU__ICM_LeftButtonSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU__ICM_LeftButtonSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU__ICM_LeftButtonSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU__ICM_LeftButtonSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU__ICM_LeftButtonSts();
}


#endif

/* Handle:   63,Name:        IHU__ICM_RightButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU__ICM_RightButtonSts(vuint8 sigData)
{
  vuint8 active = IHU__ICM_RightButtonStsIsActive(sigData);
  IlEnterCriticalIHU__ICM_RightButtonSts();
  IHU_8.IHU_8.IHU__ICM_RightButtonSts = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU__ICM_RightButtonSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU__ICM_RightButtonSts);
        }
      }
      ilIfActiveFlags.IHU_8.IHU__ICM_RightButtonSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU__ICM_RightButtonSts != 0)
    {
      ilIfActiveFlags.IHU_8.IHU__ICM_RightButtonSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU__ICM_RightButtonSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU__ICM_RightButtonSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU__ICM_RightButtonSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU__ICM_RightButtonSts();
}


#endif

/* Handle:   64,Name:    IHU_BrightnessAdjustSet_ICM,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BrightnessAdjustSet_ICM(vuint8 sigData)
{
  vuint8 active = IHU_BrightnessAdjustSet_ICMIsActive(sigData);
  IlEnterCriticalIHU_BrightnessAdjustSet_ICM();
  IHU_8.IHU_8.IHU_BrightnessAdjustSet_ICM = ((vuint8) (sigData & (vuint8) 0x0F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_BrightnessAdjustSet_ICM == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_BrightnessAdjustSet_ICM);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_BrightnessAdjustSet_ICM = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_BrightnessAdjustSet_ICM != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_BrightnessAdjustSet_ICM = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BrightnessAdjustSet_ICM] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BrightnessAdjustSet_ICM);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_BrightnessAdjustSet_ICM);
        }
      }
    }
  }
  IlLeaveCriticalIHU_BrightnessAdjustSet_ICM();
}


#endif

/* Handle:   65,Name:          IHU_DVR_QuickCameraSw,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_DVR_QuickCameraSw(vuint8 sigData)
{
  vuint8 active = IHU_DVR_QuickCameraSwIsActive(sigData);
  IlEnterCriticalIHU_DVR_QuickCameraSw();
  IHU_8.IHU_8.IHU_DVR_QuickCameraSw = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_8.IHU_DVR_QuickCameraSw == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_DVR_QuickCameraSw);
        }
      }
      ilIfActiveFlags.IHU_8.IHU_DVR_QuickCameraSw = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_8.IHU_DVR_QuickCameraSw != 0)
    {
      ilIfActiveFlags.IHU_8.IHU_DVR_QuickCameraSw = 0;
      ilTxNSendCounter[IlTxSigHndIHU_DVR_QuickCameraSw] = IlGetTxRepetitionCounter(IlTxSigHndIHU_DVR_QuickCameraSw);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_8) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_8) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_DVR_QuickCameraSw);
        }
      }
    }
  }
  IlLeaveCriticalIHU_DVR_QuickCameraSw();
}


#endif

/* Handle:   66,Name:     IHU_SeatHeatLevelSetReq_RL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatHeatLevelSetReq_RL(vuint8 sigData)
{
  vuint8 active = IHU_SeatHeatLevelSetReq_RLIsActive(sigData);
  IlEnterCriticalIHU_SeatHeatLevelSetReq_RL();
  IHU_7.IHU_7.IHU_SeatHeatLevelSetReq_RL = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RL == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatHeatLevelSetReq_RL);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RL = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RL != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RL = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatHeatLevelSetReq_RL] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatHeatLevelSetReq_RL);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatHeatLevelSetReq_RL);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatHeatLevelSetReq_RL();
}


#endif

/* Handle:   67,Name:     IHU_SeatHeatLevelSetReq_RR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatHeatLevelSetReq_RR(vuint8 sigData)
{
  vuint8 active = IHU_SeatHeatLevelSetReq_RRIsActive(sigData);
  IlEnterCriticalIHU_SeatHeatLevelSetReq_RR();
  IHU_7.IHU_7.IHU_SeatHeatLevelSetReq_RR = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RR == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatHeatLevelSetReq_RR);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RR = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RR != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatHeatLevelSetReq_RR = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatHeatLevelSetReq_RR] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatHeatLevelSetReq_RR);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatHeatLevelSetReq_RR);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatHeatLevelSetReq_RR();
}


#endif

/* Handle:   68,Name: IHU_WirelessChargingEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_WirelessChargingEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_WirelessChargingEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_WirelessChargingEnableSwSts();
  IHU_7.IHU_7.IHU_WirelessChargingEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_WirelessChargingEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_WirelessChargingEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_WirelessChargingEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_WirelessChargingEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_WirelessChargingEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_WirelessChargingEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_WirelessChargingEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_WirelessChargingEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_WirelessChargingEnableSwSts();
}


#endif

/* Handle:   69,Name:             IHU_AVAS_VolumeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AVAS_VolumeSet(vuint8 sigData)
{
  vuint8 active = IHU_AVAS_VolumeSetIsActive(sigData);
  IlEnterCriticalIHU_AVAS_VolumeSet();
  IHU_7.IHU_7.IHU_AVAS_VolumeSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_AVAS_VolumeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AVAS_VolumeSet);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_AVAS_VolumeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_AVAS_VolumeSet != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_AVAS_VolumeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AVAS_VolumeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AVAS_VolumeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AVAS_VolumeSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AVAS_VolumeSet();
}


#endif

/* Handle:   70,Name:        IHU_AVAS_AudioSourceSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AVAS_AudioSourceSet(vuint8 sigData)
{
  vuint8 active = IHU_AVAS_AudioSourceSetIsActive(sigData);
  IlEnterCriticalIHU_AVAS_AudioSourceSet();
  IHU_7.IHU_7.IHU_AVAS_AudioSourceSet = ((vuint8) (sigData & (vuint8) 0x0F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_AVAS_AudioSourceSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AVAS_AudioSourceSet);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_AVAS_AudioSourceSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_AVAS_AudioSourceSet != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_AVAS_AudioSourceSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AVAS_AudioSourceSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AVAS_AudioSourceSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AVAS_AudioSourceSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AVAS_AudioSourceSet();
}


#endif

/* Handle:   71,Name:           IHU_AVAS_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AVAS_EnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AVAS_EnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AVAS_EnableSwSts();
  IHU_7.IHU_7.IHU_AVAS_EnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_AVAS_EnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AVAS_EnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_AVAS_EnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_AVAS_EnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_AVAS_EnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AVAS_EnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AVAS_EnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AVAS_EnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AVAS_EnableSwSts();
}


#endif

/* Handle:   72,Name:      IHU_SeatMemoryEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatMemoryEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_SeatMemoryEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_SeatMemoryEnableSwSts();
  IHU_7.IHU_7.IHU_SeatMemoryEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatMemoryEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatMemoryEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatMemoryEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatMemoryEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatMemoryEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatMemoryEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatMemoryEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatMemoryEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatMemoryEnableSwSts();
}


#endif

/* Handle:   73,Name:       IHU_SeatPositionStoreReq,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatPositionStoreReq(vuint8 sigData)
{
  vuint8 active = IHU_SeatPositionStoreReqIsActive(sigData);
  IlEnterCriticalIHU_SeatPositionStoreReq();
  IHU_7.IHU_7.IHU_SeatPositionStoreReq = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatPositionStoreReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatPositionStoreReq);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatPositionStoreReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatPositionStoreReq != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatPositionStoreReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatPositionStoreReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatPositionStoreReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatPositionStoreReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatPositionStoreReq();
}


#endif

/* Handle:   74,Name:     IHU_SeatPositionCalloutReq,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatPositionCalloutReq(vuint8 sigData)
{
  vuint8 active = IHU_SeatPositionCalloutReqIsActive(sigData);
  IlEnterCriticalIHU_SeatPositionCalloutReq();
  IHU_7.IHU_7.IHU_SeatPositionCalloutReq = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatPositionCalloutReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatPositionCalloutReq);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatPositionCalloutReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatPositionCalloutReq != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatPositionCalloutReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatPositionCalloutReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatPositionCalloutReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatPositionCalloutReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatPositionCalloutReq();
}


#endif

/* Handle:   75,Name: IHU_SeatHeatVentLevelSetReq_Drive,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatHeatVentLevelSetReq_Drive(vuint8 sigData)
{
  vuint8 active = IHU_SeatHeatVentLevelSetReq_DriveIsActive(sigData);
  IlEnterCriticalIHU_SeatHeatVentLevelSetReq_Drive();
  IHU_7.IHU_7.IHU_SeatHeatVentLevelSetReq_Drive = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Drive == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatHeatVentLevelSetReq_Drive);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Drive = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Drive != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Drive = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatHeatVentLevelSetReq_Drive] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatHeatVentLevelSetReq_Drive);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatHeatVentLevelSetReq_Drive);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatHeatVentLevelSetReq_Drive();
}


#endif

/* Handle:   76,Name: IHU_SeatHeatVentLevelSetReq_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SeatHeatVentLevelSetReq_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_SeatHeatVentLevelSetReq_PsngrIsActive(sigData);
  IlEnterCriticalIHU_SeatHeatVentLevelSetReq_Psngr();
  IHU_7.IHU_7.IHU_SeatHeatVentLevelSetReq_Psngr = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SeatHeatVentLevelSetReq_Psngr);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Psngr != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_SeatHeatVentLevelSetReq_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SeatHeatVentLevelSetReq_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SeatHeatVentLevelSetReq_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SeatHeatVentLevelSetReq_Psngr);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SeatHeatVentLevelSetReq_Psngr();
}


#endif

/* Handle:   77,Name:     IHU_FaceRecSeatEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FaceRecSeatEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_FaceRecSeatEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_FaceRecSeatEnableSwSts();
  IHU_7.IHU_7.IHU_FaceRecSeatEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_FaceRecSeatEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_FaceRecSeatEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_FaceRecSeatEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_FaceRecSeatEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_FaceRecSeatEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FaceRecSeatEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FaceRecSeatEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_FaceRecSeatEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_FaceRecSeatEnableSwSts();
}


#endif

/* Handle:   78,Name: IHU_FaceRecMirrorInclineEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FaceRecMirrorInclineEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_FaceRecMirrorInclineEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_FaceRecMirrorInclineEnableSwSts();
  IHU_7.IHU_7.IHU_FaceRecMirrorInclineEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_FaceRecMirrorInclineEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_FaceRecMirrorInclineEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_FaceRecMirrorInclineEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_FaceRecMirrorInclineEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_FaceRecMirrorInclineEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FaceRecMirrorInclineEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FaceRecMirrorInclineEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_FaceRecMirrorInclineEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_FaceRecMirrorInclineEnableSwSts();
}


#endif

/* Handle:   79,Name: IHU_FaceRecAuthenticationResult,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FaceRecAuthenticationResult(vuint8 sigData)
{
  vuint8 active = IHU_FaceRecAuthenticationResultIsActive(sigData);
  IlEnterCriticalIHU_FaceRecAuthenticationResult();
  IHU_7.IHU_7.IHU_FaceRecAuthenticationResult = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_FaceRecAuthenticationResult == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_FaceRecAuthenticationResult);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_FaceRecAuthenticationResult = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_FaceRecAuthenticationResult != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_FaceRecAuthenticationResult = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FaceRecAuthenticationResult] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FaceRecAuthenticationResult);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_FaceRecAuthenticationResult);
        }
      }
    }
  }
  IlLeaveCriticalIHU_FaceRecAuthenticationResult();
}


#endif

/* Handle:   80,Name:          IHU_AccountDeletedReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AccountDeletedReq(vuint8 sigData)
{
  vuint8 active = IHU_AccountDeletedReqIsActive(sigData);
  IlEnterCriticalIHU_AccountDeletedReq();
  IHU_7.IHU_7.IHU_AccountDeletedReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_AccountDeletedReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_AccountDeletedReq);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_AccountDeletedReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_AccountDeletedReq != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_AccountDeletedReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AccountDeletedReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AccountDeletedReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_AccountDeletedReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_AccountDeletedReq();
}


#endif

/* Handle:   81,Name: IHU_EntryExitSeatCtrlEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_EntryExitSeatCtrlEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_EntryExitSeatCtrlEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_EntryExitSeatCtrlEnableSwSts();
  IHU_7.IHU_7.IHU_EntryExitSeatCtrlEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_7.IHU_EntryExitSeatCtrlEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_EntryExitSeatCtrlEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_7.IHU_EntryExitSeatCtrlEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_7.IHU_EntryExitSeatCtrlEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_7.IHU_EntryExitSeatCtrlEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_EntryExitSeatCtrlEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_EntryExitSeatCtrlEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_7) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_7) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_EntryExitSeatCtrlEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_EntryExitSeatCtrlEnableSwSts();
}


#endif

/* Handle:   82,Name:                 IHU_AUTO_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AUTO_SwSts(vuint8 sigData)
{
  if(IHU_AUTO_SwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_AUTO_SwSts();
    IHU_6.IHU_6.IHU_AUTO_SwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_AUTO_SwSts);
    IlLeaveCriticalIHU_AUTO_SwSts();
  }
}


#endif

/* Handle:   83,Name:                   IHU_AC_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AC_SwSts(vuint8 sigData)
{
  if(IHU_AC_SwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_AC_SwSts();
    IHU_6.IHU_6.IHU_AC_SwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_AC_SwSts);
    IlLeaveCriticalIHU_AC_SwSts();
  }
}


#endif

/* Handle:   84,Name:                  IHU_PTC_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_PTC_SwSts(vuint8 sigData)
{
  if(IHU_PTC_SwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_PTC_SwSts();
    IHU_6.IHU_6.IHU_PTC_SwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_PTC_SwSts);
    IlLeaveCriticalIHU_PTC_SwSts();
  }
}


#endif

/* Handle:   85,Name:                  IHU_OFF_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_OFF_SwSts(vuint8 sigData)
{
  if(IHU_OFF_SwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_OFF_SwSts();
    IHU_6.IHU_6.IHU_OFF_SwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_OFF_SwSts);
    IlLeaveCriticalIHU_OFF_SwSts();
  }
}


#endif

/* Handle:   86,Name:              IHU_RearDfstSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RearDfstSwSts(vuint8 sigData)
{
  if(IHU_RearDfstSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_RearDfstSwSts();
    IHU_6.IHU_6.IHU_RearDfstSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_RearDfstSwSts);
    IlLeaveCriticalIHU_RearDfstSwSts();
  }
}


#endif

/* Handle:   87,Name:         IHU_CirculationModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_CirculationModeSet(vuint8 sigData)
{
  if(IHU_CirculationModeSetValueChanged(sigData))
  {
    IlEnterCriticalIHU_CirculationModeSet();
    IHU_6.IHU_6.IHU_CirculationModeSet = ((vuint8) (sigData & (vuint8) 0x03));
    IlSecureEvent(IlTxSigHndIHU_CirculationModeSet);
    IlLeaveCriticalIHU_CirculationModeSet();
  }
}


#endif

/* Handle:   88,Name:              IHU_AutoDfstSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AutoDfstSwSts(vuint8 sigData)
{
  if(IHU_AutoDfstSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_AutoDfstSwSts();
    IHU_6.IHU_6.IHU_AutoDfstSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_AutoDfstSwSts);
    IlLeaveCriticalIHU_AutoDfstSwSts();
  }
}


#endif

/* Handle:   89,Name:                IHU_BlowModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BlowModeSet(vuint8 sigData)
{
  if(IHU_BlowModeSetValueChanged(sigData))
  {
    IlEnterCriticalIHU_BlowModeSet();
    IHU_6.IHU_6.IHU_BlowModeSet = ((vuint8) (sigData & (vuint8) 0x07));
    IlSecureEvent(IlTxSigHndIHU_BlowModeSet);
    IlLeaveCriticalIHU_BlowModeSet();
  }
}


#endif

/* Handle:   90,Name:             IHU_TempSet_Driver,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_TempSet_Driver(vuint8 sigData)
{
  if(IHU_TempSet_DriverValueChanged(sigData))
  {
    IlEnterCriticalIHU_TempSet_Driver();
    IHU_6.IHU_6.IHU_TempSet_Driver = ((vuint8) (sigData & (vuint8) 0x1F));
    IlSecureEvent(IlTxSigHndIHU_TempSet_Driver);
    IlLeaveCriticalIHU_TempSet_Driver();
  }
}


#endif

/* Handle:   91,Name:             IHU_BlowerlevelSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BlowerlevelSet(vuint8 sigData)
{
  if(IHU_BlowerlevelSetValueChanged(sigData))
  {
    IlEnterCriticalIHU_BlowerlevelSet();
    IHU_6.IHU_6.IHU_BlowerlevelSet = ((vuint8) (sigData & (vuint8) 0x0F));
    IlSecureEvent(IlTxSigHndIHU_BlowerlevelSet);
    IlLeaveCriticalIHU_BlowerlevelSet();
  }
}


#endif

/* Handle:   92,Name:        IHU_AnionGeneratorSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AnionGeneratorSwSts(vuint8 sigData)
{
  if(IHU_AnionGeneratorSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_AnionGeneratorSwSts();
    IHU_6.IHU_6.IHU_AnionGeneratorSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_AnionGeneratorSwSts);
    IlLeaveCriticalIHU_AnionGeneratorSwSts();
  }
}


#endif

/* Handle:   93,Name:               IHU_AQS_EnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AQS_EnablaSw(vuint8 sigData)
{
  if(IHU_AQS_EnablaSwValueChanged(sigData))
  {
    IlEnterCriticalIHU_AQS_EnablaSw();
    IHU_6.IHU_6.IHU_AQS_EnablaSw = ((vuint8) (sigData & (vuint8) 0x03));
    IlSecureEvent(IlTxSigHndIHU_AQS_EnablaSw);
    IlLeaveCriticalIHU_AQS_EnablaSw();
  }
}


#endif

/* Handle:   94,Name:           IHU_AutoBlowEnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AutoBlowEnablaSw(vuint8 sigData)
{
  if(IHU_AutoBlowEnablaSwValueChanged(sigData))
  {
    IlEnterCriticalIHU_AutoBlowEnablaSw();
    IHU_6.IHU_6.IHU_AutoBlowEnablaSw = ((vuint8) (sigData & (vuint8) 0x03));
    IlSecureEvent(IlTxSigHndIHU_AutoBlowEnablaSw);
    IlLeaveCriticalIHU_AutoBlowEnablaSw();
  }
}


#endif

/* Handle:   95,Name:      IHU_AC_MemoryModeEnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AC_MemoryModeEnablaSw(vuint8 sigData)
{
  if(IHU_AC_MemoryModeEnablaSwValueChanged(sigData))
  {
    IlEnterCriticalIHU_AC_MemoryModeEnablaSw();
    IHU_6.IHU_6.IHU_AC_MemoryModeEnablaSw = ((vuint8) (sigData & (vuint8) 0x03));
    IlSecureEvent(IlTxSigHndIHU_AC_MemoryModeEnablaSw);
    IlLeaveCriticalIHU_AC_MemoryModeEnablaSw();
  }
}


#endif

/* Handle:   96,Name:              IHU_DUAL_EnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_DUAL_EnablaSw(vuint8 sigData)
{
  if(IHU_DUAL_EnablaSwValueChanged(sigData))
  {
    IlEnterCriticalIHU_DUAL_EnablaSw();
    IHU_6.IHU_6.IHU_DUAL_EnablaSw = ((vuint8) (sigData & (vuint8) 0x03));
    IlSecureEvent(IlTxSigHndIHU_DUAL_EnablaSw);
    IlLeaveCriticalIHU_DUAL_EnablaSw();
  }
}


#endif

/* Handle:   97,Name:          IHU_FrontDefrostSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FrontDefrostSwSts(vuint8 sigData)
{
  if(IHU_FrontDefrostSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_FrontDefrostSwSts();
    IHU_6.IHU_6.IHU_FrontDefrostSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_FrontDefrostSwSts);
    IlLeaveCriticalIHU_FrontDefrostSwSts();
  }
}


#endif

/* Handle:   98,Name:           IHU_VentilationSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VentilationSwSts(vuint8 sigData)
{
  if(IHU_VentilationSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_VentilationSwSts();
    IHU_6.IHU_6.IHU_VentilationSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_VentilationSwSts);
    IlLeaveCriticalIHU_VentilationSwSts();
  }
}


#endif

/* Handle:   99,Name:                  IHU_ECO_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ECO_SwSts(vuint8 sigData)
{
  if(IHU_ECO_SwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_ECO_SwSts();
    IHU_6.IHU_6.IHU_ECO_SwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_ECO_SwSts);
    IlLeaveCriticalIHU_ECO_SwSts();
  }
}


#endif

/* Handle:  100,Name:              IHU_TempSet_Psngr,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_TempSet_Psngr(vuint8 sigData)
{
  if(IHU_TempSet_PsngrValueChanged(sigData))
  {
    IlEnterCriticalIHU_TempSet_Psngr();
    IHU_6.IHU_6.IHU_TempSet_Psngr = ((vuint8) (sigData & (vuint8) 0x1F));
    IlSecureEvent(IlTxSigHndIHU_TempSet_Psngr);
    IlLeaveCriticalIHU_TempSet_Psngr();
  }
}


#endif

/* Handle:  101,Name:             IHU_LightCtrlSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_LightCtrlSwSts(vuint8 sigData)
{
  if(IHU_LightCtrlSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_LightCtrlSwSts();
    IHU_6.IHU_6.IHU_LightCtrlSwSts = ((vuint8) (sigData & (vuint8) 0x07));
    IlSecureEvent(IlTxSigHndIHU_LightCtrlSwSts);
    IlLeaveCriticalIHU_LightCtrlSwSts();
  }
}


#endif

/* Handle:  102,Name:            IHU_FrontWiperSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FrontWiperSwSts(vuint8 sigData)
{
  if(IHU_FrontWiperSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_FrontWiperSwSts();
    IHU_6.IHU_6.IHU_FrontWiperSwSts = ((vuint8) (sigData & (vuint8) 0x07));
    IlSecureEvent(IlTxSigHndIHU_FrontWiperSwSts);
    IlLeaveCriticalIHU_FrontWiperSwSts();
  }
}


#endif

/* Handle:  103,Name:          IHU_RearFogLightSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RearFogLightSwSts(vuint8 sigData)
{
  if(IHU_RearFogLightSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_RearFogLightSwSts();
    IHU_6.IHU_6.IHU_RearFogLightSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_RearFogLightSwSts);
    IlLeaveCriticalIHU_RearFogLightSwSts();
  }
}


#endif

/* Handle:  104,Name: IHU_MirrorReversePositionStoreReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MirrorReversePositionStoreReq(vuint8 sigData)
{
  if(IHU_MirrorReversePositionStoreReqValueChanged(sigData))
  {
    IlEnterCriticalIHU_MirrorReversePositionStoreReq();
    IHU_6.IHU_6.IHU_MirrorReversePositionStoreReq = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_MirrorReversePositionStoreReq);
    IlLeaveCriticalIHU_MirrorReversePositionStoreReq();
  }
}


#endif

/* Handle:  107,Name:             IHU_RearWiperSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RearWiperSwSts(vuint8 sigData)
{
  if(IHU_RearWiperSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_RearWiperSwSts();
    IHU_6.IHU_6.IHU_RearWiperSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_RearWiperSwSts);
    IlLeaveCriticalIHU_RearWiperSwSts();
  }
}


#endif

/* Handle:  108,Name:              IHU_RearWashSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RearWashSwSts(vuint8 sigData)
{
  if(IHU_RearWashSwStsValueChanged(sigData))
  {
    IlEnterCriticalIHU_RearWashSwSts();
    IHU_6.IHU_6.IHU_RearWashSwSts = ((vuint8) (sigData & (vuint8) 0x01));
    IlSecureEvent(IlTxSigHndIHU_RearWashSwSts);
    IlLeaveCriticalIHU_RearWashSwSts();
  }
}


#endif

/* Handle:  109,Name:         IHU_InverterConfirmSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_InverterConfirmSts(vuint8 sigData)
{
  vuint8 active = IHU_InverterConfirmStsIsActive(sigData);
  IlEnterCriticalIHU_InverterConfirmSts();
  IHU_5.IHU_5.IHU_InverterConfirmSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_InverterConfirmSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_InverterConfirmSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_InverterConfirmSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_InverterConfirmSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_InverterConfirmSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_InverterConfirmSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_InverterConfirmSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_InverterConfirmSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_InverterConfirmSts();
}


#endif

/* Handle:  110,Name:        IHU_InverterEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_InverterEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_InverterEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_InverterEnableSwSts();
  IHU_5.IHU_5.IHU_InverterEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_InverterEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_InverterEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_InverterEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_InverterEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_InverterEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_InverterEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_InverterEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_InverterEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_InverterEnableSwSts();
}


#endif

/* Handle:  111,Name:           IHU_CruiseTypeSetSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_CruiseTypeSetSts(vuint8 sigData)
{
  vuint8 active = IHU_CruiseTypeSetStsIsActive(sigData);
  IlEnterCriticalIHU_CruiseTypeSetSts();
  IHU_5.IHU_5.IHU_CruiseTypeSetSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_CruiseTypeSetSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_CruiseTypeSetSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_CruiseTypeSetSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_CruiseTypeSetSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_CruiseTypeSetSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_CruiseTypeSetSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_CruiseTypeSetSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_CruiseTypeSetSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_CruiseTypeSetSts();
}


#endif

/* Handle:  112,Name:             IHU_VehWashModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VehWashModeSet(vuint8 sigData)
{
  vuint8 active = IHU_VehWashModeSetIsActive(sigData);
  IlEnterCriticalIHU_VehWashModeSet();
  IHU_5.IHU_5.IHU_VehWashModeSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_VehWashModeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VehWashModeSet);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_VehWashModeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_VehWashModeSet != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_VehWashModeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VehWashModeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VehWashModeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VehWashModeSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VehWashModeSet();
}


#endif

/* Handle:  113,Name:         IHU_SpeedLimitValueSet,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SpeedLimitValueSet(vuint8 sigData)
{
  vuint8 active = IHU_SpeedLimitValueSetIsActive(sigData);
  IlEnterCriticalIHU_SpeedLimitValueSet();
  IHU_5.IHU_5.IHU_SpeedLimitValueSet = ((vuint8) (sigData & (vuint8) 0x1F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_SpeedLimitValueSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SpeedLimitValueSet);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_SpeedLimitValueSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_SpeedLimitValueSet != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_SpeedLimitValueSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SpeedLimitValueSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SpeedLimitValueSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SpeedLimitValueSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SpeedLimitValueSet();
}


#endif

/* Handle:  114,Name:      IHU_SpeedLimitEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SpeedLimitEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_SpeedLimitEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_SpeedLimitEnableSwSts();
  IHU_5.IHU_5.IHU_SpeedLimitEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_SpeedLimitEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SpeedLimitEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_SpeedLimitEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_SpeedLimitEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_SpeedLimitEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SpeedLimitEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SpeedLimitEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SpeedLimitEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SpeedLimitEnableSwSts();
}


#endif

/* Handle:  115,Name:       IHU_RegenerationLevelSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RegenerationLevelSet(vuint8 sigData)
{
  vuint8 active = IHU_RegenerationLevelSetIsActive(sigData);
  IlEnterCriticalIHU_RegenerationLevelSet();
  IHU_5.IHU_5.IHU_RegenerationLevelSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_RegenerationLevelSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_RegenerationLevelSet);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_RegenerationLevelSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_RegenerationLevelSet != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_RegenerationLevelSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_RegenerationLevelSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_RegenerationLevelSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_RegenerationLevelSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_RegenerationLevelSet();
}


#endif

/* Handle:  116,Name:               IHU_DriveModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_DriveModeSet(vuint8 sigData)
{
  vuint8 active = IHU_DriveModeSetIsActive(sigData);
  IlEnterCriticalIHU_DriveModeSet();
  IHU_5.IHU_5.IHU_DriveModeSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_DriveModeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_DriveModeSet);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_DriveModeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_DriveModeSet != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_DriveModeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_DriveModeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_DriveModeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_DriveModeSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_DriveModeSet();
}


#endif

/* Handle:  117,Name:          IHU_OutputLimitSOCSet,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_OutputLimitSOCSet(vuint8 sigData)
{
  vuint8 active = IHU_OutputLimitSOCSetIsActive(sigData);
  IlEnterCriticalIHU_OutputLimitSOCSet();
  IHU_5.IHU_5.IHU_OutputLimitSOCSet = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_OutputLimitSOCSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_OutputLimitSOCSet);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_OutputLimitSOCSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_OutputLimitSOCSet != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_OutputLimitSOCSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_OutputLimitSOCSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_OutputLimitSOCSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_OutputLimitSOCSet);
        }
      }
    }
  }
  IlLeaveCriticalIHU_OutputLimitSOCSet();
}


#endif

/* Handle:  118,Name: IHU_ShiftVoiceRemind_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ShiftVoiceRemind_EnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_ShiftVoiceRemind_EnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_ShiftVoiceRemind_EnableSwSts();
  IHU_5.IHU_5.IHU_ShiftVoiceRemind_EnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_ShiftVoiceRemind_EnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ShiftVoiceRemind_EnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_ShiftVoiceRemind_EnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_ShiftVoiceRemind_EnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_ShiftVoiceRemind_EnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ShiftVoiceRemind_EnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ShiftVoiceRemind_EnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ShiftVoiceRemind_EnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ShiftVoiceRemind_EnableSwSts();
}


#endif

/* Handle:  119,Name: IHU_ShiftErrorActionRemindEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ShiftErrorActionRemindEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_ShiftErrorActionRemindEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_ShiftErrorActionRemindEnableSwSts();
  IHU_5.IHU_5.IHU_ShiftErrorActionRemindEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_ShiftErrorActionRemindEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_ShiftErrorActionRemindEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_ShiftErrorActionRemindEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_ShiftErrorActionRemindEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_ShiftErrorActionRemindEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ShiftErrorActionRemindEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ShiftErrorActionRemindEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_ShiftErrorActionRemindEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_ShiftErrorActionRemindEnableSwSts();
}


#endif

/* Handle:  120,Name:  IHU_SOC_ChargeLockEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SOC_ChargeLockEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_SOC_ChargeLockEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_SOC_ChargeLockEnableSwSts();
  IHU_5.IHU_5.IHU_SOC_ChargeLockEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_5.IHU_SOC_ChargeLockEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SOC_ChargeLockEnableSwSts);
        }
      }
      ilIfActiveFlags.IHU_5.IHU_SOC_ChargeLockEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_5.IHU_SOC_ChargeLockEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_5.IHU_SOC_ChargeLockEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SOC_ChargeLockEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SOC_ChargeLockEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_5) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_5) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SOC_ChargeLockEnableSwSts);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SOC_ChargeLockEnableSwSts();
}


#endif

/* Handle:  121,Name:        IHU_AutoLockEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AutoLockEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AutoLockEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AutoLockEnableSwSts();
  IHU_4.IHU_4.IHU_AutoLockEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_AutoLockEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_AutoLockEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_AutoLockEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_AutoLockEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_AutoLockEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AutoLockEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AutoLockEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_AutoLockEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_AutoLockEnableSwSts();
}


#endif

/* Handle:  122,Name:      IHU_AntiTheftModeSetSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AntiTheftModeSetSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AntiTheftModeSetSwStsIsActive(sigData);
  IlEnterCriticalIHU_AntiTheftModeSetSwSts();
  IHU_4.IHU_4.IHU_AntiTheftModeSetSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_AntiTheftModeSetSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_AntiTheftModeSetSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_AntiTheftModeSetSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_AntiTheftModeSetSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_AntiTheftModeSetSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AntiTheftModeSetSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AntiTheftModeSetSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_AntiTheftModeSetSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_AntiTheftModeSetSwSts();
}


#endif

/* Handle:  123,Name:       IHU_SteeringWheelHeatReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SteeringWheelHeatReq(vuint8 sigData)
{
  vuint8 active = IHU_SteeringWheelHeatReqIsActive(sigData);
  IlEnterCriticalIHU_SteeringWheelHeatReq();
  IHU_4.IHU_4.IHU_SteeringWheelHeatReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_SteeringWheelHeatReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_SteeringWheelHeatReq);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_SteeringWheelHeatReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_SteeringWheelHeatReq != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_SteeringWheelHeatReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SteeringWheelHeatReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SteeringWheelHeatReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_SteeringWheelHeatReq);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_SteeringWheelHeatReq();
}


#endif

/* Handle:  124,Name:    IHU_MirrorFoldEnableSwSts__,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MirrorFoldEnableSwSts__(vuint8 sigData)
{
  vuint8 active = IHU_MirrorFoldEnableSwSts__IsActive(sigData);
  IlEnterCriticalIHU_MirrorFoldEnableSwSts__();
  IHU_4.IHU_4.IHU_MirrorFoldEnableSwSts__ = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_MirrorFoldEnableSwSts__ == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_MirrorFoldEnableSwSts__);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_MirrorFoldEnableSwSts__ = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_MirrorFoldEnableSwSts__ != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_MirrorFoldEnableSwSts__ = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MirrorFoldEnableSwSts__] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MirrorFoldEnableSwSts__);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_MirrorFoldEnableSwSts__);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_MirrorFoldEnableSwSts__();
}


#endif

/* Handle:  125,Name:    IHU_FollowHomeMeEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FollowHomeMeEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_FollowHomeMeEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_FollowHomeMeEnableSwSts();
  IHU_4.IHU_4.IHU_FollowHomeMeEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_FollowHomeMeEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_FollowHomeMeEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_FollowHomeMeEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_FollowHomeMeEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_FollowHomeMeEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FollowHomeMeEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FollowHomeMeEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_FollowHomeMeEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_FollowHomeMeEnableSwSts();
}


#endif

/* Handle:  126,Name: IHU_SunroofAutoCloseEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SunroofAutoCloseEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_SunroofAutoCloseEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_SunroofAutoCloseEnableSwSts();
  IHU_4.IHU_4.IHU_SunroofAutoCloseEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_SunroofAutoCloseEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_SunroofAutoCloseEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_SunroofAutoCloseEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_SunroofAutoCloseEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_SunroofAutoCloseEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SunroofAutoCloseEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SunroofAutoCloseEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_SunroofAutoCloseEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_SunroofAutoCloseEnableSwSts();
}


#endif

/* Handle:  127,Name:    IHU_WelcomeLightEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_WelcomeLightEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_WelcomeLightEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_WelcomeLightEnableSwSts();
  IHU_4.IHU_4.IHU_WelcomeLightEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_WelcomeLightEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_WelcomeLightEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_WelcomeLightEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_WelcomeLightEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_WelcomeLightEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_WelcomeLightEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_WelcomeLightEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_WelcomeLightEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_WelcomeLightEnableSwSts();
}


#endif

/* Handle:  128,Name:             IHU_ComfortModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ComfortModeSet(vuint8 sigData)
{
  vuint8 active = IHU_ComfortModeSetIsActive(sigData);
  IlEnterCriticalIHU_ComfortModeSet();
  IHU_4.IHU_4.IHU_ComfortModeSet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_ComfortModeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_ComfortModeSet);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_ComfortModeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_ComfortModeSet != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_ComfortModeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ComfortModeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ComfortModeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_ComfortModeSet);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_ComfortModeSet();
}


#endif

/* Handle:  129,Name:        IHU_WiperSensitivitySet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_WiperSensitivitySet(vuint8 sigData)
{
  vuint8 active = IHU_WiperSensitivitySetIsActive(sigData);
  IlEnterCriticalIHU_WiperSensitivitySet();
  IHU_4.IHU_4.IHU_WiperSensitivitySet = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_WiperSensitivitySet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_WiperSensitivitySet);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_WiperSensitivitySet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_WiperSensitivitySet != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_WiperSensitivitySet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_WiperSensitivitySet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_WiperSensitivitySet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_WiperSensitivitySet);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_WiperSensitivitySet();
}


#endif

/* Handle:  130,Name:              IHU_MirrorFoldReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MirrorFoldReq(vuint8 sigData)
{
  vuint8 active = IHU_MirrorFoldReqIsActive(sigData);
  IlEnterCriticalIHU_MirrorFoldReq();
  IHU_4.IHU_4.IHU_MirrorFoldReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_MirrorFoldReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_MirrorFoldReq);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_MirrorFoldReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_MirrorFoldReq != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_MirrorFoldReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MirrorFoldReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MirrorFoldReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_MirrorFoldReq);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_MirrorFoldReq();
}


#endif

/* Handle:  131,Name:            IHU_AHB_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AHB_EnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AHB_EnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AHB_EnableSwSts();
  IHU_4.IHU_4.IHU_AHB_EnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_AHB_EnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_AHB_EnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_AHB_EnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_AHB_EnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_AHB_EnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AHB_EnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AHB_EnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_AHB_EnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_AHB_EnableSwSts();
}


#endif

/* Handle:  132,Name:         IHU_LowBeamAngleAdjust,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_LowBeamAngleAdjust(vuint8 sigData)
{
  vuint8 active = IHU_LowBeamAngleAdjustIsActive(sigData);
  IlEnterCriticalIHU_LowBeamAngleAdjust();
  IHU_4.IHU_4.IHU_LowBeamAngleAdjust = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_LowBeamAngleAdjust == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_LowBeamAngleAdjust);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_LowBeamAngleAdjust = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_LowBeamAngleAdjust != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_LowBeamAngleAdjust = 0;
      ilTxNSendCounter[IlTxSigHndIHU_LowBeamAngleAdjust] = IlGetTxRepetitionCounter(IlTxSigHndIHU_LowBeamAngleAdjust);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_LowBeamAngleAdjust);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_LowBeamAngleAdjust();
}


#endif

/* Handle:  133,Name: IHU_FrontWiperMaintenanceModeReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FrontWiperMaintenanceModeReq(vuint8 sigData)
{
  vuint8 active = IHU_FrontWiperMaintenanceModeReqIsActive(sigData);
  IlEnterCriticalIHU_FrontWiperMaintenanceModeReq();
  IHU_4.IHU_4.IHU_FrontWiperMaintenanceModeReq = ((vuint8) (sigData & (vuint8) 0x01));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_FrontWiperMaintenanceModeReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_FrontWiperMaintenanceModeReq);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_FrontWiperMaintenanceModeReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_FrontWiperMaintenanceModeReq != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_FrontWiperMaintenanceModeReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FrontWiperMaintenanceModeReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FrontWiperMaintenanceModeReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_FrontWiperMaintenanceModeReq);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_FrontWiperMaintenanceModeReq();
}


#endif

/* Handle:  134,Name: IHU_EmergencyPowerOffReqConfirm,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_EmergencyPowerOffReqConfirm(vuint8 sigData)
{
  vuint8 active = IHU_EmergencyPowerOffReqConfirmIsActive(sigData);
  IlEnterCriticalIHU_EmergencyPowerOffReqConfirm();
  IHU_4.IHU_4.IHU_EmergencyPowerOffReqConfirm = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_EmergencyPowerOffReqConfirm == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_EmergencyPowerOffReqConfirm);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_EmergencyPowerOffReqConfirm = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_EmergencyPowerOffReqConfirm != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_EmergencyPowerOffReqConfirm = 0;
      ilTxNSendCounter[IlTxSigHndIHU_EmergencyPowerOffReqConfirm] = IlGetTxRepetitionCounter(IlTxSigHndIHU_EmergencyPowerOffReqConfirm);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_EmergencyPowerOffReqConfirm);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_EmergencyPowerOffReqConfirm();
}


#endif

/* Handle:  135,Name: IHU_MirrorReverseInclineEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MirrorReverseInclineEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_MirrorReverseInclineEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_MirrorReverseInclineEnableSwSts();
  IHU_4.IHU_4.IHU_MirrorReverseInclineEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_MirrorReverseInclineEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_MirrorReverseInclineEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_MirrorReverseInclineEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_MirrorReverseInclineEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_MirrorReverseInclineEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MirrorReverseInclineEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MirrorReverseInclineEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_MirrorReverseInclineEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_MirrorReverseInclineEnableSwSts();
}


#endif

/* Handle:  136,Name:     IHU_FollowMeHomeTimeSetSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FollowMeHomeTimeSetSts(vuint8 sigData)
{
  vuint8 active = IHU_FollowMeHomeTimeSetStsIsActive(sigData);
  IlEnterCriticalIHU_FollowMeHomeTimeSetSts();
  IHU_4.IHU_4.IHU_FollowMeHomeTimeSetSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_FollowMeHomeTimeSetSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_FollowMeHomeTimeSetSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_FollowMeHomeTimeSetSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_FollowMeHomeTimeSetSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_FollowMeHomeTimeSetSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FollowMeHomeTimeSetSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FollowMeHomeTimeSetSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_FollowMeHomeTimeSetSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_FollowMeHomeTimeSetSts();
}


#endif

/* Handle:  137,Name:  IHU_DoorWindowCtrlEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_DoorWindowCtrlEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_DoorWindowCtrlEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_DoorWindowCtrlEnableSwSts();
  IHU_4.IHU_4.IHU_DoorWindowCtrlEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_DoorWindowCtrlEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_DoorWindowCtrlEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_DoorWindowCtrlEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_DoorWindowCtrlEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_DoorWindowCtrlEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_DoorWindowCtrlEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_DoorWindowCtrlEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_DoorWindowCtrlEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_DoorWindowCtrlEnableSwSts();
}


#endif

/* Handle:  138,Name: IHU_WindowAutoCloseEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_WindowAutoCloseEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_WindowAutoCloseEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_WindowAutoCloseEnableSwSts();
  IHU_4.IHU_4.IHU_WindowAutoCloseEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_WindowAutoCloseEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_WindowAutoCloseEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_WindowAutoCloseEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_WindowAutoCloseEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_WindowAutoCloseEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_WindowAutoCloseEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_WindowAutoCloseEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_WindowAutoCloseEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_WindowAutoCloseEnableSwSts();
}


#endif

/* Handle:  139,Name: IHU_RKE_SunRoofCtrlEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_RKE_SunRoofCtrlEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_RKE_SunRoofCtrlEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_RKE_SunRoofCtrlEnableSwSts();
  IHU_4.IHU_4.IHU_RKE_SunRoofCtrlEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_RKE_SunRoofCtrlEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_RKE_SunRoofCtrlEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_RKE_SunRoofCtrlEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_RKE_SunRoofCtrlEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_RKE_SunRoofCtrlEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_RKE_SunRoofCtrlEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_RKE_SunRoofCtrlEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_RKE_SunRoofCtrlEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_RKE_SunRoofCtrlEnableSwSts();
}


#endif

/* Handle:  140,Name: IHU_DomeLightDoorCtrlSwitchSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_DomeLightDoorCtrlSwitchSts(vuint8 sigData)
{
  vuint8 active = IHU_DomeLightDoorCtrlSwitchStsIsActive(sigData);
  IlEnterCriticalIHU_DomeLightDoorCtrlSwitchSts();
  IHU_4.IHU_4.IHU_DomeLightDoorCtrlSwitchSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_DomeLightDoorCtrlSwitchSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_DomeLightDoorCtrlSwitchSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_DomeLightDoorCtrlSwitchSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_DomeLightDoorCtrlSwitchSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_DomeLightDoorCtrlSwitchSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_DomeLightDoorCtrlSwitchSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_DomeLightDoorCtrlSwitchSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_DomeLightDoorCtrlSwitchSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_DomeLightDoorCtrlSwitchSts();
}


#endif

/* Handle:  141,Name:             IHU_MaxPositionSet,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MaxPositionSet(vuint8 sigData)
{
  vuint8 active = IHU_MaxPositionSetIsActive(sigData);
  IlEnterCriticalIHU_MaxPositionSet();
  IHU_4.IHU_4.IHU_MaxPositionSet = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_MaxPositionSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_MaxPositionSet);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_MaxPositionSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_MaxPositionSet != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_MaxPositionSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MaxPositionSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MaxPositionSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_MaxPositionSet);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_MaxPositionSet();
}


#endif

/* Handle:  142,Name:       IHU_FragranceEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FragranceEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_FragranceEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_FragranceEnableSwSts();
  IHU_4.IHU_4.IHU_FragranceEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_FragranceEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_FragranceEnableSwSts);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_FragranceEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_FragranceEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_FragranceEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FragranceEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FragranceEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_FragranceEnableSwSts);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_FragranceEnableSwSts();
}


#endif

/* Handle:  143,Name:  IHU_FragranceConcentrationSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FragranceConcentrationSet(vuint8 sigData)
{
  vuint8 active = IHU_FragranceConcentrationSetIsActive(sigData);
  IlEnterCriticalIHU_FragranceConcentrationSet();
  IHU_4.IHU_4.IHU_FragranceConcentrationSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_FragranceConcentrationSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_FragranceConcentrationSet);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_FragranceConcentrationSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_FragranceConcentrationSet != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_FragranceConcentrationSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FragranceConcentrationSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FragranceConcentrationSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_FragranceConcentrationSet);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_FragranceConcentrationSet();
}


#endif

/* Handle:  144,Name:       IHU_FragrancePositionSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FragrancePositionSet(vuint8 sigData)
{
  vuint8 active = IHU_FragrancePositionSetIsActive(sigData);
  IlEnterCriticalIHU_FragrancePositionSet();
  IHU_4.IHU_4.IHU_FragrancePositionSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_4.IHU_FragrancePositionSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStartEvent(IlTxSigHndIHU_FragrancePositionSet);
          }
        }
      }
      ilIfActiveFlags.IHU_4.IHU_FragrancePositionSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_4.IHU_FragrancePositionSet != 0)
    {
      ilIfActiveFlags.IHU_4.IHU_FragrancePositionSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_FragrancePositionSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_FragrancePositionSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_4) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 1) == 0)
        {
          if( * ((vuint8*) ( & ilIfActiveFlags.IHU_4) + 2) == 0)
          {
            (void) IlStopEvent(IlTxSigHndIHU_FragrancePositionSet);
          }
        }
      }
    }
  }
  IlLeaveCriticalIHU_FragrancePositionSet();
}


#endif

/* Handle:  145,Name:       IHU_VoiceCtrl_SunroofReq,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_SunroofReq(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_SunroofReqIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_SunroofReq();
  IHU_3.IHU_3.IHU_VoiceCtrl_SunroofReq = ((vuint8) (sigData & (vuint8) 0x0F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunroofReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_SunroofReq);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunroofReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunroofReq != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunroofReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_SunroofReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_SunroofReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_SunroofReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_SunroofReq();
}


#endif

/* Handle:  146,Name:      IHU_VoiceCtrl_SunshadeReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_SunshadeReq(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_SunshadeReqIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_SunshadeReq();
  IHU_3.IHU_3.IHU_VoiceCtrl_SunshadeReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunshadeReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_SunshadeReq);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunshadeReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunshadeReq != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_SunshadeReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_SunshadeReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_SunshadeReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_SunshadeReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_SunshadeReq();
}


#endif

/* Handle:  147,Name:     IHU_VoiceCtrl_WindowReq_FL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_WindowReq_FL(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_WindowReq_FLIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_WindowReq_FL();
  IHU_3.IHU_3.IHU_VoiceCtrl_WindowReq_FL = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FL == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_FL);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FL = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FL != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FL = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_WindowReq_FL] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_WindowReq_FL);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_FL);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_WindowReq_FL();
}


#endif

/* Handle:  148,Name:     IHU_VoiceCtrl_WindowReq_FR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_WindowReq_FR(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_WindowReq_FRIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_WindowReq_FR();
  IHU_3.IHU_3.IHU_VoiceCtrl_WindowReq_FR = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FR == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_FR);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FR = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FR != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_FR = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_WindowReq_FR] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_WindowReq_FR);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_FR);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_WindowReq_FR();
}


#endif

/* Handle:  149,Name:     IHU_VoiceCtrl_WindowReq_RL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_WindowReq_RL(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_WindowReq_RLIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_WindowReq_RL();
  IHU_3.IHU_3.IHU_VoiceCtrl_WindowReq_RL = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RL == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_RL);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RL = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RL != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RL = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_WindowReq_RL] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_WindowReq_RL);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_RL);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_WindowReq_RL();
}


#endif

/* Handle:  150,Name:     IHU_VoiceCtrl_WindowReq_RR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_WindowReq_RR(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_WindowReq_RRIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_WindowReq_RR();
  IHU_3.IHU_3.IHU_VoiceCtrl_WindowReq_RR = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RR == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_RR);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RR = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RR != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_WindowReq_RR = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_WindowReq_RR] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_WindowReq_RR);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_WindowReq_RR);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_WindowReq_RR();
}


#endif

/* Handle:  151,Name:       IHU_VoiceCtrl_LowbeamReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_LowbeamReq(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_LowbeamReqIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_LowbeamReq();
  IHU_3.IHU_3.IHU_VoiceCtrl_LowbeamReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_LowbeamReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_LowbeamReq);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_LowbeamReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_LowbeamReq != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_LowbeamReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_LowbeamReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_LowbeamReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_LowbeamReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_LowbeamReq();
}


#endif

/* Handle:  152,Name:         IHU_VoiceCtrl_TrunkReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_VoiceCtrl_TrunkReq(vuint8 sigData)
{
  vuint8 active = IHU_VoiceCtrl_TrunkReqIsActive(sigData);
  IlEnterCriticalIHU_VoiceCtrl_TrunkReq();
  IHU_3.IHU_3.IHU_VoiceCtrl_TrunkReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_TrunkReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_VoiceCtrl_TrunkReq);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_TrunkReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_TrunkReq != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_VoiceCtrl_TrunkReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_VoiceCtrl_TrunkReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_VoiceCtrl_TrunkReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_VoiceCtrl_TrunkReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_VoiceCtrl_TrunkReq();
}


#endif

/* Handle:  153,Name:             IHU_SmokingModeReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_SmokingModeReq(vuint8 sigData)
{
  vuint8 active = IHU_SmokingModeReqIsActive(sigData);
  IlEnterCriticalIHU_SmokingModeReq();
  IHU_3.IHU_3.IHU_SmokingModeReq = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_3.IHU_SmokingModeReq == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStartEvent(IlTxSigHndIHU_SmokingModeReq);
        }
      }
      ilIfActiveFlags.IHU_3.IHU_SmokingModeReq = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_3.IHU_SmokingModeReq != 0)
    {
      ilIfActiveFlags.IHU_3.IHU_SmokingModeReq = 0;
      ilTxNSendCounter[IlTxSigHndIHU_SmokingModeReq] = IlGetTxRepetitionCounter(IlTxSigHndIHU_SmokingModeReq);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_3) == 0)
      {
        if( * ((vuint8*) ( & ilIfActiveFlags.IHU_3) + 1) == 0)
        {
          (void) IlStopEvent(IlTxSigHndIHU_SmokingModeReq);
        }
      }
    }
  }
  IlLeaveCriticalIHU_SmokingModeReq();
}


#endif

/* Handle:  154,Name:      IHU_MassageSwithSet_Psngr,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MassageSwithSet_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_MassageSwithSet_PsngrIsActive(sigData);
  IlEnterCriticalIHU_MassageSwithSet_Psngr();
  IHU_17.IHU_17.IHU_MassageSwithSet_Psngr = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_MassageSwithSet_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MassageSwithSet_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_MassageSwithSet_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_MassageSwithSet_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_MassageSwithSet_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MassageSwithSet_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MassageSwithSet_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MassageSwithSet_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_MassageSwithSet_Psngr();
}


#endif

/* Handle:  155,Name:       IHU_MassageModeSet_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MassageModeSet_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_MassageModeSet_PsngrIsActive(sigData);
  IlEnterCriticalIHU_MassageModeSet_Psngr();
  IHU_17.IHU_17.IHU_MassageModeSet_Psngr = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_MassageModeSet_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MassageModeSet_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_MassageModeSet_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_MassageModeSet_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_MassageModeSet_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MassageModeSet_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MassageModeSet_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MassageModeSet_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_MassageModeSet_Psngr();
}


#endif

/* Handle:  156,Name:   IHU_MassageStrengthSet_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MassageStrengthSet_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_MassageStrengthSet_PsngrIsActive(sigData);
  IlEnterCriticalIHU_MassageStrengthSet_Psngr();
  IHU_17.IHU_17.IHU_MassageStrengthSet_Psngr = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_MassageStrengthSet_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MassageStrengthSet_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_MassageStrengthSet_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_MassageStrengthSet_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_MassageStrengthSet_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MassageStrengthSet_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MassageStrengthSet_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MassageStrengthSet_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_MassageStrengthSet_Psngr();
}


#endif

/* Handle:  157,Name: IHU_LumbarSupportModeSet_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_LumbarSupportModeSet_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_LumbarSupportModeSet_PsngrIsActive(sigData);
  IlEnterCriticalIHU_LumbarSupportModeSet_Psngr();
  IHU_17.IHU_17.IHU_LumbarSupportModeSet_Psngr = ((vuint8) (sigData & (vuint8) 0x07));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_LumbarSupportModeSet_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_LumbarSupportModeSet_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_LumbarSupportModeSet_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_LumbarSupportModeSet_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_LumbarSupportModeSet_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_LumbarSupportModeSet_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_LumbarSupportModeSet_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_LumbarSupportModeSet_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_LumbarSupportModeSet_Psngr();
}


#endif

/* Handle:  158,Name: IHU_MotoPositionSet_ForwardBack_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_ForwardBack_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_ForwardBack_PsngrIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_ForwardBack_Psngr();
  IHU_17.IHU_17.IHU_MotoPositionSet_ForwardBack_Psngr = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_ForwardBack_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_ForwardBack_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_ForwardBack_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_ForwardBack_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_ForwardBack_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_ForwardBack_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_ForwardBack_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_ForwardBack_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_ForwardBack_Psngr();
}


#endif

/* Handle:  159,Name: IHU_MotoPositionSet_SeatBack_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_SeatBack_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_SeatBack_PsngrIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_SeatBack_Psngr();
  IHU_17.IHU_17.IHU_MotoPositionSet_SeatBack_Psngr = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_SeatBack_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_SeatBack_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_SeatBack_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_SeatBack_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_SeatBack_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_SeatBack_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_SeatBack_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_SeatBack_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_SeatBack_Psngr();
}


#endif

/* Handle:  160,Name:         IHU_17_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_17_MsgAliveCounter(vuint8 sigData)
{
  vuint8 active = IHU_17_MsgAliveCounterIsActive(sigData);
  IlEnterCriticalIHU_17_MsgAliveCounter();
  IHU_17.IHU_17.IHU_17_MsgAliveCounter = ((vuint8) (sigData & (vuint8) 0x0F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_17_MsgAliveCounter == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_17_MsgAliveCounter);
      }
      ilIfActiveFlags.IHU_17.IHU_17_MsgAliveCounter = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_17_MsgAliveCounter != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_17_MsgAliveCounter = 0;
      ilTxNSendCounter[IlTxSigHndIHU_17_MsgAliveCounter] = IlGetTxRepetitionCounter(IlTxSigHndIHU_17_MsgAliveCounter);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_17_MsgAliveCounter);
      }
    }
  }
  IlLeaveCriticalIHU_17_MsgAliveCounter();
}


#endif

/* Handle:  161,Name: IHU_MotoPositionSet_LegSupport_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_LegSupport_Psngr(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_LegSupport_PsngrIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_LegSupport_Psngr();
  IHU_17.IHU_17.IHU_MotoPositionSet_LegSupport_Psngr = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_LegSupport_Psngr == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_LegSupport_Psngr);
      }
      ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_LegSupport_Psngr = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_LegSupport_Psngr != 0)
    {
      ilIfActiveFlags.IHU_17.IHU_MotoPositionSet_LegSupport_Psngr = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_LegSupport_Psngr] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_LegSupport_Psngr);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_17) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_LegSupport_Psngr);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_LegSupport_Psngr();
}


#endif

/* Handle:  162,Name: IHU_MotoPositionSet_ForwardBack_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_ForwardBack_Driver(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_ForwardBack_DriverIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_ForwardBack_Driver();
  IHU_16.IHU_16.IHU_MotoPositionSet_ForwardBack_Driver = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_ForwardBack_Driver == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_ForwardBack_Driver);
      }
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_ForwardBack_Driver = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_ForwardBack_Driver != 0)
    {
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_ForwardBack_Driver = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_ForwardBack_Driver] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_ForwardBack_Driver);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_ForwardBack_Driver);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_ForwardBack_Driver();
}


#endif

/* Handle:  163,Name: IHU_MotoPositionSet_UpDown_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_UpDown_Driver(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_UpDown_DriverIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_UpDown_Driver();
  IHU_16.IHU_16.IHU_MotoPositionSet_UpDown_Driver = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_UpDown_Driver == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_UpDown_Driver);
      }
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_UpDown_Driver = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_UpDown_Driver != 0)
    {
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_UpDown_Driver = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_UpDown_Driver] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_UpDown_Driver);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_UpDown_Driver);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_UpDown_Driver();
}


#endif

/* Handle:  164,Name: IHU_MotoPositionSet_SeatBack_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_SeatBack_Driver(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_SeatBack_DriverIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_SeatBack_Driver();
  IHU_16.IHU_16.IHU_MotoPositionSet_SeatBack_Driver = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_SeatBack_Driver == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_SeatBack_Driver);
      }
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_SeatBack_Driver = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_SeatBack_Driver != 0)
    {
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_SeatBack_Driver = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_SeatBack_Driver] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_SeatBack_Driver);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_SeatBack_Driver);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_SeatBack_Driver();
}


#endif

/* Handle:  165,Name:         IHU_16_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_16_MsgAliveCounter(vuint8 sigData)
{
  vuint8 active = IHU_16_MsgAliveCounterIsActive(sigData);
  IlEnterCriticalIHU_16_MsgAliveCounter();
  IHU_16.IHU_16.IHU_16_MsgAliveCounter = ((vuint8) (sigData & (vuint8) 0x0F));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_16.IHU_16_MsgAliveCounter == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_16_MsgAliveCounter);
      }
      ilIfActiveFlags.IHU_16.IHU_16_MsgAliveCounter = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_16.IHU_16_MsgAliveCounter != 0)
    {
      ilIfActiveFlags.IHU_16.IHU_16_MsgAliveCounter = 0;
      ilTxNSendCounter[IlTxSigHndIHU_16_MsgAliveCounter] = IlGetTxRepetitionCounter(IlTxSigHndIHU_16_MsgAliveCounter);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_16_MsgAliveCounter);
      }
    }
  }
  IlLeaveCriticalIHU_16_MsgAliveCounter();
}


#endif

/* Handle:  166,Name: IHU_MotoPositionSet_LegSupport_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_MotoPositionSet_LegSupport_Driver(vuint8 sigData)
{
  vuint8 active = IHU_MotoPositionSet_LegSupport_DriverIsActive(sigData);
  IlEnterCriticalIHU_MotoPositionSet_LegSupport_Driver();
  IHU_16.IHU_16.IHU_MotoPositionSet_LegSupport_Driver = ((vuint8) sigData);
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_LegSupport_Driver == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_MotoPositionSet_LegSupport_Driver);
      }
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_LegSupport_Driver = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_LegSupport_Driver != 0)
    {
      ilIfActiveFlags.IHU_16.IHU_MotoPositionSet_LegSupport_Driver = 0;
      ilTxNSendCounter[IlTxSigHndIHU_MotoPositionSet_LegSupport_Driver] = IlGetTxRepetitionCounter(IlTxSigHndIHU_MotoPositionSet_LegSupport_Driver);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_16) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_MotoPositionSet_LegSupport_Driver);
      }
    }
  }
  IlLeaveCriticalIHU_MotoPositionSet_LegSupport_Driver();
}


#endif

/* Handle:  175,Name:     IHU_FM_RadioFrequanceValue,Size: 16,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_FM_RadioFrequanceValue(vuint16 sigData)
{
  IlEnterCriticalIHU_FM_RadioFrequanceValue();
  IHU_14.IHU_14.IHU_FM_RadioFrequanceValue_0 = ((vuint8) sigData);
  IHU_14.IHU_14.IHU_FM_RadioFrequanceValue_1 = ((vuint8) (sigData >> 8));
  IlLeaveCriticalIHU_FM_RadioFrequanceValue();
}


#endif

/* Handle:  176,Name:     IHU_AM_RadioFrequanceValue,Size: 11,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AM_RadioFrequanceValue(vuint16 sigData)
{
  IlEnterCriticalIHU_AM_RadioFrequanceValue();
  IHU_14.IHU_14.IHU_AM_RadioFrequanceValue_0 = ((vuint8) (sigData & (vuint8) 0x07));
  IHU_14.IHU_14.IHU_AM_RadioFrequanceValue_1 = ((vuint8) (sigData >> 3));
  IlLeaveCriticalIHU_AM_RadioFrequanceValue();
}


#endif

/* Handle:  179,Name:               IHU_BrakemodeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_BrakemodeSet(vuint8 sigData)
{
  vuint8 active = IHU_BrakemodeSetIsActive(sigData);
  IlEnterCriticalIHU_BrakemodeSet();
  IHU_1.IHU_1.IHU_BrakemodeSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_BrakemodeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_BrakemodeSet);
      }
      ilIfActiveFlags.IHU_1.IHU_BrakemodeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_BrakemodeSet != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_BrakemodeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_BrakemodeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_BrakemodeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_BrakemodeSet);
      }
    }
  }
  IlLeaveCriticalIHU_BrakemodeSet();
}


#endif

/* Handle:  180,Name:                IHU_EPS_ModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_EPS_ModeSet(vuint8 sigData)
{
  vuint8 active = IHU_EPS_ModeSetIsActive(sigData);
  IlEnterCriticalIHU_EPS_ModeSet();
  IHU_1.IHU_1.IHU_EPS_ModeSet = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_EPS_ModeSet == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_EPS_ModeSet);
      }
      ilIfActiveFlags.IHU_1.IHU_EPS_ModeSet = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_EPS_ModeSet != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_EPS_ModeSet = 0;
      ilTxNSendCounter[IlTxSigHndIHU_EPS_ModeSet] = IlGetTxRepetitionCounter(IlTxSigHndIHU_EPS_ModeSet);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_EPS_ModeSet);
      }
    }
  }
  IlLeaveCriticalIHU_EPS_ModeSet();
}


#endif

/* Handle:  181,Name:              IHU_ESC_OFF_SwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_ESC_OFF_SwSts(vuint8 sigData)
{
  vuint8 active = IHU_ESC_OFF_SwStsIsActive(sigData);
  IlEnterCriticalIHU_ESC_OFF_SwSts();
  IHU_1.IHU_1.IHU_ESC_OFF_SwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_ESC_OFF_SwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_ESC_OFF_SwSts);
      }
      ilIfActiveFlags.IHU_1.IHU_ESC_OFF_SwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_ESC_OFF_SwSts != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_ESC_OFF_SwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_ESC_OFF_SwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_ESC_OFF_SwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_ESC_OFF_SwSts);
      }
    }
  }
  IlLeaveCriticalIHU_ESC_OFF_SwSts();
}


#endif

/* Handle:  182,Name:    IHU_CST_FunctionEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_CST_FunctionEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_CST_FunctionEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_CST_FunctionEnableSwSts();
  IHU_1.IHU_1.IHU_CST_FunctionEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_CST_FunctionEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_CST_FunctionEnableSwSts);
      }
      ilIfActiveFlags.IHU_1.IHU_CST_FunctionEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_CST_FunctionEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_CST_FunctionEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_CST_FunctionEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_CST_FunctionEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_CST_FunctionEnableSwSts);
      }
    }
  }
  IlLeaveCriticalIHU_CST_FunctionEnableSwSts();
}


#endif

/* Handle:  183,Name:    IHU_HDC_FunctionEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_HDC_FunctionEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_HDC_FunctionEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_HDC_FunctionEnableSwSts();
  IHU_1.IHU_1.IHU_HDC_FunctionEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_HDC_FunctionEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_HDC_FunctionEnableSwSts);
      }
      ilIfActiveFlags.IHU_1.IHU_HDC_FunctionEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_HDC_FunctionEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_HDC_FunctionEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_HDC_FunctionEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_HDC_FunctionEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_HDC_FunctionEnableSwSts);
      }
    }
  }
  IlLeaveCriticalIHU_HDC_FunctionEnableSwSts();
}


#endif

/* Handle:  184,Name:    IHU_AVH_FunctionEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_AVH_FunctionEnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_AVH_FunctionEnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_AVH_FunctionEnableSwSts();
  IHU_1.IHU_1.IHU_AVH_FunctionEnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_AVH_FunctionEnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_AVH_FunctionEnableSwSts);
      }
      ilIfActiveFlags.IHU_1.IHU_AVH_FunctionEnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_AVH_FunctionEnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_AVH_FunctionEnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_AVH_FunctionEnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_AVH_FunctionEnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_AVH_FunctionEnableSwSts);
      }
    }
  }
  IlLeaveCriticalIHU_AVH_FunctionEnableSwSts();
}


#endif

/* Handle:  185,Name:            IHU_PDC_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
void IlPutTxIHU_PDC_EnableSwSts(vuint8 sigData)
{
  vuint8 active = IHU_PDC_EnableSwStsIsActive(sigData);
  IlEnterCriticalIHU_PDC_EnableSwSts();
  IHU_1.IHU_1.IHU_PDC_EnableSwSts = ((vuint8) (sigData & (vuint8) 0x03));
  if(active != 0)
  {
    if(ilIfActiveFlags.IHU_1.IHU_PDC_EnableSwSts == 0)
    {
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStartEvent(IlTxSigHndIHU_PDC_EnableSwSts);
      }
      ilIfActiveFlags.IHU_1.IHU_PDC_EnableSwSts = 1;
    }
  }
  else
  {
    if(ilIfActiveFlags.IHU_1.IHU_PDC_EnableSwSts != 0)
    {
      ilIfActiveFlags.IHU_1.IHU_PDC_EnableSwSts = 0;
      ilTxNSendCounter[IlTxSigHndIHU_PDC_EnableSwSts] = IlGetTxRepetitionCounter(IlTxSigHndIHU_PDC_EnableSwSts);
      if( * (vuint8*) ( & ilIfActiveFlags.IHU_1) == 0)
      {
        (void) IlStopEvent(IlTxSigHndIHU_PDC_EnableSwSts);
      }
    }
  }
  IlLeaveCriticalIHU_PDC_EnableSwSts();
}


#endif



#if defined(IL_ENABLE_TX)
V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 IlTxIndirection[kIlNumberOfTxObjects] = 
{
  1 /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  2 /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  3 /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  4 /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  5 /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  6 /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  7 /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  8 /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  9 /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  10 /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  11 /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  12 /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  13 /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  14 /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  15 /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  16 /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  17 /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  18 /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  19 /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif


/* -----------------------------------------------------------------------------
    &&&~ CAN handle to Il start stop handle
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX) && defined(C_ENABLE_CAN_CANCEL_NOTIFICATION)
V_MEMROM0 V_MEMROM1 IlStartStopHnd V_MEMROM2 IlCanHndToIlHnd[kIlCanNumberOfTxObjects] = 
{
  { 0, 0 } /* no Il message */, 
  { 0, 1 } /* start - stop  ID: 0x00000537, IHU_ADAS_10 [BC] */, 
  { 1, 2 } /* start - stop  ID: 0x000004fc, IHU_ADAS_11 [BC] */, 
  { 2, 3 } /* start - stop  ID: 0x000003be, IHU_18 [BC] */, 
  { 3, 4 } /* start - stop  ID: 0x000003bd, IHU_13 [BC] */, 
  { 4, 5 } /* start - stop  ID: 0x000003bc, IHU_12 [BC] */, 
  { 5, 6 } /* start - stop  ID: 0x000003bb, IHU_11 [BC] */, 
  { 6, 7 } /* start - stop  ID: 0x000003ba, IHU_10 [BC] */, 
  { 7, 8 } /* start - stop  ID: 0x000003b8, IHU_8 [BC] */, 
  { 8, 9 } /* start - stop  ID: 0x000003b7, IHU_7 [BC] */, 
  { 9, 10 } /* start - stop  ID: 0x000003b6, IHU_6 [BC] */, 
  { 10, 11 } /* start - stop  ID: 0x000003b5, IHU_5 [BC] */, 
  { 11, 12 } /* start - stop  ID: 0x000003b4, IHU_4 [BC] */, 
  { 12, 13 } /* start - stop  ID: 0x000003b3, IHU_3 [BC] */, 
  { 13, 14 } /* start - stop  ID: 0x000003af, IHU_17 [BC] */, 
  { 14, 15 } /* start - stop  ID: 0x000003ae, IHU_16 [BC] */, 
  { 15, 16 } /* start - stop  ID: 0x000003ad, IHU_15 [BC] */, 
  { 16, 17 } /* start - stop  ID: 0x000003ac, IHU_14 [BC] */, 
  { 17, 18 } /* start - stop  ID: 0x000003aa, IHU_1 [BC] */, 
  { 18, 19 } /* start - stop  ID: 0x00000293, IHU_MFS [BC] */
};
#endif




/* -----------------------------------------------------------------------------
    &&&~ TxTypes for interaction layer tx messages 
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX)
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 IlTxType[kIlNumberOfTxObjects] = 
{
  kTxSendCyclic /* ID: 0x00000537, Handle: 0, IHU_ADAS_10 [BC] */, 
  kTxNoTxType /* ID: 0x000004fc, Handle: 1, IHU_ADAS_11 [BC] */, 
  kTxNoTxType /* ID: 0x000003be, Handle: 2, IHU_18 [BC] */, 
  kTxNoTxType /* ID: 0x000003bd, Handle: 3, IHU_13 [BC] */, 
  kTxSendCyclic /* ID: 0x000003bc, Handle: 4, IHU_12 [BC] */, 
  kTxSendCyclic /* ID: 0x000003bb, Handle: 5, IHU_11 [BC] */, 
  kTxNoTxType /* ID: 0x000003ba, Handle: 6, IHU_10 [BC] */, 
  kTxNoTxType /* ID: 0x000003b8, Handle: 7, IHU_8 [BC] */, 
  kTxNoTxType /* ID: 0x000003b7, Handle: 8, IHU_7 [BC] */, 
  kTxSendCyclic /* ID: 0x000003b6, Handle: 9, IHU_6 [BC] */, 
  kTxNoTxType /* ID: 0x000003b5, Handle: 10, IHU_5 [BC] */, 
  kTxNoTxType /* ID: 0x000003b4, Handle: 11, IHU_4 [BC] */, 
  kTxNoTxType /* ID: 0x000003b3, Handle: 12, IHU_3 [BC] */, 
  kTxNoTxType /* ID: 0x000003af, Handle: 13, IHU_17 [BC] */, 
  kTxNoTxType /* ID: 0x000003ae, Handle: 14, IHU_16 [BC] */, 
  kTxSendCyclic /* ID: 0x000003ad, Handle: 15, IHU_15 [BC] */, 
  kTxSendCyclic /* ID: 0x000003ac, Handle: 16, IHU_14 [BC] */, 
  kTxSendCyclic /* ID: 0x000003aa, Handle: 17, IHU_1 [BC] */, 
  kTxSendCyclic /* ID: 0x00000293, Handle: 18, IHU_MFS [BC] */
};
#endif




