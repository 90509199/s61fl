/* -----------------------------------------------------------------------------
  Filename:    can_par.h
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: DrvCan__base
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\Documents and Settings\90506667\桌面\sh52\新建文件夹\S61EV_FLV1.4_2023_03_04.dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__CAN_PAR_H__)
#define __CAN_PAR_H__

#include "can_cfg.h"
#include "v_inc.h"
#include "drv_par.h"

/* -----------------------------------------------------------------------------
    &&&~ Extern declarations of callback functions
 ----------------------------------------------------------------------------- */

#if defined(C_MULTIPLE_RECEIVE_CHANNEL) || defined(C_SINGLE_RECEIVE_CHANNEL)
#endif


/* -----------------------------------------------------------------------------
    &&&~ Extern declarations of confirmation functions
 ----------------------------------------------------------------------------- */

/* CODE CATEGORY 1 START */
extern void CanNm_NmMsgConfirmation(CanTransmitHandle txObject);
/* CODE CATEGORY 1 END */





/* -----------------------------------------------------------------------------
    &&&~ Handles of send objects
 ----------------------------------------------------------------------------- */

#define CanTxNM_IHU                          0
#define CanTxIHU_ADAS_10                     1
#define CanTxIHU_ADAS_11                     2
#define CanTxIHU_18                          3
#define CanTxIHU_13                          4
#define CanTxIHU_12                          5
#define CanTxIHU_11                          6
#define CanTxIHU_10                          7
#define CanTxIHU_8                           8
#define CanTxIHU_7                           9
#define CanTxIHU_6                           10
#define CanTxIHU_5                           11
#define CanTxIHU_4                           12
#define CanTxIHU_3                           13
#define CanTxIHU_17                          14
#define CanTxIHU_16                          15
#define CanTxIHU_15                          16
#define CanTxIHU_14                          17
#define CanTxIHU_1                           18
#define CanTxIHU_MFS                         19


/* -----------------------------------------------------------------------------
    &&&~ Access to confirmation flags
 ----------------------------------------------------------------------------- */

#define IHU_ADAS_10_conf_b                   (CanConfirmationFlags.w[0].b0)
#define CanWriteSyncIHU_ADAS_10_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_ADAS_10_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_ADAS_11_conf_b                   (CanConfirmationFlags.w[0].b1)
#define CanWriteSyncIHU_ADAS_11_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_ADAS_11_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_18_conf_b                        (CanConfirmationFlags.w[0].b2)
#define CanWriteSyncIHU_18_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_18_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_13_conf_b                        (CanConfirmationFlags.w[0].b3)
#define CanWriteSyncIHU_13_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_13_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_12_conf_b                        (CanConfirmationFlags.w[0].b4)
#define CanWriteSyncIHU_12_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_12_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_11_conf_b                        (CanConfirmationFlags.w[0].b5)
#define CanWriteSyncIHU_11_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_11_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_10_conf_b                        (CanConfirmationFlags.w[0].b6)
#define CanWriteSyncIHU_10_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_10_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_8_conf_b                         (CanConfirmationFlags.w[0].b7)
#define CanWriteSyncIHU_8_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_8_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_7_conf_b                         (CanConfirmationFlags.w[0].b10)
#define CanWriteSyncIHU_7_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_7_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_6_conf_b                         (CanConfirmationFlags.w[0].b11)
#define CanWriteSyncIHU_6_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_6_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_5_conf_b                         (CanConfirmationFlags.w[0].b12)
#define CanWriteSyncIHU_5_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_5_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_4_conf_b                         (CanConfirmationFlags.w[0].b13)
#define CanWriteSyncIHU_4_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_4_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_3_conf_b                         (CanConfirmationFlags.w[0].b14)
#define CanWriteSyncIHU_3_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_3_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_17_conf_b                        (CanConfirmationFlags.w[0].b15)
#define CanWriteSyncIHU_17_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_17_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_16_conf_b                        (CanConfirmationFlags.w[0].b16)
#define CanWriteSyncIHU_16_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_16_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_15_conf_b                        (CanConfirmationFlags.w[0].b17)
#define CanWriteSyncIHU_15_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_15_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_14_conf_b                        (CanConfirmationFlags.w[0].b20)
#define CanWriteSyncIHU_14_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_14_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_1_conf_b                         (CanConfirmationFlags.w[0].b21)
#define CanWriteSyncIHU_1_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_1_conf_b = (x); \
  CanEndFlagWriteSync(); \
}
#define IHU_MFS_conf_b                       (CanConfirmationFlags.w[0].b22)
#define CanWriteSyncIHU_MFS_conf_b(x) \
{ \
  CanStartFlagWriteSync(); \
  IHU_MFS_conf_b = (x); \
  CanEndFlagWriteSync(); \
}


/* -----------------------------------------------------------------------------
    &&&~ Handles of receive objects
 ----------------------------------------------------------------------------- */

#define CanRxCGW_SAS_2                       0
#define CanRxCGW_FCM_FRM_5                   1
#define CanRxCGW_FCM_5                       2
#define CanRxWCM_1                           3
#define CanRxAVAS_1                          4
#define CanRxTBOX_7                          5
#define CanRxCGW_OBC_1                       6
#define CanRxBCM_ALM                         7
#define CanRxCGW_BCM_SCM_3                   8
#define CanRxCGW_BCM_SCM_2                   9
#define CanRxCGW_BCM_SCM_1                   10
#define CanRxCGW_BCM_FM                      11
#define CanRxBCM_6                           12
#define CanRxCGW_BCM_TPMS_2                  13
#define CanRxCGW_FCM_10                      14
#define CanRxCGW_FCM_FRM_9                   15
#define CanRxCGW_FCM_FRM_8                   16
#define CanRxCGW_FCM_7                       17
#define CanRxCGW_FCM_6                       18
#define CanRxCGW_BCM_ACCM_3                  19
#define CanRxCGW_BCM_ACCM_1                  20
#define CanRxICM_2                           21
#define CanRxCGW_FCM_FRM_6                   22
#define CanRxCGW_BMS_9                       23
#define CanRxCGW_VCU_E_3                     24
#define CanRxCGW_PLG_1                       25
#define CanRxRCM_1                           26
#define CanRxBCM_TPMS_1                      27
#define CanRxCGW_BCM_4                       28
#define CanRxCGW_BCM_5                       29
#define CanRxCGW_VCU_B_5                     30
#define CanRxCGW_VCU_B_4                     31
#define CanRxCGW_VCU_B_3                     32
#define CanRxCGW_FCM_2                       33
#define CanRxCGW_BSD_1                       34
#define CanRxCGW_BCM_MFS_1                   35
#define CanRxCGW_BCM_PEPS_1                  36
#define CanRxCGW_VCU_B_2                     37
#define CanRxICM_1                           38
#define CanRxCGW_BMS_3                       39
#define CanRxCGW_BCM_1                       40
#define CanRxCGW_EPS_1                       41
#define CanRxCGW_BCS_EPB_1                   42
#define CanRxCGW_BCS_C_1                     43
#define CanRxCGW_EGS_1                       44
#define CanRxCGW_VCU_P_5                     45


/* -----------------------------------------------------------------------------
    &&&~ Access to data bytes of Rx messages
 ----------------------------------------------------------------------------- */

/* ID: 0x00000504, Handle: 0, CGW_SAS_2 [FC] */
#define c1_CGW_SAS_2_c                       (CGW_SAS_2._c[0])
#define c2_CGW_SAS_2_c                       (CGW_SAS_2._c[1])
#define c3_CGW_SAS_2_c                       (CGW_SAS_2._c[2])
#define c4_CGW_SAS_2_c                       (CGW_SAS_2._c[3])

/* ID: 0x000004e3, Handle: 1, CGW_FCM_FRM_5 [BC] */
#define c1_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[0])
#define c2_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[1])
#define c3_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[2])
#define c4_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[3])
#define c5_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[4])
#define c6_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[5])
#define c7_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[6])
#define c8_CGW_FCM_FRM_5_c                   (CGW_FCM_FRM_5._c[7])

/* ID: 0x000004dd, Handle: 2, CGW_FCM_5 [FC] */
#define c1_CGW_FCM_5_c                       (CGW_FCM_5._c[0])
#define c2_CGW_FCM_5_c                       (CGW_FCM_5._c[1])
#define c3_CGW_FCM_5_c                       (CGW_FCM_5._c[2])

/* ID: 0x000004ba, Handle: 3, WCM_1 [FC] */
#define c1_WCM_1_c                           (WCM_1._c[0])
#define c2_WCM_1_c                           (WCM_1._c[1])
#define c3_WCM_1_c                           (WCM_1._c[2])
#define c4_WCM_1_c                           (WCM_1._c[3])
#define c5_WCM_1_c                           (WCM_1._c[4])
#define c6_WCM_1_c                           (WCM_1._c[5])
#define c7_WCM_1_c                           (WCM_1._c[6])

/* ID: 0x000004b1, Handle: 4, AVAS_1 [FC] */
#define c1_AVAS_1_c                          (AVAS_1._c[0])

/* ID: 0x000004a8, Handle: 5, TBOX_7 [FC] */
#define c1_TBOX_7_c                          (TBOX_7._c[0])
#define c2_TBOX_7_c                          (TBOX_7._c[1])
#define c3_TBOX_7_c                          (TBOX_7._c[2])
#define c4_TBOX_7_c                          (TBOX_7._c[3])
#define c5_TBOX_7_c                          (TBOX_7._c[4])
#define c6_TBOX_7_c                          (TBOX_7._c[5])

/* ID: 0x00000461, Handle: 6, CGW_OBC_1 [BC] */
#define c1_CGW_OBC_1_c                       (CGW_OBC_1._c[0])
#define c2_CGW_OBC_1_c                       (CGW_OBC_1._c[1])
#define c3_CGW_OBC_1_c                       (CGW_OBC_1._c[2])
#define c4_CGW_OBC_1_c                       (CGW_OBC_1._c[3])
#define c5_CGW_OBC_1_c                       (CGW_OBC_1._c[4])
#define c6_CGW_OBC_1_c                       (CGW_OBC_1._c[5])
#define c7_CGW_OBC_1_c                       (CGW_OBC_1._c[6])

/* ID: 0x0000045d, Handle: 7, BCM_ALM [FC] */
#define c1_BCM_ALM_c                         (BCM_ALM._c[0])
#define c2_BCM_ALM_c                         (BCM_ALM._c[1])
#define c3_BCM_ALM_c                         (BCM_ALM._c[2])
#define c4_BCM_ALM_c                         (BCM_ALM._c[3])
#define c5_BCM_ALM_c                         (BCM_ALM._c[4])
#define c6_BCM_ALM_c                         (BCM_ALM._c[5])
#define c7_BCM_ALM_c                         (BCM_ALM._c[6])
#define c8_BCM_ALM_c                         (BCM_ALM._c[7])

/* ID: 0x0000045c, Handle: 8, CGW_BCM_SCM_3 [FC] */
#define c1_CGW_BCM_SCM_3_c                   (CGW_BCM_SCM_3._c[0])
#define c2_CGW_BCM_SCM_3_c                   (CGW_BCM_SCM_3._c[1])
#define c3_CGW_BCM_SCM_3_c                   (CGW_BCM_SCM_3._c[2])
#define c4_CGW_BCM_SCM_3_c                   (CGW_BCM_SCM_3._c[3])

/* ID: 0x0000045b, Handle: 9, CGW_BCM_SCM_2 [FC] */
#define c1_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[0])
#define c2_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[1])
#define c3_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[2])
#define c4_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[3])
#define c5_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[4])
#define c6_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[5])
#define c7_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[6])
#define c8_CGW_BCM_SCM_2_c                   (CGW_BCM_SCM_2._c[7])

/* ID: 0x0000045a, Handle: 10, CGW_BCM_SCM_1 [FC] */
#define c1_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[0])
#define c2_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[1])
#define c3_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[2])
#define c4_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[3])
#define c5_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[4])
#define c6_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[5])
#define c7_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[6])
#define c8_CGW_BCM_SCM_1_c                   (CGW_BCM_SCM_1._c[7])

/* ID: 0x00000458, Handle: 11, CGW_BCM_FM [FC] */
#define c1_CGW_BCM_FM_c                      (CGW_BCM_FM._c[0])
#define c2_CGW_BCM_FM_c                      (CGW_BCM_FM._c[1])
#define c3_CGW_BCM_FM_c                      (CGW_BCM_FM._c[2])
#define c4_CGW_BCM_FM_c                      (CGW_BCM_FM._c[3])

/* ID: 0x00000457, Handle: 12, BCM_6 [FC] */
#define c1_BCM_6_c                           (BCM_6._c[0])
#define c2_BCM_6_c                           (BCM_6._c[1])
#define c3_BCM_6_c                           (BCM_6._c[2])
#define c4_BCM_6_c                           (BCM_6._c[3])
#define c5_BCM_6_c                           (BCM_6._c[4])
#define c6_BCM_6_c                           (BCM_6._c[5])
#define c7_BCM_6_c                           (BCM_6._c[6])
#define c8_BCM_6_c                           (BCM_6._c[7])

/* ID: 0x00000455, Handle: 13, CGW_BCM_TPMS_2 [FC] */
#define c1_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[0])
#define c2_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[1])
#define c3_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[2])
#define c4_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[3])
#define c5_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[4])
#define c6_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[5])
#define c7_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[6])
#define c8_CGW_BCM_TPMS_2_c                  (CGW_BCM_TPMS_2._c[7])

/* ID: 0x000003fc, Handle: 14, CGW_FCM_10 [FC] */
#define c1_CGW_FCM_10_c                      (CGW_FCM_10._c[0])
#define c2_CGW_FCM_10_c                      (CGW_FCM_10._c[1])
#define c3_CGW_FCM_10_c                      (CGW_FCM_10._c[2])
#define c4_CGW_FCM_10_c                      (CGW_FCM_10._c[3])
#define c5_CGW_FCM_10_c                      (CGW_FCM_10._c[4])
#define c6_CGW_FCM_10_c                      (CGW_FCM_10._c[5])
#define c7_CGW_FCM_10_c                      (CGW_FCM_10._c[6])

/* ID: 0x000003fa, Handle: 15, CGW_FCM_FRM_9 [FC] */
#define c1_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[0])
#define c2_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[1])
#define c3_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[2])
#define c4_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[3])
#define c5_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[4])
#define c6_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[5])
#define c7_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[6])
#define c8_CGW_FCM_FRM_9_c                   (CGW_FCM_FRM_9._c[7])

/* ID: 0x000003ed, Handle: 16, CGW_FCM_FRM_8 [FC] */
#define c1_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[0])
#define c2_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[1])
#define c3_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[2])
#define c4_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[3])
#define c5_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[4])
#define c6_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[5])
#define c7_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[6])
#define c8_CGW_FCM_FRM_8_c                   (CGW_FCM_FRM_8._c[7])

/* ID: 0x000003de, Handle: 17, CGW_FCM_7 [FC] */
#define c1_CGW_FCM_7_c                       (CGW_FCM_7._c[0])
#define c2_CGW_FCM_7_c                       (CGW_FCM_7._c[1])
#define c3_CGW_FCM_7_c                       (CGW_FCM_7._c[2])
#define c4_CGW_FCM_7_c                       (CGW_FCM_7._c[3])
#define c5_CGW_FCM_7_c                       (CGW_FCM_7._c[4])
#define c6_CGW_FCM_7_c                       (CGW_FCM_7._c[5])

/* ID: 0x000003dc, Handle: 18, CGW_FCM_6 [FC] */
#define c1_CGW_FCM_6_c                       (CGW_FCM_6._c[0])
#define c2_CGW_FCM_6_c                       (CGW_FCM_6._c[1])
#define c3_CGW_FCM_6_c                       (CGW_FCM_6._c[2])
#define c4_CGW_FCM_6_c                       (CGW_FCM_6._c[3])
#define c5_CGW_FCM_6_c                       (CGW_FCM_6._c[4])
#define c6_CGW_FCM_6_c                       (CGW_FCM_6._c[5])

/* ID: 0x000003d4, Handle: 19, CGW_BCM_ACCM_3 [FC] */
#define c1_CGW_BCM_ACCM_3_c                  (CGW_BCM_ACCM_3._c[0])
#define c2_CGW_BCM_ACCM_3_c                  (CGW_BCM_ACCM_3._c[1])
#define c3_CGW_BCM_ACCM_3_c                  (CGW_BCM_ACCM_3._c[2])
#define c4_CGW_BCM_ACCM_3_c                  (CGW_BCM_ACCM_3._c[3])
#define c5_CGW_BCM_ACCM_3_c                  (CGW_BCM_ACCM_3._c[4])
#define c6_CGW_BCM_ACCM_3_c                  (CGW_BCM_ACCM_3._c[5])

/* ID: 0x000003d2, Handle: 20, CGW_BCM_ACCM_1 [BC] */
#define c1_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[0])
#define c2_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[1])
#define c3_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[2])
#define c4_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[3])
#define c5_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[4])
#define c6_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[5])
#define c7_CGW_BCM_ACCM_1_c                  (CGW_BCM_ACCM_1._c[6])

/* ID: 0x000003b1, Handle: 21, ICM_2 [BC] */
#define c1_ICM_2_c                           (ICM_2._c[0])
#define c2_ICM_2_c                           (ICM_2._c[1])
#define c3_ICM_2_c                           (ICM_2._c[2])
#define c4_ICM_2_c                           (ICM_2._c[3])
#define c5_ICM_2_c                           (ICM_2._c[4])

/* ID: 0x00000387, Handle: 22, CGW_FCM_FRM_6 [BC] */
#define c1_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[0])
#define c2_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[1])
#define c3_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[2])
#define c4_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[3])
#define c5_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[4])
#define c6_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[5])
#define c7_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[6])
#define c8_CGW_FCM_FRM_6_c                   (CGW_FCM_FRM_6._c[7])

/* ID: 0x00000355, Handle: 23, CGW_BMS_9 [BC] */
#define c1_CGW_BMS_9_c                       (CGW_BMS_9._c[0])
#define c2_CGW_BMS_9_c                       (CGW_BMS_9._c[1])
#define c3_CGW_BMS_9_c                       (CGW_BMS_9._c[2])
#define c4_CGW_BMS_9_c                       (CGW_BMS_9._c[3])
#define c5_CGW_BMS_9_c                       (CGW_BMS_9._c[4])
#define c6_CGW_BMS_9_c                       (CGW_BMS_9._c[5])
#define c7_CGW_BMS_9_c                       (CGW_BMS_9._c[6])

/* ID: 0x00000347, Handle: 24, CGW_VCU_E_3 [BC] */
#define c1_CGW_VCU_E_3_c                     (CGW_VCU_E_3._c[0])
#define c2_CGW_VCU_E_3_c                     (CGW_VCU_E_3._c[1])
#define c3_CGW_VCU_E_3_c                     (CGW_VCU_E_3._c[2])
#define c4_CGW_VCU_E_3_c                     (CGW_VCU_E_3._c[3])
#define c5_CGW_VCU_E_3_c                     (CGW_VCU_E_3._c[4])

/* ID: 0x0000032a, Handle: 25, CGW_PLG_1 [BC] */
#define c1_CGW_PLG_1_c                       (CGW_PLG_1._c[0])
#define c2_CGW_PLG_1_c                       (CGW_PLG_1._c[1])
#define c3_CGW_PLG_1_c                       (CGW_PLG_1._c[2])
#define c4_CGW_PLG_1_c                       (CGW_PLG_1._c[3])
#define c5_CGW_PLG_1_c                       (CGW_PLG_1._c[4])

/* ID: 0x00000328, Handle: 26, RCM_1 [BC] */
#define c1_RCM_1_c                           (RCM_1._c[0])
#define c2_RCM_1_c                           (RCM_1._c[1])
#define c3_RCM_1_c                           (RCM_1._c[2])
#define c4_RCM_1_c                           (RCM_1._c[3])
#define c5_RCM_1_c                           (RCM_1._c[4])
#define c6_RCM_1_c                           (RCM_1._c[5])
#define c7_RCM_1_c                           (RCM_1._c[6])
#define c8_RCM_1_c                           (RCM_1._c[7])

/* ID: 0x00000326, Handle: 27, BCM_TPMS_1 [BC] */
#define c1_BCM_TPMS_1_c                      (BCM_TPMS_1._c[0])
#define c2_BCM_TPMS_1_c                      (BCM_TPMS_1._c[1])
#define c3_BCM_TPMS_1_c                      (BCM_TPMS_1._c[2])
#define c4_BCM_TPMS_1_c                      (BCM_TPMS_1._c[3])
#define c5_BCM_TPMS_1_c                      (BCM_TPMS_1._c[4])

/* ID: 0x00000324, Handle: 28, CGW_BCM_4 [BC] */
#define c1_CGW_BCM_4_c                       (CGW_BCM_4._c[0])
#define c2_CGW_BCM_4_c                       (CGW_BCM_4._c[1])
#define c3_CGW_BCM_4_c                       (CGW_BCM_4._c[2])
#define c4_CGW_BCM_4_c                       (CGW_BCM_4._c[3])
#define c5_CGW_BCM_4_c                       (CGW_BCM_4._c[4])
#define c6_CGW_BCM_4_c                       (CGW_BCM_4._c[5])
#define c7_CGW_BCM_4_c                       (CGW_BCM_4._c[6])
#define c8_CGW_BCM_4_c                       (CGW_BCM_4._c[7])

/* ID: 0x00000323, Handle: 29, CGW_BCM_5 [BC] */
#define c1_CGW_BCM_5_c                       (CGW_BCM_5._c[0])
#define c2_CGW_BCM_5_c                       (CGW_BCM_5._c[1])
#define c3_CGW_BCM_5_c                       (CGW_BCM_5._c[2])
#define c4_CGW_BCM_5_c                       (CGW_BCM_5._c[3])
#define c5_CGW_BCM_5_c                       (CGW_BCM_5._c[4])
#define c6_CGW_BCM_5_c                       (CGW_BCM_5._c[5])
#define c7_CGW_BCM_5_c                       (CGW_BCM_5._c[6])
#define c8_CGW_BCM_5_c                       (CGW_BCM_5._c[7])

/* ID: 0x00000315, Handle: 30, CGW_VCU_B_5 [BC] */
#define c1_CGW_VCU_B_5_c                     (CGW_VCU_B_5._c[0])
#define c2_CGW_VCU_B_5_c                     (CGW_VCU_B_5._c[1])
#define c3_CGW_VCU_B_5_c                     (CGW_VCU_B_5._c[2])

/* ID: 0x00000314, Handle: 31, CGW_VCU_B_4 [FC] */
#define c1_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[0])
#define c2_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[1])
#define c3_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[2])
#define c4_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[3])
#define c5_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[4])
#define c6_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[5])
#define c7_CGW_VCU_B_4_c                     (CGW_VCU_B_4._c[6])

/* ID: 0x00000313, Handle: 32, CGW_VCU_B_3 [FC] */
#define c1_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[0])
#define c2_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[1])
#define c3_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[2])
#define c4_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[3])
#define c5_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[4])
#define c6_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[5])
#define c7_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[6])
#define c8_CGW_VCU_B_3_c                     (CGW_VCU_B_3._c[7])

/* ID: 0x00000307, Handle: 33, CGW_FCM_2 [BC] */
#define c1_CGW_FCM_2_c                       (CGW_FCM_2._c[0])
#define c2_CGW_FCM_2_c                       (CGW_FCM_2._c[1])
#define c3_CGW_FCM_2_c                       (CGW_FCM_2._c[2])
#define c4_CGW_FCM_2_c                       (CGW_FCM_2._c[3])
#define c5_CGW_FCM_2_c                       (CGW_FCM_2._c[4])
#define c6_CGW_FCM_2_c                       (CGW_FCM_2._c[5])

/* ID: 0x00000298, Handle: 34, CGW_BSD_1 [BC] */
#define c1_CGW_BSD_1_c                       (CGW_BSD_1._c[0])

/* ID: 0x00000294, Handle: 35, CGW_BCM_MFS_1 [BC] */
#define c1_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[0])
#define c2_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[1])
#define c3_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[2])
#define c4_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[3])
#define c5_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[4])
#define c6_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[5])
#define c7_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[6])
#define c8_CGW_BCM_MFS_1_c                   (CGW_BCM_MFS_1._c[7])

/* ID: 0x00000291, Handle: 36, CGW_BCM_PEPS_1 [BC] */
#define c1_CGW_BCM_PEPS_1_c                  (CGW_BCM_PEPS_1._c[0])
#define c2_CGW_BCM_PEPS_1_c                  (CGW_BCM_PEPS_1._c[1])
#define c3_CGW_BCM_PEPS_1_c                  (CGW_BCM_PEPS_1._c[2])
#define c4_CGW_BCM_PEPS_1_c                  (CGW_BCM_PEPS_1._c[3])
#define c5_CGW_BCM_PEPS_1_c                  (CGW_BCM_PEPS_1._c[4])

/* ID: 0x0000028a, Handle: 37, CGW_VCU_B_2 [BC] */
#define c1_CGW_VCU_B_2_c                     (CGW_VCU_B_2._c[0])
#define c2_CGW_VCU_B_2_c                     (CGW_VCU_B_2._c[1])
#define c3_CGW_VCU_B_2_c                     (CGW_VCU_B_2._c[2])
#define c4_CGW_VCU_B_2_c                     (CGW_VCU_B_2._c[3])
#define c5_CGW_VCU_B_2_c                     (CGW_VCU_B_2._c[4])
#define c6_CGW_VCU_B_2_c                     (CGW_VCU_B_2._c[5])

/* ID: 0x00000271, Handle: 38, ICM_1 [BC] */
#define c1_ICM_1_c                           (ICM_1._c[0])
#define c2_ICM_1_c                           (ICM_1._c[1])
#define c3_ICM_1_c                           (ICM_1._c[2])
#define c4_ICM_1_c                           (ICM_1._c[3])

/* ID: 0x0000025a, Handle: 39, CGW_BMS_3 [FC] */
#define c1_CGW_BMS_3_c                       (CGW_BMS_3._c[0])
#define c2_CGW_BMS_3_c                       (CGW_BMS_3._c[1])
#define c3_CGW_BMS_3_c                       (CGW_BMS_3._c[2])

/* ID: 0x00000245, Handle: 40, CGW_BCM_1 [BC] */
#define c1_CGW_BCM_1_c                       (CGW_BCM_1._c[0])
#define c2_CGW_BCM_1_c                       (CGW_BCM_1._c[1])
#define c3_CGW_BCM_1_c                       (CGW_BCM_1._c[2])
#define c4_CGW_BCM_1_c                       (CGW_BCM_1._c[3])
#define c5_CGW_BCM_1_c                       (CGW_BCM_1._c[4])
#define c6_CGW_BCM_1_c                       (CGW_BCM_1._c[5])

/* ID: 0x00000238, Handle: 41, CGW_EPS_1 [BC] */
#define c1_CGW_EPS_1_c                       (CGW_EPS_1._c[0])

/* ID: 0x00000231, Handle: 42, CGW_BCS_EPB_1 [FC] */
#define c1_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[0])
#define c2_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[1])
#define c3_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[2])
#define c4_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[3])
#define c5_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[4])
#define c6_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[5])
#define c7_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[6])
#define c8_CGW_BCS_EPB_1_c                   (CGW_BCS_EPB_1._c[7])

/* ID: 0x00000225, Handle: 43, CGW_BCS_C_1 [BC] */
#define c1_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[0])
#define c2_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[1])
#define c3_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[2])
#define c4_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[3])
#define c5_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[4])
#define c6_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[5])
#define c7_CGW_BCS_C_1_c                     (CGW_BCS_C_1._c[6])

/* ID: 0x00000215, Handle: 44, CGW_EGS_1 [BC] */
#define c1_CGW_EGS_1_c                       (CGW_EGS_1._c[0])
#define c2_CGW_EGS_1_c                       (CGW_EGS_1._c[1])
#define c3_CGW_EGS_1_c                       (CGW_EGS_1._c[2])

/* ID: 0x00000123, Handle: 45, CGW_VCU_P_5 [FC] */
#define c1_CGW_VCU_P_5_c                     (CGW_VCU_P_5._c[0])
#define c2_CGW_VCU_P_5_c                     (CGW_VCU_P_5._c[1])
#define c3_CGW_VCU_P_5_c                     (CGW_VCU_P_5._c[2])
#define c4_CGW_VCU_P_5_c                     (CGW_VCU_P_5._c[3])
#define c5_CGW_VCU_P_5_c                     (CGW_VCU_P_5._c[4])
#define c6_CGW_VCU_P_5_c                     (CGW_VCU_P_5._c[5])



/* -----------------------------------------------------------------------------
    &&&~ Access to data bytes of Tx messages
 ----------------------------------------------------------------------------- */

/* ID: 0x00000618, Handle: 0, NM_IHU [BC] */
#define c1_NM_IHU_c                          (NM_IHU._c[0])
#define c2_NM_IHU_c                          (NM_IHU._c[1])
#define c3_NM_IHU_c                          (NM_IHU._c[2])
#define c4_NM_IHU_c                          (NM_IHU._c[3])
#define c5_NM_IHU_c                          (NM_IHU._c[4])
#define c6_NM_IHU_c                          (NM_IHU._c[5])
#define c7_NM_IHU_c                          (NM_IHU._c[6])
#define c8_NM_IHU_c                          (NM_IHU._c[7])

/* ID: 0x00000537, Handle: 1, IHU_ADAS_10 [BC] */
#define c1_IHU_ADAS_10_c                     (IHU_ADAS_10._c[0])
#define c2_IHU_ADAS_10_c                     (IHU_ADAS_10._c[1])
#define c3_IHU_ADAS_10_c                     (IHU_ADAS_10._c[2])
#define c4_IHU_ADAS_10_c                     (IHU_ADAS_10._c[3])
#define c5_IHU_ADAS_10_c                     (IHU_ADAS_10._c[4])
#define c6_IHU_ADAS_10_c                     (IHU_ADAS_10._c[5])
#define c7_IHU_ADAS_10_c                     (IHU_ADAS_10._c[6])
#define c8_IHU_ADAS_10_c                     (IHU_ADAS_10._c[7])

/* ID: 0x000004fc, Handle: 2, IHU_ADAS_11 [BC] */
#define c1_IHU_ADAS_11_c                     (IHU_ADAS_11._c[0])
#define c2_IHU_ADAS_11_c                     (IHU_ADAS_11._c[1])
#define c3_IHU_ADAS_11_c                     (IHU_ADAS_11._c[2])
#define c4_IHU_ADAS_11_c                     (IHU_ADAS_11._c[3])
#define c5_IHU_ADAS_11_c                     (IHU_ADAS_11._c[4])
#define c6_IHU_ADAS_11_c                     (IHU_ADAS_11._c[5])
#define c7_IHU_ADAS_11_c                     (IHU_ADAS_11._c[6])
#define c8_IHU_ADAS_11_c                     (IHU_ADAS_11._c[7])

/* ID: 0x000003be, Handle: 3, IHU_18 [BC] */
#define c1_IHU_18_c                          (IHU_18._c[0])
#define c2_IHU_18_c                          (IHU_18._c[1])
#define c3_IHU_18_c                          (IHU_18._c[2])
#define c4_IHU_18_c                          (IHU_18._c[3])
#define c5_IHU_18_c                          (IHU_18._c[4])
#define c6_IHU_18_c                          (IHU_18._c[5])
#define c7_IHU_18_c                          (IHU_18._c[6])
#define c8_IHU_18_c                          (IHU_18._c[7])

/* ID: 0x000003bd, Handle: 4, IHU_13 [BC] */
#define c1_IHU_13_c                          (IHU_13._c[0])
#define c2_IHU_13_c                          (IHU_13._c[1])
#define c3_IHU_13_c                          (IHU_13._c[2])
#define c4_IHU_13_c                          (IHU_13._c[3])
#define c5_IHU_13_c                          (IHU_13._c[4])
#define c6_IHU_13_c                          (IHU_13._c[5])
#define c7_IHU_13_c                          (IHU_13._c[6])
#define c8_IHU_13_c                          (IHU_13._c[7])

/* ID: 0x000003bc, Handle: 5, IHU_12 [BC] */
#define c1_IHU_12_c                          (IHU_12._c[0])
#define c2_IHU_12_c                          (IHU_12._c[1])
#define c3_IHU_12_c                          (IHU_12._c[2])
#define c4_IHU_12_c                          (IHU_12._c[3])
#define c5_IHU_12_c                          (IHU_12._c[4])
#define c6_IHU_12_c                          (IHU_12._c[5])
#define c7_IHU_12_c                          (IHU_12._c[6])
#define c8_IHU_12_c                          (IHU_12._c[7])

/* ID: 0x000003bb, Handle: 6, IHU_11 [BC] */
#define c1_IHU_11_c                          (IHU_11._c[0])
#define c2_IHU_11_c                          (IHU_11._c[1])
#define c3_IHU_11_c                          (IHU_11._c[2])
#define c4_IHU_11_c                          (IHU_11._c[3])
#define c5_IHU_11_c                          (IHU_11._c[4])
#define c6_IHU_11_c                          (IHU_11._c[5])
#define c7_IHU_11_c                          (IHU_11._c[6])
#define c8_IHU_11_c                          (IHU_11._c[7])

/* ID: 0x000003ba, Handle: 7, IHU_10 [BC] */
#define c1_IHU_10_c                          (IHU_10._c[0])
#define c2_IHU_10_c                          (IHU_10._c[1])
#define c3_IHU_10_c                          (IHU_10._c[2])
#define c4_IHU_10_c                          (IHU_10._c[3])
#define c5_IHU_10_c                          (IHU_10._c[4])
#define c6_IHU_10_c                          (IHU_10._c[5])
#define c7_IHU_10_c                          (IHU_10._c[6])
#define c8_IHU_10_c                          (IHU_10._c[7])

/* ID: 0x000003b8, Handle: 8, IHU_8 [BC] */
#define c1_IHU_8_c                           (IHU_8._c[0])
#define c2_IHU_8_c                           (IHU_8._c[1])
#define c3_IHU_8_c                           (IHU_8._c[2])
#define c4_IHU_8_c                           (IHU_8._c[3])
#define c5_IHU_8_c                           (IHU_8._c[4])
#define c6_IHU_8_c                           (IHU_8._c[5])
#define c7_IHU_8_c                           (IHU_8._c[6])
#define c8_IHU_8_c                           (IHU_8._c[7])

/* ID: 0x000003b7, Handle: 9, IHU_7 [BC] */
#define c1_IHU_7_c                           (IHU_7._c[0])
#define c2_IHU_7_c                           (IHU_7._c[1])
#define c3_IHU_7_c                           (IHU_7._c[2])
#define c4_IHU_7_c                           (IHU_7._c[3])
#define c5_IHU_7_c                           (IHU_7._c[4])
#define c6_IHU_7_c                           (IHU_7._c[5])
#define c7_IHU_7_c                           (IHU_7._c[6])
#define c8_IHU_7_c                           (IHU_7._c[7])

/* ID: 0x000003b6, Handle: 10, IHU_6 [BC] */
#define c1_IHU_6_c                           (IHU_6._c[0])
#define c2_IHU_6_c                           (IHU_6._c[1])
#define c3_IHU_6_c                           (IHU_6._c[2])
#define c4_IHU_6_c                           (IHU_6._c[3])
#define c5_IHU_6_c                           (IHU_6._c[4])
#define c6_IHU_6_c                           (IHU_6._c[5])
#define c7_IHU_6_c                           (IHU_6._c[6])
#define c8_IHU_6_c                           (IHU_6._c[7])

/* ID: 0x000003b5, Handle: 11, IHU_5 [BC] */
#define c1_IHU_5_c                           (IHU_5._c[0])
#define c2_IHU_5_c                           (IHU_5._c[1])
#define c3_IHU_5_c                           (IHU_5._c[2])
#define c4_IHU_5_c                           (IHU_5._c[3])
#define c5_IHU_5_c                           (IHU_5._c[4])
#define c6_IHU_5_c                           (IHU_5._c[5])
#define c7_IHU_5_c                           (IHU_5._c[6])
#define c8_IHU_5_c                           (IHU_5._c[7])

/* ID: 0x000003b4, Handle: 12, IHU_4 [BC] */
#define c1_IHU_4_c                           (IHU_4._c[0])
#define c2_IHU_4_c                           (IHU_4._c[1])
#define c3_IHU_4_c                           (IHU_4._c[2])
#define c4_IHU_4_c                           (IHU_4._c[3])
#define c5_IHU_4_c                           (IHU_4._c[4])
#define c6_IHU_4_c                           (IHU_4._c[5])
#define c7_IHU_4_c                           (IHU_4._c[6])
#define c8_IHU_4_c                           (IHU_4._c[7])

/* ID: 0x000003b3, Handle: 13, IHU_3 [BC] */
#define c1_IHU_3_c                           (IHU_3._c[0])
#define c2_IHU_3_c                           (IHU_3._c[1])
#define c3_IHU_3_c                           (IHU_3._c[2])
#define c4_IHU_3_c                           (IHU_3._c[3])
#define c5_IHU_3_c                           (IHU_3._c[4])
#define c6_IHU_3_c                           (IHU_3._c[5])
#define c7_IHU_3_c                           (IHU_3._c[6])
#define c8_IHU_3_c                           (IHU_3._c[7])

/* ID: 0x000003af, Handle: 14, IHU_17 [BC] */
#define c1_IHU_17_c                          (IHU_17._c[0])
#define c2_IHU_17_c                          (IHU_17._c[1])
#define c3_IHU_17_c                          (IHU_17._c[2])
#define c4_IHU_17_c                          (IHU_17._c[3])
#define c5_IHU_17_c                          (IHU_17._c[4])
#define c6_IHU_17_c                          (IHU_17._c[5])
#define c7_IHU_17_c                          (IHU_17._c[6])
#define c8_IHU_17_c                          (IHU_17._c[7])

/* ID: 0x000003ae, Handle: 15, IHU_16 [BC] */
#define c1_IHU_16_c                          (IHU_16._c[0])
#define c2_IHU_16_c                          (IHU_16._c[1])
#define c3_IHU_16_c                          (IHU_16._c[2])
#define c4_IHU_16_c                          (IHU_16._c[3])
#define c5_IHU_16_c                          (IHU_16._c[4])
#define c6_IHU_16_c                          (IHU_16._c[5])
#define c7_IHU_16_c                          (IHU_16._c[6])
#define c8_IHU_16_c                          (IHU_16._c[7])

/* ID: 0x000003ad, Handle: 16, IHU_15 [BC] */
#define c1_IHU_15_c                          (IHU_15._c[0])
#define c2_IHU_15_c                          (IHU_15._c[1])
#define c3_IHU_15_c                          (IHU_15._c[2])
#define c4_IHU_15_c                          (IHU_15._c[3])
#define c5_IHU_15_c                          (IHU_15._c[4])
#define c6_IHU_15_c                          (IHU_15._c[5])
#define c7_IHU_15_c                          (IHU_15._c[6])
#define c8_IHU_15_c                          (IHU_15._c[7])

/* ID: 0x000003ac, Handle: 17, IHU_14 [BC] */
#define c1_IHU_14_c                          (IHU_14._c[0])
#define c2_IHU_14_c                          (IHU_14._c[1])
#define c3_IHU_14_c                          (IHU_14._c[2])
#define c4_IHU_14_c                          (IHU_14._c[3])
#define c5_IHU_14_c                          (IHU_14._c[4])
#define c6_IHU_14_c                          (IHU_14._c[5])
#define c7_IHU_14_c                          (IHU_14._c[6])
#define c8_IHU_14_c                          (IHU_14._c[7])

/* ID: 0x000003aa, Handle: 18, IHU_1 [BC] */
#define c1_IHU_1_c                           (IHU_1._c[0])
#define c2_IHU_1_c                           (IHU_1._c[1])
#define c3_IHU_1_c                           (IHU_1._c[2])
#define c4_IHU_1_c                           (IHU_1._c[3])
#define c5_IHU_1_c                           (IHU_1._c[4])
#define c6_IHU_1_c                           (IHU_1._c[5])
#define c7_IHU_1_c                           (IHU_1._c[6])
#define c8_IHU_1_c                           (IHU_1._c[7])

/* ID: 0x00000293, Handle: 19, IHU_MFS [BC] */
#define c1_IHU_MFS_c                         (IHU_MFS._c[0])
#define c2_IHU_MFS_c                         (IHU_MFS._c[1])
#define c3_IHU_MFS_c                         (IHU_MFS._c[2])
#define c4_IHU_MFS_c                         (IHU_MFS._c[3])
#define c5_IHU_MFS_c                         (IHU_MFS._c[4])
#define c6_IHU_MFS_c                         (IHU_MFS._c[5])
#define c7_IHU_MFS_c                         (IHU_MFS._c[6])
#define c8_IHU_MFS_c                         (IHU_MFS._c[7])



/* -----------------------------------------------------------------------------
    &&&~ RDS Access RI2.0
 ----------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
    &&&~ RDSSignalAccessStructs_RI20
 ----------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_FRM_5_RDS_msgTypeTag
{
  vbittype FCM_FRM_5_SCFONOFFSts : 2;
  vbittype FCM_FRM_5_FCWONOFFSts : 2;
  vbittype FCM_FRM_5_FCWSnvtySts : 2;
  vbittype FCM_FRM_5_AEBONOFFSts : 2;
  vbittype unused0 : 6;
  vbittype FCM_FRM_5_DAIONOFFSts : 2;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype FCM_FRM_5_MsgCntr : 4;
  vbittype unused5 : 4;
  vbittype FCM_FRM_5_CRC : 8;
} _c_CGW_FCM_FRM_5_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_OBC_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 4;
  vbittype OBC_SOC_ChargeLockEnableSwSts : 2;
  vbittype unused7 : 2;
} _c_CGW_OBC_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_ACCM_1_RDS_msgTypeTag
{
  vbittype ACCM_AUTO_Sts : 1;
  vbittype ACCM_AC_Sts : 1;
  vbittype ACCM_PTC_Sts : 1;
  vbittype ACCM_System_ON_OFF_Sts : 1;
  vbittype unused0 : 1;
  vbittype ACCM_CirculationModeSts : 2;
  vbittype unused1 : 1;
  vbittype ACCM_BlowModeSts : 3;
  vbittype ACCM_TempSetSts_Driver : 5;
  vbittype ACCM_BlowerlevelSetSts : 4;
  vbittype unused2 : 2;
  vbittype ACCM_DUAL_EnablaSts : 2;
  vbittype ACCM_AutoDefrostSts : 1;
  vbittype ACCM_VentilationSts : 1;
  vbittype unused3 : 6;
  vbittype ACCM_InternalTemp : 8;
  vbittype ACCM_ExternalTemp : 8;
  vbittype ACCM_FrontDefrostSts : 1;
  vbittype ACCM_AnionGeneratorSts : 1;
  vbittype ACCM_ECO_Mode : 1;
  vbittype ACCM_TempSetSts_Psngr : 5;
} _c_CGW_BCM_ACCM_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_ICM_2_RDS_msgTypeTag
{
  vbittype unused0 : 5;
  vbittype ICM_ThemelSetSts : 3;
  vbittype unused1 : 8;
  vbittype ICM_BrightnessAdjustSetSts : 4;
  vbittype ICM_FatigureDrivingTimeSetSts : 4;
  vbittype unused2 : 8;
  vbittype ICM_OverSpdValueSetSts : 6;
  vbittype unused3 : 2;
} _c_ICM_2_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_FRM_6_RDS_msgTypeTag
{
  vbittype FCM_FRM_6_TextinfoWarn : 4;
  vbittype FCM_FRM_6_Textinfo_Info : 4;
  vbittype FCM_FRM_6_SCF_PopoverReq : 1;
  vbittype FCM_FRM_6_TimeGapSetICM : 3;
  vbittype unused0 : 4;
  vbittype FCM_FRM_6_FCW_preWarning : 1;
  vbittype FCM_FRM_6_DistanceWarning : 1;
  vbittype FCM_FRM_6_DrvrCfmSCFDispFb : 1;
  vbittype FCM_FRM_6_SCF_SpdLimUnit : 2;
  vbittype FCM_FRM_6_SCF_SpdLimSts : 3;
  vbittype unused1 : 1;
  vbittype FCM_FRM_6_AEBMode : 3;
  vbittype FCM_FRM_6_FCWMode : 3;
  vbittype FCM_FRM_6_TakeOverReq : 1;
  vbittype unused2 : 2;
  vbittype FCM_FRM_6_ACCMode : 4;
  vbittype unused3 : 2;
  vbittype FCM_FRM_6_VSetDis_1 : 5;
  vbittype unused4 : 3;
  vbittype FCM_FRM_6_MsgCntr : 4;
  vbittype FCM_FRM_6_VSetDis_0 : 4;
  vbittype FCM_FRM_6_CRC : 8;
} _c_CGW_FCM_FRM_6_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BMS_9_RDS_msgTypeTag
{
  vbittype unused0 : 6;
  vbittype BMS_ChgWireConnectStsDisp : 2;
  vbittype unused1 : 8;
  vbittype BMS_SOC_Disp_1 : 8;
  vbittype unused2 : 2;
  vbittype BMS_SOC_Disp_0 : 6;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 4;
  vbittype BMS_ChgSts : 4;
} _c_CGW_BMS_9_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_VCU_E_3_RDS_msgTypeTag
{
  vbittype VCU_Chg_SOC_LimitSts : 6;
  vbittype VCU_BookChgModeSts : 2;
  vbittype VCU_BookChgStartTimeSts_Hour : 5;
  vbittype unused0 : 3;
  vbittype VCU_BookChgStartTimeSts_Minute : 6;
  vbittype VCU_BookChgSts1 : 2;
  vbittype VCU_BookchgStopTimeSts_Hour : 5;
  vbittype unused1 : 3;
  vbittype VCU_BookChgStopTimeSts_Minute : 6;
  vbittype VCU_BookChgSts2 : 2;
} _c_CGW_VCU_E_3_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_PLG_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 4;
  vbittype PLG_LockSts : 2;
  vbittype unused2 : 2;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype PLG_MaxPositionSetSts : 8;
} _c_CGW_PLG_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_RCM_1_RDS_msgTypeTag
{
  vbittype RCM_SnsDistance_RL : 6;
  vbittype unused0 : 2;
  vbittype RCM_SnsDistance_RMR : 6;
  vbittype unused1 : 2;
  vbittype RCM_SnsDistance_RML : 6;
  vbittype unused2 : 2;
  vbittype RCM_SnsDistance_RR : 6;
  vbittype unused3 : 2;
  vbittype RCM_SnsDistance_FL : 5;
  vbittype RCM_AudibleBeepRate : 3;
  vbittype RCM_SnsDistance_FML : 5;
  vbittype unused4 : 3;
  vbittype RCM_SnsDistance_FMR : 5;
  vbittype unused5 : 3;
  vbittype RCM_SnsDistance_FR : 5;
  vbittype RCM_WorkingSts : 1;
  vbittype RCM_DetectingSts : 2;
} _c_RCM_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_BCM_TPMS_1_RDS_msgTypeTag
{
  vbittype BCM_TirePressureWarnLampDisp : 2;
  vbittype BCM_TirePressureSystemFault : 1;
  vbittype unused0 : 5;
  vbittype BCM_TirePressureWarn_FL : 2;
  vbittype BCM_TireTempWarn_FL : 2;
  vbittype BCM_TirePressureDelta_FL : 1;
  vbittype BCM_TireSensorFault_FL : 1;
  vbittype BCM_TireSensorLowBatt_FL : 1;
  vbittype BCM_TireSensorCalibrationSts_FL : 1;
  vbittype BCM_TirePressureWarn_FR : 2;
  vbittype BCM_TireTempWarn_FR : 2;
  vbittype BCM_TirePressureDelta_FR : 1;
  vbittype BCM_TireSensorFault_FR : 1;
  vbittype BCM_TireSensorLowBatt_FR : 1;
  vbittype BCM_TireSensorCalibrationSts_FR : 1;
  vbittype BCM_TirePressureWarn_RL : 2;
  vbittype BCM_TireTempWarn_RL : 2;
  vbittype BCM_TirePressureDelta_RL : 1;
  vbittype BCM_TireSensorFault_RL : 1;
  vbittype BCM_TireSensorLowBatt_RL : 1;
  vbittype BCM_TireSensorCalibrationSts_RL : 1;
  vbittype BCM_TirePressureWarn_RR : 2;
  vbittype BCM_TireTempWarn_RR : 2;
  vbittype BCM_TirePressureDelta_RR : 1;
  vbittype BCM_TireSensorFault_RR : 1;
  vbittype BCM_TireSensorLowBatt_RR : 1;
  vbittype BCM_TireSensorCalibrationSts_RR : 1;
} _c_BCM_TPMS_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_4_RDS_msgTypeTag
{
  vbittype BCM_WinPostionSts_FL : 3;
  vbittype BCM_WinPostionSts_FR : 3;
  vbittype unused0 : 2;
  vbittype BCM_WinPostionSts_RL : 3;
  vbittype BCM_WinPostionSts_RR : 3;
  vbittype unused1 : 2;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 4;
  vbittype BCM_SunroofPositionSts : 4;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
  vbittype BCM_SunroofSts : 3;
  vbittype BCM_SunshadeReqSts : 3;
  vbittype unused7 : 2;
} _c_CGW_BCM_4_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_5_RDS_msgTypeTag
{
  vbittype unused0 : 2;
  vbittype BCM_AutoLockEnableSts : 2;
  vbittype BCM_AntiTheftModeSts : 2;
  vbittype unused1 : 2;
  vbittype unused2 : 2;
  vbittype BCM_FollowMeHomeSts : 2;
  vbittype BCM_SunroofAutoCloseEnableSts : 2;
  vbittype unused3 : 2;
  vbittype unused4 : 8;
  vbittype BCM_FollowMeTimeSetSts : 2;
  vbittype unused5 : 2;
  vbittype BCM_WiperSensitivitySts : 3;
  vbittype BCM_RearDfstSts : 1;
  vbittype BCM_LowBeamAngleAdjustSts : 3;
  vbittype BCM_FrontWiperMaintenanceModeSts : 1;
  vbittype BCM_MirrorUnfoldRemind : 1;
  vbittype unused6 : 1;
  vbittype BCM_DoorWindowCtrlEnableSts : 2;
  vbittype BCM_SunroofCtrlEnableSts_RF : 2;
  vbittype BCM_SunroofCtrlEnableSts_Remote : 2;
  vbittype BCM_WelcomelightEnableSts : 2;
  vbittype unused7 : 2;
  vbittype BCM_WindowAutoCloseEnableSts : 2;
  vbittype BCM_RKE_SunRoofCtrlEnableSts : 2;
  vbittype IHU_AHB_EnableSts : 2;
  vbittype BCM_WelcomeAnimationReq : 1;
  vbittype BCM_SteeringWheelHeatSts : 1;
  vbittype BCM_AutoBlowEnableSts : 2;
  vbittype unused8 : 5;
  vbittype BCM_DomeLightDoorCtrlSwitchSts : 1;
} _c_CGW_BCM_5_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_VCU_B_5_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 5;
  vbittype VCU_DriveMode : 3;
  vbittype unused2 : 2;
  vbittype VCU_LongRangeRemainingTime : 6;
} _c_CGW_VCU_B_5_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_2_RDS_msgTypeTag
{
  vbittype HMA_Status : 3;
  vbittype unused0 : 3;
  vbittype LDW_ELK_LKA_LDPLeftVisuali : 2;
  vbittype unused1 : 3;
  vbittype LKA_State : 3;
  vbittype LDW_ELK_LKA_LDPRightVisuali : 2;
  vbittype SLASpdlimit_1 : 3;
  vbittype SLAState : 3;
  vbittype unused2 : 2;
  vbittype SLASpdlimitWarning : 1;
  vbittype unused3 : 2;
  vbittype SLASpdlimit_0 : 5;
  vbittype unused4 : 6;
  vbittype Camera_textinfo : 2;
  vbittype FCM_2_SLASpdlimitUnits : 2;
  vbittype unused5 : 3;
  vbittype LCC_mode : 3;
} _c_CGW_FCM_2_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BSD_1_RDS_msgTypeTag
{
  vbittype BSD_RearWarnSystemEnableSts : 2;
  vbittype unused0 : 6;
} _c_CGW_BSD_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_MFS_1_RDS_msgTypeTag
{
  vbittype MFS_LeftWheelButtonSts_ScrollUp : 2;
  vbittype MFS_LeftWheelButtonSts_ScrollDown : 2;
  vbittype MFS_LeftWheelButtonSts_ToggleLeft : 2;
  vbittype MFS_LeftWheelButtonSts_ToggleRight : 2;
  vbittype MFS_LeftWheelButtonSts_Press : 2;
  vbittype MFS_WeChatButtonSts : 2;
  vbittype MFS_CustomButtonSts : 2;
  vbittype unused0 : 2;
  vbittype MFS_RightWheelButtonSts_ScrollUp : 2;
  vbittype MFS_RightWheelButtonSts_ScrollDown : 2;
  vbittype MFS_RightWheelButtonSts_ToggleLeft : 2;
  vbittype MFS_RightWheelButtonSts_ToggleRight : 2;
  vbittype MFS_RightWheelButtonSts_Press : 2;
  vbittype MFS_ReturnButtonSts : 2;
  vbittype MFS_VoiceRecognitionButtonSts : 2;
  vbittype unused1 : 2;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype MFS_MsgAliveCounter : 4;
  vbittype unused4 : 4;
  vbittype MFS_Checksum_CRC : 8;
} _c_CGW_BCM_MFS_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_PEPS_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 4;
  vbittype BCM_EmergencyPowerOffRemindSts_1 : 1;
  vbittype BCM_EmergencyPowerOffRemindSts_2 : 1;
  vbittype unused5 : 2;
} _c_CGW_BCM_PEPS_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_VCU_B_2_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 4;
  vbittype VCU_CruiseCtrlStsDisp : 2;
  vbittype unused3 : 2;
  vbittype unused4 : 8;
  vbittype VCU_RegenerationLevelSts : 3;
  vbittype VCU_SpdLimitSts : 3;
  vbittype VCU_SpeedLimitEnableSts : 2;
  vbittype VCU_SpeedLimitValueSet : 5;
  vbittype unused5 : 3;
} _c_CGW_VCU_B_2_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_ICM_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype ICM_TotalOdometer_2 : 4;
  vbittype unused1 : 3;
  vbittype ICM_LowSOC_LampSts : 1;
  vbittype ICM_TotalOdometer_1 : 8;
  vbittype ICM_TotalOdometer_0 : 8;
} _c_ICM_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_1_RDS_msgTypeTag
{
  vbittype unused0 : 5;
  vbittype BCM_KeySts : 2;
  vbittype unused1 : 1;
  vbittype unused2 : 8;
  vbittype unused3 : 1;
  vbittype BCM_RearFogLightSts : 1;
  vbittype unused4 : 6;
  vbittype BCM_DoorAjarSts_FL : 1;
  vbittype BCM_DoorAjarSts_FR : 1;
  vbittype BCM_DoorAjarSts_RR : 1;
  vbittype BCM_DoorAjarSts_RL : 1;
  vbittype BCM_HoodAjarSts : 1;
  vbittype BCM_TrunkAjarSts : 1;
  vbittype unused5 : 2;
  vbittype unused6 : 1;
  vbittype BCM_RearWashSwSts : 1;
  vbittype unused7 : 2;
  vbittype BCM_FrontWiperSwSts : 3;
  vbittype BCM_RearWiperSwSts : 1;
  vbittype BCM_LightCtrlSwSts : 3;
  vbittype unused8 : 1;
  vbittype BCM_AntiTheftMode : 3;
  vbittype unused9 : 1;
} _c_CGW_BCM_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_EPS_1_RDS_msgTypeTag
{
  vbittype unused0 : 4;
  vbittype EPS_WorkingModeSts : 2;
  vbittype unused1 : 2;
} _c_CGW_EPS_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCS_C_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 4;
  vbittype BCS_HDC_FunctionEnableSts : 1;
  vbittype BCS_AVH_FunctionEnableSts : 1;
  vbittype BCS_CST_FunctionEnableSts : 1;
  vbittype unused2 : 1;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 6;
  vbittype BCS_CST_Active : 2;
  vbittype unused6 : 8;
  vbittype unused7 : 6;
  vbittype BCS_BrakemodeSts : 2;
} _c_CGW_BCS_C_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_EGS_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype EGS_FaultLevel : 3;
  vbittype unused1 : 5;
  vbittype unused2 : 4;
  vbittype EGS_HighBeamSwitchActiveSts : 1;
  vbittype EGS_HighBeamFlashSwitchActiveSts : 1;
  vbittype EGS_FrontWiperMistSwitchActiveSts : 1;
  vbittype EGS_FrontWashSwitchActiveSts : 1;
} _c_CGW_EGS_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_SAS_2_RDS_msgTypeTag
{
  vbittype unused0 : 5;
  vbittype SAS_SteeringAngleSpd_VD : 1;
  vbittype SAS_SteeringAngle_VD : 1;
  vbittype unused1 : 1;
  vbittype SAS_SteeringAngle_1 : 8;
  vbittype SAS_SteeringAngle_0 : 8;
  vbittype SAS_SteeringAngleSpd : 8;
} _c_CGW_SAS_2_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_5_RDS_msgTypeTag
{
  vbittype FCM_5_HMAONOFFSts : 2;
  vbittype FCM_5_IESONOFFSts : 2;
  vbittype FCM_5_SLA_TSI_TLI_ONOFF_Sts : 2;
  vbittype FCM_5_LCCONOFF_Sts : 2;
  vbittype FCM_5_WarnModSts : 2;
  vbittype FCM_5_LDWONOFFSts : 2;
  vbittype unused0 : 2;
  vbittype FCM_5_LKAONOFFSts : 2;
  vbittype unused1 : 2;
  vbittype FCM_5_ELKONOFFSts : 2;
  vbittype FCM_5_LDWLDPSnvtySts : 2;
  vbittype FCM_5_LDPONOFFSts : 2;
} _c_CGW_FCM_5_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_WCM_1_RDS_msgTypeTag
{
  vbittype WCM_WirelessChargingEnableSts : 2;
  vbittype unused0 : 2;
  vbittype WCM_WirelessChargingSts : 2;
  vbittype WCM_ForeignBodyDetectedWaring : 1;
  vbittype unused1 : 1;
  vbittype WCM_OverTempWarning : 1;
  vbittype unused2 : 7;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
  vbittype WCM_MsgAliveCounter : 4;
  vbittype unused7 : 4;
} _c_WCM_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_AVAS_1_RDS_msgTypeTag
{
  vbittype AVAS_VolumeSts : 3;
  vbittype AVAS_SwSts : 1;
  vbittype AVAS_AudioSourceSts : 4;
} _c_AVAS_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_TBOX_7_RDS_msgTypeTag
{
  vbittype TBOX_GPS_Time_year : 6;
  vbittype unused0 : 2;
  vbittype TBOX_GPS_Time_Month : 4;
  vbittype unused1 : 4;
  vbittype TBOX_GPS_Time_Day : 6;
  vbittype unused2 : 2;
  vbittype TBOX_GPS_Time_Hour : 5;
  vbittype unused3 : 3;
  vbittype TBOX_GPS_Time_Minute : 6;
  vbittype unused4 : 2;
  vbittype TBOX_GPS_Time_Second : 6;
  vbittype unused5 : 2;
} _c_TBOX_7_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_BCM_ALM_RDS_msgTypeTag
{
  vbittype BCM_AmbientLightBrightnesssSts_Front : 8;
  vbittype BCM_AmbientLightBrightnessSts_Roof : 8;
  vbittype BCM_AmbientLightBrightnessSts_Rear : 8;
  vbittype BCM_AmbientLightColorSta_Front : 8;
  vbittype BCM_AmbientLightColorSts_Roof : 8;
  vbittype BCM_AmbientLightColorStst_Rear : 8;
  vbittype BCM_MsgAliveCounter : 4;
  vbittype BCM_AmbientLightModeSts : 3;
  vbittype unused0 : 1;
  vbittype BCM_AmbientLightEnableSts : 2;
  vbittype BCM_AmbientLightAreaDisplayEnableSts : 2;
  vbittype BCM_AmbientLightBreatheEnableSts : 2;
  vbittype BCM_AmbientLightBreatheWithVehSpdEnableSts : 2;
} _c_BCM_ALM_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_SCM_3_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype SCM_MassageSwithSts_Psngr : 2;
  vbittype SCM_MassageModeSts_Psngr : 3;
  vbittype SCM_MassageStrengthSts_Psngr : 3;
  vbittype SCM_LumbarSupportModeSts_Psngr : 3;
  vbittype unused2 : 5;
} _c_CGW_BCM_SCM_3_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_SCM_2_RDS_msgTypeTag
{
  vbittype SCM_SeatHeatVentSetSts_Psngr : 3;
  vbittype unused0 : 3;
  vbittype SCM_DriverSeatLocalCtrlSwithSts_Psngr : 2;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype SCM_MotoPosition_ForwardBack_Psngr : 8;
  vbittype unused3 : 8;
  vbittype SCM_MotoPosition_SeatBack_Psngr : 8;
  vbittype SCM_MsgAliveCounter_2 : 4;
  vbittype unused4 : 4;
  vbittype SCM_MotoPosition_LegSupport_Psngr : 8;
} _c_CGW_BCM_SCM_2_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_SCM_1_RDS_msgTypeTag
{
  vbittype SCM_SeatHeatVentSetSts_Drive : 3;
  vbittype unused0 : 3;
  vbittype SCM_LocalCtrlSwithSts_Driver : 2;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype SCM_MotoPosition_ForwardBack_Driver : 8;
  vbittype SCM_MotoPosition_UpDown_Driver : 8;
  vbittype SCM_MotoPosition_SeatBack_Driver : 8;
  vbittype SCM_MsgAliveCounter_1 : 4;
  vbittype unused3 : 4;
  vbittype SCM_MotoPosition_LegSupport_Driver : 8;
} _c_CGW_BCM_SCM_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_FM_RDS_msgTypeTag
{
  vbittype BCM_FM_FragranceEnableSts : 2;
  vbittype BCM_FM_FragranceSystemFaultSts : 2;
  vbittype BCM_FM_FragranceConcentration : 2;
  vbittype unused0 : 2;
  vbittype BCM_FM_FragranceType_Position_1 : 4;
  vbittype BCM_FM_FragranceType_Position_2 : 4;
  vbittype BCM_FM_FragranceType_Position_3 : 4;
  vbittype BCM_FM_FragrancePositionSts : 2;
  vbittype unused1 : 2;
  vbittype FM_FragranceReplaceRemind_1 : 1;
  vbittype FM_FragranceReplaceRemind_2 : 1;
  vbittype FM_FragranceReplaceRemind_3 : 1;
  vbittype unused2 : 5;
} _c_CGW_BCM_FM_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_BCM_6_RDS_msgTypeTag
{
  vbittype unused0 : 2;
  vbittype BCM_FaceRecSeatEnableSts : 2;
  vbittype BCM_EntryExitSeatCtrlEnableSts : 2;
  vbittype unused1 : 2;
  vbittype BCM_SeatPositionStoreResult : 3;
  vbittype unused2 : 1;
  vbittype BCM_DriverFaceRecognitionResult : 2;
  vbittype BCM_SeatPositionCalloutInhibitReq : 1;
  vbittype unused3 : 1;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
  vbittype BCM_SeatPositionCalloutSts : 3;
  vbittype BCM_DriverFaceRecognitionSts : 3;
  vbittype unused7 : 2;
  vbittype BCM_MirrorFoldEnableSts : 2;
  vbittype BCM_MirrorReverseInclineEnableSts : 2;
  vbittype BCM_FaceRecMirrorInclineEnableSts : 2;
  vbittype BCM_MirrorPositionMemoryHMIReq : 1;
  vbittype unused8 : 1;
  vbittype BCM_MirrorCtrlSts_L : 3;
  vbittype BCM_MirrorCtrlSts_R : 3;
  vbittype BCM_MirrorReversePositionStoreResult : 2;
} _c_BCM_6_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_TPMS_2_RDS_msgTypeTag
{
  vbittype BCM_TirePressure_FL : 8;
  vbittype BCM_TirePressure_FR : 8;
  vbittype BCM_TirePressure_RL : 8;
  vbittype BCM_TirePressure_RR : 8;
  vbittype BCM_TireTemp_FL : 8;
  vbittype BCM_TireTemp_FR : 8;
  vbittype BCM_TireTemp_RL : 8;
  vbittype BCM_TireTemp_RR : 8;
} _c_CGW_BCM_TPMS_2_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_10_RDS_msgTypeTag
{
  vbittype FCM_10_TSISgnGiWay : 4;
  vbittype FCM_10_TLISysSts : 2;
  vbittype FCM_10_TSISysSts : 2;
  vbittype FCM_10_TSISgnLongStayDisp : 4;
  vbittype FCM_10_TSISgnForb : 4;
  vbittype FCM_10_LDWSysSts : 3;
  vbittype FCM_10_LDPSysSts : 3;
  vbittype FCM_10_TLITrfcLi : 2;
  vbittype FCM_10_LCCEnaRcmend : 1;
  vbittype FCM_10_LCCText : 3;
  vbittype FCM_10_TSINoParkWarn : 1;
  vbittype FCM_10_ELKSysSts : 3;
  vbittype FCM_10_WarnModSts : 2;
  vbittype FCM_10_SLAONOFFSts : 2;
  vbittype FCM_10_LCCExitTextInfoSts : 2;
  vbittype FCM_10_LDPTJAELKTakeoverReq : 2;
  vbittype FCM_10_EgoLeftLineHeatgAg_1 : 5;
  vbittype FCM_10_ELKIntvMod : 3;
  vbittype unused0 : 4;
  vbittype FCM_10_EgoLeftLineHeatgAg_0 : 4;
} _c_CGW_FCM_10_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_FRM_9_RDS_msgTypeTag
{
  vbittype FCM_FRM_9_ACCObjLgtDstX_1 : 3;
  vbittype FCM_FRM_9_ACCObjTyp : 4;
  vbittype FCM_FRM_9_LeObjID : 1;
  vbittype FCM_FRM_9_ACCObjHozDstY_1 : 3;
  vbittype FCM_FRM_9_ACCObjLgtDstX_0 : 5;
  vbittype unused0 : 1;
  vbittype FCM_FRM_9_ACCObjHozDstY_0 : 7;
  vbittype FCM_FRM_9_FrntFarObjLgtDstX_1 : 3;
  vbittype FCM_FRM_9_FrntFarObjTyp : 4;
  vbittype FCM_FRM_9_FrntFarObjID : 1;
  vbittype FCM_FRM_9_FrntFarObjHozDstY_1 : 3;
  vbittype FCM_FRM_9_FrntFarObjLgtDstX_0 : 5;
  vbittype unused1 : 1;
  vbittype FCM_FRM_9_FrntFarObjHozDstY_0 : 7;
  vbittype FCM_FRM_9_MessageCounter : 4;
  vbittype unused2 : 4;
  vbittype FCM_FRM_9_CRC : 8;
} _c_CGW_FCM_FRM_9_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_FRM_8_RDS_msgTypeTag
{
  vbittype FCM_FRM_8_ACCObjLgtDstX_1 : 3;
  vbittype FCM_FRM_8_ACCObjTyp : 4;
  vbittype FCM_FRM_8_ACCObjID : 1;
  vbittype FCM_FRM_8_ACCObjHozDstY_1 : 3;
  vbittype FCM_FRM_8_ACCObjLgtDstX_0 : 5;
  vbittype unused0 : 1;
  vbittype FCM_FRM_8_ACCObjHozDstY_0 : 7;
  vbittype FCM_FRM_8_FrntFarObjLgtDstX_1 : 3;
  vbittype FCM_FRM_8_FrntFarObjTyp : 4;
  vbittype FCM_FRM_8_FrntFarObjID : 1;
  vbittype FCM_FRM_8_FrntFarObjHozDstY_1 : 3;
  vbittype FCM_FRM_8_FrntFarObjLgtDstX_0 : 5;
  vbittype unused1 : 1;
  vbittype FCM_FRM_8_FrntFarObjHozDstY_0 : 7;
  vbittype FCM_FRM_8_MessageCounter : 4;
  vbittype unused2 : 4;
  vbittype FCM_FRM_8_CRC : 8;
} _c_CGW_FCM_FRM_8_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_7_RDS_msgTypeTag
{
  vbittype FCM_7_NeborLeLineHozlDst_1 : 3;
  vbittype FCM_7_NeborLeLineTyp : 2;
  vbittype FCM_7_NeborLeLineColor : 2;
  vbittype FCM_7_NeborLeLineID : 1;
  vbittype FCM_7_NeborLeLineCrvt_1 : 2;
  vbittype FCM_7_NeborLeLineHozlDst_0 : 6;
  vbittype FCM_7_NeborLeLineCrvt_0 : 8;
  vbittype FCM_7_NeborRiLineHozlDst_1 : 3;
  vbittype FCM_7_NeborRiLineTyp : 2;
  vbittype FCM_7_NeborRiLineColor : 2;
  vbittype FCM_7_NeborRiLineID : 1;
  vbittype FCM_7_NeborRiLineCrvt_1 : 2;
  vbittype FCM_7_NeborRiLineHozlDst_0 : 6;
  vbittype FCM_7_NeborRiLineCrvt_0 : 8;
} _c_CGW_FCM_7_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_FCM_6_RDS_msgTypeTag
{
  vbittype FCM_6_EgoLeLineHozlDst_1 : 3;
  vbittype FCM_6_EgoLeLineTyp : 2;
  vbittype FCM_6_EgoLeLineColor : 2;
  vbittype FCM_6_EgoLeLineID : 1;
  vbittype FCM_6_EgoLeLineCrvt_1 : 2;
  vbittype FCM_6_EgoLeLineHozlDst_0 : 6;
  vbittype FCM_6_EgoLeLineCrvt_0 : 8;
  vbittype FCM_6_EgoRiLineHozlDst_1 : 3;
  vbittype FCM_6_EgoRiLineTyp : 2;
  vbittype FCM_6_EgoRiLineColor : 2;
  vbittype FCM_6_EgoRiLineID : 1;
  vbittype FCM_6_EgoRiLineCrvt_1 : 2;
  vbittype FCM_6_EgoRiLineHozlDst_0 : 6;
  vbittype FCM_6_EgoRiLineCrvt_0 : 8;
} _c_CGW_FCM_6_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCM_ACCM_3_RDS_msgTypeTag
{
  vbittype unused0 : 2;
  vbittype ACCM_AQS_EnableSts : 2;
  vbittype unused1 : 4;
  vbittype ACCM_PM2_5_InternalValue_1 : 8;
  vbittype ACCM_PM2_5_ExternalValue_1 : 2;
  vbittype unused2 : 4;
  vbittype ACCM_PM2_5_InternalValue_0 : 2;
  vbittype ACCM_PM2_5_ExternalValue_0 : 8;
  vbittype ACCM_PM2_5_InternalLevel : 3;
  vbittype ACCM_PM2_5_ExternalLevel : 3;
  vbittype ACCM_SmokingModeSts : 2;
  vbittype AQS_AQS_AirQualityLevel : 4;
  vbittype unused3 : 4;
} _c_CGW_BCM_ACCM_3_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_VCU_B_4_RDS_msgTypeTag
{
  vbittype VCU_ResidualOdometer_1 : 8;
  vbittype VCU_InstPowerConsum_1 : 6;
  vbittype VCU_ResidualOdometer_0 : 2;
  vbittype unused0 : 4;
  vbittype VCU_InstPowerConsum_0 : 4;
  vbittype unused1 : 8;
  vbittype VCU_TotalPowerDisp_1 : 8;
  vbittype VCU_InstPowerDisp_1 : 4;
  vbittype VCU_TotalPowerDisp_0 : 4;
  vbittype VCU_InstPowerDisp_0 : 8;
} _c_CGW_VCU_B_4_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_VCU_B_3_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 6;
  vbittype VCU_ManualToGearP_InhibitSts : 1;
  vbittype unused2 : 1;
  vbittype unused3 : 2;
  vbittype VCU_ShiftErrorActionRemindEnableSts : 2;
  vbittype unused4 : 4;
  vbittype VCU_InverterEnableSwSts : 2;
  vbittype VCU_InverterPower : 6;
  vbittype VCU_InverterInd_1 : 1;
  vbittype VCU_InverterInd_2 : 1;
  vbittype VCU_InverterInd_3 : 1;
  vbittype VCU_InverterInd_4 : 1;
  vbittype VCU_InverterFaultSts : 1;
  vbittype VCU_220V_OutputTimeInd : 3;
  vbittype unused5 : 6;
  vbittype VCU_PowerOffAutoParkInhibitSts : 1;
  vbittype VCU_DoorOpenParkInhibitSts : 1;
  vbittype unused6 : 6;
  vbittype VCU_InverterEnableSts : 2;
  vbittype VCU_OutputLimitSOC : 8;
} _c_CGW_VCU_B_3_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BMS_3_RDS_msgTypeTag
{
  vbittype BMS_HV_BattVolt_1 : 8;
  vbittype BMS_HV_BattCurr_1 : 6;
  vbittype BMS_HV_BattVolt_0 : 2;
  vbittype BMS_HV_BattCurr_0 : 8;
} _c_CGW_BMS_3_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_BCS_EPB_1_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 7;
  vbittype BCS_EPB_HMI_RemindReq : 1;
  vbittype unused3 : 4;
  vbittype BCS_EPB_ActuatorSts : 3;
  vbittype EPB_AutoApplyDisableSts : 1;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype BCS_EPB_MsgAliveCounter : 4;
  vbittype unused6 : 4;
  vbittype BCS_EPB_Checksum_CRC : 8;
} _c_CGW_BCS_EPB_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_CGW_VCU_P_5_RDS_msgTypeTag
{
  vbittype unused0 : 4;
  vbittype VCU_ActGear : 4;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype VCU_VehSpd_1 : 8;
  vbittype unused3 : 1;
  vbittype VCU_BrakePedalSts : 1;
  vbittype VCU_PowertrainReadySts : 1;
  vbittype VCU_VehSpd_0 : 5;
  vbittype unused4 : 1;
  vbittype VCU_BrakePedal_VD : 1;
  vbittype unused5 : 1;
  vbittype VCU_ActGear_VD : 1;
  vbittype VCU_VehSpd_VD : 1;
  vbittype unused6 : 3;
} _c_CGW_VCU_P_5_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_NM_IHU_RDS_msgTypeTag
{
  vbittype NM_IHU_SourceNode_ID : 8;
  vbittype NM_IHU_RepeatMsgReq : 1;
  vbittype unused0 : 3;
  vbittype NM_IHU_ActiveWakeupBit : 1;
  vbittype unused1 : 3;
  vbittype NM_IHU_UserData_0 : 8;
  vbittype unused2 : 8;
  vbittype NM_IHU_UserData_2_NM_State : 3;
  vbittype NM_IHU_UserData_2_GatewayRequest : 1;
  vbittype unused3 : 4;
  vbittype NM_IHU_UserData_3_Reserved : 8;
  vbittype NM_IHU_UserData_4_Reserved : 8;
  vbittype NM_IHU_UserData_5_Reserved : 8;
} _c_NM_IHU_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_ADAS_10_RDS_msgTypeTag
{
  vbittype NavSpeedLimitStatus : 2;
  vbittype NavSpeedLimit : 6;
  vbittype NavSpeedLimitUnits : 2;
  vbittype unused0 : 6;
  vbittype unused1 : 8;
  vbittype unused2 : 4;
  vbittype NavCurrRoadType : 4;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
} _c_IHU_ADAS_10_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_ADAS_11_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype IHU_11_FCWSwtSet : 2;
  vbittype IHU_11_FCWSnvtySet : 2;
  vbittype IHU_11_AEBSwtSet : 2;
  vbittype unused2 : 2;
  vbittype IHU_11_SCF_request_confirmed : 2;
  vbittype IHU_11_SCFSwtSet : 2;
  vbittype IHU_11_SLA_TSI_TLI_SwtSet : 2;
  vbittype unused3 : 2;
  vbittype IHU_11_LKASwtSet : 2;
  vbittype IHU_11_HMASwtSet : 2;
  vbittype IHU_11_IESSwtSet : 2;
  vbittype IHU_11_DAISwtSet : 2;
  vbittype IHU_11_LDPSwtSet : 2;
  vbittype IHU_11_WarnModSwtSet : 2;
  vbittype IHU_11_LDWSwtSet : 2;
  vbittype unused4 : 2;
  vbittype unused5 : 4;
  vbittype IHU_11_ELKSwtSet : 2;
  vbittype IHU_11_LDWLDPSnvtySetLDWLDP : 2;
  vbittype unused6 : 8;
} _c_IHU_ADAS_11_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_18_RDS_msgTypeTag
{
  vbittype IHU_AmbientLightBrightnessAdjust_Front : 8;
  vbittype IHU_AmbientLightBrightnessAdjust_Roof : 8;
  vbittype IHU_AmbientLightBrightnessAdjust_Rear : 8;
  vbittype IHU_AmbientLightColorSet_Front : 8;
  vbittype IHU_AmbientLightColorSet_Roof : 8;
  vbittype IHU_AmbientLightColorSet_Rear : 8;
  vbittype IHU_RegionColorSynchronizationReq : 1;
  vbittype unused0 : 3;
  vbittype IHU_AmbientLightModeSet : 3;
  vbittype unused1 : 1;
  vbittype IHU_AmbientLightEnableSwSts : 2;
  vbittype IHU_AmbientLightAreaDisplayEnableSwSts : 2;
  vbittype IHU_AmbientLightBreatheEnableSwSts : 2;
  vbittype IHU_AmbientLightBreatheWithVehSpdEnableSwSts : 2;
} _c_IHU_18_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_13_RDS_msgTypeTag
{
  vbittype IHU_Chg_SOC_LimitPointSet : 6;
  vbittype IHU_ChgModeSet : 2;
  vbittype IHU_BookChgStartTimeSet_Hour : 5;
  vbittype unused0 : 3;
  vbittype IHU_BookChgStartTimeSet_Minute : 6;
  vbittype IHU_BookChgTimeSet_VD : 2;
  vbittype IHU_BookChgStopTimeSet_Hour : 5;
  vbittype unused1 : 3;
  vbittype IHU_BookchgStopTimeSet_Minute : 6;
  vbittype unused2 : 2;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
} _c_IHU_13_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_12_RDS_msgTypeTag
{
  vbittype IHU_MusicLoudness_120HZ : 5;
  vbittype unused0 : 1;
  vbittype IHU_VoiceAssistantActiveSts : 1;
  vbittype unused1 : 1;
  vbittype IHU_MusicLoudness_250HZ : 5;
  vbittype unused2 : 3;
  vbittype IHU_MusicLoudness_500HZ : 5;
  vbittype unused3 : 3;
  vbittype IHU_MusicLoudness_1000HZ : 5;
  vbittype unused4 : 3;
  vbittype IHU_MusicLoudness_1500HZ : 5;
  vbittype unused5 : 3;
  vbittype IHU_MusicLoudness_2000HZ : 5;
  vbittype unused6 : 3;
  vbittype IHU_MusicLoudness_6000HZ : 5;
  vbittype unused7 : 3;
  vbittype unused8 : 8;
} _c_IHU_12_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_11_RDS_msgTypeTag
{
  vbittype IHU_GPS_Time_year : 6;
  vbittype IHU_GPS_TimeSwSts : 2;
  vbittype IHU_GPS_Time_Month : 4;
  vbittype unused0 : 4;
  vbittype IHU_GPS_Time_Day : 6;
  vbittype unused1 : 2;
  vbittype IHU_GPS_Time_Hour : 5;
  vbittype unused2 : 3;
  vbittype IHU_GPS_Time_Minute : 6;
  vbittype unused3 : 2;
  vbittype IHU_GPS_Time_Second : 6;
  vbittype unused4 : 2;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
} _c_IHU_11_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_10_RDS_msgTypeTag
{
  vbittype IHU_RearWarnSystemEnableSwSts : 2;
  vbittype unused0 : 6;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
  vbittype unused7 : 8;
} _c_IHU_10_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_8_RDS_msgTypeTag
{
  vbittype unused0 : 5;
  vbittype IHU_ICM_ThemelSet : 3;
  vbittype unused1 : 6;
  vbittype IHU_ICM_ScreenReq : 2;
  vbittype IHU_OverSpdValueSet : 6;
  vbittype IHU_ICM_Menu_OK_ButtonSts : 1;
  vbittype IHU_ICM_ModeButtonSts : 1;
  vbittype IHU_FatigureDrivingTimeSet : 4;
  vbittype IHU_ICM_UpButtonSts : 1;
  vbittype IHU_ICM_DownButtonSts : 1;
  vbittype IHU__ICM_LeftButtonSts : 1;
  vbittype IHU__ICM_RightButtonSts : 1;
  vbittype IHU_BrightnessAdjustSet_ICM : 4;
  vbittype unused2 : 4;
  vbittype unused3 : 6;
  vbittype IHU_DVR_QuickCameraSw : 1;
  vbittype unused4 : 1;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
} _c_IHU_8_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_7_RDS_msgTypeTag
{
  vbittype IHU_SeatHeatLevelSetReq_RL : 3;
  vbittype IHU_SeatHeatLevelSetReq_RR : 3;
  vbittype unused0 : 2;
  vbittype unused1 : 8;
  vbittype unused2 : 2;
  vbittype IHU_WirelessChargingEnableSwSts : 2;
  vbittype unused3 : 4;
  vbittype unused4 : 3;
  vbittype IHU_AVAS_VolumeSet : 3;
  vbittype unused5 : 2;
  vbittype IHU_AVAS_AudioSourceSet : 4;
  vbittype unused6 : 2;
  vbittype IHU_AVAS_EnableSwSts : 2;
  vbittype IHU_SeatMemoryEnableSwSts : 2;
  vbittype IHU_SeatPositionStoreReq : 3;
  vbittype IHU_SeatPositionCalloutReq : 3;
  vbittype IHU_SeatHeatVentLevelSetReq_Drive : 3;
  vbittype IHU_SeatHeatVentLevelSetReq_Psngr : 3;
  vbittype IHU_FaceRecSeatEnableSwSts : 2;
  vbittype IHU_FaceRecMirrorInclineEnableSwSts : 2;
  vbittype IHU_FaceRecAuthenticationResult : 2;
  vbittype IHU_AccountDeletedReq : 2;
  vbittype IHU_EntryExitSeatCtrlEnableSwSts : 2;
} _c_IHU_7_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_6_RDS_msgTypeTag
{
  vbittype IHU_AUTO_SwSts : 1;
  vbittype IHU_AC_SwSts : 1;
  vbittype IHU_PTC_SwSts : 1;
  vbittype IHU_OFF_SwSts : 1;
  vbittype IHU_RearDfstSwSts : 1;
  vbittype IHU_CirculationModeSet : 2;
  vbittype IHU_AutoDfstSwSts : 1;
  vbittype IHU_BlowModeSet : 3;
  vbittype IHU_TempSet_Driver : 5;
  vbittype IHU_BlowerlevelSet : 4;
  vbittype IHU_AnionGeneratorSwSts : 1;
  vbittype unused0 : 1;
  vbittype IHU_AQS_EnablaSw : 2;
  vbittype IHU_AutoBlowEnablaSw : 2;
  vbittype unused1 : 2;
  vbittype IHU_AC_MemoryModeEnablaSw : 2;
  vbittype IHU_DUAL_EnablaSw : 2;
  vbittype IHU_FrontDefrostSwSts : 1;
  vbittype IHU_VentilationSwSts : 1;
  vbittype IHU_ECO_SwSts : 1;
  vbittype IHU_TempSet_Psngr : 5;
  vbittype unused2 : 8;
  vbittype IHU_LightCtrlSwSts : 3;
  vbittype IHU_FrontWiperSwSts : 3;
  vbittype IHU_RearFogLightSwSts : 1;
  vbittype IHU_MirrorReversePositionStoreReq : 1;
  vbittype IHU_MirrorCtrlReq_L : 3;
  vbittype IHU_MirrorCtrlReq_R : 3;
  vbittype IHU_RearWiperSwSts : 1;
  vbittype IHU_RearWashSwSts : 1;
} _c_IHU_6_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_5_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 2;
  vbittype IHU_InverterConfirmSts : 2;
  vbittype IHU_InverterEnableSwSts : 2;
  vbittype IHU_CruiseTypeSetSts : 2;
  vbittype IHU_VehWashModeSet : 3;
  vbittype IHU_SpeedLimitValueSet : 5;
  vbittype IHU_SpeedLimitEnableSwSts : 2;
  vbittype IHU_RegenerationLevelSet : 3;
  vbittype IHU_DriveModeSet : 3;
  vbittype IHU_OutputLimitSOCSet : 8;
  vbittype IHU_ShiftVoiceRemind_EnableSwSts : 2;
  vbittype IHU_ShiftErrorActionRemindEnableSwSts : 2;
  vbittype IHU_SOC_ChargeLockEnableSwSts : 2;
  vbittype unused3 : 2;
  vbittype unused4 : 8;
} _c_IHU_5_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_4_RDS_msgTypeTag
{
  vbittype unused0 : 2;
  vbittype IHU_AutoLockEnableSwSts : 2;
  vbittype IHU_AntiTheftModeSetSwSts : 2;
  vbittype IHU_SteeringWheelHeatReq : 2;
  vbittype IHU_MirrorFoldEnableSwSts__ : 2;
  vbittype IHU_FollowHomeMeEnableSwSts : 2;
  vbittype IHU_SunroofAutoCloseEnableSwSts : 2;
  vbittype IHU_WelcomeLightEnableSwSts : 2;
  vbittype IHU_ComfortModeSet : 3;
  vbittype IHU_WiperSensitivitySet : 3;
  vbittype IHU_MirrorFoldReq : 2;
  vbittype IHU_AHB_EnableSwSts : 2;
  vbittype IHU_LowBeamAngleAdjust : 3;
  vbittype IHU_FrontWiperMaintenanceModeReq : 1;
  vbittype IHU_EmergencyPowerOffReqConfirm : 2;
  vbittype IHU_MirrorReverseInclineEnableSwSts : 2;
  vbittype unused1 : 2;
  vbittype IHU_FollowMeHomeTimeSetSts : 2;
  vbittype IHU_DoorWindowCtrlEnableSwSts : 2;
  vbittype IHU_WindowAutoCloseEnableSwSts : 2;
  vbittype IHU_RKE_SunRoofCtrlEnableSwSts : 2;
  vbittype unused2 : 2;
  vbittype IHU_DomeLightDoorCtrlSwitchSts : 2;
  vbittype IHU_MaxPositionSet : 8;
  vbittype IHU_FragranceEnableSwSts : 2;
  vbittype IHU_FragranceConcentrationSet : 2;
  vbittype IHU_FragrancePositionSet : 2;
  vbittype unused3 : 2;
} _c_IHU_4_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_3_RDS_msgTypeTag
{
  vbittype IHU_VoiceCtrl_SunroofReq : 4;
  vbittype IHU_VoiceCtrl_SunshadeReq : 2;
  vbittype unused0 : 2;
  vbittype IHU_VoiceCtrl_WindowReq_FL : 3;
  vbittype IHU_VoiceCtrl_WindowReq_FR : 3;
  vbittype unused1 : 2;
  vbittype IHU_VoiceCtrl_WindowReq_RL : 3;
  vbittype IHU_VoiceCtrl_WindowReq_RR : 3;
  vbittype unused2 : 2;
  vbittype IHU_VoiceCtrl_LowbeamReq : 2;
  vbittype IHU_VoiceCtrl_TrunkReq : 2;
  vbittype IHU_SmokingModeReq : 2;
  vbittype unused3 : 2;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
  vbittype unused7 : 8;
} _c_IHU_3_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_17_RDS_msgTypeTag
{
  vbittype IHU_MassageSwithSet_Psngr : 2;
  vbittype IHU_MassageModeSet_Psngr : 3;
  vbittype IHU_MassageStrengthSet_Psngr : 3;
  vbittype IHU_LumbarSupportModeSet_Psngr : 3;
  vbittype unused0 : 5;
  vbittype unused1 : 8;
  vbittype IHU_MotoPositionSet_ForwardBack_Psngr : 8;
  vbittype unused2 : 8;
  vbittype IHU_MotoPositionSet_SeatBack_Psngr : 8;
  vbittype IHU_17_MsgAliveCounter : 4;
  vbittype unused3 : 4;
  vbittype IHU_MotoPositionSet_LegSupport_Psngr : 8;
} _c_IHU_17_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_16_RDS_msgTypeTag
{
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype IHU_MotoPositionSet_ForwardBack_Driver : 8;
  vbittype IHU_MotoPositionSet_UpDown_Driver : 8;
  vbittype IHU_MotoPositionSet_SeatBack_Driver : 8;
  vbittype IHU_16_MsgAliveCounter : 4;
  vbittype unused3 : 4;
  vbittype IHU_MotoPositionSet_LegSupport_Driver : 8;
} _c_IHU_16_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_15_RDS_msgTypeTag
{
  vbittype IHU_AccountCreatSts_Num1 : 2;
  vbittype IHU_AccountCreatSts_Num2 : 2;
  vbittype IHU_AccountCreatSts_Num3 : 2;
  vbittype unused0 : 2;
  vbittype IHU_DriverFaceRecognitionSts : 3;
  vbittype unused1 : 5;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype unused5 : 8;
  vbittype unused6 : 8;
  vbittype unused7 : 8;
} _c_IHU_15_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_14_RDS_msgTypeTag
{
  vbittype IHU_RadioFrequanceMode : 2;
  vbittype IHU_RadioResearchSts : 2;
  vbittype IHU_SourceStationMode : 3;
  vbittype IHU_PowerOnSts : 1;
  vbittype IHU_FM_RadioFrequanceValue_1 : 8;
  vbittype IHU_FM_RadioFrequanceValue_0 : 8;
  vbittype IHU_AM_RadioFrequanceValue_1 : 8;
  vbittype IHU_RadioVolume : 5;
  vbittype IHU_AM_RadioFrequanceValue_0 : 3;
  vbittype unused0 : 8;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
} _c_IHU_14_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_1_RDS_msgTypeTag
{
  vbittype unused0 : 4;
  vbittype IHU_EPB_SwSts : 2;
  vbittype unused1 : 2;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 4;
  vbittype IHU_BrakemodeSet : 2;
  vbittype IHU_EPS_ModeSet : 2;
  vbittype IHU_ESC_OFF_SwSts : 2;
  vbittype IHU_CST_FunctionEnableSwSts : 2;
  vbittype IHU_HDC_FunctionEnableSwSts : 2;
  vbittype IHU_AVH_FunctionEnableSwSts : 2;
  vbittype IHU_PDC_EnableSwSts : 2;
  vbittype unused5 : 6;
  vbittype unused6 : 8;
  vbittype unused7 : 8;
} _c_IHU_1_RDS_msgType;


/* -----------------------------------------------------------------------------
    &&&~ RDSMsgStruct_RI20
 ----------------------------------------------------------------------------- */

typedef struct _c_IHU_MFS_RDS_msgTypeTag
{
  vbittype IHU_MFS_CruiseCtrlEnableButtonSts : 2;
  vbittype IHU_MFS_CruiseCancelButtonSts : 2;
  vbittype IHU_MFS_CruiseSetButtonSts : 2;
  vbittype IHU_MFS_CruiseResumeButtonSts : 2;
  vbittype IHU_MFS_SpdLimitButtonSts : 2;
  vbittype IHU_MFS_ACC_GapSB1 : 2;
  vbittype IHU_MFS_ACC_GapSB : 2;
  vbittype unused0 : 2;
  vbittype unused1 : 8;
  vbittype unused2 : 8;
  vbittype unused3 : 8;
  vbittype unused4 : 8;
  vbittype IHU_MsgAliveCounter_MFS : 4;
  vbittype unused5 : 4;
  vbittype IHU_Checksum_CRC_MFS : 8;
} _c_IHU_MFS_RDS_msgType;




/* -----------------------------------------------------------------------------
    &&&~ RDSUnionForCanObjects_RI20
 ----------------------------------------------------------------------------- */

typedef union _c_RDSBasic_bufTag
{
  vuint8 _c[8];
  _c_CGW_FCM_FRM_5_RDS_msgType CGW_FCM_FRM_5;
  _c_CGW_OBC_1_RDS_msgType CGW_OBC_1;
  _c_CGW_BCM_ACCM_1_RDS_msgType CGW_BCM_ACCM_1;
  _c_ICM_2_RDS_msgType ICM_2;
  _c_CGW_FCM_FRM_6_RDS_msgType CGW_FCM_FRM_6;
  _c_CGW_BMS_9_RDS_msgType CGW_BMS_9;
  _c_CGW_VCU_E_3_RDS_msgType CGW_VCU_E_3;
  _c_CGW_PLG_1_RDS_msgType CGW_PLG_1;
  _c_RCM_1_RDS_msgType RCM_1;
  _c_BCM_TPMS_1_RDS_msgType BCM_TPMS_1;
  _c_CGW_BCM_4_RDS_msgType CGW_BCM_4;
  _c_CGW_BCM_5_RDS_msgType CGW_BCM_5;
  _c_CGW_VCU_B_5_RDS_msgType CGW_VCU_B_5;
  _c_CGW_FCM_2_RDS_msgType CGW_FCM_2;
  _c_CGW_BSD_1_RDS_msgType CGW_BSD_1;
  _c_CGW_BCM_MFS_1_RDS_msgType CGW_BCM_MFS_1;
  _c_CGW_BCM_PEPS_1_RDS_msgType CGW_BCM_PEPS_1;
  _c_CGW_VCU_B_2_RDS_msgType CGW_VCU_B_2;
  _c_ICM_1_RDS_msgType ICM_1;
  _c_CGW_BCM_1_RDS_msgType CGW_BCM_1;
  _c_CGW_EPS_1_RDS_msgType CGW_EPS_1;
  _c_CGW_BCS_C_1_RDS_msgType CGW_BCS_C_1;
  _c_CGW_EGS_1_RDS_msgType CGW_EGS_1;
} _c_RDSBasic_buf;
typedef union _c_RDS1_bufTag
{
  vuint8 _c[4];
  _c_CGW_SAS_2_RDS_msgType CGW_SAS_2;
} _c_RDS1_buf;
typedef union _c_RDS2_bufTag
{
  vuint8 _c[3];
  _c_CGW_FCM_5_RDS_msgType CGW_FCM_5;
} _c_RDS2_buf;
typedef union _c_RDS3_bufTag
{
  vuint8 _c[7];
  _c_WCM_1_RDS_msgType WCM_1;
} _c_RDS3_buf;
typedef union _c_RDS4_bufTag
{
  vuint8 _c[1];
  _c_AVAS_1_RDS_msgType AVAS_1;
} _c_RDS4_buf;
typedef union _c_RDS5_bufTag
{
  vuint8 _c[6];
  _c_TBOX_7_RDS_msgType TBOX_7;
} _c_RDS5_buf;
typedef union _c_RDS6_bufTag
{
  vuint8 _c[8];
  _c_BCM_ALM_RDS_msgType BCM_ALM;
} _c_RDS6_buf;
typedef union _c_RDS7_bufTag
{
  vuint8 _c[4];
  _c_CGW_BCM_SCM_3_RDS_msgType CGW_BCM_SCM_3;
} _c_RDS7_buf;
typedef union _c_RDS8_bufTag
{
  vuint8 _c[8];
  _c_CGW_BCM_SCM_2_RDS_msgType CGW_BCM_SCM_2;
} _c_RDS8_buf;
typedef union _c_RDS9_bufTag
{
  vuint8 _c[8];
  _c_CGW_BCM_SCM_1_RDS_msgType CGW_BCM_SCM_1;
} _c_RDS9_buf;
typedef union _c_RDS10_bufTag
{
  vuint8 _c[4];
  _c_CGW_BCM_FM_RDS_msgType CGW_BCM_FM;
} _c_RDS10_buf;
typedef union _c_RDS11_bufTag
{
  vuint8 _c[8];
  _c_BCM_6_RDS_msgType BCM_6;
} _c_RDS11_buf;
typedef union _c_RDS12_bufTag
{
  vuint8 _c[8];
  _c_CGW_BCM_TPMS_2_RDS_msgType CGW_BCM_TPMS_2;
} _c_RDS12_buf;
typedef union _c_RDS13_bufTag
{
  vuint8 _c[7];
  _c_CGW_FCM_10_RDS_msgType CGW_FCM_10;
} _c_RDS13_buf;
typedef union _c_RDS14_bufTag
{
  vuint8 _c[8];
  _c_CGW_FCM_FRM_9_RDS_msgType CGW_FCM_FRM_9;
} _c_RDS14_buf;
typedef union _c_RDS15_bufTag
{
  vuint8 _c[8];
  _c_CGW_FCM_FRM_8_RDS_msgType CGW_FCM_FRM_8;
} _c_RDS15_buf;
typedef union _c_RDS16_bufTag
{
  vuint8 _c[6];
  _c_CGW_FCM_7_RDS_msgType CGW_FCM_7;
} _c_RDS16_buf;
typedef union _c_RDS17_bufTag
{
  vuint8 _c[6];
  _c_CGW_FCM_6_RDS_msgType CGW_FCM_6;
} _c_RDS17_buf;
typedef union _c_RDS18_bufTag
{
  vuint8 _c[6];
  _c_CGW_BCM_ACCM_3_RDS_msgType CGW_BCM_ACCM_3;
} _c_RDS18_buf;
typedef union _c_RDS19_bufTag
{
  vuint8 _c[7];
  _c_CGW_VCU_B_4_RDS_msgType CGW_VCU_B_4;
} _c_RDS19_buf;
typedef union _c_RDS20_bufTag
{
  vuint8 _c[8];
  _c_CGW_VCU_B_3_RDS_msgType CGW_VCU_B_3;
} _c_RDS20_buf;
typedef union _c_RDS21_bufTag
{
  vuint8 _c[3];
  _c_CGW_BMS_3_RDS_msgType CGW_BMS_3;
} _c_RDS21_buf;
typedef union _c_RDS22_bufTag
{
  vuint8 _c[8];
  _c_CGW_BCS_EPB_1_RDS_msgType CGW_BCS_EPB_1;
} _c_RDS22_buf;
typedef union _c_RDS23_bufTag
{
  vuint8 _c[6];
  _c_CGW_VCU_P_5_RDS_msgType CGW_VCU_P_5;
} _c_RDS23_buf;
typedef union _c_RDS_Tx_bufTag
{
  vuint8 _c[8];
  _c_NM_IHU_RDS_msgType NM_IHU;
  _c_IHU_ADAS_10_RDS_msgType IHU_ADAS_10;
  _c_IHU_ADAS_11_RDS_msgType IHU_ADAS_11;
  _c_IHU_18_RDS_msgType IHU_18;
  _c_IHU_13_RDS_msgType IHU_13;
  _c_IHU_12_RDS_msgType IHU_12;
  _c_IHU_11_RDS_msgType IHU_11;
  _c_IHU_10_RDS_msgType IHU_10;
  _c_IHU_8_RDS_msgType IHU_8;
  _c_IHU_7_RDS_msgType IHU_7;
  _c_IHU_6_RDS_msgType IHU_6;
  _c_IHU_5_RDS_msgType IHU_5;
  _c_IHU_4_RDS_msgType IHU_4;
  _c_IHU_3_RDS_msgType IHU_3;
  _c_IHU_17_RDS_msgType IHU_17;
  _c_IHU_16_RDS_msgType IHU_16;
  _c_IHU_15_RDS_msgType IHU_15;
  _c_IHU_14_RDS_msgType IHU_14;
  _c_IHU_1_RDS_msgType IHU_1;
  _c_IHU_MFS_RDS_msgType IHU_MFS;
} _c_RDS_Tx_buf;


/* -----------------------------------------------------------------------------
    &&&~ RDSMacrosForCanObjects_RI20
 ----------------------------------------------------------------------------- */

#define RDSBasic                             ((* ((_c_RDSBasic_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS1                                 ((* ((_c_RDS1_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS2                                 ((* ((_c_RDS2_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS3                                 ((* ((_c_RDS3_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS4                                 ((* ((_c_RDS4_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS5                                 ((* ((_c_RDS5_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS6                                 ((* ((_c_RDS6_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS7                                 ((* ((_c_RDS7_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS8                                 ((* ((_c_RDS8_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS9                                 ((* ((_c_RDS9_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS10                                ((* ((_c_RDS10_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS11                                ((* ((_c_RDS11_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS12                                ((* ((_c_RDS12_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS13                                ((* ((_c_RDS13_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS14                                ((* ((_c_RDS14_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS15                                ((* ((_c_RDS15_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS16                                ((* ((_c_RDS16_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS17                                ((* ((_c_RDS17_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS18                                ((* ((_c_RDS18_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS19                                ((* ((_c_RDS19_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS20                                ((* ((_c_RDS20_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS21                                ((* ((_c_RDS21_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS22                                ((* ((_c_RDS22_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDS23                                ((* ((_c_RDS23_buf MEMORY_NORMAL *)(canRDSRxPtr[0]))))
#define RDSTx                                ((* ((_c_RDS_Tx_buf MEMORY_NORMAL *)(canRDSTxPtr[0]))))




/* -----------------------------------------------------------------------------
    &&&~ Message Hardware Objects RI20
 ----------------------------------------------------------------------------- */

#define C_BASIC0_HW_OBJ                      0
#define C_BASIC0_HW_CHANNEL                  0
#define CanRxCGW_SAS_2_HW_OBJ                1
#define CanRxCGW_SAS_2_HW_CHANNEL            0
#define CanRxCGW_FCM_5_HW_OBJ                2
#define CanRxCGW_FCM_5_HW_CHANNEL            0
#define CanRxWCM_1_HW_OBJ                    3
#define CanRxWCM_1_HW_CHANNEL                0
#define CanRxAVAS_1_HW_OBJ                   4
#define CanRxAVAS_1_HW_CHANNEL               0
#define CanRxTBOX_7_HW_OBJ                   5
#define CanRxTBOX_7_HW_CHANNEL               0
#define CanRxBCM_ALM_HW_OBJ                  6
#define CanRxBCM_ALM_HW_CHANNEL              0
#define CanRxCGW_BCM_SCM_3_HW_OBJ            7
#define CanRxCGW_BCM_SCM_3_HW_CHANNEL        0
#define CanRxCGW_BCM_SCM_2_HW_OBJ            8
#define CanRxCGW_BCM_SCM_2_HW_CHANNEL        0
#define CanRxCGW_BCM_SCM_1_HW_OBJ            9
#define CanRxCGW_BCM_SCM_1_HW_CHANNEL        0
#define CanRxCGW_BCM_FM_HW_OBJ               10
#define CanRxCGW_BCM_FM_HW_CHANNEL           0
#define CanRxBCM_6_HW_OBJ                    11
#define CanRxBCM_6_HW_CHANNEL                0
#define CanRxCGW_BCM_TPMS_2_HW_OBJ           12
#define CanRxCGW_BCM_TPMS_2_HW_CHANNEL       0
#define CanRxCGW_FCM_10_HW_OBJ               13
#define CanRxCGW_FCM_10_HW_CHANNEL           0
#define CanRxCGW_FCM_FRM_9_HW_OBJ            14
#define CanRxCGW_FCM_FRM_9_HW_CHANNEL        0
#define CanRxCGW_FCM_FRM_8_HW_OBJ            15
#define CanRxCGW_FCM_FRM_8_HW_CHANNEL        0
#define CanRxCGW_FCM_7_HW_OBJ                16
#define CanRxCGW_FCM_7_HW_CHANNEL            0
#define CanRxCGW_FCM_6_HW_OBJ                17
#define CanRxCGW_FCM_6_HW_CHANNEL            0
#define CanRxCGW_BCM_ACCM_3_HW_OBJ           18
#define CanRxCGW_BCM_ACCM_3_HW_CHANNEL       0
#define CanRxCGW_VCU_B_4_HW_OBJ              19
#define CanRxCGW_VCU_B_4_HW_CHANNEL          0
#define CanRxCGW_VCU_B_3_HW_OBJ              20
#define CanRxCGW_VCU_B_3_HW_CHANNEL          0
#define CanRxCGW_BMS_3_HW_OBJ                21
#define CanRxCGW_BMS_3_HW_CHANNEL            0
#define CanRxCGW_BCS_EPB_1_HW_OBJ            22
#define CanRxCGW_BCS_EPB_1_HW_CHANNEL        0
#define CanRxCGW_VCU_P_5_HW_OBJ              23
#define CanRxCGW_VCU_P_5_HW_CHANNEL          0
#define C_TX_NORMAL_HW_OBJ                   24
#define C_TX_NORMAL_HW_CHANNEL               0





#endif /* __CAN_PAR_H__ */
