/* -----------------------------------------------------------------------------
  Filename:    il_par.h
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: Il_Vector
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\Documents and Settings\90506667\桌面\sh52\新建文件夹\S61EV_FLV1.4_2023_03_04.dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__IL_PAR_H__)
#define __IL_PAR_H__

/* -----------------------------------------------------------------------------
    &&&~ Typedefs
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX)
typedef vuint8 IltTxCounter;
#endif

#if defined(IL_ENABLE_TX)
typedef vuint8 IltTxUpdateCounter;
#endif

#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_TIMEOUT)
typedef vuint8 IltTxTimeoutCounter;
#endif

#if defined(IL_ENABLE_RX) && defined(IL_ENABLE_RX_TIMEOUT)
typedef vuint8 IltRxTimeoutCounter;
#endif

#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_SECURE_EVENT)
typedef vuint8 IltTxRepetitionCounter;
#endif

typedef struct IltIfActiveFlagsIHU_ADAS_11Tag
{
  vbittype IHU_11_FCWSwtSet : 1;
  vbittype IHU_11_FCWSnvtySet : 1;
  vbittype IHU_11_AEBSwtSet : 1;
  vbittype IHU_11_SCF_request_confirmed : 1;
  vbittype IHU_11_SCFSwtSet : 1;
  vbittype IHU_11_SLA_TSI_TLI_SwtSet : 1;
  vbittype IHU_11_LKASwtSet : 1;
  vbittype IHU_11_HMASwtSet : 1;
  vbittype IHU_11_IESSwtSet : 1;
  vbittype IHU_11_DAISwtSet : 1;
  vbittype IHU_11_LDPSwtSet : 1;
  vbittype IHU_11_WarnModSwtSet : 1;
  vbittype IHU_11_LDWSwtSet : 1;
  vbittype IHU_11_ELKSwtSet : 1;
  vbittype IHU_11_LDWLDPSnvtySetLDWLDP : 1;
  vbittype unused : 1;
} IltIfActiveFlagsIHU_ADAS_11;
typedef struct IltIfActiveFlagsIHU_18Tag
{
  vbittype IHU_AmbientLightBrightnessAdjust_Front : 1;
  vbittype IHU_AmbientLightBrightnessAdjust_Roof : 1;
  vbittype IHU_AmbientLightBrightnessAdjust_Rear : 1;
  vbittype IHU_AmbientLightColorSet_Front : 1;
  vbittype IHU_AmbientLightColorSet_Roof : 1;
  vbittype IHU_AmbientLightColorSet_Rear : 1;
  vbittype IHU_RegionColorSynchronizationReq : 1;
  vbittype IHU_AmbientLightModeSet : 1;
  vbittype IHU_AmbientLightEnableSwSts : 1;
  vbittype IHU_AmbientLightAreaDisplayEnableSwSts : 1;
  vbittype IHU_AmbientLightBreatheEnableSwSts : 1;
  vbittype IHU_AmbientLightBreatheWithVehSpdEnableSwSts : 1;
  vbittype unused : 4;
} IltIfActiveFlagsIHU_18;
typedef struct IltIfActiveFlagsIHU_13Tag
{
  vbittype IHU_Chg_SOC_LimitPointSet : 1;
  vbittype IHU_ChgModeSet : 1;
  vbittype IHU_BookChgStartTimeSet_Hour : 1;
  vbittype IHU_BookChgStartTimeSet_Minute : 1;
  vbittype IHU_BookChgTimeSet_VD : 1;
  vbittype IHU_BookChgStopTimeSet_Hour : 1;
  vbittype IHU_BookchgStopTimeSet_Minute : 1;
  vbittype unused : 1;
} IltIfActiveFlagsIHU_13;
typedef struct IltIfActiveFlagsIHU_10Tag
{
  vbittype IHU_RearWarnSystemEnableSwSts : 1;
  vbittype unused : 7;
} IltIfActiveFlagsIHU_10;
typedef struct IltIfActiveFlagsIHU_8Tag
{
  vbittype IHU_ICM_ThemelSet : 1;
  vbittype IHU_ICM_ScreenReq : 1;
  vbittype IHU_OverSpdValueSet : 1;
  vbittype IHU_ICM_Menu_OK_ButtonSts : 1;
  vbittype IHU_ICM_ModeButtonSts : 1;
  vbittype IHU_FatigureDrivingTimeSet : 1;
  vbittype IHU_ICM_UpButtonSts : 1;
  vbittype IHU_ICM_DownButtonSts : 1;
  vbittype IHU__ICM_LeftButtonSts : 1;
  vbittype IHU__ICM_RightButtonSts : 1;
  vbittype IHU_BrightnessAdjustSet_ICM : 1;
  vbittype IHU_DVR_QuickCameraSw : 1;
  vbittype unused : 4;
} IltIfActiveFlagsIHU_8;
typedef struct IltIfActiveFlagsIHU_7Tag
{
  vbittype IHU_SeatHeatLevelSetReq_RL : 1;
  vbittype IHU_SeatHeatLevelSetReq_RR : 1;
  vbittype IHU_WirelessChargingEnableSwSts : 1;
  vbittype IHU_AVAS_VolumeSet : 1;
  vbittype IHU_AVAS_AudioSourceSet : 1;
  vbittype IHU_AVAS_EnableSwSts : 1;
  vbittype IHU_SeatMemoryEnableSwSts : 1;
  vbittype IHU_SeatPositionStoreReq : 1;
  vbittype IHU_SeatPositionCalloutReq : 1;
  vbittype IHU_SeatHeatVentLevelSetReq_Drive : 1;
  vbittype IHU_SeatHeatVentLevelSetReq_Psngr : 1;
  vbittype IHU_FaceRecSeatEnableSwSts : 1;
  vbittype IHU_FaceRecMirrorInclineEnableSwSts : 1;
  vbittype IHU_FaceRecAuthenticationResult : 1;
  vbittype IHU_AccountDeletedReq : 1;
  vbittype IHU_EntryExitSeatCtrlEnableSwSts : 1;
} IltIfActiveFlagsIHU_7;
typedef struct IltIfActiveFlagsIHU_5Tag
{
  vbittype IHU_InverterConfirmSts : 1;
  vbittype IHU_InverterEnableSwSts : 1;
  vbittype IHU_CruiseTypeSetSts : 1;
  vbittype IHU_VehWashModeSet : 1;
  vbittype IHU_SpeedLimitValueSet : 1;
  vbittype IHU_SpeedLimitEnableSwSts : 1;
  vbittype IHU_RegenerationLevelSet : 1;
  vbittype IHU_DriveModeSet : 1;
  vbittype IHU_OutputLimitSOCSet : 1;
  vbittype IHU_ShiftVoiceRemind_EnableSwSts : 1;
  vbittype IHU_ShiftErrorActionRemindEnableSwSts : 1;
  vbittype IHU_SOC_ChargeLockEnableSwSts : 1;
  vbittype unused : 4;
} IltIfActiveFlagsIHU_5;
typedef struct IltIfActiveFlagsIHU_4Tag
{
  vbittype IHU_AutoLockEnableSwSts : 1;
  vbittype IHU_AntiTheftModeSetSwSts : 1;
  vbittype IHU_SteeringWheelHeatReq : 1;
  vbittype IHU_MirrorFoldEnableSwSts__ : 1;
  vbittype IHU_FollowHomeMeEnableSwSts : 1;
  vbittype IHU_SunroofAutoCloseEnableSwSts : 1;
  vbittype IHU_WelcomeLightEnableSwSts : 1;
  vbittype IHU_ComfortModeSet : 1;
  vbittype IHU_WiperSensitivitySet : 1;
  vbittype IHU_MirrorFoldReq : 1;
  vbittype IHU_AHB_EnableSwSts : 1;
  vbittype IHU_LowBeamAngleAdjust : 1;
  vbittype IHU_FrontWiperMaintenanceModeReq : 1;
  vbittype IHU_EmergencyPowerOffReqConfirm : 1;
  vbittype IHU_MirrorReverseInclineEnableSwSts : 1;
  vbittype IHU_FollowMeHomeTimeSetSts : 1;
  vbittype IHU_DoorWindowCtrlEnableSwSts : 1;
  vbittype IHU_WindowAutoCloseEnableSwSts : 1;
  vbittype IHU_RKE_SunRoofCtrlEnableSwSts : 1;
  vbittype IHU_DomeLightDoorCtrlSwitchSts : 1;
  vbittype IHU_MaxPositionSet : 1;
  vbittype IHU_FragranceEnableSwSts : 1;
  vbittype IHU_FragranceConcentrationSet : 1;
  vbittype IHU_FragrancePositionSet : 1;
} IltIfActiveFlagsIHU_4;
typedef struct IltIfActiveFlagsIHU_3Tag
{
  vbittype IHU_VoiceCtrl_SunroofReq : 1;
  vbittype IHU_VoiceCtrl_SunshadeReq : 1;
  vbittype IHU_VoiceCtrl_WindowReq_FL : 1;
  vbittype IHU_VoiceCtrl_WindowReq_FR : 1;
  vbittype IHU_VoiceCtrl_WindowReq_RL : 1;
  vbittype IHU_VoiceCtrl_WindowReq_RR : 1;
  vbittype IHU_VoiceCtrl_LowbeamReq : 1;
  vbittype IHU_VoiceCtrl_TrunkReq : 1;
  vbittype IHU_SmokingModeReq : 1;
  vbittype unused : 7;
} IltIfActiveFlagsIHU_3;
typedef struct IltIfActiveFlagsIHU_17Tag
{
  vbittype IHU_MassageSwithSet_Psngr : 1;
  vbittype IHU_MassageModeSet_Psngr : 1;
  vbittype IHU_MassageStrengthSet_Psngr : 1;
  vbittype IHU_LumbarSupportModeSet_Psngr : 1;
  vbittype IHU_MotoPositionSet_ForwardBack_Psngr : 1;
  vbittype IHU_MotoPositionSet_SeatBack_Psngr : 1;
  vbittype IHU_17_MsgAliveCounter : 1;
  vbittype IHU_MotoPositionSet_LegSupport_Psngr : 1;
} IltIfActiveFlagsIHU_17;
typedef struct IltIfActiveFlagsIHU_16Tag
{
  vbittype IHU_MotoPositionSet_ForwardBack_Driver : 1;
  vbittype IHU_MotoPositionSet_UpDown_Driver : 1;
  vbittype IHU_MotoPositionSet_SeatBack_Driver : 1;
  vbittype IHU_16_MsgAliveCounter : 1;
  vbittype IHU_MotoPositionSet_LegSupport_Driver : 1;
  vbittype unused : 3;
} IltIfActiveFlagsIHU_16;
typedef struct IltIfActiveFlagsIHU_1Tag
{
  vbittype IHU_EPB_SwSts : 1;
  vbittype IHU_BrakemodeSet : 1;
  vbittype IHU_EPS_ModeSet : 1;
  vbittype IHU_ESC_OFF_SwSts : 1;
  vbittype IHU_CST_FunctionEnableSwSts : 1;
  vbittype IHU_HDC_FunctionEnableSwSts : 1;
  vbittype IHU_AVH_FunctionEnableSwSts : 1;
  vbittype IHU_PDC_EnableSwSts : 1;
} IltIfActiveFlagsIHU_1;
typedef struct IlTIfActiveFlagTag
{
  IltIfActiveFlagsIHU_ADAS_11 IHU_ADAS_11;
  IltIfActiveFlagsIHU_18 IHU_18;
  IltIfActiveFlagsIHU_13 IHU_13;
  IltIfActiveFlagsIHU_10 IHU_10;
  IltIfActiveFlagsIHU_8 IHU_8;
  IltIfActiveFlagsIHU_7 IHU_7;
  IltIfActiveFlagsIHU_5 IHU_5;
  IltIfActiveFlagsIHU_4 IHU_4;
  IltIfActiveFlagsIHU_3 IHU_3;
  IltIfActiveFlagsIHU_17 IHU_17;
  IltIfActiveFlagsIHU_16 IHU_16;
  IltIfActiveFlagsIHU_1 IHU_1;
} IlTIfActiveFlag;


/* -----------------------------------------------------------------------------
    &&&~ Message Handles
 ----------------------------------------------------------------------------- */

#define IlRxMsgHndCGW_SAS_2                  0
#define IlRxMsgHndCGW_FCM_FRM_5              1
#define IlRxMsgHndCGW_FCM_5                  2
#define IlRxMsgHndWCM_1                      3
#define IlRxMsgHndAVAS_1                     4
#define IlRxMsgHndTBOX_7                     5
#define IlRxMsgHndCGW_OBC_1                  6
#define IlRxMsgHndBCM_ALM                    7
#define IlRxMsgHndCGW_BCM_SCM_3              8
#define IlRxMsgHndCGW_BCM_SCM_2              9
#define IlRxMsgHndCGW_BCM_SCM_1              10
#define IlRxMsgHndCGW_BCM_FM                 11
#define IlRxMsgHndBCM_6                      12
#define IlRxMsgHndCGW_BCM_TPMS_2             13
#define IlRxMsgHndCGW_FCM_10                 14
#define IlRxMsgHndCGW_FCM_FRM_9              15
#define IlRxMsgHndCGW_FCM_FRM_8              16
#define IlRxMsgHndCGW_FCM_7                  17
#define IlRxMsgHndCGW_FCM_6                  18
#define IlRxMsgHndCGW_BCM_ACCM_3             19
#define IlRxMsgHndCGW_BCM_ACCM_1             20
#define IlRxMsgHndICM_2                      21
#define IlRxMsgHndCGW_FCM_FRM_6              22
#define IlRxMsgHndCGW_BMS_9                  23
#define IlRxMsgHndCGW_VCU_E_3                24
#define IlRxMsgHndCGW_PLG_1                  25
#define IlRxMsgHndRCM_1                      26
#define IlRxMsgHndBCM_TPMS_1                 27
#define IlRxMsgHndCGW_BCM_4                  28
#define IlRxMsgHndCGW_BCM_5                  29
#define IlRxMsgHndCGW_VCU_B_5                30
#define IlRxMsgHndCGW_VCU_B_4                31
#define IlRxMsgHndCGW_VCU_B_3                32
#define IlRxMsgHndCGW_FCM_2                  33
#define IlRxMsgHndCGW_BSD_1                  34
#define IlRxMsgHndCGW_BCM_MFS_1              35
#define IlRxMsgHndCGW_BCM_PEPS_1             36
#define IlRxMsgHndCGW_VCU_B_2                37
#define IlRxMsgHndICM_1                      38
#define IlRxMsgHndCGW_BMS_3                  39
#define IlRxMsgHndCGW_BCM_1                  40
#define IlRxMsgHndCGW_EPS_1                  41
#define IlRxMsgHndCGW_BCS_EPB_1              42
#define IlRxMsgHndCGW_BCS_C_1                43
#define IlRxMsgHndCGW_EGS_1                  44
#define IlRxMsgHndCGW_VCU_P_5                45
#define IlTxMsgHndIHU_ADAS_10                0
#define IlTxMsgHndIHU_ADAS_11                1
#define IlTxMsgHndIHU_18                     2
#define IlTxMsgHndIHU_13                     3
#define IlTxMsgHndIHU_12                     4
#define IlTxMsgHndIHU_11                     5
#define IlTxMsgHndIHU_10                     6
#define IlTxMsgHndIHU_8                      7
#define IlTxMsgHndIHU_7                      8
#define IlTxMsgHndIHU_6                      9
#define IlTxMsgHndIHU_5                      10
#define IlTxMsgHndIHU_4                      11
#define IlTxMsgHndIHU_3                      12
#define IlTxMsgHndIHU_17                     13
#define IlTxMsgHndIHU_16                     14
#define IlTxMsgHndIHU_15                     15
#define IlTxMsgHndIHU_14                     16
#define IlTxMsgHndIHU_1                      17
#define IlTxMsgHndIHU_MFS                    18


/* -----------------------------------------------------------------------------
    &&&~ Signal Handles
 ----------------------------------------------------------------------------- */

#define IlRxSigHndSAS_SteeringAngleSpd_VD    IlRxMsgHndCGW_SAS_2
#define IlRxSigHndSAS_SteeringAngle_VD       IlRxMsgHndCGW_SAS_2
#define IlRxSigHndSAS_SteeringAngle          IlRxMsgHndCGW_SAS_2
#define IlRxSigHndSAS_SteeringAngleSpd       IlRxMsgHndCGW_SAS_2
#define IlRxSigHndFCM_FRM_5_SCFONOFFSts      IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_FRM_5_FCWONOFFSts      IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_FRM_5_FCWSnvtySts      IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_FRM_5_AEBONOFFSts      IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_FRM_5_DAIONOFFSts      IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_FRM_5_MsgCntr          IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_FRM_5_CRC              IlRxMsgHndCGW_FCM_FRM_5
#define IlRxSigHndFCM_5_HMAONOFFSts          IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_IESONOFFSts          IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_SLA_TSI_TLI_ONOFF_Sts IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_LCCONOFF_Sts         IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_WarnModSts           IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_LDWONOFFSts          IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_LKAONOFFSts          IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_ELKONOFFSts          IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_LDWLDPSnvtySts       IlRxMsgHndCGW_FCM_5
#define IlRxSigHndFCM_5_LDPONOFFSts          IlRxMsgHndCGW_FCM_5
#define IlRxSigHndWCM_WirelessChargingEnableSts IlRxMsgHndWCM_1
#define IlRxSigHndWCM_WirelessChargingSts    IlRxMsgHndWCM_1
#define IlRxSigHndWCM_ForeignBodyDetectedWaring IlRxMsgHndWCM_1
#define IlRxSigHndWCM_OverTempWarning        IlRxMsgHndWCM_1
#define IlRxSigHndWCM_MsgAliveCounter        IlRxMsgHndWCM_1
#define IlRxSigHndAVAS_VolumeSts             IlRxMsgHndAVAS_1
#define IlRxSigHndAVAS_SwSts                 IlRxMsgHndAVAS_1
#define IlRxSigHndAVAS_AudioSourceSts        IlRxMsgHndAVAS_1
#define IlRxSigHndTBOX_GPS_Time_year         IlRxMsgHndTBOX_7
#define IlRxSigHndTBOX_GPS_Time_Month        IlRxMsgHndTBOX_7
#define IlRxSigHndTBOX_GPS_Time_Day          IlRxMsgHndTBOX_7
#define IlRxSigHndTBOX_GPS_Time_Hour         IlRxMsgHndTBOX_7
#define IlRxSigHndTBOX_GPS_Time_Minute       IlRxMsgHndTBOX_7
#define IlRxSigHndTBOX_GPS_Time_Second       IlRxMsgHndTBOX_7
#define IlRxSigHndOBC_SOC_ChargeLockEnableSwSts IlRxMsgHndCGW_OBC_1
#define IlRxSigHndBCM_AmbientLightBrightnesssSts_Front IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightBrightnessSts_Roof IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightBrightnessSts_Rear IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightColorSta_Front IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightColorSts_Roof IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightColorStst_Rear IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_MsgAliveCounter        IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightModeSts    IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightEnableSts  IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightAreaDisplayEnableSts IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightBreatheEnableSts IlRxMsgHndBCM_ALM
#define IlRxSigHndBCM_AmbientLightBreatheWithVehSpdEnableSts IlRxMsgHndBCM_ALM
#define IlRxSigHndSCM_MassageSwithSts_Psngr  IlRxMsgHndCGW_BCM_SCM_3
#define IlRxSigHndSCM_MassageModeSts_Psngr   IlRxMsgHndCGW_BCM_SCM_3
#define IlRxSigHndSCM_MassageStrengthSts_Psngr IlRxMsgHndCGW_BCM_SCM_3
#define IlRxSigHndSCM_LumbarSupportModeSts_Psngr IlRxMsgHndCGW_BCM_SCM_3
#define IlRxSigHndSCM_SeatHeatVentSetSts_Psngr IlRxMsgHndCGW_BCM_SCM_2
#define IlRxSigHndSCM_DriverSeatLocalCtrlSwithSts_Psngr IlRxMsgHndCGW_BCM_SCM_2
#define IlRxSigHndSCM_MotoPosition_ForwardBack_Psngr IlRxMsgHndCGW_BCM_SCM_2
#define IlRxSigHndSCM_MotoPosition_SeatBack_Psngr IlRxMsgHndCGW_BCM_SCM_2
#define IlRxSigHndSCM_MsgAliveCounter_2      IlRxMsgHndCGW_BCM_SCM_2
#define IlRxSigHndSCM_MotoPosition_LegSupport_Psngr IlRxMsgHndCGW_BCM_SCM_2
#define IlRxSigHndSCM_SeatHeatVentSetSts_Drive IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndSCM_LocalCtrlSwithSts_Driver IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndSCM_MotoPosition_ForwardBack_Driver IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndSCM_MotoPosition_UpDown_Driver IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndSCM_MotoPosition_SeatBack_Driver IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndSCM_MsgAliveCounter_1      IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndSCM_MotoPosition_LegSupport_Driver IlRxMsgHndCGW_BCM_SCM_1
#define IlRxSigHndBCM_FM_FragranceEnableSts  IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FM_FragranceSystemFaultSts IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FM_FragranceConcentration IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FM_FragranceType_Position_1 IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FM_FragranceType_Position_2 IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FM_FragranceType_Position_3 IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FM_FragrancePositionSts IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndFM_FragranceReplaceRemind_1 IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndFM_FragranceReplaceRemind_2 IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndFM_FragranceReplaceRemind_3 IlRxMsgHndCGW_BCM_FM
#define IlRxSigHndBCM_FaceRecSeatEnableSts   IlRxMsgHndBCM_6
#define IlRxSigHndBCM_EntryExitSeatCtrlEnableSts IlRxMsgHndBCM_6
#define IlRxSigHndBCM_SeatPositionStoreResult IlRxMsgHndBCM_6
#define IlRxSigHndBCM_DriverFaceRecognitionResult IlRxMsgHndBCM_6
#define IlRxSigHndBCM_SeatPositionCalloutInhibitReq IlRxMsgHndBCM_6
#define IlRxSigHndBCM_SeatPositionCalloutSts IlRxMsgHndBCM_6
#define IlRxSigHndBCM_DriverFaceRecognitionSts IlRxMsgHndBCM_6
#define IlRxSigHndBCM_MirrorFoldEnableSts    IlRxMsgHndBCM_6
#define IlRxSigHndBCM_MirrorReverseInclineEnableSts IlRxMsgHndBCM_6
#define IlRxSigHndBCM_FaceRecMirrorInclineEnableSts IlRxMsgHndBCM_6
#define IlRxSigHndBCM_MirrorPositionMemoryHMIReq IlRxMsgHndBCM_6
#define IlRxSigHndBCM_MirrorCtrlSts_L        IlRxMsgHndBCM_6
#define IlRxSigHndBCM_MirrorCtrlSts_R        IlRxMsgHndBCM_6
#define IlRxSigHndBCM_MirrorReversePositionStoreResult IlRxMsgHndBCM_6
#define IlRxSigHndBCM_TirePressure_FL        IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TirePressure_FR        IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TirePressure_RL        IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TirePressure_RR        IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TireTemp_FL            IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TireTemp_FR            IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TireTemp_RL            IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndBCM_TireTemp_RR            IlRxMsgHndCGW_BCM_TPMS_2
#define IlRxSigHndFCM_10_TSISgnGiWay         IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_TLISysSts           IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_TSISysSts           IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_TSISgnLongStayDisp  IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_TSISgnForb          IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_LDWSysSts           IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_LDPSysSts           IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_TLITrfcLi           IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_LCCEnaRcmend        IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_LCCText             IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_TSINoParkWarn       IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_ELKSysSts           IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_WarnModSts          IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_SLAONOFFSts         IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_LCCExitTextInfoSts  IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_LDPTJAELKTakeoverReq IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_EgoLeftLineHeatgAg  IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_10_ELKIntvMod          IlRxMsgHndCGW_FCM_10
#define IlRxSigHndFCM_FRM_9_ACCObjLgtDstX    IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_ACCObjTyp        IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_LeObjID          IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_ACCObjHozDstY    IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_FrntFarObjLgtDstX IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_FrntFarObjTyp    IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_FrntFarObjID     IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_FrntFarObjHozDstY IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_MessageCounter   IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_9_CRC              IlRxMsgHndCGW_FCM_FRM_9
#define IlRxSigHndFCM_FRM_8_ACCObjLgtDstX    IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_ACCObjTyp        IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_ACCObjID         IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_ACCObjHozDstY    IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_FrntFarObjLgtDstX IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_FrntFarObjTyp    IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_FrntFarObjID     IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_FrntFarObjHozDstY IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_MessageCounter   IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_FRM_8_CRC              IlRxMsgHndCGW_FCM_FRM_8
#define IlRxSigHndFCM_7_NeborLeLineHozlDst   IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborLeLineTyp       IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborLeLineColor     IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborLeLineID        IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborLeLineCrvt      IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborRiLineHozlDst   IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborRiLineTyp       IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborRiLineColor     IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborRiLineID        IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_7_NeborRiLineCrvt      IlRxMsgHndCGW_FCM_7
#define IlRxSigHndFCM_6_EgoLeLineHozlDst     IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoLeLineTyp         IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoLeLineColor       IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoLeLineID          IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoLeLineCrvt        IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoRiLineHozlDst     IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoRiLineTyp         IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoRiLineColor       IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoRiLineID          IlRxMsgHndCGW_FCM_6
#define IlRxSigHndFCM_6_EgoRiLineCrvt        IlRxMsgHndCGW_FCM_6
#define IlRxSigHndACCM_AQS_EnableSts         IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndACCM_PM2_5_InternalValue   IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndACCM_PM2_5_ExternalValue   IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndACCM_PM2_5_InternalLevel   IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndACCM_PM2_5_ExternalLevel   IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndACCM_SmokingModeSts        IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndAQS_AQS_AirQualityLevel    IlRxMsgHndCGW_BCM_ACCM_3
#define IlRxSigHndACCM_AUTO_Sts              IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_AC_Sts                IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_PTC_Sts               IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_System_ON_OFF_Sts     IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_CirculationModeSts    IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_BlowModeSts           IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_TempSetSts_Driver     IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_BlowerlevelSetSts     IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_DUAL_EnablaSts        IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_AutoDefrostSts        IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_VentilationSts        IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_InternalTemp          IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_ExternalTemp          IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_FrontDefrostSts       IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_AnionGeneratorSts     IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_ECO_Mode              IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndACCM_TempSetSts_Psngr      IlRxMsgHndCGW_BCM_ACCM_1
#define IlRxSigHndICM_ThemelSetSts           IlRxMsgHndICM_2
#define IlRxSigHndICM_BrightnessAdjustSetSts IlRxMsgHndICM_2
#define IlRxSigHndICM_FatigureDrivingTimeSetSts IlRxMsgHndICM_2
#define IlRxSigHndICM_OverSpdValueSetSts     IlRxMsgHndICM_2
#define IlRxSigHndFCM_FRM_6_TextinfoWarn     IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_Textinfo_Info    IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_SCF_PopoverReq   IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_TimeGapSetICM    IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_FCW_preWarning   IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_DistanceWarning  IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_DrvrCfmSCFDispFb IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_SCF_SpdLimUnit   IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_SCF_SpdLimSts    IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_AEBMode          IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_FCWMode          IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_TakeOverReq      IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_ACCMode          IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_VSetDis          IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_MsgCntr          IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndFCM_FRM_6_CRC              IlRxMsgHndCGW_FCM_FRM_6
#define IlRxSigHndBMS_ChgWireConnectStsDisp  IlRxMsgHndCGW_BMS_9
#define IlRxSigHndBMS_SOC_Disp               IlRxMsgHndCGW_BMS_9
#define IlRxSigHndBMS_ChgSts                 IlRxMsgHndCGW_BMS_9
#define IlRxSigHndVCU_Chg_SOC_LimitSts       IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookChgModeSts         IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookChgStartTimeSts_Hour IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookChgStartTimeSts_Minute IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookChgSts1            IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookchgStopTimeSts_Hour IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookChgStopTimeSts_Minute IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndVCU_BookChgSts2            IlRxMsgHndCGW_VCU_E_3
#define IlRxSigHndPLG_LockSts                IlRxMsgHndCGW_PLG_1
#define IlRxSigHndPLG_MaxPositionSetSts      IlRxMsgHndCGW_PLG_1
#define IlRxSigHndRCM_SnsDistance_RL         IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_RMR        IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_RML        IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_RR         IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_FL         IlRxMsgHndRCM_1
#define IlRxSigHndRCM_AudibleBeepRate        IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_FML        IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_FMR        IlRxMsgHndRCM_1
#define IlRxSigHndRCM_SnsDistance_FR         IlRxMsgHndRCM_1
#define IlRxSigHndRCM_WorkingSts             IlRxMsgHndRCM_1
#define IlRxSigHndRCM_DetectingSts           IlRxMsgHndRCM_1
#define IlRxSigHndBCM_TirePressureWarnLampDisp IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureSystemFault IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureWarn_FL    IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireTempWarn_FL        IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureDelta_FL   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorFault_FL     IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorLowBatt_FL   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorCalibrationSts_FL IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureWarn_FR    IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireTempWarn_FR        IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureDelta_FR   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorFault_FR     IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorLowBatt_FR   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorCalibrationSts_FR IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureWarn_RL    IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireTempWarn_RL        IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureDelta_RL   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorFault_RL     IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorLowBatt_RL   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorCalibrationSts_RL IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureWarn_RR    IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireTempWarn_RR        IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TirePressureDelta_RR   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorFault_RR     IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorLowBatt_RR   IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_TireSensorCalibrationSts_RR IlRxMsgHndBCM_TPMS_1
#define IlRxSigHndBCM_WinPostionSts_FL       IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_WinPostionSts_FR       IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_WinPostionSts_RL       IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_WinPostionSts_RR       IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_SunroofPositionSts     IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_SunroofSts             IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_SunshadeReqSts         IlRxMsgHndCGW_BCM_4
#define IlRxSigHndBCM_AutoLockEnableSts      IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_AntiTheftModeSts       IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_FollowMeHomeSts        IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_SunroofAutoCloseEnableSts IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_FollowMeTimeSetSts     IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_WiperSensitivitySts    IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_RearDfstSts            IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_LowBeamAngleAdjustSts  IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_FrontWiperMaintenanceModeSts IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_MirrorUnfoldRemind     IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_DoorWindowCtrlEnableSts IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_SunroofCtrlEnableSts_RF IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_SunroofCtrlEnableSts_Remote IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_WelcomelightEnableSts  IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_WindowAutoCloseEnableSts IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_RKE_SunRoofCtrlEnableSts IlRxMsgHndCGW_BCM_5
#define IlRxSigHndIHU_AHB_EnableSts          IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_WelcomeAnimationReq    IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_SteeringWheelHeatSts   IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_AutoBlowEnableSts      IlRxMsgHndCGW_BCM_5
#define IlRxSigHndBCM_DomeLightDoorCtrlSwitchSts IlRxMsgHndCGW_BCM_5
#define IlRxSigHndVCU_DriveMode              IlRxMsgHndCGW_VCU_B_5
#define IlRxSigHndVCU_LongRangeRemainingTime IlRxMsgHndCGW_VCU_B_5
#define IlRxSigHndVCU_ResidualOdometer       IlRxMsgHndCGW_VCU_B_4
#define IlRxSigHndVCU_InstPowerConsum        IlRxMsgHndCGW_VCU_B_4
#define IlRxSigHndVCU_TotalPowerDisp         IlRxMsgHndCGW_VCU_B_4
#define IlRxSigHndVCU_InstPowerDisp          IlRxMsgHndCGW_VCU_B_4
#define IlRxSigHndVCU_ManualToGearP_InhibitSts IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_ShiftErrorActionRemindEnableSts IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterEnableSwSts    IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterPower          IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterInd_1          IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterInd_2          IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterInd_3          IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterInd_4          IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterFaultSts       IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_220V_OutputTimeInd     IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_PowerOffAutoParkInhibitSts IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_DoorOpenParkInhibitSts IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_InverterEnableSts      IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndVCU_OutputLimitSOC         IlRxMsgHndCGW_VCU_B_3
#define IlRxSigHndHMA_Status                 IlRxMsgHndCGW_FCM_2
#define IlRxSigHndLDW_ELK_LKA_LDPLeftVisuali IlRxMsgHndCGW_FCM_2
#define IlRxSigHndLKA_State                  IlRxMsgHndCGW_FCM_2
#define IlRxSigHndLDW_ELK_LKA_LDPRightVisuali IlRxMsgHndCGW_FCM_2
#define IlRxSigHndSLASpdlimit                IlRxMsgHndCGW_FCM_2
#define IlRxSigHndSLAState                   IlRxMsgHndCGW_FCM_2
#define IlRxSigHndSLASpdlimitWarning         IlRxMsgHndCGW_FCM_2
#define IlRxSigHndCamera_textinfo            IlRxMsgHndCGW_FCM_2
#define IlRxSigHndFCM_2_SLASpdlimitUnits     IlRxMsgHndCGW_FCM_2
#define IlRxSigHndLCC_mode                   IlRxMsgHndCGW_FCM_2
#define IlRxSigHndBSD_RearWarnSystemEnableSts IlRxMsgHndCGW_BSD_1
#define IlRxSigHndMFS_LeftWheelButtonSts_ScrollUp IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_LeftWheelButtonSts_ScrollDown IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_LeftWheelButtonSts_ToggleLeft IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_LeftWheelButtonSts_ToggleRight IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_LeftWheelButtonSts_Press IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_WeChatButtonSts        IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_CustomButtonSts        IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_RightWheelButtonSts_ScrollUp IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_RightWheelButtonSts_ScrollDown IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_RightWheelButtonSts_ToggleLeft IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_RightWheelButtonSts_ToggleRight IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_RightWheelButtonSts_Press IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_ReturnButtonSts        IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_VoiceRecognitionButtonSts IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_MsgAliveCounter        IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndMFS_Checksum_CRC           IlRxMsgHndCGW_BCM_MFS_1
#define IlRxSigHndBCM_EmergencyPowerOffRemindSts_1 IlRxMsgHndCGW_BCM_PEPS_1
#define IlRxSigHndBCM_EmergencyPowerOffRemindSts_2 IlRxMsgHndCGW_BCM_PEPS_1
#define IlRxSigHndVCU_CruiseCtrlStsDisp      IlRxMsgHndCGW_VCU_B_2
#define IlRxSigHndVCU_RegenerationLevelSts   IlRxMsgHndCGW_VCU_B_2
#define IlRxSigHndVCU_SpdLimitSts            IlRxMsgHndCGW_VCU_B_2
#define IlRxSigHndVCU_SpeedLimitEnableSts    IlRxMsgHndCGW_VCU_B_2
#define IlRxSigHndVCU_SpeedLimitValueSet     IlRxMsgHndCGW_VCU_B_2
#define IlRxSigHndICM_TotalOdometer          IlRxMsgHndICM_1
#define IlRxSigHndICM_LowSOC_LampSts         IlRxMsgHndICM_1
#define IlRxSigHndBMS_HV_BattVolt            IlRxMsgHndCGW_BMS_3
#define IlRxSigHndBMS_HV_BattCurr            IlRxMsgHndCGW_BMS_3
#define IlRxSigHndBCM_KeySts                 IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_RearFogLightSts        IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_DoorAjarSts_FL         IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_DoorAjarSts_FR         IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_DoorAjarSts_RR         IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_DoorAjarSts_RL         IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_HoodAjarSts            IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_TrunkAjarSts           IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_RearWashSwSts          IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_FrontWiperSwSts        IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_RearWiperSwSts         IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_LightCtrlSwSts         IlRxMsgHndCGW_BCM_1
#define IlRxSigHndBCM_AntiTheftMode          IlRxMsgHndCGW_BCM_1
#define IlRxSigHndEPS_WorkingModeSts         IlRxMsgHndCGW_EPS_1
#define IlRxSigHndBCS_EPB_HMI_RemindReq      IlRxMsgHndCGW_BCS_EPB_1
#define IlRxSigHndBCS_EPB_ActuatorSts        IlRxMsgHndCGW_BCS_EPB_1
#define IlRxSigHndEPB_AutoApplyDisableSts    IlRxMsgHndCGW_BCS_EPB_1
#define IlRxSigHndBCS_EPB_MsgAliveCounter    IlRxMsgHndCGW_BCS_EPB_1
#define IlRxSigHndBCS_EPB_Checksum_CRC       IlRxMsgHndCGW_BCS_EPB_1
#define IlRxSigHndBCS_HDC_FunctionEnableSts  IlRxMsgHndCGW_BCS_C_1
#define IlRxSigHndBCS_AVH_FunctionEnableSts  IlRxMsgHndCGW_BCS_C_1
#define IlRxSigHndBCS_CST_FunctionEnableSts  IlRxMsgHndCGW_BCS_C_1
#define IlRxSigHndBCS_CST_Active             IlRxMsgHndCGW_BCS_C_1
#define IlRxSigHndBCS_BrakemodeSts           IlRxMsgHndCGW_BCS_C_1
#define IlRxSigHndEGS_FaultLevel             IlRxMsgHndCGW_EGS_1
#define IlRxSigHndEGS_HighBeamSwitchActiveSts IlRxMsgHndCGW_EGS_1
#define IlRxSigHndEGS_HighBeamFlashSwitchActiveSts IlRxMsgHndCGW_EGS_1
#define IlRxSigHndEGS_FrontWiperMistSwitchActiveSts IlRxMsgHndCGW_EGS_1
#define IlRxSigHndEGS_FrontWashSwitchActiveSts IlRxMsgHndCGW_EGS_1
#define IlRxSigHndVCU_ActGear                IlRxMsgHndCGW_VCU_P_5
#define IlRxSigHndVCU_VehSpd                 IlRxMsgHndCGW_VCU_P_5
#define IlRxSigHndVCU_BrakePedalSts          IlRxMsgHndCGW_VCU_P_5
#define IlRxSigHndVCU_PowertrainReadySts     IlRxMsgHndCGW_VCU_P_5
#define IlRxSigHndVCU_BrakePedal_VD          IlRxMsgHndCGW_VCU_P_5
#define IlRxSigHndVCU_ActGear_VD             IlRxMsgHndCGW_VCU_P_5
#define IlRxSigHndVCU_VehSpd_VD              IlRxMsgHndCGW_VCU_P_5
#define IlTxSigHndNavSpeedLimitStatus        IlTxMsgHndIHU_ADAS_10
#define IlTxSigHndNavSpeedLimit              IlTxMsgHndIHU_ADAS_10
#define IlTxSigHndNavSpeedLimitUnits         IlTxMsgHndIHU_ADAS_10
#define IlTxSigHndNavCurrRoadType            IlTxMsgHndIHU_ADAS_10
#define IlTxSigHndIHU_11_FCWSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_FCWSnvtySet         IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_AEBSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_SCF_request_confirmed IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_SCFSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_SLA_TSI_TLI_SwtSet  IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_LKASwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_HMASwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_IESSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_DAISwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_LDPSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_WarnModSwtSet       IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_LDWSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_ELKSwtSet           IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_11_LDWLDPSnvtySetLDWLDP IlTxMsgHndIHU_ADAS_11
#define IlTxSigHndIHU_AmbientLightBrightnessAdjust_Front IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightBrightnessAdjust_Roof IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightBrightnessAdjust_Rear IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightColorSet_Front IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightColorSet_Roof IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightColorSet_Rear IlTxMsgHndIHU_18
#define IlTxSigHndIHU_RegionColorSynchronizationReq IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightModeSet    IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightEnableSwSts IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightAreaDisplayEnableSwSts IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightBreatheEnableSwSts IlTxMsgHndIHU_18
#define IlTxSigHndIHU_AmbientLightBreatheWithVehSpdEnableSwSts IlTxMsgHndIHU_18
#define IlTxSigHndIHU_Chg_SOC_LimitPointSet  IlTxMsgHndIHU_13
#define IlTxSigHndIHU_ChgModeSet             IlTxMsgHndIHU_13
#define IlTxSigHndIHU_BookChgStartTimeSet_Hour IlTxMsgHndIHU_13
#define IlTxSigHndIHU_BookChgStartTimeSet_Minute IlTxMsgHndIHU_13
#define IlTxSigHndIHU_BookChgTimeSet_VD      IlTxMsgHndIHU_13
#define IlTxSigHndIHU_BookChgStopTimeSet_Hour IlTxMsgHndIHU_13
#define IlTxSigHndIHU_BookchgStopTimeSet_Minute IlTxMsgHndIHU_13
#define IlTxSigHndIHU_MusicLoudness_120HZ    IlTxMsgHndIHU_12
#define IlTxSigHndIHU_VoiceAssistantActiveSts IlTxMsgHndIHU_12
#define IlTxSigHndIHU_MusicLoudness_250HZ    IlTxMsgHndIHU_12
#define IlTxSigHndIHU_MusicLoudness_500HZ    IlTxMsgHndIHU_12
#define IlTxSigHndIHU_MusicLoudness_1000HZ   IlTxMsgHndIHU_12
#define IlTxSigHndIHU_MusicLoudness_1500HZ   IlTxMsgHndIHU_12
#define IlTxSigHndIHU_MusicLoudness_2000HZ   IlTxMsgHndIHU_12
#define IlTxSigHndIHU_MusicLoudness_6000HZ   IlTxMsgHndIHU_12
#define IlTxSigHndIHU_GPS_Time_year          IlTxMsgHndIHU_11
#define IlTxSigHndIHU_GPS_TimeSwSts          IlTxMsgHndIHU_11
#define IlTxSigHndIHU_GPS_Time_Month         IlTxMsgHndIHU_11
#define IlTxSigHndIHU_GPS_Time_Day           IlTxMsgHndIHU_11
#define IlTxSigHndIHU_GPS_Time_Hour          IlTxMsgHndIHU_11
#define IlTxSigHndIHU_GPS_Time_Minute        IlTxMsgHndIHU_11
#define IlTxSigHndIHU_GPS_Time_Second        IlTxMsgHndIHU_11
#define IlTxSigHndIHU_RearWarnSystemEnableSwSts IlTxMsgHndIHU_10
#define IlTxSigHndIHU_ICM_ThemelSet          IlTxMsgHndIHU_8
#define IlTxSigHndIHU_ICM_ScreenReq          IlTxMsgHndIHU_8
#define IlTxSigHndIHU_OverSpdValueSet        IlTxMsgHndIHU_8
#define IlTxSigHndIHU_ICM_Menu_OK_ButtonSts  IlTxMsgHndIHU_8
#define IlTxSigHndIHU_ICM_ModeButtonSts      IlTxMsgHndIHU_8
#define IlTxSigHndIHU_FatigureDrivingTimeSet IlTxMsgHndIHU_8
#define IlTxSigHndIHU_ICM_UpButtonSts        IlTxMsgHndIHU_8
#define IlTxSigHndIHU_ICM_DownButtonSts      IlTxMsgHndIHU_8
#define IlTxSigHndIHU__ICM_LeftButtonSts     IlTxMsgHndIHU_8
#define IlTxSigHndIHU__ICM_RightButtonSts    IlTxMsgHndIHU_8
#define IlTxSigHndIHU_BrightnessAdjustSet_ICM IlTxMsgHndIHU_8
#define IlTxSigHndIHU_DVR_QuickCameraSw      IlTxMsgHndIHU_8
#define IlTxSigHndIHU_SeatHeatLevelSetReq_RL IlTxMsgHndIHU_7
#define IlTxSigHndIHU_SeatHeatLevelSetReq_RR IlTxMsgHndIHU_7
#define IlTxSigHndIHU_WirelessChargingEnableSwSts IlTxMsgHndIHU_7
#define IlTxSigHndIHU_AVAS_VolumeSet         IlTxMsgHndIHU_7
#define IlTxSigHndIHU_AVAS_AudioSourceSet    IlTxMsgHndIHU_7
#define IlTxSigHndIHU_AVAS_EnableSwSts       IlTxMsgHndIHU_7
#define IlTxSigHndIHU_SeatMemoryEnableSwSts  IlTxMsgHndIHU_7
#define IlTxSigHndIHU_SeatPositionStoreReq   IlTxMsgHndIHU_7
#define IlTxSigHndIHU_SeatPositionCalloutReq IlTxMsgHndIHU_7
#define IlTxSigHndIHU_SeatHeatVentLevelSetReq_Drive IlTxMsgHndIHU_7
#define IlTxSigHndIHU_SeatHeatVentLevelSetReq_Psngr IlTxMsgHndIHU_7
#define IlTxSigHndIHU_FaceRecSeatEnableSwSts IlTxMsgHndIHU_7
#define IlTxSigHndIHU_FaceRecMirrorInclineEnableSwSts IlTxMsgHndIHU_7
#define IlTxSigHndIHU_FaceRecAuthenticationResult IlTxMsgHndIHU_7
#define IlTxSigHndIHU_AccountDeletedReq      IlTxMsgHndIHU_7
#define IlTxSigHndIHU_EntryExitSeatCtrlEnableSwSts IlTxMsgHndIHU_7
#define IlTxSigHndIHU_AUTO_SwSts             IlTxMsgHndIHU_6
#define IlTxSigHndIHU_AC_SwSts               IlTxMsgHndIHU_6
#define IlTxSigHndIHU_PTC_SwSts              IlTxMsgHndIHU_6
#define IlTxSigHndIHU_OFF_SwSts              IlTxMsgHndIHU_6
#define IlTxSigHndIHU_RearDfstSwSts          IlTxMsgHndIHU_6
#define IlTxSigHndIHU_CirculationModeSet     IlTxMsgHndIHU_6
#define IlTxSigHndIHU_AutoDfstSwSts          IlTxMsgHndIHU_6
#define IlTxSigHndIHU_BlowModeSet            IlTxMsgHndIHU_6
#define IlTxSigHndIHU_TempSet_Driver         IlTxMsgHndIHU_6
#define IlTxSigHndIHU_BlowerlevelSet         IlTxMsgHndIHU_6
#define IlTxSigHndIHU_AnionGeneratorSwSts    IlTxMsgHndIHU_6
#define IlTxSigHndIHU_AQS_EnablaSw           IlTxMsgHndIHU_6
#define IlTxSigHndIHU_AutoBlowEnablaSw       IlTxMsgHndIHU_6
#define IlTxSigHndIHU_AC_MemoryModeEnablaSw  IlTxMsgHndIHU_6
#define IlTxSigHndIHU_DUAL_EnablaSw          IlTxMsgHndIHU_6
#define IlTxSigHndIHU_FrontDefrostSwSts      IlTxMsgHndIHU_6
#define IlTxSigHndIHU_VentilationSwSts       IlTxMsgHndIHU_6
#define IlTxSigHndIHU_ECO_SwSts              IlTxMsgHndIHU_6
#define IlTxSigHndIHU_TempSet_Psngr          IlTxMsgHndIHU_6
#define IlTxSigHndIHU_LightCtrlSwSts         IlTxMsgHndIHU_6
#define IlTxSigHndIHU_FrontWiperSwSts        IlTxMsgHndIHU_6
#define IlTxSigHndIHU_RearFogLightSwSts      IlTxMsgHndIHU_6
#define IlTxSigHndIHU_MirrorReversePositionStoreReq IlTxMsgHndIHU_6
#define IlTxSigHndIHU_MirrorCtrlReq_L        IlTxMsgHndIHU_6
#define IlTxSigHndIHU_MirrorCtrlReq_R        IlTxMsgHndIHU_6
#define IlTxSigHndIHU_RearWiperSwSts         IlTxMsgHndIHU_6
#define IlTxSigHndIHU_RearWashSwSts          IlTxMsgHndIHU_6
#define IlTxSigHndIHU_InverterConfirmSts     IlTxMsgHndIHU_5
#define IlTxSigHndIHU_InverterEnableSwSts    IlTxMsgHndIHU_5
#define IlTxSigHndIHU_CruiseTypeSetSts       IlTxMsgHndIHU_5
#define IlTxSigHndIHU_VehWashModeSet         IlTxMsgHndIHU_5
#define IlTxSigHndIHU_SpeedLimitValueSet     IlTxMsgHndIHU_5
#define IlTxSigHndIHU_SpeedLimitEnableSwSts  IlTxMsgHndIHU_5
#define IlTxSigHndIHU_RegenerationLevelSet   IlTxMsgHndIHU_5
#define IlTxSigHndIHU_DriveModeSet           IlTxMsgHndIHU_5
#define IlTxSigHndIHU_OutputLimitSOCSet      IlTxMsgHndIHU_5
#define IlTxSigHndIHU_ShiftVoiceRemind_EnableSwSts IlTxMsgHndIHU_5
#define IlTxSigHndIHU_ShiftErrorActionRemindEnableSwSts IlTxMsgHndIHU_5
#define IlTxSigHndIHU_SOC_ChargeLockEnableSwSts IlTxMsgHndIHU_5
#define IlTxSigHndIHU_AutoLockEnableSwSts    IlTxMsgHndIHU_4
#define IlTxSigHndIHU_AntiTheftModeSetSwSts  IlTxMsgHndIHU_4
#define IlTxSigHndIHU_SteeringWheelHeatReq   IlTxMsgHndIHU_4
#define IlTxSigHndIHU_MirrorFoldEnableSwSts__ IlTxMsgHndIHU_4
#define IlTxSigHndIHU_FollowHomeMeEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_SunroofAutoCloseEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_WelcomeLightEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_ComfortModeSet         IlTxMsgHndIHU_4
#define IlTxSigHndIHU_WiperSensitivitySet    IlTxMsgHndIHU_4
#define IlTxSigHndIHU_MirrorFoldReq          IlTxMsgHndIHU_4
#define IlTxSigHndIHU_AHB_EnableSwSts        IlTxMsgHndIHU_4
#define IlTxSigHndIHU_LowBeamAngleAdjust     IlTxMsgHndIHU_4
#define IlTxSigHndIHU_FrontWiperMaintenanceModeReq IlTxMsgHndIHU_4
#define IlTxSigHndIHU_EmergencyPowerOffReqConfirm IlTxMsgHndIHU_4
#define IlTxSigHndIHU_MirrorReverseInclineEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_FollowMeHomeTimeSetSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_DoorWindowCtrlEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_WindowAutoCloseEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_RKE_SunRoofCtrlEnableSwSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_DomeLightDoorCtrlSwitchSts IlTxMsgHndIHU_4
#define IlTxSigHndIHU_MaxPositionSet         IlTxMsgHndIHU_4
#define IlTxSigHndIHU_FragranceEnableSwSts   IlTxMsgHndIHU_4
#define IlTxSigHndIHU_FragranceConcentrationSet IlTxMsgHndIHU_4
#define IlTxSigHndIHU_FragrancePositionSet   IlTxMsgHndIHU_4
#define IlTxSigHndIHU_VoiceCtrl_SunroofReq   IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_SunshadeReq  IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_WindowReq_FL IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_WindowReq_FR IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_WindowReq_RL IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_WindowReq_RR IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_LowbeamReq   IlTxMsgHndIHU_3
#define IlTxSigHndIHU_VoiceCtrl_TrunkReq     IlTxMsgHndIHU_3
#define IlTxSigHndIHU_SmokingModeReq         IlTxMsgHndIHU_3
#define IlTxSigHndIHU_MassageSwithSet_Psngr  IlTxMsgHndIHU_17
#define IlTxSigHndIHU_MassageModeSet_Psngr   IlTxMsgHndIHU_17
#define IlTxSigHndIHU_MassageStrengthSet_Psngr IlTxMsgHndIHU_17
#define IlTxSigHndIHU_LumbarSupportModeSet_Psngr IlTxMsgHndIHU_17
#define IlTxSigHndIHU_MotoPositionSet_ForwardBack_Psngr IlTxMsgHndIHU_17
#define IlTxSigHndIHU_MotoPositionSet_SeatBack_Psngr IlTxMsgHndIHU_17
#define IlTxSigHndIHU_17_MsgAliveCounter     IlTxMsgHndIHU_17
#define IlTxSigHndIHU_MotoPositionSet_LegSupport_Psngr IlTxMsgHndIHU_17
#define IlTxSigHndIHU_MotoPositionSet_ForwardBack_Driver IlTxMsgHndIHU_16
#define IlTxSigHndIHU_MotoPositionSet_UpDown_Driver IlTxMsgHndIHU_16
#define IlTxSigHndIHU_MotoPositionSet_SeatBack_Driver IlTxMsgHndIHU_16
#define IlTxSigHndIHU_16_MsgAliveCounter     IlTxMsgHndIHU_16
#define IlTxSigHndIHU_MotoPositionSet_LegSupport_Driver IlTxMsgHndIHU_16
#define IlTxSigHndIHU_AccountCreatSts_Num1   IlTxMsgHndIHU_15
#define IlTxSigHndIHU_AccountCreatSts_Num2   IlTxMsgHndIHU_15
#define IlTxSigHndIHU_AccountCreatSts_Num3   IlTxMsgHndIHU_15
#define IlTxSigHndIHU_DriverFaceRecognitionSts IlTxMsgHndIHU_15
#define IlTxSigHndIHU_RadioFrequanceMode     IlTxMsgHndIHU_14
#define IlTxSigHndIHU_RadioResearchSts       IlTxMsgHndIHU_14
#define IlTxSigHndIHU_SourceStationMode      IlTxMsgHndIHU_14
#define IlTxSigHndIHU_PowerOnSts             IlTxMsgHndIHU_14
#define IlTxSigHndIHU_FM_RadioFrequanceValue IlTxMsgHndIHU_14
#define IlTxSigHndIHU_AM_RadioFrequanceValue IlTxMsgHndIHU_14
#define IlTxSigHndIHU_RadioVolume            IlTxMsgHndIHU_14
#define IlTxSigHndIHU_EPB_SwSts              IlTxMsgHndIHU_1
#define IlTxSigHndIHU_BrakemodeSet           IlTxMsgHndIHU_1
#define IlTxSigHndIHU_EPS_ModeSet            IlTxMsgHndIHU_1
#define IlTxSigHndIHU_ESC_OFF_SwSts          IlTxMsgHndIHU_1
#define IlTxSigHndIHU_CST_FunctionEnableSwSts IlTxMsgHndIHU_1
#define IlTxSigHndIHU_HDC_FunctionEnableSwSts IlTxMsgHndIHU_1
#define IlTxSigHndIHU_AVH_FunctionEnableSwSts IlTxMsgHndIHU_1
#define IlTxSigHndIHU_PDC_EnableSwSts        IlTxMsgHndIHU_1
#define IlTxSigHndIHU_MFS_CruiseCtrlEnableButtonSts IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MFS_CruiseCancelButtonSts IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MFS_CruiseSetButtonSts IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MFS_CruiseResumeButtonSts IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MFS_SpdLimitButtonSts  IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MFS_ACC_GapSB1         IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MFS_ACC_GapSB          IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_MsgAliveCounter_MFS    IlTxMsgHndIHU_MFS
#define IlTxSigHndIHU_Checksum_CRC_MFS       IlTxMsgHndIHU_MFS


/* -----------------------------------------------------------------------------
    &&&~ Critical section macros for signals
 ----------------------------------------------------------------------------- */

#define IlEnterCriticalNavSpeedLimitStatus() CanGlobalInterruptDisable()
#define IlLeaveCriticalNavSpeedLimitStatus() CanGlobalInterruptRestore()
#define IlEnterCriticalNavSpeedLimit()       CanGlobalInterruptDisable()
#define IlLeaveCriticalNavSpeedLimit()       CanGlobalInterruptRestore()
#define IlEnterCriticalNavSpeedLimitUnits()  CanGlobalInterruptDisable()
#define IlLeaveCriticalNavSpeedLimitUnits()  CanGlobalInterruptRestore()
#define IlEnterCriticalNavCurrRoadType()     CanGlobalInterruptDisable()
#define IlLeaveCriticalNavCurrRoadType()     CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_FCWSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_FCWSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_FCWSnvtySet()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_FCWSnvtySet()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_AEBSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_AEBSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_SCF_request_confirmed() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_SCF_request_confirmed() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_SCFSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_SCFSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_SLA_TSI_TLI_SwtSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_SLA_TSI_TLI_SwtSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_LKASwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_LKASwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_HMASwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_HMASwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_IESSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_IESSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_DAISwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_DAISwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_LDPSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_LDPSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_WarnModSwtSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_WarnModSwtSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_LDWSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_LDWSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_ELKSwtSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_ELKSwtSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_11_LDWLDPSnvtySetLDWLDP() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_11_LDWLDPSnvtySetLDWLDP() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightBrightnessAdjust_Front() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightBrightnessAdjust_Front() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightBrightnessAdjust_Roof() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightBrightnessAdjust_Roof() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightBrightnessAdjust_Rear() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightBrightnessAdjust_Rear() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightColorSet_Front() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightColorSet_Front() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightColorSet_Roof() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightColorSet_Roof() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightColorSet_Rear() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightColorSet_Rear() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RegionColorSynchronizationReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RegionColorSynchronizationReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightModeSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightModeSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightAreaDisplayEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightAreaDisplayEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightBreatheEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightBreatheEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AmbientLightBreatheWithVehSpdEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AmbientLightBreatheWithVehSpdEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_Chg_SOC_LimitPointSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_Chg_SOC_LimitPointSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ChgModeSet()      CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ChgModeSet()      CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BookChgStartTimeSet_Hour() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BookChgStartTimeSet_Hour() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BookChgStartTimeSet_Minute() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BookChgStartTimeSet_Minute() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BookChgTimeSet_VD() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BookChgTimeSet_VD() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BookChgStopTimeSet_Hour() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BookChgStopTimeSet_Hour() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BookchgStopTimeSet_Minute() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BookchgStopTimeSet_Minute() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_120HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_120HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceAssistantActiveSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceAssistantActiveSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_250HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_250HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_500HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_500HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_1000HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_1000HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_1500HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_1500HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_2000HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_2000HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MusicLoudness_6000HZ() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MusicLoudness_6000HZ() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_Time_year()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_Time_year()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_TimeSwSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_TimeSwSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_Time_Month()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_Time_Month()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_Time_Day()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_Time_Day()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_Time_Hour()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_Time_Hour()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_Time_Minute() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_Time_Minute() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_GPS_Time_Second() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_GPS_Time_Second() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RearWarnSystemEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RearWarnSystemEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ICM_ThemelSet()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ICM_ThemelSet()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ICM_ScreenReq()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ICM_ScreenReq()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_OverSpdValueSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_OverSpdValueSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ICM_Menu_OK_ButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ICM_Menu_OK_ButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ICM_ModeButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ICM_ModeButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FatigureDrivingTimeSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FatigureDrivingTimeSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ICM_UpButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ICM_UpButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ICM_DownButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ICM_DownButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU__ICM_LeftButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU__ICM_LeftButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU__ICM_RightButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU__ICM_RightButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BrightnessAdjustSet_ICM() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BrightnessAdjustSet_ICM() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_DVR_QuickCameraSw() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_DVR_QuickCameraSw() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatHeatLevelSetReq_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatHeatLevelSetReq_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatHeatLevelSetReq_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatHeatLevelSetReq_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_WirelessChargingEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_WirelessChargingEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AVAS_VolumeSet()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AVAS_VolumeSet()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AVAS_AudioSourceSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AVAS_AudioSourceSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AVAS_EnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AVAS_EnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatMemoryEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatMemoryEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatPositionStoreReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatPositionStoreReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatPositionCalloutReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatPositionCalloutReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatHeatVentLevelSetReq_Drive() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatHeatVentLevelSetReq_Drive() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SeatHeatVentLevelSetReq_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SeatHeatVentLevelSetReq_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FaceRecSeatEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FaceRecSeatEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FaceRecMirrorInclineEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FaceRecMirrorInclineEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FaceRecAuthenticationResult() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FaceRecAuthenticationResult() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AccountDeletedReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AccountDeletedReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_EntryExitSeatCtrlEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_EntryExitSeatCtrlEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AUTO_SwSts()      CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AUTO_SwSts()      CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AC_SwSts()        CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AC_SwSts()        CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_PTC_SwSts()       CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_PTC_SwSts()       CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_OFF_SwSts()       CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_OFF_SwSts()       CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RearDfstSwSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RearDfstSwSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_CirculationModeSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_CirculationModeSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AutoDfstSwSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AutoDfstSwSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BlowModeSet()     CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BlowModeSet()     CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_TempSet_Driver()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_TempSet_Driver()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BlowerlevelSet()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BlowerlevelSet()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AnionGeneratorSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AnionGeneratorSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AQS_EnablaSw()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AQS_EnablaSw()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AutoBlowEnablaSw() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AutoBlowEnablaSw() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AC_MemoryModeEnablaSw() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AC_MemoryModeEnablaSw() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_DUAL_EnablaSw()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_DUAL_EnablaSw()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FrontDefrostSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FrontDefrostSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VentilationSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VentilationSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ECO_SwSts()       CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ECO_SwSts()       CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_TempSet_Psngr()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_TempSet_Psngr()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_LightCtrlSwSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_LightCtrlSwSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FrontWiperSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FrontWiperSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RearFogLightSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RearFogLightSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MirrorReversePositionStoreReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MirrorReversePositionStoreReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MirrorCtrlReq_L() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MirrorCtrlReq_L() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MirrorCtrlReq_R() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MirrorCtrlReq_R() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RearWiperSwSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RearWiperSwSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RearWashSwSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RearWashSwSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_InverterConfirmSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_InverterConfirmSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_InverterEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_InverterEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_CruiseTypeSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_CruiseTypeSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VehWashModeSet()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VehWashModeSet()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SpeedLimitValueSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SpeedLimitValueSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SpeedLimitEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SpeedLimitEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RegenerationLevelSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RegenerationLevelSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_DriveModeSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_DriveModeSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_OutputLimitSOCSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_OutputLimitSOCSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ShiftVoiceRemind_EnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ShiftVoiceRemind_EnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ShiftErrorActionRemindEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ShiftErrorActionRemindEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SOC_ChargeLockEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SOC_ChargeLockEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AutoLockEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AutoLockEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AntiTheftModeSetSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AntiTheftModeSetSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SteeringWheelHeatReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SteeringWheelHeatReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MirrorFoldEnableSwSts__() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MirrorFoldEnableSwSts__() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FollowHomeMeEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FollowHomeMeEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SunroofAutoCloseEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SunroofAutoCloseEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_WelcomeLightEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_WelcomeLightEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ComfortModeSet()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ComfortModeSet()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_WiperSensitivitySet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_WiperSensitivitySet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MirrorFoldReq()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MirrorFoldReq()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AHB_EnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AHB_EnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_LowBeamAngleAdjust() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_LowBeamAngleAdjust() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FrontWiperMaintenanceModeReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FrontWiperMaintenanceModeReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_EmergencyPowerOffReqConfirm() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_EmergencyPowerOffReqConfirm() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MirrorReverseInclineEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MirrorReverseInclineEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FollowMeHomeTimeSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FollowMeHomeTimeSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_DoorWindowCtrlEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_DoorWindowCtrlEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_WindowAutoCloseEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_WindowAutoCloseEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RKE_SunRoofCtrlEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RKE_SunRoofCtrlEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_DomeLightDoorCtrlSwitchSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_DomeLightDoorCtrlSwitchSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MaxPositionSet()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MaxPositionSet()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FragranceEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FragranceEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FragranceConcentrationSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FragranceConcentrationSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FragrancePositionSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FragrancePositionSet() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_SunroofReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_SunroofReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_SunshadeReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_SunshadeReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_WindowReq_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_WindowReq_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_WindowReq_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_WindowReq_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_WindowReq_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_WindowReq_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_WindowReq_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_WindowReq_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_LowbeamReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_LowbeamReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_VoiceCtrl_TrunkReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_VoiceCtrl_TrunkReq() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SmokingModeReq()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SmokingModeReq()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MassageSwithSet_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MassageSwithSet_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MassageModeSet_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MassageModeSet_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MassageStrengthSet_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MassageStrengthSet_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_LumbarSupportModeSet_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_LumbarSupportModeSet_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_ForwardBack_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_ForwardBack_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_SeatBack_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_SeatBack_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_17_MsgAliveCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_17_MsgAliveCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_LegSupport_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_LegSupport_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_ForwardBack_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_ForwardBack_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_UpDown_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_UpDown_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_SeatBack_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_SeatBack_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_16_MsgAliveCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_16_MsgAliveCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MotoPositionSet_LegSupport_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MotoPositionSet_LegSupport_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AccountCreatSts_Num1() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AccountCreatSts_Num1() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AccountCreatSts_Num2() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AccountCreatSts_Num2() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AccountCreatSts_Num3() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AccountCreatSts_Num3() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_DriverFaceRecognitionSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_DriverFaceRecognitionSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RadioFrequanceMode() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RadioFrequanceMode() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RadioResearchSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RadioResearchSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_SourceStationMode() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_SourceStationMode() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_PowerOnSts()      CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_PowerOnSts()      CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_FM_RadioFrequanceValue() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_FM_RadioFrequanceValue() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AM_RadioFrequanceValue() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AM_RadioFrequanceValue() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_RadioVolume()     CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_RadioVolume()     CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_EPB_SwSts()       CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_EPB_SwSts()       CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_BrakemodeSet()    CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_BrakemodeSet()    CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_EPS_ModeSet()     CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_EPS_ModeSet()     CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_ESC_OFF_SwSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_ESC_OFF_SwSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_CST_FunctionEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_CST_FunctionEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_HDC_FunctionEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_HDC_FunctionEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AVH_FunctionEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AVH_FunctionEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_PDC_EnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_PDC_EnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_CruiseCtrlEnableButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_CruiseCtrlEnableButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_CruiseCancelButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_CruiseCancelButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_CruiseSetButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_CruiseSetButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_CruiseResumeButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_CruiseResumeButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_SpdLimitButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_SpdLimitButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_ACC_GapSB1()  CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_ACC_GapSB1()  CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MFS_ACC_GapSB()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MFS_ACC_GapSB()   CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_MsgAliveCounter_MFS() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_MsgAliveCounter_MFS() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_Checksum_CRC_MFS() CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_Checksum_CRC_MFS() CanGlobalInterruptRestore()
#define IlEnterCriticalSAS_SteeringAngleSpd_VD() CanGlobalInterruptDisable()
#define IlLeaveCriticalSAS_SteeringAngleSpd_VD() CanGlobalInterruptRestore()
#define IlEnterCriticalSAS_SteeringAngle_VD() CanGlobalInterruptDisable()
#define IlLeaveCriticalSAS_SteeringAngle_VD() CanGlobalInterruptRestore()
#define IlEnterCriticalSAS_SteeringAngle()   CanGlobalInterruptDisable()
#define IlLeaveCriticalSAS_SteeringAngle()   CanGlobalInterruptRestore()
#define IlEnterCriticalSAS_SteeringAngleSpd() CanGlobalInterruptDisable()
#define IlLeaveCriticalSAS_SteeringAngleSpd() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_SCFONOFFSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_SCFONOFFSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_FCWONOFFSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_FCWONOFFSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_FCWSnvtySts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_FCWSnvtySts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_AEBONOFFSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_AEBONOFFSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_DAIONOFFSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_DAIONOFFSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_MsgCntr()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_MsgCntr()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_5_CRC()       CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_5_CRC()       CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_HMAONOFFSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_HMAONOFFSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_IESONOFFSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_IESONOFFSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_SLA_TSI_TLI_ONOFF_Sts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_SLA_TSI_TLI_ONOFF_Sts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_LCCONOFF_Sts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_LCCONOFF_Sts()  CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_WarnModSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_WarnModSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_LDWONOFFSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_LDWONOFFSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_LKAONOFFSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_LKAONOFFSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_ELKONOFFSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_ELKONOFFSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_LDWLDPSnvtySts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_LDWLDPSnvtySts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_5_LDPONOFFSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_5_LDPONOFFSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalWCM_WirelessChargingEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalWCM_WirelessChargingEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalWCM_WirelessChargingSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalWCM_WirelessChargingSts() CanGlobalInterruptRestore()
#define IlEnterCriticalWCM_ForeignBodyDetectedWaring() CanGlobalInterruptDisable()
#define IlLeaveCriticalWCM_ForeignBodyDetectedWaring() CanGlobalInterruptRestore()
#define IlEnterCriticalWCM_OverTempWarning() CanGlobalInterruptDisable()
#define IlLeaveCriticalWCM_OverTempWarning() CanGlobalInterruptRestore()
#define IlEnterCriticalWCM_MsgAliveCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalWCM_MsgAliveCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalAVAS_VolumeSts()      CanGlobalInterruptDisable()
#define IlLeaveCriticalAVAS_VolumeSts()      CanGlobalInterruptRestore()
#define IlEnterCriticalAVAS_SwSts()          CanGlobalInterruptDisable()
#define IlLeaveCriticalAVAS_SwSts()          CanGlobalInterruptRestore()
#define IlEnterCriticalAVAS_AudioSourceSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalAVAS_AudioSourceSts() CanGlobalInterruptRestore()
#define IlEnterCriticalTBOX_GPS_Time_year()  CanGlobalInterruptDisable()
#define IlLeaveCriticalTBOX_GPS_Time_year()  CanGlobalInterruptRestore()
#define IlEnterCriticalTBOX_GPS_Time_Month() CanGlobalInterruptDisable()
#define IlLeaveCriticalTBOX_GPS_Time_Month() CanGlobalInterruptRestore()
#define IlEnterCriticalTBOX_GPS_Time_Day()   CanGlobalInterruptDisable()
#define IlLeaveCriticalTBOX_GPS_Time_Day()   CanGlobalInterruptRestore()
#define IlEnterCriticalTBOX_GPS_Time_Hour()  CanGlobalInterruptDisable()
#define IlLeaveCriticalTBOX_GPS_Time_Hour()  CanGlobalInterruptRestore()
#define IlEnterCriticalTBOX_GPS_Time_Minute() CanGlobalInterruptDisable()
#define IlLeaveCriticalTBOX_GPS_Time_Minute() CanGlobalInterruptRestore()
#define IlEnterCriticalTBOX_GPS_Time_Second() CanGlobalInterruptDisable()
#define IlLeaveCriticalTBOX_GPS_Time_Second() CanGlobalInterruptRestore()
#define IlEnterCriticalOBC_SOC_ChargeLockEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalOBC_SOC_ChargeLockEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightBrightnesssSts_Front() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightBrightnesssSts_Front() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightBrightnessSts_Roof() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightBrightnessSts_Roof() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightBrightnessSts_Rear() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightBrightnessSts_Rear() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightColorSta_Front() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightColorSta_Front() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightColorSts_Roof() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightColorSts_Roof() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightColorStst_Rear() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightColorStst_Rear() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MsgAliveCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MsgAliveCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightModeSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightModeSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightAreaDisplayEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightAreaDisplayEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightBreatheEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightBreatheEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AmbientLightBreatheWithVehSpdEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AmbientLightBreatheWithVehSpdEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MassageSwithSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MassageSwithSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MassageModeSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MassageModeSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MassageStrengthSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MassageStrengthSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_LumbarSupportModeSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_LumbarSupportModeSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_SeatHeatVentSetSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_SeatHeatVentSetSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_DriverSeatLocalCtrlSwithSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_DriverSeatLocalCtrlSwithSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_ForwardBack_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_ForwardBack_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_SeatBack_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_SeatBack_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MsgAliveCounter_2() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MsgAliveCounter_2() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_LegSupport_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_LegSupport_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_SeatHeatVentSetSts_Drive() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_SeatHeatVentSetSts_Drive() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_LocalCtrlSwithSts_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_LocalCtrlSwithSts_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_ForwardBack_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_ForwardBack_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_UpDown_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_UpDown_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_SeatBack_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_SeatBack_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MsgAliveCounter_1() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MsgAliveCounter_1() CanGlobalInterruptRestore()
#define IlEnterCriticalSCM_MotoPosition_LegSupport_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalSCM_MotoPosition_LegSupport_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragranceEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragranceEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragranceSystemFaultSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragranceSystemFaultSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragranceConcentration() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragranceConcentration() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragranceType_Position_1() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragranceType_Position_1() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragranceType_Position_2() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragranceType_Position_2() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragranceType_Position_3() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragranceType_Position_3() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FM_FragrancePositionSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FM_FragrancePositionSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFM_FragranceReplaceRemind_1() CanGlobalInterruptDisable()
#define IlLeaveCriticalFM_FragranceReplaceRemind_1() CanGlobalInterruptRestore()
#define IlEnterCriticalFM_FragranceReplaceRemind_2() CanGlobalInterruptDisable()
#define IlLeaveCriticalFM_FragranceReplaceRemind_2() CanGlobalInterruptRestore()
#define IlEnterCriticalFM_FragranceReplaceRemind_3() CanGlobalInterruptDisable()
#define IlLeaveCriticalFM_FragranceReplaceRemind_3() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FaceRecSeatEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FaceRecSeatEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_EntryExitSeatCtrlEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_EntryExitSeatCtrlEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SeatPositionStoreResult() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SeatPositionStoreResult() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DriverFaceRecognitionResult() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DriverFaceRecognitionResult() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SeatPositionCalloutInhibitReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SeatPositionCalloutInhibitReq() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SeatPositionCalloutSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SeatPositionCalloutSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DriverFaceRecognitionSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DriverFaceRecognitionSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorFoldEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorFoldEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorReverseInclineEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorReverseInclineEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FaceRecMirrorInclineEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FaceRecMirrorInclineEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorPositionMemoryHMIReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorPositionMemoryHMIReq() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorCtrlSts_L() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorCtrlSts_L() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorCtrlSts_R() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorCtrlSts_R() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorReversePositionStoreResult() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorReversePositionStoreResult() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressure_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressure_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressure_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressure_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressure_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressure_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressure_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressure_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTemp_FL()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTemp_FL()     CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTemp_FR()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTemp_FR()     CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTemp_RL()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTemp_RL()     CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTemp_RR()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTemp_RR()     CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TSISgnGiWay()  CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TSISgnGiWay()  CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TLISysSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TLISysSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TSISysSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TSISysSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TSISgnLongStayDisp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TSISgnLongStayDisp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TSISgnForb()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TSISgnForb()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_LDWSysSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_LDWSysSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_LDPSysSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_LDPSysSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TLITrfcLi()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TLITrfcLi()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_LCCEnaRcmend() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_LCCEnaRcmend() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_LCCText()      CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_LCCText()      CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_TSINoParkWarn() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_TSINoParkWarn() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_ELKSysSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_ELKSysSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_WarnModSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_WarnModSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_SLAONOFFSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_SLAONOFFSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_LCCExitTextInfoSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_LCCExitTextInfoSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_LDPTJAELKTakeoverReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_LDPTJAELKTakeoverReq() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_EgoLeftLineHeatgAg() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_EgoLeftLineHeatgAg() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_10_ELKIntvMod()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_10_ELKIntvMod()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_ACCObjLgtDstX() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_ACCObjLgtDstX() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_ACCObjTyp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_ACCObjTyp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_LeObjID()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_LeObjID()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_ACCObjHozDstY() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_ACCObjHozDstY() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_FrntFarObjLgtDstX() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_FrntFarObjLgtDstX() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_FrntFarObjTyp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_FrntFarObjTyp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_FrntFarObjID() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_FrntFarObjID() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_FrntFarObjHozDstY() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_FrntFarObjHozDstY() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_MessageCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_MessageCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_9_CRC()       CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_9_CRC()       CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_ACCObjLgtDstX() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_ACCObjLgtDstX() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_ACCObjTyp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_ACCObjTyp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_ACCObjID()  CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_ACCObjID()  CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_ACCObjHozDstY() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_ACCObjHozDstY() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_FrntFarObjLgtDstX() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_FrntFarObjLgtDstX() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_FrntFarObjTyp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_FrntFarObjTyp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_FrntFarObjID() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_FrntFarObjID() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_FrntFarObjHozDstY() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_FrntFarObjHozDstY() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_MessageCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_MessageCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_8_CRC()       CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_8_CRC()       CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborLeLineHozlDst() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborLeLineHozlDst() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborLeLineTyp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborLeLineTyp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborLeLineColor() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborLeLineColor() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborLeLineID() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborLeLineID() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborLeLineCrvt() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborLeLineCrvt() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborRiLineHozlDst() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborRiLineHozlDst() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborRiLineTyp() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborRiLineTyp() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborRiLineColor() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborRiLineColor() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborRiLineID() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborRiLineID() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_7_NeborRiLineCrvt() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_7_NeborRiLineCrvt() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoLeLineHozlDst() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoLeLineHozlDst() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoLeLineTyp()  CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoLeLineTyp()  CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoLeLineColor() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoLeLineColor() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoLeLineID()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoLeLineID()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoLeLineCrvt() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoLeLineCrvt() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoRiLineHozlDst() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoRiLineHozlDst() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoRiLineTyp()  CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoRiLineTyp()  CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoRiLineColor() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoRiLineColor() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoRiLineID()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoRiLineID()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_6_EgoRiLineCrvt() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_6_EgoRiLineCrvt() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_AQS_EnableSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_AQS_EnableSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_PM2_5_InternalValue() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_PM2_5_InternalValue() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_PM2_5_ExternalValue() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_PM2_5_ExternalValue() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_PM2_5_InternalLevel() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_PM2_5_InternalLevel() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_PM2_5_ExternalLevel() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_PM2_5_ExternalLevel() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_SmokingModeSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_SmokingModeSts() CanGlobalInterruptRestore()
#define IlEnterCriticalAQS_AQS_AirQualityLevel() CanGlobalInterruptDisable()
#define IlLeaveCriticalAQS_AQS_AirQualityLevel() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_AUTO_Sts()       CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_AUTO_Sts()       CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_AC_Sts()         CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_AC_Sts()         CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_PTC_Sts()        CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_PTC_Sts()        CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_System_ON_OFF_Sts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_System_ON_OFF_Sts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_CirculationModeSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_CirculationModeSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_BlowModeSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_BlowModeSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_TempSetSts_Driver() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_TempSetSts_Driver() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_BlowerlevelSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_BlowerlevelSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_DUAL_EnablaSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_DUAL_EnablaSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_AutoDefrostSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_AutoDefrostSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_VentilationSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_VentilationSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_InternalTemp()   CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_InternalTemp()   CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_ExternalTemp()   CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_ExternalTemp()   CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_FrontDefrostSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_FrontDefrostSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_AnionGeneratorSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_AnionGeneratorSts() CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_ECO_Mode()       CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_ECO_Mode()       CanGlobalInterruptRestore()
#define IlEnterCriticalACCM_TempSetSts_Psngr() CanGlobalInterruptDisable()
#define IlLeaveCriticalACCM_TempSetSts_Psngr() CanGlobalInterruptRestore()
#define IlEnterCriticalICM_ThemelSetSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalICM_ThemelSetSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalICM_BrightnessAdjustSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalICM_BrightnessAdjustSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalICM_FatigureDrivingTimeSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalICM_FatigureDrivingTimeSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalICM_OverSpdValueSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalICM_OverSpdValueSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_TextinfoWarn() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_TextinfoWarn() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_Textinfo_Info() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_Textinfo_Info() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_SCF_PopoverReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_SCF_PopoverReq() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_TimeGapSetICM() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_TimeGapSetICM() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_FCW_preWarning() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_FCW_preWarning() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_DistanceWarning() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_DistanceWarning() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_DrvrCfmSCFDispFb() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_DrvrCfmSCFDispFb() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_SCF_SpdLimUnit() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_SCF_SpdLimUnit() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_SCF_SpdLimSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_SCF_SpdLimSts() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_AEBMode()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_AEBMode()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_FCWMode()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_FCWMode()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_TakeOverReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_TakeOverReq() CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_ACCMode()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_ACCMode()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_VSetDis()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_VSetDis()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_MsgCntr()   CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_MsgCntr()   CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_FRM_6_CRC()       CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_FRM_6_CRC()       CanGlobalInterruptRestore()
#define IlEnterCriticalBMS_ChgWireConnectStsDisp() CanGlobalInterruptDisable()
#define IlLeaveCriticalBMS_ChgWireConnectStsDisp() CanGlobalInterruptRestore()
#define IlEnterCriticalBMS_SOC_Disp()        CanGlobalInterruptDisable()
#define IlLeaveCriticalBMS_SOC_Disp()        CanGlobalInterruptRestore()
#define IlEnterCriticalBMS_ChgSts()          CanGlobalInterruptDisable()
#define IlLeaveCriticalBMS_ChgSts()          CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_Chg_SOC_LimitSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_Chg_SOC_LimitSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookChgModeSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookChgModeSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookChgStartTimeSts_Hour() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookChgStartTimeSts_Hour() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookChgStartTimeSts_Minute() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookChgStartTimeSts_Minute() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookChgSts1()     CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookChgSts1()     CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookchgStopTimeSts_Hour() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookchgStopTimeSts_Hour() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookChgStopTimeSts_Minute() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookChgStopTimeSts_Minute() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BookChgSts2()     CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BookChgSts2()     CanGlobalInterruptRestore()
#define IlEnterCriticalPLG_LockSts()         CanGlobalInterruptDisable()
#define IlLeaveCriticalPLG_LockSts()         CanGlobalInterruptRestore()
#define IlEnterCriticalPLG_MaxPositionSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalPLG_MaxPositionSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_RL()  CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_RL()  CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_RMR() CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_RMR() CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_RML() CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_RML() CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_RR()  CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_RR()  CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_FL()  CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_FL()  CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_AudibleBeepRate() CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_AudibleBeepRate() CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_FML() CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_FML() CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_FMR() CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_FMR() CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_SnsDistance_FR()  CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_SnsDistance_FR()  CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_WorkingSts()      CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_WorkingSts()      CanGlobalInterruptRestore()
#define IlEnterCriticalRCM_DetectingSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalRCM_DetectingSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureWarnLampDisp() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureWarnLampDisp() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureSystemFault() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureSystemFault() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureWarn_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureWarn_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTempWarn_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTempWarn_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureDelta_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureDelta_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorFault_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorFault_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorLowBatt_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorLowBatt_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorCalibrationSts_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorCalibrationSts_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureWarn_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureWarn_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTempWarn_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTempWarn_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureDelta_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureDelta_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorFault_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorFault_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorLowBatt_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorLowBatt_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorCalibrationSts_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorCalibrationSts_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureWarn_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureWarn_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTempWarn_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTempWarn_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureDelta_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureDelta_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorFault_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorFault_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorLowBatt_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorLowBatt_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorCalibrationSts_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorCalibrationSts_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureWarn_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureWarn_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireTempWarn_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireTempWarn_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TirePressureDelta_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TirePressureDelta_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorFault_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorFault_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorLowBatt_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorLowBatt_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TireSensorCalibrationSts_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TireSensorCalibrationSts_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WinPostionSts_FL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WinPostionSts_FL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WinPostionSts_FR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WinPostionSts_FR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WinPostionSts_RL() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WinPostionSts_RL() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WinPostionSts_RR() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WinPostionSts_RR() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SunroofPositionSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SunroofPositionSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SunroofSts()      CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SunroofSts()      CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SunshadeReqSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SunshadeReqSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AutoLockEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AutoLockEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AntiTheftModeSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AntiTheftModeSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FollowMeHomeSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FollowMeHomeSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SunroofAutoCloseEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SunroofAutoCloseEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FollowMeTimeSetSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FollowMeTimeSetSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WiperSensitivitySts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WiperSensitivitySts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_RearDfstSts()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_RearDfstSts()     CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_LowBeamAngleAdjustSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_LowBeamAngleAdjustSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FrontWiperMaintenanceModeSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FrontWiperMaintenanceModeSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_MirrorUnfoldRemind() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_MirrorUnfoldRemind() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DoorWindowCtrlEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DoorWindowCtrlEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SunroofCtrlEnableSts_RF() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SunroofCtrlEnableSts_RF() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SunroofCtrlEnableSts_Remote() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SunroofCtrlEnableSts_Remote() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WelcomelightEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WelcomelightEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WindowAutoCloseEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WindowAutoCloseEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_RKE_SunRoofCtrlEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_RKE_SunRoofCtrlEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalIHU_AHB_EnableSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalIHU_AHB_EnableSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_WelcomeAnimationReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_WelcomeAnimationReq() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_SteeringWheelHeatSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_SteeringWheelHeatSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AutoBlowEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AutoBlowEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DomeLightDoorCtrlSwitchSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DomeLightDoorCtrlSwitchSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_DriveMode()       CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_DriveMode()       CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_LongRangeRemainingTime() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_LongRangeRemainingTime() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_ResidualOdometer() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_ResidualOdometer() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InstPowerConsum() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InstPowerConsum() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_TotalPowerDisp()  CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_TotalPowerDisp()  CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InstPowerDisp()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InstPowerDisp()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_ManualToGearP_InhibitSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_ManualToGearP_InhibitSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_ShiftErrorActionRemindEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_ShiftErrorActionRemindEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterEnableSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterEnableSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterPower()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterPower()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterInd_1()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterInd_1()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterInd_2()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterInd_2()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterInd_3()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterInd_3()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterInd_4()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterInd_4()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterFaultSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterFaultSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_220V_OutputTimeInd() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_220V_OutputTimeInd() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_PowerOffAutoParkInhibitSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_PowerOffAutoParkInhibitSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_DoorOpenParkInhibitSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_DoorOpenParkInhibitSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_InverterEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_InverterEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_OutputLimitSOC()  CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_OutputLimitSOC()  CanGlobalInterruptRestore()
#define IlEnterCriticalHMA_Status()          CanGlobalInterruptDisable()
#define IlLeaveCriticalHMA_Status()          CanGlobalInterruptRestore()
#define IlEnterCriticalLDW_ELK_LKA_LDPLeftVisuali() CanGlobalInterruptDisable()
#define IlLeaveCriticalLDW_ELK_LKA_LDPLeftVisuali() CanGlobalInterruptRestore()
#define IlEnterCriticalLKA_State()           CanGlobalInterruptDisable()
#define IlLeaveCriticalLKA_State()           CanGlobalInterruptRestore()
#define IlEnterCriticalLDW_ELK_LKA_LDPRightVisuali() CanGlobalInterruptDisable()
#define IlLeaveCriticalLDW_ELK_LKA_LDPRightVisuali() CanGlobalInterruptRestore()
#define IlEnterCriticalSLASpdlimit()         CanGlobalInterruptDisable()
#define IlLeaveCriticalSLASpdlimit()         CanGlobalInterruptRestore()
#define IlEnterCriticalSLAState()            CanGlobalInterruptDisable()
#define IlLeaveCriticalSLAState()            CanGlobalInterruptRestore()
#define IlEnterCriticalSLASpdlimitWarning()  CanGlobalInterruptDisable()
#define IlLeaveCriticalSLASpdlimitWarning()  CanGlobalInterruptRestore()
#define IlEnterCriticalCamera_textinfo()     CanGlobalInterruptDisable()
#define IlLeaveCriticalCamera_textinfo()     CanGlobalInterruptRestore()
#define IlEnterCriticalFCM_2_SLASpdlimitUnits() CanGlobalInterruptDisable()
#define IlLeaveCriticalFCM_2_SLASpdlimitUnits() CanGlobalInterruptRestore()
#define IlEnterCriticalLCC_mode()            CanGlobalInterruptDisable()
#define IlLeaveCriticalLCC_mode()            CanGlobalInterruptRestore()
#define IlEnterCriticalBSD_RearWarnSystemEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBSD_RearWarnSystemEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_LeftWheelButtonSts_ScrollUp() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_LeftWheelButtonSts_ScrollUp() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_LeftWheelButtonSts_ScrollDown() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_LeftWheelButtonSts_ScrollDown() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_LeftWheelButtonSts_ToggleLeft() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_LeftWheelButtonSts_ToggleLeft() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_LeftWheelButtonSts_ToggleRight() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_LeftWheelButtonSts_ToggleRight() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_LeftWheelButtonSts_Press() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_LeftWheelButtonSts_Press() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_WeChatButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_WeChatButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_CustomButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_CustomButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_RightWheelButtonSts_ScrollUp() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_RightWheelButtonSts_ScrollUp() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_RightWheelButtonSts_ScrollDown() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_RightWheelButtonSts_ScrollDown() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_RightWheelButtonSts_ToggleLeft() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_RightWheelButtonSts_ToggleLeft() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_RightWheelButtonSts_ToggleRight() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_RightWheelButtonSts_ToggleRight() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_RightWheelButtonSts_Press() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_RightWheelButtonSts_Press() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_ReturnButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_ReturnButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_VoiceRecognitionButtonSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_VoiceRecognitionButtonSts() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_MsgAliveCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_MsgAliveCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalMFS_Checksum_CRC()    CanGlobalInterruptDisable()
#define IlLeaveCriticalMFS_Checksum_CRC()    CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_EmergencyPowerOffRemindSts_1() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_EmergencyPowerOffRemindSts_1() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_EmergencyPowerOffRemindSts_2() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_EmergencyPowerOffRemindSts_2() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_CruiseCtrlStsDisp() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_CruiseCtrlStsDisp() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_RegenerationLevelSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_RegenerationLevelSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_SpdLimitSts()     CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_SpdLimitSts()     CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_SpeedLimitEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_SpeedLimitEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_SpeedLimitValueSet() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_SpeedLimitValueSet() CanGlobalInterruptRestore()
#define IlEnterCriticalICM_TotalOdometer()   CanGlobalInterruptDisable()
#define IlLeaveCriticalICM_TotalOdometer()   CanGlobalInterruptRestore()
#define IlEnterCriticalICM_LowSOC_LampSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalICM_LowSOC_LampSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalBMS_HV_BattVolt()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBMS_HV_BattVolt()     CanGlobalInterruptRestore()
#define IlEnterCriticalBMS_HV_BattCurr()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBMS_HV_BattCurr()     CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_KeySts()          CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_KeySts()          CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_RearFogLightSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_RearFogLightSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DoorAjarSts_FL()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DoorAjarSts_FL()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DoorAjarSts_FR()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DoorAjarSts_FR()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DoorAjarSts_RR()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DoorAjarSts_RR()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_DoorAjarSts_RL()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_DoorAjarSts_RL()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_HoodAjarSts()     CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_HoodAjarSts()     CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_TrunkAjarSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_TrunkAjarSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_RearWashSwSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_RearWashSwSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_FrontWiperSwSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_FrontWiperSwSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_RearWiperSwSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_RearWiperSwSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_LightCtrlSwSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_LightCtrlSwSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCM_AntiTheftMode()   CanGlobalInterruptDisable()
#define IlLeaveCriticalBCM_AntiTheftMode()   CanGlobalInterruptRestore()
#define IlEnterCriticalEPS_WorkingModeSts()  CanGlobalInterruptDisable()
#define IlLeaveCriticalEPS_WorkingModeSts()  CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_EPB_HMI_RemindReq() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_EPB_HMI_RemindReq() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_EPB_ActuatorSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_EPB_ActuatorSts() CanGlobalInterruptRestore()
#define IlEnterCriticalEPB_AutoApplyDisableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalEPB_AutoApplyDisableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_EPB_MsgAliveCounter() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_EPB_MsgAliveCounter() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_EPB_Checksum_CRC() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_EPB_Checksum_CRC() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_HDC_FunctionEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_HDC_FunctionEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_AVH_FunctionEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_AVH_FunctionEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_CST_FunctionEnableSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_CST_FunctionEnableSts() CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_CST_Active()      CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_CST_Active()      CanGlobalInterruptRestore()
#define IlEnterCriticalBCS_BrakemodeSts()    CanGlobalInterruptDisable()
#define IlLeaveCriticalBCS_BrakemodeSts()    CanGlobalInterruptRestore()
#define IlEnterCriticalEGS_FaultLevel()      CanGlobalInterruptDisable()
#define IlLeaveCriticalEGS_FaultLevel()      CanGlobalInterruptRestore()
#define IlEnterCriticalEGS_HighBeamSwitchActiveSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalEGS_HighBeamSwitchActiveSts() CanGlobalInterruptRestore()
#define IlEnterCriticalEGS_HighBeamFlashSwitchActiveSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalEGS_HighBeamFlashSwitchActiveSts() CanGlobalInterruptRestore()
#define IlEnterCriticalEGS_FrontWiperMistSwitchActiveSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalEGS_FrontWiperMistSwitchActiveSts() CanGlobalInterruptRestore()
#define IlEnterCriticalEGS_FrontWashSwitchActiveSts() CanGlobalInterruptDisable()
#define IlLeaveCriticalEGS_FrontWashSwitchActiveSts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_ActGear()         CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_ActGear()         CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_VehSpd()          CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_VehSpd()          CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BrakePedalSts()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BrakePedalSts()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_PowertrainReadySts() CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_PowertrainReadySts() CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_BrakePedal_VD()   CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_BrakePedal_VD()   CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_ActGear_VD()      CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_ActGear_VD()      CanGlobalInterruptRestore()
#define IlEnterCriticalVCU_VehSpd_VD()       CanGlobalInterruptDisable()
#define IlLeaveCriticalVCU_VehSpd_VD()       CanGlobalInterruptRestore()


/* -----------------------------------------------------------------------------
    &&&~ Declaration Confirmation Functions
 ----------------------------------------------------------------------------- */

/* Application signal confirmation callback functions */


/* -----------------------------------------------------------------------------
    &&&~ Declaration Indication Functions
 ----------------------------------------------------------------------------- */

/* Application signal indication callback functions */


/* -----------------------------------------------------------------------------
    &&&~ Declaration User Timeout Functions
 ----------------------------------------------------------------------------- */

#if defined(IL_ENABLE_TX) && defined(IL_ENABLE_TX_TIMEOUT)
#endif



/* -----------------------------------------------------------------------------
    &&&~ Get Rx Signal Access for signals smaller or equal 8bit
 ----------------------------------------------------------------------------- */

/* Handle:    0,Name:        SAS_SteeringAngleSpd_VD,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSAS_SteeringAngleSpd_VD()     (CGW_SAS_2.CGW_SAS_2.SAS_SteeringAngleSpd_VD)
#endif

/* Handle:    1,Name:           SAS_SteeringAngle_VD,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSAS_SteeringAngle_VD()        (CGW_SAS_2.CGW_SAS_2.SAS_SteeringAngle_VD)
#endif

/* Handle:    3,Name:           SAS_SteeringAngleSpd,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSAS_SteeringAngleSpd()        (CGW_SAS_2.CGW_SAS_2.SAS_SteeringAngleSpd)
#endif

/* Handle:    4,Name:          FCM_FRM_5_SCFONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_SCFONOFFSts()       (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_SCFONOFFSts)
#endif

/* Handle:    5,Name:          FCM_FRM_5_FCWONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_FCWONOFFSts()       (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_FCWONOFFSts)
#endif

/* Handle:    6,Name:          FCM_FRM_5_FCWSnvtySts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_FCWSnvtySts()       (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_FCWSnvtySts)
#endif

/* Handle:    7,Name:          FCM_FRM_5_AEBONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_AEBONOFFSts()       (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_AEBONOFFSts)
#endif

/* Handle:    8,Name:          FCM_FRM_5_DAIONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_DAIONOFFSts()       (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_DAIONOFFSts)
#endif

/* Handle:    9,Name:              FCM_FRM_5_MsgCntr,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_MsgCntr()           (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_MsgCntr)
#endif

/* Handle:   10,Name:                  FCM_FRM_5_CRC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_5_CRC()               (CGW_FCM_FRM_5.CGW_FCM_FRM_5.FCM_FRM_5_CRC)
#endif

/* Handle:   11,Name:              FCM_5_HMAONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_HMAONOFFSts()           (CGW_FCM_5.CGW_FCM_5.FCM_5_HMAONOFFSts)
#endif

/* Handle:   12,Name:              FCM_5_IESONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_IESONOFFSts()           (CGW_FCM_5.CGW_FCM_5.FCM_5_IESONOFFSts)
#endif

/* Handle:   13,Name:    FCM_5_SLA_TSI_TLI_ONOFF_Sts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_SLA_TSI_TLI_ONOFF_Sts() (CGW_FCM_5.CGW_FCM_5.FCM_5_SLA_TSI_TLI_ONOFF_Sts)
#endif

/* Handle:   14,Name:             FCM_5_LCCONOFF_Sts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_LCCONOFF_Sts()          (CGW_FCM_5.CGW_FCM_5.FCM_5_LCCONOFF_Sts)
#endif

/* Handle:   15,Name:               FCM_5_WarnModSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_WarnModSts()            (CGW_FCM_5.CGW_FCM_5.FCM_5_WarnModSts)
#endif

/* Handle:   16,Name:              FCM_5_LDWONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_LDWONOFFSts()           (CGW_FCM_5.CGW_FCM_5.FCM_5_LDWONOFFSts)
#endif

/* Handle:   17,Name:              FCM_5_LKAONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_LKAONOFFSts()           (CGW_FCM_5.CGW_FCM_5.FCM_5_LKAONOFFSts)
#endif

/* Handle:   18,Name:              FCM_5_ELKONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_ELKONOFFSts()           (CGW_FCM_5.CGW_FCM_5.FCM_5_ELKONOFFSts)
#endif

/* Handle:   19,Name:           FCM_5_LDWLDPSnvtySts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_LDWLDPSnvtySts()        (CGW_FCM_5.CGW_FCM_5.FCM_5_LDWLDPSnvtySts)
#endif

/* Handle:   20,Name:              FCM_5_LDPONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_5_LDPONOFFSts()           (CGW_FCM_5.CGW_FCM_5.FCM_5_LDPONOFFSts)
#endif

/* Handle:   21,Name:  WCM_WirelessChargingEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxWCM_WirelessChargingEnableSts() (WCM_1.WCM_1.WCM_WirelessChargingEnableSts)
#endif

/* Handle:   22,Name:        WCM_WirelessChargingSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxWCM_WirelessChargingSts()     (WCM_1.WCM_1.WCM_WirelessChargingSts)
#endif

/* Handle:   23,Name:  WCM_ForeignBodyDetectedWaring,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxWCM_ForeignBodyDetectedWaring() (WCM_1.WCM_1.WCM_ForeignBodyDetectedWaring)
#endif

/* Handle:   24,Name:            WCM_OverTempWarning,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxWCM_OverTempWarning()         (WCM_1.WCM_1.WCM_OverTempWarning)
#endif

/* Handle:   25,Name:            WCM_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxWCM_MsgAliveCounter()         (WCM_1.WCM_1.WCM_MsgAliveCounter)
#endif

/* Handle:   26,Name:                 AVAS_VolumeSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxAVAS_VolumeSts()              (AVAS_1.AVAS_1.AVAS_VolumeSts)
#endif

/* Handle:   27,Name:                     AVAS_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxAVAS_SwSts()                  (AVAS_1.AVAS_1.AVAS_SwSts)
#endif

/* Handle:   28,Name:            AVAS_AudioSourceSts,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxAVAS_AudioSourceSts()         (AVAS_1.AVAS_1.AVAS_AudioSourceSts)
#endif

/* Handle:   29,Name:             TBOX_GPS_Time_year,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxTBOX_GPS_Time_year()          (TBOX_7.TBOX_7.TBOX_GPS_Time_year)
#endif

/* Handle:   30,Name:            TBOX_GPS_Time_Month,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxTBOX_GPS_Time_Month()         (TBOX_7.TBOX_7.TBOX_GPS_Time_Month)
#endif

/* Handle:   31,Name:              TBOX_GPS_Time_Day,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxTBOX_GPS_Time_Day()           (TBOX_7.TBOX_7.TBOX_GPS_Time_Day)
#endif

/* Handle:   32,Name:             TBOX_GPS_Time_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxTBOX_GPS_Time_Hour()          (TBOX_7.TBOX_7.TBOX_GPS_Time_Hour)
#endif

/* Handle:   33,Name:           TBOX_GPS_Time_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxTBOX_GPS_Time_Minute()        (TBOX_7.TBOX_7.TBOX_GPS_Time_Minute)
#endif

/* Handle:   34,Name:           TBOX_GPS_Time_Second,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxTBOX_GPS_Time_Second()        (TBOX_7.TBOX_7.TBOX_GPS_Time_Second)
#endif

/* Handle:   35,Name:  OBC_SOC_ChargeLockEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxOBC_SOC_ChargeLockEnableSwSts() (CGW_OBC_1.CGW_OBC_1.OBC_SOC_ChargeLockEnableSwSts)
#endif

/* Handle:   36,Name: BCM_AmbientLightBrightnesssSts_Front,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightBrightnesssSts_Front() (BCM_ALM.BCM_ALM.BCM_AmbientLightBrightnesssSts_Front)
#endif

/* Handle:   37,Name: BCM_AmbientLightBrightnessSts_Roof,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightBrightnessSts_Roof() (BCM_ALM.BCM_ALM.BCM_AmbientLightBrightnessSts_Roof)
#endif

/* Handle:   38,Name: BCM_AmbientLightBrightnessSts_Rear,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightBrightnessSts_Rear() (BCM_ALM.BCM_ALM.BCM_AmbientLightBrightnessSts_Rear)
#endif

/* Handle:   39,Name: BCM_AmbientLightColorSta_Front,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightColorSta_Front() (BCM_ALM.BCM_ALM.BCM_AmbientLightColorSta_Front)
#endif

/* Handle:   40,Name:  BCM_AmbientLightColorSts_Roof,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightColorSts_Roof() (BCM_ALM.BCM_ALM.BCM_AmbientLightColorSts_Roof)
#endif

/* Handle:   41,Name: BCM_AmbientLightColorStst_Rear,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightColorStst_Rear() (BCM_ALM.BCM_ALM.BCM_AmbientLightColorStst_Rear)
#endif

/* Handle:   42,Name:            BCM_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MsgAliveCounter()         (BCM_ALM.BCM_ALM.BCM_MsgAliveCounter)
#endif

/* Handle:   43,Name:        BCM_AmbientLightModeSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightModeSts()     (BCM_ALM.BCM_ALM.BCM_AmbientLightModeSts)
#endif

/* Handle:   44,Name:      BCM_AmbientLightEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightEnableSts()   (BCM_ALM.BCM_ALM.BCM_AmbientLightEnableSts)
#endif

/* Handle:   45,Name: BCM_AmbientLightAreaDisplayEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightAreaDisplayEnableSts() (BCM_ALM.BCM_ALM.BCM_AmbientLightAreaDisplayEnableSts)
#endif

/* Handle:   46,Name: BCM_AmbientLightBreatheEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightBreatheEnableSts() (BCM_ALM.BCM_ALM.BCM_AmbientLightBreatheEnableSts)
#endif

/* Handle:   47,Name: BCM_AmbientLightBreatheWithVehSpdEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AmbientLightBreatheWithVehSpdEnableSts() (BCM_ALM.BCM_ALM.BCM_AmbientLightBreatheWithVehSpdEnableSts)
#endif

/* Handle:   48,Name:      SCM_MassageSwithSts_Psngr,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MassageSwithSts_Psngr()   (CGW_BCM_SCM_3.CGW_BCM_SCM_3.SCM_MassageSwithSts_Psngr)
#endif

/* Handle:   49,Name:       SCM_MassageModeSts_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MassageModeSts_Psngr()    (CGW_BCM_SCM_3.CGW_BCM_SCM_3.SCM_MassageModeSts_Psngr)
#endif

/* Handle:   50,Name:   SCM_MassageStrengthSts_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MassageStrengthSts_Psngr() (CGW_BCM_SCM_3.CGW_BCM_SCM_3.SCM_MassageStrengthSts_Psngr)
#endif

/* Handle:   51,Name: SCM_LumbarSupportModeSts_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_LumbarSupportModeSts_Psngr() (CGW_BCM_SCM_3.CGW_BCM_SCM_3.SCM_LumbarSupportModeSts_Psngr)
#endif

/* Handle:   52,Name:   SCM_SeatHeatVentSetSts_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_SeatHeatVentSetSts_Psngr() (CGW_BCM_SCM_2.CGW_BCM_SCM_2.SCM_SeatHeatVentSetSts_Psngr)
#endif

/* Handle:   53,Name: SCM_DriverSeatLocalCtrlSwithSts_Psngr,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_DriverSeatLocalCtrlSwithSts_Psngr() (CGW_BCM_SCM_2.CGW_BCM_SCM_2.SCM_DriverSeatLocalCtrlSwithSts_Psngr)
#endif

/* Handle:   54,Name: SCM_MotoPosition_ForwardBack_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_ForwardBack_Psngr() (CGW_BCM_SCM_2.CGW_BCM_SCM_2.SCM_MotoPosition_ForwardBack_Psngr)
#endif

/* Handle:   55,Name: SCM_MotoPosition_SeatBack_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_SeatBack_Psngr() (CGW_BCM_SCM_2.CGW_BCM_SCM_2.SCM_MotoPosition_SeatBack_Psngr)
#endif

/* Handle:   56,Name:          SCM_MsgAliveCounter_2,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MsgAliveCounter_2()       (CGW_BCM_SCM_2.CGW_BCM_SCM_2.SCM_MsgAliveCounter_2)
#endif

/* Handle:   57,Name: SCM_MotoPosition_LegSupport_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_LegSupport_Psngr() (CGW_BCM_SCM_2.CGW_BCM_SCM_2.SCM_MotoPosition_LegSupport_Psngr)
#endif

/* Handle:   58,Name:   SCM_SeatHeatVentSetSts_Drive,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_SeatHeatVentSetSts_Drive() (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_SeatHeatVentSetSts_Drive)
#endif

/* Handle:   59,Name:   SCM_LocalCtrlSwithSts_Driver,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_LocalCtrlSwithSts_Driver() (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_LocalCtrlSwithSts_Driver)
#endif

/* Handle:   60,Name: SCM_MotoPosition_ForwardBack_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_ForwardBack_Driver() (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_MotoPosition_ForwardBack_Driver)
#endif

/* Handle:   61,Name: SCM_MotoPosition_UpDown_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_UpDown_Driver() (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_MotoPosition_UpDown_Driver)
#endif

/* Handle:   62,Name: SCM_MotoPosition_SeatBack_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_SeatBack_Driver() (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_MotoPosition_SeatBack_Driver)
#endif

/* Handle:   63,Name:          SCM_MsgAliveCounter_1,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MsgAliveCounter_1()       (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_MsgAliveCounter_1)
#endif

/* Handle:   64,Name: SCM_MotoPosition_LegSupport_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSCM_MotoPosition_LegSupport_Driver() (CGW_BCM_SCM_1.CGW_BCM_SCM_1.SCM_MotoPosition_LegSupport_Driver)
#endif

/* Handle:   65,Name:      BCM_FM_FragranceEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragranceEnableSts()   (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragranceEnableSts)
#endif

/* Handle:   66,Name: BCM_FM_FragranceSystemFaultSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragranceSystemFaultSts() (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragranceSystemFaultSts)
#endif

/* Handle:   67,Name:  BCM_FM_FragranceConcentration,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragranceConcentration() (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragranceConcentration)
#endif

/* Handle:   68,Name: BCM_FM_FragranceType_Position_1,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragranceType_Position_1() (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragranceType_Position_1)
#endif

/* Handle:   69,Name: BCM_FM_FragranceType_Position_2,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragranceType_Position_2() (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragranceType_Position_2)
#endif

/* Handle:   70,Name: BCM_FM_FragranceType_Position_3,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragranceType_Position_3() (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragranceType_Position_3)
#endif

/* Handle:   71,Name:    BCM_FM_FragrancePositionSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FM_FragrancePositionSts() (CGW_BCM_FM.CGW_BCM_FM.BCM_FM_FragrancePositionSts)
#endif

/* Handle:   72,Name:    FM_FragranceReplaceRemind_1,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFM_FragranceReplaceRemind_1() (CGW_BCM_FM.CGW_BCM_FM.FM_FragranceReplaceRemind_1)
#endif

/* Handle:   73,Name:    FM_FragranceReplaceRemind_2,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFM_FragranceReplaceRemind_2() (CGW_BCM_FM.CGW_BCM_FM.FM_FragranceReplaceRemind_2)
#endif

/* Handle:   74,Name:    FM_FragranceReplaceRemind_3,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFM_FragranceReplaceRemind_3() (CGW_BCM_FM.CGW_BCM_FM.FM_FragranceReplaceRemind_3)
#endif

/* Handle:   75,Name:       BCM_FaceRecSeatEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FaceRecSeatEnableSts()    (BCM_6.BCM_6.BCM_FaceRecSeatEnableSts)
#endif

/* Handle:   76,Name: BCM_EntryExitSeatCtrlEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_EntryExitSeatCtrlEnableSts() (BCM_6.BCM_6.BCM_EntryExitSeatCtrlEnableSts)
#endif

/* Handle:   77,Name:    BCM_SeatPositionStoreResult,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SeatPositionStoreResult() (BCM_6.BCM_6.BCM_SeatPositionStoreResult)
#endif

/* Handle:   78,Name: BCM_DriverFaceRecognitionResult,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DriverFaceRecognitionResult() (BCM_6.BCM_6.BCM_DriverFaceRecognitionResult)
#endif

/* Handle:   79,Name: BCM_SeatPositionCalloutInhibitReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SeatPositionCalloutInhibitReq() (BCM_6.BCM_6.BCM_SeatPositionCalloutInhibitReq)
#endif

/* Handle:   80,Name:     BCM_SeatPositionCalloutSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SeatPositionCalloutSts()  (BCM_6.BCM_6.BCM_SeatPositionCalloutSts)
#endif

/* Handle:   81,Name:   BCM_DriverFaceRecognitionSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DriverFaceRecognitionSts() (BCM_6.BCM_6.BCM_DriverFaceRecognitionSts)
#endif

/* Handle:   82,Name:        BCM_MirrorFoldEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorFoldEnableSts()     (BCM_6.BCM_6.BCM_MirrorFoldEnableSts)
#endif

/* Handle:   83,Name: BCM_MirrorReverseInclineEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorReverseInclineEnableSts() (BCM_6.BCM_6.BCM_MirrorReverseInclineEnableSts)
#endif

/* Handle:   84,Name: BCM_FaceRecMirrorInclineEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FaceRecMirrorInclineEnableSts() (BCM_6.BCM_6.BCM_FaceRecMirrorInclineEnableSts)
#endif

/* Handle:   85,Name: BCM_MirrorPositionMemoryHMIReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorPositionMemoryHMIReq() (BCM_6.BCM_6.BCM_MirrorPositionMemoryHMIReq)
#endif

/* Handle:   86,Name:            BCM_MirrorCtrlSts_L,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorCtrlSts_L()         (BCM_6.BCM_6.BCM_MirrorCtrlSts_L)
#endif

/* Handle:   87,Name:            BCM_MirrorCtrlSts_R,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorCtrlSts_R()         (BCM_6.BCM_6.BCM_MirrorCtrlSts_R)
#endif

/* Handle:   88,Name: BCM_MirrorReversePositionStoreResult,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorReversePositionStoreResult() (BCM_6.BCM_6.BCM_MirrorReversePositionStoreResult)
#endif

/* Handle:   89,Name:            BCM_TirePressure_FL,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressure_FL()         (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TirePressure_FL)
#endif

/* Handle:   90,Name:            BCM_TirePressure_FR,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressure_FR()         (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TirePressure_FR)
#endif

/* Handle:   91,Name:            BCM_TirePressure_RL,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressure_RL()         (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TirePressure_RL)
#endif

/* Handle:   92,Name:            BCM_TirePressure_RR,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressure_RR()         (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TirePressure_RR)
#endif

/* Handle:   93,Name:                BCM_TireTemp_FL,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTemp_FL()             (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TireTemp_FL)
#endif

/* Handle:   94,Name:                BCM_TireTemp_FR,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTemp_FR()             (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TireTemp_FR)
#endif

/* Handle:   95,Name:                BCM_TireTemp_RL,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTemp_RL()             (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TireTemp_RL)
#endif

/* Handle:   96,Name:                BCM_TireTemp_RR,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTemp_RR()             (CGW_BCM_TPMS_2.CGW_BCM_TPMS_2.BCM_TireTemp_RR)
#endif

/* Handle:   97,Name:             FCM_10_TSISgnGiWay,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TSISgnGiWay()          (CGW_FCM_10.CGW_FCM_10.FCM_10_TSISgnGiWay)
#endif

/* Handle:   98,Name:               FCM_10_TLISysSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TLISysSts()            (CGW_FCM_10.CGW_FCM_10.FCM_10_TLISysSts)
#endif

/* Handle:   99,Name:               FCM_10_TSISysSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TSISysSts()            (CGW_FCM_10.CGW_FCM_10.FCM_10_TSISysSts)
#endif

/* Handle:  100,Name:      FCM_10_TSISgnLongStayDisp,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TSISgnLongStayDisp()   (CGW_FCM_10.CGW_FCM_10.FCM_10_TSISgnLongStayDisp)
#endif

/* Handle:  101,Name:              FCM_10_TSISgnForb,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TSISgnForb()           (CGW_FCM_10.CGW_FCM_10.FCM_10_TSISgnForb)
#endif

/* Handle:  102,Name:               FCM_10_LDWSysSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_LDWSysSts()            (CGW_FCM_10.CGW_FCM_10.FCM_10_LDWSysSts)
#endif

/* Handle:  103,Name:               FCM_10_LDPSysSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_LDPSysSts()            (CGW_FCM_10.CGW_FCM_10.FCM_10_LDPSysSts)
#endif

/* Handle:  104,Name:               FCM_10_TLITrfcLi,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TLITrfcLi()            (CGW_FCM_10.CGW_FCM_10.FCM_10_TLITrfcLi)
#endif

/* Handle:  105,Name:            FCM_10_LCCEnaRcmend,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_LCCEnaRcmend()         (CGW_FCM_10.CGW_FCM_10.FCM_10_LCCEnaRcmend)
#endif

/* Handle:  106,Name:                 FCM_10_LCCText,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_LCCText()              (CGW_FCM_10.CGW_FCM_10.FCM_10_LCCText)
#endif

/* Handle:  107,Name:           FCM_10_TSINoParkWarn,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_TSINoParkWarn()        (CGW_FCM_10.CGW_FCM_10.FCM_10_TSINoParkWarn)
#endif

/* Handle:  108,Name:               FCM_10_ELKSysSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_ELKSysSts()            (CGW_FCM_10.CGW_FCM_10.FCM_10_ELKSysSts)
#endif

/* Handle:  109,Name:              FCM_10_WarnModSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_WarnModSts()           (CGW_FCM_10.CGW_FCM_10.FCM_10_WarnModSts)
#endif

/* Handle:  110,Name:             FCM_10_SLAONOFFSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_SLAONOFFSts()          (CGW_FCM_10.CGW_FCM_10.FCM_10_SLAONOFFSts)
#endif

/* Handle:  111,Name:      FCM_10_LCCExitTextInfoSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_LCCExitTextInfoSts()   (CGW_FCM_10.CGW_FCM_10.FCM_10_LCCExitTextInfoSts)
#endif

/* Handle:  112,Name:    FCM_10_LDPTJAELKTakeoverReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_LDPTJAELKTakeoverReq() (CGW_FCM_10.CGW_FCM_10.FCM_10_LDPTJAELKTakeoverReq)
#endif

/* Handle:  114,Name:              FCM_10_ELKIntvMod,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_10_ELKIntvMod()           (CGW_FCM_10.CGW_FCM_10.FCM_10_ELKIntvMod)
#endif

/* Handle:  116,Name:            FCM_FRM_9_ACCObjTyp,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_9_ACCObjTyp()         (CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_ACCObjTyp)
#endif

/* Handle:  117,Name:              FCM_FRM_9_LeObjID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_9_LeObjID()           (CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_LeObjID)
#endif

/* Handle:  120,Name:        FCM_FRM_9_FrntFarObjTyp,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_9_FrntFarObjTyp()     (CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_FrntFarObjTyp)
#endif

/* Handle:  121,Name:         FCM_FRM_9_FrntFarObjID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_9_FrntFarObjID()      (CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_FrntFarObjID)
#endif

/* Handle:  123,Name:       FCM_FRM_9_MessageCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_9_MessageCounter()    (CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_MessageCounter)
#endif

/* Handle:  124,Name:                  FCM_FRM_9_CRC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_9_CRC()               (CGW_FCM_FRM_9.CGW_FCM_FRM_9.FCM_FRM_9_CRC)
#endif

/* Handle:  126,Name:            FCM_FRM_8_ACCObjTyp,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_8_ACCObjTyp()         (CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_ACCObjTyp)
#endif

/* Handle:  127,Name:             FCM_FRM_8_ACCObjID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_8_ACCObjID()          (CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_ACCObjID)
#endif

/* Handle:  130,Name:        FCM_FRM_8_FrntFarObjTyp,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_8_FrntFarObjTyp()     (CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_FrntFarObjTyp)
#endif

/* Handle:  131,Name:         FCM_FRM_8_FrntFarObjID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_8_FrntFarObjID()      (CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_FrntFarObjID)
#endif

/* Handle:  133,Name:       FCM_FRM_8_MessageCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_8_MessageCounter()    (CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_MessageCounter)
#endif

/* Handle:  134,Name:                  FCM_FRM_8_CRC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_8_CRC()               (CGW_FCM_FRM_8.CGW_FCM_FRM_8.FCM_FRM_8_CRC)
#endif

/* Handle:  136,Name:           FCM_7_NeborLeLineTyp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_7_NeborLeLineTyp()        (CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineTyp)
#endif

/* Handle:  137,Name:         FCM_7_NeborLeLineColor,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_7_NeborLeLineColor()      (CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineColor)
#endif

/* Handle:  138,Name:            FCM_7_NeborLeLineID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_7_NeborLeLineID()         (CGW_FCM_7.CGW_FCM_7.FCM_7_NeborLeLineID)
#endif

/* Handle:  141,Name:           FCM_7_NeborRiLineTyp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_7_NeborRiLineTyp()        (CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineTyp)
#endif

/* Handle:  142,Name:         FCM_7_NeborRiLineColor,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_7_NeborRiLineColor()      (CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineColor)
#endif

/* Handle:  143,Name:            FCM_7_NeborRiLineID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_7_NeborRiLineID()         (CGW_FCM_7.CGW_FCM_7.FCM_7_NeborRiLineID)
#endif

/* Handle:  146,Name:             FCM_6_EgoLeLineTyp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_6_EgoLeLineTyp()          (CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineTyp)
#endif

/* Handle:  147,Name:           FCM_6_EgoLeLineColor,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_6_EgoLeLineColor()        (CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineColor)
#endif

/* Handle:  148,Name:              FCM_6_EgoLeLineID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_6_EgoLeLineID()           (CGW_FCM_6.CGW_FCM_6.FCM_6_EgoLeLineID)
#endif

/* Handle:  151,Name:             FCM_6_EgoRiLineTyp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_6_EgoRiLineTyp()          (CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineTyp)
#endif

/* Handle:  152,Name:           FCM_6_EgoRiLineColor,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_6_EgoRiLineColor()        (CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineColor)
#endif

/* Handle:  153,Name:              FCM_6_EgoRiLineID,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_6_EgoRiLineID()           (CGW_FCM_6.CGW_FCM_6.FCM_6_EgoRiLineID)
#endif

/* Handle:  155,Name:             ACCM_AQS_EnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_AQS_EnableSts()          (CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_AQS_EnableSts)
#endif

/* Handle:  158,Name:       ACCM_PM2_5_InternalLevel,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_PM2_5_InternalLevel()    (CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_PM2_5_InternalLevel)
#endif

/* Handle:  159,Name:       ACCM_PM2_5_ExternalLevel,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_PM2_5_ExternalLevel()    (CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_PM2_5_ExternalLevel)
#endif

/* Handle:  160,Name:            ACCM_SmokingModeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_SmokingModeSts()         (CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.ACCM_SmokingModeSts)
#endif

/* Handle:  161,Name:        AQS_AQS_AirQualityLevel,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxAQS_AQS_AirQualityLevel()     (CGW_BCM_ACCM_3.CGW_BCM_ACCM_3.AQS_AQS_AirQualityLevel)
#endif

/* Handle:  162,Name:                  ACCM_AUTO_Sts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_AUTO_Sts()               (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_AUTO_Sts)
#endif

/* Handle:  163,Name:                    ACCM_AC_Sts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_AC_Sts()                 (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_AC_Sts)
#endif

/* Handle:  164,Name:                   ACCM_PTC_Sts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_PTC_Sts()                (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_PTC_Sts)
#endif

/* Handle:  165,Name:         ACCM_System_ON_OFF_Sts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_System_ON_OFF_Sts()      (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_System_ON_OFF_Sts)
#endif

/* Handle:  166,Name:        ACCM_CirculationModeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_CirculationModeSts()     (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_CirculationModeSts)
#endif

/* Handle:  167,Name:               ACCM_BlowModeSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_BlowModeSts()            (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_BlowModeSts)
#endif

/* Handle:  168,Name:         ACCM_TempSetSts_Driver,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_TempSetSts_Driver()      (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_TempSetSts_Driver)
#endif

/* Handle:  169,Name:         ACCM_BlowerlevelSetSts,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_BlowerlevelSetSts()      (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_BlowerlevelSetSts)
#endif

/* Handle:  170,Name:            ACCM_DUAL_EnablaSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_DUAL_EnablaSts()         (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_DUAL_EnablaSts)
#endif

/* Handle:  171,Name:            ACCM_AutoDefrostSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_AutoDefrostSts()         (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_AutoDefrostSts)
#endif

/* Handle:  172,Name:            ACCM_VentilationSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_VentilationSts()         (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_VentilationSts)
#endif

/* Handle:  173,Name:              ACCM_InternalTemp,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_InternalTemp()           (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_InternalTemp)
#endif

/* Handle:  174,Name:              ACCM_ExternalTemp,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_ExternalTemp()           (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_ExternalTemp)
#endif

/* Handle:  175,Name:           ACCM_FrontDefrostSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_FrontDefrostSts()        (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_FrontDefrostSts)
#endif

/* Handle:  176,Name:         ACCM_AnionGeneratorSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_AnionGeneratorSts()      (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_AnionGeneratorSts)
#endif

/* Handle:  177,Name:                  ACCM_ECO_Mode,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_ECO_Mode()               (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_ECO_Mode)
#endif

/* Handle:  178,Name:          ACCM_TempSetSts_Psngr,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxACCM_TempSetSts_Psngr()       (CGW_BCM_ACCM_1.CGW_BCM_ACCM_1.ACCM_TempSetSts_Psngr)
#endif

/* Handle:  179,Name:               ICM_ThemelSetSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxICM_ThemelSetSts()            (ICM_2.ICM_2.ICM_ThemelSetSts)
#endif

/* Handle:  180,Name:     ICM_BrightnessAdjustSetSts,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxICM_BrightnessAdjustSetSts()  (ICM_2.ICM_2.ICM_BrightnessAdjustSetSts)
#endif

/* Handle:  181,Name:  ICM_FatigureDrivingTimeSetSts,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxICM_FatigureDrivingTimeSetSts() (ICM_2.ICM_2.ICM_FatigureDrivingTimeSetSts)
#endif

/* Handle:  182,Name:         ICM_OverSpdValueSetSts,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxICM_OverSpdValueSetSts()      (ICM_2.ICM_2.ICM_OverSpdValueSetSts)
#endif

/* Handle:  183,Name:         FCM_FRM_6_TextinfoWarn,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_TextinfoWarn()      (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_TextinfoWarn)
#endif

/* Handle:  184,Name:        FCM_FRM_6_Textinfo_Info,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_Textinfo_Info()     (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_Textinfo_Info)
#endif

/* Handle:  185,Name:       FCM_FRM_6_SCF_PopoverReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_SCF_PopoverReq()    (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_SCF_PopoverReq)
#endif

/* Handle:  186,Name:        FCM_FRM_6_TimeGapSetICM,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_TimeGapSetICM()     (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_TimeGapSetICM)
#endif

/* Handle:  187,Name:       FCM_FRM_6_FCW_preWarning,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_FCW_preWarning()    (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_FCW_preWarning)
#endif

/* Handle:  188,Name:      FCM_FRM_6_DistanceWarning,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_DistanceWarning()   (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_DistanceWarning)
#endif

/* Handle:  189,Name:     FCM_FRM_6_DrvrCfmSCFDispFb,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_DrvrCfmSCFDispFb()  (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_DrvrCfmSCFDispFb)
#endif

/* Handle:  190,Name:       FCM_FRM_6_SCF_SpdLimUnit,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_SCF_SpdLimUnit()    (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_SCF_SpdLimUnit)
#endif

/* Handle:  191,Name:        FCM_FRM_6_SCF_SpdLimSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_SCF_SpdLimSts()     (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_SCF_SpdLimSts)
#endif

/* Handle:  192,Name:              FCM_FRM_6_AEBMode,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_AEBMode()           (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_AEBMode)
#endif

/* Handle:  193,Name:              FCM_FRM_6_FCWMode,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_FCWMode()           (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_FCWMode)
#endif

/* Handle:  194,Name:          FCM_FRM_6_TakeOverReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_TakeOverReq()       (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_TakeOverReq)
#endif

/* Handle:  195,Name:              FCM_FRM_6_ACCMode,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_ACCMode()           (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_ACCMode)
#endif

/* Handle:  197,Name:              FCM_FRM_6_MsgCntr,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_MsgCntr()           (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_MsgCntr)
#endif

/* Handle:  198,Name:                  FCM_FRM_6_CRC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_FRM_6_CRC()               (CGW_FCM_FRM_6.CGW_FCM_FRM_6.FCM_FRM_6_CRC)
#endif

/* Handle:  199,Name:      BMS_ChgWireConnectStsDisp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBMS_ChgWireConnectStsDisp()   (CGW_BMS_9.CGW_BMS_9.BMS_ChgWireConnectStsDisp)
#endif

/* Handle:  201,Name:                     BMS_ChgSts,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBMS_ChgSts()                  (CGW_BMS_9.CGW_BMS_9.BMS_ChgSts)
#endif

/* Handle:  202,Name:           VCU_Chg_SOC_LimitSts,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_Chg_SOC_LimitSts()        (CGW_VCU_E_3.CGW_VCU_E_3.VCU_Chg_SOC_LimitSts)
#endif

/* Handle:  203,Name:             VCU_BookChgModeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookChgModeSts()          (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookChgModeSts)
#endif

/* Handle:  204,Name:   VCU_BookChgStartTimeSts_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookChgStartTimeSts_Hour() (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookChgStartTimeSts_Hour)
#endif

/* Handle:  205,Name: VCU_BookChgStartTimeSts_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookChgStartTimeSts_Minute() (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookChgStartTimeSts_Minute)
#endif

/* Handle:  206,Name:                VCU_BookChgSts1,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookChgSts1()             (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookChgSts1)
#endif

/* Handle:  207,Name:    VCU_BookchgStopTimeSts_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookchgStopTimeSts_Hour() (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookchgStopTimeSts_Hour)
#endif

/* Handle:  208,Name:  VCU_BookChgStopTimeSts_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookChgStopTimeSts_Minute() (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookChgStopTimeSts_Minute)
#endif

/* Handle:  209,Name:                VCU_BookChgSts2,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BookChgSts2()             (CGW_VCU_E_3.CGW_VCU_E_3.VCU_BookChgSts2)
#endif

/* Handle:  210,Name:                    PLG_LockSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxPLG_LockSts()                 (CGW_PLG_1.CGW_PLG_1.PLG_LockSts)
#endif

/* Handle:  211,Name:          PLG_MaxPositionSetSts,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxPLG_MaxPositionSetSts()       (CGW_PLG_1.CGW_PLG_1.PLG_MaxPositionSetSts)
#endif

/* Handle:  212,Name:             RCM_SnsDistance_RL,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_RL()          (RCM_1.RCM_1.RCM_SnsDistance_RL)
#endif

/* Handle:  213,Name:            RCM_SnsDistance_RMR,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_RMR()         (RCM_1.RCM_1.RCM_SnsDistance_RMR)
#endif

/* Handle:  214,Name:            RCM_SnsDistance_RML,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_RML()         (RCM_1.RCM_1.RCM_SnsDistance_RML)
#endif

/* Handle:  215,Name:             RCM_SnsDistance_RR,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_RR()          (RCM_1.RCM_1.RCM_SnsDistance_RR)
#endif

/* Handle:  216,Name:             RCM_SnsDistance_FL,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_FL()          (RCM_1.RCM_1.RCM_SnsDistance_FL)
#endif

/* Handle:  217,Name:            RCM_AudibleBeepRate,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_AudibleBeepRate()         (RCM_1.RCM_1.RCM_AudibleBeepRate)
#endif

/* Handle:  218,Name:            RCM_SnsDistance_FML,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_FML()         (RCM_1.RCM_1.RCM_SnsDistance_FML)
#endif

/* Handle:  219,Name:            RCM_SnsDistance_FMR,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_FMR()         (RCM_1.RCM_1.RCM_SnsDistance_FMR)
#endif

/* Handle:  220,Name:             RCM_SnsDistance_FR,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_SnsDistance_FR()          (RCM_1.RCM_1.RCM_SnsDistance_FR)
#endif

/* Handle:  221,Name:                 RCM_WorkingSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_WorkingSts()              (RCM_1.RCM_1.RCM_WorkingSts)
#endif

/* Handle:  222,Name:               RCM_DetectingSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxRCM_DetectingSts()            (RCM_1.RCM_1.RCM_DetectingSts)
#endif

/* Handle:  223,Name:   BCM_TirePressureWarnLampDisp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureWarnLampDisp() (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureWarnLampDisp)
#endif

/* Handle:  224,Name:    BCM_TirePressureSystemFault,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureSystemFault() (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureSystemFault)
#endif

/* Handle:  225,Name:        BCM_TirePressureWarn_FL,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureWarn_FL()     (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureWarn_FL)
#endif

/* Handle:  226,Name:            BCM_TireTempWarn_FL,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTempWarn_FL()         (BCM_TPMS_1.BCM_TPMS_1.BCM_TireTempWarn_FL)
#endif

/* Handle:  227,Name:       BCM_TirePressureDelta_FL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureDelta_FL()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureDelta_FL)
#endif

/* Handle:  228,Name:         BCM_TireSensorFault_FL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorFault_FL()      (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorFault_FL)
#endif

/* Handle:  229,Name:       BCM_TireSensorLowBatt_FL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorLowBatt_FL()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorLowBatt_FL)
#endif

/* Handle:  230,Name: BCM_TireSensorCalibrationSts_FL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorCalibrationSts_FL() (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorCalibrationSts_FL)
#endif

/* Handle:  231,Name:        BCM_TirePressureWarn_FR,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureWarn_FR()     (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureWarn_FR)
#endif

/* Handle:  232,Name:            BCM_TireTempWarn_FR,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTempWarn_FR()         (BCM_TPMS_1.BCM_TPMS_1.BCM_TireTempWarn_FR)
#endif

/* Handle:  233,Name:       BCM_TirePressureDelta_FR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureDelta_FR()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureDelta_FR)
#endif

/* Handle:  234,Name:         BCM_TireSensorFault_FR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorFault_FR()      (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorFault_FR)
#endif

/* Handle:  235,Name:       BCM_TireSensorLowBatt_FR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorLowBatt_FR()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorLowBatt_FR)
#endif

/* Handle:  236,Name: BCM_TireSensorCalibrationSts_FR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorCalibrationSts_FR() (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorCalibrationSts_FR)
#endif

/* Handle:  237,Name:        BCM_TirePressureWarn_RL,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureWarn_RL()     (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureWarn_RL)
#endif

/* Handle:  238,Name:            BCM_TireTempWarn_RL,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTempWarn_RL()         (BCM_TPMS_1.BCM_TPMS_1.BCM_TireTempWarn_RL)
#endif

/* Handle:  239,Name:       BCM_TirePressureDelta_RL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureDelta_RL()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureDelta_RL)
#endif

/* Handle:  240,Name:         BCM_TireSensorFault_RL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorFault_RL()      (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorFault_RL)
#endif

/* Handle:  241,Name:       BCM_TireSensorLowBatt_RL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorLowBatt_RL()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorLowBatt_RL)
#endif

/* Handle:  242,Name: BCM_TireSensorCalibrationSts_RL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorCalibrationSts_RL() (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorCalibrationSts_RL)
#endif

/* Handle:  243,Name:        BCM_TirePressureWarn_RR,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureWarn_RR()     (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureWarn_RR)
#endif

/* Handle:  244,Name:            BCM_TireTempWarn_RR,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireTempWarn_RR()         (BCM_TPMS_1.BCM_TPMS_1.BCM_TireTempWarn_RR)
#endif

/* Handle:  245,Name:       BCM_TirePressureDelta_RR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TirePressureDelta_RR()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TirePressureDelta_RR)
#endif

/* Handle:  246,Name:         BCM_TireSensorFault_RR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorFault_RR()      (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorFault_RR)
#endif

/* Handle:  247,Name:       BCM_TireSensorLowBatt_RR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorLowBatt_RR()    (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorLowBatt_RR)
#endif

/* Handle:  248,Name: BCM_TireSensorCalibrationSts_RR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TireSensorCalibrationSts_RR() (BCM_TPMS_1.BCM_TPMS_1.BCM_TireSensorCalibrationSts_RR)
#endif

/* Handle:  249,Name:           BCM_WinPostionSts_FL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WinPostionSts_FL()        (CGW_BCM_4.CGW_BCM_4.BCM_WinPostionSts_FL)
#endif

/* Handle:  250,Name:           BCM_WinPostionSts_FR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WinPostionSts_FR()        (CGW_BCM_4.CGW_BCM_4.BCM_WinPostionSts_FR)
#endif

/* Handle:  251,Name:           BCM_WinPostionSts_RL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WinPostionSts_RL()        (CGW_BCM_4.CGW_BCM_4.BCM_WinPostionSts_RL)
#endif

/* Handle:  252,Name:           BCM_WinPostionSts_RR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WinPostionSts_RR()        (CGW_BCM_4.CGW_BCM_4.BCM_WinPostionSts_RR)
#endif

/* Handle:  253,Name:         BCM_SunroofPositionSts,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SunroofPositionSts()      (CGW_BCM_4.CGW_BCM_4.BCM_SunroofPositionSts)
#endif

/* Handle:  254,Name:                 BCM_SunroofSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SunroofSts()              (CGW_BCM_4.CGW_BCM_4.BCM_SunroofSts)
#endif

/* Handle:  255,Name:             BCM_SunshadeReqSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SunshadeReqSts()          (CGW_BCM_4.CGW_BCM_4.BCM_SunshadeReqSts)
#endif

/* Handle:  256,Name:          BCM_AutoLockEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AutoLockEnableSts()       (CGW_BCM_5.CGW_BCM_5.BCM_AutoLockEnableSts)
#endif

/* Handle:  257,Name:           BCM_AntiTheftModeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AntiTheftModeSts()        (CGW_BCM_5.CGW_BCM_5.BCM_AntiTheftModeSts)
#endif

/* Handle:  258,Name:            BCM_FollowMeHomeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FollowMeHomeSts()         (CGW_BCM_5.CGW_BCM_5.BCM_FollowMeHomeSts)
#endif

/* Handle:  259,Name:  BCM_SunroofAutoCloseEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SunroofAutoCloseEnableSts() (CGW_BCM_5.CGW_BCM_5.BCM_SunroofAutoCloseEnableSts)
#endif

/* Handle:  260,Name:         BCM_FollowMeTimeSetSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FollowMeTimeSetSts()      (CGW_BCM_5.CGW_BCM_5.BCM_FollowMeTimeSetSts)
#endif

/* Handle:  261,Name:        BCM_WiperSensitivitySts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WiperSensitivitySts()     (CGW_BCM_5.CGW_BCM_5.BCM_WiperSensitivitySts)
#endif

/* Handle:  262,Name:                BCM_RearDfstSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_RearDfstSts()             (CGW_BCM_5.CGW_BCM_5.BCM_RearDfstSts)
#endif

/* Handle:  263,Name:      BCM_LowBeamAngleAdjustSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_LowBeamAngleAdjustSts()   (CGW_BCM_5.CGW_BCM_5.BCM_LowBeamAngleAdjustSts)
#endif

/* Handle:  264,Name: BCM_FrontWiperMaintenanceModeSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FrontWiperMaintenanceModeSts() (CGW_BCM_5.CGW_BCM_5.BCM_FrontWiperMaintenanceModeSts)
#endif

/* Handle:  265,Name:         BCM_MirrorUnfoldRemind,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_MirrorUnfoldRemind()      (CGW_BCM_5.CGW_BCM_5.BCM_MirrorUnfoldRemind)
#endif

/* Handle:  266,Name:    BCM_DoorWindowCtrlEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DoorWindowCtrlEnableSts() (CGW_BCM_5.CGW_BCM_5.BCM_DoorWindowCtrlEnableSts)
#endif

/* Handle:  267,Name:    BCM_SunroofCtrlEnableSts_RF,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SunroofCtrlEnableSts_RF() (CGW_BCM_5.CGW_BCM_5.BCM_SunroofCtrlEnableSts_RF)
#endif

/* Handle:  268,Name: BCM_SunroofCtrlEnableSts_Remote,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SunroofCtrlEnableSts_Remote() (CGW_BCM_5.CGW_BCM_5.BCM_SunroofCtrlEnableSts_Remote)
#endif

/* Handle:  269,Name:      BCM_WelcomelightEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WelcomelightEnableSts()   (CGW_BCM_5.CGW_BCM_5.BCM_WelcomelightEnableSts)
#endif

/* Handle:  270,Name:   BCM_WindowAutoCloseEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WindowAutoCloseEnableSts() (CGW_BCM_5.CGW_BCM_5.BCM_WindowAutoCloseEnableSts)
#endif

/* Handle:  271,Name:   BCM_RKE_SunRoofCtrlEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_RKE_SunRoofCtrlEnableSts() (CGW_BCM_5.CGW_BCM_5.BCM_RKE_SunRoofCtrlEnableSts)
#endif

/* Handle:  272,Name:              IHU_AHB_EnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxIHU_AHB_EnableSts()           (CGW_BCM_5.CGW_BCM_5.IHU_AHB_EnableSts)
#endif

/* Handle:  273,Name:        BCM_WelcomeAnimationReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_WelcomeAnimationReq()     (CGW_BCM_5.CGW_BCM_5.BCM_WelcomeAnimationReq)
#endif

/* Handle:  274,Name:       BCM_SteeringWheelHeatSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_SteeringWheelHeatSts()    (CGW_BCM_5.CGW_BCM_5.BCM_SteeringWheelHeatSts)
#endif

/* Handle:  275,Name:          BCM_AutoBlowEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AutoBlowEnableSts()       (CGW_BCM_5.CGW_BCM_5.BCM_AutoBlowEnableSts)
#endif

/* Handle:  276,Name: BCM_DomeLightDoorCtrlSwitchSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DomeLightDoorCtrlSwitchSts() (CGW_BCM_5.CGW_BCM_5.BCM_DomeLightDoorCtrlSwitchSts)
#endif

/* Handle:  277,Name:                  VCU_DriveMode,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_DriveMode()               (CGW_VCU_B_5.CGW_VCU_B_5.VCU_DriveMode)
#endif

/* Handle:  278,Name:     VCU_LongRangeRemainingTime,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_LongRangeRemainingTime()  (CGW_VCU_B_5.CGW_VCU_B_5.VCU_LongRangeRemainingTime)
#endif

/* Handle:  283,Name:   VCU_ManualToGearP_InhibitSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_ManualToGearP_InhibitSts() (CGW_VCU_B_3.CGW_VCU_B_3.VCU_ManualToGearP_InhibitSts)
#endif

/* Handle:  284,Name: VCU_ShiftErrorActionRemindEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_ShiftErrorActionRemindEnableSts() (CGW_VCU_B_3.CGW_VCU_B_3.VCU_ShiftErrorActionRemindEnableSts)
#endif

/* Handle:  285,Name:        VCU_InverterEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterEnableSwSts()     (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterEnableSwSts)
#endif

/* Handle:  286,Name:              VCU_InverterPower,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterPower()           (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterPower)
#endif

/* Handle:  287,Name:              VCU_InverterInd_1,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterInd_1()           (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterInd_1)
#endif

/* Handle:  288,Name:              VCU_InverterInd_2,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterInd_2()           (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterInd_2)
#endif

/* Handle:  289,Name:              VCU_InverterInd_3,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterInd_3()           (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterInd_3)
#endif

/* Handle:  290,Name:              VCU_InverterInd_4,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterInd_4()           (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterInd_4)
#endif

/* Handle:  291,Name:           VCU_InverterFaultSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterFaultSts()        (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterFaultSts)
#endif

/* Handle:  292,Name:         VCU_220V_OutputTimeInd,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_220V_OutputTimeInd()      (CGW_VCU_B_3.CGW_VCU_B_3.VCU_220V_OutputTimeInd)
#endif

/* Handle:  293,Name: VCU_PowerOffAutoParkInhibitSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_PowerOffAutoParkInhibitSts() (CGW_VCU_B_3.CGW_VCU_B_3.VCU_PowerOffAutoParkInhibitSts)
#endif

/* Handle:  294,Name:     VCU_DoorOpenParkInhibitSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_DoorOpenParkInhibitSts()  (CGW_VCU_B_3.CGW_VCU_B_3.VCU_DoorOpenParkInhibitSts)
#endif

/* Handle:  295,Name:          VCU_InverterEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_InverterEnableSts()       (CGW_VCU_B_3.CGW_VCU_B_3.VCU_InverterEnableSts)
#endif

/* Handle:  296,Name:             VCU_OutputLimitSOC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_OutputLimitSOC()          (CGW_VCU_B_3.CGW_VCU_B_3.VCU_OutputLimitSOC)
#endif

/* Handle:  297,Name:                     HMA_Status,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxHMA_Status()                  (CGW_FCM_2.CGW_FCM_2.HMA_Status)
#endif

/* Handle:  298,Name:     LDW_ELK_LKA_LDPLeftVisuali,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxLDW_ELK_LKA_LDPLeftVisuali()  (CGW_FCM_2.CGW_FCM_2.LDW_ELK_LKA_LDPLeftVisuali)
#endif

/* Handle:  299,Name:                      LKA_State,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxLKA_State()                   (CGW_FCM_2.CGW_FCM_2.LKA_State)
#endif

/* Handle:  300,Name:    LDW_ELK_LKA_LDPRightVisuali,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxLDW_ELK_LKA_LDPRightVisuali() (CGW_FCM_2.CGW_FCM_2.LDW_ELK_LKA_LDPRightVisuali)
#endif

/* Handle:  302,Name:                       SLAState,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSLAState()                    (CGW_FCM_2.CGW_FCM_2.SLAState)
#endif

/* Handle:  303,Name:             SLASpdlimitWarning,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxSLASpdlimitWarning()          (CGW_FCM_2.CGW_FCM_2.SLASpdlimitWarning)
#endif

/* Handle:  304,Name:                Camera_textinfo,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxCamera_textinfo()             (CGW_FCM_2.CGW_FCM_2.Camera_textinfo)
#endif

/* Handle:  305,Name:         FCM_2_SLASpdlimitUnits,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxFCM_2_SLASpdlimitUnits()      (CGW_FCM_2.CGW_FCM_2.FCM_2_SLASpdlimitUnits)
#endif

/* Handle:  306,Name:                       LCC_mode,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxLCC_mode()                    (CGW_FCM_2.CGW_FCM_2.LCC_mode)
#endif

/* Handle:  307,Name:    BSD_RearWarnSystemEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBSD_RearWarnSystemEnableSts() (CGW_BSD_1.CGW_BSD_1.BSD_RearWarnSystemEnableSts)
#endif

/* Handle:  308,Name: MFS_LeftWheelButtonSts_ScrollUp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_LeftWheelButtonSts_ScrollUp() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_LeftWheelButtonSts_ScrollUp)
#endif

/* Handle:  309,Name: MFS_LeftWheelButtonSts_ScrollDown,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_LeftWheelButtonSts_ScrollDown() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_LeftWheelButtonSts_ScrollDown)
#endif

/* Handle:  310,Name: MFS_LeftWheelButtonSts_ToggleLeft,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_LeftWheelButtonSts_ToggleLeft() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_LeftWheelButtonSts_ToggleLeft)
#endif

/* Handle:  311,Name: MFS_LeftWheelButtonSts_ToggleRight,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_LeftWheelButtonSts_ToggleRight() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_LeftWheelButtonSts_ToggleRight)
#endif

/* Handle:  312,Name:   MFS_LeftWheelButtonSts_Press,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_LeftWheelButtonSts_Press() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_LeftWheelButtonSts_Press)
#endif

/* Handle:  313,Name:            MFS_WeChatButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_WeChatButtonSts()         (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_WeChatButtonSts)
#endif

/* Handle:  314,Name:            MFS_CustomButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_CustomButtonSts()         (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_CustomButtonSts)
#endif

/* Handle:  315,Name: MFS_RightWheelButtonSts_ScrollUp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_RightWheelButtonSts_ScrollUp() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_RightWheelButtonSts_ScrollUp)
#endif

/* Handle:  316,Name: MFS_RightWheelButtonSts_ScrollDown,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_RightWheelButtonSts_ScrollDown() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_RightWheelButtonSts_ScrollDown)
#endif

/* Handle:  317,Name: MFS_RightWheelButtonSts_ToggleLeft,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_RightWheelButtonSts_ToggleLeft() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_RightWheelButtonSts_ToggleLeft)
#endif

/* Handle:  318,Name: MFS_RightWheelButtonSts_ToggleRight,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_RightWheelButtonSts_ToggleRight() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_RightWheelButtonSts_ToggleRight)
#endif

/* Handle:  319,Name:  MFS_RightWheelButtonSts_Press,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_RightWheelButtonSts_Press() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_RightWheelButtonSts_Press)
#endif

/* Handle:  320,Name:            MFS_ReturnButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_ReturnButtonSts()         (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_ReturnButtonSts)
#endif

/* Handle:  321,Name:  MFS_VoiceRecognitionButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_VoiceRecognitionButtonSts() (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_VoiceRecognitionButtonSts)
#endif

/* Handle:  322,Name:            MFS_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_MsgAliveCounter()         (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_MsgAliveCounter)
#endif

/* Handle:  323,Name:               MFS_Checksum_CRC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxMFS_Checksum_CRC()            (CGW_BCM_MFS_1.CGW_BCM_MFS_1.MFS_Checksum_CRC)
#endif

/* Handle:  324,Name: BCM_EmergencyPowerOffRemindSts_1,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_EmergencyPowerOffRemindSts_1() (CGW_BCM_PEPS_1.CGW_BCM_PEPS_1.BCM_EmergencyPowerOffRemindSts_1)
#endif

/* Handle:  325,Name: BCM_EmergencyPowerOffRemindSts_2,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_EmergencyPowerOffRemindSts_2() (CGW_BCM_PEPS_1.CGW_BCM_PEPS_1.BCM_EmergencyPowerOffRemindSts_2)
#endif

/* Handle:  326,Name:          VCU_CruiseCtrlStsDisp,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_CruiseCtrlStsDisp()       (CGW_VCU_B_2.CGW_VCU_B_2.VCU_CruiseCtrlStsDisp)
#endif

/* Handle:  327,Name:       VCU_RegenerationLevelSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_RegenerationLevelSts()    (CGW_VCU_B_2.CGW_VCU_B_2.VCU_RegenerationLevelSts)
#endif

/* Handle:  328,Name:                VCU_SpdLimitSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_SpdLimitSts()             (CGW_VCU_B_2.CGW_VCU_B_2.VCU_SpdLimitSts)
#endif

/* Handle:  329,Name:        VCU_SpeedLimitEnableSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_SpeedLimitEnableSts()     (CGW_VCU_B_2.CGW_VCU_B_2.VCU_SpeedLimitEnableSts)
#endif

/* Handle:  330,Name:         VCU_SpeedLimitValueSet,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_SpeedLimitValueSet()      (CGW_VCU_B_2.CGW_VCU_B_2.VCU_SpeedLimitValueSet)
#endif

/* Handle:  332,Name:             ICM_LowSOC_LampSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxICM_LowSOC_LampSts()          (ICM_1.ICM_1.ICM_LowSOC_LampSts)
#endif

/* Handle:  335,Name:                     BCM_KeySts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_KeySts()                  (CGW_BCM_1.CGW_BCM_1.BCM_KeySts)
#endif

/* Handle:  336,Name:            BCM_RearFogLightSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_RearFogLightSts()         (CGW_BCM_1.CGW_BCM_1.BCM_RearFogLightSts)
#endif

/* Handle:  337,Name:             BCM_DoorAjarSts_FL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DoorAjarSts_FL()          (CGW_BCM_1.CGW_BCM_1.BCM_DoorAjarSts_FL)
#endif

/* Handle:  338,Name:             BCM_DoorAjarSts_FR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DoorAjarSts_FR()          (CGW_BCM_1.CGW_BCM_1.BCM_DoorAjarSts_FR)
#endif

/* Handle:  339,Name:             BCM_DoorAjarSts_RR,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DoorAjarSts_RR()          (CGW_BCM_1.CGW_BCM_1.BCM_DoorAjarSts_RR)
#endif

/* Handle:  340,Name:             BCM_DoorAjarSts_RL,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_DoorAjarSts_RL()          (CGW_BCM_1.CGW_BCM_1.BCM_DoorAjarSts_RL)
#endif

/* Handle:  341,Name:                BCM_HoodAjarSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_HoodAjarSts()             (CGW_BCM_1.CGW_BCM_1.BCM_HoodAjarSts)
#endif

/* Handle:  342,Name:               BCM_TrunkAjarSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_TrunkAjarSts()            (CGW_BCM_1.CGW_BCM_1.BCM_TrunkAjarSts)
#endif

/* Handle:  343,Name:              BCM_RearWashSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_RearWashSwSts()           (CGW_BCM_1.CGW_BCM_1.BCM_RearWashSwSts)
#endif

/* Handle:  344,Name:            BCM_FrontWiperSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_FrontWiperSwSts()         (CGW_BCM_1.CGW_BCM_1.BCM_FrontWiperSwSts)
#endif

/* Handle:  345,Name:             BCM_RearWiperSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_RearWiperSwSts()          (CGW_BCM_1.CGW_BCM_1.BCM_RearWiperSwSts)
#endif

/* Handle:  346,Name:             BCM_LightCtrlSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_LightCtrlSwSts()          (CGW_BCM_1.CGW_BCM_1.BCM_LightCtrlSwSts)
#endif

/* Handle:  347,Name:              BCM_AntiTheftMode,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCM_AntiTheftMode()           (CGW_BCM_1.CGW_BCM_1.BCM_AntiTheftMode)
#endif

/* Handle:  348,Name:             EPS_WorkingModeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEPS_WorkingModeSts()          (CGW_EPS_1.CGW_EPS_1.EPS_WorkingModeSts)
#endif

/* Handle:  349,Name:          BCS_EPB_HMI_RemindReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_EPB_HMI_RemindReq()       (CGW_BCS_EPB_1.CGW_BCS_EPB_1.BCS_EPB_HMI_RemindReq)
#endif

/* Handle:  350,Name:            BCS_EPB_ActuatorSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_EPB_ActuatorSts()         (CGW_BCS_EPB_1.CGW_BCS_EPB_1.BCS_EPB_ActuatorSts)
#endif

/* Handle:  351,Name:        EPB_AutoApplyDisableSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEPB_AutoApplyDisableSts()     (CGW_BCS_EPB_1.CGW_BCS_EPB_1.EPB_AutoApplyDisableSts)
#endif

/* Handle:  352,Name:        BCS_EPB_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_EPB_MsgAliveCounter()     (CGW_BCS_EPB_1.CGW_BCS_EPB_1.BCS_EPB_MsgAliveCounter)
#endif

/* Handle:  353,Name:           BCS_EPB_Checksum_CRC,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_EPB_Checksum_CRC()        (CGW_BCS_EPB_1.CGW_BCS_EPB_1.BCS_EPB_Checksum_CRC)
#endif

/* Handle:  354,Name:      BCS_HDC_FunctionEnableSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_HDC_FunctionEnableSts()   (CGW_BCS_C_1.CGW_BCS_C_1.BCS_HDC_FunctionEnableSts)
#endif

/* Handle:  355,Name:      BCS_AVH_FunctionEnableSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_AVH_FunctionEnableSts()   (CGW_BCS_C_1.CGW_BCS_C_1.BCS_AVH_FunctionEnableSts)
#endif

/* Handle:  356,Name:      BCS_CST_FunctionEnableSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_CST_FunctionEnableSts()   (CGW_BCS_C_1.CGW_BCS_C_1.BCS_CST_FunctionEnableSts)
#endif

/* Handle:  357,Name:                 BCS_CST_Active,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_CST_Active()              (CGW_BCS_C_1.CGW_BCS_C_1.BCS_CST_Active)
#endif

/* Handle:  358,Name:               BCS_BrakemodeSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxBCS_BrakemodeSts()            (CGW_BCS_C_1.CGW_BCS_C_1.BCS_BrakemodeSts)
#endif

/* Handle:  359,Name:                 EGS_FaultLevel,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEGS_FaultLevel()              (CGW_EGS_1.CGW_EGS_1.EGS_FaultLevel)
#endif

/* Handle:  360,Name:    EGS_HighBeamSwitchActiveSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEGS_HighBeamSwitchActiveSts() (CGW_EGS_1.CGW_EGS_1.EGS_HighBeamSwitchActiveSts)
#endif

/* Handle:  361,Name: EGS_HighBeamFlashSwitchActiveSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEGS_HighBeamFlashSwitchActiveSts() (CGW_EGS_1.CGW_EGS_1.EGS_HighBeamFlashSwitchActiveSts)
#endif

/* Handle:  362,Name: EGS_FrontWiperMistSwitchActiveSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEGS_FrontWiperMistSwitchActiveSts() (CGW_EGS_1.CGW_EGS_1.EGS_FrontWiperMistSwitchActiveSts)
#endif

/* Handle:  363,Name:   EGS_FrontWashSwitchActiveSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxEGS_FrontWashSwitchActiveSts() (CGW_EGS_1.CGW_EGS_1.EGS_FrontWashSwitchActiveSts)
#endif

/* Handle:  364,Name:                    VCU_ActGear,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_ActGear()                 (CGW_VCU_P_5.CGW_VCU_P_5.VCU_ActGear)
#endif

/* Handle:  366,Name:              VCU_BrakePedalSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BrakePedalSts()           (CGW_VCU_P_5.CGW_VCU_P_5.VCU_BrakePedalSts)
#endif

/* Handle:  367,Name:         VCU_PowertrainReadySts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_PowertrainReadySts()      (CGW_VCU_P_5.CGW_VCU_P_5.VCU_PowertrainReadySts)
#endif

/* Handle:  368,Name:              VCU_BrakePedal_VD,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_BrakePedal_VD()           (CGW_VCU_P_5.CGW_VCU_P_5.VCU_BrakePedal_VD)
#endif

/* Handle:  369,Name:                 VCU_ActGear_VD,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_ActGear_VD()              (CGW_VCU_P_5.CGW_VCU_P_5.VCU_ActGear_VD)
#endif

/* Handle:  370,Name:                  VCU_VehSpd_VD,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_RX
#define IlGetRxVCU_VehSpd_VD()               (CGW_VCU_P_5.CGW_VCU_P_5.VCU_VehSpd_VD)
#endif



/* -----------------------------------------------------------------------------
    &&&~ Get Rx Signal Access for signals greater 8bit and smaller or equal 32bit
 ----------------------------------------------------------------------------- */

/* Handle:    2,Name:              SAS_SteeringAngle,Size: 16,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxSAS_SteeringAngle(void);
#endif

/* Handle:  113,Name:      FCM_10_EgoLeftLineHeatgAg,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_10_EgoLeftLineHeatgAg(void);
#endif

/* Handle:  115,Name:        FCM_FRM_9_ACCObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint8 IlGetRxFCM_FRM_9_ACCObjLgtDstX(void);
#endif

/* Handle:  118,Name:        FCM_FRM_9_ACCObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_FRM_9_ACCObjHozDstY(void);
#endif

/* Handle:  119,Name:    FCM_FRM_9_FrntFarObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint8 IlGetRxFCM_FRM_9_FrntFarObjLgtDstX(void);
#endif

/* Handle:  122,Name:    FCM_FRM_9_FrntFarObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_FRM_9_FrntFarObjHozDstY(void);
#endif

/* Handle:  125,Name:        FCM_FRM_8_ACCObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint8 IlGetRxFCM_FRM_8_ACCObjLgtDstX(void);
#endif

/* Handle:  128,Name:        FCM_FRM_8_ACCObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_FRM_8_ACCObjHozDstY(void);
#endif

/* Handle:  129,Name:    FCM_FRM_8_FrntFarObjLgtDstX,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint8 IlGetRxFCM_FRM_8_FrntFarObjLgtDstX(void);
#endif

/* Handle:  132,Name:    FCM_FRM_8_FrntFarObjHozDstY,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_FRM_8_FrntFarObjHozDstY(void);
#endif

/* Handle:  135,Name:       FCM_7_NeborLeLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_7_NeborLeLineHozlDst(void);
#endif

/* Handle:  139,Name:          FCM_7_NeborLeLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_7_NeborLeLineCrvt(void);
#endif

/* Handle:  140,Name:       FCM_7_NeborRiLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_7_NeborRiLineHozlDst(void);
#endif

/* Handle:  144,Name:          FCM_7_NeborRiLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_7_NeborRiLineCrvt(void);
#endif

/* Handle:  145,Name:         FCM_6_EgoLeLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_6_EgoLeLineHozlDst(void);
#endif

/* Handle:  149,Name:            FCM_6_EgoLeLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_6_EgoLeLineCrvt(void);
#endif

/* Handle:  150,Name:         FCM_6_EgoRiLineHozlDst,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_6_EgoRiLineHozlDst(void);
#endif

/* Handle:  154,Name:            FCM_6_EgoRiLineCrvt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_6_EgoRiLineCrvt(void);
#endif

/* Handle:  156,Name:       ACCM_PM2_5_InternalValue,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxACCM_PM2_5_InternalValue(void);
#endif

/* Handle:  157,Name:       ACCM_PM2_5_ExternalValue,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxACCM_PM2_5_ExternalValue(void);
#endif

/* Handle:  196,Name:              FCM_FRM_6_VSetDis,Size:  9,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxFCM_FRM_6_VSetDis(void);
#endif

/* Handle:  200,Name:                   BMS_SOC_Disp,Size: 14,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxBMS_SOC_Disp(void);
#endif

/* Handle:  279,Name:           VCU_ResidualOdometer,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxVCU_ResidualOdometer(void);
#endif

/* Handle:  280,Name:            VCU_InstPowerConsum,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxVCU_InstPowerConsum(void);
#endif

/* Handle:  281,Name:             VCU_TotalPowerDisp,Size: 12,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxVCU_TotalPowerDisp(void);
#endif

/* Handle:  282,Name:              VCU_InstPowerDisp,Size: 12,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxVCU_InstPowerDisp(void);
#endif

/* Handle:  301,Name:                    SLASpdlimit,Size:  8,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint8 IlGetRxSLASpdlimit(void);
#endif

/* Handle:  331,Name:              ICM_TotalOdometer,Size: 20,UsedBytes:  3,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint32 IlGetRxICM_TotalOdometer(void);
#endif

/* Handle:  333,Name:                BMS_HV_BattVolt,Size: 10,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxBMS_HV_BattVolt(void);
#endif

/* Handle:  334,Name:                BMS_HV_BattCurr,Size: 14,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxBMS_HV_BattCurr(void);
#endif

/* Handle:  365,Name:                     VCU_VehSpd,Size: 13,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_RX
extern vuint16 IlGetRxVCU_VehSpd(void);
#endif



/* -----------------------------------------------------------------------------
    &&&~ Set Tx Signal Access for signals smaller or equal 8bit, SendType cyclic or none
 ----------------------------------------------------------------------------- */

/* Handle:    0,Name:            NavSpeedLimitStatus,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxNavSpeedLimitStatus(c) \
{ \
  IlEnterCriticalNavSpeedLimitStatus(); \
  IHU_ADAS_10.IHU_ADAS_10.NavSpeedLimitStatus = (c); \
  IlLeaveCriticalNavSpeedLimitStatus(); \
}
#endif

/* Handle:    1,Name:                  NavSpeedLimit,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxNavSpeedLimit(c) \
{ \
  IlEnterCriticalNavSpeedLimit(); \
  IHU_ADAS_10.IHU_ADAS_10.NavSpeedLimit = (c); \
  IlLeaveCriticalNavSpeedLimit(); \
}
#endif

/* Handle:    2,Name:             NavSpeedLimitUnits,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxNavSpeedLimitUnits(c) \
{ \
  IlEnterCriticalNavSpeedLimitUnits(); \
  IHU_ADAS_10.IHU_ADAS_10.NavSpeedLimitUnits = (c); \
  IlLeaveCriticalNavSpeedLimitUnits(); \
}
#endif

/* Handle:    3,Name:                NavCurrRoadType,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxNavCurrRoadType(c) \
{ \
  IlEnterCriticalNavCurrRoadType(); \
  IHU_ADAS_10.IHU_ADAS_10.NavCurrRoadType = (c); \
  IlLeaveCriticalNavCurrRoadType(); \
}
#endif

/* Handle:   38,Name:        IHU_MusicLoudness_120HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_120HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_120HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_120HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_120HZ(); \
}
#endif

/* Handle:   39,Name:    IHU_VoiceAssistantActiveSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_VoiceAssistantActiveSts(c) \
{ \
  IlEnterCriticalIHU_VoiceAssistantActiveSts(); \
  IHU_12.IHU_12.IHU_VoiceAssistantActiveSts = (c); \
  IlLeaveCriticalIHU_VoiceAssistantActiveSts(); \
}
#endif

/* Handle:   40,Name:        IHU_MusicLoudness_250HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_250HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_250HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_250HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_250HZ(); \
}
#endif

/* Handle:   41,Name:        IHU_MusicLoudness_500HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_500HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_500HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_500HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_500HZ(); \
}
#endif

/* Handle:   42,Name:       IHU_MusicLoudness_1000HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_1000HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_1000HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_1000HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_1000HZ(); \
}
#endif

/* Handle:   43,Name:       IHU_MusicLoudness_1500HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_1500HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_1500HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_1500HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_1500HZ(); \
}
#endif

/* Handle:   44,Name:       IHU_MusicLoudness_2000HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_2000HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_2000HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_2000HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_2000HZ(); \
}
#endif

/* Handle:   45,Name:       IHU_MusicLoudness_6000HZ,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MusicLoudness_6000HZ(c) \
{ \
  IlEnterCriticalIHU_MusicLoudness_6000HZ(); \
  IHU_12.IHU_12.IHU_MusicLoudness_6000HZ = (c); \
  IlLeaveCriticalIHU_MusicLoudness_6000HZ(); \
}
#endif

/* Handle:   46,Name:              IHU_GPS_Time_year,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_Time_year(c) \
{ \
  IlEnterCriticalIHU_GPS_Time_year(); \
  IHU_11.IHU_11.IHU_GPS_Time_year = (c); \
  IlLeaveCriticalIHU_GPS_Time_year(); \
}
#endif

/* Handle:   47,Name:              IHU_GPS_TimeSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_TimeSwSts(c) \
{ \
  IlEnterCriticalIHU_GPS_TimeSwSts(); \
  IHU_11.IHU_11.IHU_GPS_TimeSwSts = (c); \
  IlLeaveCriticalIHU_GPS_TimeSwSts(); \
}
#endif

/* Handle:   48,Name:             IHU_GPS_Time_Month,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_Time_Month(c) \
{ \
  IlEnterCriticalIHU_GPS_Time_Month(); \
  IHU_11.IHU_11.IHU_GPS_Time_Month = (c); \
  IlLeaveCriticalIHU_GPS_Time_Month(); \
}
#endif

/* Handle:   49,Name:               IHU_GPS_Time_Day,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_Time_Day(c) \
{ \
  IlEnterCriticalIHU_GPS_Time_Day(); \
  IHU_11.IHU_11.IHU_GPS_Time_Day = (c); \
  IlLeaveCriticalIHU_GPS_Time_Day(); \
}
#endif

/* Handle:   50,Name:              IHU_GPS_Time_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_Time_Hour(c) \
{ \
  IlEnterCriticalIHU_GPS_Time_Hour(); \
  IHU_11.IHU_11.IHU_GPS_Time_Hour = (c); \
  IlLeaveCriticalIHU_GPS_Time_Hour(); \
}
#endif

/* Handle:   51,Name:            IHU_GPS_Time_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_Time_Minute(c) \
{ \
  IlEnterCriticalIHU_GPS_Time_Minute(); \
  IHU_11.IHU_11.IHU_GPS_Time_Minute = (c); \
  IlLeaveCriticalIHU_GPS_Time_Minute(); \
}
#endif

/* Handle:   52,Name:            IHU_GPS_Time_Second,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_GPS_Time_Second(c) \
{ \
  IlEnterCriticalIHU_GPS_Time_Second(); \
  IHU_11.IHU_11.IHU_GPS_Time_Second = (c); \
  IlLeaveCriticalIHU_GPS_Time_Second(); \
}
#endif

/* Handle:  105,Name:            IHU_MirrorCtrlReq_L,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MirrorCtrlReq_L(c) \
{ \
  IlEnterCriticalIHU_MirrorCtrlReq_L(); \
  IHU_6.IHU_6.IHU_MirrorCtrlReq_L = (c); \
  IlLeaveCriticalIHU_MirrorCtrlReq_L(); \
}
#endif

/* Handle:  106,Name:            IHU_MirrorCtrlReq_R,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MirrorCtrlReq_R(c) \
{ \
  IlEnterCriticalIHU_MirrorCtrlReq_R(); \
  IHU_6.IHU_6.IHU_MirrorCtrlReq_R = (c); \
  IlLeaveCriticalIHU_MirrorCtrlReq_R(); \
}
#endif

/* Handle:  167,Name:       IHU_AccountCreatSts_Num1,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_AccountCreatSts_Num1(c) \
{ \
  IlEnterCriticalIHU_AccountCreatSts_Num1(); \
  IHU_15.IHU_15.IHU_AccountCreatSts_Num1 = (c); \
  IlLeaveCriticalIHU_AccountCreatSts_Num1(); \
}
#endif

/* Handle:  168,Name:       IHU_AccountCreatSts_Num2,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_AccountCreatSts_Num2(c) \
{ \
  IlEnterCriticalIHU_AccountCreatSts_Num2(); \
  IHU_15.IHU_15.IHU_AccountCreatSts_Num2 = (c); \
  IlLeaveCriticalIHU_AccountCreatSts_Num2(); \
}
#endif

/* Handle:  169,Name:       IHU_AccountCreatSts_Num3,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_AccountCreatSts_Num3(c) \
{ \
  IlEnterCriticalIHU_AccountCreatSts_Num3(); \
  IHU_15.IHU_15.IHU_AccountCreatSts_Num3 = (c); \
  IlLeaveCriticalIHU_AccountCreatSts_Num3(); \
}
#endif

/* Handle:  170,Name:   IHU_DriverFaceRecognitionSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_DriverFaceRecognitionSts(c) \
{ \
  IlEnterCriticalIHU_DriverFaceRecognitionSts(); \
  IHU_15.IHU_15.IHU_DriverFaceRecognitionSts = (c); \
  IlLeaveCriticalIHU_DriverFaceRecognitionSts(); \
}
#endif

/* Handle:  171,Name:         IHU_RadioFrequanceMode,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_RadioFrequanceMode(c) \
{ \
  IlEnterCriticalIHU_RadioFrequanceMode(); \
  IHU_14.IHU_14.IHU_RadioFrequanceMode = (c); \
  IlLeaveCriticalIHU_RadioFrequanceMode(); \
}
#endif

/* Handle:  172,Name:           IHU_RadioResearchSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_RadioResearchSts(c) \
{ \
  IlEnterCriticalIHU_RadioResearchSts(); \
  IHU_14.IHU_14.IHU_RadioResearchSts = (c); \
  IlLeaveCriticalIHU_RadioResearchSts(); \
}
#endif

/* Handle:  173,Name:          IHU_SourceStationMode,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_SourceStationMode(c) \
{ \
  IlEnterCriticalIHU_SourceStationMode(); \
  IHU_14.IHU_14.IHU_SourceStationMode = (c); \
  IlLeaveCriticalIHU_SourceStationMode(); \
}
#endif

/* Handle:  174,Name:                 IHU_PowerOnSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_PowerOnSts(c) \
{ \
  IlEnterCriticalIHU_PowerOnSts(); \
  IHU_14.IHU_14.IHU_PowerOnSts = (c); \
  IlLeaveCriticalIHU_PowerOnSts(); \
}
#endif

/* Handle:  177,Name:                IHU_RadioVolume,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_RadioVolume(c) \
{ \
  IlEnterCriticalIHU_RadioVolume(); \
  IHU_14.IHU_14.IHU_RadioVolume = (c); \
  IlLeaveCriticalIHU_RadioVolume(); \
}
#endif

/* Handle:  178,Name:                  IHU_EPB_SwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_EPB_SwSts(c) \
{ \
  IlEnterCriticalIHU_EPB_SwSts(); \
  IHU_1.IHU_1.IHU_EPB_SwSts = (c); \
  IlLeaveCriticalIHU_EPB_SwSts(); \
}
#endif

/* Handle:  186,Name: IHU_MFS_CruiseCtrlEnableButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_CruiseCtrlEnableButtonSts(c) \
{ \
  IlEnterCriticalIHU_MFS_CruiseCtrlEnableButtonSts(); \
  IHU_MFS.IHU_MFS.IHU_MFS_CruiseCtrlEnableButtonSts = (c); \
  IlLeaveCriticalIHU_MFS_CruiseCtrlEnableButtonSts(); \
}
#endif

/* Handle:  187,Name:  IHU_MFS_CruiseCancelButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_CruiseCancelButtonSts(c) \
{ \
  IlEnterCriticalIHU_MFS_CruiseCancelButtonSts(); \
  IHU_MFS.IHU_MFS.IHU_MFS_CruiseCancelButtonSts = (c); \
  IlLeaveCriticalIHU_MFS_CruiseCancelButtonSts(); \
}
#endif

/* Handle:  188,Name:     IHU_MFS_CruiseSetButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_CruiseSetButtonSts(c) \
{ \
  IlEnterCriticalIHU_MFS_CruiseSetButtonSts(); \
  IHU_MFS.IHU_MFS.IHU_MFS_CruiseSetButtonSts = (c); \
  IlLeaveCriticalIHU_MFS_CruiseSetButtonSts(); \
}
#endif

/* Handle:  189,Name:  IHU_MFS_CruiseResumeButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_CruiseResumeButtonSts(c) \
{ \
  IlEnterCriticalIHU_MFS_CruiseResumeButtonSts(); \
  IHU_MFS.IHU_MFS.IHU_MFS_CruiseResumeButtonSts = (c); \
  IlLeaveCriticalIHU_MFS_CruiseResumeButtonSts(); \
}
#endif

/* Handle:  190,Name:      IHU_MFS_SpdLimitButtonSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_SpdLimitButtonSts(c) \
{ \
  IlEnterCriticalIHU_MFS_SpdLimitButtonSts(); \
  IHU_MFS.IHU_MFS.IHU_MFS_SpdLimitButtonSts = (c); \
  IlLeaveCriticalIHU_MFS_SpdLimitButtonSts(); \
}
#endif

/* Handle:  191,Name:             IHU_MFS_ACC_GapSB1,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_ACC_GapSB1(c) \
{ \
  IlEnterCriticalIHU_MFS_ACC_GapSB1(); \
  IHU_MFS.IHU_MFS.IHU_MFS_ACC_GapSB1 = (c); \
  IlLeaveCriticalIHU_MFS_ACC_GapSB1(); \
}
#endif

/* Handle:  192,Name:              IHU_MFS_ACC_GapSB,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MFS_ACC_GapSB(c) \
{ \
  IlEnterCriticalIHU_MFS_ACC_GapSB(); \
  IHU_MFS.IHU_MFS.IHU_MFS_ACC_GapSB = (c); \
  IlLeaveCriticalIHU_MFS_ACC_GapSB(); \
}
#endif

/* Handle:  193,Name:        IHU_MsgAliveCounter_MFS,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_MsgAliveCounter_MFS(c) \
{ \
  IlEnterCriticalIHU_MsgAliveCounter_MFS(); \
  IHU_MFS.IHU_MFS.IHU_MsgAliveCounter_MFS = (c); \
  IlLeaveCriticalIHU_MsgAliveCounter_MFS(); \
}
#endif

/* Handle:  194,Name:           IHU_Checksum_CRC_MFS,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
#define IlPutTxIHU_Checksum_CRC_MFS(c) \
{ \
  IlEnterCriticalIHU_Checksum_CRC_MFS(); \
  IHU_MFS.IHU_MFS.IHU_Checksum_CRC_MFS = (c); \
  IlLeaveCriticalIHU_Checksum_CRC_MFS(); \
}
#endif



/* -----------------------------------------------------------------------------
    &&&~ Set Tx Signal Access extern decl
 ----------------------------------------------------------------------------- */

/* Handle:    4,Name:               IHU_11_FCWSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_FCWSwtSet(vuint8 sigData);
#endif

/* Handle:    5,Name:             IHU_11_FCWSnvtySet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_FCWSnvtySet(vuint8 sigData);
#endif

/* Handle:    6,Name:               IHU_11_AEBSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_AEBSwtSet(vuint8 sigData);
#endif

/* Handle:    7,Name:   IHU_11_SCF_request_confirmed,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_SCF_request_confirmed(vuint8 sigData);
#endif

/* Handle:    8,Name:               IHU_11_SCFSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_SCFSwtSet(vuint8 sigData);
#endif

/* Handle:    9,Name:      IHU_11_SLA_TSI_TLI_SwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_SLA_TSI_TLI_SwtSet(vuint8 sigData);
#endif

/* Handle:   10,Name:               IHU_11_LKASwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_LKASwtSet(vuint8 sigData);
#endif

/* Handle:   11,Name:               IHU_11_HMASwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_HMASwtSet(vuint8 sigData);
#endif

/* Handle:   12,Name:               IHU_11_IESSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_IESSwtSet(vuint8 sigData);
#endif

/* Handle:   13,Name:               IHU_11_DAISwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_DAISwtSet(vuint8 sigData);
#endif

/* Handle:   14,Name:               IHU_11_LDPSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_LDPSwtSet(vuint8 sigData);
#endif

/* Handle:   15,Name:           IHU_11_WarnModSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_WarnModSwtSet(vuint8 sigData);
#endif

/* Handle:   16,Name:               IHU_11_LDWSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_LDWSwtSet(vuint8 sigData);
#endif

/* Handle:   17,Name:               IHU_11_ELKSwtSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_ELKSwtSet(vuint8 sigData);
#endif

/* Handle:   18,Name:    IHU_11_LDWLDPSnvtySetLDWLDP,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_11_LDWLDPSnvtySetLDWLDP(vuint8 sigData);
#endif

/* Handle:   19,Name: IHU_AmbientLightBrightnessAdjust_Front,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightBrightnessAdjust_Front(vuint8 sigData);
#endif

/* Handle:   20,Name: IHU_AmbientLightBrightnessAdjust_Roof,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightBrightnessAdjust_Roof(vuint8 sigData);
#endif

/* Handle:   21,Name: IHU_AmbientLightBrightnessAdjust_Rear,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightBrightnessAdjust_Rear(vuint8 sigData);
#endif

/* Handle:   22,Name: IHU_AmbientLightColorSet_Front,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightColorSet_Front(vuint8 sigData);
#endif

/* Handle:   23,Name:  IHU_AmbientLightColorSet_Roof,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightColorSet_Roof(vuint8 sigData);
#endif

/* Handle:   24,Name:  IHU_AmbientLightColorSet_Rear,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightColorSet_Rear(vuint8 sigData);
#endif

/* Handle:   25,Name: IHU_RegionColorSynchronizationReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RegionColorSynchronizationReq(vuint8 sigData);
#endif

/* Handle:   26,Name:        IHU_AmbientLightModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightModeSet(vuint8 sigData);
#endif

/* Handle:   27,Name:    IHU_AmbientLightEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightEnableSwSts(vuint8 sigData);
#endif

/* Handle:   28,Name: IHU_AmbientLightAreaDisplayEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightAreaDisplayEnableSwSts(vuint8 sigData);
#endif

/* Handle:   29,Name: IHU_AmbientLightBreatheEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightBreatheEnableSwSts(vuint8 sigData);
#endif

/* Handle:   30,Name: IHU_AmbientLightBreatheWithVehSpdEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AmbientLightBreatheWithVehSpdEnableSwSts(vuint8 sigData);
#endif

/* Handle:   31,Name:      IHU_Chg_SOC_LimitPointSet,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_Chg_SOC_LimitPointSet(vuint8 sigData);
#endif

/* Handle:   32,Name:                 IHU_ChgModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ChgModeSet(vuint8 sigData);
#endif

/* Handle:   33,Name:   IHU_BookChgStartTimeSet_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BookChgStartTimeSet_Hour(vuint8 sigData);
#endif

/* Handle:   34,Name: IHU_BookChgStartTimeSet_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BookChgStartTimeSet_Minute(vuint8 sigData);
#endif

/* Handle:   35,Name:          IHU_BookChgTimeSet_VD,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BookChgTimeSet_VD(vuint8 sigData);
#endif

/* Handle:   36,Name:    IHU_BookChgStopTimeSet_Hour,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BookChgStopTimeSet_Hour(vuint8 sigData);
#endif

/* Handle:   37,Name:  IHU_BookchgStopTimeSet_Minute,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BookchgStopTimeSet_Minute(vuint8 sigData);
#endif

/* Handle:   53,Name:  IHU_RearWarnSystemEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RearWarnSystemEnableSwSts(vuint8 sigData);
#endif

/* Handle:   54,Name:              IHU_ICM_ThemelSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ICM_ThemelSet(vuint8 sigData);
#endif

/* Handle:   55,Name:              IHU_ICM_ScreenReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ICM_ScreenReq(vuint8 sigData);
#endif

/* Handle:   56,Name:            IHU_OverSpdValueSet,Size:  6,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_OverSpdValueSet(vuint8 sigData);
#endif

/* Handle:   57,Name:      IHU_ICM_Menu_OK_ButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ICM_Menu_OK_ButtonSts(vuint8 sigData);
#endif

/* Handle:   58,Name:          IHU_ICM_ModeButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ICM_ModeButtonSts(vuint8 sigData);
#endif

/* Handle:   59,Name:     IHU_FatigureDrivingTimeSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FatigureDrivingTimeSet(vuint8 sigData);
#endif

/* Handle:   60,Name:            IHU_ICM_UpButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ICM_UpButtonSts(vuint8 sigData);
#endif

/* Handle:   61,Name:          IHU_ICM_DownButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ICM_DownButtonSts(vuint8 sigData);
#endif

/* Handle:   62,Name:         IHU__ICM_LeftButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU__ICM_LeftButtonSts(vuint8 sigData);
#endif

/* Handle:   63,Name:        IHU__ICM_RightButtonSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU__ICM_RightButtonSts(vuint8 sigData);
#endif

/* Handle:   64,Name:    IHU_BrightnessAdjustSet_ICM,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BrightnessAdjustSet_ICM(vuint8 sigData);
#endif

/* Handle:   65,Name:          IHU_DVR_QuickCameraSw,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_DVR_QuickCameraSw(vuint8 sigData);
#endif

/* Handle:   66,Name:     IHU_SeatHeatLevelSetReq_RL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatHeatLevelSetReq_RL(vuint8 sigData);
#endif

/* Handle:   67,Name:     IHU_SeatHeatLevelSetReq_RR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatHeatLevelSetReq_RR(vuint8 sigData);
#endif

/* Handle:   68,Name: IHU_WirelessChargingEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_WirelessChargingEnableSwSts(vuint8 sigData);
#endif

/* Handle:   69,Name:             IHU_AVAS_VolumeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AVAS_VolumeSet(vuint8 sigData);
#endif

/* Handle:   70,Name:        IHU_AVAS_AudioSourceSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AVAS_AudioSourceSet(vuint8 sigData);
#endif

/* Handle:   71,Name:           IHU_AVAS_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AVAS_EnableSwSts(vuint8 sigData);
#endif

/* Handle:   72,Name:      IHU_SeatMemoryEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatMemoryEnableSwSts(vuint8 sigData);
#endif

/* Handle:   73,Name:       IHU_SeatPositionStoreReq,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatPositionStoreReq(vuint8 sigData);
#endif

/* Handle:   74,Name:     IHU_SeatPositionCalloutReq,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatPositionCalloutReq(vuint8 sigData);
#endif

/* Handle:   75,Name: IHU_SeatHeatVentLevelSetReq_Drive,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatHeatVentLevelSetReq_Drive(vuint8 sigData);
#endif

/* Handle:   76,Name: IHU_SeatHeatVentLevelSetReq_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SeatHeatVentLevelSetReq_Psngr(vuint8 sigData);
#endif

/* Handle:   77,Name:     IHU_FaceRecSeatEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FaceRecSeatEnableSwSts(vuint8 sigData);
#endif

/* Handle:   78,Name: IHU_FaceRecMirrorInclineEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FaceRecMirrorInclineEnableSwSts(vuint8 sigData);
#endif

/* Handle:   79,Name: IHU_FaceRecAuthenticationResult,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FaceRecAuthenticationResult(vuint8 sigData);
#endif

/* Handle:   80,Name:          IHU_AccountDeletedReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AccountDeletedReq(vuint8 sigData);
#endif

/* Handle:   81,Name: IHU_EntryExitSeatCtrlEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_EntryExitSeatCtrlEnableSwSts(vuint8 sigData);
#endif

/* Handle:   82,Name:                 IHU_AUTO_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AUTO_SwSts(vuint8 sigData);
#endif

/* Handle:   83,Name:                   IHU_AC_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AC_SwSts(vuint8 sigData);
#endif

/* Handle:   84,Name:                  IHU_PTC_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_PTC_SwSts(vuint8 sigData);
#endif

/* Handle:   85,Name:                  IHU_OFF_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_OFF_SwSts(vuint8 sigData);
#endif

/* Handle:   86,Name:              IHU_RearDfstSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RearDfstSwSts(vuint8 sigData);
#endif

/* Handle:   87,Name:         IHU_CirculationModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_CirculationModeSet(vuint8 sigData);
#endif

/* Handle:   88,Name:              IHU_AutoDfstSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AutoDfstSwSts(vuint8 sigData);
#endif

/* Handle:   89,Name:                IHU_BlowModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BlowModeSet(vuint8 sigData);
#endif

/* Handle:   90,Name:             IHU_TempSet_Driver,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_TempSet_Driver(vuint8 sigData);
#endif

/* Handle:   91,Name:             IHU_BlowerlevelSet,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BlowerlevelSet(vuint8 sigData);
#endif

/* Handle:   92,Name:        IHU_AnionGeneratorSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AnionGeneratorSwSts(vuint8 sigData);
#endif

/* Handle:   93,Name:               IHU_AQS_EnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AQS_EnablaSw(vuint8 sigData);
#endif

/* Handle:   94,Name:           IHU_AutoBlowEnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AutoBlowEnablaSw(vuint8 sigData);
#endif

/* Handle:   95,Name:      IHU_AC_MemoryModeEnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AC_MemoryModeEnablaSw(vuint8 sigData);
#endif

/* Handle:   96,Name:              IHU_DUAL_EnablaSw,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_DUAL_EnablaSw(vuint8 sigData);
#endif

/* Handle:   97,Name:          IHU_FrontDefrostSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FrontDefrostSwSts(vuint8 sigData);
#endif

/* Handle:   98,Name:           IHU_VentilationSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VentilationSwSts(vuint8 sigData);
#endif

/* Handle:   99,Name:                  IHU_ECO_SwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ECO_SwSts(vuint8 sigData);
#endif

/* Handle:  100,Name:              IHU_TempSet_Psngr,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_TempSet_Psngr(vuint8 sigData);
#endif

/* Handle:  101,Name:             IHU_LightCtrlSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_LightCtrlSwSts(vuint8 sigData);
#endif

/* Handle:  102,Name:            IHU_FrontWiperSwSts,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FrontWiperSwSts(vuint8 sigData);
#endif

/* Handle:  103,Name:          IHU_RearFogLightSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RearFogLightSwSts(vuint8 sigData);
#endif

/* Handle:  104,Name: IHU_MirrorReversePositionStoreReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MirrorReversePositionStoreReq(vuint8 sigData);
#endif

/* Handle:  107,Name:             IHU_RearWiperSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RearWiperSwSts(vuint8 sigData);
#endif

/* Handle:  108,Name:              IHU_RearWashSwSts,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RearWashSwSts(vuint8 sigData);
#endif

/* Handle:  109,Name:         IHU_InverterConfirmSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_InverterConfirmSts(vuint8 sigData);
#endif

/* Handle:  110,Name:        IHU_InverterEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_InverterEnableSwSts(vuint8 sigData);
#endif

/* Handle:  111,Name:           IHU_CruiseTypeSetSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_CruiseTypeSetSts(vuint8 sigData);
#endif

/* Handle:  112,Name:             IHU_VehWashModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VehWashModeSet(vuint8 sigData);
#endif

/* Handle:  113,Name:         IHU_SpeedLimitValueSet,Size:  5,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SpeedLimitValueSet(vuint8 sigData);
#endif

/* Handle:  114,Name:      IHU_SpeedLimitEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SpeedLimitEnableSwSts(vuint8 sigData);
#endif

/* Handle:  115,Name:       IHU_RegenerationLevelSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RegenerationLevelSet(vuint8 sigData);
#endif

/* Handle:  116,Name:               IHU_DriveModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_DriveModeSet(vuint8 sigData);
#endif

/* Handle:  117,Name:          IHU_OutputLimitSOCSet,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_OutputLimitSOCSet(vuint8 sigData);
#endif

/* Handle:  118,Name: IHU_ShiftVoiceRemind_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ShiftVoiceRemind_EnableSwSts(vuint8 sigData);
#endif

/* Handle:  119,Name: IHU_ShiftErrorActionRemindEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ShiftErrorActionRemindEnableSwSts(vuint8 sigData);
#endif

/* Handle:  120,Name:  IHU_SOC_ChargeLockEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SOC_ChargeLockEnableSwSts(vuint8 sigData);
#endif

/* Handle:  121,Name:        IHU_AutoLockEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AutoLockEnableSwSts(vuint8 sigData);
#endif

/* Handle:  122,Name:      IHU_AntiTheftModeSetSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AntiTheftModeSetSwSts(vuint8 sigData);
#endif

/* Handle:  123,Name:       IHU_SteeringWheelHeatReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SteeringWheelHeatReq(vuint8 sigData);
#endif

/* Handle:  124,Name:    IHU_MirrorFoldEnableSwSts__,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MirrorFoldEnableSwSts__(vuint8 sigData);
#endif

/* Handle:  125,Name:    IHU_FollowHomeMeEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FollowHomeMeEnableSwSts(vuint8 sigData);
#endif

/* Handle:  126,Name: IHU_SunroofAutoCloseEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SunroofAutoCloseEnableSwSts(vuint8 sigData);
#endif

/* Handle:  127,Name:    IHU_WelcomeLightEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_WelcomeLightEnableSwSts(vuint8 sigData);
#endif

/* Handle:  128,Name:             IHU_ComfortModeSet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ComfortModeSet(vuint8 sigData);
#endif

/* Handle:  129,Name:        IHU_WiperSensitivitySet,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_WiperSensitivitySet(vuint8 sigData);
#endif

/* Handle:  130,Name:              IHU_MirrorFoldReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MirrorFoldReq(vuint8 sigData);
#endif

/* Handle:  131,Name:            IHU_AHB_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AHB_EnableSwSts(vuint8 sigData);
#endif

/* Handle:  132,Name:         IHU_LowBeamAngleAdjust,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_LowBeamAngleAdjust(vuint8 sigData);
#endif

/* Handle:  133,Name: IHU_FrontWiperMaintenanceModeReq,Size:  1,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FrontWiperMaintenanceModeReq(vuint8 sigData);
#endif

/* Handle:  134,Name: IHU_EmergencyPowerOffReqConfirm,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_EmergencyPowerOffReqConfirm(vuint8 sigData);
#endif

/* Handle:  135,Name: IHU_MirrorReverseInclineEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MirrorReverseInclineEnableSwSts(vuint8 sigData);
#endif

/* Handle:  136,Name:     IHU_FollowMeHomeTimeSetSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FollowMeHomeTimeSetSts(vuint8 sigData);
#endif

/* Handle:  137,Name:  IHU_DoorWindowCtrlEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_DoorWindowCtrlEnableSwSts(vuint8 sigData);
#endif

/* Handle:  138,Name: IHU_WindowAutoCloseEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_WindowAutoCloseEnableSwSts(vuint8 sigData);
#endif

/* Handle:  139,Name: IHU_RKE_SunRoofCtrlEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_RKE_SunRoofCtrlEnableSwSts(vuint8 sigData);
#endif

/* Handle:  140,Name: IHU_DomeLightDoorCtrlSwitchSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_DomeLightDoorCtrlSwitchSts(vuint8 sigData);
#endif

/* Handle:  141,Name:             IHU_MaxPositionSet,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MaxPositionSet(vuint8 sigData);
#endif

/* Handle:  142,Name:       IHU_FragranceEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FragranceEnableSwSts(vuint8 sigData);
#endif

/* Handle:  143,Name:  IHU_FragranceConcentrationSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FragranceConcentrationSet(vuint8 sigData);
#endif

/* Handle:  144,Name:       IHU_FragrancePositionSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FragrancePositionSet(vuint8 sigData);
#endif

/* Handle:  145,Name:       IHU_VoiceCtrl_SunroofReq,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_SunroofReq(vuint8 sigData);
#endif

/* Handle:  146,Name:      IHU_VoiceCtrl_SunshadeReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_SunshadeReq(vuint8 sigData);
#endif

/* Handle:  147,Name:     IHU_VoiceCtrl_WindowReq_FL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_WindowReq_FL(vuint8 sigData);
#endif

/* Handle:  148,Name:     IHU_VoiceCtrl_WindowReq_FR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_WindowReq_FR(vuint8 sigData);
#endif

/* Handle:  149,Name:     IHU_VoiceCtrl_WindowReq_RL,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_WindowReq_RL(vuint8 sigData);
#endif

/* Handle:  150,Name:     IHU_VoiceCtrl_WindowReq_RR,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_WindowReq_RR(vuint8 sigData);
#endif

/* Handle:  151,Name:       IHU_VoiceCtrl_LowbeamReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_LowbeamReq(vuint8 sigData);
#endif

/* Handle:  152,Name:         IHU_VoiceCtrl_TrunkReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_VoiceCtrl_TrunkReq(vuint8 sigData);
#endif

/* Handle:  153,Name:             IHU_SmokingModeReq,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_SmokingModeReq(vuint8 sigData);
#endif

/* Handle:  154,Name:      IHU_MassageSwithSet_Psngr,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MassageSwithSet_Psngr(vuint8 sigData);
#endif

/* Handle:  155,Name:       IHU_MassageModeSet_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MassageModeSet_Psngr(vuint8 sigData);
#endif

/* Handle:  156,Name:   IHU_MassageStrengthSet_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MassageStrengthSet_Psngr(vuint8 sigData);
#endif

/* Handle:  157,Name: IHU_LumbarSupportModeSet_Psngr,Size:  3,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_LumbarSupportModeSet_Psngr(vuint8 sigData);
#endif

/* Handle:  158,Name: IHU_MotoPositionSet_ForwardBack_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_ForwardBack_Psngr(vuint8 sigData);
#endif

/* Handle:  159,Name: IHU_MotoPositionSet_SeatBack_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_SeatBack_Psngr(vuint8 sigData);
#endif

/* Handle:  160,Name:         IHU_17_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_17_MsgAliveCounter(vuint8 sigData);
#endif

/* Handle:  161,Name: IHU_MotoPositionSet_LegSupport_Psngr,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_LegSupport_Psngr(vuint8 sigData);
#endif

/* Handle:  162,Name: IHU_MotoPositionSet_ForwardBack_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_ForwardBack_Driver(vuint8 sigData);
#endif

/* Handle:  163,Name: IHU_MotoPositionSet_UpDown_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_UpDown_Driver(vuint8 sigData);
#endif

/* Handle:  164,Name: IHU_MotoPositionSet_SeatBack_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_SeatBack_Driver(vuint8 sigData);
#endif

/* Handle:  165,Name:         IHU_16_MsgAliveCounter,Size:  4,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_16_MsgAliveCounter(vuint8 sigData);
#endif

/* Handle:  166,Name: IHU_MotoPositionSet_LegSupport_Driver,Size:  8,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_MotoPositionSet_LegSupport_Driver(vuint8 sigData);
#endif

/* Handle:  175,Name:     IHU_FM_RadioFrequanceValue,Size: 16,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_FM_RadioFrequanceValue(vuint16 sigData);
#endif

/* Handle:  176,Name:     IHU_AM_RadioFrequanceValue,Size: 11,UsedBytes:  2,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AM_RadioFrequanceValue(vuint16 sigData);
#endif

/* Handle:  179,Name:               IHU_BrakemodeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_BrakemodeSet(vuint8 sigData);
#endif

/* Handle:  180,Name:                IHU_EPS_ModeSet,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_EPS_ModeSet(vuint8 sigData);
#endif

/* Handle:  181,Name:              IHU_ESC_OFF_SwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_ESC_OFF_SwSts(vuint8 sigData);
#endif

/* Handle:  182,Name:    IHU_CST_FunctionEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_CST_FunctionEnableSwSts(vuint8 sigData);
#endif

/* Handle:  183,Name:    IHU_HDC_FunctionEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_HDC_FunctionEnableSwSts(vuint8 sigData);
#endif

/* Handle:  184,Name:    IHU_AVH_FunctionEnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_AVH_FunctionEnableSwSts(vuint8 sigData);
#endif

/* Handle:  185,Name:            IHU_PDC_EnableSwSts,Size:  2,UsedBytes:  1,SingleSignal */
#ifdef IL_ENABLE_TX
extern void IlPutTxIHU_PDC_EnableSwSts(vuint8 sigData);
#endif




#endif /* __IL_PAR_H__ */
