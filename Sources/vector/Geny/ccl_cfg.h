/* -----------------------------------------------------------------------------
  Filename:    ccl_cfg.h
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: Ccl__core
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\WXWork\1688854678362950\Cache\File\2023-02\S61EV_FL(3).dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__CCL_CFG_H__)
#define __CCL_CFG_H__

#include "can_inc.h"
#include "il_inc.h"
#include "Nm.h"
#include "CanNm.h"
#include "NmCbdWrp.h"
/***************************************************************************/ 
/* Version                  (abcd: Main version ab Sub Version cd )        */ 
/***************************************************************************/ 

#define CCL_DLL_VERSION  0x0201
#define CCL_DLL_BUGFIX_VERSION  0x00
#define CCL__COREDLL_VERSION                 0x0311u
#define CCL__COREDLL_RELEASE_VERSION         0x36u
/***************************************************************************/ 
/* Defines                                                                 */ 
/***************************************************************************/ 

#define C_ENABLE_CCL 

#define CCL_DISABLE_ERROR_HOOK  /* CclFatalError function is disabled */ 

#define CCL_DISABLE_DEBUG /* disables the debug mode and switches the assertions to on */ 

#define CCL_DISABLE_EMC_WAKEUP

#define CCL_ENABLE_NMASR_WAKEUP_VALIDATION

#define CCL_DISABLE_WAKEUP_REG
#define CCL_ENABLE_INTERNAL_REQUEST
#define CCL_ENABLE_EXTERNAL_REQUEST
#define CCL_ENABLE_SLEEP_REPETITION

#define CCL_ENABLE_CANBEDDED_HANDLING
#define CCL_ENABLE_SCHEDULE_TASK
#define CCL_DISABLE_CONTAINER_TASK
#define CCL_DISABLE_STOP_MODE_ECU
#define CCL_DISABLE_POWER_DOWN_MODE_ECU
#define CCL_ENABLE_CUSTOMER_MODE_ECU
#define CCL_DISABLE_NET_STATE_RESTRICTION

#define CCL_ENABLE_TRCV_PORT_INT

#define CCL_ENABLE_TRCV_PORT_LEVEL_TRIGGER

#define CCL_ENABLE_SYSTEM_SHUTDOWN

#define CCL_ENABLE_SW_COM_STATE

#define CCL_ENABLE_MISSING_ACK_HANDLING

#define kCclNrOfSystemChannels 1

#define kCclNrOfChannels 1  /* number of used channels */

#define kCclNrOfNetworks 1  /* number of used networks */ 

#define CCL_DISABLE_MULTIPLE_NODES  /* no multiple nodes */ 

#define CCL_ENABLE_BUSOFF_START

#define CCL_ENABLE_BUSOFF_END

#define CCL_ENABLE_BUSSTART

#define kCclNetReqTableSize 1  /* size of network request tabless */
#define kCclNumberOfUser 2
#define kCclEmcWakeUpTime 380   /* value depends on the CCL cycle time */
#define kCclCycleTime 10


/* Communication Handles */
#define CCL_Com_Request0    1
/***************************************************************************/ 
/* Macros                                                                  */ 
/***************************************************************************/ 


  /* *** Communication Request access macros *** */
#define CclSet_Com_Request0()                             CclRequestCommunication(CCL_Com_Request0)


  /* *** Release Communication access macros *** */
#define CclRel_Com_Request0()                             CclReleaseCommunication(CCL_Com_Request0)

extern void CclComStart(void);
extern void CclComStop(void);
extern void CclComWait(void);
extern void CclComResume(void);
extern void CCL_API_CALLBACK_TYPE ApplCclComStart(void);
extern void CCL_API_CALLBACK_TYPE ApplCclComStop(void);
extern void CCL_API_CALLBACK_TYPE ApplCclComWait(void);
extern void CCL_API_CALLBACK_TYPE ApplCclComResume(void);
extern void CclBusOffStart(void);
extern void CclBusOffEnd(void);
extern void CCL_API_CALLBACK_TYPE ApplCclBusOffStart(void);
extern void CCL_API_CALLBACK_TYPE ApplCclBusOffEnd(void);
extern void CCL_API_CALLBACK_TYPE ApplCclInit(void);
extern void CCL_API_CALLBACK_TYPE ApplCclInitTrcv(void);
extern void CCL_API_CALLBACK_TYPE ApplCclWakeUpTrcv(void);
extern void CCL_API_CALLBACK_TYPE ApplCclStandbyTrcv(void);
extern void CCL_API_CALLBACK_TYPE ApplCclSleepTrcv(void);
extern void CCL_API_CALL_TYPE CclScheduleTask(void);

#endif /* __CCL_CFG_H__ */
