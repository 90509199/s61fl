/* -----------------------------------------------------------------------------
  Filename:    drv_par.c
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: GenTool_GenyDriverBase
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\Documents and Settings\90506667\����\S61EV_FL.dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#include "v_inc.h"
#include "can_inc.h"

#include "drv_par.h"
/* -----------------------------------------------------------------------------
    &&&~ Message Buffers
 ----------------------------------------------------------------------------- */

/* RAM CATEGORY 2 START */
/* PRQA S 0759 QAC_MessageBuffers */ /* MD_CBD_18.4 */
V_MEMRAM0 V_MEMRAM1 _c_IHU_1_buf V_MEMRAM2 IHU_1;
V_MEMRAM0 V_MEMRAM1 _c_IHU_3_buf V_MEMRAM2 IHU_3;
V_MEMRAM0 V_MEMRAM1 _c_IHU_4_buf V_MEMRAM2 IHU_4;
V_MEMRAM0 V_MEMRAM1 _c_IHU_5_buf V_MEMRAM2 IHU_5;
V_MEMRAM0 V_MEMRAM1 _c_IHU_6_buf V_MEMRAM2 IHU_6;
V_MEMRAM0 V_MEMRAM1 _c_IHU_7_buf V_MEMRAM2 IHU_7;
V_MEMRAM0 V_MEMRAM1 _c_IHU_8_buf V_MEMRAM2 IHU_8;
V_MEMRAM0 V_MEMRAM1 _c_IHU_10_buf V_MEMRAM2 IHU_10;
V_MEMRAM0 V_MEMRAM1 _c_IHU_11_buf V_MEMRAM2 IHU_11;
V_MEMRAM0 V_MEMRAM1 _c_IHU_12_buf V_MEMRAM2 IHU_12;
V_MEMRAM0 V_MEMRAM1 _c_IHU_13_buf V_MEMRAM2 IHU_13;
V_MEMRAM0 V_MEMRAM1 _c_IHU_14_buf V_MEMRAM2 IHU_14;
V_MEMRAM0 V_MEMRAM1 _c_IHU_15_buf V_MEMRAM2 IHU_15;
V_MEMRAM0 V_MEMRAM1 _c_NM_IHU_buf V_MEMRAM2 NM_IHU;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_MFS_1_buf V_MEMRAM2 CGW_BCM_MFS_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_ACCM_1_buf V_MEMRAM2 CGW_BCM_ACCM_1;
V_MEMRAM0 V_MEMRAM1 _c_AVAS_1_buf V_MEMRAM2 AVAS_1;
V_MEMRAM0 V_MEMRAM1 _c_WCM_1_buf V_MEMRAM2 WCM_1;
V_MEMRAM0 V_MEMRAM1 _c_TBOX_7_buf V_MEMRAM2 TBOX_7;
V_MEMRAM0 V_MEMRAM1 _c_CGW_VCU_P_5_buf V_MEMRAM2 CGW_VCU_P_5;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BMS_3_buf V_MEMRAM2 CGW_BMS_3;
V_MEMRAM0 V_MEMRAM1 _c_CGW_SAS_2_buf V_MEMRAM2 CGW_SAS_2;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCS_EPB_1_buf V_MEMRAM2 CGW_BCS_EPB_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_VCU_B_2_buf V_MEMRAM2 CGW_VCU_B_2;
V_MEMRAM0 V_MEMRAM1 _c_CGW_VCU_B_3_buf V_MEMRAM2 CGW_VCU_B_3;
V_MEMRAM0 V_MEMRAM1 _c_CGW_VCU_B_4_buf V_MEMRAM2 CGW_VCU_B_4;
V_MEMRAM0 V_MEMRAM1 _c_CGW_VCU_B_5_buf V_MEMRAM2 CGW_VCU_B_5;
V_MEMRAM0 V_MEMRAM1 _c_CGW_VCU_E_3_buf V_MEMRAM2 CGW_VCU_E_3;
V_MEMRAM0 V_MEMRAM1 _c_CGW_EGS_1_buf V_MEMRAM2 CGW_EGS_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_1_buf V_MEMRAM2 CGW_BCM_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_PEPS_1_buf V_MEMRAM2 CGW_BCM_PEPS_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_4_buf V_MEMRAM2 CGW_BCM_4;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_5_buf V_MEMRAM2 CGW_BCM_5;
V_MEMRAM0 V_MEMRAM1 _c_BCM_6_buf V_MEMRAM2 BCM_6;
V_MEMRAM0 V_MEMRAM1 _c_BCM_TPMS_1_buf V_MEMRAM2 BCM_TPMS_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_TPMS_2_buf V_MEMRAM2 CGW_BCM_TPMS_2;
V_MEMRAM0 V_MEMRAM1 _c_ICM_1_buf V_MEMRAM2 ICM_1;
V_MEMRAM0 V_MEMRAM1 _c_RCM_1_buf V_MEMRAM2 RCM_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_SCM_1_buf V_MEMRAM2 CGW_BCM_SCM_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_SCM_2_buf V_MEMRAM2 CGW_BCM_SCM_2;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BSD_1_buf V_MEMRAM2 CGW_BSD_1;
V_MEMRAM0 V_MEMRAM1 _c_IHU_16_buf V_MEMRAM2 IHU_16;
V_MEMRAM0 V_MEMRAM1 _c_IHU_17_buf V_MEMRAM2 IHU_17;
V_MEMRAM0 V_MEMRAM1 _c_IHU_18_buf V_MEMRAM2 IHU_18;
V_MEMRAM0 V_MEMRAM1 _c_IHU_ADAS_10_buf V_MEMRAM2 IHU_ADAS_10;
V_MEMRAM0 V_MEMRAM1 _c_IHU_ADAS_11_buf V_MEMRAM2 IHU_ADAS_11;
V_MEMRAM0 V_MEMRAM1 _c_IHU_MFS_buf V_MEMRAM2 IHU_MFS;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BMS_9_buf V_MEMRAM2 CGW_BMS_9;
V_MEMRAM0 V_MEMRAM1 _c_CGW_OBC_1_buf V_MEMRAM2 CGW_OBC_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCS_C_1_buf V_MEMRAM2 CGW_BCS_C_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_EPS_1_buf V_MEMRAM2 CGW_EPS_1;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_2_buf V_MEMRAM2 CGW_FCM_2;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_5_buf V_MEMRAM2 CGW_FCM_5;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_6_buf V_MEMRAM2 CGW_FCM_6;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_7_buf V_MEMRAM2 CGW_FCM_7;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_10_buf V_MEMRAM2 CGW_FCM_10;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_FRM_5_buf V_MEMRAM2 CGW_FCM_FRM_5;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_FRM_6_buf V_MEMRAM2 CGW_FCM_FRM_6;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_FRM_8_buf V_MEMRAM2 CGW_FCM_FRM_8;
V_MEMRAM0 V_MEMRAM1 _c_CGW_FCM_FRM_9_buf V_MEMRAM2 CGW_FCM_FRM_9;
V_MEMRAM0 V_MEMRAM1 _c_BCM_ALM_buf V_MEMRAM2 BCM_ALM;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_FM_buf V_MEMRAM2 CGW_BCM_FM;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_SCM_3_buf V_MEMRAM2 CGW_BCM_SCM_3;
V_MEMRAM0 V_MEMRAM1 _c_CGW_BCM_ACCM_3_buf V_MEMRAM2 CGW_BCM_ACCM_3;
V_MEMRAM0 V_MEMRAM1 _c_CGW_PLG_1_buf V_MEMRAM2 CGW_PLG_1;
V_MEMRAM0 V_MEMRAM1 _c_ICM_2_buf V_MEMRAM2 ICM_2;
/* PRQA L:QAC_MessageBuffers */

/* RAM CATEGORY 2 END */





