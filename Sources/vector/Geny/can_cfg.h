/* -----------------------------------------------------------------------------
  Filename:    can_cfg.h
  Description: Toolversion: 02.03.34.02.10.01.18.00.00.00
               
               Serial Number: CBD2100118
               Customer Info: Wuhu Mengbo Technology
                              Package: CBD_Vector_SLP2
                              Micro: Freescale S32K144
                              Compiler: Iar 8.50.9
               
               
               Generator Fwk   : GENy 
               Generator Module: DrvCan__base
               
               Configuration   : E:\sh52\Geny\GENy_SH52_MR.gny
               
               ECU: 
                       TargetSystem: Hw_S32Cpu
                       Compiler:     IAR
                       Derivates:    S32K144
               
               Channel "Channel0":
                       Databasefile: D:\Documents and Settings\90506667\桌面\sh52\新建文件夹\S61EV_FLV1.4_2023_03_04.dbc
                       Bussystem:    CAN
                       Manufacturer: Vector
                       Node:         IHU

 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2015 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__CAN_CFG_H__)
#define __CAN_CFG_H__

#include "v_cfg.h"
#define HW_IMXFLEXCAN3CPUCANDLL_VERSION      0x0204u
#define HW_IMXFLEXCAN3CPUCANDLL_RELEASE_VERSION 0x01u

#define HW__BASECPUCANDLL_VERSION            0x0303u
#define HW__BASECPUCANDLL_RELEASE_VERSION    0x03u

#define DRVCAN__BASEDLL_VERSION              0x0327u
#define DRVCAN__BASEDLL_RELEASE_VERSION      0x00u

#define DRVCAN__BASERI14DLL_VERSION          0x0209u
#define DRVCAN__BASERI14DLL_RELEASE_VERSION  0x02u

#define DRVCAN__BASERI15DLL_VERSION          0x0110u
#define DRVCAN__BASERI15DLL_RELEASE_VERSION  0x00u

#define DRVCAN__BASERI20DLL_VERSION          0x0100u
#define DRVCAN__BASERI20DLL_RELEASE_VERSION  0xFFu

#define DRVCAN__BASEHLLDLL_VERSION           0x0308u
#define DRVCAN__BASEHLLDLL_RELEASE_VERSION   0x00u

#define DRVCAN__BASERI14HLLDLL_VERSION       0x0209u
#define DRVCAN__BASERI14HLLDLL_RELEASE_VERSION 0x00u

#define DRVCAN__BASERI15HLLDLL_VERSION       0x0104u
#define DRVCAN__BASERI15HLLDLL_RELEASE_VERSION 0x00u

#define DRVCAN__BASERI20HLLDLL_VERSION       0x0103u
#define DRVCAN__BASERI20HLLDLL_RELEASE_VERSION 0x03u

#define CAN_DRV_IMXDLL_VERSION               0x0204u
#define CAN_DRV_IMXDLL_RELEASE_VERSION       0x01u


#define kCanNumberOfChannels                 1u
#define kCanNumberOfHwChannels               1u
#define kCanNumberOfPhysChannels             1u
#define C_DISABLE_MEMCOPY_SUPPORT
#define C_DISABLE_OSEK_OS
#define C_DISABLE_VARIABLE_DLC
#define C_DISABLE_DLC_FAILED_FCT
#define C_DISABLE_VARIABLE_RX_DATALEN
#define C_DISABLE_MULTI_ECU_CONFIG
#define C_DISABLE_MULTI_ECU_PHYS
#define C_DISABLE_EXTENDED_ID
#define C_DISABLE_MIXED_ID
#define C_DISABLE_RECEIVE_FCT

#define C_DISABLE_ECU_SWITCH_PASS
#define C_ENABLE_TRANSMIT_QUEUE
#define C_DISABLE_OVERRUN
#define C_DISABLE_INTCTRL_BY_APPL
#define C_DISABLE_COMMON_CAN
#define C_DISABLE_USER_CHECK
#define C_DISABLE_HARDWARE_CHECK
#define C_DISABLE_GEN_CHECK
#define C_DISABLE_INTERNAL_CHECK
#define C_DISABLE_DYN_RX_OBJECTS
#define C_DISABLE_DYN_TX_OBJECTS
#define C_DISABLE_DYN_TX_ID
#define C_DISABLE_DYN_TX_DLC
#define C_DISABLE_DYN_TX_DATAPTR
#define C_DISABLE_DYN_TX_PRETRANS_FCT
#define C_DISABLE_DYN_TX_CONF_FCT
#define C_DISABLE_EXTENDED_STATUS
#define C_ENABLE_TX_OBSERVE
#define C_DISABLE_HW_LOOP_TIMER
#define C_DISABLE_NOT_MATCHED_FCT
#define C_SECURITY_LEVEL                     30

#define C_DISABLE_MULTICHANNEL_API
#define C_DISABLE_PART_OFFLINE
#define C_ENABLE_RANGE_0
#define C_DISABLE_RANGE_1
#define C_DISABLE_RANGE_2
#define C_DISABLE_RANGE_3
#define ApplCanBusOff                        CbdWrpBusOff

#define ApplCanRange0Precopy                 CanNm_NmMsgPrecopy
#define kCanNumberOfTxObjects                20u
#define kCanNumberOfTxStatObjects            20u
#define kCanNumberOfTxDynObjects             0u
#define kCanNumberOfRxObjects                46u
#define kCanNumberOfRxStatFullCANObjects     23u
#define kCanNumberOfRxStatBasicCANObjects    23u
#define kCanNumberOfRxDynFullCANObjects      0u
#define kCanNumberOfRxDynBasicCANObjects     0u
#define kCanNumberOfRxDynObjects             0u
#define kCanNumberOfRxStatObjects            46u
#define kCanNumberOfConfFlags                19u
#define kCanNumberOfIndFlags                 0u
#define kCanNumberOfConfirmationFlags        3u
#define kCanNumberOfIndicationFlags          0u
#define kCanNumberOfInitObjects              1u
#define kCanExtNumberOfInitObjects           0
#define kCanHwRxDynFullStartIndex            31
#define C_SEARCH_LINEAR

#define C_ENABLE_RX_MSG_INDIRECTION

#define C_ENABLE_CONFIRMATION_FLAG
#define C_DISABLE_INDICATION_FLAG
#define C_DISABLE_PRETRANSMIT_FCT
#define C_ENABLE_CONFIRMATION_FCT
#define C_DISABLE_INDICATION_FCT
#define C_DISABLE_PRECOPY_FCT
#define C_ENABLE_COPY_TX_DATA
#define C_ENABLE_COPY_RX_DATA
#define C_ENABLE_DLC_CHECK
#define C_DISABLE_DLC_CHECK_MIN_DATALEN

#define C_ENABLE_GENERIC_PRECOPY
#define APPL_CAN_GENERIC_PRECOPY             IlCanGenericPrecopy

#define C_SEND_GRP_NONE                      0x00u
#define C_SEND_GRP_ALL                       0xFFu
#define C_SEND_GRP_USER0                     0x01u
#define C_SEND_GRP_USER1                     0x02u
#define C_SEND_GRP_USER2                     0x04u
#define C_SEND_GRP_USER3                     0x08u
#define C_SEND_GRP_USER4                     0x10u
#define C_SEND_GRP_USER5                     0x20u
#define C_SEND_GRP_USER6                     0x40u
#define C_SEND_GRP_USER7                     0x80u
#define C_ENABLE_CAN_CANCEL_NOTIFICATION
#define APPL_CAN_CANCELNOTIFICATION          IlCanCancelNotification

#define kCanPhysToLogChannelIndex_0
#define C_RANGE0_ACC_MASK                    0x07Fu

#define C_RANGE0_ACC_CODE                    0x600u

#define C_ENABLE_RX_FULLCAN_OBJECTS
#define C_ENABLE_RX_BASICCAN_OBJECTS
#define kCanNumberOfRxFullCANObjects         23u
#define kCanNumberOfRxFullMailboxes          23u

#define kCanNumberOfRxBasicCANObjects        23u
#define kCanNumberOfRxBasicMailboxes         1u

#define kCanInitObj1                         0
#define C_DISABLE_TX_MASK_EXT_ID
#define C_DISABLE_RX_MASK_EXT_ID
#define C_MASK_EXT_ID                        0xFFFFFFFFu

#define C_ENABLE_CAN_CAN_INTERRUPT_CONTROL
#define C_DISABLE_CAN_TX_CONF_MSG_ACCESS
#define C_DISABLE_CAN_TX_CONF_FCT

#define C_DISABLE_TX_POLLING
#define C_DISABLE_RX_BASICCAN_POLLING
#define C_DISABLE_RX_FULLCAN_POLLING
#define C_DISABLE_ERROR_POLLING
#define C_DISABLE_WAKEUP_POLLING
#define C_DISABLE_FULLCAN_OVERRUN
#define C_DISABLE_OSEK_OS_INTCAT2
#define C_DISABLE_COPY_RX_DATA_WITH_DLC
#define kCanTxQueueBytes                     4u
#define kCanNumberOfMaxBasicCAN              8u
#define kCanNumberOfHwObjPerBasicCan         1u
#define C_DISABLE_CAN_RAM_CHECK
#define C_ENABLE_SLEEP_WAKEUP
#define C_DISABLE_CANCEL_IN_HW
#define C_DISABLE_ONLINE_OFFLINE_CALLBACK_FCT

#define C_RANGE0_IDTYPE                      kCanIdTypeStd
#define kCanChannel_Channel0                 0
#define C_DISABLE_INTCTRL_ADD_CAN_FCT
#if defined(C_SINGLE_RECEIVE_BUFFER) || defined(C_MULTIPLE_RECEIVE_BUFFER)
#error "DrvCan__baseRI1.5 doesn't support Single/Multiple Receive Buffer API for the callback 'ApplCanMsgReceived'!"
#endif

#define C_DISABLE_RETRANSMIT
#define kCanNumberOfUsedCanTxIdTables        1u
#define kCanNumberOfUsedCanRxIdTables        1u
#define kCanNumberOfTxMailboxes              1u

#define kCanNumberOfUnusedMailboxes          0u

#define kCanNumberOfTxDirectObjects          0u

#define C_DISABLE_TX_FULLCAN_OBJECTS


/* -----------------------------------------------------------------------------
    &&&~ RI20
 ----------------------------------------------------------------------------- */

#define C_DISABLE_CAN_FD_USED
#define C_DISABLE_CAN_FD_FULL
#define kCanMaxRxMailboxDataLen              8u
#define kCanMaxTxMailboxDataLen              8u
#define kCanNumberOfMailboxes                25u
#define kCanNumberOfHwObjToMailboxIndirections 32u
#define kCanMailboxTxNormalIndex             24u
#define kCanMailboxTxStartIndex              24u
#define kCanMailboxRxFullStartIndex          1u
#define kCanMailboxRxBasicStartIndex         0u
#define kCanMailboxUnusedStartIndex          kCanMailboxNotUsed
#define kCanHwObjStartIndex                  0u
#define kCanHwObjToMailboxIndirectionStartIndex 0u
#define C_CAN_FD_USAGE                       C_CAN_FD_NONE
#define C_DISABLE_CAN_FD_HW_BUFFER_OPTIMIZATION
#define C_DISABLE_NESTED_INTERRUPTS
#define C_DISABLE_MULTI_ECU_RELOCATE_HWOBJ


#define C_DISABLE_INDIVIDUAL_POLLING
#define kCanBaseAddress                      0x40024000u
#define kCanIntMask1                         0xFFFFFF01u
#define kCanRxBasicPollingMask1              0x00u
#define kCanRxFullPollingMask1               0x00u
#define kCanTxPollingMask1                   0x00u
#define kCanNumberOfFilters                  0x08u
#define kCanRFFN                             0x00u
#define kCanNumberOfFullConfigurableFilters  0x08u
#define kCanNumberOfMaxMailboxes             0x20u
#define kCanNumberOfTotalFilters             8
#define C_ENABLE_INTERRUPT
#define C_DISABLE_TASD
#define C_DISABLE_GLITCH_FILTER
#define C_DISABLE_FLEXCAN_STOP_MODE
#define C_DISABLE_WORKAROUND_ERR005829
#define C_DISABLE_FLEXCAN_PARITY_CHECK_AVAILABLE
#define C_DISABLE_FLEXCAN2_DERIVATIVE
#define C_DISABLE_MB32TO63
#define C_DISABLE_MB64TO95
#define C_DISABLE_MB96TO127
#define C_ENABLE_ASYM_MAILBOXES
#define C_DISABLE_EXTENDED_BITTIMING



#endif /* __CAN_CFG_H__ */
