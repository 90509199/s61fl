/* STARTSINGLE_OF_MULTIPLE */

#ifndef _CAN_INC_H
# define _CAN_INC_H /* PRQA S 0602, 0603 */ /* MD_Can_ModuleDefine */

/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*  \file         can_inc.h
 *  \brief        Definition of can driver include files
 *  \details      Please adapt such that the compiler finds the following files: can_cfg.h, can_def.h
 *
 *********************************************************************************************************************/

/* ***************************************************************************
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     ------------------------------------
| Ht           Heike Honert              Vector Informatik GmbH
| Tkr          Torsten Kercher           Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2001-05-08  1.00.00   Ht      - creation
| 2003-01-28  1.01.00   Ht      - protection from multiple include of this file
| 2006-03-23  1.02.00   Ht      - include osek.h before can_def.h due to ISR-macro
|                               - remove include of nm_cfg.h
| 2007-03-05  1.03.00   Ht      - include v_par.h to support multiple ECU
| 2008-10-29  1.04.00   Ht      - include cfg-files of higher layers
| 2010-06-21  1.05.00   Ht      - support OSEK-OS
| 2019-06-25  1.06.00   Tkr     - ESCAN00103506: Support MISRA2012
|************************************************************************** */

# include "can_cfg.h"              /* configuration file of the CAN driver  */

# if defined( VGEN_ENABLE_TP_VW20 )
#  include "tp20vcfg.h"            /* configuration file of transport layer */
# endif
 
# if defined ( VGEN_ENABLE_NM_OSEK_D )
#  include "nm_cfg.h"              /* configuration file of NM DirOsek      */
# endif
 
# if defined ( VGEN_ENABLE_NMHIGH )
#  include "nmh_cfg.h"             /* configuration file of NM High         */
# endif

# if defined( C_ENABLE_OSEK_OS )
#  if defined(V_OSTYPE_OSEK)
#   include "osek.h"               /* include of OSEK-header                */
#  endif
#  if defined(V_OSTYPE_AUTOSAR)
#   include "Os.h"
#  endif
# endif

# if defined( VGEN_GENY )
#  include "v_par.h"               /* some vector internal defines          */
# endif

# include "can_def.h"              /* CAN driver header                     */


/* include Register header of used controller derivative */

# endif /* _CAN_INC_H */

/* STOPSINGLE_OF_MULTIPLE */
