/* Kernbauer Version: 1.14 Konfiguration: DrvCan_Arm32Flexcan3Hll Erzeugungsgangnummer: 1 */

/* STARTSINGLE_OF_MULTIPLE */


/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/** Project Name: DrvCan_Arm32Flexcan3Hll
 *
 *  \file     CAN_DRV.C
 *  \brief    Implementation of the CAN driver
 *  \details  see functional description below
 *
 *********************************************************************************************************************/
/* ***************************************************************************
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Ht           Heike Honert              Vector Informatik GmbH
| Pl           Georg Pfluegel            Vector Informatik GmbH
| Bir          Holger Birke              Vector Informatik GmbH
| Rse          Robert Schelkle           Vector Informatik GmbH
| Rli          Roman Linder              Vector Informatik GmbH
| Yoe          Yacine Ould Boukhitine    Vector Informatik GmbH
| Bns          Benjamin Schuetterle      Vector Informatik GmbH
| Gaz          Carsten Gauglitz          Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date       Ver   Author  Description
| ---------  ---  ------  ----------------------------------------------------
| 2012-01-09  1.00.00  Rse    - ESCAN00056163: Initial version: Hll - Flexcan3 - Imx6 - QCC - High-End - QNX
| 2012-09-20  1.01.00  Rse    - ESCAN00062414: Support workaround for erratum ERR005829
| 2013-01-30  1.02.00  Rse    - ESCAN00065347: Support platform MPC5700
|                             - ESCAN00064665: CANbedded only: CAN messages will not be sent any more
|                             - ESCAN00066667: Support workaround for erratum e5769
|                             - ESCAN00067035: Inconsistent combination of receive handle and receive information (id, dlc, data) is indicated to upper layer
| 2013-05-21  1.03.00  Rse    - ESCAN00067478: Support FlexCAN2 derivatives for RxFifo usage
|                             - ESCAN00067660: Support the feature Mirror Mode
| 2013-07-22  1.04.00  Rse    - ESCAN00069229: Support CAN message retransmit feature for FBL driver
|                             - ESCAN00070121: Support Vybrid with FlexCAN3
| 2013-11-21  1.05.00  Rse    - ESCAN00072129: ASR4 only: change BusOff behavior to ensure recovery time of 128x11 recessive bits
|                             - ESCAN00072322: AR4-328: Predefined Runtime Measurement Points
| 2013-12-09  1.06.00  Rse    - ESCAN00072515: AutoSar only: Can_SetControllerMode(controller, CAN_T_STOP) returns CAN_OK instead of CAN_NOT_OK
|                             - ESCAN00072597: CANbedded only: Retransmit does not work if more than 22 RxFullCAN objects are used
| 2014-01-08  1.07.00  Rse    - ESCAN00072828: CANbedded only: Compiler error: identifier "canCanInterruptCounter" is undefined
|                             - ESCAN00072863: Support Cancel in Hardware with Transmission Abort feature
|                             - ESCAN00072869: Support new interface for Bus Mirroring
| 2014-01-30  1.07.01  Rse    - ESCAN00073299: Parity check only available for Vybrid and MPC5700 derivatives
| 2014-02-06  1.07.02  Rse    - Some minor MISRA improvements
|                      Rse    - ESCAN00073889: CANbedded only: Compiler error: struct has unknown field
| 2014-03-24  1.07.03  Rse    - ESCAN00074424: Indication function is called twice for the reception of one BasicCAN message
| 2014-04-16  1.08.00  Rse    - ESCAN00075110: BusOff notification is called twice for one single BusOff event (FlexCAN2)
|                             - ESCAN00075955: Remove obsolete workaround implementation for e5769
|                             - ESCAN00075964: Delete unnecessary interrupt locks
|                             - ESCAN00075507: IAR Compiler warning: the order of volatile accesses is undefined
| 2014-06-24  1.09.00  Rse    - ESCAN00076588: CAN clock settings are invalid after reinitialization, error frames occur continuously on the bus
|                             - ESCAN00076653: Implement proper mode switch to init (FREEZE) mode in CAN controller initialization process
|                             - ESCAN00076657: MAXMB value is obsolete
|                             - ESCAN00077049: CANbedded only: Support Tx FullCAN Send Delay (External PIA)
|                             - ESCAN00077083: CANbedded only: LowLevel Message is not transmitted
|                             - ESCAN00077485: OS Exception when CAN driver is handled in User Mode
| 2014-08-13  1.10.00  Rse    - ESCAN00077825: Support derivative MPC5748G CUT2
| 2014-11-10  1.10.01  Rse    - ESCAN00079385: Compiler warning: argument of type "volatile uint32 *" is incompatible with parameter of type "volatile uint16 *"
| 2014-11-27  2.00.00  Rse    - ESCAN00078072: Support full functionality for CAN-FD (Mode 2)
|                             - ESCAN00079900: CANbedded only: Extended ID message with dominant SRR-Bit does not receive
|                             - ESCAN00079901: Support ComStackLib tables for ASR403 R11
|                             - ESCAN00079494: Add loop checks for mode switching in CAN Controller initialization process
| 2015-01-21  2.00.01  Rse    - ESCAN00080716: Standard CAN message is sent as CAN-FD message with baudrate switch
| 2015-04-09  2.00.02  Was    - ESCAN00082335: Update to Core 5.00.02, R11 (Vybrid)
| 2015-05-02  2.01.00  Rse    - ESCAN00082800: Support CANbedded compatibility
| 2015-09-02  2.02.00  Zam    - ESCAN00084930: Support Extended CAN RAM Check feature
| 2015-10-14  2.03.00  Pl     - ESCAN00085860: Support S32K with FlexCAN3
| 2015-12-03  3.00.00  Rse    - ESCAN00086974: Support R14 and process 3.0
| 2016-03-17  3.01.00  Rse    - ESCAN00088969: Support Extended RAM check for MSR403
| 2016-05-30  3.01.01  Rse    - ESCAN00090210: Compiler error: parse error near ';'
| 2016-06-20  3.01.02  Rse    - Update HL-ASR 5.04.01 for issue 89754
| 2016-08-03  3.01.03  Rse    - ESCAN00091088: Callback that indicates corrupt CAN Controller is called erroneously
| 2016-08-25  3.02.00  Rse    - ESCAN00091626: Support R14 for Arm32 platforms
|                             - ESCAN00092139: Support ISO CAN-FD
|                             - ESCAN00092218: Support possibility to use a non-CAN-FD baudrate on a CAN-FD channel
|                             - ESCAN00092333: Support asymmetric mailboxes
| 2016-10-26  3.02.01  Rse    - ESCAN00092531: Linker Error: undefined symbol found 'Can_GetCanNumberOfMaxMailboxes'
| 2016-12-12  3.02.02  Rli    - ESCAN00093255: CAN FD FULL support for Little Endian Architectures
| 2017-03-30  3.02.03  Rli    - ESCAN00094552: Compiler error: identifier Controller not declared
| 2017-04-11  4.00.00  Rse    - ESCAN00094734: Beta support of RI2.0
| 2017-11-30  4.00.01  yoe    - ESCAN00097612: Beta support of RI2.0 for Arm32Flexcan3
| 2018-02-26  4.01.00  Rli    - ESCAN00098508: Final support of RI2.0 for Arm32Flexcan3
| 2018-03-07  4.01.01  Rli    - ESCAN00098514: add MISRA relevant changes
| 2018-04-27  4.02.00  Rli    - ESCAN00099193: Final support of RI2.0 for Mpc5700Flexcan3
| 2017-10-31  5.00.00  Rli    - ESCAN00097302 Support of SafeBSW for FlexCAN3
| 2018-03-08  5.00.01  Bns    - Update HL-ASR 5.07.02 for issue 96367
|                             - ESCAN00098678 Support Extended RAM Check for S32
| 2018-03-12  5.01.00  Gaz    - ESCAN00098459 Update to Core 6.00.00
| 2018-06-19  5.02.00  Rli    - ESCAN00099727 Support of Virtual Addressing with CAN FD Mode 2
| 2018-08-02  5.03.00  Rli    - STORYC-6186: Support Silent Mode
|                             - ESCAN00100316 IMX8: Messages not received with CAN FD configuration
| 2018-10-19  5.04.00  Yoe    - STORYC-6683: Support SafeBsw for Asymmetric Mailboxes
| 2018-10-31  5.04.01  Yoe    - STORYC-6877: Support SafeBsw for S32k1xx derivatives
| 2018-11-22  5.04.02  Rli    - STORYC-7138: Update code due to Cfg5 bugfix
| 2019-01-16  5.04.03  Rli    - ESCAN00101634: FlexCAN3 e10368: Transition of the CAN FD operation enable bit may lead FlexCAN logic to an inconsistent state
| 2019-02-02  6.00.00  Rli    - ESCAN00101108: RxFifo support for FlexCAN2 removed
|                             - STORYC-6864: Support AR4-R21 and update to new HighLevel interface
|                             - STORYC-6865: Mirror Mode support with FD  inclusively
|                             - STORYC-6866: Virtual Addressing support with FD mode 2 inclusively
|                             - STORYC-6867: Silent Mode support for baudrate detection
|                             - STORYC-7901: Support AR4-R22
| 2019-05-20  6.00.01  Rli    - STORYC-8800: Support IMX ASIL and Extended Ramcheck
| 2019-07-17  6.01.00  Bns    - STORYC-8855: Support Extended Ramcheck for canbedded
|************************************************************************** */
/* ***************************************************************************
|
|    ************    Version and change information of      **********
|    ************    high level part only                   **********
|
|    Please find the version number of the whole module in the previous
|    file header.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Ht           Heike Honert              Vector Informatik GmbH
| Pl           Georg Pfluegel            Vector Informatik GmbH
| Vg           Frank Voorburg            Vector CANtech, Inc.
| An           Ahmad Nasser              Vector CANtech, Inc.
| Ml           Patrick Markl             Vector Informatik GmbH
| Seu          Eugen Stripling           Vector Informatik GmbH
| Uce          Cengiz Uenver             Vector Informatik GmbH
| Tkr          Torsten Kercher           Vector Informatik GmbH
| Jan          Jan Hammer                Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date       Ver  Author  Description
| ---------  ---  ------  ----------------------------------------------------
| 19-Jan-01  0.02     Ht  - derived form C16x V3.3
| 18-Apr-01  1.00     Pl  - derived for ARM7 TDMI
| 02-May-01  1.01     Ht  - adaption to LI1.2
|                         - change from code doupling to indexed
| 31-Oct-01  1.02     Ht  - support hash search
|                     Ht  - optimisation for message access (hardware index)
|                     Vg  - adaption for PowerPC
| 07-Nov-01  1.03     Ht  - remove some comments
|                         - support of basicCAN polling extended
| 12-Dez-01  1.04     Ht  - avoid compiler warnings for KEIL C166
|                         - ESCAN00001881: warning in CanInitPowerOn
|                         - ESCAN00001913: call of CanLL_TxEnd()
|                     Fz  - ESCAN00001914: CanInterruptRestore changed for M32C
| 02-Jan-02  1.05     Ht  - ESCAN00002009: support of polling mode improved
|                         - ESCAN00002010: Prototype of CanHL_TxConfirmation()
|                                          not available in every case.
| 12-Feb-02  1.06     Pl  - ESCAN00002279: - now it is possible to use only the error-task without the tx-task
|                                          - support of the makro  REENTRANT
|                                          - support of the makro C_HL_ENABLE_RX_INFO_STRUCT_PTR
|                                          - For better performance for the T89C51C there is a switch-case
|                                            instruction for direct call of the PreTransmitfunction
|                                          - add C_ENABLE_RX_BASICCAN_POLLING in CanInitPowerOn
| 18-Mai-02  1.07     Ht  - support Hash search with FullCAN controller
|                         - ESCAN00002707: Reception could went wrong if IL and Hash Search
|                         - ESCAN00002893: adaption to LI 1.3
| 29-Mai-02  1.08     Ht  - ESCAN00003028: Transmission could fail in Polling mode
|                         - ESCAN00003082: call Can_LL_TxEnd() in CanMsgTransmit()
|                         - ESCAN00003083: Hash search with extended ID
|                         - ESCAN00003084: Support C_COMP_METROWERKS_PPC
|                         - ESCAN00002164: Temporary vaiable "i" not defined in function CanHL_BasicCanMsgReceived
|                         - ESCAN00003085: support C_HL_ENABLE_IDTYPE_IN_ID
| 25-Jun     1.08.01  Vg  - Declared localInterruptOldFlag in CanRxTask()
|                         - Corrected call to CanWakeUp for multichannel
| 11-Jul-02  1.08.02  Ht  - ESCAN00003203: Hash Search routine does not work will with extended IDs
|                         - ESCAN00003205: Support of ranges could went wrong on some platforms
| 08-Aug-02  1.08.03  Ht  - ESCAN00003447: Transmission without databuffer and pretransmit-function
| 08-Aug-02  1.08.04  An  - Added support to Green Hills
| 09-Sep-02  1.09     Ht  - ESCAN00003837: code optimication with KernelBuilder
|                         - ESCAN00004479: change the place oft the functioncall of CanLL_TxCopyMsgTransmit
|                                          in CanMsgTransmit
| 2002-12-06 1.10     Ht  -                Support consistancy for polling tasks
|                         - ESCAN00004567: Definiton of V_NULL pointer
|                         -                remove include of string.h
|                         -                support Dummy functions for indirect function call
|                         -                optimization for one single Tx mail box
| 2003-02-04 1.11     Ht  -                optimization for polling mode
| 2003-03-19 1.12     Ht  - ESCAN00005152: optimization of CanInit() in case of Direct Tx Objects
|                         - ESCAN00005143: CompilerWarning about function prototype
|                                          CanHL_ReceivedRxHandle() and CanHL_IndRxHandle
|                         - ESCAN00005130: Wrong result of Heash Search on second or higher channel
| 2003-05-12 1.13     Ht  - ESCAN00005624: support CanHL_TxMsgDestroyed for multichannel system
|                         - ESCAN00005209: Support confirmation and indication flags for EasyCAN4
|                         - ESCAN00004721: Change data type of Handle in CanRxInfoStruct
| 2003-06-18 1.20     Ht  - ESCAN00005908: support features of RI1.4
|                         - Support FJ16LX-Workaround for multichannel system
|                         - ESCAN00005671: Dynamic Transmit Objects: ID in extended ID frames is wrong
|                         - ESCAN00005863: Notification about cancelation failes in case of CanHL_TxMsgDestroyed
| 2003-06-30 1.21     Ht  - ESCAN00006117: Common Confirmation Function: Write access to wrong memory location
|                         - ESCAN00006008: CanCanInterruptDisable in case of polling
|                         - ESCAN00006118: Optimization for Mixed ID and ID type in Own Register or ID type in ID Register
|                         - ESCAN00006100: transmission with wrong ID in mixed ID mode
|                         - ESCAN00006063: Undesirable hardware dependency for
|                                          CAN_HL (CanLL_RxBasicTxIdReceived)
| 2003-09-10 1.22     Ht  - ESCAN00006853: Support V_MEMROM0
|                         - ESCAN00006854: suppress some Compiler warnings
|                         - ESCAN00006856: support CanTask if only Wakeup in polling mode
|                         - ESCAN00006857: variable newDLC not defined in case of Variable DataLen
| 2003-10-14 1.23     Ht  - ESCAN00006858: support BrsTime for internal runtime measurement
|                         - ESCAN00006860: support Conditional Msg Receive
|                         - ESCAN00006865: support "Cancel in HW" with CanTask
|                         - ESCAN00006866: support Direct Tx Objects
|                         - ESCAN00007109: support new memory qualifier for const data and pointer to const
| 2004-01-05 1.24     Ml  - ESCAN00007206: resolved preprocessor error for Hitachi compiler
|                     Ml  - ESCAN00007254: several changes
| 2004-02-06 1.25     Ml  - ESCAN00007281: solved compilerwarning
|                     Ml  - removed compiler warnings
| 2004-02-21 1.26     Ml  - ESCAN00007670: CAN RAM check
|                     Ml  - ESCAN00007671: fixed dyn Tx object issue
|                     Ml  - ESCAN00007764: added possibility to adjust Rx handle in LL drv
|                     Ml  - ESCAN00007681: solved compilerwarning in CanHL_IndRxHandle
|                     Ml  - ESCAN00007272: solved queue transmission out of LowLevel object
|                     Ml  - ESCAN00008064: no changes
| 2004-04-16 1.27     Ml  - ESCAN00008204: no changes
|                     Ml  - ESCAN00008160: no changes
|                     Ml  - ESCAN00008266: changed name of parameter of function CanTxGetActHandle
|                     Fz  - ESCAN00008272: Compiler error due to missing array canPollingTaskActive
| 2004-05-10 1.28     Fz  - ESCAN00008328: Compiler error if cancel in hardware is active
|                         - ESCAN00008363: Hole closed when TX in interrupt and cancel in HW is used
|                         - ESCAN00008365: Switch C_ENABLE_APPLCANPREWAKEUP_FCT added
|                         - ESCAN00008391: Wrong parameter macro used in call of
|                                          CanLL_WakeUpHandling
| 2004-05-24 1.29     Ht  - ESCAN00008441: Interrupt not restored in case of internal error if TX Polling is used
| 2004-09-21 1.30     Ht  - ESCAN00008914: CAN channel may stop transmission for a certain time
|                         - ESCAN00008824: check of reference implementation version added
|                         - ESCAN00008825: No call of ApplCanMsgCancelNotification during CanInit()
|                         - ESCAN00008826: Support asssertions for "Conditional Message Received"
|                     Ml  - ESCAN00008752: Added function qualifier macros
|                     Ht  - ESCAN00008823: compiler error due to array size 0
|                         - ESCAN00008977: label without instructions
|                         - ESCAN00009485: Message via Normal Tx Object will not be sent
|                         - ESCAN00009497: support of CommonCAN and RX queue added
|                         - ESCAN00009521: Inconsitancy in total polling mode
| 2004-09-28 1.31     Ht  - ESCAN00009703: unresolved functions CAN_POLLING_IRQ_DISABLE/RESTORE()
| 2004-11-25 1.32     Ht  - move fix for ESCAN00007671 to CAN-LL of DrvCan_MpcToucanHll
|                         - ESCAN00010350: Dynamic Tx messages are send always with Std. ID
|                         - ESCAN00010388: ApplCanMsgConfirmed will only be called if realy transmitted
|                     Ml  - ESCAN00009931: The HardwareLoopCheck should have a channelparameter in multichannel systems.
|                     Ml  - ESCAN00010093: lint warning: function type inconsistent "CanCheckMemory"
|                     Ht  - ESCAN00010811: remove Misra and compiler warnings
|                         - ESCAN00010812: support Multi ECU
|                         - ESCAN00010526: CAN interrupts will be disabled accidently
|                         - ESCAN00010584: ECU may crash or behave strange with Rx queue active
| 2005-01-20 1.33     Ht  - ESCAN00010877: ApplCanMsgTransmitConf() is called erronemous
| 2005-03-03 1.34     Ht  - ESCAN00011139: Improvement/Correction of C_ENABLE_MULTI_ECU_CONFIG
|                         - ESCAN00011511: avoid PC-Lint warnings
|                         - ESCAN00011512: copy DLC in case of variable Rx Datalen
|                         - ESCAN00010847: warning due to missing brakets in can_par.c at CanChannelObject
| 2005-05-23 1.35     Ht  - ESCAN00012445: compiler error "V_MEMROMO undefined"in case of multi ECU
|                         - ESCAN00012350: Compiler Error "Illegal token channel"
| 2005-07-06 1.36     Ht  - ESCAN00012153: Compile Error: missing declaration of variable i
|                         - ESCAN00012460: Confirmation of LowLevel message will run into assertion (C_ENABLE_MULTI_ECU_PHYS enabled)
|                         - support Testpoints for CanTestKit
| 2005-07-14 1.37     Ht  - ESCAN00012892: compile error due to missing logTxObjHandle
|                         - ESCAN00012998: Compile Error: missing declaration of txHandle in CanInit()
|                         - support Testpoints for CanTestKit for FullCAN controller
| 2005-09-21 1.38     Ht  - ESCAN00013597: Linker error: Undefined symbol 'CanHL_IndRxHandle'
| 2005-11-10 1.39     Ht  - ESCAN00014331: Compile error due to missing 'else' in function CanTransmit
| 2005-04-26 2.00.00  Ht  - ESCAN00016698: support RI1.5
|                         - ESCAN00014770: Cosmic compiler reports truncating assignement
|                         - ESCAN00016191: Compiler warning about unused variable in CanTxTask
| 2007-01-23 2.01.00  Ht  - ESCAN00017279: Usage of SingleGlobalInterruptDisable lead to assertion in OSEK
|                         - ESCAN00017148: Compile error in higher layer due to missing declaration of CanTxMsgHandleToChannel
| 2007-03-14 2.02.00  Ht  - ESCAN00019825: error directives added and misra changes
|                         - ESCAN00019827: adaption to never version of VStdLib.
|                         - ESCAN00019619: V_CALLBACK_1 and V_CALLBACK_2 not defined
|                         - ESCAN00019953: Handling of FullCAN reception in interrupt instead of polling or vice versa.
|                         - ESCAN00019958: CanDynTxObjSetId() or CanDynTxObjSetExtId() will run into assertion
| 2007-03-26 2.03.00  Ht  - ESCAN00019988: Compile warnings in can_drv.c
|                         - ESCAN00018831: polling mode: function prototype without function implemenation (CanRxFullCANTask + CanRxBasicCANTask)
| 2007-04-20 2.04.00  dH  - ESCAN00020299: user assertion fired irregularly due to unknown parameter (in case of CommonCAN)
| 2007-05-02 2.05.00  Ht  - ESCAN00021069: Handling of canStatus improved, usage of preprocessor defines unified
|                         - ESCAN00021070: support relocation of HW objects in case of Multiple configuration
|                         - ESCAN00021166: Compiler Warnings: canHwChannel & canReturnCode not used in CanGetStatus
|                         - ESCAN00021223: CanCanInterruptDisabled called during Sleepmode in CanWakeupTask
|                         - ESCAN00022048: Parameter of ApplCancorruptMailbox is hardware channel instead of logical channel - Error directive added
| 2007-11-12 2.06.00  Ht  - ESCAN00023188: support CAN Controller specific polling sequence for BasicCAN objects
|                         - ESCAN00023208: Compile issue about undefined variable kCanTxQueuePadBits in the CAN driver in Bit based Tx queue
| 2008-10-20 2.07.00  Ht  - ESCAN00023010: support disabled mailboxes in case of extended RAM check
|                         - ESCAN00025706: provide C_SUPPORTS_MULTI_ECU_PHYS
|                         - ESCAN00026120: compiler warnings found on DrvCan_V85xAfcanHll RI 1.5
|                         - ESCAN00026322: ApplCanMsgNotMatched not called in special configuration
|                         - ESCAN00026413: Add possibility to reject remote frames received by FullCAN message objects
|                         - ESCAN00028758: CAN HL must support QNX
|                         - ESCAN00029788: CommonCAN for Driver which support only one Tx object improved (CanInit()).
|                         - ESCAN00029889: Compiler warning about uninitialized variable canHwChannel in CanCanInterruptDisable/Restore()
|                         - ESCAN00029891: Compiler warning: variable "rxHandle" was set but never used
|                         - ESCAN00029929: Support Extended ID Masking for Tx Fullcan messages
|                         - ESCAN00030371: Improvements (assertions, misra)
|                         - ESCAN00027931: Wrong check of "queueBitPos" size
| 2009-04-08 2.08.00  Ht  - ESCAN00034492: no automatic remove of CanCanInterruptDisable/Restore
|                         - ESCAN00031816: CANRANGExIDTYPE can be removed and direct expression used
|                         - ESCAN00032027: CanMsgTransmit shall support tCanMsgTransmitStruct pointer accesses to far RAM
|                         - ESCAN00034488: Postfix for unsigned const in perprocessor directives are not supported by all Compiler (ARM-Compiler 1.2)
| 2009-06-04 2.08.01  Ht  - ESCAN00035426: Compiler warning about truncation in CanGetStatus removed
| 2009-10-21 2.09.00  Ht  - ESCAN00036258: Compiler warning about "CanHL_ReceivedRxHandle" was declared but never referenced
|                         - ESCAN00038642: Support reentrant functions for compiler with own keyword
|                         - ESCAN00038645: support new switch C_ENABLE_UPDATE_BASE_ADDRESS
| 2010-02-01 2.10.00  Ht  - ESCAN00036260: Support configuartion without static Tx messages and only one channel (remove compiler warning)
|                         - ESCAN00040478: Handle update of virtual CanBaseAdress in accordance to QNX documentation
|                         - ESCAN00039235: Compiler Warning: Narrowing or Signed-to-Unsigned type conversion
| 2010-07-22 2.11.00  Ht  - ESCAN00044221: support Retransmit functionality for FBL
|                         - ESCAN00044222: internal changes: improve readability and
|                                          change CAN_CAN_INTERRUPT_... macros to avoid preprocessor errors for some compiler
|                         - ESCAN00044174: TxBitQueue only - compiler warning occurs about: condition is always true
| 2010-10-22 2.12.00  Ht  - ESCAN00046326: support  C_COMP_KEIL_XC800 and  C_COMP_TI_TMS320
| 2011-05-17 2.13.00  Ht  - ESCAN00048965: Add assertion check txMsgStruct for NULL-Pointer value in CanMsgTransmit() API
|                         - ESCAN00050948: support retransmit macro if kCanHwTxStartIndex != 0.
| 2012-04-23 2.14.00  Ht  - ESCAN00053779: Linker error: CanBaseAddressRequest() and CanBaseAddressActivate() are not available
|                         - ESCAN00056617: remove brackets in macro CanInterruptDisable/Restore()
|                         - ESCAN00058520: support CommonCAN in combination with RAM check
|                         - ESCAN00058521: support C_COMP_KEIL_SLC8051_CCAN
|                         - ESCAN00058522: some  message will be not be received via BasicCAN if index search and multichannel are active
|                         - ESCAN00058104: compiler warning occurs about: comparison between signed and unsigned
| 2012-04-23 2.14.01  Ht  - ESCAN00058636: some BasicCAN messages will not be received with linear search
| 2012-05-11 2.15.00  Seu - ESCAN00058891: Add switch C_HL_ENABLE_PRETRANSMIT_SWITCH_COMMENT
| 2012-07-02 2.15.01  Ht  - ESCAN00058586: Compiler warning: comparison is always true due to limited range of data type
|                         - ESCAN00059562: Compile error: Size of array CanRxMsgIndirection is zero if index search and no Rx FullCANs are used
|                         - ESCAN00059736: Compiler warning: pointless comparison of unsigned integer with zero
| 2012-10-30 2.15.02  Ht  - ESCAN00061829: Compiler error:  about(CAN_HL_HW_RX_BASIC/FULL_STARTINDEX(canHwChannel) == 0)
|                         - ESCAN00057831: Compiler warning: "canCanInterruptOldStatus" was declared but never referenced
|                         - ESCAN00057832: Compiler warning: "canCanInterruptCounter" was set but never referenced
|                         - ESCAN00062667: Verify Identity during CanOnline and Misra improvement
| 2013-05-27 2.16.00  Ht  - ESCAN00067627: support more than 255 HW objects
| 2014-04-08 2.17.00  Ht  - ESCAN00074874: internal change: support possibility to reject transmit request before request in HW has to be set
| 2014-07-22 2.18.00  Uce - ESCAN00076820: added Misra deviation comments
|                     Ht  - ESCAN00076413: remove compiler warning - comparison is always true due to limited range of data type
| 2015-02-12 2.19.00  Ht  - ESCAN00015288: support dynamic Rx objects
| 2015-08-25 2.20.00  Ht  - ESCAN00081628: reception of wrong dynamic Rx message notified or endless loop in basicCAN reception handling
|                         - ESCAN00085313: support extended ID masking for dynamic Rx messages via Rx BasicCAN object
| 2016-03-07 2.21.00  Ht  - ESCAN00089020: support compatibility macros CAN_HL_INFO_..
|                         - ESCAN00089023: Dynamic Rx objects can only be initialized during CanInitPowerOn()
| 2016-11-03 3.00.00  Tkr - ESCAN00094214: Beta support of RI2.0 and CAN-FD
| 2017-11-16 3.01.00  Tkr - ESCAN00097695: Final support of RI2.0 and CAN-FD
| 2018-01-29 3.02.00  Tkr - ESCAN00098302: Support relocation of HW objects for MultiECUConfig
|                     Jan - ESCAN00098185: Support Hash Search for more than 8 channels
| 2019-06-25 3.03.00  Tkr - ESCAN00101997: Support TxQueue byte algorithm for multi channel configurations
|                         - ESCAN00103505: Adaption of LowLevel interface
|                         - ESCAN00103506: Support MISRA2012
|                         - ESCAN00103507: Remove Retransmit functionality for FBL
|
|    ************    Version and change information of      **********
|    ************    high level part only                   **********
|
|    Please find the version number of the whole module in the previous
|    File header.
|
*************************************************************************** */

#define C_DRV_INTERNAL

/* *********************************************************************** */
/* Include files                                                           */
/* *********************************************************************** */


#include "can_inc.h"
#include "CanTp/srv_cantp.h"

/* *********************************************************************** */
/* Version check                                                           */
/* *********************************************************************** */

#if ( C_VERSION_REF_IMPLEMENTATION != 0x200 )
# error "Generated data and CAN driver source files are inconsistent!"
#endif

#if( DRVCAN_IMXFLEXCAN3HLL_VERSION != 0x0601u)
# error "Source and Header file are inconsistent!"
#endif
#if( DRVCAN_IMXFLEXCAN3HLL_RELEASE_VERSION != 0x00u)
# error "Source and Header file are inconsistent!"
#endif


#if ( DRVCAN__COREHLL_VERSION != 0x0303 )
# error "Source and header files are inconsistent!"
#endif
#if ( DRVCAN__COREHLL_RELEASE_VERSION != 0x00 )
# error "Source and header files are inconsistent!"
#endif

#if ( DRVCAN__HLLTXQUEUEBIT_VERSION != 0x0107 )
# error "TxQueue source and header versions are inconsistent!"
#endif
#if ( DRVCAN__HLLTXQUEUEBIT_RELEASE_VERSION != 0x00 )
# error "TxQueue source and header versions are inconsistent!"
#endif

#if defined( DRVCAN__HLLTXQUEUEBIT_VERSION )
# if ( ( DRVCAN__HLLTXQUEUEBIT_VERSION != 0x0107 ) || \
       ( DRVCAN__HLLTXQUEUEBIT_RELEASE_VERSION != 0x00 ) )
#  error "TxQueue version is inconsistent!"
# endif

/* defines to satisfy MISRA checker tool */
# define DRVCAN__HLLTXQUEUEBYTE_VERSION          0x0000
# define DRVCAN__HLLTXQUEUEBYTE_RELEASE_VERSION  0x00

#else
# if defined( DRVCAN__HLLTXQUEUEBYTE_VERSION )
#  if ( ( DRVCAN__HLLTXQUEUEBYTE_VERSION != 0x0105 ) || \
        ( DRVCAN__HLLTXQUEUEBYTE_RELEASE_VERSION != 0x00 ) )
#   error "TxQueue version is inconsistent!"
#  endif
# else
#  error "No TxQueue available!"
# endif

/* defines to satisfy MISRA checker tool */
# define DRVCAN__HLLTXQUEUEBIT_VERSION           0x0000
# define DRVCAN__HLLTXQUEUEBIT_RELEASE_VERSION   0x00

#endif

/* *********************************************************************** */
/* Defines                                                                 */
/* *********************************************************************** */

/* return values */
#define kCanHlFinishRx                                     ((vuint8)0x00)
#define kCanHlContinueRx                                   ((vuint8)0x01)

#define CANHL_TX_QUEUE_BIT

#if defined (MISRA_CHECK)  /* COV_CAN_MISRA */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_RX_INDEX_TBL",                    0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualIdRaw0",                   0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualDLC",                      0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualStdId",                    0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualExtId",                    0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualFdType",                   0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualIdType",                   0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanTxActualDLC",                      0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanTxActualStdId",                    0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanTxActualExtId",                    0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanTxActualFdType",                   0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanTxActualIdType",                   0310,3305          /* MD_Can_PointerCast, MD_Can_3305_LL_MsgObjectAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "GLOBAL_MAILBOX_ACCESS",               0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "LOCAL_MAILBOX_ACCESS",                0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_TxIsObjFree",                   0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsSleep",                     0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsStop",                      0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsStopRequested",             0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsStart",                     0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsStartRequested",            0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsBusOff",                    0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsAutoRecoveryActive",        0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsListenOnlyMode",            0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsPassive",                   0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanLL_HwIsWarning",                   0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CanRxActualErrorCounter",             0303,0310          /* MD_Can_HwAccess, MD_Can_PointerCast */
# pragma PRQA_MACRO_MESSAGES_OFF "CanTxActualErrorCounter",             0303,0310          /* MD_Can_HwAccess, MD_Can_PointerCast */
# pragma PRQA_MACRO_MESSAGES_OFF "pFlexCAN",                            0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "pRXFIFO",                             0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "pCanRxMask",                          0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "pCanGlitchFilter",                    0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "pCanParityCheck",                     0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "pCanFDRegister",                      0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG",             0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG_RESET",       0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG_SET",         0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_READ_PROTECTED_REG",              0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG_32BIT",       0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG_RESET_32BIT", 0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG_SET_32BIT",   0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_READ_PROTECTED_REG_32BIT",        0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG8",            0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG16",           0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG32",           0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG8_RESET",      0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG16_RESET",     0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG32_RESET",     0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG8_SET",        0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG16_SET",       0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_WRITE_PROTECTED_REG32_SET",       0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_READ_PROTECTED_REG8",             0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_READ_PROTECTED_REG16",            0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "CAN_READ_PROTECTED_REG32",            0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "assertUser",                          0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
# pragma PRQA_MACRO_MESSAGES_OFF "assertHardware",                      0303,0306          /* MD_Can_HwAccess, MD_Can_LL_HwAccess */
#endif

/* Define chiphardware                     */
/* Constants concerning can chip registers */
/* Bitmasks of FlexCAN module configuration register CANx_MCR */
#define kFlexCAN_MDIS      ((vuint16)0x8000u)       /*!< Module Disable: shut down FlexCAN clocks */
#define kFlexCAN_FRZ       ((vuint16)0x4000u)       /*!< Freeze Enable: allow entering the freeze mode */
#define kFlexCAN_FEN       ((vuint16)0x2000u)       /*!< FIFO enable */
#define kFlexCAN_HALT      ((vuint16)0x1000u)       /*!< Halt FlexCAN: request entering the freeze mode */
#define kFlexCAN_NOT_RDY   ((vuint16)0x0800u)       /*!< Not Ready: FlexCAN is in DISABLE, DOZE or STOP mode */
#define kFlexCAN_WAK_MSK   ((vuint16)0x0400u)       /*!< Wakeup Interrupt Mask: enable wakeup interrupt generation */
#define kFlexCAN_SOFT_RST  ((vuint16)0x0200u)       /*!< Soft Reset: reset FlexCAN internal state and some memory mapped registers */
#define kFlexCAN_FRZ_ACK   ((vuint16)0x0100u)       /*!< Freeze Mode Acknowledge */
#define kFlexCAN_SUPV      ((vuint16)0x0080u)       /*!< Supervisor Mode */
#define kFlexCAN_SLF_WAK   ((vuint16)0x0040u)       /*!< FlexCAN Self Wakeup by CAN bus activity */
#define kFlexCAN_WRN_EN    ((vuint16)0x0020u)       /*!< Warning Interrupt Enable */
#define kFlexCAN_LPM_ACK   ((vuint16)0x0010u)       /*!< Low Power Mode Acknowledge */
#define kFlexCAN_WAK_SRC   ((vuint16)0x0008u)       /*!< Wakeup Source: enable filtered Rx input signal */
#define kFlexCAN_DOZE      ((vuint16)0x0004u)       /*!< Doze Mode Enable: allow MCU to switch FlexCAN into DOZE low power mode */
#define kFlexCAN_NOT_DOZE  ((vuint16)0xFFFBu)       /*!< Doze Mode Disable: do not allow MCU to switch FlexCAN into DOZE low power mode */
#define kFlexCAN_SRX_DIS   ((vuint16)0x0002u)       /*!< Self Reception Disable */
#define kFlexCAN_IRMQ      ((vuint16)0x0001u)       /*!< Backwards Compatibility Configuration: enable Individual Rx Masks and queue feature */

#define kFlexCAN_LPPRIO_EN ((vuint16)0x2000u)       /*!< Local Priority Enable */
#define kFlexCAN_AEN       ((vuint16)0x1000u)       /*!< Abort Enable: enables transmit abort feature */
#define kFlexCAN_FDEN      ((vuint16)0x0800u)       /*!< CAN FD operation enable */
#define kFlexCAN_IDAM      ((vuint16)0x0300u)       /*!< ID Acceptance Mode */
#define kFlexCAN_MAXMB     ((vuint16)0x003Fu)       /*!< Maximum Number of Message Buffers: maximum number of used message buffers = MAXMB+1 */

/* Bitmasks of FlexCAN module configuration register CANx_CTRL1 */
#define kFlexCAN_BOFF_MSK  ((vuint32)0x00008000u)   /*!< FlexCAN BusOff interrupt mask */
#define kFlexCAN_LOM       ((vuint32)0x00000008u)   /*!< FlexCAN Listen Only Mode */
#define kFlexCAN_CLK_SRC   ((vuint32)0x00002000u)   /*!< FlexCAN clock source mask */
#define kFlexCAN_BOFF_REC  ((vuint32)0x00000040u)   /*!< FlexCAN auto BusOff recovery */
#define kFlexCAN_SMP       ((vuint32)0x00000080u)   /*!< FlexCAN sampling mode */

/* Bitmasks of FlexCAN module configuration register CANx_ESR */
#define kFlexCAN_WAKE_INT   (vuint32)0x00000001u    /*!< Wake Up interrupt flag */
#define kFlexCAN_ERR_INT    (vuint32)0x00000002u    /*!< Error interrupt flag */
#define kFlexCAN_BOFF_INT   (vuint32)0x00000004u    /*!< Bus Off interrupt flag */
#define kFlexCAN_FCS_BOFF   (vuint32)0x00000020u    /*!< Fault Confinement State value for BusOff */
#define kFlexCAN_FCS_EP     (vuint32)0x00000010u    /*!< Fault Confinement State value for ErrorPassive */
#define kFlexCAN_FCS        (vuint32)0x00000030u    /*!< Fault Confinement State */
#define kFlexCAN_RXTX_WARN  (vuint32)0x00000300u
#define kFlexCAN_STATUS_INT (kFlexCAN_BOFF_INT | kFlexCAN_ERR_INT | kFlexCAN_WAKE_INT)

#define kFlexCAN_MCR_REQ_ONLY_BITS (kFlexCAN_MDIS | kFlexCAN_FRZ | kFlexCAN_HALT)
#define kFlexCAN_MCR               (kFlexCAN_MDIS | kFlexCAN_FRZ | kFlexCAN_HALT | kFlexCAN_NOT_RDY | kFlexCAN_FRZ_ACK | kFlexCAN_LPM_ACK)
#define kFlexCAN_FREEZE_MODE       (kFlexCAN_FRZ | kFlexCAN_HALT | kFlexCAN_NOT_RDY | kFlexCAN_FRZ_ACK) /* FlexCAN in FREEZE mode: FRZ, HALT, NOT_RDY and FRZ_ACK bit are set */
#define kFlexCAN_FREEZE_MODE_REQ   (kFlexCAN_FRZ | kFlexCAN_HALT)
#define kFlexCAN_STOP_MODE         (kFlexCAN_NOT_RDY | kFlexCAN_LPM_ACK) /* FlexCAN in STOP powerdown mode: NOT_RDY and LPM_ACK bits set */
#define kFlexCAN_DISABLE_MODE      (kFlexCAN_MDIS | kFlexCAN_NOT_RDY | kFlexCAN_LPM_ACK) /* FlexCAN in DISABLE powerdown mode: MDIS, NOT_RDY and LPM_ACK bit are set */
#define kFlexCAN_NORMAL_MODE       ((vuint16)0x0000u) /* relevant bits for mode states must all be negated for NORMAL mode */
#define kFlexCAN_NORMAL_MODE_REQ   ((vuint16)0x0000u)
/* FlexCAN is DISABLED, independent from FREEZE mode:
  -> MDIS and LPM_ACK bit are necessary to identify DISABLE mode
  -> NOT_RDY must not be checked because it also influenced by FREEZE mode 
*/
#define kFlexCAN_DISABLE_ONLY_BITS (kFlexCAN_MDIS | kFlexCAN_LPM_ACK)
#define kNotFlexCAN_MCR_CFG_BTIS   (kFlexCAN_MCR | kFlexCAN_SOFT_RST | kFlexCAN_SUPV) /* user configurable bits are '0', not changeable bits are '1' */


#define kNotFlexCANErrBoff ((vuint32)0xFFFF3FFFu)

#define CANSFR_CLEAR      ((vuint32)0x00000000u)
#define CANSFR_SET        ((vuint32)0xFFFFFFFFu)

/* Macros to access the CODE-bits in the control/status word ------------------*/
#define kCodeMask         ((vuint16)0x0F00u)     /*!< Mask to access the CODE in the control/status word */
#define kNotDlcMask       ((vuint16)0xFF00u)
#define kNotCodeMask      ((vuint16)0xF0FFu)

/* Code definitions for receive objects */
#define kRxCodeEmpty      ((vuint16)0x0400u)    /*!< Message buffer is active and empty */
#define kRxCodeClear      ((vuint16)0x00F0u)    /*!< Mask to clear control register but leave the ID type */
#define kRxCodeOverrun    ((vuint16)0x0600u)    /*!< Second frame was received into a full buffer */
#define kRxCodeBusy       ((vuint16)0x0100u)    /*!< Receive buffer locked */

/* Code definitions for transmit objects */
#define kTxCodeTransmit     ((vuint16)0x0C40u)     /*!< transmit request in nominal bit rate  */
#define kTxCodeTransmitFD   ((vuint16)0xCC40u)     /*!< transmit request for CAN-FD: EDL (extended data length) and BRS (bit rate switch) bit must be set */
#define kNotIDEMask         ((vuint16)0xFF0Fu)
#define kTxDlcMask          ((vuint16)0x006Fu)     /*!< Mask to access the DLC in the control/status word */
#define kTxCodeFree         ((vuint16)0x0800u)     /*!< Transmit object free */
#define kTxCodeAbort        ((vuint16)0x0900u)     /*!< Abort message transmission */

#if defined( C_ENABLE_EXTENDED_ID )
# define kTxCodeInactive  ((vuint16)0x0820u)       /*!< Transmit object inactive for extended or mixed IDs */
#else
# define kTxCodeInactive  ((vuint16)0x0800u)       /*!< Transmit object inactive for standard IDs */
#endif

/* Code definitions for Rx FiFo ---------------------------- */
#define kRxFIFO_OVERRUN     (vuint32)0x00000080u  /*!< RxFIFO overrun (iflag1) */
#define kRxFIFO_WARN        (vuint32)0x00000040u  /*!< RxFIFO warning (iflag1) */
#define kRxFIFO_NEWMSG      (vuint32)0x00000020u  /*!< RxFIFO new message available (iflag1) */
#define kRxFIFO_EXT         (vuint32)0x40000000u  /*!< RxFIFO extended ID (ID tab element) */
#define kRxFIFO_REM         (vuint32)0x80000000u  /*!< RxFIFO remote frame (ID tab element) */
#define kRxFIFO_MASK        (vuint32)0xC0000000u  /*!< RxFIFO mask for ID tab element */

# define C_FLEXCAN_RXFIFO_MAXLOOP  6u  /*!< 6 RX Fifo elements available */

/* Code definitions for ctrl2 register ------------------- */
#define kFlexCAN_ECRWRE         (vuint32)0x20000000u  /*!< Enables write access to MECR register */
#define kFlexCAN_MRP            (vuint32)0x00040000u  /*!< Matching process starts from mailboxes and continues on Rx FIFO */
#define kFlexCAN_RRS            (vuint32)0x00020000u  /*!< Remote request frame is stored */
#define kFlexCAN_EACEN          (vuint32)0x00010000u  /*!< Enables the comparison of IDE and RTR bit */
#define kFlexCAN_TASD_DEFAULT   (vuint32)0x00800000u  /*!< TASD default value */
#define kFlexCAN_STFCNTEN       (vuint32)0x00001000u  /*!< Enables ISO CAN FD */

/* Code definitions for mecr  register ------------------- */
#define kFlexCAN_ECRWRDIS       (vuint32)0x80000000u  /*!< Disable MECR write */

#define kExtIDBit               (vuint32)0x80000000u

#if defined( C_ENABLE_RX_MASK_EXT_ID )
# if !defined( C_MASK_EXT_ID )
#  define C_MASK_EXT_ID ((vuint32)0xDFFFFFFFu)
# endif
#endif

#define kCanRxMaskStd     ((vuint32)0xDFFC0000u)
#if defined( C_ENABLE_EXTENDED_ID )
# if defined( C_ENABLE_RX_MASK_EXT_ID )
#  define kCanRxMaskExt   ((vuint32)C_MASK_EXT_ID)
# else
#  define kCanRxMaskExt   ((vuint32)0xDFFFFFFFu)
# endif
#endif

#if defined( C_ENABLE_EXTENDED_ID )
# define CAN_MSGID(x) (vuint32)(x) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#else
# define CAN_MSGID(x) ((vuint32)(x) << 16) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#endif

#define kCanRxFifoIntUnmask   ((vuint32)0xFFFFFF00u)

#define kCanMaskAll32 0xFFFFFFFFu
#define kCanMaskAll16 0xFFFFu

/* all bits of CAN[MCR].MCR except mode and reserved bits */  
#define kCanRamCheckMaskMCR         0x24EBu

#if defined ( C_ENABLE_MB96TO127 ) || defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
# define maxmbMask                  0x007Fu
#else
# define maxmbMask                  0x003Fu
#endif

/* all bits of CAN[MCR].MAXMB except unused/reserved bits */  
#if defined( C_ENABLE_CAN_FD_USED )
# define kCanRamCheckMaskMAXMB      (0x3300u | kFlexCAN_FDEN | maxmbMask)
#else
# define kCanRamCheckMaskMAXMB      (0x3300u | maxmbMask)
#endif

#define kCanRamCheckMailboxControl  0x0F7Fu


#define CanBswap32(x)                   (vuint32)  (( ((vuint32)((x) & 0xFF000000u)) >> 24) | /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */ \
                                                    ( ((vuint32)((x) & 0x00FF0000u)) >>  8) | \
                                                    ( ((vuint32)((x) & 0x0000FF00u)) <<  8) | \
                                                    ( ((vuint32)((x) & 0x000000FFu)) << 24))      /* swap b4-b3-b2-b1 to b1-b2-b3-b4 */

/* FlexCAN LL Transition States */
#define kCanLLStateStart                        0u
#define kCanLLStateRequested                    1u

/* *********************************************************************** */
/* Macros                                                                  */
/* *********************************************************************** */

/* PRQA S 3453 QAC_Can_Macros_C */ /* MD_MSR_FctLikeMacro */

#if !(defined( C_HL_DISABLE_RX_INFO_STRUCT_PTR ) || defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR ))
# define C_HL_ENABLE_RX_INFO_STRUCT_PTR
#endif

#if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
# define CAN_HL_P_RX_INFO_STRUCT(ch)                       (pCanRxInfoStruct)
# define CAN_HL_P_RX_INFO_STRUCT_HANDLE(ch)                (pCanRxInfoStruct->Handle)
#else
# define CAN_HL_P_RX_INFO_STRUCT(ch)                       (&canRxInfoStruct[ch])
# define CAN_HL_P_RX_INFO_STRUCT_HANDLE(ch)                (canRxInfoStruct[ch].Handle)
#endif

#if defined( C_SINGLE_RECEIVE_CHANNEL )
# if (kCanNumberOfUsedCanRxIdTables == 1)
#  define C_RANGE_MATCH_STD( idRaw0, mask, code) \
                  (  ((idRaw0) & (tCanRxId0)~MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) )
#  define C_RANGE_MATCH_EXT( idRaw0, mask, code) \
                  (  ((idRaw0) & (tCanRxId0)~MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 2)
#  define C_RANGE_MATCH_STD( idRaw0, idRaw1, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) )
#  define C_RANGE_MATCH_EXT( idRaw0, idRaw1, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 3)
#  define C_RANGE_MATCH_STD( idRaw0, idRaw1, idRaw2, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDSTD2(mask)) == MK_RX_RANGE_CODE_IDSTD2(code) ) )
#  define C_RANGE_MATCH_EXT( idRaw0, idRaw1, idRaw2, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDEXT2(mask)) == MK_RX_RANGE_CODE_IDEXT2(code) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 4)
#  define C_RANGE_MATCH_STD( idRaw0, idRaw1, idRaw2, idRaw3, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDSTD2(mask)) == MK_RX_RANGE_CODE_IDSTD2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDSTD3(mask)) == MK_RX_RANGE_CODE_IDSTD3(code) ) )
#  define C_RANGE_MATCH_EXT( idRaw0, idRaw1, idRaw2, idRaw3, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDEXT2(mask)) == MK_RX_RANGE_CODE_IDEXT2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDEXT3(mask)) == MK_RX_RANGE_CODE_IDEXT3(code) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 5)
#  define C_RANGE_MATCH_STD( idRaw0, idRaw1, idRaw2, idRaw3, idRaw4, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDSTD2(mask)) == MK_RX_RANGE_CODE_IDSTD2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDSTD3(mask)) == MK_RX_RANGE_CODE_IDSTD3(code) ) && \
                    ( ((idRaw4) & (tCanRxId4)~ MK_RX_RANGE_MASK_IDSTD4(mask)) == MK_RX_RANGE_CODE_IDSTD4(code) ) )
#  define C_RANGE_MATCH_EXT( idRaw0, idRaw1, idRaw2, idRaw3, idRaw4, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDEXT2(mask)) == MK_RX_RANGE_CODE_IDEXT2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDEXT3(mask)) == MK_RX_RANGE_CODE_IDEXT3(code) ) && \
                    ( ((idRaw4) & (tCanRxId4)~ MK_RX_RANGE_MASK_IDEXT4(mask)) == MK_RX_RANGE_CODE_IDEXT4(code) ) )
# endif
#else /* C_MULTIPLE_RECEIVE_CHANNEL */
# if (kCanNumberOfUsedCanRxIdTables == 1)
#  define C_RANGE_MATCH( idRaw0, mask, code) \
                                (  ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 2)
#  define C_RANGE_MATCH( idRaw0, idRaw1, mask, code) \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) && \
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 3)
#  define C_RANGE_MATCH( idRaw0, idRaw1, idRaw2, mask, code) \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) && \
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) && \
                                  ( ((idRaw2) & (tCanRxId2)~((mask).Id2)) == ((code).Id2) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 4)
#  define C_RANGE_MATCH( idRaw0, idRaw1, idRaw2, idRaw3, mask, code) \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) && \
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) && \
                                  ( ((idRaw2) & (tCanRxId2)~((mask).Id2)) == ((code).Id2) ) && \
                                  ( ((idRaw3) & (tCanRxId3)~((mask).Id3)) == ((code).Id3) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 5)
#  define C_RANGE_MATCH( idRaw0, idRaw1, idRaw2, idRaw3, idRaw4, mask, code) \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) && \
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) && \
                                  ( ((idRaw2) & (tCanRxId2)~((mask).Id2)) == ((code).Id2) ) && \
                                  ( ((idRaw3) & (tCanRxId3)~((mask).Id3)) == ((code).Id3) ) && \
                                  ( ((idRaw4) & (tCanRxId4)~((mask).Id4)) == ((code).Id4) ) )
# endif
#endif

#if (kCanNumberOfUsedCanRxIdTables == 1)
# define CAN_RX_IDRAW_PARA                                 idRaw0
#endif
#if (kCanNumberOfUsedCanRxIdTables == 2)
# define CAN_RX_IDRAW_PARA                                 idRaw0, idRaw1
#endif
#if (kCanNumberOfUsedCanRxIdTables == 3)
# define CAN_RX_IDRAW_PARA                                 idRaw0, idRaw1, idRaw2
#endif
#if (kCanNumberOfUsedCanRxIdTables == 4)
# define CAN_RX_IDRAW_PARA                                 idRaw0, idRaw1, idRaw2, idRaw3
#endif
#if (kCanNumberOfUsedCanRxIdTables == 5)
# define CAN_RX_IDRAW_PARA                                 idRaw0, idRaw1, idRaw2, idRaw3, idRaw4
#endif

#if defined( C_SINGLE_RECEIVE_CHANNEL )
# define channel                                           ((CanChannelHandle)0)
# define canHwChannel                                      ((CanChannelHandle)0)
# define CAN_HL_HW_CHANNEL_STARTINDEX(ch)                  ((CanChannelHandle)0)
# define CAN_HL_HW_CHANNEL_STOPINDEX(ch)                   ((CanChannelHandle)0)
# define CAN_HL_MB_MSG_TRANSMIT_INDEX(hwCh)                (kCanMailboxMsgTransmitIndex)
# define CAN_HL_MB_TX_NORMAL_INDEX(hwCh)                   (kCanMailboxTxNormalIndex)

/* Offset which has to be added to change the hardware Tx handle into a logical handle, which is unique over all channels */
/*        Tx-mailbox-Handle - CAN_HL_MB_TX_STARTINDEX(canHwChannel) + CAN_HL_LOG_MB_TX_STARTINDEX(canHwChannel) */
# define CAN_HL_TX_OFFSET_MB_TO_LOG(hwCh)                  ((vsintx)0-(vsintx)kCanMailboxTxStartIndex)
/* ESCAN00062667 */

# define CAN_HL_TX_STARTINDEX(ch)                          ((CanTransmitHandle)0)
# define CAN_HL_TX_STAT_STARTINDEX(ch)                     ((CanTransmitHandle)0)
# define CAN_HL_TX_DYN_ROM_STARTINDEX(ch)                  (kCanNumberOfTxStatObjects)
# define CAN_HL_TX_DYN_RAM_STARTINDEX(ch)                  ((CanTransmitHandle)0)
/* # define CAN_HL_RX_STARTINDEX(ch)                       ((CanReceiveHandle)0) */
/* index to access the ID tables - Basic index only for linear search
   for hash search this is the start index of the ??? */
# define CAN_HL_RX_BASIC_STARTINDEX(ch)                    ((CanReceiveHandle)0)
# define CAN_HL_RX_DYN_RAM_STARTINDEX(ch)                  ((CanReceiveHandle)0)
# define CAN_HL_RX_DYN_RAM_BASIC_STARTINDEX(ch)            (kCanNumberOfRxDynFullCANObjects)
# define CAN_HL_RX_DYN_RAM_FULL_STARTINDEX(ch)             ((CanReceiveHandle)0)
# if defined( C_SEARCH_HASH ) || \
     defined( C_SEARCH_INDEX )
#  define CAN_HL_RX_FULL_STARTINDEX(hwCh)                  ((CanReceiveHandle)0)
#  define CAN_HL_RX_DYN_ROM_STARTINDEX(ch)                 (kCanNumberOfRxStatFullCANObjects)
# else
#  define CAN_HL_RX_FULL_STARTINDEX(hwCh)                  (kCanNumberOfRxBasicCANObjects)
#  define CAN_HL_RX_DYN_ROM_STARTINDEX(ch)                 (kCanNumberOfRxStatObjects)
# endif
# define CAN_HL_INIT_OBJ_STARTINDEX(ch)                    ((vuint8)0)
# define CAN_HL_LOG_MB_TX_STARTINDEX(hwCh)                 ((CanObjectHandle)0)
# define CAN_HL_MB_TX_STARTINDEX(hwCh)                     ((CanObjectHandle)kCanMailboxTxStartIndex)
/* ESCAN00076413 */

# define CAN_HL_MB_RX_FULL_STARTINDEX(hwCh)                (kCanMailboxRxFullStartIndex)
# define CAN_HL_MB_RX_FULL_DYN_STARTINDEX(hwCh)            (kCanMailboxRxDynFullStartIndex)
# define CAN_HL_MB_RX_BASIC_STARTINDEX(hwCh)               (kCanMailboxRxBasicStartIndex)
# define CAN_HL_MB_UNUSED_STARTINDEX(hwCh)                 (kCanMailboxUnusedStartIndex)

# define CAN_HL_TX_STOPINDEX(ch)                           (kCanNumberOfTxObjects)
# define CAN_HL_TX_STAT_STOPINDEX(ch)                      (kCanNumberOfTxStatObjects)
# define CAN_HL_TX_DYN_ROM_STOPINDEX(ch)                   (kCanNumberOfTxObjects)
# define CAN_HL_TX_DYN_RAM_STOPINDEX(ch)                   (kCanNumberOfTxDynObjects)
/* # define CAN_HL_RX_STOPINDEX(ch)                        (kCanNumberOfRxObjects) */
# define CAN_HL_RX_BASIC_STOPINDEX(ch)                     (kCanNumberOfRxBasicCANObjects)
# define CAN_HL_RX_DYN_RAM_STOPINDEX(ch)                   (kCanNumberOfRxDynObjects)
# define CAN_HL_RX_DYN_RAM_BASIC_STOPINDEX(ch)             (kCanNumberOfRxDynObjects)
# if defined( C_SEARCH_HASH ) || \
     defined( C_SEARCH_INDEX )
#  define CAN_HL_RX_FULL_STOPINDEX(hwCh)                   (kCanNumberOfRxFullCANObjects)
# else
#  define CAN_HL_RX_FULL_STOPINDEX(hwCh)                   (kCanNumberOfRxBasicCANObjects+kCanNumberOfRxFullCANObjects)
# endif
# define CAN_HL_INIT_OBJ_STOPINDEX(ch)                     (kCanNumberOfInitObjects)
# define CAN_HL_LOG_MB_TX_STOPINDEX(hwCh)                  (kCanNumberOfTxMailboxes)
# define CAN_HL_MB_TX_STOPINDEX(hwCh)                      (kCanMailboxTxStartIndex     +kCanNumberOfTxMailboxes)
# define CAN_HL_MB_RX_FULL_STOPINDEX(hwCh)                 (kCanMailboxRxFullStartIndex +kCanNumberOfRxFullMailboxes)
# define CAN_HL_MB_RX_BASIC_STOPINDEX(hwCh)                (kCanMailboxRxBasicStartIndex+kCanNumberOfRxBasicMailboxes)
# define CAN_HL_MB_UNUSED_STOPINDEX(hwCh)                  (kCanMailboxUnusedStartIndex +kCanNumberOfUnusedMailboxes)

# define CAN_HL_MB_INDIRECTION_STARTINDEX(hwCh)            (kCanHwObjToMailboxIndirectionStartIndex)
# define CAN_HL_MB_TX_INDIRECTION_STARTINDEX(hwCh)         (kCanHwObjToMailboxIndirectionTxStartIndex)
# define CAN_HL_MB_RX_FULL_INDIRECTION_STARTINDEX(hwCh)    (kCanHwObjToMailboxIndirectionRxFullStartIndex)
# define CAN_HL_MB_RX_BASIC_INDIRECTION_STARTINDEX(hwCh)   (kCanHwObjToMailboxIndirectionRxBasicStartIndex)

# define CAN_HL_HW_STARTINDEX(hwCh)                        (kCanHwObjStartIndex)
# define CAN_HL_HW_TX_STARTINDEX(hwCh)                     (kCanHwTxStartIndex)
# define CAN_HL_HW_RX_FULL_STARTINDEX(hwCh)                (kCanHwRxFullStartIndex)
# define CAN_HL_HW_RX_BASIC_STARTINDEX(hwCh)               (kCanHwRxBasicStartIndex)
#else
#  define canHwChannel                                     channel   /* brackets are not allowed here due to compiler error with Renesas HEW compiler for SH2 */
#  define CAN_HL_HW_CHANNEL_STARTINDEX(ch)                 (ch)
#  define CAN_HL_HW_CHANNEL_STOPINDEX(ch)                  (ch)

# define CAN_HL_MB_MSG_TRANSMIT_INDEX(hwCh)                (Can_MailboxMsgTransmitIndex[hwCh])
# define CAN_HL_MB_TX_NORMAL_INDEX(hwCh)                   (Can_MailboxTxNormalIndex[hwCh])
/* Offset which has to be added to change the hardware Tx handle into a logical handle, which is unique over all channels */
/*        Tx-mailbox-Handle - CAN_HL_MB_TX_STARTINDEX(canHwChannel) + CAN_HL_LOG_MB_TX_STARTINDEX(canHwChannel) */
# define CAN_HL_TX_OFFSET_MB_TO_LOG(hwCh)                  (CanTxOffsetMailboxToLog[hwCh])

# define CAN_HL_TX_STARTINDEX(ch)                          (CanTxStartIndex[ch])
# define CAN_HL_TX_STAT_STARTINDEX(ch)                     (CanTxStartIndex[ch])
# define CAN_HL_TX_DYN_ROM_STARTINDEX(ch)                  (CanTxDynRomStartIndex[ch])
# define CAN_HL_TX_DYN_RAM_STARTINDEX(ch)                  (CanTxDynRamStartIndex[ch])
/* # define CAN_HL_RX_STARTINDEX(ch)                       (CanRxStartIndex[ch]) */
/* index to access the ID tables - Basic index only for linear search */
# define CAN_HL_RX_BASIC_STARTINDEX(ch)                    (CanRxBasicStartIndex[ch])
# define CAN_HL_RX_FULL_STARTINDEX(hwCh)                   (CanRxFullStartIndex[hwCh])
# define CAN_HL_RX_DYN_ROM_STARTINDEX(ch)                  (CanRxDynRomStart[ch])
# define CAN_HL_RX_DYN_RAM_STARTINDEX(ch)                  (CanRxDynRamFullStart[ch])
# define CAN_HL_RX_DYN_RAM_BASIC_STARTINDEX(ch)            (CanRxDynRamBasicStart[ch])
# define CAN_HL_RX_DYN_RAM_FULL_STARTINDEX(ch)             (CanRxDynRamFullStart[ch])
# define CAN_HL_INIT_OBJ_STARTINDEX(ch)                    (CanInitObjectStartIndex[ch])
# define CAN_HL_LOG_MB_TX_STARTINDEX(hwCh)                 (CanLogMailboxTxStartIndex[hwCh])
# define CAN_HL_MB_TX_STARTINDEX(hwCh)                     (Can_MailboxTxStartIndex[hwCh])
# define CAN_HL_MB_RX_FULL_STARTINDEX(hwCh)                (Can_MailboxRxFullStartIndex[hwCh])
# define CAN_HL_MB_RX_FULL_DYN_STARTINDEX(hwCh)            (Can_MailboxRxDynFullStartIndex[hwCh])
# define CAN_HL_MB_RX_BASIC_STARTINDEX(hwCh)               (Can_MailboxRxBasicStartIndex[hwCh])
# define CAN_HL_MB_UNUSED_STARTINDEX(hwCh)                 (Can_MailboxUnusedStartIndex[hwCh])

# define CAN_HL_TX_STOPINDEX(ch)                           (CanTxStartIndex[(ch) + 1u])
# define CAN_HL_TX_STAT_STOPINDEX(ch)                      (CanTxDynRomStartIndex[ch])
# define CAN_HL_TX_DYN_ROM_STOPINDEX(ch)                   (CanTxStartIndex[(ch) + 1u])
# define CAN_HL_TX_DYN_RAM_STOPINDEX(ch)                   (CanTxDynRamStartIndex[(ch) + 1u])
/* # define CAN_HL_RX_STOPINDEX(ch)                        (CanRxStartIndex[(ch) + 1u]) */
/* index to access the ID tables - Basic index only for linear search */
# define CAN_HL_RX_BASIC_STOPINDEX(ch)                     (CanRxFullStartIndex[CAN_HL_HW_CHANNEL_STARTINDEX(ch)])
# define CAN_HL_RX_DYN_RAM_STOPINDEX(ch)                   (CanRxDynRamFullStart[(ch) + 1u])
# define CAN_HL_RX_DYN_RAM_BASIC_STOPINDEX(ch)             (CanRxDynRamFullStart[(ch) + 1u])
# define CAN_HL_INIT_OBJ_STOPINDEX(ch)                     (CanInitObjectStartIndex[(ch) + 1u])
# define CAN_HL_LOG_MB_TX_STOPINDEX(hwCh)                  (CanLogMailboxTxStartIndex[(hwCh) +1u])
# define CAN_HL_MB_TX_STOPINDEX(hwCh)                      (Can_MailboxTxStopIndex[hwCh])
# define CAN_HL_MB_RX_FULL_STOPINDEX(hwCh)                 (Can_MailboxRxFullStopIndex[hwCh])
# define CAN_HL_MB_RX_BASIC_STOPINDEX(hwCh)                (Can_MailboxRxBasicStopIndex[hwCh])
# define CAN_HL_MB_UNUSED_STOPINDEX(hwCh)                  (Can_MailboxUnusedStopIndex[hwCh])

#  define CAN_HL_MB_INDIRECTION_STARTINDEX(hwCh)           (Can_HwObjToMailboxIndirectionStartIndex[hwCh])
#  define CAN_HL_MB_TX_INDIRECTION_STARTINDEX(hwCh)        (Can_HwObjToMailboxIndirectionTxStartIndex[hwCh])
#  define CAN_HL_MB_RX_FULL_INDIRECTION_STARTINDEX(hwCh)   (Can_HwObjToMailboxIndirectionRxFullStartIndex[hwCh])
#  define CAN_HL_MB_RX_BASIC_INDIRECTION_STARTINDEX(hwCh)  (Can_HwObjToMailboxIndirectionRxBasicStartIndex[hwCh])
#  define CAN_HL_HW_STARTINDEX(hwCh)                       (Can_HwObjStartIndex[hwCh])
#  define CAN_HL_HW_TX_STARTINDEX(hwCh)                    (Can_HwTxStartIndex[hwCh])
#  define CAN_HL_HW_RX_FULL_STARTINDEX(hwCh)               (Can_HwRxFullStartIndex[hwCh])
#  define CAN_HL_HW_RX_BASIC_STARTINDEX(hwCh)              (Can_HwRxBasicStartIndex[hwCh])
#endif

# define CAN_HL_MB_HWOBJHANDLE(mailboxHandle)              (Can_Mailbox[mailboxHandle].HwObjHandle)
# define CAN_HL_MB_HWOBJCOUNT(mailboxHandle)               (Can_Mailbox[mailboxHandle].HwObjCount)
#if defined( C_ENABLE_CAN_FD_FULL )
# define CAN_HL_MB_HWOBJMAXDATALEN(mailboxHandle)          (Can_Mailbox[mailboxHandle].HwObjMaxDataLen)
#endif
#if defined( C_ENABLE_INDIVIDUAL_POLLING )
# define CAN_HL_MB_HWOBJPOLLING(mailboxHandle)             (Can_Mailbox[mailboxHandle].HwObjPolling)
#endif


#if defined( C_SEARCH_HASH )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  if defined( C_ENABLE_EXTENDED_ID )
#   if( kHashSearchListCountEx > 0)
#    define CAN_HL_HASH_RANDOM_NUMBER_EX(ch)               (kHashSearchRandomNumberEx)
#    define CAN_HL_HASH_START_INDEX_EX(ch)                 (0u)
#    define CAN_HL_HASH_STOP_INDEX_EX(ch)                  (kHashSearchListCountEx)
#   endif
#   if defined( C_ENABLE_MIXED_ID )
#    if( kHashSearchListCount > 0)
#     define CAN_HL_HASH_RANDOM_NUMBER(ch)                 (kHashSearchRandomNumber)
#     define CAN_HL_HASH_START_INDEX(ch)                   (0u)
#     define CAN_HL_HASH_STOP_INDEX(ch)                    (kHashSearchListCount)
#    endif
#   endif /* C_ENABLE_MIXED_ID */
#  else /* C_ENABLE_EXTENDED_ID */
#   if( kHashSearchListCount > 0)
#    define CAN_HL_HASH_RANDOM_NUMBER(ch)                  (kHashSearchRandomNumber)
#    define CAN_HL_HASH_START_INDEX(ch)                    (0u)
#    define CAN_HL_HASH_STOP_INDEX(ch)                     (kHashSearchListCount)
#   endif
#  endif /* C_ENABLE_EXTENDED_ID */
# else /* C_SINGLE_RECEIVE_CHANNEL */
#  if defined( C_ENABLE_EXTENDED_ID )
#   if( kHashSearchListCountEx > 0)
#    define CAN_HL_HASH_RANDOM_NUMBER_EX(ch)               (CanHashSearchRandomNumberEx[ch])
#    define CAN_HL_HASH_START_INDEX_EX(ch)                 (CanHashStartIndexEx[ch])
#    define CAN_HL_HASH_STOP_INDEX_EX(ch)                  (CanHashStopIndexEx[ch])
#   endif
#   if defined( C_ENABLE_MIXED_ID )
#    if( kHashSearchListCount > 0)
#     define CAN_HL_HASH_RANDOM_NUMBER(ch)                 (CanHashSearchRandomNumber[ch])
#     define CAN_HL_HASH_START_INDEX(ch)                   (CanHashStartIndex[ch])
#     define CAN_HL_HASH_STOP_INDEX(ch)                    (CanHashStopIndex[ch])
#    endif
#   endif /* C_ENABLE_MIXED_ID */
#  else /* C_ENABLE_EXTENDED_ID */
#   if( kHashSearchListCount > 0)
#    define CAN_HL_HASH_RANDOM_NUMBER(ch)                  (CanHashSearchRandomNumber[ch])
#    define CAN_HL_HASH_START_INDEX(ch)                    (CanHashStartIndex[ch])
#    define CAN_HL_HASH_STOP_INDEX(ch)                     (CanHashStopIndex[ch])
#   endif
#  endif /* C_ENABLE_EXTENDED_ID */
# endif /* C_SINGLE_RECEIVE_CHANNEL */

# if defined( C_ENABLE_EXTENDED_ID )
#  define CAN_HL_HASH_COUNT_EX(ch)                         (CAN_HL_HASH_STOP_INDEX_EX(ch) - CAN_HL_HASH_START_INDEX_EX(ch))
#  if defined( C_ENABLE_MIXED_ID )
#   define CAN_HL_HASH_COUNT(ch)                           (CAN_HL_HASH_STOP_INDEX(ch) - CAN_HL_HASH_START_INDEX(ch))
#  endif
# else
#   define CAN_HL_HASH_COUNT(ch)                           (CAN_HL_HASH_STOP_INDEX(ch) - CAN_HL_HASH_START_INDEX(ch))
# endif
#endif /* C_SEARCH_HASH */

#if defined( C_SINGLE_RECEIVE_CHANNEL )

# define CANRANGE0ACCMASK(i)                               C_RANGE0_ACC_MASK
# define CANRANGE0ACCCODE(i)                               C_RANGE0_ACC_CODE
# define CANRANGE1ACCMASK(i)                               C_RANGE1_ACC_MASK
# define CANRANGE1ACCCODE(i)                               C_RANGE1_ACC_CODE
# define CANRANGE2ACCMASK(i)                               C_RANGE2_ACC_MASK
# define CANRANGE2ACCCODE(i)                               C_RANGE2_ACC_CODE
# define CANRANGE3ACCMASK(i)                               C_RANGE3_ACC_MASK
# define CANRANGE3ACCCODE(i)                               C_RANGE3_ACC_CODE

# define APPL_CAN_MSG_RECEIVED( i )                        (APPL_CAN_MSGRECEIVED( i ))

# define APPLCANRANGE0PRECOPY( i )                         (ApplCanRange0Precopy( i ))
# define APPLCANRANGE1PRECOPY( i )                         (ApplCanRange1Precopy( i ))
# define APPLCANRANGE2PRECOPY( i )                         (ApplCanRange2Precopy( i ))
# define APPLCANRANGE3PRECOPY( i )                         (ApplCanRange3Precopy( i ))

# define APPL_CAN_BUSOFF( i )                              (ApplCanBusOff())
# define APPL_CAN_WAKEUP( i )                              (ApplCanWakeUp())

# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
#  define APPLCANCANCELNOTIFICATION( i, j )                (APPL_CAN_CANCELNOTIFICATION( j ))
# else
#  define APPLCANCANCELNOTIFICATION( i, j )
# endif
# if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
#  define APPLCANMSGCANCELNOTIFICATION( i )                (APPL_CAN_MSGCANCELNOTIFICATION())
# else
#  define APPLCANMSGCANCELNOTIFICATION( i )
# endif

# define CAN_RX_INDEX_TBL(i, id)                           (CanRxIndexTbl[id])

#else

# define CANRANGE0ACCMASK(i)                               (CanChannelObject[i].RangeMask[0])
# define CANRANGE0ACCCODE(i)                               (CanChannelObject[i].RangeCode[0])
# define CANRANGE1ACCMASK(i)                               (CanChannelObject[i].RangeMask[1])
# define CANRANGE1ACCCODE(i)                               (CanChannelObject[i].RangeCode[1])
# define CANRANGE2ACCMASK(i)                               (CanChannelObject[i].RangeMask[2])
# define CANRANGE2ACCCODE(i)                               (CanChannelObject[i].RangeCode[2])
# define CANRANGE3ACCMASK(i)                               (CanChannelObject[i].RangeMask[3])
# define CANRANGE3ACCCODE(i)                               (CanChannelObject[i].RangeCode[3])

/* generated id type of the range */
# define CANRANGE0IDTYPE(i)                                (CanChannelObject[i].RangeIdType[0])
# define CANRANGE1IDTYPE(i)                                (CanChannelObject[i].RangeIdType[1])
# define CANRANGE2IDTYPE(i)                                (CanChannelObject[i].RangeIdType[2])
# define CANRANGE3IDTYPE(i)                                (CanChannelObject[i].RangeIdType[3])

# define APPL_CAN_MSG_RECEIVED( i )                        (CanChannelObject[(i)->Channel].ApplCanMsgReceivedFct(i))

# define APPLCANRANGE0PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[0](i))
# define APPLCANRANGE1PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[1](i))
# define APPLCANRANGE2PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[2](i))
# define APPLCANRANGE3PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[3](i))

# define APPL_CAN_BUSOFF( i )                              (CanChannelObject[i].ApplCanBusOffFct(i))
# define APPL_CAN_WAKEUP( i )                              (CanChannelObject[i].ApplCanWakeUpFct(i))

# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
#  define APPLCANCANCELNOTIFICATION( i, j )                (CanChannelObject[i].ApplCanCancelNotificationFct( j ))
# else
#  define APPLCANCANCELNOTIFICATION( i, j )
# endif

# if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
#  define APPLCANMSGCANCELNOTIFICATION( i )                (CanChannelObject[i].ApplCanMsgTransmitCancelNotifyFct( i ))
# else
#  define APPLCANMSGCANCELNOTIFICATION( i )
# endif

# define CAN_RX_INDEX_TBL(i, id)                           (CanRxIndexTbl[i][id])

#endif

#if defined ( C_ENABLE_CAN_CAN_INTERRUPT_CONTROL )
# define CAN_CAN_INTERRUPT_DISABLE(ch)                     (CanCanInterruptDisable(CAN_CHANNEL_CANPARA_ONLY))
# define CAN_CAN_INTERRUPT_RESTORE(ch)                     (CanCanInterruptRestore(CAN_CHANNEL_CANPARA_ONLY))
#else
# define CAN_CAN_INTERRUPT_DISABLE(ch)
# define CAN_CAN_INTERRUPT_RESTORE(ch)
#endif

/* mask for range enable status */
#define kCanRange0                                         ((vuint16)1)
#define kCanRange1                                         ((vuint16)2)
#define kCanRange2                                         ((vuint16)4)
#define kCanRange3                                         ((vuint16)8)

/* derive RxDataLen for DLC check and Datacopying according the configuration */
# define CanGetDerivedRxDataLen(rxHandle)                  (CanGetRxDataLen(rxHandle))

/* Assertions ---------------------------------------------------------------- */

#if defined( C_ENABLE_USER_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertUser(p, c, e)                              if (!(p))   {ApplCanFatalError(e);}
# else
#  define assertUser(p, c, e)                              if (!(p))   {ApplCanFatalError((c), (e));}
# endif
#else
# define assertUser(p, c, e)
#endif

#if defined( C_ENABLE_GEN_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertGen(p, c, e)                               if (!(p))   {ApplCanFatalError(e);}
# else
#  define assertGen(p, c, e)                               if (!(p))   {ApplCanFatalError((c), (e));}
# endif
#else
# define assertGen(p, c, e)
#endif

#if defined( C_ENABLE_HARDWARE_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertHardware(p, c, e)                          if (!(p))   {ApplCanFatalError(e);}
# else
#  define assertHardware(p, c, e)                          if (!(p))   {ApplCanFatalError((c), (e));}
# endif
#else
# define assertHardware(p, c, e)
#endif

#if defined( C_ENABLE_INTERNAL_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertInternal(p, c, e)                          if (!(p))   {ApplCanFatalError(e);}
# else
#  define assertInternal(p, c, e)                          if (!(p))   {ApplCanFatalError((c), (e));}
# endif
#else
# define assertInternal(p, c, e)
#endif

#if !defined( CAN_DUMMY_STATEMENT )
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
#  define CAN_DUMMY_STATEMENT(x)                           (x) = (x)
# else
#  define CAN_DUMMY_STATEMENT(x)
# endif
#endif
#if defined( C_SINGLE_RECEIVE_CHANNEL )
# define CAN_CHANNEL_DUMMY_STATEMENT
# define CAN_HW_CHANNEL_DUMMY_STATEMENT
# define CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT
#else
# define CAN_CHANNEL_DUMMY_STATEMENT                       CAN_DUMMY_STATEMENT(channel)
# define CAN_HW_CHANNEL_DUMMY_STATEMENT                    CAN_DUMMY_STATEMENT(canHwChannel)
#  define CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT       CAN_DUMMY_STATEMENT(channel)
#endif

#if defined( C_ENABLE_TRANSMIT_QUEUE )
# define kCanTxQueueShift     5

/* mask used to get the flag index from the handle */
# define kCanTxQueueMask      (((vuint8)1 << kCanTxQueueShift) - (vuint8)1)

# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define CAN_HL_TXQUEUE_PADBITS(ch)                       ((CanTransmitHandle)0)
#  define CAN_HL_TXQUEUE_STARTINDEX(ch)                    ((CanSignedTxHandle)0)
#  define CAN_HL_TXQUEUE_STOPINDEX(ch)                     ((CanSignedTxHandle)kCanTxQueueSize)
# else
#  define CAN_HL_TXQUEUE_PADBITS(ch)                       ((CanTransmitHandle)CanTxQueuePadBits[ch])
#  define CAN_HL_TXQUEUE_STARTINDEX(ch)                    (CanTxQueueStartIndex[ch])
#  define CAN_HL_TXQUEUE_STOPINDEX(ch)                     (CanTxQueueStartIndex[(ch) + 1u])
# endif
#endif

#if defined(C_ENABLE_HW_LOOP_TIMER)
# define CanHL_ApplCanTimerStart(loop)   ApplCanTimerStart(CAN_CHANNEL_CANPARA_FIRST (loop))
# define CanHL_ApplCanTimerLoop(loop)    if (ApplCanTimerLoop(CAN_CHANNEL_CANPARA_FIRST (loop)) == 0u) { break; }
# define CanHL_ApplCanTimerEnd(loop)     ApplCanTimerEnd(CAN_CHANNEL_CANPARA_FIRST (loop))
#else
# define CanHL_ApplCanTimerStart(loop)
# define CanHL_ApplCanTimerLoop(loop)
# define CanHL_ApplCanTimerEnd(loop)
# endif

/* Compatibility macros for LL--------------------------------------------------------- */
# if defined( C_ENABLE_ACCESS_PROTECTED_REG_BY_APPL )
#  define CAN_WRITE_PROTECTED_REG8(area, regPtr, val)          ApplCanWriteProtectedRegister8((regPtr), (vuint8)0xFFu, (val))
#  define CAN_WRITE_PROTECTED_REG16(area, regPtr, val)         ApplCanWriteProtectedRegister16((regPtr), (vuint16)0xFFFFu, (val))
#  define CAN_WRITE_PROTECTED_REG32(area, regPtr, val)         ApplCanWriteProtectedRegister32((regPtr), (vuint32)0xFFFFFFFFu, (val))
#  define CAN_WRITE_PROTECTED_REG8_RESET(area, regPtr, bits)   ApplCanWriteProtectedRegister8((regPtr), (bits), (vuint8)0x00u)
#  define CAN_WRITE_PROTECTED_REG16_RESET(area, regPtr, bits)  ApplCanWriteProtectedRegister16((regPtr), (bits), (vuint16)0x0000u)
#  define CAN_WRITE_PROTECTED_REG32_RESET(area, regPtr, bits)  ApplCanWriteProtectedRegister32((regPtr), (bits), (vuint32)0x00000000u)
#  define CAN_WRITE_PROTECTED_REG8_SET(area, regPtr, bits)     ApplCanWriteProtectedRegister8((regPtr), (bits), (bits))
#  define CAN_WRITE_PROTECTED_REG16_SET(area, regPtr, bits)    ApplCanWriteProtectedRegister16((regPtr), (bits), (bits))
#  define CAN_WRITE_PROTECTED_REG32_SET(area, regPtr, bits)    ApplCanWriteProtectedRegister32((regPtr), (bits), (bits))
#  define CAN_READ_PROTECTED_REG8(area, regPtr)                ApplCanReadProtectedRegister8((regPtr))
#  define CAN_READ_PROTECTED_REG16(area, regPtr)               ApplCanReadProtectedRegister16((regPtr))
#  define CAN_READ_PROTECTED_REG32(area, regPtr)               ApplCanReadProtectedRegister32((regPtr))
# else
#  define CAN_WRITE_PROTECTED_REG8(area, regPtr, val)          *(regPtr) = (val)
#  define CAN_WRITE_PROTECTED_REG16(area, regPtr, val)         *(regPtr) = (val)
#  define CAN_WRITE_PROTECTED_REG32(area, regPtr, val)         *(regPtr) = (val)
#  define CAN_WRITE_PROTECTED_REG8_RESET(area, regPtr, bits)   *(regPtr) &= (vuint8)~(bits)
#  define CAN_WRITE_PROTECTED_REG16_RESET(area, regPtr, bits)  *(regPtr) &= (vuint16)~(bits)
#  define CAN_WRITE_PROTECTED_REG32_RESET(area, regPtr, bits)  *(regPtr) &= (vuint32)~(bits)
#  define CAN_WRITE_PROTECTED_REG8_SET(area, regPtr, bits)     *(regPtr) |= (bits)
#  define CAN_WRITE_PROTECTED_REG16_SET(area, regPtr, bits)    *(regPtr) |= (bits)
#  define CAN_WRITE_PROTECTED_REG32_SET(area, regPtr, bits)    *(regPtr) |= (bits)
#  define CAN_READ_PROTECTED_REG8(area, regPtr)                *(regPtr)
#  define CAN_READ_PROTECTED_REG16(area, regPtr)               *(regPtr)
#  define CAN_READ_PROTECTED_REG32(area, regPtr)               *(regPtr)
# endif

#if defined( C_ENABLE_CAN_FD_USED )
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
#  define CAN_HL_IS_CH_CANFD(ch)             ((CanFdUsage[ch] != C_CAN_FD_NONE) ? kCanTrue : kCanFalse)
# else
#  define CAN_HL_IS_CH_CANFD(ch)             kCanTrue
# endif
#endif
#if defined( C_ENABLE_CAN_FD_FULL )
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
#  define CAN_HL_IS_CH_CANFD_FULL(ch)        ((CanFdUsage[ch] == C_CAN_FD_MODE2) ? kCanTrue : kCanFalse)
# else
#  define CAN_HL_IS_CH_CANFD_FULL(ch)        kCanTrue
# endif
#endif

#if defined( C_ENABLE_CAN_FD_FULL )
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
#  define CAN_HL_MAX_LEN(ch)                 ((CAN_HL_IS_CH_CANFD_FULL(ch) == kCanTrue) ? 64u : 8u)
# else
#  define CAN_HL_MAX_LEN(ch)                 64u
# endif
#else
# define CAN_HL_MAX_LEN(ch)                  8u
#endif

#if !defined( C_CAN_FD_PADDING_VALUE )
# define C_CAN_FD_PADDING_VALUE              0x00u
#endif

/* PRQA S 3453 FctLikeMacroLabel */ /* MD_MSR_FctLikeMacro */


#if defined( C_ENABLE_HW_LOOP_TIMER )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define   CanLL_ApplCanTimerStart(loop)   ApplCanTimerStart(loop)
#  define   CanLL_ApplCanTimerLoop(loop)    ApplCanTimerLoop(loop)
#  define   CanLL_ApplCanTimerEnd(loop)     ApplCanTimerEnd(loop)
# else
#  define   CanLL_ApplCanTimerStart(loop)   ApplCanTimerStart(channel, (loop))
#  define   CanLL_ApplCanTimerLoop(loop)    ApplCanTimerLoop(channel, (loop))
#  define   CanLL_ApplCanTimerEnd(loop)     ApplCanTimerEnd(channel, (loop))
# endif
#else
# define   CanLL_ApplCanTimerStart(loop)
# define   CanLL_ApplCanTimerLoop(loop)     CAN_OK
# define   CanLL_ApplCanTimerEnd(loop)
#endif

  /* dummy area */
# define CAN_AREA(ch) 0

#if defined ( C_ENABLE_USER_MODE_APPL )
# define CAN_WRITE_PROTECTED_REG(reg, val)              ApplCanWriteProtectedRegister16(&(reg), (vuint16)0xFFFFu, (val))
# define CAN_WRITE_PROTECTED_REG_RESET(reg, bits)       ApplCanWriteProtectedRegister16(&(reg), (bits), (vuint16)0x0000u)
# define CAN_WRITE_PROTECTED_REG_SET(reg, bits)         ApplCanWriteProtectedRegister16(&(reg), (bits), (bits))
# define CAN_READ_PROTECTED_REG(reg)                    ApplCanReadProtectedRegister16(&(reg))
# define CAN_WRITE_PROTECTED_REG_RESET_32BIT(reg, bits) ApplCanWriteProtectedRegister32(&(reg), (bits), (vuint32)0x00000000u)
# define CAN_WRITE_PROTECTED_REG_SET_32BIT(reg, bits)   ApplCanWriteProtectedRegister32(&(reg), (bits), (bits))
# define CAN_READ_PROTECTED_REG_32BIT(reg)              ApplCanReadProtectedRegister32(&(reg))
#else
# define CAN_WRITE_PROTECTED_REG(reg, val)              (reg) = (val)
# define CAN_WRITE_PROTECTED_REG_RESET(reg, bits)       (reg) &= (vuint16)(~(bits))
# define CAN_WRITE_PROTECTED_REG_SET(reg, bits)         (reg) |= (bits)
# define CAN_READ_PROTECTED_REG(reg)                    (reg)
# define CAN_WRITE_PROTECTED_REG_RESET_32BIT(reg, bits) (reg) &= (vuint32)(~(bits))
# define CAN_WRITE_PROTECTED_REG_SET_32BIT(reg, bits)   (reg) |= (bits)
# define CAN_READ_PROTECTED_REG_32BIT(reg)              (reg)
#endif

#if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
# define   CAN_DISABLE_RXTX_INTERRUPT_0TO31(canFlagPtr) (canFlagPtr)->flags1 = pFlexcan->imask1; pFlexcan->imask1 = 0u
# define   CAN_RESTORE_RXTX_INTERRUPT_0TO31(canFlag)    pFlexcan->imask1 = (canFlag).flags1
# if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  define   CAN_DISABLE_RXTX_INTERRUPT_32TO63(canFlagPtr) (canFlagPtr)->flags2 = pFlexcan->imask2; pFlexcan->imask2 = 0u
#  define   CAN_RESTORE_RXTX_INTERRUPT_32TO63(canFlag)    pFlexcan->imask2 = (canFlag).flags2
# else
#  define   CAN_DISABLE_RXTX_INTERRUPT_32TO63(canFlagPtr)
#  define   CAN_RESTORE_RXTX_INTERRUPT_32TO63(canFlag)
# endif
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  define   CAN_DISABLE_RXTX_INTERRUPT_64TO95(canFlagPtr) (canFlagPtr)->flags3 = pFlexcan->imask3; pFlexcan->imask3 = 0u
#  define   CAN_RESTORE_RXTX_INTERRUPT_64TO95(canFlag)    pFlexcan->imask3 = (canFlag).flags3
# else
#  define   CAN_DISABLE_RXTX_INTERRUPT_64TO95(canFlagPtr)
#  define   CAN_RESTORE_RXTX_INTERRUPT_64TO95(canFlag)
# endif
# if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  define   CAN_DISABLE_RXTX_INTERRUPT_96TO127(canFlagPtr) (canFlagPtr)->flags4 = pFlexcan->imask4; pFlexcan->imask4 = 0u
#  define   CAN_RESTORE_RXTX_INTERRUPT_96TO127(canFlag)    pFlexcan->imask4 = (canFlag).flags4
# else
#  define   CAN_DISABLE_RXTX_INTERRUPT_96TO127(canFlagPtr)
#  define   CAN_RESTORE_RXTX_INTERRUPT_96TO127(canFlag)
# endif
#else
# define   CAN_DISABLE_RXTX_INTERRUPT_0TO31(canFlagPtr)
# define   CAN_RESTORE_RXTX_INTERRUPT_0TO31(canFlag)
# define   CAN_DISABLE_RXTX_INTERRUPT_32TO63(canFlagPtr)
# define   CAN_RESTORE_RXTX_INTERRUPT_32TO63(canFlag)
# define   CAN_DISABLE_RXTX_INTERRUPT_64TO95(canFlagPtr)
# define   CAN_RESTORE_RXTX_INTERRUPT_64TO95(canFlag)
# define   CAN_DISABLE_RXTX_INTERRUPT_96TO127(canFlagPtr)
# define   CAN_RESTORE_RXTX_INTERRUPT_96TO127(canFlag)
#endif

#if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
# define CAN_DISABLE_BUSOFF_INTERRUPT(canFlagPtr)  (canFlagPtr)->canctrl1 = pFlexcan->control1; pFlexcan->control1 &= kNotFlexCANErrBoff
# define CAN_RESTORE_BUSOFF_INTERRUPT(canFlag)     pFlexcan->control1 |= (vuint32)((canFlag).canctrl1 & (vuint32)(~kNotFlexCANErrBoff))
#else
# define CAN_DISABLE_BUSOFF_INTERRUPT(canFlagPtr)
# define CAN_RESTORE_BUSOFF_INTERRUPT(canFlag)
#endif

#if defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
# define CAN_DISABLE_WAKEUP_INTERRUPT(canFlagPtr)  (canFlagPtr)->canmcr = CAN_READ_PROTECTED_REG(pFlexcan->canmcr); CAN_WRITE_PROTECTED_REG_RESET(pFlexcan->canmcr, kFlexCAN_WAK_MSK)
# define CAN_RESTORE_WAKEUP_INTERRUPT(canFlag)     CAN_WRITE_PROTECTED_REG_SET(pFlexcan->canmcr, (vuint16)((canFlag).canmcr & kFlexCAN_WAK_MSK))
#else
# define CAN_DISABLE_WAKEUP_INTERRUPT(canFlagPtr)
# define CAN_RESTORE_WAKEUP_INTERRUPT(canFlag)
#endif

#define CanLL_TxIsObjFree(hwCh, txObjHandle) ((GLOBAL_MAILBOX_ACCESS((hwCh), (txObjHandle), control) & kCodeMask) == kTxCodeFree)

#if defined ( C_ENABLE_SLEEP_WAKEUP )
# if defined( C_ENABLE_FLEXCAN_STOP_MODE )
#  define CanLL_HwIsSleep(hwCh)  ((CAN_READ_PROTECTED_REG((pFlexCAN(hwCh))->canmcr) & kFlexCAN_MCR) == kFlexCAN_STOP_MODE)
# else
#  define CanLL_HwIsSleep(hwCh)  ((CAN_READ_PROTECTED_REG((pFlexCAN(hwCh))->canmcr) & kFlexCAN_MCR) == kFlexCAN_DISABLE_MODE)
# endif
#else
# define CanLL_HwIsSleep(hwCh)  (kCanFalse != kCanFalse)
#endif

#define CanLL_HwIsStop(hwCh) ((CAN_READ_PROTECTED_REG((pFlexCAN(hwCh))->canmcr) & kFlexCAN_MCR) == kFlexCAN_FREEZE_MODE)

#define CanLL_HwIsStopRequested(hwCh) ((CAN_READ_PROTECTED_REG((pFlexCAN(hwCh))->canmcr) & kFlexCAN_MCR_REQ_ONLY_BITS) == kFlexCAN_FREEZE_MODE_REQ)

#define CanLL_HwIsStart(hwCh) ((CAN_READ_PROTECTED_REG((pFlexCAN(hwCh))->canmcr) & kFlexCAN_MCR) == kFlexCAN_NORMAL_MODE)

#define CanLL_HwIsStartRequested(hwCh) ((CAN_READ_PROTECTED_REG((pFlexCAN(hwCh))->canmcr) & kFlexCAN_MCR_REQ_ONLY_BITS) == kFlexCAN_NORMAL_MODE_REQ)

#define CanLL_HwIsBusOff(hwCh) (((pFlexCAN(hwCh))->estat & kFlexCAN_FCS_BOFF) != 0u)

#define CanLL_HwIsAutoRecoveryActive(hwCh) (((pFlexCAN(hwCh))->control1 &  kFlexCAN_BOFF_REC) == 0u)

#define CanLL_HwIsListenOnlyMode(hwCh) (((pFlexCAN(hwCh))->control1 & kFlexCAN_LOM) != 0u)

#if defined( C_ENABLE_EXTENDED_STATUS )
#  define CanLL_HwIsPassive(hwCh) (((pFlexCAN(hwCh))->estat & kFlexCAN_FCS) == kFlexCAN_FCS_EP)

#  define CanLL_HwIsWarning(hwCh) (((pFlexCAN(hwCh))->estat & kFlexCAN_RXTX_WARN) != 0u)
#endif /* C_ENABLE_EXTENDED_STATUS */

# define CAN_WAKEUP(hwCh)   (void)CanWakeUp( hwCh )

# if defined ( C_ENABLE_TX_FULLCAN_DELAYED_START )
#  define CanGetMailboxDelayed(hwChan, hwobject)    (((canLL_canDelayedFullCANTxRequest[hwChan][(vuint8)(hwobject >>5)] >> ((vuint32)(hwobject & 0x001Fu))) & ((vuint32) 1)) == 1)
#  define CanSetMailboxDelayed(hwChan, hwobject)      (canLL_canDelayedFullCANTxRequest[hwChan][(vuint8)(hwobject >>5)] |=  (vuint32)(((vuint32) 1u) << (vuint32)(hwobject & 0x001Fu)))
#  define CanClearMailboxDelayed(hwChan, hwobject)    (canLL_canDelayedFullCANTxRequest[hwChan][(vuint8)(hwobject >>5)] &= (vuint32)~(((vuint32) 1u) << (vuint32)(hwobject & 0x001Fu)))
# endif

#define CanBitMask(x)      ((vuint32)((vuint32)0x01u << ((x) & (0x1Fu))))
#define CanInvBitMask(x)   ((vuint32)~((vuint32)0x01u << ((x) & (0x1Fu))))

/* PRQA L:FctLikeMacroLabel */

/* PRQA L:QAC_Can_Macros_C */

/* *********************************************************************** */
/* Defines / data types / structs / unions                                 */
/* *********************************************************************** */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
#endif

/* Define CAN Chip hardware; segment must be located in locator file    */
/* register layout of the can chip                                      */
/* Structure describing CAN receive buffer. */

typedef struct
{
  CanInitHandle            initObject;
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
  tCanRxId0                idRaw0;
# if( kCanNumberOfUsedCanRxIdTables > 1 )
  tCanRxId1                idRaw1;
# endif
# if( kCanNumberOfUsedCanRxIdTables > 2 )
  tCanRxId2                idRaw2;
# endif
# if( kCanNumberOfUsedCanRxIdTables > 3 )
  tCanRxId3                idRaw3;
# endif
# if( kCanNumberOfUsedCanRxIdTables > 4 )
  tCanRxId4                idRaw4;
# endif
  tCanIdType               idType;
#endif
  vuint8                   isInitOk;
} tCanInitParaStruct;
typedef tCanInitParaStruct *CanInitParaStructPtr;

#if defined( C_ENABLE_CAN_TRANSMIT )
typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
  CanObjectHandle          logTxObjHandle;
  tCanTxId0                idRaw0;
#if( kCanNumberOfUsedCanTxIdTables > 1 )
  tCanTxId1                idRaw1;
#endif
#if( kCanNumberOfUsedCanTxIdTables > 2 )
  tCanTxId2                idRaw2;
#endif
#if( kCanNumberOfUsedCanTxIdTables > 3 )
  tCanTxId3                idRaw3;
#endif
#if( kCanNumberOfUsedCanTxIdTables > 4 )
  tCanTxId4                idRaw4;
#endif
#if defined( C_ENABLE_MIXED_ID ) 
  tCanIdType               idType;
#endif
  tCanDlc                  dlcRaw;
#if defined( C_ENABLE_PRETRANSMIT_FCT )
  CanTxInfoStruct          txStruct;
#endif
#if defined( C_ENABLE_COPY_TX_DATA )
  TxDataPtr                CanMemCopySrcPtr;
#endif
#if defined( C_ENABLE_CAN_FD_USED )
  tCanFdType               fdType;
  tCanFdBrsType            fdBrsType;
#endif
#if defined( C_ENABLE_CAN_FD_FULL ) && defined( C_ENABLE_COPY_TX_DATA )
  vuint8                   messageLen;
  vuint8                   frameLen;
  vuint8                   paddingVal;
#endif
} tCanTxTransmissionParaStruct;
typedef tCanTxTransmissionParaStruct *CanTxTransmissionParaStructPtr;
#endif

#if defined( C_ENABLE_MSG_TRANSMIT )
typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
  CanObjectHandle          logTxObjHandle;
  CanMsgTransmitStructPtr  txMsgStruct;
} tCanTxMsgTransmissionParaStruct;
typedef tCanTxMsgTransmissionParaStruct *CanTxMsgTransmissionParaStructPtr;
#endif

#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
#if defined( C_ENABLE_OVERRUN )
  vuint8                   isOverrun;
#endif
  tCanRxInfoStruct         rxStruct;
} tCanRxBasicParaStruct;
typedef tCanRxBasicParaStruct *CanRxBasicParaStructPtr;
#endif

#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
#if defined( C_ENABLE_FULLCAN_OVERRUN )
  vuint8                   isOverrun;
#endif
  tCanRxInfoStruct         rxStruct;
} tCanRxFullParaStruct;
typedef tCanRxFullParaStruct *CanRxFullParaStructPtr;
#endif

#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )  || \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
typedef struct
{
  RxDataPtr                dest;
  CanChipDataPtr           src;
  vuint8                   len;
} tCanRxCopyParaStruct;
typedef tCanRxCopyParaStruct *CanRxCopyParaStructPtr;
#endif

#if defined( C_ENABLE_RX_QUEUE )
typedef struct
{
  CanMsgTransmitStructPtr  dest;
  CanRxInfoStructPtr       src;
} tCanRxQueueCopyParaStruct;
typedef tCanRxQueueCopyParaStruct *CanRxQueueCopyParaStructPtr;
#endif

#if defined( C_ENABLE_CANCEL_IN_HW )
typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
  CanObjectHandle          logTxObjHandle;
} tCanTxCancellationParaStruct;
typedef tCanTxCancellationParaStruct *CanTxCancellationParaStructPtr;
#endif

typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
  CanObjectHandle          logTxObjHandle;
#if defined( C_ENABLE_CAN_TX_CONF_FCT ) || defined( C_ENABLE_CAN_TX_CONF_MSG_ACCESS )
  CanTxInfoStructPtr       txStructConf;
#endif
  #if defined( C_CPUTYPE_LITTLEENDIAN ) /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
  # if (defined( C_ENABLE_CAN_TX_CONF_FCT ) && defined( C_ENABLE_CAN_TX_CONF_MSG_ACCESS )) || defined(CAN_ENABLE_GENERIC_CONFIRMATION_API2)
  #  if defined ( C_ENABLE_CAN_FD_FULL )
  vuint32 canDataBuffer[16];
  #  else
  vuint32 canDataBuffer[2];
  #  endif
  # endif
  #endif
} tCanTxConfirmationParaStruct;
typedef tCanTxConfirmationParaStruct *CanTxConfirmationParaStructPtr;

#if defined( C_ENABLE_TX_POLLING ) || \
    (defined( C_ENABLE_RX_FULLCAN_OBJECTS ) &&  defined( C_ENABLE_RX_FULLCAN_POLLING )) || \
    (defined( C_ENABLE_RX_BASICCAN_POLLING ) &&  defined( C_ENABLE_RX_BASICCAN_OBJECTS ))
typedef struct
{
  CanObjectHandle          mailboxHandle;
  CanObjectHandle          hwObjHandle;
} tCanTaskParaStruct;
typedef tCanTaskParaStruct *CanTaskParaStructPtr;
#endif


/* *********************************************************************** */
/* Constants                                                               */
/* *********************************************************************** */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* ROM CATEGORY 1 START */
/* lookup table for setting the flags in the queue */
V_MEMROM0 static V_MEMROM1 tCanQueueElementType V_MEMROM2 CanShiftLookUp[1u << kCanTxQueueShift] = 
{
   (tCanQueueElementType)0x00000001u, (tCanQueueElementType)0x00000002u, (tCanQueueElementType)0x00000004u, (tCanQueueElementType)0x00000008u,
   (tCanQueueElementType)0x00000010u, (tCanQueueElementType)0x00000020u, (tCanQueueElementType)0x00000040u, (tCanQueueElementType)0x00000080u

 , (tCanQueueElementType)0x00000100u, (tCanQueueElementType)0x00000200u, (tCanQueueElementType)0x00000400u, (tCanQueueElementType)0x00000800u,
   (tCanQueueElementType)0x00001000u, (tCanQueueElementType)0x00002000u, (tCanQueueElementType)0x00004000u, (tCanQueueElementType)0x00008000u

 , (tCanQueueElementType)0x00010000u, (tCanQueueElementType)0x00020000u, (tCanQueueElementType)0x00040000u, (tCanQueueElementType)0x00080000u,
   (tCanQueueElementType)0x00100000u, (tCanQueueElementType)0x00200000u, (tCanQueueElementType)0x00400000u, (tCanQueueElementType)0x00800000u,
   (tCanQueueElementType)0x01000000u, (tCanQueueElementType)0x02000000u, (tCanQueueElementType)0x04000000u, (tCanQueueElementType)0x08000000u,
   (tCanQueueElementType)0x10000000u, (tCanQueueElementType)0x20000000u, (tCanQueueElementType)0x40000000u, (tCanQueueElementType)0x80000000u
};

/* returns the highest pending flag from the lower nibble */
V_MEMROM0 static V_MEMROM1 vsint8 V_MEMROM2 CanGetHighestFlagFromNibble[16] = /* PRQA S 3218 */ /* MD_Can_GlobalScope */
{    
  (vsint8)-1,                /* (vsint8)0xFF - changed due to misra; cast due to R32C */
  0x00,
  0x01, 0x01,
  0x02, 0x02, 0x02, 0x02,
  0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03
};
/* ROM CATEGORY 1 END */
#endif

#if defined(C_ENABLE_CAN_FD_FULL)
/* ROM CATEGORY 1 START */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanDlcToFrameLenght[16] =
{
  /* 00..07 */  0,  1,  2,  3,  4,  5,  6,  7,
  /* 08..15 */  8, 12, 16, 20, 24, 32, 48, 64
};

V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanMessageLengthToDlc[65] =
{
  /* 00..07 */  0,  1,  2,  3,  4,  5,  6,  7,
  /* 08..15 */  8,  9,  9,  9,  9, 10, 10, 10,
  /* 16..23 */ 10, 11, 11, 11, 11, 12, 12, 12,
  /* 24..31 */ 12, 13, 13, 13, 13, 13, 13, 13,
  /* 32..39 */ 13, 14, 14, 14, 14, 14, 14, 14,
  /* 40..47 */ 14, 14, 14, 14, 14, 14, 14, 14,
  /* 48..55 */ 14, 15, 15, 15, 15, 15, 15, 15,
  /* 56..63 */ 15, 15, 15, 15, 15, 15, 15, 15,
  /* 64     */ 15
};
/* ROM CATEGORY 1 END */
#endif

/* Global constants with CAN driver main and subversion */
/* ROM CATEGORY 4 START */
V_DEF_CONST(V_NONE, vuint8, CONST) kCanMainVersion   = (vuint8)(( DRVCAN_IMXFLEXCAN3HLL_VERSION ) >> 8);
V_DEF_CONST(V_NONE, vuint8, CONST) kCanSubVersion    = (vuint8)(DRVCAN_IMXFLEXCAN3HLL_VERSION & (vuint16)0x00FFu);
V_DEF_CONST(V_NONE, vuint8, CONST) kCanBugFixVersion = (vuint8)(DRVCAN_IMXFLEXCAN3HLL_RELEASE_VERSION );
/* ROM CATEGORY 4 END */

#if defined( C_ENABLE_CAN_RAM_CHECK )
/* ROM CATEGORY 4 START */
/* test bit patterns for (Extended) RAM check */
V_DEF_CONST(CAN_STATIC, vuint32, CONST) ramCheckPattern32[3] = /* PRQA S 3218 */ /* MD_Can_GlobalScope */
{
  0xAAAAAAAAu, 0x55555555u, 0x00000000u
};
/* ROM CATEGORY 4 END */
#endif



/* *********************************************************************** */
/* External declarations                                                   */
/* *********************************************************************** */

#if !defined( CANDRV_SET_CODE_TEST_POINT )
# define CANDRV_SET_CODE_TEST_POINT(x)
#else
extern vuint8 tscCTKTestPointState[CTK_MAX_TEST_POINT];
#endif


/* *********************************************************************** */
/* Global data definitions                                                 */
/* *********************************************************************** */

/* RAM CATEGORY 1 START */
#if defined( C_ENABLE_PRECOPY_FCT )
volatile CanReceiveHandle canRxHandle[kCanNumberOfChannels];
#endif
/* RAM CATEGORY 1 END */

/* RAM CATEGORY 3 START */
#if defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_DYN_TX_OBJECTS )   && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
CanTransmitHandle          confirmHandle[kCanNumberOfChannels];
#endif
/* RAM CATEGORY 3 END */

/* RAM CATEGORY 1 START */
#if defined( C_ENABLE_CONFIRMATION_FLAG )
V_MEMRAM0 volatile V_MEMRAM1_NEAR union CanConfirmationBits V_MEMRAM2_NEAR CanConfirmationFlags; /* PRQA S 0759 */ /* MD_Can_Union */
#endif

#if defined( C_ENABLE_INDICATION_FLAG )
V_MEMRAM0 volatile V_MEMRAM1_NEAR union CanIndicationBits   V_MEMRAM2_NEAR CanIndicationFlags; /* PRQA S 0759 */ /* MD_Can_Union */
#endif
/* RAM CATEGORY 1 END */

/* RAM CATEGORY 1 START */
#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
/* ##RI1.4 - 3.31: Dynamic Receive DLC */
volatile vuint8 canVariableRxDataLen[kCanNumberOfRxObjects];
#endif
/* RAM CATEGORY 1 END */

/* RAM CATEGORY 1 START */
CanChipDataPtr canRDSRxPtr[kCanNumberOfChannels]; /* PRQA S 1514 */ /* MD_Can_ExternalScope */
/* RAM CATEGORY 1 END */
/* RAM CATEGORY 1 START */
CanChipDataPtr canRDSTxPtr[kCanNumberOfTxMailboxes]; /* PRQA S 1514 */ /* MD_Can_ExternalScope */
/* RAM CATEGORY 1 END */

# if defined( C_ENABLE_RX_BASICCAN_OBJECTS ) || defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/* RAM CATEGORY 1 START*/
V_DEF_VAR(CAN_STATIC, tCanRxMsgBuffer, VAR_NOINIT) canRxMsgBuffer[kCanNumberOfHwChannels]; /* PRQA S 3218 */ /* MD_Can_GlobalScope */
/* RAM CATEGORY 1 END*/
# endif
# if defined( C_CPUTYPE_LITTLEENDIAN ) && defined(C_ENABLE_PRETRANSMIT_FCT)
/* RAM CATEGORY 1 START*/
V_DEF_VAR(CAN_STATIC, tCanTxMsgBuffer, VAR_NOINIT) canTxMsgBuffer[kCanNumberOfTxObjects]; /* PRQA S 3218 */ /* MD_Can_GlobalScope */
/* RAM CATEGORY 1 END*/
# endif

/* *********************************************************************** */
/* Local data definitions                                                  */
/* *********************************************************************** */

/* support for CAN driver features : */
/* RAM CATEGORY 1 START */
static volatile CanTransmitHandle canHandleCurTxObj[kCanNumberOfTxMailboxes];
/* RAM CATEGORY 1 END */

/* RAM CATEGORY 2 START */
#if defined( C_ENABLE_ECU_SWITCH_PASS )
static vuint8 canPassive[kCanNumberOfChannels];
#endif
/* RAM CATEGORY 2 END */

/* RAM CATEGORY 2 START */
#if defined( C_ENABLE_CAN_RAM_CHECK )
static vuint8 canComStatus[kCanNumberOfChannels]; /* stores the decision of the App after the last CAN RAM check */
#endif

#if defined( C_ENABLE_DYN_TX_OBJECTS )
static volatile vuint8 canTxDynObjReservedFlag[kCanNumberOfTxDynObjects];

# if defined( C_ENABLE_DYN_TX_ID )
static tCanTxId0 canDynTxId0[kCanNumberOfTxDynObjects];
#  if (kCanNumberOfUsedCanTxIdTables > 1)
static tCanTxId1 canDynTxId1[kCanNumberOfTxDynObjects];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 2)
static tCanTxId2 canDynTxId2[kCanNumberOfTxDynObjects];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 3)
static tCanTxId3 canDynTxId3[kCanNumberOfTxDynObjects];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 4)
static tCanTxId4 canDynTxId4[kCanNumberOfTxDynObjects];
#  endif
#  if defined( C_ENABLE_MIXED_ID )
static tCanIdType                 canDynTxIdType[kCanNumberOfTxDynObjects];
#  endif
#  if defined( C_ENABLE_CAN_FD_USED )
static tCanFdType                 canDynTxFdType[kCanNumberOfTxDynObjects];
#  endif
# endif

# if defined( C_ENABLE_DYN_TX_DLC )
static tCanDlc                    canDynTxDLC[kCanNumberOfTxDynObjects];
#  if defined( C_ENABLE_CAN_FD_FULL ) && (defined( C_ENABLE_PRETRANSMIT_FCT ) || defined( C_ENABLE_COPY_TX_DATA ))
static vuint8                     canDynTxMessageLength[kCanNumberOfTxDynObjects];
#  endif
# endif
# if defined( C_ENABLE_DYN_TX_DATAPTR )
static TxDataPtr                  canDynTxDataPtr[kCanNumberOfTxDynObjects];
# endif
# if defined( C_ENABLE_CONFIRMATION_FCT )
# endif
#endif /* C_ENABLED_DYN_TX_OBJECTS */

#if defined( C_ENABLE_TX_MASK_EXT_ID )
static tCanTxId0 canTxMask0[kCanNumberOfChannels];
# if (kCanNumberOfUsedCanTxIdTables > 1)
static tCanTxId1 canTxMask1[kCanNumberOfChannels];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
static tCanTxId2 canTxMask2[kCanNumberOfChannels];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
static tCanTxId3 canTxMask3[kCanNumberOfChannels];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
static tCanTxId4 canTxMask4[kCanNumberOfChannels];
# endif
#endif

#if defined( C_ENABLE_VARIABLE_DLC )
static tCanDlc canTxDLC_RAM[kCanNumberOfTxObjects];
#endif

static volatile vuint8 canStatus[kCanNumberOfChannels];

# if defined( C_ENABLE_PART_OFFLINE )
static vuint8 canTxPartOffline[kCanNumberOfChannels];
# endif
/* RAM CATEGORY 2 END */

/* RAM CATEGORY 1 START */
#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)
static vsintx          canCanInterruptCounter[kCanNumberOfChannels];
# if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
static tCanLLCanIntOld canCanInterruptOldStatus[kCanNumberOfHwChannels];
# endif
#endif
/* RAM CATEGORY 1 END */

/* RAM CATEGORY 4 START */
static CanInitHandle lastInitObject[kCanNumberOfChannels];
/* RAM CATEGORY 4 END */

#if defined( C_ENABLE_TX_POLLING )          || \
    defined( C_ENABLE_RX_FULLCAN_POLLING )  || \
    defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
    defined( C_ENABLE_ERROR_POLLING )       || \
    defined( C_ENABLE_WAKEUP_POLLING )      
/* RAM CATEGORY 2 START */
static vuint8 canPollingTaskActive[kCanNumberOfChannels];
/* RAM CATEGORY 2 END */
#endif

/* RAM CATEGORY 1 START */
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )  || \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
static tCanRxInfoStruct        canRxInfoStruct[kCanNumberOfChannels];
#endif
#if defined( C_ENABLE_CAN_TX_CONF_FCT )
/* ##RI-1.10 Common Callbackfunction in TxInterrupt */
static tCanTxConfInfoStruct    txInfoStructConf[kCanNumberOfChannels];
#endif

#if defined( C_ENABLE_COND_RECEIVE_FCT )
static volatile vuint8 canMsgCondRecState[kCanNumberOfChannels];
#endif

#if defined( C_ENABLE_RX_QUEUE )
# if defined( C_ENABLE_RX_QUEUE_EXTERNAL )
static tCanRxQueue* canRxQueuePtr;     /* pointer to the rx queue */
#  define canRxQueue                                       (*canRxQueuePtr)
# else
static tCanRxQueue canRxQueue;         /* the rx queue (buffer and queue variables) */
# endif
#endif
/* RAM CATEGORY 1 END */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* RAM CATEGORY 1 START */
static volatile tCanQueueElementType canTxQueueFlags[kCanTxQueueSize];
/* RAM CATEGORY 1 END */
#endif


# if defined (C_ENABLE_GET_CONTEXT) || defined (C_ENABLE_SET_CONTEXT)
#  if !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
/* RAM CATEGORY 1 START*/
V_DEF_VAR(CAN_STATIC, CANSFR_TYPE, VAR_NOINIT) canTxIntMask[kCanNumberOfHwChannels][4];
/* RAM CATEGORY 1 END*/
#  endif
# endif
# if defined (VGEN_ENABLE_MDWRAP) || defined (VGEN_ENABLE_QWRAP) || defined( C_ENABLE_UPDATE_BASE_ADDRESS )
/* RAM CATEGORY 1 START */
V_DEF_P2VAR(V_NONE, vuint32, VAR_NOINIT, REG_CANCELL) canLL_VirtualPtrFieldTemp[kCanNumberOfHwChannels]; /* PRQA S 3408 */ /* MD_Can_ExternalScope */
/* RAM CATEGORY 1 END */
# endif
# if defined (C_ENABLE_TX_FULLCAN_DELAYED_START)
/* RAM CATEGORY 1 START*/
V_DEF_VAR(CAN_STATIC volatile, CANSFR_TYPE, VAR_NOINIT) canLL_canDelayedFullCANTxRequest[kCanNumberOfChannels][4];
/* RAM CATEGORY 1 END*/
# endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */
/* Can LL Init State variable */

#if defined(C_ENABLE_SLEEP_WAKEUP)
/* RAM CATEGORY 1 START */
V_DEF_VAR(CAN_STATIC volatile, vuint8, VAR_NOINIT) canLL_canSleepState[kCanNumberOfChannels];
/* RAM CATEGORY 1 END */
/* RAM CATEGORY 1 START */
V_DEF_VAR(CAN_STATIC volatile, vuint8, VAR_NOINIT) canLL_canWakeUpState[kCanNumberOfChannels];
/* RAM CATEGORY 1 END */
#endif

/* RAM CATEGORY 1 START */
V_DEF_VAR(CAN_STATIC volatile, vuint8, VAR_NOINIT) canLL_canStartReinitState[kCanNumberOfChannels];
/* RAM CATEGORY 1 END */

#if defined (VGEN_ENABLE_MDWRAP) || defined (VGEN_ENABLE_QWRAP) || defined( C_ENABLE_UPDATE_BASE_ADDRESS )
/* RAM CATEGORY 1 START */
V_DEF_P2VAR(V_NONE, vuint32, VAR_NOINIT, REG_CANCELL) canLL_VirtualPtrField[kCanNumberOfHwChannels]; /* PRQA S 3408 */ /* MD_Can_ExternalScope */
/* RAM CATEGORY 1 END */
#endif
#if defined (C_ENABLE_HW_LOOP_TIMER)
/* RAM CATEGORY 1 START */
V_DEF_VAR(CAN_STATIC volatile, vuint8, VAR_NOINIT) canLL_FlexCANInitResultNeeded[kCanNumberOfHwChannels];
/* RAM CATEGORY 1 END */
#endif

/* *********************************************************************** */
/*  Local function prototypes                                              */
/* *********************************************************************** */

/* CODE CATEGORY 4 START */
static vuint8 CanHL_ReInit( CAN_CHANNEL_CANTYPE_FIRST vuint8 suppressRamCheck );
/* CODE CATEGORY 4 END */
/* CODE CATEGORY 4 START */
CAN_LOCAL_INLINE void CanHL_CleanUpSendState( CAN_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 4 END */

#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )  || \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
# if defined( C_ENABLE_RX_QUEUE )
/* CODE CATEGORY 1 START */
static vuint8 CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 1 END */
# endif
/* CODE CATEGORY 1 START */
# if ( kCanNumberOfRxObjects > 0 )
#  if defined( C_ENABLE_RX_QUEUE )
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_FIRST tCanRxInfoStruct *pCanRxInfoStruct );
#  else
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_ONLY );
#  endif
# endif /* ( kCanNumberOfRxObjects > 0 ) */
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_INDICATION_FLAG ) || \
     defined( C_ENABLE_INDICATION_FCT )
/* CODE CATEGORY 1 START */
static void CanHL_IndRxHandle( CanReceiveHandle rxHandle );
/* CODE CATEGORY 1 END */
# endif
#endif
#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/* CODE CATEGORY 1 START */
static void CanHL_BasicCanMsgReceived( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle );
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_RX_BASICCAN_POLLING )
/* CODE CATEGORY 1 START */
CAN_LOCAL_INLINE void CanHL_BasicCanMsgReceivedPolling( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle );
/* CODE CATEGORY 1 END */
# endif
#endif
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/* CODE CATEGORY 1 START */
static void CanHL_FullCanMsgReceived( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle );
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_RX_FULLCAN_POLLING )
/* CODE CATEGORY 1 START */
CAN_LOCAL_INLINE void CanHL_FullCanMsgReceivedPolling( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle );
/* CODE CATEGORY 1 END */
# endif
#endif

/* CODE CATEGORY 1 START */
static void CanHL_TxConfirmation( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle, CanObjectHandle txMailboxElement, CanObjectHandle txObjHandle );
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_TX_POLLING )
/* CODE CATEGORY 1 START */
CAN_LOCAL_INLINE void CanHL_TxConfirmationPolling( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle, CanObjectHandle txMailboxElement, CanObjectHandle txObjHandle );
/* CODE CATEGORY 1 END */
# endif
# if defined( C_ENABLE_CAN_TRANSMIT )
/* CODE CATEGORY 1 START */
static vuint8 CanHL_CopyDataAndStartTransmission( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle, CanTransmitHandle txHandle ) C_API_3;
/* CODE CATEGORY 1 END */
# endif /* C_ENABLE_CAN_TRANSMIT */
#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* CODE CATEGORY 4 START */
static void CanHL_DelQueuedObj( CAN_CHANNEL_CANTYPE_ONLY ) C_API_3;
/* CODE CATEGORY 4 END */
# if defined( C_ENABLE_TX_POLLING ) 
/* CODE CATEGORY 2 START */
static void CanHL_RestartTxQueue( CAN_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 2 END */
# endif
#endif

/* CODE CATEGORY 2 START */
static void CanHL_ErrorHandling( CAN_HW_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 2 END */

#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
/* CODE CATEGORY 1 START */
static void CanHL_SetVariableRxDatalen( CanReceiveHandle rxHandle, vuint8 dataLen );
/* CODE CATEGORY 1 END */
#endif

# if defined( C_ENABLE_INTERRUPT_SOURCE_SINGLE ) /* COV_CAN_FLEXCAN3_DERIVATIVE_INTERRUPT_SOURCE_SINGLE */
#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )   || \
      defined( C_ENABLE_CAN_BUSOFF_INTERRUPT ) || \
      defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
/************************************************************************************************************
 *  CanInterrupt()
 ***********************************************************************************************************/
/*! \brief         Handles all interrupt events.
 *  \details       Checks interrupt events and calls the indication or confirmation function.
 *  \param[in]     Controller          CAN controller (configuration dependent)
 *  \context       ISR1|ISR2
 *  \reentrant     TRUE for different handles
 *  \synchronous   TRUE
 *  \config        C_ENABLE_CAN_RXTX_INTERRUPT || C_ENABLE_CAN_BUSOFF_INTERRUPT || C_ENABLE_CAN_WAKEUP_INTERRUPT
 *  \pre           -
 ***********************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 1 END */
#  endif
# endif

# if defined ( C_ENABLE_INTERRUPT_SOURCE_MULTIPLE ) /* COV_CAN_FLEXCAN3_DERIVATIVE_INTERRUPT_SOURCE_MULTIPLE */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
/************************************************************************************************************
 *  CanBusOffInterrupt()
 ***********************************************************************************************************/
/*! \brief         Handles BUSOFF interrupt events.
 *  \details       Calls error handling.
 *  \param[in]     Controller          CAN controller (configuration dependent)
 *  \context       ISR1|ISR2
 *  \reentrant     TRUE for different handles
 *  \synchronous   TRUE
 *  \config        C_ENABLE_CAN_BUSOFF_INTERRUPT
 *  \pre           -
 ***********************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanBusOffInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 1 END */
#  endif
#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/************************************************************************************************************
 *  CanMailboxInterrupt()
 ***********************************************************************************************************/
/*! \brief         Handles transmission and reception interrupt events.
 *  \details       Checks interrupt events and calls the indication or confirmation function.
 *  \param[in]     Controller          CAN controller (configuration dependent)
 *  \context       ISR1|ISR2
 *  \reentrant     TRUE for different handles
 *  \synchronous   TRUE
 *  \config        C_ENABLE_CAN_RXTX_INTERRUPT
 *  \pre           -
 ***********************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanMailboxInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 1 END */
#  endif
# endif



/************************************************************************************************************
 *  CanLL_WriteReg16()
 ***********************************************************************************************************/
/*! \brief         Write value and do RamCheck
 *  \details       Write the value of the hardware mailbox or controller register and read back for RamCheck.
 *                 set initPara->isChRamCheckFail or initPara->isMbRamCheckFail in case of an HW issue to deactivate hardware.
 *  \param[in]     Controller       CAN controller.
 *                                  (only if not using "Optimize for one controller")
 *  \param[in]     regPtr           valid pointer to Register to be written.
 *  \param[in]     value            value to be written.
 *  \param[in]     readMask         mask to check the read back value.
 *  \param[in]     doRamCheck       execute the RAM check (kCanExecuteRamCheck, kCanSuppressRamCheck).
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WriteReg16(CAN_CHANNEL_CANTYPE_FIRST CanChipMsgPtr16 regPtr, vuint16 value, vuint16 readMask, CanInitParaStructPtr initPara);
/* CODE CATEGORY 4 END */

/************************************************************************************************************
 *  CanLL_WriteReg32()
 ***********************************************************************************************************/
/*! \brief         Write value and do RamCheck
 *  \details       Write the value of the hardware mailbox or controller register and read back for RamCheck.
 *                 set initPara->isChRamCheckFail or initPara->isMbRamCheckFail in case of an HW issue to deactivate hardware.
 *  \param[in]     Controller       CAN controller.
 *                                  (only if not using "Optimize for one controller")
 *  \param[in]     regPtr           valid pointer to Register to be written.
 *  \param[in]     value            value to be written.
 *  \param[in]     readMask         mask to check the read back value.
 *  \param[in]     doRamCheck       execute the RAM check (kCanExecuteRamCheck, kCanSuppressRamCheck).
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WriteReg32(CAN_CHANNEL_CANTYPE_FIRST CanChipMsgPtr32 regPtr, vuint32 value, vuint32 readMask, CanInitParaStructPtr initPara);
/* CODE CATEGORY 4 END */


/************************************************************************************************************
 *  CanLL_WriteProtectedReg16()
 ***********************************************************************************************************/
/*! \brief         Write value via OS and do RamCheck
 *  \details       Write the value of the hardware mailbox or controller register and read back for RamCheck.
 *                 set initPara->isChRamCheckFail or initPara->isMbRamCheckFail in case of an HW issue to deactivate hardware.
 *  \param[in]     Controller       CAN controller.
 *                                  (only if not using "Optimize for one controller")
 *  \param[in]     area             Memory area to be written.
 *  \param[in]     regPtr           valid pointer to Register to be written.
 *  \param[in]     value            Value to be written.
 *  \param[in]     readMask         Mask to check the read back value.
 *  \param[in]     doRamCheck       execute the RAM check (kCanExecuteRamCheck, kCanSuppressRamCheck).
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WriteProtectedReg16(CAN_CHANNEL_CANTYPE_FIRST vuint16 area, CanChipMsgPtr16 regPtr, vuint16 value, vuint16 readMask, CanInitParaStructPtr initPara);
/* CODE CATEGORY 4 END */


/**********************************************************************************************************************
 *  CanLL_InitBegin()
 *********************************************************************************************************************/
/*! \brief         Starts the channel initialization
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitBegin( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */


/**********************************************************************************************************************
 *  CanLL_InitBeginSetRegisters()
 *********************************************************************************************************************/
/*! \brief         Performs the channel register initialization
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitBeginSetRegisters( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_InitMailboxTx()
 *********************************************************************************************************************/
/*! \brief         Initializes a transmit mailbox
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitMailboxTx( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */



#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_InitMailboxRxFullCan()
 *********************************************************************************************************************/
/*! \brief         Initializes a receive FullCAN mailbox
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitMailboxRxFullCan( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */


#endif /* C_ENABLE_RX_FULLCAN_OBJECTS */

#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_InitMailboxRxBasicCan()
 *********************************************************************************************************************/
/*! \brief         Initializes a receive BasicCAN mailbox
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitMailboxRxBasicCan( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */

#endif /* C_ENABLE_RX_BASICCAN_OBJECTS */


/**********************************************************************************************************************
 *  CanLL_InitEndSetRegisters()
 *********************************************************************************************************************/
/*! \brief         Finishes the channel register initialization
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitEndSetRegisters( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_InitEnd()
 *********************************************************************************************************************/
/*! \brief         Finishes the channel initialization
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitEnd( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */

#if defined( C_ENABLE_CAN_RAM_CHECK )
/**********************************************************************************************************************
 *  CanLL_InitIsMailboxCorrupt()
 *********************************************************************************************************************/
/*! \brief         Performs the RAM check for a mailbox
 *  \details       Called by CanInit()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] initPara            Pointer to local variables of CanInit()
 *  \return        kCanFalse           Mailbox is not corrupt
 *                 kCanTrue            Mailbox is considered corrupt or failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_STATIC, vuint8, STATIC_CODE) CanLL_InitIsMailboxCorrupt( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara );
/* CODE CATEGORY 4 END */
#endif


/**********************************************************************************************************************
 *  CanLL_InitPowerOn()
 *********************************************************************************************************************/
/*! \brief         Performs the hardware specific global module initialization
 *  \details       Called by CanInitPowerOn()
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitPowerOn( void );
/* CODE CATEGORY 4 END */


/**********************************************************************************************************************
 *  CanLL_InitPowerOnChannelSpecific()
 *********************************************************************************************************************/
/*! \brief         Performs the channel dependent hardware specific global module initialization
 *  \details       Called by CanInitPowerOn()
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \return        kCanOk              Successfully completed
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitPowerOnChannelSpecific( CAN_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 4 END */

#if ((defined( C_ENABLE_CAN_TRANSMIT ) && defined( C_ENABLE_CAN_CANCEL_TRANSMIT )) ||  defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT ) ) && defined( C_ENABLE_CANCEL_IN_HW )
/**********************************************************************************************************************
 *  CanLL_TxCancelInHw()
 *********************************************************************************************************************/
/*! \brief         Performs cancellation of the transmission in hardware
 *  \details       Called by cancellation
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txCancellationPara  Pointer to local variables of cancellation
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 3 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxCancelInHw(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxCancellationParaStructPtr txCancellationPara);
/* CODE CATEGORY 3 END */
#endif
#if defined( C_ENABLE_CAN_TRANSMIT ) 
/**********************************************************************************************************************
 *  CanLL_TxBegin()
 *********************************************************************************************************************/
/*! \brief         Perform start of transmission
 *  \details       Called by transmission to prepare send object
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of transmission
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */
/**********************************************************************************************************************
 *  CanLL_TxSetMailbox()
 *********************************************************************************************************************/
/*! \brief         Set mailbox data for transmission
 *  \details       Called by transmission to set ID, DLC
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of transmission
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxSetMailbox(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_PRETRANSMIT_FCT ) 
/**********************************************************************************************************************
 *  CanLL_TxSetTxStruct()
 *********************************************************************************************************************/
/*! \brief         Set mailbox TxStruct for transmission
 *  \details       Called by transmission to set TxStruct
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of transmission
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxSetTxStruct(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */

/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxPretransmitCopyToCan(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */
# endif
# if defined( C_ENABLE_COPY_TX_DATA ) 
/**********************************************************************************************************************
 *  CanLL_TxCopyToCan()
 *********************************************************************************************************************/
/*! \brief         Set mailbox data for transmission
 *  \details       Called by transmission to set data part in mailbox
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of transmission
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxCopyToCan(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */
# endif
/**********************************************************************************************************************
 *  CanLL_TxStart()
 *********************************************************************************************************************/
/*! \brief         Trigger mailbox to start the transmission
 *  \details       Called by transmission to start transmission
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of transmission
 *  \return        kCanOk              No issue
 *                 kCanFailed          Issue occur
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxStart(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */
/**********************************************************************************************************************
 *  CanLL_TxEnd()
 *********************************************************************************************************************/
/*! \brief         Perform end handling of the transmission
 *  \details       Called by transmission to finish transmission
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of transmission
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara);
/* CODE CATEGORY 1 END */
#endif
#if defined( C_ENABLE_TX_POLLING )
/**********************************************************************************************************************
 *  CanLL_TxIsGlobalConfPending()
 *********************************************************************************************************************/
/*! \brief         Check global pending of transmission
 *  \details       Called by transmission to get global pending flag
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \return        kCanTrue            pending confirmation
 *                 kCanFailed          no pending confirmation
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxIsGlobalConfPending(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
/**********************************************************************************************************************
 *  CanLL_TxProcessPendings()
 *********************************************************************************************************************/
/*! \brief         Check pending of transmission object
 *  \details       Called by transmission to get pending flag of mailbox
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txPara              Pointer to local variables of confirmation task
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTaskParaStructPtr taskPara);
/* CODE CATEGORY 2 END */
#endif
/**********************************************************************************************************************
 *  CanLL_TxConfBegin()
 *********************************************************************************************************************/
/*! \brief         Perform start of confirmation
 *  \details       Called by confirmation
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txConfPara          Pointer to local variables of confirmation
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxConfBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara);
/* CODE CATEGORY 1 END */
#if (defined( C_ENABLE_TX_OBSERVE ) || defined( C_ENABLE_CAN_TX_CONF_FCT ) ) && defined( C_ENABLE_CANCEL_IN_HW )
/**********************************************************************************************************************
 *  CanLL_TxConfIsMsgTransmitted()
 *********************************************************************************************************************/
/*! \brief         Check message is transmitted
 *  \details       Called by confirmation
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txConfPara          Pointer to local variables of confirmation
 *  \return        kCanTrue            message is transmitted
 *                 kCanFailed          message is not transmitted
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxConfIsMsgTransmitted(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara);
/* CODE CATEGORY 1 END */
#endif
#if (defined( C_ENABLE_CAN_TX_CONF_FCT ) && defined( C_ENABLE_CAN_TX_CONF_MSG_ACCESS )) || defined(CAN_ENABLE_GENERIC_CONFIRMATION_API2)
/**********************************************************************************************************************
 *  CanLL_TxConfSetTxConfStruct()
 *********************************************************************************************************************/
/*! \brief         Set confirmation struct
 *  \details       Called by confirmation to set confirmation data struct
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txConfPara          Pointer to local variables of confirmation
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxConfSetTxConfStruct(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara);
/* CODE CATEGORY 1 END */
#endif
/**********************************************************************************************************************
 *  CanLL_TxConfEnd()
 *********************************************************************************************************************/
/*! \brief         Perform confirmation end handling
 *  \details       Called by confirmation
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] txConfPara          Pointer to local variables of confirmation
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxConfEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara);
/* CODE CATEGORY 1 END */
#if defined( C_ENABLE_MSG_TRANSMIT )
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxBeginMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara);
/* CODE CATEGORY 2 END */
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxCopyMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara);
/* CODE CATEGORY 2 END */
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxStartMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara);
/* CODE CATEGORY 2 END */
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxEndMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara);
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_MSG_TRANSMIT */

#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_RxBasicMsgReceivedBegin()
 *********************************************************************************************************************/
/*! \brief         Perform BasicCAN receive begin
 *  \details       Called by reception to begin handling
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxBasicPara         Pointer to local variables of reception
 *  \return        kCanOk              No Issue occur
 *                 kCanFailed          Issue occur
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxBasicMsgReceivedBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxBasicParaStructPtr rxBasicPara);
/* CODE CATEGORY 1 END */
/**********************************************************************************************************************
 *  CanLL_RxBasicReleaseObj()
 *********************************************************************************************************************/
/*! \brief         Release BasicCAN mailbox
 *  \details       Called by reception to release object
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxBasicPara         Pointer to local variables of reception
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxBasicReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxBasicParaStructPtr rxBasicPara);
/* CODE CATEGORY 1 END */
/**********************************************************************************************************************
 *  CanLL_RxBasicMsgReceivedEnd()
 *********************************************************************************************************************/
/*! \brief         Release BasicCAN receive end
 *  \details       Called by reception to finish handling
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxBasicPara         Pointer to local variables of reception
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxBasicMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxBasicParaStructPtr rxBasicPara);
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_RX_BASICCAN_POLLING )
/**********************************************************************************************************************
 *  CanLL_RxBasicIsGlobalIndPending()
 *********************************************************************************************************************/
/*! \brief         Check BasicCAN receive global pending
 *  \details       Called by reception to get pending state
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \return        kCanTrue            global pending
 *                 kCanFailed          no pending
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxBasicIsGlobalIndPending(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
/**********************************************************************************************************************
 *  CanLL_RxBasicProcessPendings()
 *********************************************************************************************************************/
/*! \brief         Check BasicCAN receive pending
 *  \details       Called by reception to get mailbox pending state
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] taskPara            Pointer to local variables of reception task
 *  \return        kCanTrue            mailbox pending
 *                 kCanFailed          no pending
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxBasicProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTaskParaStructPtr taskPara);
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_RX_BASICCAN_POLLING */
#endif /* C_ENABLE_RX_BASICCAN_OBJECTS */
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_RxFullMsgReceivedBegin()
 *********************************************************************************************************************/
/*! \brief         Perform FullCAN receive begin
 *  \details       Called by reception to begin handling
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxBasicPara         Pointer to local variables of reception
 *  \return        kCanOk              No Issue occur
 *                 kCanFailed          Issue occur
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxFullMsgReceivedBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxFullParaStructPtr rxFullPara);
/* CODE CATEGORY 1 END */
/**********************************************************************************************************************
 *  CanLL_RxFullReleaseObj()
 *********************************************************************************************************************/
/*! \brief         Release FullCAN mailbox
 *  \details       Called by reception to release object
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxBasicPara         Pointer to local variables of reception
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxFullReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxFullParaStructPtr rxFullPara);
/* CODE CATEGORY 1 END */
/**********************************************************************************************************************
 *  CanLL_RxFullMsgReceivedEnd()
 *********************************************************************************************************************/
/*! \brief         Release FullCAN receive end
 *  \details       Called by reception to finish handling
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxBasicPara         Pointer to local variables of reception
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxFullMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxFullParaStructPtr rxFullPara);
/* CODE CATEGORY 1 END */
# if defined( C_ENABLE_RX_FULLCAN_POLLING )
/**********************************************************************************************************************
 *  CanLL_RxFullIsGlobalIndPending()
 *********************************************************************************************************************/
/*! \brief         Check FullCAN receive global pending
 *  \details       Called by reception to get pending state
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \return        kCanTrue            global pending
 *                 kCanFailed          no pending
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxFullIsGlobalIndPending(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
/**********************************************************************************************************************
 *  CanLL_RxFullProcessPendings()
 *********************************************************************************************************************/
/*! \brief         Check FullCAN receive pending
 *  \details       Called by reception to get mailbox pending state
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] taskPara            Pointer to local variables of reception task
 *  \return        kCanTrue            mailbox pending
 *                 kCanFailed          no pending
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxFullProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTaskParaStructPtr taskPara);
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_RX_FULLCAN_POLLING */
#endif /* C_ENABLE_RX_FULLCAN_OBJECTS */
#if (defined( C_ENABLE_RX_FULLCAN_OBJECTS ) || defined( C_ENABLE_RX_BASICCAN_OBJECTS )) && ( kCanNumberOfRxObjects > 0 ) && defined( C_ENABLE_COPY_RX_DATA )
/**********************************************************************************************************************
 *  CanLL_RxCopyFromCan()
 *********************************************************************************************************************/
/*! \brief         Perform copy receive data
 *  \details       Called by reception to get received data
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in,out] rxCopyPara          Pointer to local variables of reception
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxCopyFromCan(CAN_CHANNEL_CANTYPE_FIRST CanRxCopyParaStructPtr rxCopyPara);
/* CODE CATEGORY 1 END */
#endif

/**********************************************************************************************************************
 *  CanLL_ErrorHandlingBegin()
 *********************************************************************************************************************/
/*! \brief         Perform error handling begin
 *  \details       Called by error handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ErrorHandlingBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
/**********************************************************************************************************************
 *  CanLL_BusOffOccured()
 *********************************************************************************************************************/
/*! \brief         Check BusOff occur
 *  \details       Called by error handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \return        kCanTrue            BusOff occur
 *                 kCanFailed          no BusOff occur
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_BusOffOccured(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
/**********************************************************************************************************************
 *  CanLL_ErrorHandlingEnd()
 *********************************************************************************************************************/
/*! \brief         Perform error handling end
 *  \details       Called by error handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ErrorHandlingEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
#if defined( C_ENABLE_EXTENDED_STATUS )
/**********************************************************************************************************************
 *  CanLL_GetStatusBegin()
 *********************************************************************************************************************/
/*! \brief         Perform read out of status information
 *  \details       Called by GetStatus API
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 3 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_GetStatusBegin(CAN_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 3 END */
#endif
#if defined( C_ENABLE_SLEEP_WAKEUP ) && defined( C_ENABLE_WAKEUP_POLLING )
/**********************************************************************************************************************
 *  CanLL_WakeUpOccured()
 *********************************************************************************************************************/
/*! \brief         Check Wakeup occur
 *  \details       Called by wakeup handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \return        kCanTrue            Wakeup occur
 *                 kCanFailed          no Wakeup occur
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_WakeUpOccured(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 2 END */
#endif
#if defined( C_ENABLE_SLEEP_WAKEUP )
# if (defined( C_ENABLE_CAN_WAKEUP_INTERRUPT ) || defined( C_ENABLE_WAKEUP_POLLING )) 
/**********************************************************************************************************************
 *  CanLL_WakeUpHandling()
 *********************************************************************************************************************/
/*! \brief         Perform Wakeup handling
 *  \details       Called by wakeup handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WakeUpHandling(CAN_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */
# endif
#endif
/**********************************************************************************************************************
 *  CanLL_ModeTransition()
 *********************************************************************************************************************/
/*! \brief         Perform mode change
 *  \details       Called by mode handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \param[in]     mode                mode to be set
 *  \param[in]     busOffRecovery      BusOff recovery should be done or not
 *  \param[in]     ramCheck            RamCheck should be done or not
 *  \return        kCanOk              Successfully completed
 *                 kCanRequested       Mode not yet reached
 *                 kCanFailed          Failure occured
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_STATIC, vuint8, STATIC_CODE) CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 mode, vuint8 busOffRecovery, vuint8 ramCheck);
/* CODE CATEGORY 4 END */
/**********************************************************************************************************************
 *  CanLL_StopReinit()
 *********************************************************************************************************************/
/*! \brief         Perform reinitialization for mode change
 *  \details       Called by mode handler
 *  \param[in]     channel             Index of the CAN channel (only if multiple channels are used)
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
**********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_StopReinit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */
#if defined( C_ENABLE_RX_QUEUE )
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxQueueCopyMsgObj(CAN_CHANNEL_CANTYPE_FIRST CanRxQueueCopyParaStructPtr rxQueueCopyPara);
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxQueueSetRxInfoStructExtension(CAN_CHANNEL_CANTYPE_FIRST CanRxInfoStructPtr rxStructPtr);
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_RX_QUEUE */

#if (defined( C_ENABLE_CAN_RXTX_INTERRUPT )    || \
     defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )  || \
     defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )) 
/************************************************************************************************************
 *  CanLL_CanInterruptDisable()
 ***********************************************************************************************************/
/*! \brief         Disables CAN interrupts
 *  \details       Saves the current state (enable or disable) of all 
 *                 CAN interrupt sources to localInterruptOldFlagPtr
 *                 and disables all CAN interrupt sources (RX, TX, error, wakeup).
 *  \param[in]     canHwChannel              CAN channel
 *  \param[out]    localInterruptOldFlagPtr  Pointer to global variable that holds the interrupt state
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_CanInterruptDisable(CAN_HW_CHANNEL_CANTYPE_FIRST tCanLLCanIntOldPtr localInterruptOldFlagPtr);
/* CODE CATEGORY 1 END */

/************************************************************************************************************
 *  CanLL_CanInterruptRestore()
 **********************************************************************************************************/
/*! \brief         Restores CAN interrupts.
 *  \details       Restores the previous state of all CAN interrupt sources (RX, TX, error, wakeup).
 *  \param[in]     canHwChannel             CAN channel
 *  \param[in]     localInterruptOldFlag    Global variable that holds the interrupt state
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_CanInterruptRestore(CAN_HW_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld localInterruptOldFlag);
/* CODE CATEGORY 1 END */
#endif

# if defined(VGEN_ENABLE_MDWRAP) || defined(VGEN_ENABLE_QWRAP) || defined(C_ENABLE_UPDATE_BASE_ADDRESS)
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_BaseAddressRequest(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_BaseAddressActivate(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */
# endif
# if defined(C_ENABLE_GET_CONTEXT)
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_GetModuleContext(tCanModuleContextStructPtr pContext);
/* CODE CATEGORY 4 END */
# endif
# if defined(C_ENABLE_SET_CONTEXT)
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_ModuleContextVersion(tCanModuleContextStructPtr pContext);
/* CODE CATEGORY 4 END */
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_SetModuleContext(tCanModuleContextStructPtr pContext);
/* CODE CATEGORY 4 END */
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_FinalizeSetModuleContext(tCanModuleContextStructPtr pContext);
/* CODE CATEGORY 4 END */
# endif


/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_SetFlexCANToInitMode()
 ***********************************************************************************************************/
/*! \brief         Set FlexCAN Controller to INIT mode.
 *  \details       Sets the FlexCAN Controller from any mode to INIT mode.
 *  \param[in]     Controller     CAN controller (Controller must be in valid range).
 *                                (only if not using "Optimize for one controller")
 *  \param[in]     CanInitHandle  handle to INIT-structure
 *  \return        kCanRequested: FlexCAN successfully set to init mode
 *  \return        kCanFailed:    FlexCAN failure
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_SetFlexCANToInitMode(CAN_HW_CHANNEL_CANTYPE_FIRST CanInitHandle initObject);
/* CODE CATEGORY 4 END */
/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_ExecuteSoftReset()
 ***********************************************************************************************************/
/*! \brief         Perform FlexCAN soft-reset
 *  \details       Triggers the FlexCAN Controller to perform internal soft-reset.
 *  \param[in]     Controller     CAN controller (Controller must be in valid range).
 *                                (only if not using "Optimize for one controller")
 *  \return        kCanRequested: SoftReset ongoing
 *  \return        kCanOk:        SoftReset successfully finished
 *  \return        kCanFailed:    transition failure
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_ExecuteSoftReset(CAN_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_ReInit()
 ***********************************************************************************************************/
/*! \brief         Perform Reintialisation of FlexCAN controller.
 *  \details       Use CanHL_ReInit() for reinitalization of FlexCAN Controller.
 *  \param[in]     channel        current application CAN channel (must be in valid range).
 *                                (only if not using "Optimize for one controller")
 *  \return        kCanOk:        Transition to START successfully finished
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_ReInit(CAN_CHANNEL_CANTYPE_FIRST vuint8 doRamCheck);
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_StopTransition()
 ***********************************************************************************************************/
/*! \brief         Perform FlexCAN for stop mode and control the transition.
 *  \details       Request stop mode to FlexCAN Controller and control the transition.
 *  \param[in]     channel          current application CAN channel
 *  \param[in]     canHwChannel     current HW CAN channel
 *  \param[in]     busOffRecovery   flag whether BusOff recovery shall be terminated (if relevant and possible)
 *  \param[in]     doRamCheck       flag whether RamCheck shall be executed (if relevant and possible)
 *  \return        kCanRequested:   Transition to STOP ongoing
 *  \return        kCanOk           Transition to STOP successfully finished
 *  \return        kCanFailed       Transition to STOP not possible
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_StopTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 busOffRecovery, vuint8 doRamCheck);
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_StartTransition()
 ***********************************************************************************************************/
/*! \brief         Perform FlexCAN start mode and control the transition
 *  \details       Request start mode to FlexCAN Controller and control the transition.
 *  \param[in]     channel          current application CAN channel
 *  \param[in]     canHwChannel     current HW CAN channel
 *  \param[in]     mode             target mode for the transition
 *  \param[in]     doRamCheck       flag whether RamCheck shall be executed (if relevant and possible)
 *  \return        kCanRequested:   Transition to START ongoing
 *  \return        kCanOk:          Transition to START successfully finished
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_StartTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 mode, vuint8 doRamCheck);
/* CODE CATEGORY 4 END */

#if defined ( C_ENABLE_BUSOFF_RECOVERY_COMPLIANT )
/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_BusOffEndTransition()
 ***********************************************************************************************************/
/*! \brief         Perform FlexCAN BusOffEnd transition and control the transition
 *  \details       Request BusOff Auto Recovery Mode to FlexCAN Controller. When busoff end is detected
 *                 request start mode and control the transition.
 *  \param[in]     channel          current application CAN channel
 *  \param[in]     canHwChannel     current HW CAN channel
 *  \param[in]     doRamCheck       flag whether RamCheck shall be executed (if relevant and possible)
 *  \return        kCanRequested:   Transition to START ongoing
 *  \return        kCanOk:          Transition to START successfully finished
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_BusOffEndTransition(CAN_HW_CHANNEL_CANTYPE_FIRST vuint8 doRamCheck);
/* CODE CATEGORY 4 END */
#endif

#if defined(C_ENABLE_SLEEP_WAKEUP)
/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_Sleep()
 ***********************************************************************************************************/
/*! \brief         Perform FlexCAN sleep mode
 *  \details       Request sleep mode to FlexCAN Controller and control the transition.
 *  \param[in]     Controller     CAN controller (Controller must be in valid range).
 *                                (only if not using "Optimize for one controller")
 *  \return        kCanRequested: Transition to SLEEP ongoing
 *  \return        kCanOk:        Transition to SLEEP successfully finished
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_Sleep(CAN_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */
/* CODE CATEGORY 4 START */
/************************************************************************************************************
 *  CanLL_WakeUp()
 ***********************************************************************************************************/
/*! \brief         Perform a wake-up to FlexCAN from sleep mode
 *  \details       Request started mode to FlexCAN Controller and control the transition.
 *  \param[in]     Controller     CAN controller (Controller must be in valid range).
 *                                (only if not using "Optimize for one controller")
 *  \return        kCanRequested: Transition to START ongoing
 *  \return        kCanOk:        Transition to START successfully finished
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_WakeUp(CAN_HW_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END */
#endif


#if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/* CODE CATEGORY 1 START */
/************************************************************************************************************
 *  CanLL_ComputeMailboxInterrupt()
 ***********************************************************************************************************/
/*! \brief         Computes Mailbox interrupt.
 *  \details       Determines the index of the Mailbox that notified an interrupt and call
 *                 CanLL_ComputeInterruptType.
 *  \param[in]     Controller    CAN controller (Controller must be in valid range).
 *                               (only if not using "Optimize for one controller")
 *  \param[in]     iFlags        Interrupt Flags that should be used for computation.
 *  \param[in]     iMask         Interrupt Mask that determines which iFlags are considered in interrupt context.
 *  \param[in]     startIndex    Index of first Mailbox that is part of the corresponding iFlags/iMask values.
 *  \pre           -
 *  \context       ISR1|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \config        C_ENABLE_CAN_RXTX_INTERRUPT
***********************************************************************************************************/
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanLL_ComputeMailboxInterrupt(CAN_HW_CHANNEL_CANTYPE_FIRST vuint32 iFlags, vuint32 iMask, CanObjectHandle startIndex);
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/************************************************************************************************************
 *  CanLL_ComputeInterruptType()
 ***********************************************************************************************************/
/*! \brief         Computes the communication type of the notified Mailbox interrupt (Tx/Rx FullCAN/BasicCAN).
 *  \details       Determines which communication event occurred and calls internal notification function.
 *  \param[in]     Controller    CAN controller (Controller must be in valid range)
 *                               (only if not using "Optimize for one controller")
 *  \param[in]     index         Index of the Mailbox that notified an interrupt.
 *  \pre           -
 *  \context       ISR1|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \config        C_ENABLE_CAN_RXTX_INTERRUPT
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ComputeInterruptType(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle index);
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/************************************************************************************************************
 *  CanLL_ComputeInterruptType()
 ***********************************************************************************************************/
/*! \brief         Checks all mailbox interrupt flags (Tx/Rx FullCAN/BasicCAN).
 *  \details       Determines which mailbox interrupt flags are set and processes it accordingly.
 *  \param[in]     Controller    CAN controller (Controller must be in valid range).
 *                               (only if not using "Optimize for one controller")
 *  \pre           -
 *  \context       ISR1|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \config        C_ENABLE_CAN_RXTX_INTERRUPT
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_CanMailboxInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 1 END */
#endif

/* CODE CATEGORY 1 START */
/************************************************************************************************************
 *  CanLL_ClearPendingInterrupt()
 ***********************************************************************************************************/
/*! \brief         Clears the interrupt flag related to the hwObjectHandle.
 *  \details       Determines which which interrupt flags needs to be cleared and clears the bit related to
 *                 the hwObjectHandle.
 *  \param[in]     pFlexCANLocal    base address of the CAN controller
 *  \param[in]     hwObjHandle      current hardware object to handle
 *  \pre           -
 *  \context       ISR1|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \config        -
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ClearPendingInterrupt( tFlexCANPtr pFlexCANLocal, CanObjectHandle hwObjHandle );
/* CODE CATEGORY 1 END */

#if defined (C_ENABLE_TX_POLLING) || ( defined (C_ENABLE_RX_FULLCAN_POLLING) && defined (C_ENABLE_RX_FULLCAN_OBJECTS) ) || ( (defined (C_ENABLE_RX_BASICCAN_POLLING) && defined (C_ENABLE_RX_BASICCAN_OBJECTS)) && defined (C_ENABLE_CLASSIC_MAILBOX_LAYOUT) )
/* CODE CATEGORY 1 START */
/************************************************************************************************************
 *  CanLL_GetInterruptFlag()
 ***********************************************************************************************************/
/*! \brief         Gets the corresponding interrupt flag related to a hwObjectHandle.
 *  \details       Determines which which interrupt flags needs to be returned related to the hwObjectHandle.
 *  \param[in]     pFlexCANLocal    base address of the CAN controller
 *  \param[in]     hwObjHandle      current hardware object to handle
 *  \return        iFlags           returns the content of the iFlag related to hwObjHandle
 *  \pre           -
 *  \context       ISR1|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \config        -
***********************************************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint32, STATIC_CODE) CanLL_GetInterruptFlag( tFlexCANPtr pFlexCANLocal, CanObjectHandle hwObjHandle );
/* CODE CATEGORY 1 END */
#endif


/* *********************************************************************** */
/*  Error Check                                                            */
/* *********************************************************************** */

/* *************** error check for Organi process ************************ */






/* *************** error check for not supported feature  ****************** */




#if defined( C_ENABLE_COMMON_CAN )
# error "Common CAN is not supported with this CAN driver implementation"
#endif

#if defined( C_ENABLE_MULTI_ECU_CONFIG )
# error "Multiple Configuration is not supported with this CAN driver implementation"
#endif

#if defined( C_ENABLE_CAN_RAM_CHECK_EXTENDED )
# error "Extended RAM Check is not supported with this CAN driver implementation"
#endif

#if (VSTDLIB__COREHLL_VERSION  <  0x0213 )
# error "Incompatible version of VStdLib. Core Version 2.13.00 or higher is necessary."
#endif


#if (kCanNumberOfHwChannels > 255)
#  error "Too many CAN channels. This driver can only handle up to 255 channels"
#endif

#if defined( C_SEARCH_HASH )
# if !defined( kHashSearchListCountEx )
#  error "kHashSearchListCountEx not defined"
# endif
# if !defined( kHashSearchMaxStepsEx )
#  error "kHashSearchMaxStepsEx not defined"
# endif
# if !defined( kHashSearchListCount )
#  error "kHashSearchListCount not defined"
# endif
# if !defined( kHashSearchMaxSteps )
#  error "kHashSearchMaxSteps not defined"
# endif
# if ( (kHashSearchMaxStepsEx < 1) || (kHashSearchMaxStepsEx > 32768) )
#  error "kHashSearchMaxStepsEx has ilegal value"
# endif
# if ( kHashSearchListCountEx > 32768 )
#  error "Hash table for extended ID is too large"
# endif
# if ( (kHashSearchMaxSteps < 1) || (kHashSearchMaxSteps > 32768) )
#  error "kHashSearchMaxStepsEx has ilegal value"
# endif
# if ( kHashSearchListCount > 32768 )
#  error "Hash table for standard ID is too large"
# endif
# if !defined( C_ENABLE_EXTENDED_ID) && (kHashSearchListCountEx > 0)
#  error "kHashSearchListCountEx has to be 0 in this configuration"
# endif
# if defined( C_ENABLE_EXTENDED_ID) && !defined( C_ENABLE_MIXED_ID) && (kHashSearchListCount > 0)
#  error "kHashSearchListCount has to be 0 in this configuration"
# endif
#endif



#if defined( C_ENABLE_RX_QUEUE )
# if !defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
#  error "RX-Queue requires C_HL_ENABLE_RX_INFO_STRUCT_PTR"
# endif
#endif

#if defined( C_SEARCH_INDEX ) && defined( C_ENABLE_EXTENDED_ID )
# error "Index search is not possible with extended IDs"
#endif

#if defined(C_ENABLE_GET_CONTEXT) && !defined( CANHL_TX_QUEUE_BIT )
# error "Context switch is not supported in combination with Tx Byte queue"
#endif

#if defined ( CAN_POLLING_IRQ_DISABLE ) || defined ( CAN_POLLING_IRQ_RESTORE )
# error "Feature has changed - use C_DISABLE_CAN_CAN_INTERRUPT_CONTROL to remove the CAN interrupt disabling"
#endif

#if defined( C_ENABLE_CAN_TX_CONF_MSG_ACCESS ) && defined( C_ENABLE_ECU_SWITCH_PASS )
# error "Access to transmitted message is not supported in combination with passive mode"
#endif


#if defined( C_ENABLE_CAN_FD_FULL ) && !defined( C_ENABLE_CAN_FD_USED )
# error "CAN-FD Mode2 requires CAN-FD to be enabled"
#endif

#if defined( C_ENABLE_CAN_FD_FULL ) && defined( C_ENABLE_VARIABLE_DLC )
# error "CanTransmitVarDLC() is not supported in combination with CAN-FD Mode2"
#endif



/* *********************************************************************** */
/*  Functions                                                              */
/* *********************************************************************** */

/* PRQA S 6010, 6030, 6050, 6080 QAC_Can_Functions    */ /* Function metrics are only measured */
/* PRQA S 2001, 2889             QAC_Can_Functions_HL */ /* MD_Can_Goto, MD_Can_MultipleReturn */

/* **************************************************************************
| NAME:             CanInit
| CALLED BY:        CanInitPowerOn(), Network management
| PRECONDITIONS:    none
| INPUT PARAMETERS: Handle to initstructure
| RETURN VALUES:    none
| DESCRIPTION:      initialization with a specified init structure
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanInit( CAN_CHANNEL_CANTYPE_FIRST CanInitHandle initObject )
{
  vuint8 localReturnCode;

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif
  assertUser((initObject + CAN_HL_INIT_OBJ_STARTINDEX(channel)) < CAN_HL_INIT_OBJ_STOPINDEX(channel), channel, kErrorInitObjectHdlTooLarge); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */

  lastInitObject[channel] = initObject;  /* PRQA S 2842 */ /* MD_Can_Assertion */

  localReturnCode = CanLL_ModeTransition(CAN_CHANNEL_CANPARA_FIRST kCanModeStartReinit, kCanFinishBusOffRecovery, kCanExecuteRamCheck);

  if (localReturnCode == kCanRequested)
  {
    CanHL_ApplCanTimerStart(kCanLoopInit);
    do
    {
      localReturnCode = CanLL_ModeTransition(CAN_CHANNEL_CANPARA_FIRST kCanModeStartReinit, kCanFinishBusOffRecovery, kCanExecuteRamCheck);
      CanHL_ApplCanTimerLoop(kCanLoopInit);
    } while (localReturnCode != kCanOk);
    CanHL_ApplCanTimerEnd(kCanLoopInit);
  }
}
/* CODE CATEGORY 4 END */


/* **************************************************************************
| NAME:             CanHL_ReInit
| CALLED BY:        CanLL_ModeTransition()
| PRECONDITIONS:    none
| INPUT PARAMETERS: suppression of RAM check during initialization
| RETURN VALUES:    kCanOk, kCanFailed
| DESCRIPTION:      initialization of controller registers
|                   initialization of receive and transmit message objects
************************************************************************** */
/* CODE CATEGORY 4 START */
static vuint8 CanHL_ReInit( CAN_CHANNEL_CANTYPE_FIRST vuint8 suppressRamCheck )
{
#if defined( C_ENABLE_CAN_RAM_CHECK )
  vuint8                 canRamCheckStatus;
  vuint8                 localMailboxIsCorrupt;
#endif
  CanObjectHandle        mailboxHandle;
#if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || \
    defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
  CanTransmitHandle      txHandle;
#endif
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
  CanReceiveHandle       rxHandle;
#endif
  CanObjectHandle        logTxObjHandle;
  tCanInitParaStruct     initPara;


#if defined( C_ENABLE_CAN_RAM_CHECK )
  canRamCheckStatus = kCanOk;
#endif
  initPara.isInitOk = kCanTrue;
  initPara.initObject = lastInitObject[channel] + CAN_HL_INIT_OBJ_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

  {
    /* begin the channel initialization */
    if (CanLL_InitBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara) != kCanOk) /* PRQA S 2992, 2996 */ /* MD_Can_ConstValue */
    {
      initPara.isInitOk = kCanFalse; /* PRQA S 2880 */ /* MD_Can_ConstValue */
    }

    if (initPara.isInitOk == kCanTrue) /* PRQA S 2991, 2995 */ /* MD_Can_ConstValue */
    {

      /* begin the initialization of the channel registers */
      if (CanLL_InitBeginSetRegisters(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara) != kCanOk)
      {
        initPara.isInitOk = kCanFalse;
      }
    }

    if (initPara.isInitOk == kCanTrue)
    {
      /* init Tx mailboxes -------------------------------------------------------- */
      {
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
        assertGen( ((vsintx)CAN_HL_MB_TX_STOPINDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel))
                   == (vsintx)CAN_HL_LOG_MB_TX_STOPINDEX(canHwChannel), channel, kErrorHwToLogTxObjCalculation); /* ESCAN00062667 */
        assertGen( ((vsintx)CAN_HL_MB_TX_STARTINDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel))
                   == (vsintx)CAN_HL_LOG_MB_TX_STARTINDEX(canHwChannel), channel, kErrorHwToLogTxObjCalculation); /* ESCAN00062667 */
        assertGen( CAN_HL_LOG_MB_TX_STARTINDEX(canHwChannel) <= CAN_HL_LOG_MB_TX_STOPINDEX(canHwChannel), channel, kErrorHwToLogTxObjCalculation);
#endif

        for (mailboxHandle=CAN_HL_MB_TX_STARTINDEX(canHwChannel); mailboxHandle<CAN_HL_MB_TX_STOPINDEX(canHwChannel); mailboxHandle++ )
        {
          logTxObjHandle = (CanObjectHandle)((vsintx)mailboxHandle + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2985, 4393 */ /* MD_Can_ConstValue, MD_Can_MixedSigns */
          initPara.mailboxHandle = mailboxHandle;
          initPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(mailboxHandle);

#if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || \
    defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
          if((canStatus[channel] & kCanHwIsInit) == kCanHwIsInit)
          {
            /* inform application, if a pending transmission is canceled */
            txHandle = canHandleCurTxObj[logTxObjHandle];

# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
            if( txHandle < kCanNumberOfTxObjects )
            {
              APPLCANCANCELNOTIFICATION(channel, txHandle);
            }
# endif
# if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
            if( txHandle == kCanBufferMsgTransmit )
            {
              APPLCANMSGCANCELNOTIFICATION(channel);
            }
# endif
          }
#endif

          canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                   /* MsgObj is free */

# if defined( C_ENABLE_CAN_RAM_CHECK )
          /* perform the mailbox RAM check */
          {
            localMailboxIsCorrupt = CanLL_InitIsMailboxCorrupt(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara);
          }
# endif /* C_ENABLE_CAN_RAM_CHECK */

          /* initialize the mailbox */
          (void) CanLL_InitMailboxTx(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara);

# if defined( C_ENABLE_CAN_RAM_CHECK )
          if (localMailboxIsCorrupt == kCanTrue)
          {
#  if defined( C_ENABLE_NOTIFY_CORRUPT_MAILBOX )
            ApplCanCorruptMailbox(CAN_CHANNEL_CANPARA_FIRST mailboxHandle);
#  endif
            canRamCheckStatus = kCanFailed;
          }

# endif /* C_ENABLE_CAN_RAM_CHECK */
        } /* iterate mailboxHandles */
      }

      /* init unused mailboxes ---------------------------------------------------- */
      /* unused objects are not individually disabled */


#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
      /* init Rx FullCAN mailboxes ------------------------------------- */
      {

        for (mailboxHandle=CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel); mailboxHandle<CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel); mailboxHandle++ )
        {
          initPara.mailboxHandle = mailboxHandle;
          initPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(mailboxHandle);

#  if defined( C_ENABLE_CAN_RAM_CHECK )
          /* perform the mailbox RAM check */
          {
            localMailboxIsCorrupt = CanLL_InitIsMailboxCorrupt(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara);
          }
#  endif /* C_ENABLE_CAN_RAM_CHECK */

          rxHandle = (mailboxHandle-CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel)) + CAN_HL_RX_FULL_STARTINDEX(canHwChannel); /* PRQA S 2985 */ /* MD_Can_ConstValue */
          initPara.idRaw0 = CanGetRxId0(rxHandle);
#  if (kCanNumberOfUsedCanRxIdTables > 1)
          initPara.idRaw1 = CanGetRxId1(rxHandle);
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 2)
          initPara.idRaw2 = CanGetRxId2(rxHandle);
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 3)
          initPara.idRaw3 = CanGetRxId3(rxHandle);
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 4)
          initPara.idRaw4 = CanGetRxId4(rxHandle);
#  endif
          initPara.idType = CanGetRxIdType(rxHandle);

          /* initialize the mailbox */
          (void) CanLL_InitMailboxRxFullCan(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara);

#  if defined( C_ENABLE_CAN_RAM_CHECK )
          if (localMailboxIsCorrupt == kCanTrue)
          {
#   if defined( C_ENABLE_NOTIFY_CORRUPT_MAILBOX )
            ApplCanCorruptMailbox(CAN_CHANNEL_CANPARA_FIRST mailboxHandle);
#   endif
            canRamCheckStatus = kCanFailed;
          }

#  endif /* C_ENABLE_CAN_RAM_CHECK */
        } /* iterate mailboxHandles */
      }
#endif /* C_ENABLE_RX_FULLCAN_OBJECTS */

#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
      /* init Rx BasicCAN mailboxes ------------------------------------ */
      for (mailboxHandle=CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel); mailboxHandle<CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel); mailboxHandle++ )
      {
        initPara.mailboxHandle = mailboxHandle;
        initPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(mailboxHandle);

#  if defined( C_ENABLE_CAN_RAM_CHECK )
        /* perform the mailbox RAM check */
        {
          localMailboxIsCorrupt = CanLL_InitIsMailboxCorrupt(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara);
        }
#  endif /* C_ENABLE_CAN_RAM_CHECK */

        /* initialize the mailbox */
        (void) CanLL_InitMailboxRxBasicCan(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara);

#  if defined( C_ENABLE_CAN_RAM_CHECK )
        if (localMailboxIsCorrupt == kCanTrue)
        {
#   if defined( C_ENABLE_NOTIFY_CORRUPT_MAILBOX )
          ApplCanCorruptMailbox(CAN_CHANNEL_CANPARA_FIRST mailboxHandle);
#   endif
          canRamCheckStatus = kCanFailed;
        }

#  endif /* C_ENABLE_CAN_RAM_CHECK */
      } /* iterate mailboxHandles */
#endif /* C_ENABLE_RX_BASICCAN_OBJECTS */

#if defined( C_ENABLE_INIT_POST_PROCESS )
      /* The post processing function is called within the initialization mode of the CAN controller.
         The application can directly overwrite existing configuration in the CAN controller.
         Example: overwriting the baudrate settings with dynamic values, necessary macros are provided in the LL part */
      ApplCanInitPostProcessing( CAN_CHANNEL_CANPARA_ONLY );
#endif


      /* finish the initialization of the channel registers */
      if (CanLL_InitEndSetRegisters(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara) != kCanOk) /* PRQA S 2992, 2996 */ /* MD_Can_ConstValue */
      {
        initPara.isInitOk = kCanFalse; /* PRQA S 2880 */ /* MD_Can_ConstValue */
      }
    } /* initPara.isInitOk == kCanTrue */


    /* finish the channel initialization */
    if (CanLL_InitEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &initPara) != kCanOk) /* PRQA S 2992, 2996 */ /* MD_Can_ConstValue */
    {
      initPara.isInitOk = kCanFalse; /* PRQA S 2880 */ /* MD_Can_ConstValue */
    }
  } /* end of loop over all hw channels */

#if defined( C_ENABLE_TX_OBSERVE )
  ApplCanInit( CAN_CHANNEL_CANPARA_FIRST CAN_HL_LOG_MB_TX_STARTINDEX(canHwChannel), CAN_HL_LOG_MB_TX_STOPINDEX(canHwChannel) );
#endif
#if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
  APPL_CAN_MSGTRANSMITINIT( CAN_CHANNEL_CANPARA_ONLY );
#endif

#if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canRamCheckStatus == kCanFailed)
  {
    /* the application decides whether the communication shall be disabled */
    if (ApplCanMemCheckFailed(CAN_CHANNEL_CANPARA_ONLY) == kCanDisableCommunication)
    {
      canComStatus[channel] = kCanDisableCommunication;
    }
  }
#endif

  CAN_DUMMY_STATEMENT(suppressRamCheck); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return(initPara.isInitOk);
}
/* END OF CanHL_ReInit */
/* CODE CATEGORY 4 END */


/* **************************************************************************
| NAME:             CanHL_CleanUpSendState
| CALLED BY:        CanLL_ModeTransition()
| PRECONDITIONS:    Call only after re-initialization
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      initialization of transmission status
************************************************************************** */
/* CODE CATEGORY 4 START */
CAN_LOCAL_INLINE void CanHL_CleanUpSendState( CAN_CHANNEL_CANTYPE_ONLY )
{
  CanObjectHandle  logTxObjHandle;

  for (logTxObjHandle=CAN_HL_LOG_MB_TX_STARTINDEX(canHwChannel); logTxObjHandle<CAN_HL_LOG_MB_TX_STOPINDEX(canHwChannel); logTxObjHandle++ )
  {
# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || \
     defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
    /* inform application, if a pending transmission is canceled */
    CanTransmitHandle txHandle = canHandleCurTxObj[logTxObjHandle];

#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
    if( txHandle < kCanNumberOfTxObjects )
    {
      APPLCANCANCELNOTIFICATION(channel, txHandle);
    }
#  endif
#  if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
    if( txHandle == kCanBufferMsgTransmit )
    {
      APPLCANMSGCANCELNOTIFICATION(channel);
    }
#  endif
# endif

    /* free mailbox */
    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;
  }
}
/* CODE CATEGORY 4 END */


/* **************************************************************************
| NAME:             CanInitPowerOn
| CALLED BY:        Application
| PRECONDITIONS:    This function must be called by the application before
|                   any other CAN driver function
|                   Interrupts must be disabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      Initialization of the CAN controller
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanInitPowerOn( void )
{

#if defined( C_ENABLE_VARIABLE_DLC )        || \
    defined( C_ENABLE_DYN_TX_OBJECTS )      || \
    defined( C_ENABLE_CONFIRMATION_FLAG )
  CanTransmitHandle txHandle;
#endif
#if defined( C_ENABLE_VARIABLE_RX_DATALEN ) || \
    defined( C_ENABLE_INDICATION_FLAG )
  CanReceiveHandle rxHandle;
#endif
  CAN_CHANNEL_CANTYPE_LOCAL

#if defined( VGEN_ENABLE_QWRAP )
#else
  VStdInitPowerOn();
#endif

#if defined( C_ENABLE_VARIABLE_DLC )
  for (txHandle = 0; txHandle < kCanNumberOfTxObjects; txHandle++)
  {
    assertGen(XT_TX_DLC(CanGetTxDlc(txHandle))<(vuint8)9, kCanAllChannels, kErrorTxROMDLCTooLarge);
    canTxDLC_RAM[txHandle] = CanGetTxDlc(txHandle);
  }
#endif

#if defined( C_ENABLE_DYN_TX_OBJECTS )
  /* Reset dynamic transmission object management -------------------------- */
  for (txHandle = 0; txHandle < kCanNumberOfTxDynObjects; txHandle++)
  {
    /* Reset management information */
    canTxDynObjReservedFlag[txHandle] = 0;
  }
#endif /* C_ENABLE_DYN_TX_OBJECTS */

#if defined( C_ENABLE_VARIABLE_RX_DATALEN ) 
  for (rxHandle = 0; rxHandle < kCanNumberOfRxObjects; rxHandle++)
  {
# if defined( C_ENABLE_VARIABLE_RX_DATALEN )
    /* Initialize the array with generated dlc ---------------------- */
    canVariableRxDataLen[rxHandle] = CanGetRxDataLen(rxHandle);
# endif
  }
#endif /* defined( C_ENABLE_VARIABLE_RX_DATALEN ) || defined( C_ENABLE_DYN_RX_OBJECTS ) */

#if defined( C_ENABLE_INDICATION_FLAG )
  for (rxHandle = 0; rxHandle < kCanNumberOfIndBytes; rxHandle++) {
    CanIndicationFlags._c[rxHandle] = 0;
  }
#endif

#if defined( C_ENABLE_CONFIRMATION_FLAG )
  for (txHandle = 0; txHandle < kCanNumberOfConfBytes; txHandle++) {
    CanConfirmationFlags._c[txHandle] = 0;
  }
#endif


#if defined( C_ENABLE_RX_QUEUE )
# if defined( C_ENABLE_RX_QUEUE_EXTERNAL )
  /* in this case, the rx queue pointer no initialized yet. This will be done ofter CanInitPowerOn() via ... by the application */
# else
  CanDeleteRxQueue();
# endif
#endif

  /* LowLevel specific initialization */
  if (CanLL_InitPowerOn() == kCanOk) /* PRQA S 2991, 2995 */ /* MD_Can_ConstValue */
  {
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    for (channel=0; channel<kCanNumberOfChannels; channel++)
#endif
    {
#if defined( C_ENABLE_TRANSMIT_QUEUE )
#endif

#if defined( C_ENABLE_CAN_RAM_CHECK )
      canComStatus[channel] = kCanEnableCommunication;
#endif
      canStatus[channel] = kCanStatusInit;

      /* LowLevel specific initialization */
      if (CanLL_InitPowerOnChannelSpecific(CAN_CHANNEL_CANPARA_ONLY) == kCanOk) /* PRQA S 2991, 2995 */ /* MD_Can_ConstValue */
      {
#if defined( C_ENABLE_CAN_TX_CONF_FCT )
        txInfoStructConf[channel].Channel = channel;
#endif
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )  || \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
        canRxInfoStruct[channel].Channel = channel;
#endif

#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)
        canCanInterruptCounter[channel] = 0;
#endif

#if defined( C_ENABLE_TX_POLLING )          || \
      defined( C_ENABLE_RX_FULLCAN_POLLING )  || \
      defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
      defined( C_ENABLE_ERROR_POLLING )       || \
      defined( C_ENABLE_WAKEUP_POLLING )      
        canPollingTaskActive[channel] = 0;
#endif

#if defined( C_ENABLE_DYN_TX_OBJECTS )   && \
    defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
        /* Reset dynamic transmission object management -------------------------- */
        confirmHandle[channel] = kCanBufferFree;
#endif

#if defined( C_ENABLE_TX_MASK_EXT_ID )
        canTxMask0[channel] = 0;
# if (kCanNumberOfUsedCanTxIdTables > 1)
        canTxMask1[channel] = 0;
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
        canTxMask2[channel] = 0;
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
        canTxMask3[channel] = 0;
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
        canTxMask4[channel] = 0;
# endif
#endif

#if defined( C_ENABLE_ECU_SWITCH_PASS )
        canPassive[channel]             = 0;
#endif

#if defined( C_ENABLE_PART_OFFLINE )
        canTxPartOffline[channel]       = kCanTxPartInit;
#endif
#if defined( C_ENABLE_COND_RECEIVE_FCT )
        canMsgCondRecState[channel]     = kCanTrue;
#endif
#if defined( C_ENABLE_PRECOPY_FCT )
        canRxHandle[channel] = kCanRxHandleNotUsed;
#endif

        {
#if defined( C_ENABLE_TRANSMIT_QUEUE )
          /* clear all Tx queue flags */
          CanHL_DelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
#endif

          CanInit( CAN_CHANNEL_CANPARA_FIRST 0 );

          /* canStatus is only set to init and online, if CanInit() is called for this channel. */
          canStatus[channel]              |= (kCanHwIsInit | kCanTxOn);
        }
      } /* CanLL_InitPowerOnChannelSpecific() == kCanOk */
    } /* iterate channels */
  } /* CanLL_InitPowerOn() == kCanOk */
}
/* END OF CanInitPowerOn */
/* CODE CATEGORY 4 END */


#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* **********************************************************************
| NAME:               CanHL_DelQueuedObj
| CALLED BY:
| PRECONDITIONS:
| PARAMETER:          notify: if set to 1 for every deleted obj the appl is notified
| RETURN VALUE:       -
| DESCRIPTION:        Resets the bits with are set to 0 in mask
|                     Clearing the Transmit-queue
*********************************************************************** */
/* CODE CATEGORY 4 START */
static void CanHL_DelQueuedObj( CAN_CHANNEL_CANTYPE_ONLY ) C_API_3
{
  CanSignedTxHandle     queueElementIdx;
  #if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  CanSignedTxHandle     elementBitIdx;
  CanTransmitHandle     txHandle;
  tCanQueueElementType  elem;
  #endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif

  #  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  if((canStatus[channel] & kCanHwIsInit) == kCanHwIsInit) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);        /* avoid interruption by CanHL_TxConfirmation */
    for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
      elem = canTxQueueFlags[queueElementIdx];
      if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
      {
        /* iterate through all flags and notify application for every scheduled transmission */
        for(elementBitIdx = (CanSignedTxHandle)(1u << kCanTxQueueShift) - (CanSignedTxHandle)1; elementBitIdx >= (CanSignedTxHandle)0; elementBitIdx--) /* PRQA S 4398 */ /* MD_Can_IntegerCast */
        {
          if( ( elem & CanShiftLookUp[elementBitIdx] ) != (tCanQueueElementType)0)
          {
            txHandle = (CanTransmitHandle)((((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel)); /* PRQA S 2842, 2985, 2986 */ /* MD_Can_Assertion, MD_Can_ConstValue, MD_Can_ConstValue */ /* ESCAN00039235 */
            APPLCANCANCELNOTIFICATION(channel, txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
          }
        }
        canTxQueueFlags[queueElementIdx] = (tCanQueueElementType)0;
      }
    }
    CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
  }
  else
  #  endif
  {
    for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
      canTxQueueFlags[queueElementIdx] = (tCanQueueElementType)0;
    }
  }

}
/* CODE CATEGORY 4 END */
#endif


#if defined( C_ENABLE_CAN_TRANSMIT )
# if defined( C_ENABLE_CAN_CANCEL_TRANSMIT )
/* CODE CATEGORY 3 START */
/* **************************************************************************
| NAME:             CanCancelTransmit
| CALLED BY:        Application
| PRECONDITIONS:    none
| INPUT PARAMETERS: Tx-Msg-Handle
| RETURN VALUES:    none
| DESCRIPTION:      delete on Msg-Object
************************************************************************** */
C_API_1 void C_API_2 CanCancelTransmit( CanTransmitHandle txHandle )
{
  CanDeclareGlobalInterruptOldStatus
  CAN_CHANNEL_CANTYPE_LOCAL
  CanObjectHandle        logTxObjHandle;
# if defined( C_ENABLE_CANCEL_IN_HW )
  tCanTxCancellationParaStruct txCancellationPara;
# endif

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle queueElementIdx; /* index for accessing the tx queue */
  CanSignedTxHandle elementBitIdx;  /* bit index within the tx queue element */
  CanTransmitHandle queueBitPos;  /* physical bitposition of the handle */
# endif

  if (txHandle < kCanNumberOfTxObjects)         /* legal txHandle ? */
  {
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    channel = CanGetChannelOfTxObj(txHandle);
# endif

# if defined( C_ENABLE_MULTI_ECU_PHYS )
    assertUser(((CanTxIdentityAssignment[txHandle] & V_ACTIVE_IDENTITY_MSK) != (tVIdentityMsk)0 ), channel, kErrorDisabledTxMessage); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

    CanNestedGlobalInterruptDisable();
# if defined( C_ENABLE_TRANSMIT_QUEUE )
    #if defined( C_ENABLE_INTERNAL_CHECK ) && defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if (sizeof(queueBitPos) == 1u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */
    {
      assertInternal( ((vuint16)((vuint16)kCanNumberOfTxObjects + (vuint16)CanTxQueuePadBits[kCanNumberOfChannels - 1u]) <= 256u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 2880 */ /* MD_Can_ConstValue */
    }
    else
    {
      if (((sizeof(tCanTxQueuePadBits) == 1u) && (kCanNumberOfTxObjects > (65536u - 256u))) || (sizeof(tCanTxQueuePadBits) > 1u)) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */
      {
        assertInternal( ((vuint32)((vuint32)kCanNumberOfTxObjects + (vuint32)CanTxQueuePadBits[kCanNumberOfChannels - 1u]) <= 65536u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 2880 */ /* MD_Can_ConstValue */
      }
    }
    #endif
    queueBitPos = txHandle + CAN_HL_TXQUEUE_PADBITS(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */
    queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */ /* PRQA S 4394 */ /* MD_Can_MixedSigns */
    elementBitIdx = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask); /* get the flag index wihtin the queue element */ /* PRQA S 2985, 4394 */ /* MD_Can_ConstValue, MD_Can_MixedSigns */
    if( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
    {
      canTxQueueFlags[queueElementIdx] &= (tCanQueueElementType)~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */
      APPLCANCANCELNOTIFICATION(channel, txHandle);
    }
# endif

# if defined( C_ENABLE_TX_FULLCAN_OBJECTS )
    logTxObjHandle = (CanObjectHandle)((vsintx)CanGetTxMailbox(txHandle) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2985, 4393 */ /* MD_Can_ConstValue, MD_Can_MixedSigns */
# else
    logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2985, 4393 */ /* MD_Can_ConstValue, MD_Can_MixedSigns */
# endif/* C_ENABLE_TX_FULLCAN_OBJECTS */
    if (canHandleCurTxObj[logTxObjHandle] == txHandle)
    {
      canHandleCurTxObj[logTxObjHandle] = kCanBufferCancel;

      /* ##RI1.4 - 1.6: CanCancelTransmit and CanCancelMsgTransmit */
# if defined( C_ENABLE_CANCEL_IN_HW )
#  if defined( C_ENABLE_TX_FULLCAN_OBJECTS )
      txCancellationPara.mailboxHandle = CanGetTxMailbox(txHandle);
#  else
      txCancellationPara.mailboxHandle = CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel);
#  endif /* C_ENABLE_TX_FULLCAN_OBJECTS */
      txCancellationPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(txCancellationPara.mailboxHandle);
      txCancellationPara.logTxObjHandle = logTxObjHandle;

      CanLL_TxCancelInHw(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txCancellationPara);
# endif /* C_ENABLE_CANCEL_IN_HW */
      APPLCANCANCELNOTIFICATION(channel, txHandle);
    }

    CanNestedGlobalInterruptRestore();
  } /* if (txHandle < kCanNumberOfTxObjects) */
}
/* CODE CATEGORY 3 END */
# endif /* defined( C_ENABLE_CAN_CANCEL_TRANSMIT ) */


#endif /* if defined( C_ENABLE_CAN_TRANSMIT ) */


#if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
/* **************************************************************************
| NAME:             CanCancelMsgTransmit
| CALLED BY:        Application
| PRECONDITIONS:    none
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    none
| DESCRIPTION:      delete on Msg-Object
************************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 void C_API_2 CanCancelMsgTransmit( CAN_CHANNEL_CANTYPE_ONLY )
{
  CanDeclareGlobalInterruptOldStatus
  CanObjectHandle  logTxObjHandle;
# if defined( C_ENABLE_CANCEL_IN_HW )
  tCanTxCancellationParaStruct txCancellationPara;
# endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_MB_MSG_TRANSMIT_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2842, 2985, 4393 */ /* MD_Can_Assertion, MD_Can_ConstValue, MD_Can_MixedSigns */

  CanNestedGlobalInterruptDisable();
  if (canHandleCurTxObj[logTxObjHandle] == kCanBufferMsgTransmit)
  {
    canHandleCurTxObj[logTxObjHandle] = kCanBufferCancel;

    /* ##RI1.4 - 1.6: CanCancelTransmit and CanCancelMsgTransmit */
# if defined( C_ENABLE_CANCEL_IN_HW )
    txCancellationPara.mailboxHandle = CAN_HL_MB_MSG_TRANSMIT_INDEX(canHwChannel); /* PRQA S 2842 */ /* MD_Can_Assertion */
    txCancellationPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(txCancellationPara.mailboxHandle);
    txCancellationPara.logTxObjHandle = logTxObjHandle;

    CanLL_TxCancelInHw(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txCancellationPara);
# endif
    APPLCANMSGCANCELNOTIFICATION(channel); /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 3 END */
#endif


#if defined( C_ENABLE_CAN_TRANSMIT )
# if defined( C_ENABLE_VARIABLE_DLC )
/* CODE CATEGORY 2 START */
/* **************************************************************************
| NAME:             CanTransmitVarDLC
| CALLED BY:        Netmanagement, application
| PRECONDITIONS:    Can driver must be initialized
| INPUT PARAMETERS: Handle to Tx message, DLC of Tx message
| RETURN VALUES:    kCanTxFailed: transmit failed
|                   kCanTxOk    : transmit was succesful
| DESCRIPTION:      If the CAN driver is not ready for send, the application
|                   decide, whether the transmit request is repeated or not.
************************************************************************** */
C_API_1 vuint8 C_API_2 CanTransmitVarDLC(CanTransmitHandle txHandle, vuint8 dlc) C_API_3
{
  assertUser(dlc < (vuint8)9, kCanAllChannels, kErrorTxDlcTooLarge);
  assertUser(txHandle < kCanNumberOfTxObjects, kCanAllChannels, kErrorTxHdlTooLarge);

  canTxDLC_RAM[ txHandle ] = (tCanDlc)((canTxDLC_RAM[ txHandle ] & CanLL_DlcMask) | MK_TX_DLC(dlc)); /* PRQA S 2842, 2986 */ /* MD_Can_Assertion, MD_Can_ConstValue */

  return CanTransmit( txHandle );
}
/* END OF CanTransmitVarDLC */
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_VARIABLE_DLC */


/* **************************************************************************
| NAME:             CanTransmit
| CALLED BY:        application
| PRECONDITIONS:    Can driver must be initialized
| INPUT PARAMETERS: Handle of the transmit object to be send
| RETURN VALUES:    kCanTxFailed: transmit failed
|                   kCanTxOk    : transmit was succesful
| DESCRIPTION:      If the CAN driver is not ready for send, the application
|                   decide, whether the transmit request is repeated or not.
************************************************************************** */
/* CODE CATEGORY 2 START */

C_API_1 vuint8 C_API_2 CanTransmit(CanTransmitHandle txHandle) C_API_3
{
  CanDeclareGlobalInterruptOldStatus

# if !defined( C_ENABLE_TX_POLLING )          ||\
     !defined( C_ENABLE_TRANSMIT_QUEUE )      ||\
     defined( C_ENABLE_TX_FULLCAN_OBJECTS )   ||\
     defined( C_ENABLE_INDIVIDUAL_POLLING )
  CanObjectHandle      txMailboxHandle;
  CanObjectHandle      logTxObjHandle;
  vuint8               rc;
# endif /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */
  CAN_CHANNEL_CANTYPE_LOCAL

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle queueElementIdx; /* index for accessing the tx queue */
  CanSignedTxHandle elementBitIdx;  /* bit index within the tx queue element */
  CanTransmitHandle queueBitPos;  /* physical bitposition of the handle */
# endif


  assertUser(txHandle<kCanNumberOfTxObjects, kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

# if defined( C_ENABLE_MULTI_ECU_PHYS )
  assertUser(((CanTxIdentityAssignment[txHandle] & V_ACTIVE_IDENTITY_MSK) != (tVIdentityMsk)0 ), channel, kErrorDisabledTxMessage); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  /* test offline ---------------------------------------------------------- */
  if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )           /* transmit path switched off */
  {
    return kCanTxFailed;
  }

# if defined( C_ENABLE_PART_OFFLINE )
  if ( (canTxPartOffline[channel] & CanTxSendMask[txHandle]) != (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* CAN off ? */
  {
    return (kCanTxPartOffline);
  }
# endif

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication)
  {
    return(kCanCommunicationDisabled);
  }
# endif

# if defined( C_ENABLE_SLEEP_WAKEUP )
  assertUser(!CanLL_HwIsSleep(canHwChannel), channel, kErrorCanSleep);
# endif
  assertUser(!CanLL_HwIsStop(canHwChannel), channel, kErrorCanStop);

  /* passive mode ---------------------------------------------------------- */
# if defined( C_ENABLE_ECU_SWITCH_PASS )
  if ( canPassive[channel] != (vuint8)0)                             /*  set passive ? */
  {
#  if defined( C_ENABLE_CAN_TX_CONF_FCT ) || \
      defined( C_ENABLE_CONFIRMATION_FCT )
    CAN_CAN_INTERRUPT_DISABLE(channel);      /* avoid CAN Rx interruption */
#  endif

#  if defined( C_ENABLE_CAN_TX_CONF_FCT )
/* ##RI-1.10 Common Callbackfunction in TxInterrupt */
    txInfoStructConf[channel].Handle = txHandle;
    APPL_CAN_TX_CONFIRMATION(&txInfoStructConf[channel]);
#  endif

#  if defined( C_ENABLE_CONFIRMATION_FLAG )       /* set transmit ready flag  */
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptDisable();
#   endif
    CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] |= CanGetConfirmationMask(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptRestore();
#   endif
#  endif

#  if defined( C_ENABLE_CONFIRMATION_FCT )
    {
      if ( CanGetApplConfirmationPtr(txHandle) != V_NULL ) /* PRQA S 2842 */ /* MD_Can_Assertion */
      {
         (CanGetApplConfirmationPtr(txHandle))(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */ /* call completion routine */
      }
    }
#  endif /* C_ENABLE_CONFIRMATION_FCT */

#  if defined( C_ENABLE_CAN_TX_CONF_FCT ) || \
      defined( C_ENABLE_CONFIRMATION_FCT )
    CAN_CAN_INTERRUPT_RESTORE(channel);
#  endif

    return kCanTxOk;
  }
# endif


   /* can transmit enabled */

   /* ----------------------------------------------------------------------- */
   /* ---  transmit queue with one objects ---------------------------------- */
   /* ---  transmit using fullcan objects ----------------------------------- */
   /* ----------------------------------------------------------------------- */

# if !defined( C_ENABLE_TX_POLLING )          ||\
     !defined( C_ENABLE_TRANSMIT_QUEUE )      ||\
     defined( C_ENABLE_TX_FULLCAN_OBJECTS )   ||\
     defined( C_ENABLE_INDIVIDUAL_POLLING )
#   if defined( C_ENABLE_TX_FULLCAN_OBJECTS )
  txMailboxHandle = CanGetTxMailbox(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   else
  txMailboxHandle = CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel);
#   endif
  logTxObjHandle = (CanObjectHandle)((vsintx)txMailboxHandle + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2985, 4393 */ /* MD_Can_ConstValue, MD_Can_MixedSigns */
# endif /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */

  CanNestedGlobalInterruptDisable();

  /* test offline after interrupt disable ---------------------------------- */
  if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )                /* transmit path switched off */
  {
    CanNestedGlobalInterruptRestore();
    return kCanTxFailed;
  }

# if defined( C_ENABLE_TRANSMIT_QUEUE )
#  if defined( C_ENABLE_TX_FULLCAN_OBJECTS )  ||\
      !defined( C_ENABLE_TX_POLLING )         ||\
      defined( C_ENABLE_INDIVIDUAL_POLLING )
  if (
#   if defined( C_ENABLE_TX_FULLCAN_OBJECTS )
       ( txMailboxHandle == CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel) )
#   endif
#   if defined( C_ENABLE_TX_FULLCAN_OBJECTS )    &&\
       ( !defined( C_ENABLE_TX_POLLING )         ||\
          defined( C_ENABLE_INDIVIDUAL_POLLING ) )
     &&
#   endif

#   if defined( C_ENABLE_TX_POLLING )
#    if defined( C_ENABLE_INDIVIDUAL_POLLING )
       ( (CAN_HL_MB_HWOBJPOLLING(txMailboxHandle) != (vuint8)0 )  ||   /* Object is used in polling mode! */
         (canHandleCurTxObj[logTxObjHandle] != kCanBufferFree) )       /* MsgObj used?  */
#    else
        /* write always to queue; transmission is started out of TxTask */
#    endif
#   else
       ( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree )         /* MsgObj used?  */
#   endif
     )
#  endif /*  ( C_ENABLE_TX_FULLCAN_OBJECTS )  || !( C_ENABLE_TX_POLLING ) || ( C_ENABLE_INDIVIDUAL_POLLING ) */

    {
      /* tx object 0 used -> set msg in queue: ----------------------------- */
      queueBitPos = txHandle + CAN_HL_TXQUEUE_PADBITS(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */
      queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */ /* PRQA S 4394 */ /* MD_Can_MixedSigns */
      elementBitIdx = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask); /* get the flag index wihtin the queue element */ /* PRQA S 4394 */ /* MD_Can_MixedSigns */
      canTxQueueFlags[queueElementIdx] |= CanShiftLookUp[elementBitIdx];
      CanNestedGlobalInterruptRestore();
      return kCanTxOk;
  }
# endif /* C_ENABLE_TRANSMIT_QUEUE */

# if !defined( C_ENABLE_TX_POLLING )          ||\
     !defined( C_ENABLE_TRANSMIT_QUEUE )      ||\
     defined( C_ENABLE_TX_FULLCAN_OBJECTS )   ||\
     defined( C_ENABLE_INDIVIDUAL_POLLING )

#  if defined( C_ENABLE_TRANSMIT_QUEUE )    && \
      ( defined( C_ENABLE_TX_FULLCAN_OBJECTS )  ||\
        !defined( C_ENABLE_TX_POLLING )         ||\
        defined( C_ENABLE_INDIVIDUAL_POLLING )  )
  else
#  endif
  {
  /* check for transmit message object free --------------------------------- */
    if (   ( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree )    /* MsgObj used?  */
        || ( !CanLL_TxIsObjFree(canHwChannel, CAN_HL_MB_HWOBJHANDLE(txMailboxHandle)) )
      /* hareware-txObject is not free -------------------------------------- */
       )
    {  /* object used */
      /* tx object n used, quit with error */
      CanNestedGlobalInterruptRestore();
      return kCanTxFailed;
    }
  }

  /* Obj, pMsgObject points to is free, transmit msg object: ---------------- */
  canHandleCurTxObj[logTxObjHandle] = txHandle; /* Save hdl of msgObj to be transmitted */
  CanNestedGlobalInterruptRestore();

  rc = CanHL_CopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txMailboxHandle, txHandle);

#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  if ( rc == kCanTxNotify)
  {
    rc = kCanTxFailed;      /* ignore notification if calls of CanCopy.. is performed within CanTransmit */
  }
#  endif


  return(rc);

# else /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */
# endif /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */
}
/* END OF CanTransmit */
/* CODE CATEGORY 2 END */


/* **************************************************************************
| NAME:             CanHL_CopyDataAndStartTransmission
| CALLED BY:        CanTransmit, CanHL_RestartTxQueue and CanHL_TxConfirmation
| PRECONDITIONS:    - Can driver must be initialized
|                   - canTxCurHandle[logTxObjHandle] must be set
|                   - the hardwareObject (txObjHandle) must be free
| INPUT PARAMETERS: txHandle: Handle of the transmit object to be send
|                   txObjHandle:  Nr of the HardwareObjects to use
| RETURN VALUES:    kCanTxFailed: transmit failed
|                   kCanTxOk    : transmit was succesful
| DESCRIPTION:      If the CAN driver is not ready for send, the application
|                   decide, whether the transmit request is repeated or not.
************************************************************************** */
/* CODE CATEGORY 1 START */
static vuint8 CanHL_CopyDataAndStartTransmission( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle, CanTransmitHandle txHandle) C_API_3
{
   CanDeclareGlobalInterruptOldStatus
   vuint8               rc;
   CanObjectHandle      logTxObjHandle;
#  if defined( C_ENABLE_COPY_TX_DATA )
   TxDataPtr   CanMemCopySrcPtr;
#  endif
# if defined( C_ENABLE_DYN_TX_OBJECTS )
   CanTransmitHandle    dynTxObj;
# endif /* C_ENABLE_DYN_TX_OBJECTS */
# if defined( C_ENABLE_PRETRANSMIT_FCT )
   CanTxInfoStruct      txStruct;
# endif
   tCanTxTransmissionParaStruct txPara;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
   assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif
   assertInternal(txHandle < kCanNumberOfTxObjects, kCanAllChannels, kErrorInternalTxHdlTooLarge);

# if defined( C_ENABLE_DYN_TX_OBJECTS )
#  if ( kCanNumberOfTxStatObjects == 0)
   dynTxObj = txHandle;               /* only dynamic messages are used */
#  else /* ( kCanNumberOfTxStatObjects == 0) */
   /* dynamic and static messages are in the system */
   if (txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
   {
     dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
   }
   else
   {
     dynTxObj = kCanTxHandleNotUsed;
   }
#  endif /* ( kCanNumberOfTxStatObjects == 0) */
# endif /* C_ENABLE_DYN_TX_OBJECTS */

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
   assertInternal(txMailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel), channel, kErrorTxObjHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  else
#   if (kCanMailboxTxStartIndex != 0)
   assertInternal(txMailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel), channel, kErrorTxObjHandleWrong);
#   endif
#  endif
   assertInternal(txMailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel), channel, kErrorTxObjHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */
   logTxObjHandle = (CanObjectHandle)((vsintx)txMailboxHandle + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2842, 2897, 2985, 4393 */ /* MD_Can_Assertion, MD_Can_Assertion, MD_Can_ConstValue, MD_Can_MixedSigns */

   assertHardware(CanLL_TxIsObjFree(canHwChannel, CAN_HL_MB_HWOBJHANDLE(txMailboxHandle)), channel, kErrorTxBufferBusy); /* PRQA S 2842 */ /* MD_Can_Assertion */

   txPara.mailboxHandle = txMailboxHandle;
   txPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(txMailboxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
   txPara.logTxObjHandle = logTxObjHandle;

   CanLL_TxBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

   /* set id and dlc  -------------------------------------------------------- */
   {
# if defined( C_ENABLE_DYN_TX_DLC ) || \
      defined( C_ENABLE_DYN_TX_ID )
     if (dynTxObj < kCanNumberOfTxDynObjects)
     {           /* set dynamic part of dynamic objects ---------------------- */
#  if defined( C_ENABLE_DYN_TX_ID )
        txPara.idRaw0 = canDynTxId0[dynTxObj];
#   if (kCanNumberOfUsedCanTxIdTables > 1)
        txPara.idRaw1 = canDynTxId1[dynTxObj];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 2)
        txPara.idRaw2 = canDynTxId2[dynTxObj];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 3)
        txPara.idRaw3 = canDynTxId3[dynTxObj];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 4)
        txPara.idRaw4 = canDynTxId4[dynTxObj];
#   endif
#   if defined( C_ENABLE_CAN_FD_USED )
        txPara.fdType = canDynTxFdType[dynTxObj];
#   endif
#  endif
#  if defined( C_ENABLE_DYN_TX_DLC )
        txPara.dlcRaw = canDynTxDLC[dynTxObj];
#   if defined( C_ENABLE_CAN_FD_FULL ) && defined( C_ENABLE_COPY_TX_DATA )
        txPara.messageLen = canDynTxMessageLength[dynTxObj];
#   endif
#  endif
     }
     else
     {          /* set part of static objects assocciated the dynamic -------- */
#  if defined( C_ENABLE_DYN_TX_ID )
#   if defined( C_ENABLE_TX_MASK_EXT_ID )
#    if defined( C_ENABLE_MIXED_ID )
        if (CanGetTxIdType(txHandle)==kCanIdTypeStd) /* PRQA S 2842 */ /* MD_Can_Assertion */
        {
          txPara.idRaw0 = CanGetTxId0(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#     if (kCanNumberOfUsedCanTxIdTables > 1)
          txPara.idRaw1 = CanGetTxId1(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#     endif
#     if (kCanNumberOfUsedCanTxIdTables > 2)
          txPara.idRaw2 = CanGetTxId2(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#     endif
#     if (kCanNumberOfUsedCanTxIdTables > 3)
          txPara.idRaw3 = CanGetTxId3(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#     endif
#     if (kCanNumberOfUsedCanTxIdTables > 4)
          txPara.idRaw4 = CanGetTxId4(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#     endif
        }
        else
#    endif
        {
          /* mask extened ID */
          txPara.idRaw0 = (CanGetTxId0(txHandle)|canTxMask0[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    if (kCanNumberOfUsedCanTxIdTables > 1)
          txPara.idRaw1 = (CanGetTxId1(txHandle)|canTxMask1[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 2)
          txPara.idRaw2 = (CanGetTxId2(txHandle)|canTxMask2[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 3)
          txPara.idRaw3 = (CanGetTxId3(txHandle)|canTxMask3[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 4)
          txPara.idRaw4 = (CanGetTxId4(txHandle)|canTxMask4[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
        }
#   else
        txPara.idRaw0 = CanGetTxId0(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    if (kCanNumberOfUsedCanTxIdTables > 1)
        txPara.idRaw1 = CanGetTxId1(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 2)
        txPara.idRaw2 = CanGetTxId2(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 3)
        txPara.idRaw3 = CanGetTxId3(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 4)
        txPara.idRaw4 = CanGetTxId4(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#   endif
#   if defined( C_ENABLE_CAN_FD_USED )
        txPara.fdType = CanGetTxFdType(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#  endif

#  if defined( C_ENABLE_DYN_TX_DLC )
#   if defined( C_ENABLE_VARIABLE_DLC )
        /* init DLC, RAM */
        txPara.dlcRaw = canTxDLC_RAM[txHandle]; /* PRQA S 2842 */ /* MD_Can_Assertion */
#   else
        /* init DLC, ROM */
        txPara.dlcRaw = CanGetTxDlc(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    if defined( C_ENABLE_CAN_FD_FULL ) && defined( C_ENABLE_COPY_TX_DATA )
        txPara.messageLen = CanGetTxMessageLength(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#   endif
#  endif
     }
# endif
     /* set static part commen for static and dynamic objects ---------------- */
# if defined( C_ENABLE_DYN_TX_ID )
# else
#  if defined( C_ENABLE_TX_MASK_EXT_ID )
#   if defined( C_ENABLE_MIXED_ID )
     if (CanGetTxIdType(txHandle)==kCanIdTypeStd) /* PRQA S 2842 */ /* MD_Can_Assertion */
     {
       txPara.idRaw0 = CanGetTxId0(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    if (kCanNumberOfUsedCanTxIdTables > 1)
       txPara.idRaw1 = CanGetTxId1(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 2)
       txPara.idRaw2 = CanGetTxId2(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 3)
       txPara.idRaw3 = CanGetTxId3(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
#    if (kCanNumberOfUsedCanTxIdTables > 4)
       txPara.idRaw4 = CanGetTxId4(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
     }
     else
#   endif
     {
       /* mask extened ID */
       txPara.idRaw0 = (CanGetTxId0(txHandle)|canTxMask0[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   if (kCanNumberOfUsedCanTxIdTables > 1)
       txPara.idRaw1 = (CanGetTxId1(txHandle)|canTxMask1[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 2)
       txPara.idRaw2 = (CanGetTxId2(txHandle)|canTxMask2[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 3)
       txPara.idRaw3 = (CanGetTxId3(txHandle)|canTxMask3[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 4)
       txPara.idRaw4 = (CanGetTxId4(txHandle)|canTxMask4[channel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
     }
#  else
     txPara.idRaw0 = CanGetTxId0(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   if (kCanNumberOfUsedCanTxIdTables > 1)
     txPara.idRaw1 = CanGetTxId1(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 2)
     txPara.idRaw2 = CanGetTxId2(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 3)
     txPara.idRaw3 = CanGetTxId3(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 4)
     txPara.idRaw4 = CanGetTxId4(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#  endif
#  if defined( C_ENABLE_CAN_FD_USED )
     txPara.fdType = CanGetTxFdType(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif
# endif
# if defined( C_ENABLE_DYN_TX_DLC )
# else
#  if defined( C_ENABLE_VARIABLE_DLC )
     /* init DLC, RAM */
     txPara.dlcRaw = canTxDLC_RAM[txHandle]; /* PRQA S 2842 */ /* MD_Can_Assertion */
#  else
     /* init DLC, ROM */
     txPara.dlcRaw = CanGetTxDlc(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   if defined( C_ENABLE_CAN_FD_FULL ) && defined( C_ENABLE_COPY_TX_DATA )
     txPara.messageLen = CanGetTxMessageLength(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif
#  endif
# endif

# if defined( C_ENABLE_MIXED_ID )
#   if defined( C_ENABLE_DYN_TX_DLC ) || \
       defined( C_ENABLE_DYN_TX_ID )
     if (dynTxObj < kCanNumberOfTxDynObjects)
     {                      /* set dynamic part of dynamic objects */
#    if defined( C_ENABLE_DYN_TX_ID )
       txPara.idType = canDynTxIdType[dynTxObj];
#    else
       txPara.idType = CanGetTxIdType(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
     }
     else
     {
       txPara.idType = CanGetTxIdType(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
     }
#   else
#   endif
# endif

# if defined( C_ENABLE_CAN_FD_USED )
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
     assertGen(((txPara.fdType == kCanFdTypeClassic) || (CAN_HL_IS_CH_CANFD(channel) == kCanTrue)), channel, kErrorWrongCanFdFlag); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif
#  if defined( C_ENABLE_CAN_FD_SUPPRESS_BRS )
     txPara.fdBrsType = kCanFdBrsTypeFalse;
#  else
     if (txPara.fdType == kCanFdTypeFd)
     {
       txPara.fdBrsType = kCanFdBrsTypeTrue;
     }
     else
     {
       txPara.fdBrsType = kCanFdBrsTypeFalse;
     }
#  endif
# endif

# if defined( C_ENABLE_CAN_FD_FULL ) && defined( C_ENABLE_COPY_TX_DATA )
     assertGen(txPara.messageLen <= CAN_HL_MAX_LEN(channel), channel, kErrorTxROMDLCTooLarge); /* PRQA S 2842 */ /* MD_Can_Assertion */
     txPara.frameLen = CAN_DLC2LEN(CAN_LEN2DLC(txPara.messageLen)); /* PRQA S 2842 */ /* MD_Can_Assertion */
     assertGen(txPara.frameLen <= CAN_HL_MB_HWOBJMAXDATALEN(txMailboxHandle), channel, kErrorTxROMDLCTooLarge); /* PRQA S 2842 */ /* MD_Can_Assertion */
     txPara.paddingVal = (vuint8)C_CAN_FD_PADDING_VALUE;
# endif
   }

   CanLL_TxSetMailbox(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

 /* call pretransmit function ----------------------------------------------- */
# if defined( C_ENABLE_PRETRANSMIT_FCT )

   /* pointer needed for other modules */
   CanLL_TxSetTxStruct(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);
   txStruct = txPara.txStruct;

   canRDSTxPtr[logTxObjHandle] = txStruct.pChipData;
   txStruct.Handle = txHandle;

   {
    /* Is there a PreTransmit function ? ------------------------------------- */
    if ( CanGetApplPreTransmitPtr(txHandle) != V_NULL ) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* if PreTransmit exists */
    {
      if ( (CanGetApplPreTransmitPtr(txHandle)) (txStruct) == kCanNoCopyData) /* PRQA S 2842 */ /* MD_Can_Assertion */
      {
        CanLL_TxPretransmitCopyToCan(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

        /* Do not copy the data - already done by the PreTransmit-function */
        /* --- start transmission --- */
        goto startTransmission;
      }
    }
   }
# endif /* C_ENABLE_PRETRANSMIT_FCT */

 /* copy data --------------------------------------------------------------- */
# if defined( C_ENABLE_COPY_TX_DATA )
#  if defined( C_ENABLE_DYN_TX_DATAPTR )
   if (dynTxObj < kCanNumberOfTxDynObjects)
   {
      CanMemCopySrcPtr = canDynTxDataPtr[dynTxObj];
   }
   else
#  endif
   {
     CanMemCopySrcPtr = CanGetTxDataPtr(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
   }

   if ( CanMemCopySrcPtr != V_NULL )   /* copy if buffer exists */
   {
#  if C_SECURITY_LEVEL > 10
     CanNestedGlobalInterruptDisable();
#  endif

     txPara.CanMemCopySrcPtr = CanMemCopySrcPtr;
     CanLL_TxCopyToCan(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

#  if C_SECURITY_LEVEL > 10
     CanNestedGlobalInterruptRestore();
#  endif
   }
# endif /* ( C_ENABLE_COPY_TX_DATA ) */

   CANDRV_SET_CODE_TEST_POINT(0x10A);

# if defined( C_ENABLE_PRETRANSMIT_FCT )
/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
startTransmission:
# endif

   /* test offline and handle and start transmission ------------------------ */
   CanNestedGlobalInterruptDisable();
   /* If CanTransmit was interrupted by a re-initialization or CanOffline */
   /* no transmitrequest of this action should be started      */
   if ((canHandleCurTxObj[logTxObjHandle] == txHandle) && ((canStatus[channel] & kCanTxOn) == kCanTxOn)) /* PRQA S 2842 */ /* MD_Can_Assertion */
   {
     rc = CanLL_TxStart(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

     {
# if defined( C_ENABLE_TX_OBSERVE )
       ApplCanTxObjStart( CAN_CHANNEL_CANPARA_FIRST logTxObjHandle );
# endif
       /* explicit set of rc to kCanTxOk not necessary, because kCanTxOk and kCanOk are identical (return of CanLL_TxStart) */
     }
   }
   else
   {
# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
     if (canHandleCurTxObj[logTxObjHandle] == txHandle)
     {
       /* only CanOffline was called on higher level */
       rc = kCanTxNotify;
     }
     else
# endif
     {
       rc = kCanTxFailed;
     }
     assertInternal((canHandleCurTxObj[logTxObjHandle] == txHandle) || (canHandleCurTxObj[logTxObjHandle] == kCanBufferFree),
                                                                                       channel, kErrorTxHandleWrong);
     canHandleCurTxObj[logTxObjHandle] = kCanBufferFree; /* release TxHandle (CanOffline) */
   }

   CanNestedGlobalInterruptRestore();

   CanLL_TxEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

   return (rc);

}
/* END OF CanHL_CopyDataAndStartTransmission */
/* CODE CATEGORY 1 END */
#endif /* if defined( C_ENABLE_CAN_TRANSMIT ) */


#if defined( C_ENABLE_TX_POLLING ) || \
    defined( C_ENABLE_RX_FULLCAN_POLLING )  || \
    defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
    defined( C_ENABLE_ERROR_POLLING ) || \
    defined( C_ENABLE_WAKEUP_POLLING ) 
/* **************************************************************************
| NAME:             CanTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task,
|                   - polling error bus off
|                   - polling Tx objects
|                   - polling Rx objects
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanTask(void)
{
  CAN_CHANNEL_CANTYPE_LOCAL

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (channel = 0; channel < kCanNumberOfChannels; channel++)
# endif
  {
    {
# if defined( C_ENABLE_ERROR_POLLING )
      CanErrorTask(CAN_CHANNEL_CANPARA_ONLY);
# endif

# if defined( C_ENABLE_SLEEP_WAKEUP )
#  if defined( C_ENABLE_WAKEUP_POLLING )
      CanWakeUpTask(CAN_CHANNEL_CANPARA_ONLY);
#  endif
# endif

# if defined( C_ENABLE_TX_POLLING ) 
      CanTxTask(CAN_CHANNEL_CANPARA_ONLY);
# endif

# if defined( C_ENABLE_RX_FULLCAN_POLLING ) && \
     defined( C_ENABLE_RX_FULLCAN_OBJECTS )
      CanRxFullCANTask(CAN_CHANNEL_CANPARA_ONLY);
# endif

# if defined( C_ENABLE_RX_BASICCAN_OBJECTS ) && \
    defined( C_ENABLE_RX_BASICCAN_POLLING )
      CanRxBasicCANTask(CAN_CHANNEL_CANPARA_ONLY);
# endif
    }
  }
}
/* CODE CATEGORY 2 END */
#endif


#if defined( C_ENABLE_ERROR_POLLING )
/* **************************************************************************
| NAME:             CanErrorTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task,
|                   - polling error status
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanErrorTask( CAN_CHANNEL_CANTYPE_ONLY )
{

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

    {
# if defined( C_ENABLE_SLEEP_WAKEUP )
      if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
      {
        CAN_CAN_INTERRUPT_DISABLE(channel);
        CanHL_ErrorHandling(CAN_HW_CHANNEL_CANPARA_ONLY);
        CAN_CAN_INTERRUPT_RESTORE(channel);
      }
    }

    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
}
/* CODE CATEGORY 2 END */
#endif


#if defined( C_ENABLE_SLEEP_WAKEUP )
# if defined( C_ENABLE_WAKEUP_POLLING )
/* **************************************************************************
| NAME:             CanWakeUpTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task,
|                   - polling CAN wakeup event
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanWakeUpTask(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

    if (CanLL_WakeUpOccured(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY) == kCanTrue) /* PRQA S 2992, 2996 */ /* MD_Can_ConstValue */
    {
      CANDRV_SET_CODE_TEST_POINT(0x111); /* PRQA S 2880 */ /* MD_Can_ConstValue */
      CanNestedGlobalInterruptDisable();          /* ESCAN00021223 */
      CanLL_WakeUpHandling(CAN_CHANNEL_CANPARA_ONLY); /* PRQA S 2987 */ /* MD_Can_EmptyFunction */
      CanNestedGlobalInterruptRestore();          /* ESCAN00021223 */
    }
    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
}
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_WAKEUP_POLLING */
#endif /* C_ENABLE_SLEEP_WAKEUP */


#if defined( C_ENABLE_TX_POLLING ) 
/* **************************************************************************
| NAME:             CanTxTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task,
|                   - polling Tx objects
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanTxTask( CAN_CHANNEL_CANTYPE_ONLY ) C_API_3
{
  CanObjectHandle      txMailboxHandle;
  tCanTaskParaStruct   taskPara;


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */


# if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
    {
      /* --  polling Tx objects ---------------------------------------- */

# if defined( C_ENABLE_TX_POLLING )
      /* check globally for any pending confirmation */
      if (CanLL_TxIsGlobalConfPending(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY) == kCanTrue)
      {
        for ( txMailboxHandle = CAN_HL_MB_TX_STARTINDEX(canHwChannel); txMailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel); txMailboxHandle++ ) /* PRQA S 2842 */ /* MD_Can_Assertion */
        {
#  if defined( C_ENABLE_INDIVIDUAL_POLLING )
          if ( CAN_HL_MB_HWOBJPOLLING(txMailboxHandle) != (vuint8)0 )
#  endif
          {
            taskPara.mailboxHandle = txMailboxHandle;
            taskPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(txMailboxHandle);

            /* check and process pending confirmations for a dedicated mailbox */
            CanLL_TxProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &taskPara);
          } /* if individual polling & object has to be polled */
        }
      }
# endif /* ( C_ENABLE_TX_POLLING ) */


# if defined( C_ENABLE_TRANSMIT_QUEUE )
      CanHL_RestartTxQueue( CAN_CHANNEL_CANPARA_ONLY );
# endif /*  C_ENABLE_TRANSMIT_QUEUE */

    } /* if ( CanLL_HwIsSleep... ) */

    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }


}
/* END OF CanTxTask */
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_TX_POLLING */


#if defined( C_ENABLE_TRANSMIT_QUEUE )
# if defined( C_ENABLE_TX_POLLING ) 
/* **************************************************************************
| NAME:             CanHL_RestartTxQueue
| CALLED BY:        CanTxTask, via CanLL (TxMsgDestroyed)
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      start transmission if queue entry exists and HW is free
************************************************************************** */
/* CODE CATEGORY 2 START */
static void CanHL_RestartTxQueue( CAN_CHANNEL_CANTYPE_ONLY )
{
  CanTransmitHandle    txHandle;
#  if  defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  vuint8             rc;
#  endif
  CanDeclareGlobalInterruptOldStatus

  CanSignedTxHandle         queueElementIdx;    /* use as signed due to loop */
  CanSignedTxHandle         elementBitIdx;
  tCanQueueElementType      elem;

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
#  endif


  if (canHandleCurTxObj[(vsintx)CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)] == kCanBufferFree) /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
  {
    for(queueElementIdx = CAN_HL_TXQUEUE_STOPINDEX(channel) - (CanSignedTxHandle)1; /* PRQA S 2842 */ /* MD_Can_Assertion */
                             queueElementIdx >= CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx--)
    {
      elem = canTxQueueFlags[queueElementIdx];
      if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
      {
        /* Transmit Queued Objects */
        /* start the bitsearch */
        if((elem & (tCanQueueElementType)0xFFFF0000u) != (tCanQueueElementType)0)
        {
          if((elem & (tCanQueueElementType)0xFF000000u) != (tCanQueueElementType)0)
          {
            if((elem & (tCanQueueElementType)0xF0000000u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 28] + 28;
            }
            else /* 0x0F000000u */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 24] + 24;
            }
          }
          else
          {
            if((elem & (tCanQueueElementType)0x00F00000u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 20] + 20;
            }
            else /* 0x000F0000u */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 16] + 16;
            }
          }
        }
        else
        {
          if((elem & (tCanQueueElementType)0x0000FF00u) != (tCanQueueElementType)0)
          {
            if((elem & (tCanQueueElementType)0x0000F000u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 12] + 12;
            }
            else /* 0x00000F00u */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 8] + 8;
            }
          }
          else
          {
            if((elem & (tCanQueueElementType)0x000000F0u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 4] + 4;
            }
            else /* 0x0000000Fu */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem];
            }
          }
        }
        
        txHandle = (CanTransmitHandle)((((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel)); /* PRQA S 2842, 2985, 2986 */ /* MD_Can_Assertion, MD_Can_ConstValue, MD_Can_ConstValue */
        {
            CanNestedGlobalInterruptDisable();

            if (canHandleCurTxObj[(vsintx)CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)] == kCanBufferFree) /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
            {
              if ( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
              {
                canTxQueueFlags[queueElementIdx] &= (tCanQueueElementType)~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */
                /* Save hdl of msgObj to be transmitted */
                canHandleCurTxObj[(vsintx)CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)] = txHandle; /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
                CanNestedGlobalInterruptRestore();
#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
                rc = CanHL_CopyDataAndStartTransmission(CAN_CHANNEL_CANPARA_FIRST CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel), txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
                if ( rc == kCanTxNotify)
                {
                  APPLCANCANCELNOTIFICATION(channel, txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
                }
#  else
                (void)CanHL_CopyDataAndStartTransmission(CAN_CHANNEL_CANPARA_FIRST CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel), txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif
                return;
              }
            }

            CanNestedGlobalInterruptRestore();
            return;
        }
      }
    }
  }
}
/* END OF CanHL_RestartTxQueue */
/* CODE CATEGORY 2 END */
# endif
#endif /*  C_ENABLE_TRANSMIT_QUEUE */


#if defined( C_ENABLE_RX_FULLCAN_OBJECTS ) && \
    defined( C_ENABLE_RX_FULLCAN_POLLING )
/* **************************************************************************
| NAME:             CanRxFullCANTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task,
|                   - polling Rx FullCAN objects
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanRxFullCANTask(CAN_CHANNEL_CANTYPE_ONLY) C_API_3
{
  CanObjectHandle     rxMailboxHandle;
  tCanTaskParaStruct  taskPara;


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

    {
# if defined( C_ENABLE_SLEEP_WAKEUP )
      if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
      {
        /* --  polling fullcan Rx objects ---------------------------------------- */

        /* check globally for any pending indication */
        if (CanLL_RxFullIsGlobalIndPending(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY) == kCanTrue)
        {
          for (rxMailboxHandle=CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel); rxMailboxHandle<CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel); rxMailboxHandle++ ) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
# if defined( C_ENABLE_INDIVIDUAL_POLLING )
            if ( CAN_HL_MB_HWOBJPOLLING(rxMailboxHandle) != (vuint8)0 )
# endif
            {
              taskPara.mailboxHandle = rxMailboxHandle;
              taskPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(rxMailboxHandle);

              /* check and process pending indications for a dedicated mailbox */
              CanLL_RxFullProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &taskPara);
            }  /* if individual polling & object has to be polled */
          }
        }
      } /* if ( CanLL_HwIsSleep ... )  */
    }  /* for (all associated HW channel) */
    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }

}
/* END OF CanRxTask */
/* CODE CATEGORY 2 END */
#endif /*  C_ENABLE_RX_FULLCAN_OBJECTS && C_ENABLE_RX_FULLCAN_POLLING */


#if defined( C_ENABLE_RX_BASICCAN_POLLING ) && \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/* **************************************************************************
| NAME:             CanRxBasicCANTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task,
|                   - polling Rx BasicCAN objects
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanRxBasicCANTask(CAN_CHANNEL_CANTYPE_ONLY) C_API_3
{
  CanObjectHandle     rxMailboxHandle;
  tCanTaskParaStruct  taskPara;


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */


# if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
    {
      /* --  polling basiccan Rx objects ---------------------------------------- */

      /* check globally for any pending indication */
      if (CanLL_RxBasicIsGlobalIndPending(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY) == kCanTrue)
      {
        for (rxMailboxHandle=CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel); rxMailboxHandle<CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel); rxMailboxHandle++ ) /* PRQA S 2842 */ /* MD_Can_Assertion */
        {
# if defined( C_ENABLE_INDIVIDUAL_POLLING )
          if ( CAN_HL_MB_HWOBJPOLLING(rxMailboxHandle) != (vuint8)0 )
# endif
          {
            taskPara.mailboxHandle = rxMailboxHandle;
            taskPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(rxMailboxHandle);

            /* check and process pending indications for a dedicated mailbox */
            CanLL_RxBasicProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &taskPara);
          } /* if individual polling & object has to be polled */
        }
      }
    } /* if ( CanLL_HwIsSleep... )  */

    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }

}
/* END OF CanRxTask */
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_RX_BASICCAN_POLLING && C_ENABLE_RX_BASICCAN_OBJECTS */


/* **************************************************************************
| NAME:             CanHL_ErrorHandling
| CALLED BY:        CanISR(), CanErrorTask()
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      - error interrupt (busoff, error warning, ...)
************************************************************************** */
/* CODE CATEGORY 2 START */
extern void ResetCANTxConfirmation(void);
static void CanHL_ErrorHandling( CAN_HW_CHANNEL_CANTYPE_ONLY )
{

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
#endif

  CanLL_ErrorHandlingBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY); /* PRQA S 2987 */ /* MD_Can_EmptyFunction */

  /* check for status register (bus error occured) */
  if (CanLL_BusOffOccured(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY) == kCanTrue)
  {
	  /* wl20220801 */
    ResetCANTxConfirmation();
    APPL_CAN_BUSOFF( CAN_CHANNEL_CANPARA_ONLY ); /* PRQA S 2842 */ /* MD_Can_Assertion */ /* call application specific function */
  }


  CanLL_ErrorHandlingEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY); /* PRQA S 2987 */ /* MD_Can_EmptyFunction */

}
/* END OF CanHL_ErrorHandling */
/* CODE CATEGORY 2 END */


#if defined( C_ENABLE_INDIVIDUAL_POLLING )
# if defined( C_ENABLE_TX_POLLING )
/* **************************************************************************
| NAME:             CanTxObjTask()
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: CAN_HW_CHANNEL_CANTYPE_FIRST
|                   CanObjectHandle txObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      Polling special Object
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanTxObjTask(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle)
{
  tCanTaskParaStruct taskPara;

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif
  assertUser(txMailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel), channel, kErrorHwHdlTooLarge); /* PRQA S 2842 */ /* MD_Can_Assertion */
  /* avoid compiler warning: comparison is always true; ESCAN00076413 */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(txMailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  else
#   if (kCanMailboxTxStartIndex != 0)
  assertUser(txMailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall);
#   endif
#  endif
  assertUser(CAN_HL_MB_HWOBJPOLLING(txMailboxHandle) != 0u, channel, kErrorHwObjNotInPolling); /* PRQA S 2842 */ /* MD_Can_Assertion */
  assertUser((canPollingTaskActive[channel] == 0u), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

#  if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif
    {
      taskPara.mailboxHandle = txMailboxHandle;
      taskPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(txMailboxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */

      /* check and process pending confirmations for a dedicated mailbox */
      CanLL_TxProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &taskPara);

#  if defined( C_ENABLE_TRANSMIT_QUEUE )
      if ( txMailboxHandle == CAN_HL_MB_TX_NORMAL_INDEX(channel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
      {
        CanHL_RestartTxQueue( CAN_CHANNEL_CANPARA_ONLY );
      }
#  endif /*  C_ENABLE_TRANSMIT_QUEUE */
    }

    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
}
/* CanTxObjTask */
/* CODE CATEGORY 2 END */
# endif /* defined( C_ENABLE_INDIVIDUAL_POLLING ) && defined( C_ENABLE_TX_POLLING ) */


# if defined( C_ENABLE_RX_FULLCAN_OBJECTS ) && \
    defined( C_ENABLE_RX_FULLCAN_POLLING )
/* **************************************************************************
| NAME:             CanRxFullCANObjTask()
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: CAN_HW_CHANNEL_CANTYPE_FIRST
|                   CanObjectHandle rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      Polling special Object
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanRxFullCANObjTask(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle)
{
  tCanTaskParaStruct taskPara;

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif
  assertUser(rxMailboxHandle < CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel), channel, kErrorHwHdlTooLarge); /* PRQA S 2842 */ /* MD_Can_Assertion */
  /* avoid compiler warning: comparison is always true; ESCAN00059736 */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(rxMailboxHandle >= CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  else
#   if (kCanMailboxRxFullStartIndex != 0)
  assertUser(rxMailboxHandle >= CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall);
#   endif
#  endif
  assertUser(CAN_HL_MB_HWOBJPOLLING(rxMailboxHandle) != 0u, channel, kErrorHwObjNotInPolling); /* PRQA S 2842 */ /* MD_Can_Assertion */
  assertUser((canPollingTaskActive[channel] == 0u), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

#  if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif
    {
      taskPara.mailboxHandle = rxMailboxHandle;
      taskPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(rxMailboxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */

      /* check and process pending indications for a dedicated mailbox */
      CanLL_RxFullProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &taskPara);
    }
    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
}
/* CanRxFullCANObjTask */
/* CODE CATEGORY 2 END */
# endif


# if defined( C_ENABLE_RX_BASICCAN_POLLING ) && \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/* **************************************************************************
| NAME:             CanRxBasicCANObjTask()
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: CAN_HW_CHANNEL_CANTYPE_FIRST
|                   CanObjectHandle rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      Polling special Object
************************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanRxBasicCANObjTask(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle)
{
  tCanTaskParaStruct taskPara;

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif
  assertUser(rxMailboxHandle < CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel), channel, kErrorHwHdlTooLarge); /* PRQA S 2842 */ /* MD_Can_Assertion */
  /* avoid compiler warning: comparison is always true; ESCAN00059736 */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(rxMailboxHandle >= CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  else
#   if (kCanMailboxRxBasicStartIndex != 0)
  assertUser(rxMailboxHandle >= CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall);
#   endif
#  endif
  assertUser(CAN_HL_MB_HWOBJPOLLING(rxMailboxHandle) != 0u, channel, kErrorHwObjNotInPolling); /* PRQA S 2842 */ /* MD_Can_Assertion */
  assertUser((canPollingTaskActive[channel] == 0u), channel, kErrorPollingTaskRecursion); /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canPollingTaskActive[channel] == (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

#  if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(canHwChannel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif
    {
      taskPara.mailboxHandle = rxMailboxHandle;
      taskPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(rxMailboxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */

      /* check and process pending indications for a dedicated mailbox */
      CanLL_RxBasicProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &taskPara);
    }
    canPollingTaskActive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
}
/* CanRxBasicCANObjTask */
/* CODE CATEGORY 2 END */
# endif
#endif /* C_ENABLE_INDIVIDUAL_POLLING */


#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/* **************************************************************************
| NAME:             CanHL_BasicCanMsgReceived
| CALLED BY:        CanISR(), CanHL_BasicCanMsgReceivedPolling()
| PRECONDITIONS:
| INPUT PARAMETERS: channel, rxMailboxHandle, rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      - basic can receive
************************************************************************** */
/* CODE CATEGORY 1 START */
static void CanHL_BasicCanMsgReceived( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle)
{
# if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  tCanRxInfoStruct    *pCanRxInfoStruct;
# endif

# if ( !defined( C_SEARCH_HASH ) && \
       !defined( C_SEARCH_INDEX ) ) || \
     defined( C_ENABLE_RANGE_0 ) || \
     defined( C_ENABLE_RANGE_1 ) || \
     defined( C_ENABLE_RANGE_2 ) || \
     defined( C_ENABLE_RANGE_3 )
  tCanRxId0 idRaw0;
#  if (kCanNumberOfUsedCanRxIdTables > 1)
  tCanRxId1 idRaw1;
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 2)
  tCanRxId2 idRaw2;
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 3)
  tCanRxId3 idRaw3;
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 4)
  tCanRxId4 idRaw4;
#  endif
# endif

# if defined( C_SEARCH_HASH )
#  if (kCanNumberOfRxBasicCANObjects > 0)
#   if (kHashSearchListCountEx > 0)
  vuint32          idExt;
#    if (kHashSearchListCountEx > 1)
  vuint32          winternExt;        /* prehashvalue         */
#    endif
#   endif
#   if (kHashSearchListCount > 0)
  vuint16          idStd;
#    if (kHashSearchListCount > 1)
  vuint16          winternStd;        /* prehashvalue         */
#    endif
#   endif
#   if (((kHashSearchListCountEx > 1) && (kHashSearchMaxStepsEx > 1)) ||\
        ((kHashSearchListCount > 1)   && (kHashSearchMaxSteps > 1)))
  vuint16          i_increment;    /* delta for next step  */
  vsint16          count;
#   endif
#  endif /* kCanNumberOfRxBasicCANObjects > 0 */
# endif


  tCanRxBasicParaStruct rxBasicPara;

# if defined( C_ENABLE_GENERIC_PRECOPY ) || \
    defined( C_ENABLE_PRECOPY_FCT )     || \
    defined( C_ENABLE_COPY_RX_DATA )    || \
    defined( C_ENABLE_INDICATION_FLAG ) || \
    defined( C_ENABLE_INDICATION_FCT )  || \
    defined( C_ENABLE_DLC_CHECK )       || \
    defined( C_ENABLE_NOT_MATCHED_FCT )
#  if (kCanNumberOfRxBasicCANObjects > 0)

  vuint8                   canRxHandleFound;
  CanReceiveHandle         rxHandle = kCanRxHandleNotUsed; /* PRQA S 2981 */ /* MD_Can_RedundantInit */;

#  endif /* kCanNumberOfRxBasicCANObjects > 0 */
# endif

  CANDRV_SET_CODE_TEST_POINT(0x100);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(rxMailboxHandle >= CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel), channel, kErrorMailboxHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */
# else
#  if (kCanMailboxRxBasicStartIndex != 0)
  assertInternal(rxMailboxHandle >= CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel), channel, kErrorMailboxHandleWrong);
#  endif
# endif
  assertInternal(rxMailboxHandle < CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel), channel, kErrorMailboxHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */

  rxBasicPara.mailboxHandle = rxMailboxHandle;
  rxBasicPara.hwObjHandle = rxObjHandle;
  if (CanLL_RxBasicMsgReceivedBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxBasicPara) == kCanFailed)
  {
    goto finishBasicCan;
  }

# if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  pCanRxInfoStruct =  &canRxInfoStruct[channel];
  pCanRxInfoStruct->pChipMsgObj = rxBasicPara.rxStruct.pChipMsgObj;
  pCanRxInfoStruct->pChipData = rxBasicPara.rxStruct.pChipData;
  canRDSRxPtr[channel] = pCanRxInfoStruct->pChipData; /* PRQA S 2842 */ /* MD_Can_Assertion */
# else
  canRxInfoStruct[channel].pChipMsgObj = rxBasicPara.rxStruct.pChipMsgObj; /* PRQA S 2842 */ /* MD_Can_Assertion */
  canRxInfoStruct[channel].pChipData = rxBasicPara.rxStruct.pChipData; /* PRQA S 2842 */ /* MD_Can_Assertion */
  canRDSRxPtr[channel] = canRxInfoStruct[channel].pChipData; /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
  CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel) = kCanRxHandleNotUsed;

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    goto finishBasicCan; /* ignore reception */
  }
# endif

#  if defined( C_ENABLE_OVERRUN )
  if (rxBasicPara.isOverrun == kCanTrue)
  {
    ApplCanOverrun( CAN_CHANNEL_CANPARA_ONLY );
  }
#  endif



  /* ************************** reject messages with illegal DLC ******************************************* */
# if defined( C_ENABLE_CAN_FD_FULL )
  /* DLC of classic CAN frames must not be greater than 8 */
  if ((CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) > (vuint8)8u) && (CanRxActualFdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanFdTypeFd))
# else
  /* DLC must never be greater than 8 */
  if (CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) > (vuint8)8u)
# endif
  {
    goto finishBasicCan;
  }

# if defined( C_HL_ENABLE_REJECT_ILLEGAL_DLC )
#  if defined( C_ENABLE_CAN_FD_FULL )
  /* Frame lenght must not be greater than (min) size of hardware object */
  if (CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) > CAN_HL_MB_HWOBJMAXDATALEN(rxMailboxHandle)) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    goto finishBasicCan;
  }
#  endif
# endif /* C_HL_ENABLE_REJECT_ILLEGAL_DLC */

# if defined( C_ENABLE_COND_RECEIVE_FCT )
  /* ************************** conditional message receive function  ************************************** */
  if(canMsgCondRecState[channel] == kCanTrue) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    ApplCanMsgCondReceived( CAN_HL_P_RX_INFO_STRUCT(channel) );
  }
# endif

# if defined( C_ENABLE_RECEIVE_FCT )
  /* ************************** call ApplCanMsgReceived() ************************************************** */
  if (APPL_CAN_MSG_RECEIVED( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
  {
    goto finishBasicCan;
  }
# endif

# if ( !defined( C_SEARCH_HASH ) && \
       !defined( C_SEARCH_INDEX ) ) || \
     defined( C_ENABLE_RANGE_0 ) || \
     defined( C_ENABLE_RANGE_1 ) || \
     defined( C_ENABLE_RANGE_2 ) || \
     defined( C_ENABLE_RANGE_3 )
  /* ************************** calculate idRaw for filtering ********************************************** */
#  if defined( C_ENABLE_EXTENDED_ID )
#   if defined( C_ENABLE_MIXED_ID )
  if (CanRxActualIdType(CAN_HL_P_RX_INFO_STRUCT(channel)) == kCanIdTypeExt)
#   endif
  {
#   if defined( C_ENABLE_RX_MASK_EXT_ID )
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID0( C_MASK_EXT_ID);
#    if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID1(C_MASK_EXT_ID);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID2(C_MASK_EXT_ID);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID3(C_MASK_EXT_ID);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID4(C_MASK_EXT_ID);
#    endif
#   else
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID0(0x1FFFFFFFu);
#    if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID1(0x1FFFFFFFu);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID2(0x1FFFFFFFu);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID3(0x1FFFFFFFu);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID4(0x1FFFFFFFu);
#    endif
#   endif /*  C_ENABLE_RX_MASK_EXT_ID */
  }
#   if defined( C_ENABLE_MIXED_ID )
  else
  {
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID0(0x7FFu);
#    if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID1(0x7FFu);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID2(0x7FFu);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID3(0x7FFu);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID4(0x7FFu);
#    endif
  }
#   endif
#  else /* C_ENABLE_EXTENDED_ID */
  {
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID0(0x7FFu);
#   if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID1(0x7FFu);
#   endif
#   if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID2(0x7FFu);
#   endif
#   if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID3(0x7FFu);
#   endif
#   if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID4(0x7FFu);
#   endif
  }
#  endif /* C_ENABLE_EXTENDED_ID */
# endif /* ( !defined( C_SEARCH_HASH ) && ... defined( C_ENABLE_RANGE_3 ) */

  /* ***************** Range filtering ****************************************************************** */
  {
#  if defined( C_ENABLE_RANGE_0 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange0) != (vuint16)0 ) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
#    if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE0IDTYPE(channel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#   else /* C_SINGLE_RECEIVE_CHANNEL */
    {
#    if (C_RANGE0_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) ) /* PRQA S 4399 */ /* MD_Can_IntegerCast */
#    else /* C_RANGE_0_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange0[channel] == kCanTrue) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
            pCanRxInfoStruct->Handle = kCanRxHandleRange0;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE0PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif /* C_ENABLE_RANGE_0 */

#  if defined( C_ENABLE_RANGE_1 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange1) != (vuint16)0 ) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
#    if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE1IDTYPE(channel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#   else /* C_SINGLE_RECEIVE_CHANNEL */
    {
#    if (C_RANGE1_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) ) /* PRQA S 4399 */ /* MD_Can_IntegerCast */
#    else /* C_RANGE_1_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange1[channel] == kCanTrue) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
            pCanRxInfoStruct->Handle = kCanRxHandleRange1;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE1PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif /* C_ENABLE_RANGE_1 */

#  if defined( C_ENABLE_RANGE_2 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange2) != (vuint16)0 ) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
#    if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE2IDTYPE(channel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE2ACCMASK(channel), CANRANGE2ACCCODE(channel)) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#   else /* C_SINGLE_RECEIVE_CHANNEL */
    {
#    if (C_RANGE2_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE2ACCMASK(channel), CANRANGE2ACCCODE(channel)) ) /* PRQA S 4399 */ /* MD_Can_IntegerCast */
#    else /* C_RANGE_2_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE2ACCMASK(channel), CANRANGE2ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange2[channel] == kCanTrue) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
            pCanRxInfoStruct->Handle = kCanRxHandleRange2;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE2PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif /* C_ENABLE_RANGE_2 */

#  if defined( C_ENABLE_RANGE_3 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange3) != (vuint16)0 ) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
#    if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE3IDTYPE(channel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE3ACCMASK(channel), CANRANGE3ACCCODE(channel)) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
#   else /* C_SINGLE_RECEIVE_CHANNEL */
    {
#    if (C_RANGE3_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE3ACCMASK(channel), CANRANGE3ACCCODE(channel)) ) /* PRQA S 4399 */ /* MD_Can_IntegerCast */
#    else /* C_RANGE_3_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) 
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE3ACCMASK(channel), CANRANGE3ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange3[channel] == kCanTrue) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
            pCanRxInfoStruct->Handle = kCanRxHandleRange3;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE3PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif /* C_ENABLE_RANGE_3 */

  }


# if defined( C_ENABLE_GENERIC_PRECOPY ) || \
    defined( C_ENABLE_PRECOPY_FCT )     || \
    defined( C_ENABLE_COPY_RX_DATA )    || \
    defined( C_ENABLE_INDICATION_FLAG ) || \
    defined( C_ENABLE_INDICATION_FCT )  || \
    defined( C_ENABLE_DLC_CHECK )       || \
    defined( C_ENABLE_NOT_MATCHED_FCT )
#  if (kCanNumberOfRxBasicCANObjects > 0)

  canRxHandleFound = kCanFalse;

  /* search the received id in ROM table: */


#   if defined( C_SEARCH_LINEAR )
  /* ************* Linear search ********************************************* */
  {
    for (rxHandle = CAN_HL_RX_BASIC_STARTINDEX(channel); rxHandle < CAN_HL_RX_BASIC_STOPINDEX(channel); rxHandle++)
    {
      if( idRaw0 == CanGetRxId0(rxHandle) )
      {
#    if (kCanNumberOfUsedCanRxIdTables > 1)
        if( idRaw1 == CanGetRxId1(rxHandle) )
#    endif
        {
#    if (kCanNumberOfUsedCanRxIdTables > 2)
          if( idRaw2 == CanGetRxId2(rxHandle) )
#    endif
          {
#    if (kCanNumberOfUsedCanRxIdTables > 3)
            if( idRaw3 == CanGetRxId3(rxHandle) )
#    endif
            {
#    if (kCanNumberOfUsedCanRxIdTables > 4)
              if( idRaw4 == CanGetRxId4(rxHandle) )
#    endif
              {
#    if defined( C_ENABLE_MIXED_ID )
                /* verify ID type, if not already done with the ID raw */
                if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CanGetRxIdType(rxHandle))
#    endif
                {
                  /* message found in id table */
#    if defined( C_ENABLE_RX_MSG_INDIRECTION )
                  rxHandle = CanRxMsgIndirection[rxHandle];       /* indirection for special sort-algoritms */
#    endif
                  canRxHandleFound = kCanTrue;
                  break;    /* exit loop with index rxHandle */
                }
              }
            }
          }
        }
      }
    } /* for (rxHandle ....) */
  }
#   endif /* defined( C_SEARCH_LINEAR ) */

#   if defined( C_SEARCH_HASH )
  /* ************* Hash search ********************************************** */
  {
#    if defined( C_ENABLE_EXTENDED_ID )
  /* one or more Extended ID listed */
#     if defined( C_ENABLE_MIXED_ID )
    if((CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) )) == kCanIdTypeExt)
#     endif
    {
#     if (kHashSearchListCountEx > 0)
    /* hash table has at least one entry */
    /* calculate the logical ID */
#      if defined( C_ENABLE_RX_MASK_EXT_ID )
      idExt          = (CanRxActualExtId( CAN_HL_P_RX_INFO_STRUCT(channel) ) & C_MASK_EXT_ID);
#      else
      idExt          = CanRxActualExtId( CAN_HL_P_RX_INFO_STRUCT(channel) );
#      endif

#      if (kHashSearchListCountEx == 1)
      rxHandle       = (CanReceiveHandle)0;
#      else
      winternExt     = idExt + CAN_HL_HASH_RANDOM_NUMBER_EX(channel); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
      rxHandle       = (CanReceiveHandle)((winternExt % (vuint32)CAN_HL_HASH_COUNT_EX(channel)) + CAN_HL_HASH_START_INDEX_EX(channel)); /* PRQA S 2842, 2985, 4391 */ /* MD_Can_Assertion, MD_Can_ConstValue, MD_Can_IntegerCast */
#      endif /* (kHashSearchListCountEx == 1) */

#      if ((kHashSearchListCountEx == 1) || (kHashSearchMaxStepsEx == 1))
      if (idExt == CanRxHashIdEx[rxHandle])
      {
        rxHandle = CanRxMsgIndirection[rxHandle + kHashSearchListCount]; /* PRQA S 2985 */ /* MD_Can_ConstValue */
        canRxHandleFound = kCanTrue;
      }
#      else /* (kHashSearchListCountEx == 1) || (kHashSearchMaxStepsEx == 1) */

      i_increment = (vuint16)((winternExt % (vuint32)(CAN_HL_HASH_COUNT_EX(channel) - 1u)) + (vuint8)1); /* PRQA S 2842, 4391 */ /* MD_Can_Assertion, MD_Can_IntegerCast */ /* ST10-CCAN Keil compiler complains without cast */

      for (count = (vsint16)kHashSearchMaxStepsEx; count != (vsint16)0; count--)
      {
        if (idExt == CanRxHashIdEx[rxHandle])
        {
          rxHandle = CanRxMsgIndirection[rxHandle + kHashSearchListCount]; /* PRQA S 2985 */ /* MD_Can_ConstValue */
          canRxHandleFound = kCanTrue;
          break;
        }
        else
        {
          rxHandle += i_increment;
          if( rxHandle >= CAN_HL_HASH_STOP_INDEX_EX(channel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
            rxHandle -= CAN_HL_HASH_COUNT_EX(channel); /* PRQA S 2842 */ /* MD_Can_Assertion */
          }
        }
      }
#      endif /* (kHashSearchListCountEx == 1) || (kHashSearchMaxStepsEx == 1) */
#     endif /* (kHashSearchListCountEx > 0) */
    }

#     if defined( C_ENABLE_MIXED_ID )
    else /* if((CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) )) == kCanIdTypeStd)  */
#     endif
#    endif /* If defined( C_ENABLE_EXTENDED_ID ) */

#    if defined( C_ENABLE_MIXED_ID ) || !defined( C_ENABLE_EXTENDED_ID )
    {
#     if (kHashSearchListCount > 0)
      /* hash table has at least one entry */
      idStd          = CanRxActualStdId( CAN_HL_P_RX_INFO_STRUCT(channel) ); /* calculate the logical ID */

#      if (kHashSearchListCount == 1)
      rxHandle       = (CanReceiveHandle)0;
#      else
      winternStd     = idStd + CAN_HL_HASH_RANDOM_NUMBER(channel); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
      rxHandle       = (CanReceiveHandle)((winternStd % CAN_HL_HASH_COUNT(channel)) + CAN_HL_HASH_START_INDEX(channel)); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
#      endif /* (kHashSearchListCount == 1) */

#      if ((kHashSearchListCount == 1)  || (kHashSearchMaxSteps == 1))
      if (idStd == CanRxHashId[rxHandle])
      {
        rxHandle = CanRxMsgIndirection[rxHandle];
        canRxHandleFound = kCanTrue;
      }
#      else /* ((kHashSearchListCount == 1)  || (kHashSearchMaxSteps == 1)) */

      i_increment = (vuint16)((winternStd % (CAN_HL_HASH_COUNT(channel) - 1u)) + (vuint8)1); /* PRQA S 2842 */ /* MD_Can_Assertion */

      for (count = (vsint16)kHashSearchMaxSteps; count != (vsint16)0; count--)
      {
        if (idStd == CanRxHashId[rxHandle])
        {
          rxHandle = CanRxMsgIndirection[rxHandle];
          canRxHandleFound = kCanTrue;
          break;
        }
        else
        {
          rxHandle += i_increment;
          if( rxHandle >= CAN_HL_HASH_STOP_INDEX(channel) ) /* PRQA S 2842 */ /* MD_Can_Assertion */
          {
            rxHandle -= CAN_HL_HASH_COUNT(channel); /* PRQA S 2842 */ /* MD_Can_Assertion */
          }
        }
      }
#      endif /* ((kHashSearchListCount == 1)  || (kHashSearchMaxSteps == 1)) */
#     endif /* (kHashSearchListCount > 0) */
    }
#    endif /* defined( C_ENABLE_MIXED_ID ) || !defined( C_ENABLE_EXTENDED_ID ) */
  }
#   endif /* defined( C_SEARCH_HASH ) */

#   if defined( C_SEARCH_INDEX )
  /* ************* index search *********************************************** */
  /* extended ID is not supported with index search */
  {
    rxHandle = CAN_RX_INDEX_TBL(channel, CanRxActualStdId( CAN_HL_P_RX_INFO_STRUCT(channel))); /* PRQA S 2842 */ /* MD_Can_Assertion */
    if ( rxHandle < kCanNumberOfRxObjects)     /*  match in index search detected */
    {
      canRxHandleFound = kCanTrue;
    }
  }
#   endif /* defined( C_SEARCH_INDEX ) */




  if (canRxHandleFound == kCanTrue)
  {
    /* This code is reached for
       - hash search, HIT  in static Rx objects
       - hash search, MISS in static Rx objects but HIT in the dynamic objects
       - linear search, always (HIT/MISS in static or HIT in dynamic search) so search result must be evaluated */
    /* ************************** handle filtered message ************************************************** */
    assertInternal((rxHandle < kCanNumberOfRxObjects), kCanAllChannels, kErrorRxHandleWrong); /* PRQA S 2880, 2992, 2995, 2996 */ /* MD_Can_ConstValue */ /* legal rxHandle ? */

    CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel) = rxHandle;

#  if defined( C_ENABLE_RX_QUEUE )
    if (CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY ) == kCanHlContinueRx)
#  else
    if (CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_ONLY ) == kCanHlContinueRx)
#  endif
    {
#  if defined( C_ENABLE_INDICATION_FLAG ) || \
          defined( C_ENABLE_INDICATION_FCT )
      CanLL_RxBasicReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxBasicPara);

      CanHL_IndRxHandle( rxHandle );

      CanLL_RxBasicMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxBasicPara);

      return;
#  endif
    }
  }
#  if defined( C_ENABLE_NOT_MATCHED_FCT )
  else
  {
    /* nothing found, neither static nor dynamic receive object, leave the function */
    ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
  }
#  endif

# else /* kCanNumberOfRxBasicCANObjects > 0 */
#  if defined( C_ENABLE_NOT_MATCHED_FCT )
  ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#  endif
# endif /* kCanNumberOfRxBasicCANObjects > 0 */

# endif

  goto finishBasicCan;     /* to avoid compiler warning */

/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
finishBasicCan:

  /* release receive buffer */
  CanLL_RxBasicReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxBasicPara);
  CanLL_RxBasicMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxBasicPara);


  return;
}
/* END OF BasicCan */
/* CODE CATEGORY 1 END */


# if defined( C_ENABLE_RX_BASICCAN_POLLING )
/* **************************************************************************
| NAME:             CanHL_BasicCanMsgReceivedPolling
| CALLED BY:        CanTask()
| PRECONDITIONS:
| INPUT PARAMETERS: channel, rxMailboxHandle, rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      - basic can receive
************************************************************************** */
/* CODE CATEGORY 1 START */
CAN_LOCAL_INLINE void CanHL_BasicCanMsgReceivedPolling( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle)
{
  CANDRV_SET_CODE_TEST_POINT(0x108);

  CAN_CAN_INTERRUPT_DISABLE(channel);
  CanHL_BasicCanMsgReceived( CAN_HW_CHANNEL_CANPARA_FIRST rxMailboxHandle, rxObjHandle );
  CAN_CAN_INTERRUPT_RESTORE(channel);
}
/* CODE CATEGORY 1 END */
# endif
#endif /* C_ENABLE_RX_BASICCAN_OBJECTS */


#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/* **************************************************************************
| NAME:             CanHL_FullCanMsgReceived
| CALLED BY:        CanISR(), CanHL_FullCanMsgReceivedPolling()
| PRECONDITIONS:
| INPUT PARAMETERS: channel, rxMailboxHandle, rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      - full can receive
************************************************************************** */
/* CODE CATEGORY 1 START */
static void CanHL_FullCanMsgReceived( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle )
{
  CanReceiveHandle   rxHandle;

# if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  tCanRxInfoStruct    *pCanRxInfoStruct;
# endif

  tCanRxFullParaStruct rxFullPara;

  CANDRV_SET_CODE_TEST_POINT(0x101);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(rxMailboxHandle >= CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel), channel, kErrorMailboxHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */
# else
#  if (kCanMailboxRxFullStartIndex != 0)
  assertInternal(rxMailboxHandle >= CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel), channel, kErrorMailboxHandleWrong);
#  endif
# endif
  assertInternal(rxMailboxHandle < CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel), channel, kErrorMailboxHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */

  rxFullPara.mailboxHandle = rxMailboxHandle;
  rxFullPara.hwObjHandle = rxObjHandle;
  if (CanLL_RxFullMsgReceivedBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxFullPara) == kCanFailed)
  {
    goto finishRxFullCan;
  }

# if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  /* pointer needed for other modules */
  pCanRxInfoStruct =  &canRxInfoStruct[channel];
  pCanRxInfoStruct->pChipMsgObj = rxFullPara.rxStruct.pChipMsgObj;
  pCanRxInfoStruct->pChipData = rxFullPara.rxStruct.pChipData;
  canRDSRxPtr[channel] = pCanRxInfoStruct->pChipData; /* PRQA S 2842 */ /* MD_Can_Assertion */
# else
  canRxInfoStruct[channel].pChipMsgObj = rxFullPara.rxStruct.pChipMsgObj; /* PRQA S 2842 */ /* MD_Can_Assertion */
  canRxInfoStruct[channel].pChipData = rxFullPara.rxStruct.pChipData; /* PRQA S 2842 */ /* MD_Can_Assertion */
  canRDSRxPtr[channel] = canRxInfoStruct[channel].pChipData; /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    goto finishRxFullCan; /* ignore reception */
  }
# endif

#  if defined( C_ENABLE_FULLCAN_OVERRUN )
  if (rxFullPara.isOverrun == kCanTrue)
  {
     ApplCanFullCanOverrun( CAN_CHANNEL_CANPARA_ONLY );
  }
#  endif



  /* ************************** reject messages with illegal DLC ******************************************* */
# if defined( C_ENABLE_CAN_FD_FULL )
  /* DLC of classic CAN frames must not be greater than 8 */
  if ((CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) > (vuint8)8u) && (CanRxActualFdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanFdTypeFd))
# else
  /* DLC must never be greater than 8 */
  if (CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) > (vuint8)8u)
# endif
  {
    goto finishRxFullCan;
  }

# if defined( C_HL_ENABLE_REJECT_ILLEGAL_DLC )
#  if defined( C_ENABLE_CAN_FD_FULL )
  /* Frame lenght must not be greater than (min) size of hardware object */
  if (CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) > CAN_HL_MB_HWOBJMAXDATALEN(rxMailboxHandle)) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    goto finishRxFullCan;
  }
#  endif
# endif /* C_HL_ENABLE_REJECT_ILLEGAL_DLC */

# if defined( C_ENABLE_COND_RECEIVE_FCT )
  /* ************************** conditional message receive function  ************************************** */
  if(canMsgCondRecState[channel] == kCanTrue) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    ApplCanMsgCondReceived( CAN_HL_P_RX_INFO_STRUCT(channel) );
  }
# endif

# if defined( C_ENABLE_RECEIVE_FCT )
  /* ************************** call ApplCanMsgReceived() ************************************************** */
  if (APPL_CAN_MSG_RECEIVED( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
  {
    goto finishRxFullCan;
  }
# endif

  /* calculate the message handle to access the generated data for the received message */
  rxHandle = (CanReceiveHandle)((rxMailboxHandle - CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel)) /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# if defined( C_SEARCH_HASH )
                         + kHashSearchListCount /* PRQA S 2985 */ /* MD_Can_ConstValue */
                         + kHashSearchListCountEx /* PRQA S 2985 */ /* MD_Can_ConstValue */
# endif
                         + CAN_HL_RX_FULL_STARTINDEX(canHwChannel)); /* PRQA S 2985 */ /* MD_Can_ConstValue */


# if defined( C_ENABLE_RX_MSG_INDIRECTION ) || \
     defined( C_SEARCH_HASH ) || \
     defined( C_SEARCH_INDEX )
  rxHandle = CanRxMsgIndirection[rxHandle];
# endif

  assertInternal((rxHandle < kCanNumberOfRxObjects), kCanAllChannels, kErrorRxHandleWrong);  /* legal rxHandle ? */

  CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel) = rxHandle;

# if defined( C_ENABLE_RX_QUEUE )
  if (CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY) == kCanHlContinueRx)
# else
  if (CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_ONLY ) == kCanHlContinueRx)
# endif
  {
# if defined( C_ENABLE_INDICATION_FLAG ) || \
        defined( C_ENABLE_INDICATION_FCT )
    CanLL_RxFullReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxFullPara);

    CanHL_IndRxHandle( rxHandle );

    CanLL_RxFullMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxFullPara);

    return;
# endif
  }

  goto finishRxFullCan;     /* to avoid compiler warning */

/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
finishRxFullCan:

  /* release receive buffer */
  CanLL_RxFullReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxFullPara);
  CanLL_RxFullMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &rxFullPara);


  return;
}
/* CODE CATEGORY 1 END */


# if defined( C_ENABLE_RX_FULLCAN_POLLING )
/* **************************************************************************
| NAME:             CanHL_FullCanMsgReceivedPolling
| CALLED BY:        CanTask()
| PRECONDITIONS:
| INPUT PARAMETERS: channel, rxMailboxHandle, rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      - full can receive
************************************************************************** */
/* CODE CATEGORY 1 START */
CAN_LOCAL_INLINE void CanHL_FullCanMsgReceivedPolling( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxMailboxHandle, CanObjectHandle rxObjHandle )
{
  CANDRV_SET_CODE_TEST_POINT(0x109);

  CAN_CAN_INTERRUPT_DISABLE(channel);
  CanHL_FullCanMsgReceived( CAN_HW_CHANNEL_CANPARA_FIRST rxMailboxHandle, rxObjHandle );
  CAN_CAN_INTERRUPT_RESTORE(channel);
}
/* CODE CATEGORY 1 END */
# endif
#endif /* C_ENABLE_RX_FULLCAN_OBJECTS */


#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )  || \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
# if ( kCanNumberOfRxObjects > 0 )
/* **************************************************************************
| NAME:             CanHL_ReceivedRxHandle
| CALLED BY:        CanHL_BasicCanMsgReceived, CanHL_FullCanMsgReceived
| PRECONDITIONS:
| INPUT PARAMETERS: Handle of received Message to access generated data
| RETURN VALUES:    none
| DESCRIPTION:      DLC-check, Precopy and copy of Data for received message
************************************************************************** */
/* CODE CATEGORY 1 START */
#  if defined( C_ENABLE_RX_QUEUE )
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_FIRST tCanRxInfoStruct *pCanRxInfoStruct )
{
#  else
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_ONLY )
{
#  endif
#  if !defined( C_ENABLE_RX_QUEUE ) &&\
    defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  tCanRxInfoStruct    *pCanRxInfoStruct;
#  endif

#  if defined( C_ENABLE_COPY_RX_DATA )
  tCanRxCopyParaStruct rxCopyPara;
#   if C_SECURITY_LEVEL > 20
  CanDeclareGlobalInterruptOldStatus
#   endif
#  endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif

#  if !defined( C_ENABLE_RX_QUEUE ) &&\
    defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  pCanRxInfoStruct =  &canRxInfoStruct[channel];
#  endif

#  if defined( C_ENABLE_MULTI_ECU_PHYS )
  if ( (CanRxIdentityAssignment[CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)] & V_ACTIVE_IDENTITY_MSK) == (tVIdentityMsk)0 )
  {
    /* message is not a receive message in the active indentity */
    CANDRV_SET_CODE_TEST_POINT(0x10B);
    return  kCanHlFinishRx;
  }
#  endif


#  if defined( C_ENABLE_DLC_CHECK )
#   if defined( C_ENABLE_DLC_CHECK_MIN_DATALEN )
  if ( (CanGetRxMinDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))) > CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) )
#   else
  if ( (CanGetDerivedRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))) > CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) )
#   endif
  {
    /* ##RI1.4 - 2.7: Callbackfunction-DLC-Check */
#   if defined( C_ENABLE_DLC_FAILED_FCT )
    ApplCanMsgDlcFailed( CAN_HL_P_RX_INFO_STRUCT(channel) );
#   endif /* C_ENABLE_DLC_FAILED_FCT */
    return  kCanHlFinishRx;
  }
#  endif

#  if defined( C_ENABLE_VARIABLE_RX_DATALEN )
  CanHL_SetVariableRxDatalen (CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel), CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel)));
#  endif

#  if defined( C_ENABLE_GENERIC_PRECOPY )
  if ( APPL_CAN_GENERIC_PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanCopyData)
  {
    return kCanHlFinishRx;
  }
#  endif

#  if defined( C_ENABLE_PRECOPY_FCT )
  if ( CanGetApplPrecopyPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) != V_NULL )    /* precopy routine */
  {
    /* canRxHandle in indexed drivers only for consistancy check in higher layer modules */
    canRxHandle[channel] = CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel); /* PRQA S 2842 */ /* MD_Can_Assertion */

    if ( CanGetApplPrecopyPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))( CAN_HL_P_RX_INFO_STRUCT(channel) )==kCanNoCopyData )
    {  /* precopy routine returns kCanNoCopyData:   */
      return  kCanHlFinishRx;
    }                      /* do not copy data check next irpt */
  }
#  endif

#  if defined( C_ENABLE_COPY_RX_DATA )
  /* no precopy or precopy returns kCanCopyData : copy data -- */
  /* copy via index ------------------------------------------- */
  if ( CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) != V_NULL )      /* copy if buffer exists */
  {
    /* copy data --------------------------------------------- */
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptDisable();
#   endif
    CANDRV_SET_CODE_TEST_POINT(0x107);

    rxCopyPara.dest = CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel));
#   if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
    rxCopyPara.src = pCanRxInfoStruct->pChipData;
#   else
    rxCopyPara.src = canRxInfoStruct[channel].pChipData;
#   endif
#   if defined( C_ENABLE_COPY_RX_DATA_WITH_DLC )
    if ( CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel)) < CanGetDerivedRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) )
    {
      rxCopyPara.len = CanRxActualDLC(CAN_HL_P_RX_INFO_STRUCT(channel));
    }
    else
#   endif /* C_ENABLE_COPY_RX_DATA_WITH_DLC */
    {
      rxCopyPara.len = CanGetDerivedRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel));
    }
    CanLL_RxCopyFromCan(CAN_CHANNEL_CANPARA_FIRST &rxCopyPara);

#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptRestore();
#   endif
  }
#  endif /* ( C_ENABLE_COPY_RX_DATA ) */

  CANDRV_SET_CODE_TEST_POINT(0x105);
  return kCanHlContinueRx;
}
/* END OF CanHL_ReceivedRxHandle() */
/* CODE CATEGORY 1 END */


#  if defined( C_ENABLE_INDICATION_FLAG ) || \
     defined( C_ENABLE_INDICATION_FCT )
/* **************************************************************************
| NAME:             CanHL_IndRxHandle
| CALLED BY:        CanHL_BasicCanMsgReceived, CanHL_FullCanMsgReceived
| PRECONDITIONS:
| INPUT PARAMETERS: Handle of received Message to access generated data
| RETURN VALUES:    none
| DESCRIPTION:      DLC-check, Precopy and copy of Data for received message
************************************************************************** */
/* CODE CATEGORY 1 START */
static void CanHL_IndRxHandle( CanReceiveHandle rxHandle )
{
#   if defined( C_ENABLE_INDICATION_FLAG )
#    if C_SECURITY_LEVEL > 20
  CanDeclareGlobalInterruptOldStatus
#    endif
#   endif

#   if defined( C_ENABLE_INDICATION_FLAG )
#    if C_SECURITY_LEVEL > 20
  CanNestedGlobalInterruptDisable();
#    endif
  CanIndicationFlags._c[CanGetIndicationOffset(rxHandle)] |= CanGetIndicationMask(rxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#    if C_SECURITY_LEVEL > 20
  CanNestedGlobalInterruptRestore();
#    endif
#   endif

#   if defined( C_ENABLE_INDICATION_FCT )
  if ( CanGetApplIndicationPtr(rxHandle) != V_NULL ) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    CanGetApplIndicationPtr(rxHandle)(rxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */ /* call indication routine */
  }
#   endif
}
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_INDICATION_FLAG || C_ENABLE_INDICATION_FCT  */
# endif /* ( kCanNumberOfRxObjects > 0 ) */
#endif


/* **************************************************************************
| NAME:             CanHL_TxConfirmation
| CALLED BY:        CanISR(), CanHL_TxConfirmationPolling()
| PRECONDITIONS:
| INPUT PARAMETERS: channel, txMailboxHandle, txObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      - can transmit confirmation
************************************************************************** */
/* CODE CATEGORY 1 START */
extern void SetCANTxConfirmation(void);
extern void ResetBusOffNum(void);
static void CanHL_TxConfirmation( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle, CanObjectHandle txMailboxElement, CanObjectHandle txObjHandle)
{
  CanObjectHandle       logTxObjHandle;
  CanTransmitHandle     txHandle;


#if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle         queueElementIdx;    /* use as signed due to loop */
  CanSignedTxHandle         elementBitIdx;
  tCanQueueElementType      elem;
  CanDeclareGlobalInterruptOldStatus
#else
# if defined( C_ENABLE_CONFIRMATION_FLAG )
#  if C_SECURITY_LEVEL > 20
  CanDeclareGlobalInterruptOldStatus
#  endif
# endif
#endif


#if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
  vuint8 rc;
#endif

  tCanTxConfirmationParaStruct txConfPara;


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(txMailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel), channel, kErrorMailboxHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */
# else
#  if (kCanMailboxTxStartIndex != 0)
  assertInternal(txMailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel), channel, kErrorMailboxHandleWrong);
#  endif
# endif
  assertInternal(txMailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel), channel, kErrorMailboxHandleWrong); /* PRQA S 2842 */ /* MD_Can_Assertion */
  assertInternal(txMailboxElement == 0u, channel, kErrorMailboxHandleWrong);

  logTxObjHandle = (CanObjectHandle)((vsintx)txMailboxHandle + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2842, 2897, 2985, 4393 */ /* MD_Can_Assertion, MD_Can_Assertion, MD_Can_ConstValue, MD_Can_MixedSigns */
  txHandle = canHandleCurTxObj[logTxObjHandle];           /* get saved handle */

  txConfPara.mailboxHandle = txMailboxHandle;
  txConfPara.hwObjHandle = txObjHandle;
  txConfPara.logTxObjHandle = logTxObjHandle;
  CanLL_TxConfBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txConfPara);

  /* check associated transmit handle */
  if (txHandle == kCanBufferFree)
  {
    assertInternal(kCanFalse != kCanFalse, channel, kErrorTxHandleWrong); /* PRQA S 2741 */ /* MD_Can_ConstValue */
    goto finishCanHL_TxConfirmation;
  }

#if defined( C_ENABLE_TX_OBSERVE ) || \
    defined( C_ENABLE_CAN_TX_CONF_FCT )
# if defined( C_ENABLE_CANCEL_IN_HW )
  if (CanLL_TxConfIsMsgTransmitted(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txConfPara) == kCanTrue)
# endif
  {
# if defined( C_ENABLE_TX_OBSERVE )
    {
      ApplCanTxObjConfirmed( CAN_CHANNEL_CANPARA_FIRST logTxObjHandle );
    }
# endif

# if defined( C_ENABLE_CAN_TX_CONF_FCT )
/* ##RI-1.10 Common Callbackfunction in TxInterrupt */
    txInfoStructConf[channel].Handle = txHandle; /* PRQA S 2842 */ /* MD_Can_Assertion */
#  if defined( C_ENABLE_CAN_TX_CONF_MSG_ACCESS )
    txConfPara.txStructConf = &txInfoStructConf[channel];
    CanLL_TxConfSetTxConfStruct(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txConfPara);
#  endif
    APPL_CAN_TX_CONFIRMATION(&txInfoStructConf[channel]);
# endif
  }
#endif /* defined( C_ENABLE_TX_OBSERVE ) || defined( C_ENABLE_CAN_TX_CONF_FCT ) */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
# if defined( C_ENABLE_TX_FULLCAN_OBJECTS )  || \
     defined( C_ENABLE_MSG_TRANSMIT )
  if (txMailboxHandle != CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                 /* free msg object of FullCAN or LowLevel Tx obj. */
  }
# endif
#else
  canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                   /* free msg object if queue is not used */
#endif

  if (txHandle != kCanBufferCancel)
  {
#if defined( C_ENABLE_MSG_TRANSMIT )
    if (txMailboxHandle == CAN_HL_MB_MSG_TRANSMIT_INDEX(canHwChannel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
    {
# if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
      APPL_CAN_MSGTRANSMITCONF( CAN_CHANNEL_CANPARA_ONLY );
# endif
      goto finishCanHL_TxConfirmation;
    }
#endif

#if defined( C_ENABLE_MULTI_ECU_PHYS )
    assertInternal(((CanTxIdentityAssignment[txHandle] & V_ACTIVE_IDENTITY_MSK) != (tVIdentityMsk)0 ), channel, kErrorInternalDisabledTxMessage);
#endif

#if defined( C_ENABLE_CONFIRMATION_FLAG )       /* set transmit ready flag  */
# if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptDisable();
# endif
    CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] |= CanGetConfirmationMask(txHandle);
    /* wl20220729 Diag message transmit confirmed */
    if(txHandle == 0)
    {
      SrvCanTpTxConfirm();
    }
    /* wl20220801 transmit confirmed */
    SetCANTxConfirmation();
    ResetBusOffNum();
# if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptRestore();
# endif
#endif

#if defined( C_ENABLE_CONFIRMATION_FCT )
    {
      if ( CanGetApplConfirmationPtr(txHandle) != V_NULL )
      {
        (CanGetApplConfirmationPtr(txHandle))(txHandle);   /* call completion routine  */
      }
    }
#endif /* C_ENABLE_CONFIRMATION_FCT */

  } /* end if kCanBufferCancel */

  /* check for next msg object in queue and transmit it */
#if defined( C_ENABLE_TRANSMIT_QUEUE )
# if defined( C_ENABLE_TX_FULLCAN_OBJECTS ) ||\
     defined( C_ENABLE_MSG_TRANSMIT )
  if (txMailboxHandle == CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel)) /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
  {
    CanNestedGlobalInterruptDisable();                /* ESCAN00008914 */

    for(queueElementIdx = CAN_HL_TXQUEUE_STOPINDEX(channel) - (CanSignedTxHandle)1; /* PRQA S 2842 */ /* MD_Can_Assertion */
                             queueElementIdx >= CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx--)
    {
      elem = canTxQueueFlags[queueElementIdx];
      if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
      {
        CanNestedGlobalInterruptRestore();

        /* start the bitsearch */
        if((elem & (tCanQueueElementType)0xFFFF0000u) != (tCanQueueElementType)0)
        {
          if((elem & (tCanQueueElementType)0xFF000000u) != (tCanQueueElementType)0)
          {
            if((elem & (tCanQueueElementType)0xF0000000u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 28] + 28;
            }
            else /* 0x0F000000u */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 24] + 24;
            }
          }
          else
          {
            if((elem & (tCanQueueElementType)0x00F00000u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 20] + 20;
            }
            else /* 0x000F0000u */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 16] + 16;
            }
          }
        }
        else
        {
          if((elem & (tCanQueueElementType)0x0000FF00u) != (tCanQueueElementType)0)
          {
            if((elem & (tCanQueueElementType)0x0000F000u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 12] + 12;
            }
            else /* 0x00000F00u */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 8] + 8;
            }
          }
          else
          {
            if((elem & (tCanQueueElementType)0x000000F0u) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 4] + 4;
            }
            else /* 0x0000000Fu */
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem];
            }
          }
        }
        
        txHandle = (CanTransmitHandle)((((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel)); /* PRQA S 2842, 2985, 2986 */ /* MD_Can_Assertion, MD_Can_ConstValue, MD_Can_ConstValue */
        {
            /* compute the logical message handle */
            CanNestedGlobalInterruptDisable();

            if ( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
            {
              canTxQueueFlags[queueElementIdx] &= (tCanQueueElementType)~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */

              CanNestedGlobalInterruptRestore();
              canHandleCurTxObj[logTxObjHandle] = txHandle; /* Save hdl of msgObj to be transmitted */
# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
              rc = CanHL_CopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txMailboxHandle, txHandle );
              if ( rc == kCanTxNotify)
              {
                APPLCANCANCELNOTIFICATION(channel, txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
              }
# else /* C_ENABLE_CAN_CANCEL_NOTIFICATION */
              (void)CanHL_CopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txMailboxHandle, txHandle );
# endif /* C_ENABLE_CAN_CANCEL_NOTIFICATION */

              goto finishCanHL_TxConfirmation;
            }

            /* meanwhile, the queue is empty. E.g. due to CanOffline on higher level */
            canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* free msg object if queue is empty */
            CanNestedGlobalInterruptRestore();
            goto finishCanHL_TxConfirmation;
        }
        /* no entry found in Queue */
# if defined( CANHL_TX_QUEUE_BIT )
# else
        CanNestedGlobalInterruptDisable();                /* ESCAN00008914 */
                                                 /* unreachable in case of Bit-Queue */
# endif
      }
    }

    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* free msg object if queue is empty */
    CanNestedGlobalInterruptRestore();                 /* ESCAN00008914 */
  }
#endif

/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
finishCanHL_TxConfirmation:

  CanLL_TxConfEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txConfPara);

  CAN_DUMMY_STATEMENT(txMailboxElement); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return;
}
/* END OF CanTxInterrupt */
/* CODE CATEGORY 1 END */


#if defined( C_ENABLE_TX_POLLING )
/* **************************************************************************
| NAME:             CanHL_TxConfirmationPolling
| CALLED BY:        CanTask()
| PRECONDITIONS:
| INPUT PARAMETERS: channel, txMailboxHandle, txObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      - can transmit confirmation
************************************************************************** */
/* CODE CATEGORY 1 START */
CAN_LOCAL_INLINE void CanHL_TxConfirmationPolling( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle txMailboxHandle, CanObjectHandle txMailboxElement, CanObjectHandle txObjHandle )
{
  CANDRV_SET_CODE_TEST_POINT(0x110);

  CAN_CAN_INTERRUPT_DISABLE(channel);
  CanHL_TxConfirmation(CAN_HW_CHANNEL_CANPARA_FIRST txMailboxHandle, txMailboxElement, txObjHandle);
  CAN_CAN_INTERRUPT_RESTORE(channel);
}
/* CODE CATEGORY 1 END */
#endif


#if defined( C_ENABLE_ECU_SWITCH_PASS )
/* **********************************************************************
| NAME:               CanSetActive
| CALLED BY:          application
| PRECONDITIONS:      none
| PARAMETER:          none or channel
| RETURN VALUE:       none
| DESCRIPTION:        Set the CAN driver into active mode
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetActive( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

  canPassive[channel] = 0; /* PRQA S 2842 */ /* MD_Can_Assertion */
}
/* END OF CanSetActive */
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanSetPassive
| CALLED BY:          application
| PRECONDITIONS:      none
| PARAMETER:          none or channel
| RETURN VALUE:       none
| DESCRIPTION:        Set the can driver into passive mode
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetPassive( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

  canPassive[channel] = 1; /* PRQA S 2842 */ /* MD_Can_Assertion */

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  /* clear all Tx queue flags: */
  CanHL_DelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
# endif

}
/* END OF CanSetPassive */
/* CODE CATEGORY 4 END */
#endif /* IF defined( C_ENABLE_ECU_SWITCH_PASS ) */


#if defined( C_ENABLE_OFFLINE )
/* **********************************************************************
| NAME:               CanOnline( CanChannelHandle channel )
| CALLED BY:          netmanagement
| PRECONDITIONS:      none
| PARAMETER:          none or channel
| RETURN VALUE:       none
| DESCRIPTION:        Switch on transmit path
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanOnline(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif
  {

    CanNestedGlobalInterruptDisable();

    canStatus[channel] |= kCanTxOn; /* PRQA S 2842 */ /* MD_Can_Assertion */

#   if defined( C_ENABLE_ONLINE_OFFLINE_CALLBACK_FCT )
    APPL_CAN_ONLINE(CAN_CHANNEL_CANPARA_ONLY);
#   endif
    CanNestedGlobalInterruptRestore();
  }

}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanOffline( CanChannelHandle channel )
| CALLED BY:          netmanagement
| PRECONDITIONS:      none
| PARAMETER:          none or channel
| RETURN VALUE:       none
| DESCRIPTION:        Switch off transmit path
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanOffline(CAN_CHANNEL_CANTYPE_ONLY) C_API_3
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

  CanNestedGlobalInterruptDisable();

  canStatus[channel] &= kCanTxNotOn; /* PRQA S 2842 */ /* MD_Can_Assertion */

# if defined( C_ENABLE_ONLINE_OFFLINE_CALLBACK_FCT )
  APPL_CAN_OFFLINE(CAN_CHANNEL_CANPARA_ONLY);
# endif
  CanNestedGlobalInterruptRestore();

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanHL_DelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
# endif

}
/* CODE CATEGORY 4 END */
#endif /* if defined( C_ENABLE_OFFLINE ) */


#if defined( C_ENABLE_PART_OFFLINE )
/* **********************************************************************
| NAME:               CanSetPartOffline
| CALLED BY:          application
| PRECONDITIONS:      none
| PARAMETER:          (channel), sendGroup
| RETURN VALUE:       none
| DESCRIPTION:        Switch partial off transmit path
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetPartOffline(CAN_CHANNEL_CANTYPE_FIRST vuint8 sendGroup)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

  CanNestedGlobalInterruptDisable();
  /* set offlinestate and save for use of CanOnline/CanOffline */
  canTxPartOffline[channel] |= sendGroup; /* PRQA S 2842 */ /* MD_Can_Assertion */
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanSetPartOnline
| CALLED BY:          application
| PRECONDITIONS:      none
| PARAMETER:          (channel), invSendGroup
| RETURN VALUE:       none
| DESCRIPTION:        Switch partial on transmit path
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetPartOnline(CAN_CHANNEL_CANTYPE_FIRST vuint8 invSendGroup)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

  CanNestedGlobalInterruptDisable();
  canTxPartOffline[channel] &= invSendGroup; /* PRQA S 2842 */ /* MD_Can_Assertion */
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanGetPartMode
| CALLED BY:          application
| PRECONDITIONS:      none
| PARAMETER:          none or channel
| RETURN VALUE:       canTxPartOffline
| DESCRIPTION:        return status of partoffline-Mode
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 vuint8 C_API_2 CanGetPartMode(CAN_CHANNEL_CANTYPE_ONLY)
{
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

  return canTxPartOffline[channel]; /* PRQA S 2842 */ /* MD_Can_Assertion */
}
/* CODE CATEGORY 4 END */
#endif


/* **************************************************************************
| NAME:             CanGetStatus
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    Bit coded status (more than one status can be set):
|                   kCanTxOn
|                   kCanHwIsStop
|                   kCanHwIsInit
|                   kCanHwIsInconsistent
|                   kCanHwIsWarning
|                   kCanHwIsPassive
|                   kCanHwIsBusOff
|                   kCanHwIsSleep
| DESCRIPTION:      returns the status of the transmit path and the CAN hardware.
|                   Only one of the statusbits Sleep, Busoff, Passive, Warning is set.
|                   Sleep has the highest priority, error warning the lowerst.
************************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 vuint8 C_API_2 CanGetStatus( CAN_CHANNEL_CANTYPE_ONLY ) C_API_3
{
#if defined( C_ENABLE_EXTENDED_STATUS )
#endif

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif

#if defined( C_ENABLE_EXTENDED_STATUS )
  CanLL_GetStatusBegin(CAN_CHANNEL_CANPARA_ONLY); /* PRQA S 2987 */ /* MD_Can_EmptyFunction */

# if defined( C_ENABLE_SLEEP_WAKEUP )
  /* *************************** verify Sleep mode *********************************** */
  if ( CanLL_HwIsSleep(canHwChannel) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsSleep ) ); } /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  /* *************************** verify Stop mode *********************************** */
  if ( CanLL_HwIsStop(canHwChannel) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsStop ) ); } /* PRQA S 2842 */ /* MD_Can_Assertion */

  /* *************************** verify Busoff *********************************** */
  if ( CanLL_HwIsBusOff(canHwChannel) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsBusOff ) ); } /* PRQA S 2842 */ /* MD_Can_Assertion */

  /* *************************** verify Error Passive **************************** */
  {
    if ( CanLL_HwIsPassive(canHwChannel) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsPassive ) ); } /* PRQA S 2842 */ /* MD_Can_Assertion */
  }

  /* *************************** verify Error Warning **************************** */
  {
    if ( CanLL_HwIsWarning(canHwChannel) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsWarning ) ); } /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
#endif
  return ( VUINT8_CAST (canStatus[channel] & kCanTxOn) ); /* PRQA S 2842 */ /* MD_Can_Assertion */

}
/* END OF CanGetStatus */
/* CODE CATEGORY 3 END */


/* **************************************************************************
| NAME:             CanSleep
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    kCanOk, if CanSleep was successfull
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      disable CAN
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 vuint8 C_API_2 CanSleep( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_ENABLE_SLEEP_WAKEUP )
  vuint8           canReturnCode;
#endif

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif
#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)
  assertUser((canCanInterruptCounter[channel] == (vsintx)0), channel, kErrorDisabledCanInt); /* PRQA S 2842 */ /* MD_Can_Assertion */
#endif

#if defined( C_ENABLE_COND_RECEIVE_FCT )
  /* this has to be done, even if SLEEP_WAKEUP is not enabled */
  canMsgCondRecState[channel] = kCanTrue; /* PRQA S 2842 */ /* MD_Can_Assertion */
#endif

#if defined( C_ENABLE_SLEEP_WAKEUP )
  assertUser((canStatus[channel] & kCanTxOn) != kCanTxOn, channel, kErrorCanOnline); /* PRQA S 2842 */ /* MD_Can_Assertion */

  {
    assertUser(!CanLL_HwIsStop(canHwChannel), channel, kErrorCanStop); /* PRQA S 2842 */ /* MD_Can_Assertion */

    canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeSleep, kCanFinishBusOffRecovery, kCanSuppressRamCheck);

    if (canReturnCode == kCanRequested)
    {
      CanHL_ApplCanTimerStart(kCanLoopSleep);
      do
      {
        canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeSleep, kCanFinishBusOffRecovery, kCanSuppressRamCheck);
        CanHL_ApplCanTimerLoop(kCanLoopSleep);
      } while (canReturnCode != kCanOk);
      CanHL_ApplCanTimerEnd(kCanLoopSleep);
    }

  }
  return canReturnCode;
#else
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return kCanNotSupported;
#endif
}
/* END OF CanSleep */
/* CODE CATEGORY 4 END */


/* **************************************************************************
| NAME:             CanWakeUp
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    kCanOk, if CanWakeUp was successfull
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      enable CAN
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 vuint8 C_API_2 CanWakeUp( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_ENABLE_SLEEP_WAKEUP )
  vuint8           canReturnCode;

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif
#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)
  assertUser((canCanInterruptCounter[channel] == (vsintx)0), channel, kErrorDisabledCanInt); /* PRQA S 2842 */ /* MD_Can_Assertion */
#endif

  {
    canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeWakeupStart, kCanFinishBusOffRecovery, kCanSuppressRamCheck);

    if (canReturnCode == kCanRequested)
    {
      CanHL_ApplCanTimerStart(kCanLoopWakeup);
      do
      {
        canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeWakeupStart, kCanFinishBusOffRecovery, kCanSuppressRamCheck);
        CanHL_ApplCanTimerLoop(kCanLoopWakeup);
      } while (canReturnCode != kCanOk);
      CanHL_ApplCanTimerEnd(kCanLoopWakeup);
    }

  }
  return canReturnCode;
#else
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return kCanNotSupported;
#endif /* C_ENABLE_SLEEP_WAKEUP */
}
/* END OF CanWakeUp */
/* CODE CATEGORY 4 END */


#if defined( C_ENABLE_STOP )
/* **************************************************************************
| NAME:             CanStop
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    kCanOk, if success
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      stop CAN-controller
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 vuint8 C_API_2 CanStop( CAN_CHANNEL_CANTYPE_ONLY )
{
  vuint8           canReturnCode;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif
  assertUser((canStatus[channel] & kCanTxOn) != kCanTxOn, channel, kErrorCanOnline); /* PRQA S 2842 */ /* MD_Can_Assertion */

  {
# if defined( C_ENABLE_SLEEP_WAKEUP )
    assertUser(!CanLL_HwIsSleep(canHwChannel), channel, kErrorCanSleep); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

    canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeStopReinitFast, kCanFinishBusOffRecovery, kCanSuppressRamCheck);

    if (canReturnCode == kCanRequested)
    {
      CanHL_ApplCanTimerStart(kCanLoopStop);
      do
      {
        canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeStopReinitFast, kCanFinishBusOffRecovery, kCanSuppressRamCheck);
        CanHL_ApplCanTimerLoop(kCanLoopStop);
      } while (canReturnCode != kCanOk);
      CanHL_ApplCanTimerEnd(kCanLoopStop);
    }

  }
  return canReturnCode;
}
/* CODE CATEGORY 4 END */


/* **************************************************************************
| NAME:             CanStart
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    kCanOk, if success
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      restart CAN-controller
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 vuint8 C_API_2 CanStart( CAN_CHANNEL_CANTYPE_ONLY )
{
  vuint8           canReturnCode;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  {
    canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeStart, kCanFinishBusOffRecovery, kCanSuppressRamCheck);

    if (canReturnCode == kCanRequested)
    {
      CanHL_ApplCanTimerStart(kCanLoopStart);
      do
      {
        canReturnCode = CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST kCanModeStart, kCanFinishBusOffRecovery, kCanSuppressRamCheck);
        CanHL_ApplCanTimerLoop(kCanLoopStart);
      } while (canReturnCode != kCanOk);
      CanHL_ApplCanTimerEnd(kCanLoopStart);
    }

  }
  return canReturnCode;
}
/* CODE CATEGORY 4 END */
#endif /* if defined( C_ENABLE_STOP ) */


/* **************************************************************************
| NAME:             CanResetBusSleep
| CALLED BY:        Network management, application
| PRECONDITIONS:    CanOffline() has to be called before
| INPUT PARAMETERS: Handle to initstructure
| RETURN VALUES:    none
| DESCRIPTION:      Prepare hardware for sleep mode, e.g. clear pending tx requests
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanResetBusSleep( CAN_CHANNEL_CANTYPE_FIRST CanInitHandle initObject )
{
  vuint8 localReturnCode;

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif
  assertUser((canStatus[channel] & kCanTxOn) != kCanTxOn, channel, kErrorCanOnline); /* PRQA S 2842 */ /* MD_Can_Assertion */

  localReturnCode = CanLL_ModeTransition(CAN_CHANNEL_CANPARA_FIRST kCanModeResetBusSleep, kCanFinishBusOffRecovery, kCanExecuteRamCheck);

  if (localReturnCode == kCanRequested)
  {
    CanHL_ApplCanTimerStart(kCanLoopInit);
    do
    {
      localReturnCode = CanLL_ModeTransition(CAN_CHANNEL_CANPARA_FIRST kCanModeResetBusSleep, kCanFinishBusOffRecovery, kCanExecuteRamCheck);
      CanHL_ApplCanTimerLoop(kCanLoopInit);
    } while (localReturnCode != kCanOk);
    CanHL_ApplCanTimerEnd(kCanLoopInit);
  }

  /* the handle to initstructure is not used - the currently active structure is not changed */
  CAN_DUMMY_STATEMENT(initObject); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 4 END */


/* **************************************************************************
| NAME:             CanResetBusOff
| CALLED BY:        Network management, application
| PRECONDITIONS:    CanOffline() has to be called before
| INPUT PARAMETERS: Handle to initstructure
|                   kCanModeResetBusOffStart or kCanModeResetBusOffEnd identifier
| RETURN VALUES:    none
| DESCRIPTION:      BusOff handling
************************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanResetBusOff( CAN_CHANNEL_CANTYPE_FIRST CanInitHandle initObject, vuint8 transition )
{
  vuint8 localReturnCode;

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#endif
  assertUser((canStatus[channel] & kCanTxOn) != kCanTxOn, channel, kErrorCanOnline); /* PRQA S 2842 */ /* MD_Can_Assertion */

  localReturnCode = CanLL_ModeTransition(CAN_CHANNEL_CANPARA_FIRST transition, kCanContinueBusOffRecovery, kCanSuppressRamCheck);

  if (localReturnCode == kCanRequested)
  {
    CanHL_ApplCanTimerStart(kCanLoopInit);
    do
    {
      localReturnCode = CanLL_ModeTransition(CAN_CHANNEL_CANPARA_FIRST transition, kCanContinueBusOffRecovery, kCanSuppressRamCheck);
      CanHL_ApplCanTimerLoop(kCanLoopInit);
    } while (localReturnCode != kCanOk);
    CanHL_ApplCanTimerEnd(kCanLoopInit);
  }

  /* the handle to initstructure is not used - the currently active structure is not changed */
  CAN_DUMMY_STATEMENT(initObject); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 4 END */


#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL)
/* **************************************************************************
| NAME:             CanCanInterruptDisable
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      disables CAN interrupts and stores old interrupt status
************************************************************************** */
/* CODE CATEGORY 1 START */
C_API_1 void C_API_2 CanCanInterruptDisable( CAN_CHANNEL_CANTYPE_ONLY ) C_API_3
{
# if defined (C_ENABLE_OSEK_CAN_INTCTRL)

  {
    OsCanCanInterruptDisable(CAN_HW_CHANNEL_CANPARA_ONLY);
  }
# else /* defined (C_ENABLE_OSEK_CAN_INTCTRL) */

  CanDeclareGlobalInterruptOldStatus
#  if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
#  endif

  /* local variable must reside on stack or registerbank, switched */
  /* in interrupt level                                            */
  /* disable global interrupt                                      */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif
  assertUser(canCanInterruptCounter[channel]<(vsint8)0x7f, kCanAllChannels, kErrorIntDisableTooOften); /* PRQA S 2842 */ /* MD_Can_Assertion */

  CanNestedGlobalInterruptDisable();
  if (canCanInterruptCounter[channel] == (vsintx)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* if 0 then save old interrupt status */
  {
#  if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
    {
#   if defined( C_ENABLE_SLEEP_WAKEUP )
      assertUser(!CanLL_HwIsSleep(canHwChannel), channel, kErrorCanSleep); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif

      CanLL_CanInterruptDisable(CAN_HW_CHANNEL_CANPARA_FIRST &canCanInterruptOldStatus[canHwChannel]);
    }
#  endif
#  if defined( C_ENABLE_INTCTRL_ADD_CAN_FCT )
    ApplCanAddCanInterruptDisable(channel);
#  endif
  }
  canCanInterruptCounter[channel]++; /* PRQA S 2842 */ /* MD_Can_Assertion */ /* common for all platforms */

  CanNestedGlobalInterruptRestore();
# endif /* C_ENABLE_OSEK_CAN_INTCTRL */
}
/* END OF CanCanInterruptDisable */
/* CODE CATEGORY 1 END */


/* **************************************************************************
| NAME:             CanCanInterruptRestore
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      restores the old interrupt status of the CAN interrupt if
|                   canCanInterruptCounter[channel] is zero
************************************************************************** */
/* CODE CATEGORY 1 START */
C_API_1 void C_API_2 CanCanInterruptRestore( CAN_CHANNEL_CANTYPE_ONLY ) C_API_3
{
# if defined (C_ENABLE_OSEK_CAN_INTCTRL)

  {
    OsCanCanInterruptRestore(CAN_HW_CHANNEL_CANPARA_ONLY);
  }
# else /* defined (C_ENABLE_OSEK_CAN_INTCTRL) */

  CanDeclareGlobalInterruptOldStatus
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif
  assertUser(canCanInterruptCounter[channel]>(vsintx)0, kCanAllChannels, kErrorIntRestoreTooOften); /* PRQA S 2842 */ /* MD_Can_Assertion */

  CanNestedGlobalInterruptDisable();
  /* restore CAN interrupt */
  canCanInterruptCounter[channel]--; /* PRQA S 2842 */ /* MD_Can_Assertion */

  if (canCanInterruptCounter[channel] == (vsintx)0) /* PRQA S 2842 */ /* MD_Can_Assertion */ /* restore interrupt if canCanInterruptCounter=0 */
  {
#  if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
    {
#   if defined( C_ENABLE_SLEEP_WAKEUP )
      assertUser(!CanLL_HwIsSleep(canHwChannel), channel, kErrorCanSleep); /* PRQA S 2842 */ /* MD_Can_Assertion */
#   endif

      CanLL_CanInterruptRestore(CAN_HW_CHANNEL_CANPARA_FIRST canCanInterruptOldStatus[canHwChannel]); /* PRQA S 2842 */ /* MD_Can_Assertion */
    }
#  endif

#  if defined( C_ENABLE_INTCTRL_ADD_CAN_FCT )
    ApplCanAddCanInterruptRestore(channel);
#  endif
  }
  CanNestedGlobalInterruptRestore();
# endif /* defined (C_ENABLE_OSEK_CAN_INTCTRL) */
}
/* END OF CanCanInterruptRestore */
/* CODE CATEGORY 1 END */
#endif /* defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) */


#if defined( C_ENABLE_MSG_TRANSMIT )
/* **********************************************************************
| NAME:               CanMsgTransmit
| CALLED BY:          CanReceivedFunction
| PRECONDITIONS:      Called in Receive Interrupt
| PARAMETER:          Pointer to message buffer data block; This can either be
|                     a RAM structure data block or the receive buffer in the
|                     CAN hardware
| RETURN VALUE:       The return value says that a transmit request was successful
|                     or not
| DESCRIPTION:        Transmit functions for gateway issues (with dynamic
|                     messages). If the transmit buffer is not free, the message
|                     is inserted in the FIFO ring buffer.
*********************************************************************** */
/* CODE CATEGORY 2 START */
# if defined ( V_ENABLE_USED_GLOBAL_VAR )
/* txMsgStruct is located in far memory */
C_API_1 vuint8 C_API_2 CanMsgTransmit( CAN_CHANNEL_CANTYPE_FIRST V_MEMRAM1_FAR tCanMsgTransmitStruct V_MEMRAM2_FAR V_MEMRAM3_FAR *txMsgStruct )
{
# else
C_API_1 vuint8 C_API_2 CanMsgTransmit( CAN_CHANNEL_CANTYPE_FIRST tCanMsgTransmitStruct *txMsgStruct )
{
# endif
  CanDeclareGlobalInterruptOldStatus
  vuint8                   rc;
  CanObjectHandle          txMailboxHandle;
  CanObjectHandle          logTxObjHandle;

  tCanTxMsgTransmissionParaStruct txPara;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  assertUser( (txMsgStruct != V_NULL), channel, kErrorNullPointerParameter);


  CanNestedGlobalInterruptDisable();

  /* --- test on CAN transmit switch --- */
  if ( (canStatus[channel] & kCanTxOn) != kCanTxOn ) /* PRQA S 2842 */ /* MD_Can_Assertion */                /* transmit path switched off */
  {
    CanNestedGlobalInterruptRestore();
    return kCanTxFailed;
  }

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    CanNestedGlobalInterruptRestore();
    return(kCanCommunicationDisabled);
  }
# endif

# if defined( C_ENABLE_SLEEP_WAKEUP )
  assertUser(!CanLL_HwIsSleep(canHwChannel), channel, kErrorCanSleep); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
  assertUser(!CanLL_HwIsStop(canHwChannel), channel, kErrorCanStop); /* PRQA S 2842 */ /* MD_Can_Assertion */

  /* --- check on passive state --- */
# if defined( C_ENABLE_ECU_SWITCH_PASS )
  if ( canPassive[channel] != (vuint8)0) /* PRQA S 2842 */ /* MD_Can_Assertion */                            /*  set passive ? */
  {
    CanNestedGlobalInterruptRestore();
#  if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
    APPL_CAN_MSGTRANSMITCONF( CAN_CHANNEL_CANPARA_ONLY );
#  endif
    return (kCanTxOk);
  }
# endif /* C_ENABLE_ECU_SWITCH_PASS */

  /* calculate index for canHandleCurTxObj (logical object handle) */
  logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_MB_MSG_TRANSMIT_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 2842, 2985, 4393 */ /* MD_Can_Assertion, MD_Can_ConstValue, MD_Can_MixedSigns */

  /* check for transmit message object free --------------------------------- */
  /* MsgObj used?  */
  if (( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree ))
  {
    CanNestedGlobalInterruptRestore();
    return kCanTxFailed;
  }

  /* Obj, pMsgObject points to is free, transmit msg object: ---------------- */
  /* Save hdl of msgObj to be transmitted */
  canHandleCurTxObj[logTxObjHandle] = kCanBufferMsgTransmit;
  CanNestedGlobalInterruptRestore();



  txMailboxHandle = CAN_HL_MB_MSG_TRANSMIT_INDEX(canHwChannel); /* PRQA S 2842 */ /* MD_Can_Assertion */
  assertHardware(CanLL_TxIsObjFree(canHwChannel, CAN_HL_MB_HWOBJHANDLE(txMailboxHandle)), channel, kErrorTxBufferBusy); /* PRQA S 2842 */ /* MD_Can_Assertion */

  txPara.mailboxHandle = txMailboxHandle;
  txPara.hwObjHandle = CAN_HL_MB_HWOBJHANDLE(txMailboxHandle);
  txPara.logTxObjHandle = logTxObjHandle;
  txPara.txMsgStruct = txMsgStruct;

  CanLL_TxBeginMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

  CanNestedGlobalInterruptDisable();
  /* Copy all data into transmit object */


  /* If CanTransmit was interrupted by a re-initialization or CanOffline */
  /* no transmitrequest of this action should be started      */
  if ((canHandleCurTxObj[logTxObjHandle] == kCanBufferMsgTransmit) && ((canStatus[channel] & kCanTxOn) == kCanTxOn)) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
     CanLL_TxCopyMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

     rc = CanLL_TxStartMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

     {
# if defined( C_ENABLE_TX_OBSERVE )
       ApplCanTxObjStart( CAN_CHANNEL_CANPARA_FIRST logTxObjHandle );
# endif
       /* explicit set of rc to kCanTxOk not necessary, because kCanTxOk and kCanOk are identical (return of CanLL_TxStartMsgTransmit) */
     }
  }
  else
  {
    /* release TxHandle (CanOffline) */
    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;
    rc = kCanTxFailed;
  }

  CanNestedGlobalInterruptRestore();

  CanLL_TxEndMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &txPara);

  return rc;
}
/* END OF CanMsgTransmit() */
/* CODE CATEGORY 2 END */
#endif


#if defined( C_ENABLE_DYN_TX_OBJECTS )
/* **********************************************************************
| NAME:           CanGetDynTxObj
| PARAMETER:      txHandle - Handle of the dynamic object to reserve
| RETURN VALUE:   kCanNoTxDynObjAvailable (0xFF) -
|                   object not available
|                 0..F0 -
|                   Handle to dynamic transmission object
| DESCRIPTION:    Function reserves and return a handle to a dynamic
|                   transmission object
|
|                 To use dynamic transmission, an application must get
|                 a dynamic object from CAN-driver.
|                 Before transmission, application must set all attributes
|                 (id, dlc, data, confirmation function/flag, pretransmission
|                 etc. - as configurated).
|                 Application can use a dynamic object for one or many
|                 transmissions (as it likes) - but finally, it must
|                 release the dynamic object by calling CanReleaseDynTxObj.
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 CanTransmitHandle C_API_2 CanGetDynTxObj(CanTransmitHandle txHandle ) C_API_3
{
  CanTransmitHandle nTxDynObj;
  CanDeclareGlobalInterruptOldStatus
  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif

  nTxDynObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

  CanNestedGlobalInterruptDisable();
  if ( canTxDynObjReservedFlag[nTxDynObj] != (vuint8)0)
  {
    CanNestedGlobalInterruptRestore();
    return kCanNoTxDynObjAvailable;
  }
  /*  Mark dynamic object as used  */
  canTxDynObjReservedFlag[nTxDynObj] = 1;

# if defined( C_ENABLE_CONFIRMATION_FLAG )
  CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] &= (vuint8)(~CanGetConfirmationMask(txHandle)); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
  CanNestedGlobalInterruptRestore();

  /* Initialize dynamic object */
# if defined( C_ENABLE_DYN_TX_DATAPTR )
  canDynTxDataPtr[nTxDynObj] = V_NULL;
# endif


  return (txHandle);
}
/* CODE CATEGORY 3 END */


/* **********************************************************************
| NAME:           CanReleaseDynTxObj
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
| RETURN VALUE:   --
| DESCRIPTION:    Function releases dynamic transmission object
|                   which was reserved before (calling CanGetDynTxObj)
|
|                 After a transmission of one or more messages is finished,
|                 application must free the reserved resource, formally the
|                 dynamic transmission object calling this function.
|
|                 As the number of dynamic transmission object is limited,
|                 application should not keep unused dynamic transmission
|                 objects for a longer time.
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 vuint8 C_API_2 CanReleaseDynTxObj(CanTransmitHandle txHandle) C_API_3
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL
# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle queueElementIdx; /* index for accessing the tx queue */
  CanSignedTxHandle elementBitIdx;  /* bit index within the tx queue element */
  CanTransmitHandle queueBitPos;  /* physical bitposition of the handle */
# endif

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

  assertInternal((canTxDynObjReservedFlag[dynTxObj] != (vuint8)0), channel, kErrorReleasedUnusedDynObj);

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  #if defined( C_ENABLE_INTERNAL_CHECK ) && defined( C_MULTIPLE_RECEIVE_CHANNEL )
  if (sizeof(queueBitPos) == 1u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */
  {
    assertInternal( ((vuint16)((vuint16)kCanNumberOfTxObjects + (vuint16)CanTxQueuePadBits[kCanNumberOfChannels - 1u]) <= 256u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 2880 */ /* MD_Can_ConstValue */
  }
  else
  {
    if (((sizeof(tCanTxQueuePadBits) == 1u) && (kCanNumberOfTxObjects > (65536u - 256u))) || (sizeof(tCanTxQueuePadBits) > 1u)) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */
    {
      assertInternal( ((vuint32)((vuint32)kCanNumberOfTxObjects + (vuint32)CanTxQueuePadBits[kCanNumberOfChannels - 1u]) <= 65536u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 2880 */ /* MD_Can_ConstValue */
    }
  }
  #endif
  queueBitPos = txHandle + CAN_HL_TXQUEUE_PADBITS(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */
  queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */ /* PRQA S 4394 */ /* MD_Can_MixedSigns */
  elementBitIdx = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask); /* get the flag index wihtin the queue element */ /* PRQA S 2985, 4394 */ /* MD_Can_ConstValue, MD_Can_MixedSigns */
  if( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
  {
  }
  else
# endif
  {
    if (
# if defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
         (confirmHandle[channel] == txHandle) ||       /* confirmation active ? */
# endif
         (canHandleCurTxObj[(vsintx)CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)] != txHandle) ) /* PRQA S 2985 */ /* MD_Can_ConstValue */
    {
      /*  Mark dynamic object as not used  */
      canTxDynObjReservedFlag[dynTxObj] = 0;
      return(kCanDynReleased);
    }
  }
  return(kCanDynNotReleased);
}
/* CODE CATEGORY 3 END */
#endif /* C_ENABLE_DYN_TX_OBJECTS */


#if defined( C_ENABLE_DYN_TX_ID )
# if !defined( C_ENABLE_EXTENDED_ID ) ||\
     defined( C_ENABLE_MIXED_ID )
/* **********************************************************************
| NAME:           CanDynTxObjSetId
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
|                 id -
|                   Id (standard-format) to register with dynamic object
| RETURN VALUE:   --
| DESCRIPTION:    Function registers submitted id (standard format)
|                 with dynamic object referenced by handle.
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanDynTxObjSetId(CanTransmitHandle txHandle, vuint16 id) C_API_3
{
  CanTransmitHandle dynTxObj;

  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif
  assertUser(id <= (vuint16)0x7FF, channel, kErrorWrongId);

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

#  if defined( C_ENABLE_MIXED_ID )
  canDynTxIdType[dynTxObj] = kCanIdTypeStd;
#  endif
#  if defined( C_ENABLE_CAN_FD_USED )
  canDynTxFdType[dynTxObj] = kCanFdTypeClassic;
#  endif

  canDynTxId0[dynTxObj] = MK_STDID0(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  if (kCanNumberOfUsedCanTxIdTables > 1)
  canDynTxId1[dynTxObj] = MK_STDID1(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 2)
  canDynTxId2[dynTxObj] = MK_STDID2(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 3)
  canDynTxId3[dynTxObj] = MK_STDID3(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 4)
  canDynTxId4[dynTxObj] = MK_STDID4(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
}
/* CODE CATEGORY 2 END */


#  if defined(C_ENABLE_CAN_FD_USED)
/* **********************************************************************
| NAME:           CanDynTxObjSetFdId
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
|                 id -
|                   Id (standard-format) to register with dynamic object
| RETURN VALUE:   --
| DESCRIPTION:    Function registers submitted id (standard and FD format)
|                 with dynamic object referenced by handle.
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanDynTxObjSetFdId(CanTransmitHandle txHandle, vuint16 id) C_API_3
{
  CanTransmitHandle dynTxObj;

  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
#  endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif
  assertUser(id <= (vuint16)0x7FF, channel, kErrorWrongId);
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(CAN_HL_IS_CH_CANFD(channel)==kCanTrue, channel, kErrorNoCanFd);
# endif

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

#  if defined( C_ENABLE_MIXED_ID )
  canDynTxIdType[dynTxObj] = kCanIdTypeStd;
#  endif
  canDynTxFdType[dynTxObj] = kCanFdTypeFd;


  canDynTxId0[dynTxObj] = MK_STDID0(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  if (kCanNumberOfUsedCanTxIdTables > 1)
  canDynTxId1[dynTxObj] = MK_STDID1(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 2)
  canDynTxId2[dynTxObj] = MK_STDID2(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 3)
  canDynTxId3[dynTxObj] = MK_STDID3(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 4)
  canDynTxId4[dynTxObj] = MK_STDID4(id); /* PRQA S 4491 */ /* MD_Can_IntegerCast */
#  endif
}
/* CODE CATEGORY 2 END */
#  endif /* C_ENABLE_CAN_FD_USED */
# endif /* !defined( C_ENABLE_EXTENDED_ID ) || defined( C_ENABLE_MIXED_ID ) */
#endif /* C_ENABLE_DYN_TX_ID */


#if defined( C_ENABLE_DYN_TX_ID ) && \
    defined( C_ENABLE_EXTENDED_ID )
/* **********************************************************************
| NAME:           CanDynTxObjSetExtId32
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
|                 id -
|                   Id (extended-format) to register with dynamic object
| RETURN VALUE:   --
| DESCRIPTION:    Function registers submitted id (extended format)
|                 with dynamic object referenced by handle.
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanDynTxObjSetExtId32(CanTransmitHandle txHandle, vuint32 id) C_API_3
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif
  assertUser(id <= (vuint32)0x1FFFFFFF, channel, kErrorWrongId);

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

# if defined( C_ENABLE_MIXED_ID )
  canDynTxIdType[dynTxObj] = kCanIdTypeExt;
# endif
#  if defined( C_ENABLE_CAN_FD_USED )
  canDynTxFdType[dynTxObj] = kCanFdTypeClassic;
#  endif

  canDynTxId0[dynTxObj]      = MK_EXTID0(id);
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canDynTxId1[dynTxObj]      = MK_EXTID1(id);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canDynTxId2[dynTxObj]      = MK_EXTID2(id);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canDynTxId3[dynTxObj]      = MK_EXTID3(id);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canDynTxId4[dynTxObj]      = MK_EXTID4(id);
# endif
}
/* CODE CATEGORY 2 END */


# if defined(C_ENABLE_CAN_FD_USED)
/* **********************************************************************
| NAME:           CanDynTxObjSetFdExtId32
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
|                 id -
|                   Id (extended-format) to register with dynamic object
| RETURN VALUE:   --
| DESCRIPTION:    Function registers submitted id (extended and FD format)
|                 with dynamic object referenced by handle.
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanDynTxObjSetFdExtId32(CanTransmitHandle txHandle, vuint32 id) C_API_3
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif
  assertUser(id <= (vuint16)0x1FFFFFFF, channel, kErrorWrongId);
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(CAN_HL_IS_CH_CANFD(channel)==kCanTrue, channel, kErrorNoCanFd);
# endif

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

# if defined( C_ENABLE_MIXED_ID )
  canDynTxIdType[dynTxObj] = kCanIdTypeExt;
# endif
  canDynTxFdType[dynTxObj] = kCanFdTypeFd;

  canDynTxId0[dynTxObj]      = MK_EXTID0(id);
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canDynTxId1[dynTxObj]      = MK_EXTID1(id);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canDynTxId2[dynTxObj]      = MK_EXTID2(id);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canDynTxId3[dynTxObj]      = MK_EXTID3(id);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canDynTxId4[dynTxObj]      = MK_EXTID4(id);
# endif
}
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_CAN_FD_USED */
#endif


#if defined( C_ENABLE_DYN_TX_DLC )
/* **********************************************************************
| NAME:           CanDynTxObjSetMessageLength
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
|                 messageLen -
|                   message length to register with dynamic object
| RETURN VALUE:   --
| DESCRIPTION:    Function registers data length code with
|                 dynamic object referenced by submitted handle.
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanDynTxObjSetMessageLength(CanTransmitHandle txHandle, vuint8 messageLength) C_API_3
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif
  assertUser(messageLength <= CAN_HL_MAX_LEN(channel), channel, kErrorTxDlcTooLarge);

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

# if defined( C_ENABLE_EXTENDED_ID )
  canDynTxDLC[dynTxObj] = MK_TX_DLC_EXT(CAN_LEN2DLC(messageLength)); /* PRQA S 2842 */ /* MD_Can_Assertion */
# else
  canDynTxDLC[dynTxObj] = MK_TX_DLC(CAN_LEN2DLC(messageLength)); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
# if defined( C_ENABLE_CAN_FD_FULL ) && (defined( C_ENABLE_PRETRANSMIT_FCT ) || defined( C_ENABLE_COPY_TX_DATA ))
  canDynTxMessageLength[dynTxObj] = messageLength;
# endif
}
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_DYN_TX_DLC */


#if defined( C_ENABLE_DYN_TX_DATAPTR )
/* **********************************************************************
| NAME:           CanDynTxObjSetDataPtr
| PARAMETER:      hTxObj -
|                   Handle of dynamic transmission object
|                 pData -
|                   data reference to be stored in data buffer of dynamic object
| RETURN VALUE:   --
| DESCRIPTION:    Functions stores reference to data registered with
|                 dynamic object.
|
|                 The number of byte copied is (always) 8. The number of
|                 relevant (and consequently evaluated) byte is to be
|                 taken from function CanDynObjGetDLC.
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanDynTxObjSetDataPtr(CanTransmitHandle txHandle, void* pData) C_API_3
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL

  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);
# if ( kCanNumberOfTxStatObjects > 0) || defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);
# endif

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 2985 */ /* MD_Can_ConstValue */

  canDynTxDataPtr[dynTxObj] = (TxDataPtr)pData; /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
}
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_DYN_TX_DATAPTR */






#if defined( C_ENABLE_TX_MASK_EXT_ID )
/* **********************************************************************
| NAME:               CanSetTxIdExtHi
| CALLED BY:
| PRECONDITIONS:      CanInitPower should already been called.
| PARAMETER:          new source address for the 29-bit CAN-ID
| RETURN VALUE:       -
| DESCRIPTION:        Sets the source address in the lower 8 bit of the
|                     29-bit CAN identifier.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetTxIdExtHi( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif
  assertUser(mask <= (vuint8)0x1F, channel, kErrorWrongMask);

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0x00FFFFFFUL)) | MK_EXTID0((vuint32)mask<<24); /* PRQA S 2842 */ /* MD_Can_Assertion */
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0x00FFFFFFUL)) | MK_EXTID1((vuint32)mask<<24); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0x00FFFFFFUL)) | MK_EXTID2((vuint32)mask<<24); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0x00FFFFFFUL)) | MK_EXTID3((vuint32)mask<<24); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0x00FFFFFFUL)) | MK_EXTID4((vuint32)mask<<24); /* PRQA S 2842 */ /* MD_Can_Assertion */
# endif
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanSetTxIdExtMidHi
| CALLED BY:
| PRECONDITIONS:      CanInitPower should already been called.
| PARAMETER:          new source address for the 29-bit CAN-ID
| RETURN VALUE:       -
| DESCRIPTION:        Sets the source address in the lower 8 bit of the
|                     29-bit CAN identifier.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetTxIdExtMidHi( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0xFF00FFFFUL)) | MK_EXTID0((vuint32)mask<<16); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0xFF00FFFFUL)) | MK_EXTID1((vuint32)mask<<16); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0xFF00FFFFUL)) | MK_EXTID2((vuint32)mask<<16); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0xFF00FFFFUL)) | MK_EXTID3((vuint32)mask<<16); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0xFF00FFFFUL)) | MK_EXTID4((vuint32)mask<<16); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanSetTxIdExtMidLo
| CALLED BY:
| PRECONDITIONS:      CanInitPower should already been called.
| PARAMETER:          new source address for the 29-bit CAN-ID
| RETURN VALUE:       -
| DESCRIPTION:        Sets the source address in the lower 8 bit of the
|                     29-bit CAN identifier.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetTxIdExtMidLo( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0xFFFF00FFUL)) | MK_EXTID0((vuint32)mask<<8); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0xFFFF00FFUL)) | MK_EXTID1((vuint32)mask<<8); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0xFFFF00FFUL)) | MK_EXTID2((vuint32)mask<<8); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0xFFFF00FFUL)) | MK_EXTID3((vuint32)mask<<8); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0xFFFF00FFUL)) | MK_EXTID4((vuint32)mask<<8); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:               CanSetTxIdExtLo
| CALLED BY:
| PRECONDITIONS:      CanInitPower should already been called.
| PARAMETER:          new source address for the 29-bit CAN-ID
| RETURN VALUE:       -
| DESCRIPTION:        Sets the source address in the lower 8 bit of the
|                     29-bit CAN identifier.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetTxIdExtLo( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0xFFFFFF00UL)) | MK_EXTID0((vuint32)mask); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0xFFFFFF00UL)) | MK_EXTID1((vuint32)mask); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0xFFFFFF00UL)) | MK_EXTID2((vuint32)mask); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0xFFFFFF00UL)) | MK_EXTID3((vuint32)mask); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0xFFFFFF00UL)) | MK_EXTID4((vuint32)mask); /* PRQA S 2842, 2985 */ /* MD_Can_Assertion, MD_Can_ConstValue */
# endif
}
/* CODE CATEGORY 4 END */
#endif


#if defined( C_ENABLE_TX_OBSERVE )
/* **********************************************************************
| NAME:               CanTxGetActHandle
| CALLED BY:
| PRECONDITIONS:
| PARAMETER:          logical hardware object handle
| RETURN VALUE:       handle of the message in the assigned mailbox
| DESCRIPTION:        get transmit handle of the message, which is currently
|                     in the mailbox txHwObject.
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 CanTransmitHandle C_API_2 CanTxGetActHandle( CanObjectHandle logicalTxHdl ) C_API_3
{
  assertUser(logicalTxHdl < kCanNumberOfTxMailboxes, kCanAllChannels, kErrorTxHwHdlTooLarge);

  return (canHandleCurTxObj[logicalTxHdl]); /* PRQA S 2842 */ /* MD_Can_Assertion */
}
/* CODE CATEGORY 3 END */
#endif


#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
/* **********************************************************************
| NAME:               CanHL_SetVariableRxDatalen
| CALLED BY:
| PRECONDITIONS:
| PARAMETER:          rxHandle: Handle of receive Message for which the datalen has
|                               to be changed
|                     dataLen:  new number of bytes, which have to be copied to the
|                               message buffer.
| RETURN VALUE:       -
| DESCRIPTION:        change the dataLen of a receive message to copy a
|                     smaller number of bytes than defined in the database.
|                     the dataLen can only be decreased. If the parameter
|                     dataLen is bigger than the statically defined value
|                     the statically defined value will be set.
*********************************************************************** */
/* CODE CATEGORY 1 START */
static void CanHL_SetVariableRxDatalen (CanReceiveHandle rxHandle, vuint8 dataLen)
{
  assertInternal(rxHandle < kCanNumberOfRxObjects, kCanAllChannels, kErrorRxHandleWrong);  /* legal rxHandle ? */
  /* assertion for dataLen not necessary due to runtime check */

  if (dataLen < CanGetDerivedRxDataLen(rxHandle)) /* PRQA S 2842 */ /* MD_Can_Assertion */
  {
    canVariableRxDataLen[rxHandle] = dataLen; /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
  else
  {
    canVariableRxDataLen[rxHandle] = CanGetDerivedRxDataLen(rxHandle); /* PRQA S 2842 */ /* MD_Can_Assertion */
  }
}
/* CODE CATEGORY 1 END */
#endif




#if defined( C_ENABLE_COND_RECEIVE_FCT )
/* **********************************************************************
| NAME:               CanSetMsgReceivedCondition
| CALLED BY:          Application
| PRECONDITIONS:
| PARAMETER:          -.
| RETURN VALUE:       -
| DESCRIPTION:        The service function CanSetMsgReceivedCondition()
|                     enables the calling of ApplCanMsgCondReceived()
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 void C_API_2 CanSetMsgReceivedCondition( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((channel < kCanNumberOfChannels), kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  canMsgCondRecState[channel] = kCanTrue; /* PRQA S 2842 */ /* MD_Can_Assertion */
}
/* CODE CATEGORY 3 END */


/* **********************************************************************
| NAME:               CanResetMsgReceivedCondition
| CALLED BY:          Application
| PRECONDITIONS:
| PARAMETER:          -
| RETURN VALUE:       -
| DESCRIPTION:        The service function CanResetMsgReceivedCondition()
|                     disables the calling of ApplCanMsgCondReceived()
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 void C_API_2 CanResetMsgReceivedCondition( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((channel < kCanNumberOfChannels), kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  canMsgCondRecState[channel] = kCanFalse; /* PRQA S 2842 */ /* MD_Can_Assertion */
}
/* CODE CATEGORY 3 END */


/* **********************************************************************
| NAME:               CanGetMsgReceivedCondition
| CALLED BY:          Application
| PRECONDITIONS:
| PARAMETER:          -
| RETURN VALUE:       status of Conditional receive function:
|                     kCanTrue : Condition is set -> ApplCanMsgCondReceived
|                                will be called
|                     kCanFalse: Condition is not set -> ApplCanMsgCondReceived
|                                will not be called
| DESCRIPTION:        The service function CanGetMsgReceivedCondition()
|                     returns the status of the condition for calling
|                     ApplCanMsgCondReceived()
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 vuint8 C_API_2 CanGetMsgReceivedCondition( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((channel < kCanNumberOfChannels), kCanAllChannels, kErrorChannelHdlTooLarge);
# endif

  return( canMsgCondRecState[channel] ); /* PRQA S 2842 */ /* MD_Can_Assertion */
}
/* CODE CATEGORY 3 END */
#endif


#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
/* **********************************************************************
| NAME:           ApplCanChannelDummy
| PARAMETER:      channel
|                 current receive channel
| RETURN VALUE:   ---
| DESCRIPTION:    dummy-function for unused Callback-functions
*********************************************************************** */
/* CODE CATEGORY 3 START */
C_API_1 void C_API_2 ApplCanChannelDummy( CanChannelHandle channel )
{
  CAN_DUMMY_STATEMENT(channel); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 3 END */
#endif /* C_MULTIPLE_RECEIVE_CHANNEL */


#if defined( C_MULTIPLE_RECEIVE_CHANNEL ) 
/* **********************************************************************
| NAME:           ApplCanRxStructPtrDummy
| PARAMETER:      rxStruct
|                 pointer of CanRxInfoStruct
| RETURN VALUE:   kCanCopyData
| DESCRIPTION:    dummy-function for unused Callback-functions
*********************************************************************** */
/* CODE CATEGORY 1 START */
C_API_1 vuint8 C_API_2 ApplCanRxStructPtrDummy( CanRxInfoStructPtr rxStruct )
{
  CAN_DUMMY_STATEMENT(rxStruct); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return kCanCopyData;
}
/* CODE CATEGORY 1 END */


/* **********************************************************************
| NAME:           ApplCanTxHandleDummy
| PARAMETER:      txHandle
|                 transmit handle
| RETURN VALUE:   ---
| DESCRIPTION:    dummy-function for unused Callback-functions
*********************************************************************** */
/* CODE CATEGORY 1 START */
C_API_1 void C_API_2 ApplCanTxHandleDummy( CanTransmitHandle txHandle )
{
  CAN_DUMMY_STATEMENT(txHandle); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */
#endif /* C_MULTIPLE_RECEIVE_CHANNEL || C_HL_ENABLE_DUMMY_FCT_CALL */




#if defined( C_ENABLE_RX_QUEUE )
/* **********************************************************************
| NAME:               CanHL_ReceivedRxHandleQueue
| CALLED BY:          CanHL_BasicCanMsgReceived, CanHL_FullCanMsgReceived
| Preconditions:      none
| PARAMETER:          none
| RETURN VALUE:       none
| DESCRIPTION:        Writes receive data into queue or starts further
|                     processing for this message
*********************************************************************** */
/* CODE CATEGORY 1 START */
static vuint8 CanHL_ReceivedRxHandleQueue(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus
  tCanRxInfoStruct    *pCanRxInfoStruct;

  tCanRxQueueCopyParaStruct rxQueueCopyPara;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);
# endif

  /* Rx Queue is supported with C_HL_ENABLE_RX_INFO_STRUCT_PTR only! */
  pCanRxInfoStruct =  &canRxInfoStruct[channel];

  /* if C_ENABLE_APPLCANPRERXQUEUE is not set, a macro ApplCanPreRxQueue has to be provided by the tool */
  /* in case of ranges, ApplCanPreRxQueue has to return kCanCopyData! */
# if defined( C_ENABLE_APPLCANPRERXQUEUE )
  if(ApplCanPreRxQueue(CAN_HL_P_RX_INFO_STRUCT(channel)) == kCanCopyData)
# endif
  {
    /* Disable the interrupts because nested interrupts can take place -
      CAN interrupts of all channels have to be disabled here */
    CanNestedGlobalInterruptDisable();
    if(canRxQueue.canRxQueueCount < kCanRxQueueSize)   /* Queue full ? */
    {
      if (canRxQueue.canRxQueueWriteIndex == (kCanRxQueueSize - 1u) )
      {
        canRxQueue.canRxQueueWriteIndex = 0;
      }
      else
      {
        canRxQueue.canRxQueueWriteIndex++;
      }
      canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].Channel = channel;
      canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].Handle = pCanRxInfoStruct->Handle;

      rxQueueCopyPara.dest = (CanMsgTransmitStructPtr)&canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].CanChipMsgObj;
      rxQueueCopyPara.src = pCanRxInfoStruct;

      CanLL_RxQueueCopyMsgObj(CAN_CHANNEL_CANPARA_FIRST &rxQueueCopyPara);

      canRxQueue.canRxQueueCount++;
    }
# if defined( C_ENABLE_RXQUEUE_OVERRUN_NOTIFY )
    else
    {
      ApplCanRxQueueOverrun();
    }
# endif
    CanNestedGlobalInterruptRestore();
  }
# if defined( C_ENABLE_APPLCANPRERXQUEUE )
  else
  {
    /* Call the application call-back functions and set flags */
#  if defined( C_ENABLE_RX_QUEUE_RANGE )
    if (pCanRxInfoStruct->Handle < kCanNumberOfRxObjects )
#  endif
    {
      return CanHL_ReceivedRxHandle(CAN_CHANNEL_CANPARA_FIRST pCanRxInfoStruct);
    }
  }
# endif
  return kCanHlFinishRx;
}
/* CODE CATEGORY 1 END */


/* **********************************************************************
| NAME:               CanHandleRxMsg
| CALLED BY:          Application
| Preconditions:      none
| PARAMETER:          none
| RETURN VALUE:       none
| DESCRIPTION:        Calls PreCopy and/or Indication, if existent and
|                     set the indication flag
*********************************************************************** */
/* CODE CATEGORY 2 START */
C_API_1 void C_API_2 CanHandleRxMsg(void)
{
  CanDeclareGlobalInterruptOldStatus
  CAN_CHANNEL_CANTYPE_LOCAL
  tCanRxInfoStruct        localCanRxInfoStruct;

  while ( canRxQueue.canRxQueueCount != (vuintx)0 )
  {

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    channel = canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].Channel;
# endif


    CAN_CAN_INTERRUPT_DISABLE( channel );

    /* Call the application call-back functions and set flags */
    localCanRxInfoStruct.Handle      = canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].Handle;
    localCanRxInfoStruct.pChipData   = (CanChipDataPtr)&(canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].CanChipMsgObj.DataFld[0]);
    canRDSRxPtr[channel] = localCanRxInfoStruct.pChipData;
    localCanRxInfoStruct.pChipMsgObj = (CanChipMsgPtr) &(canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].CanChipMsgObj); /* PRQA S 0310 */ /* MD_Can_PointerCast */
    localCanRxInfoStruct.Channel     = channel;

    CanLL_RxQueueSetRxInfoStructExtension(CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct);

# if defined( C_ENABLE_RX_QUEUE_RANGE )
    switch (localCanRxInfoStruct.Handle)
    {
#  if defined( C_ENABLE_RANGE_0 )
      case kCanRxHandleRange0: (void)APPLCANRANGE0PRECOPY( &localCanRxInfoStruct ); break;
#  endif
#  if defined( C_ENABLE_RANGE_1 )
      case kCanRxHandleRange1: (void)APPLCANRANGE1PRECOPY( &localCanRxInfoStruct ); break;
#  endif
#  if defined( C_ENABLE_RANGE_2 )
      case kCanRxHandleRange2: (void)APPLCANRANGE2PRECOPY( &localCanRxInfoStruct ); break;
#  endif
#  if defined( C_ENABLE_RANGE_3 )
      case kCanRxHandleRange3: (void)APPLCANRANGE3PRECOPY( &localCanRxInfoStruct ); break;
#  endif
      default:
#  if defined( C_ENABLE_INDICATION_FLAG ) || \
      defined( C_ENABLE_INDICATION_FCT )
             if( CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct ) == kCanHlContinueRx )
             {
               CanHL_IndRxHandle(localCanRxInfoStruct.Handle);
             }
#  else
             (void) CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct );
#  endif
             break;
   }
# else
#  if defined( C_ENABLE_INDICATION_FLAG ) || \
      defined( C_ENABLE_INDICATION_FCT )
    if( CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct ) == kCanHlContinueRx )
    {
      CanHL_IndRxHandle(localCanRxInfoStruct.Handle);
    }
#  else
    (void) CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct );
#  endif
# endif

    CAN_CAN_INTERRUPT_RESTORE( channel );

    CanNestedGlobalInterruptDisable();
    if (canRxQueue.canRxQueueReadIndex == (kCanRxQueueSize - 1u) )
    {
      canRxQueue.canRxQueueReadIndex = 0;
    }
    else
    {
      canRxQueue.canRxQueueReadIndex++;
    }
    canRxQueue.canRxQueueCount--;
    CanNestedGlobalInterruptRestore();
  }
  return;
}
/* END OF CanHandleRxMsg() */
/* CODE CATEGORY 2 END */


/* **********************************************************************
| NAME:               CanDeleteRxQueue
| CALLED BY:          Application, CAN driver
| Preconditions:      none
| PARAMETER:          none
| RETURN VALUE:       none
| DESCRIPTION:        delete receive queue
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanDeleteRxQueue(void)
{
  CanDeclareGlobalInterruptOldStatus

  CanNestedGlobalInterruptDisable();
  canRxQueue.canRxQueueWriteIndex  = (vuintx)0xFFFFFFFFu;
  canRxQueue.canRxQueueReadIndex   = 0;
  canRxQueue.canRxQueueCount       = 0;
  CanNestedGlobalInterruptRestore();
}
/* END OF CanDeleteRxQueue() */
/* CODE CATEGORY 4 END */


# if defined( C_ENABLE_RX_QUEUE_EXTERNAL )
/* **********************************************************************
| NAME:               CanSetRxQueuePtr
| CALLED BY:          Application
| Preconditions:      none
| PARAMETER:          pQueue: pointer to the external queue which is used by the driver
| RETURN VALUE:       none
| DESCRIPTION:        If the driver is configured to use a rx queue it is
|                     possible to use an application defined queue.
|                     In case of a QNX system the queue is located in the
|                     system page
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanSetRxQueuePtr(tCanRxQueue* pQueue)
{
  canRxQueuePtr = pQueue;
}
/* CODE CATEGORY 4 END */
# endif /* C_ENABLE_EXTERNAL_RX_QUEUE */
#endif /* C_ENABLE_RX_QUEUE */




#if defined(VGEN_ENABLE_MDWRAP) || \
    defined(VGEN_ENABLE_QWRAP)  || \
    defined(C_ENABLE_UPDATE_BASE_ADDRESS)
/* **********************************************************************
| NAME:           CanBaseAddressRequest
| CALLED BY:      Application
| Preconditions:  none
| PARAMETER:      channel: the CAN channel for which the address is requested
| RETURN VALUE:   ---
| DESCRIPTION:    The application calls this function in order to tell the
|                 CAN driver to request the computation of the virtual
|                 address of the CAN controller.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanBaseAddressRequest(CAN_CHANNEL_CANTYPE_ONLY)
{

  {
    CanLL_BaseAddressRequest(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY);
  }
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:           CanBaseAddressActivate
| CALLED BY:      Application
| Preconditions:  Interrupts have to be disabled
| PARAMETER:      channel: the CAN channel for which the address is requested
| RETURN VALUE:   ---
| DESCRIPTION:    The application calls this function in order to tell the
|                 CAN driver to activate the virtual address of the CAN
|                 controller. The adress has to be requested with
|                 CanBaseAddressRequest() before.
|                 Call is only allowed on Task level and must not interrupt
|                 any CAN driver service functions.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanBaseAddressActivate(CAN_CHANNEL_CANTYPE_ONLY)
{

  {
    CanLL_BaseAddressActivate(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY);
  }
}
/* CODE CATEGORY 4 END */
#endif /* defined(VGEN_ENABLE_MDWRAP) || defined(VGEN_ENABLE_QWRAP) || defined(C_ENABLE_UPDATE_BASE_ADDRESS) */


#if defined(C_ENABLE_GET_CONTEXT)
/* **********************************************************************
| NAME:               CanGetModuleContext
| CALLED BY:          Application
| Preconditions:      none
| PARAMETER:          pContext: pointer to a context strucuture
| RETURN VALUE:       none
| DESCRIPTION:        This function copies the context of the component to a
|                     structure.
|                     interruption by Tx-ISR, busoff-ISR or change of
|                     condititonal message receive are not allowed during contextswitch.
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanGetModuleContext(tCanModuleContextStructPtr pContext)
{
  CAN_CHANNEL_CANTYPE_LOCAL

  CanTransmitHandle txHandle;
# if defined( C_ENABLE_VARIABLE_RX_DATALEN ) || \
     defined( C_ENABLE_INDICATION_FLAG )
  CanReceiveHandle rxHandle;
# endif

# if defined(C_ENABLE_VARIABLE_DLC)
  for (txHandle = 0; txHandle < kCanNumberOfTxObjects; txHandle++)
  {
    pContext->canTxDLC_RAM[txHandle] = canTxDLC_RAM[txHandle];
  }
# endif

# if defined( C_ENABLE_DYN_TX_OBJECTS )
  /*  Reset dynamic transmission object management -------------------------- */
  for (txHandle = 0; txHandle < kCanNumberOfTxDynObjects; txHandle++)
  {
    pContext->canTxDynObjReservedFlag[txHandle] = canTxDynObjReservedFlag[txHandle];

#  if defined( C_ENABLE_DYN_TX_ID )
    pContext->canDynTxId0[txHandle] = canDynTxId0[txHandle];
#   if (kCanNumberOfUsedCanTxIdTables > 1)
    pContext->canDynTxId1[txHandle] = canDynTxId1[txHandle];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 2)
    pContext->canDynTxId2[txHandle] = canDynTxId2[txHandle];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 3)
    pContext->canDynTxId3[txHandle] = canDynTxId3[txHandle];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 4)
    pContext->canDynTxId4[txHandle] = canDynTxId4[txHandle];
#   endif
#   if defined( C_ENABLE_MIXED_ID )
    pContext->canDynTxIdType[txHandle] = canDynTxIdType[txHandle];
#   endif
#   if defined( C_ENABLE_CAN_FD_USED )
    pContext->canDynTxFdType[txHandle] = canDynTxFdType[txHandle];
#   endif
#  endif

#  if defined( C_ENABLE_DYN_TX_DLC )
    pContext->canDynTxDLC[txHandle] = canDynTxDLC[txHandle];
#   if defined( C_ENABLE_CAN_FD_FULL ) && (defined( C_ENABLE_PRETRANSMIT_FCT ) || defined( C_ENABLE_COPY_TX_DATA ))
    pContext->canDynTxMessageLength[txHandle] = canDynTxMessageLength[txHandle];
#   endif
#  endif
#  if defined( C_ENABLE_DYN_TX_DATAPTR )
    pContext->canDynTxDataPtr[txHandle] = canDynTxDataPtr[txHandle];
#  endif
#  if defined( C_ENABLE_CONFIRMATION_FCT )
#  endif
  }
# endif /* C_ENABLE_DYN_TX_OBJECTS */

#if defined( C_ENABLE_VARIABLE_RX_DATALEN ) 
  /*  Initialize the array with received dlc -------------------------- */
  for (rxHandle = 0; rxHandle < kCanNumberOfRxObjects; rxHandle++) {
# if defined( C_ENABLE_VARIABLE_RX_DATALEN )
    pContext->canVariableRxDataLen[rxHandle] = canVariableRxDataLen[rxHandle];
# endif
  }
# endif

#if defined( C_ENABLE_INDICATION_FLAG )
  for (rxHandle = 0; rxHandle < kCanNumberOfIndBytes; rxHandle++) {
    pContext->CanIndicationFlags._c[rxHandle] = CanIndicationFlags._c[rxHandle];
  }
#endif

#if defined( C_ENABLE_CONFIRMATION_FLAG )
  for (txHandle = 0; txHandle < kCanNumberOfConfBytes; txHandle++) {
    pContext->CanConfirmationFlags._c[txHandle] = CanConfirmationFlags._c[txHandle];
  }
#endif

#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL) &&\
    defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (canHwChannel=0; canHwChannel<kCanNumberOfHwChannels; canHwChannel++)
# endif
  {
    pContext->canCanInterruptOldStatus[canHwChannel] = canCanInterruptOldStatus[canHwChannel];
  }
#endif

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (channel=0; channel<kCanNumberOfChannels; channel++)
#endif
  {
    pContext->lastInitObject[channel] = lastInitObject[channel];

#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)
    pContext->canCanInterruptCounter[channel] = canCanInterruptCounter[channel];
#endif

    pContext->canStatus[channel] = canStatus[channel];

# if defined( C_ENABLE_PART_OFFLINE )
    pContext->canTxPartOffline[channel] = canTxPartOffline[channel];
# endif

#if defined( C_ENABLE_ECU_SWITCH_PASS )
    pContext->canPassive[channel] = canPassive[channel];
#endif

#if defined( C_ENABLE_CAN_RAM_CHECK )
    pContext->canComStatus[channel] = canComStatus[channel];
#endif

#if defined( C_ENABLE_TX_MASK_EXT_ID )
    pContext->canTxMask0[channel] = canTxMask0[channel];
# if (kCanNumberOfUsedCanTxIdTables > 1)
    pContext->canTxMask1[channel] = canTxMask1[channel];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
    pContext->canTxMask2[channel] = canTxMask2[channel];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
    pContext->canTxMask3[channel] = canTxMask3[channel];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
    pContext->canTxMask4[channel] = canTxMask4[channel];
# endif
#endif

#if defined( C_ENABLE_COND_RECEIVE_FCT )
    pContext->canMsgCondRecState[channel] = canMsgCondRecState[channel];
#endif

  } /* end  for (channel=0; channel<kCanNumberOfChannels; channel++) */

  for (txHandle = 0; txHandle < kCanNumberOfTxMailboxes; txHandle++)
  {
    pContext->canHandleCurTxObj[txHandle] = canHandleCurTxObj[txHandle];      /* MsgObj is free */
  }


#if defined( C_ENABLE_TRANSMIT_QUEUE )
  for (txHandle = 0; txHandle < kCanTxQueueSize; txHandle++)
  {
    pContext->canTxQueueFlags[txHandle] = canTxQueueFlags[txHandle];
  }
#endif

  CanLL_GetModuleContext(pContext);
}
/* CODE CATEGORY 4 END */
#endif


#if defined(C_ENABLE_SET_CONTEXT)
/* **********************************************************************
| NAME:           CanSetModuleContext
| PARAMETER:      pContext: pointer to a context strucuture
| RETURN VALUE:   vuint8:
| DESCRIPTION:    This function copies a given context to a component
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 vuint8 C_API_2 CanSetModuleContext(tCanModuleContextStructPtr pContext)
{
  vuint8 ret;

  CAN_CHANNEL_CANTYPE_LOCAL

  CanTransmitHandle txHandle;
# if defined( C_ENABLE_VARIABLE_RX_DATALEN ) || \
     defined( C_ENABLE_INDICATION_FLAG )
  CanReceiveHandle rxHandle;
# endif

  if(CanLL_ModuleContextVersion(pContext) == kCanOk)
  {
    ret = 1;

    /* VStdInitPowerOn() is called by higher layer and is not allowed to be called here */

# if defined(C_ENABLE_VARIABLE_DLC)
    for (txHandle = 0; txHandle < kCanNumberOfTxObjects; txHandle++)
    {
      canTxDLC_RAM[txHandle] = pContext->canTxDLC_RAM[txHandle];
    }
# endif

# if defined( C_ENABLE_DYN_TX_OBJECTS )
    /*  Reset dynamic transmission object management -------------------------- */
    for (txHandle = 0; txHandle < kCanNumberOfTxDynObjects; txHandle++)
    {
      canTxDynObjReservedFlag[txHandle] = pContext->canTxDynObjReservedFlag[txHandle];

#  if defined( C_ENABLE_DYN_TX_ID )
      canDynTxId0[txHandle] = pContext->canDynTxId0[txHandle];
#   if (kCanNumberOfUsedCanTxIdTables > 1)
      canDynTxId1[txHandle] = pContext->canDynTxId1[txHandle];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 2)
      canDynTxId2[txHandle] = pContext->canDynTxId2[txHandle];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 3)
      canDynTxId3[txHandle] = pContext->canDynTxId3[txHandle];
#   endif
#   if (kCanNumberOfUsedCanTxIdTables > 4)
      canDynTxId4[txHandle] = pContext->canDynTxId4[txHandle];
#   endif
#   if defined( C_ENABLE_MIXED_ID )
      canDynTxIdType[txHandle] = pContext->canDynTxIdType[txHandle];
#   endif
#   if defined( C_ENABLE_CAN_FD_USED )
      canDynTxFdType[txHandle] = pContext->canDynTxFdType[txHandle];
#   endif
#  endif

#  if defined( C_ENABLE_DYN_TX_DLC )
      canDynTxDLC[txHandle] = pContext->canDynTxDLC[txHandle];
#   if defined( C_ENABLE_CAN_FD_FULL ) && (defined( C_ENABLE_PRETRANSMIT_FCT ) || defined( C_ENABLE_COPY_TX_DATA ))
      canDynTxMessageLength[txHandle] = pContext->canDynTxMessageLength[txHandle];
#   endif
#  endif
#  if defined( C_ENABLE_DYN_TX_DATAPTR )
      canDynTxDataPtr[txHandle] = pContext->canDynTxDataPtr[txHandle];
#  endif
#  if defined( C_ENABLE_CONFIRMATION_FCT )
#  endif
    }
# endif /* C_ENABLE_DYN_TX_OBJECTS */

#if defined( C_ENABLE_VARIABLE_RX_DATALEN ) 
    /*  Initialize the array with received dlc -------------------------- */
    for (rxHandle = 0; rxHandle < kCanNumberOfRxObjects; rxHandle++) {
# if defined( C_ENABLE_VARIABLE_RX_DATALEN )
      canVariableRxDataLen[rxHandle] = pContext->canVariableRxDataLen[rxHandle];
# endif
    }
# endif

# if defined( C_ENABLE_INDICATION_FLAG )
    for (rxHandle = 0; rxHandle < kCanNumberOfIndBytes; rxHandle++) {
      CanIndicationFlags._c[rxHandle] = pContext->CanIndicationFlags._c[rxHandle];
    }
# endif

# if defined( C_ENABLE_CONFIRMATION_FLAG )
    for (txHandle = 0; txHandle < kCanNumberOfConfBytes; txHandle++) {
      CanConfirmationFlags._c[txHandle] = pContext->CanConfirmationFlags._c[txHandle];
    }
# endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    for (canHwChannel=0; canHwChannel<kCanNumberOfHwChannels; canHwChannel++)
# endif
    {
# if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL) &&\
     defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
      canCanInterruptOldStatus[canHwChannel] = pContext->canCanInterruptOldStatus[canHwChannel];
# endif
    }

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    for (channel=0; channel<kCanNumberOfChannels; channel++)
# endif
    {
      lastInitObject[channel] = pContext->lastInitObject[channel];
#if defined( C_ENABLE_CAN_TX_CONF_FCT )
      txInfoStructConf[channel].Channel = channel;
#endif
#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )  || \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
      canRxInfoStruct[channel].Channel = channel;
#endif

#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)
      canCanInterruptCounter[channel] = pContext->canCanInterruptCounter[channel];
#endif

#if defined( C_ENABLE_TX_POLLING )          || \
      defined( C_ENABLE_RX_FULLCAN_POLLING )  || \
      defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
      defined( C_ENABLE_ERROR_POLLING )       || \
      defined( C_ENABLE_WAKEUP_POLLING )      
      canPollingTaskActive[channel] = 0;
#endif

      canStatus[channel] = pContext->canStatus[channel];

#  if defined( C_ENABLE_PART_OFFLINE )
      canTxPartOffline[channel] = pContext->canTxPartOffline[channel];
#  endif

# if defined( C_ENABLE_ECU_SWITCH_PASS )
      canPassive[channel] = pContext->canPassive[channel];
# endif

# if defined( C_ENABLE_CAN_RAM_CHECK )
      canComStatus[channel] = pContext->canComStatus[channel];
# endif

# if defined( C_ENABLE_TX_MASK_EXT_ID )
      canTxMask0[channel] = pContext->canTxMask0[channel];
#  if (kCanNumberOfUsedCanTxIdTables > 1)
      canTxMask1[channel] = pContext->canTxMask1[channel];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 2)
      canTxMask2[channel] = pContext->canTxMask2[channel];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 3)
      canTxMask3[channel] = pContext->canTxMask3[channel];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 4)
      canTxMask4[channel] = pContext->canTxMask4[channel];
#  endif
# endif

# if defined( C_ENABLE_COND_RECEIVE_FCT )
      canMsgCondRecState[channel] = pContext->canMsgCondRecState[channel];
# endif
# if defined( C_ENABLE_PRECOPY_FCT )
      canRxHandle[channel] = kCanRxHandleNotUsed;
# endif
# if defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_DYN_TX_OBJECTS )   && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
      confirmHandle[channel] = kCanBufferFree;
# endif
      CanBaseAddressRequest(CAN_CHANNEL_CANPARA_ONLY);
      CanBaseAddressActivate(CAN_CHANNEL_CANPARA_ONLY);
    } /* end  for (channel=0; channel<kCanNumberOfChannels; channel++) */

    for (txHandle = 0; txHandle < kCanNumberOfTxMailboxes; txHandle++)
    {
      canHandleCurTxObj[txHandle] = pContext->canHandleCurTxObj[txHandle];      /* MsgObj is free */
    }


# if defined( C_ENABLE_TRANSMIT_QUEUE )
    for (txHandle = 0; txHandle < kCanTxQueueSize; txHandle++)
    {
      canTxQueueFlags[txHandle] = pContext->canTxQueueFlags[txHandle];
    }
# endif

    CanLL_SetModuleContext(pContext);
  }
  else
  {
    ret = 0;
  }

  return(ret);
}
/* CODE CATEGORY 4 END */


/* **********************************************************************
| NAME:           CanFinalizeSetModuleContext
| PARAMETER:      pContext: pointer to a context strucuture
| RETURN VALUE:   -
| DESCRIPTION:    This function finalizes the Context switch
|                 e.g. restores the Tx interrupt enable status
*********************************************************************** */
/* CODE CATEGORY 4 START */
C_API_1 void C_API_2 CanFinalizeSetModuleContext(tCanModuleContextStructPtr pContext)
{
  CAN_HW_CHANNEL_CANTYPE_LOCAL

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (canHwChannel=0; canHwChannel<kCanNumberOfHwChannels; canHwChannel++)
# endif
  {
    CanLL_FinalizeSetModuleContext(pContext);
  }
}
/* CODE CATEGORY 4 END */
#endif /* defined(C_ENABLE_SET_CONTEXT) */

/* PRQA L:QAC_Can_Functions_HL */

# if defined( C_ENABLE_MEMCOPY_SUPPORT )
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanCopyToCan
****************************************************************************/
V_DEF_FUNC_API(V_NONE, void, CODE) CanCopyToCan(CanChipDataPtr dst, V_DEF_P2VAR_PARA(V_NONE, void, AUTOMATIC, APPL_VAR) src, vuint8 len) C_API_3 /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  /* ----- Local Variables ---------------------------------------------- */
  vuint8 canllidx;

  /* ----- Implementation ----------------------------------------------- */
  for(canllidx = 0u; canllidx < len; canllidx++)
  {
    dst[canllidx] = ((V_DEF_P2VAR_PARA(V_NONE, vuint8, AUTOMATIC, APPL_VAR))src)[canllidx]; /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
  }
}
/* CODE CATEGORY 1 END */

/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanCopyFromCan
****************************************************************************/
V_DEF_FUNC_API(V_NONE, void, CODE) CanCopyFromCan(V_DEF_P2VAR_PARA(V_NONE, void, AUTOMATIC, APPL_VAR) dst, CanChipDataPtr src, vuint8 len) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  /* ----- Local Variables ---------------------------------------------- */
  vuint8 canllidx;
  /* ----- Implementation ----------------------------------------------- */
  for(canllidx = 0u; canllidx < len; canllidx++)
  {
    ((V_DEF_P2VAR_PARA(V_NONE, vuint8, AUTOMATIC, APPL_VAR))dst)[canllidx] = src[canllidx]; /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
  }
}
/* CODE CATEGORY 1 END */
# endif
# if defined( C_ENABLE_TX_FULLCAN_DELAYED_START )
/************************************************************************
| NAME:               CanTxFullCanDelayedStart
| CALLED BY:          Application 
| Preconditions:      none
| PARAMETER:          none
| RETURN VALUE:       none
| Description:        This function requests the transmission for hardware
|                     objects which are pending to be send.
*************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC_API(V_NONE, void, CODE) CanTxFullCanDelayedStart(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus
  CanObjectHandle txObjHandle;
#  if !defined C_ENABLE_CAN_FD_FULL
  tFlexCANPtr pFlexCANLocal;
#  endif

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif

#  if !defined C_ENABLE_CAN_FD_FULL
  pFlexCANLocal = pFlexCAN(canHwChannel);
#  endif
  /* Disable global interrupts */
  CanNestedGlobalInterruptDisable();
  /* Tx FullCANs are sorted by their priority so the ID with the highest priority has the lowest txObjHandle */
  for(txObjHandle = Can_GetMailboxHwHandle(CAN_HL_MB_TX_STARTINDEX(canHwChannel)); txObjHandle <= Can_GetMailboxHwHandle(CAN_HL_MB_TX_STOPINDEX(canHwChannel) - 1u); txObjHandle++)
  {
    if (CanGetMailboxDelayed(canHwChannel, txObjHandle))
    {
      LOCAL_MAILBOX_ACCESS(canHwChannel, txObjHandle, control) |= kTxCodeTransmit; /* start tx of the message */ /* SBSW_CAN_LL02 */
#  if defined (C_ENABLE_WORKAROUND_ERR005829)
      /* set the reserved mailbox twice to CodeInactive. This workaround is recommended by freescale. */
      LOCAL_MAILBOX_ACCESS(canHwChannel, INDEX_RSVD_MB(canHwChannel), control) |= kTxCodeInactive; /* SBSW_CAN_LL02 */
      LOCAL_MAILBOX_ACCESS(canHwChannel, INDEX_RSVD_MB(canHwChannel), control) |= kTxCodeInactive; /* SBSW_CAN_LL02 */
#  endif
    }
  }
  /* Clear the internal Tx fullcan pendings */
  canLL_canDelayedFullCANTxRequest[channel][0] = 0u;
  canLL_canDelayedFullCANTxRequest[channel][1] = 0u;
  canLL_canDelayedFullCANTxRequest[channel][2] = 0u;
  canLL_canDelayedFullCANTxRequest[channel][3] = 0u;
  /* Restore global interrupts */
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 1 END */
# endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */
# if defined ( C_ENABLE_DRIVER_STATUS )
/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanGetDriverStatus
****************************************************************************/
V_DEF_FUNC_API(V_NONE, vuint8, CODE) CanGetDriverStatus(CAN_CHANNEL_CANTYPE_ONLY)
{ 
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Development Error Checks ------------------------------------- */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);
#  endif

  /* ----- Implementation ----------------------------------------------- */
#   error "Not supported"
}
/* CODE CATEGORY 4 END */
# endif /* C_ENABLE_DRIVER_STATUS         */


/****************************************************************************
| NAME:             CanLL_WriteReg16
****************************************************************************/
/*
|<DataModelStart>| CanLL_WriteReg16
Relation_Context:
# CanHL_InitXXX() #
RamCheck
Relation:
OneChOpt
ChannelAmount
Parameter_PreCompile:
Parameter_Data:
Constrain:
|<DataModelEnd>|
*/
/*!
 * \internal
 *  - #10 write register value
 *    - #20 read back value for RamCheck
 *      - #30 return issue when read back value differs
 * \endinternal
 */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WriteReg16( CAN_CHANNEL_CANTYPE_FIRST CanChipMsgPtr16 regPtr, vuint16 value, vuint16 readMask, CanInitParaStructPtr initPara ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
{
  /* #10 write register value */
  *regPtr = value; /* SBSW_CAN_LL03 */
  CAN_DUMMY_STATEMENT(readMask); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(initPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# if !defined( C_SINGLE_RECEIVE_CHANNEL )
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# endif
}

/****************************************************************************
| NAME:             CanLL_WriteReg32
****************************************************************************/
/*
|<DataModelStart>| CanLL_WriteReg32
Relation_Context:
# CanHL_InitXXX() #
RamCheck
Relation:
OneChOpt
ChannelAmount
Parameter_PreCompile:
Parameter_Data:
Constrain:
|<DataModelEnd>|
*/
/*!
 * \internal
 *  - #10 write register value
 *    - #20 read back value for RamCheck
 *      - #30 return issue when read back value differs
 * \endinternal
 */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WriteReg32( CAN_CHANNEL_CANTYPE_FIRST CanChipMsgPtr32 regPtr, vuint32 value, vuint32 readMask, CanInitParaStructPtr initPara ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
{
  /* #10 write register value */
  *regPtr = value; /* SBSW_CAN_LL03 */
  CAN_DUMMY_STATEMENT(readMask); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(initPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# if !defined( C_SINGLE_RECEIVE_CHANNEL )
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# endif
}


/****************************************************************************
| NAME:             CanLL_WriteProtectedReg16
****************************************************************************/
/*
|<DataModelStart>| CanLL_WriteProtectedReg16
Relation_Context:
# CanHL_InitXXX() #
RamCheck
Relation:
OneChOpt
ChannelAmount
Parameter_PreCompile:
Parameter_Data:
Constrain:
|<DataModelEnd>|
*/
/*!
 * \internal
 *  - #10 write register value
 *    - #20 read back value for RamCheck
 *      - #30 return issue when read back value differs
 * \endinternal
 */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WriteProtectedReg16( CAN_CHANNEL_CANTYPE_FIRST vuint16 area, CanChipMsgPtr16 regPtr, vuint16 value, vuint16 readMask, CanInitParaStructPtr initPara ) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  /* #10 write register value */
  CAN_WRITE_PROTECTED_REG16(area, regPtr, value); /* SBSW_CAN_LL01 */
  CAN_DUMMY_STATEMENT(readMask); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(initPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# if !defined( C_SINGLE_RECEIVE_CHANNEL )
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# endif
  CAN_DUMMY_STATEMENT(area); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
} /* PRQA S 6060 */ /* MD_MSR_STPAR */


/**********************************************************************************************************************
 *  CanLL_InitBegin
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitBegin( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara ) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  vuint8 returnValue;

#if defined (C_ENABLE_GET_CONTEXT) || defined (C_ENABLE_SET_CONTEXT)
# if !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
  canTxIntMask[canHwChannel][0] = 0u;
  canTxIntMask[canHwChannel][1] = 0u;
  canTxIntMask[canHwChannel][2] = 0u;
  canTxIntMask[canHwChannel][3] = 0u;
# endif
#endif
#if defined (C_ENABLE_TX_FULLCAN_DELAYED_START)
/* clear delayed FullCAN TX pending requests */
  canLL_canDelayedFullCANTxRequest[channel][0] = 0u;
  canLL_canDelayedFullCANTxRequest[channel][1] = 0u;
  canLL_canDelayedFullCANTxRequest[channel][2] = 0u;
  canLL_canDelayedFullCANTxRequest[channel][3] = 0u;
#endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */

  /* #10 Set FlexCAN to Init Mode */
  returnValue = CanLL_SetFlexCANToInitMode(CAN_HW_CHANNEL_CANPARA_FIRST initPara->initObject);
  if (returnValue == kCanOk) /* COV_CAN_LL_HARDWARE_BEHAVIOUR */
  {
    /* #20 Perform a soft reset */
    returnValue = CanLL_ExecuteSoftReset(CAN_HW_CHANNEL_CANPARA_ONLY);
  }

  return(returnValue);
}
/* CODE CATEGORY 4 END */


/**********************************************************************************************************************
 *  CanLL_InitBeginSetRegisters
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitBeginSetRegisters( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara )
{
  vuint16 tmpCANmaxmb, tmpCANmcr;
  vuint32 tmpCANctrl2, tmpCANcontrol1;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
#if defined ( C_ENABLE_FLEXCAN_PARITY_CHECK_AVAILABLE )
  tParityCheckPtr pParityCheckLocal = pCanParityCheck(canHwChannel);
#endif
#if defined( C_ENABLE_GLITCH_FILTER )
  tGlitchFilterPtr pGlitchFilterLocal = pCanGlitchFilter(canHwChannel);
#endif

  /* #10 Configure CAN[MCR].MAXMB register value */
  tmpCANmaxmb = ((vuint16)Can_GetMailboxHwHandle(CAN_HL_MB_TX_STOPINDEX(canHwChannel) - (vuint16)1u))
#if defined( C_ENABLE_CANCEL_IN_HW )
  /* #11 Set AEN bit to enable Abort feature (Cancel in Hardware) */
  | kFlexCAN_AEN
#endif
  ;
#if defined( C_ENABLE_CAN_FD_USED )
# if defined( C_MULTIPLE_RECEIVE_CHANNEL ) 
  if (Can_IsHasCANFDBaudrateOfControllerConfig(channel)) /* COV_CAN_FD_BAUDRATE_AVAILABILITY */
# endif
  {
    /* #12 Set FDEN bit to enable CAN FD */
    tmpCANmaxmb |= kFlexCAN_FDEN;
  }
#endif 
  /* #15 Write configured value to register */
  CanLL_WriteProtectedReg16(CAN_CHANNEL_CANPARA_FIRST CAN_AREA(canHwChannel), &pFlexCANLocal->canmaxmb, tmpCANmaxmb, kCanRamCheckMaskMAXMB, initPara); /* SBSW_CAN_LL01 */
  
  /* #20 Configure CAN[MCR].MCR register value */
  tmpCANmcr = CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kNotFlexCAN_MCR_CFG_BTIS; /* SBSW_CAN_LL01 */
  /* #21 Set IRMQ bit to allow individual buffer masking configuration */
  tmpCANmcr |= kFlexCAN_IRMQ
  /* #22 Set SRXDIS bit to disable self-reception feature */
  | kFlexCAN_SRX_DIS
#if defined( C_ENABLE_GLITCH_FILTER )
  /* #23 Set WAK_SRC bit to integrate low-pass filter to protect the FLEXCAN_RX input from spurious wake up */
  | kFlexCAN_WAK_SRC
#endif
#if !defined( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  /* #24 Set FEN bit to enable RX FIFO */
  | kFlexCAN_FEN
#endif
#if defined( C_ENABLE_SLEEP_WAKEUP ) && defined( C_ENABLE_FLEXCAN_STOP_MODE )
  /* #25 Set SLFWAK bit to enable wakeup by bus */
  | kFlexCAN_SLF_WAK
#endif
  ;
#if (defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)) 
# if defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
  /* #26 Set WAK_MSK for wakeup interrupts */
  if(Can_GetCanInterruptCounter(channel) == 0u)
  {
    tmpCANmcr |= kFlexCAN_WAK_MSK;
  }
# endif
#endif
  /* #29 Write configured value to register */
  CanLL_WriteProtectedReg16(CAN_CHANNEL_CANPARA_FIRST CAN_AREA(canHwChannel), &pFlexCANLocal->canmcr, tmpCANmcr, kCanRamCheckMaskMCR, initPara); /* SBSW_CAN_LL01 */
 
  /* #30 Configure CAN[CTRL1] register value */
# if defined( C_ENABLE_EXTENDED_BITTIMING )
  tmpCANcontrol1 = Can_GetControl1OfInitObject(initPara->initObject) & kFlexCAN_CLK_SRC;
# else
  tmpCANcontrol1 = Can_GetControl1OfInitObject(initPara->initObject);
# endif
# if defined( C_ENABLE_FLEXCAN_SAMPLING_MODE )
  /* #31 Set sampling mode */
  tmpCANcontrol1 |= kFlexCAN_SMP;
# endif
  /* #32 Disable BusOff Auto Recovery */
  tmpCANcontrol1 |= kFlexCAN_BOFF_REC;
#if (defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)) 
# if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )  
  /* #33 Enable BusOff interrupt */
  if(Can_GetCanInterruptCounter(channel) == 0u)
  {
    tmpCANcontrol1 |= kFlexCAN_BOFF_MSK;
  }
# endif
#endif
  /* #39 Write configured value to register */
  CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->control1, tmpCANcontrol1, kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */

#if defined( C_ENABLE_EXTENDED_BITTIMING )
  /* #40 Write CAN[CBT] register value (extended bit timing) */
  CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->cbt, Can_GetCBTOfInitObject(initPara->initObject), kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */
#endif

#if defined( C_ENABLE_CAN_FD_USED )
# if defined( C_MULTIPLE_RECEIVE_CHANNEL ) 
  if (Can_IsHasCANFDBaudrateOfControllerConfig(canHwChannel)) /* COV_CAN_FD_BAUDRATE_AVAILABILITY */
# endif
  {
    tCanFDRegisterPtr pCanFDRegisterLocal = pCanFDRegister(canHwChannel);
    /* #50 Write CAN[FDCTRL] register value (CAN FD control) */
    CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pCanFDRegisterLocal->fdctrl, Can_GetFDCTRLOfInitObjectFD(initPara->initObject), kCanMaskAll32, initPara); /* SBSW_CAN_LL09 */
    /* #60 Write CAN[FDCBT] register value (CAN FD bit timing) */
    CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pCanFDRegisterLocal->fdcbt, Can_GetFDCBTOfInitObjectFD(initPara->initObject), kCanMaskAll32, initPara); /* SBSW_CAN_LL09 */
  }
#endif

  /* #70 Configure CAN[CTRL2] register value */
  /* #71 Set RFFN value  */
  tmpCANctrl2 = kFlexCAN_MRP | kFlexCAN_RRS | kFlexCAN_EACEN
# if !defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  | RFFN_VALUE(canHwChannel)
# endif

  /* #72 Set ECR Write Enable */
#if defined ( C_ENABLE_FLEXCAN_PARITY_CHECK_AVAILABLE )
  | kFlexCAN_ECRWRE
#endif
  /* #73 Set TASD Value */
#if defined( C_ENABLE_TASD )
  | Can_GetTASDOfInitObject(initPara->initObject)
#else
  | kFlexCAN_TASD_DEFAULT
#endif
  ;
#if defined( C_ENABLE_CAN_FD_USED )
# if defined (C_ENABLE_ISO_CANFD)
  /* #74 Set CAN ISO FD */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL ) 
  if (Can_IsHasCANFDBaudrateOfControllerConfig(channel)) /* COV_CAN_FD_BAUDRATE_AVAILABILITY */
#  endif
  {
    tmpCANctrl2 |= kFlexCAN_STFCNTEN;
  }
# endif
#endif
  /* #79 Write configured value to register */
  CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->ctrl2, tmpCANctrl2, kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */

#if defined ( C_ENABLE_FLEXCAN_PARITY_CHECK_AVAILABLE )
  /* #80 Enable write on MECR register */
  pParityCheckLocal->mecr &= (vuint32)(~kFlexCAN_ECRWRDIS); /* SBSW_CAN_LL09 */
  /* #81 Configuration of MECR register */
# if !defined ( C_ENABLE_FLEXCAN_PARITY_CHECK )
  /* #82 Disable parity check functionality (not supported but enabled by default) */
  CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pParityCheckLocal->mecr, (vuint32)0x00000300u, kCanMaskAll32, initPara); /* SBSW_CAN_LL09 */
# endif
  /* #83 Disable write on MECR register */
  pParityCheckLocal->mecr |= kFlexCAN_ECRWRDIS; /* SBSW_CAN_LL09 */
#endif

#if defined( C_ENABLE_GLITCH_FILTER )
  /* # 90 Configure glitch filter register value */
  CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pGlitchFilterLocal->gfwR, GLITCH_FILTER_WIDTH(canHwChannel), kCanMaskAll32, initPara); /* SBSW_CAN_LL09 */
#endif

  return(kCanOk);
}
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_InitMailboxTx
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitMailboxTx( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara )
{
  vuint8 retVal = kCanFailed; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
#if !defined ( C_ENABLE_CAN_FD_FULL )
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
#endif

#if defined (C_ENABLE_GET_CONTEXT) || defined (C_ENABLE_SET_CONTEXT)
# if !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
  /* calculate tx interrupt mask, only used in QNX systems */
#  if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  if(initPara->hwObjHandle > 95)
  {
    canTxIntMask[canHwChannel][3] |= (CAN_INT_MASK4(canHwChannel) & CanBitMask(initPara->hwObjHandle));
  }
  else
#  endif
  {
#  if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    if(initPara->hwObjHandle > 63)
    {
      canTxIntMask[canHwChannel][2] |= (CAN_INT_MASK3(canHwChannel) & CanBitMask(initPara->hwObjHandle));
    }
    else
#  endif
    {
#  if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
      if(initPara->hwObjHandle > 31)
      {
        canTxIntMask[canHwChannel][1] |= (CAN_INT_MASK2(canHwChannel) & CanBitMask(initPara->hwObjHandle));
      }
      else
#  endif
      {
        canTxIntMask[canHwChannel][0] |= (CAN_INT_MASK1(canHwChannel) & CanBitMask(initPara->hwObjHandle));
      }
    }
  }
# endif
#endif

  /* #10 Configure TX mailbox to INACTIVE */
  {
    {
      retVal = kCanOk;
      CanLL_WriteReg16( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, initPara->hwObjHandle, control), kTxCodeInactive, kCanRamCheckMailboxControl, initPara ); /* SBSW_CAN_LL02 */
    }
  }

  return(retVal);
}
/* CODE CATEGORY 4 END */



#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_InitMailboxRxFullCan
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitMailboxRxFullCan( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara )
{
  vuint8 retVal = kCanFailed; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
# if !defined ( C_ENABLE_CAN_FD_FULL )
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif
  
  {
    tCanRxMaskPtr pIndivRxMask = pCanRxMask(canHwChannel); /* pointer to FlexCAN individual receive mask registers */
    retVal = kCanOk;

    /* #20 Set message ID */
    CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, initPara->hwObjHandle, msgID), CAN_MSGID(initPara->idRaw0), kCanMaskAll32, initPara ); /* SBSW_CAN_LL02 */
    /* #30 set ID type and set mailbox ready for receive */
    CanLL_WriteReg16( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, initPara->hwObjHandle, control), kRxCodeEmpty | (vuint16)(initPara->idType), kCanRamCheckMailboxControl, initPara ); /* SBSW_CAN_LL02 */

    /* #40 Initialize individual mask values for each FullCAN mailbox */
# if defined( C_ENABLE_EXTENDED_ID )
#  if defined( C_ENABLE_MIXED_ID )
    if((LOCAL_MAILBOX_ACCESS(canHwChannel, initPara->hwObjHandle, control) & (vuint16)kCanIdTypeExt) == 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */ /* SBSW_CAN_LL02 */
    {
      CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pIndivRxMask->indivRxMask[initPara->hwObjHandle], kCanRxMaskStd, kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */
    }
    else
#  endif
    {
      CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pIndivRxMask->indivRxMask[initPara->hwObjHandle], kCanRxMaskExt, kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */
    }
# else
    CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pIndivRxMask->indivRxMask[initPara->hwObjHandle], kCanRxMaskStd, kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */
# endif
  }

  return(retVal);
}
/* CODE CATEGORY 4 END */


#endif /* C_ENABLE_RX_FULLCAN_OBJECTS */

#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_InitMailboxRxBasicCan
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitMailboxRxBasicCan( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara )
{
  vuint8 retVal = kCanFailed; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
# if !defined ( C_ENABLE_CAN_FD_FULL )
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif
  vuint8 filterHandle;
# if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  CanObjectHandle hwObjCount;
  CanObjectHandle hwObjHandle;
# else
  vuint32 locMsgIDMask;
  tRXFIFOPtr pRxFIFO = pRXFIFO(canHwChannel);
# endif
  tCanRxMaskPtr pIndivRxMask = pCanRxMask(canHwChannel); /* pointer to FlexCAN individual receive mask registers */
# if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )


  {
    retVal = kCanOk;
    /* #20 No Rx-Fifo: configure RX BASICCAN hardware objects */
    for (hwObjCount = 0u; hwObjCount < Can_GetMailboxSize(initPara->mailboxHandle); hwObjCount++)
    {
      hwObjHandle = initPara->hwObjHandle + hwObjCount;
      filterHandle = (vuint8)((vuint16)Can_GetInitBasicCanIndex(initPara->initObject) + ((vuint16)hwObjHandle - (vuint16)(Can_GetMailboxHwHandle(CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel))))); /* calculate BasicCan offset */
      /* #30 Configure message ID */
      CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, msgID), Can_GetInitCodeOfInitBasicCan(filterHandle) & (vuint32)0x1FFFFFFFu, kCanMaskAll32, initPara ); /* SBSW_CAN_LL02 */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
      /* #40 Configure mask */
      CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pIndivRxMask->indivRxMask[hwObjHandle], (Can_GetInitMaskOfInitBasicCan(filterHandle) & (vuint32)0x1FFFFFFFu) | 0xC0000000u, kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
      /* #50 Configure mailbox as empty and consider CAN ID type */
#  if defined(C_ENABLE_EXTENDED_ID)
#   if defined(C_ENABLE_MIXED_ID)   
      if((Can_GetInitCodeOfInitBasicCan(filterHandle) & kExtIDBit) == 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
      {
        CanLL_WriteReg16( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control), kRxCodeEmpty, kCanRamCheckMailboxControl, initPara ); /* SBSW_CAN_LL02 */
      }
      else
#   endif
      {
        CanLL_WriteReg16( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control), kRxCodeEmpty | (vuint16)kCanIdTypeExt, kCanRamCheckMailboxControl, initPara ); /* SBSW_CAN_LL02 */
      }
  #  else
      CanLL_WriteReg16( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control), kRxCodeEmpty, kCanRamCheckMailboxControl, initPara ); /* SBSW_CAN_LL02 */
#  endif
    }
  }

# else /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
  {
    {
      retVal = kCanOk;
      /* #70 Configure Rx-Fifo */
      for(filterHandle = 0u; filterHandle < NUMBER_OF_FILTERS(canHwChannel); filterHandle++)
      {
        if (filterHandle < NUMBER_OF_FULL_CONFIGURABLE_FILTERS(canHwChannel)) /* PRQA S 2991, 2995 */ /* MD_Can_ConstValue */ /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
        {
          /* #80 Initialize individual masks for Rx Fifo */
          locMsgIDMask = Can_GetInitMaskOfInitBasicCan(Can_GetInitBasicCanIndex(initPara->initObject) + filterHandle) & (vuint32)0x1FFFFFFFu; /* PRQA S 2985 */ /* MD_Can_ConstValue */
          CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pIndivRxMask->indivRxMask[filterHandle], (vuint32)((locMsgIDMask << 1) | kRxFIFO_MASK), kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */
        }

        /* #90 Initialize Id table for Rx Fifo */
        locMsgIDMask = Can_GetInitCodeOfInitBasicCan(Can_GetInitBasicCanIndex(initPara->initObject) + filterHandle) & (vuint32)0x1FFFFFFFu; /* PRQA S 2985 */ /* MD_Can_ConstValue */

        /* #100 Consider CAN ID type */
        if((Can_GetInitCodeOfInitBasicCan(Can_GetInitBasicCanIndex(initPara->initObject) + filterHandle) & kExtIDBit) != 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */ /* PRQA S 2991,2992,2995,2996 */ /* MD_Can_ConstValue */ /* COV_CAN_GENDATA_FAILURE */
        {
          CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pRxFIFO->rxIDTAB[filterHandle], (vuint32)((locMsgIDMask << 1) | kRxFIFO_EXT), kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */ /* PRQA S 2880 */ /*  MD_MSR_Unreachable */
        }
        else
        {
          CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pRxFIFO->rxIDTAB[filterHandle], (vuint32)(locMsgIDMask << 1), kCanMaskAll32, initPara ); /* SBSW_CAN_LL09 */
          pRxFIFO->rxIDTAB[filterHandle] = (vuint32)(locMsgIDMask << 1); /* SBSW_CAN_LL09 */
        }
      }
    }
  }
# endif /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */

# if !defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  /* #110 Set global mask register for Rx Fifo to 'must match' (bits REM and EXT must always match) */
  CanLL_WriteReg32( CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST &pFlexCANLocal->rxfgmask, kRxFIFO_MASK | (vuint32)0x3FFFFFFEu, kCanMaskAll32, initPara ); /* SBSW_CAN_LL01 */
# endif

  return(retVal);
}
/* CODE CATEGORY 4 END */

#endif /* C_ENABLE_RX_BASICCAN_OBJECTS */


/**********************************************************************************************************************
 *  CanLL_InitEndSetRegisters
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitEndSetRegisters( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara )
{
  vuint32 tmpCANimask1;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

#if (defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) && !defined (C_ENABLE_OSEK_CAN_INTCTRL)) 
  if (Can_GetCanInterruptCounter(channel) == 0u)
  {
    /* #10 Configure CAN[IMASK1] register value, take RxFifo into account if used */
    tmpCANimask1 =  CAN_INT_MASK1(canHwChannel)
# if !defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
    & kCanRxFifoIntUnmask
# endif
    ;
# if !defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
#  if !defined( C_ENABLE_RX_BASICCAN_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
#   if defined( C_ENABLE_INDIVIDUAL_POLLING )
    if ((CAN_INT_MASK1(canHwChannel) & (vuint32)0x01u) == (vuint32)0x01u) /* PRQA S 2742 */ /* MD_Can_ConstValue */ /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   endif
    {
      tmpCANimask1 |= kRxFIFO_NEWMSG; /* PRQA S 2880 */ /*  MD_MSR_Unreachable */ /* enable Rx FIFO interrupt */
    }
#  endif
# endif
    CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->imask1, tmpCANimask1, kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */

# if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    /* #20 Configure CAN[IMASK2] register value */
    CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->imask2, CAN_INT_MASK2(canHwChannel), kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */
# endif  
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  if defined ( C_ENABLE_ASYM_MAILBOXES ) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
    if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  endif
    {
      /* #30 Configure CAN[IMASK3] register value */
      CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->imask3, CAN_INT_MASK3(canHwChannel), kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */
    }
# endif
# if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    /* #40 Configure CAN[IMASK4] register value */
    CanLL_WriteReg32(CAN_CHANNEL_CANPARA_FIRST &pFlexCANLocal->imask4, CAN_INT_MASK4(canHwChannel), kCanMaskAll32, initPara); /* SBSW_CAN_LL01 */
# endif
  }
#endif

  CAN_DUMMY_STATEMENT(initPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  return(kCanOk);
}
/* CODE CATEGORY 4 END */
/**********************************************************************************************************************
 *  CanLL_InitEnd
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitEnd( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara )
{
  /* #10 Nothing to do */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(initPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return(kCanOk);
}
/* CODE CATEGORY 4 END */

#if defined( C_ENABLE_CAN_RAM_CHECK )
/**********************************************************************************************************************
 *  CanLL_InitIsMailboxCorrupt
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_STATIC, vuint8, STATIC_CODE) CanLL_InitIsMailboxCorrupt( CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanInitParaStructPtr initPara ) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
   /* ----- Local Variables ---------------------------------------------- */
  vuint8    result, idx, wordLength, i;

  CanObjectHandle hwObjHandle;
  CanObjectHandle hwObjCount;


# if defined ( C_ENABLE_CAN_FD_FULL )
# else
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif

   /* ----- Implementation ----------------------------------------------- */
  result = kCanFalse;

  {
    hwObjHandle = initPara->hwObjHandle;

    for (hwObjCount = 0u; hwObjCount < Can_GetMailboxSize(initPara->mailboxHandle); hwObjCount++)
    {
      /* #20 DLC and IDE register check */
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) = (vuint16)0x0055u; /* SBSW_CAN_LL02 */
      if((LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) & (vuint16)0x007Fu) != (vuint16)0x0055u) /* COV_CAN_HARDWARE_FAILURE */ /* SBSW_CAN_LL02 */
      {
        result = kCanTrue;
      }
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) = (vuint16)0x002Au; /* SBSW_CAN_LL02 */
      if((LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) & (vuint16)0x007Fu) != (vuint16)0x002Au) /* COV_CAN_HARDWARE_FAILURE */ /* SBSW_CAN_LL02 */
      {
        result = kCanTrue;
      }
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) = (vuint16)0x0000u; /* SBSW_CAN_LL02 */
      if((LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) & (vuint16)0x007Fu) != (vuint16)0x0000u) /* COV_CAN_HARDWARE_FAILURE */ /* SBSW_CAN_LL02 */
      {
        result = kCanTrue; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
      }

     /* #30 Iterate test patterns */
      for(i = 0u; i < 3u; i++)
      {
        /* #40 ID register check */
        LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, msgID) = ramCheckPattern32[i]; /* SBSW_CAN_LL02 */
        if((LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, msgID) != ramCheckPattern32[i])) /* COV_CAN_HARDWARE_FAILURE */ /* SBSW_CAN_LL02 */
        {
          result = kCanTrue; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
        }
        /* #50 Data register check */
# if defined ( C_ENABLE_CAN_FD_FULL )
        wordLength = CANFD_MAILBOX_MAX_WORD_LENGTH(canHwChannel);
# else
        wordLength = 2u; /* check 8 data bytes */
# endif
        for (idx = 0; idx < wordLength; idx++)
        {
          LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, data[idx]) = ramCheckPattern32[i]; /* SBSW_CAN_LL02 */
          if(LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, data[idx]) != ramCheckPattern32[i]) /* COV_CAN_HARDWARE_FAILURE */ /* SBSW_CAN_LL02 */
          {
            result = kCanTrue; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
          }
        }
      }
      hwObjHandle++;
    }

  }

  return result;
} /* PRQA S 6010,6030,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STMIF */
/* CODE CATEGORY 4 END */
#endif /* C_ENABLE_CAN_RAM_CHECK */


/**********************************************************************************************************************
 *  CanLL_InitPowerOn
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitPowerOn(void)
{
  vuint8 retVal = kCanOk; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */

  return (retVal);
}
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_InitPowerOnChannelSpecific
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_InitPowerOnChannelSpecific(CAN_CHANNEL_CANTYPE_ONLY)
{
#if defined( VGEN_ENABLE_MDWRAP ) || defined( VGEN_ENABLE_QWRAP ) || defined( C_ENABLE_UPDATE_BASE_ADDRESS )
  /* #20 Get virtual base address of FlexCAN controller */
# if defined (C_MULTIPLE_RECEIVE_CHANNEL)
  canLL_VirtualPtrField[channel]     = (vuint32 *)ApplCanPowerOnGetBaseAddress((vuint32)(CanHwChannelData[channel].CanBaseAddress), (vuint16)((vuint16)0xC00u + (vuint16)sizeof(tCanFDRegister))); /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
  canLL_VirtualPtrFieldTemp[channel] = canLL_VirtualPtrField[channel];
# else
  canLL_VirtualPtrField[0]     = (vuint32*)ApplCanPowerOnGetBaseAddress((vuint32)(kCanBaseAddress), (vuint16)((vuint16)0xC00u + (vuint16)sizeof(tCanFDRegister))); /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
  canLL_VirtualPtrFieldTemp[0] = canLL_VirtualPtrField[0];
# endif
#endif /* QWRAP / MDWRAP */

#if defined(C_ENABLE_SLEEP_WAKEUP)
  canLL_canSleepState[channel] = kCanLLStateStart;
  canLL_canWakeUpState[channel] = kCanLLStateStart;
#endif

  canLL_canStartReinitState[channel] = kCanLLStateStart;

# if defined (C_ENABLE_HW_LOOP_TIMER)
  /* #30 initialize init control flag */
  canLL_FlexCANInitResultNeeded[channel] = kCanTrue; /* SBSW_CAN_LL14 */
# endif

  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  return (kCanOk);
}
/* CODE CATEGORY 4 END */



#if ((defined( C_ENABLE_CAN_TRANSMIT ) && defined( C_ENABLE_CAN_CANCEL_TRANSMIT )) ||  defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT ) ) && defined( C_ENABLE_CANCEL_IN_HW )
/**********************************************************************************************************************
 *  CanLL_TxCancelInHw
 *********************************************************************************************************************/
/* CODE CATEGORY 3 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxCancelInHw(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxCancellationParaStructPtr txCancellationPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
# if defined( C_ENABLE_TX_FULLCAN_DELAYED_START )
  /* Only fullcans are considered */
  if ( (txCancellationPara->hwObjHandle != Can_GetMailboxHwHandle(CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel)))
#  if defined ( C_ENABLE_MSG_TRANSMIT )
  && (txCancellationPara->hwObjHandle != Can_GetMailboxHwHandle(CAN_HL_MB_MSG_TRANSMIT_INDEX(canHwChannel)))
#  endif
  )
  {
    CanDeclareGlobalInterruptOldStatus
  
    /* Disable global interrupts */
    CanNestedGlobalInterruptDisable();
    /* Check whether the cancelled tx object has the internal delayed flag set,
       if yes -> the Tx is not requested yet (in HW) and the appropriate canHandleCurTxObj[logTxObjHandle] is set to kCanBufferCancel here
                 and it has to be not sent by CanTxFullCanDelayedStart()
       if no  -> the Tx is already requested - Tx or Abort acknowledge will come as usual */
    if (CanGetMailboxDelayed(canHwChannel, txCancellationPara->hwObjHandle))
    { /* the Tx is not requested yet */
      /* Clear the pending Tx fullcan object from internal delayed Tx flags */
      CanClearMailboxDelayed(canHwChannel, txCancellationPara->hwObjHandle);
      /* free the canHandleCurTxObj[logTxObjHandle]. This must be done here, because the Tx is not requested in HW and NO TxACK nor AbACK will come, where
         the semaphore would be freed */
      txCancellationPara->logTxObjHandle = kCanBufferFree;
    }
    else
    {
      GLOBAL_MAILBOX_ACCESS(canHwChannel, txCancellationPara->hwObjHandle, control) = kTxCodeAbort; /* SBSW_CAN_LL02 */
    }

    /* Enable global interrupts */
    CanNestedGlobalInterruptRestore();
  }
  else
  {
    GLOBAL_MAILBOX_ACCESS(canHwChannel, txCancellationPara->hwObjHandle, control) = kTxCodeAbort; /* SBSW_CAN_LL02 */
  }
# else /* C_ENABLE_TX_FULLCAN_DELAYED_START */
  /* #10 calculate mailbox to abort and issue ABORT to it */
  GLOBAL_MAILBOX_ACCESS(canHwChannel, txCancellationPara->hwObjHandle, control) = kTxCodeAbort; /* SBSW_CAN_LL02 */
# endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */
}
/* CODE CATEGORY 3 END */
#endif



#if defined( C_ENABLE_CAN_TRANSMIT ) 
/**********************************************************************************************************************
 *  CanLL_TxBegin
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara)
{
  /* #10 CanLL_TxBegin: nothing to do here */
  CAN_DUMMY_STATEMENT(txPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_TxSetMailbox
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxSetMailbox(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
# if !defined ( C_ENABLE_CAN_FD_FULL )
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif
  vuint16 tmpControlVal;
  CanObjectHandle hwObjHandle;

  /* #10 Determine mailbox to be transmitted */
  hwObjHandle = txPara->hwObjHandle;
  /* #20 Configure message ID */
# if defined( C_ENABLE_EXTENDED_ID )
  LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, msgID) = (vuint32)(txPara->idRaw0); /* SBSW_CAN_LL02 */
# else
  LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, msgID) = (vuint32)(txPara->idRaw0) << 16; /* SBSW_CAN_LL02 */
# endif
  /* #30 Configure control word */
  tmpControlVal = (vuint16)(((vuint16)txPara->dlcRaw & kTxDlcMask) | kTxCodeFree); /* write IDE,SRR and DLC */

# if defined( C_ENABLE_MIXED_ID ) 
#  if defined( C_ENABLE_DYN_TX_DLC ) || defined( C_ENABLE_DYN_TX_ID )
  tmpControlVal &= kNotIDEMask; /* clear IDE and SRR bit */
  tmpControlVal |= (vuint16)(txPara->idType);
#  endif
# endif

# if defined(C_ENABLE_CAN_FD_USED)
  tmpControlVal |= (vuint16)(txPara->fdType | txPara->fdBrsType);
# endif
  
  LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) = tmpControlVal; /* SBSW_CAN_LL02 */
}
/* CODE CATEGORY 1 END */

# if defined( C_ENABLE_PRETRANSMIT_FCT ) 
/**********************************************************************************************************************
 *  CanLL_TxSetTxStruct
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxSetTxStruct(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara)
{  
  /* get pointer to DLC and ID field in message object */
# if defined( C_CPUTYPE_LITTLEENDIAN )  /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
  /* get pointer to first element in tCanMsgObj */
  txPara->txStruct.pChipMsgObj = (CanChipMsgPtr)&(GLOBAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, timestamp)); /* SBSW_CAN_LL04 */
# endif
  /* get pointer to datafield in message object */
# if defined( C_CPUTYPE_LITTLEENDIAN ) /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
  txPara->txStruct.pChipData = (CanChipDataPtr)&(canTxMsgBuffer[txPara->logTxObjHandle].data[0]);
# endif
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_TxPretransmitCopyToCan
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxPretransmitCopyToCan(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
# if defined( C_CPUTYPE_LITTLEENDIAN ) /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
#  if defined ( C_ENABLE_CAN_FD_FULL )
  vuint32     canTxData;
  vuint8      idx, bidx, wordlength;
#  else
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  vuint32     canTxBuf[2];
#  endif
# endif

# if defined( C_CPUTYPE_LITTLEENDIAN ) /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
#  if defined ( C_ENABLE_CAN_FD_FULL )
  wordlength = CANFD_MAILBOX_MAX_WORD_LENGTH(canHwChannel);
  bidx = 0u;
  for (idx = 0u; idx < wordlength; idx++)
  {
    canTxData = ((vuint32)(txPara->txStruct.pChipData[bidx]) << 24) | ((vuint32)(txPara->txStruct.pChipData[bidx+1u]) << 16) | ((vuint32)(txPara->txStruct.pChipData[bidx+2u]) << 8) | ((vuint32)(txPara->txStruct.pChipData[bidx+3u]));
    LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, data[idx]) = canTxData; /* SBSW_CAN_LL02 */
    bidx += 4u;
  }
#  else
  ((vuint8*)canTxBuf)[0] = txPara->txStruct.pChipData[3];
  ((vuint8*)canTxBuf)[1] = txPara->txStruct.pChipData[2];
  ((vuint8*)canTxBuf)[2] = txPara->txStruct.pChipData[1];
  ((vuint8*)canTxBuf)[3] = txPara->txStruct.pChipData[0];
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, data[0]) = canTxBuf[0]; /* SBSW_CAN_LL02 */
  ((vuint8*)canTxBuf)[4] = txPara->txStruct.pChipData[7];
  ((vuint8*)canTxBuf)[5] = txPara->txStruct.pChipData[6];
  ((vuint8*)canTxBuf)[6] = txPara->txStruct.pChipData[5];
  ((vuint8*)canTxBuf)[7] = txPara->txStruct.pChipData[4];
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, data[1]) = canTxBuf[1]; /* SBSW_CAN_LL02 */
#  endif
# endif

  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */
# endif /* C_ENABLE_PRETRANSMIT_FCT */

# if defined( C_ENABLE_COPY_TX_DATA ) 
/**********************************************************************************************************************
 *  CanLL_TxCopyToCan
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxCopyToCan(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
#  if defined ( C_ENABLE_CAN_FD_FULL )
  vuint32     canFDTxBuf[16];
  vuint8 idx, idxByte, idxWord, dataWordLen, sduWordLen, sduResLen;
  vuint32 paddingWordValue;
#  else
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  vuint32     canTxBuf[2];
#  endif
  CanObjectHandle hwObjHandle;

  /* #10 Determine mailbox to be used */
  hwObjHandle = txPara->hwObjHandle;
#  if defined(C_ENABLE_CAN_FD_FULL)
  /* #20 Preparation for FD data copying */
  {
    sduWordLen = txPara->messageLen >> 2;
    sduResLen = txPara->messageLen & 0x03u;
    CanCopyFDPadding2Uint32(paddingWordValue, txPara->paddingVal);

    /* #30 Copy FD data into tx message object */
    idxByte = 0u;
    /* #40 Copy four bytes into one word */
    for (idxWord = 0u; idxWord < sduWordLen; idxWord++)
    {
      CanCopyFDDataBytes2Uint32(canFDTxBuf[idxWord], &(txPara->CanMemCopySrcPtr[idxByte])); /* SBSW_CAN_LL11 */
      idxByte += 4u;
    }
    /* #50 Data bytes do not fit into a whole word, mix data bytes and padding value */
    if (sduResLen != 0u)
    {
      canFDTxBuf[idxWord] = 0u; /* SBSW_CAN_LL11 */
      for (idx = idxByte; idx < txPara->messageLen; idx++)
      {
        CanAdd1FDByte2Uint32(canFDTxBuf[idxWord], txPara->CanMemCopySrcPtr[idx]); /* SBSW_CAN_LL11 */ /* SBSW_CAN_LL11 */
      }
      for (idx = txPara->messageLen; idx < (txPara->messageLen + (4u - sduResLen)); idx++)
      {
        CanAdd1FDByte2Uint32(canFDTxBuf[idxWord], txPara->paddingVal); /* SBSW_CAN_LL11 */ /* SBSW_CAN_LL11 */
      }
      idxWord++;
    }
    /* #60 Add padding */
    dataWordLen = (txPara->frameLen + 3u) >> 2;
    for (idx = idxWord; idx < dataWordLen; idx++) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
    {
      canFDTxBuf[idx] = paddingWordValue; /* SBSW_CAN_LL11 */
    }
    /* #70 Copy prepared FD message to mailbox */
    for (idx = 0u; idx < dataWordLen; idx++)
    {
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, data[idx]) = canFDTxBuf[idx]; /* SBSW_CAN_LL02 */
    }
  }
#  else
  /* #80 Prepare and copy non-FD data into tx message object */
  CanCopyDataBytes2Uint32(canTxBuf, txPara->CanMemCopySrcPtr); /* SBSW_CAN_LL13 */  /* SBSW_CAN_LL13 */
  LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, data[0]) = canTxBuf[0]; /* SBSW_CAN_LL02 */
  LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, data[1]) = canTxBuf[1]; /* SBSW_CAN_LL02 */
#  endif
}
/* CODE CATEGORY 1 END */
# endif

/**********************************************************************************************************************
 *  CanLL_TxStart
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxStart(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
# if !defined ( C_ENABLE_CAN_FD_FULL )
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif
  CanObjectHandle hwObjHandle;

  /* #10 Determine mailbox to be used */
  hwObjHandle = txPara->hwObjHandle;

#  if defined (C_ENABLE_TX_FULLCAN_DELAYED_START)
  /* if transmission request is a Tx FullCAN - store transmit request */
  if (hwObjHandle != Can_GetMailboxHwHandle(CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel)))
  { 
    CanSetMailboxDelayed(canHwChannel, hwObjHandle);
  }
  else
  /* if transmission request is a Tx Normal - send as usual */
#  endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */
  {
    /* #20 Request transmission */
    LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) |= kTxCodeTransmit; /* start tx of the message */ /* SBSW_CAN_LL02 */
# if defined (C_ENABLE_WORKAROUND_ERR005829)
    /* #30 Workaround ERR005829: set the reserved mailbox twice to CodeInactive. This workaround is recommended by Freescale */
    LOCAL_MAILBOX_ACCESS(canHwChannel, INDEX_RSVD_MB(canHwChannel), control) |= kTxCodeInactive; /* SBSW_CAN_LL02 */
    LOCAL_MAILBOX_ACCESS(canHwChannel, INDEX_RSVD_MB(canHwChannel), control) |= kTxCodeInactive; /* SBSW_CAN_LL02 */
# endif
  }
  
  return(kCanOk);
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_TxEnd
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxTransmissionParaStructPtr txPara)
{
  /* #10 CanLL_TxEnd: nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(txPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */
#endif /* C_ENABLE_CAN_TRANSMIT */

#if defined( C_ENABLE_TX_POLLING )
/**********************************************************************************************************************
 *  CanLL_TxIsGlobalConfPending
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxIsGlobalConfPending(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  vuint32 iFlags;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  vuint8 retVal = kCanFalse; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */

  /* #10 Retrieve IFLAG1 */
  iFlags = pFlexCANLocal->iflag1 & CAN_TX_POLL_MASK1(canHwChannel);
# if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #20 Retrieve IFLAG2 */
  iFlags |= pFlexCANLocal->iflag2 & CAN_TX_POLL_MASK2(canHwChannel);
# endif
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  if defined ( C_ENABLE_ASYM_MAILBOXES ) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
  if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  endif
  {
    /* #30 Retrieve IFLAG3 */
    iFlags |= pFlexCANLocal->iflag3 & CAN_TX_POLL_MASK3(canHwChannel);
  }
# endif
# if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #40 Retrieve IFLAG4 */
  iFlags |= pFlexCANLocal->iflag4 & CAN_TX_POLL_MASK4(canHwChannel);
# endif

  /* #50 Check for pending confirmation */
  if (iFlags != 0u)
  {
    retVal = kCanTrue;
  }
  
  return retVal;
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_TxProcessPendings
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTaskParaStructPtr taskPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  vuint32 iFlags;
  CanObjectHandle mailboxElement = 0u;
  CanObjectHandle hwObjHandle = taskPara->hwObjHandle;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  
  {
  /* #10 Get iflag corresponding to hwObjHandle */
    iFlags = CanLL_GetInterruptFlag(pFlexCANLocal, hwObjHandle); /* SBSW_CAN_LL01 */

    /* #20 Check for confirmation condition */
    if( ((iFlags & CanBitMask(hwObjHandle)) != 0u)) /* PRQA S 2985 */ /* MD_Can_ConstValue */ /* COV_CAN_FULLCAN_LL_TX_POLLING */
    {
      /* #30 Perform TX confirmation */
      CanHL_TxConfirmationPolling(CAN_HW_CHANNEL_CANPARA_FIRST  taskPara->mailboxHandle, mailboxElement, hwObjHandle);
    }
    hwObjHandle++; /* PRQA S 2983 */ /* MD_MSR_DummyStmt */
  }
}
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_TX_POLLING */

/**********************************************************************************************************************
 *  CanLL_TxConfBegin
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxConfBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* #10 Clear pending interrupt flag */
  CanLL_ClearPendingInterrupt(pFlexCANLocal, txConfPara->hwObjHandle); /* SBSW_CAN_LL01 */
}
/* CODE CATEGORY 1 END */

#if (defined( C_ENABLE_TX_OBSERVE ) || defined( C_ENABLE_CAN_TX_CONF_FCT ) ) && defined( C_ENABLE_CANCEL_IN_HW )
/**********************************************************************************************************************
 *  CanLL_TxConfIsMsgTransmitted
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxConfIsMsgTransmitted(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  vuint8 isMsgTransmitted = kCanTrue;

  /* #10 Check for transmitted message */
  if (!CanLL_TxIsObjFree(canHwChannel, txConfPara->hwObjHandle))
  {
    /* #20 Set mailbox to free if message was cancelled */
    GLOBAL_MAILBOX_ACCESS(canHwChannel, txConfPara->hwObjHandle, control) = kTxCodeFree; /* SBSW_CAN_LL02 */
    isMsgTransmitted = kCanFalse;
  }

  return isMsgTransmitted;
}
/* CODE CATEGORY 1 END */
#endif

#if (defined( C_ENABLE_CAN_TX_CONF_FCT ) && defined( C_ENABLE_CAN_TX_CONF_MSG_ACCESS )) || defined(CAN_ENABLE_GENERIC_CONFIRMATION_API2)
/**********************************************************************************************************************
 *  CanLL_TxConfSetTxConfStruct
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxConfSetTxConfStruct(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara)
{
#if defined (C_ENABLE_ECU_SWITCH_PASS)
#else
  /* # 10 Get Confirmation information from mailbox */
  txConfPara->txInfoStructConf->pChipMsgObj = (CanChipMsgPtr)&(GLOBAL_MAILBOX_ACCESS(canHwChannel, txConfPara->hwObjHandle, control));
  txConfPara->txInfoStructConf->pChipData = (CanChipDataPtr)&(GLOBAL_MAILBOX_ACCESS(canHwChannel, txConfPara->hwObjHandle, data[0]));
#endif
}
/* CODE CATEGORY 1 END */
#endif

/**********************************************************************************************************************
 *  CanLL_TxConfEnd
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxConfEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxConfirmationParaStructPtr txConfPara)
{
  /* #10 CanLL_TxConfEnd: nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(txConfPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */

#if defined( C_ENABLE_MSG_TRANSMIT )
/**********************************************************************************************************************
 *  CanLL_TxBeginMsgTransmit
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxBeginMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara)
{
  /* CanLL_TxBeginMsgTransmit: nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(txPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_TxCopyMsgTransmit
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxCopyMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
# if defined(C_ENABLE_CAN_FD_FULL)
  vuint32     canFDTxBuf[16];
  vuint8      messageLen;
  vuint8      frameLen;
  vuint8      idx, idxByte, idxWord, dataWordLen, sduWordLen, sduResLen;
  vuint32     paddingWordValue;
# else
  vuint32     canTxBuf[2];
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif

# if defined(C_ENABLE_CAN_FD_FULL)
  messageLen = (vuint8)((txPara->txMsgStruct)->TimeStamp);
  frameLen = (vuint8)CAN_DLC2LEN((txPara->txMsgStruct)->DlcRaw);

  assertUser(messageLen <= CAN_HL_MAX_LEN(channel), channel, kErrorTxDlcTooLarge); /* CM_CAN_LL11 */
# endif

#if defined(C_ENABLE_CAN_FD_USED)
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, control) = (vuint16)(((vuint16)((txPara->txMsgStruct)->DlcRaw) & (kTxDlcMask | kCanFdTypeFd | kCanFdBrsTypeTrue)) | kTxCodeFree); /* SBSW_CAN_LL02 */
# else
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, control) = (vuint16)(((vuint16)((txPara->txMsgStruct)->DlcRaw) & kTxDlcMask) | kTxCodeFree); /* SBSW_CAN_LL02 */
# endif
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, msgID) = (vuint32)((txPara->txMsgStruct)->IdRaw); /* SBSW_CAN_LL02 */

#  if defined(C_ENABLE_CAN_FD_FULL)
  /* copy FD data into tx message object */
  sduWordLen = messageLen >> 2;
  sduResLen = messageLen & 0x03u;
  CanCopyFDPadding2Uint32(paddingWordValue, (vuint8)C_CAN_FD_PADDING_VALUE);

  idxByte = 0u;
  for (idxWord = 0u; idxWord < sduWordLen; idxWord++)
  {
    CanCopyFDDataBytes2Uint32(canFDTxBuf[idxWord], &((txPara->txMsgStruct)->DataFld[idxByte])); /* SBSW_CAN_LL11 */
    idxByte += 4u;
  }
  if (sduResLen != 0u)
  {
    /* data bytes do not fit into a whole word, mix data bytes and padding value */
    canFDTxBuf[idxWord] = 0u; /* SBSW_CAN_LL11 */
    for (idx = idxByte; idx < messageLen; idx++)
    {
      CanAdd1FDByte2Uint32(canFDTxBuf[idxWord], (txPara->txMsgStruct)->DataFld[idx]); /* SBSW_CAN_LL11 */ /* SBSW_CAN_LL11 */
    }
    for (idx = messageLen; idx < (messageLen + (4u - sduResLen)); idx++)
    {
      CanAdd1FDByte2Uint32(canFDTxBuf[idxWord], paddingWordValue); /* PRQA S 2985 */ /* MD_Can_ConstValue */ /* SBSW_CAN_LL11 */ /* SBSW_CAN_LL11 */
    }
    idxWord++;
  }
  dataWordLen = (frameLen + 3u) >> 2;
  for (idx = idxWord; idx < dataWordLen; idx++) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
  {
    canFDTxBuf[idx] = paddingWordValue; /* SBSW_CAN_LL11 */
  }

  for (idx = 0u; idx < dataWordLen; idx++)
  {
    LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, data[idx]) = canFDTxBuf[idx]; /* SBSW_CAN_LL02 */
  }
#  else
  /* copy data into tx message object */
  CanCopyDataBytes2Uint32(canTxBuf, (txPara->txMsgStruct)->DataFld); /* SBSW_CAN_LL13 */ /* SBSW_CAN_LL13 */
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, data[0]) = canTxBuf[0];  /* SBSW_CAN_LL02 */
  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, data[1]) = canTxBuf[1];  /* SBSW_CAN_LL02 */
#  endif
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_TxStartMsgTransmit
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_TxStartMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
# if !defined (C_ENABLE_CAN_FD_FULL)
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
# endif

  LOCAL_MAILBOX_ACCESS(canHwChannel, txPara->hwObjHandle, control) |= kTxCodeTransmit; /* start tx of the message */ /* SBSW_CAN_LL02 */
# if defined (C_ENABLE_WORKAROUND_ERR005829)
  /* set the reserved mailbox twice to CodeInactive. This workaround is recommended by freescale. */
  LOCAL_MAILBOX_ACCESS(canHwChannel, INDEX_RSVD_MB(canHwChannel), control) |= kTxCodeInactive; /* SBSW_CAN_LL02 */
  LOCAL_MAILBOX_ACCESS(canHwChannel, INDEX_RSVD_MB(canHwChannel), control) |= kTxCodeInactive; /* SBSW_CAN_LL02 */
# endif

  return(kCanOk);
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_TxEndMsgTransmit
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_TxEndMsgTransmit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTxMsgTransmissionParaStructPtr txPara)
{
  /* CanLL_TxEndMsgTransmit: nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(txPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_MSG_TRANSMIT */


#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/**********************************************************************************************************************
 *  CanLL_RxBasicMsgReceivedBegin
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxBasicMsgReceivedBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxBasicParaStructPtr rxBasicPara)
{
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  vuint8 retVal = kCanTrue; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
# if defined ( C_ENABLE_CAN_FD_FULL )
  vuint8 idx;
# endif
# if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
#  if defined( C_ENABLE_PROTECTED_RX_PROCESS )
  CanDeclareGlobalInterruptOldStatus
#  endif
  Can_ReturnType loopResult = CAN_OK; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
# endif

# if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
#  if defined( C_ENABLE_PROTECTED_RX_PROCESS )
  CanNestedGlobalInterruptDisable();
#  endif
  /* #10 Request access to BasicCAN mailbox and copy control word */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).control = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, control); /* SBSW_CAN_LL06 */
  if ((Can_GetRxMsgBufferOfControllerData(canHwChannel).control & kRxCodeBusy) == kRxCodeBusy) /* COV_CAN_HARDWARE_PROCESSING_TIME */
  {
    CanLL_ApplCanTimerStart(kCanLoopMsgReception); /* start hw loop timer */ /* SBSW_CAN_LL08 */
    do
    { /* Check busy state of receive object */
      Can_GetRxMsgBufferOfControllerData(canHwChannel).control = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, control); /* SBSW_CAN_LL06 */
      loopResult = CanLL_ApplCanTimerLoop(kCanLoopMsgReception);
    } while(((Can_GetRxMsgBufferOfControllerData(canHwChannel).control & kRxCodeBusy) == kRxCodeBusy) && (loopResult == CAN_OK)); /* COV_CAN_TIMEOUT_DURATION */ /* PRQA S 2995 */ /* MD_Can_ConstValue */
    CanLL_ApplCanTimerEnd(kCanLoopMsgReception); /* stop hw loop timer */ /* SBSW_CAN_LL08 */
  }
# else /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
  /* #20 Copy control word from RxFifo */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).control = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, control); /* SBSW_CAN_LL06 */
# endif /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
      
  /* #30 Copy id to RAM buffer */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).msgID = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, msgID); /* SBSW_CAN_LL06 */
  /* #40 Copy data to RAM buffer */
# if defined( C_CPUTYPE_LITTLEENDIAN ) /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
  {
    vuint32 tmpData; /* avoid IAR compiler warning */
#  if defined ( C_ENABLE_CAN_FD_FULL )
    for (idx = 0u; idx < CANFD_MAILBOX_MAX_WORD_LENGTH(canHwChannel); idx++)
    {
      tmpData = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, data[idx]); /* SBSW_CAN_LL02 */
      Can_GetRxMsgBufferOfControllerData(canHwChannel).data[idx] = CanBswap32(tmpData); /* SBSW_CAN_LL06 */
    }
#  else
    tmpData = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, data[0]); /* SBSW_CAN_LL02 */
    Can_GetRxMsgBufferOfControllerData(canHwChannel).data[0] = CanBswap32(tmpData); /* SBSW_CAN_LL06 */
    tmpData = LOCAL_MAILBOX_ACCESS(canHwChannel, rxBasicPara->hwObjHandle, data[1]); /* SBSW_CAN_LL02 */
    Can_GetRxMsgBufferOfControllerData(canHwChannel).data[1] = CanBswap32(tmpData); /* SBSW_CAN_LL06 */
#  endif
  }
# endif

# if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  /* #50 Clear pending interrupt flag for BaisCAN */
  CanLL_ClearPendingInterrupt(pFlexCANLocal, rxBasicPara->hwObjHandle); /* SBSW_CAN_LL01 */

  /* to unlock only the current receive buffer the timestamp of another not-Rx message object must be read
   * reading the global timer results in a global unlock of all receive buffers */
  /* #60 Unlock BasicCAN mailbox */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).timestamp = LOCAL_MAILBOX_ACCESS(canHwChannel, Can_GetMailboxHwHandle(CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel)), timestamp); /* unlock current receive buffer */ /* SBSW_CAN_LL06 */

#  if defined( C_ENABLE_PROTECTED_RX_PROCESS )
  CanNestedGlobalInterruptRestore();
#  endif

#  if defined( C_ENABLE_HW_LOOP_TIMER )
  if (loopResult != CAN_OK) /* COV_CAN_TIMEOUT_DURATION */
  {    
    retVal = kCanFailed;
  }
#  endif
# else /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
  /* #70 Clear RxFIFO pending interrupt flag after message is stored */
  pFlexCANLocal->iflag1 = (vuint32)kRxFIFO_NEWMSG; /* SBSW_CAN_LL01 */
# endif /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */

  /* #80 Save pointer of control word and data */
  rxBasicPara->rxStruct.pChipMsgObj = (CanChipMsgPtr) &(Can_GetRxMsgBufferOfControllerData(canHwChannel).control); /* SBSW_CAN_LL04 */
  rxBasicPara->rxStruct.pChipData = (CanChipDataPtr) &(Can_GetRxMsgBufferOfControllerData(canHwChannel).data[0]); /* SBSW_CAN_LL04 */

# if defined( C_ENABLE_OVERRUN )
  /* #90 Save overrun status */
#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  if((Can_GetRxMsgBufferOfControllerData(canHwChannel).control & kCodeMask) == kRxCodeOverrun)
  {
#  else
  if((pFlexCANLocal->iflag1 & kRxFIFO_OVERRUN) != 0u)
  {
    pFlexCANLocal->iflag1 = kRxFIFO_OVERRUN | kRxFIFO_WARN; /* clear warning and overflow flag */ /* SBSW_CAN_LL01 */
#  endif
    rxBasicPara->isOverrun = (vuint8)kCanTrue; /* SBSW_CAN_LL04 */
  }
  else
  {
    rxBasicPara->isOverrun = (vuint8)kCanFalse; /* SBSW_CAN_LL04 */
  }
# endif

  return (retVal);
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_RxBasicReleaseObj
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxBasicReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxBasicParaStructPtr rxBasicPara)
{
  /* #10 CanLL_RxBasicReleaseObj(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(rxBasicPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_RxBasicMsgReceivedEnd
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxBasicMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxBasicParaStructPtr rxBasicPara)
{
  /* #10 CanLL_RxBasicMsgReceivedEnd(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(rxBasicPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */

# if defined( C_ENABLE_RX_BASICCAN_POLLING )
/**********************************************************************************************************************
 *  CanLL_RxBasicIsGlobalIndPending
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxBasicIsGlobalIndPending(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  vuint8 retVal = kCanFalse; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  vuint32 iFlags;
  
  /* #10 Retrieve IFLAG1 */
  iFlags = pFlexCANLocal->iflag1 & CAN_RXBASIC_POLL_MASK1(canHwChannel);
#   if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #20 Retrieve IFLAG2 */
  iFlags |= pFlexCANLocal->iflag2 & CAN_RXBASIC_POLL_MASK2(canHwChannel);
#   endif
#   if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #30 Retrieve IFLAG3 */
#    if defined ( C_ENABLE_ASYM_MAILBOXES ) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
  if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#    endif
  {
    iFlags |= pFlexCANLocal->iflag3 & CAN_RXBASIC_POLL_MASK3(canHwChannel);
  }
#   endif
#   if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #40 Retrieve IFLAG4 */
  iFlags |= pFlexCANLocal->iflag4 & CAN_RXBASIC_POLL_MASK4(canHwChannel);
#   endif
  /* #50 Check for pending RX BesicCAN message */
  if (iFlags != 0u)
  {
#  else /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
  /* #60 Check for pending RxFifo message */
  if ((pFlexCANLocal->iflag1 & kRxFIFO_NEWMSG) != 0u)
  {
#  endif /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
    retVal = kCanTrue;
  }
  
  return retVal;
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_RxBasicProcessPendings
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxBasicProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTaskParaStructPtr taskPara) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{

  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  vuint32 iFlags = 0u; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  CanObjectHandle hwObjCount;
  CanObjectHandle hwObjHandle;
#  else
  vuint8 fifoLoopCnt=0u;
#  endif

#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  /* #10 BasicCAN receive process */
  for (hwObjCount = 0u; hwObjCount < Can_GetMailboxSize(taskPara->mailboxHandle); hwObjCount++)
  {
    hwObjHandle = taskPara->hwObjHandle + hwObjCount;
    /* 20 Get corresponding iflag */
    iFlags = CanLL_GetInterruptFlag(pFlexCANLocal, hwObjHandle); /* SBSW_CAN_LL01 */
    /* #30 Check for received BasicCAN message */
    if((iFlags & CanBitMask(hwObjHandle)) != 0u) /* PRQA S 2985 */ /* MD_Can_ConstValue */
    {
      taskPara->hwObjHandle = hwObjHandle; /* SBSW_CAN_LL04 */
      CanHL_BasicCanMsgReceivedPolling(CAN_HW_CHANNEL_CANPARA_FIRST taskPara->mailboxHandle, hwObjHandle);
    }
  }
#  else /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
  /* #40 Check for RxFifo message */
  while(((pFlexCANLocal->iflag1 & kRxFIFO_NEWMSG) != 0u) && (fifoLoopCnt < C_FLEXCAN_RXFIFO_MAXLOOP)) /* COV_CAN_RXFIFO_HANDLING */
  {
    fifoLoopCnt++;
    CanHL_BasicCanMsgReceivedPolling(CAN_HW_CHANNEL_CANPARA_FIRST taskPara->mailboxHandle, taskPara->hwObjHandle);
  }
#  endif /* C_ENABLE_CLASSIC_MAILBOX_LAYOUT */
}
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_RX_BASICCAN_POLLING */
#endif /* C_ENABLE_RX_BASICCAN_OBJECTS */

#if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
/**********************************************************************************************************************
*  CanLL_RxFullMsgReceivedBegin
*********************************************************************************************************************/
/* CODE CATEGORY 1 START */
extern void SrvMcanRxInd(vuint32 buffIdx, vuint32 cs, vuint32 msgId, vuint8* data, vuint8 dataLen);
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxFullMsgReceivedBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxFullParaStructPtr rxFullPara)
{
# if defined( C_ENABLE_PROTECTED_RX_PROCESS )
  CanDeclareGlobalInterruptOldStatus
# endif
# if defined ( C_ENABLE_CAN_FD_FULL )
  vuint8 idx;
# endif
  vuint8 retVal = kCanOk; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  Can_ReturnType loopResult = CAN_OK; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

# if defined( C_ENABLE_PROTECTED_RX_PROCESS )
  CanNestedGlobalInterruptDisable();
# endif
  /* #10 Request access to FullCAN mailbox and copy control word */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).control = LOCAL_MAILBOX_ACCESS(canHwChannel, rxFullPara->hwObjHandle, control); /* SBSW_CAN_LL06 */
  if ((Can_GetRxMsgBufferOfControllerData(canHwChannel).control & kRxCodeBusy) == kRxCodeBusy) /* COV_CAN_HARDWARE_PROCESSING_TIME */
  {
    CanLL_ApplCanTimerStart(kCanLoopMsgReception); /* start hw loop timer */ /* SBSW_CAN_LL08 */
    do
    { /* Check busy state of receive object */
      Can_GetRxMsgBufferOfControllerData(canHwChannel).control = LOCAL_MAILBOX_ACCESS(canHwChannel, rxFullPara->hwObjHandle, control); /* SBSW_CAN_LL06 */
      loopResult = CanLL_ApplCanTimerLoop(kCanLoopMsgReception);
    } while(((Can_GetRxMsgBufferOfControllerData(canHwChannel).control & kRxCodeBusy) == kRxCodeBusy) && (loopResult == CAN_OK)); /* COV_CAN_TIMEOUT_DURATION */ /* PRQA S 2995 */ /* MD_Can_ConstValue */
    CanLL_ApplCanTimerEnd(kCanLoopMsgReception); /* stop hw loop timer */ /* SBSW_CAN_LL08 */
  }

  /* #20 Copy id to RAM buffer */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).msgID = LOCAL_MAILBOX_ACCESS(canHwChannel, rxFullPara->hwObjHandle, msgID); /* SBSW_CAN_LL06 */
  /* #30 Copy data to RAM buffer */
# if defined( C_CPUTYPE_LITTLEENDIAN ) /* COV_CAN_FLEXCAN3_DERIVATIVE_LITTLE_ENDIAN */
  {
    vuint32 tmpData; /* avoid IAR compiler warning */
#  if defined ( C_ENABLE_CAN_FD_FULL )
    for (idx = 0u; idx < CANFD_MAILBOX_MAX_WORD_LENGTH(canHwChannel); idx++)
    {
      tmpData = LOCAL_MAILBOX_ACCESS(canHwChannel, rxFullPara->hwObjHandle, data[idx]); /* SBSW_CAN_LL02 */
      Can_GetRxMsgBufferOfControllerData(canHwChannel).data[idx] = CanBswap32(tmpData); /* SBSW_CAN_LL06 */
    }
#  else
    tmpData = LOCAL_MAILBOX_ACCESS(canHwChannel, rxFullPara->hwObjHandle, data[0]); /* SBSW_CAN_LL02 */
    Can_GetRxMsgBufferOfControllerData(canHwChannel).data[0] = CanBswap32(tmpData); /* SBSW_CAN_LL06 */
    tmpData = LOCAL_MAILBOX_ACCESS(canHwChannel, rxFullPara->hwObjHandle, data[1]); /* SBSW_CAN_LL02 */
    Can_GetRxMsgBufferOfControllerData(canHwChannel).data[1] = CanBswap32(tmpData); /* SBSW_CAN_LL06 */
    /* wl20220720 */
    if(((canRxMsgBuffer[0].msgID >> 18u) == 0x743) || ((canRxMsgBuffer[0].msgID >> 18u) == 0x7DF))
    {
      SrvMcanRxInd(0, 0, canRxMsgBuffer[0].msgID >> 18u, canRxMsgBuffer[0].data, 8u);
    }
#  endif
  }
# endif

  /* #40 Clear pending interrupt flag for FullCAN */
  CanLL_ClearPendingInterrupt(pFlexCANLocal, rxFullPara->hwObjHandle); /* SBSW_CAN_LL01 */

  /* to unlock only the current receive buffer the timestamp of another not-Rx message object must be read
   * reading the global timer results in a global unlock of all receive buffers */
  /* #50 Unlock FullCAN mailbox */
  Can_GetRxMsgBufferOfControllerData(canHwChannel).timestamp = LOCAL_MAILBOX_ACCESS(canHwChannel, Can_GetMailboxHwHandle(CAN_HL_MB_TX_NORMAL_INDEX(canHwChannel)), timestamp); /* unlock current receive buffer */ /* SBSW_CAN_LL06 */

# if defined( C_ENABLE_PROTECTED_RX_PROCESS )
  CanNestedGlobalInterruptRestore();
# endif

# if defined( C_ENABLE_HW_LOOP_TIMER )
  if (loopResult != CAN_OK) /* COV_CAN_TIMEOUT_DURATION */
  {    
    retVal = kCanFailed;
  }
# endif

  /* #60 Save pointer of control word and data */
  rxFullPara->rxStruct.pChipMsgObj = (CanChipMsgPtr) &(Can_GetRxMsgBufferOfControllerData(canHwChannel).control); /* SBSW_CAN_LL04 */
  rxFullPara->rxStruct.pChipData = (CanChipDataPtr) &(Can_GetRxMsgBufferOfControllerData(canHwChannel).data[0]); /* SBSW_CAN_LL04 */

# if defined( C_ENABLE_FULLCAN_OVERRUN )
  /* #70 Save overrun status */
  if ((Can_GetRxMsgBufferOfControllerData(canHwChannel).control & kCodeMask) == kRxCodeOverrun) /* COV_CAN_FULL_OVERRUN_HANDLING */
  {
    rxFullPara->isOverrun = kCanTrue; /* SBSW_CAN_LL04 */
  }
  else
  {
    rxFullPara->isOverrun = kCanFalse; /* SBSW_CAN_LL04 */
  }
# endif

  return (retVal);
}
/* CODE CATEGORY 1 END */


/**********************************************************************************************************************
 *  CanLL_RxFullReleaseObj
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxFullReleaseObj(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxFullParaStructPtr rxFullPara)
{
  /* #10 CanLL_RxFullReleaseObj(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(rxFullPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_RxFullMsgReceivedEnd
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxFullMsgReceivedEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanRxFullParaStructPtr rxFullPara)
{
  /* #10 CanLL_RxFullMsgReceivedEnd(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(rxFullPara); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */


# if defined( C_ENABLE_RX_FULLCAN_POLLING )
/**********************************************************************************************************************
 *  CanLL_RxFullIsGlobalIndPending
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_RxFullIsGlobalIndPending(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  vuint32 iFlags;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  vuint8 retVal = kCanFalse; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */

  /* #10 Retrieve IFLAG1 of Rx FullCAN */
  iFlags = pFlexCANLocal->iflag1 & CAN_RXFULL_POLL_MASK1(canHwChannel);
#if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #20 Retrieve IFLAG2 of Rx FullCAN */
  iFlags |= pFlexCANLocal->iflag2 & CAN_RXFULL_POLL_MASK2(canHwChannel);
#endif
#if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #30 Retrieve IFLAG3 of Rx FullCAN */
# if defined ( C_ENABLE_ASYM_MAILBOXES ) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
  if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
# endif
  {
    iFlags |= pFlexCANLocal->iflag3 & CAN_RXFULL_POLL_MASK3(canHwChannel);
  }
#endif
#if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #40 Retrieve IFLAG of Rx FullCAN */
  iFlags |= pFlexCANLocal->iflag4 & CAN_RXFULL_POLL_MASK4(canHwChannel);
#endif
  /* #50 Check for received FullCAN message */
  if (iFlags != 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
  {
    retVal = kCanTrue;
  }
  
  return retVal;
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_RxFullProcessPendings
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxFullProcessPendings(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST CanTaskParaStructPtr taskPara) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */ /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  vuint32 iFlags;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* #10 Get iflag of corresponding hwObject */
  iFlags = CanLL_GetInterruptFlag(pFlexCANLocal, taskPara->hwObjHandle); /* SBSW_CAN_LL01 */
  /* #20 Check for received FullCAN message */
  if((iFlags & CanBitMask(taskPara->hwObjHandle)) != 0u) /* COV_CAN_FULLCAN_LL_RX_POLLING */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
  {
    CanHL_FullCanMsgReceivedPolling(CAN_HW_CHANNEL_CANPARA_FIRST Can_GetHwToMbHandle(canHwChannel, taskPara->hwObjHandle), taskPara->hwObjHandle); /* PRQA S 2986 */ /* MD_Can_ConstValue */
  }
}
/* CODE CATEGORY 2 END */
# endif /* C_ENABLE_RX_FULLCAN_POLLING */
#endif /* C_ENABLE_RX_FULLCAN_OBJECTS */


#if (defined( C_ENABLE_RX_FULLCAN_OBJECTS ) || defined( C_ENABLE_RX_BASICCAN_OBJECTS )) && ( kCanNumberOfRxObjects > 0 ) && defined( C_ENABLE_COPY_RX_DATA )
/**********************************************************************************************************************
 *  CanLL_RxCopyFromCan
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxCopyFromCan(CAN_CHANNEL_CANTYPE_FIRST CanRxCopyParaStructPtr rxCopyPara)
{
  vuint8 canllidx;
  for (canllidx = 0u; canllidx < rxCopyPara->len; canllidx++)
  {
    rxCopyPara->dest[canllidx] = rxCopyPara->src[canllidx];
  }

  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */
#endif


/**********************************************************************************************************************
 *  CanLL_ErrorHandlingBegin
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ErrorHandlingBegin(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  /* #10 CanLL_ErrorHandlingBegin(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 2 END */

/**********************************************************************************************************************
 *  CanLL_BusOffOccured
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_BusOffOccured(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  vuint8 retVal = kCanFalse; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  
  /* #10 Check for BusOff */
  if((pFlexCANLocal->estat & kFlexCAN_BOFF_INT) != 0u) /* COV_CAN_BUSOFF_HANDLING */
  {
    /* #20 clear busoff and error interrupt flags */
    pFlexCANLocal->estat = (CANSFR_TYPE)(kFlexCAN_BOFF_INT | kFlexCAN_ERR_INT); /* clear busoff and error interrupt flags */ /* SBSW_CAN_LL01 */

#if defined ( C_ENABLE_BUSOFF_RECOVERY_COMPLIANT )
    /* #30 Delete pending transmit requests */
    {
      CanObjectHandle mailboxHandle;
      for (mailboxHandle = CAN_HL_MB_TX_STARTINDEX(canHwChannel); mailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel); mailboxHandle++)
      {
        LOCAL_MAILBOX_ACCESS(canHwChannel, Can_GetMailboxHwHandle(mailboxHandle), control) = kTxCodeInactive; /* set all mailboxes inactive */ /* SBSW_CAN_LL02 */
      }
    }
#endif
    retVal = kCanTrue;
  }
  
  return retVal;
}
/* CODE CATEGORY 2 END */


/**********************************************************************************************************************
 *  CanLL_ErrorHandlingEnd
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ErrorHandlingEnd(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  /* #10 CanLL_ErrorHandlingEnd(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 2 END */

#if defined( C_ENABLE_EXTENDED_STATUS )
/**********************************************************************************************************************
 *  CanLL_GetStatusBegin
 *********************************************************************************************************************/
/* CODE CATEGORY 3 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_GetStatusBegin(CAN_CHANNEL_CANTYPE_ONLY)
{
  /* #10 CanLL_GetStatusBegin(): nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 3 END */
#endif

#if defined( C_ENABLE_SLEEP_WAKEUP ) && defined( C_ENABLE_WAKEUP_POLLING )
/**********************************************************************************************************************
 *  CanLL_WakeUpOccured
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_WakeUpOccured(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  vuint8 retVal = kCanFalse; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  
  /* #10 Check for wakeup event */
  if((pFlexCAN(canHwChannel)->estat & kFlexCAN_WAKE_INT) != 0)
  {
    retVal = kCanTrue;
  }
  
  return retVal;
}
/* CODE CATEGORY 2 END */
#endif

#if defined( C_ENABLE_SLEEP_WAKEUP )
# if (defined( C_ENABLE_CAN_WAKEUP_INTERRUPT ) || defined( C_ENABLE_WAKEUP_POLLING )) 
/**********************************************************************************************************************
 *  CanLL_WakeUpHandling
 *********************************************************************************************************************/
 /* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_WakeUpHandling(CAN_CHANNEL_CANTYPE_ONLY) /* PRQA S 3219 */ /* MD_MSR_DummyStmt */
{
# if defined( C_ENABLE_FLEXCAN_STOP_MODE )
  /* ----- Local Variables ---------------------------------------------- */
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* ----- Implementation ----------------------------------------------- */
  /* notify the application here and call WAKEUP handling */
#  if defined( C_ENABLE_APPLCANPREWAKEUP_FCT ) 
  /* #10 application callout to handle wakeup preconditions like transceiver mode */
  ApplCanPreWakeUp(CAN_CHANNEL_CANPARA_ONLY);
#  endif

  pFlexCANLocal->estat = kFlexCAN_WAKE_INT; /* clear pending wakeup interrupt flag */ /* SBSW_CAN_LL01 */

  /* #20 if hardware does not wake up automatically call wakeup handling and go to start */
  /* In case of CommonCAN, it might be necessary to call CanWakeUp() / CAN_WAKEUP() even if the hardware
     wakes up automatically to make sure all associated HW channels are awaken. */
  CAN_WAKEUP( channel );
  /* #30 call wakeup indication */
  APPL_CAN_WAKEUP( channel );
# else
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
# endif
}
/* CODE CATEGORY 4 END */
# endif
#endif

/**********************************************************************************************************************
 *  CanLL_ModeTransition
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_STATIC, vuint8, STATIC_CODE) CanLL_ModeTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 mode, vuint8 busOffRecovery, vuint8 ramCheck)
{
  vuint8 retVal = kCanFailed; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  if (mode != kCanModeStartReinit)
  {
    canLL_canStartReinitState[channel] = kCanLLStateStart;
  }
  switch(mode)
  {
#if defined(C_ENABLE_SLEEP_WAKEUP)
    /* #100 SLEEP request */
    case kCanModeSleep:
      retVal = CanLL_Sleep(CAN_HW_CHANNEL_CANPARA_ONLY);
      break;

    /* #200 WAKEUP-START request */
    case kCanModeWakeupStart:
      retVal = CanLL_WakeUp(CAN_HW_CHANNEL_CANPARA_ONLY);
      break;

#endif /* C_ENABLE_SLEEP_WAKEUP */


#if defined(C_ENABLE_STOP) 
    /* #500 STOP reinit fast request */
    case kCanModeStopReinitFast:
      retVal = CanLL_StopTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST busOffRecovery, ramCheck);
      break;
#endif

    /* #600 START request */
    case kCanModeStart:
    /* #700 START reinit request */
    case kCanModeStartReinit:
      retVal = CanLL_StartTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST mode, ramCheck);
      break;

    /* #900 BUSOFF start request */
    case kCanModeResetBusOffStart:
#if defined ( C_ENABLE_BUSOFF_RECOVERY_COMPLIANT )
      retVal = kCanOk;
#else
      retVal = CanLL_ReInit(CAN_CHANNEL_CANPARA_FIRST ramCheck);
#endif
      break;

    /* #1000 BUSOFF end request */
    case kCanModeResetBusOffEnd:
#if defined ( C_ENABLE_BUSOFF_RECOVERY_COMPLIANT )
      retVal = CanLL_BusOffEndTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST ramCheck);
#else
      retVal = CanLL_StartTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_FIRST mode, ramCheck);
#endif
      break;

    /* #1100 RESET bus sleep */
    case kCanModeResetBusSleep:
      retVal = CanLL_ReInit(CAN_CHANNEL_CANPARA_FIRST ramCheck);
      break;

    default:
      /* retVal is kCanFailed */
      break;
  }

  return retVal;
}
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_StopReinit
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_StopReinit(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  CanObjectHandle hwObjHandle;
  CanObjectHandle mailboxHandle = 0u; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  tFlexCANPtr     pFlexCANLocal;
# if defined ( C_ENABLE_RX_BASICCAN_OBJECTS )
#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  CanObjectHandle hwObjCount;
#  else
  vuint8 fifoLoopCnt = 0u;
#  endif
# endif

  /* #10 Clear all pending Rx/Tx interrupts */
  pFlexCANLocal = pFlexCAN(canHwChannel);
  pFlexCANLocal->iflag1 = CANSFR_SET; /* clear pending Rx/Tx interrupts */ /* SBSW_CAN_LL01 */
# if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  pFlexCANLocal->iflag2 = CANSFR_SET; /* clear pending Rx/Tx interrupts */ /* SBSW_CAN_LL01 */
# endif
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  if defined ( C_ENABLE_ASYM_MAILBOXES ) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
  if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  endif
  {
    pFlexCANLocal->iflag3 = CANSFR_SET; /* clear pending Rx/Tx interrupts */ /* SBSW_CAN_LL01 */
  }
# endif
# if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  pFlexCANLocal->iflag4 = CANSFR_SET; /* clear pending Rx/Tx interrupts */ /* SBSW_CAN_LL01 */
# endif

  /* #20 Clear pending BusOff and error interrupts */
  pFlexCANLocal->estat = (vuint32)kFlexCAN_STATUS_INT; /* SBSW_CAN_LL01 */

  /* #30 Activate BusOff auto recovery */
  pFlexCANLocal->control1 &= (vuint32)(~kFlexCAN_BOFF_REC); /* SBSW_CAN_LL01 */

  /* #40 Inactivate all Tx mailboxes */
  {
    CanObjectHandle logTxObjHandle;
#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
    CanTransmitHandle txHandle;
#  endif

    for (mailboxHandle = CAN_HL_MB_TX_STARTINDEX(canHwChannel); mailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel); mailboxHandle++)
    {
      hwObjHandle = Can_GetMailboxHwHandle(mailboxHandle);
      logTxObjHandle = (CanObjectHandle)((vsintx)mailboxHandle + CAN_HL_TX_OFFSET_MB_TO_LOG(canHwChannel)); /* PRQA S 4393 */ /* MD_Can_MixedSigns */

#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
      /* inform application, if a pending transmission is canceled */
      txHandle = canHandleCurTxObj[logTxObjHandle];
#   if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
      if (txHandle < kCanNumberOfTxObjects)
      {
        APPLCANCANCELNOTIFICATION(channel, txHandle);
      }
#   endif
#   if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
      if(txHandle == kCanBufferMsgTransmit)
      {
        APPLCANMSGCANCELNOTIFICATION(channel);
      }
#   endif
#  endif
      canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) = kTxCodeInactive; /* set mailbox inactive */ /* SBSW_CAN_LL02 */
    }
  }


# if defined ( C_ENABLE_RX_FULLCAN_OBJECTS )
  /* #50 Disable all Rx FullCAN mailboxes */
  for (mailboxHandle = CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel); mailboxHandle < CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel); mailboxHandle++)
  {
    hwObjHandle = Can_GetMailboxHwHandle(mailboxHandle);
    LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) &= kRxCodeClear; /* clear control register and do not change the ID type */ /* SBSW_CAN_LL02 */
    LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) |= kRxCodeEmpty; /* set mailbox ready for receive */ /* SBSW_CAN_LL02 */
  }
# endif

# if defined ( C_ENABLE_RX_BASICCAN_OBJECTS )
#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
  /* #60 Disable all Rx BasicCAN mailboxes */
  for (mailboxHandle = CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel); mailboxHandle < CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel); mailboxHandle++)
  {
    for (hwObjCount = 0u; hwObjCount < Can_GetMailboxSize(mailboxHandle); hwObjCount++)
    {
      hwObjHandle = Can_GetMailboxHwHandle(mailboxHandle) + hwObjCount;
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) &= kRxCodeClear; /* clear control register and do not change the ID type */ /* SBSW_CAN_LL02 */
      LOCAL_MAILBOX_ACCESS(canHwChannel, hwObjHandle, control) |= kRxCodeEmpty; /* set mailbox ready for receive */ /* SBSW_CAN_LL02 */
    }
  }
#  else
  /* #70 Clear pending RxFIFO interrupts */
  while(((pFlexCANLocal->iflag1 & kRxFIFO_NEWMSG) != 0u) && (fifoLoopCnt < C_FLEXCAN_RXFIFO_MAXLOOP)) /* COV_CAN_RXFIFO_HANDLING */
  {
    pFlexCANLocal->iflag1 = (vuint32)kRxFIFO_NEWMSG; /* clear RxFIFO pending interrupts */ /* SBSW_CAN_LL01 */
    fifoLoopCnt++;
  }
#  endif
# endif
}
/* CODE CATEGORY 4 END */

#if defined( C_ENABLE_RX_QUEUE )
/**********************************************************************************************************************
 *  CanLL_RxQueueCopyMsgObj
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxQueueCopyMsgObj(CAN_CHANNEL_CANTYPE_FIRST CanRxQueueCopyParaStructPtr rxQueueCopyPara)
{
  vuint8 idx;
  vuint8 objLen;
# if defined(C_ENABLE_CAN_FD_FULL)
  objLen = CanRxActualDLC(rxQueueCopyPara->src) + 8u;
#else
  objLen = 16u;
# endif
  for (idx = 0u; idx < objLen; idx++)
  {
    ((CanChipDataPtr)(rxQueueCopyPara->dest))[idx] = ((CanChipDataPtr)rxQueueCopyPara->src->pChipMsgObj)[idx];
  }
  CAN_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_RxQueueSetRxInfoStructExtension
 *********************************************************************************************************************/
/* CODE CATEGORY 2 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_RxQueueSetRxInfoStructExtension(CAN_CHANNEL_CANTYPE_FIRST CanRxInfoStructPtr rxStructPtr)
{
  /* #10 nothing to do here */
  CAN_CHANNEL_AND_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(rxStructPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 2 END */
#endif /* C_ENABLE_RX_QUEUE */

#if (defined( C_ENABLE_CAN_RXTX_INTERRUPT )    || \
     defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )  || \
     defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )) 
/**********************************************************************************************************************
 *  CanLL_CanInterruptDisable
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_CanInterruptDisable(CAN_HW_CHANNEL_CANTYPE_FIRST tCanLLCanIntOldPtr localInterruptOldFlagPtr)
{
#if defined( C_ENABLE_CAN_RXTX_INTERRUPT )   || \
    defined( C_ENABLE_CAN_BUSOFF_INTERRUPT ) || \
    defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
  {
    /* ----- Local Variables ---------------------------------------------- */
    tFlexCANPtr pFlexcan = pFlexCAN(canHwChannel);

    /* ----- Implementation ----------------------------------------------- */
    /* #10 Disable RxTx interrupts */
    CAN_DISABLE_RXTX_INTERRUPT_0TO31(localInterruptOldFlagPtr); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
    CAN_DISABLE_RXTX_INTERRUPT_32TO63(localInterruptOldFlagPtr); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
# if defined (C_ENABLE_ASYM_MAILBOXES) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
    if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
# endif
    {
      CAN_DISABLE_RXTX_INTERRUPT_64TO95(localInterruptOldFlagPtr); /* PRQA S 2880 */ /* MD_Can_ConstValue */ /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
    }
    CAN_DISABLE_RXTX_INTERRUPT_96TO127(localInterruptOldFlagPtr); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */

    /* #20 Disable BusOff interrupts */
    CAN_DISABLE_BUSOFF_INTERRUPT(localInterruptOldFlagPtr); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */

    /* #30 Disable WakeUp interrupts */
    CAN_DISABLE_WAKEUP_INTERRUPT(localInterruptOldFlagPtr); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
  }
#else
  CAN_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(localInterruptOldFlagPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
#endif
}
/* CODE CATEGORY 1 END */

/**********************************************************************************************************************
 *  CanLL_CanInterruptRestore
 *********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_CanInterruptRestore(CAN_HW_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld localInterruptOldFlag)
{
#if defined( C_ENABLE_CAN_RXTX_INTERRUPT )   || \
    defined( C_ENABLE_CAN_BUSOFF_INTERRUPT ) || \
    defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
  {
    /* ----- Local Variables ---------------------------------------------- */
    tFlexCANPtr pFlexcan = pFlexCAN(canHwChannel);

    /* ----- Implementation ----------------------------------------------- */
    /* #10 Restore RxTx interrupts */
    CAN_RESTORE_RXTX_INTERRUPT_0TO31(localInterruptOldFlag); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
    CAN_RESTORE_RXTX_INTERRUPT_32TO63(localInterruptOldFlag); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
# if defined (C_ENABLE_ASYM_MAILBOXES) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
    if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
# endif
    {
      CAN_RESTORE_RXTX_INTERRUPT_64TO95(localInterruptOldFlag); /* PRQA S 2880 */ /*  MD_MSR_Unreachable */ /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
    }
    CAN_RESTORE_RXTX_INTERRUPT_96TO127(localInterruptOldFlag); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */

    /* #20 Restore BusOff interrupts */
    CAN_RESTORE_BUSOFF_INTERRUPT(localInterruptOldFlag); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */

    /* #30 Restore WakeUp interrupts */
    CAN_RESTORE_WAKEUP_INTERRUPT(localInterruptOldFlag); /* SBSW_CAN_LL07 */ /* SBSW_CAN_LL01 */
  }
#else
  CAN_HW_CHANNEL_DUMMY_STATEMENT; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(localInterruptOldFlag); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
#endif
}
/* CODE CATEGORY 1 END */
#endif

# if defined(VGEN_ENABLE_MDWRAP) || defined(VGEN_ENABLE_QWRAP) || defined(C_ENABLE_UPDATE_BASE_ADDRESS)
/**********************************************************************************************************************
 *  CanLL_BaseAddressRequest
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_BaseAddressRequest(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  /* #10 Get virtual address for FLEXCAN area from application */
#  if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
  canLL_VirtualPtrFieldTemp[canHwChannel] = (V_DEF_P2VAR_PARA(V_NONE, vuint32, AUTOMATIC, APPL_VAR)) ApplCanGetBaseAddress((vuint32)CanHwChannelData[canHwChannel].CanBaseAddress, (sizeof(tFlexCAN)+0x3ffu+sizeof(tCanRxMask)+0x64u)); /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
#  else /* C_MULTIPLE_RECEIVE_CHANNEL */
  canLL_VirtualPtrFieldTemp[canHwChannel] = (V_DEF_P2VAR_PARA(V_NONE, vuint32, AUTOMATIC, APPL_VAR)) ApplCanGetBaseAddress((vuint32)kCanBaseAddress, (sizeof(tFlexCAN)+0x3ffu+sizeof(tCanRxMask)+0x64u)); /* PRQA S 0316 */ /* MD_Can_PointerVoidCast */
#  endif /* C_MULTIPLE_RECEIVE_CHANNEL */
}
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_BaseAddressActivate
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_BaseAddressActivate(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_ONLY)
{
  canLL_VirtualPtrField[canHwChannel] = canLL_VirtualPtrFieldTemp[canHwChannel];
}
/* CODE CATEGORY 4 END */
# endif  /* VGEN_ENABLE_MDWRAP || VGEN_ENABLE_QWRAP || C_ENABLE_UPDATE_BASE_ADDRESS */

# if defined(C_ENABLE_GET_CONTEXT)
/**********************************************************************************************************************
 *  CanLL_GetModuleContext
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_GetModuleContext(tCanModuleContextStructPtr pContext)
{
  CAN_HW_CHANNEL_CANTYPE_LOCAL

  pContext->canMagicNumber = (((vuint32)DRVCAN_IMXFLEXCAN3HLL_VERSION) << 8) 
                             | ((vuint32)(DRVCAN_IMXFLEXCAN3HLL_RELEASE_VERSION & 0xFFu));

  /* copy hardware specific variables */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (canHwChannel = 0u; canHwChannel < kCanNumberOfHwChannels; canHwChannel++)
#  endif
  {
#  if defined( C_ENABLE_TX_FULLCAN_DELAYED_START )
    {
      CanChannelHandle localChannelidx;
      localChannelidx = canHwChannel;
      pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][0] = canLL_canDelayedFullCANTxRequest[localChannelidx][0];
      pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][1] = canLL_canDelayedFullCANTxRequest[localChannelidx][1];
      pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][2] = canLL_canDelayedFullCANTxRequest[localChannelidx][2];
      pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][3] = canLL_canDelayedFullCANTxRequest[localChannelidx][3];
    }
#  endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */

#  if !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
    pContext->canTxIntMask[canHwChannel][0] = canTxIntMask[canHwChannel][0];
    pContext->canTxIntMask[canHwChannel][1] = canTxIntMask[canHwChannel][1];
    pContext->canTxIntMask[canHwChannel][2] = canTxIntMask[canHwChannel][2];
    pContext->canTxIntMask[canHwChannel][3] = canTxIntMask[canHwChannel][3];
  
  /* disable Tx interrupts */
    pFlexCAN(canHwChannel)->imask1 &= (vuint32)~canTxIntMask[canHwChannel][0];
#   if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    pFlexCAN(canHwChannel)->imask2 &= (vuint32)~canTxIntMask[canHwChannel][1];
#   endif
#   if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    pFlexCAN(canHwChannel)->imask3 &= (vuint32)~canTxIntMask[canHwChannel][2];
#   endif
#   if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    pFlexCAN(canHwChannel)->imask4 &= (vuint32)~canTxIntMask[canHwChannel][3];
#   endif

#  endif
  /* disbale BusOff interrupt */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
    pFlexCAN(canHwChannel)->control1 &= (vuint32)(~kFlexCAN_BOFF_MSK);
#  endif
  }
}
/* CODE CATEGORY 4 END */
# endif /* C_ENABLE_GET_CONTEXT */

# if defined(C_ENABLE_SET_CONTEXT)
/**********************************************************************************************************************
 *  CanLL_ModuleContextVersion
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_ModuleContextVersion(tCanModuleContextStructPtr pContext) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  vuint8 retVal = kCanFailed; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  
  if(pContext->canMagicNumber == ((((vuint32)DRVCAN_IMXFLEXCAN3HLL_VERSION) << 8) 
                                 | ((vuint32)(DRVCAN_IMXFLEXCAN3HLL_RELEASE_VERSION & 0xFFu))))
  {
    retVal = kCanOk;
  }

  return retVal;
}
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_SetModuleContext
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_SetModuleContext(tCanModuleContextStructPtr pContext) /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  CAN_HW_CHANNEL_CANTYPE_LOCAL

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (canHwChannel  =0u; canHwChannel < kCanNumberOfHwChannels; canHwChannel++)
#  endif
  {
#  if defined( C_ENABLE_TX_FULLCAN_DELAYED_START )
    {
      CanChannelHandle localChannelidx;

      localChannelidx = canHwChannel;
      canLL_canDelayedFullCANTxRequest[localChannelidx][0] = pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][0];
      canLL_canDelayedFullCANTxRequest[localChannelidx][1] = pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][1];
      canLL_canDelayedFullCANTxRequest[localChannelidx][2] = pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][2];
      canLL_canDelayedFullCANTxRequest[localChannelidx][3] = pContext->canLL_canDelayedFullCANTxRequest[localChannelidx][3];
    }
#  endif /* C_ENABLE_TX_FULLCAN_DELAYED_START */

#  if !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
    canTxIntMask[canHwChannel][0] = pContext->canTxIntMask[canHwChannel][0];
    canTxIntMask[canHwChannel][1] = pContext->canTxIntMask[canHwChannel][1];
    canTxIntMask[canHwChannel][2] = pContext->canTxIntMask[canHwChannel][2];
    canTxIntMask[canHwChannel][3] = pContext->canTxIntMask[canHwChannel][3];
#  endif
  }
}
/* CODE CATEGORY 4 END */

/**********************************************************************************************************************
 *  CanLL_FinalizeSetModuleContext
 *********************************************************************************************************************/
/* CODE CATEGORY 4 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_FinalizeSetModuleContext(tCanModuleContextStructPtr pContext)
{
  CAN_HW_CHANNEL_CANTYPE_LOCAL

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (canHwChannel = 0u; canHwChannel < kCanNumberOfHwChannels; canHwChannel++)
#  endif
  {
#  if !defined ( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
#   if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    pFlexCAN(canHwChannel)->imask4 |= (vuint32)canTxIntMask[canHwChannel][3];
#   endif
#   if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    pFlexCAN(canHwChannel)->imask3 |= (vuint32)canTxIntMask[canHwChannel][2];
#   endif
#   if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    pFlexCAN(canHwChannel)->imask2 |= (vuint32)canTxIntMask[canHwChannel][1];
#   endif
    pFlexCAN(canHwChannel)->imask1 |= (vuint32)canTxIntMask[canHwChannel][0];
#  endif
/* enable BusOff interrupts */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
    pFlexCAN(canHwChannel)->control1 |= kFlexCAN_BOFF_MSK;
#  endif
  }
  CAN_DUMMY_STATEMENT(pContext); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}
/* CODE CATEGORY 4 END */
# endif /* C_ENABLE_SET_CONTEXT */

/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_SetFlexCANToInitMode
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_SetFlexCANToInitMode(CAN_HW_CHANNEL_CANTYPE_FIRST CanInitHandle initObject)
{
  /* ----- Local Variables ---------------------------------------------- */
  Can_ReturnType loopResult = CAN_OK; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  vuint8 returnCode = kCanOk; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if FlexCAN is DISABLED or in a NON_BUSOFF state (and in NON_LOM mode) */
  if (((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_DISABLE_MODE) == kFlexCAN_DISABLE_MODE)
    || (((CAN_READ_PROTECTED_REG_32BIT(pFlexCANLocal->estat) & kFlexCAN_FCS_BOFF) == 0u)
#if defined(C_ENABLE_SILENT_MODE)
    && ((CAN_READ_PROTECTED_REG_32BIT(pFlexCANLocal->control1) & kFlexCAN_LOM) == 0u)
#endif
  )) /* COV_CAN_INIT_STIMULATION */ /* SBSW_CAN_LL01 */ /* SBSW_CAN_LL01 */ /* SBSW_CAN_LL01 */
  {
    /* #20 Check if FlexCAN is ENABLED */
    if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_DISABLE_ONLY_BITS) == 0u) /* COV_CAN_INIT_STIMULATION */ /* SBSW_CAN_LL01 */
    {
      /* #30 Enter FREEZE mode (INIT mode) of FlexCAN */
      CAN_WRITE_PROTECTED_REG_SET(pFlexCANLocal->canmcr, kFlexCAN_FRZ); /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_SET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* SBSW_CAN_LL01 */
      if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_MCR) != kFlexCAN_FREEZE_MODE) /* COV_CAN_HARDWARE_PROCESSING_TIME */ /* SBSW_CAN_LL01 */
      {
        CanLL_ApplCanTimerStart(kCanLoopEnterFreezeModeInit); /* start hw loop timer */ /* SBSW_CAN_LL08 */
        do  
        { /* wait for FRZACK */  
          loopResult = CanLL_ApplCanTimerLoop(kCanLoopEnterFreezeModeInit);
        } while(((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_MCR) != kFlexCAN_FREEZE_MODE) && (loopResult == CAN_OK)); /* COV_CAN_TIMEOUT_DURATION */ /* SBSW_CAN_LL01 */ /* PRQA S 2995,0303 */ /* MD_Can_ConstValue */ /* MD_Can_HwAccess */
        CanLL_ApplCanTimerEnd(kCanLoopEnterFreezeModeInit); /* stop hw loop timer */ /* SBSW_CAN_LL08 */
#if defined( C_ENABLE_HW_LOOP_TIMER )
        if ((loopResult != CAN_OK) && (canLL_FlexCANInitResultNeeded[canHwChannel] == kCanTrue)) /* COV_CAN_TIMEOUT_DURATION */
        {
          returnCode = kCanFailed;
        }
#endif
      }
      
      /* #40 Enter DISABLE mode of FlexCAN for clock selection */
      CAN_WRITE_PROTECTED_REG_SET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* disable FlexCAN module before clock selection */ /* SBSW_CAN_LL01 */
      if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_LPM_ACK) != kFlexCAN_LPM_ACK) /* COV_CAN_HARDWARE_PROCESSING_TIME */ /* SBSW_CAN_LL01 */
      {
        CanLL_ApplCanTimerStart(kCanLoopEnterDisableModeInit); /* start hw loop timer */ /* SBSW_CAN_LL08 */
        do
        { /* wait for FlexCAN is disabled */
          loopResult = CanLL_ApplCanTimerLoop(kCanLoopEnterDisableModeInit);
        } while(((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_LPM_ACK) != kFlexCAN_LPM_ACK) && (loopResult == CAN_OK)); /* COV_CAN_TIMEOUT_DURATION */ /* SBSW_CAN_LL01 */ /* PRQA S 2995,0303 */ /* MD_Can_ConstValue */ /* MD_Can_HwAccess */
        CanLL_ApplCanTimerEnd(kCanLoopEnterDisableModeInit);  /* stop hw loop timer */ /* SBSW_CAN_LL08 */
#if defined( C_ENABLE_HW_LOOP_TIMER )
        if ((loopResult != CAN_OK)  && (canLL_FlexCANInitResultNeeded[canHwChannel] == kCanTrue)) /* COV_CAN_TIMEOUT_DURATION */
        {
          returnCode = kCanFailed;
        }
#endif
      }
    }
    
    /* #50 Set FlexCAN clock source */
    if((Can_GetControl1OfInitObject(initObject) & kFlexCAN_CLK_SRC) != 0u) /* COV_CAN_CLK_SRC_GENERATION */
    {
      CAN_WRITE_PROTECTED_REG_SET_32BIT(pFlexCANLocal->control1, kFlexCAN_CLK_SRC); /* SBSW_CAN_LL01 */
    }
    else
    {
      CAN_WRITE_PROTECTED_REG_RESET_32BIT(pFlexCANLocal->control1, kFlexCAN_CLK_SRC); /* SBSW_CAN_LL01 */
    }

    /* #60 Leave DISABLE mode of FlexCAN */
    CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* clear MDIS bit */ /* SBSW_CAN_LL01 */
    if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_LPM_ACK) == kFlexCAN_LPM_ACK) /* COV_CAN_HARDWARE_PROCESSING_TIME */ /* SBSW_CAN_LL01 */
    {
      CanLL_ApplCanTimerStart(kCanLoopLeaveDisableModeInit); /* start hw loop timer */ /* SBSW_CAN_LL08 */
      do
      { /* wait for FlexCAN is enabled */
        loopResult = CanLL_ApplCanTimerLoop(kCanLoopLeaveDisableModeInit);
      } while(((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_LPM_ACK) == kFlexCAN_LPM_ACK) && (loopResult == CAN_OK)); /* COV_CAN_TIMEOUT_DURATION */ /* SBSW_CAN_LL01 */ /* PRQA S 2995,0303 */ /* MD_Can_ConstValue */ /* MD_Can_HwAccess */
      CanLL_ApplCanTimerEnd(kCanLoopLeaveDisableModeInit);  /* stop hw loop timer */ /* SBSW_CAN_LL08 */
#if defined( C_ENABLE_HW_LOOP_TIMER )
      if ((loopResult != CAN_OK)  && (canLL_FlexCANInitResultNeeded[canHwChannel] == kCanTrue)) /* COV_CAN_TIMEOUT_DURATION */
      {
        returnCode = kCanFailed;
      }
#endif
    }
  }

  return (returnCode);
} /* PRQA S 6010,6030 */ /* MD_MSR_STPTH,MD_MSR_STCYC */
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_ExecuteSoftReset
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_ExecuteSoftReset(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  Can_ReturnType loopResult1 = CAN_OK; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
#if defined( C_ENABLE_CAN_FD_USED ) && defined( C_ENABLE_WORKAROUND_ERR010368 )
  Can_ReturnType loopResult2 = CAN_OK; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
#endif
  vuint8 returnCode = kCanOk; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  
  /* ----- Implementation ----------------------------------------------- */     
  /* *********************************************************************** */
  /* Perform FlexCAN soft reset.                                             */
  /* Please note: the soft reset cannot be applied while clocks are          */
  /* shut down: low power mode or clocks are not enabled.                    */
  /* Please check clock settings if soft reset cannot be finalized.          */
  /* Errata e10368: Perform a second soft reset in case of FD enabled        */
  /* *********************************************************************** */
  /* #10 Execute SoftReset of FlexCAN */
  CAN_WRITE_PROTECTED_REG(pFlexCANLocal->canmcr, kFlexCAN_SOFT_RST); /* SBSW_CAN_LL01 */
  if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_SOFT_RST) == kFlexCAN_SOFT_RST) /* COV_CAN_HARDWARE_PROCESSING_TIME */ /* SBSW_CAN_LL01 */
  {
    /* #20 Wait for soft reset confirmation from hardware */
    CanLL_ApplCanTimerStart(kCanLoopResetInit); /* start hw loop timer */ /* SBSW_CAN_LL08 */
    do
    { /* wait for SOFT_RST */
      loopResult1 = CanLL_ApplCanTimerLoop(kCanLoopResetInit);
    } while(((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_SOFT_RST) == kFlexCAN_SOFT_RST) && (loopResult1 == CAN_OK)); /* SBSW_CAN_LL01 */ /* COV_CAN_TIMEOUT_DURATION */ /* PRQA S 2995,0303 */ /* MD_Can_ConstValue */ /* MD_Can_HwAccess */
    CanLL_ApplCanTimerEnd(kCanLoopResetInit);  /* stop hw loop timer */ /* SBSW_CAN_LL08 */
  }
#if defined( C_ENABLE_CAN_FD_USED ) && defined( C_ENABLE_WORKAROUND_ERR010368 )
  /* #30 Execute SoftReset of FlexCAN second time (errata e10368) if CAN FD is enabled */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL ) 
  if (Can_IsHasCANFDBaudrateOfControllerConfig(canHwChannel)) /* COV_CAN_FD_BAUDRATE_AVAILABILITY */
# endif
  {
    CAN_WRITE_PROTECTED_REG(pFlexCANLocal->canmcr, kFlexCAN_SOFT_RST); /* SBSW_CAN_LL01 */
    if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_SOFT_RST) == kFlexCAN_SOFT_RST) /* COV_CAN_HARDWARE_PROCESSING_TIME */ /* SBSW_CAN_LL01 */
    {
      /* #40 Wait for soft reset confirmation from hardware second time (errata e10368) if CAN FD is enabled */
      CanLL_ApplCanTimerStart(kCanLoopResetInit); /* start hw loop timer */ /* SBSW_CAN_LL08 */
      do
      { /* wait for SOFT_RST */
        loopResult2 = CanLL_ApplCanTimerLoop(kCanLoopResetInit);
      } while(((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_SOFT_RST) == kFlexCAN_SOFT_RST) && (loopResult2 == CAN_OK)); /* SBSW_CAN_LL01 */ /* COV_CAN_TIMEOUT_DURATION */ /* PRQA S 2995,0303 */ /* MD_Can_ConstValue */ /* MD_Can_HwAccess */
      CanLL_ApplCanTimerEnd(kCanLoopResetInit);  /* stop hw loop timer */ /* SBSW_CAN_LL08 */
    }
  }
#endif

#if defined(C_ENABLE_HW_LOOP_TIMER)
  if (((loopResult1 != CAN_OK)
# if defined( C_ENABLE_CAN_FD_USED ) && defined( C_ENABLE_WORKAROUND_ERR010368 )
      || (loopResult2 != CAN_OK)
# endif
      ) && (canLL_FlexCANInitResultNeeded[canHwChannel] == kCanTrue)) /* COV_CAN_TIMEOUT_DURATION */
  {
    returnCode = kCanFailed;
  }
#endif

  /* #50 Reset SUPV bit to grant access to CAN register (except MCR register) in User Mode */
  /* set all CAN register except the MCR register in unrestricted memory space directly after soft reset */
  CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_SUPV); /* SBSW_CAN_LL01 */

  return (returnCode);
}
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_ReInit
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_ReInit(CAN_CHANNEL_CANTYPE_FIRST vuint8 doRamCheck)
{
  vuint8 retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* *********************************************************************** */
  /* Reinitialisation of FlexCAN                                             */
  /* *********************************************************************** */
  /* #10 Request CanHL_ReInit() */
#if defined (C_ENABLE_HW_LOOP_TIMER)
        canLL_FlexCANInitResultNeeded[channel] = kCanFalse; /* SBSW_CAN_LL14 */
#endif
        retVal = CanHL_ReInit(CAN_CHANNEL_CANPARA_FIRST doRamCheck);
#if defined (C_ENABLE_HW_LOOP_TIMER)
        canLL_FlexCANInitResultNeeded[channel] = kCanTrue; /* SBSW_CAN_LL14 */
#endif
  return (retVal);
}
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_StopTransition
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_StopTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 busOffRecovery, vuint8 doRamCheck)
{
  vuint8 retVal;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* ----- Implementation ----------------------------------------------- */
  /* *********************************************************************** */
  /* Handle stop transition of FlexCAN                                       */
  /* *********************************************************************** */
  if (CanLL_HwIsBusOff(canHwChannel)) /* COV_CAN_STOP_BOFF_STIMULATION */ /* SBSW_CAN_LL01 */
  { /* #10 Check for BusOff state */
    if (busOffRecovery == kCanContinueBusOffRecovery) /* COV_CAN_STOP_BOFF_STIMULATION */
    { /* #20 Stop Transition in case of kCanContinueBusOffRecovery */
      retVal = kCanFailed;
    }
    else
    { /* #30 Stop Transition in case of kCanFinishBusOffRecovery */
      retVal = CanLL_ReInit(CAN_CHANNEL_CANPARA_FIRST doRamCheck);
    }
  }
# if defined (C_ENABLE_SILENT_MODE)
  else if (CanLL_HwIsListenOnlyMode(canHwChannel)) /* SBSW_CAN_LL01 */
  { /* #40 Stop Transition in case of silent mode active */
      retVal = CanLL_ReInit(CAN_CHANNEL_CANPARA_FIRST doRamCheck);
  }
# endif
  else
  { /* #50 Fast Re-Init Stop Transition */
    if (CanLL_HwIsStop(canHwChannel)) /* SBSW_CAN_LL01 */
    { /* #60 Check for stop transition finished */
      CanLL_StopReinit(CAN_CHANNEL_AND_HW_CHANNEL_CANPARA_ONLY);
      CanHL_CleanUpSendState(CAN_CHANNEL_CANPARA_ONLY);
      retVal = kCanOk;
    }
    else if (CanLL_HwIsStopRequested(canHwChannel)) /* SBSW_CAN_LL01 */
    { /* #70 Check for stop mode requested */
      retVal = kCanRequested;
    }
    else
    { /* #80 Request stop mode for all other cases */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* clear MDIS bit */ /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_SET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* set FRZ bit */ /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_SET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* set HALT bit */ /* SBSW_CAN_LL01 */
      retVal = kCanRequested;
    }
  }

  return (retVal);
}
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_StartTransition
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_StartTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 mode, vuint8 doRamCheck)
{
  vuint8 retVal;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
  retVal = kCanOk;
  /* ----- Implementation ----------------------------------------------- */
  /* *********************************************************************** */
  /* Handle start transition of FlexCAN                                      */
  /* *********************************************************************** */
  if (mode == kCanModeStartReinit)
  { /* #10 Check for kCanModeStartReinit */
    if (canLL_canStartReinitState[canHwChannel] == kCanLLStateStart)
    { /* #20 Check for reinitialization execution */
      retVal = CanHL_ReInit(CAN_CHANNEL_CANPARA_FIRST doRamCheck);
      canLL_canStartReinitState[canHwChannel] = kCanLLStateRequested;
    }
  }
  if (retVal == kCanOk)
  {
    if (CanLL_HwIsStart(canHwChannel)) /* SBSW_CAN_LL01 */
    { /* #30 Check for start transition finished */
      canLL_canStartReinitState[canHwChannel] = kCanLLStateStart;
      retVal = kCanOk;
    }
    else if (CanLL_HwIsStartRequested(canHwChannel)) /* SBSW_CAN_LL01 */ /* COV_CAN_START_STIMULATION */
    { /* #40 Check for start mode requested */
      retVal = kCanRequested;
    }
    else
    { /* #50 Process start mode */
      {
        /* #70 Request start mode */
        /* request NORMAL mode - considered as CanStart mode */
        CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* clear MDIS bit */ /* SBSW_CAN_LL01 */
        CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* clear HALT bit */ /* SBSW_CAN_LL01 */
        CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* clear FRZ bit  */ /* SBSW_CAN_LL01 */

        /* #70 Deactivate BusOff auto recovery mode */
        /* the transition CanStop - CanStart does also leave a potential BusOff state */
        pFlexCANLocal->control1 |= kFlexCAN_BOFF_REC; /* SBSW_CAN_LL01 */

        retVal = kCanRequested;
      }
    }
  }
  CAN_DUMMY_STATEMENT(doRamCheck); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  CAN_DUMMY_STATEMENT(mode); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  return (retVal);
}
/* CODE CATEGORY 4 END */

#if defined ( C_ENABLE_BUSOFF_RECOVERY_COMPLIANT )
/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_BusOffEndTransition
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_BusOffEndTransition(CAN_CHANNEL_AND_HW_CHANNEL_CANTYPE_FIRST vuint8 doRamCheck)
{
  vuint8 retVal;
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* ----- Implementation ----------------------------------------------- */
  /* *********************************************************************** */
  /* Handle busoff end transition of FlexCAN                                      */
  /* *********************************************************************** */
  if (CanLL_HwIsBusOff(canHwChannel)) /* SBSW_CAN_LL01 */
  { /* #10 Check for busoff state */
    if (!CanLL_HwIsAutoRecoveryActive(canHwChannel)) /* SBSW_CAN_LL01 */ /* COV_CAN_BUSOFFEND_STIMULATION */
    { /* #20 Activate busoff auto recovery */
      pFlexCANLocal->control1 &= (vuint32)(~kFlexCAN_BOFF_REC); /* SBSW_CAN_LL01 */
    }
    if (!CanLL_HwIsStartRequested(canHwChannel)) /* COV_CAN_BUSOFFEND_STIMULATION */ /* SBSW_CAN_LL01 */
    { /* #30 Start auto recovery process in case of CAN controler is not started */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* clear MDIS bit */ /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* clear HALT bit */ /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* clear FRZ bit  */ /* SBSW_CAN_LL01 */
    }
    retVal = kCanRequested;
  }
  else
  { /* #40 Check for busoff state left */
    if (CanLL_HwIsStart(canHwChannel)) /* COV_CAN_BUSOFFEND_STIMULATION */ /* SBSW_CAN_LL01 */
    { /* #50 Check for start transition finished */
      if (CanLL_HwIsAutoRecoveryActive(canHwChannel)) /* SBSW_CAN_LL01 */
      { /* #60 In case of autorecovery finished: Controller re-initialization */
        retVal = CanLL_ReInit(CAN_CHANNEL_CANPARA_FIRST doRamCheck);
        if (retVal == kCanOk) /* COV_CAN_BUSOFFEND_STIMULATION */
        {
          CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* clear MDIS bit */ /* SBSW_CAN_LL01 */
          CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* clear HALT bit */ /* SBSW_CAN_LL01 */
          CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* clear FRZ bit  */ /* SBSW_CAN_LL01 */

          pFlexCANLocal->control1 |= kFlexCAN_BOFF_REC; /* SBSW_CAN_LL01 */
          retVal = kCanRequested;
        }
      }
      else
      { /* #70 Start transition finished */
        retVal = kCanOk;
      }
    }
    else if (CanLL_HwIsStartRequested(canHwChannel)) /* COV_CAN_BUSOFFEND_STIMULATION */ /* SBSW_CAN_LL01 */
    { /* #80 Check for start mode requested */
       retVal = kCanRequested;
    }
    else
    { /* #90 Request start mode in case of busoff end due to a controller re-initialization */
      /* request NORMAL mode - considered as CanStart mode */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* clear MDIS bit */ /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* clear HALT bit */ /* SBSW_CAN_LL01 */
      CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* clear FRZ bit  */ /* SBSW_CAN_LL01 */

      /* the transition CanStop - CanStart does also leave a potential BusOff state */
      pFlexCANLocal->control1 |= kFlexCAN_BOFF_REC; /* SBSW_CAN_LL01 */

      retVal = kCanRequested;
    }
  }

  return (retVal);
}
/* CODE CATEGORY 4 END */
#endif

#if defined(C_ENABLE_SLEEP_WAKEUP)
/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_Sleep
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_Sleep(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  vuint8 returnCode = kCanOk; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */

# if defined( C_ENABLE_FLEXCAN_STOP_MODE )
  returnCode = ApplCanEnterLowPowerMode(CanHwLogToPhys[canHwChannel]);
  canLL_canSleepState[canHwChannel] = kCanLLStateStart;
  returnCode = kCanOk;
# else
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* ----- Implementation ----------------------------------------------- */
  /* *********************************************************************** */
  /* Request sleep mode of FlexCAN                                           */
  /* Wait for sleep mode is reached                     .                    */
  /* *********************************************************************** */
  if (canLL_canSleepState[canHwChannel] == kCanLLStateStart)
  {
    /* #10 Request sleep mode of FlexCAN */
    CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* clear HALT bit */ /* SBSW_CAN_LL01 */
    CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* clear FRZ bit */ /* SBSW_CAN_LL01 */
    CAN_WRITE_PROTECTED_REG_SET(pFlexCANLocal->canmcr, kFlexCAN_MDIS);   /* request the DISABLE mode */ /* SBSW_CAN_LL01 */
    canLL_canSleepState[canHwChannel] = kCanLLStateRequested;
    returnCode = kCanRequested;
  }
  else
  {
    /*  #20 Wait for sleep mode is reached */
    if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_MCR) != kFlexCAN_DISABLE_MODE) /* SBSW_CAN_LL01 */
    {
      returnCode = kCanRequested;
    }
    else
    {
      canLL_canSleepState[canHwChannel] = kCanLLStateStart;
      returnCode = kCanOk;
    }
  }
# endif
  return (returnCode);
}
/* CODE CATEGORY 4 END */

/* CODE CATEGORY 4 START */
/****************************************************************************
| NAME:             CanLL_WakeUp
****************************************************************************/
/*!
 * \internal
 *  - #10 Request start mode of FlexCAN
 *  - #20 Wait for start mode is reached
 * \endinternal
 */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint8, STATIC_CODE) CanLL_WakeUp(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  vuint8 returnCode = kCanOk; /* PRQA S 2981,2982 */ /* MD_MSR_RetVal */

# if defined( C_ENABLE_FLEXCAN_STOP_MODE )
  returnCode =  ApplCanLeaveLowPowerMode(CanHwLogToPhys[canHwChannel]);
  canLL_canWakeUpState[canHwChannel] = kCanLLStateStart;
  returnCode = kCanOk;
# else
  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);

  /* ----- Implementation ----------------------------------------------- */
  /* *********************************************************************** */
  /* Request sleep mode of FlexCAN                                           */
  /* Wait for sleep mode is reached                     .                    */
  /* *********************************************************************** */
  if (canLL_canWakeUpState[canHwChannel] == kCanLLStateStart)
  {
    /* #10 Request start mode of FlexCAN */
    CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_MDIS); /* quit DISABLE mode and switch into NORMAL mode */ /* SBSW_CAN_LL01 */
    CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_HALT); /* clear HALT bit */ /* SBSW_CAN_LL01 */
    CAN_WRITE_PROTECTED_REG_RESET(pFlexCANLocal->canmcr, kFlexCAN_FRZ);  /* clear FRZ bit  */ /* SBSW_CAN_LL01 */
    canLL_canWakeUpState[canHwChannel] = kCanLLStateRequested;
    returnCode = kCanRequested;
  }
  else
  {
    /*  #20 Wait for start mode is reached */
    if ((CAN_READ_PROTECTED_REG(pFlexCANLocal->canmcr) & kFlexCAN_MCR) != (vuint16)0u) /* SBSW_CAN_LL01 */
    {
      returnCode = kCanRequested;
    }
    else
    {
      canLL_canWakeUpState[canHwChannel] = kCanLLStateStart;
      returnCode = kCanOk;
    }
  }
# endif
  return (returnCode);
}
/* CODE CATEGORY 4 END */
#endif


#if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanLL_ComputeMailboxInterrupt
****************************************************************************/
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanLL_ComputeMailboxInterrupt(CAN_HW_CHANNEL_CANTYPE_FIRST vuint32 iFlags, vuint32 iMask, CanObjectHandle startIndex) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
{
  vuint32 activeInterrupts;
  CanObjectHandle index;
  
  activeInterrupts = iFlags & iMask;
  index = startIndex;
  /* #10 For all active pending interrupts */
  while(activeInterrupts != 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
  {
    /* #20 Compute index of active interrupt */
    while((activeInterrupts & (vuint32)0xFFu) == 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
    {
      activeInterrupts >>= 8;
      index += (CanObjectHandle)8u;
    }    
    if((activeInterrupts & (vuint32)0x0Fu) == 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
    {
      activeInterrupts >>= 4;
      index += (CanObjectHandle)4u;
    }        
    while((activeInterrupts & (vuint32)0x01u) == 0u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
    {
      activeInterrupts >>= 1;
      index++;
    }

    /* #30 Call computation of interrupt type */
    {
      CanLL_ComputeInterruptType(CAN_HW_CHANNEL_CANPARA_FIRST index);
    }

    index++;
    activeInterrupts = (iFlags & iMask) >> (index-startIndex);
  }
}
/* CODE CATEGORY 1 END */

/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanLL_ComputeInterruptType
****************************************************************************/
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ComputeInterruptType(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle index) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
{
  {
#  if !defined( C_ENABLE_RX_FULLCAN_POLLING ) && defined( C_ENABLE_RX_FULLCAN_OBJECTS ) || !defined( C_ENABLE_RX_BASICCAN_POLLING ) && defined( C_ENABLE_RX_BASICCAN_OBJECTS ) && defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT ) || !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
    CanObjectHandle mailboxHandle;
    mailboxHandle = Can_GetHwToMbHandle(canHwChannel, index); /* PRQA S 	2983, 2986 */ /* MD_MSR_DummyStmt, MD_Can_ConstValue */
#  endif
#  if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
#   if !defined( C_ENABLE_RX_FULLCAN_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
      /* #10 Check if pending interrupt is caused due to a FullCAN reception */
#    if defined (C_SINGLE_RECEIVE_CHANNEL)
#     if (kCanMailboxRxFullStartIndex == 0u)
    if((mailboxHandle < CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel)))
#     else
    if((mailboxHandle >= CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel)) && (mailboxHandle < CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel)))
#     endif
#    else
    if((mailboxHandle >= CAN_HL_MB_RX_FULL_STARTINDEX(canHwChannel)) && (mailboxHandle < CAN_HL_MB_RX_FULL_STOPINDEX(canHwChannel)))
#    endif
    {
      CanHL_FullCanMsgReceived(CAN_HW_CHANNEL_CANPARA_FIRST mailboxHandle, index);
    }
#   endif
#  endif

#  if defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
#   if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
#    if !defined( C_ENABLE_RX_BASICCAN_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
    /* #20 Check if pending interrupt is caused due to a BasicCAN reception */
#     if defined( C_ENABLE_RX_FULLCAN_OBJECTS )
    if((mailboxHandle >= CAN_HL_MB_RX_BASIC_STARTINDEX(canHwChannel)) && (mailboxHandle < CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel)))
#     else
    if(mailboxHandle < CAN_HL_MB_RX_BASIC_STOPINDEX(canHwChannel))
#     endif
    {
      CanHL_BasicCanMsgReceived(CAN_HW_CHANNEL_CANPARA_FIRST mailboxHandle, index);
    }
#    endif
#   endif
#  endif

#  if !defined( C_ENABLE_TX_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
    /* #30 Check if pending interrupt is caused due to a successful transmission */
    if((mailboxHandle >= CAN_HL_MB_TX_STARTINDEX(canHwChannel)) && (mailboxHandle < CAN_HL_MB_TX_STOPINDEX(canHwChannel)))
    {
      CanHL_TxConfirmation(CAN_HW_CHANNEL_CANPARA_FIRST mailboxHandle, 0, index);
    }
#  endif
  }

}
/* CODE CATEGORY 1 END */

/****************************************************************************
| NAME:             CanLL_CanMailboxInterrupt
****************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_CanMailboxInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
{
  vuint32 iflag; /* avoid IAR compiler warning */
  vuint32 imask; /* avoid IAR compiler warning */

  tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
#if defined( C_ENABLE_RX_BASICCAN_OBJECTS ) 
# if !defined ( C_ENABLE_CLASSIC_MAILBOX_LAYOUT )
#  if !defined( C_ENABLE_RX_BASICCAN_POLLING ) || defined( C_ENABLE_INDIVIDUAL_POLLING )
  /* #10 Check if pending interrupt is caused due to a BasicCAN reception (RxFifo) */

#   if defined(C_ENABLE_INDIVIDUAL_POLLING)
  /* additional check is necessary to ensure only objects configured for interrupt are processed here */
  if (!Can_IsMailboxIndivPolling(Can_GetHwToMbHandle(canHwChannel, 0u))) /* COV_CAN_HWOBJINDIVPOLLING */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
#   endif
  {
    vuint8 fifoLoopCnt;
    fifoLoopCnt = 0u;
    while(((pFlexCANLocal->iflag1 & kRxFIFO_NEWMSG) != 0u) && (fifoLoopCnt < C_FLEXCAN_RXFIFO_MAXLOOP)) /* COV_CAN_RXFIFO_HANDLING */
    {
      fifoLoopCnt++;
      CanHL_BasicCanMsgReceived(CAN_HW_CHANNEL_CANPARA_FIRST Can_GetHwToMbHandle(canHwChannel, 0u), 0u); /* PRQA S 2985 */ /* MD_Can_ConstValue */
    }
  }
#  endif
# endif
#endif

  /* #20 Call mailbox interrupt computation function for Mailboxes 0 to 31 */
  iflag = pFlexCANLocal->iflag1;
  imask = pFlexCANLocal->imask1;
  CanLL_ComputeMailboxInterrupt(CAN_HW_CHANNEL_CANPARA_FIRST iflag, imask, 0u);
# if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #30 Call mailbox interrupt computation function for Mailboxes 32 to 63 */
  iflag = pFlexCANLocal->iflag2;
  imask = pFlexCANLocal->imask2;
  CanLL_ComputeMailboxInterrupt(CAN_HW_CHANNEL_CANPARA_FIRST iflag, imask, 32u);
# endif
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  if defined ( C_ENABLE_ASYM_MAILBOXES ) /* COV_CAN_FLEXCAN3_DERIVATIVE_ASYM_MAILBOXES */
  if (NUMBER_OF_MAX_MAILBOXES(canHwChannel) > 64u) /* PRQA S 2741,2742 */ /* MD_Can_ConstValue */ /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
#  endif
  {
    /* #40 Call mailbox interrupt computation function for Mailboxes 64 to 95 */
    iflag = pFlexCANLocal->iflag3;
    imask = pFlexCANLocal->imask3;
    CanLL_ComputeMailboxInterrupt(CAN_HW_CHANNEL_CANPARA_FIRST iflag, imask, 64u);
  }
# endif
# if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  /* #50 Call mailbox interrupt computation function for Mailboxes 96 to 127 */
  iflag = pFlexCANLocal->iflag4;
  imask = pFlexCANLocal->imask4;
  CanLL_ComputeMailboxInterrupt(CAN_HW_CHANNEL_CANPARA_FIRST iflag, imask, 96u);
# endif
}
/* CODE CATEGORY 1 END */
#endif /* C_ENABLE_CAN_RXTX_INTERRUPT */

/**********************************************************************************************************************
*  CanLL_ClearPendingInterrupt
*********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, void, STATIC_CODE) CanLL_ClearPendingInterrupt( tFlexCANPtr pFlexCANLocal, CanObjectHandle hwObjHandle )
{
  if (hwObjHandle < 32u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
  { /* #10 Clear pending interrupt for mailbox 0 to 31 */
    pFlexCANLocal->iflag1 = CanBitMask(hwObjHandle); /* clear pending interrupt flag  */ /* SBSW_CAN_LL01 */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
  }
#if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  else
  { /* #20 Clear pending interrupt for mailbox 32 to 63 */
    if (hwObjHandle < 64u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
    {
      pFlexCANLocal->iflag2 = CanBitMask(hwObjHandle); /* clear pending interrupt flag  */ /* SBSW_CAN_LL01 */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
    }
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    else
    { /* #30 Clear pending interrupt for mailbox 64 to 95 */
      if (hwObjHandle < 96u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
      {
        pFlexCANLocal->iflag3 = CanBitMask(hwObjHandle); /* clear pending interrupt flag  */ /* SBSW_CAN_LL01 */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
      }
#  if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
      else
      { /* #40 Clear pending interrupt for mailbox 96 to 128 */
        pFlexCANLocal->iflag4 = CanBitMask(hwObjHandle); /* clear pending interrupt flag  */ /* SBSW_CAN_LL01 */ /* PRQA S 2985 */ /* MD_Can_ConstValue */
      }
#  endif
    }
# endif
  }
#endif
}
/* CODE CATEGORY 1 END */

#if defined (C_ENABLE_TX_POLLING) || ( defined (C_ENABLE_RX_FULLCAN_POLLING) && defined (C_ENABLE_RX_FULLCAN_OBJECTS) ) || ( (defined (C_ENABLE_RX_BASICCAN_POLLING) && defined (C_ENABLE_RX_BASICCAN_OBJECTS)) && defined (C_ENABLE_CLASSIC_MAILBOX_LAYOUT) )
/**********************************************************************************************************************
*  CanLL_GetInterruptFlag
*********************************************************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_LOCAL_INLINE, vuint32, STATIC_CODE) CanLL_GetInterruptFlag( tFlexCANPtr pFlexCANLocal, CanObjectHandle hwObjHandle ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */ /* PRQA S 3673 */ /* MD_Can_NoneConstParameterPointer */
{
  vuint32 iFlags = 0u;

  if (hwObjHandle < 32u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
  { /* #10 Get iflag for mailboxes 0 to 31 */
    iFlags = pFlexCANLocal->iflag1;
  }
#if defined ( C_ENABLE_MB32TO63 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
  else
  {
    if (hwObjHandle < 64u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
    { /* #20 Get iflag for mailboxes 32 to 63 */
      iFlags = pFlexCANLocal->iflag2;
    }
# if defined ( C_ENABLE_MB64TO95 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
    else
    {
      if (hwObjHandle < 96u) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
      { /* #30 Get iflag for mailboxes 64 to 95 */
        iFlags = pFlexCANLocal->iflag3;
      }
#  if defined ( C_ENABLE_MB96TO127 ) /* COV_CAN_FLEXCAN3_DERIVATIVE_NUMBER_OF_HW_MB */
      else
      { /* #40 Get iflag for mailboxes 96 to 128 */
        iFlags = pFlexCANLocal->iflag4;
      }
#  endif
    }
# endif
  }
#endif

  return (iFlags);
}
/* CODE CATEGORY 1 END */
#endif



/* *********************************************************************** */
/* Interrupt functions                                                     */
/* *********************************************************************** */


#if defined( C_ENABLE_INTERRUPT_SOURCE_SINGLE ) /* COV_CAN_FLEXCAN3_DERIVATIVE_INTERRUPT_SOURCE_SINGLE */
# if defined( C_ENABLE_CAN_RXTX_INTERRUPT )   || \
     defined( C_ENABLE_CAN_BUSOFF_INTERRUPT ) || \
     defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
/* **************************************************************************
| NAME:             CanInterrupt<Name>
| CALLED BY:        CanIsr<Name>_<physicalChannelIndex>()
| PRECONDITIONS:
| INPUT PARAMETERS: canHwChannel
| RETURN VALUES:    none
| DESCRIPTION:      Interrupt service functions according to the CAN controller
|                   interrupt structure
|                   - check for the interrupt reason ( interrupt source )
|                   - work appropriate interrupt:
|                     + status/error interrupt (BUSOFF, wakeup, error warning)
|                     + basic can receive
|                     + full can receive
|                     + can transmit
|
|                   If an RX-Interrupt occurs while the CAN controller is in Sleep mode, 
|                   a wakeup has to be generated. 
|
|                   If an TX-Interrupt occurs while the CAN controller is in Sleep mode, 
|                   an assertion has to be called and the interrupt has to be ignored.
|
|                   The name of BrsTimeStrt...() and BrsTimeStop...() can be adapted to 
|                   really used name of the interrupt functions.
|
************************************************************************** */
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY )
{
  {
#  if defined( C_ENABLE_CAN_WAKEUP_INTERRUPT ) || defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
    tFlexCANPtr pFlexCANLocal = pFlexCAN(canHwChannel);
#  endif
  

#  if defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
    /* #10 Process wakeup interrupt */
    if((pFlexCANLocal->estat & kFlexCAN_WAKE_INT) != 0u)
    {
      CanLL_WakeUpHandling(CAN_HW_CHANNEL_CANPARA_ONLY);
    }
#  endif

#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
#   if defined(C_ENABLE_SET_CONTEXT) || defined(C_ENABLE_GET_CONTEXT)
    /* for QNX: interrupt function is called periodically from mdwrap (MdWrapPollCan) while context switch is processed.
     * busoff interrupts have to be suppressed meanwhile. if busoff occurs during context switch the busoff interrupt flag is set.
     * this condition assures that such a busoff event is only processed if busoff interrupts are enabled. The same behavior is
     * implemented for Normal Tx interrupts. See code below. For FullCAN Tx interrupts there is no need to check if the corresponding
     * interrupt masks are set. For them only configured interrupts are considered (iMask2/1).
     * NOTE: QNX only works with single ISR API. */
    /* #20 Process busoff interrupt */
    if ((pFlexCANLocal->control1 & kFlexCAN_BOFF_MSK) != 0u)
#   endif
    {
      if((pFlexCANLocal->estat & kFlexCAN_BOFF_INT) != 0u) /* COV_CAN_BUSOFF_INTERRUPT_ONLY */
      {
        CanHL_ErrorHandling( CAN_HW_CHANNEL_CANPARA_ONLY );
      }
    }
#  endif

#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
    /*  #30 Process mailbox interrupt */
    CanLL_CanMailboxInterrupt(CAN_HW_CHANNEL_CANPARA_ONLY);
#  endif /* C_ENABLE_CAN_RXTX_INTERRUPT */

  }
} /* end of CanInterrupt */
/* CODE CATEGORY 1 END */
# endif
#endif /* C_ENABLE_INTERRUPT_SOURCE_SINGLE */

#if defined ( C_ENABLE_INTERRUPT_SOURCE_MULTIPLE ) /* COV_CAN_FLEXCAN3_DERIVATIVE_INTERRUPT_SOURCE_MULTIPLE */
# if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
/****************************************************************************
| NAME:             CanBusOffInterrupt
****************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanBusOffInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY )
{

  /* #10 Call internal indication function if BUSOFF occurred */
  {
    CanHL_ErrorHandling( CAN_HW_CHANNEL_CANPARA_ONLY );
  }

}
/* CODE CATEGORY 1 END */
# endif
# if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/****************************************************************************
| NAME:             CanMailboxInterrupt
****************************************************************************/
/* CODE CATEGORY 1 START */
V_DEF_FUNC(CAN_STATIC, void, STATIC_CODE) CanMailboxInterrupt( CAN_HW_CHANNEL_CANTYPE_ONLY )
{

  /* #10 Process mailbox interrupt */
  {
    CanLL_CanMailboxInterrupt(CAN_HW_CHANNEL_CANPARA_ONLY);
  }

}
# endif
#endif /* C_ENABLE_INTERRUPT_SOURCE_MULTIPLE */

#if defined( C_SINGLE_RECEIVE_CHANNEL )
# define CanPhysToLogChannel_0
# define CanPhysToLogChannelIndex_0
# define CanPhysToLogChannel_1
# define CanPhysToLogChannelIndex_1
# define CanPhysToLogChannel_2
# define CanPhysToLogChannelIndex_2
# define CanPhysToLogChannel_3
# define CanPhysToLogChannelIndex_3
#else
# define CanPhysToLogChannel_0       kCanPhysToLogChannelIndex_0
# define CanPhysToLogChannelIndex_0  kCanPhysToLogChannelIndex_0,
# define CanPhysToLogChannel_1       kCanPhysToLogChannelIndex_1
# define CanPhysToLogChannelIndex_1  kCanPhysToLogChannelIndex_1,
# define CanPhysToLogChannel_2       kCanPhysToLogChannelIndex_2
# define CanPhysToLogChannelIndex_2  kCanPhysToLogChannelIndex_2,
# define CanPhysToLogChannel_3       kCanPhysToLogChannelIndex_3
# define CanPhysToLogChannelIndex_3  kCanPhysToLogChannelIndex_3,
#endif

/* ISR functions */

#if defined( C_ENABLE_INTERRUPT_SOURCE_SINGLE ) /* COV_CAN_FLEXCAN3_DERIVATIVE_INTERRUPT_SOURCE_SINGLE */
# if defined( C_ENABLE_CAN_RXTX_INTERRUPT )   || \
     defined( C_ENABLE_CAN_BUSOFF_INTERRUPT ) || \
     defined( C_ENABLE_CAN_WAKEUP_INTERRUPT )
#  if defined( kCanPhysToLogChannelIndex_0 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
/****************************************************************************
| NAME:             CanIsr_0()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_0Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_0Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanIsr_0 )
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_0Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_0Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanIsr_0( void )
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanInterrupt(Can_GetPhysToLogChannel(0)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanInterrupt(CanPhysToLogChannel_0); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanIsr */
/* CODE CATEGORY 1 END */
#  endif /* (kCanPhysToLogChannelIndex_0) */
#  if defined( kCanPhysToLogChannelIndex_1 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
/****************************************************************************
| NAME:             CanIsr_1()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_1Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_1Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanIsr_1 )
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_1Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_1Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanIsr_1( void )
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanInterrupt(Can_GetPhysToLogChannel(1)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanInterrupt(CanPhysToLogChannel_1); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanIsr */
/* CODE CATEGORY 1 END */
#  endif /* (kCanPhysToLogChannelIndex_1) */
#  if defined( kCanPhysToLogChannelIndex_2 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
/****************************************************************************
| NAME:             CanIsr_2()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_2Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_2Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanIsr_2 )
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_2Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_2Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanIsr_2( void )
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanInterrupt(Can_GetPhysToLogChannel(2)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanInterrupt(CanPhysToLogChannel_2); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanIsr */
/* CODE CATEGORY 1 END */
#  endif /* (kCanPhysToLogChannelIndex_2) */
#  if defined( kCanPhysToLogChannelIndex_3 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
/****************************************************************************
| NAME:             CanIsr_3()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_3Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_3Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanIsr_3 )
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_3Cat)/* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_3Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanIsr_3( void )
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanInterrupt(Can_GetPhysToLogChannel(3)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanInterrupt(CanPhysToLogChannel_3); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanIsr */
/* CODE CATEGORY 1 END */
#  endif /* (kCanPhysToLogChannelIndex_3) */
# endif /* C_ENABLE_CAN_RXTX_INTERRUPT || C_ENABLE_CAN_BUSOFF_INTERRUPT || C_ENABLE_CAN_WAKEUP_INTERRUPT */
#endif /* C_ENABLE_INTERRUPT_SOURCE_SINGLE */

#if defined( C_ENABLE_INTERRUPT_SOURCE_MULTIPLE ) /* COV_CAN_FLEXCAN3_DERIVATIVE_INTERRUPT_SOURCE_MULTIPLE */
# if defined( kCanPhysToLogChannelIndex_0 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
/****************************************************************************
| NAME:             CanBusOffIsr_0()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_0Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_0Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanBusOffIsr_0 ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_0Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_0Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanBusOffIsr_0( void ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanBusOffInterrupt(Can_GetPhysToLogChannel(0)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanBusOffInterrupt(CanPhysToLogChannel_0); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanBusOffIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_BUSOFF_INTERRUPT */

#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/****************************************************************************
| NAME:             CanMailboxIsr_0()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_0Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_0Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanMailboxIsr_0 ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_0Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_0Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanMailboxIsr_0( void ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanMailboxInterrupt(Can_GetPhysToLogChannel(0)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanMailboxInterrupt(CanPhysToLogChannel_0); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanMailboxIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_RXTX_INTERRUPT */
# endif /* (kCanPhysToLogChannelIndex_0) */
# if defined( kCanPhysToLogChannelIndex_1 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
/****************************************************************************
| NAME:             CanBusOffIsr_1()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_1Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_1Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanBusOffIsr_1 ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_1Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_1Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanBusOffIsr_1( void ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanBusOffInterrupt(Can_GetPhysToLogChannel(1)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanBusOffInterrupt(CanPhysToLogChannel_1); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanBusOffIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_BUSOFF_INTERRUPT */

#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/****************************************************************************
| NAME:             CanMailboxIsr_1()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_1Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_1Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanMailboxIsr_1 ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_1Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_1Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanMailboxIsr_1( void ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanMailboxInterrupt(Can_GetPhysToLogChannel(1)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanMailboxInterrupt(CanPhysToLogChannel_1); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanMailboxIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_RXTX_INTERRUPT */
# endif /* (kCanPhysToLogChannelIndex_1) */
# if defined( kCanPhysToLogChannelIndex_2 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
/****************************************************************************
| NAME:             CanBusOffIsr_2()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_2Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_2Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanBusOffIsr_2 ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_2Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_2Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanBusOffIsr_2( void ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanBusOffInterrupt(Can_GetPhysToLogChannel(2)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanBusOffInterrupt(CanPhysToLogChannel_2); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanBusOffIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_BUSOFF_INTERRUPT */

#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/****************************************************************************
| NAME:             CanMailboxIsr_2()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_2Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_2Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanMailboxIsr_2 ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_2Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_2Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanMailboxIsr_2( void ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanMailboxInterrupt(Can_GetPhysToLogChannel(2)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanMailboxInterrupt(CanPhysToLogChannel_2); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanMailboxIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_RXTX_INTERRUPT */
# endif /* (kCanPhysToLogChannelIndex_2) */
# if defined( kCanPhysToLogChannelIndex_3 ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_CAN_BUSOFF_INTERRUPT )
/****************************************************************************
| NAME:             CanBusOffIsr_3()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_3Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_3Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanBusOffIsr_3 ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_3Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_3Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanBusOffIsr_3( void ) /* COV_CAN_GENDATA_NOT_IN_ALL_CONFIG */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanBusOffInterrupt(Can_GetPhysToLogChannel(3)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanBusOffInterrupt(CanPhysToLogChannel_3); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanBusOffIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_BUSOFF_INTERRUPT */

#  if defined( C_ENABLE_CAN_RXTX_INTERRUPT )
/****************************************************************************
| NAME:             CanMailboxIsr_3()
****************************************************************************/
/* CODE CATEGORY 1 START */
#   if defined( C_ENABLE_OSEK_OS ) && defined( C_ENABLE_OSEK_OS_INTCAT2 ) /* COV_CAN_OS_USAGE */
#    if defined (osdIsrCanIsr_3Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#     if (osdIsrCanIsr_3Cat != 2u)
#      error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#     endif
#    endif
ISR( CanMailboxIsr_3 ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   else
#    if defined( C_ENABLE_OSEK_OS ) /* COV_CAN_OS_USAGE */
#     if defined (osdIsrCanIsr_3Cat) /* COV_CAN_LL_OSCAT_CONFIG_CHECK XF */
#      if (osdIsrCanIsr_3Cat != 1u)
#       error "inconsistent configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#      endif
#     endif
#    endif
void CanMailboxIsr_3( void ) /* COV_CAN_MAILBOX_INT_INDIVPOLLING */
#   endif /* C_ENABLE_OSEK_OS */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 call interrupt handler */
#   if defined( CAN_USE_PHYSTOLOG_MAPPING )
  CanMailboxInterrupt(Can_GetPhysToLogChannel(3)); /* call Interrupthandling with identity dependend logical channel */
#   else
  CanMailboxInterrupt(CanPhysToLogChannel_3); /* call Interrupthandling with logical channel */
#   endif
} /* END OF CanMailboxIsr */
/* CODE CATEGORY 1 END */
#  endif /* C_ENABLE_CAN_RXTX_INTERRUPT */
# endif /* (kCanPhysToLogChannelIndex_3) */
#endif

/* PRQA L:QAC_Can_Functions */


/* *********************************************************************** */
/*  MISRA deviations                                                       */
/* *********************************************************************** */
/* Justification for module-specific MISRA deviations:

MD_MSR_DummyStmt:
PRQA message 1338, 2983, 3112
  Reason: Dummy assignment for API parameters or variables that are never used or only used in some configurations.
  Risk: Missing functionality (parameter or variable should have been used).
  Prevention: Code inspection and test of the different variants in the component test.

MD_MSR_FctLikeMacro:
PRQA message 3453
  Reason: Function-like macros are used to fulfill the required code efficiency.
  Risk: The code is difficult to understand or may not work as expected.
  Prevention: Code inspection and test of the different variants in the component test.

MD_Can_Goto:
PRQA message 2001
  Reason: 'goto' statements are used to reduce code complexity.
  Risk: Incorrect jump.
  Prevention: Code inspection and runtime tests.

MD_Can_MultipleReturn:
PRQA message 2889
  Reason: Multiple return paths are used to reduce code complexity.
  Risk: Return a function too soon.
  Prevention: Code inspection and runtime tests.

MD_Can_ParameterName:
PRQA message 0784
  Reason: API parameter that is also used as a macro name is accepted for compatibility reasons.
  Risk: The macro can change the declaration unintentionally.
  Prevention: Relevant inconsistencies in function declarations, definitions and calls are detected by the compiler.

MD_Can_ConstValue:
PRQA message 2741, 2742, 2880, 2985, 2986, 2990, 2991, 2992, 2993, 2994, 2995, 2996
  Reason: Value is constant depending on configuration aspects or platform specific implementation. This leads to constant control expressions, unreachable code or redundant operations.
  Risk: Wrong or missing functionality.
  Prevention: Code inspection and test of the different variants in the component test.

MD_Can_EmptyFunction:
PRQA message 2987
  Reason: Function is empty depending on configuration aspects and platform specific implementation.
  Risk: Function implementation missing.
  Prevention: Code inspection and test of the different variants in the component test.

MD_Can_NoneConstParameterPointer:
PRQA message 3673
  Reason: Non-const pointer parameter is required by the internal interface or compatibility reasons but depending on the configuration or specific platform implementation the target may not always be modified.
  Risk: Read only data could be modified without intention.
  Prevention: Code inspection and test of the different variants in the component test.

MD_Can_ModuleDefine:
PRQA message 0602, 0603
  Reason: Usage of reserved identifiers with leading underscores is accepted for compatibility reasons.
  Risk: Name conflicts.
  Prevention: Compile and link of the different variants in the component and integration test.

MD_Can_RedundantInit:
PRQA message 2981
  Reason: Reduce code complexity by using an explicit variable initializer that may be always modified before being used in some configurations.
  Risk: Unintended change of value.
  Prevention: Code inspection and test of the different variants in the component test.

MD_Can_GlobalScope:
PRQA message 3218
  Reason: The usage of variables depends on configuration aspects and they may be used only once or defined globally to improve overview.
  Risk: None.
  Prevention: None.

MD_Can_ExternalScope:
PRQA message 1514, 3408, 3447, 3451, 3210
  Reason: The variable is used by other modules and can't be declared static.
  Risk: Name conflicts.
  Prevention: Compile and link of the different variants in the component and integration test.

MD_Can_GenData:
PRQA message 1533
  Reason: These constants are defined in a generated file and cannot be moved to the static source file.
  Risk: None.
  Prevention: None.

MD_Can_Union:
PRQA message 0750, 0759
  Reason: Using union type to handle different data accesses.
  Risk: Misinterpreted data.
  Prevention: Code inspection and test of the different variants in the component test.

MD_Can_Assertion:
PRQA message 2842, 2897
  Reason: Assertion leads to apparent out of bounds indexing or casting a negative value to a signed type.
  Risk: Undefined behaviour.
  Prevention: Code inspection. The assertion itself prevents the apparent anomaly.

MD_Can_PointerVoidCast:
PRQA message 0314, 0316
  Reason: API is defined with pointer to void parameter, so pointer has to be casted to or from void.
  Risk: Wrong data access or undefiend behavior for platforms where the byte alignment is not arbitrary.
  Prevention: Code inspection and test with the target compiler/platform in the component test.

MD_Can_PointerCast:
PRQA message 0310
  Reason: Different pointer type is used to access data.
  Risk: Wrong memory is accessed or alignment is incorrect.
  Prevention: Code inspection and test of different variants in the component test.

MD_Can_NoneVolatilePointerCast:
PRQA message 0312
  Reason: Cast to none volatile pointer.
  Risk: Incorrect multiple context access.
  Prevention: Code inspection checks that the value is not multiple accessed.

MD_Can_HwAccess:
PRQA message 0303
  Reason: Hardware access needs cast between a pointer to volatile object and an integral type.
  Risk: Access of unknown memory.
  Prevention: Runtime tests.

MD_Can_MixedSigns:
PRQA message 4393, 4394
  Reason: Casting from signed to unsigned types and vice versa is needed as different types are intentionally used.
  Risk: Value is changed during cast.
  Prevention: Code inspection and test of different variants in the component test.

MD_Can_IntegerCast:
PRQA message 4391, 4398, 4399, 4491
  Reason: Explicit cast to a different integer type.
  Risk: Value is changed during cast.
  Prevention: Code inspection and test of different variants in the component test.

MD_Can_CompilerAbstraction:
PRQA message 0342
  Reason: Glue operator used for compiler abstraction.
  Risk: Only K&R compiler support glue operator.
  Prevention: Compile test show whether compiler accept glue operator.

MD_Can_NoElseAfterIf:
PRQA message 2004
  Reason: No default handling needed for if-else-if here.
  Risk: Missing default handling.
  Prevention: Code inspection and test of different variants in the component test.

MD_Can_IncompleteForLoop:
PRQA message 3418
  Reason: Comma operator in for-loop header is used to get a compact code.
  Risk: Uninitialized variable.
  Prevention: Code inspection and test of different variants in the component test.

MD_Can_3305_LL_MsgObjectAccess
PRQA message 3305
  Reason: Pointer keeps either a RX message buffer object or a HW message buffer object. Both are stored as a uint8 pointer. For access the appropriate structure is used.
  Risk: Wrong object structure might be accessed
  Prevention: Code review and runtime tests

MD_Can_LL_HwAccess:
PRQA message 0306
  Reason: Hardware access needs cast between a pointer object and an integral type in case of Virtual Addressing.
  Risk: Access of unknown memory.
  Prevention: Runtime tests.
  
MD_Can_ArraySizeUnknown:
PRQA message 3684
  Reason: Arrays declared without size, because size depends on configuration and is unknown here, especially for linktime tables.
  Risk: Data access outside table.
  Prevention: Code inspection and test of the different variants in the component test.
  
MD_Can_FlexibleStructMember:
PRQA message 	1039
  Reason: Array struct member is of size one and at the end of the struct.
  Risk: None.
  Prevention: None.
*/

/* Kernbauer Version: 1.14 Konfiguration: DrvCan_Arm32Flexcan3Hll Erzeugungsgangnummer: 1 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCan_Arm32Flexcan3Hll Erzeugungsgangnummer: 1 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCan_Arm32Flexcan3Hll Erzeugungsgangnummer: 1 */

