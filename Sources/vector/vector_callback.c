#include "includes.h"
#include "Service/srv_mcan.h"
//#include "ccl_inc.h"

#define firstframe_valid_time			(3800)					//3.8 sec
#define NM_MSG_ID_START				(0x600)		
#define NM_MSG_ID_END					(0x67F)		
#define DIAG_REV_PHY						(0x74A)		
#define DIAG_REV_FUNC					(0x7DF)		

//CAN_Node_Timeout Node_Timeout;

volatile uint32 check_nm_firstframe_timer_tick = 0;

extern DEVICE r_Device;

void ApplTpRxIndication(canuint16 datLen)
{

}

void ApplTpRxErrorIndication(canuint8 errNo)
{

}

void ApplTpTxConfirmation(canuint8 state)
{

}

canuint8* ApplTpRxGetBuffer(canuint16 datLen)
{

}

canuint8 ApplTpTxErrorIndication(canuint8 errNo)
{

}


void CCL_API_CALLBACK_TYPE ApplCclSleepTrcv(void)
{

}

void VectorIsrInit(void)
{
	//INT_SYS_InstallHandler(CAN0_Wake_Up_IRQn, CanBusOffIsr_0, (isr_t *)0);
	INT_SYS_InstallHandler(CAN0_ORed_IRQn, CanBusOffIsr_0, (isr_t *)0);
	INT_SYS_InstallHandler(CAN0_ORed_0_15_MB_IRQn, CanMailboxIsr_0, (isr_t *)0);
	INT_SYS_InstallHandler(CAN0_ORed_16_31_MB_IRQn, CanMailboxIsr_0, (isr_t *)0);
	
	//INT_SYS_EnableIRQ(CAN0_Wake_Up_IRQn);
	//INT_SYS_SetPriority(CAN0_Wake_Up_IRQn, 0 );
	INT_SYS_EnableIRQ(CAN0_ORed_IRQn);
	INT_SYS_SetPriority(CAN0_ORed_IRQn, 0 );
	INT_SYS_EnableIRQ(CAN0_ORed_0_15_MB_IRQn);
	INT_SYS_SetPriority(CAN0_ORed_0_15_MB_IRQn, 0 );
	INT_SYS_EnableIRQ(CAN0_ORed_16_31_MB_IRQn);
	INT_SYS_SetPriority(CAN0_ORed_16_31_MB_IRQn, 0 );
}

void VectorIsrDeInit(void)
{
	INT_SYS_DisableIRQ(CAN0_ORed_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_0_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_16_31_MB_IRQn);
}


void vPort_C_ISRHandler(void)
{

}

//void vPort_D_ISRHandler(void)
//{
//}

void ApplInitExtInterrupt(void)
{
	#ifdef VECTOR_PASSIVE_WAKEUP
	PORTC->PCR[7]|= PORT_PCR_MUX(1)|PORT_PCR_IRQC(10);
	INT_SYS_InstallHandler(PORTC_IRQn, vPort_C_ISRHandler, (isr_t *)0);
	INT_SYS_SetPriority( PORTC_IRQn, 2 );

	DPRINTF(DBG_INFO,"%s\n\r",__FUNCTION__);
	#endif
	//PORTD->PCR[6]|=
	//INT_SYS_InstallHandler(PORTD_IRQn, vPort_D_ISRHandler, (isr_t *)0);
	//INT_SYS_SetPriority( PORTD_IRQn, 2 );

}
uint8 vector_onoff=0;
void ApplClearExtInterrupt(void)
{
	#ifdef VECTOR_PASSIVE_WAKEUP
	if(vector_onoff)
	{
		PORTC->PCR[7] |= PORT_PCR_ISF(0);
		DPRINTF(DBG_INFO,"%s\n\r",__FUNCTION__);
	}
	#endif
}

void ApplDisableExtInterrupt(void)
{
//	DPRINTF(DBG_INFO,"%s\n\r",__FUNCTION__);
	#ifdef VECTOR_PASSIVE_WAKEUP
	INT_SYS_DisableIRQ(PORTC_IRQn);
	#endif
	//vector_onoff=1;
}

void ApplEnableExtInterrupt(void)
{
	#ifdef VECTOR_PASSIVE_WAKEUP
	if(vector_onoff)
	{
		MCU_CAN0_EN(OFF);
		MCU_CAN0_STB(OFF);
		INT_SYS_EnableIRQ(PORTC_IRQn);
//		DPRINTF(DBG_INFO,"%s\n\r",__FUNCTION__);
		vector_onoff = 0;
	}
	#endif
}

void ApplGetExtInterrupt(void)
{
}

uint8 ApplGet_NM_state(void)
{
	return (CanNm_NmState[0] );
}

uint8 u8BusOffNum = 0u; 
uint8 u8CANTxConfirmed = 0u; 
uint8 GetBusOffNum(void)
{
	return u8BusOffNum;
}

void ResetBusOffNum(void)
{
	u8BusOffNum = 0u;
}

void ApplCanTxConfirmation(CanTxInfoStructPtr txStruct)
{
    (void)txStruct;
}
void SetCANTxConfirmation(void)
{
	u8CANTxConfirmed = 1u;
}

uint8 GetCANTxConfirmation(void)
{
	return u8CANTxConfirmed;
}

void ApplCbdWrpBusOffNotification(void)
{
}

void ResetCANTxConfirmation(void)
{
	u8CANTxConfirmed = 0u;
}

void ApplCclPreCCLTask(void){}

void ApplCclCanTxObjStart( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txHwObject ){}

void CCL_API_CALLBACK_TYPE ApplCclBusOffEnd(void)
{
	uint8 u8Value = 0;
	u8Value = 0;
}

void CCL_API_CALLBACK_TYPE ApplCclBusOffStart(void)
{
	/* Condition is right, allow to check busoff DTC */
	if(GetDTCStartProcessState() == 1u)
	{
		if(u8BusOffNum < 3u)
		{
			u8BusOffNum += 1;
		}
		else
		{
			/* Do nothing */
		}
	}
	else
	{
		/* Do nothing */
	}
}

void CCL_API_CALLBACK_TYPE ApplCclInit(void){}

FUNC( void, CANNM_APPL_CODE ) Appl_CanNm_TxConfirmation( CONST( NetworkHandleType, AUTOMATIC ) nmChannelHandle )
{
	if (r_Device.r_System->accign_state.cnt > 50 && r_Device.r_System->accign_state.state == OFF && ApplGet_NM_state() == NM_STATE_NORMAL_OPERATION)
	{
		r_Device.r_System->ccl_active = OFF;
	}
}

void ApplCclCbdWrpStateChange(NetworkHandleType nmChannelHandle, Nm_StateType nmPreviousState, Nm_StateType nmCurrentState)
{
	//wx20220801
	uint8 userData[6] = {0};

	userData[1] = r_Device.r_System->usrdata1;

	if (nmCurrentState == NM_STATE_REPEAT_MESSAGE && nmPreviousState == NM_STATE_BUS_SLEEP)
	{
		userData[2] = 0x01;
	}
	else if(nmCurrentState == NM_STATE_REPEAT_MESSAGE && nmPreviousState == NM_STATE_PREPARE_BUS_SLEEP)
	{
		userData[2] = 0x02;
	}
	else if(nmCurrentState == NM_STATE_REPEAT_MESSAGE && nmPreviousState == NM_STATE_NORMAL_OPERATION)
	{
		userData[2] = 0x03;
	}
	else if(nmCurrentState == NM_STATE_REPEAT_MESSAGE && nmPreviousState == NM_STATE_READY_SLEEP)
	{
		userData[2] = 0x04;
	}
	else if(nmCurrentState == NM_STATE_NORMAL_OPERATION && nmPreviousState == NM_STATE_REPEAT_MESSAGE)
	{
		userData[2] = 0x05;
	}
	else if(nmCurrentState == NM_STATE_NORMAL_OPERATION && nmPreviousState == NM_STATE_READY_SLEEP)
	{
		userData[2] = 0x06;
	}

	userData[2] |= 0x08;
	Nm_SetUserData(nmChannelHandle, userData);
}

void CCL_API_CALLBACK_TYPE ApplCclComStart(void)
{

}


void CCL_API_CALLBACK_TYPE ApplCclComStop(void){}

void CCL_API_CALLBACK_TYPE ApplCclComWait(void){}

void CCL_API_CALLBACK_TYPE ApplCclComResume(void){}

void CCL_API_CALLBACK_TYPE ApplCclInitTrcv(void){}

void CCL_API_CALLBACK_TYPE ApplCclWakeUpTrcv(void){}

void CCL_API_CALLBACK_TYPE ApplCclStandbyTrcv(void){}

void ApplCclCanTxObjConfirmed( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txHwObject ){}

//vuint8 CCL_API_CALLBACK_TYPE ApplCclCanStandby(CCL_CHANNEL_CCLTYPE_FIRST vuint8 sleepResult)
vuint8 CCL_API_CALLBACK_TYPE ApplCclCanStandby(vuint8 sleepResult)
{
  return 0;
}

void CCL_API_CALLBACK_TYPE BusWakeUp(void){}


void CCL_API_CALLBACK_TYPE FirstComRequset(void)
{
	//DPRINTF(DBG_INFO,"%s\n\r",__FUNCTION__);
}


void CCL_API_CALLBACK_TYPE BusSleep(void)
{
	//DPRINTF(DBG_INFO,"%s\n\r",__FUNCTION__);
}


void ApplCclCanInit( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txHwObjectFirstUsed, CanObjectHandle txHwObjectFirstUnused )
{
}


void ApplIlFatalError(vuint8 errorNumber)
{

}



void ApplALIVE_MFS_2SigConfirmation(void)
{

	static uint8_t MFS_ALIVE_MFS_2 = 0;
	static uint8_t MFS_ALIVE_MFS_2_cnt = 0;

	MFS_ALIVE_MFS_2_cnt++;


}

void ApplRRM13_MessageCounterSigConfirmation(void)
{
	static uint8_t RRM_13_MSGCNT = 0;
	static uint8_t RRM_13_MSGCNT_CNT = 0;

	RRM_13_MSGCNT_CNT++;

	if(RRM_13_MSGCNT_CNT == 3)
	{

	}
}

void ApplRRM15_MessageCounterSigConfirmation(void)
{
	static uint8_t RRM_15_MSGCNT = 0;
	static uint8_t RRM_15_MSGCNT_CNT = 0;

	RRM_15_MSGCNT_CNT++;

	if(RRM_15_MSGCNT_CNT == 3)
	{

	}
}


void RRM_1_Confirmation(CanTransmitHandle txObject)
{
	static uint8 RRM_1_Cnt = 0;


}

void RRM_5_Confirmation(CanTransmitHandle txObject)
{
	static uint8 RRM_5_Cnt = 0;

	RRM_5_Cnt++;




}

void RRM_8_Confirmation(CanTransmitHandle txObject)
{
	static uint8 RRM_8_Cnt = 0;

	RRM_8_Cnt++;


}

void RRM_12_Confirmation(CanTransmitHandle txObject)
{

}

void RRM_13_Confirmation(CanTransmitHandle txObject)
{

}


void RRM_DMS_1_Confirmation(CanTransmitHandle txObject)
{

}

void RRM_Set_HUD_Confirmation(CanTransmitHandle txObject)
{

}

void RRM_MFS_2_Confirmation(CanTransmitHandle txObject)
{
	static uint8 MFS_2_Cnt = 0;


}

vuint8 RRM_Set_ESC_Pretransmit(CanTxInfoStruct ctis)
{

}

vuint8 RRM_MFS_2_Pretransmit(CanTxInfoStruct ctis)
{

}

//Node Active










//reserved
void ApplCGW_EMS_1_GMsgTimeout(void)
{

}

void ApplCGW_FCM_2MsgTimeout(void)
{

}


void ApplCGW_CEM_BCM_3MsgTimeout(void)
{

}

void ApplCGW_DHM_1MsgTimeout(void)
{

}
void ApplCGW_CEM_SRFMsgTimeout(void)
{

}
void ApplCGW_CEM_BCM_7MsgTimeout(void)
{

}
void ApplCGW_CEM_BCM_6MsgTimeout(void)
{

}
void ApplCGW_CEM_BCM_4MsgTimeout(void)
{

}
void ApplHUD_1MsgTimeout(void)
{

}
void ApplCEM_RLHSMsgTimeout(void)
{

}
void ApplCEM_FRAGMsgTimeout(void)
{

}
void ApplCGW_RBCM_1MsgTimeout(void)
{

}
void ApplCGW_PM25_1MsgTimeout(void)
{

}
void ApplCGW_CEM_IPM_3MsgTimeout(void)
{

}
void ApplCGW_ICM_4MsgTimeout(void)
{

}
void ApplTBOX_3MsgTimeout(void)
{

}
void ApplCGW_CEM_BCM_5MsgTimeout(void)
{

}
void ApplCGW_CEM_IPMMsgTimeout(void)
{

}
void ApplAVM_APA_2_CAN3ToCAN4MsgTimeout(void)
{

}
void ApplCGW_ICM_7MsgTimeout(void)
{

}
void ApplCGW_ABS_ESP_5MsgTimeout(void)
{

}
void ApplCGW_ICM_6MsgTimeout(void)
{

}

void ApplCGW_CEM_ALM_1MsgTimeout(void){}
void ApplCGW_CEM_PEPSMsgTimeout(void){}
void ApplCGW_APA_4MsgTimeout(void){}
void ApplCGW_YAS_2MsgTimeout(void){}
void ApplWPC_1MsgTimeout(void){}
void ApplCGW_ICM_2MsgTimeout(void){}
void ApplCGW_SRR_1MsgTimeout(void){}
void ApplCGW_CEM_RADAR_1MsgTimeout(void){}
void ApplCGW_PLG_1MsgTimeout(void){}
void ApplCGW_ICM_1MsgTimeout(void){}
void ApplAVM_APA_3_CAN3ToCAN4MsgTimeout(void){}
void ApplCGW_WSD_1MsgTimeout(void){}
void ApplCGW_CEM_AQSMsgTimeout(void){}
void ApplCGW_CEM_BCM_13MsgTimeout(void){}
void ApplCGW_CEM_BCM_2MsgTimeout(void){}

void ApplCGW_CEM_BCM_1MsgTimeout(void){}
void ApplCGW_EPS_1MsgTimeout(void){}
void ApplCGW_SAM_1_GMsgTimeout(void){}
void ApplMFS_3MsgTimeout(void){}
void ApplCGW_ABM_1_GMsgTimeout(void){}
void ApplCGW_ABS_ESP_3MsgTimeout(void){}

void ApplCGW_ABS_ESP_1MsgTimeout(void){}

void ApplCGW_EMS_4MsgTimeout(void){}
/*
vuint8 C_CALLBACK_2 ApplCanMsgReceived(CanRxInfoStructPtr rxStruct)
{
	vuint32 vHandle;

	//i think under this function is changed
	vHandle = CanRxActualStdId(rxStruct);

	if((NM_MSG_ID_START<=vHandle)&&(vHandle<=NM_MSG_ID_END))
	{
		DPRINTF(DBG_INFO,"NM RX\n\r");

		if(ApplGet_NM_state()==NM_STATE_READY_SLEEP)
		{

		}
	}

	return kCanNoCopyData;
}
*/
vuint8 C_CALLBACK_2 ApplCanMsgReceived(CanRxInfoStructPtr rxStruct)
{
	vuint32 vHandle;
	static int counter=0;

	//i think under this function is changed
	vHandle = CanRxActualStdId(rxStruct);

	//check NM msg
	#ifdef CHERY_VECTOR_SPEC1
	if((NM_MSG_ID_START<=vHandle)&&(vHandle<=NM_MSG_ID_END))
	{
		if(ApplGet_NM_state()==NM_STATE_BUS_SLEEP)
		{
			if(nm_firstframe_cnt<1)
			{
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt++;
				return kCanNoCopyData;
			}
			else if((nm_firstframe_cnt ==1)&&((GetElapsedTime(check_nm_firstframe_timer_tick)<firstframe_valid_time)))
			{
				CclCanWakeUpInt();
				diag_wakeup_flag =ON;
				DPRINTF(DBG_INFO,"%s MN_MSG %d %d\n\r",__FUNCTION__ ,nm_firstframe_cnt,ApplGet_NM_state());
				system_boot_tick = GetTickCount(); 			//need to check nm state
				TX_RRM_NM_USR_DATA1(NETWORK_WAKEUP);		//default value
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt=0;
				return kCanCopyData;
			}
			else if(firstframe_valid_time<=GetElapsedTime(check_nm_firstframe_timer_tick))
			{
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt=0;
				return kCanNoCopyData;
			}
		}
		else if(ApplGet_NM_state()==NM_STATE_PREPARE_BUS_SLEEP)
		{
			CclCanWakeUpInt();
			check_nm_firstframe_timer_tick= GetTickCount();
			nm_firstframe_cnt=0;
			DPRINTF(DBG_INFO,"%s MN_MSG NM_STATE_PREPARE_BUS_SLEEP %d %d\n\r",__FUNCTION__ ,nm_firstframe_cnt,ApplGet_NM_state());
			system_boot_tick = GetTickCount();// diagnostic message is 4sec
		}
	}
	#else
	if((NM_MSG_ID_START<=vHandle)&&(vHandle<=NM_MSG_ID_END))
	{

		if(ApplGet_NM_state()==NM_STATE_BUS_SLEEP)
		{
			DPRINTF(DBG_INFO,"%s MN_MSG %d\n\r",__FUNCTION__ ,ApplGet_NM_state());
			if(nm_firstframe_cnt<1)
			{
				//diag_wakeup_flag =ON;
				//nm_firstframe_cnt=0;
				//r_Device.r_System->usrdata1 = NETWORK_WAKEUP;
				//TX_RRM_NM_USR_DATA1(NETWORK_WAKEUP);		//default value
				check_nm_firstframe_timer_tick= GetTickCount();
			}
		}
		else if(ApplGet_NM_state()==NM_STATE_PREPARE_BUS_SLEEP)
		{
			CclCanWakeUpInt();
			check_nm_firstframe_timer_tick= GetTickCount();
			//nm_firstframe_cnt=0;
			DPRINTF(DBG_INFO,"%s MN_MSG NM_STATE_PREPARE_BUS_SLEEP %d\n\r",__FUNCTION__ ,ApplGet_NM_state());
			//system_boot_tick = GetTickCount();// diagnostic message is 4sec
		}

	}
	#endif

	#ifdef CHERY_VECTOR_SPEC2
	if((vHandle==DIAG_REV_PHY)||(vHandle==DIAG_REV_FUNC))
	{
		check_diag_timer_tick= GetTickCount();
		system_boot_tick = GetTickCount();// diagnostic message is 4sec
		SrvMcanSetChangeFlg(DIAG_RX_74A_IDX);
		if((ApplGet_NM_state()==NM_STATE_BUS_SLEEP))
		{
			DPRINTF(DBG_INFO,"%s DIAG_MSG %d %d\n\r",__FUNCTION__,nm_firstframe_cnt,ApplGet_NM_state());
			if(nm_firstframe_cnt<1)
			{
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt++;
			}
			else if((nm_firstframe_cnt ==1)&&((GetElapsedTime(check_nm_firstframe_timer_tick)<firstframe_valid_time)))
			{
				r_Device.r_System->usrdata1  = ECUSPEC_AWAKE|DIGNOSTIC_AWAKE|ECUSPEC_WAKEUP;
				TX_RRM_NM_USR_DATA1(ECUSPEC_AWAKE|DIGNOSTIC_AWAKE|ECUSPEC_WAKEUP);		//default value  diag message wakeup
				diag_wakeup_flag =ON;
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt=0;
			}
			else if(firstframe_valid_time<=GetElapsedTime(check_nm_firstframe_timer_tick))
			{
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt=0;
			}
		}
		else if(ApplGet_NM_state()==NM_STATE_PREPARE_BUS_SLEEP)
		{
			diag_wakeup_flag =ON;
			check_nm_firstframe_timer_tick= GetTickCount();
			nm_firstframe_cnt=0;
		}
	}
	#else
	if((vHandle==DIAG_REV_PHY)||(vHandle==DIAG_REV_FUNC))
	{
#if 0
		//check_diag_timer_tick= GetTickCount();
		system_boot_tick = GetTickCount();// diagnostic message is 4sec
		SrvMcanSetChangeFlg(DIAG_RX_74A_IDX);
		if((ApplGet_NM_state()==NM_STATE_BUS_SLEEP))
		{
			DPRINTF(DBG_INFO,"%s DIAG_MSG %d %d\n\r",__FUNCTION__,nm_firstframe_cnt,ApplGet_NM_state());
			if(nm_firstframe_cnt<1)
	{
				//r_Device.r_System->usrdata1  = ECUSPEC_AWAKE|DIGNOSTIC_AWAKE|ECUSPEC_WAKEUP;
				TX_RRM_NM_USR_DATA1(ECUSPEC_AWAKE|DIGNOSTIC_AWAKE|ECUSPEC_WAKEUP);		//default value  diag message wakeup
				diag_wakeup_flag =ON;
				check_nm_firstframe_timer_tick= GetTickCount();
				nm_firstframe_cnt=0;
			}
	}
		else if(ApplGet_NM_state()==NM_STATE_PREPARE_BUS_SLEEP)
	{
			diag_wakeup_flag =ON;
			check_nm_firstframe_timer_tick= GetTickCount();
			nm_firstframe_cnt=0;
		}
#endif
	}
	#endif
	return kCanCopyData;
}
