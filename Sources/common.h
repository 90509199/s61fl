/*
 * common.h
 *
 *  Created on: 2018. 5. 15.
 *      Author: Digen
 */

#ifndef COMMON_H_
#define COMMON_H_

#ifdef PWM_CHANGE_FREQ
#define LCD_BL_STEP1			(99)
#define LCD_BL_STEP2			(97)
#define LCD_BL_STEP3			(95)
#define LCD_BL_STEP4			(93)	
#define LCD_BL_STEP5			(90)	
#define LCD_BL_STEP6			(70)	
#define LCD_BL_STEP7			(60)	
#define LCD_BL_STEP8			(55)	
#define LCD_BL_STEP9			(3)
#define LCD_BL_STEP10		(1)	
#define LCD_BL_DEFAULT		(LCD_BL_STEP6)	
#else
//JANG_211203
#if 1
#define LCD_BL_STEP1			(95)
#define LCD_BL_STEP2			(85)
#define LCD_BL_STEP3			(75)
#define LCD_BL_STEP4			(65)	
#define LCD_BL_STEP5			(55)	
#define LCD_BL_STEP6			(45)	
#define LCD_BL_STEP7			(35)	
#define LCD_BL_STEP8			(25)	
#define LCD_BL_STEP9			(15)
#define LCD_BL_STEP10		(5)	
#define LCD_BL_DEFAULT		(LCD_BL_STEP5)	
#else
#define LCD_BL_STEP1			(99)
#define LCD_BL_STEP2			(90)
#define LCD_BL_STEP3			(80)
#define LCD_BL_STEP4			(70)	
#define LCD_BL_STEP5			(60)	
#define LCD_BL_STEP6			(50)	
#define LCD_BL_STEP7			(40)	
#define LCD_BL_STEP8			(30)	
#define LCD_BL_STEP9			(20)
#define LCD_BL_STEP10		(10)	
#define LCD_BL_DEFAULT		(LCD_BL_STEP6)	
#endif
#endif

uint8 DriverDoorOpen(void);
uint8 KeyFortification(void);
uint8 R_Gear_Operate(void);
uint8 Set_LCD_Brightness(uint8 data);

#endif /* COMMON_H_ */
