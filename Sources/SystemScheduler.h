/*
 * SystemScheduler.h
 *
 *  Created on: 2020. 3. 5.
 *      Author: Digen
 */

#ifndef SYSTEMSCHEDULER_H_
#define SYSTEMSCHEDULER_H_
/* ABS */

/* SRV */

#include "Service/std_timer.h"

/* NM */

/**************** Initialize ********************/
#define SRV_INIT()					StdTimerInit();\
									SrvCanTpInit();\
									DiagInit();\
									DtcMgrInit();\
									MntFltInit()



/**************** Loop Task *********************/
#define SRV_LOOP_MGR()			SrvMcanHsMgr();\
								SrvCanTpMgr();\
								DiagMgr();\
								MntFltMgr()


#define APP_LOOP_MGR()

/********* Interface******************************/
#define SYS_INIT()					SRV_INIT()


#define SYS_LOOP_MGR()			SRV_LOOP_MGR()


#endif /* SYSTEMSCHEDULER_H_ */
