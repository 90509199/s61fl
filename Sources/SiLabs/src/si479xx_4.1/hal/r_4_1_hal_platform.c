/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
/************************************************/
/*** Copy from hal_platform_evb.c(hal/si_evb) ***/
/************************************************/

#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210426 SGsoft */
#define _r_4_1_hal_platform_h_global_
	
#include "main.h"

#if 0
#include "platform_selector.h"

//#if defined(PLATFORM_EVB)
#include "si_evb/hal_platform_evb.h"
#include "si_evb/external/evb_cypress_lib/Si479xxapi.h"
#include "platform_options.h"
#include "core/debug_macro.h"
#include "core/c_util.h"
#endif	/*0*/

#include <stdlib.h>

#if 0
// Using common definitions for mock file storage
#include "fake_storage.c"


static bool s_open = false;
static bool s_has_eeprom = false;
// Local copy of daughtercard's EEPROM data
static eeprom_common_config s_dc_eeprom;
#endif	/*0*/


RETURN_CODE convert_errcode(return_codes driver_ret)
{
    switch (driver_ret) {
    case success:
        return SUCCESS;
    case invalid_input:
        return INVALID_INPUT;
    case invalid_mode:
        return INVALID_MODE;
    case timeout:
        return TIMEOUT;
    case command_error:
        return COMMAND_ERROR;
    case unsupported_function:
        return UNSUPPORTED_FUNCTION;
    case firmware_image_invalid:
        return FIRMWARE_IMAGE_INVALID;

    case activation_required:
    case usb_speed_too_low:
    case bbm_error:
    case unknown_device:
    case mcu_error:
        return HAL_ERROR;

    default:
        D_PRINTF("UNRECOGNIZED SI479xxApi.h RETURN CODE (0x%x)!\n", driver_ret);
        return HAL_ERROR;
    }
}

static RETURN_CODE open_first_board()
{
	#if 1
	return SUCCESS;
	#else
    if (s_open) { 
        return SUCCESS;
    } else {
        return_codes usbret = usbret = SI479xx_evbOpen(-1);
        usbret |= SI479xx_evbIOdefaults();
        usbret |= SI479xx_daughtercardConfig_loadFromEEPROM();

        uint8_t present = 0;
        // Load some EEPROM information if using EVB
        usbret |= SI479xx_deviceInfo_readFromEEPROM(Daughtercard, &present, (UINT8*)&s_dc_eeprom);
        if (usbret != success) {
            D_PRINTF("ERROR: failed to open first EVB. Close any other connected programs first.");
            return convert_errcode(usbret);
        }

        s_has_eeprom = TO_BOOL(present);
        s_open = true;
        return SUCCESS;
    }
	#endif	/*1*/
}

RETURN_CODE hal_readConfig(struct hal_stored_cfg* cfg)
{
	#if 0
    RETURN_CODE r = open_first_board();
    if (r != SUCCESS) {
        return r;
    }

    if (s_has_eeprom) {
        // Daughtercards are specified by a board revision
        if (s_dc_eeprom.boardTypeMajor == 5 && s_dc_eeprom.boardTypeMinor == 18)
            D_ASSERT_MSG(false, "3T2D no longer supported by sample code!");
        else if (s_dc_eeprom.boardTypeMajor == 7 && s_dc_eeprom.boardTypeMinor == 5)
            cfg->connected_board = DC_2DT1F;
        else if (s_dc_eeprom.boardTypeMajor == 7 && s_dc_eeprom.boardTypeMinor == 6)
            cfg->connected_board = DC_1DT1T1F;
        else if (s_dc_eeprom.boardTypeMajor == 5 && s_dc_eeprom.boardTypeMinor == 25)
            cfg->connected_board = DC_1T;
        else if (s_dc_eeprom.boardTypeMajor == 5 && s_dc_eeprom.boardTypeMinor == 27)
            cfg->connected_board = DC_1T1D_84;
        else if (s_dc_eeprom.boardTypeMajor == 5 && s_dc_eeprom.boardTypeMinor == 28)
            cfg->connected_board = DC_1T1D_48;
        else
            return HAL_ERROR;
    }
	#endif	/*0*/
    return SUCCESS;
}

RETURN_CODE hal_initHardware(uint8_t len, const struct hal_hw_cfg* chip_cfgs)
{
#if 1
	return SUCCESS;
#else
    //Begin EVB specific initialization code
    RETURN_CODE r = SUCCESS;
    return_codes usbret = success;

    r = open_first_board();
    if (r != SUCCESS) {
        return r;
    }

#if 0
    // For DLL debug purposes.
    uint8_t major, minor;
    usbret |= SI479xx_evbGetDllVersion(&major, &minor);
    D_PRINTF("DLL version = %d.%d\n", major, minor);
#endif

    // Does not try to match clkrate, instead breaks into SLOW or FAST modes.
    if (OPTION_HAL_SPI_CLKRATE_HZ < (20*1E6)) {
        usbret |= SI479xx_evbSetBusMode(SlowSPI);
    } else {
        // Limit the max SPI rate so that the rate does not have to be changed when switching between tuner + demod ICs;
        // This results in much faster operation if there are multiple chips.
        uint8_t spirate_index;
        return_codes tmp = SI479xx_GetRecommendedFastSPIRate(&spirate_index);
        if (tmp == success) {
            usbret |= SI479xx_FastSPIRate(spirate_index);
            usbret |= SI479xx_ForceFastSPIRate(true);
        }
        usbret |= tmp;
    }

    return convert_errcode(usbret);
#endif	/*1*/
}

RETURN_CODE hal_resetIC(uint8_t icNum)
{
    return_codes usbret = success;									/* 210429 SGsoft */

	
    usbret = SI479xx_reset(icNum, 2);
	
    return convert_errcode(usbret);
	
}


RETURN_CODE hal_audioEnable(void)
{
    // EVB does not have hardware for this.
    D_PRINTF("audioEnable() not available for EVB.\n");
    return SUCCESS;
}

RETURN_CODE hal_audioDisable(void)
{
    // EVB does not have hardware for this.
    D_PRINTF("audioDisable() not available for EVB.\n");
    return SUCCESS;
}

RETURN_CODE hal_powerDownHardware(void)
{
#if 1
	return SUCCESS;
#else
    return_codes usbret = success;

    // Put the chips in reset state.
    //  Not doing this just leaves the radio running.
    // usbret |= SI479xx_reset(0xFF,0);

    // close the driver connection to the EVB
    usbret |= SI479xx_DetachFromEVB();
    usbret |= SI479xx_evbClose();
    s_open = false;
    return convert_errcode(usbret);
#endif	/*1*/
}

RETURN_CODE hal_writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer)
{
    return convert_errcode(SI479xx_writeCommand(icNum, length, buffer));
}

RETURN_CODE hal_readReply(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer)
{
    return convert_errcode(SI479xx_readReply(icNum, length, buffer));
}

RETURN_CODE hal_waitForInterrupt(dc_config_chip_select icNum, uint32_t timeout_ms)
{
    // EVB does not support interrupts
    return UNSUPPORTED_FUNCTION;
}

//#endif	/*PLATFORM_EVB*/
#endif	/*SI479XX_FIRM_REL_4_1*/
