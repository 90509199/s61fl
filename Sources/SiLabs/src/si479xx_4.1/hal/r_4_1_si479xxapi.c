/***************************************************************************************
                  Silicon Laboratories Broadcast Automotive Tuner + Demod SDK

   EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
     THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
     TO THIS SOURCE FILE.
   IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
     PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.

   Hardware Abstraction Layer for EVB functions - to be replaced when porting ot a different hardware platform
   FILE: HAL.c
   Supported IC : Si479xx + Si461x
   Date: September 21, 2015
  (C) Copyright 2015, Silicon Laboratories, Inc. All rights reserved.
****************************************************************************************/

#include "sg_defines.h"							

#if defined(SI479XX_FIRM_REL_4_1)		/* 210427 SGsoft */	
#define _r_4_1_si479xx_api_h_global_

#include "main.h"

#include <string.h>						/* 190128 SGsoft */	
#include <stdlib.h>
#include "includes.h" //JANG_RADIO_PORTING

///#include "hal_platform.h"
///#include "si479xxapi.h"


#if defined(OPTION_BUS_SPI)
void pTUN_SSB_CHIP0(U8 mode)															/* 180601 SGsoft */	
{
 //JANG_RADIO_PORTING
	#if 0
	if (mode){
		LL_GPIO_SetPinMode(TUN_SSB_GPIO_Port, TUN_SSB_Pin, LL_GPIO_MODE_INPUT);
	}else{
		LL_GPIO_SetPinMode(TUN_SSB_GPIO_Port, TUN_SSB_Pin, LL_GPIO_MODE_OUTPUT);
		LL_GPIO_ResetOutputPin(TUN_SSB_GPIO_Port, TUN_SSB_Pin);
	}
	#endif
}


void pTUN_SSB_CHIP1(U8 mode)									/* 200204 SGsoft */												
{
	;
  	
}


void pTUN_SSB_CHIP3(U8 mode)									/* 200204 SGsoft */					
{
	;
	
}


void pTUN_SSB(dc_config_chip_select icNum, U8 mode)															
{
	switch(icNum)
	{
		case IC1:
			pTUN_SSB_CHIP1(mode);
			break;

		case IC3:
			pTUN_SSB_CHIP3(mode);
			break;

		case IC0:
		default:
			pTUN_SSB_CHIP0(mode);
			break;
	}
  	
}
#endif	/*OPTION_BUS_SPI*/


int SI479xx_reset(dc_config_chip_select icBitfield, SI479xx_reset_mode mode)				/* 200109 SGsoft */
{	
#if 1		/* 210630 SGsoft */	
	return success;
#else
	if (mode == group_reset){
		Delay10Us(3);
		if (icBitfield & IC0){ pTRST(PORT_LOW); }
		#if 0		/* 210512 SGsoft */	
		if (icBitfield & IC1){ pTRST_CHIP1(PORT_LOW); }
		if (icBitfield & IC3){ pTRST_CHIP3(PORT_LOW); }
		#endif	/*0*/
		Delay10Us(33);
		if (icBitfield & IC0){ pTRST(PORT_HIGH); }
		#if 0		/* 210512 SGsoft */	
		if (icBitfield & IC1){ pTRST_CHIP1(PORT_HIGH); }
		if (icBitfield & IC3){ pTRST_CHIP3(PORT_HIGH); }
		#endif	/*0*/
		Delay10Us(3);

		return success;
	}
	
	if (icBitfield & IC0)
	{
		switch(mode)
		{
			case line_low:
				pTRST(PORT_LOW);
				break;

			case line_high:
				pTRST(PORT_HIGH);
				break;

			case individual_reset:
				Delay10Us(3);
				pTRST(PORT_LOW);
				Delay10Us(33);
				pTRST(PORT_HIGH);
				Delay10Us(3);
				break;

			default:
				break;
		}
	}

	#if 0		/* 210512 SGsoft */	
	if (icBitfield & IC1)
	{
		switch(mode)
		{
			case line_low:
				pTRST_CHIP1(PORT_LOW);
				break;

			case line_high:
				pTRST_CHIP1(PORT_HIGH);
				break;

			case individual_reset:
				Delay10Us(3);
				pTRST_CHIP1(PORT_LOW);
				Delay10Us(33);
				pTRST_CHIP1(PORT_HIGH);
				Delay10Us(3);
				break;

			default:
				break;
		}
	}

	if (icBitfield & IC3)
	{
		switch(mode)
		{
			case line_low:
				pTRST_CHIP3(PORT_LOW);
				break;

			case line_high:
				pTRST_CHIP3(PORT_HIGH);
				break;

			case individual_reset:
				Delay10Us(3);
				pTRST_CHIP3(PORT_LOW);
				Delay10Us(33);
				pTRST_CHIP3(PORT_HIGH);
				Delay10Us(3);
				break;

			default:
				break;
		}
	}
	#endif	/*0*/

	return success;
#endif	/*1*/
	
}


return_codes SI479xx_readReply (dc_config_chip_select icNum, UINT16 length, UINT8 *buffer)		/* 190118 SGsoft */
{
	U8 cmd = 0x00/*READ_REPLY*/;
	Tuner_CS_EN(OFF);

#if defined(OPTION_BUS_I2C)		/* 210510 SGsoft */	
	U8 result;							

	result = AgI2CTxData(TUN_I2C, OPTION_BUS_I2C__IC0_ADDR, NO_SUB_ADDR, &cmd, 1);
	result = AgI2CRxData(TUN_I2C, OPTION_BUS_I2C__IC0_ADDR, NO_SUB_ADDR, buffer, length);
#else
#if 1
	U8 rx_buf[256]={0,};
//JANG PORTING
	LPSPI_DRV_MasterTransferBlocking(LPSPICOM3,0,&rx_buf[0],length+1,1000);
//	LPSPI_DRV_MasterTransferBlocking(LPSPICOM3,0,&rx_buf[0],length+2,1000);

	memcpy(&buffer[0],&rx_buf[1],length);
	#if 0			//YOON_220418 : Tuner AST Data 
	{
		uint8 i=0;
		DPRINTF(DBG_INFO,"%s : ",__FUNCTION__);
		for(i=0;i<length;i++)
			DPRINTF(DBG_INFO,"%x  ", buffer[i]);	
		DPRINTF(DBG_INFO,"  task : %d  proc: %d seq:%d\n\r",stTuner.task ,stTuner.prc,stTuner.seq);
	}
	#endif
#else

	U8 dummy = 0, txallowed;								/* 190613 SGsoft */	
	U16 TxXferCount, RxXferCount, i;


	LL_SPI_ClearFlag_OVR(SPI1);
	
	pTUN_SSB(icNum, PORT_LOW);								/* 210427 SGsoft */
	
	TxXferCount = 1;
	RxXferCount = TxXferCount;
	i = 0;
	txallowed = 1;
	while (TxXferCount || RxXferCount)
	{
		if (txallowed && LL_SPI_IsActiveFlag_TXE(SPI1)){
			LL_SPI_TransmitData8(SPI1, cmd);
			TxXferCount--;
			txallowed = 0;
		}

		if (RxXferCount && LL_SPI_IsActiveFlag_RXNE(SPI1)){
			dummy = LL_SPI_ReceiveData8(SPI1);
			i++;
			RxXferCount--;
			txallowed = 1;
		}
	}

	RxXferCount = length;
	TxXferCount = RxXferCount;
	i = 0;
	txallowed = 1;
	while (TxXferCount || RxXferCount){
		if (txallowed && TxXferCount && LL_SPI_IsActiveFlag_TXE(SPI1)){
			LL_SPI_TransmitData8(SPI1, dummy);
			TxXferCount--;
			txallowed = 0;
		}
			
		if (RxXferCount && LL_SPI_IsActiveFlag_RXNE(SPI1)){
			buffer[i] = LL_SPI_ReceiveData8(SPI1);
			i++;
			RxXferCount--;
			txallowed = 1;
		}
	}
		
	pTUN_SSB(icNum, PORT_HIGH);									/* 210427 SGsoft */
	#endif
#endif	/*OPTION_BUS_I2C*/

	#if 0	/* 180604 SGsoft */
	if (length >= 4){											
		TunPwrStat = buffer[3] >> 6;
	}
	#endif	/*0*/
	Tuner_CS_EN(ON);
	return success;
	
}


return_codes SI479xx_writeCommand (dc_config_chip_select icNum, UINT16 length, UINT8 *buffer)				/* 190118 SGsoft */
{
	Tuner_CS_EN(OFF);
#if defined(OPTION_BUS_I2C)		/* 210510 SGsoft */	
	AgI2CTxData(TUN_I2C, OPTION_BUS_I2C__IC0_ADDR, NO_SUB_ADDR, buffer, length);
#else
      //JANG_RADIO_PORTING
#if 1	
	LPSPI_DRV_MasterTransferBlocking(LPSPICOM3,&buffer[0],NULL,length,1000);
#if 0
	{
		uint8 i=0;
		DPRINTF(DBG_INFO,"%s : ",__FUNCTION__);
		for(i=0;i<length;i++)
			DPRINTF(DBG_INFO,"%x  ", buffer[i]);	
		DPRINTF(DBG_INFO,"\n\r");
	}
#endif
#else
	U8 /*dummy,*/ txallowed;								/* 190613 SGsoft */	
	U16 TxXferCount, RxXferCount, i;


	LL_SPI_ClearFlag_OVR(SPI1);
	
	pTUN_SSB(icNum, PORT_LOW);								/* 210427 SGsoft */

	TxXferCount = length;
	RxXferCount = TxXferCount;
	i = 0;

	txallowed = 1;
	while (TxXferCount || RxXferCount)
	{
		if (txallowed && LL_SPI_IsActiveFlag_TXE(SPI1)){
			LL_SPI_TransmitData8(SPI1, buffer[i]);
			TxXferCount--;
			txallowed = 0;
		}

		if (RxXferCount && LL_SPI_IsActiveFlag_RXNE(SPI1)){
			/*dummy = */LL_SPI_ReceiveData8(SPI1);			/* 190613 SGsoft */	
			i++;
			RxXferCount--;
			txallowed = 1;
		}
	}
	
	pTUN_SSB(icNum, PORT_HIGH);								/* 210427 SGsoft */
#endif
#endif	/*OPTION__BUS_I2C*/
	Tuner_CS_EN(ON);
	return success;

}


uint8_t sgstatus[SI479XX_CMD_READ_STATUS_REP_LENGTH];
RETURN_CODE Si479xx_wait_for_cts_timeout(dc_config_chip_select icNum)							/* 200526 SGsoft */
{
	RETURN_CODE ret = SUCCESS;
	//uint8_t status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };

	
	ret |= wait_for_CTS_timeout(icNum, 200, ARRAY_SZ(sgstatus), sgstatus, true);

	return ret;
	
}

#endif	/*SI479XX_FIRM_REL_4_1*/
