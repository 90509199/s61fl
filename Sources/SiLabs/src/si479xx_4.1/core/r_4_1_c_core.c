/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210416 SGsoft */
#define _r_4_1_c_core_h_global_
	
#include "main.h"

///#include "core/c_core.h"
///#include "core/debug_macro.h"
///#include "common_types.h"

#include <math.h>
#include <string.h>

// Macros that generate function definitions.
FN_SWAP(int32_t);
FN_SWAP(int16_t);
FN_SWAP(int8_t);
FN_SWAP(uint8_t);

FN_IS_EQUAL(int32_t);
FN_IS_EQUAL(int8_t);
FN_IS_EQUAL(uint8_t);

FN_COMPARE(int32_t);
FN_COMPARE(int16_t);
FN_COMPARE(int8_t);
FN_COMPARE(uint8_t);


uint32_t power_uint(uint32_t base, uint32_t exp)
{
    uint32_t out = base;
    uint32_t prior = base;
    if (exp == 0) 
        return 1;

    for (uint32_t i = 0; i < exp; i++) {
        out *= base;
        if (out < prior) {
            D_PRINTF("WARN: overflow!\n");
            return uint32_t_MAX;
        }
        prior = out;
    }
    return out;
}

uint32_t power_two(uint32_t exp)
{
    if (exp >= 32) {
        D_PRINTF("WARN: overflow!\n");
        return uint32_t_MAX;
    }
    return (1 << exp);
}

void pabort(const char* s)
{
    perror(s);
    abort();
}

RETURN_CODE get_chip_select(uint8_t ic_num, dc_config_chip_select* chip_out)
{
    if (chip_out == NULL)
        return INVALID_INPUT;

    if (ic_num > 8)
        return INVALID_INPUT;

    *chip_out = (1 << ic_num);
    return SUCCESS;
}

RETURN_CODE get_ic_num(dc_config_chip_select chip, uint8_t* ic_num_out)
{
    bool found = false;
    int32_t bit = 0;

    if (ic_num_out == NULL)
        return INVALID_INPUT;

    for (bit = 0; bit < 8; bit++) {
        if ((1 << bit) & chip) {
            if (found) {
                D_PRINTF("ERROR: can only get a single ic_num from a dc_config_chip_select bitmask...\n");
                return INVALID_INPUT;
            }

            *ic_num_out = bit;
            found = true;
        }
    }

    return found ? SUCCESS : INVALID_INPUT;
}
#if 0		//YOON_211129_v2
size_t fwrite_pos(FILE* f, size_t pos, uint8_t* buf, size_t buflen)
{
    if (!f || !buf) {
//        D_PRINTF("ERROR: invalid input fwrite_pos..\n");
        return 0;
    }

    if (fseek(f, pos, SEEK_SET) != 0)
        return 0;

    return fwrite(buf, 1, buflen, f);
}

size_t fread_pos(FILE* f, size_t pos, uint8_t* buf, size_t buflen)
{
    if (!f || !buf) {
//        D_PRINTF("ERROR: invalid input fread_pos..\n");
        return 0;
    }

    if (fseek(f, pos, SEEK_SET) != 0)
        return 0;

    return fread(buf, 1, buflen, f);
}
#endif
bool is_bigendian()
{
    int n = 1;
    // mem : value
    // little endian
    // 0x0 :  1
    // 0x1 :  0
    // 0x2 :  0
    // 0x3 :  0

    // big endian
    // 0x0 :  0
    // 0x1 :  0
    // 0x2 :  0
    // 0x3 :  1

    return (*(char*)&n) == 0;
}

// modifies original buffer!
void reverse_buffer(uint32_t length, uint8_t* buf)
{
    uint8_t* start = buf;
    uint8_t* end = buf + length - 1;
    uint8_t tmp;

    for (; start < end; start += 1, end -= 1) {
        // swap MSB and LSB relative positions
        tmp = *start;
        *start = *end;
        *end = tmp;
    }
}

uint32_t get_32bit_le(const uint8_t* buffer)
{
    D_ASSERT(buffer);
    uint32_t copy = *(uint32_t*)buffer;
    if (is_bigendian()) {
        reverse_buffer(4, (uint8_t*)&copy);
    }
    return copy;
}

uint32_t get_32bit_le_offset(uint8_t* buffer, int offset)
{
    D_ASSERT(buffer);

    return get_32bit_le(&(buffer[offset]));
}

uint16_t get_16bit_le(const uint8_t* buffer)
{
    D_ASSERT(buffer);
    uint16_t copy = *(uint16_t*)buffer;
    if (is_bigendian()) {
        reverse_buffer(2, (uint8_t*)&copy);
    }
    return copy;
}

uint16_t get_16bit_le_offset(uint8_t* buffer, int offset)
{
    D_ASSERT(buffer);

    return get_16bit_le(&(buffer[offset]));
}

// Changes buffer endiannes if necessary.
void to_little_endian(uint32_t length, uint8_t* buffer)
{
    if (is_bigendian()) {
        reverse_buffer(length, buffer);
    }
}

void convert_array_to_le(uint8_t* orig, size_t orig_elem_count, uint8_t elem_size, uint8_t* le_out)
{
    D_ASSERT_MSG(le_out, "Caller must allocate output buffer!\n");

    for (size_t i = 0; i < orig_elem_count; i++) {
        size_t offset = elem_size * i;
        uint8_t* orig_elem = orig + offset;
        uint8_t* out_elem = le_out + offset;
        memcpy(out_elem, orig_elem, elem_size);
        to_little_endian(elem_size, out_elem);
    }
}

int32_t round_up(int32_t num, int32_t divisor)
{
    int32_t rem = num % divisor;
    if (rem == 0)
        return num;

    return num + (divisor - rem);
}

bool arr_contains(uint32_t len, const uint32_t* arr, uint32_t value)
{
    uint32_t i;
    for (i = 0; i < len; i++) {
        if (arr[i] == value)
            return true;
    }
    return false;
}

// Regular function definitions
void swap_ints(void* a, void* b)
{
    D_ASSERT(a != NULL);
    D_ASSERT(b != NULL);
    int32_t* pa = a;
    int32_t* pb = b;

    int32_t tmp;
    tmp = *pa;
    *pa = *pb;
    *pb = tmp;
}

bool are_ints_equal(void* a, void* b)
{
    D_ASSERT(a != NULL);
    D_ASSERT(b != NULL);

    int32_t* pa = a;
    int32_t* pb = b;

    if (a == NULL || b == NULL)
        return false;

    return (*pa) == (*pb);
}

bool array_contains(uint8_t* arr, uint32_t elem_count, uint32_t elem_size, void* test, fp_compare fn_elems_equal)
{
    for (uint32_t i = 0; i < elem_count; i++) {
        void* elem = arr + i * elem_size;
        if (fn_elems_equal(elem, test))
            return true;
    }
    return false;
}

void remove_array_elem(uint8_t i_remove, uint8_t* list, uint32_t elem_count, uint8_t elem_size)
{
    D_ASSERT(list && elem_count);

    uint8_t* start = list + i_remove * elem_size;
    uint8_t* next = start + elem_size;
    size_t remaining = (elem_count - i_remove - 1) * elem_size;
    memmove(start, next, remaining);
}

RETURN_CODE remove_duplicates(uint8_t* list_start, uint32_t* elem_count, uint8_t elem_sz, fp_is_equal fn_equal, fp_swap fn_swap)
{
    uint8_t* ip;

    uint32_t i, j, k;

    if (list_start == NULL || elem_count == NULL || fn_equal == NULL || fn_swap == NULL)
        return INVALID_INPUT;

    if (elem_sz <= 0)
        return INVALID_INPUT;

    // for each element in the list (except last)
    for (i = 0; i < (*elem_count - 1); i++) {
        ip = list_start + elem_sz * i;

        // check if another element is the same
        for (j = i + 1; j < *elem_count;) {
            uint8_t* jp = list_start + elem_sz * j;
            if (fn_equal(ip, jp)) {
                // found equivalent element, must remove from list.
                // shift the remaining elements forward one slot.
                for (k = j; k < (*elem_count - 1); k++) {
                    uint8_t* kp = list_start + elem_sz * k;
                    uint8_t* next = list_start + elem_sz * k + elem_sz;
                    fn_swap(kp, next);
                }

                (*elem_count)--;
                // not going to next elem yet.
            } else {
                j++;
            }
        }
    }

    return SUCCESS;
}

RETURN_CODE bubble_sort(uint8_t* list_start, uint32_t elem_count, uint8_t elem_sz,
    fp_compare fn_compare, fp_swap fn_swap)
{
    bool swapped;

    if (list_start == NULL || fn_compare == NULL || fn_swap == NULL)
        return INVALID_INPUT;

    if (elem_sz <= 0)
        return INVALID_INPUT;

    do {
        uint32_t i;
        swapped = false;
        for (i = 1; i < elem_count; i++) {
            uint8_t* b = list_start + elem_sz * i;
            uint8_t* a = b - elem_sz;

            if (fn_compare(a, b) > 0) {
                fn_swap(a, b);
                swapped = true;
            }
        }
    } while (swapped);

    return SUCCESS;
}

RETURN_CODE bubble_sort_weighted(uint32_t elem_count,
    uint8_t* list_start, uint8_t elem_sz, fp_swap fn_swap_elem,
    uint8_t* weights_start, uint8_t weight_sz, fp_swap fn_swap_weight,
    fp_compare fn_compare_weight)
{
    bool swapped;

    if (elem_count == 0)
        return SUCCESS;

    if (list_start == NULL || elem_sz <= 0 || fn_swap_elem == NULL)
        return INVALID_INPUT;

    if (weights_start == NULL || weight_sz <= 0 || fn_swap_weight == NULL || fn_compare_weight == NULL)
        return INVALID_INPUT;

    do {
        uint32_t i;
        swapped = false;
        for (i = 1; i < elem_count; i++) {
            uint8_t* s_b = weights_start + weight_sz * i;
            uint8_t* s_a = s_b - weight_sz;

            uint8_t* b = list_start + elem_sz * i;
            uint8_t* a = b - elem_sz;

            if (fn_compare_weight(s_a, s_b) > 0) {
                fn_swap_weight(s_a, s_b);
                fn_swap_elem(a, b);
                swapped = true;
            }
        }
    } while (swapped);

    return SUCCESS;
}
	
#endif	/*SI479XX_FIRM_REL_4_1*/