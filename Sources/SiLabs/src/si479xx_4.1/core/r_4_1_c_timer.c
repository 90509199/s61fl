/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210426 SGsoft */
#define _r_4_1_c_timer_h_global_
		
#include "main.h"

#if 0
#include "core/c_timer.h"
#include "core/debug_macro.h"
#include "osal/misc.h"
#endif	/*0*/


void ct_start(c_timer* t)
{
    D_ASSERT(t);
    t->has_overflowed = false;
    t->has_rolled_over = false;
    t->ticks_start = os_ticks_ms();
}

uint32_t ct_elapsed_ms(c_timer* t)
{
    D_ASSERT(t);

    uint32_t tick = os_ticks_ms();
    if (tick <= t->ticks_start)
        t->has_rolled_over = true;

    if (t->has_rolled_over && tick > t->ticks_start)
        t->has_overflowed = true;

    if (t->has_overflowed)
        return uint32_t_MAX;

    if (t->has_rolled_over) {
        uint32_t pre_rollover = uint32_t_MAX - (t->ticks_start);
        return pre_rollover + tick;
    } else {
        return tick - (t->ticks_start);
    }
}

uint32_t ct_elapsed_s(c_timer* t)
{
    D_ASSERT(t);

    uint32_t ms = ct_elapsed_ms(t);
    if (t->has_overflowed)
        return uint32_t_MAX;

    return (ms / 1000);
}

#endif	/*SI479XX_FIRM_REL_4_1*/
