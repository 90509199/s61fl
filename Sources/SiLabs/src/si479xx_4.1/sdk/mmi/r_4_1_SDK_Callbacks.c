/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_sdk_callbacks_h_global_
		
#include "main.h"

#if 0
#include "SDK_Callbacks.h"
#include "util/cmd_logger_cwrap.h"
#include "util/sdk_macro.h"
#include "osal/io_console.h"
#include "common_types.h"
#include "platform_options.h"
#include "sdk/receiver/Receiver_API.h"
#endif	/*0*/

#ifdef _CONSOLE
// Unnecessary for most users - used for debug in the cpp console version
// ************************************************************************
#include <Windows.h>
#include <stdio.h>
// ************************************************************************
#endif

//#define DEBUG_PRINT_UPDATES_RDS_FREQUENT
//#define DEBUG_PRINT_UPDATES_RDS
///#define DEBUG_PRINT_UPDATES					/* 210428 SGsoft */

// helper macros for repetitive ifdef blocks
#ifdef DEBUG_PRINT_UPDATES_RDS
#define CB_PRINTF_RDS(fmt)             \
    do {                                    \
        sio_printf(fmt "\n"); \
    } while (0)
#else
#if 1		/* 210428 SGsoft */
#define CB_PRINTF_RDS(fmt)
#else
#define CB_PRINTF_RDS(fmt)           \
    do {                                  \
        c_log_commentf(fmt); \
    } while (0)
#endif	/*1*/
#endif

#ifdef DEBUG_PRINT_UPDATES_RDS_FREQUENT
#define CB_PRINTF_RDS_F(fmt)           \
    do {                                    \
        sio_printf(fmt "\n"); \
    } while (0)
#else
#if 1		/* 210428 SGsoft */
#define CB_PRINTF_RDS_F(fmt) 
#else
#define CB_PRINTF_RDS_F(fmt)         \
    do {                                  \
        c_log_commentf(fmt); \
    } while (0)
#endif	/*1*/
#endif

#ifdef DEBUG_PRINT_UPDATES
#define CB_PRINTF0(fmt)                 \
    do {                                    \
        sio_printf(fmt "\n"); \
    } while (0)
#define CB_PRINTF(fmt,...)                 \
    do {                                    \
        sio_printf(fmt "\n", __VA_ARGS__); \
    } while (0)
#else
#if 1		/* 210428 SGsoft */
#define CB_PRINTF0(fmt)
#define CB_PRINTF(fmt,...)
#else
#define CB_PRINTF0(fmt)               \
    do {                                  \
        c_log_commentf(fmt); \
    } while (0)
#define CB_PRINTF(fmt,...)               \
    do {                                  \
        c_log_commentf(fmt, __VA_ARGS__); \
    } while (0)
#endif	/*1*/
#endif

bool NeverStopSeeking() { return false; }

// Default to always return false
bool (*ShouldStopFmSeek)() = NeverStopSeeking;
bool (*ShouldStopAmSeek)() = NeverStopSeeking;
bool (*ShouldStopDabSeek)() = NeverStopSeeking;

bool cb_radio_mode_updated = false;
RADIO_MODE_TYPE cb_radio_mode = RADIO_MODE_NORADIO;

void UiUpdateMode_default(RADIO_MODE_TYPE new_type)
{
    sio_printf("UI should change to radio mode: %d \n", new_type);
    if (new_type != cb_radio_mode)
        cb_radio_mode_updated = true;
    cb_radio_mode = new_type;
}

// default
void (*UiUpdateMode)(RADIO_MODE_TYPE) = UiUpdateMode_default;

void CALLBACK_Updated_Data_ctx(SDK_CALLBACKS_UPDATED_DATA_TYPE updatedType, void* cb_data)
{
    switch (updatedType) {
#ifdef OPTION_INCLUDE_MODE_AM
    case SCB_AM_SEEK_PROCESS_UPDATE:
        CB_PRINTF0("::::SCB_AM_SEEK_PROCESS_UPDATE::::::");
        if (ShouldStopAmSeek && ShouldStopAmSeek())
            SeekStop();
        break;

    case SCB_AM_SEEK_PROCESS_COMPLETE:
        CB_PRINTF0("::::SCB_AM_SEEK_PROCESS_COMPLETE::::::");
        break;
#endif // OPTION_INCLUDE_MODE_AM

    case SCB_FM_SEEK_PROCESS_UPDATE:
        CB_PRINTF0("::::SCB_FM_SEEK_PROCESS_UPDATE::::::");
        if (ShouldStopFmSeek && ShouldStopFmSeek())
            SeekStop();
        break;

    case SCB_FM_SEEK_PROCESS_COMPLETE:
        CB_PRINTF0("::::SCB_FM_SEEK_PROCESS_COMPLETE::::::");
        break;

    case SCB_UPDATED_RDS_PI:
        CB_PRINTF_RDS_F("::::SCB_UPDATED_RDS_PI::::");
        break;
    case SCB_UPDATED_RDS_PTY:
        CB_PRINTF_RDS_F("::::SCB_UPDATED_RDS_PTY::::");
        break;
    case SCB_UPDATED_RDS_RT:
        CB_PRINTF_RDS("::::SCB_UPDATED_RDS_RT::::");
        break;
    case SCB_UPDATED_RDS_PST:
        CB_PRINTF_RDS("::::SCB_UPDATED_RDS_PST::::");
        break;
    case SCB_UPDATED_RDS_TIME:
        CB_PRINTF_RDS("::::SCB_UPDATED_RDS_TIME::::");
        break;
    case SCB_UPDATED_RDS_AF:
        CB_PRINTF_RDS("::::SCB_UPDATED_RDS_AF::::");
        break;
#ifdef OPTION_INCLUDE_MODE_FMHD
    //SIS
    case SCB_UPDATED_SIS_SLOGAN:
        CB_PRINTF_RDS("::::SCB_UPDATED_SIS_SLOGAN::::");
        break;
    case SCB_UPDATED_SIS_UNIVERSAL_SHORT_NAME:
        CB_PRINTF0("::::SCB_UPDATED_SIS_UNIVERSAL_SHORT_NAME::::");
        break;
    case SCB_UPDATED_SIS_STATION_MESSAGE:
        CB_PRINTF0("::::SCB_UPDATED_SIS_STATION_MESSAGE::::");
        break;
    case SCB_UPDATED_SIS_STATION_NAME_LONG:
        CB_PRINTF0("::::SCB_UPDATED_SIS_STATION_NAME_LONG::::");
        break;
    case SCB_UPDATED_SIS_STATION_NAME_SHORT:
        CB_PRINTF0("::::SCB_UPDATED_SIS_STATION_NAME_SHORT::::");
        break;
    case SCB_UPDATED_SIS_STATION_ID:
        CB_PRINTF0("::::SCB_UPDATED_SIS_STATION_ID::::");
        break;
    case SCB_UPDATED_SIS_LOC_LAT:
        CB_PRINTF0("::::SCB_UPDATED_SIS_LOC_LAT::::");
        break;
    case SCB_UPDATED_SIS_LOC_LON:
        CB_PRINTF0("::::SCB_UPDATED_SIS_LOC_LON::::");
        break;
    case SCB_UPDATED_PSD_TITLE:
        CB_PRINTF0("::::SCB_UPDATED_PSD_TITLE::::");
        break;
    case SCB_UPDATED_PSD_ARTIST:
        CB_PRINTF0("::::SCB_UPDATED_PSD_ARTIST::::");
        break;
    case SCB_UPDATED_PSD_ALBUM:
        CB_PRINTF0("::::SCB_UPDATED_PSD_ALBUM::::");
        break;
    case SCB_UPDATED_PSD_GENRE:
        CB_PRINTF0("::::SCB_UPDATED_PSD_GENRE::::");
        break;
    case SCB_UPDATED_HD_ALERT:
        CB_PRINTF0("::::SCB_UPDATED_HD_ALERT::::");
        break;
    case SCB_UPDATED_SERVICE_LIST_FAST:
        CB_PRINTF0("::::SCB_UPDATED_SERVICE_LIST_FAST::::");
        break;
    case SCB_UPDATED_SERVICE_LIST_AUDIO:
        CB_PRINTF0("::::SCB_UPDATED_SERVICE_LIST_AUDIO::::");
        break;
    case SCB_UPDATED_SERVICE_LIST_DATA:
        CB_PRINTF0("::::SCB_UPDATED_SERVICE_LIST_DATA::::");
        break;
#endif //OPTION_INCLUDE_MODE_FMHD
#ifdef OPTION_INCLUDE_MODE_DAB
    case SCB_DAB_SERVICE_LIST_REQUIRED_UPDATE:
        CB_PRINTF0("::::NEW_SERVICE_LIST_RECEIVED::::");
        break;
    //DLS
    case SCB_UPDATED_DLS_STRING:
        CB_PRINTF0("::::SCB_UPDATED_DLS_STRING::::");
        break;
    case SCB_CLEAR_DLS_COMMAND:
        CB_PRINTF0("::::SCB_CLEAR_DLS_COMMAND::::");
        break;
    case SCB_UPDATED_SERVICE_LIST_DAB:
        CB_PRINTF0("::::SCB_UPDATED_SERVICE_LIST_DAB::::");
        break;
    case SCB_UPDATED_SERVICE_LINKING_DAB:
        CB_PRINTF0("::::SCB_UPDATED_SERVICE_LINKING_DAB::::");
        break;
    case SCB_SERVICE_LIST_BUFFER_FULL_ERROR:
        CB_PRINTF0("::::SCB_SERVICE_LIST_BUFFER_FULL_ERROR::::");
        break;
    case SCB_DAB_TUNE_SCAN_PROCESS_UPDATE:
        if (cb_data) {
            struct cb_dab_scan* cb = cb_data;
            cb->continue_scan = true;
            // todo? cancel if user input
        }

#ifdef DEBUG_PRINT_UPDATES
        {
            uint8_t fi = 0;
            if (cb_data) {
                struct cb_dab_scan* cb = cb_data;
                fi = cb->freq_index;
                if (cb->bg) {
                    CB_PRINTF("::::SCB_DAB_TUNE_SCAN_PROCESS_UPDATE:BG, FREQ=%6u", fi);
                } else {
                    sio_clear_screen();
                    CB_PRINTF0("::::SCB_DAB_TUNE_SCAN_PROCESS_UPDATE::::");
                    CB_PRINTF("FREQ:%6u", fi);
                }
            } else {
                sio_clear_screen();
                fi = MetricsGetDabPtr()->tuner.freq_index;
                CB_PRINTF0("::::SCB_DAB_TUNE_SCAN_PROCESS_UPDATE::::");
                CB_PRINTF("FREQ:%6u", fi);
            }
        }
#endif
        break;
    case SCB_DAB_TUNE_REQUIRED_UPDATE:
        // CB_PRINTF0("::::SERVICE_START_REQUIRES_A_RETUNE::::");
        break;
    case SCB_DAB_TUNE_SCAN_PROCESS_COMPLETE:
        CB_PRINTF0("::::SCB_DAB_TUNE_SCAN_PROCESS_COMPLETE::::");
        break;
    case SCB_EVENT_ENSEMBLE_RECONFIGURATION:
        CB_PRINTF0("::::SCB_EVENT_ENSEMBLE_RECONFIGURATION::::");
        break;
    case SCB_EVENT_ENSEMBLE_RECONFIGURATION_WARNING:
        CB_PRINTF0("::::SCB_EVENT_ENSEMBLE_RECONFIGURATION_WARNING::::");
        break;
    case SCB_EVENT_CURRENT_SERVICE_NO_LONGER_AVAILABLE:
        CB_PRINTF0("::::SCB_EVENT_CURRENT_SERVICE_NO_LONGER_AVAILABLE::::");
        break;
    case SCB_SERVICE_ACQ_DAB:
        CB_PRINTF0("::::SCB_SERVICE_ACQ_DAB::::");
        if (UiUpdateMode)
            UiUpdateMode(RADIO_MODE_DAB);
        break;

    case SCB_SERVICE_ACQ_FM:
        CB_PRINTF0("::::SCB_SERVICE_ACQ_FM::::");
        if (UiUpdateMode)
            UiUpdateMode(RADIO_MODE_FM);
        break;

#endif //OPTION_INCLUDE_MODE_DAB
    default:
        CB_PRINTF0("::::UPDATE CALLBACK ERROR::::");
        break;
    }

    sio_update_display();
}

#endif	/*SI479XX_FIRM_REL_4_1*/
