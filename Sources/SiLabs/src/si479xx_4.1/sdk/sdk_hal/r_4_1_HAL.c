/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210424 SGsoft */
#define _r_4_1_hal_h_global_
		
#include "main.h"

#if 0
#include "HAL.h"
#include "hal/hal_platform.h"
#include "platform_options.h"
#include "platform_types/siErrLog.h"

#include "core/c_core.h"
#include "core/c_timer.h"
#include "core/c_util.h"
#include "osal/misc.h"
#include "util/cmd_logger_cwrap.h"
#include "util/sdk_macro.h"

#include "apis/si46xx_dab_firmware_api_constants.h"
#include "apis/si479xx_detail_status_api.h"
#include "apis/si479xx_detail_status_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "apis/si479xx_hub_api.h"
#include "apis/si479xx_hub_api_constants.h"
#endif	/*0*/

#include <stdlib.h>
#include <string.h>

RETURN_CODE lock_chip(dc_config_chip_select ic, void* tmp)
{
    // stub 
    return SUCCESS;
}

RETURN_CODE unlock_chip(dc_config_chip_select ic)
{
    // stub 
    return SUCCESS;
}

static RETURN_CODE handle_audint(dc_config_chip_select ic, bool* is_fatal);
static RETURN_CODE handle_swerr(dc_config_chip_select ic);
static RETURN_CODE log_main_hifi_error_data(dc_config_chip_select ic);
RETURN_CODE error_check(dc_config_chip_select ic, uint16_t status_len, uint8_t* status, bool ok_to_send_cmds);

static struct hal_status_cfg s_status_cfg = { 0 };
static bool s_err_check_enabled = true;

// This is the max command length accepted for HIFI_CMDx
// Add 4 bytes for the HIFI_CMD, plus 252 for the packets.
static uint8_t s_hifi_buf[HIFI_MAX_CMD_LEN + 4];
static uint8_t s_hifi_offset = SI479XX_CMD_HIFI_CMD0_ARG_DATA_0_INDEX;
static uint8_t s_hifi_packet_count = 0;
static bool s_log_as_hifi = false;

static uint32_t s_hal_timeout_ms = 200;

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
#else
#define HAL_LAST_REPLY_SZ 6
static uint8_t s_status[OPTION_HAL_MAX_CHIPS][HAL_LAST_REPLY_SZ] = { 0 };
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

static void set_default_status_cfg(struct hal_status_cfg* cfg)
{
    if (!cfg)
        return;

    cfg->acts_polling = true;
    cfg->cts_polling = true;
}

static uint32_t get_status_word(dc_config_chip_select ic)
{
#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	return 0;
#else
    uint8_t chipnum = 0;
    if (SUCCESS == get_ic_num(ic, &chipnum)) {
        // CTS is in same position on all currently supported chip types.
        return *((uint32_t*)&s_status[chipnum][0]);
    } else {
        D_ASSERT_MSG(false, "ERROR: get_status_word() 'ic' invalid (x%x)!\n", ic);
        return 0;
    }
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

}

static bool is_status_mask_set(dc_config_chip_select ic, uint32_t bitmask)
{
    uint8_t chipnum = 0;
    uint32_t status = get_status_word(ic);
    return status & bitmask;
}

static bool is_cts_set(dc_config_chip_select ic)
{
    // CTS is in same position on all currently supported chip types.
    return is_status_mask_set(ic, 0x0080);
}

static bool is_acts_set(dc_config_chip_select ic, uint8_t pipe)
{
    uint32_t acts_mask = (1 << pipe) << (2 * 8);
    return is_status_mask_set(ic, acts_mask);
}

static void clear_status(dc_config_chip_select ic)
{
#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
#else
    uint8_t chipnum = 0;
    if (SUCCESS == get_ic_num(ic, &chipnum)) {
        memset(&s_status[chipnum][0], 0, HAL_LAST_REPLY_SZ);
    }
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

}

static void update_status(dc_config_chip_select ic, uint8_t* reply, size_t reply_len)
{
#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
#else
    D_ASSERT_MSG(reply, "update_status() : arg reply NULL");
    clear_status(ic);
    uint8_t chipnum = 0;
    if (SUCCESS == get_ic_num(ic, &chipnum)) {
        memcpy(&s_status[chipnum][0], reply, MIN(HAL_LAST_REPLY_SZ, reply_len));
    }
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

}

static RETURN_CODE wait_for_CTS_timeout(dc_config_chip_select icNum,
    uint32_t numReadTimeout, uint8_t statusLength, uint8_t* status, bool ok_to_send_cmds)
{
    RETURN_CODE ret = SUCCESS;
    // SDK_LOG("WAIT_FOR_CTS IC:%d TIMEOUT_MS:%d", icNum, numReadTimeout);

    if (s_status_cfg.cts_polling) {
        for (uint32_t i = 0; i < numReadTimeout; i++) {
            ret |= readReply(icNum, statusLength, status);
            if (ret != SUCCESS)
                return ret;

            // Quitting if any error bits set, except for COMMAND_ERROR,
            //   which should still wait for CTS
            ret = error_check(icNum, statusLength, status, ok_to_send_cmds);
            if (ret != COMMAND_ERROR && ret != SUCCESS)
                return ret;

            if (status[0] & SI479XX_CMD_READ_STATUS_REP_CTS_BIT) {
                // CTS set
                return ret;
            }
            // TODO: could replace with timer, but this gives up some cpu time
            os_delay(1);
        }
        return TIMEOUT;
    } else {
        c_timer timer;
        ct_start(&timer);

        // Using 200ms as the frequency of our checks;
        //  This limits the actual TIMEOUT case to be within the range
        //  (numReadTimeout, numReadTimeout + interrupt_timeout_ms).
        uint32_t interrupt_timeout_ms = 0/*200*/;			/* 210430 SGsoft */
        if (numReadTimeout < interrupt_timeout_ms)
            interrupt_timeout_ms = numReadTimeout;

        SI479XX_read_status__data status = { 0 };
        const uint8_t min_waits = 1;
        uint8_t attempts = 0;
        while (true) {
            if (attempts > min_waits) {
                if (ct_elapsed_ms(&timer) >= numReadTimeout) {
                    return TIMEOUT;
                }
            } else {
                attempts++;
            }

            ret = hal_waitForInterrupt(icNum, interrupt_timeout_ms);
            if (ret == SUCCESS) {
                // An interrupt fired, but what triggered it ?
                ret = SI479XX_read_status(icNum, &status);
                if (ret != SUCCESS) {
                    return ret;
                }
                // TODO -  do same error checking as in polling ?
                if (status.CTS == SI479XX_CMD_READ_STATUS_REP_CTS_ENUM_READY) {
                    return SUCCESS;
                }
            }
        }
    }
    return TIMEOUT;
}

// wait_for_CTS()
// Between every command write and before parsing the reply,
//	a check for the CTS bit being set is necessary.
// This code also checks for error bits, and logs these.
//   The following code performs a polling version of that check with a timeout option.
RETURN_CODE wait_for_CTS(dc_config_chip_select icNum,
    uint32_t numReadTimeout, uint8_t statusLength, uint8_t* status)
{
    // First check the stored status word -
    //   this prevents incorrect TIMEOUTs when using CTSINT
#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	return wait_for_CTS_timeout(icNum, numReadTimeout, statusLength, status, false);
#else
    if (is_cts_set(icNum)) {
        return SUCCESS;
    } else {
        return wait_for_CTS_timeout(icNum, numReadTimeout, statusLength, status, false);
    }
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

}

RETURN_CODE wait_for_ACTS(dc_config_chip_select icNum, uint8_t pipe, uint32_t numReadTimeout)
{
    // first check stored status word
    if (is_acts_set(icNum, pipe)) {
        return SUCCESS;
    } else {
        RETURN_CODE ret = SUCCESS;
        uint8_t status[SI479XX_CMD_READ_STATUS_REP_LENGTH];
        uint16_t status_len = sizeof(status);

        // Poll part for CTS, then poll for ACTS
        ret = wait_for_CTS_timeout(icNum, numReadTimeout, status_len, status, false);
        if (ret != SUCCESS)
            return ret;

        // SDK_LOG("WAIT_FOR_ACTS%d IC:%d TIMEOUT_MS:%d", pipe, icNum, numReadTimeout);
        const uint8_t acts_mask = (1 << pipe);
        for (uint32_t i = 0; i < numReadTimeout; i++) {
            if (status[2] & acts_mask) {
                return SUCCESS;
            }
            os_delay(1);

            ret |= readReply(icNum, status_len, status);
            if (ret != SUCCESS)
                return ret;

            /*
            //   which should still wait for CTS
            ret = error_check(icNum, status_len, status);
            if(ret != COMMAND_ERROR && ret != SUCCESS)
                return ret;
            */
        }
        return TIMEOUT;
    }
}

static char* get_error_log_codename(uint8_t error_code)
{
    uint32_t code_word = error_code << 8 * 3;

    // UPDATE: if 'siErrLog.h' gets updated.
    switch (code_word) {
    case ERROR_INVALID_PORT_MANAGER_PIPE_HEADER:
        return "ERROR_INVALID_PORT_MANAGER_PIPE_HEADER";
    case ERROR_INVALID_PORT_MANAGER_COMMAND_COUNT:
        return "ERROR_INVALID_PORT_MANAGER_COMMAND_COUNT";
    case ERROR_INVALID_PORT_MANAGER_PACKET_LENGTH:
        return "ERROR_INVALID_PORT_MANAGER_PACKET_LENGTH";
    case ERROR_INVALID_PORT_MANAGER_COMMAND:
        return "ERROR_INVALID_PORT_MANAGER_COMMAND";
    case ERROR_INVALID_PORT_MANAGER_COMMAND_LENGTH:
        return "ERROR_INVALID_PORT_MANAGER_COMMAND_LENGTH";
    case ERROR_PORT_MANAGER_COMMAND_REFUSED:
        return "ERROR_PORT_MANAGER_COMMAND_REFUSED";

    case ERROR_INVALID_HUB_MANAGER_PIPE_HEADER:
        return "ERROR_INVALID_HUB_MANAGER_PIPE_HEADER";
    case ERROR_INVALID_HUB_MANAGER_COMMAND_COUNT:
        return "ERROR_INVALID_HUB_MANAGER_COMMAND_COUNT";
    case ERROR_INVALID_HUB_MANAGER_PACKET_LENGTH:
        return "ERROR_INVALID_HUB_MANAGER_PACKET_LENGTH";
    case ERROR_INVALID_HUB_MANAGER_COMMAND:
        return "ERROR_INVALID_HUB_MANAGER_COMMAND";
    case ERROR_INVALID_HUB_MANAGER_COMMAND_LENGTH:
        return "ERROR_INVALID_HUB_MANAGER_COMMAND_LENGTH";

    case ERROR_INVALID_AUD_PROC_MANAGER_PIPE_HEADER:
        return "ERROR_INVALID_AUD_PROC_MANAGER_PIPE_HEADER";
    case ERROR_INVALID_AUD_PROC_MANAGER_COMMAND_COUNT:
        return "ERROR_INVALID_AUD_PROC_MANAGER_COMMAND_COUNT";
    case ERROR_INVALID_AUD_PROC_MANAGER_PACKET_LENGTH:
        return "ERROR_INVALID_AUD_PROC_MANAGER_PACKET_LENGTH";
    case ERROR_INVALID_AUD_PROC_MANAGER_COMMAND:
        return "ERROR_INVALID_AUD_PROC_MANAGER_COMMAND";
    case ERROR_INVALID_AUD_PROC_MANAGER_COMMAND_LENGTH:
        return "ERROR_INVALID_AUD_PROC_MANAGER_COMMAND_LENGTH";

    case ERROR_OFF_BOUNDARY_MEMORY_ACCESS:
        return "ERROR_OFF_BOUNDARY_MEMORY_ACCESS";
    case ERROR_DIVIDE_BY_ZERO:
        return "ERROR_DIVIDE_BY_ZERO";
    case ERROR_SPURIOUS_SWI:
        return "ERROR_SPURIOUS_SWI";
    case ERROR_FRAME_OVER_RUN:
        return "ERROR_FRAME_OVER_RUN";
    case ERROR_I2S_LOST_CLOCK:
        return "ERROR_I2S_LOST_CLOCK";
    case ERROR_I2S_RX_OVER_RUN:
        return "ERROR_I2S_RX_OVER_RUN";
    case ERROR_I2S_RX_UNDER_RUN:
        return "ERROR_I2S_RX_UNDER_RUN";
    case ERROR_I2S_TX_OVER_RUN:
        return "ERROR_I2S_TX_OVER_RUN";
    case ERROR_I2S_TX_UNDER_RUN:
        return "ERROR_I2S_TX_UNDER_RUN";
    case ERROR_READ_NODE_UNDER_RUN:
        return "ERROR_READ_NODE_UNDER_RUN";
    case ERROR_INCOMPATIBLE_TOPOLOGY:
        return "ERROR_INCOMPATIBLE_TOPOLOGY";
    case ERROR_AUDIO_PROCESS_OVER_RUN:
        return "ERROR_AUDIO_PROCESS_OVER_RUN";
    case ERROR_FATAL_EXCEPTION:
        return "ERROR_FATAL_EXCEPTION";
    case ERROR_NON_FATAL_EXCEPTION:
        return "ERROR_NON_FATAL_EXCEPTION";
    case ERROR_PIPE_IS_IN_USE:
        return "ERROR_PIPE_IS_IN_USE";
    case ERROR_INVALID_WAVEFORM_FORMAT:
        return "ERROR_INVALID_WAVEFORM_FORMAT";
    case ERROR_INVALID_WAVEFORM_SAMPLE_COUNT:
        return "ERROR_INVALID_WAVEFORM_SAMPLE_COUNT";
    case ERROR_AUD_PROC_CONSTRUCTOR_MODULE_FAIL:
        return "ERROR_AUD_PROC_CONSTRUCTOR_MODULE_FAIL";
    case ERROR_INVALID_GPNUM:
        return "ERROR_INVALID_GPNUM";
    default:
        return "UNKNOWN_ERROR";
    }
}

RETURN_CODE set_status_cfg(struct hal_status_cfg cfg)
{
    s_status_cfg = cfg;
    return SUCCESS;
}

struct hal_status_cfg get_status_cfg()
{
    return s_status_cfg;
}

RETURN_CODE ctsSetMode(bool use_polling)
{
    struct hal_status_cfg cfg = get_status_cfg();
    cfg.cts_polling = use_polling;
    return set_status_cfg(cfg);
}

void get_hw_cfg(const struct dc_cfg* dc, uint8_t len, struct hal_hw_cfg* chips)
{
    D_ASSERT_MSG(dc && chips, "null argument");
    for (int i = 0; i < len; i++) {
        switch (dc->chip_types[i]) {
        case Si479xx:
        case Si4796x:
            chips[i].edge_type = HAL_HW_INT_RISING_EDGE;
            break;
        case Si461x:
        case Si469x:
            chips[i].edge_type = HAL_HW_INT_FALLING_EDGE;
            break;
        case NotPresent:
            break;
        default:
            D_ASSERT_MSG(false, "chiptype %d not implemented.\n", dc->chip_types[i]);
            break;
        }
    }
}

RETURN_CODE initHardware(const struct dc_cfg* dc)
{
    set_default_status_cfg(&s_status_cfg);
	
	#if 1		/* 210427 SGsoft */
	return SUCCESS;
	#else
    if (dc) {
        // Configure interrupt lines (but starts in polling mode).
        struct hal_hw_cfg chips[OPTION_HAL_MAX_CHIPS];
        uint8_t len = ARRAY_SZ(chips);
        get_hw_cfg(dc, len, chips);
        return hal_initHardware(len, chips);
    } else {
        // forces polling-based
        return hal_initHardware(0, NULL);
    }
	#endif	/*1*/
}

RETURN_CODE resetIC(uint8_t icNum)
{
    return hal_resetIC(icNum);
}

RETURN_CODE powerDownHardware(void)
{
    return hal_powerDownHardware();
}

void startHifiCmd()
{
    // Start offset to allow adding the HIFI_CMD header later.
    s_hifi_offset = SI479XX_CMD_HIFI_CMD0_ARG_DATA_0_INDEX;
    s_hifi_packet_count = 0;
    s_log_as_hifi = true;
}

void stopHifiCmd()
{
    s_log_as_hifi = false;
}

RETURN_CODE writeHifiCmd(dc_config_chip_select ic, uint8_t pipe)
{
    RETURN_CODE ret = SUCCESS;
    uint16_t data_len = s_hifi_offset - SI479XX_CMD_HIFI_CMD0_ARG_DATA_0_INDEX;

    // creating HIFI_CMDx
    // CMD
    s_hifi_buf[0] = 0x80 | pipe;
    // PACKET_COUNT
    s_hifi_buf[1] = s_hifi_packet_count;
    // LENGTH[7:0]
    s_hifi_buf[2] = (data_len & 0xFF);
    // LENGTH[15:8]
    s_hifi_buf[3] = ((data_len >> 8) & 0xFF);

    ret = wait_for_ACTS(ic, pipe, 1000);
    if (ret != SUCCESS) {
        SDK_PRINTF("ERROR: ACTS%d not raised!\n", pipe);
        return ret;
    }

    return writeCommand(ic, s_hifi_offset, s_hifi_buf);
}

RETURN_CODE writeCommand_wait(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer, uint32_t timeout_ms)
{
    RETURN_CODE ret = SUCCESS;
    uint32_t orig = getCtsTimeout();
    setCtsTimeout(timeout_ms);
#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum, length, buffer);
#else
	ret = writeCommand(icNum, length, buffer);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/
    setCtsTimeout(orig);
    return ret;
}

bool toggle_errcheck(bool enable)
{
    bool tmp = s_err_check_enabled;
    s_err_check_enabled = enable;
    return tmp;
}

void error_exit(const char* msg)
{
#if (FEAT_DEBUG_211125 == 0)		/* 211125 SGsoft */
    D_PRINTF(stderr, msg);
    D_PRINTF(stderr, "\nWriting error-log.txt...\n");
    c_dump(ADD_RESOURCE_DIR("state/sdk-error-log.txt"));
    D_PRINTF(stderr, "error-log.txt complete.\n");
    fflush(stderr);
    exit(-1);
#endif	/*FEAT_DEBUG_211125*/

}

RETURN_CODE handle_swerr(dc_config_chip_select ic)
{
    // SWERR is fatal, but log information before quitting.
    uint8_t err_reply[80];
    uint32_t i = 0;
    SI479XX_DETAIL_STATUS_swerr_info__data swerr_info;

    toggle_errcheck(false);
    readReply(ic, sizeof(err_reply), err_reply);
    SDK_LOG("SWERR: 80 byte reply = [");
    for (i = 0; i < ARRAY_SZ(err_reply); i++)
        c_log_commentf("0x%x,", err_reply[i]);
    c_log_commentf("]\n");

    // Also dump DETAIL_STATUS:SWERR_INFO reply
    c_log_comment("Trying to get DETAIL_STATUS:SWERR_INFO reply...");
    SI479XX_DETAIL_STATUS_swerr_info(ic, &swerr_info);
    c_log_comment("DETAIL_STATUS:SWERR_INFO reply={");
    c_log_commentf("LASTSWERR=0x%x,", swerr_info.LAST_SWERR);
    c_log_commentf("INFO0=0x%x,", swerr_info.INFO0);
    c_log_commentf("INFO1=0x%x,", swerr_info.INFO1);
    c_log_commentf("INFO2=0x%x,", swerr_info.INFO2);
    c_log_commentf("INFO3=0x%x,", swerr_info.INFO3);
    c_log_commentf("INFO4=0x%x,", swerr_info.INFO4);
    c_log_commentf("INFO5=0x%x,", swerr_info.INFO5);
    c_log_commentf("INFO6=0x%x,", swerr_info.INFO6);
    c_log_comment("}");

    if (swerr_info.LAST_SWERR == SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_LAST_SWERR_ENUM_HIFIERRORS) {
        // HIFI caused the SWERR, get as much HIFI information as possible.
        log_main_hifi_error_data(ic);
    }

    return SUCCESS;
}

bool is_fatal_audio_error_code(uint8_t error_code)
{
    uint32_t code_word = error_code << 8 * 3;

    // UPDATE: if 'siErrLog.h' gets updated.
    switch (code_word) {
    case ERROR_OFF_BOUNDARY_MEMORY_ACCESS:
    case ERROR_DIVIDE_BY_ZERO:
    case ERROR_INCOMPATIBLE_TOPOLOGY:
    case ERROR_FATAL_EXCEPTION:
        return true;

    default:
        return false;
    }
    return false;
}

RETURN_CODE log_main_hifi_error_data(dc_config_chip_select ic)
{
    RETURN_CODE ret = SUCCESS;
    const int CRASHDUMP_SZ = 40;
    const int ERRLOG_SZ = 196;
    const int PIPEX_SZ = 516;
    uint8_t buffer[516];

    // All command/byte streams are logged by writeCommand/readReply,
    //  so there is no need to explicitly log them here.
    c_log_comment("Dumping HIFI_LOG:BLOCK=CRASHDUMP");
    ret = SI479XX_hifi_log__command(ic, SI479XX_CMD_HIFI_LOG_ARG_BLOCK_ENUM_CRASHDUMP);
    ret |= readReply(ic, CRASHDUMP_SZ, buffer);

    c_log_comment("Dumping HIFI_LOG:BLOCK=ERRLOG");
    ret = SI479XX_hifi_log__command(ic, SI479XX_CMD_HIFI_LOG_ARG_BLOCK_ENUM_ERRLOG);
    ret |= readReply(ic, ERRLOG_SZ, buffer);
    // could format this reply like the AUDIO_HUB:ERROR_LOG_READ

    c_log_comment("Dumping HIFI_LOG:BLOCK=PIPE0");
    ret = SI479XX_hifi_log__command(ic, SI479XX_CMD_HIFI_LOG_ARG_BLOCK_ENUM_PIPE0);
    ret |= readReply(ic, PIPEX_SZ, buffer);

    c_log_comment("Dumping HIFI_LOG:BLOCK=PIPE1");
    ret = SI479XX_hifi_log__command(ic, SI479XX_CMD_HIFI_LOG_ARG_BLOCK_ENUM_PIPE1);
    ret |= readReply(ic, PIPEX_SZ, buffer);

    c_log_comment("Dumping HIFI_LOG:BLOCK=PIPE2");
    ret = SI479XX_hifi_log__command(ic, SI479XX_CMD_HIFI_LOG_ARG_BLOCK_ENUM_PIPE2);
    ret |= readReply(ic, PIPEX_SZ, buffer);

    c_log_comment("Dumping HIFI_LOG:BLOCK=PIPE7");
    ret = SI479XX_hifi_log__command(ic, SI479XX_CMD_HIFI_LOG_ARG_BLOCK_ENUM_PIPE7);
    ret |= readReply(ic, PIPEX_SZ, buffer);

    return ret;
}

RETURN_CODE handle_audint(dc_config_chip_select ic, bool* is_fatal)
{
    RETURN_CODE ret = SUCCESS;
    if (!is_fatal)
        return INVALID_INPUT;

    *is_fatal = false;

    // Use the audio_hub sub API to read the error and clear the log.
    SDK_LOG("Reading AUDIO_HUB error log...");

    SI479XX_HUB_error_log_read__data error_log = { 0 };
    ret = SI479XX_HUB_error_log_read(ic, 0, true, SI479XX_HUB_CMD_ERROR_LOG_READ_ARG_LENGTH_MIN, &error_log);
    if (ret != SUCCESS)
        goto free_error_log;

    SDK_LOG("ERROR[] = {");
    for (uint8_t i = 0; i < error_log.ERROR_DATA_COUNT; i++) {
        char* name = get_error_log_codename(error_log.ERROR[i].ERROR_CODE);
        bool fatal_error = is_fatal_audio_error_code(error_log.ERROR[i].ERROR_CODE);
        char* fatal_msg = fatal_error ? "FATAL " : "";

        // Generally these should not be true (otherwise SWERR would've been set and AUDIO_HUB commands will not respond)
        if (fatal_error)
            *is_fatal = true;

        uint32_t error = get_32bit_le(&error_log.ERROR[i].ERROR_BYTE_0);
        uint32_t frame = get_32bit_le(&error_log.ERROR[i].FRAME_NUMBER_BYTE_0);

        SDK_LOG("  %sERR#%d : Name=%s Error=0x%08x Frame=0x%08x,", fatal_msg, i, name, error, frame);
    }
    SDK_LOG("}");

free_error_log:
    free(error_log.ERROR);
    return ret;
}

RETURN_CODE error_check(dc_config_chip_select ic, uint16_t status_len, uint8_t* status, bool ok_to_send_cmds)
{
    // most status word bits differ between parts (tuner, linker, demod, etc...)
    // So we need to know what part the chip is before checking for errors.
    if (!s_err_check_enabled)
        return SUCCESS;

    RETURN_CODE ret = SUCCESS;
    dc_config_chip_type ic_type;

    ret = syscfg_get_ic_type(ic, &ic_type);
    if (ret != SUCCESS) {
        SDK_PRINTF("WARN: error_check() could not determine chip type...\n");
        return ret;
    }

    switch (ic_type) {
    case Si479xx:
    case Si4796x:
        if (status[3] & SI479XX_CMD_READ_STATUS_REP_NRERR_BIT) {
            c_log_comment("NRERR !");
            c_log_err(ic, ERRT_NR, status_len, status);
            error_exit("NRERR : (non-recoverable error) must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI479XX_CMD_READ_STATUS_REP_ARBERR_BIT) {
            c_log_comment("ARBERR !");
            c_log_err(ic, ERRT_ARB, status_len, status);
            error_exit("ARBERR : must reboot part. Sample code requires restart.\n");

		#if (FEAT_DEBUG_211125)		/* 211125 SGsoft */
			fgARBERR = 1;
			memcpy(ARBERR_STATUS, status, 4);
		#endif	/*FEAT_DEBUG_211125*/

		#if (FEAT_DEBUG_ARBERR)		/* 211018 SGsoft */
			{
				uint8_t arg_data = 0x00;
				uint8_t rep_data[32];
				
				ret |= SI4796X_detail_status(IC0, 0xd0, &arg_data, 1, &rep_data);
				if (ret == SUCCESS){
					/*** print rep_data ***/
				}
			}
		#endif	/*FEAT_DEBUG_ARBERR*/
		
            return NR_WARN_ERROR;
        }

        if (status[3] & SI479XX_CMD_READ_STATUS_REP_REPERR_BIT) {
            c_log_comment("REPERR !");
            c_log_err(ic, ERRT_REP, status_len, status);
            error_exit("REPERR : must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI479XX_CMD_READ_STATUS_REP_CMDERR_BIT) {
            c_log_comment("CMDERR !");
            c_log_err(ic, ERRT_CMD, status_len, status);
            error_exit("CMDERR : must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI479XX_CMD_READ_STATUS_REP_TMPERR_BIT) {
            c_log_comment("TMPERR !");
            c_log_err(ic, ERRT_TMP, status_len, status);
            error_exit("TMPERR : Board too hot? Must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[0] & SI479XX_CMD_READ_STATUS_REP_SWERR_BIT) {
            // SWERR
            c_log_comment("SWERR !");
            handle_swerr(ic);
            error_exit("SWERR: must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[0] & SI479XX_CMD_READ_STATUS_REP_VDERR_BIT) {
            c_log_comment("VDERR !");
            c_log_err(ic, ERRT_VD, status_len, status);
            error_exit("VDERR: must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[2] & SI479XX_CMD_READ_STATUS_REP_AUDINT_BIT) {
            c_log_comment("AUDINT !");
            toggle_errcheck(false);
            // NOTE: requires commands be sent to get the HiFi error log.
            // Have to wait until we can send commands to properly handle.
            if (ok_to_send_cmds) {
                bool is_fatal = false;
                RETURN_CODE ret = handle_audint(ic, &is_fatal);
                // c_log_err(ic, ERRT_AUDINT, status_len, status);

                if (ret == SUCCESS && !is_fatal) {
                    ret |= COMMAND_ERROR;
                } else {
                    error_exit("Unrecoverable AUDINT - HIFI has crashed: must reboot part. Sample code requires restart.\n");
                    return NR_WARN_ERROR;
                }
            }
            toggle_errcheck(true);
        }

        // Tuner parts assumed to have basically same status word (at least for errors)
        if (status[0] & SI479XX_CMD_READ_STATUS_REP_APIERR_BIT) {
            // APIERR set
            // read the error code
            uint8_t err_reply[5];
            readReply(ic, sizeof(err_reply), err_reply);
            c_log_comment("APIERR !");
            c_log_err(ic, ERRT_API, sizeof(err_reply), err_reply);
			si479xx_errcode = err_reply[4];											/* 210429 SGsoft */
            ret |= COMMAND_ERROR;
        }

        if (status[3] & SI479XX_CMD_READ_STATUS_REP_HOTWRN_BIT) {
            c_log_comment("HOTWRN !");
            c_log_err(ic, ERRT_HOTWRN, status_len, status);
            error_exit("HOTWRN : Board too hot? Should reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        break;

	#if 0		/* 210505 SGsoft */	
    case Si461x:
    case Si469x:

        if (status[3] & SI461X_DAB_CMD_READ_STATUS_REP_ERRNR_BIT) {
            c_log_comment("NRERR !");
            c_log_err(ic, ERRT_NR, status_len, status);
            error_exit("NRERR : Must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI461X_DAB_CMD_READ_STATUS_REP_ARBERR_BIT) {
            c_log_comment("ARBERR !");
            c_log_err(ic, ERRT_ARB, status_len, status);
            error_exit("ARBERR : Must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI461X_DAB_CMD_READ_STATUS_REP_REPOFERR_BIT) {
            c_log_comment("REPERR !");
            c_log_err(ic, ERRT_REP, status_len, status);
            error_exit("REPERR : Must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI461X_DAB_CMD_READ_STATUS_REP_CMDOFERR_BIT) {
            c_log_comment("CMDERR !");
            c_log_err(ic, ERRT_CMD, status_len, status);
            error_exit("CMDERR : Must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

        if (status[3] & SI461X_DAB_CMD_READ_STATUS_REP_DSPERR_BIT) {
            c_log_comment("DSPERR !");
            c_log_err(ic, ERRT_DSP, status_len, status);
            error_exit("REPERR : Must reboot part. Sample code requires restart.\n");
            return NR_WARN_ERROR;
        }

#if 0 // TODO
        if (status[3] & SI461X_DAB_CMD_READ_STATUS_REP_RFFE_ERR_BIT)
        {
            // Should not occur in the sample code since we don't use the RF Front End 
            //  of any of the Si46xx family of parts.
            c_log_comment("RFFE_ERR !");
            c_log_err(ic, ERRT_DSP, status_len, status);
            ret |= COMMAND_ERROR;
        }
#endif

        // Demod and linker parts assumed to have basically same status word (at least for errors)
        if (status[0] & SI461X_DAB_CMD_READ_STATUS_REP_ERR_CMD_BIT) {
            // APIERR set , read the error code
            uint8_t err_reply[5];
            readReply(ic, sizeof(err_reply), err_reply);
            c_log_comment("APIERR !");
            c_log_err(ic, ERRT_API, sizeof(err_reply), err_reply);
            ret |= COMMAND_ERROR;
        }
        break;
	#endif	/*0*/

    default:
        SDK_PRINTF("ERR: error_check(): unrecognized ic_type: %d\n", ic_type);
        return INVALID_INPUT;
    }

    return ret;
}

// Command write and reply read
// The most common interaction with the Si479xx is to write a command,
//   wait for CTS (using repeated reads to confirm the bit), then read the full reply payload for the command.
//   The following code performs a polling version of a typical single command/reply transaction.
//   This assumes there is a separate function for writing a command and reading the reply.
//   This is a recommended way to structure the host code as there will be need to read multiple times without resending a command.
RETURN_CODE command(dc_config_chip_select icNum, uint16_t commandLength,
    uint8_t* commandBuffer, uint16_t replyLength, uint8_t* replyBuffer, uint32_t numReadTimeout)
{
    RETURN_CODE ret = SUCCESS;
    if ((commandLength == 0) || (replyLength == 0))
        return INVALID_INPUT;

    //It is possible that there is a pending command error - we need to be able to send another command to clear out this state
    ret = writeCommand(icNum, commandLength, commandBuffer);
    if (ret == SUCCESS) {
        ret = readReply(icNum, replyLength, replyBuffer);
    }
    return ret;
}

void setCtsTimeout(uint32_t timeoutMs)
{
    s_hal_timeout_ms = timeoutMs;
}

uint32_t getCtsTimeout()
{
    return s_hal_timeout_ms;
}

RETURN_CODE writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer)
{
    RETURN_CODE ret = SUCCESS;
    uint8_t status[5] = { 0 };
    ///uint8_t buf_copy[0x2000] = { 0 };					/* 210429 SGsoft */

    D_ASSERT_MSG(sizeof(buf_copy) >= length, "buf_copy[] not big enough to store copy of command. sizeof(buf_copy)=0x%x, cmd length=0x%x", sizeof(buf_copy), length);

    // Make a copy of the command. If error handling needs to use APIs, this prevents the buffer from getting overwritten.
    ///memcpy(buf_copy, buffer, length);					/* 210429 SGsoft */

    if (s_log_as_hifi) {
        SDK_PRINTF("should not hit this in writeCommand - TODO remove me.\n");
    }

    ret |= wait_for_CTS(icNum, s_hal_timeout_ms, ARRAY_SZ(status), status);

    // It is possible that there is a pending command error -
    //  we need to be able to send another command to clear out this state.
    if ((ret == SUCCESS) || (ret == COMMAND_ERROR)) {
        // Commands need to be at least 2 bytes in length, shorter commands are an API mistake. (except READ_REPLY)
        if (length < 2 && buffer/*buf_copy*/[0] != SI479XX_CMD_READ_STATUS) {			/* 210429 SGsoft */
            SDK_LOG("API mistake? cmd length <2 bytes (%d)", length);
            length = 2;
        }

        // Clear the saved status for this chip - it's no longer valid.
        clear_status(icNum);
#if 0
        printf("\rwriteCommand() ic=0x%x len=0x%x, cmd=", icNum, length);
        for(int i = 0; i < length; i++) {
            printf("%02x ", buf_copy[i]);
            if (i > 20) {
                printf("...");
                break;
            }
        } 
        printf("\n");
#endif
        ret = hal_writeCommand(icNum, length, buffer/*buf_copy*/);			/* 210429 SGsoft */
        c_log_command(ret, icNum, length, buffer/*buf_copy*/);

        if (ret == SUCCESS)
            ret |= wait_for_CTS(icNum, s_hal_timeout_ms, ARRAY_SZ(status), status);
    }

    return ret;
}

RETURN_CODE readReply(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer)
{
    RETURN_CODE ret = SUCCESS;
    ClearMemory(buffer, length);
    ret = hal_readReply(icNum, length, buffer);
    if (ret & HAL_ERROR) {
        c_log_comment("HAL_ERROR !");
        c_dump(ADD_RESOURCE_DIR("state/sdk_hal_error.txt"));
        SDK_PRINTF("HAL_ERROR! (try power cycling the board?)\n");
        D_ASSERT(false);
    }
    if (ret == SUCCESS) {
        update_status(icNum, buffer, length);

#if 0
        printf("\rreadReply() ic=0x%x, len=0x%x, reply=", icNum, length);
        for(int i = 0; i < length; i++) {
            printf("%02x ", buffer[i]);
            if (i > 20) {
                printf("...");
                break;
            }
        } 
        printf("\n");
#endif
    }

    c_log_reply(ret, icNum, length, buffer);
    return ret;
}


#define HIFI_DEFAULT_TIMEOUT_MS 1000
RETURN_CODE hifi_command(dc_config_chip_select icNum, uint8_t pipe, uint8_t packet_count, uint16_t length, uint8_t* buffer)
{
    RETURN_CODE ret = SUCCESS;
    uint8_t status[5] = { 0 };
    uint8_t buf_copy[0x2000] = { 0 };
    D_ASSERT_MSG(sizeof(buf_copy) >= length, "buf_copy[] not big enough to store copy of command. sizeof(buf_copy)=0x%x, cmd length=0x%x", sizeof(buf_copy), length);

    // Make a copy of the command. If error handling needs to use APIs, this prevents the buffer from getting overwritten.
    memcpy(buf_copy, buffer, length);
    if (pipe >= SI479XX_MAX_NUM_PIPES) {
        return INVALID_INPUT;
    }

    if (s_log_as_hifi) {
        // Need to take out the header information from all the packets.
        const uint8_t hifi_cmd_header = SI479XX_CMD_HIFI_CMD0_ARG_DATA_0_INDEX;
        uint16_t packet_len = length;
        if ((packet_len + s_hifi_offset) > sizeof(s_hifi_buf)) {
            SDK_PRINTF("Current HIFI_CMD is too long! Must split up into multiple.\n");
            return MEMORY_ERROR;
        }

        // Copy just the packet data in to the buffer.
        memcpy(s_hifi_buf + s_hifi_offset, buf_copy, packet_len);
        s_hifi_offset += packet_len;
        s_hifi_packet_count++;
        return SUCCESS;
    }

    // wait for CTS / ACTS
    ret = wait_for_ACTS(icNum, pipe, HIFI_DEFAULT_TIMEOUT_MS);
    if (ret != SUCCESS)
        return ret;

    // send hifi command via matching pipe
    ret = SI479XX_HifiCmdCommandFunction[pipe](icNum, packet_count, length, buffer);
    return ret;
}

RETURN_CODE hifi_response(dc_config_chip_select icNum, uint8_t pipe, SI479XX_hifi_resp__data* resp)
{
    if (pipe >= SI479XX_MAX_NUM_PIPES)
        return INVALID_INPUT;

    if (!resp)
        return INVALID_INPUT;

    // wait for CTS / ACTS
    RETURN_CODE ret = wait_for_ACTS(icNum, pipe, HIFI_DEFAULT_TIMEOUT_MS);
    if (ret != SUCCESS)
        return ret;

    ret = SI479XX_HifiRespCommandFunction[pipe](icNum);
    if (ret != SUCCESS)
        return ret;

    ret = wait_for_ACTS(icNum, pipe, HIFI_DEFAULT_TIMEOUT_MS);
    if (ret != SUCCESS)
        return ret;

    ret = SI479XX_HifiRespReplyFunction[pipe](icNum, resp);
    return ret;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
