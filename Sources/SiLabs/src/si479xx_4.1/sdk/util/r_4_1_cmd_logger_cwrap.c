/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210426 SGsoft */
#define _r_4_1_cmd_logger_cwrap_h_global_
		
#include "main.h"

#if 0
#include "util/cmd_logger_cwrap.h"
#include "util/cmd_logger.h"

#include "util/to_cpp.h"
#include "util/sdk_macro.h"
#include "core/string_util.h"

#include <stdarg.h>
#include <string>

using namespace std;
#endif	/*0*/


#if 0		/* 210429 SGsoft */
void c_log_command(RETURN_CODE ret, uint8_t ic, uint16_t length, const uint8_t * buffer)
{
    ///g_logger.log_command(ret, ic, length, buffer);
}

void c_log_reply(RETURN_CODE ret, uint8_t ic, uint16_t length, const uint8_t * buffer)
{
    ///g_logger.log_reply(ret, ic, length, buffer);
}


void c_log_comment(const char* str)
{
    ///g_logger.log_comment(str);
}


void c_log_commentf(const char* format, ...)
{
	#if 0
    va_list args;
    va_start(args, format);
    int required_sz = vsnprintf(NULL, 0, format, args);
    vector<char> cstr;
    cstr.resize(required_sz + 1);
    vsnprintf(&cstr[0], cstr.size(), format, args);
    c_log_comment(&cstr[0]);

    va_end(args);
	#endif	/*0*/
}


void c_log_err(uint8_t ic, ERR_TYPE type, uint16_t length, const uint8_t * buffer)
{
    ///g_logger.log_printable(make_unique<ErrorLog>(ic, type, length, buffer));
}


void c_log_svc(const dab_service_info * svc)
{
	#if 0
    string s = su::to_string(*svc);
    g_logger.log_printable(make_unique<PrintableString>(s));
	#endif	/*0*/
}

void c_log_svc_list(const dab_service_list * list)
{
	#if 0
	if (list == nullptr)
		return;

    string liststr = su::to_string(*list);
    g_logger.log_printable(make_unique<PrintableString>(liststr));
	#endif	/*0*/
}




void c_log_xbar_status(const SI479XX_HUB_xbar_query__data * data)
{
	#if 0
    string s = su::to_string(*data);
    g_logger.log_printable(make_unique<PrintableString>(s));
	#endif	/*0*/
}

void enable_logger()
{
    ///g_logger.set_enable(true);
}

void disable_logger()
{
    ///g_logger.set_enable(false);
}

void c_dump(const uint8_t * fname)
{
    ///g_logger.dump((const char*)fname);
}
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
