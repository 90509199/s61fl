/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_firmware_load_helpers_h_global_
	
#include "main.h"

#if 0
#include "boot/Firmware_Load_Helpers.h"
#include "boot/FirmwarePaths.h"
#include "boot/boot_types.h"
#include "boot/default_args.h"
#include "boot/fw_guids.h"
#include "boot/si46xx_initial_startup_sample.h"
#include "boot/si479xx_initial_startup_sample.h"

#include "apis/si4690_dab_firmware_api_constants.h"
#include "apis/si46xx_dab_firmware_api_constants.h"
#include "apis/si4796x_firmware_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"

#include "common_types.h"
#include "core/c_core.h"
#include "core/c_util.h"
#include "osal/misc.h"
#include "util/cmd_logger_cwrap.h"
#include "util/flash_parser.h"
#include "util/sdk_macro.h"

#include "HAL.h"
#include "common/global_data.h"
#include "platform/System_Configuration.h"
#endif	/*0*/

///#include <platform_options.h>						/* 210426 SGsoft */
#include <stdio.h>
#include <string.h>
#if 0		/* 210630 SGsoft */	
#include <sys/stat.h>
#endif	/*0*/

// Default powerup arguments.
#if 1		/* 210512 SGsoft */	
#define PU_4790_CTUN 									24/*18*/				/* 210513 SGsoft */	
#define PU_4790_XTALFREQ 								36864000
#define PU_4790_IBIAS 									67
#define PU_4790_VIO 									SI479XX_CMD_POWER_UP_ARG_VIO_ENUM_V3P3
#define PU_4790_TR_SIZE 								SI479XX_CMD_POWER_UP_ARG_TR_SIZE_ENUM_ENABLE
#define PU_4790_IC0_CLKO_CURRENT 						SI479XX_CMD_POWER_UP_ARG_CLKO_CURRENT_ENUM_DEFAULT
#define PU_4790_ICX_CLKO_CURRENT 						SI479XX_CMD_POWER_UP_ARG_CLKO_CURRENT_ENUM_DEFAULT
#define PU_4790_ICX_CLKMODE 							SI479XX_CMD_POWER_UP_ARG_CLK_MODE_ENUM_SINGLE_DC

#else
#define PU_4790_CTUN 20
#define PU_4790_XTALFREQ 36864000
#define PU_4790_IBIAS 67
#define PU_4790_VIO SI479XX_CMD_POWER_UP_ARG_VIO_ENUM_V3P3
#define PU_4790_TR_SIZE SI479XX_CMD_POWER_UP_ARG_TR_SIZE_ENUM_ENABLE
#define PU_4790_IC0_CLKO_CURRENT SI479XX_CMD_POWER_UP_ARG_CLKO_CURRENT_ENUM_DEFAULT
#define PU_4790_ICX_CLKO_CURRENT SI479XX_CMD_POWER_UP_ARG_CLKO_CURRENT_ENUM_DEFAULT
#define PU_4790_ICX_CLKMODE SI479XX_CMD_POWER_UP_ARG_CLK_MODE_ENUM_SINGLE_DC
#endif	/*1*/

// GE / DE powerup args usually same
#define PU_4796_VIO PU_4790_VIO
#define PU_4796_CTUN PU_4790_CTUN
#define PU_4796_IBIAS PU_4790_IBIAS
#define PU_4796_TR_SIZE PU_4790_TR_SIZE
#define PU_4796_XTALFREQ PU_4790_XTALFREQ
#define PU_4796_ICX_CLKMODE PU_4790_ICX_CLKMODE
#define PU_4796_IC0_CLKO_CURRENT PU_4790_IC0_CLKO_CURRENT
#define PU_4796_ICX_CLKO_CURRENT PU_4790_ICX_CLKO_CURRENT

#define PU_4690_XTALFREQ PU_4790_XTALFREQ
// falcon/demods always driven by tuner clk output on supported daughtercards, these fields do not matter.
#define PU_4690_TR_SIZE 0
#define PU_4690_IBIAS 0
#define PU_4690_IBIAS_RUN 0
#define PU_4690_CTUN 0


#if 0		/* 210510 SGsoft */	
RETURN_CODE add_fw_components(dc_config_chip_type type, struct image_node* components, uint8_t num_components, bool host_load);

RETURN_CODE get_falcon_addr(const struct flash_cfg* cfg, FW_IMAGE_COMPONENT image, uint32_t* addr);

//Performs the LOAD_CONFIG step from Si479xx Programming Guide  values.
RETURN_CODE preBoot_get_loadconfig(DAUGHTERCARD_TYPE dc_type)
{
    RETURN_CODE ret = SUCCESS;
    struct load_config_args* args = NULL;

    switch (dc_type) {
	#if 0		/* 210504 SGsoft */	
    case DC_2DT1F:
        args = load_config_2dt1f;
        break;
    case DC_1DT1T1F:
        args = load_config_1dt1t1f;
        break;
	#endif	/*0*/
    case DC_1T:
    case DC_1T1D_48:
    case DC_1T1D_84:
        SDK_PRINTF("Skipping unnecessary LOAD_CONFIG step for single tuner daughtercards.\n");
        return SUCCESS;

    default:
        return INVALID_INPUT;
    }

    uint8_t chip_count = args[0].cfg.numchips;
    for (int i = 0; i < chip_count; i++)
        ret |= preBoot_flashSetLoadConfig(args[i].ic, args[i].cfg);

    return ret;
}

FW_IMAGE_COMPONENT get_matching_demod_image(RADIO_MODE_TYPE type)
{
    switch (type) {
    case RADIO_MODE_AM:
        return IMAGE_DEMOD_AMHD;
    case RADIO_MODE_FM:
        return IMAGE_DEMOD_FMHD;
    case RADIO_MODE_DAB:
        return IMAGE_DEMOD_DAB;
    default:
        D_ASSERT_MSG(false, "ERROR: no demod mode for radio mode=%d\n", type);
        return IMAGE_UNKNOWN;
    }
}

static RETURN_CODE add_hifi_images(dc_config_chip_select ics, dc_config_chip_type ic_type, bool host_load)
{
    switch (g_evb_system_cfg.audio.routing) {
    case ACFG_HIFI_PORT_ONLY: {
        struct image_node hub_images[] = {
            { .ic_mask = ics, .component = IMAGE_AUDIO_HUB_BASE },
            { .ic_mask = ics, .component = IMAGE_AUDIO_HUB_TOPOLOGY },
            { .ic_mask = ics, .component = IMAGE_AUDIO_HUB_PARAMS },
        };
        return add_fw_components(ic_type, hub_images, ARRAY_SZ(hub_images), host_load);
    }

    case ACFG_HIFI_REF_DESIGN: {
        struct image_node ref_images[] = {
            { .ic_mask = ics, .component = IMAGE_AUDIO_6CH_BASE },
            { .ic_mask = ics, .component = IMAGE_AUDIO_6CH_TOPOLOGY },
            { .ic_mask = ics, .component = IMAGE_AUDIO_6CH_PARAMS },
            { .ic_mask = ics, .component = IMAGE_SWPKG_07_6CHAN },
        };
        // Load the 6 channel reference design firmware.
        return add_fw_components(ic_type, ref_images, ARRAY_SZ(ref_images), host_load);
    }

    case ACFG_TUNERONLY:
        // No hifi image
        break;

    default:
        return INVALID_MODE;
    }
    return SUCCESS;
}

RETURN_CODE add_fw_components(dc_config_chip_type type, struct image_node* components, uint8_t num_components, bool host_load)
{
    if (!components) {
        return INVALID_INPUT;
    }

    for (uint8_t i = 0; i < num_components; i++) {
        RETURN_CODE r = SUCCESS;

		#if 1		/* 210427 SGsoft */
		 uint32_t guid = get_tuner_guid(type, components[i].component);
         if (guid != 0){
             r = preBoot_flashAddGuid(components[i].ic_mask, guid);
         }
		#else
        if (host_load) {
            r = preBoot_addFirmwareImageComponent(components[i].ic_mask, getFilePath_Image(type, components[i].component));
        } else {
            uint32_t guid = get_tuner_guid(type, components[i].component);
            if (guid != 0)
                r = preBoot_flashAddGuid(components[i].ic_mask, guid);
        }
		#endif	/*1*/
        if (r != SUCCESS) {
            return r;
        }
    }
    return SUCCESS;
}
#endif	/*0*/

struct si479xx_powerup_args get_default_pup_args_4790(dc_config_chip_select ic, OPERATION_MODE opmode)
{
    struct si479xx_powerup_args pup = {
        // no harm in enabling even if unused
        .ctsien = SI479XX_CMD_POWER_UP_ARG_CTSIEN_ENUM_DISABLE/*SI479XX_CMD_POWER_UP_ARG_CTSIEN_ENUM_ENABLE*/,	/* 210512 SGsoft */	
        .trsize = PU_4790_TR_SIZE,
        .ctun = PU_4790_CTUN,
        .xtal_freq = PU_4790_XTALFREQ,
        .ibias = PU_4790_IBIAS,
        .chipid = 0,
        .afs = g_evb_system_cfg.opts.powerup_afs,
        .vio = PU_4790_VIO,
        .eziq_master = SI479XX_CMD_POWER_UP_ARG_EZIQ_MASTER_ENUM_MASTER,
        .eziq_enable = SI479XX_CMD_POWER_UP_ARG_EZIQ_ENABLE_ENUM_DISABLE/*SI479XX_CMD_POWER_UP_ARG_EZIQ_ENABLE_ENUM_ONE*/,/* 210429 SGsoft */
	};
    if (ic == IC0) {
        pup.clkout = SI479XX_CMD_POWER_UP_ARG_CLKOUT_ENUM_DISABLE/*SI479XX_CMD_POWER_UP_ARG_CLKOUT_ENUM_ENABLE*/;
        pup.clkmode = SI479XX_CMD_POWER_UP_ARG_CLK_MODE_ENUM_XTAL;
        pup.clko_current = PU_4790_IC0_CLKO_CURRENT;
    } else {
        pup.clkout = SI479XX_CMD_POWER_UP_ARG_CLKOUT_ENUM_DISABLE;
        pup.clkmode = PU_4790_ICX_CLKMODE;
        pup.clko_current = PU_4790_ICX_CLKO_CURRENT;
    }
    return pup;
}

struct si479xx_powerup_args get_default_pup_args_4796(dc_config_chip_select ic, OPERATION_MODE opmode)
{
    // Assumptions: IC0 connected to XTAL, all others receive clk
    struct si479xx_powerup_args pup = {
        // no harm in enabling even if unused
        .ctsien = SI4796X_CMD_POWER_UP_ARG_CTSIEN_ENUM_ENABLE,
        .trsize = PU_4796_TR_SIZE,
        .ctun = PU_4796_CTUN,
        .xtal_freq = PU_4796_XTALFREQ,
        .ibias = PU_4796_IBIAS,
        .chipid = 0,
        .afs = g_evb_system_cfg.opts.powerup_afs,
        .vio = PU_4796_VIO,
        .eziq_master = SI4796X_CMD_POWER_UP_ARG_EZIQ_MASTER_ENUM_MASTER,
        .eziq_enable = SI4796X_CMD_POWER_UP_ARG_EZIQ_ENABLE_ENUM_TWO,
    };

    if (ic == IC0) {
        pup.clkout = SI4796X_CMD_POWER_UP_ARG_CLKOUT_ENUM_ENABLE;
        pup.clkmode = SI4796X_CMD_POWER_UP_ARG_CLK_MODE_ENUM_XTAL;
        pup.clko_current = PU_4796_IC0_CLKO_CURRENT;
    } else {
        pup.clkout = SI4796X_CMD_POWER_UP_ARG_CLKOUT_ENUM_DISABLE;
        pup.clkmode = PU_4796_ICX_CLKMODE;
        pup.clko_current = PU_4796_ICX_CLKO_CURRENT;
    }
    return pup;
}

#if 0		/* 210510 SGsoft */	
RETURN_CODE add_mrc_images(dc_config_chip_select ic_mask, dc_config_chip_type type, OPERATION_MODE opmode)
{
    RETURN_CODE r = SUCCESS;
    if (opmode == OPMODE_HD) {
        if (g_evb_system_cfg.opts.hd_use_mrc) {
            struct image_node mrc_images[] = {
                { .ic_mask = (ic_mask), .component = IMAGE_MRC_HD },
                { .ic_mask = (ic_mask), .component = IMAGE_SWPKG_04_HD_MRC },
            };
            r = add_fw_components(type, mrc_images, ARRAY_SZ(mrc_images), syscfg_is_host_boot());
        }
    } else {
        if (g_evb_system_cfg.opts.dab_use_mrc) {
            struct image_node mrc_images[] = {
                { .ic_mask = (ic_mask), .component = IMAGE_MRC_DAB },
                { .ic_mask = (ic_mask), .component = IMAGE_SWPKG_05_DAB_MRC },
            };
            r = add_fw_components(type, mrc_images, ARRAY_SZ(mrc_images), syscfg_is_host_boot());
        }
    }
    return r;
}
#endif	/*0*/

static RETURN_CODE set_flash_opts()
{
#if 1		/* 210510 SGsoft */	
	return SUCCESS;
#else
    struct flash_image img;
    if (flashcfg_find(&g_flash_cfg, FLASH_FW_TUNER, &img)) {
        return preBoot_flashSetOptions(img.image.address, img.image.size);
    } else {
        SDK_LOG("Tuner flash addr/size unknown!");
        return INVALID_INPUT;
    }
#endif	/*1*/
}

#if 0 		/* 210505 SGsoft */
static RETURN_CODE fw_load_system_2DT1F(OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode)
{
    RETURN_CODE ret = SUCCESS;
    const dc_config_chip_type type = Si4796x;
    bool host_load = syscfg_is_host_boot();

    // Tuner boot setup
    ret = preBoot_startImageSetup();
    // Specify powerup arguments
    struct si479xx_powerup_args pup_args0 = get_default_pup_args_4796(IC0, opmode);
    struct si479xx_powerup_args pup_args1 = get_default_pup_args_4796(IC1, opmode);
    pup_args1.chipid = 1;
    ret |= preBoot_setPowerup(IC0, pup_args0);
    ret |= preBoot_setPowerup(IC1, pup_args1);

    // Chip numbers (affects development boards only)
    if (syscfg_is_audio_tuneronly())
        ret |= preBoot_changePartNumber(IC0, '6', '2', 'G', 'E');
    else
        ret |= preBoot_changePartNumber(IC0, '7', '2', 'G', 'E');

    ret |= preBoot_changePartNumber(IC1, '6', '2', 'G', 'E');

    if (!host_load) {
        ret |= preBoot_get_loadconfig(DC_2DT1F);
        ret |= set_flash_opts();
    }

    // Specify fw components
    if (opmode == OPMODE_HD) {
        struct image_node fw_comps[] = {
            { .ic_mask = (IC0 | IC1), .component = IMAGE_PATCH },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_TUNER_FMAMWB },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_TUNER_FMAMWB_PROP_DEFAULTS },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_SWPKG_01_FM_DIVERSITY },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_SWPKG_02_FMAM_IQ },
        };
        ret |= add_fw_components(type, fw_comps, ARRAY_SZ(fw_comps), host_load);
        ret |= add_hifi_images(IC0, type, host_load);
        ret |= add_mrc_images(IC0 | IC1, type, opmode);

    } else {
        struct image_node fw_comps[] = {
            { .ic_mask = (IC0 | IC1), .component = IMAGE_PATCH },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_TUNER_FMAMDAB },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_TUNER_FMAMDAB_PROP_DEFAULTS },
            { .ic_mask = (IC0 | IC1), .component = IMAGE_SWPKG_11_DRM },
        };
        ret |= add_fw_components(type, fw_comps, ARRAY_SZ(fw_comps), host_load);
        ret |= add_hifi_images(IC0, type, host_load);
        ret |= add_mrc_images(IC1, type, opmode);
    }
    ret |= preBoot_appendChipCompleteBytes(IC0);
    ret |= preBoot_appendChipCompleteBytes(IC1);

    ret |= resetIC(IC0 | IC1 | IC3);
    // Go through tuner boot process with prior configuration settings.
    ret |= multiTunerBoot(IC0 | IC1, !host_load);

    // Boot falcon.
    if (opmode == OPMODE_HD) {
        FW_IMAGE_COMPONENT demod_img = radio_mode == RADIO_MODE_FM ? IMAGE_DEMOD_FMHD : IMAGE_DEMOD_AMHD;
        ret |= fw_load_falcon(IC3, IMAGE_FALCON_HD, demod_img, demod_img);
    } else {
        ret |= fw_load_falcon(IC3, IMAGE_FALCON_DAB, IMAGE_DEMOD_DAB, IMAGE_DEMOD_DAB);
    }
    return ret;
}
#endif	/*0*/

RETURN_CODE fw_load_system_1T(bool is_48pin, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode)						/* 210427 SGsoft */
{
    RETURN_CODE ret = SUCCESS;
    uint8_t afs = g_evb_system_cfg.opts.powerup_afs;
    dc_config_chip_type chip_type = Si479xx;
    bool host_load = syscfg_is_host_boot();
	// get the power up args from eeprom
    bool hifi_capable = !is_48pin;
    bool use_hifi = hifi_capable && !syscfg_is_audio_tuneronly();


#if 1
	ret |= preBoot_startImageSetup();

	struct si479xx_powerup_args pup_args = get_default_pup_args_4790(IC0, opmode);
	ret |= preBoot_setPowerup(IC0, pup_args);

	#if 0
	if (!host_load) {
        RETURN_ERR(set_flash_opts());
    }

	#ifdef OPTION_IMAGETYPE_HD		/* 210503 SGsoft */	
	struct image_node fw_comps[] = {
       { IC0, IMAGE_PATCH },
       { IC0, IMAGE_TUNER_FMAMWB },
       { IC0, IMAGE_TUNER_FMAMWB_PROP_DEFAULTS },
       { IC0, IMAGE_SWPKG_03_WB}		/* 210504 SGsoft */	
    };
	#else
	struct image_node fw_comps[] = {
       { IC0, IMAGE_PATCH },
       { IC0, IMAGE_TUNER_FMAMDAB },
       { IC0, IMAGE_TUNER_SEJIN_PROP_DEFAULTS/*IMAGE_TUNER_FMAMDAB_PROP_DEFAULTS*/ },		/* 210508 SGsoft */
    };
	#endif	/*OPTION_IMAGETYPE_HD*/
	   
    RETURN_ERR(add_fw_components(chip_type, fw_comps, ARRAY_SZ(fw_comps), host_load));
	if (use_hifi){
    	RETURN_ERR(add_hifi_images(IC0, chip_type, host_load));
	}
	#endif	/*1*/
        
	// Reset tuners and load the firmware
    ret |= resetIC(IC0);
    // Must boot IC providing CLKOUT first!
    c_log_comment("Host loading IC0....");
	
    ret |= multiTunerBoot(IC0, !host_load);
    c_log_comment("Host loading IC0 - DONE....");
    ret |= postBoot_emptyImageSetup();
#else
    // get the power up args from eeprom
    bool hifi_capable = !is_48pin;
    bool use_hifi = hifi_capable && !syscfg_is_audio_tuneronly();
    ret |= preBoot_startImageSetup();

    struct si479xx_powerup_args pup_args = get_default_pup_args_4790(IC0, opmode);
    ret |= preBoot_setPowerup(IC0, pup_args);

    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_PATCH));
    if (opmode == OPMODE_DAB) {
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMDAB));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMDAB_PROP_DEFAULTS));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_SWPKG_11_DRM));
    } else if (opmode == OPMODE_HD) {
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMWB));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMWB_PROP_DEFAULTS));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_SWPKG_02_FMAM_IQ));
    }
    if (use_hifi)
        ret |= add_hifi_images(IC0, chip_type, host_load);

    ret |= preBoot_appendChipCompleteBytes(IC0);

    if (use_hifi)
        ret |= preBoot_changePartNumber(IC0, '1', '2', 'G', 'E');
    else
        ret |= preBoot_changePartNumber(IC0, '0', '2', 'G', 'E');

    // Reset tuners and load the firmware
    ret |= resetIC(IC0 | IC3);
    // Must boot IC providing CLKOUT first!
    c_log_comment("Host loading IC0....");
    ret |= multiTunerBoot(IC0, !host_load);
    c_log_comment("Host loading IC0 - DONE....");
    ret |= postBoot_emptyImageSetup();

    demod_id demod = { .ic = IC3, .select = DEMOD_SELECT_SINGLE };

    switch (radio_mode) {
    case RADIO_MODE_AM:
        demod_fw = IMAGE_DEMOD_AMHD;
        break;
    case RADIO_MODE_FM:
        demod_fw = IMAGE_DEMOD_FMHD;
        break;
    case RADIO_MODE_DAB:
        demod_fw = IMAGE_DEMOD_DAB;
        break;
    }
    ret |= fw_load_demod_image(demod, demod_fw);
#endif	/*1*/

    return ret;
}


#if 0		/* 210505 SGsoft */
RETURN_CODE fw_load_system_1T1D(bool is_48pin, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode)
{
    RETURN_CODE ret = SUCCESS;
    uint8_t afs = g_evb_system_cfg.opts.powerup_afs;
    dc_config_chip_type chip_type = Si479xx;
    FW_IMAGE_COMPONENT demod_fw;
    bool host_load = syscfg_is_host_boot();

    // get the power up args from eeprom
    bool hifi_capable = !is_48pin;
    bool use_hifi = hifi_capable && !syscfg_is_audio_tuneronly();
    ret |= preBoot_startImageSetup();

    struct si479xx_powerup_args pup_args = get_default_pup_args_4790(IC0, opmode);
    ret |= preBoot_setPowerup(IC0, pup_args);

    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_PATCH));
    if (opmode == OPMODE_DAB) {
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMDAB));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMDAB_PROP_DEFAULTS));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_SWPKG_11_DRM));
    } else if (opmode == OPMODE_HD) {
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMWB));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_TUNER_FMAMWB_PROP_DEFAULTS));
        ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(chip_type, IMAGE_SWPKG_02_FMAM_IQ));
    }
    if (use_hifi)
        ret |= add_hifi_images(IC0, chip_type, host_load);

    ret |= preBoot_appendChipCompleteBytes(IC0);

    if (use_hifi)
        ret |= preBoot_changePartNumber(IC0, '1', '2', 'G', 'E');
    else
        ret |= preBoot_changePartNumber(IC0, '0', '2', 'G', 'E');

    // Reset tuners and load the firmware
    ret |= resetIC(IC0 | IC3);
    // Must boot IC providing CLKOUT first!
    c_log_comment("Host loading IC0....");
    ret |= multiTunerBoot(IC0, !host_load);
    c_log_comment("Host loading IC0 - DONE....");
    ret |= postBoot_emptyImageSetup();

    demod_id demod = { .ic = IC3, .select = DEMOD_SELECT_SINGLE };

    switch (radio_mode) {
    case RADIO_MODE_AM:
        demod_fw = IMAGE_DEMOD_AMHD;
        break;
    case RADIO_MODE_FM:
        demod_fw = IMAGE_DEMOD_FMHD;
        break;
    case RADIO_MODE_DAB:
        demod_fw = IMAGE_DEMOD_DAB;
        break;
    }
    ret |= fw_load_demod_image(demod, demod_fw);

    return ret;
}

RETURN_CODE fw_load_system_1DT1T1F(OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode)
{
    RETURN_CODE ret = SUCCESS;
    uint8_t afs = g_evb_system_cfg.opts.powerup_afs;
    bool host_load = syscfg_is_host_boot();

    if (opmode == OPMODE_DAB) {
        SDK_ASSERT(0, "DAB bootup not implemented for 1DT1T1F board.\n");
    }
    // Reset ICs and load the firmware
    ret |= resetIC(IC0 | IC1 | IC3);

    struct si479xx_powerup_args pup_args0 = get_default_pup_args_4796(IC0, opmode);
    struct si479xx_powerup_args pup_args1 = get_default_pup_args_4790(IC1, opmode);

    ret |= preBoot_startImageSetup();
    ret |= preBoot_setPowerup(IC0, pup_args0);
    ret |= preBoot_setPowerup(IC1, pup_args1);
    if (!host_load) {
        ret |= preBoot_get_loadconfig(DC_1DT1T1F);
        ret |= set_flash_opts();
    }

    ret |= preBoot_changePartNumber(IC1, '5', '2', 'G', 'E');
    if (syscfg_is_audio_tuneronly())
        ret |= preBoot_changePartNumber(IC0, '6', '2', 'G', 'E');
    else
        ret |= preBoot_changePartNumber(IC0, '7', '2', 'G', 'E');

    // GE patch must come before DE patch
    // IC1 is 4795 (single tuner)
    ret |= preBoot_addFirmwareImageComponent(IC1, getFilePath_Image(Si479xx, IMAGE_PATCH));
    ret |= preBoot_addFirmwareImageComponent(IC1, getFilePath_Image(Si479xx, IMAGE_TUNER_FMAMWB));
    ret |= preBoot_addFirmwareImageComponent(IC1, getFilePath_Image(Si479xx, IMAGE_TUNER_FMAMWB_PROP_DEFAULTS));
    // IC0 is 4796 (dual tuner)
    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(Si4796x, IMAGE_PATCH));
    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(Si4796x, IMAGE_TUNER_FMAMWB));
    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(Si4796x, IMAGE_TUNER_FMAMWB_PROP_DEFAULTS));
    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(Si4796x, IMAGE_SWPKG_01_FM_DIVERSITY));
    ret |= preBoot_addFirmwareImageComponent(IC0, getFilePath_Image(Si4796x, IMAGE_SWPKG_02_FMAM_IQ));
    ret |= add_hifi_images(IC0, Si4796x, host_load);
    ret |= add_mrc_images(IC0, Si4796x, OPMODE_HD);
    ret |= preBoot_appendChipCompleteBytes(IC0);
    ret |= preBoot_appendChipCompleteBytes(IC1);

    ret |= multiTunerBoot(IC0 | IC1, !host_load);

    // Boot falcon.
    FW_IMAGE_COMPONENT demod_img = radio_mode == RADIO_MODE_FM ? IMAGE_DEMOD_FMHD : IMAGE_DEMOD_AMHD;
    ret |= fw_load_falcon(IC3, IMAGE_FALCON_HD, demod_img, demod_img);

    return ret;
}
#endif	/*0*/

RETURN_CODE fw_load_system_cfg(DAUGHTERCARD_TYPE dc_type, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode, const char* flash_cfg)
{
    RETURN_CODE ret = SUCCESS;
	#if 0		/* 210510 SGsoft */	
    if (syscfg_is_flash_boot() /*&& flash_cfg*/) {
        ret = flashcfg_read_csv(flash_cfg, &g_flash_cfg);
        if (ret != SUCCESS) {
            SDK_PRINTF("could not load flash configuration");
            return ret;
        }
    }
	#endif	/*0*/

    //firmware loading is very board specific,
    // so not much effort was put into sharing code....
    switch (dc_type) {
	#if 0 		/* 210505 SGsoft */
    case DC_2DT1F:
        ret = fw_load_system_2DT1F(opmode, radio_mode);
        break;

    case DC_1DT1T1F:
        ret = fw_load_system_1DT1T1F(opmode, radio_mode);
        break;

    case DC_1T1D_84:
        ret = fw_load_system_1T1D(false, opmode, radio_mode);
        break;

    case DC_1T1D_48:
        ret = fw_load_system_1T1D(true, opmode, radio_mode);
        break;
	#endif	/*0*/

	case DC_1T:															
        ret = fw_load_system_1T(true, opmode, radio_mode);
        break;

    default:
        SDK_PRINTF("ERROR: unsupported DC type=0x%x, cannot boot.", dc_type);
        return UNSUPPORTED_FUNCTION;
    }

    c_log_comment("BOOT STATUS:");
    c_log_comment(ret == SUCCESS ? " OK!" : " FAILED?");

    return ret;
}

RETURN_CODE fw_load_system(DAUGHTERCARD_TYPE dc_type, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode)
{
    return fw_load_system_cfg(dc_type, opmode, radio_mode, NULL/*getFilePath_FlashCfg(dc_type, opmode)*/);		/* 210427 SGsoft */
}

#if 0 		/* 210505 SGsoft */
static RETURN_CODE get_demod_flash_addr(const struct flash_cfg* cfg, FW_IMAGE_COMPONENT image, uint32_t* addr)
{
    if (!cfg || !addr) {
        SDK_PRINTF("invalid arg");
        return INVALID_INPUT;
    }

    enum FLASH_FW_IMAGE flash_type;
    switch (image) {
    case IMAGE_DEMOD_PATCH:
        flash_type = FLASH_FW_DEMOD_PATCH;
        break;
    case IMAGE_DEMOD_DAB:
        flash_type = FLASH_FW_DEMOD_DAB;
        break;
    case IMAGE_DEMOD_FMHD:
        flash_type = FLASH_FW_DEMOD_FMHD;
        break;
    case IMAGE_DEMOD_AMHD:
        flash_type = FLASH_FW_DEMOD_AMHD;
        break;
    default:
        return INVALID_INPUT;
    }

    struct flash_image img;
    bool ok = flashcfg_find(cfg, flash_type, &img);
    if (ok)
        *addr = img.image.address;

    return ok ? SUCCESS : INVALID_INPUT;
}

RETURN_CODE fw_load_demod_image(demod_id demod, FW_IMAGE_COMPONENT mode)
{
    RETURN_CODE ret = SUCCESS;
#if 0		/* 210428 SGsoft */

    switch (mode) {
    case IMAGE_DEMOD_DAB:
    case IMAGE_DEMOD_AMHD:
    case IMAGE_DEMOD_FMHD:
        // valid
        break;
    default:
        return INVALID_INPUT;
    }

    // Have to check if the demod is standalone (si461x),
    //  or a falcon satellite demod (si4690)
    if (demod.select == DEMOD_SELECT_SINGLE) {
        // standalone demod
        ret = resetIC(demod.ic);
        if (ret != SUCCESS)
            return ret;
        if (syscfg_is_host_boot()) {
            ret = host_boot_demod(demod.ic, NULL, mode);
        } else {
            uint32_t addr = 0;
            if (SUCCESS != get_demod_flash_addr(&g_flash_cfg, mode, &addr))
                return INVALID_INPUT;

            uint8_t chip_index;
            ret = get_ic_num(demod.ic, &chip_index);
            if (ret != SUCCESS)
                return ret;

            ret = flash_boot_demod(demod.ic, NULL, addr);
        }
    } else {
        if (syscfg_is_host_boot()) {
            ret |= host_reboot_satellite(demod.ic, demod.select, mode);
        } else {
            SDK_PRINTF("Rebooting satellite demod!\n");
            uint32_t addr_patch, addr_image;
            if (SUCCESS != get_falcon_addr(&g_flash_cfg, IMAGE_DEMOD_PATCH, &addr_patch))
                return INVALID_INPUT;
            if (SUCCESS != get_falcon_addr(&g_flash_cfg, mode, &addr_image))
                return INVALID_INPUT;

            uint8_t chip_index;
            ret = get_ic_num(demod.ic, &chip_index);
            if (ret != SUCCESS)
                return ret;
            if (demod.select == DEMOD_SELECT_BOTH) {
                ret |= flashReboot_4690_satellite(demod.ic, DEMOD_SELECT1, 1, addr_patch, addr_image);
                ret |= flashReboot_4690_satellite(demod.ic, DEMOD_SELECT2, 1, addr_patch, addr_image);
            } else {
                ret |= flashReboot_4690_satellite(demod.ic, demod.select, 1, addr_patch, addr_image);
            }
        }
    }
#endif	/*0*/

    return ret;
}

RETURN_CODE get_falcon_addr(const struct flash_cfg* cfg, FW_IMAGE_COMPONENT image, uint32_t* addr)
{
#if 1		/* 210428 SGsoft */
	return SUCCESS;
#else
    if (!cfg || !addr) {
        SDK_PRINTF("invalid arg");
        return INVALID_INPUT;
    }

    enum FLASH_FW_IMAGE flash_type;
    switch (image) {
    case IMAGE_FALCON_DAB:
        flash_type = FLASH_FW_LINKER_DAB;
        break;
    case IMAGE_FALCON_HD:
        flash_type = FLASH_FW_LINKER_HD;
        break;
    case IMAGE_DEMOD_PATCH:
        flash_type = FLASH_FW_DEMOD_PATCH;
        break;
    case IMAGE_DEMOD_DAB:
        flash_type = FLASH_FW_SAT_DAB;
        break;
    case IMAGE_DEMOD_FMHD:
        flash_type = FLASH_FW_SAT_FMHD;
        break;
    case IMAGE_DEMOD_AMHD:
        flash_type = FLASH_FW_SAT_AMHD;
        break;
    default:
        flash_type = image;
        break;
    }

    struct flash_image img;
    bool ok = flashcfg_find(cfg, flash_type, &img);
    if (ok)
        *addr = img.image.address;

    return ok ? SUCCESS : INVALID_INPUT;
#endif	/*1*/
}

RETURN_CODE fw_load_falcon(dc_config_chip_select ic, FW_IMAGE_COMPONENT linker_image, FW_IMAGE_COMPONENT sat1_image, FW_IMAGE_COMPONENT sat2_image)
{
    RETURN_CODE ret = 0;
#if 0		/* 210428 SGsoft */
    if (syscfg_is_host_boot()) {
        // Use default powerup args.
        ret = host_load_falcon(ic, NULL, linker_image, sat1_image, sat2_image);
    } else {
        // Must first determine the flash addresses of the specified image components.
        uint32_t patch_addr, linker_addr, sat1_addr, sat2_addr;

        ret |= get_falcon_addr(&g_flash_cfg, FLASH_FW_DEMOD_PATCH, &patch_addr);
        ret |= get_falcon_addr(&g_flash_cfg, linker_image, &linker_addr);
        ret |= get_falcon_addr(&g_flash_cfg, sat1_image, &sat1_addr);
        ret |= get_falcon_addr(&g_flash_cfg, sat2_image, &sat2_addr);
        if (ret != SUCCESS) {
            SDK_PRINTF("Did not find all falcon image component flash addresses.\n");
            return INVALID_INPUT;
        }
        ret = flash_boot_falcon(ic, patch_addr, linker_addr, sat1_addr, sat2_addr, NULL);
    }
#endif	/*0*/
    return ret;
}


RETURN_CODE fw_current_demod_image(demod_id demod, FW_IMAGE_COMPONENT* image_type)
{
    if (!image_type)
        return INVALID_INPUT;

    *image_type = IMAGE_UNKNOWN;

    RETURN_CODE ret = SUCCESS;
    uint8_t image;
    ret |= dab_demod_get_sys_state(demod, &image);
    if (ret != SUCCESS)
        return ret;

    switch (image) {
    case SI4690_DAB_CMD_GET_SYS_STATE_REP_IMAGE_ENUM_BL:
        SDK_PRINTF("WARN: DEMOD is BOOTLOADER?");
        *image_type = IMAGE_DEMOD_PATCH;
        break;
    case SI4690_DAB_CMD_GET_SYS_STATE_REP_IMAGE_ENUM_FMHD_DEMOD:
        *image_type = IMAGE_DEMOD_FMHD;
        break;
    case SI4690_DAB_CMD_GET_SYS_STATE_REP_IMAGE_ENUM_AMHD_DEMOD:
        *image_type = IMAGE_DEMOD_AMHD;
        break;
    case SI4690_DAB_CMD_GET_SYS_STATE_REP_IMAGE_ENUM_DAB_DEMOD:
        *image_type = IMAGE_DEMOD_DAB;
        break;

    // TODO : need to update to distinguish between DAB/HD images ?
    case SI4690_DAB_CMD_GET_SYS_STATE_REP_IMAGE_ENUM_SEAMLESS_LINKER:
        *image_type = IMAGE_FALCON_DAB;
        break;

    case SI4690_DAB_CMD_GET_SYS_STATE_REP_IMAGE_ENUM_TDMB_DAB_DATA_ONLY:
    default:
        break;
    }

    return SUCCESS;
}
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
