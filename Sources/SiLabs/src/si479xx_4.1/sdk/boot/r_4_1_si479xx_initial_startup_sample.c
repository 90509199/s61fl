/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_init_startup_h_global_
	
#include "main.h"

#if 0
#include "boot/si479xx_initial_startup_sample.h"
#include "boot/Firmware_Load_Helpers.h"
#include "platform_types/platform_options.h"

#include "HAL.h"
#include "common_types.h"
#include "osal/misc.h"
#include "core/c_core.h"
#include "core/c_util.h"

#include "apis/si479xx_flash_api.h"
#include "apis/si46xx_common.h"
#include "apis/si4796x_firmware_api.h"
#include "apis/si4796x_firmware_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "util/sdk_macro.h"
#endif	/*0*/

#include <string.h>

#if defined(OPTION_BOOT_FROM_HOST)		/* 210429 SGsoft */
///#include "sejin_si4790x_image_4_1_4_flash_util_210427.h"
///#include "sejin_hd_si4790x_image_4_1_4_210503.h"				/* 210503 SGsoft */
///#include "sejin_hd_wb_si4790x_image_4_1_4_210504.h"
///#include "sejin_si4790x_image_4_1_4_china_210504.h"				/* 210504 SGsoft */
///#include "sejin_si4790x_image_4_1_4_na_210504.h"
//#include "sejin_create_si4790x_image_4_1_4_sejin_default_210508.h"	/* 210508 SGsoft */	
#include "sejin_create_si4790x_image_4_1_4_sejin_default_210510.h"		/* 210510 SGsoft */	
#endif	/*OPTION_BOOT_FROM_HOST*/


static RETURN_CODE powerup_479xx(dc_config_chip_select ic, dc_config_chip_type type,
        struct si479xx_powerup_args* powerup_args, uint8_t* uncorrectable, uint8_t* correctable);

static RETURN_CODE load_init_479x(dc_config_chip_select icNum, uint8_t count, uint32_t* guidBuffer);

static RETURN_CODE load_config_payload(dc_config_chip_select ic, struct flash_load_config_element cfg, bool connected_to_flash);

static RETURN_CODE host_load_worker(dc_config_chip_select icNum, uint32_t imageBufferSize, uint8_t* imageBuffer);

static RETURN_CODE powerup(dc_config_chip_select ic,
    uint8_t ctsien, uint8_t clkout, uint8_t clkmode, uint8_t trsize, uint8_t ctun, uint32_t xtalfreq, uint8_t ibias,
    uint8_t afs, uint8_t chipid,
    uint8_t clko_current, uint8_t vio, 
    uint8_t eziq_master, uint8_t eziq_enable,
    uint8_t* uncorrectable, uint8_t* correctable)
{
    RETURN_CODE ret = SUCCESS;
    SI479XX_power_up__data pup_reply = { 0 };

    //Quick error check
    if ((ctsien > 1) || (clkout > 1) || (afs > 1) || (chipid > 7) || (clko_current > 7) || (vio > 1) )
    {
        return INVALID_INPUT;
    }

    ret = SI479XX_power_up(ic, 
        ctsien, clko_current, vio, clkout, clkmode, trsize, ctun, xtalfreq,
        ibias, afs, chipid, eziq_master, eziq_enable,
         &pup_reply);
    if (ret != SUCCESS)
        return ret;

    if (uncorrectable != NULL)
        *uncorrectable = pup_reply.UNCORRECTABLE;
    if (correctable != NULL)
        *correctable = pup_reply.CORRECTABLE;

    if (pup_reply.UNCORRECTABLE) {
        SDK_PRINTF("ERROR: powerup uncorrectable errors detected: %d, part damaged!\n", pup_reply.UNCORRECTABLE);
        return INVALID_MODE;
    }
    SI479XX_read_status__data status = { 0 };
    ret = SI479XX_read_status(ic, &status);
    if (ret != SUCCESS)
        return ret;

    if (status.PUP_STATE != SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTLOADER) {
        SDK_PRINTF("ERROR:PUP_STATE didn't transition to 'bootloader' after powerup\n");
        return INVALID_MODE;
    }

    return SUCCESS;
}


static RETURN_CODE powerup_479xx(dc_config_chip_select ic, dc_config_chip_type type, struct si479xx_powerup_args* powerup_args, uint8_t* uncorrectable, uint8_t* correctable)
{
    if (!powerup_args) {
        return INVALID_INPUT;
    }

    switch (type) {
    case Si4796x:
    case Si479xx: {
        // powerup args currently the same
        const struct si479xx_powerup_args* pargs = powerup_args;
        return powerup(ic,
            pargs->ctsien, pargs->clkout, pargs->clkmode,
            pargs->trsize, pargs->ctun, pargs->xtal_freq, pargs->ibias,
            pargs->afs, pargs->chipid, pargs->clko_current,
            pargs->vio, pargs->eziq_master, pargs->eziq_enable, uncorrectable, correctable);
        break;
    }

    default:
        D_ASSERT_MSG(false, "ERROR: invalid chip type=%d\n", type);
        return INVALID_INPUT;
    }
}

// Boot with some status checks.
static RETURN_CODE boot_worker(dc_config_chip_select ic_mask)
{
    RETURN_CODE ret = SUCCESS;

    // Sometimes takes a bit longer for BOOTREADY...
    const uint16_t max_tries = 100;

    // Assumes IC0 is connected to flash, increasing numbers chain towards flash
    for (int8_t i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
        dc_config_chip_select ic = (1 << i);
        if ((ic & ic_mask) == 0)
            continue;

        SI479XX_read_status__data status = {0};
        for (uint16_t tries = 0; tries < max_tries; tries++) {
            ret = SI479XX_read_status(ic, &status);
            if (ret != SUCCESS)
                return ret;

            if (status.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTREADY)
                break;

            // not ready yet...
            os_delay(1);
        }

        if (status.PUP_STATE != SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTREADY) {
            //PUP_STATE should be 'boot ready' when ready for the 'boot' command
            SDK_PRINTF("ERROR: chip 0x%x not BOOT READY.\n", ic);
            return ret | INVALID_MODE;
        }

        ret = SI4796X_boot(ic);
        if (ret != SUCCESS) {
            SDK_PRINTF("ERR: chip 0x%x BOOT error (0x%x)!\n", ic, ret);
            return ret;
        }
    }

    // Validate that all chips BOOTed successfully...
    for (int8_t i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
        dc_config_chip_select ic = (1 << i);
        if ((ic & ic_mask) == 0)
            continue;

        SI479XX_read_status__data status = {0};
        ret = SI479XX_read_status(ic, &status);
        if (ret != SUCCESS) {
            SDK_PRINTF("ERR: failed to get status word post-BOOT (%d), chip=0x%x\n", ret, ic);
            return ret;
        }
        if (status.PUP_STATE != SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_APPLICATION) {
            // PUP_STATE should be 'application' once booted
            SDK_PRINTF("ERROR: chip 0x%x PUP_STATE (%d) not APPLICATION after BOOT..\n", ic, status.PUP_STATE);
            return ret | INVALID_MODE;
        }
    }


    return ret;
}

#define LOAD_INIT__MAX_NUM_GUID 20
static RETURN_CODE load_init_479x(dc_config_chip_select ic, uint8_t count, uint32_t* guidBuffer)
{
    RETURN_CODE ret = SUCCESS;

    //Error check for an invalid number of items
    if ((count < 1) || (count > LOAD_INIT__MAX_NUM_GUID))
        return INVALID_INPUT;

    ret |= SI479XX_load_init(ic, count, guidBuffer);

    return ret;
}

// Load config worker for when loading config from EEPROM
// Will TIMEOUT on chips that aren't directly connected to the flash chip.
static RETURN_CODE load_config_payload(dc_config_chip_select ic, struct flash_load_config_element cfg, bool connected_to_flash)
{
    RETURN_CODE ret = SUCCESS;
    uint8_t cmd[SI4796X_CMD_LOAD_CONFIG_LENGTH] = { 0 };
    uint16_t payload_bytelen = cfg.payload_word_count * sizeof(uint32_t);
    uint16_t cmdlen = payload_bytelen + 4;
    // little endian payload
    uint8_t le_payload[SI4796X_CMD_LOAD_CONFIG_LENGTH] = { 0 };

    cmd[0] = 0x0c;
    cmd[1] = cfg.numchips;
    cmd[2] = cfg.chipid;
    cmd[3] = cfg.role;

    convert_array_to_le((uint8_t*)cfg.payload, cfg.payload_word_count, 4, le_payload);
    memcpy((cmd + 4), le_payload, payload_bytelen);

    //NOTE: LOAD_CONFIG is weird command -> will only return CTS on
    // the IC connected to flash (see note in programming guide).
    // So, in this specific case, we expect a timeout error from the wait_for_CTS()
    if (connected_to_flash) {
        ret = writeCommand(ic, cmdlen, cmd);
    } else {
        // Short timeout because it will timeout on chips not connected to flash.
        ret = writeCommand_wait(ic, cmdlen, cmd, 10);
        CLEAR_BITS(ret, TIMEOUT);
    }

    return ret;
}

#define HOST_LOAD__LENGTH 4
#define HOST_LOAD__PACKET_MAX_SIZE (MIN(SI479XX_CMD_BUFFER_LENGTH, 4096) - HOST_LOAD__LENGTH)
static RETURN_CODE host_load_worker(dc_config_chip_select ic_mask, uint32_t imageBufferSize, uint8_t* imageBuffer)
{
    RETURN_CODE ret = SUCCESS;
    uint32_t remaining = imageBufferSize;
    uint32_t offset = 0;
    uint16_t packet_sz;

    // Host_Load image using max packet size
    while (remaining > 0) {
        if (remaining >= HOST_LOAD__PACKET_MAX_SIZE)
            packet_sz = HOST_LOAD__PACKET_MAX_SIZE;
        else
            packet_sz = (uint16_t)remaining;

        // Currently hardware (spidev driver), and generated API does not support 'broadcast',
        //   so we have to duplicate the commands to all tuners individually.
        for (uint8_t i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
            dc_config_chip_select ic = (1<<i) & ic_mask;
            if (ic == 0) 
                continue; 

            ret = SI479XX_host_load(ic, packet_sz, &imageBuffer[offset]);
            if (ret != SUCCESS) 
                return ret;
        }

        remaining -= packet_sz;
        offset += packet_sz;
    }

    return ret;
}

//*************************************************************************
// New Bootloader Support for the ROM1 of 4790
//*************************************************************************
static struct tuner_multiboot_cfg* s_boot = NULL;
//------------------------------------------------------------------

//Function to clear any preboot objects and start fresh
RETURN_CODE preBoot_startImageSetup()
{
    int32_t i = 0;

    postBoot_emptyImageSetup();
    s_boot = malloc(sizeof(struct tuner_multiboot_cfg));
    if (s_boot == NULL)
        return MEMORY_ERROR;

    s_boot->image.num_components = 0;
    memset(s_boot->image.components, 0, sizeof(s_boot->image.components));

    s_boot->image.key.length = 0;
    memset(s_boot->image.key.buffer, 0, sizeof(s_boot->image.key.buffer));

	#if 0		/* 210429 SGsoft */
    s_boot->image.image_len = 0;
    memset(s_boot->image.image_buffer, 0, sizeof(s_boot->image.image_buffer));
	#endif	/*0*/

    for (i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
        s_boot->chips[i].load_cfg.numchips = 0;
        s_boot->chips[i].load_cfg.payload_word_count = 0;
    }

    for (i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
        s_boot->chips[i].part_override.cust_major = 0;
        s_boot->chips[i].part_override.cust_minor = 0;
        s_boot->chips[i].part_override.pn_major = 0;
        s_boot->chips[i].part_override.pn_minor = 0;
    }

    return SUCCESS;
}

//Function to clear any preboot objects
RETURN_CODE postBoot_emptyImageSetup()
{
    if (s_boot != NULL) {
        free(s_boot);
        s_boot = NULL;
    }

    return SUCCESS;
}

RETURN_CODE preBoot_flashSetLoadConfig(dc_config_chip_select ic, struct flash_load_config_element cfg)
{
    if (s_boot == NULL)
        return INVALID_MODE;
    if (cfg.payload == NULL)
        return INVALID_INPUT;

    uint8_t bootIndex = 0;
    if (get_ic_num(ic, &bootIndex) != SUCCESS)
        return INVALID_INPUT;

    s_boot->chips[bootIndex].load_cfg = cfg;
    return SUCCESS;
}

//Function to load a firmware image component
// (mcu image, hifi image, properties/servo settings, topology, topology module settings, swpkgs)
//into memory for sending to the part
RETURN_CODE preBoot_addFirmwareImageComponent(dc_config_chip_select ic_mask, uint8_t* filepathimage)
{
#if 0		/* 210429 SGsoft */
    if (!filepathimage)
        return INVALID_INPUT;
    if (s_boot == NULL)
        return INVALID_MODE;
    if (s_boot->image.num_components >= FIRMWARE_COMPONENT_MAX_COUNT) {
        SDK_PRINTF("ERROR: reached max firmware component count (%d)\n", FIRMWARE_COMPONENT_MAX_COUNT);
        return INVALID_INPUT;
    }

    FILE* fp = fopen((char*)filepathimage, "rb");
    if (!fp) {
        SDK_PRINTF("ERROR: failed to open firmware file '%s'\n", filepathimage);
        return INVALID_INPUT;
    }

    bool is_patch = false;
    uint32_t guid = read_si479xx_guid(filepathimage, &is_patch);
    if (guid == 0 && !is_patch) {
        // Regular (non-patch) files must contain GUID....
        fclose(fp);
        return INVALID_INPUT;
    }

    uint8_t* file_buf = malloc(MAX_SIZE_IMAGE_BUFFER_BYTES);
    if (!file_buf)  {
        fclose(fp);
        return MEMORY_ERROR;
    }
    uint32_t file_buf_sz = fread(file_buf, 1, MAX_SIZE_IMAGE_BUFFER_BYTES, fp);
    fclose(fp);

    //Copy image to main image buffer
    memcpy(&s_boot->image.image_buffer[s_boot->image.image_len], file_buf, file_buf_sz);
    s_boot->image.image_len += file_buf_sz;
    free(file_buf);

    // Patches do not have GUIDs
    if (!is_patch) {
        // Add GUID to guid table with specified ic
        bool guid_present = false;
        for (int32_t i = 0; i < s_boot->image.num_components; i++) {
            if (s_boot->image.components[i].guid == guid) {
                // check version...
                s_boot->image.components[s_boot->image.num_components++].ic_mask |= ic_mask;
                guid_present = true;
                break;
            }
        }

        if (!guid_present) {
            s_boot->image.components[s_boot->image.num_components].guid = guid;
            s_boot->image.components[s_boot->image.num_components].ic_mask = ic_mask;
            s_boot->image.num_components++;
            // add version
        }
    }
#endif	/*0*/
    return SUCCESS;
}

//Function to add guid to boot element object for flash loading.
RETURN_CODE preBoot_flashAddGuid(dc_config_chip_select ic_mask, uint32_t guid)
{
#if 0		/* 210510 SGsoft */	
    // Add GUID to guid table with specified ic
    for (int32_t i = 0; i < s_boot->image.num_components; i++) {
        if (s_boot->image.components[i].guid == guid) {
            s_boot->image.components[i].ic_mask |= ic_mask;
            return SUCCESS;
        }
    }
    if (s_boot->image.num_components < FIRMWARE_COMPONENT_MAX_COUNT) {
        s_boot->image.components[s_boot->image.num_components].guid = guid;
        s_boot->image.components[s_boot->image.num_components].ic_mask = ic_mask;
        s_boot->image.num_components++;
        return SUCCESS;
    } else if (s_boot->image.num_components >= FIRMWARE_COMPONENT_MAX_COUNT) {
        return INVALID_INPUT;
    }
#endif	/*0*/
    return SUCCESS;
}

RETURN_CODE preBoot_changePartNumber(dc_config_chip_select ic_mask, uint8_t pn_major_ascii,
    uint8_t pn_minor_ascii, uint8_t cust_major_ascii, uint8_t cust_minor_ascii)
{
    for (int32_t i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
        if ((ic_mask & (1 << i)) == 0)
            continue;

        s_boot->chips[i].part_override.pn_major = pn_major_ascii;
        s_boot->chips[i].part_override.pn_minor = pn_minor_ascii;
        s_boot->chips[i].part_override.cust_major = cust_major_ascii;
        s_boot->chips[i].part_override.cust_minor = cust_minor_ascii;
    }
    return SUCCESS;
}

// Function to load a customer encryption key blob file to enable decryption of a customer encrypted image
// While the file is selected before loading firmware, the application of the image is during the loading process.
RETURN_CODE preBoot_addEncryptionKey(dc_config_chip_select ic, uint8_t* filepathimage)
{
#if 0		/* 210510 SGsoft */	
    uint32_t i = 0;
    FILE* fp;
    uint8_t fileReadBuffer[ENCRYPTION_KEY__MAX_FILE_LENGTH];
    uint32_t numRead;
    char byteBuf[3];
    uint16_t tempHexVal;
    int bufferCount = 0;

    if (s_boot == NULL)
        return INVALID_MODE;

    fp = fopen((char*)filepathimage, "r");
    if (fp == NULL)
        return INVALID_INPUT;

    numRead = fread(fileReadBuffer, 1, ENCRYPTION_KEY__MAX_FILE_LENGTH, fp);
    fclose(fp);

    byteBuf[2] = '\0';

    if ((numRead > 0) && (numRead <= ENCRYPTION_KEY__MAX_FILE_LENGTH)) {
        //Copy image to encryption buffer
        for (i = 0; i < numRead; i += 2) {
            byteBuf[0] = fileReadBuffer[i];
            byteBuf[1] = fileReadBuffer[i + 1];
            sscanf(byteBuf, "%hx", &tempHexVal);
            s_boot->image.key.buffer[bufferCount++] = (uint8_t)(tempHexVal & 0xFF);
        }
        s_boot->image.key.length += bufferCount;
    } else {
        return INVALID_INPUT;
    }

    //************************************************
#endif	/*0*/

    return SUCCESS;
}


// Function to append chip complete bytes to current end of firmware buffer.
// These bytes tell the selected chip to stop listening to the load and get ready to boot.
RETURN_CODE preBoot_appendChipCompleteBytes(dc_config_chip_select ic_mask)
{
    RETURN_CODE ret = SUCCESS;
#if 0		/* 210429 SGsoft */
    // chipid is complete_flag[8]
    const uint8_t chipid_index = 8;
    uint8_t complete_flag[] = { 0x1a, 0x1a, 0x1a, 0x1a, 6, 4, 0, 0, 0, 0, 0, 0 };
    if (s_boot == NULL)
        return INVALID_MODE;

    for (uint8_t i = 0; i < OPTION_HAL_MAX_CHIPS; i++) {
        uint8_t ic = (1 << i);
        if ((ic & ic_mask) == 0)
            continue;

        complete_flag[chipid_index] = i;

        // Append to end of the fw image buffer.
        memcpy(&s_boot->image.image_buffer[s_boot->image.image_len],
            complete_flag, sizeof(complete_flag));

        s_boot->image.image_len += sizeof(complete_flag);
    }
#endif	/*0*/
    return ret;
}


RETURN_CODE preBoot_setPowerup(dc_config_chip_select ic, struct si479xx_powerup_args powerup)
{
    uint8_t chip_index = 0;
    RETURN_CODE r;
    if (SUCCESS != (r = get_ic_num(ic, &chip_index)))
        return r;

    if (s_boot == NULL)
        return INVALID_MODE;

    s_boot->chips[chip_index].powerup = powerup;
    return SUCCESS;
}


RETURN_CODE multiTunerBoot(dc_config_chip_select ic_mask, bool flash_boot)
{
    RETURN_CODE ret = SUCCESS;
    uint8_t uncorrectable, correctable;
    dc_config_chip_select ic_flash = IC0;
	///SI479XX_get_part_info__data pi[OPTION_HAL_MAX_CHIPS];										/* 210429 SGsoft */
	///SI479XX_get_func_info__data fi[OPTION_HAL_MAX_CHIPS];										

	
    if (s_boot == NULL)
        return INVALID_MODE;

	if (!flash_boot) {										/* 210510 SGsoft */
		#if 0
        s_boot->flash.address = addr;
    	s_boot->flash.size = size;
		#endif	/*0*/
    }

#ifdef OPTION_IMAGETYPE_DAB
	s_boot->image.components[0].guid = 0x00000B;
	s_boot->image.components[0].ic_mask = IC0;

	s_boot->image.components[1].guid = 0x800001;
	s_boot->image.components[1].ic_mask = IC0;
	
	s_boot->image.num_components = 2;
#endif	/*OPTION_IMAGETYPE_DAB*/
	
	#if 0
    if (s_boot->image.image_len <= 0 && !flash_boot) {
        SDK_PRINTF("ERROR: no fw image created -> call preBoot_addFirmwareImageComponent() first!\n");
        return FIRMWARE_IMAGE_INVALID;
    }
	#endif	/*0*/

    // Commands before BOOT usually take longer to raise CTS...
    uint32_t orig_cts = getCtsTimeout();
    setCtsTimeout(2000);

    // Start from largest chip, which is assumed to be 'southmost' chip connected to flash.
    // LOAD_CONFIG should be sent to flash connected chip first.
    for (int32_t i = OPTION_HAL_MAX_CHIPS; i >= 0; i--) {
        dc_config_chip_select ic = (1 << i);
        if ((ic & ic_mask) == 0)
            continue;

        if (ic > ic_flash)
            ic_flash = ic;

        // Currently no difference between 90/96 powerup format, only in possible values.
        ret = powerup_479xx(ic, Si479xx, &s_boot->chips[i].powerup, &uncorrectable, &correctable);
        if (ret != SUCCESS) {
            SDK_PRINTF("ERROR: powerup failed (%d) for chip 0x%x\n", ret, ic);
            goto exit;
        }

		///ret |= SI479XX_get_part_info(ic, &pi[ic]);						/* 210429 SGsoft */

		#if 0		/* 210427 SGsoft */
        struct part_override_element* override = &s_boot->chips[i].part_override;
        if (override->pn_major != 0 && override->pn_minor != 0) {
            ret = SI479XX_part_override(ic,
                override->pn_major, override->pn_minor,
                'A', '2',
                override->cust_major, override->cust_minor);

            if (ret != SUCCESS) {
                SDK_PRINTF("ERROR: part override failed (%d) for chip 0x%x\n", ret, ic);
                goto exit;
            }
        }
		#endif	/*0*/

        if (s_boot->image.key.length > 0) {
            ret = SI479XX_key_exchange(ic, s_boot->image.key.buffer, s_boot->image.key.length);
            if (ret != SUCCESS) {
                SDK_PRINTF("ERROR: key exchange failed (%d) for chip 0x%x\n", ret, ic);
                goto exit;
            }
        }

        uint32_t guid_count = 0;
        uint32_t guids[FIRMWARE_COMPONENT_MAX_COUNT] = { 0 };
        for (int32_t i = 0; i < s_boot->image.num_components; i++) {
            if (ic & s_boot->image.components[i].ic_mask) {
                guids[guid_count++] = s_boot->image.components[i].guid;
            }
        }
        if (guid_count <= 0) {
            SDK_PRINTF("ERROR: no GUIDs specified for chip 0x%x\n", ic);
            ret = FIRMWARE_IMAGE_INVALID;
            goto exit;
        }

        ret = load_init_479x(ic, guid_count, guids);
        if (ret != SUCCESS) {
            SDK_PRINTF("ERROR: load_init error (%d) for chip 0x%x\n", ret, ic);
            goto exit;
        }

        if (flash_boot) {
            struct flash_load_config_element* loadcfg = &s_boot->chips[i].load_cfg;
            if (loadcfg->numchips > 0) {
                if (loadcfg->payload_word_count <= 0) {
                    SDK_PRINTF("ERROR: no flash load_config arguments specified for chip 0x%x\n", ic);
                    ret = INVALID_INPUT;
                    goto exit;
                }

                bool ic_flash = s_boot->chips[i].load_cfg.role == SI479XX_CMD_LOAD_CONFIG_ARG_ROLE_ENUM_MASTER;
                ret = load_config_payload(ic, s_boot->chips[i].load_cfg, ic_flash);
                if (ret != SUCCESS) {
                    SDK_PRINTF("ERROR: load_config error (%d) for chip 0x%x\n", ret, ic);
                    goto exit;
                }
            }
        }
    }

    // HOST/FLASH Load Firmware image
    if (flash_boot) {
        ret = SI479XX_FLASH_load_img(ic_flash, s_boot->flash.address, s_boot->flash.size);
        if (ret != SUCCESS) {
            SDK_PRINTF("ERROR: flash_load image failed! (%d)  loading 0x%x bytes @addr=0x%x\n",
                ret, s_boot->flash.size, s_boot->flash.address);
            goto exit;
        }
    } else {
    #ifdef OPTION_BOOT_FROM_HOST			/* 210514 SGsoft */	
        ///ret = host_load_worker(ic_mask, s_boot->image.image_len, s_boot->image.image_buffer);
		ret = host_load_worker(ic_mask, si4790x_firm_size, si4790x_firm);			/* 210429 SGsoft */
        if (ret != SUCCESS) {
            SDK_PRINTF("ERROR: host_load (%d) for chips 0x%x\n", ret, ic_mask);
            goto exit;
        }
	#endif	/*OPTION_BOOT_FROM_HOST*/
    }

    // BOOT all chips, starting from XTAL.
    ret = boot_worker(ic_mask);

	#if 0
	for (int32_t i = OPTION_HAL_MAX_CHIPS; i >= 0; i--) {										/* 210429 SGsoft */
		dc_config_chip_select ic = (1 << i);
		if ((ic & ic_mask) == 0){
			continue;
		}
		ret |= SI479XX_get_func_info(ic, &fi[ic]);								
	}
	#endif	/*0*/
	
exit:
    setCtsTimeout(orig_cts);
    return ret;
}


RETURN_CODE preBoot_flashSetOptions(uint32_t addr, uint32_t size)
{
    if (s_boot == NULL)
        return INVALID_MODE;

    s_boot->flash.address = addr;
    s_boot->flash.size = size;
    return SUCCESS;
}


#if 0		/* 210510 SGsoft */
RETURN_CODE flashBootMultichipCfg(dc_config_chip_select ic, struct flash_cfg cfg)
{	
    struct flash_image img;
    if (flashcfg_find(&cfg, FLASH_FW_TUNER, &img)) {
        return flashBootMultichip(ic, img.image.address, img.image.size);
    } else {
        SDK_LOG("Tuner flash addr/size unknown!");
        return INVALID_INPUT;
    }
}
#endif	/*0*/


RETURN_CODE flashBootMultichip(dc_config_chip_select icNum, uint32_t flash_start_addr, uint32_t flash_img_size)
{
    RETURN_CODE r = preBoot_flashSetOptions(flash_start_addr, flash_img_size);
    r |= multiTunerBoot(icNum, true);
    return r;
}


#ifdef OPTION_BOOT_FROM_HOST			/* 210514 SGsoft */	
RETURN_CODE hostBootSingle_479xx(dc_config_chip_select ic, dc_config_chip_type type,
    struct si479xx_powerup_args* powerup_args,
    uint8_t* uncorrectable, uint8_t* correctable)
{
    // set powerup args
    if (!powerup_args)
        return INVALID_INPUT;

    RETURN_CODE r = preBoot_setPowerup(ic, *powerup_args);
    if (r != SUCCESS)
        return r;

    return multiTunerBoot(ic, false);
}


RETURN_CODE HostLoadWorker(void)															/* 210430 SGsoft */
{
	 return host_load_worker(IC0, si4790x_firm_size, si4790x_firm);
	 
}
#endif	/*OPTION_BOOT_FROM_HOST*/

#endif	/*SI479XX_FIRM_REL_4_1*/
