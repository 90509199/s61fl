/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_hub_api_h_global_
		
#include "main.h"

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "all_apis.h"
 
#include "si479xx_hub_api_constants.h"
#include "si479xx_hub_api.h"
#endif	/*0*/

#if 1		/* 210427 SGsoft */
extern uint32_t silab_cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];						
extern uint32_t silab_reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];

static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t* hifi_data_arg_u32 = (uint32_t *)&silab_cmd_arg_u32[4];
static uint16_t* hifi_data_arg_u16 = (uint16_t*)&silab_cmd_arg_u32[4];
static uint8_t*	hifi_data_arg = (uint8_t*)&silab_cmd_arg_u32[4];
static int32_t*	hifi_data_arg_i32 = (int32_t*)&silab_cmd_arg_u32[4];
static int16_t*	hifi_data_arg_i16 = (int16_t*)&silab_cmd_arg_u32[4];

#else
static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t	hifi_data_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];
static uint16_t*	hifi_data_arg_u16 = (uint16_t*)hifi_data_arg_u32;
static uint8_t*	hifi_data_arg = (uint8_t*)hifi_data_arg_u32;
static int32_t*	hifi_data_arg_i32 = (int32_t*)hifi_data_arg_u32;
static int16_t*	hifi_data_arg_i16 = (int16_t*)hifi_data_arg_u32;
#endif	/*1*/

static uint32_t*	hifi_reply_buffer_u32 = NULL;
static uint16_t*	hifi_reply_buffer_u16 = NULL;
static uint8_t* 	hifi_reply_buffer = NULL;
static int32_t* 	hifi_reply_buffer_i32 = NULL;
static int16_t* 	hifi_reply_buffer_i16 = NULL;

static void setReplyBufferPointers(void* data)
{
    hifi_reply_buffer = (uint8_t*) data;
    hifi_reply_buffer_u32 = (uint32_t*) data;
    hifi_reply_buffer_u16 = (uint16_t*) data;
    hifi_reply_buffer_i32 = (int32_t*)data;
    hifi_reply_buffer_i16 = (int16_t*)data;
}


RETURN_CODE SI479XX_HUB_hub_mgr_nop__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_HUB_MGR_NOP;

	SI479XX_HUB_CMD_HUB_MGR_NOP_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_HUB_MGR_NOP;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_hub_mgr_nop__parse(uint8_t* buf, SI479XX_HUB_hub_mgr_nop__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_HUB_MGR_NOP_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_HUB_MGR_NOP_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_HUB_MGR_NOP_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_hub_mgr_nop__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_hub_mgr_nop__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_HUB_MGR_NOP)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_hub_mgr_nop__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_hub_mgr_nop(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_hub_mgr_nop__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_hub_mgr_nop__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_hub_mgr_nop__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_debug_enable__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_DEBUG_ENABLE;

	SI479XX_HUB_CMD_DEBUG_ENABLE_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_DEBUG_ENABLE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_debug_enable__parse(uint8_t* buf, SI479XX_HUB_debug_enable__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_DEBUG_ENABLE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_DEBUG_ENABLE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_DEBUG_ENABLE_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_debug_enable__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_debug_enable__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_DEBUG_ENABLE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_debug_enable__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_debug_enable(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_debug_enable__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_debug_enable__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_debug_enable__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_debug_disable__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_DEBUG_DISABLE;

	SI479XX_HUB_CMD_DEBUG_DISABLE_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_DEBUG_DISABLE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_debug_disable__parse(uint8_t* buf, SI479XX_HUB_debug_disable__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_DEBUG_DISABLE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_DEBUG_DISABLE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_DEBUG_DISABLE_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_debug_disable__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_debug_disable__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_DEBUG_DISABLE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_debug_disable__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_debug_disable(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_debug_disable__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_debug_disable__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_debug_disable__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_trace_port__command(uint8_t icNum,uint8_t pipe, uint8_t connect, uint16_t length, uint8_t port_id)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TRACE_PORT;

	SI479XX_HUB_CMD_TRACE_PORT_ARG_CONNECT_write_u8(connect);
	SI479XX_HUB_CMD_TRACE_PORT_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TRACE_PORT_ARG_PORT_ID_write_u8(port_id);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TRACE_PORT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_trace_port__parse(uint8_t* buf, SI479XX_HUB_trace_port__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TRACE_PORT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TRACE_PORT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TRACE_PORT_REP_LENGTH_value;
	replyData->PORT_ID = SI479XX_HUB_CMD_TRACE_PORT_REP_PORT_ID_value;
	replyData->CONNECT = SI479XX_HUB_CMD_TRACE_PORT_REP_CONNECT_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_trace_port__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_port__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TRACE_PORT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_trace_port__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_trace_port(uint8_t icNum, uint8_t pipe, uint8_t  connect, uint16_t  length, uint8_t  port_id, SI479XX_HUB_trace_port__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_trace_port__command(icNum, pipe, connect, length, port_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_trace_port__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_trace_node__command(uint8_t icNum,uint8_t pipe, uint8_t connect, uint16_t length, uint8_t xbar_id, uint8_t node_id)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TRACE_NODE;

	SI479XX_HUB_CMD_TRACE_NODE_ARG_CONNECT_write_u8(connect);
	SI479XX_HUB_CMD_TRACE_NODE_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TRACE_NODE_ARG_XBAR_ID_write_u8(xbar_id);
	SI479XX_HUB_CMD_TRACE_NODE_ARG_NODE_ID_write_u8(node_id);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TRACE_NODE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_trace_node__parse(uint8_t* buf, SI479XX_HUB_trace_node__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TRACE_NODE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TRACE_NODE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TRACE_NODE_REP_LENGTH_value;
	replyData->XBAR_ID = SI479XX_HUB_CMD_TRACE_NODE_REP_XBAR_ID_value;
	replyData->NODE_ID = SI479XX_HUB_CMD_TRACE_NODE_REP_NODE_ID_value;
	replyData->CONNECT = SI479XX_HUB_CMD_TRACE_NODE_REP_CONNECT_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_trace_node__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_node__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TRACE_NODE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_trace_node__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_trace_node(uint8_t icNum, uint8_t pipe, uint8_t  connect, uint16_t  length, uint8_t  xbar_id, uint8_t  node_id, SI479XX_HUB_trace_node__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_trace_node__command(icNum, pipe, connect, length, xbar_id, node_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_trace_node__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_slab_trace_mask_read__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ;

	SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_slab_trace_mask_read__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ_REP_LENGTH_value;
	{
		int32_t n = 0;
		uint8_t* trace_mask = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = 16;
		trace_mask = replyData->TRACE_MASK;
		for (n = 0; n < array_elem_count; n++) {
			trace_mask[n] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ_REP_TRACE_MASK_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_slab_trace_mask_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SLAB_TRACE_MASK_READ)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_slab_trace_mask_read__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_slab_trace_mask_read(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_slab_trace_mask_read__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_slab_trace_mask_read__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_slab_trace_mask_read__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_slab_trace_mask_write__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t* trace_mask, uint16_t trace_mask_len)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE;

	SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE_ARG_LENGTH_write_u16(length);
	for(n=0;n < trace_mask_len;n++) {
		SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE_ARG_TRACE_MASK_N_write_u8(n,trace_mask[n]);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_slab_trace_mask_write__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_slab_trace_mask_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SLAB_TRACE_MASK_WRITE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_slab_trace_mask_write__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_slab_trace_mask_write(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t*  trace_mask, uint16_t  trace_mask_len, SI479XX_HUB_slab_trace_mask_write__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_slab_trace_mask_write__command(icNum, pipe, length, trace_mask, trace_mask_len);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_slab_trace_mask_write__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT;

	SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT_ARG_TRACE_MASK_BIT_NUMBER_write_field(trace_mask_bit_number);
	SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_set_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_set_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SLAB_TRACE_MASK_SET_BIT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_slab_trace_mask_set_bit__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_slab_trace_mask_set_bit__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_slab_trace_mask_set_bit__command(icNum, pipe, trace_mask_bit_number, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_slab_trace_mask_set_bit__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT;

	SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT_ARG_TRACE_MASK_BIT_NUMBER_write_field(trace_mask_bit_number);
	SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_clr_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_clr_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SLAB_TRACE_MASK_CLR_BIT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_slab_trace_mask_clr_bit__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_slab_trace_mask_clr_bit__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_slab_trace_mask_clr_bit__command(icNum, pipe, trace_mask_bit_number, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_slab_trace_mask_clr_bit__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_trace_mask_read__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TRACE_MASK_READ;

	SI479XX_HUB_CMD_TRACE_MASK_READ_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TRACE_MASK_READ;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_trace_mask_read__parse(uint8_t* buf, SI479XX_HUB_trace_mask_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TRACE_MASK_READ_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TRACE_MASK_READ_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TRACE_MASK_READ_REP_LENGTH_value;
	{
		int32_t n = 0;
		uint8_t* trace_mask = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = 16;
		trace_mask = replyData->TRACE_MASK;
		for (n = 0; n < array_elem_count; n++) {
			trace_mask[n] = SI479XX_HUB_CMD_TRACE_MASK_READ_REP_TRACE_MASK_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_trace_mask_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TRACE_MASK_READ)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_trace_mask_read__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_trace_mask_read(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_trace_mask_read__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_trace_mask_read__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_trace_mask_read__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_trace_mask_write__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t* trace_mask, uint16_t trace_mask_len)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TRACE_MASK_WRITE;

	SI479XX_HUB_CMD_TRACE_MASK_WRITE_ARG_LENGTH_write_u16(length);
	for(n=0;n < trace_mask_len;n++) {
		SI479XX_HUB_CMD_TRACE_MASK_WRITE_ARG_TRACE_MASK_N_write_u8(n,trace_mask[n]);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TRACE_MASK_WRITE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_trace_mask_write__parse(uint8_t* buf, SI479XX_HUB_trace_mask_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TRACE_MASK_WRITE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TRACE_MASK_WRITE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TRACE_MASK_WRITE_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_trace_mask_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TRACE_MASK_WRITE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_trace_mask_write__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_trace_mask_write(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t*  trace_mask, uint16_t  trace_mask_len, SI479XX_HUB_trace_mask_write__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_trace_mask_write__command(icNum, pipe, length, trace_mask, trace_mask_len);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_trace_mask_write__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_trace_mask_set_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TRACE_MASK_SET_BIT;

	SI479XX_HUB_CMD_TRACE_MASK_SET_BIT_ARG_TRACE_MASK_BIT_NUMBER_write_field(trace_mask_bit_number);
	SI479XX_HUB_CMD_TRACE_MASK_SET_BIT_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TRACE_MASK_SET_BIT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_trace_mask_set_bit__parse(uint8_t* buf, SI479XX_HUB_trace_mask_set_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TRACE_MASK_SET_BIT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TRACE_MASK_SET_BIT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TRACE_MASK_SET_BIT_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_trace_mask_set_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_set_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TRACE_MASK_SET_BIT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_trace_mask_set_bit__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_trace_mask_set_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_trace_mask_set_bit__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_trace_mask_set_bit__command(icNum, pipe, trace_mask_bit_number, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_trace_mask_set_bit__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_trace_mask_clr_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT;

	SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT_ARG_TRACE_MASK_BIT_NUMBER_write_field(trace_mask_bit_number);
	SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_trace_mask_clr_bit__parse(uint8_t* buf, SI479XX_HUB_trace_mask_clr_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_trace_mask_clr_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_clr_bit__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TRACE_MASK_CLR_BIT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_trace_mask_clr_bit__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_trace_mask_clr_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_trace_mask_clr_bit__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_trace_mask_clr_bit__command(icNum, pipe, trace_mask_bit_number, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_trace_mask_clr_bit__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_error_log_read__command(uint8_t icNum,uint8_t pipe, uint8_t clear_log, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_ERROR_LOG_READ;

	SI479XX_HUB_CMD_ERROR_LOG_READ_ARG_CLEAR_LOG_write_field(clear_log);
	SI479XX_HUB_CMD_ERROR_LOG_READ_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_ERROR_LOG_READ;
	}

	return ret;

}


RETURN_CODE SI479XX_HUB_error_log_read__parse(uint8_t* buf, SI479XX_HUB_error_log_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_LENGTH_value;
	{
		int32_t n = 0;
		SI479XX_HUB_error_log_read_error__data* error = NULL;
		int32_t array_elem_count = 0;

		// manual

		array_elem_count = replyData->LENGTH / 8;
		replyData->ERROR_DATA_COUNT = array_elem_count;

		replyData->ERROR = malloc(sizeof(SI479XX_HUB_error_log_read_error__data) * array_elem_count);
		if(replyData->ERROR == NULL) {
			return MEMORY_ERROR;
		}
		error = replyData->ERROR;
		for(n = 0; n < array_elem_count; n++) {
			error[n].ERROR_BYTE_0 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_ERROR_BYTE_0_N_value(n);
			error[n].ERROR_BYTE_1 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_ERROR_BYTE_1_N_value(n);
			error[n].ERROR_BYTE_2 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_ERROR_BYTE_2_N_value(n);
			error[n].ERROR_CODE = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_ERROR_CODE_N_value(n);
			error[n].FRAME_NUMBER_BYTE_0 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_FRAME_NUMBER_BYTE_0_N_value(n);
			error[n].FRAME_NUMBER_BYTE_1 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_FRAME_NUMBER_BYTE_1_N_value(n);
			error[n].FRAME_NUMBER_BYTE_2 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_FRAME_NUMBER_BYTE_2_N_value(n);
			error[n].FRAME_NUMBER_BYTE_3 = SI479XX_HUB_CMD_ERROR_LOG_READ_REP_ERROR_FRAME_NUMBER_BYTE_3_N_value(n);
 
		}

	}
	return ret;
}

RETURN_CODE SI479XX_HUB_error_log_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_error_log_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_ERROR_LOG_READ)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if(ret == SUCCESS) {
			ret = SI479XX_HUB_error_log_read__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}

RETURN_CODE SI479XX_HUB_error_log_read(uint8_t icNum, uint8_t pipe, uint8_t  clear_log, uint16_t  length, SI479XX_HUB_error_log_read__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_error_log_read__command(icNum, pipe, clear_log, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_error_log_read__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_error_log_options__command(uint8_t icNum,uint8_t pipe, uint8_t nowrap, uint8_t nolog, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_ERROR_LOG_OPTIONS;

	SI479XX_HUB_CMD_ERROR_LOG_OPTIONS_ARG_NOWRAP_write_field(nowrap);
	SI479XX_HUB_CMD_ERROR_LOG_OPTIONS_ARG_NOLOG_write_field(nolog);
	SI479XX_HUB_CMD_ERROR_LOG_OPTIONS_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_ERROR_LOG_OPTIONS;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_error_log_options__parse(uint8_t* buf, SI479XX_HUB_error_log_options__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_ERROR_LOG_OPTIONS_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_ERROR_LOG_OPTIONS_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_ERROR_LOG_OPTIONS_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_error_log_options__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_error_log_options__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_ERROR_LOG_OPTIONS)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_error_log_options__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_error_log_options(uint8_t icNum, uint8_t pipe, uint8_t  nowrap, uint8_t  nolog, uint16_t  length, SI479XX_HUB_error_log_options__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_error_log_options__command(icNum, pipe, nowrap, nolog, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_error_log_options__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_dsp_memory_query__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_DSP_MEMORY_QUERY;

	SI479XX_HUB_CMD_DSP_MEMORY_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_DSP_MEMORY_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_dsp_memory_query__parse(uint8_t* buf, SI479XX_HUB_dsp_memory_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_DSP_MEMORY_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_DSP_MEMORY_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_DSP_MEMORY_QUERY_REP_LENGTH_value;
	replyData->MIN_STACK = SI479XX_HUB_CMD_DSP_MEMORY_QUERY_REP_MIN_STACK_value;
	replyData->MAX_HEAP = SI479XX_HUB_CMD_DSP_MEMORY_QUERY_REP_MAX_HEAP_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_dsp_memory_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_dsp_memory_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_DSP_MEMORY_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_dsp_memory_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_dsp_memory_query(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_dsp_memory_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_dsp_memory_query__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_dsp_memory_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_frame_number_query__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_FRAME_NUMBER_QUERY;

	SI479XX_HUB_CMD_FRAME_NUMBER_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_FRAME_NUMBER_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_frame_number_query__parse(uint8_t* buf, SI479XX_HUB_frame_number_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_FRAME_NUMBER_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_FRAME_NUMBER_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_FRAME_NUMBER_QUERY_REP_LENGTH_value;
	replyData->FRAME_NUMBER = SI479XX_HUB_CMD_FRAME_NUMBER_QUERY_REP_FRAME_NUMBER_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_frame_number_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_frame_number_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_FRAME_NUMBER_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_frame_number_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_frame_number_query(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_frame_number_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_frame_number_query__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_frame_number_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_query_waveform_heap__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP;

	SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_query_waveform_heap__parse(uint8_t* buf, SI479XX_HUB_query_waveform_heap__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP_REP_LENGTH_value;
	replyData->FREE_BYTES = SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP_REP_FREE_BYTES_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_query_waveform_heap__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_waveform_heap__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_QUERY_WAVEFORM_HEAP)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_query_waveform_heap__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_query_waveform_heap(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_query_waveform_heap__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_query_waveform_heap__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_query_waveform_heap__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_cpu_occupancy_query__command(uint8_t icNum,uint8_t pipe, uint8_t reset_peak, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY;

	SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_ARG_RESET_PEAK_write_field(reset_peak);
	SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_cpu_occupancy_query__parse(uint8_t* buf, SI479XX_HUB_cpu_occupancy_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_REP_LENGTH_value;
	replyData->CPU_AVE = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_REP_CPU_AVE_value;
	replyData->CPU_PEAK = SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY_REP_CPU_PEAK_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_cpu_occupancy_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_cpu_occupancy_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_CPU_OCCUPANCY_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_cpu_occupancy_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_cpu_occupancy_query(uint8_t icNum, uint8_t pipe, uint8_t  reset_peak, uint16_t  length, SI479XX_HUB_cpu_occupancy_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_cpu_occupancy_query__command(icNum, pipe, reset_peak, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_cpu_occupancy_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_set_sleep_mode__command(uint8_t icNum,uint8_t pipe, uint8_t sleep_mode, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SET_SLEEP_MODE;

	SI479XX_HUB_CMD_SET_SLEEP_MODE_ARG_SLEEP_MODE_write_field(sleep_mode);
	SI479XX_HUB_CMD_SET_SLEEP_MODE_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SET_SLEEP_MODE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_set_sleep_mode__parse(uint8_t* buf, SI479XX_HUB_set_sleep_mode__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SET_SLEEP_MODE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SET_SLEEP_MODE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SET_SLEEP_MODE_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_set_sleep_mode__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_sleep_mode__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SET_SLEEP_MODE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_set_sleep_mode__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_set_sleep_mode(uint8_t icNum, uint8_t pipe, uint8_t  sleep_mode, uint16_t  length, SI479XX_HUB_set_sleep_mode__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_set_sleep_mode__command(icNum, pipe, sleep_mode, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_set_sleep_mode__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_sdk_enable__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SDK_ENABLE;

	SI479XX_HUB_CMD_SDK_ENABLE_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SDK_ENABLE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_sdk_enable__parse(uint8_t* buf, SI479XX_HUB_sdk_enable__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SDK_ENABLE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SDK_ENABLE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SDK_ENABLE_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_sdk_enable__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_sdk_enable__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SDK_ENABLE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_sdk_enable__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_sdk_enable(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_sdk_enable__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_sdk_enable__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_sdk_enable__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}




RETURN_CODE SI479XX_HUB_xbar_connect_node__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_connect_node_p2n_cnx__data* p2n_cnx, uint16_t p2n_cnx_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_XBAR_CONNECT_NODE;

	SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_LENGTH_write_u16(length);
	for(n=0;n < p2n_cnx_length;n++)
	{
		SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_P2N_CNX_PORT_ID_N_write_u8(n,p2n_cnx[n].PORT_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_P2N_CNX_CHAN_ID_N_write_field(n,p2n_cnx[n].CHAN_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_P2N_CNX_NODE_ID_N_write_u8(n,p2n_cnx[n].NODE_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_P2N_CNX_GAIN_DB_N_write_i16(n,p2n_cnx[n].GAIN_DB);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if(ret == SUCCESS)
	{
		lastCommands[icIndex] = SI479XX_HUB_CMD_XBAR_CONNECT_NODE;
	}

	return ret;

}


RETURN_CODE SI479XX_HUB_xbar_connect_node__parse(uint8_t* buf, SI479XX_HUB_xbar_connect_node__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_LENGTH_value;
	replyData->P2N_CNX_LENGTH = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_LENGTH_value;
	{
		int32_t n = 0;
		SI479XX_HUB_xbar_connect_node_p2n_cnx__data* p2n_cnx = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->P2N_CNX_LENGTH;
		replyData->P2N_CNX = (SI479XX_HUB_xbar_connect_node_p2n_cnx__data*)malloc(sizeof(SI479XX_HUB_xbar_connect_node_p2n_cnx__data) * array_elem_count);
		if(replyData->P2N_CNX == NULL) {
			return MEMORY_ERROR;
		}
		p2n_cnx = replyData->P2N_CNX;
		for(n = 0; n < array_elem_count; n++) {
			p2n_cnx[n].PORT_ID = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_PORT_ID_N_value(n);
			p2n_cnx[n].CHAN_ID = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_CHAN_ID_N_value(n);
			p2n_cnx[n].NODE_ID = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_NODE_ID_N_value(n);
			p2n_cnx[n].XBAR_ID = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_XBAR_ID_N_value(n);
			p2n_cnx[n].GAIN_DB = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_GAIN_DB_N_value(n);
			p2n_cnx[n].CNX_INFO = SI479XX_HUB_CMD_XBAR_CONNECT_NODE_REP_P2N_CNX_CNX_INFO_N_value(n);
 
		}

	}
	return ret;
}

RETURN_CODE SI479XX_HUB_xbar_connect_node__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_connect_node__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_XBAR_CONNECT_NODE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if(ret == SUCCESS) {
			ret = SI479XX_HUB_xbar_connect_node__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}

RETURN_CODE SI479XX_HUB_xbar_connect_node(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_connect_node_p2n_cnx__data*  p2n_cnx, uint16_t  p2n_cnx_length, SI479XX_HUB_xbar_connect_node__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_xbar_connect_node__command(icNum, pipe, length, p2n_cnx, p2n_cnx_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_xbar_connect_node__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}





RETURN_CODE SI479XX_HUB_xbar_disconnect_node__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data* p2n_dnx, uint16_t p2n_dnx_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE;

	SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_ARG_LENGTH_write_u16(length);
	for(n=0;n < p2n_dnx_length;n++)
	{
		SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_ARG_P2N_DNX_PORT_ID_N_write_u8(n,p2n_dnx[n].PORT_ID);
		SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_ARG_P2N_DNX_CHAN_ID_N_write_field(n,p2n_dnx[n].CHAN_ID);
		SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_ARG_P2N_DNX_NODE_ID_N_write_u8(n,p2n_dnx[n].NODE_ID);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if(ret == SUCCESS)
	{
		lastCommands[icIndex] = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE;
	}

	return ret;

}


RETURN_CODE SI479XX_HUB_xbar_disconnect_node__parse(uint8_t* buf, SI479XX_HUB_xbar_disconnect_node__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_LENGTH_value;
	replyData->P2N_DNX_LENGTH = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_P2N_DNX_LENGTH_value;
	{
		int32_t n = 0;
		SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data* p2n_dnx = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->P2N_DNX_LENGTH;
		replyData->P2N_DNX = (SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data*)malloc(sizeof(SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data) * array_elem_count);
		if(replyData->P2N_DNX == NULL) {
			return MEMORY_ERROR;
		}
		p2n_dnx = replyData->P2N_DNX;
		for(n = 0; n < array_elem_count; n++) {
			p2n_dnx[n].PORT_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_P2N_DNX_PORT_ID_N_value(n);
			p2n_dnx[n].CHAN_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_P2N_DNX_CHAN_ID_N_value(n);
			p2n_dnx[n].NODE_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_P2N_DNX_NODE_ID_N_value(n);
			p2n_dnx[n].XBAR_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE_REP_P2N_DNX_XBAR_ID_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_xbar_disconnect_node__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_disconnect_node__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_XBAR_DISCONNECT_NODE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if(ret == SUCCESS) {
			ret = SI479XX_HUB_xbar_disconnect_node__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}

RETURN_CODE SI479XX_HUB_xbar_disconnect_node(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data*  p2n_dnx, uint16_t  p2n_dnx_length, SI479XX_HUB_xbar_disconnect_node__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_xbar_disconnect_node__command(icNum, pipe, length, p2n_dnx, p2n_dnx_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_xbar_disconnect_node__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_xbar_query__command(uint8_t icNum,uint8_t pipe, uint8_t xbar_id, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_XBAR_QUERY;

	SI479XX_HUB_CMD_XBAR_QUERY_ARG_XBAR_ID_write_u8(xbar_id);
	SI479XX_HUB_CMD_XBAR_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_XBAR_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_xbar_query__parse(uint8_t* buf, SI479XX_HUB_xbar_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_XBAR_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_XBAR_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_XBAR_QUERY_REP_LENGTH_value;
	replyData->XBAR_ID = SI479XX_HUB_CMD_XBAR_QUERY_REP_XBAR_ID_value;
	replyData->NUMBER_OF_CONNECTIONS = SI479XX_HUB_CMD_XBAR_QUERY_REP_NUMBER_OF_CONNECTIONS_value;
	{
		int32_t n = 0;
		SI479XX_HUB_xbar_query_cnx__data* cnx = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->NUMBER_OF_CONNECTIONS;
		replyData->CNX = (SI479XX_HUB_xbar_query_cnx__data*)malloc(sizeof(SI479XX_HUB_xbar_query_cnx__data) * array_elem_count);
		if (replyData->CNX == NULL) {
			return MEMORY_ERROR;
		}
		cnx = replyData->CNX;
		for (n = 0; n < array_elem_count; n++) {
			cnx[n].CONNECTION_BYTE_0 = SI479XX_HUB_CMD_XBAR_QUERY_REP_CNX_CONNECTION_BYTE_0_N_value(n);
			cnx[n].CONNECTION_BYTE_1 = SI479XX_HUB_CMD_XBAR_QUERY_REP_CNX_CONNECTION_BYTE_1_N_value(n);
			cnx[n].CONNECTION_BYTE_2 = SI479XX_HUB_CMD_XBAR_QUERY_REP_CNX_CONNECTION_BYTE_2_N_value(n);
			cnx[n].CONNECTION_BYTE_3 = SI479XX_HUB_CMD_XBAR_QUERY_REP_CNX_CONNECTION_BYTE_3_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_xbar_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_XBAR_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_xbar_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_xbar_query(uint8_t icNum, uint8_t pipe, uint8_t  xbar_id, uint16_t  length, SI479XX_HUB_xbar_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_xbar_query__command(icNum, pipe, xbar_id, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_xbar_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}




RETURN_CODE SI479XX_HUB_xbar_connect_port__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_connect_port_p2p_cnx__data* p2p_cnx, uint16_t p2p_cnx_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_XBAR_CONNECT_PORT;

	SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_LENGTH_write_u16(length);
	for(n=0;n < p2p_cnx_length;n++)
	{
		SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_IN_PORT_ID_N_write_u8(n,p2p_cnx[n].IN_PORT_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_IN_CHAN_ID_N_write_field(n,p2p_cnx[n].IN_CHAN_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_OUT_PORT_ID_N_write_u8(n,p2p_cnx[n].OUT_PORT_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_OUT_CHAN_ID_N_write_field(n,p2p_cnx[n].OUT_CHAN_ID);
		SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_GAIN_DB_N_write_i16(n,p2p_cnx[n].GAIN_DB);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if(ret == SUCCESS)
	{
		lastCommands[icIndex] = SI479XX_HUB_CMD_XBAR_CONNECT_PORT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_xbar_connect_port__parse(uint8_t* buf, SI479XX_HUB_xbar_connect_port__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_LENGTH_value;
	replyData->P2P_CNX_LENGTH = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_LENGTH_value;
	{
		int32_t n = 0;
		SI479XX_HUB_xbar_connect_port_p2p_cnx__data* p2p_cnx = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->P2P_CNX_LENGTH;
		replyData->P2P_CNX = (SI479XX_HUB_xbar_connect_port_p2p_cnx__data*)malloc(sizeof(SI479XX_HUB_xbar_connect_port_p2p_cnx__data) * array_elem_count);
		if (replyData->P2P_CNX == NULL) {
			return MEMORY_ERROR;
		}
		p2p_cnx = replyData->P2P_CNX;
		for (n = 0; n < array_elem_count; n++) {
			p2p_cnx[n].IN_PORT_ID = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_IN_PORT_ID_N_value(n);
			p2p_cnx[n].IN_CHAN_ID = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_IN_CHAN_ID_N_value(n);
			p2p_cnx[n].OUT_PORT_ID = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_OUT_PORT_ID_N_value(n);
			p2p_cnx[n].OUT_CHAN_ID = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_OUT_CHAN_ID_N_value(n);
			p2p_cnx[n].GAIN_DB = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_GAIN_DB_N_value(n);
			p2p_cnx[n].POLARITY = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_POLARITY_N_value(n);
			p2p_cnx[n].DECAY_RAMP = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_DECAY_RAMP_N_value(n);
			p2p_cnx[n].ATTACK_RAMP = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_ATTACK_RAMP_N_value(n);
			p2p_cnx[n].CNX_INFO = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_REP_P2P_CNX_CNX_INFO_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_xbar_connect_port__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_connect_port__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_XBAR_CONNECT_PORT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_xbar_connect_port__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_xbar_connect_port(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_connect_port_p2p_cnx__data*  p2p_cnx, uint16_t  p2p_cnx_length, SI479XX_HUB_xbar_connect_port__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_xbar_connect_port__command(icNum, pipe, length, p2p_cnx, p2p_cnx_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_xbar_connect_port__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_xbar_disconnect_port__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data* p2p_dnx, uint16_t p2p_dnx_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT;

	SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_ARG_LENGTH_write_u16(length);
	for(n=0;n < p2p_dnx_length;n++) {
		SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_ARG_P2P_DNX_IN_PORT_ID_N_write_u8(n,p2p_dnx[n].IN_PORT_ID);
		SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_ARG_P2P_DNX_IN_CHAN_ID_N_write_field(n,p2p_dnx[n].IN_CHAN_ID);
		SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_ARG_P2P_DNX_OUT_PORT_ID_N_write_u8(n,p2p_dnx[n].OUT_PORT_ID);
		SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_ARG_P2P_DNX_OUT_CHAN_ID_N_write_field(n,p2p_dnx[n].OUT_CHAN_ID);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_xbar_disconnect_port__parse(uint8_t* buf, SI479XX_HUB_xbar_disconnect_port__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_LENGTH_value;
	replyData->P2P_DNX_LENGTH = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_P2P_DNX_LENGTH_value;
	{
		int32_t n = 0;
		SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data* p2p_dnx = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->P2P_DNX_LENGTH;
		replyData->P2P_DNX = (SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data*)malloc(sizeof(SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data) * array_elem_count);
		if (replyData->P2P_DNX == NULL) {
			return MEMORY_ERROR;
		}
		p2p_dnx = replyData->P2P_DNX;
		for (n = 0; n < array_elem_count; n++) {
			p2p_dnx[n].IN_PORT_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_P2P_DNX_IN_PORT_ID_N_value(n);
			p2p_dnx[n].IN_CHAN_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_P2P_DNX_IN_CHAN_ID_N_value(n);
			p2p_dnx[n].OUT_PORT_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_P2P_DNX_OUT_PORT_ID_N_value(n);
			p2p_dnx[n].OUT_CHAN_ID = SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT_REP_P2P_DNX_OUT_CHAN_ID_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_xbar_disconnect_port__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_disconnect_port__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_XBAR_DISCONNECT_PORT)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_xbar_disconnect_port__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_xbar_disconnect_port(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data*  p2p_dnx, uint16_t  p2p_dnx_length, SI479XX_HUB_xbar_disconnect_port__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_xbar_disconnect_port__command(icNum, pipe, length, p2p_dnx, p2p_dnx_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_xbar_disconnect_port__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_set_connection_gain__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3, int16_t gain_db)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SET_CONNECTION_GAIN;

	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_CNX_ID_write_u8(cnx_id);
	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_CONNECTION_BYTE_0_write_u8(connection_byte_0);
	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_CONNECTION_BYTE_1_write_u8(connection_byte_1);
	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_CONNECTION_BYTE_2_write_u8(connection_byte_2);
	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_CONNECTION_BYTE_3_write_u8(connection_byte_3);
	SI479XX_HUB_CMD_SET_CONNECTION_GAIN_ARG_GAIN_DB_write_i16(gain_db);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SET_CONNECTION_GAIN;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_set_connection_gain__parse(uint8_t* buf, SI479XX_HUB_set_connection_gain__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_LENGTH_value;
	replyData->CNX_ID = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_CNX_ID_value;
	replyData->CONNECTION_BYTE_0 = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_CONNECTION_BYTE_0_value;
	replyData->CONNECTION_BYTE_1 = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_CONNECTION_BYTE_1_value;
	replyData->CONNECTION_BYTE_2 = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_CONNECTION_BYTE_2_value;
	replyData->CONNECTION_BYTE_3 = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_CONNECTION_BYTE_3_value;
	replyData->GAIN_DB = SI479XX_HUB_CMD_SET_CONNECTION_GAIN_REP_GAIN_DB_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_set_connection_gain__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_connection_gain__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SET_CONNECTION_GAIN)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_set_connection_gain__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_set_connection_gain(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, int16_t  gain_db, SI479XX_HUB_set_connection_gain__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_set_connection_gain__command(icNum, pipe, cnx_id, length, connection_byte_0, connection_byte_1, connection_byte_2, connection_byte_3, gain_db);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_set_connection_gain__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_set_connection_ramp_times__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3, uint16_t ramp_up_time, uint16_t ramp_dn_time)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES;

	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_CNX_ID_write_u8(cnx_id);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_CONNECTION_BYTE_0_write_u8(connection_byte_0);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_CONNECTION_BYTE_1_write_u8(connection_byte_1);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_CONNECTION_BYTE_2_write_u8(connection_byte_2);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_CONNECTION_BYTE_3_write_u8(connection_byte_3);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_RAMP_UP_TIME_write_u16(ramp_up_time);
	SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_ARG_RAMP_DN_TIME_write_u16(ramp_dn_time);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_set_connection_ramp_times__parse(uint8_t* buf, SI479XX_HUB_set_connection_ramp_times__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_LENGTH_value;
	replyData->CNX_ID = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_CNX_ID_value;
	replyData->CONNECTION_BYTE_0 = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_CONNECTION_BYTE_0_value;
	replyData->CONNECTION_BYTE_1 = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_CONNECTION_BYTE_1_value;
	replyData->CONNECTION_BYTE_2 = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_CONNECTION_BYTE_2_value;
	replyData->CONNECTION_BYTE_3 = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_CONNECTION_BYTE_3_value;
	replyData->RAMP_UP_TIME = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_RAMP_UP_TIME_value;
	replyData->RAMP_DN_TIME = SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES_REP_RAMP_DN_TIME_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_set_connection_ramp_times__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_connection_ramp_times__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SET_CONNECTION_RAMP_TIMES)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_set_connection_ramp_times__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_set_connection_ramp_times(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, uint16_t  ramp_up_time, uint16_t  ramp_dn_time, SI479XX_HUB_set_connection_ramp_times__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_set_connection_ramp_times__command(icNum, pipe, cnx_id, length, connection_byte_0, connection_byte_1, connection_byte_2, connection_byte_3, ramp_up_time, ramp_dn_time);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_set_connection_ramp_times__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_query_connection_gain__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN;

	SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_ARG_CNX_ID_write_u8(cnx_id);
	SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_ARG_CONNECTION_BYTE_0_write_u8(connection_byte_0);
	SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_ARG_CONNECTION_BYTE_1_write_u8(connection_byte_1);
	SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_ARG_CONNECTION_BYTE_2_write_u8(connection_byte_2);
	SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_ARG_CONNECTION_BYTE_3_write_u8(connection_byte_3);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_query_connection_gain__parse(uint8_t* buf, SI479XX_HUB_query_connection_gain__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_LENGTH_value;
	replyData->CNX_ID = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_CNX_ID_value;
	replyData->GAIN_STATE = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_GAIN_STATE_value;
	replyData->CONNECTION_BYTE_0 = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_CONNECTION_BYTE_0_value;
	replyData->CONNECTION_BYTE_1 = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_CONNECTION_BYTE_1_value;
	replyData->CONNECTION_BYTE_2 = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_CONNECTION_BYTE_2_value;
	replyData->CONNECTION_BYTE_3 = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_CONNECTION_BYTE_3_value;
	replyData->GAIN_DB = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_GAIN_DB_value;
	replyData->FS_ERR = SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN_REP_FS_ERR_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_query_connection_gain__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_connection_gain__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_QUERY_CONNECTION_GAIN)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_query_connection_gain__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_query_connection_gain(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, SI479XX_HUB_query_connection_gain__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_query_connection_gain__command(icNum, pipe, cnx_id, length, connection_byte_0, connection_byte_1, connection_byte_2, connection_byte_3);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_query_connection_gain__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_query_connection_ramp__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP;

	SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_ARG_CNX_ID_write_u8(cnx_id);
	SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_ARG_CONNECTION_BYTE_0_write_u8(connection_byte_0);
	SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_ARG_CONNECTION_BYTE_1_write_u8(connection_byte_1);
	SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_ARG_CONNECTION_BYTE_2_write_u8(connection_byte_2);
	SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_ARG_CONNECTION_BYTE_3_write_u8(connection_byte_3);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_query_connection_ramp__parse(uint8_t* buf, SI479XX_HUB_query_connection_ramp__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_LENGTH_value;
	replyData->CNX_ID = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_CNX_ID_value;
	replyData->POLARITY = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_POLARITY_value;
	replyData->DECAY_RAMP = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_DECAY_RAMP_value;
	replyData->ATTACK_RAMP = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_ATTACK_RAMP_value;
	replyData->CONNECTION_BYTE_0 = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_CONNECTION_BYTE_0_value;
	replyData->CONNECTION_BYTE_1 = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_CONNECTION_BYTE_1_value;
	replyData->CONNECTION_BYTE_2 = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_CONNECTION_BYTE_2_value;
	replyData->CONNECTION_BYTE_3 = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_CONNECTION_BYTE_3_value;
	replyData->RAMP_UP_TIME = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_RAMP_UP_TIME_value;
	replyData->RAMP_DN_TIME = SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP_REP_RAMP_DN_TIME_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_query_connection_ramp__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_connection_ramp__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_QUERY_CONNECTION_RAMP)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_query_connection_ramp__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_query_connection_ramp(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, SI479XX_HUB_query_connection_ramp__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_query_connection_ramp__command(icNum, pipe, cnx_id, length, connection_byte_0, connection_byte_1, connection_byte_2, connection_byte_3);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_query_connection_ramp__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_node_query__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t xbar_id, uint8_t node_id)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_NODE_QUERY;

	SI479XX_HUB_CMD_NODE_QUERY_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_NODE_QUERY_ARG_XBAR_ID_write_u8(xbar_id);
	SI479XX_HUB_CMD_NODE_QUERY_ARG_NODE_ID_write_u8(node_id);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_NODE_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_node_query__parse(uint8_t* buf, SI479XX_HUB_node_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_NODE_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_NODE_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_NODE_QUERY_REP_LENGTH_value;
	replyData->XBAR_ID = SI479XX_HUB_CMD_NODE_QUERY_REP_XBAR_ID_value;
	replyData->NODE_ID = SI479XX_HUB_CMD_NODE_QUERY_REP_NODE_ID_value;
	replyData->CNX = SI479XX_HUB_CMD_NODE_QUERY_REP_CNX_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_node_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_node_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_NODE_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_node_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_node_query(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  xbar_id, uint8_t  node_id, SI479XX_HUB_node_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_node_query__command(icNum, pipe, length, xbar_id, node_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_node_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_port_mute__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t port_id, uint8_t chan_id, uint8_t mutetype, uint8_t mutestate)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_PORT_MUTE;

	SI479XX_HUB_CMD_PORT_MUTE_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_PORT_MUTE_ARG_PORT_ID_write_u8(port_id);
	SI479XX_HUB_CMD_PORT_MUTE_ARG_CHAN_ID_write_field(chan_id);
	SI479XX_HUB_CMD_PORT_MUTE_ARG_MUTETYPE_write_field(mutetype);
	SI479XX_HUB_CMD_PORT_MUTE_ARG_MUTESTATE_write_field(mutestate);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_PORT_MUTE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_port_mute__parse(uint8_t* buf, SI479XX_HUB_port_mute__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_PORT_MUTE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_PORT_MUTE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_PORT_MUTE_REP_LENGTH_value;
	replyData->PORT_ID = SI479XX_HUB_CMD_PORT_MUTE_REP_PORT_ID_value;
	replyData->CHAN_ID = SI479XX_HUB_CMD_PORT_MUTE_REP_CHAN_ID_value;
	replyData->MUTETYPE = SI479XX_HUB_CMD_PORT_MUTE_REP_MUTETYPE_value;
	replyData->MUTESTATE = SI479XX_HUB_CMD_PORT_MUTE_REP_MUTESTATE_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_port_mute__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_port_mute__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_PORT_MUTE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_port_mute__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_port_mute(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  port_id, uint8_t  chan_id, uint8_t  mutetype, uint8_t  mutestate, SI479XX_HUB_port_mute__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_port_mute__command(icNum, pipe, length, port_id, chan_id, mutetype, mutestate);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_port_mute__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_port_query__command(uint8_t icNum,uint8_t pipe, uint8_t port_id, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_PORT_QUERY;

	SI479XX_HUB_CMD_PORT_QUERY_ARG_PORT_ID_write_u8(port_id);
	SI479XX_HUB_CMD_PORT_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_PORT_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_port_query__parse(uint8_t* buf, SI479XX_HUB_port_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_PORT_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_PORT_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_PORT_QUERY_REP_LENGTH_value;
	replyData->PORT_ID = SI479XX_HUB_CMD_PORT_QUERY_REP_PORT_ID_value;
	replyData->PORT_STATE = SI479XX_HUB_CMD_PORT_QUERY_REP_PORT_STATE_value;
	replyData->CHAN_STATE = SI479XX_HUB_CMD_PORT_QUERY_REP_CHAN_STATE_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_port_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_port_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_PORT_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_port_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_port_query(uint8_t icNum, uint8_t pipe, uint8_t  port_id, uint16_t  length, SI479XX_HUB_port_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_port_query__command(icNum, pipe, port_id, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_port_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_clock_domain_sync__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t clock_domain, uint8_t port_id)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC;

	SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_ARG_CLOCK_DOMAIN_write_u8(clock_domain);
	SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_ARG_PORT_ID_write_u8(port_id);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_clock_domain_sync__parse(uint8_t* buf, SI479XX_HUB_clock_domain_sync__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_REP_LENGTH_value;
	replyData->CLOCK_DOMAIN = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_REP_CLOCK_DOMAIN_value;
	replyData->PORT_ID = SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC_REP_PORT_ID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_clock_domain_sync__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_clock_domain_sync__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_CLOCK_DOMAIN_SYNC)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_clock_domain_sync__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_clock_domain_sync(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  clock_domain, uint8_t  port_id, SI479XX_HUB_clock_domain_sync__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_clock_domain_sync__command(icNum, pipe, length, clock_domain, port_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_clock_domain_sync__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_clock_domain_query__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY;

	SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_clock_domain_query__parse(uint8_t* buf, SI479XX_HUB_clock_domain_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_REP_LENGTH_value;
	replyData->CLOCK_DOMAIN = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_REP_CLOCK_DOMAIN_value;
	replyData->PORT_ID = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_REP_PORT_ID_value;
	replyData->ERR_PPM = SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY_REP_ERR_PPM_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_clock_domain_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_clock_domain_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_CLOCK_DOMAIN_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_clock_domain_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_clock_domain_query(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_clock_domain_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_clock_domain_query__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_clock_domain_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_request__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint16_t amp_abs, int16_t num_tone_repeat, SI479XX_HUB_tonegen_request_tone_descriptor__data* tone_descriptor, uint16_t tone_descriptor_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_REQUEST;

	SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_GEN_ID_write_field(tone_gen_id);
	SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_AMP_ABS_write_u16(amp_abs);
	SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_NUM_TONE_REPEAT_write_i16(num_tone_repeat);
	for(n=0;n < tone_descriptor_length;n++) {
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_SUM_OR_MPY_N_write_field(n,tone_descriptor[n].SUM_OR_MPY);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_FALLRAMP_N_write_field(n,tone_descriptor[n].FALLRAMP);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_DECAYRAMP_N_write_field(n,tone_descriptor[n].DECAYRAMP);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_ATTACKRAMP_N_write_field(n,tone_descriptor[n].ATTACKRAMP);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_WAVETYPE_N_write_field(n,tone_descriptor[n].WAVETYPE);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_OSCID_N_write_field(n,tone_descriptor[n].OSCID);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_MOD_INDEX_N_write_field(n,tone_descriptor[n].MOD_INDEX);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_AMP_PEAK_N_write_u8(n,tone_descriptor[n].AMP_PEAK);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_AMP_DECAY_N_write_u8(n,tone_descriptor[n].AMP_DECAY);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_FREQUENCY_N_write_u16(n,tone_descriptor[n].FREQUENCY);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_START_TIME_N_write_u16(n,tone_descriptor[n].START_TIME);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_ATTACK_TIME_N_write_u16(n,tone_descriptor[n].ATTACK_TIME);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_PEAK_DURATION_N_write_field(n,tone_descriptor[n].PEAK_DURATION);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_PEAK_TIME_N_write_u16(n,tone_descriptor[n].PEAK_TIME);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_DECAY_TIME_N_write_u16(n,tone_descriptor[n].DECAY_TIME);
		SI479XX_HUB_CMD_TONEGEN_REQUEST_ARG_TONE_DESCRIPTOR_FALL_TIME_N_write_u16(n,tone_descriptor[n].FALL_TIME);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_REQUEST;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_request__parse(uint8_t* buf, SI479XX_HUB_tonegen_request__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_REQUEST_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_REQUEST_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_REQUEST_REP_LENGTH_value;
	replyData->TONE_GEN_ID = SI479XX_HUB_CMD_TONEGEN_REQUEST_REP_TONE_GEN_ID_value;
	replyData->TONE_QUEUE_COUNT = SI479XX_HUB_CMD_TONEGEN_REQUEST_REP_TONE_QUEUE_COUNT_value;
	replyData->TONE_QUEUE_DEPTH = SI479XX_HUB_CMD_TONEGEN_REQUEST_REP_TONE_QUEUE_DEPTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_request__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_request__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_REQUEST)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_request__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_request(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint16_t  amp_abs, int16_t  num_tone_repeat, SI479XX_HUB_tonegen_request_tone_descriptor__data*  tone_descriptor, uint16_t  tone_descriptor_length, SI479XX_HUB_tonegen_request__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_request__command(icNum, pipe, tone_gen_id, length, amp_abs, num_tone_repeat, tone_descriptor, tone_descriptor_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_request__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_modify__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint8_t tone_des_id, uint8_t tone_prm_id, uint16_t tone_prm_val)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_MODIFY;

	SI479XX_HUB_CMD_TONEGEN_MODIFY_ARG_TONE_GEN_ID_write_field(tone_gen_id);
	SI479XX_HUB_CMD_TONEGEN_MODIFY_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TONEGEN_MODIFY_ARG_TONE_DES_ID_write_field(tone_des_id);
	SI479XX_HUB_CMD_TONEGEN_MODIFY_ARG_TONE_PRM_ID_write_field(tone_prm_id);
	SI479XX_HUB_CMD_TONEGEN_MODIFY_ARG_TONE_PRM_VAL_write_u16(tone_prm_val);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_MODIFY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_modify__parse(uint8_t* buf, SI479XX_HUB_tonegen_modify__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_LENGTH_value;
	replyData->TONE_GEN_ID = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_TONE_GEN_ID_value;
	replyData->TONE_DES_ID = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_TONE_DES_ID_value;
	replyData->TONE_PRM_ID = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_TONE_PRM_ID_value;
	replyData->TONE_PRM_VAL = SI479XX_HUB_CMD_TONEGEN_MODIFY_REP_TONE_PRM_VAL_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_modify__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_modify__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_MODIFY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_modify__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_modify(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint8_t  tone_des_id, uint8_t  tone_prm_id, uint16_t  tone_prm_val, SI479XX_HUB_tonegen_modify__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_modify__command(icNum, pipe, tone_gen_id, length, tone_des_id, tone_prm_id, tone_prm_val);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_modify__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_stop__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint8_t abort_option, uint8_t queue_option, uint16_t decay_time)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_STOP;

	SI479XX_HUB_CMD_TONEGEN_STOP_ARG_TONE_GEN_ID_write_field(tone_gen_id);
	SI479XX_HUB_CMD_TONEGEN_STOP_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TONEGEN_STOP_ARG_ABORT_OPTION_write_field(abort_option);
	SI479XX_HUB_CMD_TONEGEN_STOP_ARG_QUEUE_OPTION_write_field(queue_option);
	SI479XX_HUB_CMD_TONEGEN_STOP_ARG_DECAY_TIME_write_u16(decay_time);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_STOP;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_stop__parse(uint8_t* buf, SI479XX_HUB_tonegen_stop__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_STOP_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_STOP_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_STOP_REP_LENGTH_value;
	replyData->TONE_GEN_ID = SI479XX_HUB_CMD_TONEGEN_STOP_REP_TONE_GEN_ID_value;
	replyData->ABORT_OPTION = SI479XX_HUB_CMD_TONEGEN_STOP_REP_ABORT_OPTION_value;
	replyData->QUEUE_OPTION = SI479XX_HUB_CMD_TONEGEN_STOP_REP_QUEUE_OPTION_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_stop__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_stop__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_STOP)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_stop__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_stop(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint8_t  abort_option, uint8_t  queue_option, uint16_t  decay_time, SI479XX_HUB_tonegen_stop__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_stop__command(icNum, pipe, tone_gen_id, length, abort_option, queue_option, decay_time);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_stop__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_play__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint8_t tone_seq_id)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_PLAY;

	SI479XX_HUB_CMD_TONEGEN_PLAY_ARG_TONE_GEN_ID_write_field(tone_gen_id);
	SI479XX_HUB_CMD_TONEGEN_PLAY_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TONEGEN_PLAY_ARG_TONE_SEQ_ID_write_u8(tone_seq_id);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_PLAY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_play__parse(uint8_t* buf, SI479XX_HUB_tonegen_play__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_PLAY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_PLAY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_PLAY_REP_LENGTH_value;
	replyData->TONE_GEN_ID = SI479XX_HUB_CMD_TONEGEN_PLAY_REP_TONE_GEN_ID_value;
	replyData->TONE_SEQ_ID = SI479XX_HUB_CMD_TONEGEN_PLAY_REP_TONE_SEQ_ID_value;
	replyData->TONE_QUEUE_DEPTH = SI479XX_HUB_CMD_TONEGEN_PLAY_REP_TONE_QUEUE_DEPTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_play__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_play__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_PLAY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_play__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_play(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint8_t  tone_seq_id, SI479XX_HUB_tonegen_play__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_play__command(icNum, pipe, tone_gen_id, length, tone_seq_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_play__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_add__command(uint8_t icNum,uint8_t pipe, uint8_t newtoneid, uint16_t length, uint16_t amp_abs, int16_t num_tone_repeat, SI479XX_HUB_tonegen_add_tone_descriptor__data* tone_descriptor, uint16_t tone_descriptor_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_ADD;

	SI479XX_HUB_CMD_TONEGEN_ADD_ARG_NEWTONEID_write_u8(newtoneid);
	SI479XX_HUB_CMD_TONEGEN_ADD_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_TONEGEN_ADD_ARG_AMP_ABS_write_u16(amp_abs);
	SI479XX_HUB_CMD_TONEGEN_ADD_ARG_NUM_TONE_REPEAT_write_i16(num_tone_repeat);
	for(n=0;n < tone_descriptor_length;n++) {
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_SUM_OR_MPY_N_write_field(n,tone_descriptor[n].SUM_OR_MPY);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_FALLRAMP_N_write_field(n,tone_descriptor[n].FALLRAMP);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_DECAYRAMP_N_write_field(n,tone_descriptor[n].DECAYRAMP);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_ATTACKRAMP_N_write_field(n,tone_descriptor[n].ATTACKRAMP);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_WAVETYPE_N_write_field(n,tone_descriptor[n].WAVETYPE);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_OSCID_N_write_field(n,tone_descriptor[n].OSCID);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_MOD_INDEX_N_write_field(n,tone_descriptor[n].MOD_INDEX);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_AMP_PEAK_N_write_u8(n,tone_descriptor[n].AMP_PEAK);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_AMP_DECAY_N_write_u8(n,tone_descriptor[n].AMP_DECAY);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_FREQUENCY_N_write_u16(n,tone_descriptor[n].FREQUENCY);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_START_TIME_N_write_u16(n,tone_descriptor[n].START_TIME);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_ATTACK_TIME_N_write_u16(n,tone_descriptor[n].ATTACK_TIME);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_PEAK_DURATION_N_write_field(n,tone_descriptor[n].PEAK_DURATION);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_PEAK_TIME_N_write_u16(n,tone_descriptor[n].PEAK_TIME);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_DECAY_TIME_N_write_u16(n,tone_descriptor[n].DECAY_TIME);
		SI479XX_HUB_CMD_TONEGEN_ADD_ARG_TONE_DESCRIPTOR_FALL_TIME_N_write_u16(n,tone_descriptor[n].FALL_TIME);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_ADD;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_add__parse(uint8_t* buf, SI479XX_HUB_tonegen_add__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_ADD_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_ADD_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_ADD_REP_LENGTH_value;
	replyData->NEWTONEID = SI479XX_HUB_CMD_TONEGEN_ADD_REP_NEWTONEID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_add__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_add__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_ADD)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_add__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_add(uint8_t icNum, uint8_t pipe, uint8_t  newtoneid, uint16_t  length, uint16_t  amp_abs, int16_t  num_tone_repeat, SI479XX_HUB_tonegen_add_tone_descriptor__data*  tone_descriptor, uint16_t  tone_descriptor_length, SI479XX_HUB_tonegen_add__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_add__command(icNum, pipe, newtoneid, length, amp_abs, num_tone_repeat, tone_descriptor, tone_descriptor_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_add__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_query__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_QUERY;

	SI479XX_HUB_CMD_TONEGEN_QUERY_ARG_TONE_GEN_ID_write_field(tone_gen_id);
	SI479XX_HUB_CMD_TONEGEN_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_query__parse(uint8_t* buf, SI479XX_HUB_tonegen_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_LENGTH_value;
	replyData->TONE_GEN_ID = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_TONE_GEN_ID_value;
	replyData->STATE = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_STATE_value;
	replyData->TONE_QUEUE_COUNT = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_TONE_QUEUE_COUNT_value;
	replyData->TONE_QUEUE_DEPTH = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_TONE_QUEUE_DEPTH_value;
	replyData->ACT_TONE_REPEAT = SI479XX_HUB_CMD_TONEGEN_QUERY_REP_ACT_TONE_REPEAT_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_query(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, SI479XX_HUB_tonegen_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_query__command(icNum, pipe, tone_gen_id, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_query_list__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST;

	SI479XX_HUB_CMD_TONEGEN_QUERY_LIST_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_query_list__parse(uint8_t* buf, SI479XX_HUB_tonegen_query_list__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST_REP_LENGTH_value;
	replyData->TONELIST_LENGTH = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST_REP_TONELIST_LENGTH_value;
	{
		int32_t n = 0;
		uint8_t* tonelist = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->TONELIST_LENGTH;
		replyData->TONELIST = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
		if (replyData->TONELIST == NULL) {
			return MEMORY_ERROR;
		}
		tonelist = replyData->TONELIST;
		for (n = 0; n < array_elem_count; n++) {
			tonelist[n] = SI479XX_HUB_CMD_TONEGEN_QUERY_LIST_REP_TONELIST_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_query_list__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_query_list__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_QUERY_LIST)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_query_list__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_query_list(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_tonegen_query_list__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_query_list__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_query_list__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_tonegen_query_seq_id__command(uint8_t icNum,uint8_t pipe, uint8_t tone_seq_id, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID;

	SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_ARG_TONE_SEQ_ID_write_u8(tone_seq_id);
	SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_tonegen_query_seq_id__parse(uint8_t* buf, SI479XX_HUB_tonegen_query_seq_id__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_LENGTH_value;
	replyData->TONE_SEQ_ID = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_SEQ_ID_value;
	replyData->AMP_ABS = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_AMP_ABS_value;
	replyData->NUM_TONE_REPEAT = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_NUM_TONE_REPEAT_value;
	replyData->TONE_DESCRIPTOR_LENGTH = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_LENGTH_value;
	{
		int32_t n = 0;
		SI479XX_HUB_tonegen_query_seq_id_tone_descriptor__data* tone_descriptor = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->TONE_DESCRIPTOR_LENGTH;
		replyData->TONE_DESCRIPTOR = (SI479XX_HUB_tonegen_query_seq_id_tone_descriptor__data*)malloc(sizeof(SI479XX_HUB_tonegen_query_seq_id_tone_descriptor__data) * array_elem_count);
		if (replyData->TONE_DESCRIPTOR == NULL) {
			return MEMORY_ERROR;
		}
		tone_descriptor = replyData->TONE_DESCRIPTOR;
		for (n = 0; n < array_elem_count; n++) {
			tone_descriptor[n].SUM_OR_MPY = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_SUM_OR_MPY_N_value(n);
			tone_descriptor[n].FALLRAMP = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_FALLRAMP_N_value(n);
			tone_descriptor[n].DECAYRAMP = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_DECAYRAMP_N_value(n);
			tone_descriptor[n].ATTACKRAMP = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_ATTACKRAMP_N_value(n);
			tone_descriptor[n].WAVETYPE = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_WAVETYPE_N_value(n);
			tone_descriptor[n].OSCID = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_OSCID_N_value(n);
			tone_descriptor[n].MOD_INDEX = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_MOD_INDEX_N_value(n);
			tone_descriptor[n].AMP_PEAK = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_AMP_PEAK_N_value(n);
			tone_descriptor[n].AMP_DECAY = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_AMP_DECAY_N_value(n);
			tone_descriptor[n].FREQUENCY = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_FREQUENCY_N_value(n);
			tone_descriptor[n].START_TIME = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_START_TIME_N_value(n);
			tone_descriptor[n].ATTACK_TIME = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_ATTACK_TIME_N_value(n);
			tone_descriptor[n].PEAK_DURATION = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_PEAK_DURATION_N_value(n);
			tone_descriptor[n].PEAK_TIME = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_PEAK_TIME_N_value(n);
			tone_descriptor[n].DECAY_TIME = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_DECAY_TIME_N_value(n);
			tone_descriptor[n].FALL_TIME = SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID_REP_TONE_DESCRIPTOR_FALL_TIME_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_tonegen_query_seq_id__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_query_seq_id__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_TONEGEN_QUERY_SEQ_ID)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_tonegen_query_seq_id__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_tonegen_query_seq_id(uint8_t icNum, uint8_t pipe, uint8_t  tone_seq_id, uint16_t  length, SI479XX_HUB_tonegen_query_seq_id__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_tonegen_query_seq_id__command(icNum, pipe, tone_seq_id, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_tonegen_query_seq_id__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_wave_play__command(uint8_t icNum,uint8_t pipe, uint8_t wave_gen_id, uint16_t length, uint8_t wave_count, uint8_t loop_control, uint16_t loop_count, SI479XX_HUB_wave_play_wave__data* wave)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_WAVE_PLAY;

	SI479XX_HUB_CMD_WAVE_PLAY_ARG_WAVE_GEN_ID_write_field(wave_gen_id);
	SI479XX_HUB_CMD_WAVE_PLAY_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_WAVE_PLAY_ARG_WAVE_COUNT_write_field(wave_count);
	SI479XX_HUB_CMD_WAVE_PLAY_ARG_LOOP_CONTROL_write_u8(loop_control);
	SI479XX_HUB_CMD_WAVE_PLAY_ARG_LOOP_COUNT_write_u16(loop_count);
	for(n=0;n < wave_count;n++) {
		SI479XX_HUB_CMD_WAVE_PLAY_ARG_WAVE_WAVE_ID_N_write_i8(n,wave[n].WAVE_ID);
		SI479XX_HUB_CMD_WAVE_PLAY_ARG_WAVE_NOISE_LEVEL_N_write_u8(n,wave[n].NOISE_LEVEL);
		SI479XX_HUB_CMD_WAVE_PLAY_ARG_WAVE_SILENCE_DURATION_N_write_u16(n,wave[n].SILENCE_DURATION);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_WAVE_PLAY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_wave_play__parse(uint8_t* buf, SI479XX_HUB_wave_play__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_WAVE_PLAY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_WAVE_PLAY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_WAVE_PLAY_REP_LENGTH_value;
	replyData->WAVE_GEN_ID = SI479XX_HUB_CMD_WAVE_PLAY_REP_WAVE_GEN_ID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_wave_play__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_play__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_WAVE_PLAY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_wave_play__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_wave_play(uint8_t icNum, uint8_t pipe, uint8_t  wave_gen_id, uint16_t  length, uint8_t  wave_count, uint8_t  loop_control, uint16_t  loop_count, SI479XX_HUB_wave_play_wave__data*  wave, SI479XX_HUB_wave_play__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_wave_play__command(icNum, pipe, wave_gen_id, length, wave_count, loop_control, loop_count, wave);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_wave_play__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_wave_stop__command(uint8_t icNum,uint8_t pipe, uint8_t wave_gen_id, uint16_t length, uint8_t abort_option, uint16_t decay_time)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_WAVE_STOP;

	SI479XX_HUB_CMD_WAVE_STOP_ARG_WAVE_GEN_ID_write_field(wave_gen_id);
	SI479XX_HUB_CMD_WAVE_STOP_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_WAVE_STOP_ARG_ABORT_OPTION_write_field(abort_option);
	SI479XX_HUB_CMD_WAVE_STOP_ARG_DECAY_TIME_write_u16(decay_time);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_WAVE_STOP;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_wave_stop__parse(uint8_t* buf, SI479XX_HUB_wave_stop__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_WAVE_STOP_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_WAVE_STOP_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_WAVE_STOP_REP_LENGTH_value;
	replyData->WAVE_GEN_ID = SI479XX_HUB_CMD_WAVE_STOP_REP_WAVE_GEN_ID_value;
	replyData->ABORT_OPTION = SI479XX_HUB_CMD_WAVE_STOP_REP_ABORT_OPTION_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_wave_stop__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_stop__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_WAVE_STOP)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_wave_stop__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_wave_stop(uint8_t icNum, uint8_t pipe, uint8_t  wave_gen_id, uint16_t  length, uint8_t  abort_option, uint16_t  decay_time, SI479XX_HUB_wave_stop__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_wave_stop__command(icNum, pipe, wave_gen_id, length, abort_option, decay_time);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_wave_stop__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_wave_add__command(uint8_t icNum,uint8_t pipe, uint8_t newwaveid, uint16_t length, uint8_t* wave_file_header, uint8_t* wave_data_header, uint8_t* wave_data, uint16_t wave_file_header_length, uint16_t wave_data_header_length, uint16_t wave_data_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_WAVE_ADD;

	SI479XX_HUB_CMD_WAVE_ADD_ARG_NEWWAVEID_write_field(newwaveid);
	SI479XX_HUB_CMD_WAVE_ADD_ARG_LENGTH_write_u16(length);
	for(n=0;n < wave_file_header_length;n++) {
		SI479XX_HUB_CMD_WAVE_ADD_ARG_WAVE_FILE_HEADER_N_write_u8(n,wave_file_header[n]);
	}
	for(n=0;n < wave_data_header_length;n++) {
		SI479XX_HUB_CMD_WAVE_ADD_ARG_WAVE_DATA_HEADER_N_write_u8(n,wave_data_header[n]);
	}
	for(n=0;n < wave_data_length;n++) {
		SI479XX_HUB_CMD_WAVE_ADD_ARG_WAVE_DATA_N_write_u8(n,wave_data[n]);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_WAVE_ADD;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_wave_add__parse(uint8_t* buf, SI479XX_HUB_wave_add__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_WAVE_ADD_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_WAVE_ADD_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_WAVE_ADD_REP_LENGTH_value;
	replyData->NEWWAVEID = SI479XX_HUB_CMD_WAVE_ADD_REP_NEWWAVEID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_wave_add__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_add__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_WAVE_ADD)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_wave_add__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_wave_add(uint8_t icNum, uint8_t pipe, uint8_t  newwaveid, uint16_t  length, uint8_t*  wave_file_header, uint8_t*  wave_data_header, uint8_t*  wave_data, uint16_t  wave_file_header_length, uint16_t  wave_data_header_length, uint16_t  wave_data_length, SI479XX_HUB_wave_add__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_wave_add__command(icNum, pipe, newwaveid, length, wave_file_header, wave_data_header, wave_data, wave_file_header_length, wave_data_header_length, wave_data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_wave_add__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_wave_append__command(uint8_t icNum,uint8_t pipe, uint8_t newwaveid, uint16_t length, uint8_t* wave_data_header, uint8_t* wave_data, uint16_t wave_data_header_length, uint16_t wave_data_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_WAVE_APPEND;

	SI479XX_HUB_CMD_WAVE_APPEND_ARG_NEWWAVEID_write_field(newwaveid);
	SI479XX_HUB_CMD_WAVE_APPEND_ARG_LENGTH_write_u16(length);
	for(n=0;n < wave_data_header_length;n++) {
		SI479XX_HUB_CMD_WAVE_APPEND_ARG_WAVE_DATA_HEADER_N_write_u8(n,wave_data_header[n]);
	}
	for(n=0;n < wave_data_length;n++) {
		SI479XX_HUB_CMD_WAVE_APPEND_ARG_WAVE_DATA_N_write_u8(n,wave_data[n]);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_WAVE_APPEND;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_wave_append__parse(uint8_t* buf, SI479XX_HUB_wave_append__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_WAVE_APPEND_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_WAVE_APPEND_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_WAVE_APPEND_REP_LENGTH_value;
	replyData->NEWWAVEID = SI479XX_HUB_CMD_WAVE_APPEND_REP_NEWWAVEID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_wave_append__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_append__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_WAVE_APPEND)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_wave_append__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_wave_append(uint8_t icNum, uint8_t pipe, uint8_t  newwaveid, uint16_t  length, uint8_t*  wave_data_header, uint8_t*  wave_data, uint16_t  wave_data_header_length, uint16_t  wave_data_length, SI479XX_HUB_wave_append__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_wave_append__command(icNum, pipe, newwaveid, length, wave_data_header, wave_data, wave_data_header_length, wave_data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_wave_append__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_wave_gen_query__command(uint8_t icNum,uint8_t pipe, uint8_t wave_gen_id, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_WAVE_GEN_QUERY;

	SI479XX_HUB_CMD_WAVE_GEN_QUERY_ARG_WAVE_GEN_ID_write_field(wave_gen_id);
	SI479XX_HUB_CMD_WAVE_GEN_QUERY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_WAVE_GEN_QUERY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_wave_gen_query__parse(uint8_t* buf, SI479XX_HUB_wave_gen_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_LENGTH_value;
	replyData->WAVE_GEN_ID = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_WAVE_GEN_ID_value;
	replyData->WAVE_GEN_STATE = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_WAVE_GEN_STATE_value;
	replyData->WAVE_GEN_LOOPCOUNT = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_WAVE_GEN_LOOPCOUNT_value;
	replyData->WAVE_GEN_PLAY_LIST_INDEX = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_WAVE_GEN_PLAY_LIST_INDEX_value;
	replyData->WAVE_GEN_PLAY_LIST_LENGTH = SI479XX_HUB_CMD_WAVE_GEN_QUERY_REP_WAVE_GEN_PLAY_LIST_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_wave_gen_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_gen_query__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_WAVE_GEN_QUERY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_wave_gen_query__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_wave_gen_query(uint8_t icNum, uint8_t pipe, uint8_t  wave_gen_id, uint16_t  length, SI479XX_HUB_wave_gen_query__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_wave_gen_query__command(icNum, pipe, wave_gen_id, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_wave_gen_query__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_wave_query_list__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_WAVE_QUERY_LIST;

	SI479XX_HUB_CMD_WAVE_QUERY_LIST_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_WAVE_QUERY_LIST;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_wave_query_list__parse(uint8_t* buf, SI479XX_HUB_wave_query_list__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_WAVE_QUERY_LIST_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_WAVE_QUERY_LIST_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_WAVE_QUERY_LIST_REP_LENGTH_value;
	replyData->WAVELIST_LENGTH = SI479XX_HUB_CMD_WAVE_QUERY_LIST_REP_WAVELIST_LENGTH_value;
	{
		int32_t n = 0;
		int8_t* wavelist = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->WAVELIST_LENGTH;
		replyData->WAVELIST = (int8_t*)malloc(sizeof(int8_t) * array_elem_count);
		if (replyData->WAVELIST == NULL) {
			return MEMORY_ERROR;
		}
		wavelist = replyData->WAVELIST;
		for (n = 0; n < array_elem_count; n++) {
			wavelist[n] = SI479XX_HUB_CMD_WAVE_QUERY_LIST_REP_WAVELIST_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_wave_query_list__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_query_list__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_WAVE_QUERY_LIST)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_wave_query_list__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_wave_query_list(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_wave_query_list__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_wave_query_list__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_wave_query_list__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_parameter_write__command(uint8_t icNum,uint8_t pipe, uint8_t parmid, uint16_t length, uint8_t* param, uint16_t param_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_PARAMETER_WRITE;

	SI479XX_HUB_CMD_PARAMETER_WRITE_ARG_PARMID_write_field(parmid);
	SI479XX_HUB_CMD_PARAMETER_WRITE_ARG_LENGTH_write_u16(length);
	for(n=0;n < param_length;n++) {
		SI479XX_HUB_CMD_PARAMETER_WRITE_ARG_PARAM_N_write_u8(n,param[n]);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_PARAMETER_WRITE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_parameter_write__parse(uint8_t* buf, SI479XX_HUB_parameter_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_PARAMETER_WRITE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_PARAMETER_WRITE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_PARAMETER_WRITE_REP_LENGTH_value;
	replyData->PARMID = SI479XX_HUB_CMD_PARAMETER_WRITE_REP_PARMID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_parameter_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_parameter_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_PARAMETER_WRITE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_parameter_write__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_parameter_write(uint8_t icNum, uint8_t pipe, uint8_t  parmid, uint16_t  length, uint8_t*  param, uint16_t  param_length, SI479XX_HUB_parameter_write__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_parameter_write__command(icNum, pipe, parmid, length, param, param_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_parameter_write__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_parameter_read__command(uint8_t icNum,uint8_t pipe, uint8_t parmid, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_PARAMETER_READ;

	SI479XX_HUB_CMD_PARAMETER_READ_ARG_PARMID_write_field(parmid);
	SI479XX_HUB_CMD_PARAMETER_READ_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_PARAMETER_READ;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_parameter_read__parse(uint8_t* buf, SI479XX_HUB_parameter_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_PARAMETER_READ_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_PARAMETER_READ_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_PARAMETER_READ_REP_LENGTH_value;
	replyData->PARMID = SI479XX_HUB_CMD_PARAMETER_READ_REP_PARMID_value;
	replyData->PARAM_LENGTH = SI479XX_HUB_CMD_PARAMETER_READ_REP_PARAM_LENGTH_value;
	{
		int32_t n = 0;
		uint8_t* param = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->PARAM_LENGTH;
		replyData->PARAM = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
		if (replyData->PARAM == NULL) {
			return MEMORY_ERROR;
		}
		param = replyData->PARAM;
		for (n = 0; n < array_elem_count; n++) {
			param[n] = SI479XX_HUB_CMD_PARAMETER_READ_REP_PARAM_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_parameter_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_parameter_read__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_PARAMETER_READ)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_parameter_read__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_parameter_read(uint8_t icNum, uint8_t pipe, uint8_t  parmid, uint16_t  length, SI479XX_HUB_parameter_read__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_parameter_read__command(icNum, pipe, parmid, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_parameter_read__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_coeff_write__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t filterid, uint8_t ntaps, int32_t* coeff, uint16_t coeff_length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	int n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_COEFF_WRITE;

	SI479XX_HUB_CMD_COEFF_WRITE_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_COEFF_WRITE_ARG_FILTERID_write_field(filterid);
	SI479XX_HUB_CMD_COEFF_WRITE_ARG_NTAPS_write_field(ntaps);
	for(n=0;n < coeff_length;n++) {
		SI479XX_HUB_CMD_COEFF_WRITE_ARG_COEFF_N_write_i32(n,coeff[n]);
	}

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_COEFF_WRITE;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_coeff_write__parse(uint8_t* buf, SI479XX_HUB_coeff_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_COEFF_WRITE_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_COEFF_WRITE_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_COEFF_WRITE_REP_LENGTH_value;
	replyData->FILTERID = SI479XX_HUB_CMD_COEFF_WRITE_REP_FILTERID_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_coeff_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_coeff_write__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_COEFF_WRITE)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_coeff_write__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_coeff_write(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  filterid, uint8_t  ntaps, int32_t*  coeff, uint16_t  coeff_length, SI479XX_HUB_coeff_write__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_coeff_write__command(icNum, pipe, length, filterid, ntaps, coeff, coeff_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_coeff_write__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_set_dac_offset__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint32_t dac_offset)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SET_DAC_OFFSET;

	SI479XX_HUB_CMD_SET_DAC_OFFSET_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_SET_DAC_OFFSET_ARG_DAC_OFFSET_write_u32(dac_offset);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SET_DAC_OFFSET;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_set_dac_offset__parse(uint8_t* buf, SI479XX_HUB_set_dac_offset__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SET_DAC_OFFSET_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SET_DAC_OFFSET_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SET_DAC_OFFSET_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_set_dac_offset__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_dac_offset__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SET_DAC_OFFSET)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_set_dac_offset__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_set_dac_offset(uint8_t icNum, uint8_t pipe, uint16_t  length, uint32_t  dac_offset, SI479XX_HUB_set_dac_offset__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_set_dac_offset__command(icNum, pipe, length, dac_offset);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_set_dac_offset__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_query_dac_offset__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_QUERY_DAC_OFFSET;

	SI479XX_HUB_CMD_QUERY_DAC_OFFSET_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_QUERY_DAC_OFFSET;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_query_dac_offset__parse(uint8_t* buf, SI479XX_HUB_query_dac_offset__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_QUERY_DAC_OFFSET_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_QUERY_DAC_OFFSET_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_QUERY_DAC_OFFSET_REP_LENGTH_value;
	replyData->DAC_OFFSET = SI479XX_HUB_CMD_QUERY_DAC_OFFSET_REP_DAC_OFFSET_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_query_dac_offset__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_dac_offset__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_QUERY_DAC_OFFSET)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_query_dac_offset__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_query_dac_offset(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_query_dac_offset__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_query_dac_offset__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_query_dac_offset__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_set_adc_dc_notch__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint32_t dc_alpha)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_SET_ADC_DC_NOTCH;

	SI479XX_HUB_CMD_SET_ADC_DC_NOTCH_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_SET_ADC_DC_NOTCH_ARG_DC_ALPHA_write_u32(dc_alpha);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_SET_ADC_DC_NOTCH;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_set_adc_dc_notch__parse(uint8_t* buf, SI479XX_HUB_set_adc_dc_notch__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_SET_ADC_DC_NOTCH_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_SET_ADC_DC_NOTCH_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_SET_ADC_DC_NOTCH_REP_LENGTH_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_set_adc_dc_notch__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_adc_dc_notch__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_SET_ADC_DC_NOTCH)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_set_adc_dc_notch__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_set_adc_dc_notch(uint8_t icNum, uint8_t pipe, uint16_t  length, uint32_t  dc_alpha, SI479XX_HUB_set_adc_dc_notch__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_set_adc_dc_notch__command(icNum, pipe, length, dc_alpha);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_set_adc_dc_notch__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_query_topology_memory__command(uint8_t icNum,uint8_t pipe, uint16_t length)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY;

	SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY_ARG_LENGTH_write_u16(length);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_query_topology_memory__parse(uint8_t* buf, SI479XX_HUB_query_topology_memory__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY_REP_LENGTH_value;
	replyData->TOP_SIZE = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY_REP_TOP_SIZE_value;
	replyData->PRM_SIZE = SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY_REP_PRM_SIZE_value;
	return ret;
}
RETURN_CODE SI479XX_HUB_query_topology_memory__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_topology_memory__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_QUERY_TOPOLOGY_MEMORY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_query_topology_memory__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_query_topology_memory(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_query_topology_memory__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_query_topology_memory__command(icNum, pipe, length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_query_topology_memory__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_HUB_read_topology_memory__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint16_t prm_offset, uint16_t prm_size)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	ClearMemory(hifi_data_arg, length + HIFI_HEADER_SIZE);

	hifi_data_arg[0] = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY;

	SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_ARG_LENGTH_write_u16(length);
	SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_ARG_PRM_OFFSET_write_u16(prm_offset);
	SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_ARG_PRM_SIZE_write_u16(prm_size);

	ret = iapis_hifiWriteCommand(icNum, pipe, 1, length + HIFI_HEADER_SIZE, hifi_data_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY;
	}

	return ret;

}

RETURN_CODE SI479XX_HUB_read_topology_memory__parse(uint8_t* buf, SI479XX_HUB_read_topology_memory__data* replyData)
{
	RETURN_CODE ret = SUCCESS;

	setReplyBufferPointers(buf);
	replyData->CMD_NUM = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_REP_CMD_NUM_value;
	replyData->STATUS = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_REP_STATUS_value;
	replyData->LENGTH = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_REP_LENGTH_value;
	replyData->PRM_OFFSET = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_REP_PRM_OFFSET_value;
	replyData->PRM_SIZE = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_REP_PRM_SIZE_value;
	{
		int32_t n = 0;
		uint8_t* data = NULL;
		int32_t array_elem_count = 0;

		array_elem_count = replyData->PRM_SIZE;
		replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
		if (replyData->DATA == NULL) {
			return MEMORY_ERROR;
		}
		data = replyData->DATA;
		for (n = 0; n < array_elem_count; n++) {
			data[n] = SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY_REP_DATA_N_value(n);
 
		}

	}
	return ret;
}
RETURN_CODE SI479XX_HUB_read_topology_memory__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_read_topology_memory__data* replyData)
{
	RETURN_CODE ret = SUCCESS;
	uint8_t icIndex = 0;
	SI479XX_hifi_resp__data response_data = {0};

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS || !(pipe < SI479XX_MAX_NUM_PIPES && pipe >= 0)) {
		return INVALID_INPUT;
	}
	response_data.DATA = NULL;
	if ((ret == SUCCESS) && (lastCommands[icIndex] == SI479XX_HUB_CMD_READ_TOPOLOGY_MEMORY)) {
		// Memory gets allocated to response_data.DATA by this call and must be freed
		ret = iapis_hifiReadReply(icNum, pipe, &response_data);
		if (ret == SUCCESS) {
			ret = SI479XX_HUB_read_topology_memory__parse(response_data.DATA, replyData);
			free(response_data.DATA);
		}
	}
	return ret;
}


RETURN_CODE SI479XX_HUB_read_topology_memory(uint8_t icNum, uint8_t pipe, uint16_t  length, uint16_t  prm_offset, uint16_t  prm_size, SI479XX_HUB_read_topology_memory__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_HUB_read_topology_memory__command(icNum, pipe, length, prm_offset, prm_size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_HUB_read_topology_memory__reply(icNum, pipe, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
