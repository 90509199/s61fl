/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_flash_api_h_global_
		
#include "main.h"

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "all_apis.h"

#include "si479xx_flash_api_constants.h"
#include "si479xx_flash_api.h"
#endif	/*0*/

#if 1		/* 210427 SGsoft */
extern uint32_t silab_cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];						
extern uint32_t silab_reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];

static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t* cmd_arg_u32 = (uint32_t *)&silab_cmd_arg_u32[1];
static uint16_t*cmd_arg_u16 = (uint16_t*)&silab_cmd_arg_u32[1];
static uint8_t*	cmd_arg = (uint8_t*)&silab_cmd_arg_u32[1];
static int32_t*	cmd_arg_i32 = (int32_t*)&silab_cmd_arg_u32[1];
static int16_t*	cmd_arg_i16 = (int16_t*)&silab_cmd_arg_u32[1];

static uint32_t* reply_arg_u32 = (uint32_t *)&silab_reply_buffer_u32[4];
static uint16_t* reply_arg_u16 = (uint16_t*)&silab_reply_buffer_u32[4];
static uint8_t*	reply_arg = (uint8_t*)&silab_reply_buffer_u32[4];
static int32_t* reply_arg_i32 = (int32_t*)&silab_reply_buffer_u32[4];
static int16_t* reply_arg_i16 = (int16_t*)&silab_reply_buffer_u32[4];

#else
static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t	cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];
static uint16_t*	cmd_arg_u16 = (uint16_t*)cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)cmd_arg_u32;

static uint32_t	reply_arg_u32[SI479XX_RPY_BUFFER_LENGTH/4];
static uint16_t*	reply_arg_u16 = (uint16_t*)reply_arg_u32;
static uint8_t*	reply_arg = (uint8_t*)reply_arg_u32;
static int32_t* 	reply_arg_i32 = (int32_t*)reply_arg_u32;
static int16_t* 	reply_arg_i16 = (int16_t*)reply_arg_u32;
#endif	/*1*/

RETURN_CODE SI479XX_FLASH_load_img__command(uint8_t icNum,uint32_t addr, uint32_t size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_FLASH_CMD_LOAD_IMG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_FLASH_CMD_LOAD_IMG;

	SI479XX_FLASH_CMD_LOAD_IMG_ARG_ADDR_write_u32(addr);
	SI479XX_FLASH_CMD_LOAD_IMG_ARG_SIZE_write_u32(size);

	ret = SI479XX_FLASH__FLASH_LOAD_SUB(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_FLASH_CMD_LOAD_IMG;
	}

	return ret;

}


RETURN_CODE SI479XX_FLASH_load_img(uint8_t icNum, uint32_t  addr, uint32_t  size)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_FLASH_load_img__command(icNum, addr, size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_FLASH_get_prop__command(uint8_t icNum,uint16_t prop_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_FLASH_CMD_GET_PROP_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_FLASH_CMD_GET_PROP;

	SI479XX_FLASH_CMD_GET_PROP_ARG_PROP_ID_write_u16(prop_id);

	ret = SI479XX_FLASH__FLASH_LOAD_SUB(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_FLASH_CMD_GET_PROP;
	}

	return ret;

}

RETURN_CODE SI479XX_FLASH_get_prop__reply(uint8_t icNum, uint16_t* prop_val)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (prop_val == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_FLASH_CMD_GET_PROP) {
		rdrep_len = SI479XX_FLASH_CMD_GET_PROP_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			*prop_val = SI479XX_FLASH_CMD_GET_PROP_REP_PROP_VAL_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_FLASH_get_prop(uint8_t icNum, uint16_t  prop_id, uint16_t*  prop_val)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_FLASH_get_prop__command(icNum, prop_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (prop_val != NULL) {
		r |= SI479XX_FLASH_get_prop__reply(icNum, prop_val);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}




RETURN_CODE SI479XX_FLASH_set_prop__command(uint8_t icNum, SI479XX_FLASH_set_prop_propset__data* propset, uint16_t propset_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if(icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = 2 + SI479XX_FLASH_CMD_SET_PROP_LENGTH + (propset_length * sizeof(SI479XX_FLASH_set_prop_propset__data));
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_FLASH_CMD_SET_PROP;

	for(n=0;n < propset_length;n++)
	{
		SI479XX_FLASH_CMD_SET_PROP_ARG_PROPSET_PROP_ID_N_write_u16(n,propset[n].PROP_ID);
		SI479XX_FLASH_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_N_write_u16(n,propset[n].PROP_VAL);
	}

	ret = SI479XX_FLASH__FLASH_LOAD_SUB(icNum, cmd_len, cmd_arg);

	if(ret == SUCCESS)
	{
		lastCommands[icIndex] = SI479XX_FLASH_CMD_SET_PROP;
	}

	return ret;

}


RETURN_CODE SI479XX_FLASH_set_prop(uint8_t icNum, SI479XX_FLASH_set_prop_propset__data*  propset, uint16_t  propset_length)
{
	RETURN_CODE r = 0;
	r = SI479XX_FLASH_set_prop__command(icNum, propset, propset_length);
	return r;
}


RETURN_CODE SI479XX_FLASH_read_block__command(uint8_t icNum,uint32_t addr, uint32_t size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_FLASH_CMD_READ_BLOCK_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_FLASH_CMD_READ_BLOCK;

	SI479XX_FLASH_CMD_READ_BLOCK_ARG_ADDR_write_u32(addr);
	SI479XX_FLASH_CMD_READ_BLOCK_ARG_SIZE_write_u32(size);

	ret = SI479XX_FLASH__FLASH_LOAD_SUB(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_FLASH_CMD_READ_BLOCK;
	}

	return ret;

}

RETURN_CODE SI479XX_FLASH_read_block__reply(uint8_t icNum, uint8_t* data, uint16_t size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_FLASH_CMD_READ_BLOCK) {
		rdrep_len = SI479XX_FLASH_CMD_READ_BLOCK_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_FLASH_CMD_READ_BLOCK_REP_LENGTH + SUBCMD_HEADER_SIZE + (size * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_arg);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = size;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_FLASH_CMD_READ_BLOCK_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_FLASH_read_block(uint8_t icNum, uint32_t  addr, uint32_t  arg_size, uint8_t*  data, uint16_t  rep_size)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_FLASH_read_block__command(icNum, addr, arg_size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (data != NULL) {
		r |= SI479XX_FLASH_read_block__reply(icNum, data, rep_size);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
