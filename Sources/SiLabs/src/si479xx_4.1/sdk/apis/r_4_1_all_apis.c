/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_all_apis_h_global_
	
#include "main.h"

///#include "all_apis.h"
///#include "core/debug_macro.h"


RETURN_CODE iapis_writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer)
{
    D_ASSERT_MSG(g_all_apis_functions.write_command, "write_command implementation not provided!");
    if (!g_all_apis_functions.write_command)
        return UNSUPPORTED_FUNCTION;

    return g_all_apis_functions.write_command(icNum, length, buffer);
}

RETURN_CODE iapis_readReply(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer)
{
    D_ASSERT_MSG(g_all_apis_functions.read_reply, "read_reply implementation not provided!");
    if (!g_all_apis_functions.read_reply)
        return UNSUPPORTED_FUNCTION;

    return g_all_apis_functions.read_reply(icNum, length, buffer);
}

RETURN_CODE iapis_hifiWriteCommand(dc_config_chip_select icNum, uint8_t pipe, uint8_t packet_count, uint16_t length, uint8_t* buffer)
{ 
    D_ASSERT_MSG(g_all_apis_functions.hifi_command, "hifi_command implementation not provided!");
    if (!g_all_apis_functions.hifi_command)
        return UNSUPPORTED_FUNCTION;

    return g_all_apis_functions.hifi_command(icNum, pipe, packet_count, length, buffer);
}

RETURN_CODE iapis_hifiReadReply(dc_config_chip_select icNum, uint8_t pipe, SI479XX_hifi_resp__data* resp)
{ 
    D_ASSERT_MSG(g_all_apis_functions.hifi_response, "hifi_response implementation not provided!");
    if (!g_all_apis_functions.hifi_response)
        return UNSUPPORTED_FUNCTION;

    if (!resp)
        return INVALID_INPUT;

    return g_all_apis_functions.hifi_response(icNum, pipe, resp);
}

bool iapis_isSatellite(demod_id demod)
{
    return demod.select != DEMOD_SELECT_INVALID
        && demod.select != DEMOD_SELECT_SINGLE
        && demod.select != DEMOD_SELECT_LINKER;
}

RETURN_CODE iapis_acquireLock(dc_config_chip_select ic)
{
    if (g_all_apis_functions.acquire_lock)
        return g_all_apis_functions.acquire_lock(ic);
    else
	return SUCCESS;
}


RETURN_CODE iapis_releaseLock(dc_config_chip_select ic)
{
    if (g_all_apis_functions.release_lock)
        return g_all_apis_functions.release_lock(ic);
    else
	return SUCCESS;
}

#ifdef OPTION_CONVERT_BIG_ENDIAN
uint16_t _swap_16(uint16_t value)
{
    uint16_t swapped; 
    uint8_t* swappedBytes = (uint8_t*)&swapped;
    uint8_t* valueBytes = (uint8_t*)&value;

    swappedBytes[0] = valueBytes[1];
    swappedBytes[1] = valueBytes[0];

    return swapped;
}

uint32_t _swap_32 (uint32_t value)
{
    uint32_t swapped; 
    uint8_t* swappedBytes = (uint8_t*)&swapped;
    uint8_t* valueBytes = (uint8_t*)&value;

    swappedBytes[0] = valueBytes[3];
    swappedBytes[1] = valueBytes[2];
    swappedBytes[2] = valueBytes[1];
    swappedBytes[3] = valueBytes[0];

    return swapped;
}
#endif


uint8_t getCommandIndex(uint8_t icNum)
{
	switch(icNum)
	{
	case 0x01:
		return 0;
	case 0x02:
		return 1;
	case 0x04:
		return 2;
	case 0x08:
		return 3;
	case 0x10:
		return 4;
	case 0x20:
		return 5;
	case 0x40:
		return 6;
	case 0x80:
		return 7;
	default:
		return MAX_NUM_PARTS;
	}
}

#endif	/*SI479XX_FIRM_REL_4_1*/
