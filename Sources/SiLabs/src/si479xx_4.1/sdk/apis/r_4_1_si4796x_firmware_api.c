/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si4796x command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si4796x_firmware_api_h_global_
		
#include "main.h"

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "all_apis.h"

#include "si4796x_firmware_api_constants.h"
#include "si4796x_firmware_api.h"
#endif	/*0*/

#if 1		/* 210427 SGsoft */
extern uint32_t silab_cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];						
extern uint32_t silab_reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];

static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t	*cmd_arg_u32 = (uint32_t *)&silab_cmd_arg_u32;
static uint16_t*cmd_arg_u16 = (uint16_t*)&silab_cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)&silab_cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)&silab_cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)&silab_cmd_arg_u32;

static uint32_t	*reply_buffer_u32 = (uint32_t *)&silab_reply_buffer_u32;
static uint16_t* reply_buffer_u16 = (uint16_t*)&silab_reply_buffer_u32;
static uint8_t*	reply_buffer = (uint8_t*)&silab_reply_buffer_u32;
static int32_t* reply_buffer_i32 = (int32_t*)&silab_reply_buffer_u32;
static int16_t* reply_buffer_i16 = (int16_t*)&silab_reply_buffer_u32;

#else
#ifndef SI4796X_CMD_BUFFER_LENGTH
#define SI4796X_CMD_BUFFER_LENGTH (4096 + 32)
#endif

#ifndef SI4796X_RPY_BUFFER_LENGTH
#define SI4796X_RPY_BUFFER_LENGTH (8192)
#endif


static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t	cmd_arg_u32[SI4796X_CMD_BUFFER_LENGTH/4];
static uint16_t*	cmd_arg_u16 = (uint16_t*)cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)cmd_arg_u32;

static uint32_t	reply_buffer_u32[SI4796X_RPY_BUFFER_LENGTH/4];
static uint16_t*	reply_buffer_u16 = (uint16_t*)reply_buffer_u32;
static uint8_t*	reply_buffer = (uint8_t*)reply_buffer_u32;
static int32_t* 	reply_buffer_i32 = (int32_t*)reply_buffer_u32;
static int16_t* 	reply_buffer_i16 = (int16_t*)reply_buffer_u32;
#endif	/*1*/


RETURN_CODE SI4796X_read_status__reply(uint8_t icNum, SI4796X_read_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	if (lastCommands[icIndex] == SI4796X_CMD_READ_STATUS) {
		rdrep_len = SI4796X_CMD_READ_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->CTS = SI4796X_CMD_READ_STATUS_REP_CTS_value;
			replyData->APIERR = SI4796X_CMD_READ_STATUS_REP_APIERR_value;
			replyData->SWERR = SI4796X_CMD_READ_STATUS_REP_SWERR_value;
			replyData->VDERR = SI4796X_CMD_READ_STATUS_REP_VDERR_value;
			replyData->ALERTINT = SI4796X_CMD_READ_STATUS_REP_ALERTINT_value;
			replyData->RDSINT = SI4796X_CMD_READ_STATUS_REP_RDSINT_value;
			replyData->STCINT = SI4796X_CMD_READ_STATUS_REP_STCINT_value;
			replyData->RDS1INT = SI4796X_CMD_READ_STATUS_REP_RDS1INT_value;
			replyData->STC1INT = SI4796X_CMD_READ_STATUS_REP_STC1INT_value;
			replyData->AUDINT = SI4796X_CMD_READ_STATUS_REP_AUDINT_value;
			replyData->ACTS3 = SI4796X_CMD_READ_STATUS_REP_ACTS3_value;
			replyData->ACTS2 = SI4796X_CMD_READ_STATUS_REP_ACTS2_value;
			replyData->ACTS1 = SI4796X_CMD_READ_STATUS_REP_ACTS1_value;
			replyData->ACTS0 = SI4796X_CMD_READ_STATUS_REP_ACTS0_value;
			replyData->PUP_STATE = SI4796X_CMD_READ_STATUS_REP_PUP_STATE_value;
			replyData->HOTWRN = SI4796X_CMD_READ_STATUS_REP_HOTWRN_value;
			replyData->TMPERR = SI4796X_CMD_READ_STATUS_REP_TMPERR_value;
			replyData->CMDERR = SI4796X_CMD_READ_STATUS_REP_CMDERR_value;
			replyData->REPERR = SI4796X_CMD_READ_STATUS_REP_REPERR_value;
			replyData->ARBERR = SI4796X_CMD_READ_STATUS_REP_ARBERR_value;
			replyData->NRERR = SI4796X_CMD_READ_STATUS_REP_NRERR_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_read_status(uint8_t icNum, SI4796X_read_status__data*  replyData)
{
	RETURN_CODE r = 0;
	uint8_t icIndex = 0;
	uint8_t origCmd = 0;
	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	iapis_acquireLock(icNum);
	// Workaround to avoid changing generated reply wihtout a real command.
	origCmd = lastCommands[icIndex];
	lastCommands[icIndex] = SI4796X_CMD_READ_STATUS;
	r = SI4796X_read_status__reply(icNum, replyData);
	lastCommands[icIndex] = origCmd;
	iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_power_up__command(uint8_t icNum,uint8_t ctsien, uint8_t clko_current, uint8_t vio, uint8_t clkout, uint8_t clk_mode, uint8_t tr_size, uint8_t ctun, uint32_t xtal_freq, uint8_t ibias, uint8_t afs, uint8_t chipid, uint8_t eziq_master, uint8_t eziq_enable)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_POWER_UP_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_POWER_UP;

	SI4796X_CMD_POWER_UP_ARG_CTSIEN_write_field(ctsien);
	SI4796X_CMD_POWER_UP_ARG_CLKO_CURRENT_write_field(clko_current);
	SI4796X_CMD_POWER_UP_ARG_VIO_write_field(vio);
	SI4796X_CMD_POWER_UP_ARG_CLKOUT_write_field(clkout);
	SI4796X_CMD_POWER_UP_ARG_CLK_MODE_write_field(clk_mode);
	SI4796X_CMD_POWER_UP_ARG_TR_SIZE_write_field(tr_size);
	SI4796X_CMD_POWER_UP_ARG_CTUN_write_field(ctun);
	SI4796X_CMD_POWER_UP_ARG_XTAL_FREQ_write_u32(xtal_freq);
	SI4796X_CMD_POWER_UP_ARG_IBIAS_write_u8(ibias);
	SI4796X_CMD_POWER_UP_ARG_AFS_write_field(afs);
	SI4796X_CMD_POWER_UP_ARG_CHIPID_write_field(chipid);
	SI4796X_CMD_POWER_UP_ARG_EZIQ_MASTER_write_field(eziq_master);
	SI4796X_CMD_POWER_UP_ARG_EZIQ_ENABLE_write_field(eziq_enable);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_POWER_UP;
	}

	return ret;

}

RETURN_CODE SI4796X_power_up__reply(uint8_t icNum, SI4796X_power_up__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_POWER_UP) {
		rdrep_len = SI4796X_CMD_POWER_UP_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->UNCORRECTABLE = SI4796X_CMD_POWER_UP_REP_UNCORRECTABLE_value;
			replyData->CORRECTABLE = SI4796X_CMD_POWER_UP_REP_CORRECTABLE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_power_up(uint8_t icNum, uint8_t  ctsien, uint8_t  clko_current, uint8_t  vio, uint8_t  clkout, uint8_t  clk_mode, uint8_t  tr_size, uint8_t  ctun, uint32_t  xtal_freq, uint8_t  ibias, uint8_t  afs, uint8_t  chipid, uint8_t  eziq_master, uint8_t  eziq_enable, SI4796X_power_up__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_power_up__command(icNum, ctsien, clko_current, vio, clkout, clk_mode, tr_size, ctun, xtal_freq, ibias, afs, chipid, eziq_master, eziq_enable);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_power_up__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_err__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_ERR_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_ERR;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_ERR;
	}

	return ret;

}

RETURN_CODE SI4796X_err__reply(uint8_t icNum, uint8_t* code)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (code == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_ERR) {
		rdrep_len = SI4796X_CMD_ERR_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			*code = SI4796X_CMD_ERR_REP_CODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_err(uint8_t icNum, uint8_t*  code)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_err__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (code != NULL) {
		r |= SI4796X_err__reply(icNum, code);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_cmd0__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_CMD0_LENGTH + length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_CMD0;

	SI4796X_CMD_HIFI_CMD0_ARG_PACKET_COUNT_write_u8(packet_count);
	SI4796X_CMD_HIFI_CMD0_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI4796X_CMD_HIFI_CMD0_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_CMD0;
	}

	return ret;

}


RETURN_CODE SI4796X_hifi_cmd0(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_cmd0__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_cmd1__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_CMD1_LENGTH + length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_CMD1;

	SI4796X_CMD_HIFI_CMD1_ARG_PACKET_COUNT_write_u8(packet_count);
	SI4796X_CMD_HIFI_CMD1_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI4796X_CMD_HIFI_CMD1_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_CMD1;
	}

	return ret;

}


RETURN_CODE SI4796X_hifi_cmd1(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_cmd1__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_cmd2__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_CMD2_LENGTH + length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_CMD2;

	SI4796X_CMD_HIFI_CMD2_ARG_PACKET_COUNT_write_u8(packet_count);
	SI4796X_CMD_HIFI_CMD2_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI4796X_CMD_HIFI_CMD2_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_CMD2;
	}

	return ret;

}


RETURN_CODE SI4796X_hifi_cmd2(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_cmd2__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_cmd3__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_CMD3_LENGTH + length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_CMD3;

	SI4796X_CMD_HIFI_CMD3_ARG_PACKET_COUNT_write_u8(packet_count);
	SI4796X_CMD_HIFI_CMD3_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI4796X_CMD_HIFI_CMD3_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_CMD3;
	}

	return ret;

}


RETURN_CODE SI4796X_hifi_cmd3(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_cmd3__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_resp0__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_RESP0_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_RESP0;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_RESP0;
	}

	return ret;

}

RETURN_CODE SI4796X_hifi_resp0__reply(uint8_t icNum, SI4796X_hifi_resp0__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_HIFI_RESP0) {
		rdrep_len = SI4796X_CMD_HIFI_RESP0_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI4796X_CMD_HIFI_RESP0_REP_PIPE_value;
			replyData->PACKET_COUNT = SI4796X_CMD_HIFI_RESP0_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI4796X_CMD_HIFI_RESP0_REP_LENGTH_value;

			calculated_response_length = SI4796X_CMD_HIFI_RESP0_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_HIFI_RESP0_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_hifi_resp0(uint8_t icNum, SI4796X_hifi_resp0__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_resp0__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_hifi_resp0__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_resp1__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_RESP1_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_RESP1;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_RESP1;
	}

	return ret;

}

RETURN_CODE SI4796X_hifi_resp1__reply(uint8_t icNum, SI4796X_hifi_resp1__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_HIFI_RESP1) {
		rdrep_len = SI4796X_CMD_HIFI_RESP1_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI4796X_CMD_HIFI_RESP1_REP_PIPE_value;
			replyData->PACKET_COUNT = SI4796X_CMD_HIFI_RESP1_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI4796X_CMD_HIFI_RESP1_REP_LENGTH_value;

			calculated_response_length = SI4796X_CMD_HIFI_RESP1_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_HIFI_RESP1_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_hifi_resp1(uint8_t icNum, SI4796X_hifi_resp1__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_resp1__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_hifi_resp1__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_resp2__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_RESP2_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_RESP2;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_RESP2;
	}

	return ret;

}

RETURN_CODE SI4796X_hifi_resp2__reply(uint8_t icNum, SI4796X_hifi_resp2__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_HIFI_RESP2) {
		rdrep_len = SI4796X_CMD_HIFI_RESP2_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI4796X_CMD_HIFI_RESP2_REP_PIPE_value;
			replyData->PACKET_COUNT = SI4796X_CMD_HIFI_RESP2_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI4796X_CMD_HIFI_RESP2_REP_LENGTH_value;

			calculated_response_length = SI4796X_CMD_HIFI_RESP2_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_HIFI_RESP2_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_hifi_resp2(uint8_t icNum, SI4796X_hifi_resp2__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_resp2__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_hifi_resp2__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_resp3__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_RESP3_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_RESP3;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_RESP3;
	}

	return ret;

}

RETURN_CODE SI4796X_hifi_resp3__reply(uint8_t icNum, SI4796X_hifi_resp3__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_HIFI_RESP3) {
		rdrep_len = SI4796X_CMD_HIFI_RESP3_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI4796X_CMD_HIFI_RESP3_REP_PIPE_value;
			replyData->PACKET_COUNT = SI4796X_CMD_HIFI_RESP3_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI4796X_CMD_HIFI_RESP3_REP_LENGTH_value;

			calculated_response_length = SI4796X_CMD_HIFI_RESP3_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_HIFI_RESP3_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_hifi_resp3(uint8_t icNum, SI4796X_hifi_resp3__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_resp3__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_hifi_resp3__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_datalogger_source__command(uint8_t icNum,uint8_t source)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DATALOGGER_SOURCE_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DATALOGGER_SOURCE;

	SI4796X_CMD_DATALOGGER_SOURCE_ARG_SOURCE_write_field(source);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DATALOGGER_SOURCE;
	}

	return ret;

}


RETURN_CODE SI4796X_datalogger_source(uint8_t icNum, uint8_t  source)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_datalogger_source__command(icNum, source);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_datalogger_trace__command(uint8_t icNum,uint8_t group_id, uint8_t destination)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DATALOGGER_TRACE_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DATALOGGER_TRACE;

	SI4796X_CMD_DATALOGGER_TRACE_ARG_GROUP_ID_write_field(group_id);
	SI4796X_CMD_DATALOGGER_TRACE_ARG_DESTINATION_write_field(destination);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DATALOGGER_TRACE;
	}

	return ret;

}


RETURN_CODE SI4796X_datalogger_trace(uint8_t icNum, uint8_t  group_id, uint8_t  destination)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_datalogger_trace__command(icNum, group_id, destination);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_fmhd_diversity__command(uint8_t icNum,uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FMHD_DIVERSITY_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FMHD_DIVERSITY;

	SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_write_field(mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FMHD_DIVERSITY;
	}

	return ret;

}


RETURN_CODE SI4796X_fmhd_diversity(uint8_t icNum, uint8_t  mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_fmhd_diversity__command(icNum, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_fmhd_diversity_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FMHD_DIVERSITY_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FMHD_DIVERSITY_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FMHD_DIVERSITY_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_fmhd_diversity_status__reply(uint8_t icNum, SI4796X_fmhd_diversity_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_FMHD_DIVERSITY_STATUS) {
		rdrep_len = SI4796X_CMD_FMHD_DIVERSITY_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->ACQUIRED = SI4796X_CMD_FMHD_DIVERSITY_STATUS_REP_ACQUIRED_value;
			replyData->MODE = SI4796X_CMD_FMHD_DIVERSITY_STATUS_REP_MODE_value;
			replyData->PSMI = SI4796X_CMD_FMHD_DIVERSITY_STATUS_REP_PSMI_value;
			replyData->COMB_WEIGHT = SI4796X_CMD_FMHD_DIVERSITY_STATUS_REP_COMB_WEIGHT_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_fmhd_diversity_status(uint8_t icNum, SI4796X_fmhd_diversity_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_fmhd_diversity_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_fmhd_diversity_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_fmhd_diversity_acquire__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FMHD_DIVERSITY_ACQUIRE_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FMHD_DIVERSITY_ACQUIRE;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FMHD_DIVERSITY_ACQUIRE;
	}

	return ret;

}


RETURN_CODE SI4796X_fmhd_diversity_acquire(uint8_t icNum)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_fmhd_diversity_acquire__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_fm_phase_diversity__command(uint8_t icNum,uint8_t audio, uint8_t comb)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FM_PHASE_DIVERSITY_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FM_PHASE_DIVERSITY;

	SI4796X_CMD_FM_PHASE_DIVERSITY_ARG_AUDIO_write_field(audio);
	SI4796X_CMD_FM_PHASE_DIVERSITY_ARG_COMB_write_field(comb);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FM_PHASE_DIVERSITY;
	}

	return ret;

}


RETURN_CODE SI4796X_fm_phase_diversity(uint8_t icNum, uint8_t  audio, uint8_t  comb)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_fm_phase_diversity__command(icNum, audio, comb);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_fm_phase_div_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FM_PHASE_DIV_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FM_PHASE_DIV_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FM_PHASE_DIV_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_fm_phase_div_status__reply(uint8_t icNum, SI4796X_fm_phase_div_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_FM_PHASE_DIV_STATUS) {
		rdrep_len = SI4796X_CMD_FM_PHASE_DIV_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->AUDIO = SI4796X_CMD_FM_PHASE_DIV_STATUS_REP_AUDIO_value;
			replyData->COMB = SI4796X_CMD_FM_PHASE_DIV_STATUS_REP_COMB_value;
			replyData->PHSDIVCORR = SI4796X_CMD_FM_PHASE_DIV_STATUS_REP_PHSDIVCORR_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_fm_phase_div_status(uint8_t icNum, SI4796X_fm_phase_div_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_fm_phase_div_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_fm_phase_div_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_fm_phase_diversity_local_metrics__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS;
	}

	return ret;

}

RETURN_CODE SI4796X_fm_phase_diversity_local_metrics__reply(uint8_t icNum, SI4796X_fm_phase_diversity_local_metrics__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS) {
		rdrep_len = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->RSSIDB = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_RSSIDB_value;
			replyData->RSSIDB_ADJ = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_RSSIDB_ADJ_value;
			replyData->RSSINCDB = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_RSSINCDB_value;
			replyData->RSSINCDB_ADJ = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_RSSINCDB_ADJ_value;
			replyData->CHAN_BW = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_CHAN_BW_value;
			replyData->LASSI = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_LASSI_value;
			replyData->HASSI = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_HASSI_value;
			replyData->ASSI100 = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_ASSI100_value;
			replyData->LASSI200 = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_LASSI200_value;
			replyData->HASSI200 = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_HASSI200_value;
			replyData->ASSI200 = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_ASSI200_value;
			replyData->FREQOFFSET_WB = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_FREQOFFSET_WB_value;
			replyData->RESERVED1 = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_RESERVED1_value;
			replyData->FREQOFFSET = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_FREQOFFSET_value;
			replyData->MULTIPATH = SI4796X_CMD_FM_PHASE_DIVERSITY_LOCAL_METRICS_REP_MULTIPATH_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_fm_phase_diversity_local_metrics(uint8_t icNum, SI4796X_fm_phase_diversity_local_metrics__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_fm_phase_diversity_local_metrics__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_fm_phase_diversity_local_metrics__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dab_diversity__command(uint8_t icNum,uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAB_DIVERSITY_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAB_DIVERSITY;

	SI4796X_CMD_DAB_DIVERSITY_ARG_MODE_write_field(mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAB_DIVERSITY;
	}

	return ret;

}


RETURN_CODE SI4796X_dab_diversity(uint8_t icNum, uint8_t  mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dab_diversity__command(icNum, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dab_diversity_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAB_DIVERSITY_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAB_DIVERSITY_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAB_DIVERSITY_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_dab_diversity_status__reply(uint8_t icNum, SI4796X_dab_diversity_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_DAB_DIVERSITY_STATUS) {
		rdrep_len = SI4796X_CMD_DAB_DIVERSITY_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->ACQUIRED = SI4796X_CMD_DAB_DIVERSITY_STATUS_REP_ACQUIRED_value;
			replyData->DABDETECT = SI4796X_CMD_DAB_DIVERSITY_STATUS_REP_DABDETECT_value;
			replyData->MODE = SI4796X_CMD_DAB_DIVERSITY_STATUS_REP_MODE_value;
			replyData->COMB_WEIGHT = SI4796X_CMD_DAB_DIVERSITY_STATUS_REP_COMB_WEIGHT_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_dab_diversity_status(uint8_t icNum, SI4796X_dab_diversity_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dab_diversity_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_dab_diversity_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dab_diversity_acquire__command(uint8_t icNum,uint8_t tx_mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAB_DIVERSITY_ACQUIRE_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAB_DIVERSITY_ACQUIRE;

	SI4796X_CMD_DAB_DIVERSITY_ACQUIRE_ARG_TX_MODE_write_field(tx_mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAB_DIVERSITY_ACQUIRE;
	}

	return ret;

}


RETURN_CODE SI4796X_dab_diversity_acquire(uint8_t icNum, uint8_t  tx_mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dab_diversity_acquire__command(icNum, tx_mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_send_test_pattern__command(uint8_t icNum,uint8_t port, uint8_t length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SEND_TEST_PATTERN_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SEND_TEST_PATTERN;

	SI4796X_CMD_SEND_TEST_PATTERN_ARG_PORT_write_field(port);
	SI4796X_CMD_SEND_TEST_PATTERN_ARG_LENGTH_write_u8(length);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SEND_TEST_PATTERN;
	}

	return ret;

}

RETURN_CODE SI4796X_send_test_pattern__reply(uint8_t icNum, uint8_t* data, uint16_t length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_SEND_TEST_PATTERN) {
		rdrep_len = SI4796X_CMD_SEND_TEST_PATTERN_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI4796X_CMD_SEND_TEST_PATTERN_REP_LENGTH + (length * sizeof(uint8_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = length;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_SEND_TEST_PATTERN_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_send_test_pattern(uint8_t icNum, uint8_t  port, uint8_t  arg_length, uint8_t*  data, uint16_t  rep_length)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_send_test_pattern__command(icNum, port, arg_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (data != NULL) {
		r |= SI4796X_send_test_pattern__reply(icNum, data, rep_length);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_log__command(uint8_t icNum,uint8_t block)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_LOG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_LOG;

	SI4796X_CMD_HIFI_LOG_ARG_BLOCK_write_field(block);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_LOG;
	}

	return ret;

}

RETURN_CODE SI4796X_hifi_log__reply(uint8_t icNum, uint8_t* log)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (log == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_HIFI_LOG) {
		rdrep_len = SI4796X_CMD_HIFI_LOG_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_hifi_log(uint8_t icNum, uint8_t  block, uint8_t*  log)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_log__command(icNum, block);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (log != NULL) {
		r |= SI4796X_hifi_log__reply(icNum, log);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_host_load__command(uint8_t icNum,uint16_t size, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HOST_LOAD_LENGTH + size;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HOST_LOAD;

	SI4796X_CMD_HOST_LOAD_ARG_SIZE_write_u16(size);
	for(n=0;n < size;n++) {
		SI4796X_CMD_HOST_LOAD_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HOST_LOAD;
	}

	return ret;

}


RETURN_CODE SI4796X_host_load(uint8_t icNum, uint16_t  size, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_host_load__command(icNum, size, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_flash_load__command(uint8_t icNum,uint8_t subcmd, uint8_t* data, uint16_t data_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_FLASH_LOAD_LENGTH + data_length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_FLASH_LOAD;

	SI4796X_CMD_FLASH_LOAD_ARG_SUBCMD_write_u8(subcmd);
	for(n=0;n < data_length;n++) {
		SI4796X_CMD_FLASH_LOAD_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_FLASH_LOAD;
	}

	return ret;

}

RETURN_CODE SI4796X_flash_load__reply(uint8_t icNum, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_FLASH_LOAD) {
		rdrep_len = SI4796X_CMD_FLASH_LOAD_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI4796X_CMD_FLASH_LOAD_REP_LENGTH;
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 32;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_FLASH_LOAD_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_flash_load(uint8_t icNum, uint8_t  subcmd, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_flash_load__command(icNum, subcmd, arg_data, data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (rep_data != NULL) {
		r |= SI4796X_flash_load__reply(icNum, rep_data);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_load_init__command(uint8_t icNum,uint8_t count, uint32_t* guid)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_LOAD_INIT_LENGTH + (count * sizeof(uint32_t));
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_LOAD_INIT;

	SI4796X_CMD_LOAD_INIT_ARG_COUNT_write_u8(count);
	for(n=0;n < count;n++) {
		SI4796X_CMD_LOAD_INIT_ARG_GUID_N_write_u32(n,guid[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_LOAD_INIT;
	}

	return ret;

}


RETURN_CODE SI4796X_load_init(uint8_t icNum, uint8_t  count, uint32_t*  guid)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_load_init__command(icNum, count, guid);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_boot__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_BOOT_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_BOOT;

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_BOOT;
	}

	return ret;

}


RETURN_CODE SI4796X_boot(uint8_t icNum)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_boot__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_get_part_info__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_GET_PART_INFO_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_GET_PART_INFO;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_GET_PART_INFO;
	}

	return ret;

}

RETURN_CODE SI4796X_get_part_info__reply(uint8_t icNum, SI4796X_get_part_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_GET_PART_INFO) {
		rdrep_len = SI4796X_CMD_GET_PART_INFO_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->CHIPREV = SI4796X_CMD_GET_PART_INFO_REP_CHIPREV_value;
			replyData->ROMREV = SI4796X_CMD_GET_PART_INFO_REP_ROMREV_value;
			replyData->FAMILY = SI4796X_CMD_GET_PART_INFO_REP_FAMILY_value;
			replyData->PN0 = SI4796X_CMD_GET_PART_INFO_REP_PN0_value;
			replyData->PN1 = SI4796X_CMD_GET_PART_INFO_REP_PN1_value;
			replyData->MAJOR = SI4796X_CMD_GET_PART_INFO_REP_MAJOR_value;
			replyData->CUST1 = SI4796X_CMD_GET_PART_INFO_REP_CUST1_value;
			replyData->CUST2 = SI4796X_CMD_GET_PART_INFO_REP_CUST2_value;
			replyData->CORRECTED = SI4796X_CMD_GET_PART_INFO_REP_CORRECTED_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_get_part_info(uint8_t icNum, SI4796X_get_part_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_get_part_info__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_get_part_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_get_power_up_args__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_GET_POWER_UP_ARGS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_GET_POWER_UP_ARGS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_GET_POWER_UP_ARGS;
	}

	return ret;

}

RETURN_CODE SI4796X_get_power_up_args__reply(uint8_t icNum, SI4796X_get_power_up_args__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_GET_POWER_UP_ARGS) {
		rdrep_len = SI4796X_CMD_GET_POWER_UP_ARGS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->CTSIEN = SI4796X_CMD_GET_POWER_UP_ARGS_REP_CTSIEN_value;
			replyData->CLKO_CURRENT = SI4796X_CMD_GET_POWER_UP_ARGS_REP_CLKO_CURRENT_value;
			replyData->VIO = SI4796X_CMD_GET_POWER_UP_ARGS_REP_VIO_value;
			replyData->CLKOUT = SI4796X_CMD_GET_POWER_UP_ARGS_REP_CLKOUT_value;
			replyData->CLK_MODE = SI4796X_CMD_GET_POWER_UP_ARGS_REP_CLK_MODE_value;
			replyData->TR_SIZE = SI4796X_CMD_GET_POWER_UP_ARGS_REP_TR_SIZE_value;
			replyData->CTUN = SI4796X_CMD_GET_POWER_UP_ARGS_REP_CTUN_value;
			replyData->XTAL_FREQ = SI4796X_CMD_GET_POWER_UP_ARGS_REP_XTAL_FREQ_value;
			replyData->IBIAS = SI4796X_CMD_GET_POWER_UP_ARGS_REP_IBIAS_value;
			replyData->AFS = SI4796X_CMD_GET_POWER_UP_ARGS_REP_AFS_value;
			replyData->CHIPID = SI4796X_CMD_GET_POWER_UP_ARGS_REP_CHIPID_value;
			replyData->EZIQ_MASTER = SI4796X_CMD_GET_POWER_UP_ARGS_REP_EZIQ_MASTER_value;
			replyData->EZIQ_ENABLE = SI4796X_CMD_GET_POWER_UP_ARGS_REP_EZIQ_ENABLE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_get_power_up_args(uint8_t icNum, SI4796X_get_power_up_args__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_get_power_up_args__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_get_power_up_args__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_part_override__command(uint8_t icNum,uint8_t pn0, uint8_t pn1, uint8_t major, uint8_t romascii, uint8_t cust1, uint8_t cust2)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_PART_OVERRIDE_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_PART_OVERRIDE;

	SI4796X_CMD_PART_OVERRIDE_ARG_PN0_write_u8(pn0);
	SI4796X_CMD_PART_OVERRIDE_ARG_PN1_write_u8(pn1);
	SI4796X_CMD_PART_OVERRIDE_ARG_MAJOR_write_u8(major);
	SI4796X_CMD_PART_OVERRIDE_ARG_ROMASCII_write_u8(romascii);
	SI4796X_CMD_PART_OVERRIDE_ARG_CUST1_write_u8(cust1);
	SI4796X_CMD_PART_OVERRIDE_ARG_CUST2_write_u8(cust2);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_PART_OVERRIDE;
	}

	return ret;

}


RETURN_CODE SI4796X_part_override(uint8_t icNum, uint8_t  pn0, uint8_t  pn1, uint8_t  major, uint8_t  romascii, uint8_t  cust1, uint8_t  cust2)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_part_override__command(icNum, pn0, pn1, major, romascii, cust1, cust2);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_load_config__command(uint8_t icNum,uint8_t numchips, uint8_t chipid, uint8_t role, uint8_t mosi_in, uint8_t miso_in, uint8_t intb_in, uint8_t nvsclk_in, uint8_t nvssb_in, uint8_t nvmosi_in, uint8_t nvmiso_in, uint8_t rsvd_in, uint8_t iqfsp_in, uint8_t iqfsn_in, uint8_t ioutp_in, uint8_t ioutn_in, uint8_t qoutp_in, uint8_t qoutn_in, uint8_t ssb_in, uint8_t sclk_in, uint8_t dout0_in, uint8_t dclk0_in, uint8_t dfs0_in, uint8_t dout1_in, uint8_t dclk1_in, uint8_t dfs1_in, uint8_t iqclkp_in, uint8_t iqclkn_in, uint8_t diclk2_in, uint8_t difs2_in, uint8_t din3_in, uint8_t diclk3_in, uint8_t difs3_in, uint8_t din4_in, uint8_t diclk4_in, uint8_t difs4_in, uint8_t din0_in, uint8_t gp0_in, uint8_t gp1_in, uint8_t gp2_in, uint8_t din1_in, uint8_t diclk1_in, uint8_t difs1_in, uint8_t din2_in, uint8_t difsa_in, uint8_t diclka_in, uint8_t aout0_in, uint8_t aout1_in, uint8_t icp_in, uint8_t icn_in, uint8_t difs0_in, uint8_t diclk0_in, uint8_t zifq1_in, uint8_t zifi1_in, uint8_t mosi_out, uint8_t miso_out, uint8_t intb_out, uint8_t nvsclk_out, uint8_t nvssb_out, uint8_t nvmosi_out, uint8_t nvmiso_out, uint8_t rsvd_out, uint8_t iqfsp_out, uint8_t iqfsn_out, uint8_t ioutp_out, uint8_t ioutn_out, uint8_t qoutp_out, uint8_t qoutn_out, uint8_t ssb_out, uint8_t sclk_out, uint8_t dout0_out, uint8_t dclk0_out, uint8_t dfs0_out, uint8_t dout1_out, uint8_t dclk1_out, uint8_t dfs1_out, uint8_t iqclkp_out, uint8_t iqclkn_out, uint8_t diclk2_out, uint8_t difs2_out, uint8_t din3_out, uint8_t diclk3_out, uint8_t difs3_out, uint8_t din4_out, uint8_t diclk4_out, uint8_t difs4_out, uint8_t din0_out, uint8_t gp0_out, uint8_t gp1_out, uint8_t gp2_out, uint8_t din1_out, uint8_t diclk1_out, uint8_t difs1_out, uint8_t din2_out, uint8_t difsa_out, uint8_t diclka_out, uint8_t icp_out, uint8_t icn_out, uint8_t difs0_out, uint8_t diclk0_out, uint8_t zifq1_out, uint8_t zifi1_out, uint32_t clksel8, uint32_t spicfg3, uint32_t drvdel, uint32_t capdel, uint32_t spisctrl, uint32_t nvsclk, uint32_t nvssb, uint32_t nvmosi, uint32_t nvmiso, uint32_t diclk0, uint32_t difs0, uint32_t icn, uint32_t icp, uint32_t debugp, uint32_t debugn, uint32_t isfsp, uint32_t isfsn, uint32_t zifi0, uint32_t zifclk, uint32_t zifq0, uint32_t ziffs, uint32_t zifi1, uint32_t zifq1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_LOAD_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_LOAD_CONFIG;

	SI4796X_CMD_LOAD_CONFIG_ARG_NUMCHIPS_write_u8(numchips);
	SI4796X_CMD_LOAD_CONFIG_ARG_CHIPID_write_u8(chipid);
	SI4796X_CMD_LOAD_CONFIG_ARG_ROLE_write_u8(role);
	SI4796X_CMD_LOAD_CONFIG_ARG_MOSI_IN_write_field(mosi_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_MISO_IN_write_field(miso_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_INTB_IN_write_field(intb_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVSCLK_IN_write_field(nvsclk_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVSSB_IN_write_field(nvssb_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVMOSI_IN_write_field(nvmosi_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVMISO_IN_write_field(nvmiso_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_RSVD_IN_write_field(rsvd_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQFSP_IN_write_field(iqfsp_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQFSN_IN_write_field(iqfsn_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_IOUTP_IN_write_field(ioutp_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_IOUTN_IN_write_field(ioutn_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_QOUTP_IN_write_field(qoutp_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_QOUTN_IN_write_field(qoutn_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_SSB_IN_write_field(ssb_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_SCLK_IN_write_field(sclk_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DOUT0_IN_write_field(dout0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DCLK0_IN_write_field(dclk0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DFS0_IN_write_field(dfs0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DOUT1_IN_write_field(dout1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DCLK1_IN_write_field(dclk1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DFS1_IN_write_field(dfs1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQCLKP_IN_write_field(iqclkp_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQCLKN_IN_write_field(iqclkn_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK2_IN_write_field(diclk2_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS2_IN_write_field(difs2_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN3_IN_write_field(din3_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK3_IN_write_field(diclk3_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS3_IN_write_field(difs3_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN4_IN_write_field(din4_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK4_IN_write_field(diclk4_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS4_IN_write_field(difs4_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN0_IN_write_field(din0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_GP0_IN_write_field(gp0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_GP1_IN_write_field(gp1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_GP2_IN_write_field(gp2_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN1_IN_write_field(din1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK1_IN_write_field(diclk1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS1_IN_write_field(difs1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN2_IN_write_field(din2_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFSA_IN_write_field(difsa_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLKA_IN_write_field(diclka_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_AOUT0_IN_write_field(aout0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_AOUT1_IN_write_field(aout1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_ICP_IN_write_field(icp_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_ICN_IN_write_field(icn_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS0_IN_write_field(difs0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK0_IN_write_field(diclk0_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFQ1_IN_write_field(zifq1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFI1_IN_write_field(zifi1_in);
	SI4796X_CMD_LOAD_CONFIG_ARG_MOSI_OUT_write_field(mosi_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_MISO_OUT_write_field(miso_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_INTB_OUT_write_field(intb_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVSCLK_OUT_write_field(nvsclk_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVSSB_OUT_write_field(nvssb_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVMOSI_OUT_write_field(nvmosi_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVMISO_OUT_write_field(nvmiso_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_RSVD_OUT_write_field(rsvd_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQFSP_OUT_write_field(iqfsp_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQFSN_OUT_write_field(iqfsn_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_IOUTP_OUT_write_field(ioutp_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_IOUTN_OUT_write_field(ioutn_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_QOUTP_OUT_write_field(qoutp_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_QOUTN_OUT_write_field(qoutn_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_SSB_OUT_write_field(ssb_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_SCLK_OUT_write_field(sclk_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DOUT0_OUT_write_field(dout0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DCLK0_OUT_write_field(dclk0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DFS0_OUT_write_field(dfs0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DOUT1_OUT_write_field(dout1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DCLK1_OUT_write_field(dclk1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DFS1_OUT_write_field(dfs1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQCLKP_OUT_write_field(iqclkp_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_IQCLKN_OUT_write_field(iqclkn_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK2_OUT_write_field(diclk2_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS2_OUT_write_field(difs2_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN3_OUT_write_field(din3_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK3_OUT_write_field(diclk3_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS3_OUT_write_field(difs3_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN4_OUT_write_field(din4_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK4_OUT_write_field(diclk4_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS4_OUT_write_field(difs4_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN0_OUT_write_field(din0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_GP0_OUT_write_field(gp0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_GP1_OUT_write_field(gp1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_GP2_OUT_write_field(gp2_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN1_OUT_write_field(din1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK1_OUT_write_field(diclk1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS1_OUT_write_field(difs1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIN2_OUT_write_field(din2_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFSA_OUT_write_field(difsa_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLKA_OUT_write_field(diclka_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_ICP_OUT_write_field(icp_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_ICN_OUT_write_field(icn_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS0_OUT_write_field(difs0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK0_OUT_write_field(diclk0_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFQ1_OUT_write_field(zifq1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFI1_OUT_write_field(zifi1_out);
	SI4796X_CMD_LOAD_CONFIG_ARG_CLKSEL8_write_u32(clksel8);
	SI4796X_CMD_LOAD_CONFIG_ARG_SPICFG3_write_u32(spicfg3);
	SI4796X_CMD_LOAD_CONFIG_ARG_DRVDEL_write_u32(drvdel);
	SI4796X_CMD_LOAD_CONFIG_ARG_CAPDEL_write_u32(capdel);
	SI4796X_CMD_LOAD_CONFIG_ARG_SPISCTRL_write_u32(spisctrl);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVSCLK_write_u32(nvsclk);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVSSB_write_u32(nvssb);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVMOSI_write_u32(nvmosi);
	SI4796X_CMD_LOAD_CONFIG_ARG_NVMISO_write_u32(nvmiso);
	SI4796X_CMD_LOAD_CONFIG_ARG_DICLK0_write_u32(diclk0);
	SI4796X_CMD_LOAD_CONFIG_ARG_DIFS0_write_u32(difs0);
	SI4796X_CMD_LOAD_CONFIG_ARG_ICN_write_u32(icn);
	SI4796X_CMD_LOAD_CONFIG_ARG_ICP_write_u32(icp);
	SI4796X_CMD_LOAD_CONFIG_ARG_DEBUGP_write_u32(debugp);
	SI4796X_CMD_LOAD_CONFIG_ARG_DEBUGN_write_u32(debugn);
	SI4796X_CMD_LOAD_CONFIG_ARG_ISFSP_write_u32(isfsp);
	SI4796X_CMD_LOAD_CONFIG_ARG_ISFSN_write_u32(isfsn);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFI0_write_u32(zifi0);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFCLK_write_u32(zifclk);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFQ0_write_u32(zifq0);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFFS_write_u32(ziffs);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFI1_write_u32(zifi1);
	SI4796X_CMD_LOAD_CONFIG_ARG_ZIFQ1_write_u32(zifq1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_LOAD_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_load_config(uint8_t icNum, uint8_t  numchips, uint8_t  chipid, uint8_t  role, uint8_t  mosi_in, uint8_t  miso_in, uint8_t  intb_in, uint8_t  nvsclk_in, uint8_t  nvssb_in, uint8_t  nvmosi_in, uint8_t  nvmiso_in, uint8_t  rsvd_in, uint8_t  iqfsp_in, uint8_t  iqfsn_in, uint8_t  ioutp_in, uint8_t  ioutn_in, uint8_t  qoutp_in, uint8_t  qoutn_in, uint8_t  ssb_in, uint8_t  sclk_in, uint8_t  dout0_in, uint8_t  dclk0_in, uint8_t  dfs0_in, uint8_t  dout1_in, uint8_t  dclk1_in, uint8_t  dfs1_in, uint8_t  iqclkp_in, uint8_t  iqclkn_in, uint8_t  diclk2_in, uint8_t  difs2_in, uint8_t  din3_in, uint8_t  diclk3_in, uint8_t  difs3_in, uint8_t  din4_in, uint8_t  diclk4_in, uint8_t  difs4_in, uint8_t  din0_in, uint8_t  gp0_in, uint8_t  gp1_in, uint8_t  gp2_in, uint8_t  din1_in, uint8_t  diclk1_in, uint8_t  difs1_in, uint8_t  din2_in, uint8_t  difsa_in, uint8_t  diclka_in, uint8_t  aout0_in, uint8_t  aout1_in, uint8_t  icp_in, uint8_t  icn_in, uint8_t  difs0_in, uint8_t  diclk0_in, uint8_t  zifq1_in, uint8_t  zifi1_in, uint8_t  mosi_out, uint8_t  miso_out, uint8_t  intb_out, uint8_t  nvsclk_out, uint8_t  nvssb_out, uint8_t  nvmosi_out, uint8_t  nvmiso_out, uint8_t  rsvd_out, uint8_t  iqfsp_out, uint8_t  iqfsn_out, uint8_t  ioutp_out, uint8_t  ioutn_out, uint8_t  qoutp_out, uint8_t  qoutn_out, uint8_t  ssb_out, uint8_t  sclk_out, uint8_t  dout0_out, uint8_t  dclk0_out, uint8_t  dfs0_out, uint8_t  dout1_out, uint8_t  dclk1_out, uint8_t  dfs1_out, uint8_t  iqclkp_out, uint8_t  iqclkn_out, uint8_t  diclk2_out, uint8_t  difs2_out, uint8_t  din3_out, uint8_t  diclk3_out, uint8_t  difs3_out, uint8_t  din4_out, uint8_t  diclk4_out, uint8_t  difs4_out, uint8_t  din0_out, uint8_t  gp0_out, uint8_t  gp1_out, uint8_t  gp2_out, uint8_t  din1_out, uint8_t  diclk1_out, uint8_t  difs1_out, uint8_t  din2_out, uint8_t  difsa_out, uint8_t  diclka_out, uint8_t  icp_out, uint8_t  icn_out, uint8_t  difs0_out, uint8_t  diclk0_out, uint8_t  zifq1_out, uint8_t  zifi1_out, uint32_t  clksel8, uint32_t  spicfg3, uint32_t  drvdel, uint32_t  capdel, uint32_t  spisctrl, uint32_t  nvsclk, uint32_t  nvssb, uint32_t  nvmosi, uint32_t  nvmiso, uint32_t  diclk0, uint32_t  difs0, uint32_t  icn, uint32_t  icp, uint32_t  debugp, uint32_t  debugn, uint32_t  isfsp, uint32_t  isfsn, uint32_t  zifi0, uint32_t  zifclk, uint32_t  zifq0, uint32_t  ziffs, uint32_t  zifi1, uint32_t  zifq1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_load_config__command(icNum, numchips, chipid, role, mosi_in, miso_in, intb_in, nvsclk_in, nvssb_in, nvmosi_in, nvmiso_in, rsvd_in, iqfsp_in, iqfsn_in, ioutp_in, ioutn_in, qoutp_in, qoutn_in, ssb_in, sclk_in, dout0_in, dclk0_in, dfs0_in, dout1_in, dclk1_in, dfs1_in, iqclkp_in, iqclkn_in, diclk2_in, difs2_in, din3_in, diclk3_in, difs3_in, din4_in, diclk4_in, difs4_in, din0_in, gp0_in, gp1_in, gp2_in, din1_in, diclk1_in, difs1_in, din2_in, difsa_in, diclka_in, aout0_in, aout1_in, icp_in, icn_in, difs0_in, diclk0_in, zifq1_in, zifi1_in, mosi_out, miso_out, intb_out, nvsclk_out, nvssb_out, nvmosi_out, nvmiso_out, rsvd_out, iqfsp_out, iqfsn_out, ioutp_out, ioutn_out, qoutp_out, qoutn_out, ssb_out, sclk_out, dout0_out, dclk0_out, dfs0_out, dout1_out, dclk1_out, dfs1_out, iqclkp_out, iqclkn_out, diclk2_out, difs2_out, din3_out, diclk3_out, difs3_out, din4_out, diclk4_out, difs4_out, din0_out, gp0_out, gp1_out, gp2_out, din1_out, diclk1_out, difs1_out, din2_out, difsa_out, diclka_out, icp_out, icn_out, difs0_out, diclk0_out, zifq1_out, zifi1_out, clksel8, spicfg3, drvdel, capdel, spisctrl, nvsclk, nvssb, nvmosi, nvmiso, diclk0, difs0, icn, icp, debugp, debugn, isfsp, isfsn, zifi0, zifclk, zifq0, ziffs, zifi1, zifq1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_get_chip_info__command(uint8_t icNum,uint8_t info_type)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_GET_CHIP_INFO_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_GET_CHIP_INFO;

	SI4796X_CMD_GET_CHIP_INFO_ARG_INFO_TYPE_write_u8(info_type);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_GET_CHIP_INFO;
	}

	return ret;

}

RETURN_CODE SI4796X_get_chip_info__reply(uint8_t icNum, SI4796X_get_chip_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_GET_CHIP_INFO) {
		rdrep_len = SI4796X_CMD_GET_CHIP_INFO_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->LENGTH = SI4796X_CMD_GET_CHIP_INFO_REP_LENGTH_value;

			calculated_response_length = SI4796X_CMD_GET_CHIP_INFO_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_GET_CHIP_INFO_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_get_chip_info(uint8_t icNum, uint8_t  info_type, SI4796X_get_chip_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_get_chip_info__command(icNum, info_type);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_get_chip_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_key_exchange__command(uint8_t icNum,uint8_t* data, uint16_t data_len)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_KEY_EXCHANGE_LENGTH + data_len;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_KEY_EXCHANGE;

	for(n=0;n < data_len;n++) {
		SI4796X_CMD_KEY_EXCHANGE_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_KEY_EXCHANGE;
	}

	return ret;

}


RETURN_CODE SI4796X_key_exchange(uint8_t icNum, uint8_t*  data, uint16_t  data_len)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_key_exchange__command(icNum, data, data_len);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_get_func_info__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_GET_FUNC_INFO_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_GET_FUNC_INFO;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_GET_FUNC_INFO;
	}

	return ret;

}

RETURN_CODE SI4796X_get_func_info__reply(uint8_t icNum, SI4796X_get_func_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_GET_FUNC_INFO) {
		rdrep_len = SI4796X_CMD_GET_FUNC_INFO_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->VERINFO0 = SI4796X_CMD_GET_FUNC_INFO_REP_VERINFO0_value;
			replyData->VERINFO1 = SI4796X_CMD_GET_FUNC_INFO_REP_VERINFO1_value;
			replyData->VERINFO2 = SI4796X_CMD_GET_FUNC_INFO_REP_VERINFO2_value;
			replyData->VERINFO3 = SI4796X_CMD_GET_FUNC_INFO_REP_VERINFO3_value;
			replyData->VERINFO4 = SI4796X_CMD_GET_FUNC_INFO_REP_VERINFO4_value;
			replyData->HIFIINFO0 = SI4796X_CMD_GET_FUNC_INFO_REP_HIFIINFO0_value;
			replyData->HIFIINFO1 = SI4796X_CMD_GET_FUNC_INFO_REP_HIFIINFO1_value;
			replyData->HIFIINFO2 = SI4796X_CMD_GET_FUNC_INFO_REP_HIFIINFO2_value;
			replyData->HIFIINFO3 = SI4796X_CMD_GET_FUNC_INFO_REP_HIFIINFO3_value;
			replyData->CUSTINFO0 = SI4796X_CMD_GET_FUNC_INFO_REP_CUSTINFO0_value;
			replyData->CUSTINFO1 = SI4796X_CMD_GET_FUNC_INFO_REP_CUSTINFO1_value;
			replyData->CUSTINFO2 = SI4796X_CMD_GET_FUNC_INFO_REP_CUSTINFO2_value;
			replyData->CUSTINFO3 = SI4796X_CMD_GET_FUNC_INFO_REP_CUSTINFO3_value;
			replyData->NOSVN = SI4796X_CMD_GET_FUNC_INFO_REP_NOSVN_value;
			replyData->LOCATION = SI4796X_CMD_GET_FUNC_INFO_REP_LOCATION_value;
			replyData->MIXEDREV = SI4796X_CMD_GET_FUNC_INFO_REP_MIXEDREV_value;
			replyData->LOCALMOD = SI4796X_CMD_GET_FUNC_INFO_REP_LOCALMOD_value;
			replyData->SVNID = SI4796X_CMD_GET_FUNC_INFO_REP_SVNID_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_get_func_info(uint8_t icNum, SI4796X_get_func_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_get_func_info__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_get_func_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_set_property__command(uint8_t icNum,uint16_t prop, uint16_t data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SET_PROPERTY_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SET_PROPERTY;

	SI4796X_CMD_SET_PROPERTY_ARG_PROP_write_u16(prop);
	SI4796X_CMD_SET_PROPERTY_ARG_DATA_write_u16(data);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SET_PROPERTY;
	}

	return ret;

}


RETURN_CODE SI4796X_set_property(uint8_t icNum, uint16_t  prop, uint16_t  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_set_property__command(icNum, prop, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_get_property__command(uint8_t icNum,uint8_t count, uint16_t prop)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_GET_PROPERTY_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_GET_PROPERTY;

	SI4796X_CMD_GET_PROPERTY_ARG_COUNT_write_u8(count);
	SI4796X_CMD_GET_PROPERTY_ARG_PROP_write_u16(prop);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_GET_PROPERTY;
	}

	return ret;

}

RETURN_CODE SI4796X_get_property__reply(uint8_t icNum, uint16_t* data, uint16_t count)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_GET_PROPERTY) {
		rdrep_len = SI4796X_CMD_GET_PROPERTY_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI4796X_CMD_GET_PROPERTY_REP_LENGTH + (count * sizeof(uint16_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = count;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI4796X_CMD_GET_PROPERTY_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_get_property(uint8_t icNum, uint8_t  arg_count, uint16_t  prop, uint16_t*  data, uint16_t  rep_count)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_get_property__command(icNum, arg_count, prop);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (data != NULL) {
		r |= SI4796X_get_property__reply(icNum, data, rep_count);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_agc_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_AGC_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_AGC_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_AGC_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_agc_status__reply(uint8_t icNum, SI4796X_agc_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_AGC_STATUS) {
		rdrep_len = SI4796X_CMD_AGC_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->TM_B3A_RF = SI4796X_CMD_AGC_STATUS_REP_TM_B3A_RF_value;
			replyData->TM_FMA_RF = SI4796X_CMD_AGC_STATUS_REP_TM_FMA_RF_value;
			replyData->TM_AM_MS = SI4796X_CMD_AGC_STATUS_REP_TM_AM_MS_value;
			replyData->TM_B3B_RF = SI4796X_CMD_AGC_STATUS_REP_TM_B3B_RF_value;
			replyData->TM_FMB_RF = SI4796X_CMD_AGC_STATUS_REP_TM_FMB_RF_value;
			replyData->TM_AM_RF = SI4796X_CMD_AGC_STATUS_REP_TM_AM_RF_value;
			replyData->TM_IF1 = SI4796X_CMD_AGC_STATUS_REP_TM_IF1_value;
			replyData->TM_IF0 = SI4796X_CMD_AGC_STATUS_REP_TM_IF0_value;
			replyData->AM_RF_INDEX = SI4796X_CMD_AGC_STATUS_REP_AM_RF_INDEX_value;
			replyData->AM_RF_GAIN = SI4796X_CMD_AGC_STATUS_REP_AM_RF_GAIN_value;
			replyData->FMB_RF_INDEX = SI4796X_CMD_AGC_STATUS_REP_FMB_RF_INDEX_value;
			replyData->FMB_RF_GAIN = SI4796X_CMD_AGC_STATUS_REP_FMB_RF_GAIN_value;
			replyData->B3B_RF_INDEX = SI4796X_CMD_AGC_STATUS_REP_B3B_RF_INDEX_value;
			replyData->B3B_RF_GAIN = SI4796X_CMD_AGC_STATUS_REP_B3B_RF_GAIN_value;
			replyData->AM_MS_INDEX = SI4796X_CMD_AGC_STATUS_REP_AM_MS_INDEX_value;
			replyData->AM_MS_GAIN = SI4796X_CMD_AGC_STATUS_REP_AM_MS_GAIN_value;
			replyData->FMA_RF_INDEX = SI4796X_CMD_AGC_STATUS_REP_FMA_RF_INDEX_value;
			replyData->FMA_RF_GAIN = SI4796X_CMD_AGC_STATUS_REP_FMA_RF_GAIN_value;
			replyData->B3A_RF_INDEX = SI4796X_CMD_AGC_STATUS_REP_B3A_RF_INDEX_value;
			replyData->B3A_RF_GAIN = SI4796X_CMD_AGC_STATUS_REP_B3A_RF_GAIN_value;
			replyData->IF0_INDEX = SI4796X_CMD_AGC_STATUS_REP_IF0_INDEX_value;
			replyData->IF0_GAIN = SI4796X_CMD_AGC_STATUS_REP_IF0_GAIN_value;
			replyData->IF1_INDEX = SI4796X_CMD_AGC_STATUS_REP_IF1_INDEX_value;
			replyData->IF1_GAIN = SI4796X_CMD_AGC_STATUS_REP_IF1_GAIN_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_agc_status(uint8_t icNum, SI4796X_agc_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_agc_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_agc_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_detail_status__command(uint8_t icNum,uint8_t subcmd, uint8_t* data, uint16_t data_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DETAIL_STATUS_LENGTH + data_length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DETAIL_STATUS;

	SI4796X_CMD_DETAIL_STATUS_ARG_SUBCMD_write_u8(subcmd);
	for(n=0;n < data_length;n++) {
		SI4796X_CMD_DETAIL_STATUS_ARG_DATA_N_write_u8(n,data[n]);
	}

#if (FEAT_DEBUG_211125)		/* 211125 SGsoft */
	ret = hal_writeCommand(icNum, cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_DEBUG_211125*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DETAIL_STATUS;
	}

	return ret;

}


RETURN_CODE SI4796X_detail_status__reply(uint8_t icNum, uint8_t* data)
{
    RETURN_CODE ret = 0;
    uint8_t icIndex = 0;
    uint32_t rdrep_len = 0;
    uint32_t calculated_response_length;

    icIndex = getCommandIndex(icNum);
    if(icIndex >= MAX_NUM_PARTS) {
        return INVALID_INPUT;
    }
    if(data == NULL) {
        return INVALID_INPUT;
    }
    if(lastCommands[icIndex] == SI4796X_CMD_DETAIL_STATUS)
    {
        rdrep_len = SI4796X_CMD_DETAIL_STATUS_REP_LENGTH;
        if(rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
            return WRAPPER_ERROR;
        }
        ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
        if(ret == SUCCESS)
        {

            calculated_response_length = SI4796X_CMD_DETAIL_STATUS_REP_LENGTH;
            if(calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
                return WRAPPER_ERROR;
            }
            ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
            if(ret != SUCCESS)
            {
                return ret;
            }

            {
                int32_t n = 0;
                int32_t array_elem_count = 0;

                array_elem_count = 8;
                for(n = 0; n < array_elem_count; n++) {
                    data[n] = SI4796X_CMD_DETAIL_STATUS_REP_DATA_N_value(n);
 
                }

            }
        }
    }
    else
    {
        ret = COMMAND_ERROR;
    }
    return ret;
}

RETURN_CODE SI4796X_detail_status(uint8_t icNum, uint8_t  subcmd, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_detail_status__command(icNum, subcmd, arg_data, data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (rep_data != NULL) {
		r |= SI4796X_detail_status__reply(icNum, rep_data);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_jtag_unlock__command(uint8_t icNum,uint8_t jtag_sel, uint8_t* image_id, uint16_t image_id_len)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_JTAG_UNLOCK_LENGTH + image_id_len;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_JTAG_UNLOCK;

	SI4796X_CMD_HIFI_JTAG_UNLOCK_ARG_JTAG_SEL_write_field(jtag_sel);
	for(n=0;n < image_id_len;n++) {
		SI4796X_CMD_HIFI_JTAG_UNLOCK_ARG_IMAGE_ID_N_write_u8(n,image_id[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_JTAG_UNLOCK;
	}

	return ret;

}


RETURN_CODE SI4796X_hifi_jtag_unlock(uint8_t icNum, uint8_t  jtag_sel, uint8_t*  image_id, uint16_t  image_id_len)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_jtag_unlock__command(icNum, jtag_sel, image_id, image_id_len);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_hifi_pipe_reset__command(uint8_t icNum,uint8_t reset)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_HIFI_PIPE_RESET_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_HIFI_PIPE_RESET;

	SI4796X_CMD_HIFI_PIPE_RESET_ARG_RESET_write_field(reset);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_HIFI_PIPE_RESET;
	}

	return ret;

}


RETURN_CODE SI4796X_hifi_pipe_reset(uint8_t icNum, uint8_t  reset)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_hifi_pipe_reset__command(icNum, reset);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_get_api__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_GET_API_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_GET_API;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_GET_API;
	}

	return ret;

}

RETURN_CODE SI4796X_get_api__reply(uint8_t icNum, SI4796X_get_api__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_GET_API) {
		rdrep_len = SI4796X_CMD_GET_API_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->APIVERSION0 = SI4796X_CMD_GET_API_REP_APIVERSION0_value;
			replyData->APIVERSION1 = SI4796X_CMD_GET_API_REP_APIVERSION1_value;
			replyData->APIVERSION2 = SI4796X_CMD_GET_API_REP_APIVERSION2_value;
			replyData->APIVERSION3 = SI4796X_CMD_GET_API_REP_APIVERSION3_value;
			replyData->DYNAMIC_ZIF = SI4796X_CMD_GET_API_REP_DYNAMIC_ZIF_value;
			replyData->ADVANCED_TUNER = SI4796X_CMD_GET_API_REP_ADVANCED_TUNER_value;
			replyData->ADVANCED_AUDIO = SI4796X_CMD_GET_API_REP_ADVANCED_AUDIO_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_get_api(uint8_t icNum, SI4796X_get_api__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_get_api__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_get_api__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_tuner__command(uint8_t icNum,uint8_t id, uint8_t* data, uint16_t data_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_TUNER_LENGTH + data_length;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	///ClearMemory(cmd_arg, cmd_len);											/* 210512 SGsoft */	

	cmd_arg[0] = SI4796X_CMD_TUNER;

	SI4796X_CMD_TUNER_ARG_ID_write_u8(id);
	
	#if 1		/* 211018 SGsoft */		
	cmd_arg[2] = 0x00;			/* padding */
	cmd_arg[3] = 0x00;			/* padding */
	#endif	/*1*/
	
	for(n=0;n < data_length;n++) {
		SI4796X_CMD_TUNER_ARG_DATA_N_write_u8(n,data[n]);
	}

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_TUNER;
	}

	return ret;

}

RETURN_CODE SI4796X_tuner__reply(uint8_t icNum, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_TUNER) {
		rdrep_len = SI4796X_CMD_TUNER_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_tuner(uint8_t icNum, uint8_t  id, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_tuner__command(icNum, id, arg_data, data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (rep_data != NULL) {
		r |= SI4796X_tuner__reply(icNum, rep_data);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dab_set_freq_list__command(uint8_t icNum,uint8_t num_freqs, uint32_t* freq)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAB_SET_FREQ_LIST_LENGTH + (num_freqs * sizeof(uint32_t));
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAB_SET_FREQ_LIST;

	SI4796X_CMD_DAB_SET_FREQ_LIST_ARG_NUM_FREQS_write_u8(num_freqs);
	for(n=0;n < num_freqs;n++) {
		SI4796X_CMD_DAB_SET_FREQ_LIST_ARG_FREQ_N_write_u32(n,freq[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAB_SET_FREQ_LIST;
	}

	return ret;

}


RETURN_CODE SI4796X_dab_set_freq_list(uint8_t icNum, uint8_t  num_freqs, uint32_t*  freq)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dab_set_freq_list__command(icNum, num_freqs, freq);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dab_get_freq_list__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAB_GET_FREQ_LIST_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAB_GET_FREQ_LIST;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAB_GET_FREQ_LIST;
	}

	return ret;

}

RETURN_CODE SI4796X_dab_get_freq_list__reply(uint8_t icNum, SI4796X_dab_get_freq_list__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->FREQ = NULL;
	replyData->NUM_FREQS = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_DAB_GET_FREQ_LIST) {
		rdrep_len = SI4796X_CMD_DAB_GET_FREQ_LIST_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->NUM_FREQS = SI4796X_CMD_DAB_GET_FREQ_LIST_REP_NUM_FREQS_value;

			calculated_response_length = SI4796X_CMD_DAB_GET_FREQ_LIST_REP_LENGTH + (replyData->NUM_FREQS * sizeof(uint32_t));
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint32_t* freq = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->NUM_FREQS;
				replyData->FREQ = (uint32_t*)malloc(sizeof(uint32_t) * array_elem_count);
				if (replyData->FREQ == NULL) {
					return MEMORY_ERROR;
				}
				freq = replyData->FREQ;
				for (n = 0; n < array_elem_count; n++) {
					freq[n] = SI4796X_CMD_DAB_GET_FREQ_LIST_REP_FREQ_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_dab_get_freq_list(uint8_t icNum, SI4796X_dab_get_freq_list__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dab_get_freq_list__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_dab_get_freq_list__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_pin_status__command(uint8_t icNum,uint8_t group)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_PIN_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_PIN_STATUS;

	SI4796X_CMD_PIN_STATUS_ARG_GROUP_write_field(group);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_PIN_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_pin_status__reply(uint8_t icNum, SI4796X_pin_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->PIN = NULL;
	replyData->NUMPINS = 0;
	if (lastCommands[icIndex] == SI4796X_CMD_PIN_STATUS) {
		rdrep_len = SI4796X_CMD_PIN_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->NUMPINS = SI4796X_CMD_PIN_STATUS_REP_NUMPINS_value;

			calculated_response_length = SI4796X_CMD_PIN_STATUS_REP_LENGTH + (replyData->NUMPINS * SI4796X_CMD_PIN_STATUS_REP_PIN_SIZE);
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				SI4796X_pin_status_pin__data* pin = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->NUMPINS;
				replyData->PIN = (SI4796X_pin_status_pin__data*)malloc(sizeof(SI4796X_pin_status_pin__data) * array_elem_count);
				if (replyData->PIN == NULL) {
					return MEMORY_ERROR;
				}
				pin = replyData->PIN;
				for (n = 0; n < array_elem_count; n++) {
					pin[n].DEGLITCH = SI4796X_CMD_PIN_STATUS_REP_PIN_DEGLITCH_N_value(n);
					pin[n].INVERT = SI4796X_CMD_PIN_STATUS_REP_PIN_INVERT_N_value(n);
					pin[n].INTERRUPT_TYPE = SI4796X_CMD_PIN_STATUS_REP_PIN_INTERRUPT_TYPE_N_value(n);
					pin[n].DIRECTION = SI4796X_CMD_PIN_STATUS_REP_PIN_DIRECTION_N_value(n);
					pin[n].GPI = SI4796X_CMD_PIN_STATUS_REP_PIN_GPI_N_value(n);
					pin[n].GPOUT = SI4796X_CMD_PIN_STATUS_REP_PIN_GPOUT_N_value(n);
					pin[n].SPDIFTX1 = SI4796X_CMD_PIN_STATUS_REP_PIN_SPDIFTX1_N_value(n);
					pin[n].SPDIFTX0 = SI4796X_CMD_PIN_STATUS_REP_PIN_SPDIFTX0_N_value(n);
					pin[n].I2STX4 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2STX4_N_value(n);
					pin[n].I2STX3 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2STX3_N_value(n);
					pin[n].I2STX2 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2STX2_N_value(n);
					pin[n].I2STX1 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2STX1_N_value(n);
					pin[n].I2STX0 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2STX0_N_value(n);
					pin[n].BLEND = SI4796X_CMD_PIN_STATUS_REP_PIN_BLEND_N_value(n);
					pin[n].SPDIFRX1 = SI4796X_CMD_PIN_STATUS_REP_PIN_SPDIFRX1_N_value(n);
					pin[n].SPDIFRX0 = SI4796X_CMD_PIN_STATUS_REP_PIN_SPDIFRX0_N_value(n);
					pin[n].I2SRX4 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2SRX4_N_value(n);
					pin[n].I2SRX3 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2SRX3_N_value(n);
					pin[n].I2SRX2 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2SRX2_N_value(n);
					pin[n].I2SRX1 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2SRX1_N_value(n);
					pin[n].I2SRX0 = SI4796X_CMD_PIN_STATUS_REP_PIN_I2SRX0_N_value(n);
					pin[n].GPIB = SI4796X_CMD_PIN_STATUS_REP_PIN_GPIB_N_value(n);
					pin[n].GPIA = SI4796X_CMD_PIN_STATUS_REP_PIN_GPIA_N_value(n);
					pin[n].CLIP1 = SI4796X_CMD_PIN_STATUS_REP_PIN_CLIP1_N_value(n);
					pin[n].CLIP0 = SI4796X_CMD_PIN_STATUS_REP_PIN_CLIP0_N_value(n);
					pin[n].ICIN3 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICIN3_N_value(n);
					pin[n].ICIN2 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICIN2_N_value(n);
					pin[n].ICIN1 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICIN1_N_value(n);
					pin[n].ICIN0 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICIN0_N_value(n);
					pin[n].I2SCKFS = SI4796X_CMD_PIN_STATUS_REP_PIN_I2SCKFS_N_value(n);
					pin[n].DAC = SI4796X_CMD_PIN_STATUS_REP_PIN_DAC_N_value(n);
					pin[n].ICOUT4 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICOUT4_N_value(n);
					pin[n].ICOUT3 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICOUT3_N_value(n);
					pin[n].ICOUT2 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICOUT2_N_value(n);
					pin[n].ICOUT1 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICOUT1_N_value(n);
					pin[n].ICOUT0 = SI4796X_CMD_PIN_STATUS_REP_PIN_ICOUT0_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_pin_status(uint8_t icNum, uint8_t  group, SI4796X_pin_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_pin_status__command(icNum, group);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_pin_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_pin_config_hifi__command(uint8_t icNum,uint8_t pin_id_0, uint8_t enable_0, uint8_t deglitch_0, uint8_t invert_0, uint8_t interrupt_type_0, uint8_t pin_id_1, uint8_t enable_1, uint8_t deglitch_1, uint8_t invert_1, uint8_t interrupt_type_1, uint8_t pin_id_a, uint8_t enable_a, uint8_t deglitch_a, uint8_t invert_a, uint8_t interrupt_type_a, uint8_t pin_id_b, uint8_t enable_b, uint8_t deglitch_b, uint8_t invert_b, uint8_t interrupt_type_b)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_PIN_CONFIG_HIFI_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_PIN_CONFIG_HIFI;

	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_0_write_u8(pin_id_0);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_0_write_field(enable_0);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_0_write_field(deglitch_0);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INVERT_0_write_field(invert_0);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_0_write_field(interrupt_type_0);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_1_write_u8(pin_id_1);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_1_write_field(enable_1);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_1_write_field(deglitch_1);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INVERT_1_write_field(invert_1);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_1_write_field(interrupt_type_1);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_A_write_u8(pin_id_a);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_A_write_field(enable_a);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_A_write_field(deglitch_a);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INVERT_A_write_field(invert_a);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_A_write_field(interrupt_type_a);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_B_write_u8(pin_id_b);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_B_write_field(enable_b);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_B_write_field(deglitch_b);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INVERT_B_write_field(invert_b);
	SI4796X_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_B_write_field(interrupt_type_b);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_PIN_CONFIG_HIFI;
	}

	return ret;

}


RETURN_CODE SI4796X_pin_config_hifi(uint8_t icNum, uint8_t  pin_id_0, uint8_t  enable_0, uint8_t  deglitch_0, uint8_t  invert_0, uint8_t  interrupt_type_0, uint8_t  pin_id_1, uint8_t  enable_1, uint8_t  deglitch_1, uint8_t  invert_1, uint8_t  interrupt_type_1, uint8_t  pin_id_a, uint8_t  enable_a, uint8_t  deglitch_a, uint8_t  invert_a, uint8_t  interrupt_type_a, uint8_t  pin_id_b, uint8_t  enable_b, uint8_t  deglitch_b, uint8_t  invert_b, uint8_t  interrupt_type_b)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_pin_config_hifi__command(icNum, pin_id_0, enable_0, deglitch_0, invert_0, interrupt_type_0, pin_id_1, enable_1, deglitch_1, invert_1, interrupt_type_1, pin_id_a, enable_a, deglitch_a, invert_a, interrupt_type_a, pin_id_b, enable_b, deglitch_b, invert_b, interrupt_type_b);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_pin_config_gpo__command(uint8_t icNum,uint8_t numpins, SI4796X_pin_config_gpo_pin__data* pin)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_PIN_CONFIG_GPO_LENGTH + (numpins * sizeof(SI4796X_pin_config_gpo_pin__data));
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_PIN_CONFIG_GPO;

	SI4796X_CMD_PIN_CONFIG_GPO_ARG_NUMPINS_write_u8(numpins);
	for(n=0;n < numpins;n++) {
		SI4796X_CMD_PIN_CONFIG_GPO_ARG_PIN_PIN_ID_N_write_u8(n,pin[n].PIN_ID);
		SI4796X_CMD_PIN_CONFIG_GPO_ARG_PIN_ENABLE_N_write_field(n,pin[n].ENABLE);
		SI4796X_CMD_PIN_CONFIG_GPO_ARG_PIN_DRIVE_N_write_field(n,pin[n].DRIVE);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_PIN_CONFIG_GPO;
	}

	return ret;

}


RETURN_CODE SI4796X_pin_config_gpo(uint8_t icNum, uint8_t  numpins, SI4796X_pin_config_gpo_pin__data*  pin)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_pin_config_gpo__command(icNum, numpins, pin);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2stx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2STX0_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2STX0_CONFIG;

	SI4796X_CMD_I2STX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2STX0_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2STX0_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2STX0_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2STX0_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2STX0_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2STX0_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2STX0_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2STX0_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2STX0_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2STX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2STX0_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2STX0_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2STX0_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2STX0_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2STX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2STX0_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2STX0_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2STX0_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2STX0_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2STX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2stx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2stx0_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2stx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2STX1_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2STX1_CONFIG;

	SI4796X_CMD_I2STX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2STX1_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2STX1_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2STX1_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2STX1_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2STX1_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2STX1_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2STX1_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2STX1_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2STX1_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2STX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2STX1_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2STX1_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2STX1_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2STX1_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2STX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2STX1_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2STX1_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2STX1_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2STX1_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2STX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2stx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2stx1_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2stx2_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2STX2_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2STX2_CONFIG;

	SI4796X_CMD_I2STX2_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2STX2_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2STX2_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2STX2_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2STX2_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2STX2_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2STX2_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2STX2_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2STX2_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2STX2_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2STX2_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2STX2_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2STX2_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2STX2_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2STX2_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2STX2_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2STX2_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2STX2_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2STX2_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2STX2_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2STX2_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2stx2_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2stx2_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2stx3_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2STX3_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2STX3_CONFIG;

	SI4796X_CMD_I2STX3_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2STX3_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2STX3_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2STX3_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2STX3_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2STX3_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2STX3_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2STX3_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2STX3_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2STX3_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2STX3_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2STX3_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2STX3_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2STX3_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2STX3_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2STX3_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2STX3_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2STX3_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2STX3_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2STX3_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2STX3_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2stx3_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2stx3_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2stx4_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t lvdsdata, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2STX4_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2STX4_CONFIG;

	SI4796X_CMD_I2STX4_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2STX4_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2STX4_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2STX4_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2STX4_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2STX4_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2STX4_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2STX4_CONFIG_ARG_LVDSDATA_write_field(lvdsdata);
	SI4796X_CMD_I2STX4_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2STX4_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2STX4_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2STX4_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2STX4_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2STX4_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2STX4_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2STX4_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2STX4_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2STX4_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2STX4_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2STX4_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2STX4_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2STX4_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2stx4_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  lvdsdata, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2stx4_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, lvdsdata, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2srx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2SRX0_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2SRX0_CONFIG;

	SI4796X_CMD_I2SRX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2SRX0_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2SRX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2srx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2srx0_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2srx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2SRX1_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2SRX1_CONFIG;

	SI4796X_CMD_I2SRX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2SRX1_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2SRX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2srx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2srx1_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2srx2_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2SRX2_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2SRX2_CONFIG;

	SI4796X_CMD_I2SRX2_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2SRX2_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2SRX2_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2srx2_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2srx2_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2srx3_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2SRX3_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2SRX3_CONFIG;

	SI4796X_CMD_I2SRX3_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2SRX3_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2SRX3_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2srx3_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2srx3_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2srx4_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2SRX4_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2SRX4_CONFIG;

	SI4796X_CMD_I2SRX4_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_ENABLE_write_field(enable);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_FORCE_write_field(force);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_MASTER_write_field(master);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_DATA0_write_field(data0);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_FILL_write_field(fill);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_SWAP_write_field(swap);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_HIZ_write_field(hiz);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_LENGTH_write_field(length);
	SI4796X_CMD_I2SRX4_CONFIG_ARG_DATA1_write_u8(data1);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2SRX4_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_i2srx4_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2srx4_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_i2s_control__command(uint8_t icNum,uint8_t port, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_I2S_CONTROL_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_I2S_CONTROL;

	SI4796X_CMD_I2S_CONTROL_ARG_PORT_write_field(port);
	SI4796X_CMD_I2S_CONTROL_ARG_MODE_write_field(mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_I2S_CONTROL;
	}

	return ret;

}

RETURN_CODE SI4796X_i2s_control__reply(uint8_t icNum, SI4796X_i2s_control__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_I2S_CONTROL) {
		rdrep_len = SI4796X_CMD_I2S_CONTROL_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->SAMPLE_RATE = SI4796X_CMD_I2S_CONTROL_REP_SAMPLE_RATE_value;
			replyData->WHITENCONTROL = SI4796X_CMD_I2S_CONTROL_REP_WHITENCONTROL_value;
			replyData->PURPOSE = SI4796X_CMD_I2S_CONTROL_REP_PURPOSE_value;
			replyData->FORCE = SI4796X_CMD_I2S_CONTROL_REP_FORCE_value;
			replyData->MASTER = SI4796X_CMD_I2S_CONTROL_REP_MASTER_value;
			replyData->CLKFS = SI4796X_CMD_I2S_CONTROL_REP_CLKFS_value;
			replyData->LVDSDATA = SI4796X_CMD_I2S_CONTROL_REP_LVDSDATA_value;
			replyData->DATA0 = SI4796X_CMD_I2S_CONTROL_REP_DATA0_value;
			replyData->FILL = SI4796X_CMD_I2S_CONTROL_REP_FILL_value;
			replyData->SLOT_SIZE = SI4796X_CMD_I2S_CONTROL_REP_SLOT_SIZE_value;
			replyData->SAMPLE_SIZE = SI4796X_CMD_I2S_CONTROL_REP_SAMPLE_SIZE_value;
			replyData->BITORDER = SI4796X_CMD_I2S_CONTROL_REP_BITORDER_value;
			replyData->SWAP = SI4796X_CMD_I2S_CONTROL_REP_SWAP_value;
			replyData->CLKINV = SI4796X_CMD_I2S_CONTROL_REP_CLKINV_value;
			replyData->FRAMING_MODE = SI4796X_CMD_I2S_CONTROL_REP_FRAMING_MODE_value;
			replyData->CLOCKDOMAIN = SI4796X_CMD_I2S_CONTROL_REP_CLOCKDOMAIN_value;
			replyData->INVERTFS = SI4796X_CMD_I2S_CONTROL_REP_INVERTFS_value;
			replyData->MODE = SI4796X_CMD_I2S_CONTROL_REP_MODE_value;
			replyData->HIZ = SI4796X_CMD_I2S_CONTROL_REP_HIZ_value;
			replyData->LENGTH = SI4796X_CMD_I2S_CONTROL_REP_LENGTH_value;
			replyData->DATA1 = SI4796X_CMD_I2S_CONTROL_REP_DATA1_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_i2s_control(uint8_t icNum, uint8_t  port, uint8_t  mode, SI4796X_i2s_control__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_i2s_control__command(icNum, port, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_i2s_control__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_spdiftx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SPDIFTX0_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SPDIFTX0_CONFIG;

	SI4796X_CMD_SPDIFTX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_SPDIFTX0_CONFIG_ARG_PIN_write_u8(pin);
	SI4796X_CMD_SPDIFTX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_SPDIFTX0_CONFIG_ARG_PARITY_write_field(parity);
	SI4796X_CMD_SPDIFTX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SPDIFTX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_spdiftx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_spdiftx0_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_spdiftx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SPDIFTX1_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SPDIFTX1_CONFIG;

	SI4796X_CMD_SPDIFTX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_SPDIFTX1_CONFIG_ARG_PIN_write_u8(pin);
	SI4796X_CMD_SPDIFTX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_SPDIFTX1_CONFIG_ARG_PARITY_write_field(parity);
	SI4796X_CMD_SPDIFTX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SPDIFTX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_spdiftx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_spdiftx1_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_spdifrx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size, uint8_t parity_err, uint8_t valid_err)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SPDIFRX0_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SPDIFRX0_CONFIG;

	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_PIN_write_u8(pin);
	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_PARITY_write_field(parity);
	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_PARITY_ERR_write_field(parity_err);
	SI4796X_CMD_SPDIFRX0_CONFIG_ARG_VALID_ERR_write_field(valid_err);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SPDIFRX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_spdifrx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size, uint8_t  parity_err, uint8_t  valid_err)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_spdifrx0_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size, parity_err, valid_err);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_spdifrx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size, uint8_t parity_err, uint8_t valid_err)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SPDIFRX1_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SPDIFRX1_CONFIG;

	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_PIN_write_u8(pin);
	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_PARITY_write_field(parity);
	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_PARITY_ERR_write_field(parity_err);
	SI4796X_CMD_SPDIFRX1_CONFIG_ARG_VALID_ERR_write_field(valid_err);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SPDIFRX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_spdifrx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size, uint8_t  parity_err, uint8_t  valid_err)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_spdifrx1_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size, parity_err, valid_err);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_spdif_control__command(uint8_t icNum,uint8_t port, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_SPDIF_CONTROL_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_SPDIF_CONTROL;

	SI4796X_CMD_SPDIF_CONTROL_ARG_PORT_write_field(port);
	SI4796X_CMD_SPDIF_CONTROL_ARG_MODE_write_field(mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_SPDIF_CONTROL;
	}

	return ret;

}

RETURN_CODE SI4796X_spdif_control__reply(uint8_t icNum, SI4796X_spdif_control__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_SPDIF_CONTROL) {
		rdrep_len = SI4796X_CMD_SPDIF_CONTROL_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->SAMPLE_RATE = SI4796X_CMD_SPDIF_CONTROL_REP_SAMPLE_RATE_value;
			replyData->PIN = SI4796X_CMD_SPDIF_CONTROL_REP_PIN_value;
			replyData->CLOCKDOMAIN = SI4796X_CMD_SPDIF_CONTROL_REP_CLOCKDOMAIN_value;
			replyData->PARITY = SI4796X_CMD_SPDIF_CONTROL_REP_PARITY_value;
			replyData->SAMPLE_SIZE = SI4796X_CMD_SPDIF_CONTROL_REP_SAMPLE_SIZE_value;
			replyData->PARITY_ERR = SI4796X_CMD_SPDIF_CONTROL_REP_PARITY_ERR_value;
			replyData->VALID_ERR = SI4796X_CMD_SPDIF_CONTROL_REP_VALID_ERR_value;
			replyData->MODE = SI4796X_CMD_SPDIF_CONTROL_REP_MODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_spdif_control(uint8_t icNum, uint8_t  port, uint8_t  mode, SI4796X_spdif_control__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_spdif_control__command(icNum, port, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_spdif_control__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dac_config__command(uint8_t icNum,uint8_t pumode01, uint8_t dac01_mode, uint8_t pumode23, uint8_t dac23_mode, uint8_t pumode45, uint8_t dac45_mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAC_CONFIG_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAC_CONFIG;

	SI4796X_CMD_DAC_CONFIG_ARG_PUMODE01_write_field(pumode01);
	SI4796X_CMD_DAC_CONFIG_ARG_DAC01_MODE_write_field(dac01_mode);
	SI4796X_CMD_DAC_CONFIG_ARG_PUMODE23_write_field(pumode23);
	SI4796X_CMD_DAC_CONFIG_ARG_DAC23_MODE_write_field(dac23_mode);
	SI4796X_CMD_DAC_CONFIG_ARG_PUMODE45_write_field(pumode45);
	SI4796X_CMD_DAC_CONFIG_ARG_DAC45_MODE_write_field(dac45_mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAC_CONFIG;
	}

	return ret;

}


RETURN_CODE SI4796X_dac_config(uint8_t icNum, uint8_t  pumode01, uint8_t  dac01_mode, uint8_t  pumode23, uint8_t  dac23_mode, uint8_t  pumode45, uint8_t  dac45_mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dac_config__command(icNum, pumode01, dac01_mode, pumode23, dac23_mode, pumode45, dac45_mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_dac_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_DAC_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_DAC_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_DAC_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_dac_status__reply(uint8_t icNum, SI4796X_dac_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_DAC_STATUS) {
		rdrep_len = SI4796X_CMD_DAC_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->DAC01_MODE = SI4796X_CMD_DAC_STATUS_REP_DAC01_MODE_value;
			replyData->DAC23_MODE = SI4796X_CMD_DAC_STATUS_REP_DAC23_MODE_value;
			replyData->DAC45_MODE = SI4796X_CMD_DAC_STATUS_REP_DAC45_MODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_dac_status(uint8_t icNum, SI4796X_dac_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_dac_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI4796X_dac_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_adc_connect__command(uint8_t icNum,uint8_t adc_channel, uint8_t source_pins, uint8_t gain)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_ADC_CONNECT_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_ADC_CONNECT;

	SI4796X_CMD_ADC_CONNECT_ARG_ADC_CHANNEL_write_field(adc_channel);
	SI4796X_CMD_ADC_CONNECT_ARG_SOURCE_PINS_write_field(source_pins);
	SI4796X_CMD_ADC_CONNECT_ARG_GAIN_write_field(gain);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_ADC_CONNECT;
	}

	return ret;

}


RETURN_CODE SI4796X_adc_connect(uint8_t icNum, uint8_t  adc_channel, uint8_t  source_pins, uint8_t  gain)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_adc_connect__command(icNum, adc_channel, source_pins, gain);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_adc_disconnect__command(uint8_t icNum,uint8_t adc_channel)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_ADC_DISCONNECT_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_ADC_DISCONNECT;

	SI4796X_CMD_ADC_DISCONNECT_ARG_ADC_CHANNEL_write_field(adc_channel);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_ADC_DISCONNECT;
	}

	return ret;

}


RETURN_CODE SI4796X_adc_disconnect(uint8_t icNum, uint8_t  adc_channel)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_adc_disconnect__command(icNum, adc_channel);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI4796X_adc_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI4796X_CMD_ADC_STATUS_LENGTH;
	if (cmd_len > SI4796X_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI4796X_CMD_ADC_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI4796X_CMD_ADC_STATUS;
	}

	return ret;

}

RETURN_CODE SI4796X_adc_status__reply(uint8_t icNum, SI4796X_adc_status_adc__data* adc)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (adc == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI4796X_CMD_ADC_STATUS) {
		rdrep_len = SI4796X_CMD_ADC_STATUS_REP_LENGTH;
		if (rdrep_len > SI4796X_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI4796X_CMD_ADC_STATUS_REP_LENGTH;
			if (calculated_response_length > SI4796X_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 6;
				for (n = 0; n < array_elem_count; n++) {
					adc[n].SOURCE_PINS = SI4796X_CMD_ADC_STATUS_REP_ADC_SOURCE_PINS_N_value(n);
					adc[n].GAIN = SI4796X_CMD_ADC_STATUS_REP_ADC_GAIN_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI4796X_adc_status(uint8_t icNum, SI4796X_adc_status_adc__data*  adc)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI4796X_adc_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (adc != NULL) {
		r |= SI4796X_adc_status__reply(icNum, adc);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
