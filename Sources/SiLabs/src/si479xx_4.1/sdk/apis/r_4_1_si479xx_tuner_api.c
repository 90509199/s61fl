/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_tuner_api_h_global_
		
#include "main.h"

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "all_apis.h"

#include "si479xx_tuner_api_constants.h"
#include "si479xx_tuner_api.h"
#endif	/*0*/

#if 1		/* 210427 SGsoft */
extern uint32_t silab_cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];						
extern uint32_t silab_reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];

static uint8_t lastCommands[MAX_NUM_PARTS];
static uint8_t lastTuner;

static uint32_t* cmd_arg_u32 = (uint32_t *)&silab_cmd_arg_u32[4];
static uint16_t*cmd_arg_u16 = (uint16_t*)&silab_cmd_arg_u32[4];
static uint8_t*	cmd_arg = (uint8_t*)&silab_cmd_arg_u32[4];
static int32_t*	cmd_arg_i32 = (int32_t*)&silab_cmd_arg_u32[4];
static int16_t*	cmd_arg_i16 = (int16_t*)&silab_cmd_arg_u32[4];

static uint32_t* reply_arg_u32 = (uint32_t *)&silab_reply_buffer_u32[4];
static uint16_t* reply_arg_u16 = (uint16_t*)&silab_reply_buffer_u32[4];
static uint8_t*	reply_arg = (uint8_t*)&silab_reply_buffer_u32[4];
static int32_t* reply_arg_i32 = (int32_t*)&silab_reply_buffer_u32[4];
static int16_t* reply_arg_i16 = (int16_t*)&silab_reply_buffer_u32[4];

#else
static uint8_t lastCommands[MAX_NUM_PARTS];
static uint8_t lastTuner;

static uint32_t	cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];
static uint16_t*	cmd_arg_u16 = (uint16_t*)cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)cmd_arg_u32;

static uint32_t	reply_arg_u32[SI479XX_RPY_BUFFER_LENGTH/4];
static uint16_t*	reply_arg_u16 = (uint16_t*)reply_arg_u32;
static uint8_t*	reply_arg = (uint8_t*)reply_arg_u32;
static int32_t* 	reply_arg_i32 = (int32_t*)reply_arg_u32;
static int16_t* 	reply_arg_i16 = (int16_t*)reply_arg_u32;
#endif	/*1*/


RETURN_CODE SI479XX_TUNER_set_property__command(uint8_t icNum,uint8_t id, uint16_t prop, uint16_t data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_SET_PROPERTY_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_SET_PROPERTY;

	SI479XX_TUNER_CMD_SET_PROPERTY_ARG_PROP_write_u16(prop);
	SI479XX_TUNER_CMD_SET_PROPERTY_ARG_DATA_write_u16(data);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_SET_PROPERTY;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_set_property(uint8_t icNum, uint8_t id, uint16_t  prop, uint16_t  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_set_property__command(icNum, id, prop, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_get_property__command(uint8_t icNum,uint8_t id, uint8_t count, uint16_t prop)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_GET_PROPERTY_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_GET_PROPERTY;

	SI479XX_TUNER_CMD_GET_PROPERTY_ARG_COUNT_write_u8(count);
	SI479XX_TUNER_CMD_GET_PROPERTY_ARG_PROP_write_u16(prop);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_GET_PROPERTY;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_get_property__reply(uint8_t icNum, uint8_t id, uint16_t* data, uint16_t count)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_GET_PROPERTY) {
		rdrep_len = SI479XX_TUNER_CMD_GET_PROPERTY_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_TUNER_CMD_GET_PROPERTY_REP_LENGTH + SUBCMD_HEADER_SIZE + (count * sizeof(uint16_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_arg);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = count;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_TUNER_CMD_GET_PROPERTY_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_get_property(uint8_t icNum, uint8_t id, uint8_t  arg_count, uint16_t  prop, uint16_t*  data, uint16_t  rep_count)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_get_property__command(icNum, id, arg_count, prop);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (data != NULL) {
		r |= SI479XX_TUNER_get_property__reply(icNum, id, data, rep_count);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_get_mode__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_GET_MODE_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_GET_MODE;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_GET_MODE;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_get_mode__reply(uint8_t icNum, uint8_t id, uint8_t* mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (mode == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_GET_MODE) {
		rdrep_len = SI479XX_TUNER_CMD_GET_MODE_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			*mode = SI479XX_TUNER_CMD_GET_MODE_REP_MODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_get_mode(uint8_t icNum, uint8_t id, uint8_t*  mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_get_mode__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (mode != NULL) {
		r |= SI479XX_TUNER_get_mode__reply(icNum, id, mode);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_set_mode__command(uint8_t icNum,uint8_t id, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_SET_MODE_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_SET_MODE;

	SI479XX_TUNER_CMD_SET_MODE_ARG_MODE_write_field(mode);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_SET_MODE;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_set_mode(uint8_t icNum, uint8_t id, uint8_t  mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_set_mode__command(icNum, id, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_set_radio_audio_filter__command(uint8_t icNum,uint8_t id, uint8_t varfc_en, uint8_t hifi_en, uint8_t i2s_en, uint8_t iir_en, int32_t biquad0_b0, int32_t biquad0_b1, int32_t biquad0_b2, int32_t biquad0_a1, int32_t biquad0_a2, int32_t biquad1_b0, int32_t biquad1_b1, int32_t biquad1_b2, int32_t biquad1_a1, int32_t biquad1_a2, int32_t biquad2_b0, int32_t biquad2_b1, int32_t biquad2_b2, int32_t biquad2_a1, int32_t biquad2_a2)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER;

	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_VARFC_EN_write_field(varfc_en);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_HIFI_EN_write_field(hifi_en);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_I2S_EN_write_field(i2s_en);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_IIR_EN_write_field(iir_en);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD0_B0_write_i32(biquad0_b0);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD0_B1_write_i32(biquad0_b1);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD0_B2_write_i32(biquad0_b2);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD0_A1_write_i32(biquad0_a1);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD0_A2_write_i32(biquad0_a2);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD1_B0_write_i32(biquad1_b0);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD1_B1_write_i32(biquad1_b1);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD1_B2_write_i32(biquad1_b2);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD1_A1_write_i32(biquad1_a1);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD1_A2_write_i32(biquad1_a2);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD2_B0_write_i32(biquad2_b0);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD2_B1_write_i32(biquad2_b1);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD2_B2_write_i32(biquad2_b2);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD2_A1_write_i32(biquad2_a1);
	SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER_ARG_BIQUAD2_A2_write_i32(biquad2_a2);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_SET_RADIO_AUDIO_FILTER;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_set_radio_audio_filter(uint8_t icNum, uint8_t id, uint8_t  varfc_en, uint8_t  hifi_en, uint8_t  i2s_en, uint8_t  iir_en, int32_t  biquad0_b0, int32_t  biquad0_b1, int32_t  biquad0_b2, int32_t  biquad0_a1, int32_t  biquad0_a2, int32_t  biquad1_b0, int32_t  biquad1_b1, int32_t  biquad1_b2, int32_t  biquad1_a1, int32_t  biquad1_a2, int32_t  biquad2_b0, int32_t  biquad2_b1, int32_t  biquad2_b2, int32_t  biquad2_a1, int32_t  biquad2_a2)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_set_radio_audio_filter__command(icNum, id, varfc_en, hifi_en, i2s_en, iir_en, biquad0_b0, biquad0_b1, biquad0_b2, biquad0_a1, biquad0_a2, biquad1_b0, biquad1_b1, biquad1_b2, biquad1_a1, biquad1_a2, biquad2_b0, biquad2_b1, biquad2_b2, biquad2_a1, biquad2_a2);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_get_radio_audio_filter__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_get_radio_audio_filter__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_get_radio_audio_filter__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER) {
		rdrep_len = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->VARFC_EN = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_VARFC_EN_value;
			replyData->HIFI_EN = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_HIFI_EN_value;
			replyData->I2S_EN = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_I2S_EN_value;
			replyData->IIR_EN = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_IIR_EN_value;
			replyData->BIQUAD0_B0 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD0_B0_value;
			replyData->BIQUAD0_B1 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD0_B1_value;
			replyData->BIQUAD0_B2 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD0_B2_value;
			replyData->BIQUAD0_A1 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD0_A1_value;
			replyData->BIQUAD0_A2 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD0_A2_value;
			replyData->BIQUAD1_B0 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD1_B0_value;
			replyData->BIQUAD1_B1 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD1_B1_value;
			replyData->BIQUAD1_B2 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD1_B2_value;
			replyData->BIQUAD1_A1 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD1_A1_value;
			replyData->BIQUAD1_A2 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD1_A2_value;
			replyData->BIQUAD2_B0 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD2_B0_value;
			replyData->BIQUAD2_B1 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD2_B1_value;
			replyData->BIQUAD2_B2 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD2_B2_value;
			replyData->BIQUAD2_A1 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD2_A1_value;
			replyData->BIQUAD2_A2 = SI479XX_TUNER_CMD_GET_RADIO_AUDIO_FILTER_REP_BIQUAD2_A2_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_get_radio_audio_filter(uint8_t icNum, uint8_t id, SI479XX_TUNER_get_radio_audio_filter__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_get_radio_audio_filter__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_get_radio_audio_filter__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t tunemode, uint8_t servo, uint8_t injection, uint16_t freq)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_TUNE_FREQ_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_TUNE_FREQ;

	SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_write_field(tunemode);
	SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_write_field(servo);
	SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_write_field(injection);
	SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_FREQ_write_u16(freq);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_TUNE_FREQ;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_fm_tune_freq(uint8_t icNum, uint8_t id, uint8_t  tunemode, uint8_t  servo, uint8_t  injection, uint16_t  freq)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_tune_freq__command(icNum, id, tunemode, servo, injection, freq);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_seek_start__command(uint8_t icNum,uint8_t id, uint8_t seekup, uint8_t wrap, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SEEK_START_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SEEK_START;

	SI479XX_TUNER_CMD_FM_SEEK_START_ARG_SEEKUP_write_field(seekup);
	SI479XX_TUNER_CMD_FM_SEEK_START_ARG_WRAP_write_field(wrap);
	SI479XX_TUNER_CMD_FM_SEEK_START_ARG_MODE_write_field(mode);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SEEK_START;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_fm_seek_start(uint8_t icNum, uint8_t id, uint8_t  seekup, uint8_t  wrap, uint8_t  mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_seek_start__command(icNum, id, seekup, wrap, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_RSQ_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_RSQ_STATUS;

	SI479XX_TUNER_CMD_FM_RSQ_STATUS_ARG_ATTUNE_write_field(attune);
	SI479XX_TUNER_CMD_FM_RSQ_STATUS_ARG_CANCEL_write_field(cancel);
	SI479XX_TUNER_CMD_FM_RSQ_STATUS_ARG_STCACK_write_field(stcack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_RSQ_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_rsq_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_rsq_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_RSQ_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->HDSEARCHDONE = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_HDSEARCHDONE_value;
			replyData->BLTF = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_BLTF_value;
			replyData->T3READY = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_T3READY_value;
			replyData->T2READY = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_T2READY_value;
			replyData->T1READY = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_T1READY_value;
			replyData->INJECT = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_INJECT_value;
			replyData->AFCRL = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_AFCRL_value;
			replyData->VALID = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_VALID_value;
			replyData->FREQ = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_FREQ_value;
			replyData->FREQOFF = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_FREQOFF_value;
			replyData->RSSI = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RSSI_value;
			replyData->SNR = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_SNR_value;
			replyData->LASSI = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_LASSI_value;
			replyData->HASSI = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_HASSI_value;
			replyData->ASSI100 = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_ASSI100_value;
			replyData->LASSI200 = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_LASSI200_value;
			replyData->HASSI200 = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_HASSI200_value;
			replyData->ASSI200 = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_ASSI200_value;
			replyData->MULTIPATH = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_MULTIPATH_value;
			replyData->USN = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_USN_value;
			replyData->DEVIATION = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_DEVIATION_value;
			replyData->COCHANL = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_COCHANL_value;
			replyData->PDEV = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_PDEV_value;
			replyData->RDSDEV = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RDSDEV_value;
			replyData->SSDEV = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_SSDEV_value;
			replyData->PILOTLOCK = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_PILOTLOCK_value;
			replyData->HDLEVEL = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_HDLEVEL_value;
			replyData->RSSI_DP = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RSSI_DP_value;
			replyData->SNR_DP = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_SNR_DP_value;
			replyData->MULTIPATH_DP = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_MULTIPATH_DP_value;
			replyData->USN_DP = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_USN_DP_value;
			replyData->RSSI_ADJ = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RSSI_ADJ_value;
			replyData->PHSDIV_COMB_WGHT = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_PHSDIV_COMB_WGHT_value;
			replyData->RDS_CHAN_BW = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RDS_CHAN_BW_value;
			replyData->RSSI_NC = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RSSI_NC_value;
			replyData->RSSI_NC_ADJ = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_RSSI_NC_ADJ_value;
			replyData->FILTERED_HDLEVEL = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_FILTERED_HDLEVEL_value;
			replyData->CLEANSIGNAL = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_CLEANSIGNAL_value;
			replyData->MAXDEV = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_MAXDEV_value;
			replyData->USN_MONO = SI479XX_TUNER_CMD_FM_RSQ_STATUS_REP_USN_MONO_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI479XX_TUNER_fm_rsq_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_rsq_status__command(icNum, id, attune, cancel, stcack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_rsq_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_rds_status__command(uint8_t icNum,uint8_t id, uint8_t statusonly, uint8_t mtfifo, uint8_t intack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_RDS_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_RDS_STATUS;

	SI479XX_TUNER_CMD_FM_RDS_STATUS_ARG_STATUSONLY_write_field(statusonly);
	SI479XX_TUNER_CMD_FM_RDS_STATUS_ARG_MTFIFO_write_field(mtfifo);
	SI479XX_TUNER_CMD_FM_RDS_STATUS_ARG_INTACK_write_field(intack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_RDS_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_rds_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_rds_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_RDS_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->TPPTYINT = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_TPPTYINT_value;
			replyData->PIINT = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_PIINT_value;
			replyData->SYNCINT = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_SYNCINT_value;
			replyData->FIFOINT = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_FIFOINT_value;
			replyData->TPPTYVALID = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_TPPTYVALID_value;
			replyData->PIVALID = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_PIVALID_value;
			replyData->SYNC = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_SYNC_value;
			replyData->FIFOLOST = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_FIFOLOST_value;
			replyData->TP = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_TP_value;
			replyData->PTY = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_PTY_value;
			replyData->PI = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_PI_value;
			replyData->RDSFIFOUSED = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_RDSFIFOUSED_value;
			replyData->BLERA = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLERA_value;
			replyData->BLERB = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLERB_value;
			replyData->BLERC = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLERC_value;
			replyData->BLERD = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLERD_value;
			replyData->BLOCKA = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLOCKA_value;
			replyData->BLOCKB = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLOCKB_value;
			replyData->BLOCKC = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLOCKC_value;
			replyData->BLOCKD = SI479XX_TUNER_CMD_FM_RDS_STATUS_REP_BLOCKD_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_rds_status(uint8_t icNum, uint8_t id, uint8_t  statusonly, uint8_t  mtfifo, uint8_t  intack, SI479XX_TUNER_fm_rds_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_rds_status__command(icNum, id, statusonly, mtfifo, intack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_rds_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_rds_blockcount__command(uint8_t icNum,uint8_t id, uint8_t clear)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT;

	SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT_ARG_CLEAR_write_field(clear);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_rds_blockcount__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_rds_blockcount__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT) {
		rdrep_len = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->EXPECTED = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT_REP_EXPECTED_value;
			replyData->RECEIVED = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT_REP_RECEIVED_value;
			replyData->UNCORRECTABLE = SI479XX_TUNER_CMD_FM_RDS_BLOCKCOUNT_REP_UNCORRECTABLE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_rds_blockcount(uint8_t icNum, uint8_t id, uint8_t  clear, SI479XX_TUNER_fm_rds_blockcount__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_rds_blockcount__command(icNum, id, clear);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_rds_blockcount__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_servo_status__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SERVO_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SERVO_STATUS;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SERVO_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_servo_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_servo_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_SERVO_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->CHANBW = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_CHANBW_value;
			replyData->SOFTMUTE = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_SOFTMUTE_value;
			replyData->HICUT = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_HICUT_value;
			replyData->LOWCUT = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_LOWCUT_value;
			replyData->STBLEND = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_STBLEND_value;
			replyData->HIBLEND = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_HIBLEND_value;
			replyData->NBUSN1THR = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_NBUSN1THR_value;
			replyData->NBUSN2THR = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_NBUSN2THR_value;
			replyData->FADETHRESH = SI479XX_TUNER_CMD_FM_SERVO_STATUS_REP_FADETHRESH_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_servo_status(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_servo_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_servo_status__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_servo_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_servo_trigger_status__command(uint8_t icNum,uint8_t id, uint8_t trigint_ack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS;

	SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_ARG_TRIGINT_ACK_write_field(trigint_ack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_servo_trigger_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_servo_trigger_status_servotrigger__data* servotrigger)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (servotrigger == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_arg);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 4;
				for (n = 0; n < array_elem_count; n++) {
					servotrigger[n].X_IN = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_X_IN_N_value(n);
					servotrigger[n].ABOVE_INT = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ABOVE_INT_N_value(n);
					servotrigger[n].BELOW_INT = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_BELOW_INT_N_value(n);
					servotrigger[n].ACTIVE_INT = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ACTIVE_INT_N_value(n);
					servotrigger[n].ACTIVE = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ACTIVE_N_value(n);
					servotrigger[n].CONFIGURED = SI479XX_TUNER_CMD_FM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_CONFIGURED_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_servo_trigger_status(uint8_t icNum, uint8_t id, uint8_t  trigint_ack, SI479XX_TUNER_fm_servo_trigger_status_servotrigger__data*  servotrigger)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_servo_trigger_status__command(icNum, id, trigint_ack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (servotrigger != NULL) {
		r |= SI479XX_TUNER_fm_servo_trigger_status__reply(icNum, id, servotrigger);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_signal_event_count__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SIGNAL_EVENT_COUNT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SIGNAL_EVENT_COUNT;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SIGNAL_EVENT_COUNT;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_signal_event_count__reply(uint8_t icNum, uint8_t id, uint16_t* nb_aud_cnt)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (nb_aud_cnt == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_SIGNAL_EVENT_COUNT) {
		rdrep_len = SI479XX_TUNER_CMD_FM_SIGNAL_EVENT_COUNT_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			*nb_aud_cnt = SI479XX_TUNER_CMD_FM_SIGNAL_EVENT_COUNT_REP_NB_AUD_CNT_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_signal_event_count(uint8_t icNum, uint8_t id, uint16_t*  nb_aud_cnt)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_signal_event_count__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (nb_aud_cnt != NULL) {
		r |= SI479XX_TUNER_fm_signal_event_count__reply(icNum, id, nb_aud_cnt);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_set_servo__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_id, int16_t min, int16_t max, int16_t init, uint8_t accumulator)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SET_SERVO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SET_SERVO;

	SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_write_u8(fm_servo_id);
	SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_MIN_write_i16(min);
	SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_MAX_write_i16(max);
	SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_INIT_write_i16(init);
	SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_ACCUMULATOR_write_u8(accumulator);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SET_SERVO;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_fm_set_servo(uint8_t icNum, uint8_t id, uint8_t  fm_servo_id, int16_t  min, int16_t  max, int16_t  init, uint8_t  accumulator)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_set_servo__command(icNum, id, fm_servo_id, min, max, init, accumulator);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_get_servo__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_GET_SERVO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_GET_SERVO;

	SI479XX_TUNER_CMD_FM_GET_SERVO_ARG_FM_SERVO_ID_write_u8(fm_servo_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_GET_SERVO;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_get_servo__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_get_servo__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_GET_SERVO) {
		rdrep_len = SI479XX_TUNER_CMD_FM_GET_SERVO_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->SETTING = SI479XX_TUNER_CMD_FM_GET_SERVO_REP_SETTING_value;
			replyData->MIN = SI479XX_TUNER_CMD_FM_GET_SERVO_REP_MIN_value;
			replyData->MAX = SI479XX_TUNER_CMD_FM_GET_SERVO_REP_MAX_value;
			replyData->INIT = SI479XX_TUNER_CMD_FM_GET_SERVO_REP_INIT_value;
			replyData->ACCUMULATOR = SI479XX_TUNER_CMD_FM_GET_SERVO_REP_ACCUMULATOR_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_get_servo(uint8_t icNum, uint8_t id, uint8_t  fm_servo_id, SI479XX_TUNER_fm_get_servo__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_get_servo__command(icNum, id, fm_servo_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_get_servo__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_set_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_transform_id, uint8_t fm_metric_id, uint8_t fm_servo_id, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate, uint8_t enable_other0, uint8_t other_transform_at_ymax, uint8_t enable_other1, uint8_t other_transform_at_ymin, uint8_t enable_trigger, uint8_t invert_trigger, uint8_t fm_servo_trigger_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_TRANSFORM_ID_write_field(fm_servo_transform_id);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_write_u8(fm_metric_id);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_write_u8(fm_servo_id);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_X1_write_i16(x1);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_Y1_write_i16(y1);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_X2_write_i16(x2);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_Y2_write_i16(y2);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_P1TOP2RATE_write_u16(p1top2rate);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_P2TOP1RATE_write_u16(p2top1rate);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_ENABLE_OTHER0_write_field(enable_other0);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_OTHER_TRANSFORM_AT_YMAX_write_field(other_transform_at_ymax);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_ENABLE_OTHER1_write_field(enable_other1);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_OTHER_TRANSFORM_AT_YMIN_write_field(other_transform_at_ymin);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_ENABLE_TRIGGER_write_field(enable_trigger);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_INVERT_TRIGGER_write_field(invert_trigger);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_TRIGGER_ID_write_field(fm_servo_trigger_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_fm_set_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_servo_transform_id, uint8_t  fm_metric_id, uint8_t  fm_servo_id, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate, uint8_t  enable_other0, uint8_t  other_transform_at_ymax, uint8_t  enable_other1, uint8_t  other_transform_at_ymin, uint8_t  enable_trigger, uint8_t  invert_trigger, uint8_t  fm_servo_trigger_id)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_set_servo_transform__command(icNum, id, fm_servo_transform_id, fm_metric_id, fm_servo_id, x1, y1, x2, y2, p1top2rate, p2top1rate, enable_other0, other_transform_at_ymax, enable_other1, other_transform_at_ymin, enable_trigger, invert_trigger, fm_servo_trigger_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_set_fast_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_fast_servo_transform_id, uint8_t enable, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_FM_FAST_SERVO_TRANSFORM_ID_write_field(fm_fast_servo_transform_id);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_ENABLE_write_u8(enable);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_X1_write_i16(x1);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_Y1_write_i16(y1);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_X2_write_i16(x2);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_Y2_write_i16(y2);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_P1TOP2RATE_write_u16(p1top2rate);
	SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM_ARG_P2TOP1RATE_write_u16(p2top1rate);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SET_FAST_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_fm_set_fast_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_fast_servo_transform_id, uint8_t  enable, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_set_fast_servo_transform__command(icNum, id, fm_fast_servo_transform_id, enable, x1, y1, x2, y2, p1top2rate, p2top1rate);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_get_fast_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_fast_servo_transform_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_ARG_FM_FAST_SERVO_TRANSFORM_ID_write_field(fm_fast_servo_transform_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_get_fast_servo_transform__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_get_fast_servo_transform__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM) {
		rdrep_len = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->FM_METRIC_ID = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_FM_METRIC_ID_value;
			replyData->FM_SERVO_ID = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_FM_SERVO_ID_value;
			replyData->X1 = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_X1_value;
			replyData->Y1 = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_Y1_value;
			replyData->X2 = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_X2_value;
			replyData->Y2 = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_Y2_value;
			replyData->P1TOP2RATE = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_P1TOP2RATE_value;
			replyData->P2TOP1RATE = SI479XX_TUNER_CMD_FM_GET_FAST_SERVO_TRANSFORM_REP_P2TOP1RATE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_get_fast_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_fast_servo_transform_id, SI479XX_TUNER_fm_get_fast_servo_transform__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_get_fast_servo_transform__command(icNum, id, fm_fast_servo_transform_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_get_fast_servo_transform__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_get_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_transform_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_ARG_FM_SERVO_TRANSFORM_ID_write_field(fm_servo_transform_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_get_servo_transform__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_get_servo_transform__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM) {
		rdrep_len = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->FM_METRIC_ID = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_FM_METRIC_ID_value;
			replyData->FM_SERVO_ID = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_FM_SERVO_ID_value;
			replyData->X1 = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_X1_value;
			replyData->Y1 = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_Y1_value;
			replyData->X2 = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_X2_value;
			replyData->Y2 = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_Y2_value;
			replyData->P1TOP2RATE = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_P1TOP2RATE_value;
			replyData->P2TOP1RATE = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_P2TOP1RATE_value;
			replyData->OTHER0_ENABLED = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_OTHER0_ENABLED_value;
			replyData->OTHER_TRANSFORM_AT_YMAX = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_OTHER_TRANSFORM_AT_YMAX_value;
			replyData->OTHER1_ENABLED = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_OTHER1_ENABLED_value;
			replyData->OTHER_TRANSFORM_AT_YMIN = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_OTHER_TRANSFORM_AT_YMIN_value;
			replyData->TRIGGER_ENABLED = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_TRIGGER_ENABLED_value;
			replyData->TRIGGER_INVERTED = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_TRIGGER_INVERTED_value;
			replyData->FM_SERVO_TRIGGER_ID = SI479XX_TUNER_CMD_FM_GET_SERVO_TRANSFORM_REP_FM_SERVO_TRIGGER_ID_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_get_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_servo_transform_id, SI479XX_TUNER_fm_get_servo_transform__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_get_servo_transform__command(icNum, id, fm_servo_transform_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_get_servo_transform__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_set_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_trigger_id, uint8_t inactive_int, uint8_t active_int, uint8_t fm_metric_id, int16_t lower, int16_t upper)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER;

	SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_ARG_FM_SERVO_TRIGGER_ID_write_field(fm_servo_trigger_id);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_ARG_INACTIVE_INT_write_field(inactive_int);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_ARG_ACTIVE_INT_write_field(active_int);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_ARG_FM_METRIC_ID_write_u8(fm_metric_id);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_ARG_LOWER_write_i16(lower);
	SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER_ARG_UPPER_write_i16(upper);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_SET_SERVO_TRIGGER;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_fm_set_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  fm_servo_trigger_id, uint8_t  inactive_int, uint8_t  active_int, uint8_t  fm_metric_id, int16_t  lower, int16_t  upper)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_set_servo_trigger__command(icNum, id, fm_servo_trigger_id, inactive_int, active_int, fm_metric_id, lower, upper);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_fm_get_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_trigger_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER;

	SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_ARG_FM_SERVO_TRIGGER_ID_write_field(fm_servo_trigger_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_fm_get_servo_trigger__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_fm_get_servo_trigger__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER) {
		rdrep_len = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->INACTIVE_INT = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_INACTIVE_INT_value;
			replyData->ACTIVE_INT = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_ACTIVE_INT_value;
			replyData->IN_RANGE = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_IN_RANGE_value;
			replyData->FM_METRIC_ID = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_FM_METRIC_ID_value;
			replyData->LOWER = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_LOWER_value;
			replyData->UPPER = SI479XX_TUNER_CMD_FM_GET_SERVO_TRIGGER_REP_UPPER_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_fm_get_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  fm_servo_trigger_id, SI479XX_TUNER_fm_get_servo_trigger__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_fm_get_servo_trigger__command(icNum, id, fm_servo_trigger_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_fm_get_servo_trigger__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_set_servo__command(uint8_t icNum,uint8_t id, uint8_t am_servo_id, int16_t min, int16_t max, int16_t init, uint8_t accumulator)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SET_SERVO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SET_SERVO;

	SI479XX_TUNER_CMD_AM_SET_SERVO_ARG_AM_SERVO_ID_write_u8(am_servo_id);
	SI479XX_TUNER_CMD_AM_SET_SERVO_ARG_MIN_write_i16(min);
	SI479XX_TUNER_CMD_AM_SET_SERVO_ARG_MAX_write_i16(max);
	SI479XX_TUNER_CMD_AM_SET_SERVO_ARG_INIT_write_i16(init);
	SI479XX_TUNER_CMD_AM_SET_SERVO_ARG_ACCUMULATOR_write_u8(accumulator);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SET_SERVO;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_am_set_servo(uint8_t icNum, uint8_t id, uint8_t  am_servo_id, int16_t  min, int16_t  max, int16_t  init, uint8_t  accumulator)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_set_servo__command(icNum, id, am_servo_id, min, max, init, accumulator);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_get_servo__command(uint8_t icNum,uint8_t id, uint8_t am_servo_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_GET_SERVO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_GET_SERVO;

	SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_write_u8(am_servo_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_GET_SERVO;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_get_servo__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_get_servo__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_GET_SERVO) {
		rdrep_len = SI479XX_TUNER_CMD_AM_GET_SERVO_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->SETTING = SI479XX_TUNER_CMD_AM_GET_SERVO_REP_SETTING_value;
			replyData->MIN = SI479XX_TUNER_CMD_AM_GET_SERVO_REP_MIN_value;
			replyData->MAX = SI479XX_TUNER_CMD_AM_GET_SERVO_REP_MAX_value;
			replyData->INIT = SI479XX_TUNER_CMD_AM_GET_SERVO_REP_INIT_value;
			replyData->ACCUMULATOR = SI479XX_TUNER_CMD_AM_GET_SERVO_REP_ACCUMULATOR_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_get_servo(uint8_t icNum, uint8_t id, uint8_t  am_servo_id, SI479XX_TUNER_am_get_servo__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_get_servo__command(icNum, id, am_servo_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_am_get_servo__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_set_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t am_servo_transform_id, uint8_t am_metric_id, uint8_t am_servo_id, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate, uint8_t enable_other0, uint8_t other_transform_at_ymax, uint8_t enable_other1, uint8_t other_transform_at_ymin, uint8_t enable_trigger, uint8_t invert_trigger, uint8_t am_servo_trigger_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_TRANSFORM_ID_write_field(am_servo_transform_id);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_write_u8(am_metric_id);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_ID_write_u8(am_servo_id);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_X1_write_i16(x1);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_Y1_write_i16(y1);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_X2_write_i16(x2);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_Y2_write_i16(y2);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_P1TOP2RATE_write_u16(p1top2rate);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_P2TOP1RATE_write_u16(p2top1rate);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_ENABLE_OTHER0_write_field(enable_other0);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_OTHER_TRANSFORM_AT_YMAX_write_field(other_transform_at_ymax);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_ENABLE_OTHER1_write_field(enable_other1);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_OTHER_TRANSFORM_AT_YMIN_write_field(other_transform_at_ymin);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_ENABLE_TRIGGER_write_field(enable_trigger);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_INVERT_TRIGGER_write_field(invert_trigger);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_TRIGGER_ID_write_field(am_servo_trigger_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_am_set_servo_transform(uint8_t icNum, uint8_t id, uint8_t  am_servo_transform_id, uint8_t  am_metric_id, uint8_t  am_servo_id, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate, uint8_t  enable_other0, uint8_t  other_transform_at_ymax, uint8_t  enable_other1, uint8_t  other_transform_at_ymin, uint8_t  enable_trigger, uint8_t  invert_trigger, uint8_t  am_servo_trigger_id)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_set_servo_transform__command(icNum, id, am_servo_transform_id, am_metric_id, am_servo_id, x1, y1, x2, y2, p1top2rate, p2top1rate, enable_other0, other_transform_at_ymax, enable_other1, other_transform_at_ymin, enable_trigger, invert_trigger, am_servo_trigger_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_get_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t am_servo_transform_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_ARG_AM_SERVO_TRANSFORM_ID_write_field(am_servo_transform_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_get_servo_transform__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_get_servo_transform__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM) {
		rdrep_len = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->AM_METRIC_ID = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_AM_METRIC_ID_value;
			replyData->AM_SERVO_ID = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_AM_SERVO_ID_value;
			replyData->X1 = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_X1_value;
			replyData->Y1 = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_Y1_value;
			replyData->X2 = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_X2_value;
			replyData->Y2 = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_Y2_value;
			replyData->P1TOP2RATE = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_P1TOP2RATE_value;
			replyData->P2TOP1RATE = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_P2TOP1RATE_value;
			replyData->OTHER0_ENABLED = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_OTHER0_ENABLED_value;
			replyData->OTHER_TRANSFORM_AT_YMAX = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_OTHER_TRANSFORM_AT_YMAX_value;
			replyData->OTHER1_ENABLED = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_OTHER1_ENABLED_value;
			replyData->OTHER_TRANSFORM_AT_YMIN = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_OTHER_TRANSFORM_AT_YMIN_value;
			replyData->TRIGGER_ENABLED = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_TRIGGER_ENABLED_value;
			replyData->TRIGGER_INVERTED = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_TRIGGER_INVERTED_value;
			replyData->AM_SERVO_TRIGGER_ID = SI479XX_TUNER_CMD_AM_GET_SERVO_TRANSFORM_REP_AM_SERVO_TRIGGER_ID_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_get_servo_transform(uint8_t icNum, uint8_t id, uint8_t  am_servo_transform_id, SI479XX_TUNER_am_get_servo_transform__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_get_servo_transform__command(icNum, id, am_servo_transform_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_am_get_servo_transform__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_set_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t am_servo_trigger_id, uint8_t inactive_int, uint8_t active_int, uint8_t am_metric_id, int16_t lower, int16_t upper)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER;

	SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_ARG_AM_SERVO_TRIGGER_ID_write_field(am_servo_trigger_id);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_ARG_INACTIVE_INT_write_field(inactive_int);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_ARG_ACTIVE_INT_write_field(active_int);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_ARG_AM_METRIC_ID_write_u8(am_metric_id);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_ARG_LOWER_write_i16(lower);
	SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER_ARG_UPPER_write_i16(upper);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SET_SERVO_TRIGGER;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_am_set_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  am_servo_trigger_id, uint8_t  inactive_int, uint8_t  active_int, uint8_t  am_metric_id, int16_t  lower, int16_t  upper)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_set_servo_trigger__command(icNum, id, am_servo_trigger_id, inactive_int, active_int, am_metric_id, lower, upper);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_get_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t am_servo_trigger_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER;

	SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_ARG_AM_SERVO_TRIGGER_ID_write_field(am_servo_trigger_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_get_servo_trigger__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_get_servo_trigger__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER) {
		rdrep_len = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->INACTIVE_INT = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_INACTIVE_INT_value;
			replyData->ACTIVE_INT = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_ACTIVE_INT_value;
			replyData->IN_RANGE = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_IN_RANGE_value;
			replyData->AM_METRIC_ID = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_AM_METRIC_ID_value;
			replyData->LOWER = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_LOWER_value;
			replyData->UPPER = SI479XX_TUNER_CMD_AM_GET_SERVO_TRIGGER_REP_UPPER_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_get_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  am_servo_trigger_id, SI479XX_TUNER_am_get_servo_trigger__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_get_servo_trigger__command(icNum, id, am_servo_trigger_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_am_get_servo_trigger__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_set_servo__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_id, int16_t min, int16_t max, int16_t init, uint8_t accumulator)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_SET_SERVO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_SET_SERVO;

	SI479XX_TUNER_CMD_WB_SET_SERVO_ARG_WB_SERVO_ID_write_u8(wb_servo_id);
	SI479XX_TUNER_CMD_WB_SET_SERVO_ARG_MIN_write_i16(min);
	SI479XX_TUNER_CMD_WB_SET_SERVO_ARG_MAX_write_i16(max);
	SI479XX_TUNER_CMD_WB_SET_SERVO_ARG_INIT_write_i16(init);
	SI479XX_TUNER_CMD_WB_SET_SERVO_ARG_ACCUMULATOR_write_u8(accumulator);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_SET_SERVO;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_wb_set_servo(uint8_t icNum, uint8_t id, uint8_t  wb_servo_id, int16_t  min, int16_t  max, int16_t  init, uint8_t  accumulator)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_set_servo__command(icNum, id, wb_servo_id, min, max, init, accumulator);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_get_servo__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_GET_SERVO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_GET_SERVO;

	SI479XX_TUNER_CMD_WB_GET_SERVO_ARG_WB_SERVO_ID_write_u8(wb_servo_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_GET_SERVO;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_get_servo__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_get_servo__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_GET_SERVO) {
		rdrep_len = SI479XX_TUNER_CMD_WB_GET_SERVO_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->SETTING = SI479XX_TUNER_CMD_WB_GET_SERVO_REP_SETTING_value;
			replyData->MIN = SI479XX_TUNER_CMD_WB_GET_SERVO_REP_MIN_value;
			replyData->MAX = SI479XX_TUNER_CMD_WB_GET_SERVO_REP_MAX_value;
			replyData->INIT = SI479XX_TUNER_CMD_WB_GET_SERVO_REP_INIT_value;
			replyData->ACCUMULATOR = SI479XX_TUNER_CMD_WB_GET_SERVO_REP_ACCUMULATOR_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_get_servo(uint8_t icNum, uint8_t id, uint8_t  wb_servo_id, SI479XX_TUNER_wb_get_servo__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_get_servo__command(icNum, id, wb_servo_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_wb_get_servo__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_set_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_transform_id, uint8_t wb_metric_id, uint8_t wb_servo_id, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate, uint8_t enable_other0, uint8_t other_transform_at_ymax, uint8_t enable_other1, uint8_t other_transform_at_ymin, uint8_t enable_trigger, uint8_t invert_trigger, uint8_t wb_servo_trigger_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_WB_SERVO_TRANSFORM_ID_write_field(wb_servo_transform_id);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_WB_METRIC_ID_write_u8(wb_metric_id);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_WB_SERVO_ID_write_u8(wb_servo_id);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_X1_write_i16(x1);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_Y1_write_i16(y1);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_X2_write_i16(x2);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_Y2_write_i16(y2);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_P1TOP2RATE_write_u16(p1top2rate);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_P2TOP1RATE_write_u16(p2top1rate);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_ENABLE_OTHER0_write_field(enable_other0);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_OTHER_TRANSFORM_AT_YMAX_write_field(other_transform_at_ymax);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_ENABLE_OTHER1_write_field(enable_other1);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_OTHER_TRANSFORM_AT_YMIN_write_field(other_transform_at_ymin);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_ENABLE_TRIGGER_write_field(enable_trigger);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_INVERT_TRIGGER_write_field(invert_trigger);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM_ARG_WB_SERVO_TRIGGER_ID_write_field(wb_servo_trigger_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_SET_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_wb_set_servo_transform(uint8_t icNum, uint8_t id, uint8_t  wb_servo_transform_id, uint8_t  wb_metric_id, uint8_t  wb_servo_id, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate, uint8_t  enable_other0, uint8_t  other_transform_at_ymax, uint8_t  enable_other1, uint8_t  other_transform_at_ymin, uint8_t  enable_trigger, uint8_t  invert_trigger, uint8_t  wb_servo_trigger_id)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_set_servo_transform__command(icNum, id, wb_servo_transform_id, wb_metric_id, wb_servo_id, x1, y1, x2, y2, p1top2rate, p2top1rate, enable_other0, other_transform_at_ymax, enable_other1, other_transform_at_ymin, enable_trigger, invert_trigger, wb_servo_trigger_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_get_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_transform_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM;

	SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_ARG_WB_SERVO_TRANSFORM_ID_write_field(wb_servo_transform_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_get_servo_transform__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_get_servo_transform__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM) {
		rdrep_len = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->WB_METRIC_ID = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_WB_METRIC_ID_value;
			replyData->WB_SERVO_ID = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_WB_SERVO_ID_value;
			replyData->X1 = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_X1_value;
			replyData->Y1 = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_Y1_value;
			replyData->X2 = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_X2_value;
			replyData->Y2 = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_Y2_value;
			replyData->P1TOP2RATE = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_P1TOP2RATE_value;
			replyData->P2TOP1RATE = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_P2TOP1RATE_value;
			replyData->OTHER0_ENABLED = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_OTHER0_ENABLED_value;
			replyData->OTHER_TRANSFORM_AT_YMAX = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_OTHER_TRANSFORM_AT_YMAX_value;
			replyData->OTHER1_ENABLED = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_OTHER1_ENABLED_value;
			replyData->OTHER_TRANSFORM_AT_YMIN = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_OTHER_TRANSFORM_AT_YMIN_value;
			replyData->TRIGGER_ENABLED = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_TRIGGER_ENABLED_value;
			replyData->TRIGGER_INVERTED = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_TRIGGER_INVERTED_value;
			replyData->WB_SERVO_TRIGGER_ID = SI479XX_TUNER_CMD_WB_GET_SERVO_TRANSFORM_REP_WB_SERVO_TRIGGER_ID_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_get_servo_transform(uint8_t icNum, uint8_t id, uint8_t  wb_servo_transform_id, SI479XX_TUNER_wb_get_servo_transform__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_get_servo_transform__command(icNum, id, wb_servo_transform_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_wb_get_servo_transform__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_set_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_trigger_id, uint8_t inactive_int, uint8_t active_int, uint8_t wb_metric_id, int16_t lower, int16_t upper)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER;

	SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_ARG_WB_SERVO_TRIGGER_ID_write_field(wb_servo_trigger_id);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_ARG_INACTIVE_INT_write_field(inactive_int);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_ARG_ACTIVE_INT_write_field(active_int);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_ARG_WB_METRIC_ID_write_u8(wb_metric_id);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_ARG_LOWER_write_i16(lower);
	SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER_ARG_UPPER_write_i16(upper);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_SET_SERVO_TRIGGER;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_wb_set_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  wb_servo_trigger_id, uint8_t  inactive_int, uint8_t  active_int, uint8_t  wb_metric_id, int16_t  lower, int16_t  upper)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_set_servo_trigger__command(icNum, id, wb_servo_trigger_id, inactive_int, active_int, wb_metric_id, lower, upper);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_get_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_trigger_id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER;

	SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_ARG_WB_SERVO_TRIGGER_ID_write_field(wb_servo_trigger_id);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_get_servo_trigger__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_get_servo_trigger__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER) {
		rdrep_len = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->INACTIVE_INT = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_INACTIVE_INT_value;
			replyData->ACTIVE_INT = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_ACTIVE_INT_value;
			replyData->IN_RANGE = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_IN_RANGE_value;
			replyData->WB_METRIC_ID = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_WB_METRIC_ID_value;
			replyData->LOWER = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_LOWER_value;
			replyData->UPPER = SI479XX_TUNER_CMD_WB_GET_SERVO_TRIGGER_REP_UPPER_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_get_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  wb_servo_trigger_id, SI479XX_TUNER_wb_get_servo_trigger__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_get_servo_trigger__command(icNum, id, wb_servo_trigger_id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_wb_get_servo_trigger__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t digital, uint8_t tunemode, uint8_t servo, uint8_t injection, uint16_t freq)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_TUNE_FREQ_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_TUNE_FREQ;

	SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_DIGITAL_write_field(digital);
	SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_TUNEMODE_write_field(tunemode);
	SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_SERVO_write_field(servo);
	SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_INJECTION_write_field(injection);
	SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_FREQ_write_u16(freq);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_TUNE_FREQ;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_am_tune_freq(uint8_t icNum, uint8_t id, uint8_t  digital, uint8_t  tunemode, uint8_t  servo, uint8_t  injection, uint16_t  freq)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_tune_freq__command(icNum, id, digital, tunemode, servo, injection, freq);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_seek_start__command(uint8_t icNum,uint8_t id, uint8_t seekup, uint8_t digital, uint8_t wrap, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SEEK_START_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SEEK_START;

	SI479XX_TUNER_CMD_AM_SEEK_START_ARG_SEEKUP_write_field(seekup);
	SI479XX_TUNER_CMD_AM_SEEK_START_ARG_DIGITAL_write_field(digital);
	SI479XX_TUNER_CMD_AM_SEEK_START_ARG_WRAP_write_field(wrap);
	SI479XX_TUNER_CMD_AM_SEEK_START_ARG_MODE_write_field(mode);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SEEK_START;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_am_seek_start(uint8_t icNum, uint8_t id, uint8_t  seekup, uint8_t  digital, uint8_t  wrap, uint8_t  mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_seek_start__command(icNum, id, seekup, digital, wrap, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_RSQ_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_RSQ_STATUS;

	SI479XX_TUNER_CMD_AM_RSQ_STATUS_ARG_ATTUNE_write_field(attune);
	SI479XX_TUNER_CMD_AM_RSQ_STATUS_ARG_CANCEL_write_field(cancel);
	SI479XX_TUNER_CMD_AM_RSQ_STATUS_ARG_STCACK_write_field(stcack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_RSQ_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_rsq_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_rsq_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_RSQ_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->HDSEARCHDONE = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_HDSEARCHDONE_value;
			replyData->DRM30_DETECT_DONE = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_DRM30_DETECT_DONE_value;
			replyData->BLTF = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_BLTF_value;
			replyData->T3READY = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_T3READY_value;
			replyData->T2READY = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_T2READY_value;
			replyData->T1READY = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_T1READY_value;
			replyData->INJECT = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_INJECT_value;
			replyData->AFCRL = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_AFCRL_value;
			replyData->VALID = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_VALID_value;
			replyData->FREQ = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_FREQ_value;
			replyData->FREQOFF = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_FREQOFF_value;
			replyData->RSSI = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_RSSI_value;
			replyData->SNR = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_SNR_value;
			replyData->LASSI = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_LASSI_value;
			replyData->HASSI = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_HASSI_value;
			replyData->LASSI2 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_LASSI2_value;
			replyData->HASSI2 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_HASSI2_value;
			replyData->LASSI3 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_LASSI3_value;
			replyData->HASSI3 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_HASSI3_value;
			replyData->LASSI4 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_LASSI4_value;
			replyData->HASSI4 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_HASSI4_value;
			replyData->ASSI = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_ASSI_value;
			replyData->ASSI2 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_ASSI2_value;
			replyData->ASSI3 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_ASSI3_value;
			replyData->ASSI4 = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_ASSI4_value;
			replyData->IQSNR = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_IQSNR_value;
			replyData->MODINDEX = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_MODINDEX_value;
			replyData->HDLEVEL = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_HDLEVEL_value;
			replyData->RSSI_ADJ = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_RSSI_ADJ_value;
			replyData->RSSI_NC = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_RSSI_NC_value;
			replyData->RSSI_NC_ADJ = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_RSSI_NC_ADJ_value;
			replyData->FILTERED_HDLEVEL = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_FILTERED_HDLEVEL_value;
			replyData->AM_SYMMETRY = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_AM_SYMMETRY_value;
			replyData->COCHANM = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_COCHANM_value;
			replyData->AMIC_BLEND = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_AMIC_BLEND_value;
			replyData->AVC_STATUS = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_AVC_STATUS_value;
			replyData->DRM30MODE = SI479XX_TUNER_CMD_AM_RSQ_STATUS_REP_DRM30MODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI479XX_TUNER_am_rsq_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_rsq_status__command(icNum, id, attune, cancel, stcack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_am_rsq_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_servo_status__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SERVO_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SERVO_STATUS;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SERVO_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_servo_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_servo_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_SERVO_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->CHANBW = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_CHANBW_value;
			replyData->SOFTMUTE = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_SOFTMUTE_value;
			replyData->HICUT = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_HICUT_value;
			replyData->LOWCUT = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_LOWCUT_value;
			replyData->NBAUDTH = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_NBAUDTH_value;
			replyData->AMVARFC = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_AMVARFC_value;
			replyData->NBIQ2TH = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_NBIQ2TH_value;
			replyData->NBIQ3TH = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_NBIQ3TH_value;
			replyData->MAXAVC = SI479XX_TUNER_CMD_AM_SERVO_STATUS_REP_MAXAVC_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_servo_status(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_servo_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_servo_status__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_am_servo_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_servo_trigger_status__command(uint8_t icNum,uint8_t id, uint8_t trigint_ack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS;

	SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_ARG_TRIGINT_ACK_write_field(trigint_ack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_servo_trigger_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_servo_trigger_status_servotrigger__data* servotrigger)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (servotrigger == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_arg);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 4;
				for (n = 0; n < array_elem_count; n++) {
					servotrigger[n].X_IN = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_X_IN_N_value(n);
					servotrigger[n].ABOVE_INT = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ABOVE_INT_N_value(n);
					servotrigger[n].BELOW_INT = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_BELOW_INT_N_value(n);
					servotrigger[n].ACTIVE_INT = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ACTIVE_INT_N_value(n);
					servotrigger[n].ACTIVE = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ACTIVE_N_value(n);
					servotrigger[n].CONFIGURED = SI479XX_TUNER_CMD_AM_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_CONFIGURED_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_servo_trigger_status(uint8_t icNum, uint8_t id, uint8_t  trigint_ack, SI479XX_TUNER_am_servo_trigger_status_servotrigger__data*  servotrigger)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_servo_trigger_status__command(icNum, id, trigint_ack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (servotrigger != NULL) {
		r |= SI479XX_TUNER_am_servo_trigger_status__reply(icNum, id, servotrigger);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_am_signal_event_count__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_am_signal_event_count__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_signal_event_count__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT) {
		rdrep_len = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->NB_AUD_CNT = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT_REP_NB_AUD_CNT_value;
			replyData->NB_IQ_CNT2 = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT_REP_NB_IQ_CNT2_value;
			replyData->NB_IQ_CNT3 = SI479XX_TUNER_CMD_AM_SIGNAL_EVENT_COUNT_REP_NB_IQ_CNT3_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_am_signal_event_count(uint8_t icNum, uint8_t id, SI479XX_TUNER_am_signal_event_count__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_am_signal_event_count__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_am_signal_event_count__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_dab_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t detectmode1, uint16_t freq_index)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_DAB_TUNE_FREQ_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_DAB_TUNE_FREQ;

	SI479XX_TUNER_CMD_DAB_TUNE_FREQ_ARG_DETECTMODE1_write_field(detectmode1);
	SI479XX_TUNER_CMD_DAB_TUNE_FREQ_ARG_FREQ_INDEX_write_u16(freq_index);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_DAB_TUNE_FREQ;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_dab_tune_freq(uint8_t icNum, uint8_t id, uint8_t  detectmode1, uint16_t  freq_index)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_dab_tune_freq__command(icNum, id, detectmode1, freq_index);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_dab_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_DAB_RSQ_STATUS;

	SI479XX_TUNER_CMD_DAB_RSQ_STATUS_ARG_ATTUNE_write_field(attune);
	SI479XX_TUNER_CMD_DAB_RSQ_STATUS_ARG_CANCEL_write_field(cancel);
	SI479XX_TUNER_CMD_DAB_RSQ_STATUS_ARG_STCACK_write_field(stcack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_DAB_RSQ_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_dab_rsq_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_dab_rsq_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_DAB_RSQ_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->T1READY = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_T1READY_value;
			replyData->VALID = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_VALID_value;
			replyData->FREQ_INDEX = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_FREQ_INDEX_value;
			replyData->FREQ = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_FREQ_value;
			replyData->SQI = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_SQI_value;
			replyData->RSSI = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_RSSI_value;
			replyData->DABDETECT = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_DABDETECT_value;
			replyData->RSSI_ADJ = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_RSSI_ADJ_value;
			replyData->DIGITAL_GAIN = SI479XX_TUNER_CMD_DAB_RSQ_STATUS_REP_DIGITAL_GAIN_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_dab_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI479XX_TUNER_dab_rsq_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_dab_rsq_status__command(icNum, id, attune, cancel, stcack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_dab_rsq_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t tunemode, uint8_t servo, uint8_t injection, uint16_t freq)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_TUNE_FREQ_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_TUNE_FREQ;

	SI479XX_TUNER_CMD_WB_TUNE_FREQ_ARG_TUNEMODE_write_field(tunemode);
	SI479XX_TUNER_CMD_WB_TUNE_FREQ_ARG_SERVO_write_field(servo);
	SI479XX_TUNER_CMD_WB_TUNE_FREQ_ARG_INJECTION_write_field(injection);
	SI479XX_TUNER_CMD_WB_TUNE_FREQ_ARG_FREQ_write_u16(freq);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_TUNE_FREQ;
		lastTuner = id;
	}

	return ret;

}


RETURN_CODE SI479XX_TUNER_wb_tune_freq(uint8_t icNum, uint8_t id, uint8_t  tunemode, uint8_t  servo, uint8_t  injection, uint16_t  freq)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_tune_freq__command(icNum, id, tunemode, servo, injection, freq);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_RSQ_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_RSQ_STATUS;

	SI479XX_TUNER_CMD_WB_RSQ_STATUS_ARG_ATTUNE_write_field(attune);
	SI479XX_TUNER_CMD_WB_RSQ_STATUS_ARG_CANCEL_write_field(cancel);
	SI479XX_TUNER_CMD_WB_RSQ_STATUS_ARG_STCACK_write_field(stcack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_RSQ_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_rsq_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_rsq_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_RSQ_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->T3READY = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_T3READY_value;
			replyData->T2READY = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_T2READY_value;
			replyData->T1READY = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_T1READY_value;
			replyData->INJECT = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_INJECT_value;
			replyData->AFCRL = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_AFCRL_value;
			replyData->VALID = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_VALID_value;
			replyData->FREQ = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_FREQ_value;
			replyData->FREQOFF = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_FREQOFF_value;
			replyData->RSSI = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_RSSI_value;
			replyData->SNR = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_SNR_value;
			replyData->ISSI = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_ISSI_value;
			replyData->LASSI = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_LASSI_value;
			replyData->HASSI = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_HASSI_value;
			replyData->ASSI = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_ASSI_value;
			replyData->RSSI_ADJ = SI479XX_TUNER_CMD_WB_RSQ_STATUS_REP_RSSI_ADJ_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI479XX_TUNER_wb_rsq_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_rsq_status__command(icNum, id, attune, cancel, stcack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_wb_rsq_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_servo_status__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_SERVO_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_SERVO_STATUS;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_SERVO_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_servo_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_servo_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_SERVO_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_WB_SERVO_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->CHANBW = SI479XX_TUNER_CMD_WB_SERVO_STATUS_REP_CHANBW_value;
			replyData->SOFTMUTE = SI479XX_TUNER_CMD_WB_SERVO_STATUS_REP_SOFTMUTE_value;
			replyData->HICUT = SI479XX_TUNER_CMD_WB_SERVO_STATUS_REP_HICUT_value;
			replyData->LOWCUT = SI479XX_TUNER_CMD_WB_SERVO_STATUS_REP_LOWCUT_value;
			replyData->NBTHR = SI479XX_TUNER_CMD_WB_SERVO_STATUS_REP_NBTHR_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_servo_status(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_servo_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_servo_status__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_TUNER_wb_servo_status__reply(icNum, id, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_servo_trigger_status__command(uint8_t icNum,uint8_t id, uint8_t trigint_ack)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS;

	SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_ARG_TRIGINT_ACK_write_field(trigint_ack);

	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_servo_trigger_status__reply(uint8_t icNum, uint8_t id, SI479XX_TUNER_wb_servo_trigger_status_servotrigger__data* servotrigger)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (servotrigger == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS) {
		rdrep_len = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_LENGTH + SUBCMD_HEADER_SIZE;
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_arg);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 4;
				for (n = 0; n < array_elem_count; n++) {
					servotrigger[n].X_IN = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_X_IN_N_value(n);
					servotrigger[n].ABOVE_INT = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ABOVE_INT_N_value(n);
					servotrigger[n].BELOW_INT = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_BELOW_INT_N_value(n);
					servotrigger[n].ACTIVE_INT = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ACTIVE_INT_N_value(n);
					servotrigger[n].ACTIVE = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_ACTIVE_N_value(n);
					servotrigger[n].CONFIGURED = SI479XX_TUNER_CMD_WB_SERVO_TRIGGER_STATUS_REP_SERVOTRIGGER_CONFIGURED_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_servo_trigger_status(uint8_t icNum, uint8_t id, uint8_t  trigint_ack, SI479XX_TUNER_wb_servo_trigger_status_servotrigger__data*  servotrigger)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_servo_trigger_status__command(icNum, id, trigint_ack);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (servotrigger != NULL) {
		r |= SI479XX_TUNER_wb_servo_trigger_status__reply(icNum, id, servotrigger);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_TUNER_wb_signal_event_count__command(uint8_t icNum,uint8_t id)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_TUNER_CMD_WB_SIGNAL_EVENT_COUNT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_TUNER_CMD_WB_SIGNAL_EVENT_COUNT;


	ret = TUNER_SUBCMD(icNum, id, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_TUNER_CMD_WB_SIGNAL_EVENT_COUNT;
		lastTuner = id;
	}

	return ret;

}

RETURN_CODE SI479XX_TUNER_wb_signal_event_count__reply(uint8_t icNum, uint8_t id, uint16_t* nb_aud_cnt)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (nb_aud_cnt == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_TUNER_CMD_WB_SIGNAL_EVENT_COUNT) {
		rdrep_len = SI479XX_TUNER_CMD_WB_SIGNAL_EVENT_COUNT_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			*nb_aud_cnt = SI479XX_TUNER_CMD_WB_SIGNAL_EVENT_COUNT_REP_NB_AUD_CNT_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_TUNER_wb_signal_event_count(uint8_t icNum, uint8_t id, uint16_t*  nb_aud_cnt)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_TUNER_wb_signal_event_count__command(icNum, id);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (nb_aud_cnt != NULL) {
		r |= SI479XX_TUNER_wb_signal_event_count__reply(icNum, id, nb_aud_cnt);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
