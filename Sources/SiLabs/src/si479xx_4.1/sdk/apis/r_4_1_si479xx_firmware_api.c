/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_firmware_api_h_global_
		
#include "main.h"
#include "debug.h"
#if 0
#include "common_types.h"
#include "feature_types.h"
#include "all_apis.h"

#include "si479xx_firmware_api_constants.h"
#include "si479xx_firmware_api.h"
#endif	/*0*/

#if 1		/* 210427 SGsoft */
uint32_t silab_cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];						
uint32_t silab_reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];

static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t* cmd_arg_u32 = (uint32_t *)&silab_cmd_arg_u32;
static uint16_t*cmd_arg_u16 = (uint16_t*)&silab_cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)&silab_cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)&silab_cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)&silab_cmd_arg_u32;

static uint32_t* reply_buffer_u32 = (uint32_t *)&silab_reply_buffer_u32;
static uint16_t* reply_buffer_u16 = (uint16_t*)&silab_reply_buffer_u32;
static uint8_t*	reply_buffer = (uint8_t*)&silab_reply_buffer_u32;
static int32_t* reply_buffer_i32 = (int32_t*)&silab_reply_buffer_u32;
static int16_t* reply_buffer_i16 = (int16_t*)&silab_reply_buffer_u32;

#else
#ifndef SI479XX_CMD_BUFFER_LENGTH
#define SI479XX_CMD_BUFFER_LENGTH (4096 + 32)
#endif

#ifndef SI479XX_RPY_BUFFER_LENGTH
#define SI479XX_RPY_BUFFER_LENGTH (8192)
#endif


static uint8_t lastCommands[MAX_NUM_PARTS];

static uint32_t	cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];
static uint16_t*	cmd_arg_u16 = (uint16_t*)cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)cmd_arg_u32;

static uint32_t	reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];
static uint16_t*	reply_buffer_u16 = (uint16_t*)reply_buffer_u32;
static uint8_t*	reply_buffer = (uint8_t*)reply_buffer_u32;
static int32_t* 	reply_buffer_i32 = (int32_t*)reply_buffer_u32;
static int16_t* 	reply_buffer_i16 = (int16_t*)reply_buffer_u32;
#endif	/*1*/


RETURN_CODE SI479XX_read_status__reply(uint8_t icNum, SI479XX_read_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	if (lastCommands[icIndex] == SI479XX_CMD_READ_STATUS) {
		rdrep_len = SI479XX_CMD_READ_STATUS_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->CTS = SI479XX_CMD_READ_STATUS_REP_CTS_value;
			replyData->APIERR = SI479XX_CMD_READ_STATUS_REP_APIERR_value;
			replyData->SWERR = SI479XX_CMD_READ_STATUS_REP_SWERR_value;
			replyData->VDERR = SI479XX_CMD_READ_STATUS_REP_VDERR_value;
			replyData->RDSINT = SI479XX_CMD_READ_STATUS_REP_RDSINT_value;
			replyData->STCINT = SI479XX_CMD_READ_STATUS_REP_STCINT_value;
			replyData->AUDINT = SI479XX_CMD_READ_STATUS_REP_AUDINT_value;
			replyData->ACTS3 = SI479XX_CMD_READ_STATUS_REP_ACTS3_value;
			replyData->ACTS2 = SI479XX_CMD_READ_STATUS_REP_ACTS2_value;
			replyData->ACTS1 = SI479XX_CMD_READ_STATUS_REP_ACTS1_value;
			replyData->ACTS0 = SI479XX_CMD_READ_STATUS_REP_ACTS0_value;
			replyData->PUP_STATE = SI479XX_CMD_READ_STATUS_REP_PUP_STATE_value;
			replyData->HOTWRN = SI479XX_CMD_READ_STATUS_REP_HOTWRN_value;
			replyData->TMPERR = SI479XX_CMD_READ_STATUS_REP_TMPERR_value;
			replyData->CMDERR = SI479XX_CMD_READ_STATUS_REP_CMDERR_value;
			replyData->REPERR = SI479XX_CMD_READ_STATUS_REP_REPERR_value;
			replyData->ARBERR = SI479XX_CMD_READ_STATUS_REP_ARBERR_value;
			replyData->NRERR = SI479XX_CMD_READ_STATUS_REP_NRERR_value;

			TunPUPState = replyData->PUP_STATE;											/* 210429 SGsoft */
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_read_status(uint8_t icNum, SI479XX_read_status__data*  replyData)
{
	RETURN_CODE r = 0;
	uint8_t icIndex = 0;
	uint8_t origCmd = 0;
	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	iapis_acquireLock(icNum);
	// Workaround to avoid changing generated reply wihtout a real command.
	origCmd = lastCommands[icIndex];
	lastCommands[icIndex] = SI479XX_CMD_READ_STATUS;
	r = SI479XX_read_status__reply(icNum, replyData);
	lastCommands[icIndex] = origCmd;
	iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_power_up__command(uint8_t icNum,uint8_t ctsien, uint8_t clko_current, uint8_t vio, uint8_t clkout, uint8_t clk_mode, uint8_t tr_size, uint8_t ctun, uint32_t xtal_freq, uint8_t ibias, uint8_t afs, uint8_t chipid, uint8_t eziq_master, uint8_t eziq_enable)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_POWER_UP_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_POWER_UP;

	SI479XX_CMD_POWER_UP_ARG_CTSIEN_write_field(ctsien);
	SI479XX_CMD_POWER_UP_ARG_CLKO_CURRENT_write_field(clko_current);
	SI479XX_CMD_POWER_UP_ARG_VIO_write_field(vio);
	SI479XX_CMD_POWER_UP_ARG_CLKOUT_write_field(clkout);
	SI479XX_CMD_POWER_UP_ARG_CLK_MODE_write_field(clk_mode);
	SI479XX_CMD_POWER_UP_ARG_TR_SIZE_write_field(tr_size);
	SI479XX_CMD_POWER_UP_ARG_CTUN_write_field(ctun);
	SI479XX_CMD_POWER_UP_ARG_XTAL_FREQ_write_u32(xtal_freq);
	SI479XX_CMD_POWER_UP_ARG_IBIAS_write_u8(ibias);
	SI479XX_CMD_POWER_UP_ARG_AFS_write_field(afs);
	SI479XX_CMD_POWER_UP_ARG_CHIPID_write_field(chipid);
	SI479XX_CMD_POWER_UP_ARG_EZIQ_MASTER_write_field(eziq_master);
	SI479XX_CMD_POWER_UP_ARG_EZIQ_ENABLE_write_field(eziq_enable);

	DPRINTF(DBG_INFO,"SI479XX_power_up__command %x %x %x %x %x %x %x %x\n\r", cmd_arg[0],cmd_arg[1],cmd_arg[2],cmd_arg[3],cmd_arg[4],cmd_arg[5],cmd_arg[6],cmd_arg[7]);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_POWER_UP;
	}

	return ret;

}

RETURN_CODE SI479XX_power_up__reply(uint8_t icNum, SI479XX_power_up__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_POWER_UP) {
		rdrep_len = SI479XX_CMD_POWER_UP_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->UNCORRECTABLE = SI479XX_CMD_POWER_UP_REP_UNCORRECTABLE_value;
			replyData->CORRECTABLE = SI479XX_CMD_POWER_UP_REP_CORRECTABLE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_power_up(uint8_t icNum, uint8_t  ctsien, uint8_t  clko_current, uint8_t  vio, uint8_t  clkout, uint8_t  clk_mode, uint8_t  tr_size, uint8_t  ctun, uint32_t  xtal_freq, uint8_t  ibias, uint8_t  afs, uint8_t  chipid, uint8_t  eziq_master, uint8_t  eziq_enable, SI479XX_power_up__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_power_up__command(icNum, ctsien, clko_current, vio, clkout, clk_mode, tr_size, ctun, xtal_freq, ibias, afs, chipid, eziq_master, eziq_enable);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_power_up__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_err__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_ERR_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_ERR;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_ERR;
	}

	return ret;

}

RETURN_CODE SI479XX_err__reply(uint8_t icNum, uint8_t* code)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (code == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_ERR) {
		rdrep_len = SI479XX_CMD_ERR_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			*code = SI479XX_CMD_ERR_REP_CODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_err(uint8_t icNum, uint8_t*  code)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_err__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (code != NULL) {
		r |= SI479XX_err__reply(icNum, code);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_cmd0__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_CMD0_LENGTH + length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_CMD0;

	SI479XX_CMD_HIFI_CMD0_ARG_PACKET_COUNT_write_u8(packet_count);
	SI479XX_CMD_HIFI_CMD0_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI479XX_CMD_HIFI_CMD0_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_CMD0;
	}

	return ret;

}


RETURN_CODE SI479XX_hifi_cmd0(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_cmd0__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_cmd1__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_CMD1_LENGTH + length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_CMD1;

	SI479XX_CMD_HIFI_CMD1_ARG_PACKET_COUNT_write_u8(packet_count);
	SI479XX_CMD_HIFI_CMD1_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI479XX_CMD_HIFI_CMD1_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_CMD1;
	}

	return ret;

}


RETURN_CODE SI479XX_hifi_cmd1(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_cmd1__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_cmd2__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_CMD2_LENGTH + length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_CMD2;

	SI479XX_CMD_HIFI_CMD2_ARG_PACKET_COUNT_write_u8(packet_count);
	SI479XX_CMD_HIFI_CMD2_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI479XX_CMD_HIFI_CMD2_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_CMD2;
	}

	return ret;

}


RETURN_CODE SI479XX_hifi_cmd2(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_cmd2__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_cmd3__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_CMD3_LENGTH + length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_CMD3;

	SI479XX_CMD_HIFI_CMD3_ARG_PACKET_COUNT_write_u8(packet_count);
	SI479XX_CMD_HIFI_CMD3_ARG_LENGTH_write_u16(length);
	for(n=0;n < length;n++) {
		SI479XX_CMD_HIFI_CMD3_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_CMD3;
	}

	return ret;

}


RETURN_CODE SI479XX_hifi_cmd3(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_cmd3__command(icNum, packet_count, length, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_resp0__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_RESP0_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_RESP0;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_RESP0;
	}

	return ret;

}

RETURN_CODE SI479XX_hifi_resp0__reply(uint8_t icNum, SI479XX_hifi_resp0__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_HIFI_RESP0) {
		rdrep_len = SI479XX_CMD_HIFI_RESP0_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI479XX_CMD_HIFI_RESP0_REP_PIPE_value;
			replyData->PACKET_COUNT = SI479XX_CMD_HIFI_RESP0_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI479XX_CMD_HIFI_RESP0_REP_LENGTH_value;

			calculated_response_length = SI479XX_CMD_HIFI_RESP0_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_HIFI_RESP0_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_hifi_resp0(uint8_t icNum, SI479XX_hifi_resp0__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_resp0__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_hifi_resp0__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_resp1__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_RESP1_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_RESP1;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_RESP1;
	}

	return ret;

}

RETURN_CODE SI479XX_hifi_resp1__reply(uint8_t icNum, SI479XX_hifi_resp1__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_HIFI_RESP1) {
		rdrep_len = SI479XX_CMD_HIFI_RESP1_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI479XX_CMD_HIFI_RESP1_REP_PIPE_value;
			replyData->PACKET_COUNT = SI479XX_CMD_HIFI_RESP1_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI479XX_CMD_HIFI_RESP1_REP_LENGTH_value;

			calculated_response_length = SI479XX_CMD_HIFI_RESP1_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_HIFI_RESP1_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_hifi_resp1(uint8_t icNum, SI479XX_hifi_resp1__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_resp1__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_hifi_resp1__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_resp2__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_RESP2_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_RESP2;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_RESP2;
	}

	return ret;

}

RETURN_CODE SI479XX_hifi_resp2__reply(uint8_t icNum, SI479XX_hifi_resp2__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_HIFI_RESP2) {
		rdrep_len = SI479XX_CMD_HIFI_RESP2_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI479XX_CMD_HIFI_RESP2_REP_PIPE_value;
			replyData->PACKET_COUNT = SI479XX_CMD_HIFI_RESP2_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI479XX_CMD_HIFI_RESP2_REP_LENGTH_value;

			calculated_response_length = SI479XX_CMD_HIFI_RESP2_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_HIFI_RESP2_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_hifi_resp2(uint8_t icNum, SI479XX_hifi_resp2__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_resp2__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_hifi_resp2__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_resp3__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_RESP3_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_RESP3;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_RESP3;
	}

	return ret;

}

RETURN_CODE SI479XX_hifi_resp3__reply(uint8_t icNum, SI479XX_hifi_resp3__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_HIFI_RESP3) {
		rdrep_len = SI479XX_CMD_HIFI_RESP3_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->PIPE = SI479XX_CMD_HIFI_RESP3_REP_PIPE_value;
			replyData->PACKET_COUNT = SI479XX_CMD_HIFI_RESP3_REP_PACKET_COUNT_value;
			replyData->LENGTH = SI479XX_CMD_HIFI_RESP3_REP_LENGTH_value;

			calculated_response_length = SI479XX_CMD_HIFI_RESP3_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_HIFI_RESP3_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_hifi_resp3(uint8_t icNum, SI479XX_hifi_resp3__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_resp3__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_hifi_resp3__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_datalogger_source__command(uint8_t icNum,uint8_t source)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DATALOGGER_SOURCE_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DATALOGGER_SOURCE;

	SI479XX_CMD_DATALOGGER_SOURCE_ARG_SOURCE_write_field(source);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DATALOGGER_SOURCE;
	}

	return ret;

}


RETURN_CODE SI479XX_datalogger_source(uint8_t icNum, uint8_t  source)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_datalogger_source__command(icNum, source);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_datalogger_trace__command(uint8_t icNum,uint8_t group_id, uint8_t destination)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DATALOGGER_TRACE_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DATALOGGER_TRACE;

	SI479XX_CMD_DATALOGGER_TRACE_ARG_GROUP_ID_write_field(group_id);
	SI479XX_CMD_DATALOGGER_TRACE_ARG_DESTINATION_write_field(destination);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DATALOGGER_TRACE;
	}

	return ret;

}


RETURN_CODE SI479XX_datalogger_trace(uint8_t icNum, uint8_t  group_id, uint8_t  destination)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_datalogger_trace__command(icNum, group_id, destination);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_send_test_pattern__command(uint8_t icNum,uint8_t port, uint8_t length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_SEND_TEST_PATTERN_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_SEND_TEST_PATTERN;

	SI479XX_CMD_SEND_TEST_PATTERN_ARG_PORT_write_field(port);
	SI479XX_CMD_SEND_TEST_PATTERN_ARG_LENGTH_write_u8(length);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_SEND_TEST_PATTERN;
	}

	return ret;

}

RETURN_CODE SI479XX_send_test_pattern__reply(uint8_t icNum, uint8_t* data, uint16_t length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_SEND_TEST_PATTERN) {
		rdrep_len = SI479XX_CMD_SEND_TEST_PATTERN_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_CMD_SEND_TEST_PATTERN_REP_LENGTH + (length * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = length;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_SEND_TEST_PATTERN_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_send_test_pattern(uint8_t icNum, uint8_t  port, uint8_t  arg_length, uint8_t*  data, uint16_t  rep_length)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_send_test_pattern__command(icNum, port, arg_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (data != NULL) {
		r |= SI479XX_send_test_pattern__reply(icNum, data, rep_length);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_log__command(uint8_t icNum,uint8_t block)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_LOG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_LOG;

	SI479XX_CMD_HIFI_LOG_ARG_BLOCK_write_field(block);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_LOG;
	}

	return ret;

}

RETURN_CODE SI479XX_hifi_log__reply(uint8_t icNum, uint8_t* log)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (log == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_HIFI_LOG) {
		rdrep_len = SI479XX_CMD_HIFI_LOG_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_hifi_log(uint8_t icNum, uint8_t  block, uint8_t*  log)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_log__command(icNum, block);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (log != NULL) {
		r |= SI479XX_hifi_log__reply(icNum, log);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_host_load__command(uint8_t icNum,uint16_t size, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HOST_LOAD_LENGTH + size;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HOST_LOAD;

	SI479XX_CMD_HOST_LOAD_ARG_SIZE_write_u16(size);
	for(n=0;n < size;n++) {
		SI479XX_CMD_HOST_LOAD_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HOST_LOAD;
	}

	return ret;

}


RETURN_CODE SI479XX_host_load(uint8_t icNum, uint16_t  size, uint8_t*  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_host_load__command(icNum, size, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_flash_load__command(uint8_t icNum,uint8_t subcmd, uint8_t* data, uint16_t data_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_FLASH_LOAD_LENGTH + data_length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	///ClearMemory(cmd_arg, cmd_len);								/* 210512 SGsoft */	

	cmd_arg[0] = SI479XX_CMD_FLASH_LOAD;

	SI479XX_CMD_FLASH_LOAD_ARG_SUBCMD_write_u8(subcmd);
	for(n=0;n < data_length;n++) {
		SI479XX_CMD_FLASH_LOAD_ARG_DATA_N_write_u8(n,data[n]);
	}

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_FLASH_LOAD;
	}

	return ret;

}

RETURN_CODE SI479XX_flash_load__reply(uint8_t icNum, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_FLASH_LOAD) {
		rdrep_len = SI479XX_CMD_FLASH_LOAD_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_CMD_FLASH_LOAD_REP_LENGTH;
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 32;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_FLASH_LOAD_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_flash_load(uint8_t icNum, uint8_t  subcmd, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_flash_load__command(icNum, subcmd, arg_data, data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (rep_data != NULL) {
		r |= SI479XX_flash_load__reply(icNum, rep_data);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_load_init__command(uint8_t icNum,uint8_t count, uint32_t* guid)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_LOAD_INIT_LENGTH + (count * sizeof(uint32_t));
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_LOAD_INIT;

	SI479XX_CMD_LOAD_INIT_ARG_COUNT_write_u8(count);
	for(n=0;n < count;n++) {
		SI479XX_CMD_LOAD_INIT_ARG_GUID_N_write_u32(n,guid[n]);
	}

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_LOAD_INIT;
	}

	return ret;

}


RETURN_CODE SI479XX_load_init(uint8_t icNum, uint8_t  count, uint32_t*  guid)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_load_init__command(icNum, count, guid);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_boot__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_BOOT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_BOOT;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_BOOT;
	}

	return ret;

}


RETURN_CODE SI479XX_boot(uint8_t icNum)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_boot__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_get_part_info__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_GET_PART_INFO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_GET_PART_INFO;

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_GET_PART_INFO;
	}

	return ret;

}

RETURN_CODE SI479XX_get_part_info__reply(uint8_t icNum, SI479XX_get_part_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_GET_PART_INFO) {
		rdrep_len = SI479XX_CMD_GET_PART_INFO_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->CHIPREV = SI479XX_CMD_GET_PART_INFO_REP_CHIPREV_value;
			replyData->ROMREV = SI479XX_CMD_GET_PART_INFO_REP_ROMREV_value;
			replyData->FAMILY = SI479XX_CMD_GET_PART_INFO_REP_FAMILY_value;
			replyData->PN0 = SI479XX_CMD_GET_PART_INFO_REP_PN0_value;
			replyData->PN1 = SI479XX_CMD_GET_PART_INFO_REP_PN1_value;
			replyData->MAJOR = SI479XX_CMD_GET_PART_INFO_REP_MAJOR_value;
			replyData->CUST1 = SI479XX_CMD_GET_PART_INFO_REP_CUST1_value;
			replyData->CUST2 = SI479XX_CMD_GET_PART_INFO_REP_CUST2_value;
			replyData->CORRECTED = SI479XX_CMD_GET_PART_INFO_REP_CORRECTED_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_get_part_info(uint8_t icNum, SI479XX_get_part_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_get_part_info__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_get_part_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_get_power_up_args__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_GET_POWER_UP_ARGS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_GET_POWER_UP_ARGS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_GET_POWER_UP_ARGS;
	}

	return ret;

}

RETURN_CODE SI479XX_get_power_up_args__reply(uint8_t icNum, SI479XX_get_power_up_args__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_GET_POWER_UP_ARGS) {
		rdrep_len = SI479XX_CMD_GET_POWER_UP_ARGS_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->CTSIEN = SI479XX_CMD_GET_POWER_UP_ARGS_REP_CTSIEN_value;
			replyData->CLKO_CURRENT = SI479XX_CMD_GET_POWER_UP_ARGS_REP_CLKO_CURRENT_value;
			replyData->VIO = SI479XX_CMD_GET_POWER_UP_ARGS_REP_VIO_value;
			replyData->CLKOUT = SI479XX_CMD_GET_POWER_UP_ARGS_REP_CLKOUT_value;
			replyData->CLK_MODE = SI479XX_CMD_GET_POWER_UP_ARGS_REP_CLK_MODE_value;
			replyData->TR_SIZE = SI479XX_CMD_GET_POWER_UP_ARGS_REP_TR_SIZE_value;
			replyData->CTUN = SI479XX_CMD_GET_POWER_UP_ARGS_REP_CTUN_value;
			replyData->XTAL_FREQ = SI479XX_CMD_GET_POWER_UP_ARGS_REP_XTAL_FREQ_value;
			replyData->IBIAS = SI479XX_CMD_GET_POWER_UP_ARGS_REP_IBIAS_value;
			replyData->AFS = SI479XX_CMD_GET_POWER_UP_ARGS_REP_AFS_value;
			replyData->CHIPID = SI479XX_CMD_GET_POWER_UP_ARGS_REP_CHIPID_value;
			replyData->EZIQ_MASTER = SI479XX_CMD_GET_POWER_UP_ARGS_REP_EZIQ_MASTER_value;
			replyData->EZIQ_ENABLE = SI479XX_CMD_GET_POWER_UP_ARGS_REP_EZIQ_ENABLE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_get_power_up_args(uint8_t icNum, SI479XX_get_power_up_args__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_get_power_up_args__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_get_power_up_args__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_part_override__command(uint8_t icNum,uint8_t pn0, uint8_t pn1, uint8_t major, uint8_t romascii, uint8_t cust1, uint8_t cust2)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_PART_OVERRIDE_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_PART_OVERRIDE;

	SI479XX_CMD_PART_OVERRIDE_ARG_PN0_write_u8(pn0);
	SI479XX_CMD_PART_OVERRIDE_ARG_PN1_write_u8(pn1);
	SI479XX_CMD_PART_OVERRIDE_ARG_MAJOR_write_u8(major);
	SI479XX_CMD_PART_OVERRIDE_ARG_ROMASCII_write_u8(romascii);
	SI479XX_CMD_PART_OVERRIDE_ARG_CUST1_write_u8(cust1);
	SI479XX_CMD_PART_OVERRIDE_ARG_CUST2_write_u8(cust2);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_PART_OVERRIDE;
	}

	return ret;

}


RETURN_CODE SI479XX_part_override(uint8_t icNum, uint8_t  pn0, uint8_t  pn1, uint8_t  major, uint8_t  romascii, uint8_t  cust1, uint8_t  cust2)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_part_override__command(icNum, pn0, pn1, major, romascii, cust1, cust2);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_load_config__command(uint8_t icNum,uint8_t numchips, uint8_t chipid, uint8_t role, uint8_t mosi_in, uint8_t miso_in, uint8_t intb_in, uint8_t nvsclk_in, uint8_t nvssb_in, uint8_t nvmosi_in, uint8_t nvmiso_in, uint8_t rsvd_in, uint8_t iqfsp_in, uint8_t iqfsn_in, uint8_t ioutp_in, uint8_t ioutn_in, uint8_t qoutp_in, uint8_t qoutn_in, uint8_t ssb_in, uint8_t sclk_in, uint8_t dout0_in, uint8_t dclk0_in, uint8_t dfs0_in, uint8_t dout1_in, uint8_t dclk1_in, uint8_t dfs1_in, uint8_t iqclkp_in, uint8_t iqclkn_in, uint8_t diclk2_in, uint8_t difs2_in, uint8_t din3_in, uint8_t diclk3_in, uint8_t difs3_in, uint8_t din4_in, uint8_t diclk4_in, uint8_t difs4_in, uint8_t din0_in, uint8_t gp0_in, uint8_t gp1_in, uint8_t gp2_in, uint8_t din1_in, uint8_t diclk1_in, uint8_t difs1_in, uint8_t din2_in, uint8_t aout0_in, uint8_t aout1_in, uint8_t icp_in, uint8_t icn_in, uint8_t difs0_in, uint8_t diclk0_in, uint8_t mosi_out, uint8_t miso_out, uint8_t intb_out, uint8_t nvsclk_out, uint8_t nvssb_out, uint8_t nvmosi_out, uint8_t nvmiso_out, uint8_t rsvd_out, uint8_t iqfsp_out, uint8_t iqfsn_out, uint8_t ioutp_out, uint8_t ioutn_out, uint8_t qoutp_out, uint8_t qoutn_out, uint8_t ssb_out, uint8_t sclk_out, uint8_t dout0_out, uint8_t dclk0_out, uint8_t dfs0_out, uint8_t dout1_out, uint8_t dclk1_out, uint8_t dfs1_out, uint8_t iqclkp_out, uint8_t iqclkn_out, uint8_t diclk2_out, uint8_t difs2_out, uint8_t din3_out, uint8_t diclk3_out, uint8_t difs3_out, uint8_t din4_out, uint8_t diclk4_out, uint8_t difs4_out, uint8_t din0_out, uint8_t gp0_out, uint8_t gp1_out, uint8_t gp2_out, uint8_t din1_out, uint8_t diclk1_out, uint8_t difs1_out, uint8_t din2_out, uint8_t icp_out, uint8_t icn_out, uint8_t difs0_out, uint8_t diclk0_out, uint32_t clksel8, uint32_t spicfg3, uint32_t drvdel, uint32_t capdel, uint32_t spisctrl, uint32_t nvsclk, uint32_t nvssb, uint32_t nvmosi, uint32_t nvmiso, uint32_t diclk0, uint32_t difs0, uint32_t icn, uint32_t icp, uint32_t debugp, uint32_t debugn, uint32_t isfsp, uint32_t isfsn, uint32_t zifiout, uint32_t zifclk, uint32_t zifqout, uint32_t ziffs)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_LOAD_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_LOAD_CONFIG;

	SI479XX_CMD_LOAD_CONFIG_ARG_NUMCHIPS_write_u8(numchips);
	SI479XX_CMD_LOAD_CONFIG_ARG_CHIPID_write_u8(chipid);
	SI479XX_CMD_LOAD_CONFIG_ARG_ROLE_write_u8(role);
	SI479XX_CMD_LOAD_CONFIG_ARG_MOSI_IN_write_field(mosi_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_MISO_IN_write_field(miso_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_INTB_IN_write_field(intb_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVSCLK_IN_write_field(nvsclk_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVSSB_IN_write_field(nvssb_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVMOSI_IN_write_field(nvmosi_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVMISO_IN_write_field(nvmiso_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_RSVD_IN_write_field(rsvd_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQFSP_IN_write_field(iqfsp_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQFSN_IN_write_field(iqfsn_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_IOUTP_IN_write_field(ioutp_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_IOUTN_IN_write_field(ioutn_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_QOUTP_IN_write_field(qoutp_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_QOUTN_IN_write_field(qoutn_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_SSB_IN_write_field(ssb_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_SCLK_IN_write_field(sclk_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DOUT0_IN_write_field(dout0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DCLK0_IN_write_field(dclk0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DFS0_IN_write_field(dfs0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DOUT1_IN_write_field(dout1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DCLK1_IN_write_field(dclk1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DFS1_IN_write_field(dfs1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQCLKP_IN_write_field(iqclkp_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQCLKN_IN_write_field(iqclkn_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK2_IN_write_field(diclk2_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS2_IN_write_field(difs2_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN3_IN_write_field(din3_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK3_IN_write_field(diclk3_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS3_IN_write_field(difs3_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN4_IN_write_field(din4_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK4_IN_write_field(diclk4_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS4_IN_write_field(difs4_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN0_IN_write_field(din0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_GP0_IN_write_field(gp0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_GP1_IN_write_field(gp1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_GP2_IN_write_field(gp2_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN1_IN_write_field(din1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK1_IN_write_field(diclk1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS1_IN_write_field(difs1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN2_IN_write_field(din2_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_AOUT0_IN_write_field(aout0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_AOUT1_IN_write_field(aout1_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_ICP_IN_write_field(icp_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_ICN_IN_write_field(icn_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS0_IN_write_field(difs0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK0_IN_write_field(diclk0_in);
	SI479XX_CMD_LOAD_CONFIG_ARG_MOSI_OUT_write_field(mosi_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_MISO_OUT_write_field(miso_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_INTB_OUT_write_field(intb_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVSCLK_OUT_write_field(nvsclk_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVSSB_OUT_write_field(nvssb_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVMOSI_OUT_write_field(nvmosi_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVMISO_OUT_write_field(nvmiso_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_RSVD_OUT_write_field(rsvd_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQFSP_OUT_write_field(iqfsp_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQFSN_OUT_write_field(iqfsn_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_IOUTP_OUT_write_field(ioutp_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_IOUTN_OUT_write_field(ioutn_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_QOUTP_OUT_write_field(qoutp_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_QOUTN_OUT_write_field(qoutn_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_SSB_OUT_write_field(ssb_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_SCLK_OUT_write_field(sclk_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DOUT0_OUT_write_field(dout0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DCLK0_OUT_write_field(dclk0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DFS0_OUT_write_field(dfs0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DOUT1_OUT_write_field(dout1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DCLK1_OUT_write_field(dclk1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DFS1_OUT_write_field(dfs1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQCLKP_OUT_write_field(iqclkp_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_IQCLKN_OUT_write_field(iqclkn_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK2_OUT_write_field(diclk2_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS2_OUT_write_field(difs2_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN3_OUT_write_field(din3_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK3_OUT_write_field(diclk3_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS3_OUT_write_field(difs3_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN4_OUT_write_field(din4_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK4_OUT_write_field(diclk4_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS4_OUT_write_field(difs4_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN0_OUT_write_field(din0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_GP0_OUT_write_field(gp0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_GP1_OUT_write_field(gp1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_GP2_OUT_write_field(gp2_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN1_OUT_write_field(din1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK1_OUT_write_field(diclk1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS1_OUT_write_field(difs1_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIN2_OUT_write_field(din2_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_ICP_OUT_write_field(icp_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_ICN_OUT_write_field(icn_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS0_OUT_write_field(difs0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK0_OUT_write_field(diclk0_out);
	SI479XX_CMD_LOAD_CONFIG_ARG_CLKSEL8_write_u32(clksel8);
	SI479XX_CMD_LOAD_CONFIG_ARG_SPICFG3_write_u32(spicfg3);
	SI479XX_CMD_LOAD_CONFIG_ARG_DRVDEL_write_u32(drvdel);
	SI479XX_CMD_LOAD_CONFIG_ARG_CAPDEL_write_u32(capdel);
	SI479XX_CMD_LOAD_CONFIG_ARG_SPISCTRL_write_u32(spisctrl);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVSCLK_write_u32(nvsclk);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVSSB_write_u32(nvssb);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVMOSI_write_u32(nvmosi);
	SI479XX_CMD_LOAD_CONFIG_ARG_NVMISO_write_u32(nvmiso);
	SI479XX_CMD_LOAD_CONFIG_ARG_DICLK0_write_u32(diclk0);
	SI479XX_CMD_LOAD_CONFIG_ARG_DIFS0_write_u32(difs0);
	SI479XX_CMD_LOAD_CONFIG_ARG_ICN_write_u32(icn);
	SI479XX_CMD_LOAD_CONFIG_ARG_ICP_write_u32(icp);
	SI479XX_CMD_LOAD_CONFIG_ARG_DEBUGP_write_u32(debugp);
	SI479XX_CMD_LOAD_CONFIG_ARG_DEBUGN_write_u32(debugn);
	SI479XX_CMD_LOAD_CONFIG_ARG_ISFSP_write_u32(isfsp);
	SI479XX_CMD_LOAD_CONFIG_ARG_ISFSN_write_u32(isfsn);
	SI479XX_CMD_LOAD_CONFIG_ARG_ZIFIOUT_write_u32(zifiout);
	SI479XX_CMD_LOAD_CONFIG_ARG_ZIFCLK_write_u32(zifclk);
	SI479XX_CMD_LOAD_CONFIG_ARG_ZIFQOUT_write_u32(zifqout);
	SI479XX_CMD_LOAD_CONFIG_ARG_ZIFFS_write_u32(ziffs);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_LOAD_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_load_config(uint8_t icNum, uint8_t  numchips, uint8_t  chipid, uint8_t  role, uint8_t  mosi_in, uint8_t  miso_in, uint8_t  intb_in, uint8_t  nvsclk_in, uint8_t  nvssb_in, uint8_t  nvmosi_in, uint8_t  nvmiso_in, uint8_t  rsvd_in, uint8_t  iqfsp_in, uint8_t  iqfsn_in, uint8_t  ioutp_in, uint8_t  ioutn_in, uint8_t  qoutp_in, uint8_t  qoutn_in, uint8_t  ssb_in, uint8_t  sclk_in, uint8_t  dout0_in, uint8_t  dclk0_in, uint8_t  dfs0_in, uint8_t  dout1_in, uint8_t  dclk1_in, uint8_t  dfs1_in, uint8_t  iqclkp_in, uint8_t  iqclkn_in, uint8_t  diclk2_in, uint8_t  difs2_in, uint8_t  din3_in, uint8_t  diclk3_in, uint8_t  difs3_in, uint8_t  din4_in, uint8_t  diclk4_in, uint8_t  difs4_in, uint8_t  din0_in, uint8_t  gp0_in, uint8_t  gp1_in, uint8_t  gp2_in, uint8_t  din1_in, uint8_t  diclk1_in, uint8_t  difs1_in, uint8_t  din2_in, uint8_t  aout0_in, uint8_t  aout1_in, uint8_t  icp_in, uint8_t  icn_in, uint8_t  difs0_in, uint8_t  diclk0_in, uint8_t  mosi_out, uint8_t  miso_out, uint8_t  intb_out, uint8_t  nvsclk_out, uint8_t  nvssb_out, uint8_t  nvmosi_out, uint8_t  nvmiso_out, uint8_t  rsvd_out, uint8_t  iqfsp_out, uint8_t  iqfsn_out, uint8_t  ioutp_out, uint8_t  ioutn_out, uint8_t  qoutp_out, uint8_t  qoutn_out, uint8_t  ssb_out, uint8_t  sclk_out, uint8_t  dout0_out, uint8_t  dclk0_out, uint8_t  dfs0_out, uint8_t  dout1_out, uint8_t  dclk1_out, uint8_t  dfs1_out, uint8_t  iqclkp_out, uint8_t  iqclkn_out, uint8_t  diclk2_out, uint8_t  difs2_out, uint8_t  din3_out, uint8_t  diclk3_out, uint8_t  difs3_out, uint8_t  din4_out, uint8_t  diclk4_out, uint8_t  difs4_out, uint8_t  din0_out, uint8_t  gp0_out, uint8_t  gp1_out, uint8_t  gp2_out, uint8_t  din1_out, uint8_t  diclk1_out, uint8_t  difs1_out, uint8_t  din2_out, uint8_t  icp_out, uint8_t  icn_out, uint8_t  difs0_out, uint8_t  diclk0_out, uint32_t  clksel8, uint32_t  spicfg3, uint32_t  drvdel, uint32_t  capdel, uint32_t  spisctrl, uint32_t  nvsclk, uint32_t  nvssb, uint32_t  nvmosi, uint32_t  nvmiso, uint32_t  diclk0, uint32_t  difs0, uint32_t  icn, uint32_t  icp, uint32_t  debugp, uint32_t  debugn, uint32_t  isfsp, uint32_t  isfsn, uint32_t  zifiout, uint32_t  zifclk, uint32_t  zifqout, uint32_t  ziffs)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_load_config__command(icNum, numchips, chipid, role, mosi_in, miso_in, intb_in, nvsclk_in, nvssb_in, nvmosi_in, nvmiso_in, rsvd_in, iqfsp_in, iqfsn_in, ioutp_in, ioutn_in, qoutp_in, qoutn_in, ssb_in, sclk_in, dout0_in, dclk0_in, dfs0_in, dout1_in, dclk1_in, dfs1_in, iqclkp_in, iqclkn_in, diclk2_in, difs2_in, din3_in, diclk3_in, difs3_in, din4_in, diclk4_in, difs4_in, din0_in, gp0_in, gp1_in, gp2_in, din1_in, diclk1_in, difs1_in, din2_in, aout0_in, aout1_in, icp_in, icn_in, difs0_in, diclk0_in, mosi_out, miso_out, intb_out, nvsclk_out, nvssb_out, nvmosi_out, nvmiso_out, rsvd_out, iqfsp_out, iqfsn_out, ioutp_out, ioutn_out, qoutp_out, qoutn_out, ssb_out, sclk_out, dout0_out, dclk0_out, dfs0_out, dout1_out, dclk1_out, dfs1_out, iqclkp_out, iqclkn_out, diclk2_out, difs2_out, din3_out, diclk3_out, difs3_out, din4_out, diclk4_out, difs4_out, din0_out, gp0_out, gp1_out, gp2_out, din1_out, diclk1_out, difs1_out, din2_out, icp_out, icn_out, difs0_out, diclk0_out, clksel8, spicfg3, drvdel, capdel, spisctrl, nvsclk, nvssb, nvmosi, nvmiso, diclk0, difs0, icn, icp, debugp, debugn, isfsp, isfsn, zifiout, zifclk, zifqout, ziffs);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_get_chip_info__command(uint8_t icNum,uint8_t info_type)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_GET_CHIP_INFO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_GET_CHIP_INFO;

	SI479XX_CMD_GET_CHIP_INFO_ARG_INFO_TYPE_write_u8(info_type);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_GET_CHIP_INFO;
	}

	return ret;

}

RETURN_CODE SI479XX_get_chip_info__reply(uint8_t icNum, SI479XX_get_chip_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->DATA = NULL;
	replyData->LENGTH = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_GET_CHIP_INFO) {
		rdrep_len = SI479XX_CMD_GET_CHIP_INFO_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->LENGTH = SI479XX_CMD_GET_CHIP_INFO_REP_LENGTH_value;

			calculated_response_length = SI479XX_CMD_GET_CHIP_INFO_REP_LENGTH + (replyData->LENGTH * sizeof(uint8_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint8_t* data = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->LENGTH;
				replyData->DATA = (uint8_t*)malloc(sizeof(uint8_t) * array_elem_count);
				if (replyData->DATA == NULL) {
					return MEMORY_ERROR;
				}
				data = replyData->DATA;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_GET_CHIP_INFO_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_get_chip_info(uint8_t icNum, uint8_t  info_type, SI479XX_get_chip_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_get_chip_info__command(icNum, info_type);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_get_chip_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_key_exchange__command(uint8_t icNum,uint8_t* data, uint16_t data_len)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_KEY_EXCHANGE_LENGTH + data_len;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_KEY_EXCHANGE;

	for(n=0;n < data_len;n++) {
		SI479XX_CMD_KEY_EXCHANGE_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_KEY_EXCHANGE;
	}

	return ret;

}


RETURN_CODE SI479XX_key_exchange(uint8_t icNum, uint8_t*  data, uint16_t  data_len)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_key_exchange__command(icNum, data, data_len);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_get_func_info__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_GET_FUNC_INFO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_GET_FUNC_INFO;

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_GET_FUNC_INFO;
	}

	return ret;

}

RETURN_CODE SI479XX_get_func_info__reply(uint8_t icNum, SI479XX_get_func_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_GET_FUNC_INFO) {
		rdrep_len = SI479XX_CMD_GET_FUNC_INFO_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->VERINFO0 = SI479XX_CMD_GET_FUNC_INFO_REP_VERINFO0_value;
			replyData->VERINFO1 = SI479XX_CMD_GET_FUNC_INFO_REP_VERINFO1_value;
			replyData->VERINFO2 = SI479XX_CMD_GET_FUNC_INFO_REP_VERINFO2_value;
			replyData->VERINFO3 = SI479XX_CMD_GET_FUNC_INFO_REP_VERINFO3_value;
			replyData->VERINFO4 = SI479XX_CMD_GET_FUNC_INFO_REP_VERINFO4_value;
			replyData->HIFIINFO0 = SI479XX_CMD_GET_FUNC_INFO_REP_HIFIINFO0_value;
			replyData->HIFIINFO1 = SI479XX_CMD_GET_FUNC_INFO_REP_HIFIINFO1_value;
			replyData->HIFIINFO2 = SI479XX_CMD_GET_FUNC_INFO_REP_HIFIINFO2_value;
			replyData->HIFIINFO3 = SI479XX_CMD_GET_FUNC_INFO_REP_HIFIINFO3_value;
			replyData->CUSTINFO0 = SI479XX_CMD_GET_FUNC_INFO_REP_CUSTINFO0_value;
			replyData->CUSTINFO1 = SI479XX_CMD_GET_FUNC_INFO_REP_CUSTINFO1_value;
			replyData->CUSTINFO2 = SI479XX_CMD_GET_FUNC_INFO_REP_CUSTINFO2_value;
			replyData->CUSTINFO3 = SI479XX_CMD_GET_FUNC_INFO_REP_CUSTINFO3_value;
			replyData->NOSVN = SI479XX_CMD_GET_FUNC_INFO_REP_NOSVN_value;
			replyData->LOCATION = SI479XX_CMD_GET_FUNC_INFO_REP_LOCATION_value;
			replyData->MIXEDREV = SI479XX_CMD_GET_FUNC_INFO_REP_MIXEDREV_value;
			replyData->LOCALMOD = SI479XX_CMD_GET_FUNC_INFO_REP_LOCALMOD_value;
			replyData->SVNID = SI479XX_CMD_GET_FUNC_INFO_REP_SVNID_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_get_func_info(uint8_t icNum, SI479XX_get_func_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_get_func_info__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_get_func_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_set_property__command(uint8_t icNum,uint16_t prop, uint16_t data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_SET_PROPERTY_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_SET_PROPERTY;

	SI479XX_CMD_SET_PROPERTY_ARG_PROP_write_u16(prop);
	SI479XX_CMD_SET_PROPERTY_ARG_DATA_write_u16(data);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_SET_PROPERTY;
	}

	return ret;

}


RETURN_CODE SI479XX_set_property(uint8_t icNum, uint16_t  prop, uint16_t  data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_set_property__command(icNum, prop, data);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_get_property__command(uint8_t icNum,uint8_t count, uint16_t prop)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_GET_PROPERTY_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_GET_PROPERTY;

	SI479XX_CMD_GET_PROPERTY_ARG_COUNT_write_u8(count);
	SI479XX_CMD_GET_PROPERTY_ARG_PROP_write_u16(prop);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_GET_PROPERTY;
	}

	return ret;

}

RETURN_CODE SI479XX_get_property__reply(uint8_t icNum, uint16_t* data, uint16_t count)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_GET_PROPERTY) {
		rdrep_len = SI479XX_CMD_GET_PROPERTY_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_CMD_GET_PROPERTY_REP_LENGTH + (count * sizeof(uint16_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = count;
				for (n = 0; n < array_elem_count; n++) {
					data[n] = SI479XX_CMD_GET_PROPERTY_REP_DATA_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_get_property(uint8_t icNum, uint8_t  arg_count, uint16_t  prop, uint16_t*  data, uint16_t  rep_count)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_get_property__command(icNum, arg_count, prop);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (data != NULL) {
		r |= SI479XX_get_property__reply(icNum, data, rep_count);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_agc_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_AGC_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_AGC_STATUS;

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_AGC_STATUS;
	}

	return ret;

}

RETURN_CODE SI479XX_agc_status__reply(uint8_t icNum, SI479XX_agc_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_AGC_STATUS) {
		rdrep_len = SI479XX_CMD_AGC_STATUS_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->TM_AM_MS = SI479XX_CMD_AGC_STATUS_REP_TM_AM_MS_value;
			replyData->TM_B3_RF = SI479XX_CMD_AGC_STATUS_REP_TM_B3_RF_value;
			replyData->TM_FM_RF = SI479XX_CMD_AGC_STATUS_REP_TM_FM_RF_value;
			replyData->TM_AM_RF = SI479XX_CMD_AGC_STATUS_REP_TM_AM_RF_value;
			replyData->TM_IF = SI479XX_CMD_AGC_STATUS_REP_TM_IF_value;
			replyData->AM_RF_INDEX = SI479XX_CMD_AGC_STATUS_REP_AM_RF_INDEX_value;
			replyData->AM_RF_GAIN = SI479XX_CMD_AGC_STATUS_REP_AM_RF_GAIN_value;
			replyData->FM_RF_INDEX = SI479XX_CMD_AGC_STATUS_REP_FM_RF_INDEX_value;
			replyData->FM_RF_GAIN = SI479XX_CMD_AGC_STATUS_REP_FM_RF_GAIN_value;
			replyData->B3_RF_INDEX = SI479XX_CMD_AGC_STATUS_REP_B3_RF_INDEX_value;
			replyData->B3_RF_GAIN = SI479XX_CMD_AGC_STATUS_REP_B3_RF_GAIN_value;
			replyData->AM_MS_INDEX = SI479XX_CMD_AGC_STATUS_REP_AM_MS_INDEX_value;
			replyData->AM_MS_GAIN = SI479XX_CMD_AGC_STATUS_REP_AM_MS_GAIN_value;
			replyData->IF_INDEX = SI479XX_CMD_AGC_STATUS_REP_IF_INDEX_value;
			replyData->IF_GAIN = SI479XX_CMD_AGC_STATUS_REP_IF_GAIN_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_agc_status(uint8_t icNum, SI479XX_agc_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_agc_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_agc_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_detail_status__command(uint8_t icNum,uint8_t subcmd, uint8_t* data, uint16_t data_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DETAIL_STATUS_LENGTH + data_length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DETAIL_STATUS;

	SI479XX_CMD_DETAIL_STATUS_ARG_SUBCMD_write_u8(subcmd);
	for(n=0;n < data_length;n++) {
		SI479XX_CMD_DETAIL_STATUS_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DETAIL_STATUS;
	}

	return ret;

}


RETURN_CODE SI479XX_detail_status__reply(uint8_t icNum, uint8_t* data)
{
    RETURN_CODE ret = 0;
    uint8_t icIndex = 0;
    uint32_t rdrep_len = 0;
    uint32_t calculated_response_length;

    icIndex = getCommandIndex(icNum);
    if(icIndex >= MAX_NUM_PARTS) {
        return INVALID_INPUT;
    }
    if(data == NULL) {
        return INVALID_INPUT;
    }
    if(lastCommands[icIndex] == SI479XX_CMD_DETAIL_STATUS)
    {
        rdrep_len = SI479XX_CMD_DETAIL_STATUS_REP_LENGTH;
        if(rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
            return WRAPPER_ERROR;
        }
        ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
        if(ret == SUCCESS)
        {

            calculated_response_length = SI479XX_CMD_DETAIL_STATUS_REP_LENGTH;
            if(calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
                return WRAPPER_ERROR;
            }
            ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
            if(ret != SUCCESS)
            {
                return ret;
            }

            {
                int32_t n = 0;
                int32_t array_elem_count = 0;

                array_elem_count = 8;
                for(n = 0; n < array_elem_count; n++) {
                    data[n] = SI479XX_CMD_DETAIL_STATUS_REP_DATA_N_value(n);
 
                }

            }
        }
    }
    else
    {
        ret = COMMAND_ERROR;
    }
    return ret;
}

RETURN_CODE SI479XX_detail_status(uint8_t icNum, uint8_t  subcmd, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_detail_status__command(icNum, subcmd, arg_data, data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (rep_data != NULL) {
		r |= SI479XX_detail_status__reply(icNum, rep_data);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_jtag_unlock__command(uint8_t icNum,uint8_t jtag_sel, uint8_t* image_id, uint16_t image_id_len)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_JTAG_UNLOCK_LENGTH + image_id_len;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_JTAG_UNLOCK;

	SI479XX_CMD_HIFI_JTAG_UNLOCK_ARG_JTAG_SEL_write_field(jtag_sel);
	for(n=0;n < image_id_len;n++) {
		SI479XX_CMD_HIFI_JTAG_UNLOCK_ARG_IMAGE_ID_N_write_u8(n,image_id[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_JTAG_UNLOCK;
	}

	return ret;

}


RETURN_CODE SI479XX_hifi_jtag_unlock(uint8_t icNum, uint8_t  jtag_sel, uint8_t*  image_id, uint16_t  image_id_len)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_jtag_unlock__command(icNum, jtag_sel, image_id, image_id_len);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_hifi_pipe_reset__command(uint8_t icNum,uint8_t reset)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_HIFI_PIPE_RESET_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_HIFI_PIPE_RESET;

	SI479XX_CMD_HIFI_PIPE_RESET_ARG_RESET_write_field(reset);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_HIFI_PIPE_RESET;
	}

	return ret;

}


RETURN_CODE SI479XX_hifi_pipe_reset(uint8_t icNum, uint8_t  reset)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_hifi_pipe_reset__command(icNum, reset);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_get_api__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_GET_API_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_GET_API;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_GET_API;
	}

	return ret;

}

RETURN_CODE SI479XX_get_api__reply(uint8_t icNum, SI479XX_get_api__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_GET_API) {
		rdrep_len = SI479XX_CMD_GET_API_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->APIVERSION0 = SI479XX_CMD_GET_API_REP_APIVERSION0_value;
			replyData->APIVERSION1 = SI479XX_CMD_GET_API_REP_APIVERSION1_value;
			replyData->APIVERSION2 = SI479XX_CMD_GET_API_REP_APIVERSION2_value;
			replyData->APIVERSION3 = SI479XX_CMD_GET_API_REP_APIVERSION3_value;
			replyData->DYNAMIC_ZIF = SI479XX_CMD_GET_API_REP_DYNAMIC_ZIF_value;
			replyData->ADVANCED_TUNER = SI479XX_CMD_GET_API_REP_ADVANCED_TUNER_value;
			replyData->ADVANCED_AUDIO = SI479XX_CMD_GET_API_REP_ADVANCED_AUDIO_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_get_api(uint8_t icNum, SI479XX_get_api__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_get_api__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_get_api__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_tuner__command(uint8_t icNum,uint8_t id, uint8_t* data, uint16_t data_length)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_TUNER_LENGTH + data_length;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_TUNER;

	SI479XX_CMD_TUNER_ARG_ID_write_u8(id);
	for(n=0;n < data_length;n++) {
		SI479XX_CMD_TUNER_ARG_DATA_N_write_u8(n,data[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_TUNER;
	}

	return ret;

}

RETURN_CODE SI479XX_tuner__reply(uint8_t icNum, uint8_t* data)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (data == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_TUNER) {
		rdrep_len = SI479XX_CMD_TUNER_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_tuner(uint8_t icNum, uint8_t  id, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_tuner__command(icNum, id, arg_data, data_length);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (rep_data != NULL) {
		r |= SI479XX_tuner__reply(icNum, rep_data);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_dab_set_freq_list__command(uint8_t icNum,uint8_t num_freqs, uint32_t* freq)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DAB_SET_FREQ_LIST_LENGTH + (num_freqs * sizeof(uint32_t));
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DAB_SET_FREQ_LIST;

	SI479XX_CMD_DAB_SET_FREQ_LIST_ARG_NUM_FREQS_write_u8(num_freqs);
	for(n=0;n < num_freqs;n++) {
		SI479XX_CMD_DAB_SET_FREQ_LIST_ARG_FREQ_N_write_u32(n,freq[n]);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DAB_SET_FREQ_LIST;
	}

	return ret;

}


RETURN_CODE SI479XX_dab_set_freq_list(uint8_t icNum, uint8_t  num_freqs, uint32_t*  freq)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_dab_set_freq_list__command(icNum, num_freqs, freq);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_dab_get_freq_list__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DAB_GET_FREQ_LIST_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DAB_GET_FREQ_LIST;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DAB_GET_FREQ_LIST;
	}

	return ret;

}

RETURN_CODE SI479XX_dab_get_freq_list__reply(uint8_t icNum, SI479XX_dab_get_freq_list__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->FREQ = NULL;
	replyData->NUM_FREQS = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_DAB_GET_FREQ_LIST) {
		rdrep_len = SI479XX_CMD_DAB_GET_FREQ_LIST_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->NUM_FREQS = SI479XX_CMD_DAB_GET_FREQ_LIST_REP_NUM_FREQS_value;

			calculated_response_length = SI479XX_CMD_DAB_GET_FREQ_LIST_REP_LENGTH + (replyData->NUM_FREQS * sizeof(uint32_t));
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				uint32_t* freq = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->NUM_FREQS;
				replyData->FREQ = (uint32_t*)malloc(sizeof(uint32_t) * array_elem_count);
				if (replyData->FREQ == NULL) {
					return MEMORY_ERROR;
				}
				freq = replyData->FREQ;
				for (n = 0; n < array_elem_count; n++) {
					freq[n] = SI479XX_CMD_DAB_GET_FREQ_LIST_REP_FREQ_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_dab_get_freq_list(uint8_t icNum, SI479XX_dab_get_freq_list__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_dab_get_freq_list__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_dab_get_freq_list__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_pin_status__command(uint8_t icNum,uint8_t group)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_PIN_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_PIN_STATUS;

	SI479XX_CMD_PIN_STATUS_ARG_GROUP_write_field(group);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_PIN_STATUS;
	}

	return ret;

}

RETURN_CODE SI479XX_pin_status__reply(uint8_t icNum, SI479XX_pin_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	replyData->PIN = NULL;
	replyData->NUMPINS = 0;
	if (lastCommands[icIndex] == SI479XX_CMD_PIN_STATUS) {
		rdrep_len = SI479XX_CMD_PIN_STATUS_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->NUMPINS = SI479XX_CMD_PIN_STATUS_REP_NUMPINS_value;

			calculated_response_length = SI479XX_CMD_PIN_STATUS_REP_LENGTH + (replyData->NUMPINS * SI479XX_CMD_PIN_STATUS_REP_PIN_SIZE);
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				SI479XX_pin_status_pin__data* pin = NULL;
				int32_t array_elem_count = 0;

				array_elem_count = replyData->NUMPINS;
				replyData->PIN = (SI479XX_pin_status_pin__data*)malloc(sizeof(SI479XX_pin_status_pin__data) * array_elem_count);
				if (replyData->PIN == NULL) {
					return MEMORY_ERROR;
				}
				pin = replyData->PIN;
				for (n = 0; n < array_elem_count; n++) {
					pin[n].DEGLITCH = SI479XX_CMD_PIN_STATUS_REP_PIN_DEGLITCH_N_value(n);
					pin[n].INVERT = SI479XX_CMD_PIN_STATUS_REP_PIN_INVERT_N_value(n);
					pin[n].INTERRUPT_TYPE = SI479XX_CMD_PIN_STATUS_REP_PIN_INTERRUPT_TYPE_N_value(n);
					pin[n].DIRECTION = SI479XX_CMD_PIN_STATUS_REP_PIN_DIRECTION_N_value(n);
					pin[n].GPI = SI479XX_CMD_PIN_STATUS_REP_PIN_GPI_N_value(n);
					pin[n].GPOUT = SI479XX_CMD_PIN_STATUS_REP_PIN_GPOUT_N_value(n);
					pin[n].SPDIFTX1 = SI479XX_CMD_PIN_STATUS_REP_PIN_SPDIFTX1_N_value(n);
					pin[n].SPDIFTX0 = SI479XX_CMD_PIN_STATUS_REP_PIN_SPDIFTX0_N_value(n);
					pin[n].I2STX4 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2STX4_N_value(n);
					pin[n].I2STX3 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2STX3_N_value(n);
					pin[n].I2STX2 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2STX2_N_value(n);
					pin[n].I2STX1 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2STX1_N_value(n);
					pin[n].I2STX0 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2STX0_N_value(n);
					pin[n].BLEND = SI479XX_CMD_PIN_STATUS_REP_PIN_BLEND_N_value(n);
					pin[n].SPDIFRX1 = SI479XX_CMD_PIN_STATUS_REP_PIN_SPDIFRX1_N_value(n);
					pin[n].SPDIFRX0 = SI479XX_CMD_PIN_STATUS_REP_PIN_SPDIFRX0_N_value(n);
					pin[n].I2SRX4 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2SRX4_N_value(n);
					pin[n].I2SRX3 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2SRX3_N_value(n);
					pin[n].I2SRX2 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2SRX2_N_value(n);
					pin[n].I2SRX1 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2SRX1_N_value(n);
					pin[n].I2SRX0 = SI479XX_CMD_PIN_STATUS_REP_PIN_I2SRX0_N_value(n);
					pin[n].GPIB = SI479XX_CMD_PIN_STATUS_REP_PIN_GPIB_N_value(n);
					pin[n].GPIA = SI479XX_CMD_PIN_STATUS_REP_PIN_GPIA_N_value(n);
					pin[n].CLIP1 = SI479XX_CMD_PIN_STATUS_REP_PIN_CLIP1_N_value(n);
					pin[n].CLIP0 = SI479XX_CMD_PIN_STATUS_REP_PIN_CLIP0_N_value(n);
					pin[n].ICIN3 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICIN3_N_value(n);
					pin[n].ICIN2 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICIN2_N_value(n);
					pin[n].ICIN1 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICIN1_N_value(n);
					pin[n].ICIN0 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICIN0_N_value(n);
					pin[n].I2SCKFS = SI479XX_CMD_PIN_STATUS_REP_PIN_I2SCKFS_N_value(n);
					pin[n].DAC = SI479XX_CMD_PIN_STATUS_REP_PIN_DAC_N_value(n);
					pin[n].ICOUT4 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICOUT4_N_value(n);
					pin[n].ICOUT3 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICOUT3_N_value(n);
					pin[n].ICOUT2 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICOUT2_N_value(n);
					pin[n].ICOUT1 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICOUT1_N_value(n);
					pin[n].ICOUT0 = SI479XX_CMD_PIN_STATUS_REP_PIN_ICOUT0_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_pin_status(uint8_t icNum, uint8_t  group, SI479XX_pin_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_pin_status__command(icNum, group);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_pin_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_pin_config_hifi__command(uint8_t icNum,uint8_t pin_id_0, uint8_t enable_0, uint8_t deglitch_0, uint8_t invert_0, uint8_t interrupt_type_0, uint8_t pin_id_1, uint8_t enable_1, uint8_t deglitch_1, uint8_t invert_1, uint8_t interrupt_type_1, uint8_t pin_id_a, uint8_t enable_a, uint8_t deglitch_a, uint8_t invert_a, uint8_t interrupt_type_a, uint8_t pin_id_b, uint8_t enable_b, uint8_t deglitch_b, uint8_t invert_b, uint8_t interrupt_type_b)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_PIN_CONFIG_HIFI_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_PIN_CONFIG_HIFI;

	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_0_write_u8(pin_id_0);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_0_write_field(enable_0);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_0_write_field(deglitch_0);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INVERT_0_write_field(invert_0);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_0_write_field(interrupt_type_0);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_1_write_u8(pin_id_1);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_1_write_field(enable_1);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_1_write_field(deglitch_1);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INVERT_1_write_field(invert_1);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_1_write_field(interrupt_type_1);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_A_write_u8(pin_id_a);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_A_write_field(enable_a);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_A_write_field(deglitch_a);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INVERT_A_write_field(invert_a);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_A_write_field(interrupt_type_a);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_PIN_ID_B_write_u8(pin_id_b);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_ENABLE_B_write_field(enable_b);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_DEGLITCH_B_write_field(deglitch_b);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INVERT_B_write_field(invert_b);
	SI479XX_CMD_PIN_CONFIG_HIFI_ARG_INTERRUPT_TYPE_B_write_field(interrupt_type_b);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_PIN_CONFIG_HIFI;
	}

	return ret;

}


RETURN_CODE SI479XX_pin_config_hifi(uint8_t icNum, uint8_t  pin_id_0, uint8_t  enable_0, uint8_t  deglitch_0, uint8_t  invert_0, uint8_t  interrupt_type_0, uint8_t  pin_id_1, uint8_t  enable_1, uint8_t  deglitch_1, uint8_t  invert_1, uint8_t  interrupt_type_1, uint8_t  pin_id_a, uint8_t  enable_a, uint8_t  deglitch_a, uint8_t  invert_a, uint8_t  interrupt_type_a, uint8_t  pin_id_b, uint8_t  enable_b, uint8_t  deglitch_b, uint8_t  invert_b, uint8_t  interrupt_type_b)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_pin_config_hifi__command(icNum, pin_id_0, enable_0, deglitch_0, invert_0, interrupt_type_0, pin_id_1, enable_1, deglitch_1, invert_1, interrupt_type_1, pin_id_a, enable_a, deglitch_a, invert_a, interrupt_type_a, pin_id_b, enable_b, deglitch_b, invert_b, interrupt_type_b);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_pin_config_gpo__command(uint8_t icNum,uint8_t numpins, SI479XX_pin_config_gpo_pin__data* pin)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;
	uint32_t n;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_PIN_CONFIG_GPO_LENGTH + (numpins * sizeof(SI479XX_pin_config_gpo_pin__data));
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_PIN_CONFIG_GPO;

	SI479XX_CMD_PIN_CONFIG_GPO_ARG_NUMPINS_write_u8(numpins);
	for(n=0;n < numpins;n++) {
		SI479XX_CMD_PIN_CONFIG_GPO_ARG_PIN_PIN_ID_N_write_u8(n,pin[n].PIN_ID);
		SI479XX_CMD_PIN_CONFIG_GPO_ARG_PIN_ENABLE_N_write_field(n,pin[n].ENABLE);
		SI479XX_CMD_PIN_CONFIG_GPO_ARG_PIN_DRIVE_N_write_field(n,pin[n].DRIVE);
	}

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_PIN_CONFIG_GPO;
	}

	return ret;

}


RETURN_CODE SI479XX_pin_config_gpo(uint8_t icNum, uint8_t  numpins, SI479XX_pin_config_gpo_pin__data*  pin)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_pin_config_gpo__command(icNum, numpins, pin);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2stx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2STX0_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2STX0_CONFIG;

	SI479XX_CMD_I2STX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2STX0_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2STX0_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2STX0_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2STX0_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2STX0_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2STX0_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2STX0_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2STX0_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2STX0_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2STX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2STX0_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2STX0_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2STX0_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2STX0_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2STX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2STX0_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2STX0_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2STX0_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2STX0_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum, cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2STX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2stx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2stx0_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2stx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2STX1_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2STX1_CONFIG;

	SI479XX_CMD_I2STX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2STX1_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2STX1_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2STX1_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2STX1_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2STX1_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2STX1_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2STX1_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2STX1_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2STX1_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2STX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2STX1_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2STX1_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2STX1_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2STX1_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2STX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2STX1_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2STX1_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2STX1_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2STX1_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2STX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2stx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2stx1_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2stx2_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2STX2_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2STX2_CONFIG;

	SI479XX_CMD_I2STX2_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2STX2_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2STX2_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2STX2_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2STX2_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2STX2_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2STX2_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2STX2_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2STX2_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2STX2_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2STX2_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2STX2_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2STX2_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2STX2_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2STX2_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2STX2_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2STX2_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2STX2_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2STX2_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2STX2_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2STX2_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2stx2_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2stx2_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2stx3_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2STX3_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2STX3_CONFIG;

	SI479XX_CMD_I2STX3_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2STX3_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2STX3_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2STX3_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2STX3_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2STX3_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2STX3_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2STX3_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2STX3_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2STX3_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2STX3_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2STX3_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2STX3_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2STX3_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2STX3_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2STX3_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2STX3_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2STX3_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2STX3_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2STX3_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2STX3_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2stx3_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2stx3_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2stx4_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t lvdsdata, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2STX4_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2STX4_CONFIG;

	SI479XX_CMD_I2STX4_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2STX4_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2STX4_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2STX4_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2STX4_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2STX4_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2STX4_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2STX4_CONFIG_ARG_LVDSDATA_write_field(lvdsdata);
	SI479XX_CMD_I2STX4_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2STX4_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2STX4_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2STX4_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2STX4_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2STX4_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2STX4_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2STX4_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2STX4_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2STX4_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2STX4_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2STX4_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2STX4_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2STX4_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2stx4_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  lvdsdata, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2stx4_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, lvdsdata, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2srx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2SRX0_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2SRX0_CONFIG;

	SI479XX_CMD_I2SRX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2SRX0_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2SRX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2srx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2srx0_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2srx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2SRX1_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2SRX1_CONFIG;

	SI479XX_CMD_I2SRX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2SRX1_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2SRX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2srx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2srx1_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2srx2_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2SRX2_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2SRX2_CONFIG;

	SI479XX_CMD_I2SRX2_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2SRX2_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2SRX2_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2srx2_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2srx2_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2srx3_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2SRX3_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2SRX3_CONFIG;

	SI479XX_CMD_I2SRX3_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2SRX3_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2SRX3_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2srx3_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2srx3_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2srx4_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2SRX4_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2SRX4_CONFIG;

	SI479XX_CMD_I2SRX4_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_WHITENCONTROL_write_field(whitencontrol);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_ENABLE_write_field(enable);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_PURPOSE_write_field(purpose);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_FORCE_write_field(force);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_MASTER_write_field(master);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_CLKFS_write_field(clkfs);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_DATA0_write_field(data0);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_FILL_write_field(fill);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_SLOT_SIZE_write_field(slot_size);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_BITORDER_write_field(bitorder);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_SWAP_write_field(swap);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_CLKINV_write_field(clkinv);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_FRAMING_MODE_write_field(framing_mode);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_INVERTFS_write_field(invertfs);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_HIZ_write_field(hiz);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_LENGTH_write_field(length);
	SI479XX_CMD_I2SRX4_CONFIG_ARG_DATA1_write_u8(data1);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2SRX4_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_i2srx4_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2srx4_config__command(icNum, sample_rate, whitencontrol, enable, purpose, force, master, clkfs, data0, fill, slot_size, sample_size, bitorder, swap, clkinv, framing_mode, clockdomain, invertfs, hiz, length, data1);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_i2s_control__command(uint8_t icNum,uint8_t port, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_I2S_CONTROL_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_I2S_CONTROL;

	SI479XX_CMD_I2S_CONTROL_ARG_PORT_write_field(port);
	SI479XX_CMD_I2S_CONTROL_ARG_MODE_write_field(mode);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum, cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_I2S_CONTROL;
	}

	return ret;

}

RETURN_CODE SI479XX_i2s_control__reply(uint8_t icNum, SI479XX_i2s_control__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_I2S_CONTROL) {
		rdrep_len = SI479XX_CMD_I2S_CONTROL_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->SAMPLE_RATE = SI479XX_CMD_I2S_CONTROL_REP_SAMPLE_RATE_value;
			replyData->WHITENCONTROL = SI479XX_CMD_I2S_CONTROL_REP_WHITENCONTROL_value;
			replyData->PURPOSE = SI479XX_CMD_I2S_CONTROL_REP_PURPOSE_value;
			replyData->FORCE = SI479XX_CMD_I2S_CONTROL_REP_FORCE_value;
			replyData->MASTER = SI479XX_CMD_I2S_CONTROL_REP_MASTER_value;
			replyData->CLKFS = SI479XX_CMD_I2S_CONTROL_REP_CLKFS_value;
			replyData->LVDSDATA = SI479XX_CMD_I2S_CONTROL_REP_LVDSDATA_value;
			replyData->DATA0 = SI479XX_CMD_I2S_CONTROL_REP_DATA0_value;
			replyData->FILL = SI479XX_CMD_I2S_CONTROL_REP_FILL_value;
			replyData->SLOT_SIZE = SI479XX_CMD_I2S_CONTROL_REP_SLOT_SIZE_value;
			replyData->SAMPLE_SIZE = SI479XX_CMD_I2S_CONTROL_REP_SAMPLE_SIZE_value;
			replyData->BITORDER = SI479XX_CMD_I2S_CONTROL_REP_BITORDER_value;
			replyData->SWAP = SI479XX_CMD_I2S_CONTROL_REP_SWAP_value;
			replyData->CLKINV = SI479XX_CMD_I2S_CONTROL_REP_CLKINV_value;
			replyData->FRAMING_MODE = SI479XX_CMD_I2S_CONTROL_REP_FRAMING_MODE_value;
			replyData->CLOCKDOMAIN = SI479XX_CMD_I2S_CONTROL_REP_CLOCKDOMAIN_value;
			replyData->INVERTFS = SI479XX_CMD_I2S_CONTROL_REP_INVERTFS_value;
			replyData->MODE = SI479XX_CMD_I2S_CONTROL_REP_MODE_value;
			replyData->HIZ = SI479XX_CMD_I2S_CONTROL_REP_HIZ_value;
			replyData->LENGTH = SI479XX_CMD_I2S_CONTROL_REP_LENGTH_value;
			replyData->DATA1 = SI479XX_CMD_I2S_CONTROL_REP_DATA1_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_i2s_control(uint8_t icNum, uint8_t  port, uint8_t  mode, SI479XX_i2s_control__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_i2s_control__command(icNum, port, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_i2s_control__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_spdiftx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_SPDIFTX0_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_SPDIFTX0_CONFIG;

	SI479XX_CMD_SPDIFTX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_SPDIFTX0_CONFIG_ARG_PIN_write_u8(pin);
	SI479XX_CMD_SPDIFTX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_SPDIFTX0_CONFIG_ARG_PARITY_write_field(parity);
	SI479XX_CMD_SPDIFTX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_SPDIFTX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_spdiftx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_spdiftx0_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_spdiftx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_SPDIFTX1_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_SPDIFTX1_CONFIG;

	SI479XX_CMD_SPDIFTX1_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_SPDIFTX1_CONFIG_ARG_PIN_write_u8(pin);
	SI479XX_CMD_SPDIFTX1_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_SPDIFTX1_CONFIG_ARG_PARITY_write_field(parity);
	SI479XX_CMD_SPDIFTX1_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_SPDIFTX1_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_spdiftx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_spdiftx1_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_spdifrx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size, uint8_t parity_err, uint8_t valid_err)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_SPDIFRX0_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_SPDIFRX0_CONFIG;

	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_SAMPLE_RATE_write_u8(sample_rate);
	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_PIN_write_u8(pin);
	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_CLOCKDOMAIN_write_field(clockdomain);
	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_PARITY_write_field(parity);
	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_SAMPLE_SIZE_write_field(sample_size);
	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_PARITY_ERR_write_field(parity_err);
	SI479XX_CMD_SPDIFRX0_CONFIG_ARG_VALID_ERR_write_field(valid_err);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_SPDIFRX0_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_spdifrx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size, uint8_t  parity_err, uint8_t  valid_err)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_spdifrx0_config__command(icNum, sample_rate, pin, clockdomain, parity, sample_size, parity_err, valid_err);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_spdif_control__command(uint8_t icNum,uint8_t port, uint8_t mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_SPDIF_CONTROL_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_SPDIF_CONTROL;

	SI479XX_CMD_SPDIF_CONTROL_ARG_PORT_write_field(port);
	SI479XX_CMD_SPDIF_CONTROL_ARG_MODE_write_field(mode);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_SPDIF_CONTROL;
	}

	return ret;

}

RETURN_CODE SI479XX_spdif_control__reply(uint8_t icNum, SI479XX_spdif_control__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_SPDIF_CONTROL) {
		rdrep_len = SI479XX_CMD_SPDIF_CONTROL_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->SAMPLE_RATE = SI479XX_CMD_SPDIF_CONTROL_REP_SAMPLE_RATE_value;
			replyData->PIN = SI479XX_CMD_SPDIF_CONTROL_REP_PIN_value;
			replyData->CLOCKDOMAIN = SI479XX_CMD_SPDIF_CONTROL_REP_CLOCKDOMAIN_value;
			replyData->PARITY = SI479XX_CMD_SPDIF_CONTROL_REP_PARITY_value;
			replyData->SAMPLE_SIZE = SI479XX_CMD_SPDIF_CONTROL_REP_SAMPLE_SIZE_value;
			replyData->PARITY_ERR = SI479XX_CMD_SPDIF_CONTROL_REP_PARITY_ERR_value;
			replyData->VALID_ERR = SI479XX_CMD_SPDIF_CONTROL_REP_VALID_ERR_value;
			replyData->MODE = SI479XX_CMD_SPDIF_CONTROL_REP_MODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_spdif_control(uint8_t icNum, uint8_t  port, uint8_t  mode, SI479XX_spdif_control__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_spdif_control__command(icNum, port, mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_spdif_control__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_dac_config__command(uint8_t icNum,uint8_t pumode01, uint8_t dac01_mode, uint8_t pumode23, uint8_t dac23_mode, uint8_t pumode45, uint8_t dac45_mode)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DAC_CONFIG_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DAC_CONFIG;

	SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_write_field(pumode01);
	SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_write_field(dac01_mode);
	SI479XX_CMD_DAC_CONFIG_ARG_PUMODE23_write_field(pumode23);
	SI479XX_CMD_DAC_CONFIG_ARG_DAC23_MODE_write_field(dac23_mode);
	SI479XX_CMD_DAC_CONFIG_ARG_PUMODE45_write_field(pumode45);
	SI479XX_CMD_DAC_CONFIG_ARG_DAC45_MODE_write_field(dac45_mode);

#if (FEAT_TUN_DIGEN_SEQ_4_1)		/* 210518 SGsoft */	
	ret = DG_writeCommand(icNum,  cmd_len, cmd_arg);
#else
	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DAC_CONFIG;
	}

	return ret;

}


RETURN_CODE SI479XX_dac_config(uint8_t icNum, uint8_t  pumode01, uint8_t  dac01_mode, uint8_t  pumode23, uint8_t  dac23_mode, uint8_t  pumode45, uint8_t  dac45_mode)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_dac_config__command(icNum, pumode01, dac01_mode, pumode23, dac23_mode, pumode45, dac45_mode);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_dac_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_DAC_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_DAC_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_DAC_STATUS;
	}

	return ret;

}

RETURN_CODE SI479XX_dac_status__reply(uint8_t icNum, SI479XX_dac_status__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_DAC_STATUS) {
		rdrep_len = SI479XX_CMD_DAC_STATUS_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {
			replyData->DAC01_MODE = SI479XX_CMD_DAC_STATUS_REP_DAC01_MODE_value;
			replyData->DAC23_MODE = SI479XX_CMD_DAC_STATUS_REP_DAC23_MODE_value;
			replyData->DAC45_MODE = SI479XX_CMD_DAC_STATUS_REP_DAC45_MODE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_dac_status(uint8_t icNum, SI479XX_dac_status__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_dac_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_dac_status__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_adc_connect__command(uint8_t icNum,uint8_t adc_channel, uint8_t source_pins, uint8_t gain)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_ADC_CONNECT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_ADC_CONNECT;

	SI479XX_CMD_ADC_CONNECT_ARG_ADC_CHANNEL_write_field(adc_channel);
	SI479XX_CMD_ADC_CONNECT_ARG_SOURCE_PINS_write_field(source_pins);
	SI479XX_CMD_ADC_CONNECT_ARG_GAIN_write_field(gain);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_ADC_CONNECT;
	}

	return ret;

}


RETURN_CODE SI479XX_adc_connect(uint8_t icNum, uint8_t  adc_channel, uint8_t  source_pins, uint8_t  gain)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_adc_connect__command(icNum, adc_channel, source_pins, gain);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_adc_disconnect__command(uint8_t icNum,uint8_t adc_channel)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_ADC_DISCONNECT_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_ADC_DISCONNECT;

	SI479XX_CMD_ADC_DISCONNECT_ARG_ADC_CHANNEL_write_field(adc_channel);

	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_ADC_DISCONNECT;
	}

	return ret;

}


RETURN_CODE SI479XX_adc_disconnect(uint8_t icNum, uint8_t  adc_channel)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_adc_disconnect__command(icNum, adc_channel);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_adc_status__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_CMD_ADC_STATUS_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_CMD_ADC_STATUS;


	ret = iapis_writeCommand(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_CMD_ADC_STATUS;
	}

	return ret;

}

RETURN_CODE SI479XX_adc_status__reply(uint8_t icNum, SI479XX_adc_status_adc__data* adc)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;
	uint32_t calculated_response_length;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (adc == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_CMD_ADC_STATUS) {
		rdrep_len = SI479XX_CMD_ADC_STATUS_REP_LENGTH;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_buffer);
		if (ret == SUCCESS) {

			calculated_response_length = SI479XX_CMD_ADC_STATUS_REP_LENGTH;
			if (calculated_response_length > SI479XX_RPY_BUFFER_LENGTH) {
				return WRAPPER_ERROR;
			}
			ret = iapis_readReply(icNum, calculated_response_length, reply_buffer);
			if (ret != SUCCESS) {
				return ret;
			}

			{
				int32_t n = 0;
				int32_t array_elem_count = 0;

				array_elem_count = 5;
				for (n = 0; n < array_elem_count; n++) {
					adc[n].SOURCE_PINS = SI479XX_CMD_ADC_STATUS_REP_ADC_SOURCE_PINS_N_value(n);
					adc[n].GAIN = SI479XX_CMD_ADC_STATUS_REP_ADC_GAIN_N_value(n);
 
				}

			}
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_adc_status(uint8_t icNum, SI479XX_adc_status_adc__data*  adc)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_adc_status__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (adc != NULL) {
		r |= SI479XX_adc_status__reply(icNum, adc);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
