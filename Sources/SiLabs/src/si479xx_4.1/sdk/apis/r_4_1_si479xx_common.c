/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
 
#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_common_h_global_
	
#include "main.h"

//#include "si479xx_common.h"
//#include "si479xx_firmware_api.h"


// common_base end

RETURN_CODE SI479XX_FLASH_UTIL__FLASH_UTIL_SUB(uint8_t icNum, uint16_t length, uint8_t* subcmdbuf)
{
    //stripping off the 'subcmd' from the array
    return SI479XX_flash_load__command(icNum, subcmdbuf[0], (subcmdbuf + 1), (length - 1));
}

RETURN_CODE SI479XX_FLASH__FLASH_LOAD_SUB(uint8_t icNum, uint16_t length, uint8_t* subcmdbuf)
{
    //stripping off the 'subcmd' from the array
    return SI479XX_flash_load__command(icNum, subcmdbuf[0], (subcmdbuf + 1), (length - 1));
}


RETURN_CODE SI479XX_TUNER_TUNER(uint8_t icNum, uint8_t id, uint16_t length, uint8_t* subcmd)
{
	return SI479XX_tuner__command(icNum, id, subcmd, length);
}


RETURN_CODE SI479XX_DETAIL_STATUS_DETAIL_STATUS(uint8_t icNum, uint16_t length, uint8_t* subcmd)
{
	// Pulling the first byte out of the sub API buffer (SUBCMD arg on main API)
	return SI479XX_detail_status__command(icNum, subcmd[0], (subcmd+1), (length-1));
}

RETURN_CODE DETAIL_STATUS_SUB(uint8_t icNum, uint16_t length, uint8_t * subcmd)
{
	return SI479XX_detail_status__command(icNum, subcmd[0], (subcmd+1), (length-1));
}

#endif	/*SI479XX_FIRM_REL_4_1*/
