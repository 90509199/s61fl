/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_si479xx_detail_status_api_h_global_
		
#include "main.h"

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "all_apis.h"

#include "si479xx_detail_status_api_constants.h"
#include "si479xx_detail_status_api.h"
#endif	/*0*/

#if 1		/* 210427 SGsoft */
extern uint32_t silab_cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];						
extern uint32_t silab_reply_buffer_u32[SI479XX_RPY_BUFFER_LENGTH/4];

static uint8_t lastCommands[MAX_NUM_PARTS];
static uint8_t lastTuner;

static uint32_t* cmd_arg_u32 = (uint32_t *)&silab_cmd_arg_u32[1];
static uint16_t*cmd_arg_u16 = (uint16_t*)&silab_cmd_arg_u32[1];
static uint8_t*	cmd_arg = (uint8_t*)&silab_cmd_arg_u32[1];
static int32_t*	cmd_arg_i32 = (int32_t*)&silab_cmd_arg_u32[1];
static int16_t*	cmd_arg_i16 = (int16_t*)&silab_cmd_arg_u32[1];

static uint32_t* reply_arg_u32 = (uint32_t *)&silab_reply_buffer_u32[4];
static uint16_t* reply_arg_u16 = (uint16_t*)&silab_reply_buffer_u32[4];
static uint8_t*	reply_arg = (uint8_t*)&silab_reply_buffer_u32[4];
static int32_t* reply_arg_i32 = (int32_t*)&silab_reply_buffer_u32[4];
static int16_t* reply_arg_i16 = (int16_t*)&silab_reply_buffer_u32[4];

#else
static uint8_t lastCommands[MAX_NUM_PARTS];
static uint8_t lastTuner;

static uint32_t	cmd_arg_u32[SI479XX_CMD_BUFFER_LENGTH/4];
static uint16_t*	cmd_arg_u16 = (uint16_t*)cmd_arg_u32;
static uint8_t*	cmd_arg = (uint8_t*)cmd_arg_u32;
static int32_t*	cmd_arg_i32 = (int32_t*)cmd_arg_u32;
static int16_t*	cmd_arg_i16 = (int16_t*)cmd_arg_u32;

static uint32_t	reply_arg_u32[SI479XX_RPY_BUFFER_LENGTH/4];
static uint16_t*	reply_arg_u16 = (uint16_t*)reply_arg_u32;
static uint8_t*	reply_arg = (uint8_t*)reply_arg_u32;
static int32_t* 	reply_arg_i32 = (int32_t*)reply_arg_u32;
static int16_t* 	reply_arg_i16 = (int16_t*)reply_arg_u32;
#endif	/*1*/


RETURN_CODE SI479XX_DETAIL_STATUS_die_temperature__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_DETAIL_STATUS_CMD_DIE_TEMPERATURE_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_DETAIL_STATUS_CMD_DIE_TEMPERATURE;


	ret = DETAIL_STATUS_SUB(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_DETAIL_STATUS_CMD_DIE_TEMPERATURE;
	}

	return ret;

}

RETURN_CODE SI479XX_DETAIL_STATUS_die_temperature__reply(uint8_t icNum, int16_t* temperature)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (temperature == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_DETAIL_STATUS_CMD_DIE_TEMPERATURE) {
		rdrep_len = SI479XX_DETAIL_STATUS_CMD_DIE_TEMPERATURE_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			*temperature = SI479XX_DETAIL_STATUS_CMD_DIE_TEMPERATURE_REP_TEMPERATURE_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_DETAIL_STATUS_die_temperature(uint8_t icNum, int16_t*  temperature)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_DETAIL_STATUS_die_temperature__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (temperature != NULL) {
		r |= SI479XX_DETAIL_STATUS_die_temperature__reply(icNum, temperature);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}



RETURN_CODE SI479XX_DETAIL_STATUS_swerr_info__command(uint8_t icNum)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t cmd_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	cmd_len = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_LENGTH;
	if (cmd_len > SI479XX_CMD_BUFFER_LENGTH) {
		return WRAPPER_ERROR;
	}
	ClearMemory(cmd_arg, cmd_len);

	cmd_arg[0] = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO;


	ret = DETAIL_STATUS_SUB(icNum, cmd_len, cmd_arg);

	if (ret == SUCCESS) {
		lastCommands[icIndex] = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO;
	}

	return ret;

}

RETURN_CODE SI479XX_DETAIL_STATUS_swerr_info__reply(uint8_t icNum, SI479XX_DETAIL_STATUS_swerr_info__data* replyData)
{
	RETURN_CODE ret = 0;
	uint8_t icIndex = 0;
	uint32_t rdrep_len = 0;

	icIndex = getCommandIndex(icNum);
	if (icIndex >= MAX_NUM_PARTS) {
		return INVALID_INPUT;
	}
	if (replyData == NULL) {
		return INVALID_INPUT;
	}
	if (lastCommands[icIndex] == SI479XX_DETAIL_STATUS_CMD_SWERR_INFO) {
		rdrep_len = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_LENGTH + SUBCMD_HEADER_SIZE;
		if (rdrep_len > SI479XX_RPY_BUFFER_LENGTH) {
			return WRAPPER_ERROR;
		}
		ret = iapis_readReply(icNum, rdrep_len, reply_arg);
		if (ret == SUCCESS) {
			replyData->LAST_SWERR = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_LAST_SWERR_value;
			replyData->INFO0 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO0_value;
			replyData->INFO1 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO1_value;
			replyData->INFO2 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO2_value;
			replyData->INFO3 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO3_value;
			replyData->INFO4 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO4_value;
			replyData->INFO5 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO5_value;
			replyData->INFO6 = SI479XX_DETAIL_STATUS_CMD_SWERR_INFO_REP_INFO6_value;
		}
	}
	else
	{
		ret = COMMAND_ERROR;
	}
	return ret;
}


RETURN_CODE SI479XX_DETAIL_STATUS_swerr_info(uint8_t icNum, SI479XX_DETAIL_STATUS_swerr_info__data*  replyData)
{
	RETURN_CODE r = 0;
	r = iapis_acquireLock(icNum);
	if (r != SUCCESS)
		return r;
	r = SI479XX_DETAIL_STATUS_swerr_info__command(icNum);
	if(r != SUCCESS) {
		r |= iapis_releaseLock(icNum);
		return r;
	}
	if (replyData != NULL) {
		r |= SI479XX_DETAIL_STATUS_swerr_info__reply(icNum, replyData);
	}
	r |= iapis_releaseLock(icNum);
	return r;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
