/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft *//* 210504 SGsoft */	
#define _r_4_1_audio_cfg_h_global_
		
#include "main.h"

#if 0
#include "apis/si46xx_hd_firmware_api.h"
#include "audio/ref_audio.h"
#include "audio/ref_audio_atl.h"
#include "audio/ref_audio_core.h"

#include "common/Firmware_API_Common.h"
#include "common/common_tuner.h"
#include "receiver/RefAudio_API.h"

#include "core/c_core.h"
#include "core/c_util.h"
#include "util/sdk_macro.h"
#include "HAL.h"

#include "apis/si4690_dab_firmware_api.h"
#include "apis/si4796x_tuner_api_constants.h"
#include "apis/si479xx_dsp_api.h"
#include "apis/si479xx_dsp_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "apis/si479xx_hub_api.h"
#include "apis/si479xx_hub_api_constants.h"
#endif	/*0*/

#include <stdlib.h>

#if 0
void audcfg_free(audio_cfg* cfg)
{
    if (cfg == NULL)
        return;

    switch (cfg->routing) {
    case ACFG_TUNERONLY:
        // nothing to free
        break;

    case ACFG_HIFI_PORT_ONLY:
        free(cfg->subcfg);
        break;

    case ACFG_HIFI_REF_DESIGN:
        // not freeing cfg->subcfg because it refers to static data.
        break;
    }

    cfg->subcfg = NULL;
}

static RETURN_CODE init_hifi_cfg(audio_cfg* cfg, uint8_t inport)
{
    RETURN_CODE ret = SUCCESS;

    if (!cfg)
        return INVALID_INPUT;

    switch (cfg->routing) {
    case ACFG_HIFI_REF_DESIGN: {
        audio_cfg_hifi_ref* acfg = &atl_ref6;
        cfg->subcfg = acfg;
        // Only need to change the ATL_UNKNOWN fields.
        acfg->audio_src_port = inport;
        break;
    }
    case ACFG_HIFI_PORT_ONLY: {
        // AUDIO_HUB image: Use port to port connections from tuner to DAC.
        audio_cfg_hifi_p2p* acfg = malloc(sizeof(audio_cfg_hifi_p2p));
        if (acfg == NULL)
            return MEMORY_ERROR;
        cfg->subcfg = acfg;

        // hub connections from input port to DAC01
        hub_cnx* cnxs = acfg->conns;
        cnxs[0].type = CNX_PORT_TO_PORT;
        cnxs[0].pp.in_chan = 0;
        cnxs[0].pp.in_port = inport;
        cnxs[0].pp.out_chan = 0;
        cnxs[0].pp.out_port = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_OUT_PORT_ID_ENUM_DAC0;

        cnxs[1].type = CNX_PORT_TO_PORT;
        cnxs[1].pp.in_chan = 1;
        cnxs[1].pp.in_port = inport;
        cnxs[1].pp.out_chan = 0;
        cnxs[1].pp.out_port = SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_OUT_PORT_ID_ENUM_DAC1;
        break;
    }
    default:
        break;
    }

    return ret;
}
#endif	/*0*/

// Initializes the 'audio_cfg* cfg' to the desired audio configuration, also based off of the bootup.
// This code assumes a particular setup in the Initialize() implementation.
RETURN_CODE audcfg_init(audio_cfg* cfg, DAUGHTERCARD_TYPE dc_type, OPERATION_MODE opmode)
{
    if (cfg == NULL)
        return INVALID_INPUT;

    g_sf_audio_cfg = cfg;

    // Currently all daughtercards use IC0 for audio output/HIFI.
    cfg->ic = IC0;
    cfg->dc_type = dc_type;
	#if 0		/* 210504 SGsoft */	
    if (dc_type == DC_1DT1T1F || dc_type == DC_2DT1F)
        cfg->falcon = g_linker;
	#endif	/*0*/

    if (cfg->routing == ACFG_TUNERONLY) {
        cfg->subcfg = NULL;
        return SUCCESS;
    }

    // Additional setup for hub/reference design audio setups.
    switch (dc_type) {
	#if 0		/* 210504 SGsoft */	
    case DC_1T1D_84: {
        if (opmode == OPMODE_HD) {
            return init_hifi_cfg(cfg, SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_IN_PORT_ID_ENUM_I2SRX0);
        }
        if (opmode == OPMODE_DAB) {
            // Assumes initial radio mode is DAB (I2S input)
            return init_hifi_cfg(cfg, SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_IN_PORT_ID_ENUM_I2SRX0);
        }
        break;
    }
	#endif	/*0*/

    case DC_1T:
    case DC_1T1D_48: {
        // 48 pin tuner does not have HIFI capability.
        D_ASSERT_MSG(false, "ERROR: 1T1D 48pin daughtercard does not have Hifi capabilities.");
        return UNSUPPORTED_FUNCTION;
    }

	#if 0		/* 210504 SGsoft */	
    case DC_1DT1T1F: // Intentionally fall through to DC_2DT1F
    case DC_2DT1F:
        return init_hifi_cfg(cfg, SI479XX_HUB_CMD_XBAR_CONNECT_PORT_ARG_P2P_CNX_IN_PORT_ID_ENUM_I2SRX0);
	#endif	/*0*/

    default:
        SDK_PRINTF("ERROR: audcfg_init() - unsupported daughtercard %d\n", dc_type);
        return UNSUPPORTED_FUNCTION;
    }

    return SUCCESS;
}

RETURN_CODE audcfg_setup(const audio_cfg* cfg)
{
    RETURN_CODE ret = SUCCESS;

    if (cfg == NULL)
        return INVALID_INPUT;

    switch (cfg->routing) {
    case ACFG_TUNERONLY:
        // Tuneronly does not have hub connections to setup.
        break;

	#if 0		/* 210429 SGsoft */
    case ACFG_HIFI_PORT_ONLY: {
        audio_cfg_hifi_p2p* acfg = cfg->subcfg;
        const uint8_t audio_mode = SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_AUDIO;
        const uint8_t none_mode = SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_NONE;
        uint8_t conn_count = ARRAY_SZ(acfg->conns);

        ret |= SI479XX_dac_config(cfg->ic,
            SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
            (conn_count >= 2 ? audio_mode : none_mode),
            SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
            (conn_count >= 4 ? audio_mode : none_mode),
            SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
            (conn_count >= 6 ? audio_mode : none_mode));

        ret |= make_xbar_cnxs(cfg->ic, conn_count, acfg->conns, -128);
        ret |= set_xbar_cnxs_gain(cfg->ic, conn_count, acfg->conns, 0);
        break;
    }

    case ACFG_HIFI_REF_DESIGN: {
        audio_cfg_hifi_ref* acfg = cfg->subcfg;
        // ret |= setup_ref_design(cfg->ic, acfg->audio_src_port, acfg);
        ret |= Audio_Init(acfg->audio_src_port);
        break;
    }
	#endif	/*0*/

    default:
        SDK_PRINTF("WARN: invalid audio_cfg routing : 0x%x\n", cfg->routing);
        return INVALID_INPUT;
    }

    return ret;
}

#if 0
RETURN_CODE audcfg_set_volume(const audio_cfg* cfg, uint16_t attenuation_db)
{
    if (cfg == NULL)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    // If muted do nothing
    AUDIO_MUTE mute_state;
    ret = audcfg_get_mute(cfg, &mute_state);
    if (ret != SUCCESS)
        return ret;

    if (mute_state != AUDIO_MUTE_NOT_MUTED)
        return SUCCESS;

    switch (cfg->routing) {
    case ACFG_TUNERONLY: {
        uint16_t vol_value;
        tuner_id tuner;
        // assumption that tuner-only mode is only used by single tuner parts
        tuner.ic = cfg->ic;
        tuner.id = TUNER_ID0;

        //using radio properties (DAC must be connected directly to tuner, not to hifi)
        attenuation_db = MIN(attenuation_db, SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_VOL_MAX - 1);
        vol_value = SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_VOL_MAX - attenuation_db;

        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR,
            vol_value);
        break;
    }

    case ACFG_HIFI_PORT_ONLY: {
        const audio_cfg_hifi_p2p* hcfg = cfg->subcfg;
        // Apply to all connections given.
        ret |= set_xbar_cnxs_gain(cfg->ic, ARRAY_SZ(hcfg->conns), hcfg->conns, -attenuation_db);
        break;
    }

    case ACFG_HIFI_REF_DESIGN: {
        audio_cfg_hifi_ref* acfg = cfg->subcfg;
        ret |= set_volume_modules(acfg, attenuation_db);
        break;
    }
    }

    if (ret == SUCCESS)
        g_board_metrics.audio.attenuation_db = attenuation_db;

    return ret;
}

RETURN_CODE audcfg_set_mute(const audio_cfg* cfg, AUDIO_MUTE mute_type)
{
    RETURN_CODE ret = SUCCESS;

    switch (cfg->routing) {
    case ACFG_TUNERONLY: {
        // assumption that tuneronly mode is only used on single tuner parts
        tuner_id tuner = {.ic = cfg->ic, .id = TUNER_ID0 };
        uint16_t val = SI4796X_TUNER_PROP_RADIO_MUTE_DEFAULT;
        if (mute_type == AUDIO_MUTE_BOTH)
			SET_PROP_ENUM(val, SI4796X_TUNER_PROP_RADIO_MUTE_DAC_OUTPUT_MUTE, BOTH);
        else
			SET_PROP_ENUM(val, SI4796X_TUNER_PROP_RADIO_MUTE_DAC_OUTPUT_MUTE, UNMUTE);
        ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, val);
        break;
    }

    case ACFG_HIFI_PORT_ONLY: {
        const audio_cfg_hifi_p2p* hcfg = cfg->subcfg;

        // Currently hifi p-p will either mute or unmute all.
        // There is no left/right muting distinction.
        // This could potentially be added by only muting odd/even connections.
        uint8_t port_mute;
        const uint8_t conn_count = ARRAY_SZ(hcfg->conns);
        if (mute_type == AUDIO_MUTE_BOTH) {
            port_mute = SI479XX_HUB_CMD_PORT_MUTE_ARG_MUTESTATE_ENUM_MUTE;
        } else {
            port_mute = SI479XX_HUB_CMD_PORT_MUTE_ARG_MUTESTATE_ENUM_UNMUTE;
        }

        ret = lock_chip(cfg->ic, NULL);
        RETURN_ERR(ret);
        // Mute all output connections with single HIFI_CMD
        startHifiCmd();
        for (uint16_t i = 0; i < conn_count; i++) {
            const port_to_port_cnx* cnx = &hcfg->conns[i].pp;
            ret |= SI479XX_HUB_port_mute__command(cfg->ic, 0,
                SI479XX_HUB_CMD_PORT_MUTE_ARG_LENGTH_MIN,
                cnx->out_port,
                cnx->out_chan,
                SI479XX_HUB_CMD_PORT_MUTE_ARG_MUTETYPE_ENUM_RAMP,
                port_mute);
        }
        stopHifiCmd();
        if (ret == SUCCESS) {
            ret |= writeHifiCmd(cfg->ic, AUDIO_HUB_PIPE);
            // Check reply packets for success STATUS.
            ret |= hifi_check_all_status(cfg->ic, AUDIO_HUB_PIPE, conn_count);
        }
        ret |= unlock_chip(cfg->ic);
        break;
    }

    case ACFG_HIFI_REF_DESIGN:
        ret = set_ref_mute(cfg->subcfg, g_board_metrics.audio.attenuation_db, mute_type);
        break;
    }

    if (ret == SUCCESS)
        g_board_metrics.audio.mute = mute_type;

    return ret;
}

RETURN_CODE audcfg_get_volume(const audio_cfg* cfg, uint16_t* attenuation_db)
{
    if (cfg == NULL || attenuation_db == NULL)
        return INVALID_INPUT;

    *attenuation_db = g_board_metrics.audio.attenuation_db;
    return SUCCESS;

    // Following shows how this could be read back from the part.
    /*
    RETURN_CODE ret = SUCCESS;
	switch (cfg->routing)
	{
	case ACFG_TUNERONLY:
	{
		uint16_t vol_value;

		ret |= get_tuner_prop(cfg->ic,
			SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR,
			&vol_value);

		*attenuation_db = SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_VOL_MAX - vol_value;
		return ret;
	}

	case ACFG_HIFI_PORT_ONLY:
	{
		const audio_cfg_hifi_p2p* hcfg = cfg->subcfg;
		port_to_port_cnx* cnx;
		SI479XX_query_connection_gain__data query;
		double value = 0;

		// Need to convert attenuation into S16.8 format.
		// 0 dB is FS signal, negative gain is attenuation.
		if (hcfg->conn_count <= 0)
		{
			SDK_PRINTF("ERROR? no port connections in audio cfg.\n");
			*attenuation_db = 0;
			return SUCCESS;
		}

		cnx = &hcfg->conns[0];

		ret = SI479XX_query_connection_gain(cfg->ic, 0,
			SI479XX_CMD_QUERY_CONNECTION_GAIN_ARG_CNX_ID_ENUM_PORT_TO_PORT,
			SI479XX_CMD_QUERY_CONNECTION_GAIN_ARG_LENGTH_MIN,
			cnx->out_chan, cnx->out_port,
			cnx->in_chan, cnx->in_port, &query);

		value = get_binary_point_value(query.GAIN_DB, true, 16, 8);
		*attenuation_db = (uint16_t)(-value);

		break;
	}

	case ACFG_HIFI_REF_DESIGN:
		// See log_volume_module() implementation.
		return UNSUPPORTED_FUNCTION;
	}

	return ret;
	*/
}

RETURN_CODE audcfg_get_mute(const audio_cfg* cfg, AUDIO_MUTE* mute_type)
{

    *mute_type = g_board_metrics.audio.mute;
    return SUCCESS;

    // Following shows how to read back from part.
    /*
    RETURN_CODE ret = SUCCESS;
	switch (cfg->routing)
	{
	case ACFG_TUNERONLY:
	{
		uint16_t value = 0;
		ret |= get_tuner_prop(cfg->ic,
			SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, &value);

		*mute_type = value;
		break;
	}

	case ACFG_HIFI_PORT_ONLY:
	{
		const audio_cfg_hifi_p2p* hcfg = cfg->subcfg;
		port_to_port_cnx* cnx;
		SI479XX_port_query__data query;
		double value = 0;

		if (hcfg->conn_count <= 0)
		{
			SDK_PRINTF("ERROR? no port connections in audio cfg.\n");
			*mute_type = AUDIO_MUTE_NOT_MUTED;
			return SUCCESS;
		}

		cnx = &hcfg->conns[0];

		ret = SI479XX_port_query(cfg->ic, 0,
			cnx->out_port, SI479XX_CMD_PORT_QUERY_ARG_LENGTH_MIN, &query);

		*mute_type = (query.CHAN_STATE & (1 << cnx->out_chan)) ? AUDIO_MUTE_LEFT_RIGHT : AUDIO_MUTE_NOT_MUTED;
		break;
	}

	case ACFG_HIFI_REF_DESIGN:
		// See log_volume_module()  (ref design uses volume for MUTE too.)
		return UNSUPPORTED_FUNCTION;
	}

	return ret;
	*/
}

RETURN_CODE audcfg_change_source_port(const audio_cfg* cfg, uint8_t new_port)
{
    RETURN_CODE ret = SUCCESS;

    switch (cfg->routing) {
    case ACFG_TUNERONLY: {
        SDK_PRINTF("WARN: in tuner-only audio configurations, cannot call audcfg_change_source_port().");
        return INVALID_MODE;
    }

    case ACFG_HIFI_PORT_ONLY: {
        audio_cfg_hifi_p2p* hcfg = cfg->subcfg;
        const uint8_t conn_count = ARRAY_SZ(hcfg->conns);

        // Disconnect existing.
        ret |= set_xbar_cnxs_gain(cfg->ic, conn_count, hcfg->conns, -128);
        ret |= break_xbar_cnxs(cfg->ic, conn_count, hcfg->conns);

        // Update input port
        for (uint8_t i = 0; i < conn_count; i++)
            hcfg->conns[i].pp.in_port = new_port;

        // Connect new port (ramp from mute)
        ret |= make_xbar_cnxs(cfg->ic, conn_count, hcfg->conns, -128);
        // Restore prior volume level
        ret |= set_xbar_cnxs_gain(cfg->ic, conn_count, hcfg->conns, -g_board_metrics.audio.attenuation_db);
        break;
    }

    case ACFG_HIFI_REF_DESIGN: {
        audio_cfg_hifi_ref* acfg = cfg->subcfg;
        const uint8_t conn_count = ARRAY_SZ(acfg->in_cnxs);

        // Disconnect existing.
        ret |= set_xbar_cnxs_gain(cfg->ic, conn_count, acfg->in_cnxs, -128);
        ret |= break_xbar_cnxs(cfg->ic, conn_count, acfg->in_cnxs);

        // Update input port
        for (uint8_t i = 0; i < conn_count; i++)
            acfg->in_cnxs[i].pn.port_id = new_port;

        // Connect new port (ramp from mute)
        ret |= make_xbar_cnxs(cfg->ic, conn_count, acfg->in_cnxs, -128);
        ret |= set_xbar_cnxs_gain(cfg->ic, conn_count, acfg->in_cnxs, 0);
        // Prior volume level already set (in volume modules)
        break;
    }
    default:
        SDK_PRINTF("ERROR: invalid audio routing type in audcfg_change_source_port().");
        return INVALID_INPUT;
    }

    return ret;
}

RETURN_CODE audcfg_switch_src(const audio_cfg* cfg, LINKER_INPUT_SOURCE src)
{
    D_ASSERT(cfg);
    // Depends on daughtercard type, and the audio routing type.
    // On boards with a falcon we just switch the falcon audio source.
    // On other boards we need to monkey with the tuner's audio setup.
    RETURN_CODE r = SUCCESS;

    if (cfg->dc_type == DC_2DT1F || cfg->dc_type == DC_1DT1T1F) {
        // Falcon setup
        r = linker_set_audio_source(cfg->falcon, src);
    } else {
        // Tuner setup
        switch (cfg->routing) {
        case ACFG_TUNERONLY:
            // Si47901 'tuneronly' parts do not support 'DAB2' because that would require another I2S RX port.
            if (src == LINKER_INPUT_DEMOD2)
                r = INVALID_INPUT;
            else if (src == LINKER_INPUT_EXT_FM) {
                // Tuner quark audio
                r = SI479XX_dac_config(cfg->ic,
                    SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_FAST, SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMOD,
                    0, 0,
                    0, 0);
            } else {
                // RX0 audio
                r = SI479XX_dac_config(cfg->ic,
                    SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_FAST, SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMODDIGIN,
                    0, 0,
                    0, 0);
            }
            break;
        case ACFG_HIFI_PORT_ONLY:
        case ACFG_HIFI_REF_DESIGN:
            // For FM, output quark audio
            // For DAB1/DAB2, output incoming I2S RX audio.
            if (src == LINKER_INPUT_EXT_FM)
                r = audcfg_change_source_port(cfg, SI479XX_HUB_CMD_PORT_MUTE_ARG_PORT_ID_ENUM_TUNER0_AUDIO_TO_HIFI);
            else if (src == LINKER_INPUT_DEMOD1)
                r = audcfg_change_source_port(cfg, SI479XX_HUB_CMD_PORT_MUTE_ARG_PORT_ID_ENUM_I2SRX0);
            else if (src == LINKER_INPUT_DEMOD2)
                r = audcfg_change_source_port(cfg, SI479XX_HUB_CMD_PORT_MUTE_ARG_PORT_ID_ENUM_I2SRX1);
            break;
        }
    }

    if (r == SUCCESS) {
        // for ConsoleApp project, we need to update current DAB receiver data.
        // 2DT1F -> DEMOD1 is background
        if (src == LINKER_INPUT_DEMOD1 && cfg->dc_type == DC_2DT1F) {
            g_dab_state = &g_dab_states[0];
            g_dab_cfg = g_dab_parts.main;
        } else if (src == LINKER_INPUT_DEMOD2) {
            g_dab_state = &g_dab_states[1];
            g_dab_cfg = g_dab_parts.bg;
        }
    }

    return r;
}

RETURN_CODE audcfg_get_state(const audio_cfg* cfg, audio_state* state_out)
{
    RETURN_CODE ret = SUCCESS;

    if (!cfg || !state_out)
        return INVALID_INPUT;

    ret |= audcfg_get_mute(cfg, &state_out->mute);
    ret |= audcfg_get_volume(cfg, &state_out->attenuation_db);

    return ret;
}

RETURN_CODE audcfg_restore_state(const audio_cfg* cfg, audio_state state)
{
    RETURN_CODE ret = SUCCESS;

    if (!cfg)
        return INVALID_INPUT;

    ret |= audcfg_set_mute(cfg, state.mute);
    ret |= audcfg_set_volume(cfg, state.attenuation_db);
    // Set MONO here too?

    return ret;
}

RETURN_CODE audcfg_sf_switch_src(LINKER_INPUT_SOURCE src)
{
    if (g_sf_audio_cfg == NULL)
        return INVALID_MODE;

    return audcfg_switch_src(g_sf_audio_cfg, src);
}
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
