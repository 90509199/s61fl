/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_system_configuration_h_global_
		
#include "main.h"

#if 0
#include "platform/System_Configuration.h"
#include "platform/audio_cfg.h"
#include "receiver/IFirmware_API_Manager.h"
#include "util/sdk_macro.h"
#include "core/c_util.h"
#include "core/c_core.h"
#include "common/global_data.h"

#include "receiver/dc_specific/dc_all.h"

#include "apis/si479xx_firmware_api_constants.h"
#endif	/*0*/

#include <stdlib.h>

DAUGHTERCARD_TYPE syscfg_get_dc_type();

/***************************************************************************************
			daughtercard_cfg public implementation
****************************************************************************************/
bool syscfg_is_initialized(const system_config* config)
{
    return (config != NULL) && (config->fw.initialize != NULL);
}

RETURN_CODE syscfg_set_options(system_config* cfg, const struct hal_stored_cfg* hw_cfg)
{
    if (!cfg)
        return INVALID_INPUT;

    audio_cfg* audio = &cfg->audio;
    system_options* opts = &cfg->opts;
    switch (OPTION_AUDIO_MODE) {
    case ACFG_HIFI_PORT_ONLY:
    case ACFG_HIFI_REF_DESIGN:
    case ACFG_TUNERONLY:
        // all valid settings.
        audio->routing = OPTION_AUDIO_MODE;
        break;
    default:
//        D_ASSERT_MSG(false, "ERROR: invalid value for OPTION_AUDIO_MODE");		//YOON_211129_v2
        return UNSUPPORTED_FUNCTION;
    }

#if defined(OPTION_BOOT_FROM_HOST)
    opts->use_host_boot = true;
#elif defined(OPTION_BOOT_FROM_FLASH)
    opts->use_host_boot = false;
#else
    #error "Required option OPTION_BOOT_FROM_xxx not defined!"
#endif

    opts->hd_use_mrc = false;
#ifndef OPTION_HDMRC_OFF
    // check if daughtercard is even capable of mrc
    switch (cfg->dc_type) {
    case DC_2DT1F:
    case DC_1DT1T1F:
        opts->hd_use_mrc = true;
        break;
    }
#endif

    opts->dab_use_mrc = false;
#ifndef OPTION_DABMRC_OFF
    switch (cfg->dc_type) {
    case DC_2DT1F:
    case DC_1DT1T1F:
        opts->dab_use_mrc = true;
        break;
    }
#endif

    switch (OPTION_HD_IQRATE) {
    default:
        D_ASSERT_MSG(false, "INVALID OPTION_HD_IQRATE=%d", OPTION_HD_IQRATE);
        return UNSUPPORTED_FUNCTION;
    case 744:
        opts->hd_iqrate = SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_RATE_ENUM_IQ744KS;
        break;
    case 650:
        opts->hd_iqrate = SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_RATE_ENUM_IQ650KS;
        break;
    }
    switch (OPTION_HD_IQSLOT) {
    default:
        D_ASSERT_MSG(false, "INVALID OPTION_HD_IQSLOT=%d", OPTION_HD_IQSLOT);
        return UNSUPPORTED_FUNCTION;
    case 16:
        opts->hd_iqslot = SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE_ENUM_SIXTEEN;
        break;
    case 18:
        opts->hd_iqslot = SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE_ENUM_EIGHTEEN;
        break;
    }

    switch (OPTION_AUDIO_RATE) {
    case 48000:
        opts->powerup_afs = SI479XX_CMD_POWER_UP_ARG_AFS_ENUM_FS48;
        break;
    case 44100:
        opts->powerup_afs = SI479XX_CMD_POWER_UP_ARG_AFS_ENUM_FS44;
        break;
    default:
        D_ASSERT_MSG(false, "INVALID OPTION_AUDIO_RATE=%d", OPTION_AUDIO_RATE);
        return UNSUPPORTED_FUNCTION;
    }

    return SUCCESS;
}

static RETURN_CODE set_parts__dab(const system_config* config)
{
    // Setup the parts so that 'Implementation__common' functions work correctly.
    RETURN_CODE ret = syscfg_get_dab(config, &g_dab_parts);
    g_fm_cfg = g_dab_parts.fm_am;
    g_dab_cfg = g_dab_parts.main;
    return ret;
}

static RETURN_CODE set_fw__dab(system_config* cfg)
{
    // Setup the firmware API implementations so that they work correctly for DAB.
    if (cfg == NULL)
        return INVALID_INPUT;

    // Some functions require specific daughtercard implementations.
    // Many functions' implementations are generic so that they only need to
    //	 know '_g_dab_parts'  data to implement them.
    struct firmware_manager_pimpls* fps = &cfg->fw;
    fps->initialize = NULL;
    fps->set_radio_mode = NULL;
    switch (cfg->dc_type) {
    case DC_1DT1T1F:
        SDK_PRINTF("DAB not implemented for 1DT1T1F\n");
        break;
    case DC_1T:
        // Only provided for BasicBoot
        #if 1		/* 210427 SGsoft */
		fps->initialize = Initialize_DAB_1T;
        fps->set_radio_mode = SetRadioMode_DAB_1T;
		#else
        SDK_PRINTF("WARNING: Single Tuner Audio SDK board not supported by ConsoleApp.\n");
		#endif	/*1*/
        break;

	#if 0		/* 210429 SGsoft */
    case DC_2DT1F:
        fps->initialize = Initialize_DAB_2DT1F;
        fps->set_radio_mode = SetRadioMode_DAB_2DT1F;
        break;

    case DC_1T1D_84:
    case DC_1T1D_48:
        fps->initialize = Initialize_DAB_1T1D;
        fps->set_radio_mode = SetRadioMode_DAB_1T1D;
        break;
	#endif	/*0*/

    default:
        D_ASSERT_MSG(false, "ERROR: OPMODE_HD unsupported daughtercard type %d\n", cfg->dc_type);
        return UNSUPPORTED_FUNCTION;
    }

    RETURN_CODE ret = SUCCESS;
    ret = set_parts__dab(cfg);
    ///ret = set_parts__hd(cfg);							/* 210504 SGsoft */	
    return ret;
}

static RETURN_CODE set_parts__hd(const system_config* config)
{
    // Setup chips for Implementation__common
    RETURN_CODE ret = syscfg_get_hd(config, &g_hd_parts);
    g_fm_cfg = g_hd_parts.primary;
    // HD has no DAB configuration.
    return ret;
}

static RETURN_CODE set_fw__hd(system_config* sys)
{
#if 1		/* 210429 SGsoft */
	return SUCCESS;
#else
    // Setup FM/AM (HD) impelementations for fw_impl.
    if (sys == NULL)
        return INVALID_INPUT;

    // Some functions require specific daughtercard implementations.
    // Many functions' implementations are generic so that they only need to
    //	 know 'fad_parts'  data to implement them.
    struct firmware_manager_pimpls* fps = &sys->fw;
    switch (sys->dc_type) {
    case DC_1DT1T1F:
        fps->initialize = Initialize_HD_1DT1T1F;
        fps->set_radio_mode = SetRadioMode_HD_1DT1T1F;
        break;

    case DC_2DT1F:
        fps->initialize = Initialize_HD_2DT1F;
        fps->set_radio_mode = SetRadioMode_HD_2DT1F;
        break;

    case DC_1T1D_84:
    case DC_1T1D_48:
        fps->initialize = Initialize_HD_1T1D;
        fps->set_radio_mode = SetRadioMode_HD_1T1D;
        break;

    case DC_1T:
        // Only provided for BasicBoot
        SDK_PRINTF("WARNING: Single Tuner Audio SDK board not supported by ConsoleApp.\n");
        fps->initialize = NULL;
        fps->set_radio_mode = NULL;
        break;

    default:
        D_ASSERT_MSG(false, "ERROR: OPMODE_HD unsupported daughtercard type %d\n", sys->dc_type);
        return UNSUPPORTED_FUNCTION;
    }

    return set_parts__hd(sys);
#endif	/*1*/

}

void syscfg_set_api_interface()
{
    g_all_apis_functions.write_command = writeCommand;
    g_all_apis_functions.read_reply = readReply;

    g_all_apis_functions.hifi_command = hifi_command;
    g_all_apis_functions.hifi_response = hifi_response;

    g_all_apis_functions.acquire_lock = NULL;
    g_all_apis_functions.release_lock = NULL;
}

RETURN_CODE syscfg_initialize(system_config* config, OPERATION_MODE mode, const struct hal_stored_cfg* hw_cfg)
{
    RETURN_CODE ret = SUCCESS;

    if (!config)
        return INVALID_INPUT;

    config->opmode = mode;
    if (hw_cfg) {
        config->dc_type = hw_cfg->connected_board;
    } else {
        config->dc_type = syscfg_get_dc_type();
    }
    ret |= syscfg_set_options(config, hw_cfg);

    ///ret |= dcfg_initialize(&config->board, config->dc_type);		syscfg_initialize
    if (ret != SUCCESS) {
        SDK_PRINTF("Selected daughtercard type: %d not yet supported.\n", config->dc_type);
        return ret;
    }

    // Audio routing tightly connected to board layout.
    ret = audcfg_init(&config->audio, config->dc_type, mode);
    if (ret != SUCCESS) {
        SDK_PRINTF("Failed to get audio configuration.\n");
        return ret;
    }

    // Mainly Initialize / SetRadioMode implementations 
    switch (mode) {
    case OPMODE_HD:
        set_fw__hd(config);
        break;
    case OPMODE_DAB:
        set_fw__dab(config);
        break;
    default:
        return INVALID_INPUT;
    }
    syscfg_set_api_interface();

    return SUCCESS;
}

// Releases resources held by a system_config.
// Invalidates the config pointer.
void syscfg_free(system_config* config)
{
    if (config != NULL) {
        audcfg_free(&config->audio);

        config->opmode = OPMODE_INVALID;
        config->dc_type = DC_NONE;
    }
}

bool syscfg_has_linker(const system_config* config)
{
    if (config == NULL) {
        SDK_PRINTF("ERROR: syscfg_has_linker null arg.");
        return false;
    }

    for (int i = 0; i < ARRAY_SZ(config->board.chip_types); i++) {
        if (config->board.chip_types[i] == Si469x)
            return true;
    }
    return false;
}

bool syscfg_has_dual_tuner(const system_config* config)
{
    if (config == NULL) {
        SDK_PRINTF("ERROR: syscfg_has_dual_tuner null arg.");
        return false;
    }

    switch (config->dc_type) {
    case DC_2DT1F:
    case DC_1DT1T1F:
        return true;

    default:
        return false;
    }
}

RETURN_CODE syscfg_get_first_linker(const system_config* cfg, linker_id* linker)
{
    if (cfg == NULL || linker == NULL)
        return INVALID_INPUT;

    if (!syscfg_has_linker(cfg))
        return UNSUPPORTED_FUNCTION;

    for (int i = 0; i < ARRAY_SZ(cfg->board.chip_types); i++) {
        if (cfg->board.chip_types[i] == Si469x) {
            linker->ic = (1 << i);
            return SUCCESS;
        }
    }
    SDK_PRINTF("ERROR: no linker found!\n");
    return UNSUPPORTED_FUNCTION;
}

fm_chips get_default_fm_chips()
{
    fm_chips def = { 0 };
    def.tuner.ic = 0;
    def.tuner.id = TUNER_ID_INVALID;
    def.div_tuner.ic = 0;
    def.div_tuner.id = TUNER_ID_INVALID;
    def.hd_demod.ic = 0;
    def.hd_demod.select = DEMOD_SELECT_INVALID;

    def.hd_enabled = false;
    def.phase_div_enabled = false;
    def.hd_div_enabled = false;

    return def;
}

dab_chips get_default_dab_chips()
{
    dab_chips chips = { 0 };

    chips.dab_mrc_enabled = false;

    chips.tuner.ic = 0;
    chips.tuner.id = TUNER_ID_INVALID;

    chips.mrc_tuner.ic = 0;
    chips.mrc_tuner.id = TUNER_ID_INVALID;

    chips.demod.ic = 0;
    chips.demod.select = DEMOD_SELECT_INVALID;

    return chips;
}

RETURN_CODE syscfg_get_hd(const system_config* config, hd_mode_chips* chips)
{
    RETURN_CODE ret = SUCCESS;

    if (config == NULL || chips == NULL)
        return INVALID_INPUT;

    // Disable all additional chips by default.
    chips->has_background = false;
    chips->back.phase_div_enabled = false;
    chips->back.hd_div_enabled = false;
    chips->back.hd_enabled = false;

    chips->primary.phase_div_enabled = false;
    chips->primary.hd_div_enabled = false;
    chips->primary.hd_enabled = false;

    switch (config->dc_type) {
    case DC_1DT1T1F: {
        // CHIP_0 either has HiFi audio capabilities OR HD-MRC capabilities
        chips->primary.tuner.ic = IC0;
        chips->primary.tuner.id = TUNER_ID0;
        chips->primary.phase_div_enabled = true;
        chips->primary.hd_div_enabled = true;
        chips->primary.div_tuner.ic = IC0;
        chips->primary.div_tuner.id = TUNER_ID1;
        chips->primary.hd_enabled = true;
        demod_id hdback = {.ic = IC3, .select = DEMOD_SELECT1 };
        chips->primary.hd_demod = hdback;

        // CHIP_1 is 'background'
        chips->has_background = true;
        chips->back.tuner.ic = IC1;
        chips->back.tuner.id = TUNER_ID0;
        chips->back.phase_div_enabled = false;
        chips->back.hd_div_enabled = false;
        chips->back.hd_enabled = true;
        demod_id hdprimary = {.ic = IC3, .select = DEMOD_SELECT2 };
        chips->back.hd_demod = hdprimary;
        break;
    }

    case DC_2DT1F: {
        // CHIP_0 either has HiFi audio capabilities OR HD-MRC capabilities
        chips->primary.tuner.ic = IC0;
        chips->primary.tuner.id = TUNER_ID0;
        chips->primary.phase_div_enabled = true;
        chips->primary.hd_div_enabled = true;
        chips->primary.div_tuner.ic = IC0;
        chips->primary.div_tuner.id = TUNER_ID1;
        chips->primary.hd_enabled = true;
        demod_id hdback = {.ic = IC3, .select = DEMOD_SELECT1 };
        chips->primary.hd_demod = hdback;

        // CHIP_1 is 'background'
        chips->has_background = true;
        chips->back.tuner.ic = IC1;
        chips->back.tuner.id = TUNER_ID0;
        chips->back.phase_div_enabled = true;
        chips->back.hd_div_enabled = true;
        chips->back.div_tuner.ic = IC1;
        chips->back.div_tuner.id = TUNER_ID1;
        chips->back.hd_enabled = true;
        demod_id hdprimary = {.ic = IC3, .select = DEMOD_SELECT2 };
        chips->back.hd_demod = hdprimary;
        break;
    }

    case DC_1T1D_84:
    case DC_1T1D_48: {
        // CHIP_0 is FM/AM tuner + audio chip
        chips->primary.tuner.ic = IC0;
        chips->primary.tuner.id = TUNER_ID0;

        // CHIP_3 is HD Demod
        chips->primary.hd_enabled = true;
        chips->primary.hd_demod.ic = IC3;
        chips->primary.hd_demod.select = DEMOD_SELECT_SINGLE;
        break;
    }

	case DC_1T: {												/* 210504 SGsoft */	
        // CHIP_0 either has HiFi audio capabilities OR HD-MRC capabilities
        chips->primary.tuner.ic = IC0;
        chips->primary.tuner.id = TUNER_ID0;
		break;
	}
	
    default:
        return UNSUPPORTED_FUNCTION;
    }

    // Override with system options
    if (!config->opts.hd_use_mrc) {
        chips->primary.hd_div_enabled = false;
        chips->back.hd_div_enabled = false;
    }

    return ret;
}

RETURN_CODE syscfg_get_dab(const system_config* config, dab_mode_chips* chips)
{
    RETURN_CODE ret = SUCCESS;

    if (config == NULL || chips == NULL)
        return INVALID_INPUT;

    // Disable all additional chips by default.
    chips->has_background_dab = false;
    chips->has_fm = true;
    chips->main.dab_mrc_enabled = false;
    chips->bg.dab_mrc_enabled = false;

    chips->fm_am.phase_div_enabled = false;
    chips->fm_am.hd_div_enabled = false;
    chips->fm_am.hd_enabled = false;

    switch (config->dc_type) {
    case DC_1DT1T1F: {
        SDK_PRINTF("DC_1DT1T1F : DAB mode not implemented.\n");
        break;
    }

    case DC_2DT1F: {
        // CHIP_0 is always FM analog + audio chip.
        chips->fm_am.tuner.ic = IC0;
        chips->fm_am.tuner.id = TUNER_ID0;

        if (config->opts.dab_use_mrc) {
            // Need second DAB tuner on CHIP_0
            chips->has_background_dab = true;
            chips->bg.tuner.ic = IC0;
            chips->bg.tuner.id = TUNER_ID1;
            chips->bg.demod.ic = IC3;
            chips->bg.demod.select = DEMOD_SELECT2;

            // Primary DAB receiver is DAB-MRC on CHIP_1
            chips->main.tuner.ic = IC1;
            chips->main.tuner.id = TUNER_ID0;
            chips->main.dab_mrc_enabled = true;
            chips->main.mrc_tuner.ic = IC1;
            chips->main.mrc_tuner.id = TUNER_ID1;
            chips->main.demod.ic = IC3;
            chips->main.demod.select = DEMOD_SELECT1;
        } else {
            // CHIP_1 DAB/DAB 
            chips->main.tuner.ic = IC1;
            chips->main.tuner.id = TUNER_ID0;
            chips->main.demod.ic = IC3;
            chips->main.demod.select = DEMOD_SELECT1;
            chips->has_background_dab = true;
            chips->bg.tuner.ic = IC1;
            chips->bg.tuner.id = TUNER_ID1;
            chips->bg.demod.ic = IC3;
            chips->bg.demod.select = DEMOD_SELECT2;
        }

        break;
    }

    case DC_1T1D_84:
    case DC_1T1D_48: {
        // CHIP_0 is DAB tuner
        // Can alternatively be FM/AM analog tuner
        chips->main.tuner.ic = IC0;
        chips->main.tuner.id = TUNER_ID0;
        chips->fm_am.tuner = chips->main.tuner;

        // CHIP_3 is DAB Demod
        chips->main.demod.ic = IC3;
        chips->main.demod.select = DEMOD_SELECT_SINGLE;
        break;
    }

	case DC_1T: {											/* 210427 SGsoft */
		// CHIP_0 is DAB tuner
		// Can alternatively be FM/AM analog tuner
		chips->main.tuner.ic = IC0;
		chips->main.tuner.id = TUNER_ID0;
		chips->fm_am.tuner = chips->main.tuner;
		break;
	}

    default:
        return UNSUPPORTED_FUNCTION;
    }

    // Override DAB MRC options
    if (!config->opts.dab_use_mrc) {
        chips->main.dab_mrc_enabled = false;
        chips->bg.dab_mrc_enabled = false;
    }

    return ret;
}

// Only checking for supported types,
// otherwise returns INVALID_INPUT
DAUGHTERCARD_TYPE syscfg_get_dc_type()
{
    DAUGHTERCARD_TYPE tmp = DC_NONE;
#ifdef OPTION_DC_TYPE
    tmp = OPTION_DC_TYPE;
#endif
    // D_ASSERT_MSG(tmp != DC_NONE, "ERROR: Must define OPTION_DC_TYPE if no EEPROM connected!");
    return tmp;
}

//
// Following functions are convenience functions around g_evb_system_cfg
//

RETURN_CODE syscfg_get_ic_type(dc_config_chip_select ic, dc_config_chip_type* type)
{
    uint8_t index = 0;
    RETURN_CODE r = get_ic_num(ic, &index);
    if (r != SUCCESS)
        return r;

    *type = g_evb_system_cfg.board.chip_types[index];
    return SUCCESS;
}

bool syscfg_is_audio_tuneronly()
{
    return g_evb_system_cfg.audio.routing == ACFG_TUNERONLY;
}

bool syscfg_is_audio_ref6ch()
{
    return g_evb_system_cfg.audio.routing == ACFG_HIFI_REF_DESIGN;
}

bool syscfg_is_host_boot()
{
    return g_evb_system_cfg.opts.use_host_boot;
}

bool syscfg_is_flash_boot()
{
    return !syscfg_is_host_boot();
}

bool syscfg_is_1T1D_48()
{
    return g_evb_system_cfg.dc_type == DC_1T1D_48;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
