/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if 0/*defined(SI479XX_FIRM_REL_4_1)*/		/* 210419 SGsoft *//* 210504 SGsoft */	
#define _r_4_1_daughtercard_cfg_h_global_
		
#include "main.h"

#if 0
#include "platform/daughtercard_cfg.h"
#include "core/c_util.h"
#include "core/c_core.h"
#include "util/sdk_macro.h"
#include "platform_options.h"
#include "platform/System_Configuration.h"
#endif	/*0*/

#include <stdio.h>
#include <stdlib.h>

bool dcfg_is_multi_tuner(tuner_id tuner)
{
    return tuner.id != TUNER_ID0;
}

bool dcfg_is_satellite(demod_id demod)
{
    return demod.select != DEMOD_SELECT_INVALID
        && demod.select != DEMOD_SELECT_SINGLE
        && demod.select != DEMOD_SELECT_LINKER;
}

bool dcfg_is_tuner_ic(const struct dc_cfg* cfg, dc_config_chip_select ic)
{
    if (cfg == NULL) {
        SDK_PRINTF("ERROR: bad arg for dcfg_is_tuner_ic(), cfg was NULL\n");
        return false;
    }

    uint8_t index;
    RETURN_CODE r = get_ic_num(ic, &index);
    if (r != SUCCESS) {
        SDK_PRINTF("ERROR: bad arg for dcfg_is_tuner_ic(), bad 'ic'\n");
        return false;
    }

    switch (cfg->chip_types[index]) {
    case Si479xx:
    case Si4796x:
        return true;
    default:
        return false;
    }
}

bool dcfg_is_demod_ic(const struct dc_cfg* cfg, dc_config_chip_select ic)
{
    if (cfg == NULL) {
        SDK_PRINTF("ERROR: bad arg for dcfg_is_tuner_ic(), cfg was NULL\n");
        return false;
    }

    uint8_t index;
    RETURN_CODE r = get_ic_num(ic, &index);
    if (r != SUCCESS) {
        SDK_PRINTF("ERROR: bad arg for dcfg_is_tuner_ic(), bad 'ic'\n");
        return false;
    }

    return (cfg->chip_types[index] == Si461x);
}

LINKER_INPUT_SOURCE dcfg_linker_src(demod_id demod)
{
    switch (demod.select) {
    case DEMOD_SELECT1:
        return LINKER_INPUT_DEMOD1;

    case DEMOD_SELECT2:
        return LINKER_INPUT_DEMOD2;

    case DEMOD_SELECT_SINGLE: {
        // Hardcoded for silabs daughtercards ...
        uint8_t chip_num = 0;
        get_ic_num(demod.ic, &chip_num);
        return (LINKER_INPUT_DEMOD1 + (chip_num - 3));
    }
    default:
        return LINKER_INPUT_INVALID;
    }
}

// Defining where chips are on the different daughtercards.
// (NotPresent - 1) used as unique terminal value.
#define BC_CHIP_TYPE_TERMINAL (NotPresent - 1)
static struct dc_cfg s_dc_boards[DC_NONE] = {
	{
        .chip_types = {BC_CHIP_TYPE_TERMINAL},
        ///.description = "DC_3T2D: ???",												/* 210427 SGsoft */
    },
    {
        .chip_types = {Si4796x, Si4796x, NotPresent, Si469x, BC_CHIP_TYPE_TERMINAL},
        ///.description = "2DT1F: two Dual Eagle tuners + one falcon",					/* 210427 SGsoft */
    },
    {
        .chip_types = {Si4796x, Si479xx, NotPresent, Si469x, BC_CHIP_TYPE_TERMINAL},
        ///.description = "1DT1T1F: Dual + Global Eagle tuners + one falcon",			/* 210427 SGsoft */
    },
    {
        .chip_types = {Si479xx, NotPresent, NotPresent, Si461x, BC_CHIP_TYPE_TERMINAL},
        ///.description = "1T1D_84: Global Eagle (84pin package)+ one demod",			/* 210427 SGsoft */
    },
    {
        .chip_types = {Si479xx, NotPresent, NotPresent, Si461x, BC_CHIP_TYPE_TERMINAL},
        ///.description = "1T1D_48: Global Eagle (48pin package)+ one demod",			/* 210427 SGsoft */
    },
    {
        .chip_types = {Si479xx, BC_CHIP_TYPE_TERMINAL},
        ///.description = "1T: Single Global Eagle /audio sdk board.",					/* 210427 SGsoft */
    }
};

RETURN_CODE dcfg_initialize(struct dc_cfg* cfg, DAUGHTERCARD_TYPE dc_type)
{
    if (!cfg)
        return INVALID_INPUT;

    if (dc_type < 0 || dc_type >= DC_NONE) {
        D_ASSERT_MSG(0, "dcfg_initialize() - invalid daughtercard type!\n");
        return INVALID_INPUT;
    }

    // Copies configuration into memory.
    *cfg = s_dc_boards[dc_type];
    bool reached_end = false;
    // Fix up trailing chip_type array entries.
    for (uint8_t i = 0; i < ARRAY_SZ(cfg->chip_types); i++) {
        if (reached_end) {
            cfg->chip_types[i] = NotPresent;
        } else if (cfg->chip_types[i] == BC_CHIP_TYPE_TERMINAL) {
            cfg->chip_types[i] = NotPresent;
            reached_end = true;
        }
    }

    return SUCCESS;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
