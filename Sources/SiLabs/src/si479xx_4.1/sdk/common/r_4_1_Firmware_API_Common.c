/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#include "sg_defines.h"								

#if defined(SI479XX_FIRM_REL_4_1)		/* 210416 SGsoft */
#define _r_4_1_firmware_api_common_h_global_

#include "main.h"

#if 0
#include "common/Firmware_API_Common.h"
#include "common/common_system.h"
#include "common/global_data.h"

#include "util/cmd_logger_cwrap.h"
#include "util/sdk_macro.h"
#include "data-handlers/DAB_DLS_Handler.h"
#include "data-handlers/DAB_Service_List_Handler.h"
#include "data-handlers/FMHD_Service_List_Handler.h"
#include "data-handlers/RDS_Handler.h"
#include "HAL.h"
#include "mmi/SDK_Callbacks.h"
#include "receiver/IFirmware_API_Manager.h"
#include "platform/audio_cfg.h"
#include "receiver/Receiver_API.h"

#include "apis/si4690_dab_firmware_api.h"
#include "apis/si4690_dab_firmware_api_constants.h"
#include "apis/si46xx_dab_firmware_api_constants.h"
#include "apis/si4796x_firmware_api.h"
#include "apis/si4796x_tuner_api.h"
#include "apis/si4796x_tuner_api_constants.h"
#include "apis/si479xx_detail_status_api.h"
#include "apis/si479xx_detail_status_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "apis/si479xx_hub_api.h"
#include "apis/si479xx_hub_api_constants.h"
#endif	/*0*/

RETURN_CODE set_prop(uint8_t icNum, uint16_t prop_id, uint16_t value)
{
    return SI479XX_set_property(icNum, prop_id, value);
}

RETURN_CODE get_prop(uint8_t icNum, uint16_t prop_id, uint16_t* value)
{
    RETURN_CODE ret = SUCCESS;
    ret = SI479XX_get_property(icNum, 1, prop_id, value, 1);
    return ret;
}

#if 0
RETURN_CODE set_rds_properties(tuner_id tuner)
{
    RETURN_CODE ret = SUCCESS;
    //Setting properties for RDS data
    // FM_RDS_CONFIG
    // allows SOME errors + enables rds processing
    ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_FM_RDS_CONFIG_ADDR, 0x0001);

    // FM_RDS_INTERRUPT_FIFO_COUNT
    //number of RDS groups before FIFO interrupt raised
    ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_FM_RDS_INTERRUPT_FIFO_COUNT_ADDR, 5);

    // FM_RDS_INTERRUPT_SOURCE
    // setup interrupts for getting RDS data.  Only setting up tuner0,
    // could potentially setup both tuners and do fancier RDS handling.
    ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_FM_RDS_INTERRUPT_SOURCE_ADDR, 0x001F);

    return ret;
}

RETURN_CODE Tune__common(const fm_chips* chips, uint32_t freq_khz)
{
    RETURN_CODE ret = SUCCESS;

    if (!chips)
        return INVALID_INPUT;

    switch (g_radio_mode) {
    case RADIO_MODE_FM: {
        // Use 10kHz units
        uint16_t freq_10khz = freq_khz / 10;
        ret |= fm_tune(chips, &g_fm_state, freq_10khz);
        break;
    }

    case RADIO_MODE_AM: {
        // For now, DRM not used
        // use 1khz unit
        const demod_id* hd_demod = chips->hd_enabled ? &chips->hd_demod : NULL;
        ret |= am_tune(chips->tuner, hd_demod, freq_khz, false, 0, &g_am_state.metrics);
        break;
    }

    default:
        return INVALID_MODE;
    }

    // There is no wait for STC, though the tune is not considered valid until STC is set.
    return ret;
}

RETURN_CODE SeekStart__common(const fm_chips* fmam_chips, bool seekup, bool wrap, bool hdseekonly)
{
    if (!is_radio_mode_fmam(g_radio_mode))
        return INVALID_MODE;

    if (!fmam_chips)
        return INVALID_INPUT;

    if (fmam_chips->hd_enabled)
        hd_init_data();

    RETURN_CODE ret = SUCCESS;
    bool changed_freq = false;
    switch (g_radio_mode) {
    case RADIO_MODE_FM:
        ret |= fm_seek(fmam_chips, &g_fm_state, seekup, wrap, hdseekonly, &changed_freq);
        CALLBACK_Updated_Data(SCB_FM_SEEK_PROCESS_COMPLETE);
        break;

    case RADIO_MODE_AM: {
        // For now, DRM  not used.
        const demod_id* hd_demod = fmam_chips->hd_enabled ? &fmam_chips->hd_demod : NULL;
        ret |= am_seek(fmam_chips->tuner, hd_demod, &g_am_state, seekup, wrap, hdseekonly, false, &changed_freq);
        CALLBACK_Updated_Data(SCB_AM_SEEK_PROCESS_COMPLETE);
        break;
    }
    }

#ifdef OPTION_HANDLE_ADVANCED_SERVICES
    empty_adv_service_list();
#endif

    ret |= UpdateMetrics__common(fmam_chips, NULL, NULL);
    return ret;
}

RETURN_CODE get_si479xx_temp(dc_config_chip_select ic, int16_t* temp_c)
{
    RETURN_CODE ret = SUCCESS;
    if (!temp_c)
        return INVALID_INPUT;

    ret |= SI479XX_DETAIL_STATUS_die_temperature(ic, temp_c);

    return ret;
}

#endif	/*0*/

#if 0
RETURN_CODE UpdateMetrics__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, dab_system_data* dab)
{
    RETURN_CODE ret = SUCCESS;
    int16_t temp_c = 0;

    //need to know the xbar connection that we've set up in Initialize() to query the gain
    ret |= UpdateAudioMetrics(fmam_chips->tuner);

    ret |= get_si479xx_temp(IC0, &temp_c);
    g_board_metrics.chip_temps[0] = temp_c;

    //Dont ack the STC we check it to confirm tune/seek status
    switch (g_radio_mode) {
    case RADIO_MODE_FM:
        if (!fmam_chips)
            return INVALID_INPUT | ret;

        ret |= update_all_fm_metrics(fmam_chips, &g_fm_state, &g_hd_data);

        // If HD is enabled, we need to keep Demod and Tuner servos same.
        if (fmam_chips->hd_enabled)
            ret |= hd_update_demod_servos(fmam_chips->tuner, fmam_chips->hd_demod, RADIO_MODE_FM);

        break;

    case RADIO_MODE_AM:
        if (!fmam_chips)
            return INVALID_INPUT | ret;

        ret |= update_am_metrics(fmam_chips->tuner, &g_am_state.metrics, false, false, false);
        if (fmam_chips->hd_enabled) {
            ret |= update_hd_metrics(fmam_chips->hd_demod, &g_hd_data, &g_am_state.hd_metrics, &g_hd_data.svc_list);
            ret |= hd_update_demod_servos(fmam_chips->tuner, fmam_chips->hd_demod, RADIO_MODE_AM);
        }
        break;

    case RADIO_MODE_DAB:
        if (!dab_chips)
            return INVALID_INPUT | ret;

        ret |= get_si479xx_temp(dab_chips->tuner.ic, &temp_c);
        g_board_metrics.chip_temps[dab_chips->tuner.ic] = temp_c;
        ret |= update_dab_metrics(dab_chips, false, false, dab);
        break;
    }

    return ret;
}

RETURN_CODE UpdateServiceList__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, dab_system_data* dab, hd_system_data* hd)
{
    RETURN_CODE ret = SUCCESS;

    switch (g_radio_mode) {
    case RADIO_MODE_AM:
    case RADIO_MODE_FM: {
        HD_DEMOD_hd_get_event_status__data fges;

        if (!fmam_chips)
            return INVALID_INPUT;

        // Update HD service list if HD enabled.
        if (!fmam_chips->hd_enabled)
            return SUCCESS;

        // Do not ack as we may need it for SIS
        ret = hd_demod_hd_get_event_status(fmam_chips->hd_demod, false, &fges);
        if (ret == SUCCESS)
            ret = update_HD_service_list_worker(&hd->svc_list, fmam_chips->hd_demod, fges);

        break;
    }

    case RADIO_MODE_DAB:
        if (!dab_chips)
            return INVALID_INPUT;

        return DAB_event_handler(dab_chips->demod, dab);

    default:
        SDK_PRINTF("WARN: invalid call to UpdateServiceList_DAB()");
        return INVALID_MODE;
    }

    return ret;
}

RETURN_CODE UpdateDataServiceData__common(RADIO_MODE_TYPE mode, const fm_chips* fmam_chips, rds_decoder* rds, const dab_chips* dab_chips, hd_system_data* hd, dab_system_data* dab)
{
    RETURN_CODE ret = SUCCESS;

    if (is_radio_mode_fmam(mode) && !fmam_chips)
        return INVALID_INPUT;

    if (mode == RADIO_MODE_FM)
        ret |= update_fm_rds(fmam_chips->tuner, rds);

    if (is_radio_mode_fmam(mode) && fmam_chips->hd_enabled)
        ret |= UpdateDataServiceData_FMHD(fmam_chips->tuner, fmam_chips->hd_demod, hd);

    if (mode == RADIO_MODE_DAB) {
        demod_ints int_status = { 0 };
        ret |= demod_get_int_status(dab_chips->demod, &int_status);

        // Check for valid data in the data service interface
        //**************************************************************************
        if (int_status.DSRVINT) {
            DAB_DEMOD_get_digital_service_data__data gdsd = { 0 };
            do {
                // Clear DSRVINT
                ret |= dab_demod_get_digital_service_data(dab_chips->demod, false, true, &gdsd);
                // SDK_PRINTF("DIG SVC DATA:BUFF_COUNT=%d", gdsd.BUFF_COUNT);

                // Sometimes this loops gets in a state where the command to read digital service data always reports a command error.
                if ((gdsd.BYTE_COUNT > 0) && (ret == SUCCESS)) {
//Send to appropriate handler
//If PSD data we handle is seperately from the tracked service list
#ifdef OPTION_DECODE_DLS
                    // TODO current implementation will not work long term
                    // - as we need to handle other PAD data types, TDC, MOT, etc
                    if ((gdsd.SERVICE_ID == dab->current.service_id)
                        && (gdsd.COMP_ID == dab->current.component_id)) {
                        dls_update(&dab->dls_decoder, gdsd);
                        goto freeServiceData;
                    }
#endif //OPTION_DECODE_DLS

#ifdef OPTION_HANDLE_ADVANCED_SERVICES
                    // Check if there are digital services to update
                    ret |= adv_update(_service_list, &gdsd);
#endif
                }

            freeServiceData:
                free(gdsd.PAYLOAD);

            } while (ret == SUCCESS && gdsd.BUFF_COUNT > 0);
        }
    }

    return ret;
}

RETURN_CODE StartProcessingChannel__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, GENERAL_SERVICE_TYPE type, uint32_t service_id, uint32_t component_id, uint8_t* service_buffer, dab_system_data* dab, hd_system_data* hd)
{
    RETURN_CODE ret = SUCCESS;

    //Check if it is an audio or data service we need to start
    switch (type) {
    case SERVICE_TYPE_DAB_AUDIO:
        if (!dab_chips)
            return INVALID_INPUT;

        return DAB_start_processing_channel(dab_chips->demod, type, service_id, component_id, service_buffer, dab);

    default:
        if (!fmam_chips)
            return INVALID_INPUT;

        if (fmam_chips->hd_enabled) {
            ret = fmhd_start_processing(hd, fmam_chips->hd_demod, type, service_id, component_id, service_buffer);
            if (ret != UNSUPPORTED_FUNCTION)
                break;

#ifdef OPTION_HANDLE_ADVANCED_SERVICES
            ret = adv_start_processing(fmam_chips->hd_demod, type, service_id, component_id, service_buffer);
            if (ret != UNSUPPORTED_FUNCTION)
                break;
#endif // OPTION_HANDLE_ADVANCED_SERVICES
        }
        return INVALID_INPUT;
    }

    return ret;
}

RETURN_CODE StopProcessingChannel__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, GENERAL_SERVICE_TYPE type, uint32_t service_id, uint32_t component_id, hd_system_data* hd, dab_system_data* dab)
{
    RETURN_CODE ret = SUCCESS;

    switch (type) {
    case SERVICE_TYPE_DAB_AUDIO:
        if (!dab_chips)
            return INVALID_INPUT;

        // Check if it is the current audio service.
        if ((service_id == dab->current.service_id)
            && (component_id == dab->current.component_id)) {
            //End as an audio service
            ret = dab_demod_stop_digital_service(dab_chips->demod,
                SI461X_DAB_CMD_STOP_DIGITAL_SERVICE_ARG_SERTYPE_ENUM_AUDIO,
                service_id, component_id);

            dab->current.service_id = 0;
            dab->current.component_id = 0;
        }
        break;

    default:
        // Check fmhd service types
        if (!fmam_chips)
            return INVALID_INPUT;

        // Check if it is the current audio service.
        if ((service_id == hd->audio_sid)
            && (component_id == hd->audio_cid)) {
            //End as an audio service
            ret = dab_demod_stop_digital_service(fmam_chips->hd_demod,
                SI461X_DAB_CMD_STOP_DIGITAL_SERVICE_ARG_SERTYPE_ENUM_AUDIO,
                service_id, component_id);

            hd->audio_sid = 0;
            hd->audio_cid = 0;
        }

// Pass along, some other data service.
#ifdef OPTION_HANDLE_ADVANCED_SERVICES
        ret = adv_stop_processing(service_id, component_id);
#endif
    }

    return ret;
}
#endif	/*0*/

RETURN_CODE set_am_tune_defaults(tuner_id tuner)
{
    // These properties affect which frequencies are considered valid,
    //   and how quickly the check is performed.
    RETURN_CODE err = SUCCESS;

    // TIMER1 : duration before TIMER1_METRICS are checked.
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_AM_VALID_TIMER1_ADDR,
        SI4796X_TUNER_PROP_AM_VALID_TIMER1_DEFAULT);

    // TIMER1_METRICS: which metrics are checked, determine if frequency is VALID
    // e.g. check RSSI
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_AM_VALID_TIMER1_METRICS_ADDR,
        SI4796X_TUNER_PROP_AM_VALID_TIMER1_METRICS_RSSI_BIT);

    // setting the RSSI metric's threshold for VALID
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_AM_VALID_RSSI_THRESHOLD_ADDR,
        SI4796X_TUNER_PROP_AM_VALID_RSSI_THRESHOLD_DEFAULT);

    // Continue setting TIMER2, TIMER3 and associated metrics + thresholds.
    // ...

    // Using defaults, so not setting others.

    return err;
}

RETURN_CODE set_fm_tune_defaults(tuner_id tuner)
{
    // these properties affect which frequencies
    // are considered valid, and how quickly the check is performed.
    RETURN_CODE err = SUCCESS;

    // TIMER1 : duration before TIMER1_METRICS are checked.
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_FM_VALID_TIMER1_ADDR,
        5);

    // TIMER1_METRICS: which metrics are checked, determine if frequency is VALID
    // e.g. check RSSI
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_FM_VALID_TIMER1_METRICS_ADDR,
        SI4796X_TUNER_PROP_FM_VALID_TIMER1_METRICS_RSSI_BIT);

    // setting the RSSI metric's threshold for VALID
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_ADDR,
        SI4796X_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_DEFAULT);

    // Continue setting TIMER2, TIMER3 and associated metrics + thresholds.
    // ...

    // Using defaults, so not setting others.

    return err;
}

// Sets properties to a default setting for AF Tune or AF Check
RETURN_CODE set_fm_aftune_defaults(tuner_id tuner)
{
    RETURN_CODE err = SUCCESS;

    // AF_TIMER1 : duration before AF_TIMER1_METRICS are checked.
    // (usually faster than normal TIMER1)
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_FM_VALID_AF_TIMER1_ADDR,
        SI4796X_TUNER_PROP_FM_VALID_AF_TIMER1_DEFAULT);

    // AF_TIMER1_METRICS: which metrics are checked, determine if frequency is VALID
    // e.g. check RSSI
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_FM_VALID_AF_TIMER1_METRICS_ADDR,
        SI4796X_TUNER_PROP_FM_VALID_AF_TIMER1_METRICS_RSSI_BIT);

    // NOTE: unlike the TIMERs, the metric thresholds are the SAME for NORMAL and AF tunes.
    // setting the RSSI metric's threshold for VALID
    err |= set_tuner_prop(tuner,
        SI4796X_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_ADDR,
        SI4796X_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_DEFAULT);

    // Continue setting TIMER2, TIMER3 and associated metrics + thresholds.
    // ...

    // Using defaults, so not setting others.
    return err;
}

#define FORCE_MONO_SETTING 0
#define FORCE_STEREO_SETTING 50

#if 0		/* 210504 SGsoft */	
bool is_fm_mono(tuner_id tuner)
{
    RETURN_CODE ret = SUCCESS;
    SI4796X_TUNER_fm_get_servo__data servo_data;
    ret |= SI4796X_TUNER_fm_get_servo(tuner.ic, tuner.id,
        SI4796X_TUNER_CMD_FM_GET_SERVO_ARG_FM_SERVO_ID_ENUM_STBLEND, 
        &servo_data);
    if (ret != SUCCESS) {
        SDK_PRINTF("ERROR: failed to check if FM is MONO or STEREO.");
        return false;
    }

    return (servo_data.MAX == FORCE_MONO_SETTING);
}

RETURN_CODE force_mono_fm(tuner_id tuner, AUDIO_MONO_STEREO_SWITCH audio_MonStereo)
{
    RETURN_CODE ret = SUCCESS;
    SI4796X_TUNER_fm_get_servo__data servo_data;
    // User wishes to re-enable stereo - return the previous setting.
    ret |= SI4796X_TUNER_fm_get_servo(tuner.ic, tuner.id,
        SI4796X_TUNER_CMD_FM_GET_SERVO_ARG_FM_SERVO_ID_ENUM_STBLEND,
        &servo_data);
    if (ret != SUCCESS) {
        SDK_PRINTF("ERROR: Failed to get FM servo STBLEND status for forcing MONO/STEREO\n");
        return ret;
    }

    uint16_t max = audio_MonStereo == AUDIO_MONO ? FORCE_MONO_SETTING : FORCE_STEREO_SETTING;

    // set MAX to force MONO , or enable STEREO
    ret |= SI4796X_TUNER_fm_set_servo(tuner.ic, tuner.id,
        SI4796X_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_STBLEND,
        servo_data.MIN, max,
        servo_data.INIT, servo_data.ACCUMULATOR);

    return ret;
}

RETURN_CODE UpdateAudioMetrics(tuner_id tuner)
{
    RETURN_CODE ret = SUCCESS;

    ret |= audcfg_get_mute(&g_evb_system_cfg.audio, &g_board_metrics.audio.mute);
    ret |= audcfg_get_volume(&g_evb_system_cfg.audio, &g_board_metrics.audio.attenuation_db);

    g_board_metrics.audio.is_mono = is_fm_mono(tuner);

    return ret;
}

RETURN_CODE SetBandLimits_common(const fm_chips* chips, BAND_LIMITS limits)
{
    RETURN_CODE ret = SUCCESS;
    bool use_second_tuner = false;

    if (!chips)
        return INVALID_INPUT;

    use_second_tuner = chips->phase_div_enabled || chips->hd_div_enabled;
    ret = set_band_limit_properties(chips->tuner, limits);
    if (ret != SUCCESS)
        return ret;

    if (g_radio_mode == RADIO_MODE_AM)
        ret |= update_band_limits(chips->tuner, g_radio_mode, &g_am_state.band.band_top, &g_am_state.band.band_bottom);
    else
        ret |= update_band_limits(chips->tuner, g_radio_mode, &g_fm_state.band.band_top, &g_fm_state.band.band_bottom);

    if (use_second_tuner && g_radio_mode == RADIO_MODE_FM) {
        ret |= set_band_limit_properties(chips->div_tuner, limits);
    }

    return ret;
}

#define FM_SEEK_STEP_SIZE_CONVERSION_FACTOR 10

RETURN_CODE SetStepSize__common(const fm_chips* chips, uint8_t stepsize_khz)
{
    RETURN_CODE ret = SUCCESS;
    uint16_t step = 0;
    if (g_radio_mode == RADIO_MODE_FM) {
        step = (stepsize_khz / FM_SEEK_STEP_SIZE_CONVERSION_FACTOR);
        ret = set_tuner_prop(chips->tuner, SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR, step);
        if (chips->phase_div_enabled) {
            ret |= set_tuner_prop(chips->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR, step);
        }
        if (ret == SUCCESS)
            g_fm_state.band.band_step = step;
    } else if (g_radio_mode == RADIO_MODE_AM) {
        step = stepsize_khz;
        ret = set_tuner_prop(chips->tuner, SI4796X_TUNER_PROP_AM_SEEK_SPACING_ADDR, step);
        if (ret == SUCCESS)
            g_am_state.band.band_step = step;
    }

    return ret;
}

RETURN_CODE UpdateRadioDNS_FMRDS_common(radioDNS_fm* elements)
{
    UpdateMetrics();
    elements->FREQUENCY_10KHZ = g_fm_state.metrics.FREQUENCY_10KHZ;

    elements->PI = RDSGetProgramIdentifier();
    if (elements->PI == 0)
        return COMMAND_ERROR;

    elements->GCC = RDSGetECC();
    if (elements->GCC == 0)
        return COMMAND_ERROR;
    elements->GCC = (elements->GCC | ((elements->PI & 0xF000) >> 4));

    return SUCCESS;
}

//Calculates each of the pieces of the DAB RadioDNS string
//Note: This does not support the <apptype-uatype>.<pa>. variant
//
//String format: <scids>.<sid>.<eid>.<gcc>.dab.radiodns.org
//    - scids: in 1 character
//    - sid: in 4 characters (when the upper 16 bits are 0) or
//           in 8 characters (when the upper 16 bits are non-zero)
//    - eid: in 4 characters
//    - gcc: in 3 characters
RETURN_CODE UpdateRadioDNS_DAB(radioDNS_dab* elements)
{
    c_log_comment("UpdateRadioDNS_DAB()");

    RETURN_CODE ret = SUCCESS;

    if (g_radio_mode != RADIO_MODE_DAB)
        return INVALID_MODE;

    if (g_dab_state->current.service_id == 0)
        return SUCCESS;

    uint32_t tempGCC = 0;

    elements->SID = g_dab_state->current.service_id;

    DAB_DEMOD_dab_get_service_info__data dgsi = { 0 };
    ret |= dab_demod_dab_get_service_info(g_dab_cfg.demod, g_dab_state->current.service_id, &dgsi);

    DAB_DEMOD_dab_get_component_info__data dgci = { 0 };
    ret |= dab_demod_dab_get_component_info(g_dab_cfg.demod, g_dab_state->current.service_id,
        g_dab_state->current.component_id,
        &dgci);
    free(dgci.UADATA);
    dgci.UADATA = NULL;

    //Do ensemble info read last so the LABEL field
    //is still valid in the reply buffer (even though it isn't used)
    //Illustrative of proper usage of the api
    DAB_DEMOD_dab_get_ensemble_info__data dgei = { 0 };
    ret |= dab_demod_dab_get_ensemble_info(g_dab_cfg.demod, &dgei);

    if (ret == SUCCESS) {
        elements->SCIDS = dgci.GLOBAL_ID;
        elements->EID = dgei.EID;

        if ((g_dab_state->current.service_id & 0xFFFF0000) == 0) {
            //use the service ECC if available, otherwise use the ensemble ecc.
            if (dgsi.SRV_ECC != 0) {
                tempGCC = dgsi.SRV_ECC;
            } else {
                tempGCC = dgei.ENSEMBLE_ECC;
            }

            elements->GCC = (uint16_t)(tempGCC | ((g_dab_state->current.service_id & 0x0000F000) >> 4));
        } else {
            tempGCC = (((g_dab_state->current.service_id & 0x00F00000) >> 12) | (g_dab_state->current.service_id >> 24));
            elements->GCC = (uint16_t)tempGCC;
        }
    }

    return ret;
}

RETURN_CODE Test_GetBER_DAB(uint8_t berPattern, uint32_t* errorBits,
    uint32_t* totalBits, bool* test_passed)
{
    c_log_comment("Test_GetBER_DAB()");

    return DAB_test_get_ber(g_dab_cfg.demod, berPattern, errorBits, totalBits, test_passed);
}
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
