/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210416 SGsoft */
#define _r_4_1_global_data_h_global_
	
#include "main.h"

#if 0
#include "common/global_data.h"

#include "common/common_tuner.h"
#include "data-handlers/DAB_DLS_Handler.h"
#include "data-handlers/DAB_Service_List_Handler.h"
#include "data-handlers/FMHD_Alert_Handler.h"
#include "data-handlers/FMHD_PSD_Handler.h"
#include "data-handlers/FMHD_SIS_Handler.h"
#include "data-handlers/FMHD_Service_List_Handler.h"
#include "data-handlers/RDS_Handler.h"
#include "linker/linker.h"
#include "receiver/Receiver_API.h"

#include "core/c_util.h"
#include "util/sdk_macro.h"

#include "apis/si4796x_firmware_api.h"
#include "platform_options.h"
#endif	/*0*/

#include <string.h>


/***************************************************************************************
						Functions 
***************************************************************************************/
#if 0		/* 210505 SGsoft */	
static void empty_metrics_board()
{
    // Reset chip temps.
    uint8_t i = 0;
    for (i = 0; i < ARRAY_SZ(g_board_metrics.chip_temps); i++) {
        g_board_metrics.chip_temps[i] = CHIP_TEMP_UNKNOWN;
    }

    // Reset audio state.
    g_board_metrics.audio.attenuation_db = 0;
    g_board_metrics.audio.is_mono = false;
    g_board_metrics.audio.mute = AUDIO_MUTE_NOT_MUTED;
}

void dab_system_init(dab_system_data* d, sfc_handle* sfc, dab_service_list* sl)
{
    D_ASSERT(d && sfc && sl);

    d->sf_cache = sfc;
    d->svc_list = sl;
    dls_init(&d->dls_decoder);
    empty_dab_metrics(&d->metrics);
}
#endif	/*0*/

void global_data_init()
{
    // Static data should be initialized to 0, but just ensuring known initial values.
    #if 0		/* 210504 SGsoft */	
    empty_am_metrics(&g_am_state.metrics);
    empty_hd_metrics(&g_am_state.hd_metrics);
    empty_fm_metrics(&g_fm_state.metrics);
    empty_hd_metrics(&g_fm_state.hd_metrics);
    empty_fm_diversity_metrics(&g_fm_state.pd_metrics);
	#endif	/*0*/
    ///empty_metrics_board(&g_board_metrics);			/* 210505 SGsoft */	

	#if 0		/* 210504 SGsoft */	
    g_fm_state.rds = create_rds_decoder();
    rds_init(&g_fm_state.rds);
	#endif	/*0*/
	#if 0		/* 210427 SGsoft */
    alert_init(&g_hd_data.alert);
    hdsl_init(&g_hd_data.svc_list);
    psd_init(&g_hd_data.psd);
    sis_init(&g_hd_data.sis);
	#endif	/*0*/
	#if 0
    dabsl_init_list(&g_dab_svc_list);

    if (g_linker)
        linker_delete(g_linker);
    g_linker = linker_new();
	#endif	/*0*/

	#if 0		/* 210427 SGsoft */
    if (g_dab_sfc)
        sfc_delete(g_dab_sfc);
	#endif	/*0*/

// load service following cache if file exists
#if 0/*OPTION_STATEFUL*/		/* 210427 SGsoft */
    g_dab_sfc = sfc_read(ADD_RESOURCE_DIR("state/dab/saved-sfc.txt"));
#else
    ///g_dab_sfc = NULL;		/* 210504 SGsoft */	
#endif
	#if 0		/* 210427 SGsoft */
    if (g_dab_sfc == NULL) {
        SDK_PRINTF("Creating new service following cache.\n");
        g_dab_sfc = sfc_new();
    }
	#endif	/*0*/

	#if 0		/* 210504 SGsoft */	
    for (uint8_t i = 0; i < ARRAY_SZ(g_dab_states); i++)
        dab_system_init(&g_dab_states[i], g_dab_sfc, &g_dab_svc_list);

    g_dab_state = &g_dab_states[0];
	#endif	/*0*/
}

void global_data_finalize()
{
#if 0		/* 210504 SGsoft */	
    if (g_linker) {
        linker_delete(g_linker);
        g_linker = NULL;
    }

    if (SaveServiceList_DAB() != SUCCESS)
        SDK_PRINTF_SI("ERROR: failed to save DAB service list.\n");

#if OPTION_STATEFUL
    if (g_dab_sfc)
        sfc_write(g_dab_sfc, fstate_dab_service_following_cache);
    else
        SDK_PRINTF_SI("WARN: no DAB service following cache data to save.");
#else
    SDK_PRINTF_SI("service following cache not saved (OPTION_STATEFUL) ");
#endif
    if (g_dab_sfc) {
        sfc_delete(g_dab_sfc);
        g_dab_sfc = NULL;
    }

#ifdef OPTION_HANDLE_ADVANCED_SERVICES
    empty_adv_service_list();
#endif
#endif	/*0*/
}


#if 0		/* 210504 SGsoft */	
void empty_fm_system_metrics(fm_system_data* fm)
{
    if (!fm)
        return;

    empty_fm_metrics(&fm->metrics);
    empty_hd_metrics(&fm->hd_metrics);
    empty_fm_diversity_metrics(&fm->pd_metrics);
    empty_hd_mrc_metrics(&fm->mrc_metrics);
}

void empty_am_system_metrics(am_system_data* am)
{
    if (!am)
        return;
    empty_am_metrics(&am->metrics);
    empty_hd_metrics(&am->hd_metrics);
}

void empty_dab_system_metrics(dab_system_data* dab)
{
    if (!dab)
        return;
    empty_dab_metrics(&dab->metrics);
    empty_dab_mrc_metrics(&dab->mrc_metrics);
}

void empty_fm_metrics(fm_metrics* m)
{
    m->FREQUENCY_10KHZ = 0;
    m->MULTIPATH = 0;
    m->RSSI = 0;
    m->SNR = 0;
    m->VALID = 0;
}

void empty_hd_mrc_metrics(hd_mrc_metrics* m)
{
    m->acquired = false;
    m->comb_weight = 0;
    m->mode = 0;
    m->psmi = 0;
}

void empty_fm_diversity_metrics(fm_diversity_metrics* m)
{
    m->AUDIO = 0;
    m->COMB = 0;
    m->PHS_DIV_CORR = 0;

    m->diversity_metrics.FREQUENCY_10KHZ = 0;
    m->diversity_metrics.MULTIPATH = 0;
    m->diversity_metrics.RSSI = 0;
    m->diversity_metrics.SNR = 0;
    m->diversity_metrics.VALID = 0;
}

void empty_am_metrics(am_metrics* m)
{
    m->FREQUENCY_1KHZ = 0;
    m->MODULATION_INDEX = 0;
    m->RSSI = 0;
    m->SNR = 0;
    m->VALID = 0;
}

void empty_hd_metrics(hd_metrics* m)
{
    m->ACQ = 0;
    m->SRC_DIG = 0;
    m->CDNR = 0;
    m->DAAI = 0;
    m->HDLOGO = 0;
    //m->HDDETECTED = 0;
    m->CURRENT_AUDIO_SERVICE = 0;
    m->CURRENT_AUDIO_SERVICE__LIST_INDEX = 0xFF;
}

void empty_dab_metrics_tuner(dab_tuner_metrics* met)
{
    met->freq_index = 0xFF;
    met->sqi = 0;
    met->rssi = 0;
    met->rssi_adj = 0;
    met->dab_detect = 0;
    met->valid = 0;
}

void empty_dab_metrics_demod(dab_demod_metrics* met)
{
    memset(met, 0, sizeof(dab_demod_metrics));
}

void empty_dab_metrics(dab_metrics* metrics)
{
    empty_dab_metrics_tuner(&metrics->tuner);
    empty_dab_metrics_demod(&metrics->demod);

    metrics->current_svc_svclist_index = DAB_METRIC_AUDIO_SERVICE_LIST__INDEX_NOT_SET;
}

void empty_dab_mrc_metrics(dab_mrc_metrics* metrics)
{
    empty_dab_metrics_tuner(&metrics->div_tuner);
    metrics->acquired = 0;
    metrics->comb_weight = 0;
    metrics->dab_detect = 0;
    metrics->mode = 0;
}

bool is_radio_mode_fmam(RADIO_MODE_TYPE mode)
{
    switch (mode) {
    case RADIO_MODE_FM:
    case RADIO_MODE_AM:
        return true;

    default:
        return false;
    }
}
#endif	/*0*/

#if 0		/* 210504 SGsoft */	
const board_metrics* MetricsGetBoardPtr()
{
    return &g_board_metrics;
}

const hd_mrc_metrics* MetricsGetHdMrcPtr()
{
    return &g_fm_state.mrc_metrics;
}

const fm_diversity_metrics* MetricsGetFmDiversityPtr()
{
    return &g_fm_state.pd_metrics;
}

const fm_metrics* MetricsGetFmPtr()
{
    return &g_fm_state.metrics;
}

const uint8_t* RDSGetRadioTextPtr()
{
    return g_fm_state.rds.rt_display;
}

const uint8_t* RDSGetProgramServiceTextPtr()
{
    return g_fm_state.rds.ps_display;
}

uint16_t RDSGetProgramIdentifier(void)
{
    return g_fm_state.rds.pi_display;
}

bool RDSProgramIdentifierValid()
{
    return g_fm_state.rds.pi_display != 0;
}

uint8_t RDSGetProgramType(void)
{
    return g_fm_state.rds.pty_display;
}

#ifdef OPTION_RADIODNS
uint8_t RDSGetECC(void)
{
    return g_fm_state.rds.ecc_display;
}
#endif
rds_time RDSGetCurrentTime(void)
{
    return g_fm_state.rds.current_time;
}

void ConfigureRDSOptions(bool rds_ignore_AB_flag)
{
    rds_ignore_AB(&g_fm_state.rds, rds_ignore_AB_flag);
}

const am_metrics* MetricsGetAmPtr()
{
    return &g_am_state.metrics;
}

#ifdef OPTION_INCLUDE_MODE_AMHD
const hd_metrics* MetricsGetAmhdPtr()
{
    return &g_am_state.hd_metrics;
}
#endif

#ifdef OPTION_INCLUDE_MODE_FMHD
const hd_metrics* MetricsGetFmhdPtr()
{
    return &g_fm_state.hd_metrics;
}

const fmhd_service_list_fast* FMHDServiceListFastPtr()
{
    return &g_hd_data.svc_list.g_hd_svclist_fast;
}

const fmhd_service_list* FMHDServiceListAudioPtr()
{
    return &g_hd_data.svc_list.hd_svclist_audio;
}

#ifdef OPTION_DECODE_SIS
const fmhd_sis_slogan* SISGetSloganPtr()
{
    return &g_hd_data.sis.slgnDisplay;
}

const fmhd_sis_universal_short_name* SISGetUSNPtr()
{
    return &g_hd_data.sis.usnDisplay;
}

const fmhd_sis_station_message* SISGetSMPtr()
{
    return &g_hd_data.sis.smDisplay;
}

const uint8_t* SISGetSNLPtr()
{
    return g_hd_data.sis.snlDisplay;
}

const uint8_t* SISGetSNSPtr()
{
    return g_hd_data.sis.snsDisplay;
}

const uint8_t* SISGetStationIDPtr()
{
    return g_hd_data.sis.stationID;
}

const uint8_t* SISGetStationLocLatPtr()
{
    return g_hd_data.sis.stationLocationLat;
}

const uint8_t* SISGetStationLocLongPtr()
{
    return g_hd_data.sis.stationLocationLong;
}
#endif //OPTION_DECODE_SIS

#ifdef OPTION_DECODE_PSD
const psd_decoder* PSDGetPtr()
{
    return &g_hd_data.psd;
}

#endif //OPTION_DECODE_PSD

#ifdef OPTION_DECODE_HD_ALERTS
const fmhd_alert_string* HDAlertGetPtr()
{
    return &g_hd_data.alert.display;
}

#endif //OPTION_DECODE_HD_ALERTS

#endif //OPTION_INCLUDE_MODE_FMHD

#ifdef OPTION_INCLUDE_MODE_DAB
dab_service_list* DABServiceListAudioPtr()
{
    return g_dab_state->svc_list;
}

const dab_metrics* MetricsGetDabPtr()
{
    return &g_dab_state->metrics;
}

const dab_mrc_metrics* MetricsGetDabMrcPtr()
{
    return &g_dab_state->mrc_metrics;
}
#ifdef OPTION_DECODE_DLS
const dab_dls_string* DLSGetPtr()
{
    return &g_dab_state->dls_decoder;
}

#endif //OPTION_DECODE_DLS

#endif //OPTION_INCLUDE_MODE_DAB
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
