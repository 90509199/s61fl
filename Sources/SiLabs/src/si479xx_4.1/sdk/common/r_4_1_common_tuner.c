/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210416 SGsoft */
#define _r_4_1_common_tuner_h_global_
	
#include "main.h"

#if 0
#include "common/common_tuner.h"
#include "common/Firmware_API_Common.h"
#include "common/HD_common.h"
#include "common/global_data.h"

#include "mmi/SDK_Callbacks.h"
#include "platform/audio_cfg.h"
#include "receiver/IFirmware_API_Manager.h"
#include "receiver/Receiver_API.h"

#include "data-handlers/DAB_Service_List_Handler.h"
#include "data-handlers/FMHD_Service_List_Handler.h"
#include "data-handlers/RDS_Handler.h"

#include "HAL.h"
#include "core/c_util.h"
#include "osal/misc.h"
#include "util/cmd_logger_cwrap.h"
#include "util/sdk_macro.h"

#include "apis/hd_demod_commands_wrapper.h"
#include "apis/si4690_dab_firmware_api.h"
#include "apis/si4690_dab_firmware_api_constants.h"
#include "apis/si4796x_firmware_api.h"
#include "apis/si4796x_firmware_api_constants.h"
#include "apis/si4796x_tuner_api.h"
#include "apis/si4796x_tuner_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "apis/si479xx_hub_api.h"
#include "apis/si479xx_hub_api_constants.h"
#endif	/*0*/

void copy_fm_metrics(fm_metrics* fm, SI4796X_TUNER_fm_rsq_status__data* data)
{
    if (fm == NULL || data == NULL)
        return;
    fm->FREQUENCY_10KHZ = data->FREQ;
    fm->MULTIPATH = data->MULTIPATH;
    fm->RSSI = data->RSSI;
    fm->SNR = data->SNR;
    fm->VALID = data->VALID;
    fm->HD_LEVEL = data->HDLEVEL;
}

static void copy_am_metrics(am_metrics* am, SI4796X_TUNER_am_rsq_status__data* data)
{
    if (am == NULL || data == NULL)
        return;

    am->FREQUENCY_1KHZ = data->FREQ;
    am->RSSI = data->RSSI;
    am->SNR = data->SNR;
    am->MODULATION_INDEX = data->MODINDEX;
    am->VALID = data->VALID;
}

RETURN_CODE si479xx_enable_ctsint(uint8_t ic)
{
    uint16_t propval = 0;
    SET_PROP_ENUM(propval, SI479XX_PROP_INTERRUPT_ENABLE0_CTSIEN, ENABLED);
    return set_prop(ic, SI479XX_PROP_INTERRUPT_ENABLE0_ADDR, propval);
}

SI479XX_read_status__data GetSi479XXStatus(uint8_t icNum)
{
    RETURN_CODE ret = SUCCESS;
    SI479XX_read_status__data status;

    ret = SI479XX_read_status(icNum, &status);
    D_ASSERT_MSG(ret == SUCCESS, "GetSi479XXStatus() error=0x%x\n", ret);

    return status;
}

SI4796X_read_status__data GetSi4796XStatus(uint8_t icNum)
{
    RETURN_CODE ret = SUCCESS;
    SI4796X_read_status__data status;

    ret = SI4796X_read_status(icNum, &status);
    D_ASSERT_MSG(ret == SUCCESS, "GetSi479XXStatus() error=0x%x\n", ret);

    return status;
}

#if 0		/* 210505 SGsoft */	
RETURN_CODE set_hd_iq(dc_config_chip_select ic, uint8_t slotsize_enum, uint8_t rate_enum)
{
    uint16_t propval = 0;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ENABLE_MODE_SPECIFIC, FIXED_RATES);
    SET_PROP_FIELD(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE, slotsize_enum);
    SET_PROP_FIELD(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_RATE, rate_enum);
    return set_prop(ic, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ADDR, propval);
}

RETURN_CODE set_dab_drm_iq(dc_config_chip_select ic)
{
    RETURN_CODE r = SUCCESS;
    // Set tuner to use mode-specific IQ as DRM example
#if 0
    uint16_t propval = SI479XX_PROP_EZIQ_FIXED_SETTINGS_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ENABLE_MODE_SPECIFIC, MODE_SPECIFIC);
    r = set_prop(ic, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ADDR, propval);
#else
    // TODO - currently DAB MRC uses FIXED options even when MODE_SPECIFIC set,
    // so we need to set the DAB options here.
    uint16_t propval = SI479XX_PROP_EZIQ_FIXED_SETTINGS_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ENABLE_MODE_SPECIFIC, MODE_SPECIFIC);
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_RATE,IQ2048KS);
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE,EIGHTEEN);
    r = set_prop(ic, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ADDR, propval);
#endif

    // No need for I/Q in FM mode
    propval = SI479XX_PROP_EZIQ_ZIF_FM_CONFIG0_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_FM_CONFIG0_ENABLE, DISABLE);
    r |= set_prop(ic, SI479XX_PROP_EZIQ_ZIF_FM_CONFIG0_ADDR, propval);

    // Set DRM rates for AM mode
    propval = SI479XX_PROP_EZIQ_ZIF_AM_CONFIG0_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_AM_CONFIG0_ENABLE, ENABLE);
    r |= set_prop(ic, SI479XX_PROP_EZIQ_ZIF_AM_CONFIG0_ADDR, propval);

    propval = SI479XX_PROP_EZIQ_ZIF_AM_CONFIG1_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_AM_CONFIG1_SLOTSIZE, SIXTEEN);
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_AM_CONFIG1_SAMPLERATE, IQ192KS);
    r |= set_prop(ic, SI479XX_PROP_EZIQ_ZIF_AM_CONFIG1_ADDR, propval);

    // Set DAB rates for DAB mode
    propval = SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG0_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG0_ENABLE, ENABLE);
    r |= set_prop(ic, SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG0_ADDR, propval);

    propval = SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG1_DEFAULT;
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG1_SLOTSIZE, EIGHTEEN);
    SET_PROP_ENUM(propval, SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG1_SAMPLERATE, IQ2048KS);
    r |= set_prop(ic, SI479XX_PROP_EZIQ_ZIF_DAB_CONFIG1_ADDR, propval);

    return r;
}
#endif	/*0*/


RETURN_CODE set_zif_port(dc_config_chip_select ic, uint8_t enable, uint8_t src)
{
    // Modify desired zif port
    uint16_t val = SI479XX_PROP_EZIQ_OUTPUT_SOURCE_DEFAULT;
	SET_PROP_FIELD(val, SI479XX_PROP_EZIQ_OUTPUT_SOURCE_ZIF0_ENABLE, enable);
	SET_PROP_FIELD(val, SI479XX_PROP_EZIQ_OUTPUT_SOURCE_ZIF0_SOURCE, src);
    return set_prop(ic, SI479XX_PROP_EZIQ_OUTPUT_SOURCE_ADDR, val);
}


RETURN_CODE fm_chips_init(const fm_chips* c, RADIO_MODE_TYPE boot_region)
{
    D_ASSERT(c);
    RETURN_CODE r = SUCCESS;

    r |= set_mode(c->tuner, RADIO_MODE_FM);
    if (c->phase_div_enabled || c->hd_div_enabled)
        r |= set_mode(c->div_tuner, RADIO_MODE_FM);

    r |= set_am_tune_defaults(c->tuner);
    r |= set_fm_tune_defaults(c->tuner);
    r |= set_rds_properties(c->tuner);

    if (boot_region == RADIO_MODE_DAB) {
        // DAB region
        // FM spacing usually 0.1 MHz
        r |= set_tuner_prop(c->tuner, SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR, 10);
        r |= set_tuner_prop(c->tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR, 8750);
        r |= set_tuner_prop(c->tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR, 10800);
    } else {
        // HD region
        // FM spacing usually 0.2 MHz
        r |= set_tuner_prop(c->tuner, SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR, 20);
        // r |= set_tuner_prop(c->tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR, 8750);
        // r |= set_tuner_prop(c->tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR, 10800);
    }

    if (c->phase_div_enabled || c->hd_div_enabled) {
        r |= set_am_tune_defaults(c->div_tuner);
        r |= set_fm_tune_defaults(c->div_tuner);
        r |= set_rds_properties(c->div_tuner);

        if (boot_region == RADIO_MODE_DAB) {
            // DAB region
            // FM spacing usually 0.1 MHz
            r |= set_tuner_prop(c->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR, 10);
            r |= set_tuner_prop(c->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR, 8750);
            r |= set_tuner_prop(c->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR, 10800);
        } else {
            // HD region
            // FM spacing usually 0.2 MHz
            r |= set_tuner_prop(c->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR, 20);
            // r |= set_tuner_prop(c->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR, 8750);
            // r |= set_tuner_prop(c->div_tuner, SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR, 10800);
        }
    }

    return r;
}

RETURN_CODE set_tuner_antennae(dc_config_chip_select ic, TUNER_SUBCMD_ID id, ANTENNA fm, ANTENNA b3)
{
    uint16_t addr, val;

    addr = SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_ADDR;
    addr += id;

    uint16_t fm_field, b3_field;
    switch (fm) {
    case ANT_A:
        fm_field = SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_FMT0_ENUM_FMA;
        break;
    case ANT_B:
        fm_field = SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_FMT0_ENUM_FMB;
        break;
    default:
        return INVALID_INPUT;
    }

    switch (b3) {
    case ANT_A:
        b3_field = SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_B3T0_ENUM_B3A;
        break;
    case ANT_B:
        b3_field = SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_B3T0_ENUM_B3B;
        break;
    default:
        return INVALID_INPUT;
    }

    // assumes defaults are same for both tuner props
    val = SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_DEFAULT;
    SET_PROP_FIELD(val, SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_FMT0, fm_field);
    SET_PROP_FIELD(val, SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_B3T0, b3_field);

    return set_prop(ic, addr, val);
}

RETURN_CODE set_tuner_prop(tuner_id tuner, uint16_t prop_id, uint16_t value)
{
    if (tuner.id == TUNER_ID_INVALID) {
        return SI4796X_set_property(tuner.ic, prop_id, value);
    } else {
        return SI4796X_TUNER_set_property(tuner.ic, tuner.id, prop_id, value);
    }
}

RETURN_CODE get_tuner_prop(tuner_id tuner, uint16_t prop_id, uint16_t* value)
{
    RETURN_CODE ret = SUCCESS;
    if (tuner.id == TUNER_ID_INVALID) {
        ret = SI4796X_get_property(tuner.ic, 1, prop_id, value, 1);
    } else {
        ret = SI4796X_TUNER_get_property(tuner.ic, tuner.id, 1, prop_id, value, 1);
    }
    return ret;
}

RETURN_CODE set_mode(tuner_id tuner, uint8_t mode)
{
    RETURN_CODE ret = SUCCESS;
    ret |= SI4796X_TUNER_set_mode(tuner.ic, tuner.id, mode);
    ret |= wait_for_stc(tuner, STC_DEFAULT_TIMEOUT_MS);
    return ret;
}

#if 0		/* 210505 SGsoft */	
RETURN_CODE update_fm_rds(tuner_id tuner, rds_decoder* rds)
{
    RETURN_CODE ret = SUCCESS;

    // Update RDS if present
    SI4796X_read_status__data status = GetSi4796XStatus(tuner.ic);
    if (status.RDSINT) {
        // Prevents infinite loops if rds_update() is slower than RDS blocks are received.
        const uint32_t max_blocks_read = 25;
        uint32_t blocks_read = 0;
        SI4796X_TUNER_fm_rds_status__data frs;
        do {
            ret |= SI4796X_TUNER_fm_rds_status(tuner.ic, tuner.id, false, false, true, &frs);

            if (ret != SUCCESS)
                return ret;

            if (frs.RDSFIFOUSED > 0)
                rds_update(rds, frs);

            // SDK_PRINTF("RDSFIFOUSED: %d\n", frs.RDSFIFOUSED);
            // c_log_commentf("RDSFIFOUSED: %d", frs.RDSFIFOUSED);

            blocks_read++;
            if (blocks_read >= max_blocks_read) {
                c_log_commentf("Did not parse all RDS blocks. Parsed: %d, Remaining: %d\n", max_blocks_read, frs.RDSFIFOUSED);
                break;
            }
        } while (frs.RDSFIFOUSED > 0);
    }

    return ret;
}

RETURN_CODE set_band_limit_properties(tuner_id tuner, BAND_LIMITS limits)
{
    RETURN_CODE ret = SUCCESS;

    switch (limits) {
    case FM_JAPAN_76_90:
        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR,
            7600);

        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR,
            9000);
        break;

    case FM_STANDARD_875_1079:
        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR,
            8750);

        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR,
            10790);

        break;

    case FM_WIDE_78_108:
        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR,
            7800);

        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR,
            10800);
        break;

    case AM_DEFAULT:
        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_AM_SEEK_BAND_BOTTOM_ADDR,
            SI4796X_TUNER_PROP_AM_SEEK_BAND_BOTTOM_DEFAULT);

        ret |= set_tuner_prop(tuner,
            SI4796X_TUNER_PROP_AM_SEEK_BAND_TOP_ADDR,
            SI4796X_TUNER_PROP_AM_SEEK_BAND_TOP_DEFAULT);
        break;

    default:
        ret = INVALID_INPUT;
        break;
    }
    return ret;
}

RETURN_CODE update_band_limits(tuner_id tuner, RADIO_MODE_TYPE band_type, uint16_t* band_top, uint16_t* band_bottom)
{
    RETURN_CODE ret = SUCCESS;
    uint16_t top_addr, bot_addr;

    if (!band_top || !band_bottom)
        return INVALID_INPUT;

    switch (band_type) {
    case RADIO_MODE_AM:
        top_addr = SI4796X_TUNER_PROP_AM_SEEK_BAND_TOP_ADDR;
        bot_addr = SI4796X_TUNER_PROP_AM_SEEK_BAND_BOTTOM_ADDR;
        break;

    case RADIO_MODE_FM:
        top_addr = SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR;
        bot_addr = SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR;
        break;

    default:
        return INVALID_INPUT;
    }

    ret |= get_tuner_prop(tuner, top_addr, band_top);
    ret |= get_tuner_prop(tuner, bot_addr, band_bottom);

    return ret;
}

RETURN_CODE fm_band_scan_check(tuner_id tuner, bool pi_only, fm_scan_result* result, bool* stop, bool* added, cb_fm_scan_result cb_freq)
{
    RETURN_CODE ret = SUCCESS;
    if (!added || !result || !stop)
        return INVALID_INPUT;

    *added = false;

    SI4796X_TUNER_fm_rsq_status__data rsq = { 0 };
    ret = SI4796X_TUNER_fm_rsq_status(tuner.ic, tuner.id, false, false, true, &rsq);
    if (ret != SUCCESS)
        return ret;

    uint8_t quality = fm_calc_quality(&rsq);
    result->freq_10khz = rsq.FREQ;
    result->quality = quality;
    if (pi_only) {
        SI4796X_TUNER_fm_rds_status__data rds = { 0 };
        ret = fm_rds_pi_poll(tuner, 100, &rds);
        if (ret != SUCCESS) {
            // failed to get PI code
            *added = false;
        } else if (rds.PIVALID) {
            result->pi_valid = true;
            result->pi_code = rds.PI;
            *added = true;
        }
    } else if (rsq.VALID) {
        *added = true;
    }

    // callback
    if (*added && cb_freq)
        *stop = cb_freq(*result);

    // hit end of band, scan complete.
    if (rsq.BLTF)
        *stop = true;

    return SUCCESS;
}

RETURN_CODE fm_band_scan(tuner_id tuner, bool pi_only, uint16_t band_bottom, uint8_t freq_count, fm_scan_result* results, uint8_t* freq_added, cb_fm_scan_result cb_new_freq)
{
    if (!results || !freq_added)
        return INVALID_INPUT;

    *freq_added = 0;
    // initial tune to band bottom, scan up.
    RETURN_CODE ret = SUCCESS;
    ret = fm_tune_single(tuner, band_bottom);
    if (ret != SUCCESS)
        return ret;

    uint8_t num_freqs_found = 0;
    bool added = false;
    bool stop = false;
    while (true) {
        // check current frequency
        ret = fm_band_scan_check(tuner, pi_only, &results[num_freqs_found], &stop, &added, cb_new_freq);
        if (ret != SUCCESS)
            return ret;

        if (added) {
            num_freqs_found++;
            *freq_added = num_freqs_found;
            if (num_freqs_found == freq_count)
                return SUCCESS;
        }

        if (stop)
            return SUCCESS;

        // continue scanning
        ret = SI4796X_TUNER_fm_seek_start(tuner.ic, tuner.id, true, false, SI4796X_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_SEARCH);
        ret |= wait_for_stc(tuner, 5000);
        if (ret != SUCCESS)
            return ret;
    }

    return SUCCESS;
}

RETURN_CODE fm_phase_diversity(dc_config_chip_select ic, FMPD_AUDIO audio_source, bool combine)
{
    return SI4796X_fm_phase_diversity(ic, audio_source, combine);
}

// Tunes, starts RDS, and updates metrics.
RETURN_CODE am_tune(tuner_id tuner, const demod_id* hd_demod, uint16_t freq, bool drm_tune, int16_t drm_offset, am_metrics* metrics)
{
    RETURN_CODE ret = SUCCESS;

    // TODO - add drm offset ?
    ret = SI4796X_TUNER_am_tune_freq(tuner.ic, tuner.id,
        drm_tune,
        SI4796X_TUNER_CMD_AM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        SI4796X_TUNER_CMD_AM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        SI4796X_TUNER_CMD_AM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
        freq);
    ret |= wait_for_stc(tuner, STC_DEFAULT_TIMEOUT_MS);

    if (hd_demod) {
        hd_init_data();
        // Not checking if actually acquired.
        ret |= hd_acquire(*hd_demod, freq, 0, NULL);
        CLEAR_BITS(ret, TIMEOUT);
    }

    if (metrics) {
        empty_am_metrics(metrics);
        if (ret == SUCCESS)
            metrics->FREQUENCY_1KHZ = freq;
    }

    return ret;
}

RETURN_CODE am_seek(tuner_id tuner, const demod_id* hd_demod, am_system_data* am, bool seekup, bool wrap, bool digital_only, bool drm, bool* changed_freq)
{
    if (!am)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    uint8_t search_mode;
    uint16_t orig_freq = 0, curr_freq = 0;

    am->continue_seek = true;
    if (digital_only && hd_demod) {
        search_mode = SI4796X_TUNER_CMD_AM_SEEK_START_ARG_MODE_ENUM_DIGSEARCH;
    } else {
        search_mode = SI4796X_TUNER_CMD_AM_SEEK_START_ARG_MODE_ENUM_SEARCH;
    }

    ret |= get_am_tuned_freq(tuner, &orig_freq);
    ret |= SI4796X_TUNER_am_seek_start(tuner.ic, tuner.id,
        seekup, drm, wrap, search_mode);
    ret |= am_confirm_seek_complete(tuner, &am->continue_seek);
    empty_am_metrics(&am->metrics);
    am->continue_seek = false;

    ret |= get_am_tuned_freq(tuner, &curr_freq);
    *changed_freq = (curr_freq != orig_freq);

    if (hd_demod) {
        // Not checking if actually acquired.
        ret |= hd_acquire(*hd_demod, curr_freq, 0, NULL);
        CLEAR_BITS(ret, TIMEOUT);
    }

    return ret;
}

// Tunes, starts RDS, and updates metrics.
RETURN_CODE fm_tune(const fm_chips* chips, fm_system_data* fm, uint16_t freq_10khz)
{
    if (!chips)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    bool has_diversity = chips->phase_div_enabled || chips->hd_div_enabled;

    // Checking for invalid combinations of inputs.
    if (chips->hd_div_enabled && !chips->hd_enabled)
        return INVALID_INPUT;

    // Listen to diversity tuner while tuning primary.
    if (chips->hd_div_enabled) {
        ret |= SI4796X_fmhd_diversity(chips->div_tuner.ic, SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_ENUM_TUNER1);
    }
    if (chips->phase_div_enabled)
        ret |= fm_phase_diversity(chips->tuner.ic, FMPD_DIVERSITY, false);

    // Tune primary to new station.
    ret |= fm_tune_single(chips->tuner, freq_10khz);

    // Listen to primary while tuning diversity.
    if (chips->phase_div_enabled)
        ret |= fm_phase_diversity(chips->tuner.ic, FMPD_PRIMARY, false);

    if (chips->hd_div_enabled) {
        ret |= SI4796X_fmhd_diversity(chips->div_tuner.ic, SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_ENUM_TUNER0);
    }

    if (has_diversity) {
        // Checking primary tuner freq (possibly does not change)
        SI4796X_TUNER_fm_rsq_status__data primary;
        ret |= SI4796X_TUNER_fm_rsq_status(chips->tuner.ic, chips->tuner.id, false, false, false, &primary);

        ret |= SI4796X_TUNER_fm_tune_freq(chips->div_tuner.ic, chips->div_tuner.id,
            SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_FAST,
            SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
            SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
            primary.FREQ);
        ret |= wait_for_stc(chips->div_tuner, STC_DEFAULT_TIMEOUT_MS);
    }

    // Reengage the diversity combining
    if (chips->phase_div_enabled)
        ret |= fm_phase_diversity(chips->tuner.ic, FMPD_PRIMARY, true);

    if (chips->hd_div_enabled) {
        ret |= SI4796X_fmhd_diversity(chips->div_tuner.ic, SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_ENUM_COMBINE);
        ret |= SI4796X_fmhd_diversity_acquire(chips->div_tuner.ic);
    }

    if (fm)
        empty_fm_system_metrics(fm);

    if (chips->hd_enabled) {
        // Reacquire HD, not checking if acquired
        hd_init_data();
        ret |= hd_acquire(chips->hd_demod, freq_10khz, 0, NULL);
        CLEAR_BITS(ret, TIMEOUT);
    }

    if (fm && ret == SUCCESS) {
        //Force metrics for fast switching
        fm->metrics.FREQUENCY_10KHZ = freq_10khz;
        rds_init(&fm->rds);
    }

    return ret;
}

RETURN_CODE get_am_tuned_freq(tuner_id tuner, uint16_t* current_freq)
{
    if (current_freq == NULL)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    SI4796X_TUNER_am_rsq_status__data rsq = { 0 };
    ret = SI4796X_TUNER_am_rsq_status(tuner.ic, tuner.id,
        SI4796X_TUNER_CMD_AM_RSQ_STATUS_ARG_ATTUNE_ENUM_CURRENT,
        false, false, &rsq);

    *current_freq = rsq.FREQ;
    return ret;
}

RETURN_CODE get_fm_tuned_freq(tuner_id tuner, uint16_t* current_freq)
{
    RETURN_CODE ret = SUCCESS;
    SI4796X_TUNER_fm_rsq_status__data rsq;

    if (current_freq == NULL)
        return INVALID_INPUT;

    ret |= SI4796X_TUNER_fm_rsq_status(tuner.ic, tuner.id,
        SI4796X_TUNER_CMD_FM_RSQ_STATUS_ARG_ATTUNE_ENUM_CURRENT,
        false, false, &rsq);

    *current_freq = rsq.FREQ;
    return ret;
}

RETURN_CODE fm_confirm_seek_complete(tuner_id tuner, bool* seek_continue)
{
    RETURN_CODE ret = SUCCESS;
    uint16_t timeout = OPTION_FM_TIMEOUT_SEEK_MS;
    while (!is_seek_tune_complete(tuner)) {
        if (timeout < OPTION_FM_SEEK_CALLBACK_RATE_MS)
            return TIMEOUT;

        os_delay(OPTION_FM_SEEK_CALLBACK_RATE_MS);
        timeout -= OPTION_FM_SEEK_CALLBACK_RATE_MS;

        // Update metrics so callback can print most recent info
        ret = UpdateMetrics();
        if (ret != SUCCESS)
            return ret;

        // Hands off control to the MMI code for UI update (button checks) - opportunity to cancel the scan
        CALLBACK_Updated_Data(SCB_FM_SEEK_PROCESS_UPDATE);
        if (seek_continue && (*seek_continue == false)) {
            SDK_PRINTF("User cancelled FM seek.\n");
            return ret;
        }
    }

    ret |= UpdateMetrics();
    return ret;
}

RETURN_CODE fm_seek_tuner(tuner_id tuner, fm_system_data* fm,
    bool seekup, bool wrap, uint8_t seek_mode, bool* freq_has_changed, uint16_t* new_freq)
{
    RETURN_CODE ret = SUCCESS;
    uint16_t orig_freq;

    if (!freq_has_changed || !new_freq || !fm)
        return INVALID_INPUT;

    ret |= get_fm_tuned_freq(tuner, &orig_freq);
    ret |= SI4796X_TUNER_fm_seek_start(tuner.ic, tuner.id, seekup, wrap, seek_mode);

    fm->continue_seek = true;
    ret |= fm_confirm_seek_complete(tuner, &fm->continue_seek);
    fm->continue_seek = false;

    // updating outputs.
    ret |= get_fm_tuned_freq(tuner, new_freq);
    *freq_has_changed = (*new_freq != orig_freq);
    if (*freq_has_changed) {
        empty_fm_system_metrics(fm);
        rds_init(&fm->rds);
    }

    return ret;
}

RETURN_CODE fm_rds_pi_poll(tuner_id tuner, uint16_t timeout_ms, SI4796X_TUNER_fm_rds_status__data* rds)
{
    RETURN_CODE ret = SUCCESS;

    if (!rds)
        return INVALID_INPUT;

    ClearMemory(rds, sizeof(*rds));
    // Validate current station.
    SI4796X_TUNER_fm_rsq_status__data rsq;
    ret |= SI4796X_TUNER_fm_rsq_status(tuner.ic, tuner.id, false, false, true, &rsq);
    if (ret != SUCCESS)
        return ret;

    if (rsq.VALID == 0) {
        // must have valid frequency for RDS
        return INVALID_MODE;
    }

    uint32_t wait = 0;
    while (true) {
        ret |= SI4796X_TUNER_fm_rds_status(tuner.ic, tuner.id, true, false, true, rds);
        if (ret != SUCCESS)
            return ret;

        if (rds->PIVALID) {
            // SDK_PRINTF("PIVALID in %d ms\n", wait);
            return SUCCESS;
        }

        if (wait >= timeout_ms)
            return TIMEOUT;

        os_delay(1);
        wait++;
    }
    return ret;
}

RETURN_CODE fm_tune_single(tuner_id fm_tuner, uint16_t freq_10khz)
{
    RETURN_CODE ret = SUCCESS;
    ret = SI4796X_TUNER_fm_tune_freq(fm_tuner.ic, fm_tuner.id,
        SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
        freq_10khz);
    ret |= wait_for_stc(fm_tuner, STC_DEFAULT_TIMEOUT_MS);
    return ret;
}

RETURN_CODE fm_seek(const fm_chips* chips, fm_system_data* fm, bool seekup, bool wrap, bool hdseek_only, bool* changed_freq)
{
    if (!changed_freq || !chips || !fm)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    uint16_t new_freq = 0;
    uint8_t seek_mode = 0;
    bool has_diversity = chips->phase_div_enabled || chips->hd_div_enabled;

    seek_mode = (chips->hd_enabled && hdseek_only) ? SI4796X_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_HDSEARCH : SI4796X_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_SEARCH;

    // Listen to the second tuner while seeking on first.
    if (chips->hd_div_enabled) {
        ret |= SI4796X_fmhd_diversity(chips->div_tuner.ic, SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_ENUM_TUNER0);
    }
    if (chips->phase_div_enabled)
        ret |= fm_phase_diversity(chips->tuner.ic, FMPD_DIVERSITY, false);

    // Seek for new frequency on primary tuner.
    ret |= fm_seek_tuner(chips->tuner, fm, seekup, wrap, seek_mode, changed_freq, &new_freq);

    // Switch audio to primary
    if (chips->phase_div_enabled)
        ret |= fm_phase_diversity(chips->tuner.ic, FMPD_PRIMARY, false);
    if (chips->hd_div_enabled) {
        ret |= SI4796X_fmhd_diversity(chips->div_tuner.ic, SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_ENUM_TUNER0);
    }

    // Retune diversity if necessary.
    if (*changed_freq && has_diversity) {
        ret |= SI4796X_TUNER_fm_tune_freq(chips->div_tuner.ic, chips->div_tuner.id,
            SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_FAST,
            SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
            SI4796X_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
            new_freq);
        ret |= wait_for_stc(chips->div_tuner, STC_DEFAULT_TIMEOUT_MS);
    }
    // Recombine
    if (chips->phase_div_enabled)
        ret |= fm_phase_diversity(chips->tuner.ic, FMPD_PRIMARY, true);
    if (chips->hd_div_enabled) {
        ret |= SI4796X_fmhd_diversity(chips->div_tuner.ic, SI4796X_CMD_FMHD_DIVERSITY_ARG_MODE_ENUM_COMBINE);
        ret |= SI4796X_fmhd_diversity_acquire(chips->div_tuner.ic);
    }
    if (chips->hd_enabled) {
        // Reacquire, not checking for ACQ
        hd_init_data();
        ret |= hd_acquire(chips->hd_demod, new_freq, 0, NULL);
        CLEAR_BITS(ret, TIMEOUT);
    }

    return ret;
}
#endif	/*0*/

RETURN_CODE wait_for_stc(tuner_id tuner, uint32_t timeout_ms)
{
    const uint32_t delay_ms = 5;
    uint32_t remaining_ms = timeout_ms;

    SDK_LOG("WAIT_FOR_STC IC:%d TUNER:%d TIMEOUT_MS:%d", tuner.ic, tuner.id, timeout_ms);
    while (!is_seek_tune_complete(tuner)) {
        if (remaining_ms <= delay_ms)
            return TIMEOUT;

        remaining_ms -= delay_ms;
        os_delay(delay_ms);
    }
    return SUCCESS;
}

#if 0		/* 210505 SGsoft */	
RETURN_CODE get_iqslot(tuner_id tuner, uint8_t* iqslot)
{
    if (iqslot == NULL)
        return INVALID_INPUT;

    // Check if mode-specific
    uint16_t value = 0;
    RETURN_CODE ret = get_prop(tuner.ic, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ADDR, &value);
    if (ret != SUCCESS)
        return ret;

    uint8_t mode_specific = GET_PROP_FIELD(value, SI479XX_PROP_EZIQ_FIXED_SETTINGS_ENABLE_MODE_SPECIFIC);
    uint8_t slot = 0;
    if (mode_specific) {
        // must know tuner mode
        uint8_t mode;
        ret = SI4796X_TUNER_get_mode(tuner.ic, tuner.id, &mode);
        if (ret != SUCCESS)
            return ret;

        uint16_t cfg_prop_addr = 0;
        switch (mode) {
        case SI4796X_TUNER_CMD_GET_MODE_REP_MODE_ENUM_AM:
            cfg_prop_addr = SI4796X_PROP_EZIQ_ZIF_AM_CONFIG1_ADDR;
            break;
        case SI4796X_TUNER_CMD_GET_MODE_REP_MODE_ENUM_FM:
            cfg_prop_addr = SI4796X_PROP_EZIQ_ZIF_FM_CONFIG1_ADDR;
            break;
        case SI4796X_TUNER_CMD_GET_MODE_REP_MODE_ENUM_DAB:
            cfg_prop_addr = SI4796X_PROP_EZIQ_ZIF_DAB_CONFIG1_ADDR;
            break;
        default:
            SDK_LOG("INVALID TUNER MODE\n");
            return INVALID_MODE;
        }

        uint16_t eziq_cfg1 = 0;
        ret = get_prop(tuner.ic, cfg_prop_addr, &eziq_cfg1);
        if (ret != SUCCESS)
            return ret;

        slot = GET_PROP_FIELD(eziq_cfg1, SI4796X_PROP_EZIQ_ZIF_FM_CONFIG1_SLOTSIZE);
    } else {
        // fixed mode
        slot = GET_PROP_FIELD(value, SI4796X_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE);
    }

    switch (slot) {
    case SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE_ENUM_EIGHTEEN:
        *iqslot = 18;
        break;
    case SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE_ENUM_SIXTEEN:
        *iqslot = 16;
        break;
    case SI479XX_PROP_EZIQ_FIXED_SETTINGS_FIXED_SLOTSIZE_ENUM_TWENTY:
        *iqslot = 20;
        break;
    default:
        SDK_PRINTF("UNRECOGNIZED SLOT ENUM VALUE!\n");
        ret = COMMAND_ERROR;
        break;
    }

    return ret;
}

RETURN_CODE am_confirm_seek_complete(tuner_id tuner, bool* seek_continue)
{
    RETURN_CODE ret = SUCCESS;

    // Confirm tune complete
    uint16_t timeout = OPTION_AM_TIMEOUT_SEEK_MS;
    while (!is_seek_tune_complete(tuner)) {
        if (timeout < OPTION_AM_SEEK_CALLBACK_RATE_MS)
            return TIMEOUT;

        os_delay(OPTION_AM_SEEK_CALLBACK_RATE_MS);
        timeout -= OPTION_AM_SEEK_CALLBACK_RATE_MS;
        // update metrics before callback, so it can get latest
        ret = UpdateMetrics();
        if (ret != SUCCESS)
            return ret;

        // Hands off control to the MMI code for UI update (button checks) - opportunity to cancel the scan
        CALLBACK_Updated_Data(SCB_AM_SEEK_PROCESS_UPDATE);
        if (seek_continue && (*seek_continue == false)) {
            SDK_PRINTF("User cancelled AM seek.\n");
            return ret;
        }
    }

    ret |= UpdateMetrics();
    return ret;
}

RETURN_CODE fm_tune_step(fm_chips* chips, fm_system_data* fm, bool stepup)
{
    if (!fm || !chips)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    uint16_t desired_freq_10khz;
    uint16_t init_freq_10khz;

    init_freq_10khz = fm->metrics.FREQUENCY_10KHZ;

    if (stepup) {
        desired_freq_10khz = init_freq_10khz + fm->band.band_step;
        if (desired_freq_10khz > fm->band.band_top)
            desired_freq_10khz = fm->band.band_bottom;
    } else {
        desired_freq_10khz = init_freq_10khz - fm->band.band_step;
        if (desired_freq_10khz < fm->band.band_bottom)
            desired_freq_10khz = fm->band.band_top;
    }

    ret = fm_tune(chips, fm, desired_freq_10khz);
    if (ret == SUCCESS)
        fm->metrics.FREQUENCY_10KHZ = desired_freq_10khz;

    return ret;
}

uint8_t fm_calc_quality(SI4796X_TUNER_fm_rsq_status__data* rsq)
{
    if (!rsq)
        return 0;

    // convert SNR to  0 - 100 range
    // int8_t snr = LIMIT(rsq->SNR, -5, SI4796X_TUNER_CMD_FM_RSQ_STATUS_REP_SNR_MAX);
    double snr_pct = TO_PERCENT(rsq->SNR, -5, SI4796X_TUNER_CMD_FM_RSQ_STATUS_REP_SNR_MAX);
    // other metrics ?

    uint8_t q = (uint8_t)(100 * snr_pct);
    if (rsq->VALID == SI4796X_TUNER_CMD_FM_RSQ_STATUS_REP_VALID_ENUM_NOTVALID)
        q = 0;

    return q;
}

RETURN_CODE fm_update_quality(tuner_id tuner, uint8_t* quality, fm_metrics* m)
{
    RETURN_CODE r = SUCCESS;
    if (!quality)
        return INVALID_INPUT;

    SI4796X_TUNER_fm_rsq_status__data rsq = { 0 };
    r = SI4796X_TUNER_fm_rsq_status(tuner.ic, tuner.id, false, false, false, &rsq);
    if (r == SUCCESS) {
        *quality = fm_calc_quality(&rsq);
        if (m)
            copy_fm_metrics(m, &rsq);
    }
    return r;
}
#endif	/*0*/

bool is_seek_tune_complete(tuner_id tuner)
{
    SI4796X_read_status__data status;

    status = GetSi4796XStatus(tuner.ic);
    switch (tuner.id) {
    case 0:
        return status.STCINT;
    case 1:
        return status.STC1INT;

    default:
        SDK_PRINTF("is_seek_tune_complete() invalid argument 'tuner.id': %d\n", tuner.id);
        D_ASSERT(false);
        return false;
    }
}

#if 0		/* 210505 SGsoft */	
RETURN_CODE update_fm_div_metrics(tuner_id tuner, fm_diversity_metrics* pd_metrics)
{
    if (!pd_metrics)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    SI4796X_fm_phase_div_status__data phaseDivStatus;
    ret |= SI4796X_fm_phase_div_status(tuner.ic, &phaseDivStatus);
    if (ret == SUCCESS) {
        pd_metrics->AUDIO = phaseDivStatus.AUDIO;
        pd_metrics->COMB = phaseDivStatus.COMB;
        pd_metrics->PHS_DIV_CORR = phaseDivStatus.PHSDIVCORR;
    }
    return ret;
}

RETURN_CODE update_fm_metrics(tuner_id tuner, fm_metrics* metrics, bool attune, bool cancel, bool stcack)
{
    if (metrics == NULL)
        return INVALID_INPUT;

    RETURN_CODE ret = SUCCESS;
    SI4796X_TUNER_fm_rsq_status__data fm_rsq = { 0 };
    ret = SI4796X_TUNER_fm_rsq_status(tuner.ic, tuner.id, attune, cancel, stcack, &fm_rsq);
    if (ret == SUCCESS)
        copy_fm_metrics(metrics, &fm_rsq);
    else
        empty_fm_metrics(metrics);

    return ret;
}


RETURN_CODE update_hd_mrc_metrics(tuner_id tuner, hd_mrc_metrics* metrics)
{
    RETURN_CODE ret = SUCCESS;
    SI4796X_fmhd_diversity_status__data div_status;
    if (metrics == NULL)
        return INVALID_INPUT;

    ret |= SI4796X_fmhd_diversity_status(tuner.ic, &div_status);

    if (ret == SUCCESS) {
        metrics->acquired = div_status.ACQUIRED;
        metrics->comb_weight = div_status.COMB_WEIGHT;
        metrics->mode = div_status.MODE;
        metrics->psmi = div_status.PSMI;
    }

    return ret;
}
#endif	/*0*/

RETURN_CODE update_am_metrics(tuner_id tuner, am_metrics* metrics, uint8_t attune, uint8_t cancel, uint8_t stcack)
{
    RETURN_CODE ret = SUCCESS;
    SI4796X_TUNER_am_rsq_status__data am_rsq = { 0 };

    ret = SI4796X_TUNER_am_rsq_status(tuner.ic, tuner.id, attune, cancel, stcack, &am_rsq);
    if (ret == SUCCESS) {
        copy_am_metrics(metrics, &am_rsq);
    } else {
        empty_am_metrics(metrics);
    }
    return ret;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
