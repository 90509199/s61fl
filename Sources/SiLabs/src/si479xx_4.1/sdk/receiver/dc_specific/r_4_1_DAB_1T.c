/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210427 SGsoft */
#define _r_4_1_dab_1t_h_global_
		
#include "main.h"

#if 0
#include "receiver/dc_specific/DAB_1T1D.h"
#include "receiver/Implementation__common.h"

#include "audio/ref_audio_core.h"
#include "boot/Firmware_Load_Helpers.h"
#include "common/DAB_common.h"
#include "common/Firmware_API_Common.h"
#include "common/HD_common.h"
#include "common/common_demod.h"
#include "common/common_system.h"
#include "common/common_tuner.h"
#include "common/global_data.h"

#include "core/c_util.h"
#include "util/sdk_macro.h"

#include "apis/dab_demod_commands_wrapper.h"
#include "apis/si46xx_dab_firmware_api_constants.h"
#include "apis/si4796x_tuner_api.h"
#include "apis/si4796x_tuner_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "apis/si479xx_hub_api.h"
#include "apis/si479xx_hub_api_constants.h"
#include "apis/si479xx_tuner_api_constants.h"
#endif	/*0*/

// Convenience mappings. 1T1D only has 1 tuner 1 demod anyways
static tuner_id tuner;
static demod_id demod;

RETURN_CODE Initialize_DAB_1T(OPERATION_MODE mode)
{
    RETURN_CODE ret = SUCCESS;
	U16 propval = 0;

    tuner = g_dab_parts.main.tuner;
    ///demod = g_dab_parts.main.demod;

    // Boot parts for DAB.
    ret |= fw_load_system(g_evb_system_cfg.dc_type, OPMODE_DAB, RADIO_MODE_DAB);
    if (ret != SUCCESS)
        return ret;
    log_si479xx_part_info(IC0);

	ret |= setup_tuneronly_audio_1T(mode, demod, false);

    ret |= audcfg_setup(&g_evb_system_cfg.audio);

	///ucBand = B_WB;																/* 210504 SGsoft */	
	///ChangeBand(1);
	///WriteTunFreq(9310/*9190*/);	
	///AudioMute(AUDIO_MUTE_NOT_MUTED);
	///audcfg__set_mute(&g_evb_system_cfg.audio, TUN_MUTE_OFF);					/* 180827 SGsoft */
	{
		tuner_id tuner = {.ic = g_evb_system_cfg.audio.ic, .id = TUNER_ID0 };

		ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, 80);
		//ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, AUDIO_MUTE_NOT_MUTED);

		ret |= get_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, &propval);
		ret |= get_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, &propval);
	}


	#if 0
	{
		U8 i;				/* 210509 SGsoft */	
		SI479XX_TUNER_fm_get_servo__data  fmservo[9];
		SI479XX_TUNER_fm_get_fast_servo_transform__data	fastservo[5];
		SI479XX_TUNER_fm_get_servo_transform__data fmservotrans[14];

		SI479XX_TUNER_am_get_servo__data  amservo[8];
		SI479XX_TUNER_am_get_servo_transform__data amservotrans[4];
		U16 mainpropval[8];
		U16 tunerpropval[16];
		
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_CHANBW, &fmservo[0]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_SOFTMUTE, &fmservo[1]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_HICUT, &fmservo[2]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_LOWCUT, &fmservo[3]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_STBLEND, &fmservo[4]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_HIBLEND, &fmservo[5]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_NBUSN1THR, &fmservo[6]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_NBUSN2THR, &fmservo[7]);
		ret |= SI479XX_TUNER_fm_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_FM_SET_SERVO_ARG_FM_SERVO_ID_ENUM_FADETHRESH, &fmservo[8]);

		for (i = 0;i < 5;i++){
			ret |= SI479XX_TUNER_fm_get_fast_servo_transform(tuner.ic, tuner.id, i + 1, &fastservo[i]);
		}

		for (i = 0;i < 14;i++){
			ret |= SI479XX_TUNER_fm_get_servo_transform(tuner.ic, tuner.id, i + 1, &fmservotrans[i]);
		}

		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_CHANBW, &amservo[0]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_SOFTMUTE, &amservo[1]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_HICUT, &amservo[2]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_LOWCUT, &amservo[3]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_NBAUDTH, &amservo[4]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_NBIQ2TH, &amservo[5]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_NBIQ3TH, &amservo[6]);
		ret |= SI479XX_TUNER_am_get_servo(tuner.ic, tuner.id, SI479XX_TUNER_CMD_AM_GET_SERVO_ARG_AM_SERVO_ID_ENUM_MAXAVC, &amservo[7]);

		for (i = 0;i < 4;i++){
			ret |= SI479XX_TUNER_am_get_servo_transform(tuner.ic, tuner.id, i + 1, &amservotrans[i]);
		}
		
		ret |= get_prop(tuner.ic, 0x050a, &mainpropval[0]);
		ret |= get_prop(tuner.ic, 0x050b, &mainpropval[1]);
		ret |= get_prop(tuner.ic, 0x0512, &mainpropval[2]);
		ret |= get_prop(tuner.ic, 0x0518, &mainpropval[3]);
		ret |= get_prop(tuner.ic, 0x0c00, &mainpropval[4]);
		ret |= get_prop(tuner.ic, 0x0c01, &mainpropval[5]);
		ret |= get_prop(tuner.ic, 0x0c02, &mainpropval[6]);
		ret |= get_prop(tuner.ic, 0x0c03, &mainpropval[7]);

		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2102, &tunerpropval[0], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2201, &tunerpropval[1], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2312, &tunerpropval[2], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2313, &tunerpropval[3], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2315, &tunerpropval[4], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2318, &tunerpropval[5], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2400, &tunerpropval[6], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x2901, &tunerpropval[7], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4201, &tunerpropval[8], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4306, &tunerpropval[9], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4307, &tunerpropval[10], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4308, &tunerpropval[11], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4400, &tunerpropval[12], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4401, &tunerpropval[13], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4402, &tunerpropval[14], 1);
		ret |= SI479XX_TUNER_get_property(tuner.ic, tuner.id, 1, 0x4a00, &tunerpropval[15], 1);

		while(1);
	}
	#endif	/*0*/
	
    return ret;
}

// Can change to FM/AM, but this is only analog audio.
// Does not reboot either part.
RETURN_CODE SetRadioMode_DAB_1T(RADIO_MODE_TYPE mode)
{
    RETURN_CODE ret = SUCCESS;
#if 0		/* 210429 SGsoft */
    bool first_call = g_radio_mode == RADIO_MODE_UNKNOWN;

    if (g_radio_mode == mode) {
        SDK_PRINTF("Duplicate SetRadioMode()");
        return SUCCESS;
    }

    uint8_t current_tuner_mode = 0;
    ret = SI4796X_TUNER_get_mode(tuner.ic, tuner.id, &current_tuner_mode);
    if (ret != SUCCESS)
        return ret;

    if (!first_call) {
        if (syscfg_is_audio_tuneronly()) {
            // Reconfigure DAC for different audio source.
            uint8_t purpose = 0;
            if (mode == RADIO_MODE_DAB) {
                purpose = SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMODDIGIN;
            } else {
                purpose = SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMOD;
            }
            // NOTE: pumode doesn't matter since the DAC is already powered up, and we're just switching the connection.
            ret |= SI479XX_dac_config(tuner.ic, 0, purpose, 0, 0, 0, 0);
        } else {
            // Need to switch hub connections between FM/AM and DAB
            if (mode == RADIO_MODE_DAB) {
                // Make audio source I2S RX0
                ret |= audcfg_change_source_port(&g_evb_system_cfg.audio, SI479XX_HUB_CMD_PORT_MUTE_ARG_PORT_ID_ENUM_I2SRX0);
            } else if (current_tuner_mode == RADIO_MODE_DAB) {
                // Transition from DAB to either FM/AM
                // Make audio source TUNER0.
                ret |= audcfg_change_source_port(&g_evb_system_cfg.audio, SI479XX_HUB_CMD_PORT_MUTE_ARG_PORT_ID_ENUM_TUNER0_AUDIO_TO_HIFI);
            }
        }
    }

    if (current_tuner_mode == RADIO_MODE_DAB) {
        ret |= dab_demod_dab_idle(demod);
    }

    switch (mode) {
    case RADIO_MODE_FM:
        ret |= set_mode(tuner, RADIO_MODE_FM);
        break;

    case RADIO_MODE_AM:
        // In future, demod will have to reboot to DRM here ?
        ret |= set_mode(tuner, RADIO_MODE_AM);
        // can match IQ but can't acquire DRM or do anything
        ret |= demod_match_tuner_iq(demod, tuner);
        break;

    case RADIO_MODE_DAB:
        // In future, demod will have to reboot to DAB here ?
        ret |= set_mode(tuner, RADIO_MODE_DAB);
        ret |= demod_match_tuner_iq(demod, tuner);
        break;

    default:
        D_ASSERT_MSG(false, "ERROR: unrecognized radio mode %d\n", mode);
        return INVALID_INPUT;
    }

    g_radio_mode = mode;
#endif	/*0*/
    return ret;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
