/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_receiver_api_h_global_
		
#include "main.h"

#if 0
#include "receiver/Receiver_API.h"

#include "common/Firmware_API_Common.h"

#include "util/cmd_logger_cwrap.h"
#include "util/sdk_macro.h"
#include "common/DAB_common.h"
#include "common/common_system.h"
#include "common/global_data.h"
#include "follow/service_following.h"
#include "common_types.h"
#include "feature_types.h"
#include "platform_options.h"
#include "HAL.h"
#include "hal/hal_platform.h"

#include "apis/si46xx_dab_firmware_api.h"
#include "apis/si46xx_dab_firmware_api_constants.h"
#include "apis/si46xx_hd_firmware_api_constants.h"
#endif	/*0*/

#include <stdio.h>
#include <stdlib.h>

// Macro for asserting at runtime that a fw implementation has been provided.
#define ASSERT_FW_IMPL(fn)                                                      \
    if (fn == NULL) {                                                           \
        D_PRINTF(stderr, "ERROR: fw implementation not provided for " #fn "\n"); \
        return UNSUPPORTED_FUNCTION;                                            \
    }

//******************************************************************************************
// Begin "Public" Functions
//******************************************************************************************

RETURN_CODE SetRadioMode(RADIO_MODE_TYPE mode)
{
    ASSERT_FW_IMPL(g_evb_system_cfg.fw.set_radio_mode);
    SDK_LOG("SetRadioMode(): mode=0x%x", mode);
    return g_evb_system_cfg.fw.set_radio_mode(mode);
}

static RETURN_CODE sf_initialize(OPERATION_MODE mode)
{
    RETURN_CODE ret = SUCCESS;
#if 0
    if (mode == OPMODE_DAB) {
        // Service following capabilities are pretty specific to board.
        if (g_evb_system_cfg.dc_type == DC_1T1D_48
            || g_evb_system_cfg.dc_type == DC_1T1D_84) {
            ret |= sf_init_single(&g_evb_system_cfg, &g_sf_ctx, g_linker, audcfg_sf_switch_src, &g_fm_state, g_dab_state);
        } else {
            ret |= sf_init_fm_dab_dab(&g_evb_system_cfg, &g_sf_ctx, g_linker, audcfg_sf_switch_src, &g_fm_state, &g_dab_states[0], &g_dab_states[1]);
            bool has_linker = syscfg_has_linker(&g_evb_system_cfg);
            g_sf_ctx.opts.align_srcs = has_linker;
        }
        // Service following disabled by default, left to user to enable.
        g_sf_ctx.opts.enabled = false;
        sf_reapply_options(&g_sf_ctx);

        if (ret != SUCCESS) {
            SDK_PRINTF("ERROR: failed to initialize service following code.");
            return ret;
        }
    }
#endif	/*0*/
    return ret;
}

RETURN_CODE Initialize(OPERATION_MODE mode)
{
    RETURN_CODE ret = SUCCESS;

#if 1		/* 210427 SGsoft */
	global_data_init();

	ret |= syscfg_initialize(&g_evb_system_cfg, mode, NULL);
	RETURN_ERR(ret);
	ret |= initHardware(&g_evb_system_cfg.board);
	RETURN_ERR(ret);

	ASSERT_FW_IMPL(g_evb_system_cfg.fw.initialize);
    ret |= g_evb_system_cfg.fw.initialize(mode);		

#else
    global_data_init();

    struct hal_stored_cfg cfg;
    struct hal_stored_cfg* pcfg = &cfg;
    ret = hal_readConfig(pcfg);
    if (ret == UNSUPPORTED_FUNCTION) {
        pcfg = NULL;
    } else if (ret != SUCCESS) {
        return ret;
    }
    ret = syscfg_initialize(&g_evb_system_cfg, mode, pcfg);
    RETURN_ERR(ret);
    ret = initHardware(&g_evb_system_cfg.board);
    RETURN_ERR(ret);

    enable_logger();
    SDK_LOG("Initialize(): mode=0x%x", mode);
    ASSERT_FW_IMPL(g_evb_system_cfg.fw.initialize);
    ret = g_evb_system_cfg.fw.initialize(mode);
    SDK_LOG("Initialize() complete: ret=0x%x", ret);

    ret |= sf_initialize(mode);

    if (LoadServiceList_DAB(&g_dab_prior_svc_index) != SUCCESS)
        SDK_PRINTF_SI("WARN: failed to load DAB service list from storage. Starting fresh.\n");
#endif	/*1*/

    return ret;
}

const system_config* GetSystemConfig()
{
    return &g_evb_system_cfg;
}


#if 0		/* 210504 SGsoft */	
RETURN_CODE Finalize()
{
    c_log_comment("Finalize()");
    RETURN_CODE ret = SUCCESS;

    global_data_finalize();

    ret = powerDownHardware();

    syscfg_free(&g_evb_system_cfg);
    return ret;
}

// freq is always in khz
RETURN_CODE Tune(uint32_t freq_khz)
{
    SDK_LOG("Tune(): freq=%d kHz", freq_khz);

    if (!is_radio_mode_fmam(g_radio_mode))
        return UNSUPPORTED_FUNCTION;

    return Tune__common(&g_fm_cfg, freq_khz);
}

RETURN_CODE SetStepSize(uint8_t stepsize_khz)
{
    SDK_LOG("SetStepSize(): size=%d(khz)", stepsize_khz);

    if (!is_radio_mode_fmam(g_radio_mode))
        return UNSUPPORTED_FUNCTION;

    return SetStepSize__common(&g_fm_cfg, stepsize_khz);
}

RETURN_CODE SetBandLimits(BAND_LIMITS limits)
{
    SDK_LOG("SetBandLimits() : limits=%d", limits);

    if (!is_radio_mode_fmam(g_radio_mode))
        return UNSUPPORTED_FUNCTION;

    return SetBandLimits_common(&g_fm_cfg, limits);
}

RETURN_CODE TuneStep(bool stepup)
{
    SDK_LOG("TuneStep() : stepup=%d", stepup);
    if (!is_radio_mode_fmam(g_radio_mode))
        return INVALID_MODE;

    RETURN_CODE ret = SUCCESS;
    if (is_seek_tune_complete(g_fm_cfg.tuner)) {
        switch (g_radio_mode) {
        case RADIO_MODE_FM:
            ret = fm_tune_step(&g_fm_cfg, &g_fm_state, stepup);
            break;

        case RADIO_MODE_AM: {
            uint32_t target_freq;
            if (stepup) {
                target_freq = g_am_state.metrics.FREQUENCY_1KHZ + g_am_state.band.band_step;
                if (target_freq > g_am_state.band.band_top)
                    target_freq = g_am_state.band.band_bottom;
            } else {
                target_freq = g_am_state.metrics.FREQUENCY_1KHZ - g_am_state.band.band_step;
                if (target_freq < g_am_state.band.band_bottom)
                    target_freq = g_am_state.band.band_top;
            }
            ret = Tune(target_freq);
            if (ret == SUCCESS)
                g_am_state.metrics.FREQUENCY_1KHZ = target_freq;
            break;
        }
        }
    } else {
        // Tune was called too fast, if error call again
        ret = COMMAND_ERROR;
    }

    return ret;
}

RETURN_CODE SeekStart(bool seekup, bool wrap, bool hdseekonly)
{
    SDK_LOG("SeekStart(): seekup=%d, wrap=%d, hd-only=%d", seekup, wrap, hdseekonly);

    if (!is_radio_mode_fmam(g_radio_mode))
        return INVALID_MODE;

    return SeekStart__common(&g_fm_cfg, seekup, wrap, hdseekonly);
}

RETURN_CODE SeekStop(void)
{
    c_log_comment("SeekStop()");

    RETURN_CODE ret = SUCCESS;
    // Currently not really useful.
    //	SeekStart() already waits for the seek to finish (or get cancelled) before returning.

    switch (g_radio_mode) {
    case RADIO_MODE_FM:
        g_fm_state.continue_seek = false;
        ret |= SI4796X_TUNER_fm_rsq_status(g_fm_cfg.tuner.ic, g_fm_cfg.tuner.id,
            false, true, false, NULL);
        break;

    case RADIO_MODE_AM:
        g_am_state.continue_seek = false;
        ret |= SI4796X_TUNER_am_rsq_status(g_fm_cfg.tuner.ic, g_fm_cfg.tuner.id,
            false, true, false, NULL);
        break;
    }

    return ret;
}

#define AUDIO_MAX_ATTENUATION 95

RETURN_CODE AudioLevel(uint16_t level)
{
    c_log_comment("AudioLevel()");
    uint16_t attenuation_db = level;
    if (attenuation_db > AUDIO_MAX_ATTENUATION) {
        SDK_PRINTF("WARNING: capping AudioLevel() attenuation.\n");
        attenuation_db = AUDIO_MAX_ATTENUATION;
    }

    // This should also now use the audio subcfg.
    return audcfg_set_volume(&g_evb_system_cfg.audio, attenuation_db);
}

RETURN_CODE AudioMute(AUDIO_MUTE mute)
{
    c_log_comment("AudioMute()");

    if (mute >= AUDIO_MUTE_ERR || mute < AUDIO_MUTE_NOT_MUTED)
        return INVALID_INPUT;

    return audcfg_set_mute(&g_evb_system_cfg.audio, mute);
}

// Test function added to the SDK for measuring MONO performance of the 46xx
RETURN_CODE ForceMono_FM(AUDIO_MONO_STEREO_SWITCH audio_MonStereo)
{
    c_log_comment("ForceMono_FM()");
    if (g_radio_mode != RADIO_MODE_FM)
        return INVALID_MODE;

    // TODO - should apply to all FM tuners.
    RETURN_CODE ret = SUCCESS;
    tuner_id tuner0 = {.ic = IC0, .id = TUNER_ID0 };
    ret |= force_mono_fm(tuner0, audio_MonStereo);

    return ret;
}

RETURN_CODE UpdateMetrics()
{
    c_log_comment("UpdateMetrics()");
    return UpdateMetrics__common(&g_fm_cfg, &g_dab_cfg, g_dab_state);
}

RETURN_CODE UpdateServiceList(void)
{
    c_log_comment("UpdateServiceList()");
    return UpdateServiceList__common(&g_fm_cfg, &g_dab_cfg, g_dab_state, &g_hd_data);
}

RETURN_CODE UpdateDataServiceData(void)
{
    c_log_comment("UpdateDataServiceData()");
    return UpdateDataServiceData__common(g_radio_mode, &g_fm_cfg, &g_fm_state.rds, &g_dab_cfg, &g_hd_data, g_dab_state);
}

RETURN_CODE StartProcessingChannel(GENERAL_SERVICE_TYPE type,
    uint32_t service_id, uint32_t component_id, uint8_t* service_buffer)
{
    SDK_LOG("StartProcessingChannel() : sid=0x%x  cid=0x%x type=0x%x", service_id, component_id, type);

    return StartProcessingChannel__common(&g_fm_cfg, &g_dab_cfg, type, service_id, component_id, service_buffer, g_dab_state, &g_hd_data);
}

RETURN_CODE StopProcessingChannel(uint32_t service_id, uint32_t component_id)
{
    c_log_comment("StopProcessingChannel()");

    RETURN_CODE ret = SUCCESS;
    bool is_audio_service = false;

    //Check if it is the current audio service
    switch (g_radio_mode) {
    case RADIO_MODE_DAB:
        if (service_id == g_dab_state->current.service_id
            && component_id == g_dab_state->current.component_id) {
            is_audio_service = true;
            ret = dab_demod_stop_digital_service(g_dab_cfg.demod,
                SI461X_DAB_CMD_STOP_DIGITAL_SERVICE_ARG_SERTYPE_ENUM_AUDIO,
                service_id, component_id);

            g_dab_state->current.component_id = 0;
            g_dab_state->current.service_id = 0;
        }
        break;
    default:
        if (service_id == g_hd_data.audio_sid
            && component_id == g_hd_data.audio_cid) {
            is_audio_service = true;
            ret = hd_demod_stop_digital_service(g_fm_cfg.hd_demod,
                SI461X_HD_CMD_STOP_DIGITAL_SERVICE_ARG_SERTYPE_ENUM_AUDIO,
                service_id, component_id);

            g_hd_data.audio_cid = 0;
            g_hd_data.audio_sid = 0;
        }
        break;
    }

#ifdef OPTION_HANDLE_ADVANCED_SERVICES
    if (!is_audio_service) {
        ret = adv_stop_processing(service_id, component_id);
    }
#endif

    return ret;
}

#ifdef OPTION_RADIODNS
//Calculates each of the pieces of the RDS RadioDNS string
//Note: This does not support the country code form
//		of the string as that information is not available from the broadcast alone
//
//String format: <frequency>.<pi>.<gcc>.fm.radiodns.org
//    - frequency: in 5 characters (leading 0 for frequencies below 100MHz)
//    - pi: in 4 characters
//    - gcc: in 3 characters
RETURN_CODE UpdateRadioDNS_FMRDS(radioDNS_fm* elements)
{
    c_log_comment("UpdateRadioDNS_FMRDS()");

    if (g_radio_mode != RADIO_MODE_FM)
        return INVALID_MODE;

    return UpdateRadioDNS_FMRDS_common(elements);
}

#endif
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
