/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210419 SGsoft */
#define _r_4_1_implementation_common_h_global_
		
#include "main.h"

#if 0
#include "receiver/Implementation__common.h"
#include "receiver/Receiver_API.h"

#include "audio/ref_audio_core.h"
#include "common/Firmware_API_Common.h"
#include "common/common_system.h"
#include "common/global_data.h"
#include "linker/linker.h"

#include "util/cmd_logger_cwrap.h"
#include "util/sdk_macro.h"
#include "core/c_core.h"
#include "HAL.h"

#include "apis/si4690_dab_firmware_api.h"
#include "apis/si4690_dab_firmware_api_constants.h"
#include "apis/si46xx_dab_firmware_api_constants.h"
#include "apis/si4796x_firmware_api_constants.h"
#include "apis/si4796x_tuner_api.h"
#include "apis/si4796x_tuner_api_constants.h"
#include "apis/si479xx_firmware_api.h"
#include "apis/si479xx_firmware_api_constants.h"
#include "apis/si479xx_hub_api_constants.h"
#endif	/*0*/


RETURN_CODE setup_tuneronly_audio_1T(OPERATION_MODE opmode, demod_id demod, bool enable_tuner_tx)				/* 210429 SGsoft */
{
    // Enable CLK master ports first.
    RETURN_CODE ret = SUCCESS;


    ret |= SI479XX_dac_config(IC0,
        SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
        SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMOD,
        0,
        0,
        0,
        0);

    return ret;
	
}

#if 0		/* 210505 SGsoft */	
RETURN_CODE setup_tuneronly_audio_1T1D(OPERATION_MODE opmode, demod_id demod, bool enable_tuner_tx)
{
    tuner_digport_cfg shared_cfg = digport_default();
    shared_cfg.MASTER = SI479XX_CMD_I2SRX0_CONFIG_ARG_MASTER_ENUM_SLAVE;

    // pinouts differ between 1T1D 84 and 48 pin daughtercards.
    uint8_t tx_clkfs, rx_clkfs, rx_data, tx_master;
    if (syscfg_is_1T1D_48()) {
        rx_data = SI479XX_CMD_I2SRX0_CONFIG_ARG_DATA0_ENUM_DIN0;
        rx_clkfs = SI479XX_CMD_I2SRX0_CONFIG_ARG_CLKFS_ENUM_IN0;
        tx_clkfs = SI479XX_CMD_I2SRX0_CONFIG_ARG_CLKFS_ENUM_GP;
        tx_master = SI479XX_CMD_I2SRX0_CONFIG_ARG_MASTER_ENUM_SLAVE;
    } else {
        rx_data = SI479XX_CMD_I2SRX0_CONFIG_ARG_DATA0_ENUM_DIN1;
        rx_clkfs = SI479XX_CMD_I2SRX0_CONFIG_ARG_CLKFS_ENUM_IN1;
        tx_clkfs = SI479XX_CMD_I2SRX0_CONFIG_ARG_CLKFS_ENUM_OUT0;
        // 84 pin must be TX clk master (buffer - see schematic)
        tx_master = SI479XX_CMD_I2SRX0_CONFIG_ARG_MASTER_ENUM_MASTER;
    }

    tuner_digport_cfg rx_cfg = shared_cfg;
    rx_cfg.CLOCKDOMAIN = 1;
    rx_cfg.CLKFS = rx_clkfs;
    rx_cfg.DATA0 = rx_data;

    tuner_digport_cfg tx_cfg = shared_cfg;
    tx_cfg.CLKFS = tx_clkfs;
    tx_cfg.DATA0 = SI479XX_CMD_I2STX0_CONFIG_ARG_DATA0_ENUM_DOUT0;
    tx_cfg.MASTER = tx_master;
    if (!tx_master) {
        tx_cfg.CLOCKDOMAIN = 2;
    }

    // Enable CLK master ports first.
    RETURN_CODE ret = SUCCESS;
    if (enable_tuner_tx && tx_master) {
        ret |= digport_i2s_config(IC0, PORT_I2S_TX0, &tx_cfg);
    }

    ret |= demod_set_i2s_cfg(demod, PORT_I2S_RX0, rx_cfg);
    if (enable_tuner_tx) {
        ret |= demod_set_i2s_cfg(demod, PORT_I2S_TX0, tx_cfg);
    }
    ret |= demod_start_i2s(demod);

    if (enable_tuner_tx && !tx_master) {
        ret |= digport_i2s_config(IC0, PORT_I2S_TX0, &tx_cfg);
    }
    ret |= digport_i2s_config(IC0, PORT_I2S_RX0, &rx_cfg);

    uint8_t dac01_control;
    if (opmode == OPMODE_HD)
        dac01_control = SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMODDIGIN;
    else
        dac01_control = SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMOD;

    ret |= SI479XX_dac_config(IC0,
        SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
        dac01_control,
        SI479XX_CMD_DAC_CONFIG_ARG_PUMODE23_ENUM_SLOW,
        SI479XX_CMD_DAC_CONFIG_ARG_DAC23_MODE_ENUM_NONE,
        SI479XX_CMD_DAC_CONFIG_ARG_PUMODE45_ENUM_SLOW,
        SI479XX_CMD_DAC_CONFIG_ARG_DAC45_MODE_ENUM_NONE);

    return ret;
}


RETURN_CODE setup_tuner_audio_2DT1F(LINKER_AUDIO_MODE audio_mode)
{
    RETURN_CODE ret = SUCCESS;
    tuner_digport_cfg shared_cfg = digport_default();
    shared_cfg.MASTER = SI4796X_CMD_I2SRX0_CONFIG_ARG_MASTER_ENUM_SLAVE;

    const uint8_t falcon_tx_clkdomain = 1;
    const uint8_t falcon_rx_clkdomain = 2;

    // I2S TX from IC0
    tuner_digport_cfg txcfg = shared_cfg;
    txcfg.CLOCKDOMAIN = falcon_tx_clkdomain;
    txcfg.CLKFS = SI4796X_CMD_I2STX0_CONFIG_ARG_CLKFS_ENUM_OUT0;
    txcfg.DATA0 = SI4796X_CMD_I2STX0_CONFIG_ARG_DATA0_ENUM_DOUT0;

    tuner_digport_cfg rxcfg = shared_cfg;
    rxcfg.CLOCKDOMAIN = falcon_rx_clkdomain;
    rxcfg.CLKFS = SI4796X_CMD_I2SRX0_CONFIG_ARG_CLKFS_ENUM_IN1;
    rxcfg.DATA0 = SI4796X_CMD_I2SRX0_CONFIG_ARG_DATA0_ENUM_DIN1;

    // Enable falcon I2S first (CLK master)
    demod_id linker = {.ic = IC3, .select = DEMOD_SELECT_LINKER };
    ret |= demod_set_i2s_cfg(linker, PORT_I2S_TX0, txcfg);
    ret |= demod_set_i2s_cfg(linker, PORT_I2S_RX0, rxcfg);
    ret |= SI4690_DAB_sl_set_audio_mode(IC3, audio_mode);

    // Enable tuner I2S ports (CLK slave)
    ret |= digport_i2s_config(IC0, PORT_I2S_TX0, &txcfg);
    ret |= digport_i2s_config(IC0, PORT_I2S_RX0, &rxcfg);

    if (syscfg_is_audio_tuneronly()) {
        // DACs from IC0
        ret |= SI4796X_dac_config(IC0,
            SI4796X_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
            SI4796X_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMOD0DIGIN,
            SI4796X_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
            SI4796X_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_NONE,
            SI4796X_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
            SI4796X_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_NONE);
    } else {
        // Connect Tuner0 to I2S TX0
        ret |= connect_ports(IC0,
            SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_P2N_CNX_PORT_ID_ENUM_TUNER0_AUDIO_TO_HIFI, 0,
            SI479XX_HUB_CMD_XBAR_CONNECT_NODE_ARG_P2N_CNX_PORT_ID_ENUM_I2STX0, 0, 2, 0);
    }

    return ret;
}
#endif	/*0*/

void log_si479xx_part_info(dc_config_chip_select ic)
{
    RETURN_CODE err = SUCCESS;
    SI479XX_get_part_info__data pi;
    SI479XX_get_chip_info__data ci;
    SI479XX_get_func_info__data fi;
    uint8_t chip;

    get_ic_num(ic, &chip);

    SDK_LOG("INFO ABOUT CHIP_%d - START", chip);
    err = SI479XX_get_part_info(ic, &pi);

    if (err) {
        c_log_comment("ERROR: SI479XX_get_part_info()");
    } else {
        SDK_LOG("PART_INFO{ PN1:'%c', PN0:'%c', CUST2:'%c', CUST1:'%c', MAJOR:0x%x, CHIPREV:0x%x, ROMREV:0x%x}",
            pi.PN1, pi.PN0, pi.CUST2, pi.CUST1, pi.MAJOR, pi.CHIPREV, pi.ROMREV);
    }

    err = SI479XX_get_chip_info(ic, SI479XX_CMD_GET_CHIP_INFO_ARG_INFO_TYPE_ENUM_SERIAL_NUMBER, &ci);
    if (err) {
        c_log_comment("ERROR: SI479XX_get_chip_info()");
    } else {
        uint8_t i;
        c_log_comment("get_chip_info() - INFO_TYPE=SERIAL_NUMBER: ");
        for (i = 0; i < ci.LENGTH; i++)
            SDK_LOG("0x%x", ci.DATA[i]);
    }
    free(ci.DATA);

    err = SI479XX_get_func_info(ic, &fi);
    if (err) {
        c_log_comment("ERROR: SI479XX_get_func_info()");
    } else {
        SDK_LOG("FUNC_INFO{ VERINFO:%x.%x.%x.%x.%x HIFI:%x.%x.%x.%x CUST:%x.%x.%x.%x  NOSVN?%d LOCATION=%x MIXEDREV?%x LOCALMOD?%x  SVNID=%x",
            fi.VERINFO0, fi.VERINFO1, fi.VERINFO2, fi.VERINFO3, fi.VERINFO4,
            fi.HIFIINFO3, fi.HIFIINFO2, fi.HIFIINFO1, fi.HIFIINFO0,
            fi.CUSTINFO3, fi.CUSTINFO2, fi.CUSTINFO1, fi.CUSTINFO0,
            fi.NOSVN, fi.LOCALMOD, fi.MIXEDREV, fi.LOCALMOD, fi.SVNID);
    }

    SDK_LOG("INFO ABOUT CHIP_%d - END", chip);
}

#if 0		/* 210505 SGsoft */	
RETURN_CODE log_demod_part_info(demod_id demod)
{
    RETURN_CODE ret = SUCCESS;
    DAB_DEMOD_get_func_info__data func_info;
    uint8_t image = 0;

    SDK_LOG("demod (ic=0x%x, select=0x%x) : {", demod.ic, demod.select);

    ret = dab_demod_get_sys_state(demod, &image);
    if (ret == SUCCESS)
        SDK_LOG("Image=0x%x,", image);
    else
        SDK_LOG("ERROR: fw_current_demod_image()");

    ret = dab_demod_get_func_info(demod, &func_info);
    if (ret == SUCCESS) {
        SDK_LOG("GET_FUNC_INFO={REVEXT=0x%x, REVBRANCH=0x%x, REVINT=0x%x, NOSVN=0x%x, LOCATION=0x%x, MIXEDREV=0x%x, LOCALMOD=0x%x, SVNID=0x%x},",
            func_info.REVEXT, func_info.REVBRANCH, func_info.REVINT, func_info.NOSVN, func_info.LOCATION, func_info.MIXEDREV, func_info.LOCALMOD, func_info.SVNID);
    } else {
        SDK_LOG("ERROR: demod_get_func_info()");
    }

    SDK_LOG("}");
    return ret;
}

RETURN_CODE log_falcon_part_info(dc_config_chip_select ic)
{
    RETURN_CODE ret = SUCCESS;
    demod_id d1 = {.ic = ic, .select = DEMOD_SELECT1 };
    demod_id d2 = {.ic = ic, .select = DEMOD_SELECT2 };

    SDK_LOG("INFO for Falcon IC=0x%x {", ic);
    SI4690_DAB_get_part_info__data part_info = { 0 };
    ret = SI4690_DAB_get_part_info(ic, &part_info);
    if (ret == SUCCESS)
        SDK_LOG("GET_PART_INFO - linker : { PART=0x%x, ROMID=0x%x, CHIPREV=0x%x}", part_info.PART, part_info.ROMID, part_info.CHIPREV);
    else
        SDK_LOG("ERROR: SI4690_get_part_info()");

    SI4690_DAB_get_func_info__data func_info = { 0 };
    ret |= SI4690_DAB_get_func_info(ic, &func_info);
    SDK_LOG("linker GET_FUNC_INFO={REVEXT=0x%x, REVBRANCH=0x%x, REVINT=0x%x, NOSVN=0x%x, LOCATION=0x%x, MIXEDREV=0x%x, LOCALMOD=0x%x, SVNID=0x%x},",
        func_info.REVEXT, func_info.REVBRANCH, func_info.REVINT, func_info.NOSVN, func_info.LOCATION, func_info.MIXEDREV, func_info.LOCALMOD, func_info.SVNID);

    SDK_LOG(" satellite demod1 : {");
    ret |= log_demod_part_info(d1);
    SDK_LOG(" }");

    SDK_LOG(" satellite demod2 : {");
    ret |= log_demod_part_info(d2);
    SDK_LOG(" }");

    SDK_LOG("}");
    return ret;
}
#endif	/*0*/

#endif	/*SI479XX_FIRM_REL_4_1*/
