/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#include "sg_defines.h"								
	
#if defined(SI479XX_FIRM_REL_4_1)		/* 210416 SGsoft */
#define _r_4_1_flash_util_h_global_
	
#include "main.h"

///#include "flash_util.h"

///#include "core/debug_macro.h"
///#include "flash_crc.h"

///#include "sdk/util/cmd_logger_cwrap.h"
///#include "core/c_core.h"

///#include "sdk/apis/si479xx_firmware_api_constants.h"
///#include "sdk/apis/si479xx_flash_utility_api.h"
///#include "sdk/apis/si479xx_flash_utility_api_constants.h"
///#include "sdk/boot/FirmwarePaths.h"
///#include "sdk/boot/si479xx_initial_startup_sample.h"


static RETURN_CODE set_default_flash_props(dc_config_chip_select ic)
{
    RETURN_CODE ret = SUCCESS;
    SI479XX_FLASH_UTIL_set_prop_propset__data props[10] = { 0 };

    // Set the flash_utility properties. These will be specific to the flash chip, refer to its datasheet.
    // The values here are for the silabs' EVB flash chip.

    // SPI clk freq (MHz)
    props[0].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_ADDR;
    props[0].PROP_VAL = 20;

    // SPI Mode
    props[1].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_ADDR;
    props[1].PROP_VAL = 0;

    // Not using DUAL SPI read
    props[2].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_ADDR;
    props[2].PROP_VAL = 0;

    // READ cmd
    props[3].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_ADDR;
    props[3].PROP_VAL = 0x3;

    // High speed READ cmd
    props[4].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_ADDR;
    props[4].PROP_VAL = 0xb;

    // WRITE cmd
    props[5].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_ADDR;
    props[5].PROP_VAL = 0x2;

    // ERASE SECTOR cmd
    props[6].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ADDR;
    // props[6].PROP_VAL = 0x20;
    props[6].PROP_VAL = 0xD7;

    // ERASE SECTOR size (kilobytes)
    props[7].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ADDR;
    props[7].PROP_VAL = 0x4;

    // ERASE CHIP cmd
    props[8].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ADDR;
    props[8].PROP_VAL = 0xC7;

    // Not using WRITE PROTECT cmd
    props[9].PROP_ID = SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_ADDR;
    props[9].PROP_VAL = 0x00;

    return SI479XX_FLASH_UTIL_set_prop(ic, props, 10);
}


RETURN_CODE boot_flash_utility(dc_config_chip_select tuner, dc_config_chip_type type, const struct flash_props* opt_props)
{
    bool is_dual_eagle = type == Si4796x;
    int ret = preBoot_startImageSetup();
	#if 0		/* 210510 SGsoft */	
    ret |= preBoot_addFirmwareImageComponent(tuner, getFilePath_Image(type, IMAGE_PATCH));
    ret |= preBoot_addFirmwareImageComponent(tuner, getFilePath_Image(type, IMAGE_TUNER_FLASH_UTILITY));
    // Requires firmware images in same directory as exe
    if (is_dual_eagle) {
        ret |= preBoot_changePartNumber(tuner, '6', '2', 'G', 'E');
    } else {
        ret |= preBoot_changePartNumber(tuner, '0', '2', 'G', 'E');
    }
    // For POWER_UP chipid, not chip addressing
    ret |= preBoot_appendChipCompleteBytes(IC0);
    ret |= resetIC(tuner);

    uint8_t cor, uncor;
    // Boot chip connected to flash memory.
    // NOTE: flash_utility is unique in that it does not require a CLK/XTAL input to BOOT.
    struct si479xx_powerup_args pup_args = {
        .ctsien = SI479XX_CMD_POWER_UP_ARG_CTSIEN_ENUM_DISABLE,
        .clkout = SI479XX_CMD_POWER_UP_ARG_CLKOUT_ENUM_DISABLE,
        .clkmode = SI479XX_CMD_POWER_UP_ARG_CLK_MODE_ENUM_XTAL,
        .trsize = SI479XX_CMD_POWER_UP_ARG_TR_SIZE_ENUM_ENABLE,
        .ctun = 20,
        .xtal_freq = 36864000,
        .ibias = 67,
        .afs = SI479XX_CMD_POWER_UP_ARG_AFS_ENUM_FS48,
        .chipid = 0,
        .clko_current = SI479XX_CMD_POWER_UP_ARG_CLKO_CURRENT_ENUM_DEFAULT,
        .vio = SI479XX_CMD_POWER_UP_ARG_VIO_ENUM_V3P3,
        .eziq_master = SI479XX_CMD_POWER_UP_ARG_EZIQ_MASTER_ENUM_MASTER,
        .eziq_enable = SI479XX_CMD_POWER_UP_ARG_EZIQ_ENABLE_ENUM_DISABLE,
    };
    ret |= hostBootSingle_479xx(tuner, type, &pup_args, &uncor, &cor);
    ret |= postBoot_emptyImageSetup();
    if (ret != SUCCESS)
        return ret;

    if (opt_props) {
        // Set the default flash_utility properties.
        ret = SI479XX_FLASH_UTIL_set_prop(tuner, opt_props->props, opt_props->prop_count);
    } else {
        // Set the default flash_utility properties.
        ret = set_default_flash_props(tuner);
    }
	#endif	/*0*/
    return ret;
}


bool erase_chip(dc_config_chip_select tuner)
{
    // Erasing whole chip takes looooong time for CTS.
    uint32_t orig_timeout = getCtsTimeout();
    setCtsTimeout(30000);

    RETURN_CODE r = SI479XX_FLASH_UTIL_erase_chip(tuner);
    if (r != SUCCESS) {
        D_PRINTF(stderr, "Failed to erase chip!\n");
        setCtsTimeout(orig_timeout);
        return false;
    }

    setCtsTimeout(orig_timeout);
    return true;
}

static uint32_t align(uint32_t addr, uint32_t block_size)
{
    uint32_t r = addr % block_size;
    return addr - r;
}

static uint32_t get_sector_size(const struct flash_chip_cfg* opt_cfg)
{
    if (opt_cfg) {
        return opt_cfg->sector_size;
    } else {
        const uint32_t kilobyte = power_two(10);
        return kilobyte * SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_DEFAULT;
    }
}

static RETURN_CODE write_sector(dc_config_chip_select ic, const uint8_t* image, uint32_t image_size, uint32_t addr, bool erase, uint32_t sector_size)
{
    RETURN_CODE r;
    if (erase) {
        uint32_t addr_aligned = align(addr, sector_size);
        r = SI479XX_FLASH_UTIL_erase_sector(ic, addr_aligned);
        if (r != SUCCESS) {
            D_PRINTF(stderr, "ERR: failed to erase sector @addr 0x%x\n", addr_aligned);
            return r;
        }
    }

    const int max_write_attempts = 10;
    for (int i = 0; i < max_write_attempts; i++) {
        r = SI479XX_FLASH_UTIL_write_block(ic, addr, image_size, image);

        // Now check the crc32 of the block written
        uint32_t crc_correct, crc_part;
        crc_correct = 0xFFFFFFFF;
        crc32_update(&crc_correct, image, image_size);
        r |= SI479XX_FLASH_UTIL_check_crc32(ic, addr, image_size, &crc_part);
        if (r != SUCCESS) {
            D_PRINTF(stderr, "ERR: write_block+check_crc32 error (addr 0x%x), r=%d\n", addr, r);
            return r;
        }

        if (crc_correct != crc_part) {
            ///printf("Attempt %d/%d: crc mismatch!\n", i, max_write_attempts);
            ///printf("calculated: 0x%08x\n", crc_correct);
            ///printf("actual    : 0x%08x\n", crc_part);
            continue;
        } else {
            // Write successful - crc match
            return SUCCESS;
        }
    }

    return HAL_ERROR;
}

RETURN_CODE write_image_bytes(dc_config_chip_select ic, const uint8_t* image, uint32_t image_size, uint32_t addr, bool erase_first, uint32_t sector_size)
{
    RETURN_CODE r;
    // Write partial sector first if not aligned....
    uint32_t image_offset = 0;
    uint32_t addr_aligned = align(addr, sector_size);
    if (addr_aligned != addr) {
        // Partial sector write... 
        // BUG : partial size could be large than image size
        uint32_t write_size = 0;
        uint32_t partial_size = sector_size - (addr - addr_aligned);
        bool small_write = image_size < partial_size;
        if (small_write) {
            write_size = image_size;
        } else {
            write_size = partial_size;
        }
        r = write_sector(ic, image, write_size, addr, erase_first, sector_size);
        if (r != SUCCESS || small_write) 
            return r;

        image_offset += partial_size;
    }

    // Write aligned sectors
    for (; image_offset < image_size;
           image_offset += sector_size) 
    {
        uint32_t remaining = image_size - image_offset;
        uint32_t write_size = MIN(remaining, sector_size);

        r = write_sector(ic, (image + image_offset), write_size, (addr + image_offset), erase_first, sector_size); 
        if (r != SUCCESS) 
            return r;
    }

    return SUCCESS;
}

RETURN_CODE write_image(dc_config_chip_select ic, const char* file, uint32_t addr, bool erase_first, const struct flash_chip_cfg* opt_cfg)
{
    int r;
    RETURN_CODE ret = SUCCESS;
#if 0		/* 210429 SGsoft */
    if (!file) 
        return INVALID_INPUT;

    FILE* fin = fopen(file, "rb");
    if (!fin) {
        perror("fopen");
        return INVALID_INPUT;
    }

    uint32_t sector_size = get_sector_size(opt_cfg);
    uint8_t* bytes = calloc(sector_size, 1);
    if (!bytes) {
        perror("calloc");
        ret = MEMORY_ERROR;
        goto free_file;
    }

    // Write partial sector first if not aligned....
    uint32_t image_offset = addr;
    uint32_t addr_aligned = align(addr, sector_size);
    if (addr_aligned != addr) {
        // Partial sector write... 
        uint32_t partial_size = sector_size - (addr - addr_aligned);
        size_t read_size = fread(bytes, 1, partial_size, fin);
        if (ferror(fin)) {
            fprintf(stderr, "failed to read input file '%s'\n", file);
            ret = INVALID_INPUT;
            goto free_sector;
        }

        ret = write_sector(ic, bytes, read_size, addr, erase_first, sector_size);
        if (ret != SUCCESS) 
            goto free_sector;

        image_offset = addr_aligned + partial_size;
    }

    // Write rest of image 
    while (!feof(fin)) {
        size_t read_size = fread(bytes, 1, sector_size, fin);
        if (ferror(fin)) {
            fprintf(stderr, "failed to read input file '%s'\n", file);
            ret = INVALID_INPUT;
            goto free_sector;
        }
        // Assuming that (read_size == 0) && !feof(fin) && !ferror(fin)  is impossible. 
        if (read_size == 0)
            continue;

        ret = write_sector(ic, bytes, read_size, image_offset, erase_first, sector_size); 
        if (ret != SUCCESS) 
            goto free_sector;

        image_offset += read_size;
    }

free_sector:
    free(bytes);

free_file:
    r = fclose(fin);
    if (r == EOF) {
        perror("fclose");
        ret |= LIB_ERROR;
    }
#endif	/*0*/
    return ret;
}


RETURN_CODE read_image(dc_config_chip_select ic, const char* file, uint32_t addr, uint32_t size)
{
    RETURN_CODE ret = SUCCESS;
#if 0		/* 210428 SGsoft */
    if (!file) 
        return INVALID_INPUT;

    const uint32_t max_read_size = SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MAX;
    uint8_t chunk[SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MAX] = {0};
    FILE* fout = fopen(file, "wb");
    if (!fout) {
        perror("fopen");
        return INVALID_INPUT;
    }

    uint32_t read_size = 0;
    for (uint32_t bytes_read = 0; bytes_read < size; bytes_read += read_size) {
        uint32_t remaining = size - bytes_read;
        read_size = MIN(remaining, max_read_size);

        ret = SI479XX_FLASH_UTIL_read_block(ic, (addr + bytes_read), read_size, chunk, read_size);
        if (ret != SUCCESS) {
            break;
        }

        size_t bytes_written = fwrite(chunk, 1, read_size, fout);
        if (bytes_written != read_size) {
            D_PRINTF("issue writing image ot file.\n");
            ret = LIB_ERROR;
            break;
        }
    }

    if (0 != fclose(fout)) {
        perror("fclose");
    }
#endif	/*0*/
    return ret;
}

// Reads flash bytes.
RETURN_CODE read_image_bytes(dc_config_chip_select ic, uint8_t* image, uint32_t addr, uint32_t size)
{
    RETURN_CODE ret = SUCCESS;
    if (!image) 
        return INVALID_INPUT;

    uint32_t read_size = 0;
    const uint32_t max_read_size = SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MAX;
    for (uint32_t bytes_read = 0; bytes_read < size; bytes_read += read_size) {
        uint32_t remaining = size - bytes_read;
        read_size = MIN(remaining, max_read_size);

        ret = SI479XX_FLASH_UTIL_read_block(ic, (addr + bytes_read), read_size, (image + bytes_read), read_size);
        if (ret != SUCCESS) {
            return ret;
        }
    }

    return ret;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
