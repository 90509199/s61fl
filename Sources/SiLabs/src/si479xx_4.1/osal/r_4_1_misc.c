/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
	
#include "sg_defines.h"								
		
#if defined(SI479XX_FIRM_REL_4_1)		/* 210426 SGsoft */
#define _r_4_1_misc_h_global_
		
#include "main.h"

#if 0
#include "osal/misc.h"
#include "core/debug_macro.h"


extern "C" {
    #include "core/c_core.h"
}
#endif	/*0*/


void os_delay(uint32_t delay_ms)
{
}

uint32_t os_ticks_ms()
{
    return 0;
}

bool osio_has_user_input() 
{
    return TRUE;
}

size_t osio_get_line(char* line, size_t max_len)
{
    return 0;
}

#endif	/*SI479XX_FIRM_REL_4_1*/
