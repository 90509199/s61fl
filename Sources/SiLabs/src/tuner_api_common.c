/***************************************************************************************
                  Silicon Laboratories Broadcast Si475x/6x/9x module code

   EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
     THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
     TO THIS SOURCE FILE.
   IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
     PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.

   Date: June 06 2014
  (C) Copyright 2014, Silicon Laboratories, Inc. All rights reserved.
****************************************************************************************/
#include "sg_defines.h"	

#if defined(ENABLE_TUNER_SI479XX)		
#define	TUNER_API_COMMON_H_GLOBAL_
#define	_AG_TUNMAIN_H_GLOBAL_

#include "main.h"


void RadioInit(U8 area)											/* 211201 SGsoft */
{
	U8 i;
	
	
	ucArea = area;																				
	ucBand = B_FM1;														
	for (i = 0;i < NUM_OF_PRESET_MEMORY;i++){
		PresetMem[i] = default_freq[ucArea*12 + i];
	}

	ucMem = 1;												
	//lFreq = PresetMem[ucMem - 1];																
	
}


void Delay(volatile U32 nCount)											
{
  	for(; nCount!= 0;nCount--);
	
}


#define	DELAY_TIME_BASE_10US		0x36		
void Delay10Us(U16 us)								
{
	U32	dval;


	dval = (U32)us * DELAY_TIME_BASE_10US;
	Delay(dval);
	
}


#define	DELAY_TIME_BASE_MS			0x1880	
void DelayMs(U16 ms)									
{
	U32 dval;


	dval = (U32)ms * DELAY_TIME_BASE_MS;
	Delay(dval);
	
}


void pTRST(U8 out)					
{	
#if 1
	SiLabs_Tuner_RST(out);//JANG_RADIO_PORTING
#else
	if (out){ 													/* Tuner Reset : Active LOW */
			LL_GPIO_SetOutputPin(TUN_RST_GPIO_Port, TUN_RST_Pin);
		}else{
			LL_GPIO_ResetOutputPin(TUN_RST_GPIO_Port, TUN_RST_Pin);
		}
#endif

}


void TunerReset(void)																
{
#if defined(SI479XX_FIRM_REL_2_3) || defined(SI479XX_FIRM_REL_4_1)	
	Delay10Us(3);
	pTRST(0);
	Delay10Us(33);
	pTRST(1);
	Delay10Us(3);
#endif	/*SI479XX_FIRM_REL_2_3*/

}


void TunProc(void)														
{
#if defined(SI479XX_FIRM_REL_2_3) || defined(SI479XX_FIRM_REL_4_1)		
	if (ucTun == TUN_STATE_SEEK_DN)
	{
		if (SeekCh(SEEK_DN) == TRUE){
			SeekEnd();
		}
	}
	else if (ucTun == TUN_STATE_SEEK_UP)
	{
		if (SeekCh(SEEK_UP) == TRUE){
			SeekEnd();
		}
	}
	else if (ucTun == TUN_STATE_SEEK_STOP)
	{					
		SeekEnd();
	}
	else
	{
		if (TmrTunRSQ == 0){
			TmrTunRSQ = 5000;	
			CheckTunRSQ();
		}
	}
#endif	/*SI479XX_FIRM_REL_2_3*/

}


void CheckTunRSQ(void)								
{
#if defined(SI479XX_FIRM_REL_4_1)
#if (FEAT_TUN_DIGEN_SEQ_4_1)		
	stTuner.prc = TUNER_PRC_CHECK_RSQ;					
	stTuner.task = TUNER_TASK_PRC_CHECK_RSQ_START;
#else
	RETURN_CODE ret = SUCCESS;
	tuner_id tuner = { 0 };
	
	
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	if (ucBand <= B_FM_MAX) 											
	{
		SI479XX_TUNER_fm_rsq_status__data primary = { 0 };
			
		ret |= SI479XX_TUNER_fm_rsq_status__command(tuner.ic, tuner.id, FALSE, FALSE, FALSE);
		ret |= SI479XX_TUNER_fm_rsq_status__reply(tuner.ic, tuner.id, &primary); 							
	
		STIND = (primary.PDEV > 30)? 1: 0;
		CHVALID = primary.VALID? 1 : 0; 

		RSSI = primary.RSSI_ADJ;									/* -128 ~ 127 (dBuV) *//* 180922 SGsoft */
		SNR = primary.SNR;											/* -128 ~ 60 (dB) */
		MULT = primary.MULTIPATH;									/* 0 ~ 100 (%) */			
		DEV = primary.DEVIATION;									/* 0 ~ 255 (kHz) */
		PDEV = primary.PDEV;										/* 0 ~ 127 (100Hz) */
		USN =  primary.USN;											/* 0 ~ 127 (-dBFS) */
		LASSI = primary.LASSI;										/* -128 ~ 127 (dBc) */	
		HASSI = primary.HASSI;										/* -128 ~ 127 (dBc) */	
		FREQOFF = primary.FREQOFF;									/* -128 ~ 127 (kHz) */	
		COCHANL = primary.COCHANL;									/* 0 ~ 127 (-dBFS) */ 
	}								
	else
	{
		SI479XX_TUNER_am_rsq_status__data primary = { 0 };
	
		ret |= SI479XX_TUNER_am_rsq_status__command(tuner.ic, tuner.id, FALSE, FALSE, FALSE);
		ret |= SI479XX_TUNER_am_rsq_status__reply(tuner.ic, tuner.id, &primary); 							
			
		CHVALID = primary.VALID? 1 : 0; 

		RSSI = primary.RSSI_ADJ;									
		SNR = primary.SNR;
		LASSI = primary.LASSI;
		HASSI = primary.HASSI;
		FREQOFF = primary.FREQOFF;		
	}
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/
	
#elif defined(SI479XX_FIRM_REL_2_3)	
#if (FEAT_TUN_DIGEN_SEQ)		
	stTuner.prc = TUNER_PRC_CHECK_RSQ;					
	stTuner.task = TUNER_TASK_PRC_CHECK_RSQ_START;
#else
	RETURN_CODE ret = SUCCESS;
	uint8_t icNum, id;
	
	
#ifdef OPTION__IMAGETYPE__HD		
	icNum = hd_parts.primary.tuner.ic;
	id = hd_parts.primary.tuner.id;
#else
	icNum = dab_parts.fm_am.tuner.ic;
	id = dab_parts.fm_am.tuner.id;
#endif	/*OPTION__IMAGETYPE__HD*/

	if (ucBand <= B_FM_MAX) 											
	{
		SI479XX_TUNER_fm_rsq_status__data primary;
			
		ret |= SI479XX_TUNER_fm_rsq_status__command(icNum, id, FALSE, FALSE, FALSE);
		ret |= SI479XX_TUNER_fm_rsq_status__reply(icNum, id, &primary); 							
	
		STIND = (primary.PDEV > 30)? 1: 0;
		CHVALID = primary.VALID? 1 : 0; 

		RSSI = primary.RSSI_ADJ;									/* -128 ~ 127 (dBuV) *//* 180922 SGsoft */
		SNR = primary.SNR;											/* -128 ~ 60 (dB) */
		MULT = primary.MULTIPATH;									/* 0 ~ 100 (%) */			
		DEV = primary.DEVIATION;									/* 0 ~ 255 (kHz) */
		PDEV = primary.PDEV;										/* 0 ~ 127 (100Hz) */
		USN =  primary.USN;											/* 0 ~ 127 (-dBFS) */
		LASSI = primary.LASSI;										/* -128 ~ 127 (dBc) */	
		HASSI = primary.HASSI;										/* -128 ~ 127 (dBc) */	
		FREQOFF = primary.FREQOFF;									/* -128 ~ 127 (kHz) */	
		COCHANL = primary.COCHANL;									/* 0 ~ 127 (-dBFS) */ 
	}								
	else
	{
		SI479XX_TUNER_am_rsq_status__data primary;
	
		ret |= SI479XX_TUNER_am_rsq_status__command(icNum, id, FALSE, FALSE, FALSE);
		ret |= SI479XX_TUNER_am_rsq_status__reply(icNum, id, &primary); 							
			
		CHVALID = primary.VALID? 1 : 0; 

		RSSI = primary.RSSI_ADJ;									/* 180922 SGsoft */
		SNR = primary.SNR;
		LASSI = primary.LASSI;
		HASSI = primary.HASSI;
		FREQOFF = primary.FREQOFF;		
	}
#endif	/*FEAT_TUN_DIGEN_SEQ*/
#endif	/*SI479XX_FIRM_REL_2_3*/

}


void WriteTunFreq(U16 freq)												
{
#if defined(SI479XX_FIRM_REL_4_1)		  
	RETURN_CODE ret = SUCCESS;
	U16 esccnt = 0;
	tuner_id tuner;														


#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	ucMem = 0;															

	STCINT = 0;
	RSSI = 0;									
	SNR = 0;
	CHVALID = 0xFF;
	esccnt = 500;
	
	if (ucBand <= B_FM_MAX)												
	{
		volatile SI479XX_TUNER_fm_rsq_status__data primary;
		
		ret = SI479XX_TUNER_fm_tune_freq__command(tuner.ic, tuner.id,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
        											freq);

		ret = wait_for_stc(tuner, STC_DEFAULT_TIMEOUT_MS);
		if (ret == SUCCESS){					
			STCINT = 1;
		}
		ret = SI479XX_TUNER_fm_rsq_status(tuner.ic, tuner.id, false, false, false, &primary);
		
		RSSI = primary.RSSI;																
		SNR = primary.SNR;
		STBLEND = /*primary.BLENDPIN*/(primary.PDEV > 30)? 1: 0;
		CHVALID = primary.VALID? 1 : 0;											
	}
	else if (ucBand == B_WB)					/* 210504 SGsoft */	
	{
		SI479XX_TUNER_wb_rsq_status__data primary;
			
		ret = SI479XX_TUNER_wb_tune_freq__command(tuner.ic, tuner.id, 
													SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
													freq);
		
		ret = wait_for_stc(tuner, STC_DEFAULT_TIMEOUT_MS);
		if (ret == SUCCESS){					
			STCINT = 1;
		}
		ret = SI479XX_TUNER_wb_rsq_status(tuner.ic, tuner.id, false, false, false, &primary);

		RSSI = primary.RSSI;																
		SNR = primary.SNR;
		CHVALID = primary.VALID? 1 : 0;			
	}
	else
	{
		volatile SI479XX_TUNER_am_rsq_status__data primary;
		
		ret = SI479XX_TUNER_am_tune_freq__command(tuner.ic, tuner.id,
													0,							/* 200813 SGsoft */	
        											SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        											SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        											SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
        											freq);

		ret = wait_for_stc(tuner, STC_DEFAULT_TIMEOUT_MS);
		if (ret == SUCCESS){					/* 190130 SGsoft */	
			STCINT = 1;
		}
		ret = SI479XX_TUNER_am_rsq_status(tuner.ic, tuner.id, false, false, false, &primary);				
		
		RSSI = primary.RSSI;																
		SNR = primary.SNR;
		CHVALID = primary.VALID? 1 : 0;		
	}
	
	if (CHVALID == 0x01){
		;
	}
	
#elif defined(SI479XX_FIRM_REL_2_3)	
	RETURN_CODE ret = SUCCESS;
	U16 esccnt;
	uint8_t icNum, id;


#ifdef OPTION__IMAGETYPE__HD		
	icNum = hd_parts.primary.tuner.ic;
	id = hd_parts.primary.tuner.id;
#else
	icNum = dab_parts.fm_am.tuner.ic;
	id = dab_parts.fm_am.tuner.id;
#endif	/*OPTION__IMAGETYPE__HD*/

	ucMem = 0;															
	
	RSSI = 0;
	SNR = 0;
	esccnt = 500;
	
	//if (ucBand <= B_FM_MAX)												
	if(freq > 8700)
	{
		SI479XX_TUNER_fm_rsq_status__data primary;
		SI479XX_read_status__data status;
		
		ret |= SI479XX_TUNER_fm_tune_freq__command(icNum, id,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        											SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
        											freq);

		STCINT = 0;
		while(!STCINT){
			ret |= SI479XX_read_status__command(icNum);
			ret |= SI479XX_read_status__reply(icNum, &status);
			STCINT = status.STCINT;
			Delay10Us(1);
		}
		
        ret |= SI479XX_TUNER_fm_rsq_status__command(icNum, id, 
													FALSE, 
													FALSE, 
													FALSE);
        ret |= SI479XX_TUNER_fm_rsq_status__reply(icNum, id, &primary);								

		RSSI = primary.RSSI;																
		SNR = primary.SNR;
		STBLEND = (primary.PDEV > 30)? 1: 0;									
		CHVALID = primary.VALID? 1 : 0;											
	}					
	else
	{
		SI479XX_TUNER_am_rsq_status__data primary;
		SI479XX_read_status__data status;
		
		ret |= SI479XX_TUNER_am_tune_freq__command(icNum, id,
        											SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
        											SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
        											SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
        											freq);

		STCINT = 0;
		while(!STCINT){
			ret |= SI479XX_read_status__command(icNum);
			ret |= SI479XX_read_status__reply(icNum, &status);
			STCINT = status.STCINT;
			Delay10Us(1);
		}

		ret |= SI479XX_TUNER_am_rsq_status__command(icNum, id, FALSE, FALSE, FALSE);
        ret |= SI479XX_TUNER_am_rsq_status__reply(icNum, id, &primary);								
		
		RSSI = primary.RSSI;																
		SNR = primary.SNR;
		CHVALID = primary.VALID? 1 : 0;		
	}
	
	if (CHVALID == 0x01){
		;
	}
#endif	/*SI479XX_FIRM_REL_2_3*/

}


void ChangeBand(U8 fgsetmd)									
{
#if defined(SI479XX_FIRM_REL_4_1)		   
#if (FEAT_TUN_DIGEN_SEQ_4_1)		
	stTuner.prc = TUNER_PRC_BAND_CHANGE;					
	stTuner.task = TUNER_TASK_PRC_BAND_CHANGE_START;
#else
	RETURN_CODE ret = SUCCESS;	
	U8 band;		
	tuner_id tuner;
	uint8_t mode = 0;


	STCINT = 0;														

#ifdef OPTION_IMAGETYPE_DAB		/* 210430 SGsoft */
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, AUDIO_MUTE_BOTH);

	ucTun = TUN_STATE_DONE;										
	ucPreMem = 0;																	
	
	fgRadManu = 0;																			

	if (ucBand <= B_FM_MAX)
	{										
		band = RADIO_MODE_FM;												
		ucSTMO = S_STEREO;										
		lSFreq=AB_Info[(ucArea*2)].Start_Freq;	
		lEFreq=AB_Info[(ucArea*2)].Stop_Freq;	
		iStep=AB_Info[(ucArea*2)].Step;				
	}
#if defined(ENABLE_BAND_WB)
	else if (ucBand == B_WB){									
		band = RADIO_MODE_WB;
		lSFreq = 0;	
		lEFreq = 6;	
		iStep = 1;	
	}
#endif	/*ENABLE_BAND_WB*/
	else
	{
		band = RADIO_MODE_AM;											
		ucSTMO = S_MONO;										
		lSFreq=AB_Info[(ucArea*2)+1].Start_Freq;	
		lEFreq=AB_Info[(ucArea*2)+1].Stop_Freq;	
		iStep=AB_Info[(ucArea*2)+1].Step;
	}
	
	if ((lFreq <lSFreq) || (lFreq>lEFreq)){
		lFreq = lSFreq;	
	}

	#if 0	
	if (ucBand0 == ucBand){																		
		fgsetmd = 0;
	}
	#endif	/*0*/

	if (fgsetmd){		
		ret |= set_mode(tuner, band);
        ///ret |= SI4796X_TUNER_get_mode(tuner.ic, tuner.id, &mode);				
		ret |= si479x_set_customer_setting(band);									
	}

	ucBand0 = ucBand;	

	WriteTunFreq(lFreq);					

	ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, AUDIO_MUTE_NOT_MUTED);
	
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/

#elif defined(SI479XX_FIRM_REL_2_3)	
#if (FEAT_TUN_DIGEN_SEQ)		
	stTuner.prc = TUNER_PRC_BAND_CHANGE;					
	stTuner.task = TUNER_TASK_PRC_BAND_CHANGE_START;
#else
	RETURN_CODE ret = SUCCESS;	
	U8 band;		
	tuner_id tuner;


	//audcfg__set_mute(&_evb_system_cfg.audio, 1);			/* 180810 SGsoft */

#ifdef OPTION__IMAGETYPE__HD		/* 180608 SGsoft */
	tuner.ic = hd_parts.primary.tuner.ic;
	tuner.id = hd_parts.primary.tuner.id;
#else
	tuner.ic = dab_parts.fm_am.tuner.ic;
	tuner.id = dab_parts.fm_am.tuner.id;
#endif	/*OPTION__IMAGETYPE__HD*/

	ucTun = TUN_STATE_DONE;										
	ucPreMem = 0;																	
	
	fgRadManu = 0;																			

	if (ucBand <= B_FM_MAX)
	{										
		band = RADIO_MODE_FM;												
		ucSTMO = S_STEREO;										
		lSFreq=AB_Info[(ucArea*2)].Start_Freq;	
		lEFreq=AB_Info[(ucArea*2)].Stop_Freq;	
		iStep=AB_Info[(ucArea*2)].Step;				
	}
#if defined(ENABLE_BAND_WB)
	else if (ucBand == B_WB){									
		band = RADIO_MODE_WB;
		lSFreq = 0;	
		lEFreq = 6;	
		iStep = 1;	
	}
#endif	/*ENABLE_BAND_WB*/
	else
	{
		band = RADIO_MODE_AM;											
		ucSTMO = S_MONO;										
		lSFreq=AB_Info[(ucArea*2)+1].Start_Freq;	
		lEFreq=AB_Info[(ucArea*2)+1].Stop_Freq;	
		iStep=AB_Info[(ucArea*2)+1].Step;
	}
	
	if ((lFreq <lSFreq) || (lFreq>lEFreq)){
		lFreq = lSFreq;	
	}

	#if 0	/* 180804 SGsoft */
	if (ucBand0 == ucBand){																		
		fgsetmd = 0;
	}
	#endif	/*0*/

	if (fgsetmd){		
	#if defined(ENABLE_BAND_WB)
		if (ucBand <= B_FM_MAX){	
			set_mode(tuner, RADIO_MODE_AM);							
		}else if (ucBand == B_WB){	
			set_mode(tuner, RADIO_MODE_FM);	
		}else{
			set_mode(tuner, RADIO_MODE_WB);	
		}
		DelayMs(100);	
	#endif	/*ENABLE_BAND_WB*/
		
		ret |= set_mode(tuner, band);
	    si479x_set_customer_setting(band);
	}

	ucBand0 = ucBand;	

	WriteTunFreq(lFreq);				

	//audcfg__set_mute(&_evb_system_cfg.audio, 0);							
#endif	/*FEAT_TUN_DIGEN_SEQ*/
#endif	/*SI479XX_FIRM_REL_2_3*/

}


U8 IncDecFreq(U8 dir)
{
#if defined(SI479XX_FIRM_REL_2_3) || defined(SI479XX_FIRM_REL_4_1)			
	U8 band_limit=0;


    if(dir == FREQ_UP)
	{
    	if(lFreq == lEFreq){
			lFreq = lSFreq;
			band_limit = 1;
		}else{
			lFreq += iStep;
		}
    }
    else
    {
        if(lFreq == lSFreq){
			lFreq = lEFreq;
			band_limit = 1;
		}else{
			lFreq -= iStep;
		}
   	}
				
	return band_limit;
#endif	/*SI479XX_FIRM_REL_2_3 || SI479XX_FIRM_REL_4_1*/
	
}


void ChScan(U8 opt, U8 updn)
{			
	if (!opt){																	
		IncDecFreq(updn);
		WriteTunFreq(lFreq);
	}

}


U8 SeekCh(U8 dir)												
{
	U8 result, fdir;			
							

	result = FALSE;

	if (dir == SEEK_UP){
		fdir = FREQ_UP;
	}else{
		fdir = FREQ_DN;
	}
	
	IncDecFreq(fdir);						
	WriteTunFreq(lFreq);
	
	if ((CHVALID == 0x01) || (lFreq == lFreq0)) {  
		result = TRUE;
	}

	return result;

}


void Seek_Start(U8 dir)
{
	if (dir == SEEK_DN){
		ucTun = TUN_STATE_SEEK_DN;
	}else{
		ucTun = TUN_STATE_SEEK_UP;
	}
	lFreq0 = lFreq;

}


void SeekEnd(void)
{
	ucTun = TUN_STATE_DONE;

}


void SeekCancel(void)
{
	SeekEnd();
	
}


void RadSeekDown(void)												
{
#if (FEAT_TUN_DIGEN_SEQ) || (FEAT_TUN_DIGEN_SEQ_4_1)		
	ucPreMem = 0;														
	if (fgRadManu){
		stTuner.prc = TUNER_PRC_STEP_DOWN;					
		stTuner.task = TUNER_TASK_PRC_STEP_DOWN_START;
	}else{
		stTuner.prc = TUNER_PRC_SEEK_DOWN;					
		stTuner.task = TUNER_TASK_PRC_SEEK_DOWN_START;
	}
	
#else
	ucPreMem = 0;														
	if (fgRadManu){
		ChScan(CH_SEEK_MANU, FREQ_DN);
	}else{
		Seek_Start(SEEK_DN);												
	}
#endif	/*FEAT_TUN_DIGEN_SEQ || FEAT_TUN_DIGEN_SEQ_4_1*/

}


void RadKeySeekDown(void)
{
	if (ucTun == TUN_STATE_DONE){
		RadSeekDown();													
	}
	else if ((ucTun == TUN_STATE_AUTO_STORE) || (ucTun == TUN_STATE_SEEK_DN) || (ucTun == TUN_STATE_SEEK_UP))
	{
		SeekCancel();
	}
	#if 0
	else if (ucTun == TUN_STATE_PRESET_SCAN)
	{
		RadPScanCancel();
		RadSeekDown();													
	}
	#endif	/*0*/

}


void RadSeekUp(void)													
{
#if (FEAT_TUN_DIGEN_SEQ) || (FEAT_TUN_DIGEN_SEQ_4_1)		
	ucPreMem = 0;														
	if (fgRadManu){
		stTuner.prc = TUNER_PRC_STEP_UP;					
		stTuner.task = TUNER_TASK_PRC_STEP_UP_START;
	}else{
		stTuner.prc = TUNER_PRC_SEEK_UP;					
		stTuner.task = TUNER_TASK_PRC_SEEK_UP_START;
	}
#else
	ucPreMem = 0;														
	if (fgRadManu){
		ChScan(CH_SEEK_MANU, FREQ_UP);
	}else{
		Seek_Start(SEEK_UP);												
	}
#endif	/*FEAT_TUN_DIGEN_SEQ || FEAT_TUN_DIGEN_SEQ_4_1*/

}


void RadKeySeekUp(void)
{
	if (ucTun == TUN_STATE_DONE){
		RadSeekUp();													
	}
	else if ((ucTun == TUN_STATE_AUTO_STORE) || (ucTun == TUN_STATE_SEEK_DN) || (ucTun == TUN_STATE_SEEK_UP))
	{
		SeekCancel();								
		RadSeekUp();													
	}
	#if 0
	else if (ucTun == TUN_STATE_PRESET_SCAN)
	{
		RadPScanCancel();
		RadSeekUp();												
	}
	#endif	/*0*/

}


void TunFreqPreset(U8 prenum)								
{
#if defined(SI479XX_FIRM_REL_2_3) || defined(SI479XX_FIRM_REL_4_1)			
	lFreq = PresetMem[prenum];								
	WriteTunFreq(lFreq);		
#endif	/*SI479XX_FIRM_REL_2_3 || SI479XX_FIRM_REL_4_1*/

}


void RadKeyTunPreset(U8 mem)										
{
	if ((mem == 0) || (mem > NUM_OF_PRESET_MEMORY)){
		return;
	}
	
	if ((ucTun == TUN_STATE_AUTO_STORE) || (ucTun == TUN_STATE_SEEK_DN) || (ucTun == TUN_STATE_SEEK_UP))
	{
		SeekEnd();
	}
	else if (ucTun == TUN_STATE_PRESET_SCAN)
	{
		///RadPScanCancel();
	}

	if (mem){
		ucMem = mem - 1;
	}else{
		ucMem++;
		if (ucMem == NUM_OF_PRESET_MEMORY){
			ucMem = 0;
		}
	}

	TunFreqPreset(ucMem);
	ucTun = TUN_STATE_DONE;

}


void TunSetVolume(U8 level)												
{
#if defined(SI479XX_FIRM_REL_4_1)
	tuner_id tuner;


#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	set_tuner_prop(tuner, SI479XX_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, level);

#elif defined(SI479XX_FIRM_REL_2_3)
	tuner_id tuner;


#ifdef OPTION__IMAGETYPE__HD		
	tuner.ic = hd_parts.primary.tuner.ic;
	tuner.id = hd_parts.primary.tuner.id;
#else
	tuner.ic = dab_parts.fm_am.tuner.ic;
	tuner.id = dab_parts.fm_am.tuner.id;
#endif	/*OPTION__IMAGETYPE__HD*/

	set_tuner_prop(tuner, SI479XX_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, level);
#endif	/*SI479XX_FIRM_REL_4_1*/

}


void TunSeekUpDn(U8      band, U8 dir)												
{
#if defined(SI479XX_FIRM_REL_4_1)
	tuner_id tuner;


#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	if (dir == SEEK_UP)
	{
		if (band < B_AM1)
		{
			SI479XX_TUNER_fm_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_SEEKUP_ENUM_UP, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
		else
		{
			SI479XX_TUNER_am_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_SEEKUP_ENUM_UP, 
												0,
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
	}
	else
	{
		if (band < B_AM1)
		{
			SI479XX_TUNER_fm_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_SEEKUP_ENUM_DOWN, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
		else
		{
			SI479XX_TUNER_am_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_SEEKUP_ENUM_DOWN, 
												0,
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
	}

#elif defined(SI479XX_FIRM_REL_2_3)
	tuner_id tuner;

	
#ifdef OPTION__IMAGETYPE__HD		/* 180608 SGsoft */
	tuner.ic = hd_parts.primary.tuner.ic;
	tuner.id = hd_parts.primary.tuner.id;
#else
	tuner.ic = dab_parts.fm_am.tuner.ic;
	tuner.id = dab_parts.fm_am.tuner.id;
#endif	/*OPTION__IMAGETYPE__HD*/

	if (dir == SEEK_UP)
	{
		if (band < B_AM1)
		{
			SI479XX_TUNER_fm_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_SEEKUP_ENUM_UP, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
		else
		{
			SI479XX_TUNER_am_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_SEEKUP_ENUM_UP, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
	}
	else
	{
		if (band < B_AM1)
		{
			SI479XX_TUNER_fm_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_SEEKUP_ENUM_DOWN, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_FM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
		else
		{
			SI479XX_TUNER_am_seek_start__command(tuner.ic, tuner.id, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_SEEKUP_ENUM_DOWN, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_WRAP_ENUM_WRAP, 
												SI479XX_TUNER_CMD_AM_SEEK_START_ARG_MODE_ENUM_SEARCH);
		}
	}
#endif	/*SI479XX_FIRM_REL_4_1*/

}


U8 CheckPresetNum(void)									
{
	U8 i;

	for (i = 0;i < NUM_OF_PRESET_MEMORY;i++){			
		if (lFreq == PresetMem[i]){
			return (i + 1);
		}
	}

	return 0;

}


void si479x_set_customer_setting(uint8_t band)
{
#if defined(SI479XX_FIRM_REL_4_1)		
	return SUCCESS;

#elif defined(SI479XX_FIRM_REL_2_3)
	tuner_id tuner;
	RETURN_CODE err = SUCCESS;
//	SI479XX_TUNER_am_get_servo__data AmServoInfo;

	
#ifdef OPTION__IMAGETYPE__HD		
	tuner.ic = hd_parts.primary.tuner.ic;
	tuner.id = hd_parts.primary.tuner.id;
#else
	tuner.ic = dab_parts.fm_am.tuner.ic;
	tuner.id = dab_parts.fm_am.tuner.id;
#endif	/*OPTION__IMAGETYPE__HD*/


	if (band == RADIO_MODE_FM)															
    {
		if (ucArea == A_USA){
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 20);						/* 20kHz */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 0);					/* 75us */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0001);			/* WIDE */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_ACI100_THLD_ADDR, 127);				/* ACI100 Disable */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DCNR_DEFAULTS_CONTROL_ADDR, 0);				/* NA */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_DEFAULTS_CONTROL_ADDR, 0);			/* NA */
		}else if (ucArea == A_EU){
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 5);						/* 50kHz */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 1);					/* 50us */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0000);			/* NARROW */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DCNR_DEFAULTS_CONTROL_ADDR, 1);				/* EU */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_DEFAULTS_CONTROL_ADDR, 1);			/* EU */
		}else{
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 10);						/* 100kHz */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 0);					/* 75us */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0000);			/* NARROW */
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DCNR_DEFAULTS_CONTROL_ADDR, 1);
			err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_DEFAULTS_CONTROL_ADDR, 1);	
		}

		err |= SI479XX_TUNER_fm_set_servo_transform__command
				(tuner.ic, tuner.id, 
				13, 																	/*TRANSFORM_ID*/
				SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ,/*FM_METRIC_ID*/
				SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_SOFTMUTE,	/*FM_SERVO_ID*/ 
				-30,																	/*X1*/
				80,																		/*Y1*/ 
				9,																		/*X2*/
				0,																		/*Y2*/
				2000,																	/*P1TOP2RATE*/ 
				16);																	/*P2TOP1RATE attack*/
		
		err |= SI479XX_TUNER_fm_set_servo_transform__command
				(tuner.ic, tuner.id, 
				6, 																		/*TRANSFORM_ID*/
				SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ,/*FM_METRIC_ID*/
				SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_STBLEND,	/*FM_SERVO_ID*/ 
				25,																		/*X1*/
				0,																		/*Y1*/
				55/*57*/,																/*X2*/
				50,																		/*Y2*/
				2000,																	/*P1TOP2RATE*/ 
				100);	
				
		err |= SI479XX_TUNER_fm_set_servo_transform__command
				(tuner.ic, tuner.id, 
				2, 																		/*TRANSFORM_ID*/
				SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ,/*FM_METRIC_ID*/
				SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_HICUT,	/*FM_SERVO_ID*/ 
				5,																		/*X1*/
				50,																		/*Y1*/
				80/*84*/,																/*X2*/
				210,																	/*Y2*/
				2000,																	/*P1TOP2RATE*/ 
				100);																	/*attack*/
				
		err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_ADDR, 14);			/* -128 ~ 127 dBuV */ 
		err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_VALID_SNR_THRESHOLD_ADDR, 10);			/* -128 ~ 127 dB */
		
		err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, 96);				/* 0 ~ 96 */

		err |= SI479XX_TUNER_set_radio_audio_filter__command(tuner.ic, tuner.id, 		
            												FALSE, TRUE,
            												/*    b0          b1        b2            a1       a2    */
            												//0x000E17AC, 0x001C2F57, 0x000E17AC, 0x0013AF2B, 0xFFF3F227,		/* 10kHz LPF(S,1) *//* SW32_F22 */
															0x00105B3D, 0x0020B679, 0x00105B3D, 0x0009D25E, 0xFFF4C0B0,		/* 11kHz LPF(S,1) *//* SW32_F22 */
            												0x00400000, 0, 0, 0, 0,							
            												0x00400000, 0, 0, 0, 0);		
	}
	else if (band == RADIO_MODE_WB)																
	{
		;
	}
	else
	{
		err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_AM_VALID_RSSI_THRESHOLD_ADDR, 23/*16*/);	/* -128 ~ 127 dBuV */ // 45db
		err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_AM_VALID_SNR_THRESHOLD_ADDR, 15);			/* -128 ~ 127 dB */
		
		err |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, 96);				/* 0 ~ 96 */

	}

	return err;					
#endif	/*SI479XX_FIRM_REL_4_1*/
										
}

#endif	/*ENABLE_TUNER_SI479XX*/
