/*
 *
 * Copyright (c) 2021 SEJINSYSTEM Co. Ltd.
 * 
 *
 */

#include "sg_defines.h"

#if (FEAT_TUN_DIGEN_SEQ_4_1)		
#define	_SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_

#include "main.h"
#include "includes.h"									

#define FLASH_LOAD_IMAGE_START_ADD  0x2000		
#define SI479X_FW_SIZE				0x2DAA7		/* 187,047-byte, sejin_create_si4790x_image_4_1_4_sejin_default_flash_util_210513.bin */

#if (FEAT_FLASH_UTIL)		/* 210428 SGsoft */
//#include "sejin_create_si4790x_image_4_1_4_sejin_default_flash_util_220415.h" //"Sejin_create_si4790x_image_4_1_4_sejin_default_210513.h"//"flash_utility_only_4_0_0_0_8.h"		/* for test */
//"sejin_si4790x_image_2_3_29_flash_util_210325.h"

#endif	/*FEAT_FLASH_UTIL*/


RETURN_CODE DG_writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t *buffer)
{
    RETURN_CODE ret = SUCCESS;
    return_codes usbret = success;
    uint8_t status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
    uint8_t buf_copy[SI479XX_CMD_BUFFER_LENGTH] = { 0 };							
		

    // Make a copy of the command. If error handling needs to use APIs, this prevents the buffer from getting overwritten.
    memcpy(buf_copy, buffer, length);

   	// Commands need to be at least 2 bytes in length, shorter commands are an API mistake. (except READ_REPLY)
    if (length < 2 && buf_copy[0] != SI479XX_CMD_READ_STATUS)
    {
        ///c_log_commentf("API mistake? cmd length <2 bytes (%d)", length);
        length = 2;
    }
    usbret = SI479xx_writeCommand(icNum, length, buf_copy);
    ret = convert_errcode(usbret);
      
    return ret;
		
}


void TunerProcess(void)
{
	RETURN_CODE ret = SUCCESS;
	

#if (FEAT_DEBUG_211125)		/* 211125 SGsoft */
	if (fgARBERR){
		fgARBERR = 0;
	
		stTuner.prc = TUNER_PRC_ERROR;
		stTuner.seq = 1;
	}
#endif	/*FEAT_DEBUG_211125*/

	switch(stTuner.prc)
	{
	#if (FEAT_DEBUG_211125)		/* 211125 SGsoft */
		case TUNER_PRC_ERROR:
			switch(stTuner.seq)
			{
				case 1:
					{	
						uint8_t arg_data = 0x00;
						
						ret |= SI4796X_detail_status__command(IC0, 0xd0, &arg_data, 1);
						TmrTunerTaskSeq = 5;			/*5ms*/
						stTuner.seq = 2;
					}
					break;

				case 2:
					if (TmrTunerTaskSeq == 0)
					{	
						uint8_t rep_data[32];

						ret |= hal_readReply(IC0, 32, rep_data);
						memcpy(DETAIL_STATUS, rep_data, 32);

						stTuner.prc = TUNER_PRC_INIT;
						stTuner.seq = 0;
					}
					break;

				default:
					break;
			}
			break;
	#endif	/*FEAT_DEBUG_211125*/
	
		case TUNER_PRC_INIT:
			ret = TunerPrcInitSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_BAND_CHANGE:
			ret = TunerPrcBandChangeSeq();
			if (ret != SUCCESS){
				;
			}
			break;
			
		case TUNER_PRC_STEP_UP:
			ret = TunerPrcStepUpSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_STEP_DOWN:
			ret = TunerPrcStepDownSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_SEEK_UP:
			ret = TunerPrcSeekUpSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_SEEK_DOWN:
			ret = TunerPrcSeekDownSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_PRESET_TUNE:
			ret = TunerPrcPresetTuneSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_PRESET_SCAN:
			ret = TunerPrcPresetScanSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_AUTO_STORE:
			ret = TunerPrcAutoStoreSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_CHECK_RSQ:
			ret = TunerPrcCheckRsqSeq();
			if (ret != SUCCESS){
				;
			}
			break;

		case TUNER_PRC_DIRECT_TUNE:									
			ret = TunerPrcDirectTuneSeq();
			if (ret != SUCCESS){
				;
			}
			break;

	#if (FEAT_FLASH_UTIL)		
		case TUNER_PRC_FLASH_UTIL:
			ret = TunerPrcFlashUtilSeq();
			if (ret != SUCCESS){
				;
			}
			break;
	#endif	/*FEAT_FLASH_UTIL*/
			
		default:
			break;

	}

	if (stTuner.prc0 != stTuner.prc){									
		stTuner.prc00 = stTuner.prc0;
		stTuner.prc0 = stTuner.prc;
	}
	
}

void TunerPrcInitStart(void)
{
	stTuner.prc = TUNER_PRC_INIT;
	stTuner.task = TUNER_TASK_FW_LOAD;
	stTuner.seq = TUNER_FW_LOAD_RESET_L;
	
}


void TunerPrcBandChangeStart(void)
{
	stTuner.prc = TUNER_PRC_BAND_CHANGE;
	stTuner.task = TUNER_TASK_AUDIO_MUTE_ON;
	stTuner.seq = TUNER_AUDIO_MUTE_ON_1;
	
}


void TunerPrcStepUpStart(void)
{
	stTuner.prc = TUNER_PRC_STEP_UP;
	stTuner.task = TUNER_TASK_STEP_UP;
	
}


void TunerPrcStepDownStart(void)
{
	stTuner.prc = TUNER_PRC_STEP_DOWN;
	stTuner.task = TUNER_TASK_STEP_DOWN;
	
}


void TunerPrcSeekUpStart(void)
{
	stTuner.prc = TUNER_PRC_SEEK_UP;
	stTuner.task = TUNER_TASK_AUDIO_MUTE_ON;					
	stTuner.seq = TUNER_AUDIO_MUTE_ON_1;

	lFreq0 = lFreq;
	
}


void TunerPrcSeekDownStart(void)
{
	stTuner.prc = TUNER_PRC_SEEK_DOWN;
	stTuner.task = TUNER_TASK_AUDIO_MUTE_ON;					
	stTuner.seq = TUNER_AUDIO_MUTE_ON_1;

	lFreq0 = lFreq;
	
}


void TunerPrcPresetTuneStart(void)								
{
	stTuner.prc = TUNER_PRC_PRESET_TUNE;
	if (ucBand <= B_FM_MAX){
		stTuner.task = TUNER_TASK_FM_TUNE;
		stTuner.seq = TUNER_FM_TUNE_1;
	}else{
		stTuner.task = TUNER_TASK_AM_TUNE;
		stTuner.seq = TUNER_AM_TUNE_1;
	}
	
	ucMem = ucSetMem;												
//	lFreq = PresetMem[ucMem];
	
}


void TunerPrcPresetScanStart(void)
{	
	stTuner.prc = TUNER_PRC_PRESET_SCAN;
	if (ucBand <= B_FM_MAX){
		stTuner.task = TUNER_TASK_FM_TUNE;
		stTuner.seq = TUNER_FM_TUNE_1;
	}else{
		stTuner.task = TUNER_TASK_AM_TUNE;
		stTuner.seq = TUNER_AM_TUNE_1;
	}

	ucMem = 0;
	lFreq0 = lFreq;
//	lFreq = PresetMem[ucMem];

}


void TunerPrcAutoStoreStart(void)
{	
	U8 i;

	
	stTuner.prc = TUNER_PRC_AUTO_STORE;
	stTuner.task = TUNER_TASK_AUDIO_MUTE_ON;	
	stTuner.seq = TUNER_AUDIO_MUTE_ON_1;

	PreAutoStore.chcnt = 0;											
	for (i = 0;i < NUM_OF_PRESET_MEMORY;i++){							
		PreAutoStore.Freq[i] = PresetMem[i];
		PreAutoStore.SNR[i] = 0;
	}
	ucPreMem = 0;	
	
	lFreq0 = lFreq;

}


void TunerPrcCheckRsqStart(void)													
{	
	stTuner.prc = TUNER_PRC_CHECK_RSQ;
	stTuner.task = TUNER_TASK_CHECK_RSQ;					
	stTuner.seq = TUNER_CHECK_RSQ_1;
	
}


void TunerPrcDirectTuneStart(void)							/* 210417 SGsoft */													
{	
	stTuner.prc = TUNER_PRC_DIRECT_TUNE;
	if (ucBand <= B_FM_MAX){
		stTuner.task = TUNER_TASK_FM_TUNE;
		stTuner.seq = TUNER_FM_TUNE_1;
	}else{
		stTuner.task = TUNER_TASK_AM_TUNE;
		stTuner.seq = TUNER_AM_TUNE_1;
	}
	lFreq = drtlFreq;					

}


RETURN_CODE TunerPrcInitSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_INIT_START)									
	{
		TunerPrcInitStart();
	}
	else if (stTuner.task == TUNER_TASK_FW_LOAD)
	{
		ret = TunerTaskInit(&stTuner.seq);
	
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_AUDIO_CONFIG;
			stTuner.seq = TUNER_AUDIO_CONFIG_1;
		}
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_CONFIG)
	{
		ret = TunerTaskAudioConfig(&stTuner.seq);
	
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			#if 0		//TEST code
			stTuner.prc = TUNER_PRC_BAND_CHANGE;               
			stTuner.task = TUNER_TASK_PRC_BAND_CHANGE_START;
			#else
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
			#endif
		}
	}
	
	return ret;

}


RETURN_CODE TunerPrcBandChangeSeq(void)
{
    volatile RETURN_CODE ret = SUCCESS;

    if (stTuner.task == TUNER_TASK_PRC_BAND_CHANGE_START)									
    {
    	if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN)
    	{
    		;
    	}
    	else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE)
    	{
    		;
    	}		
    	
    	TunerPrcBandChangeStart();
		DPRINTF(DBG_INFO,"TunerPrcBandChangeStart\n\r");	
    }
    else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_ON)								
    {
    	TunerTaskAudioMuteOn(&stTuner.seq);
   	if (stTuner.seq == TUNER_TASK_SEQ_END)
    	{
    		stTuner.task = TUNER_TASK_BAND_SET;
    		stTuner.seq = TUNER_BAND_SET_1;
			DPRINTF(DBG_INFO,"TunerTaskAudioMuteOn\n\r");	
    	}
    }
    else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_OFF)									
    {
    	TunerTaskAudioMuteOff(&stTuner.seq);
    	if (stTuner.seq == TUNER_TASK_SEQ_END)
    	{
    		#if 1
    		stTuner.task = TUNER_TASK_NONE;
    		stTuner.prc = TUNER_PRC_NONE;
				DPRINTF(DBG_INFO,"TunerTaskAudioMuteOff\n\r");
    		#else
    		stTuner.task = TUNER_TASK_BAND_SET;
    		stTuner.prc = TUNER_BAND_SET_1;
    		#endif
    	}
    }
    else if (stTuner.task == TUNER_TASK_BAND_SET)
    {
    	ret = TunerTaskBandSet(&stTuner.seq);

    	if (stTuner.seq == TUNER_TASK_SEQ_END){
				DPRINTF(DBG_INFO,"TunerTaskBandSet\n\r");
    		if (ucBand <= B_FM_MAX){	/*FM*/
    			stTuner.task = TUNER_TASK_FM_AREA;
    			if (ucArea == A_EU){
    				stTuner.seq = TUNER_FM_AREA_EU_1;
    			}else if (ucArea == A_USA){
    				stTuner.seq = TUNER_FM_AREA_USA_1;
    		#if (FEAT_TUN_AREA_CHINA)		
    			}else if (ucArea == A_CHINA){
    				stTuner.seq = TUNER_FM_AREA_CHINA_1;
    		#endif	/*FEAT_TUN_AREA_CHINA*/
    			}else{
    				stTuner.seq = TUNER_FM_AREA_KR_1;
    			}
    		}else{	/*AM*/
                    #if (FEAT_CUSTOM_TUNING)		
                        stTuner.task = TUNER_TASK_AM_CUSTOM_TUNING;
                        stTuner.seq = TUNER_AM_CUSTOM_TUNING_1;
                    #else
    		#if (FEAT_DIGEN_4_1_TUNING)		/* 210803 SGsoft */
    			svindex = 0xFF;
    		#endif	/*FEAT_DIGEN_4_1_TUNING*/
                stTuner.task = TUNER_TASK_AM_CUSTOM;
                stTuner.seq = TUNER_AM_CUSTOM_1;
            #endif	/*FEAT_CUSTOM_TUNING*/
    		}
    	}
    }
    else if (stTuner.task == TUNER_TASK_FM_AREA)
    {
    	ret = TunerTaskFmArea(&stTuner.seq);
    	
    	if (stTuner.seq == TUNER_TASK_SEQ_END){
				DPRINTF(DBG_INFO,"TunerTaskFmArea\n\r");
        #if (FEAT_CUSTOM_TUNING)		
            stTuner.task = TUNER_TASK_FM_CUSTOM_TUNING;
            stTuner.seq = TUNER_FM_CUSTOM_TUNING_1;
        #else
    	#if (FEAT_DIGEN_4_1_TUNING)		/* 210803 SGsoft */
    		svindex = 0xFF;
    	#endif	/*FEAT_DIGEN_4_1_TUNING*/
            stTuner.task = TUNER_TASK_FM_CUSTOM;
    		stTuner.seq = TUNER_FM_CUSTOM_1;
        #endif	/*FEAT_CUSTOM_TUNING*/
    	}
    }
    else if (stTuner.task == TUNER_TASK_FM_CUSTOM)
    {
    	ret = TunerTaskFmCustom(&stTuner.seq);

    	if (stTuner.seq == TUNER_TASK_SEQ_END){
				DPRINTF(DBG_INFO,"TunerTaskFmCustom\n\r");
    		stTuner.task = TUNER_TASK_FM_TUNE;
    		stTuner.seq = TUNER_FM_TUNE_1;
    	}
    }
    else if (stTuner.task == TUNER_TASK_AM_CUSTOM)
    {
    	ret = TunerTaskAmCustom(&stTuner.seq);

    	if (stTuner.seq == TUNER_TASK_SEQ_END){
    		stTuner.task = TUNER_TASK_AM_TUNE;
    		stTuner.seq = TUNER_AM_TUNE_1;
    	}
    }
#if (FEAT_CUSTOM_TUNING)		
    else if (stTuner.task == TUNER_TASK_FM_CUSTOM_TUNING)
    {
        ret = TunerTaskFmCustomTuning(&stTuner.seq);

        if (stTuner.seq == TUNER_TASK_SEQ_END){
					DPRINTF(DBG_INFO,"TunerTaskFmCustomTuning\n\r");
            stTuner.task = TUNER_TASK_FM_TUNE;
            stTuner.seq = TUNER_FM_TUNE_1;
        }
    }
    else if (stTuner.task == TUNER_TASK_AM_CUSTOM_TUNING)
    {
        ret = TunerTaskAmCustomTuning(&stTuner.seq);

        if (stTuner.seq == TUNER_TASK_SEQ_END){
            stTuner.task = TUNER_TASK_AM_TUNE;
            stTuner.seq = TUNER_AM_TUNE_1;
        }
    }
#endif	/*FEAT_CUSTOM_TUNING*/
    else if (stTuner.task == TUNER_TASK_FM_TUNE)
    {
	ret = TunerTaskFmTune(&stTuner.seq, lFreq);

	if (stTuner.seq == TUNER_TASK_SEQ_END)
	{
		#if 1
		DPRINTF(DBG_INFO,"TunerTaskFmTune\n\r");
		stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;				
		stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
		#else
		stTuner.task = TUNER_TASK_NONE;
		stTuner.prc = TUNER_PRC_NONE;
		#endif
	}
    }
    else if (stTuner.task == TUNER_TASK_AM_TUNE)
    {
    	ret = TunerTaskAmTune(&stTuner.seq, lFreq);

    	if (stTuner.seq == TUNER_TASK_SEQ_END)
    	{	
    		#if 1
    		stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;				
    		stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
    		#else
    		stTuner.task = TUNER_TASK_NONE;
    		stTuner.prc = TUNER_PRC_NONE;
    		#endif
    	}
    }

    return ret;
}


RETURN_CODE TunerPrcStepUpSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_STEP_UP_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}	

		TunerPrcStepUpStart();
	}
	else if (stTuner.task == TUNER_TASK_STEP_UP)
	{
		IncDecFreq(FREQ_UP);
		if (ucBand <= B_FM_MAX){
			stTuner.task = TUNER_TASK_FM_TUNE;
			stTuner.seq = TUNER_FM_TUNE_1;
		}else{
			stTuner.task = TUNER_TASK_AM_TUNE;
			stTuner.seq = TUNER_AM_TUNE_1;
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
		
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;	

//			ucMem = CheckPresetNum();
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
		
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;

//			ucMem = CheckPresetNum();
		}
	}

	return ret;
	
}


RETURN_CODE TunerPrcStepDownSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_STEP_DOWN_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}	
		
		TunerPrcStepDownStart();
	}
	else if (stTuner.task == TUNER_TASK_STEP_DOWN)
	{
		IncDecFreq(FREQ_DN);
		if (ucBand <= B_FM_MAX){
			stTuner.task = TUNER_TASK_FM_TUNE;
			stTuner.seq = TUNER_FM_TUNE_1;
		}else{
			stTuner.task = TUNER_TASK_AM_TUNE;
			stTuner.seq = TUNER_AM_TUNE_1;
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
		
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;	
	
			ucMem = CheckPresetNum();
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
		
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
	
			ucMem = CheckPresetNum();
		}
	}

	return ret;
	
}


RETURN_CODE TunerPrcSeekUpSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;
	uint8 tmp=0;


	if (stTuner.task == TUNER_TASK_PRC_SEEK_UP_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_SEEK_UP){
			return ret;
		}else if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}
		
		TunerPrcSeekUpStart();
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_ON)									
	{
		TunerTaskAudioMuteOn(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_SEEK_UP;
		}
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_OFF)									
	{
		TunerTaskAudioMuteOff(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}
	else if (stTuner.task == TUNER_TASK_SEEK_UP)
	{
		IncDecFreq(FREQ_UP);
		if (ucBand <= B_FM_MAX){
			stTuner.task = TUNER_TASK_FM_TUNE;
			stTuner.seq = TUNER_FM_TUNE_1;
		}else{
			stTuner.task = TUNER_TASK_AM_TUNE;
			stTuner.seq = TUNER_AM_TUNE_1;
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if ((CHVALID == 0x01) || (lFreq == lFreq0))
			{  
				stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;			
				stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
				radio_packet.buf[4]= RADIO_SEEK_FINISH;
				radio_packet.buf[5]= 0x01;
				DPRINTF(DBG_INFO,"%s %d valid:%d\n\r",__FUNCTION__,lFreq,ucMem);		
			
//				ucMem = CheckPresetNum();
			}
			else
			{
				stTuner.task = TUNER_TASK_SEEK_UP;
				radio_packet.buf[4]= RADIO_SEEK_PROGRESS;;
				radio_packet.buf[5]= 0x00;
				DPRINTF(DBG_INFO,"%s %d invalid\n\r",__FUNCTION__,lFreq);
			}
			radio_packet.flag = ON;
			radio_packet.len = 7;
			radio_packet.buf[0]= 0x01;
			radio_packet.buf[1]= 0x00;
			radio_packet.buf[2]= (uint8)(lFreq>>8);
			radio_packet.buf[3]=(uint8)lFreq;
			if(RSSI<0)
				tmp=0;
			else
				tmp = (uint8)RSSI;
			radio_packet.buf[6]=tmp;
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if ((CHVALID == 0x01) || (lFreq == lFreq0))
			{  
				stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;			
				stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
				radio_packet.buf[4]= RADIO_SEEK_FINISH;
				radio_packet.buf[5]= 0x01;
				DPRINTF(DBG_INFO,"%s %d\n\r",__FUNCTION__,lFreq);			

//				ucMem = CheckPresetNum();
			}
			else
			{
				stTuner.task = TUNER_TASK_SEEK_UP;
				radio_packet.buf[4]= RADIO_SEEK_PROGRESS;;
				radio_packet.buf[5]= 0x00;
			}
			radio_packet.flag = ON;
			radio_packet.len = 7;
			radio_packet.buf[0]= 0x01;
			radio_packet.buf[1]= 0x00;
			radio_packet.buf[2]= (uint8)(lFreq>>8);
			radio_packet.buf[3]=(uint8)lFreq;
			if(RSSI<0)
				tmp=0;
			else
				tmp = (uint8)RSSI;
			radio_packet.buf[6]=tmp;
		}
	}

	return ret;
	
}


RETURN_CODE TunerPrcSeekDownSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;
	uint8 tmp=0;


	if (stTuner.task == TUNER_TASK_PRC_SEEK_DOWN_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_SEEK_DOWN){
			return ret;
		}else if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}
		
		TunerPrcSeekDownStart();
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_ON)									
	{
		TunerTaskAudioMuteOn(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_SEEK_DOWN;
		}
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_OFF)									
	{
		TunerTaskAudioMuteOff(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}
	else if (stTuner.task == TUNER_TASK_SEEK_DOWN)
	{
		IncDecFreq(FREQ_DN);
		if (ucBand <= B_FM_MAX){
			stTuner.task = TUNER_TASK_FM_TUNE;
			stTuner.seq = TUNER_FM_TUNE_1;
		}else{
			stTuner.task = TUNER_TASK_AM_TUNE;
			stTuner.seq = TUNER_AM_TUNE_1;
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if ((CHVALID == 0x01) || (lFreq == lFreq0))
			{  
				stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;			
				stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
				radio_packet.buf[4]= RADIO_SEEK_FINISH;
				radio_packet.buf[5]= 0x01;
//				ucMem = CheckPresetNum();
			}else{
				radio_packet.buf[4]= RADIO_SEEK_PROGRESS;
				radio_packet.buf[5]= 0x00;
				stTuner.task = TUNER_TASK_SEEK_DOWN;
			}
			radio_packet.flag = ON;
			radio_packet.len = 7;
			radio_packet.buf[0]= 0x01;
			radio_packet.buf[1]= 0x00;
			radio_packet.buf[2]= (uint8)(lFreq>>8);
			radio_packet.buf[3]=(uint8)lFreq;
			if(RSSI<0)
				tmp=0;
			else
				tmp = (uint8)RSSI;
			radio_packet.buf[6]=tmp;
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if ((CHVALID == 0x01) || (lFreq == lFreq0))
			{  
				stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;			
				stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
				radio_packet.buf[4]= RADIO_SEEK_FINISH;
				radio_packet.buf[5]= 0x01;
//				ucMem = CheckPresetNum();
			}else{
				radio_packet.buf[4]= RADIO_SEEK_PROGRESS;
				radio_packet.buf[5]= 0x00;
				stTuner.task = TUNER_TASK_SEEK_DOWN;
			}
			radio_packet.flag = ON;
			radio_packet.len = 7;
			radio_packet.buf[0]= 0x01;
			radio_packet.buf[1]= 0x00;
			radio_packet.buf[2]= (uint8)(lFreq>>8);
			radio_packet.buf[3]=(uint8)lFreq;
			if(RSSI<0)
				tmp=0;
			else
				tmp = (uint8)RSSI;
			radio_packet.buf[6]=tmp;
		}
	}

	return ret;
	
}


RETURN_CODE TunerPrcPresetTuneSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_PRESET_TUNE_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}
		
		TunerPrcPresetTuneStart();
	
		///DPRINTF(DBG_INFO,"%s %d\n\r",__FUNCTION__,lFreq);			/*digen*/
		
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}

	return ret;
	
}


RETURN_CODE TunerPrcPresetScanSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_PRESET_SCAN_START)									
	{
		if ((stTuner.prc0 == TUNER_PRC_PRESET_SCAN) && (stTuner.prc00 != TUNER_PRC_AUTO_STORE)){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}	
		stTuner.prc00 = TUNER_PRC_NONE;
		
		TunerPrcPresetScanStart();
	}
	else if (stTuner.task == TUNER_TASK_PRESET_SCAN)
	{
		if (TmrTunerTaskSeq == 0){
			ucMem++;
			if (ucMem >= NUM_OF_PRESET_MEMORY){
				ucMem = 0xFF;
//				lFreq = lFreq0;
			}else{
//				lFreq = PresetMem[ucMem];
			}
			
			if (ucBand <= B_FM_MAX){
				stTuner.task = TUNER_TASK_FM_TUNE;
				stTuner.seq = TUNER_FM_TUNE_1;
			}else{
				stTuner.task = TUNER_TASK_AM_TUNE;
				stTuner.seq = TUNER_AM_TUNE_1;
			}
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if (ucMem == 0xFF)
			{
				stTuner.task = TUNER_TASK_NONE;
				stTuner.prc = TUNER_PRC_NONE;
			}
			else
			{
				stTuner.task = TUNER_TASK_PRESET_SCAN;
				TmrTunerTaskSeq = 5000;
			}
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if (ucMem == 0xFF)
			{
				stTuner.task = TUNER_TASK_NONE;
				stTuner.prc = TUNER_PRC_NONE;
			}
			else
			{
				stTuner.task = TUNER_TASK_PRESET_SCAN;
				TmrTunerTaskSeq = 5000;
			}
		}
	}

	return ret;
	
}


RETURN_CODE TunerPrcAutoStoreSeq(void)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 i = 0, j = 0;
	uint8 tmp=0;


	if (stTuner.task == TUNER_TASK_PRC_AUTO_STORE_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}else if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}
		
		TunerPrcAutoStoreStart();
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_ON)									
	{
		TunerTaskAudioMuteOn(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_SEEK_UP;
		}
	}
	else if (stTuner.task == TUNER_TASK_AUDIO_MUTE_OFF)									
	{
		TunerTaskAudioMuteOff(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
//			stTuner.prc = TUNER_PRC_PRESET_SCAN;					
//			stTuner.task = TUNER_TASK_PRC_PRESET_SCAN_START;
			ucSetMem = 0;
			stTuner.prc = TUNER_PRC_PRESET_TUNE;
			stTuner.task = TUNER_TASK_PRC_PRESET_TUNE_START;
		}
	}
	else if (stTuner.task == TUNER_TASK_SEEK_UP)
	{
		IncDecFreq(FREQ_UP);
		if (ucBand <= B_FM_MAX){
			stTuner.task = TUNER_TASK_FM_TUNE;
			stTuner.seq = TUNER_FM_TUNE_1;
		}else{
			stTuner.task = TUNER_TASK_AM_TUNE;
			stTuner.seq = TUNER_AM_TUNE_1;
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			#if 1		//sorting ast freq
			if (CHVALID == 0x01)
			{  
				DPRINTF(DBG_INFO,"%s %d\n\r",__FUNCTION__,lFreq);			/*digen*/
				
				if (PreAutoStore.chcnt == 0){
					PreAutoStore.Freq[PreAutoStore.chcnt] = lFreq;
					PreAutoStore.SNR[PreAutoStore.chcnt] = SNR;
					PreAutoStore.chcnt++;
				}else{
					U16 tmpfreq, tmpsnr;
					
					for(i = 0;i < PreAutoStore.chcnt;i++){
						if (SNR > PreAutoStore.SNR[i]){
							tmpfreq = PreAutoStore.Freq[i];
							tmpsnr = PreAutoStore.SNR[i];
							PreAutoStore.Freq[i] = lFreq;
							PreAutoStore.SNR[i] = SNR;
							#if 0	/* 170112 SGsoft */
							for(j = (i+1);j < PreAutoStore.chcnt;j++){
								PreAutoStore.Freq[j] = tmpfreq;
								PreAutoStore.SNR[j] = tmpsnr;
								tmpfreq = PreAutoStore.Freq[j+1];
								tmpsnr = PreAutoStore.SNR[j+1];							
							}
							#endif	/*0*/
							break;
						}
					}
				}
				radio_packet.buf[5]= 0x1;
			}
			else
			{
				DPRINTF(DBG_INFO,"FM %s  invalid %d\n\r",__FUNCTION__,lFreq);
				radio_packet.buf[5]= 0x0;
			}
			
			if (PreAutoStore.chcnt < NUM_OF_PRESET_MEMORY){
				if (i == PreAutoStore.chcnt){
					PreAutoStore.Freq[i] = lFreq;
					PreAutoStore.SNR[i] = SNR;
					PreAutoStore.chcnt++;														
				}
			}
			
			if (lFreq == lFreq0)
			{
				U16 tmpfreq = 0, tmpsnr = 0;

				for(i = 0;i < (NUM_OF_PRESET_MEMORY/*PreAutoStore.chcnt*/ - 1);i++){					
					for(j = (i+1);j < NUM_OF_PRESET_MEMORY/*PreAutoStore.chcnt*/;j++){
						if (PreAutoStore.Freq[i] > PreAutoStore.Freq[j]){
							tmpfreq = PreAutoStore.Freq[i];
							tmpsnr = PreAutoStore.SNR[i];		
							PreAutoStore.Freq[i] = PreAutoStore.Freq[j];
							PreAutoStore.SNR[i] = PreAutoStore.SNR[j];
							PreAutoStore.Freq[j] = tmpfreq;
							PreAutoStore.SNR[j] = tmpsnr;
						}
					}	
				}
				
				for (i = 0;i < PreAutoStore.chcnt;i++){
					PresetMem[i] = PreAutoStore.Freq[i];
				
					///DPRINTF(DBG_INFO,"FM %s %d\n\r",__FUNCTION__,PresetMem[i]);				/*digen*/
				
					///SavePresetMemory(ucBand, i, PresetMem[i]);
				}
				#if 1
				stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;			/* 210327 SGsoft */
				stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
				#else
				 stTuner.prc = TUNER_PRC_NONE;
           	 	stTuner.task = TUNER_TASK_NONE;         
				#endif
				radio_packet.buf[4]= RADIO_AST_FINISH;
				radio_packet.flag = 2;
			}
			else
			{
				stTuner.task = TUNER_TASK_SEEK_UP;
				radio_packet.buf[4]= RADIO_AST_PROGRESS;
				radio_packet.flag = ON;
			}
			
			radio_packet.len = 7;
			radio_packet.buf[0]= 0x01;
			radio_packet.buf[1]= 0x00;
			radio_packet.buf[2]= (uint8)(lFreq>>8);
			radio_packet.buf[3]=(uint8)lFreq;
			if(RSSI<0)
				tmp=0;
			else
				tmp = (uint8)RSSI;
			radio_packet.buf[6]=tmp;
		#endif
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			if (CHVALID == 0x01)
			{  
				if (PreAutoStore.chcnt == 0){
					PreAutoStore.Freq[PreAutoStore.chcnt] = lFreq;
					PreAutoStore.SNR[PreAutoStore.chcnt] = SNR;
					PreAutoStore.chcnt++;
				}else{
					U16 tmpfreq, tmpsnr;
					
					for(i = 0;i < PreAutoStore.chcnt;i++){
						if (SNR > PreAutoStore.SNR[i]){
							tmpfreq = PreAutoStore.Freq[i];
							tmpsnr = PreAutoStore.SNR[i];
							PreAutoStore.Freq[i] = lFreq;
							PreAutoStore.SNR[i] = SNR;
							#if 0	/* 170112 SGsoft */
							for(j = (i+1);j < PreAutoStore.chcnt;j++){
								PreAutoStore.Freq[j] = tmpfreq;
								PreAutoStore.SNR[j] = tmpsnr;
								tmpfreq = PreAutoStore.Freq[j+1];
								tmpsnr = PreAutoStore.SNR[j+1];							
							}
							#endif	/*0*/
							break;
						}
					}
				}
				radio_packet.buf[5]= 0x1;
			}
			else
			{
				radio_packet.buf[5]= 0x0;
//				DPRINTF(DBG_INFO,"AM %s  invalid %d\n\r",__FUNCTION__,lFreq);
			}

			if (PreAutoStore.chcnt < NUM_OF_PRESET_MEMORY){
				if (i == PreAutoStore.chcnt){
					PreAutoStore.Freq[i] = lFreq;
					PreAutoStore.SNR[i] = SNR;
					PreAutoStore.chcnt++;														
				}
			}
			
			if (lFreq == lFreq0)
			{
				U16 tmpfreq = 0, tmpsnr = 0;

				for(i = 0;i < (NUM_OF_PRESET_MEMORY/*PreAutoStore.chcnt*/ - 1);i++){					
					for(j = (i+1);j < NUM_OF_PRESET_MEMORY/*PreAutoStore.chcnt*/;j++){
						if (PreAutoStore.Freq[i] > PreAutoStore.Freq[j]){
							tmpfreq = PreAutoStore.Freq[i];
							tmpsnr = PreAutoStore.SNR[i];		
							PreAutoStore.Freq[i] = PreAutoStore.Freq[j];
							PreAutoStore.SNR[i] = PreAutoStore.SNR[j];
							PreAutoStore.Freq[j] = tmpfreq;
							PreAutoStore.SNR[j] = tmpsnr;
						}
					}	
				}
				
				for (i = 0;i < PreAutoStore.chcnt;i++){
					PresetMem[i] = PreAutoStore.Freq[i];
				
					///DPRINTF(DBG_INFO,"AM %s %d\n\r",__FUNCTION__,PresetMem[i]);			
	
//					SavePresetMemory(ucBand, i, PresetMem[i]);
				}

				///TunerPrcPresetScanStart();
				stTuner.task = TUNER_TASK_AUDIO_MUTE_OFF;			
				stTuner.seq = TUNER_AUDIO_MUTE_OFF_1;
				radio_packet.buf[4]= RADIO_AST_FINISH;
				radio_packet.flag = 2;
			}
			else
			{
				stTuner.task = TUNER_TASK_SEEK_UP;
				radio_packet.buf[4]= RADIO_AST_PROGRESS;
				radio_packet.flag = ON;
			}
			radio_packet.len = 7;
			radio_packet.buf[0]= 0x01;
			radio_packet.buf[1]= 0x00;
			radio_packet.buf[2]= (uint8)(lFreq>>8);
			radio_packet.buf[3]=(uint8)lFreq;
			if(RSSI<0)
				tmp=0;
			else
				tmp = (uint8)RSSI;
			radio_packet.buf[6]=tmp;
		}
	}

	return ret;
	
}




RETURN_CODE TunerPrcCheckRsqSeq(void)											
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };


	if (stTuner.task == TUNER_TASK_PRC_CHECK_RSQ_START)
	{
		TunerPrcCheckRsqStart();
	}
	else if (stTuner.task == TUNER_TASK_CHECK_RSQ)									
	{
		ret = TunerTaskCheckRsq(&stTuner.seq);
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}
	
	return ret;
	
}


RETURN_CODE TunerPrcDirectTuneSeq(void)											
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_DIRECT_TUNE_START)									
	{
		if (stTuner.prc0 == TUNER_PRC_PRESET_SCAN){
			;
		}else if (stTuner.prc0 == TUNER_PRC_AUTO_STORE){
			;
		}

		if ((drtlFreq >= lSFreq) && (drtlFreq <= lEFreq)){
			drtlFreq -= (drtlFreq - lSFreq) % iStep;
			TunerPrcDirectTuneStart();
		}else{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
			
			return ERROR;
		}
	}
	else if (stTuner.task == TUNER_TASK_FM_TUNE)
	{
		ret = TunerTaskFmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}
	else if (stTuner.task == TUNER_TASK_AM_TUNE)
	{
		ret = TunerTaskAmTune(&stTuner.seq, lFreq);
					
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
		}
	}

	return ret;
	
}


RETURN_CODE TunerTaskInit(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	SI479XX_get_part_info__data pi = { 0 };
	SI479XX_get_func_info__data fi = { 0 };
	uint8 i =0;
	

	if (EscCnt >= 50){														
		*task_seq = TUNER_TASK_SEQ_END;
		return HAL_ERROR;
	}
	
	switch(*task_seq)
	{
		case TUNER_FW_LOAD_RESET_L:
		#if defined(OPTION_BUS_I2C)		
			I2C_Init(TUN_I2C);
		#endif	/*OPTION__BUS_I2C*/
			pTRST(0);
			TmrTunerTaskSeq = 2;
		
			*task_seq = TUNER_FW_LOAD_RESET_H;
			break;

		case TUNER_FW_LOAD_RESET_H:
			if (TmrTunerTaskSeq == 0){
				pTRST(1);
				TmrTunerTaskSeq = 3;

			#if (FEAT_TUN_AREA_CHINA)		/* 211201 SGsoft */
				RadioInit(A_CHINA);
			#else
				RadioInit(A_KR);
			#endif	/*FEAT_TUN_AREA_CHINA*/
				
				if (ucBand <= B_FM_MAX){								
					ucSTMO = S_STEREO;						
					lSFreq=AB_Info[(ucArea*2)].Start_Freq;	
					lEFreq=AB_Info[(ucArea*2)].Stop_Freq;	
					iStep=AB_Info[(ucArea*2)].Step;
				}else{
					ucSTMO = S_MONO;			
					lSFreq=AB_Info[(ucArea*2)+1].Start_Freq;	
					lEFreq=AB_Info[(ucArea*2)+1].Stop_Freq;	
					iStep=AB_Info[(ucArea*2)+1].Step;
				}
		
				*task_seq = TUNER_FW_LOAD_SYS_CFG;
			}
			break;

		case TUNER_FW_LOAD_SYS_CFG:
			if (TmrTunerTaskSeq == 0){
				global_data_init();

			#ifdef OPTION_IMAGETYPE_DAB
				ret |= syscfg_initialize(&g_evb_system_cfg, OPMODE_DAB, NULL);						 
			#else
				ret |= syscfg_initialize(&g_evb_system_cfg, OPMODE_HD, NULL);
			#endif	/*OPTION_IMAGETYPE_DAB*/
				RETURN_ERR(ret);
			
				ret |= initHardware(&g_evb_system_cfg.board);
				RETURN_ERR(ret);
				
				if (g_evb_system_cfg.audio.dc_type == DC_1T){
					*task_seq = TUNER_FW_LOAD_POWER_UP_C;
				}else{
					return ERROR;
				}
			}
			break;

		case TUNER_FW_LOAD_POWER_UP_C:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				struct si479xx_powerup_args pup_args = get_default_pup_args_4790(IC0, OPMODE_INVALID);

				ret |= SI479XX_power_up__command(IC0, 
												pup_args.ctsien, pup_args.clko_current, pup_args.vio, pup_args.clkout,  
												pup_args.clkmode, pup_args.trsize, pup_args.ctun, pup_args.xtal_freq,
												pup_args.ibias, pup_args.afs, pup_args.chipid, pup_args.eziq_master, pup_args.eziq_enable);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_POWER_UP_R;
				}
			}
			break;

		case TUNER_FW_LOAD_POWER_UP_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				SI479XX_power_up__data pup_reply = { 0 };
				U8 uncorrectable = 0, correctable = 0;
				
 				ret |= SI479XX_power_up__reply(IC0, &pup_reply);
				if (ret == SUCCESS){
					uncorrectable = pup_reply.UNCORRECTABLE;
					correctable = pup_reply.CORRECTABLE;
				
					TmrTunerTaskSeq = 1;											
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_CHECK_BOOTLOADER_R;
					
				}
			}
			break;

		case TUNER_FW_LOAD_CHECK_BOOTLOADER_R:
			if (TmrTunerTaskSeq == 0)
			{
				SI479XX_read_status__data rply = { 0 };							

				ret |= SI479XX_read_status(IC0, &rply);
				if (rply.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTLOADER) {
       				*task_seq = TUNER_FW_LOAD_PART_INFO_C;
				}else{
					TmrTunerTaskSeq = 1;												
					EscCnt++;
    			}
			}
			break;

		case TUNER_FW_LOAD_PART_INFO_C:
			ret |= SI479XX_get_part_info__command(IC0);
			if (ret == SUCCESS){
				*task_seq = TUNER_FW_LOAD_PART_INFO_R;
			}
			break;

	#if 0	/* 220415 SGsoft */
		case TUNER_FW_LOAD_PART_INFO_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_get_part_info__reply(IC0, &pi);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_4_1_VER_CHECK_C;
				}
			}
			break;

		case TUNER_FW_LOAD_4_1_VER_CHECK_C:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_FLASH_read_block__command(IC0, 0x2DAB0, 16); /* 16-byte : VERSION4.1.0.0.4 */
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_4_1_VER_CHECK_R;
				}
			}
			break;

		case TUNER_FW_LOAD_4_1_VER_CHECK_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				uint8_t verinfo[16] = { 0, };
				
				ret |= SI479XX_FLASH_read_block__reply(IC0, verinfo, 16);
				r_Device.ShareMemory[SHAREMEM_500_SiLab_Flash_Ver0] = verinfo[7];
				r_Device.ShareMemory[SHAREMEM_500_SiLab_Flash_Ver1] = verinfo[9];
				r_Device.ShareMemory[SHAREMEM_500_SiLab_Flash_Ver2] = verinfo[15];
				DPRINTF(DBG_INFO,"\r\nSiLab Tuner Current version : %x.%x%x\r\n",verinfo[7],verinfo[9],verinfo[15]);
			
				/* to compare tuner api version */
				
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_LOAD_INIT_C;
				}
			}
			break;

	#else
		case TUNER_FW_LOAD_PART_INFO_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_get_part_info__reply(IC0, &pi);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_LOAD_INIT_C;
				}
			}
			break;
	#endif	/*1*/

		case TUNER_FW_LOAD_LOAD_INIT_C:
			{
				uint32_t guid_table[2] = { 0 };
				uint8_t guidcnt = 0;
//				U8 flashblock[32];

				if (g_evb_system_cfg.audio.routing == ACFG_TUNERONLY){
					guid_table[0] = 0x00000B;								 	/*mcu.radio_amfmdab.boot*/
					guid_table[1] = 0x800001;									/*mcu.sejin.defaults.4.1.0.0.4.boot*/
					guidcnt = 2;
				}

			 	#if 0 		 
			 	{
//					U8 flashblock[32];
			
					SI479XX_FLASH_set_prop_propset__data flash_load_prop_list[2] = { {0x0001, 20}, {0x0002, 0} };
					
					ret |= SI479XX_FLASH_set_prop__command(IC0, &flash_load_prop_list[0], 1);
					ret |= SI479XX_FLASH_set_prop__command(IC0, &flash_load_prop_list[1], 1);

					ret |= SI479XX_FLASH_read_block__command(IC0, FLASH_LOAD_IMAGE_START_ADD, 32);
					ret |= SI479XX_FLASH_read_block__reply(IC0, flashblock, 32);
			 	}
			 	#endif 	/*0*/

				ret |= SI479XX_load_init__command(IC0, guidcnt, guid_table);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_FLASH_LOAD_LOAD_C;
				}
			}
			break;

		case TUNER_FW_LOAD_FLASH_LOAD_LOAD_C:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				#if defined(OPTION_BOOT_FROM_HOST)
					ret |= HostLoadWorker();
				#else
					ret |= SI479XX_FLASH_load_img(IC0, FLASH_LOAD_IMAGE_START_ADD, SI479X_FW_SIZE);
				#endif	/*OPTION_BOOT_FROM_HOST*/
				
				if (ret == SUCCESS){
					TmrTunerTaskSeq = 5;
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_WAIT_CTS;
				}
			}
			break;
			
		case TUNER_FW_LOAD_WAIT_CTS:
			if (TmrTunerTaskSeq == 0){
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
				if (ret == SUCCESS){
					TmrTunerTaskSeq = 0;
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_CHECK_BOOTREADY_R;
				}else{
					TmrTunerTaskSeq = 5;
					EscCnt++;
				}
			}
			break;

		case TUNER_FW_LOAD_CHECK_BOOTREADY_R:
			if (TmrTunerTaskSeq == 0)
			{
				SI479XX_read_status__data rply = { 0 };							

				ret |= SI479XX_read_status(IC0, &rply);
				if (rply.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTREADY){
       				*task_seq = TUNER_FW_LOAD_BOOT_C;
				}else{
					TmrTunerTaskSeq = 5;
					EscCnt++;
    			}
			}
			break;

		case TUNER_FW_LOAD_BOOT_C:
//			delay_ms(100);
			{
				ret |= SI4796X_boot__command(IC0);

				if (ret == SUCCESS){
					TmrTunerTaskSeq = 5;
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_CHECK_APPLICATION_R;
				}
			}
			break;
			
		case TUNER_FW_LOAD_CHECK_APPLICATION_R:
			if (TmrTunerTaskSeq == 0)
			{
				SI479XX_read_status__data rply = { 0 };							

				ret |= SI479XX_read_status(IC0, &rply);
				if (rply.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_APPLICATION){
       				*task_seq = TUNER_FW_LOAD_FUNC_INFO_C;
				}else{
					TmrTunerTaskSeq = 5;
					EscCnt++;
    			}
			}
			break;

		case TUNER_FW_LOAD_FUNC_INFO_C:
			ret |= SI479XX_get_func_info__command(IC0);
			if (ret == SUCCESS){
				*task_seq = TUNER_FW_LOAD_FUNC_INFO_R;
			}
			break;

		case TUNER_FW_LOAD_FUNC_INFO_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){

				ret |= SI479XX_get_func_info__reply(IC0, &fi);
				if (ret == SUCCESS){
					*task_seq = TUNER_TASK_SEQ_END;
				}
			}
			break;
	
		default:
			break;
	
	}

	return ret;

}


RETURN_CODE TunerTaskAudioConfig(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = {.ic = g_evb_system_cfg.audio.ic, .id = TUNER_ID0 };
	

	if (g_evb_system_cfg.audio.routing == ACFG_TUNERONLY)						
	{
		switch(*task_seq)
		{
			case TUNER_AUDIO_CONFIG_1:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
				if (ret == SUCCESS)
				{
					ret |= SI479XX_dac_config__command(IC0,
												SI479XX_CMD_DAC_CONFIG_ARG_PUMODE01_ENUM_SLOW,
												SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_NONE/*SI479XX_CMD_DAC_CONFIG_ARG_DAC01_MODE_ENUM_DEMOD*/, 
												0,
												0,
												0,
												0);
					
					if (ret == SUCCESS){
						*task_seq = TUNER_AUDIO_CONFIG_2;
					}
				}
				break;

			case TUNER_AUDIO_CONFIG_2:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
				if (ret == SUCCESS)
				{
					ret |= set_tuner_prop(tuner, SI479XX_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR, SI479XX_TUNER_PROP_RADIO_ANALOG_VOLUME_VOL_MAX);
					if (ret == SUCCESS){
						*task_seq = TUNER_AUDIO_CONFIG_3;
					}
				}
				break;

			case TUNER_AUDIO_CONFIG_3:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
				if (ret == SUCCESS)
				{
					ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, SI4796X_TUNER_PROP_RADIO_MUTE_ANALOG_DEMOD_INPUT_MUTE_ENUM_BOTH);
					if (ret == SUCCESS){
						fgRadMuteOnOff = AUDIO_MUTE_BOTH;
						*task_seq = TUNER_AUDIO_CONFIG_4;
					}
				}
				break;

			case TUNER_AUDIO_CONFIG_4:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
				if (ret == SUCCESS){
					*task_seq = TUNER_AUDIO_CONFIG_5;
				}
				break;

			case TUNER_AUDIO_CONFIG_5:		
				#if 1
				ret |= SI479XX_i2stx0_config(tuner.ic, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_SAMPLE_RATE_ENUM_FS48KS, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_WHITENCONTROL_ENUM_DISABLE, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_ENABLE_ENUM_ENABLE,
											SI479XX_CMD_I2STX0_CONFIG_ARG_PURPOSE_ENUM_AUDIO, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_FORCE_ENUM_OFF, 					/* 210526 SGsoft */
											SI479XX_CMD_I2STX0_CONFIG_ARG_MASTER_ENUM_SLAVE, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_CLKFS_ENUM_GP, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_DATA0_ENUM_DOUT0, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_FILL_ENUM_ZEROS, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_SLOT_SIZE_ENUM_THIRTYTWO, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_SAMPLE_SIZE_ENUM_THIRTYTWO, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_BITORDER_ENUM_MSBFIRST, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_SWAP_ENUM_LEFTRIGHT, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_CLKINV_ENUM_NORMAL, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_FRAMING_MODE_ENUM_I2S, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_CLOCKDOMAIN_ENUM_AUDIOSYNTH, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_INVERTFS_ENUM_NORMAL, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_HIZ_ENUM_ZERO, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_LENGTH_ENUM_OFF, 
											SI479XX_CMD_I2STX0_CONFIG_ARG_DATA1_ENUM_NONE);
			
				if (ret == SUCCESS){
					*task_seq = TUNER_AUDIO_CONFIG_6;
				}
				#else
				*task_seq = TUNER_AUDIO_CONFIG_7;
				#endif
				break;

			case TUNER_AUDIO_CONFIG_6:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);					
				if (ret == SUCCESS){
					ret |= SI479XX_i2s_control__command(tuner.ic, 0/*PORT_I2S_TX0*/, 4/*PORT_MODE_ENABLE*/);
					if (ret == SUCCESS){
						*task_seq = TUNER_AUDIO_CONFIG_7;
					}
				}
				break; 

			case TUNER_AUDIO_CONFIG_7:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);						
				if (ret == SUCCESS){
					*task_seq = TUNER_TASK_SEQ_END;										
				}
				break; 
			
			default:
				break;
		}
	}
	else if (g_evb_system_cfg.audio.routing == ACFG_HIFI_PORT_ONLY)						
	{
		;
	}
	else if (g_evb_system_cfg.audio.routing == ACFG_HIFI_REF_DESIGN)						
	{
		;
	}

	return ret;

}


RETURN_CODE TunerTaskAudioMuteOn(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };

	
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/
	
	switch(*task_seq)
	{
		case TUNER_AUDIO_MUTE_ON_1:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
			if (ret == SUCCESS)
			{
				ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, SI4796X_TUNER_PROP_RADIO_MUTE_ANALOG_DEMOD_INPUT_MUTE_ENUM_BOTH);					/* MUTE ON */
				if (ret == SUCCESS){
					fgRadMuteOnOff = AUDIO_MUTE_BOTH;
					*task_seq = TUNER_AUDIO_MUTE_ON_2;
				}
			}
			break;
		
		case TUNER_AUDIO_MUTE_ON_2:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
			if (ret == SUCCESS)
			{
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;

		default:
			break;
	}

	return ret;
	
}


RETURN_CODE TunerTaskAudioMuteOff(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };
	

#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	switch(*task_seq)
	{
		case TUNER_AUDIO_MUTE_OFF_1:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
			if (ret == SUCCESS)
			{
				ret |= set_tuner_prop(tuner, SI4796X_TUNER_PROP_RADIO_MUTE_ADDR, SI4796X_TUNER_PROP_RADIO_MUTE_ANALOG_DEMOD_INPUT_MUTE_ENUM_UNMUTE);					/* MUTE OFF */
				if (ret == SUCCESS){
					fgRadMuteOnOff = AUDIO_MUTE_NOT_MUTED;
					*task_seq = TUNER_AUDIO_MUTE_OFF_2;
				}
			}
			break;
		
		case TUNER_AUDIO_MUTE_OFF_2:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
			if (ret == SUCCESS)
			{
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;

		default:
			break;
	}
	
	return ret;
	
}


RETURN_CODE TunerTaskBandSet(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH];
	tuner_id tuner;


#ifdef OPTION_IMAGETYPE_DAB		/* 210430 SGsoft */
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	switch(*task_seq)
	{
		case TUNER_BAND_SET_1:											
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);			
			if (ret == SUCCESS)
			{
				U8 band, i;
				
				ucTun = TUN_STATE_DONE;										
				ucPreMem = 0;												

				fgRadManu = 0;											
			
				///LoadPresetMemory(ucBand);									
				///LoadCurFreq(ucBand);																		

				if (ucBand <= B_FM_MAX){
	               band = RADIO_MODE_FM;
	               ucSTMO = S_STEREO;                              
	               lSFreq=AB_Info[(ucArea*2)].Start_Freq;   
	               lEFreq=AB_Info[(ucArea*2)].Stop_Freq;   
	               iStep=AB_Info[(ucArea*2)].Step;   

	               for (i = 0;i < NUM_OF_PRESET_MEMORY;i++){
	                  PresetMem[i] = default_freq[ucArea*12 + i];
	               }
	               ucMem = 0;
//	               lFreq = 9310/*PresetMem[ucMem]*/;
	            }else if (ucBand == B_WB){   
	               band = RADIO_MODE_WB;
	               lSFreq = 0;   
	               lEFreq = 6;   
	               iStep = 1;         
	            }else{      
	               band = RADIO_MODE_AM;
	               ucSTMO = S_MONO;
	               lSFreq=AB_Info[(ucArea*2)+1].Start_Freq;   
	               lEFreq=AB_Info[(ucArea*2)+1].Stop_Freq;   
	               iStep=AB_Info[(ucArea*2)+1].Step;

	               for (i = 0;i < NUM_OF_PRESET_MEMORY;i++){
	                  PresetMem[i] = default_freq[ucArea*12 + NUM_OF_PRESET_MEMORY + i];
	               }
	               	ucMem = 0;
//	               	lFreq = PresetMem[ucMem];
//					lFreq = 1404;
	            }
	               
	            if ((lFreq <lSFreq) || (lFreq>lEFreq)){
	               lFreq = lSFreq;   
	            }

	            ret |= SI479XX_TUNER_set_mode__command(tuner.ic, tuner.id, band);
	            if (ret == SUCCESS){
					TmrTunerTaskSeq = 5;
	               *task_seq = TUNER_BAND_SET_2;
	            }
	         }
				
			break;

		case TUNER_BAND_SET_2:
			if (TmrTunerTaskSeq == 0)
			{ 
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);

#if (FEAT_DEBUG_ARBERR) 	/* 211018 SGsoft *//* 211117 SGsoft */							
							{
								uint8_t arg_data = 0x00;
								uint8_t rep_data[32] = { 0, };
							
								ret |= SI4796X_detail_status__command(IC0, 0xd0, &arg_data, 1);
								DelayMs(10);
								ret |= SI4796X_detail_status__reply(IC0, rep_data);
								
								if (ret == SUCCESS){
									/*** print rep_data ***/
									*task_seq = TUNER_TASK_SEQ_END;
								}
							}
#endif	/*FEAT_DEBUG_ARBERR*/

		
				if (ret == SUCCESS){
					SI479XX_read_status__data status = { 0 };

					ret |= SI479XX_read_status(IC0, &status);
					STCINT = status.STCINT;
					if (STCINT){
						*task_seq = TUNER_TASK_SEQ_END;
					}else{
						TmrTunerTaskSeq = 5;
					}
				}
			}
			break;
		
		default:
			break;
	}

	return ret;
	
}


RETURN_CODE TunerTaskFmArea(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };
		
	
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/
		
	switch(*task_seq)
	{
		/* A_KR */
		case TUNER_FM_AREA_KR_1:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 10);	
				if (ret == SUCCESS){
					*task_seq = TUNER_FM_AREA_KR_2;
				}
			}
			break;

		case TUNER_FM_AREA_KR_2:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_KR_3;
			}
			break;

		case TUNER_FM_AREA_KR_3:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 0);	

			
#if (FEAT_DEBUG_ARBERR) 	/* 211018 SGsoft *//* 211117 SGsoft */							
			{
				uint8_t arg_data = 0x00;
				uint8_t rep_data[32] = { 0, };
			
				ret |= SI4796X_detail_status__command(IC0, 0xd0, &arg_data, 1);
				DelayMs(10);
				ret |= SI4796X_detail_status__reply(IC0, rep_data);
				
				if (ret == SUCCESS){
					/*** print rep_data ***/
					*task_seq = TUNER_TASK_SEQ_END;
				}
			}
#endif	/*FEAT_DEBUG_ARBERR*/
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_KR_4;
			}
			break;

		case TUNER_FM_AREA_KR_4:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_KR_5;
			}
			break;

		case TUNER_FM_AREA_KR_5:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0020);	/*FAST SERVO & NARROW*/
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_KR_6;
			}
			break;

		case TUNER_FM_AREA_KR_6:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;

		/* A_USA */
		case TUNER_FM_AREA_USA_1:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 20);	
			if (ret == SUCCESS){
				*task_seq =  TUNER_FM_AREA_USA_2;
			}
			break;

		case TUNER_FM_AREA_USA_2:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_USA_3;
			}
			break;

		case TUNER_FM_AREA_USA_3:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 0);		
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_USA_4;
			}
			break;

		case TUNER_FM_AREA_USA_4:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_USA_5;
			}
			break;

		case TUNER_FM_AREA_USA_5:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0021);		/*FAST SERVO & WIDE*/
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_USA_6;
			}
			break;

		case TUNER_FM_AREA_USA_6:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_USA_7;
			}
			break;

		case TUNER_FM_AREA_USA_7:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_ACI100_THLD_ADDR, 127);	
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_USA_8;
			}
			break;

		case TUNER_FM_AREA_USA_8:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;
			
		/* A_EU */
		case TUNER_AM_AREA_EU_1:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 5);
			if (ret == SUCCESS){
				*task_seq = TUNER_AM_AREA_EU_2;
			}
			break;

		case TUNER_AM_AREA_EU_2:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_AM_AREA_EU_3;
			}
			break;

		case TUNER_AM_AREA_EU_3:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 1);	
			if (ret == SUCCESS){
				*task_seq = TUNER_AM_AREA_EU_4;
			}
			break;

		case TUNER_AM_AREA_EU_4:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_AM_AREA_EU_5;
			}
			break;

		case TUNER_AM_AREA_EU_5:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0020);			/*FAST SERVO & NARROW*/
			if (ret == SUCCESS){
				*task_seq = TUNER_AM_AREA_EU_6;
			}
			break;

		case TUNER_AM_AREA_EU_6:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;

	#if (FEAT_TUN_AREA_CHINA)		
		/* A_CHINA */
		case TUNER_FM_AREA_CHINA_1:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_SEEK_SPACING_ADDR, 10); 		/*100kHz*/
				if (ret == SUCCESS){
					*task_seq = TUNER_FM_AREA_CHINA_2;
				}
			}
			break;
		
		case TUNER_FM_AREA_CHINA_2:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_CHINA_3;
			}
			break;
		
		case TUNER_FM_AREA_CHINA_3:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR, 0); 			/*75us*/	
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_CHINA_4;
			}
			break;
		
		case TUNER_FM_AREA_CHINA_4:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_CHINA_5;
			}
			break;
		
		case TUNER_FM_AREA_CHINA_5:
			set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR, 0x0020);	/*FAST SERVO & NARROW*/
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_AREA_CHINA_6;
			}
			break;
		
		case TUNER_FM_AREA_CHINA_6:
			ret |= wait_for_CTS(tuner.ic, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;
	#endif	/*FEAT_TUN_AREA_CHINA*/
	
		default:
			break;

	}

	return ret;

}


RETURN_CODE TunerTaskFmCustom(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };
			
		
#ifdef OPTION_IMAGETYPE_DAB		/* 210513 SGsoft */
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	switch(*task_seq)
	{
	#if (FEAT_TUN_AREA_CHINA)		
	#if (FEAT_DIGEN_4_1_TUNING)		/* 210803 SGsoft */
		case TUNER_FM_CUSTOM_1:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
				ret |= SI479XX_TUNER_fm_set_servo(tuner.ic, tuner.id, 
												FM_SERVO[svindex].sid, FM_SERVO[svindex].min, FM_SERVO[svindex].max, 
												FM_SERVO[svindex].init, FM_SERVO[svindex].acc);
				if (ret == SUCCESS){
					svindex++;
					if (svindex >= (sizeof(FM_SERVO)/sizeof(CUSTOM_SERVO))){
						svindex = 0xFF;
			        	*task_seq = TUNER_FM_CUSTOM_2;
					}
				}
			}
			break;

		case TUNER_FM_CUSTOM_2:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
                ret |= SI479XX_TUNER_fm_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         svindex + 1, 															/*TRANSFORM_ID*/
                         FM_SERVO_TRANSFORM[svindex].mid,                                       /*FM_METRIC_ID*/
                         FM_SERVO_TRANSFORM[svindex].sid,	    								/*FM_SERVO_ID*/
                         FM_SERVO_TRANSFORM[svindex].x1,										/*X1*/
                         FM_SERVO_TRANSFORM[svindex].y1,										/*Y1*/
                         FM_SERVO_TRANSFORM[svindex].x2,										/*X2*/
                         FM_SERVO_TRANSFORM[svindex].y2,										/*Y2*/
                         FM_SERVO_TRANSFORM[svindex].p1_p2,										/*P1TOP2RATE*/
                         FM_SERVO_TRANSFORM[svindex].p2_p1,										/*P2TOP1RATE*/
                         0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(FM_SERVO_TRANSFORM)/sizeof(CUSTOM_SERVO_TRANSFORM))){
						svindex = 0xFF;
			        	*task_seq = TUNER_FM_CUSTOM_3;
					}
                }
            }
            break;

		case TUNER_FM_CUSTOM_3:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
                ret |= SI479XX_TUNER_fm_set_fast_servo_transform__command
	                    (tuner.ic, tuner.id,
	                     FM_FAST_TRANSFORM[svindex].fid, 										/*FAST_TRANSFORM_ID*/
	                     FM_FAST_TRANSFORM[svindex].en,	                                		/*ENABLE*/
	                     FM_FAST_TRANSFORM[svindex].x1,											/*X1*/
	                     FM_FAST_TRANSFORM[svindex].y1,											/*Y1*/
	                     FM_FAST_TRANSFORM[svindex].x2,											/*X2*/
	                     FM_FAST_TRANSFORM[svindex].y2,											/*Y2*/
	                     FM_FAST_TRANSFORM[svindex].p1_p2,										/*P1TOP2RATE*/
	                     FM_FAST_TRANSFORM[svindex].p2_p1);										/*P2TOP1RATE*/
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(FM_FAST_TRANSFORM)/sizeof(CUSTOM_FAST_SERVO_TRANSFORM))){
						svindex = 0xFF;
			        	*task_seq = TUNER_FM_CUSTOM_4;
					}
                }
            }
            break;

		case TUNER_FM_CUSTOM_4:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
                ret |= set_tuner_prop(tuner, FM_PROPERTY[svindex].prop, FM_PROPERTY[svindex].val);	
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(FM_PROPERTY)/sizeof(CUSTOM_PROPERTY))){
						svindex = 0xFF;
			        	*task_seq = TUNER_FM_CUSTOM_5;
					}
                }
            }
            break;

		case TUNER_FM_CUSTOM_5:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}

				tuner.id = TUNER_ID_INVALID;
                ret |= set_tuner_prop(tuner, COMMON_PROPERTY[svindex].prop, COMMON_PROPERTY[svindex].val);	
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(COMMON_PROPERTY)/sizeof(CUSTOM_PROPERTY))){
						svindex = 0xFF;
			        	*task_seq = TUNER_TASK_SEQ_END;
					}
                }
            }
            break;

	#else
		case TUNER_FM_CUSTOM_1:
			ucBand0 = ucBand;	
			*task_seq = TUNER_TASK_SEQ_END;
			break;
	#endif	/*FEAT_DIGEN_4_1_TUNING*/
	
	#else
		case TUNER_FM_CUSTOM_1:
			ucBand0 = ucBand;	
			*task_seq = TUNER_TASK_SEQ_END;
			break;
	#endif	/*FEAT_TUN_AREA_CHINA*/

		default:
			break;
	}

	return ret;

}


RETURN_CODE TunerTaskAmCustom(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };

	
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	switch(*task_seq)
	{
	#if (FEAT_DIGEN_4_1_TUNING)		/* 211217 SGsoft */
		case TUNER_AM_CUSTOM_1:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
				ret |= SI479XX_TUNER_am_set_servo(tuner.ic, tuner.id, 
												AM_SERVO[svindex].sid, AM_SERVO[svindex].min, AM_SERVO[svindex].max, 
												AM_SERVO[svindex].init, AM_SERVO[svindex].acc);
				if (ret == SUCCESS){
					svindex++;
					if (svindex >= (sizeof(AM_SERVO)/sizeof(CUSTOM_SERVO))){
						svindex = 0xFF;
			        	*task_seq = TUNER_AM_CUSTOM_2;
					}
				}
			}
			break;

		case TUNER_AM_CUSTOM_2:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
                ret |= SI479XX_TUNER_am_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         svindex + 1, 															/*TRANSFORM_ID*/
                         AM_SERVO_TRANSFORM[svindex].mid,                                       /*AM_METRIC_ID*/
                         AM_SERVO_TRANSFORM[svindex].sid,	    								/*AM_SERVO_ID*/
                         AM_SERVO_TRANSFORM[svindex].x1,										/*X1*/
                         AM_SERVO_TRANSFORM[svindex].y1,										/*Y1*/
                         AM_SERVO_TRANSFORM[svindex].x2,										/*X2*/
                         AM_SERVO_TRANSFORM[svindex].y2,										/*Y2*/
                         AM_SERVO_TRANSFORM[svindex].p1_p2,										/*P1TOP2RATE*/
                         AM_SERVO_TRANSFORM[svindex].p2_p1,										/*P2TOP1RATE*/
                         0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(AM_SERVO_TRANSFORM)/sizeof(CUSTOM_SERVO_TRANSFORM))){
						svindex = 0xFF;
			        	*task_seq = TUNER_AM_CUSTOM_3;
					}
                }
            }
            break;

		case TUNER_AM_CUSTOM_3:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}
                ret |= set_tuner_prop(tuner, AM_PROPERTY[svindex].prop, AM_PROPERTY[svindex].val);	
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(AM_PROPERTY)/sizeof(CUSTOM_PROPERTY))){
						svindex = 0xFF;
			        	*task_seq = TUNER_AM_CUSTOM_4;
					}
                }
            }
            break;

		case TUNER_AM_CUSTOM_4:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
				if (svindex == 0xFF){
					svindex = 0;
				}

				tuner.id = TUNER_ID_INVALID;
                ret |= set_tuner_prop(tuner, COMMON_PROPERTY[svindex].prop, COMMON_PROPERTY[svindex].val);	
                if (ret == SUCCESS){
                    svindex++;
					if (svindex >= (sizeof(COMMON_PROPERTY)/sizeof(CUSTOM_PROPERTY))){
						svindex = 0xFF;
			        	*task_seq = TUNER_TASK_SEQ_END;
					}
                }
            }
            break;
	#else
		case TUNER_AM_CUSTOM_1:
			*task_seq = TUNER_TASK_SEQ_END;
			break;
	#endif	/*FEAT_DIGEN_4_1_TUNING*/		
			
		default:
			break;
	}

	return ret;

}


#if (FEAT_CUSTOM_TUNING)		
RETURN_CODE TunerTaskFmCustomTuning(U8 *task_seq)
{
    volatile RETURN_CODE ret = SUCCESS;
    U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
    tuner_id tuner = { 0 };
    U16 metric_id = 0;
	

#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

    switch(*task_seq)
    {
        case TUNER_FM_CUSTOM_TUNING_1:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                if  ((FM_TUNINIG_SERVO_TRANSFORM[1].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[1].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[1].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[1].y2 == 0)){
                    metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
                }else{
                    metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_SNR;
                }
                ret |= SI479XX_TUNER_fm_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         1, 																		/*TRANSFORM_ID*/
                         metric_id,                                                                 /*FM_METRIC_ID*/
                         SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_HICUT,	    /*FM_SERVO_ID*/
                         FM_TUNINIG_SERVO_TRANSFORM[1].x1,											/*X1*/
                         FM_TUNINIG_SERVO_TRANSFORM[1].y1,											/*Y1*/
                         FM_TUNINIG_SERVO_TRANSFORM[1].x2,											/*X2*/
                         FM_TUNINIG_SERVO_TRANSFORM[1].y2,											/*Y2*/
                         FM_TUNINIG_SERVO_TRANSFORM[1].p1_p2,										/*P1TOP2RATE*/
                         FM_TUNINIG_SERVO_TRANSFORM[1].p2_p1,										/*P2TOP1RATE*/
                         0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    *task_seq = TUNER_FM_CUSTOM_TUNING_2;
                }
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_2:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_3;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_3:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[2].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[2].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[2].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[2].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     2, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_HICUT,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[2].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[2].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[2].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[2].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[2].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[2].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_4;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_4:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_5;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_5:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[3].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[3].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[3].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[3].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_SNR;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     3, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_HICUT,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[3].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[3].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[3].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[3].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[3].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[3].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_6;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_6:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_7;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_7:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[4].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[4].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[4].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[4].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     4, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_LOWCUT,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[4].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[4].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[4].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[4].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[4].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[4].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_8;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_8:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_9;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_9:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[5].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[5].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[5].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[5].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_SNR;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     5, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_STBLEND,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[5].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[5].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[5].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[5].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[5].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[5].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_10;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_10:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_11;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_11:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[6].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[6].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[6].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[6].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     6, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_STBLEND,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[6].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[6].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[6].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[6].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[6].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[6].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_12;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_12:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_13;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_13:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[7].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[7].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[7].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[7].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_PILOTLOCK;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     7, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_STBLEND,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[7].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[7].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[7].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[7].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[7].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[7].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_14;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_14:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_15;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_15:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[8].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[8].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[8].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[8].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_SNR;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     8, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_HIBLEND,	    /*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[8].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[8].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[8].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[8].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[8].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[8].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_16;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_16:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_17;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_17:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[9].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[9].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[9].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[9].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_SNR;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     9, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_NBUSN2THR,	/*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[9].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[9].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[9].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[9].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[9].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[9].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_18;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_18:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_19;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_19:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[10].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[10].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[10].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[10].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_NC_ADJ;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     10, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_NBUSN2THR,	/*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[10].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[10].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[10].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[10].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[10].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[10].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_20;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_20:
            set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_ADDR, 17);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_21;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_21:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[11].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[11].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[11].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[11].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_SNR;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     11, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_FADETHRESH,	/*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[11].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[11].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[11].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[11].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[11].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[11].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_22;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_22:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_23;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_23:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[12].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[12].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[12].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[12].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_NC_ADJ;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     12, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_CHANBW,		/*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[12].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[12].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[12].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[12].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[12].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[12].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_24;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_24:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_25;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_25:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[13].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[13].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[13].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[13].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_ASSI200;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     13, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_FADETHRESH,	/*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[13].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[13].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[13].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[13].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[13].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[13].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_26;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_26:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_27;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_27:
            if  ((FM_TUNINIG_SERVO_TRANSFORM[14].x1 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[14].y1 == 0) && \
                    (FM_TUNINIG_SERVO_TRANSFORM[14].x2 == 0) && (FM_TUNINIG_SERVO_TRANSFORM[14].y2 == 0)){
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_DISABLE;
            }else{
                metric_id = SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_METRIC_ID_ENUM_RSSI_ADJ;
            }
            ret |= SI479XX_TUNER_fm_set_servo_transform__command
                    (tuner.ic, tuner.id,
                     14, 																		/*TRANSFORM_ID*/
                     metric_id,                                                                 /*FM_METRIC_ID*/
                     SI479XX_TUNER_CMD_FM_SET_SERVO_TRANSFORM_ARG_FM_SERVO_ID_ENUM_SOFTMUTE,	/*FM_SERVO_ID*/
                     FM_TUNINIG_SERVO_TRANSFORM[14].x1,											/*X1*/
                     FM_TUNINIG_SERVO_TRANSFORM[14].y1,											/*Y1*/
                     FM_TUNINIG_SERVO_TRANSFORM[14].x2,											/*X2*/
                     FM_TUNINIG_SERVO_TRANSFORM[14].y2,											/*Y2*/
                     FM_TUNINIG_SERVO_TRANSFORM[14].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_SERVO_TRANSFORM[14].p2_p1,										/*P2TOP1RATE*/
                     0, 0, 0, 0, 0, 0, 0);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_28;
            }
            break;


        case TUNER_FM_CUSTOM_TUNING_28:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_29;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_29:
            set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_ADDR, FmValidRssi);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_30;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_30:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_31;
            }
            break;
        case TUNER_FM_CUSTOM_TUNING_31:
            set_tuner_prop(tuner, SI479XX_TUNER_PROP_FM_VALID_SNR_THRESHOLD_ADDR, FmValidSnr);
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_32;
            }
            break;
			
		case TUNER_FM_CUSTOM_TUNING_32:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_33;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_33:
            ret |= SI479XX_TUNER_fm_set_fast_servo_transform__command
                    (tuner.ic, tuner.id,
                     1, 																			/*FAST_TRANSFORM_ID*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].enable,	                                	/*ENABLE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].x1,											/*X1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].y1,											/*Y1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].x2,											/*X2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].y2,											/*Y2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[1].p2_p1);										/*P2TOP1RATE*/
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_34;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_34:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_35;
            }
            break;
			
		case TUNER_FM_CUSTOM_TUNING_35:
            ret |= SI479XX_TUNER_fm_set_fast_servo_transform__command
                    (tuner.ic, tuner.id,
                     2, 																			/*FAST_TRANSFORM_ID*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].enable,	                                	/*ENABLE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].x1,											/*X1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].y1,											/*Y1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].x2,											/*X2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].y2,											/*Y2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[2].p2_p1);										/*P2TOP1RATE*/
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_36;
            }
            break;
			
		case TUNER_FM_CUSTOM_TUNING_36:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_37;
            }
            break;
			
		case TUNER_FM_CUSTOM_TUNING_37:
            ret |= SI479XX_TUNER_fm_set_fast_servo_transform__command
                    (tuner.ic, tuner.id,
                     3, 																			/*FAST_TRANSFORM_ID*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].enable,	                                	/*ENABLE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].x1,											/*X1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].y1,											/*Y1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].x2,											/*X2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].y2,											/*Y2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[3].p2_p1);										/*P2TOP1RATE*/
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_38;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_38:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_39;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_39:
            ret |= SI479XX_TUNER_fm_set_fast_servo_transform__command
                    (tuner.ic, tuner.id,
                     4, 																			/*FAST_TRANSFORM_ID*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].enable,	                                	/*ENABLE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].x1,											/*X1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].y1,											/*Y1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].x2,											/*X2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].y2,											/*Y2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[4].p2_p1);										/*P2TOP1RATE*/
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_40;
            }
            break;
			
		case TUNER_FM_CUSTOM_TUNING_40:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_41;
            }
            break;

		case TUNER_FM_CUSTOM_TUNING_41:
            ret |= SI479XX_TUNER_fm_set_fast_servo_transform__command
                    (tuner.ic, tuner.id,
                     5, 																			/*FAST_TRANSFORM_ID*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].enable,	                                	/*ENABLE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].x1,											/*X1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].y1,											/*Y1*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].x2,											/*X2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].y2,											/*Y2*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].p1_p2,										/*P1TOP2RATE*/
                     FM_TUNINIG_FAST_SERVO_TRANSFORM[5].p2_p1);										/*P2TOP1RATE*/
            if (ret == SUCCESS){
                *task_seq = TUNER_FM_CUSTOM_TUNING_42;
            }
            break;

        case TUNER_FM_CUSTOM_TUNING_42:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                ucBand0 = ucBand;
                *task_seq = TUNER_TASK_SEQ_END;
            }
            break;

		#if 0
		case TUNER_FM_CUSTOM_TUNING_43:
			{
				U8 i = 0;
				SI479XX_TUNER_fm_get_servo_transform__data replyData[14] = { 0 };

				for(i = 0;i < 15;i++){
					ret |= SI479XX_TUNER_fm_get_servo_transform__command(tuner.ic, tuner.id, i + 1);
					DelayMs(10);
					ret |= SI479XX_TUNER_fm_get_servo_transform__reply(tuner.ic, tuner.id, &replyData[i]);
					DelayMs(10);
				}

				if (ret == SUCCESS){
                	*task_seq = TUNER_FM_CUSTOM_TUNING_44;
           		}
			}
			break;

		case TUNER_FM_CUSTOM_TUNING_44:
			{
				U8 i = 0;
				SI479XX_TUNER_fm_get_fast_servo_transform__data replyData[5] = { 0 };

				for(i = 0;i < 5;i++){
					ret |= SI479XX_TUNER_fm_get_fast_servo_transform__command(tuner.ic, tuner.id, i + 1);
					DelayMs(10);
					ret |= SI479XX_TUNER_fm_get_fast_servo_transform__reply(tuner.ic, tuner.id, &replyData[i]);
					DelayMs(10);
				}

				if (ret == SUCCESS){
                	*task_seq = TUNER_FM_CUSTOM_TUNING_42;
           		}
			}
			break;
		#endif	/*0*/
		
        default:
            break;
    }

    return ret;

}


RETURN_CODE TunerTaskAmCustomTuning(U8 *task_seq)
{
    volatile RETURN_CODE ret = SUCCESS;
    U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
    tuner_id tuner = { 0 };
    U16 metric_id = 0;


#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/


    switch(*task_seq)
    {
        case TUNER_AM_CUSTOM_TUNING_1:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                if  ((AM_TUNINIG_SERVO_TRANSFORM[1].x1 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[1].y1 == 0) && \
                    (AM_TUNINIG_SERVO_TRANSFORM[1].x2 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[1].y2 == 0)){
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_DISABLE;
                }else{
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_SNR;
                }
                ret |= SI479XX_TUNER_am_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         1, 																		/*TRANSFORM_ID*/
                         metric_id,                                                                 /*AM_METRIC_ID*/
                         SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_ID_ENUM_HICUT,	    /*AM_SERVO_ID*/
                         AM_TUNINIG_SERVO_TRANSFORM[1].x1,											/*X1*/
                         AM_TUNINIG_SERVO_TRANSFORM[1].y1,											/*Y1*/
                         AM_TUNINIG_SERVO_TRANSFORM[1].x2,											/*X2*/
                         AM_TUNINIG_SERVO_TRANSFORM[1].y2,											/*Y2*/
                         AM_TUNINIG_SERVO_TRANSFORM[1].p1_p2,										/*P1TOP2RATE*/
                         AM_TUNINIG_SERVO_TRANSFORM[1].p2_p1,										/*P2TOP1RATE*/
                     	 0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    *task_seq = TUNER_AM_CUSTOM_TUNING_2;
                }
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_2:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_AM_CUSTOM_TUNING_3;
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_3:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                if  ((AM_TUNINIG_SERVO_TRANSFORM[2].x1 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[2].y1 == 0) && \
                    (AM_TUNINIG_SERVO_TRANSFORM[2].x2 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[2].y2 == 0)){
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_DISABLE;
                }else{
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_RSSI_ADJ;
                }
                ret |= SI479XX_TUNER_am_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         2, 																		/*TRANSFORM_ID*/
                         metric_id,                                                                 /*AM_METRIC_ID*/
                         SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_ID_ENUM_NBIQ2TH,	    /*AM_SERVO_ID*/
                         AM_TUNINIG_SERVO_TRANSFORM[2].x1,											/*X1*/
                         AM_TUNINIG_SERVO_TRANSFORM[2].y1,											/*Y1*/
                         AM_TUNINIG_SERVO_TRANSFORM[2].x2,											/*X2*/
                         AM_TUNINIG_SERVO_TRANSFORM[2].y2,											/*Y2*/
                         AM_TUNINIG_SERVO_TRANSFORM[2].p1_p2,										/*P1TOP2RATE*/
                         AM_TUNINIG_SERVO_TRANSFORM[2].p2_p1,										/*P2TOP1RATE*/
                         0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    *task_seq = TUNER_AM_CUSTOM_TUNING_4;
                }
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_4:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_AM_CUSTOM_TUNING_5;
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_5:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                if  ((AM_TUNINIG_SERVO_TRANSFORM[3].x1 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[3].y1 == 0) && \
                    (AM_TUNINIG_SERVO_TRANSFORM[3].x2 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[3].y2 == 0)){
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_DISABLE;
                }else{
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_RSSI_ADJ;
                }
                ret |= SI479XX_TUNER_am_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         3, 																		/*TRANSFORM_ID*/
                         metric_id,                                                                 /*AM_METRIC_ID*/
                         SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_ID_ENUM_SOFTMUTE,	/*AM_SERVO_ID*/
                         AM_TUNINIG_SERVO_TRANSFORM[3].x1,											/*X1*/
                         AM_TUNINIG_SERVO_TRANSFORM[3].y1,											/*Y1*/
                         AM_TUNINIG_SERVO_TRANSFORM[3].x2,											/*X2*/
                         AM_TUNINIG_SERVO_TRANSFORM[3].y2,											/*Y2*/
                         AM_TUNINIG_SERVO_TRANSFORM[3].p1_p2,										/*P1TOP2RATE*/
                         AM_TUNINIG_SERVO_TRANSFORM[3].p2_p1,										/*P2TOP1RATE*/
                         0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    *task_seq = TUNER_AM_CUSTOM_TUNING_6;
                }
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_6:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_AM_CUSTOM_TUNING_7;
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_7:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                if  ((AM_TUNINIG_SERVO_TRANSFORM[4].x1 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[4].y1 == 0) && \
                    (AM_TUNINIG_SERVO_TRANSFORM[4].x2 == 0) && (AM_TUNINIG_SERVO_TRANSFORM[4].y2 == 0)){
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_DISABLE;
                }else{
                    metric_id = SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_METRIC_ID_ENUM_RSSI_ADJ;
                }
                ret |= SI479XX_TUNER_am_set_servo_transform__command
                        (tuner.ic, tuner.id,
                         4, 																		/*TRANSFORM_ID*/
                         metric_id,                                                                 /*AM_METRIC_ID*/
                         SI479XX_TUNER_CMD_AM_SET_SERVO_TRANSFORM_ARG_AM_SERVO_ID_ENUM_LOWCUT,		/*AM_SERVO_ID*/
                         AM_TUNINIG_SERVO_TRANSFORM[4].x1,											/*X1*/
                         AM_TUNINIG_SERVO_TRANSFORM[4].y1,											/*Y1*/
                         AM_TUNINIG_SERVO_TRANSFORM[4].x2,											/*X2*/
                         AM_TUNINIG_SERVO_TRANSFORM[4].y2,											/*Y2*/
                         AM_TUNINIG_SERVO_TRANSFORM[4].p1_p2,										/*P1TOP2RATE*/
                         AM_TUNINIG_SERVO_TRANSFORM[4].p2_p1,										/*P2TOP1RATE*/
                         0, 0, 0, 0, 0, 0, 0);
                if (ret == SUCCESS){
                    *task_seq = TUNER_AM_CUSTOM_TUNING_8;
                }
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_8:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_AM_CUSTOM_TUNING_9;
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_9:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                set_tuner_prop(tuner, SI479XX_TUNER_PROP_AM_VALID_RSSI_THRESHOLD_ADDR, AmValidRssi);		/* -128 ~ 127 dBuV */ 
                if (ret == SUCCESS){
                    *task_seq = TUNER_AM_CUSTOM_TUNING_10;
                }
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_10:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_AM_CUSTOM_TUNING_11;
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_11:
            set_tuner_prop(tuner, SI479XX_TUNER_PROP_AM_VALID_SNR_THRESHOLD_ADDR, AmValidSnr);				/* -128 ~ 127 dB */
            if (ret == SUCCESS){
                *task_seq = TUNER_AM_CUSTOM_TUNING_12;
            }
            break;

        case TUNER_AM_CUSTOM_TUNING_12:
            ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);	
            if (ret == SUCCESS){
                *task_seq = TUNER_TASK_SEQ_END;
            }
            break;

		#if 0
		case TUNER_AM_CUSTOM_TUNING_13:
			{
				U8 i = 0;
				SI479XX_TUNER_am_get_servo_transform__data replyData[4] = { 0 };

				for(i = 0;i < 5;i++){
					ret |= SI479XX_TUNER_am_get_servo_transform__command(tuner.ic, tuner.id, i + 1);
					DelayMs(10);
					ret |= SI479XX_TUNER_am_get_servo_transform__reply(tuner.ic, tuner.id, &replyData[i]);
					DelayMs(10);
				}

				if (ret == SUCCESS){
                	*task_seq = TUNER_AM_CUSTOM_TUNING_12;
           		}
			}
			break;
		#endif	/*0*/

        default:
            break;
    }

    return ret;

}
#endif		/*FEAT_CUSTOM_TUNING*/


RETURN_CODE TunerTaskFmTune(U8 *task_seq, U16 freq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };
					
				
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	if ((freq < lSFreq) || (freq > lEFreq)) 
	{
		stTuner.prc =TUNER_PRC_NONE;
		stTuner.task =TUNER_TASK_NONE;
		return ret;
	}
		
	switch(*task_seq)
	{
		case TUNER_FM_TUNE_1:			
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				uint32_t orig_cts = getCtsTimeout();
			
				RSSI = 0;
				SNR = 0;

				ret |= SI479XX_TUNER_fm_tune_freq(tuner.ic, tuner.id,
												SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
												SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
												SI479XX_TUNER_CMD_FM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
												freq);
				if (ret == SUCCESS){
					STCINT = 0;
					TmrTunerTaskSeq = 5;
					EscCnt = 0;
					*task_seq = TUNER_FM_TUNE_2;
				}
			}
			break;

		
		case TUNER_FM_TUNE_2:
			if (TmrTunerTaskSeq == 0)
			{
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
				if (ret == SUCCESS){
					SI479XX_read_status__data rply = { 0 };							/* 210516 SGsoft */	

					ret |= SI479XX_read_status(IC0, &rply);
					STCINT = rply.STCINT;
					if (STCINT){
						*task_seq = TUNER_FM_TUNE_3;
					}else{
						TmrTunerTaskSeq = 5;
						EscCnt++;
					}
				}
			}
			break;

		case TUNER_FM_TUNE_3:
			ret |= SI479XX_TUNER_fm_rsq_status__command(tuner.ic, tuner.id,
														FALSE, 
														FALSE, 
														FALSE);
			if (ret == SUCCESS){
				*task_seq = TUNER_FM_TUNE_4;
			}
			break;

		case TUNER_FM_TUNE_4:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				volatile SI479XX_TUNER_fm_rsq_status__data primary = { 0 };
					
				ret |= SI479XX_TUNER_fm_rsq_status__reply(tuner.ic, tuner.id, &primary);
				if (ret == SUCCESS){
					RSSI = primary.RSSI;																
					SNR = primary.SNR;
					STBLEND = (primary.PDEV > 30)? 1: 0;	
					CHVALID = primary.VALID? 1 : 0; 									
			
					*task_seq = TUNER_TASK_SEQ_END;
				}
			}
			break;

		default:
			break;
	}

	return ret;

}


RETURN_CODE TunerTaskAmTune(U8 *task_seq, U16 freq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };
						
					
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;							
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/

	if ((freq < lSFreq) || (freq > lEFreq)) 
	{
		stTuner.prc =TUNER_PRC_NONE;
		stTuner.task =TUNER_TASK_NONE;
		
		return ret;
	}

//	DPRINTF(DBG_INFO," %s %d %d %d\n\r",__FUNCTION__,freq,lSFreq,lEFreq);
	
	switch(*task_seq)
	{
		case TUNER_AM_TUNE_1:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				RSSI = 0;
				SNR = 0;
			
				ret |= SI479XX_TUNER_am_tune_freq(tuner.ic, tuner.id,
												0,
	        									SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_TUNEMODE_ENUM_NORMAL,
	        									SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_SERVO_ENUM_ADAPT,
	        									SI479XX_TUNER_CMD_AM_TUNE_FREQ_ARG_INJECTION_ENUM_AUTO,
	        									freq);
				if (ret == SUCCESS){
					STCINT = 0;
					TmrTunerTaskSeq = 5;
					EscCnt = 0;
					*task_seq = TUNER_AM_TUNE_2;
				}
			}
			break;
		
		case TUNER_AM_TUNE_2:
			if (TmrTunerTaskSeq == 0)
			{
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
				if (ret == SUCCESS){
					SI479XX_read_status__data rply = { 0 };								

					ret |= SI479XX_read_status(IC0, &rply);
					STCINT = rply.STCINT;
					if (STCINT){
						*task_seq = TUNER_AM_TUNE_3;
					}else{
						TmrTunerTaskSeq = 5;
						EscCnt++;
					}
				}
			}
			break;
		
		case TUNER_AM_TUNE_3:
			ret |= SI479XX_TUNER_am_rsq_status__command(tuner.ic, tuner.id,
														FALSE, 
														FALSE, 
														FALSE);
			if (ret == SUCCESS){
				*task_seq = TUNER_AM_TUNE_4;
			}
			break;
		
		case TUNER_AM_TUNE_4:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				volatile SI479XX_TUNER_am_rsq_status__data primary;
					
				ret |= SI479XX_TUNER_am_rsq_status__reply(tuner.ic, tuner.id, &primary);
				if (ret == SUCCESS){
					RSSI = primary.RSSI;																
					SNR = primary.SNR;
					CHVALID = primary.VALID? 1 : 0; 									
			
					*task_seq = TUNER_TASK_SEQ_END;
				}
			}
			break;
		
		default:
			break;
	}

	return ret;

}


RETURN_CODE TunerTaskCheckRsq(U8 *task_seq)											
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	tuner_id tuner = { 0 };

	
#ifdef OPTION_IMAGETYPE_DAB		
	tuner.ic = g_dab_parts.fm_am.tuner.ic;								
	tuner.id = g_dab_parts.fm_am.tuner.id;
#else
	tuner.ic = g_hd_parts.primary.tuner.ic;
	tuner.id = g_hd_parts.primary.tuner.id;
#endif	/*OPTION_IMAGETYPE_DAB*/


	if (ucBand <= B_FM_MAX) 											
	{
		/*FM*/
		switch(*task_seq)
		{
			case TUNER_CHECK_RSQ_1:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status); 		
				if (ret == SUCCESS)
				{
					ret |= SI479XX_TUNER_fm_rsq_status__command(tuner.ic, tuner.id, FALSE, FALSE, FALSE);
					if (ret == SUCCESS){
						*task_seq = TUNER_CHECK_RSQ_2;
					}
				}
				break;
	
			case TUNER_CHECK_RSQ_2:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);		
				if (ret == SUCCESS)
				{
					SI479XX_TUNER_fm_rsq_status__data primary;
					
					ret |= SI479XX_TUNER_fm_rsq_status__reply(tuner.ic, tuner.id, &primary);
					if (ret == SUCCESS){
						STBLEND = (primary.PDEV > 30)? 1: 0;
						CHVALID = primary.VALID? 1 : 0; 

						RSSI = primary.RSSI_ADJ;									/* -128 ~ 127 (dBuV) */
						SNR = primary.SNR;											/* -128 ~ 60 (dB) */
						MULT = primary.MULTIPATH;									/* 0 ~ 100 (%) */		
						DEV = primary.DEVIATION;									/* 0 ~ 255 (kHz) */
						PDEV = primary.PDEV;										/* 0 ~ 127 (100Hz) */
						USN =  primary.USN;											/* 0 ~ 127 (-dBFS) */
						LASSI = primary.LASSI;										/* -128 ~ 127 (dBc) */	
						HASSI = primary.HASSI;										/* -128 ~ 127 (dBc) */	
						FREQOFF = primary.FREQOFF;									/* -128 ~ 127 (kHz) */	
						COCHANL = primary.COCHANL;									/* 0 ~ 127 (-dBFS) */ 		

						*task_seq = TUNER_CHECK_RSQ_3;		
					}
				}
				break;

			case TUNER_CHECK_RSQ_3:													
				ret |= SI479XX_TUNER_fm_servo_status__command(tuner.ic, tuner.id);		
				if (ret == SUCCESS)
				{
					*task_seq = TUNER_CHECK_RSQ_4;
				}
				break;

			case TUNER_CHECK_RSQ_4:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);		
				if (ret == SUCCESS)
				{
					SI479XX_TUNER_fm_servo_status__data servo;
						
					ret |= SI479XX_TUNER_fm_servo_status__reply(tuner.ic, tuner.id, &servo);
					if (ret == SUCCESS){
						CHANBW = servo.CHANBW;									/* 5 ~ 150 (kHz) */
						SOFTMUTE = servo.SOFTMUTE;								/* 0 ~ 100 (dB) */
						HICUT = servo.HICUT;									/* 2 ~ 220 (100Hz) */
						LOWCUT = servo.LOWCUT;									/* 1 ~ 100 (10Hz) */
						FM_STBLEND = servo.STBLEND;								/* 0 ~ 50 (dB) */					
						FM_HIBLEND = servo.HIBLEND;								/* 0 ~ 210 (100Hz) */
						FM_NBUSN1THR = servo.NBUSN1THR;							/* 0 ~ 100 (dB) */
						FM_NBUSN2THR =  servo.NBUSN2THR;						/* 0 ~ 100 (dB) */
						FM_FADETHRESH = servo.FADETHRESH;						/* 0 ~ 100 % */
	
						*task_seq = TUNER_TASK_SEQ_END;		
					}
				}
				break;
	
			default:
				break;
		}
	}
	else
	{
		/*AM*/
		switch(*task_seq)
		{
			case TUNER_CHECK_RSQ_1:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);		
				if (ret == SUCCESS)
				{
					ret |= SI479XX_TUNER_am_rsq_status__command(tuner.ic, tuner.id, FALSE, FALSE, FALSE);
					if (ret == SUCCESS){
						*task_seq = TUNER_CHECK_RSQ_2;
					}
				}
				break;
	
			case TUNER_CHECK_RSQ_2:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);		
				if (ret == SUCCESS)
				{
					SI479XX_TUNER_am_rsq_status__data primary;
					
					ret |= SI479XX_TUNER_am_rsq_status__reply(tuner.ic, tuner.id, &primary);
					if (ret == SUCCESS){
						CHVALID = primary.VALID? 1 : 0; 

						RSSI = primary.RSSI_ADJ;									/* -128 ~ 127 (dBuV) */
						SNR = primary.SNR;											/* -128 ~ 60 (dB) */
						LASSI = primary.LASSI;										/* -128 ~ 127 (dBc) */	
						HASSI = primary.HASSI;										/* -128 ~ 127 (dBc) */	
						FREQOFF = primary.FREQOFF;									/* -128 ~ 127 (25Hz) */			

						*task_seq = TUNER_CHECK_RSQ_3;		
					}
				}
				break;

			case TUNER_CHECK_RSQ_3:													
				ret |= SI479XX_TUNER_am_servo_status__command(tuner.ic, tuner.id);		
				if (ret == SUCCESS)
				{
					*task_seq = TUNER_CHECK_RSQ_4;
				}
				break;

			case TUNER_CHECK_RSQ_4:
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status); 		
				if (ret == SUCCESS)
				{
					SI479XX_TUNER_am_servo_status__data servo;
						
					ret |= SI479XX_TUNER_am_servo_status__reply(tuner.ic, tuner.id, &servo);
					if (ret == SUCCESS){
						CHANBW = servo.CHANBW;										/* 10 ~ 120 (100Hz) */
						SOFTMUTE = servo.SOFTMUTE;									/* 0 ~ 100 (dB) */
						HICUT = servo.HICUT;										/* 2 ~ 220 (100Hz) */
						LOWCUT = servo.LOWCUT;										/* 1 ~ 100 (10Hz) */
						AM_NBAUDTH = servo.NBAUDTH;									/* 0 ~ 100 (dB) */
						AM_NBIQ2TH = servo.NBIQ2TH;									/* 0 ~ 100 (dB) */
						AM_NBIQ3TH = servo.NBIQ3TH;									/* 0 ~ 100 (dB) */
						AM_MAXAVC = servo.MAXAVC;									/* -100 ~ 100 (dB) */
	
						*task_seq = TUNER_TASK_SEQ_END;		
					}
				}
				break;
	
			default:
				break;
		}
	}

	return ret;

}


#if (FEAT_FLASH_UTIL)		/* 210428 SGsoft */
const uint32_t g_crc32_table[256] = {
        0x00000000,0x04C11DB7,0x09823B6E,0x0D4326D9,0x130476DC,0x17C56B6B,0x1A864DB2,0x1E475005,
        0x2608EDB8,0x22C9F00F,0x2F8AD6D6,0x2B4BCB61,0x350C9B64,0x31CD86D3,0x3C8EA00A,0x384FBDBD,
        0x4C11DB70,0x48D0C6C7,0x4593E01E,0x4152FDA9,0x5F15ADAC,0x5BD4B01B,0x569796C2,0x52568B75,
        0x6A1936C8,0x6ED82B7F,0x639B0DA6,0x675A1011,0x791D4014,0x7DDC5DA3,0x709F7B7A,0x745E66CD,
        0x9823B6E0,0x9CE2AB57,0x91A18D8E,0x95609039,0x8B27C03C,0x8FE6DD8B,0x82A5FB52,0x8664E6E5,
        0xBE2B5B58,0xBAEA46EF,0xB7A96036,0xB3687D81,0xAD2F2D84,0xA9EE3033,0xA4AD16EA,0xA06C0B5D,
        0xD4326D90,0xD0F37027,0xDDB056FE,0xD9714B49,0xC7361B4C,0xC3F706FB,0xCEB42022,0xCA753D95,
        0xF23A8028,0xF6FB9D9F,0xFBB8BB46,0xFF79A6F1,0xE13EF6F4,0xE5FFEB43,0xE8BCCD9A,0xEC7DD02D,
        0x34867077,0x30476DC0,0x3D044B19,0x39C556AE,0x278206AB,0x23431B1C,0x2E003DC5,0x2AC12072,
        0x128E9DCF,0x164F8078,0x1B0CA6A1,0x1FCDBB16,0x018AEB13,0x054BF6A4,0x0808D07D,0x0CC9CDCA,
        0x7897AB07,0x7C56B6B0,0x71159069,0x75D48DDE,0x6B93DDDB,0x6F52C06C,0x6211E6B5,0x66D0FB02,
        0x5E9F46BF,0x5A5E5B08,0x571D7DD1,0x53DC6066,0x4D9B3063,0x495A2DD4,0x44190B0D,0x40D816BA,
        0xACA5C697,0xA864DB20,0xA527FDF9,0xA1E6E04E,0xBFA1B04B,0xBB60ADFC,0xB6238B25,0xB2E29692,
        0x8AAD2B2F,0x8E6C3698,0x832F1041,0x87EE0DF6,0x99A95DF3,0x9D684044,0x902B669D,0x94EA7B2A,
        0xE0B41DE7,0xE4750050,0xE9362689,0xEDF73B3E,0xF3B06B3B,0xF771768C,0xFA325055,0xFEF34DE2,
        0xC6BCF05F,0xC27DEDE8,0xCF3ECB31,0xCBFFD686,0xD5B88683,0xD1799B34,0xDC3ABDED,0xD8FBA05A,
        0x690CE0EE,0x6DCDFD59,0x608EDB80,0x644FC637,0x7A089632,0x7EC98B85,0x738AAD5C,0x774BB0EB,
        0x4F040D56,0x4BC510E1,0x46863638,0x42472B8F,0x5C007B8A,0x58C1663D,0x558240E4,0x51435D53,
        0x251D3B9E,0x21DC2629,0x2C9F00F0,0x285E1D47,0x36194D42,0x32D850F5,0x3F9B762C,0x3B5A6B9B,
        0x0315D626,0x07D4CB91,0x0A97ED48,0x0E56F0FF,0x1011A0FA,0x14D0BD4D,0x19939B94,0x1D528623,
        0xF12F560E,0xF5EE4BB9,0xF8AD6D60,0xFC6C70D7,0xE22B20D2,0xE6EA3D65,0xEBA91BBC,0xEF68060B,
        0xD727BBB6,0xD3E6A601,0xDEA580D8,0xDA649D6F,0xC423CD6A,0xC0E2D0DD,0xCDA1F604,0xC960EBB3,
        0xBD3E8D7E,0xB9FF90C9,0xB4BCB610,0xB07DABA7,0xAE3AFBA2,0xAAFBE615,0xA7B8C0CC,0xA379DD7B,
        0x9B3660C6,0x9FF77D71,0x92B45BA8,0x9675461F,0x8832161A,0x8CF30BAD,0x81B02D74,0x857130C3,
        0x5D8A9099,0x594B8D2E,0x5408ABF7,0x50C9B640,0x4E8EE645,0x4A4FFBF2,0x470CDD2B,0x43CDC09C,
        0x7B827D21,0x7F436096,0x7200464F,0x76C15BF8,0x68860BFD,0x6C47164A,0x61043093,0x65C52D24,
        0x119B4BE9,0x155A565E,0x18197087,0x1CD86D30,0x029F3D35,0x065E2082,0x0B1D065B,0x0FDC1BEC,
        0x3793A651,0x3352BBE6,0x3E119D3F,0x3AD08088,0x2497D08D,0x2056CD3A,0x2D15EBE3,0x29D4F654,
        0xC5A92679,0xC1683BCE,0xCC2B1D17,0xC8EA00A0,0xD6AD50A5,0xD26C4D12,0xDF2F6BCB,0xDBEE767C,
        0xE3A1CBC1,0xE760D676,0xEA23F0AF,0xEEE2ED18,0xF0A5BD1D,0xF464A0AA,0xF9278673,0xFDE69BC4,
        0x89B8FD09,0x8D79E0BE,0x803AC667,0x84FBDBD0,0x9ABC8BD5,0x9E7D9662,0x933EB0BB,0x97FFAD0C,
        0xAFB010B1,0xAB710D06,0xA6322BDF,0xA2F33668,0xBCB4666D,0xB8757BDA,0xB5365D03,0xB1F740B4,
};

//**************************************************************************************************
//                                      crc32_update()
//  Description:
//      Does a CRC32 on a block of data.  The CRC32 state is stored in *p_crc32.
//      To initialize,  set *p_crc32 = CRC32_INITIAL_VALUE.
//  Inputs:
//      *p_crc32 - Initial crc32 value
//      data - Points to a block of data to update the CRC32 on
//      len - Length of the block of data in bytes
//  Updates:
//      *p_crc32 - Updated CRC32 value

void crc32_update(uint32_t *p_crc32, const void* data, uint32_t len)
{
	uint32_t crc32 = *p_crc32;
    unsigned i = 0;

	for (i = 0; i < len; i++) {
		uint8_t idx = ((const uint8_t*)data)[i] ^ (uint8_t)((crc32>>24) & 0x000000FF);
		crc32 = (crc32<<8) ^ g_crc32_table[idx];
	}
	*p_crc32 = crc32;
}


void TunerPrcFlashUtilStart(void)
{
	//SectorLastAddr = SERIAL_FLASH_START_ADDR + si4790x_firm_size - si4790x_firm_size % ONE_SECTOR_SIZE;
	SectorCurAddr = SERIAL_FLASH_START_ADDR;
	restsize = 256;
	CurFirmIndex = 0;
					
	stTuner.prc = TUNER_PRC_FLASH_UTIL;
	stTuner.task = TUNER_TASK_FLASH_UTIL;
	stTuner.seq = TUNER_FW_LOAD_RESET_L;

	DPRINTF(DBG_INFO,"SiLab 4.1 Version Size : %d Byte \n\r",restsize);	
}

void TunerSerialFlashUpdate(void)			//YOON_220414
{
	stTuner.prc = TUNER_PRC_FLASH_UTIL;
	stTuner.task = TUNER_TASK_PRC_FLASH_UTIL_START;
	EscCnt =0;
	DPRINTF(DBG_INFO,"\n\rSiLab 4.1 TunerSerialFlashUpdate Start!!\n\r");
}

RETURN_CODE TunerPrcFlashUtilSeq(void)											
{
	volatile RETURN_CODE ret = SUCCESS;


	if (stTuner.task == TUNER_TASK_PRC_FLASH_UTIL_START)									
	{
		TunerPrcFlashUtilStart();
	}
	else if (stTuner.task == TUNER_TASK_FLASH_UTIL)
	{
		ret = TunerTaskFlashUtil(&stTuner.seq);
	
		if (stTuner.seq == TUNER_TASK_SEQ_END)
		{
			stTuner.task = TUNER_TASK_NONE;
			stTuner.prc = TUNER_PRC_NONE;
			DPRINTF(DBG_INFO,"SiLab 4.1 Unable to update due to tuner version mismatch \n\r\n");
		}
	}
	
	return ret;

}


extern uint8_t* read_block_buffer;
RETURN_CODE TunerTaskFlashUtil(U8 *task_seq)
{
	volatile RETURN_CODE ret = SUCCESS;
	U8 status[SI479XX_CMD_READ_STATUS_REP_LENGTH] = { 0 };
	SI479XX_get_part_info__data pi = { 0 };
	SI479XX_get_func_info__data fi = { 0 };
	
	if (EscCnt >= 50){														
		*task_seq = TUNER_TASK_SEQ_END;
		return HAL_ERROR;
	}

//	DPRINTF(DBG_INFO,"*task_seq : %d \n\r",*task_seq);	
	switch(*task_seq)
	{
		case TUNER_FW_LOAD_RESET_L:
		#if defined(OPTION__BUS_I2C)		
			I2C_Init(TUN_I2C);
		#endif	/*OPTION__BUS_I2C*/
			pTRST(0);
			TmrTunerTaskSeq = 2;
			*task_seq = TUNER_FW_LOAD_RESET_H;
			break;

		case TUNER_FW_LOAD_RESET_H:
			if (TmrTunerTaskSeq == 0){
				pTRST(1);
				TmrTunerTaskSeq = 3;

				*task_seq = TUNER_FW_LOAD_POWER_UP_C;
			}
			break;

		case TUNER_FW_LOAD_POWER_UP_C:
			if (TmrTunerTaskSeq == 0){
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
				if (ret == SUCCESS){
					struct si479xx_powerup_args pup_args = get_default_pup_args_4790(IC0, OPMODE_INVALID);

					ret |= SI479XX_power_up__command(IC0, 
													pup_args.ctsien, pup_args.clko_current, pup_args.vio, pup_args.clkout,  
													pup_args.clkmode, pup_args.trsize, pup_args.ctun, pup_args.xtal_freq,
													pup_args.ibias, pup_args.afs, pup_args.chipid, pup_args.eziq_master, pup_args.eziq_enable);
					if (ret == SUCCESS){
						*task_seq = TUNER_FW_LOAD_POWER_UP_R;
					}
				}
			}
			break;

		case TUNER_FW_LOAD_POWER_UP_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				SI479XX_power_up__data pup_reply = { 0 };
				U8 uncorrectable = 0, correctable = 0;
				
 				ret |= SI479XX_power_up__reply(IC0, &pup_reply);
				if (ret == SUCCESS){
					uncorrectable = pup_reply.UNCORRECTABLE;
					correctable = pup_reply.CORRECTABLE;
				
					TmrTunerTaskSeq = 1;												
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_CHECK_BOOTLOADER_R;
				}
			}
			break;
			
		case TUNER_FW_LOAD_CHECK_BOOTLOADER_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				SI479XX_read_status__data rply = { 0 };								

				ret |= SI479XX_read_status(IC0, &rply);
				if (rply.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTLOADER) {
       				*task_seq = TUNER_FW_LOAD_PART_INFO_C;
				}else{
					TmrTunerTaskSeq = 1;											
					EscCnt++;
    			}
			}
			break;

		case TUNER_FW_LOAD_PART_INFO_C:
			ret |= SI479XX_get_part_info__command(IC0);
			if (ret == SUCCESS){
				*task_seq = TUNER_FW_LOAD_PART_INFO_R;
			}
			break;

		case TUNER_FW_LOAD_PART_INFO_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_get_part_info__reply(IC0, &pi);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_LOAD_INIT_C;
				}
			}
			break;

		case TUNER_FW_LOAD_LOAD_INIT_C:
			{
				uint32_t guid_table = 0;

				guid_table = 0x123456;										/* flash_utility.boot */	
			
				ret |= SI479XX_load_init__command(IC0, 1, &guid_table);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_FLASH_LOAD_LOAD_C;
				}
			}
			break;

		case TUNER_FW_LOAD_FLASH_LOAD_LOAD_C:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_FLASH_load_img(IC0, FLASH_UTIL_IMAGE_START_ADD, FLASH_UTIL_IMAGE_SIZE);
				if (ret == SUCCESS){
					TmrTunerTaskSeq = 5;
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_WAIT_CTS;
				}
			}
			break;
			
		case TUNER_FW_LOAD_WAIT_CTS:
			if (TmrTunerTaskSeq == 0){
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_LOAD_CHECK_BOOTREADY_R;
				}else{
					TmrTunerTaskSeq = 5;
				}
			}
			break;
			
		case TUNER_FW_LOAD_CHECK_BOOTREADY_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				SI479XX_read_status__data rply = { 0 };							

				ret |= SI479XX_read_status(IC0, &rply);
				if (rply.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_BOOTREADY){
       				*task_seq = TUNER_FW_LOAD_BOOT_C;
				}else{
					TmrTunerTaskSeq = 5;
					EscCnt++;
    			}
			}
			break;
			
		case TUNER_FW_LOAD_BOOT_C:
			{
				uint32_t orig_cts = getCtsTimeout();
				
    			setCtsTimeout(2000);
				
				ret |= SI4796X_boot__command(IC0);

				if (ret == SUCCESS){
					TmrTunerTaskSeq = 5;
					EscCnt = 0;
					*task_seq = TUNER_FW_LOAD_CHECK_APPLICATION_R;
				}
				
				setCtsTimeout(orig_cts);
			}
			break;
			
		case TUNER_FW_LOAD_CHECK_APPLICATION_R:
			if(TmrTunerTaskSeq == 0)
			{
				ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
				if (ret == SUCCESS){
					SI479XX_read_status__data rply = { 0 };							

					ret |= SI479XX_read_status(IC0, &rply);
					if (rply.PUP_STATE == SI479XX_CMD_READ_STATUS_REP_PUP_STATE_ENUM_APPLICATION){
	       				*task_seq = TUNER_FW_LOAD_FUNC_INFO_C;
					}else{
						TmrTunerTaskSeq = 5;
						EscCnt++;
	    			}
				}
			}
			break;

		case TUNER_FW_LOAD_FUNC_INFO_C:
			ret |= SI479XX_get_func_info__command(IC0);
			if (ret == SUCCESS){
				*task_seq = TUNER_FW_LOAD_FUNC_INFO_R;
			}
			break;

		case TUNER_FW_LOAD_FUNC_INFO_R:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_get_func_info__reply(IC0, &fi);
				DPRINTF(DBG_INFO,"Current version : %x.%x%x%x%x\r\n",fi.VERINFO0,fi.VERINFO1,fi.VERINFO2,fi.VERINFO3,fi.VERINFO4);
				if (ret == SUCCESS){
					*task_seq = TUNER_FW_FLASH_UPDATE_1;
					///while(1);
				}
			}
			break;

		case TUNER_FW_FLASH_UPDATE_1:
			if (SectorCurAddr <= SectorLastAddr){
				ret |= SI479XX_FLASH_UTIL_erase_sector__command(IC0, SectorCurAddr);
				DPRINTF(DBG_INFO,"Write SectorCurAddr : %x \r\n", SectorCurAddr);
				BlockIndex = 0;
				*task_seq = TUNER_FW_FLASH_UPDATE_2;
			}else{
				*task_seq = TUNER_TASK_SEQ_END;
			}
			break;

		case TUNER_FW_FLASH_UPDATE_2:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				if (BlockIndex < ONE_SECTOR_BLOCK_TOTAL)
				{
					if (restsize == 0){
						*task_seq = TUNER_TASK_SEQ_END;
						stTuner.task = TUNER_TASK_NONE;
						stTuner.prc = TUNER_PRC_NONE;

						DPRINTF(DBG_INFO,"SiLab Tuner Update Finished!!_UP2 : %x : %x \r\n",BlockIndex,restsize);
						
						/* System Reset after Tuner Serial_flash Update !!*/
						RecordReset(10,NULL);
						delay_ms(100);						//YOON_220414
						SystemSoftwareReset();				//YOON_220414
						break;
					}else if (restsize < ONE_BLOCK_SIZE){
						blocksize = restsize;
					}else{
						blocksize = ONE_BLOCK_SIZE;
					}

					crc32_correct = 0xFFFFFFFF;
					//crc32_update(&crc32_correct, &si4790x_firm[CurFirmIndex], blocksize);
							
//					ret |= SI479XX_FLASH_UTIL_write_block__command(IC0, SectorCurAddr, blocksize, &si4790x_firm[CurFirmIndex]);
//					DPRINTF(DBG_INFO,"Write = SectorCurAddr : %x , SectorLastAddr : %x , BlockIndex : %x , blocksize ; %x , CurFirmIndex : %x , Data_buffer : %x \r\n"
//						,SectorCurAddr,SectorLastAddr,BlockIndex,blocksize,CurFirmIndex,si4790x_firm[CurFirmIndex]);

					*task_seq = TUNER_FW_FLASH_UPDATE_3;
				}
				else
				{
					*task_seq = TUNER_FW_FLASH_UPDATE_1;
				}
			}
			break;

		case TUNER_FW_FLASH_UPDATE_3:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_FLASH_UTIL_read_block__command(IC0, SectorCurAddr, blocksize);
				*task_seq = TUNER_FW_FLASH_UPDATE_4;
			}
			break;

		case TUNER_FW_FLASH_UPDATE_4:
			ret |= wait_for_CTS(IC0, 1, ARRAY_SZ(status), status);
			if (ret == SUCCESS){
				ret |= SI479XX_FLASH_UTIL_read_block__reply(IC0, read_block_buffer, blocksize);
//				DPRINTF(DBG_INFO,"Reply = SectorCurAddr : %x , SectorLastAddr : %x , BlockIndex : %x , blocksize ; %x , CurFirmIndex : %x , Read_buffer : %x\r\n "
//					,SectorCurAddr,SectorLastAddr,BlockIndex,blocksize,CurFirmIndex,*read_block_buffer);

				crc32_calc = 0xFFFFFFFF;
				crc32_update(&crc32_calc, read_block_buffer, blocksize);

				#if 1
				if (crc32_correct != crc32_calc){
					*task_seq = TUNER_TASK_SEQ_END;
					
					DPRINTF(DBG_INFO,"SiLab Tuner Update Finished!!_UP4 : %x : %x \r\n",crc32_correct,crc32_calc);
					return ERROR;
				}
				#endif
				BlockIndex++;										/* 220407 SGsoft */
				SectorCurAddr += blocksize;
				CurFirmIndex += blocksize;
				restsize -= blocksize;
							
				*task_seq = TUNER_FW_FLASH_UPDATE_2;
			}
			break;
		
		default:
			break;
	
	}

	return ret;

}
#endif	/*FEAT_FLASH_UTIL*/

#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/
