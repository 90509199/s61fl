/***************************************************************************************
                  Silicon Laboratories Broadcast Si475x/6x/9x module code

   EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
     THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
     TO THIS SOURCE FILE.
   IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
     PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.

   Date: June 06 2014
  (C) Copyright 2014, Silicon Laboratories, Inc. All rights reserved.
****************************************************************************************/
#ifndef _TUNER_API_COMMON_H_
#define _TUNER_API_COMMON_H_

#ifdef TUNER_API_COMMON_H_GLOBAL_	/* 160227 AGsoft */
#define TUNER_API_COMMON_EXT
#else
#define TUNER_API_COMMON_EXT	extern
#endif	/*TUNER_API_COMMON_H_GLOBAL_*/

#if 0
#include "type.h"
#include "tuner_platform.h"
#endif	/*0*/

typedef enum 								
{
  ERROR = 1, 											
  SUCCESS = !ERROR
} ErrorStatus;

/*-------------------------------------------------------------------------------------*/
#define	SI479XXAddr 							0xD0		

TUNER_API_COMMON_EXT	U8 fgRadManu;						


TUNER_API_COMMON_EXT void RadioInit(U8 area);										/* 211201 SGsoft */

TUNER_API_COMMON_EXT void Delay(volatile U32 nCount);
TUNER_API_COMMON_EXT void Delay10Us(U16 us);
TUNER_API_COMMON_EXT void DelayMs(U16 ms);
TUNER_API_COMMON_EXT void pTRST(U8 out);
TUNER_API_COMMON_EXT void TunerReset(void);
TUNER_API_COMMON_EXT void TunProc(void);
TUNER_API_COMMON_EXT void CheckTunRSQ(void);
TUNER_API_COMMON_EXT void WriteTunFreq(U16 freq);
TUNER_API_COMMON_EXT void ChangeBand(U8 fgsetmd);
TUNER_API_COMMON_EXT U8 IncDecFreq(U8 dir);
TUNER_API_COMMON_EXT void ChScan(U8 opt, U8 updn);
TUNER_API_COMMON_EXT U8 SeekCh(U8 dir);
TUNER_API_COMMON_EXT void Seek_Start(U8 dir);
TUNER_API_COMMON_EXT void SeekEnd(void);
TUNER_API_COMMON_EXT void SeekCancel(void);
TUNER_API_COMMON_EXT void RadSeekDown(void);
TUNER_API_COMMON_EXT void RadKeySeekDown(void);
TUNER_API_COMMON_EXT void RadSeekUp(void);
TUNER_API_COMMON_EXT void RadKeySeekUp(void);
TUNER_API_COMMON_EXT void TunFreqPreset(U8 prenum);
TUNER_API_COMMON_EXT void RadKeyTunPreset(U8 mem);
TUNER_API_COMMON_EXT void TunSetVolume(U8 level);
TUNER_API_COMMON_EXT void TunSeekUpDn(U8 band, U8 dir);
TUNER_API_COMMON_EXT U8 CheckPresetNum(void);
TUNER_API_COMMON_EXT void si479x_set_customer_setting(uint8_t band);

#endif 
