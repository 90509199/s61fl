/*
 *
 * Copyright (c) 2009 AGTech Co. Ltd. (AG@BaHananO)
 * 
 *
 */

#ifndef _SG_TUN_DIGEN_SEQ_4_1_H_
#define	_SG_TUN_DIGEN_SEQ_4_1_H_

#ifdef _SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_
#define SG_TUN_DIGEN_SEQ_4_1_EXT
#else
#define SG_TUN_DIGEN_SEQ_4_1_EXT		extern
#endif	/*_SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_*/


enum _TUNER_PROCESS {
	TUNER_PRC_NONE 						= 0x00,
	TUNER_PRC_INIT 						= 0x01,
	TUNER_PRC_BAND_CHANGE 				= 0x02,
	TUNER_PRC_STEP_UP	 				= 0x03,
	TUNER_PRC_STEP_DOWN	 				= 0x04,
	TUNER_PRC_SEEK_UP					= 0x05,
	TUNER_PRC_SEEK_DOWN					= 0x06,
	
	TUNER_PRC_PRESET_TUNE				= 0x07,
	TUNER_PRC_PRESET_SCAN				= 0x08,
	TUNER_PRC_AUTO_STORE				= 0x09,

	TUNER_PRC_CHECK_RSQ					= 0x0A,				

	TUNER_PRC_DIRECT_TUNE 				= 0x0B				
	
#if (FEAT_FLASH_UTIL)		
	,TUNER_PRC_FLASH_UTIL				= 0x0C
#endif	/*FEAT_FLASH_UTIL*/

#if (FEAT_DEBUG_211125)		/* 211125 SGsoft */
	,TUNER_PRC_ERROR					= 0x0D
#endif	/*FEAT_DEBUG_211125*/

};
	
enum _TUNER_TASK {
	TUNER_TASK_NONE 					= 0x00,
	TUNER_TASK_FW_LOAD 					= 0x01,
	TUNER_TASK_AUDIO_CONFIG 			= 0x02,
	TUNER_TASK_AUDIO_MUTE_ON 			= 0x03,
	TUNER_TASK_AUDIO_MUTE_OFF 			= 0x04,
	TUNER_TASK_BAND_SET					= 0x05,
	TUNER_TASK_FM_AREA					= 0x06,
	TUNER_TASK_AM_AREA					= 0x07,
	TUNER_TASK_FM_CUSTOM				= 0x08,
	TUNER_TASK_AM_CUSTOM				= 0x09,
#if (FEAT_CUSTOM_TUNING)		
    TUNER_TASK_FM_CUSTOM_TUNING			= 0xA8,
    TUNER_TASK_AM_CUSTOM_TUNING			= 0xA9,
#endif		/*FEAT_CUSTOM_TUNING*/
	TUNER_TASK_FM_TUNE					= 0x0A,
	TUNER_TASK_AM_TUNE					= 0x0B,

	TUNER_TASK_STEP_UP					= 0x0C,
	TUNER_TASK_STEP_DOWN				= 0x0D,
	TUNER_TASK_SEEK_UP					= 0x0E,
	TUNER_TASK_SEEK_DOWN				= 0x0F,

	TUNER_TASK_PRESET_SCAN				= 0x10,
	TUNER_TASK_AUTO_STORE				= 0x11,

	TUNER_TASK_CHECK_RSQ				= 0x12,					

#if (FEAT_FLASH_UTIL)		
	TUNER_TASK_FLASH_UTIL				= 0x13,	
#endif	/*FEAT_FLASH_UTIL*/

	TUNER_TASK_PRC_INIT_START 			= 0x80,					
	TUNER_TASK_PRC_BAND_CHANGE_START 	= 0x81,
	TUNER_TASK_PRC_STEP_UP_START	 	= 0x82,
	TUNER_TASK_PRC_STEP_DOWN_START	 	= 0x83,
	TUNER_TASK_PRC_SEEK_UP_START		= 0x84,
	TUNER_TASK_PRC_SEEK_DOWN_START		= 0x85,
	TUNER_TASK_PRC_PRESET_TUNE_START	= 0x86,
	TUNER_TASK_PRC_PRESET_SCAN_START	= 0x87,
	TUNER_TASK_PRC_AUTO_STORE_START		= 0x88,
	TUNER_TASK_PRC_CHECK_RSQ_START		= 0x89,					
	TUNER_TASK_PRC_DIRECT_TUNE_START    = 0x8A					
#if (FEAT_FLASH_UTIL)		
	,TUNER_TASK_PRC_FLASH_UTIL_START = 0x8B	
#endif	/*FEAT_FLASH_UTIL*/

};

#define TUNER_TASK_SEQ_END				0
enum _TUNER_SEQ_FW_LOAD {
	TUNER_FW_LOAD_RESET_L 						=  1,
	TUNER_FW_LOAD_RESET_H 						=  2,
	TUNER_FW_LOAD_SYS_CFG 						=  3,
	TUNER_FW_LOAD_POWER_UP_C 					=  4,
	TUNER_FW_LOAD_POWER_UP_R 					=  5,
	TUNER_FW_LOAD_CHECK_BOOTLOADER_C 			=  6,
	TUNER_FW_LOAD_CHECK_BOOTLOADER_R 			=  7,
	TUNER_FW_LOAD_PART_INFO_C 					=  8,
	TUNER_FW_LOAD_PART_INFO_R 					=  9,
	TUNER_FW_LOAD_LOAD_INIT_C 					=  10,
	TUNER_FW_LOAD_FLASH_LOAD_LOAD_C 			=  11,
	TUNER_FW_LOAD_WAIT_CTS 						=  12,
	TUNER_FW_LOAD_CHECK_BOOTREADY_C 			=  13,
	TUNER_FW_LOAD_CHECK_BOOTREADY_R 			=  14,
	TUNER_FW_LOAD_BOOT_C 						=  15,
	TUNER_FW_LOAD_CHECK_APPLICATION_C 			=  16,
	TUNER_FW_LOAD_CHECK_APPLICATION_R 			=  17,
	TUNER_FW_LOAD_FUNC_INFO_C 					=  18,
	TUNER_FW_LOAD_FUNC_INFO_R 					=  19,
	
	TUNER_FW_LOAD_4_1_VER_CHECK_C 				=  40,			/* 220415 SGsoft */
	TUNER_FW_LOAD_4_1_VER_CHECK_R 				=  41
	
#if (FEAT_FLASH_UTIL)		
	,TUNER_FW_FLASH_UPDATE_1					=  21,
	TUNER_FW_FLASH_UPDATE_2						=  22,
	TUNER_FW_FLASH_UPDATE_3						=  23,
	TUNER_FW_FLASH_UPDATE_4						=  24,
	TUNER_FW_FLASH_UPDATE_5						=  25,
	TUNER_FW_FLASH_UPDATE_6						=  26,
	TUNER_FW_FLASH_UPDATE_7						=  27,
	TUNER_FW_FLASH_UPDATE_8						=  28,
	TUNER_FW_FLASH_UPDATE_9						=  29,
	TUNER_FW_FLASH_UPDATE_10					=  30,
	TUNER_FW_FLASH_UPDATE_11					=  31,
	TUNER_FW_FLASH_UPDATE_12					=  32
#endif	/*FEAT_FLASH_UTIL*/

};

enum _TUNER_SEQ_AUDIO_CONFIG {
	TUNER_AUDIO_CONFIG_1 						= 1,
	TUNER_AUDIO_CONFIG_2 						= 2,
	TUNER_AUDIO_CONFIG_3 						= 3,
	TUNER_AUDIO_CONFIG_4 						= 4,
	TUNER_AUDIO_CONFIG_5 						= 5,
	TUNER_AUDIO_CONFIG_6 						= 6,
	TUNER_AUDIO_CONFIG_7 						= 7,
	TUNER_AUDIO_CONFIG_8 						= 8,
	TUNER_AUDIO_CONFIG_9 						= 9
};

enum _TUNER_SEQ_AUDIO_MUTE_ON {
	TUNER_AUDIO_MUTE_ON_1 						= 1,
	TUNER_AUDIO_MUTE_ON_2 						= 2,
	TUNER_AUDIO_MUTE_ON_3 						= 3,
	TUNER_AUDIO_MUTE_ON_4 						= 4,
	TUNER_AUDIO_MUTE_ON_5 						= 5
};

enum _TUNER_SEQ_AUDIO_MUTE_OFF {
	TUNER_AUDIO_MUTE_OFF_1 						= 1,
	TUNER_AUDIO_MUTE_OFF_2 						= 2,
	TUNER_AUDIO_MUTE_OFF_3 						= 3,
	TUNER_AUDIO_MUTE_OFF_4 						= 4,
	TUNER_AUDIO_MUTE_OFF_5 						= 5
};

enum _TUNER_SEQ_BAND_SET {
	TUNER_BAND_SET_1							= 1,
	TUNER_BAND_SET_2							= 2,
	TUNER_BAND_SET_3							= 3,
	TUNER_BAND_SET_4							= 4
};

enum _TUNER_SEQ_FM_AREA {
	TUNER_FM_AREA_KR_1							= 1,
	TUNER_FM_AREA_KR_2							= 2,
	TUNER_FM_AREA_KR_3							= 3,
	TUNER_FM_AREA_KR_4							= 4,
	TUNER_FM_AREA_KR_5							= 5,
	TUNER_FM_AREA_KR_6							= 6,
	TUNER_FM_AREA_KR_7							= 7,
	TUNER_FM_AREA_KR_8							= 8,
	TUNER_FM_AREA_KR_9							= 9,
	TUNER_FM_AREA_KR_10							= 10,

	TUNER_FM_AREA_USA_1							= 21,
	TUNER_FM_AREA_USA_2							= 22,
	TUNER_FM_AREA_USA_3							= 23,
	TUNER_FM_AREA_USA_4							= 24,
	TUNER_FM_AREA_USA_5							= 25,
	TUNER_FM_AREA_USA_6							= 26,
	TUNER_FM_AREA_USA_7							= 27,
	TUNER_FM_AREA_USA_8							= 28,
	TUNER_FM_AREA_USA_9							= 29,
	TUNER_FM_AREA_USA_10						= 30,
	TUNER_FM_AREA_USA_11						= 31,
	TUNER_FM_AREA_USA_12						= 32,
	
	TUNER_FM_AREA_EU_1							= 41,
	TUNER_FM_AREA_EU_2							= 42,
	TUNER_FM_AREA_EU_3							= 43,
	TUNER_FM_AREA_EU_4							= 44,
	TUNER_FM_AREA_EU_5							= 45,
	TUNER_FM_AREA_EU_6							= 46,
	TUNER_FM_AREA_EU_7							= 47,
	TUNER_FM_AREA_EU_8							= 48,
	TUNER_FM_AREA_EU_9							= 49,
	TUNER_FM_AREA_EU_10							= 50

#if (FEAT_TUN_AREA_CHINA)		
   ,TUNER_FM_AREA_CHINA_1						= 61,
	TUNER_FM_AREA_CHINA_2						= 62,
	TUNER_FM_AREA_CHINA_3						= 63,
	TUNER_FM_AREA_CHINA_4						= 64,
	TUNER_FM_AREA_CHINA_5						= 65,
	TUNER_FM_AREA_CHINA_6						= 66,
	TUNER_FM_AREA_CHINA_7						= 67,
	TUNER_FM_AREA_CHINA_8						= 68,
	TUNER_FM_AREA_CHINA_9						= 69,
	TUNER_FM_AREA_CHINA_10						= 70
#endif	/*FEAT_TUN_AREA_CHINA*/

};

enum _TUNER_SEQ_AM_AREA {
	TUNER_AM_AREA_KR_1							= 1,
	TUNER_AM_AREA_KR_2							= 2,
	TUNER_AM_AREA_KR_3							= 3,
	TUNER_AM_AREA_KR_4							= 4,
	TUNER_AM_AREA_KR_5							= 5,
	TUNER_AM_AREA_KR_6							= 6,
	TUNER_AM_AREA_KR_7							= 7,
	TUNER_AM_AREA_KR_8							= 8,
	TUNER_AM_AREA_KR_9							= 9,
	TUNER_AM_AREA_KR_10							= 10,
		  
	TUNER_AM_AREA_USA_1							= 21,
	TUNER_AM_AREA_USA_2							= 22,
	TUNER_AM_AREA_USA_3							= 23,
	TUNER_AM_AREA_USA_4							= 24,
	TUNER_AM_AREA_USA_5							= 25,
	TUNER_AM_AREA_USA_6							= 26,
	TUNER_AM_AREA_USA_7							= 27,
	TUNER_AM_AREA_USA_8							= 28,
	TUNER_AM_AREA_USA_9							= 29,
	TUNER_AM_AREA_USA_10						= 30,
		  
	TUNER_AM_AREA_EU_1							= 41,
	TUNER_AM_AREA_EU_2							= 42,
	TUNER_AM_AREA_EU_3							= 43,
	TUNER_AM_AREA_EU_4							= 44,
	TUNER_AM_AREA_EU_5							= 45,
	TUNER_AM_AREA_EU_6							= 46,
	TUNER_AM_AREA_EU_7							= 47,
	TUNER_AM_AREA_EU_8							= 48,
	TUNER_AM_AREA_EU_9							= 49,
	TUNER_AM_AREA_EU_10							= 50
	
};

enum _TUNER_SEQ_FM_CUSTOM {
	TUNER_FM_CUSTOM_1							= 1,
	TUNER_FM_CUSTOM_2							= 2,
	TUNER_FM_CUSTOM_3							= 3,
	TUNER_FM_CUSTOM_4							= 4,
	TUNER_FM_CUSTOM_5							= 5,
	TUNER_FM_CUSTOM_6							= 6,
	TUNER_FM_CUSTOM_7							= 7,
	TUNER_FM_CUSTOM_8							= 8,
	TUNER_FM_CUSTOM_9							= 9,
	TUNER_FM_CUSTOM_10							= 10,
	TUNER_FM_CUSTOM_11							= 11,
	TUNER_FM_CUSTOM_12							= 12,
	TUNER_FM_CUSTOM_13							= 13,
	TUNER_FM_CUSTOM_14							= 14,
	TUNER_FM_CUSTOM_15							= 15,					

	TUNER_FM_CUSTOM_80							= 80,
	TUNER_FM_CUSTOM_81							= 81,
	TUNER_FM_CUSTOM_82							= 82,			
	TUNER_FM_CUSTOM_83							= 83
};

enum _TUNER_SEQ_AM_CUSTOM {
	TUNER_AM_CUSTOM_1							= 1,
	TUNER_AM_CUSTOM_2							= 2,
	TUNER_AM_CUSTOM_3							= 3,
	TUNER_AM_CUSTOM_4							= 4,
	TUNER_AM_CUSTOM_5							= 5,
	TUNER_AM_CUSTOM_6							= 6,
	TUNER_AM_CUSTOM_7							= 7,				
	TUNER_AM_CUSTOM_8							= 8,
	TUNER_AM_CUSTOM_9							= 9,

	TUNER_AM_CUSTOM_80							= 80,			
	TUNER_AM_CUSTOM_81							= 81,
	TUNER_AM_CUSTOM_82							= 82,			
	TUNER_AM_CUSTOM_83							= 83
};

#if (FEAT_CUSTOM_TUNING)		/* 210414 SGsoft */
enum _TUNER_SEQ_FM_CUSTOM_TUNING {
    TUNER_FM_CUSTOM_TUNING_1							= 1,
    TUNER_FM_CUSTOM_TUNING_2							= 2,
    TUNER_FM_CUSTOM_TUNING_3							= 3,
    TUNER_FM_CUSTOM_TUNING_4							= 4,
    TUNER_FM_CUSTOM_TUNING_5							= 5,
    TUNER_FM_CUSTOM_TUNING_6							= 6,
    TUNER_FM_CUSTOM_TUNING_7							= 7,
    TUNER_FM_CUSTOM_TUNING_8							= 8,
    TUNER_FM_CUSTOM_TUNING_9							= 9,
    TUNER_FM_CUSTOM_TUNING_10							= 10,
    TUNER_FM_CUSTOM_TUNING_11							= 11,
    TUNER_FM_CUSTOM_TUNING_12							= 12,
    TUNER_FM_CUSTOM_TUNING_13							= 13,
    TUNER_FM_CUSTOM_TUNING_14							= 14,
    TUNER_FM_CUSTOM_TUNING_15							= 15,
    TUNER_FM_CUSTOM_TUNING_16							= 16,
    TUNER_FM_CUSTOM_TUNING_17							= 17,
    TUNER_FM_CUSTOM_TUNING_18							= 18,
    TUNER_FM_CUSTOM_TUNING_19							= 19,
    TUNER_FM_CUSTOM_TUNING_20							= 20,
    TUNER_FM_CUSTOM_TUNING_21							= 21,
    TUNER_FM_CUSTOM_TUNING_22							= 22,
    TUNER_FM_CUSTOM_TUNING_23							= 23,
    TUNER_FM_CUSTOM_TUNING_24							= 24,
    TUNER_FM_CUSTOM_TUNING_25							= 25,
    TUNER_FM_CUSTOM_TUNING_26							= 26,
    TUNER_FM_CUSTOM_TUNING_27							= 27,
    TUNER_FM_CUSTOM_TUNING_28							= 28,
    TUNER_FM_CUSTOM_TUNING_29							= 29,
    TUNER_FM_CUSTOM_TUNING_30							= 30,
    TUNER_FM_CUSTOM_TUNING_31							= 31,
    TUNER_FM_CUSTOM_TUNING_32							= 32,
    TUNER_FM_CUSTOM_TUNING_33							= 33,
    TUNER_FM_CUSTOM_TUNING_34							= 34,
    TUNER_FM_CUSTOM_TUNING_35							= 35,
    TUNER_FM_CUSTOM_TUNING_36							= 36,
    TUNER_FM_CUSTOM_TUNING_37							= 37,
    TUNER_FM_CUSTOM_TUNING_38							= 38,
    TUNER_FM_CUSTOM_TUNING_39							= 39,
    TUNER_FM_CUSTOM_TUNING_40							= 40,
    TUNER_FM_CUSTOM_TUNING_41							= 41,
    TUNER_FM_CUSTOM_TUNING_42							= 42,
    TUNER_FM_CUSTOM_TUNING_43							= 43,
    TUNER_FM_CUSTOM_TUNING_44							= 44,
    TUNER_FM_CUSTOM_TUNING_45							= 45,
    TUNER_FM_CUSTOM_TUNING_46							= 46,
    TUNER_FM_CUSTOM_TUNING_47							= 47,
    TUNER_FM_CUSTOM_TUNING_48							= 48,
    TUNER_FM_CUSTOM_TUNING_49							= 49
};

enum _TUNER_SEQ_AM_CUSTOM_TUNING {
    TUNER_AM_CUSTOM_TUNING_1							= 1,
    TUNER_AM_CUSTOM_TUNING_2							= 2,
    TUNER_AM_CUSTOM_TUNING_3							= 3,
    TUNER_AM_CUSTOM_TUNING_4							= 4,
    TUNER_AM_CUSTOM_TUNING_5							= 5,
    TUNER_AM_CUSTOM_TUNING_6							= 6,
    TUNER_AM_CUSTOM_TUNING_7							= 7,
    TUNER_AM_CUSTOM_TUNING_8							= 8,
    TUNER_AM_CUSTOM_TUNING_9							= 9,
    TUNER_AM_CUSTOM_TUNING_10							= 10,
    TUNER_AM_CUSTOM_TUNING_11							= 11,
    TUNER_AM_CUSTOM_TUNING_12							= 12,
    TUNER_AM_CUSTOM_TUNING_13							= 13,
    TUNER_AM_CUSTOM_TUNING_14							= 14,
    TUNER_AM_CUSTOM_TUNING_15							= 15,
    TUNER_AM_CUSTOM_TUNING_16							= 16,
    TUNER_AM_CUSTOM_TUNING_17							= 17,
    TUNER_AM_CUSTOM_TUNING_18							= 18,
    TUNER_AM_CUSTOM_TUNING_19							= 19
};
#endif		/*FEAT_CUSTOM_TUNING*/

enum _TUNER_SEQ_FM_TUNE {
	TUNER_FM_TUNE_1								= 1,
	TUNER_FM_TUNE_2								= 2,
	TUNER_FM_TUNE_3								= 3,
	TUNER_FM_TUNE_4								= 4,
	TUNER_FM_TUNE_5								= 5
};

enum _TUNER_SEQ_AM_TUNE {
	TUNER_AM_TUNE_1								= 1,
	TUNER_AM_TUNE_2								= 2,
	TUNER_AM_TUNE_3								= 3,
	TUNER_AM_TUNE_4								= 4,
	TUNER_AM_TUNE_5								= 5
};

enum _TUNER_SEQ_SEEK_UP {
	TUNER_SEEK_UP_1								= 1,
	TUNER_SEEK_UP_2								= 2,
	TUNER_SEEK_UP_3								= 3,
	TUNER_SEEK_UP_4								= 4,
	TUNER_SEEK_UP_5								= 5
};

enum _TUNER_SEQ_SEEK_DOWN {
	TUNER_SEEK_DOWN_1							= 1,
	TUNER_SEEK_DOWN_2							= 2,
	TUNER_SEEK_DOWN_3							= 3,
	TUNER_SEEK_DOWN_4							= 4,
	TUNER_SEEK_DOWN_5							= 5
};

enum _TUNER_SEQ_CHECK_RSQ {										
	TUNER_CHECK_RSQ_1							= 1,
	TUNER_CHECK_RSQ_2							= 2,
	TUNER_CHECK_RSQ_3							= 3,
	TUNER_CHECK_RSQ_4							= 4,
	TUNER_CHECK_RSQ_5							= 5
};


#if (FEAT_CUSTOM_TUNING)		/* 210516 SGsoft */	
typedef struct _CUSTOM_TUNING_SERVO_TRANSFORM
{
    s16 x1;
    s16 y1;
    s16 x2;
    s16 y2;
    s16 p1_p2;
    s16 p2_p1;
} CUSTOM_TUNING_SERVO_TRANSFORM;

typedef struct _CUSTOM_TUNING_FAST_SERVO_TRANSFORM
{
    s16 x1;
    s16 y1;
    s16 x2;
    s16 y2;
    s16 p1_p2;
    s16 p2_p1;
	U16 enable;
} CUSTOM_TUNING_FAST_SERVO_TRANSFORM;


#ifdef _SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_		
CUSTOM_TUNING_SERVO_TRANSFORM FM_TUNINIG_SERVO_TRANSFORM[15] =
{
        {0, 0, 0, 0, 0, 0},                         /* no meaning */
        {8, -55, 30, 0, 4000, 2000},                /* [1] SNR(0x03) - HICUT(0x04) */
        {5, 30, 84, 210, 4000, 100},                /* [2] RSSI_ADJ(0x1D) - HICUT(0x04) */
        {-5, -30, 8, 0, 100, 100},                  /* [3] SNR(0x03) - HICUT(0x04) */
        {1, 12, 10, 2, 100, 100},                   /* [4] RSSI_ADJ(0x1D) - LOWCUT(0x05) */
        {-10, 0, 19, 50, 4000, 1000},               /* [5] SNR(0x03) - STBLEND(0x06) */
        {25, 0, 57, 50, 4000, 100},                 /* [6] RSSI_ADJ(0x1D) - STBLEND(0x06) */
        {2, 0, 10, 60, 1000, 1000},                 /* [7] PILOTLOCK(0x13) - STBLEND(0x06) */
        {9, 0, 35, 210, 5000, 100},                 /* [8] SNR(0x03) - HIBLEND(0x07) */
        {16, 18, 30, 36, 100, 100},                 /* [9] SNR(0x03) - NBUSN2THR(0x09) */
        {-7, 9, 27, 36, 1000, 1000},                /* [10] RSSI_NC_ADJ(0x21) - NBUSN2THR(0x09) */
		{0, 50, 14, 30, 200, 200},                  /* [11] SNR(0x03) - FADETHRESH(0x0C) */
        {-6, 32, 3, 150, 1000, 1000},               /* [12] RSSI_NC_ADJ(0x21) - CHANBW(0x02) */
        {-10, 30, 10, 60, 10, 5000},                /* [13] ASSI_200(0x0A) - FADETHRESH(0x0C) */
        {-30, 63, 7, 0, 2000, 500}                  /* [14] RSSI_ADJ(0x1D) - SOFTMUTE(0x03) */
};

CUSTOM_TUNING_FAST_SERVO_TRANSFORM FM_TUNINIG_FAST_SERVO_TRANSFORM[6] =
{
      	{0, 0, 0, 0, 0, 0, 0},                      /* no meaning */
      	{0, 149, 1, 150, 17, 17, 1},                /* [1] ASSI100(0x07) - CHANBW(0x02) */
      	{-19, 150, 19, 40, 51, 17, 1},             	/* [2] ASSI200(0x0A) - CHANBW(0x02) */
      	{15, 30, 75, 150, 1, 51, 1},                /* [3] DEVIATION(0x0D) - CHANBW(0x02) */
      	{7, 15, 75, 150, 17, 17, 1},                /* [4] MAXDEV(0x24) - CHANBW(0x02) */
      	{90, 0, 95, 100, 1, 1, 1}               	/* [5] CLEANSIGNAL(0x23) - CHANBW(0x02) */
};

CUSTOM_TUNING_SERVO_TRANSFORM AM_TUNINIG_SERVO_TRANSFORM[5] =
{
        {0, 0, 0, 0, 0, 0},                         /* no meaning */
        {0, 10, 8, 40, 2000, 16},                   /* [1] SNR(0x03) - HICUT(0x04) */
        {30, 18, 44, 35, 500, 500},                 /* [2] RSSI_ADJ(0x17) - NBIQ2TH(0x08) */
        {0, 63, 30, 0, 50, 16},                     /* [3] RSSI_ADJ(0x17) - SOFTMUTE(0x03) */
        {10, 100, 30, 1, 10, 500}                   /* [4] RSSI_ADJ(0x17) - LOWCUT(0x05) */
};

U16 FmValidRssi = 14;
U16 FmValidSnr = 10;
U16 AmValidRssi = 20;
U16 AmValidSnr = 15;
#else
extern CUSTOM_TUNING_SERVO_TRANSFORM FM_TUNINIG_SERVO_TRANSFORM[15];
extern CUSTOM_TUNING_FAST_SERVO_TRANSFORM FM_TUNINIG_FAST_SERVO_TRANSFORM[6];
extern CUSTOM_TUNING_SERVO_TRANSFORM AM_TUNINIG_SERVO_TRANSFORM[5];
extern U16 FmValidRssi;
extern U16 FmValidSnr;
extern U16 AmValidRssi;
extern U16 AmValidSnr;
#endif	/*_SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_*/
#endif		/*FEAT_CUSTOM_TUNING*/


typedef struct _TUNER_TO_DO
{
	U8 prc00;													
	U8 prc0;													
	U8 prc;
	U8 task;
	U8 seq;
} TUNER_TO_DO;

SG_TUN_DIGEN_SEQ_4_1_EXT TUNER_TO_DO stTuner;

SG_TUN_DIGEN_SEQ_4_1_EXT U16 TmrTunerTaskSeq;						/* 1ms base timer */
SG_TUN_DIGEN_SEQ_4_1_EXT U8	ucSetMem;								
SG_TUN_DIGEN_SEQ_4_1_EXT AUDIO_MUTE fgRadMuteOnOff;					
SG_TUN_DIGEN_SEQ_4_1_EXT U16 drtlFreq;								

SG_TUN_DIGEN_SEQ_4_1_EXT U8 CHANBW, SOFTMUTE, HICUT, LOWCUT;									
SG_TUN_DIGEN_SEQ_4_1_EXT U8 FM_STBLEND, FM_HIBLEND, FM_NBUSN1THR, FM_NBUSN2THR, FM_FADETHRESH;
SG_TUN_DIGEN_SEQ_4_1_EXT U8 AM_NBAUDTH, AM_NBIQ2TH, AM_NBIQ3TH;
SG_TUN_DIGEN_SEQ_4_1_EXT s8 AM_AVC, AM_MAXAVC;

SG_TUN_DIGEN_SEQ_4_1_EXT U8 EscCnt;																

#if (FEAT_FLASH_UTIL)		
#define	SERIAL_FLASH_START_ADDR			0x0000//0x2000/*0x4B000*/				//YOON_220415 : 2.3 에서는 Start ADDR = 0x000 , 4.1 이후부터 Start ADDR = 0x2000 으로 Setting 필요. 	/* 210526 SGsoft */						
#define	ONE_SECTOR_SIZE					0x1000									/*4KB*/
#define	ONE_BLOCK_SIZE					0x80								    /*128B*/
#define	ONE_SECTOR_BLOCK_TOTAL			ONE_SECTOR_SIZE / ONE_BLOCK_SIZE

#if 0
#define FLASH_UTIL_IMAGE_START_ADD  	0x00000								
#define FLASH_UTIL_IMAGE_SIZE			0x4691A
#else
#if 1		/* 210518 SGsoft */
#define FLASH_UTIL_IMAGE_START_ADD  	0x0000						//YOON_220415 : 2.3 에서는 Start ADDR = 0x000 , 4.1 이후부터 Start ADDR = 0x2000 으로 Setting 필요. 
#define FLASH_UTIL_IMAGE_SIZE			0x2BD81//0x1CE0				//YOON_220407 : 저장되어 있는 Firmware Version Size 로 변경 필요 - 2.3 버전 크기 /* sejin_si479xx_flash_util_only_image_4_1_4_210510.bin */
#else
#define FLASH_UTIL_IMAGE_START_ADD  	SERIAL_FLASH_START_ADDR					
#define FLASH_UTIL_IMAGE_SIZE			0x2BD81//0x1CE0						
#endif	/*1*/
#endif	/*1*/


SG_TUN_DIGEN_SEQ_4_1_EXT U32 SectorLastAddr, SectorCurAddr, CurFirmIndex, BlockIndex;
SG_TUN_DIGEN_SEQ_4_1_EXT uint32_t restsize, blocksize, crc32_correct, crc32_calc;
#endif	/*FEAT_FLASH_UTIL*/

#if (FEAT_DIGEN_4_1_TUNING)		/* 210803 SGsoft */
typedef struct _CUSTOM_SERVO
{
	u8 sid;
    s16 min;
    s16 max;
    s16 init;
    u8 acc;
} CUSTOM_SERVO;

typedef struct _CUSTOM_SERVO_TRANSFORM
{
	u8 mid;
	u8 sid;
    s16 x1;
    s16 y1;
    s16 x2;
    s16 y2;
    s16 p1_p2;
    s16 p2_p1;
} CUSTOM_SERVO_TRANSFORM;

typedef struct _CUSTOM_FAST_SERVO_TRANSFORM
{
	u8 fid;
	u8 en;
    s16 x1;
    s16 y1;
    s16 x2;
    s16 y2;
    s16 p1_p2;
    s16 p2_p1;
} CUSTOM_FAST_SERVO_TRANSFORM;

typedef struct _CUSTOM_PROPERTY
{
    u16 prop;
    u16 val;
} CUSTOM_PROPERTY;



#ifdef _SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_	
#if (FEAT_DIGEN_4_1_TUNING_211216)		/* 211216 SGsoft */
CUSTOM_SERVO FM_SERVO[7] =
{
	{ 0x02, 0x0032, 0x0096, 0x004b, 0x00 },
	{ 0x03, 0x0000, 0x001e, 0x0000, 0x01 },
	{ 0x04, 0x0014, 0x00d2, 0x0002, 0x00 },
	{ 0x06, 0x0000, 0x0028, 0x0000, 0x00 },
	{ 0x07, 0x000a, 0x00d2, 0x0000, 0x00 },
	{ 0x12, 0x0014, 0x00a0, 0x00a0, 0x01 },
	{ 0x13, 0xff80, 0x003c, 0xff80, 0x01 }		
};
#else
CUSTOM_SERVO FM_SERVO[5] =
{
	{ 0x02, 0x0032, 0x0096, 0x004b, 0x00 },
	{ 0x03, 0x0000, 0x001e, 0x0000, 0x01 },
	{ 0x04, 0x0014, 0x00d2, 0x0002, 0x00 },
	{ 0x06, 0x0000, 0x0028, 0x0000, 0x00 },
	{ 0x07, 0x000a, 0x00d2, 0x0000, 0x00 }
};
#endif	/*FEAT_DIGEN_4_1_TUNING_211216*/

CUSTOM_SERVO AM_SERVO[4] =
{
	{ 0x02, 0x001e, 0x0046, 0x001e, 0x00 },
	{ 0x03, 0x0000, 0x0028, 0x0000, 0x01 },
	{ 0x05, 0x0003, 0x0032, 0x0001, 0x01 },
	{ 0x06, 0x0014, 0x0032, 0x0000, 0x00 }
};

#if (FEAT_DIGEN_4_1_TUNING_211201) || (FEAT_DIGEN_4_1_TUNING_211216)		/* 211216 SGsoft */		
CUSTOM_SERVO_TRANSFORM FM_SERVO_TRANSFORM[20] =
{
	{ 0x1d,0x02,0xfffa,0x0024,0x0003,0x0096,0x0064,0x0064 },
	{ 0x03,0x0c,0x0000,0x0032,0x000e,0x001e,0x00c8,0x00c8 },
	{ 0x0a,0x0c,0xfff6,0x001e,0x000a,0x003c,0x000a,0x1388 },
	{ 0x1d,0x07,0x0027,0x0014,0x003c,0x00b4,0x07d0,0x01f4 },
	{ 0x0c,0x07,0x0032,0x0014,0x0041,0x00b4,0x07d0,0x01f4 },
	{ 0x0b,0x07,0x0008,0x00b4,0x001c,0x0014,0x07d0,0x01f4 },
	{ 0x1d,0x04,0x0006,0x0014,0x0026,0x00b4,0x1388,0x03e8 },
	{ 0x0c,0x04,0x001e,0x0014,0x0032,0x00b4,0x1388,0x03e8 },
	{ 0x0b,0x04,0x001e,0x00b4,0x0037,0x0014,0x03e8,0x1388 },
	{ 0x03,0x05,0x0002,0x000c,0x000a,0x0002,0x1388,0x03e8 },
	{ 0x03,0x09,0x0010,0x0012,0x001e,0x0024,0x0032,0x0032 },
	{ 0x1d,0x09,0xfff9,0xfffb,0x001b,0x0024,0x0032,0x0032 },
	{ 0x03,0x06,0x0005,0x0000,0x0014,0x0028,0x1f40,0x03e8 },
	{ 0x1d,0x06,0x000f,0x0000,0x003c,0x0028,0x1f40,0x03e8 },
	{ 0x13,0x06,0x0002,0x0000,0x000a,0x003c,0x03e8,0x03e8 },
	{ 0x03,0x13,0xffd4,0xff7f,0x0028,0x003c,0x0001,0x0001 },
	{ 0x03,0x12,0x0003,0x000a,0x0024,0x00a0,0x1388,0x03e8 },
	{ 0x03,0x11,0x0005,0x0018,0x0023,0x0000,0x1388,0x03e8 },
	{ 0x1d,0x03,0xffec,0x0014,0x0000,0x0000,0x00fa,0x00fa },
	{ 0x03,0x03,0xfff6,0x0014,0xffff,0x0000,0x0032,0x0032 }
};
#else
CUSTOM_SERVO_TRANSFORM FM_SERVO_TRANSFORM[19] =
{
	{ 0x20, 0x02, 0xfffa, 0x0024, 0x0003, 0x0096, 0x0064, 0x0064 },
	{ 0x03, 0x0c, 0x0000, 0x0032, 0x000e, 0x001e, 0x00c8, 0x00c8 },
	{ 0x0a, 0x0c, 0xfff6, 0x001e, 0x000a, 0x003c, 0x000a, 0x1388 },
	{ 0x1d, 0x07, 0x0027, 0x0014, 0x003c, 0x00b4, 0x07d0, 0x01f4 },
	{ 0x0c, 0x07, 0x0032, 0x0014, 0x0041, 0x00b4, 0x07d0, 0x01f4 },
	{ 0x0b, 0x07, 0x0008, 0x00b4, 0x001c, 0x0014, 0x07d0, 0x01f4 },
	{ 0x1d, 0x04, 0x0006, 0x0014, 0x0026, 0x00b4, 0x1388, 0x03e8 },
	{ 0x0c, 0x04, 0x001e, 0x0014, 0x0032, 0x00b4, 0x1388, 0x03e8 },
	{ 0x0b, 0x04, 0x001e, 0x00b4, 0x0037, 0x0014, 0x03e8, 0x1388 },
	{ 0x03, 0x12, 0x0003, 0x000a, 0x0024, 0x00a0, 0x1388, 0x03e8 },
	{ 0x03, 0x11, 0x0005, 0x0018, 0x0023, 0x0000, 0x1388, 0x03e8 },
	{ 0x21, 0x05, 0x000f, 0x000c, 0x0028, 0x0002, 0x1388, 0x03e8 },
	{ 0x03, 0x09, 0x0010, 0x0012, 0x001e, 0x0024, 0x0032, 0x0032 },
	{ 0x21, 0x09, 0xfff9, 0xfffb, 0x001b, 0x0024, 0x0032, 0x0032 },
	{ 0x21, 0x03, 0xffec, 0x0014, 0x0000, 0x0000, 0x00fa, 0x00fa },
	{ 0x03, 0x03, 0xfff6, 0x0014, 0xffff, 0x0000, 0x0032, 0x0032 },
	{ 0x03, 0x06, 0x0005, 0x0000, 0x0014, 0x0028, 0x1f40, 0x03e8 },
	{ 0x1d, 0x06, 0x000f, 0x0000, 0x003c, 0x0028, 0x1f40, 0x03e8 },
	{ 0x13, 0x06, 0x0002, 0x0000, 0x000a, 0x003c, 0x03e8, 0x03e8 }
};
#endif	/*FEAT_DIGEN_4_1_TUNING_211201 || FEAT_DIGEN_4_1_TUNING_211216*/

CUSTOM_SERVO_TRANSFORM AM_SERVO_TRANSFORM[5] =
{
	{ 0x19, 0x02, 0x0023, 0x001e, 0x003c, 0x0046, 0x03e8, 0x0014 },
	{ 0x03, 0x04, 0x001e, 0x000a, 0x002a, 0x0028, 0x07d0, 0x0010 },
	{ 0x19, 0x08, 0x001e, 0x0012, 0x002c, 0x0023, 0x01f4, 0x01f4 },
	{ 0x17, 0x03, 0xffec, 0x001e, 0x001e, 0x0000, 0x01f4, 0x01f4 },
	{ 0x03, 0x03, 0xffec, 0x000f, 0x001e, 0x0000, 0x0001, 0x0001 }
};

#if (FEAT_DIGEN_4_1_TUNING_211216)		/* 211216 SGsoft */
CUSTOM_FAST_SERVO_TRANSFORM FM_FAST_TRANSFORM[3] =
{
	{ 0x01, 0x01, 0xffed, 0x0096, 0x0013, 0x0028, 0x0011, 0x0011 },
	{ 0x03, 0x01, 0x000f, 0x0032, 0x004b, 0x0096, 0x000a, 0x001e },
	{ 0x05, 0x01, 0x005a, 0x0000, 0x005f, 0x0064, 0x000a, 0x000a }
};
#else
CUSTOM_FAST_SERVO_TRANSFORM FM_FAST_TRANSFORM[2] =
{
	{ 0x03, 0x00, 0x000f, 0x0032, 0x004b, 0x0096, 0x000a, 0x001e },
	{ 0x05, 0x01, 0x005a, 0x0000, 0x005f, 0x0064, 0x000a, 0x000a }
};
#endif	/*FEAT_DIGEN_4_1_TUNING_211216*/

#if (FEAT_DIGEN_4_1_TUNING_211216)		/* 211216 SGsoft */
CUSTOM_PROPERTY FM_PROPERTY[18] =
{
	{ 0x0700, 0x0040 },
	{ 0x2201, 0x00f6 },
	{ 0x2300, 0x0003 },
	{ 0x2301, 0x0052 },
	{ 0x2302, 0x000c },
	{ 0x2303, 0x00d7 },
	{ 0x2304, 0x0000 },
	{ 0x2305, 0x0000 },
	{ 0x2311, 0x000a },
	{ 0x2312, 0x000e },
	{ 0x2313, 0x0005 }, /***/
	{ 0x2315, 0x0000 },	/***/
	{ 0x2316, 0x001e },
	{ 0x2317, 0x000a },
	{ 0x2318, 0x0023 },
	{ 0x231a, 0x0004 },
	{ 0x2901, 0x00f4 }, /***/
	{ 0x2a02, 0x0001 }	/***/
};
#else
CUSTOM_PROPERTY FM_PROPERTY[13] =
{
	{ 0x0700, 0x0054 },
	{ 0x2301, 0x0012 },
	{ 0x2302, 0x000e },
	{ 0x2304, 0x0023 },
	{ 0x2305, 0x0020 },
	{ 0x2311, 0x0007 },
	{ 0x2312, 0x0009 },
	{ 0x2313, 0x0005 }, 
	{ 0x2315, 0x0000 },
	{ 0x2316, 0x0050 },
	{ 0x2318, 0x0014 },
	{ 0x2901, 0x00f4 },
	{ 0x2a02, 0x0001 }
};
#endif	/*FEAT_DIGEN_4_1_TUNING_211216*/

#if (FEAT_DIGEN_4_1_TUNING_211216)		/* 211216 SGsoft */
CUSTOM_PROPERTY AM_PROPERTY[6] =
{
	{ 0x0700, 0x0040 },
	{ 0x4201, 0x00fd },
	{ 0x4304, 0x0000 },
	{ 0x4305, 0x0007 },
	{ 0x4307, 0x000a },
	{ 0x430c, 0x0014 }
};
#else
CUSTOM_PROPERTY AM_PROPERTY[13/*10*/] =
{
	{ 0x0700, 0x0054 },
	{ 0x2301, 0x0012 },
	{ 0x2302, 0x000e },
	{ 0x2304, 0x0023 },
	{ 0x2305, 0x0020 },
	{ 0x2311, 0x0007 },
	{ 0x2312, 0x0009 },
	{ 0x2313, 0x0005 }, 
	{ 0x2315, 0x0000 },
	{ 0x2316, 0x0050 },
	{ 0x2318, 0x0014 },
	{ 0x2901, 0x00f4 },
	{ 0x2a02, 0x0001 }
};
#endif	/*FEAT_DIGEN_4_1_TUNING_211216*/

CUSTOM_PROPERTY COMMON_PROPERTY[5/*6*/] =
{
	{ 0x050a, 0x0006 },
	{ 0x050b, 0x0000 },
	{ 0x0512, 0x0000 },
	{ 0x0518, 0x0008 },
	{ 0x0e01, 0x0142 },
	//{ 0x0e02, 0x0780 }

};

#else
#if (FEAT_DIGEN_4_1_TUNING_211216)		/* 211216 SGsoft */
extern CUSTOM_SERVO FM_SERVO[7];
extern CUSTOM_SERVO AM_SERVO[4];
extern CUSTOM_SERVO_TRANSFORM FM_SERVO_TRANSFORM[20];
extern CUSTOM_SERVO_TRANSFORM AM_SERVO_TRANSFORM[5];
extern CUSTOM_FAST_SERVO_TRANSFORM FM_FAST_TRANSFORM[3];
extern CUSTOM_PROPERTY FM_PROPERTY[18];
extern CUSTOM_PROPERTY AM_PROPERTY[6];
extern CUSTOM_PROPERTY COMMON_PROPERTY[5/*6*/];
#else
extern CUSTOM_SERVO FM_SERVO[5];
extern CUSTOM_SERVO AM_SERVO[4];
extern CUSTOM_SERVO_TRANSFORM FM_SERVO_TRANSFORM[19];
extern CUSTOM_SERVO_TRANSFORM AM_SERVO_TRANSFORM[5];
extern CUSTOM_FAST_SERVO_TRANSFORM FM_FAST_TRANSFORM[2];
extern CUSTOM_PROPERTY FM_PROPERTY[13/*18*/];
extern CUSTOM_PROPERTY AM_PROPERTY[13/*18*/];
extern CUSTOM_PROPERTY COMMON_PROPERTY[5/*6*/];
#endif	/*FEAT_DIGEN_4_1_TUNING_211216*/
#endif	/*_SG_TUN_DIGEN_SEQ_4_1_H_GLOBAL_*/

SG_TUN_DIGEN_SEQ_4_1_EXT u8 svindex;
#endif	/*FEAT_DIGEN_4_1_TUNING*/


SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE DG_writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t *buffer);

SG_TUN_DIGEN_SEQ_4_1_EXT void TunerProcess(void);

SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcInitStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcBandChangeStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcStepUpStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcStepDownStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcSeekUpStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcSeekDownStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcPresetTuneStart(void);							
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcPresetScanStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcAutoStoreStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcCheckRsqStart(void);								
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcDirectTuneStart(void);							

SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcNoneSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcInitSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcBandChangeSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcStepUpSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcStepDownSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcSeekUpSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcSeekDownSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcPresetTuneSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcPresetScanSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcAutoStoreSeq(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcCheckRsqSeq(void);							
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcDirectTuneSeq(void);						

SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskInit(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskAudioConfig(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT  RETURN_CODE TunerTaskAudioMuteOn(U8 *task_seq);				
SG_TUN_DIGEN_SEQ_4_1_EXT  RETURN_CODE TunerTaskAudioMuteOff(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskBandSet(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskFmArea(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskFmCustom(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskAmCustom(U8 *task_seq);
#if (FEAT_CUSTOM_TUNING)		/* 210414 SGsoft */
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskFmCustomTuning(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskAmCustomTuning(U8 *task_seq);
#endif		/*FEAT_CUSTOM_TUNING*/
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskFmTune(U8 *task_seq, U16 freq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskAmTune(U8 *task_seq, U16 freq);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskCheckRsq(U8 *task_seq);					

#if (FEAT_FLASH_UTIL)		
SG_TUN_DIGEN_SEQ_4_1_EXT void crc32_update(uint32_t *p_crc32, const void* data, uint32_t len);

SG_TUN_DIGEN_SEQ_4_1_EXT void TunerPrcFlashUtilStart(void);
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerPrcFlashUtilSeq(void);	
SG_TUN_DIGEN_SEQ_4_1_EXT RETURN_CODE TunerTaskFlashUtil(U8 *task_seq);
SG_TUN_DIGEN_SEQ_4_1_EXT void TunerSerialFlashUpdate(void);				//YOON_220414

#endif	/*FEAT_FLASH_UTIL*/

#endif	/*_SG_TUN_DIGEN_SEQ_4_1_H_*/
