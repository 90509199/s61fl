#ifndef _AG_TUNMAIN_H_
#define _AG_TUNMAIN_H_

#ifdef _AG_TUNMAIN_H_GLOBAL_
#define AG_TUNMAIN_EXT
#else
#define AG_TUNMAIN_EXT	extern
#endif //_AG_TUNMAIN_H_GLOBAL_


#define	CH_SEEK_AUTO						1
#define	CH_SEEK_MANU						0

#define	FREQ_UP								0x01
#define	FREQ_DN								0x02
#define	SEEK_UP								FREQ_UP
#define	SEEK_DN								FREQ_DN

#define	TUN_STATE_DONE						0
#define	TUN_STATE_SEEK_UP					1
#define	TUN_STATE_SEEK_DN					2
#define	TUN_STATE_SEEK_STOP					3
#define	TUN_STATE_AUTO_STORE				4		
#define	TUN_STATE_PRESET_SCAN				5
#if defined(ENABLE_TUNER_DEBUG)
#define	TUN_STATE_TUNER_DEBUG				6		
#define	TUN_STATE_TUNER_DEBUG2				7		
#define	TUN_STATE_TUNER_DEBUG3				8		
#endif	//ENABLE_TUNER_DEBUG

//#define UP 				1 
//#define DOWN 				0 
#define BND_LIMIT 			1
#define BND_NOT_LIMIT 		0

#define CH_BAND 			1				
#define CH_FREQ 			2				
#define CH_MEM 				3				
#define CH_MUTE 			4				
#define CH_LODX 			5 				
#define CH_MEM_FREQ 		6     		
#define CH_MOST				7
#define CH_TEST1 			8				

#define DIP_AREA 			3
#define DIP_SEL1 			2
#define DIP_SEL2 			1

#if (FEAT_TUN_AREA_CHINA)		
enum {A_KR, A_EU, A_SA, A_USA, A_CHINA, A_ORT, A_JPN, A_LW};			
#else
enum {A_KR, A_EU, A_SA, A_USA, A_ORT, A_JPN, A_LW};			
#endif	/*FEAT_TUN_AREA_CHINA*/
					
enum {B_FM1, B_FM2, B_FM3, B_AM1, B_AM2, B_WB, B_ROT};													
enum {S_MONO, S_STEREO, S_SEEK, S_MUTE, S_SEEKABORT, S_NOSTEREO};
enum {LX_DX, LX_LO};

#if defined(ENABLE_TUNER_SI479XX)		
#define	B_FM_MAX		B_FM3					
#define	B_AM_MAX		B_AM2					
#else
#define	B_FM_MAX		B_FM2					
#define	B_AM_MAX		B_AM1					
#endif	/*ENABLE_TUNER_SI479XX*/

typedef struct _AreaBandInfo_
{
	U16 Start_Freq;					
	U16 Stop_Freq; 					
	U8 Step;							
}AreaBandInfo;					//Area Band Infomation

#ifdef _AG_TUNMAIN_H_GLOBAL_
U8	ucBand=0, 	ucArea=0, 	ucTun=0, ucBand0 = 0xFF;				
U8	ucMem=0,	ucSTMO=0;		
U16	iStep=0;				
U32 lSFreq=0, 	lEFreq=0; 
U16	lFreq=0, lFreq0=0;				
U8	CHVALID, ucMemB;							

#if defined(ENABLE_TUNER_SI479XX)		
volatile s8 SNR0, SNR;											
volatile s8 RSSI0, RSSI;
volatile U8 STIND, STBLEND, MULT, AGCEN, AGCINDEX, AFCRL, DEV, PDEV, USN, COCHANL, STCINT;		
volatile s8 FREQOFF, ASSI100, ASSI200, LASSI, HASSI;								
#elif defined(ENABLE_TUNER_SI473X)									
volatile U8 SNR0, SNR, SNRCNT;							
volatile U8 RSSI0, RSSI, RSSICNT;
#elif defined(ENABLE_TUNER_SI474X)	
volatile U8 SNR0, SNR, SNRCNT;											
volatile U8 RSSI0, RSSI, RSSICNT;
volatile U8 STBLEND, MULT, AGCEN, AGCINDEX, AFCRL;	
volatile s8 FREQOFF;
#elif defined(ENABLE_TUNER_NXP_TEF663X)							
volatile U8 SNR0, SNR, rSNR;									
volatile U8 RSSI0, RSSI, rRSSI;
volatile U8 SNRCNT, RSSICNT, rMULTI;	
#else
volatile s8 SNR0, SNR, rSNR;									
volatile s8 RSSI0, RSSI, rRSSI;
volatile U8 SNRCNT, RSSICNT;	
#endif /*/ENABLE_TUNER_SI479XX*/

#if (FEAT_TUN_AREA_CHINA)
const U16 default_freq[12*5]={			
#else
const U16 default_freq[12*4]={		
#endif	/*FEAT_TUN_AREA_CHINA*/
	8750,	8810,	9810,	10410,	10800,	8750,		//Korea FM	
	531,	603,	999,	1404,	1602,	531,		//Korea AM	

	8750, 	9000, 	9800, 	10600, 	10800, 	8750,		//Europe FM
	522, 	603, 	999, 	1404, 	1620,	522,		//Europe AM
	
	8750, 	9000, 	9800, 	10600, 	10800, 	8750,		//South-America FM
	520, 	600, 	1000,	1400,	1620,	520,		//South-America AM
	
	8750, 	9010, 	9810, 	10610, 	10790, 	8750,		//USA(North America) FM					
	530, 	600, 	1000,	1400,	1710,	530			//USA(North America) AM
	
#if (FEAT_TUN_AREA_CHINA)		
   ,8750,	8810,	9810,	10410,	10800,	8750,		//CHINA FM	
	531,	603,	999,	1404,	1602,	531			//CHINA AM	
#endif	/*FEAT_TUN_AREA_CHINA*/
};

#if (FEAT_TUN_AREA_CHINA)		
const AreaBandInfo AB_Info[2*5]={      
#else
const AreaBandInfo AB_Info[2*4]={      	
#endif	/*FEAT_TUN_AREA_CHINA*/
	{8750, 	10800,	10},               					//Korea FM    
	{531, 	1602, 	9},                					//Korea AM    
	
	{8750, 	10800, 	5},									//Europe FM 
	{522, 	1620, 	9},                 				//Europe AM 
	
	{8750, 	10800,	10},               					//South-America FM    
	{520, 	1620, 	10},                				//South-America AM 
	
	{8750, 	10790,	20},               					//USA(North America) FM    				
	{530, 	1710, 	10}                					//USA(North America) AM  
#if (FEAT_TUN_AREA_CHINA)		
   ,{8750, 	10800,	10},               					//CHINA FM    				
	{531, 	1602, 	9},                					//CHINA AM 
#endif	/*FEAT_TUN_AREA_CHINA*/
	
};

const U16 WBFreq[7]={ 400, 425, 450, 475, 500, 525, 550 };						

#else
extern U8 ucBand,	ucArea,	ucTun, ucBand0;				
extern U8 ucMem, ucSTMO;		
extern U16	iStep;														
extern U32	lSFreq, lEFreq; 
extern U16	lFreq, lFreq0;									
extern U8	CHVALID, ucMemB;															

#if defined(ENABLE_TUNER_SI479XX)		
extern volatile s8 SNR0, SNR;											
extern volatile s8 RSSI0, RSSI;
extern volatile U8 STIND, STBLEND, MULT, AGCEN, AGCINDEX, AFCRL, DEV, PDEV, USN, COCHANL, STCINT;		
extern volatile s8 FREQOFF, ASSI100, ASSI200, LASSI, HASSI;								
#elif defined(ENABLE_TUNER_SI473X)									
extern volatile U8 SNR0, SNR, SNRCNT;							
extern volatile U8 RSSI0, RSSI, RSSICNT;
#elif defined(ENABLE_TUNER_SI474X)								
extern volatile U8 SNR0, SNR, SNRCNT;											
extern volatile U8 RSSI0, RSSI, RSSICNT;
extern volatile U8 STBLEND, MULT, AGCEN, AGCINDEX, AFCRL;	
extern volatile s8 FREQOFF;
#elif defined(ENABLE_TUNER_NXP_TEF663X)							
extern volatile U8 SNR0, SNR, rSNR;									
extern volatile U8 RSSI0, RSSI, rRSSI;
extern volatile U8 SNRCNT, RSSICNT, rMULTI;	
#else
extern volatile s8 SNR0, SNR, rSNR;									
extern volatile s8 RSSI0, RSSI, rRSSI;
extern volatile U8 SNRCNT, RSSICNT;	
#endif	/*ENABLE_TUNER_SI479XX*/

#if (FEAT_TUN_AREA_CHINA)
extern const U16 default_freq[12*5];				
extern const AreaBandInfo AB_Info[2*5];
#else
extern const U16 default_freq[12*4];				
extern const AreaBandInfo AB_Info[2*4];
#endif	/*FEAT_TUN_AREA_CHINA*/

extern const U16 WBFreq[7];								
#endif //_AG_TUNMAIN_H_GLOBAL_

AG_TUNMAIN_EXT  U8	ucPreMem;										

#define NUM_OF_PRESET_MEMORY		6
AG_TUNMAIN_EXT  U16	PresetMem[NUM_OF_PRESET_MEMORY];			

#if defined(ENABLE_TUNER_SI473X)	
typedef struct _PresetAutoStore_								
{
	U16 Freq[NUM_OF_PRESET_MEMORY];					
	U8 SNR[NUM_OF_PRESET_MEMORY];
	U8 chcnt;
}PresetAutoStore;	
#else
typedef struct _PresetAutoStore_								
{
	U16 Freq[NUM_OF_PRESET_MEMORY];					
	s8 SNR[NUM_OF_PRESET_MEMORY];								
	U8 chcnt;
}PresetAutoStore;	
#endif	//defined(ENABLE_TUNER_SI473X)	

AG_TUNMAIN_EXT  PresetAutoStore PreAutoStore;

AG_TUNMAIN_EXT  U8 TmrTunRSQ;			

#if (FEAT_DEBUG_211125)		/* 211125 SGsoft */
AG_TUNMAIN_EXT U8 fgARBERR;
AG_TUNMAIN_EXT U8 ARBERR_STATUS[4];
AG_TUNMAIN_EXT U8 DETAIL_STATUS[32];
#endif	/*FEAT_DEBUG_211125*/

#endif //_AG_TUNMAIN_H_
