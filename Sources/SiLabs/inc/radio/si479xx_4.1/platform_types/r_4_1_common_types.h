/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef _COMMON_TYPES_
#define _COMMON_TYPES_

#ifdef __cplusplus
extern "C" {
#endif

///#include "platform_selector.h"

// If the std header is unavailable, define the required types for your platform here.
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#if 1		/* 210630 SGsoft */
#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif
#endif	/*1*/

#if 0/*defined(PLATFORM_EVB)*/		/* 210416 SGsoft */
#include "hal/si_evb/external/evb_cypress_lib/Si479xxapi.h"						
#else // defined(PLATFORM_EVB)

typedef enum {
    IC0 = 0x01,
    IC1 = 0x02,
    IC2 = 0x04,
    IC3 = 0x08,
    IC4 = 0x10,
    IC5 = 0x20,
    IC6 = 0x40,
    IC7 = 0x80
} dc_config_chip_select_enum;
typedef uint8_t dc_config_chip_select;

typedef enum {
    Si479xx = 0,
    Si461x = 1,
    Si476x = 2,
    Si469x = 3,
    Si4796x = 4,
    Si463x = 5,
    Si4697 = 6,
    Si462x = 7,
    NotPresent = 0xFF
} dc_config_chip_type;

#endif // defined(PLATFORM_EVB)

// Other platforms will require the error codes to be defined
// bitmask of errors
typedef enum {
    ///SUCCESS = 0,										/* 210416 SGsoft */
    // Errors when using the 'Si479xxapi.h' functions (USB driver)
    HAL_ERROR = (1 << 0),
    INVALID_INPUT = (1 << 1),
    INVALID_MODE = (1 << 2),
    TIMEOUT = (1 << 3),
    // Indicates that there was an error when allocating memory in the fw apis.
    MEMORY_ERROR = (1 << 4),
    // for part APIERR (recoverable errors)
    COMMAND_ERROR = (1 << 5),
    // for other part warnings or non-recoverable errors.
    NR_WARN_ERROR = (1 << 6),
    FIRMWARE_IMAGE_INVALID = (1 << 7),
    UNSUPPORTED_FUNCTION = (1 << 8),
    // Generated code has detected an error (e.g. command/reply buffer overflow possibility)
    WRAPPER_ERROR = (1 << 9),
    // external library error (e.g. c stdlibrary)
    LIB_ERROR = (1 << 10),
    NET_ERROR = (1 << 11),
} RETURN_CODE_ENUM;

typedef uint16_t RETURN_CODE;

// Defining min/max for the above typedefs
#define uint8_t_MAX UCHAR_MAX
#define uint8_t_MIN 0
#define int8_t_MAX CHAR_MAX
#define int8_t_MIN CHAR_MIN
#define uint16_t_MAX USHRT_MAX
#define uint16_t_MIN 0
#define int16_t_MAX SHRT_MAX
#define int16_t_MIN 0
#define int32_t_MAX INT_MAX
#define int32_t_MIN INT_MIN
#define uint32_t_MAX UINT_MAX

#define uint32_t_MIN 0


#ifdef __cplusplus
}
#endif

#endif

