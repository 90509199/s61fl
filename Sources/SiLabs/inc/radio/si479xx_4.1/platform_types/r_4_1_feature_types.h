/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef FEATURE_TYPES_H_
#define FEATURE_TYPES_H_

///#include "common_types.h"
///#include "platform_options.h"

// forward declarations for using pointers to types:
typedef struct linker_context linker_context;
typedef struct linker_level_status linker_level_status;
typedef struct linker_time_status linker_time_status;

// Basically represents the different err bits from parts' status word.
typedef enum {
    // tuner api
    ERRT_API,
    ERRT_SW,
    ERRT_VD,
    ERRT_HOTWRN,
    ERRT_TMP,
    ERRT_CMD,
    ERRT_REP,
    ERRT_ARB,
    ERRT_NR,

    //hifi err?
    ERRT_AUDINT,

    // linker api
    ERRT_RFFE,
    ERRT_DSP,
} ERR_TYPE;

typedef enum {
    DC_2DT1F = 0,
    DC_1DT1T1F,
    // Two different packages for the tuner on 1T1D
    DC_1T1D_84,
    DC_1T1D_48,

    // Currently only used for BasicBoot project.
    DC_1T, // Single Global Eagle (audio SDK board)

    DC_NONE,
} DAUGHTERCARD_TYPE;

// Different modes of operation.
// Support will vary based on daughtercard.
typedef enum {
    //supported on some platforms
    // FM (HD), AM (HD)
    OPMODE_HD,
    // FM, AM (analog), with DAB
    OPMODE_DAB,

    OPMODE_INVALID,

} OPERATION_MODE;

// Some images support multiple radio types,
//   so these allow specifying unique modes in same image
typedef enum {
    RADIO_MODE_FM = 0x0,
    RADIO_MODE_AM = 0x1,
    RADIO_MODE_DAB = 0x2,
    RADIO_MODE_WB = 0x3,
    RADIO_MODE_NORADIO = 0x4,
    RADIO_MODE_UNKNOWN = 0x5
} RADIO_MODE_TYPE;

typedef enum BAND_LIMITS {
    FM_JAPAN_76_90,
    FM_STANDARD_875_1079,
    FM_WIDE_78_108,
    AM_DEFAULT,
    BAND_MAX,
} BAND_LIMITS;

typedef enum GENERAL_SERVICE_TYPE {
    //SERVICE_TYPE_FMRX_RDS = 0x00,

    SERVICE_TYPE_HD_AUDIO = 0x10,
    //SERVICE_TYPE_FMHD_SIS = 0x11,
    //SERVICE_TYPE_FMHD_PSD = 0x12,
    SERVICE_TYPE_FMHD_LOT = 0x13,

    SERVICE_TYPE_DAB_AUDIO = 0x20,
    SERVICE_TYPE_DAB_SLS = 0x21,

    // associated with the current audio service - no need to start seperately
    SERVICE_TYPE_DAB_DLS = 0x22,
    SERVICE_TYPE_DAB_MOT = 0x23,

    SERVICE_TYPE_DMB_VIDEO = 0x30,

    SERVICE_TYPE_ERR
} GENERAL_SERVICE_TYPE;

typedef enum BASEBAND_STATUS {
    BASEBAND_IDLE = 0x00,
    BASEBAND_INIT,
    BASEBAND_READY,
    BASEBAND_PROCESSING_COMMAND,
    BASEBAND_SCANNING,
    BASEBAND_FINALIZE,
    BASEBAND_FIRMWAREUPDATE,
    BASEBAND_ERROR
} BASEBAND_STATUS;

typedef enum AUDIO_MUTE {
    AUDIO_MUTE_NOT_MUTED = 0,
    AUDIO_MUTE_BOTH,
    AUDIO_MUTE_ERR,
} AUDIO_MUTE;

typedef enum AUDIO_MONO_STEREO_SWITCH {
    AUDIO_STEREO = 0,
    AUDIO_MONO = 1,
    AUDIO_ERR
} AUDIO_MONO_STEREO_SWITCH;

//******************************************************************
// Metrics for each mode
//******************************************************************
typedef struct {
    uint16_t attenuation_db;
    AUDIO_MUTE mute;
    bool is_mono;
} audio_state;

#define CHIP_TEMP_UNKNOWN 255
typedef struct {
    audio_state audio;
    // Chip temp ranges from -50 to 150, or CHIP_TEMP_UNKNOWN if it has not been read.
    int16_t chip_temps[OPTION_HAL_MAX_CHIPS];
} board_metrics;

typedef struct fm_metrics {
    uint16_t FREQUENCY_10KHZ;
    int8_t RSSI;
    int8_t SNR;
    uint8_t HD_LEVEL;
    uint8_t MULTIPATH;
    uint8_t VALID;
} fm_metrics;

typedef struct hd_mrc_metrics {
    // FMHD_DIVERSITY_STATUS
    bool acquired;
    uint8_t mode;
    uint8_t psmi;
    int8_t comb_weight;
} hd_mrc_metrics;

typedef struct fm_diversity_metrics {
    // tuner fm metrics
    fm_metrics diversity_metrics;

    // From fm_div_status command:
    // Which chipid is providing audio?
    uint8_t AUDIO;
    //0 -> not combined, 1 -> combining
    uint8_t COMB;
    // Cross correlation of audio for the two tuners in negative dB.
    // 0 is perfect correlation, smaller values are better correlation.
    uint8_t PHS_DIV_CORR;
} fm_diversity_metrics;

typedef struct am_metrics {
    uint16_t FREQUENCY_1KHZ;
    int8_t RSSI;
    int8_t SNR;
    uint8_t MODULATION_INDEX;
    uint8_t VALID;
} am_metrics;

// Only metrics from DEMOD, see FM metrics for TUNER metrics.
typedef struct hd_metrics {
    // HD_DIGRAD_STATUS
    bool ACQ;
    uint8_t DAAI;
    uint8_t CDNR;

    // 0=analog, 1=digital
    uint8_t SRC_DIG;
    // 0=remove logo, 1=display logo
    uint8_t HDLOGO;

    // Current Playing Audio Service
    uint8_t CURRENT_AUDIO_SERVICE;
    //The Service List Index for the Current Service - 0xFF if service list incomplete
    uint8_t CURRENT_AUDIO_SERVICE__LIST_INDEX;
} hd_metrics;

#define DAB_METRIC_AUDIO_SERVICE_LIST__INDEX_NOT_SET 0xFF

typedef struct dab_tuner_metrics {
    bool valid;
    // not set = 0xFF
    uint8_t freq_index;
    uint32_t freq_hz;
    uint8_t dab_detect;
    int8_t sqi;
    int8_t rssi;
    int8_t rssi_adj;
} dab_tuner_metrics;

typedef struct dab_mrc_metrics {
    // diversity tuner rsq status
    dab_tuner_metrics div_tuner;

    // combining metrics
    bool acquired;
    uint8_t dab_detect;
    uint8_t mode;
    int8_t comb_weight;
} dab_mrc_metrics;

typedef struct radioDNS_fm {
    uint16_t FREQUENCY_10KHZ;
    uint16_t PI;
    uint16_t GCC;
} radioDNS_fm;

typedef struct radioDNS_dab {
    uint8_t SCIDS;
    uint32_t SID;
    uint16_t EID;
    uint16_t GCC;
} radioDNS_dab;

//******************************************************************
// FMHD SIS / PSD - Structs
//******************************************************************
#if 0		/* 210630 SGsoft */	
#define FMHD_SIS_SLOGAN_BUFFER_SIZE 95
//need 12 + 3 to allow space for the -FM append option
#define FMHD_SIS_UNIVERSAL_SHORT_NAME_BUFFER_SIZE 15
#define FMHD_SIS_STATION_MESSAGE_BUFFER_SIZE 190
#define FMHD_SIS_STATION_NAME_LONG_BUFFER_SIZE 56
// need 4 + 3 to allow space for the -FM append option
#define FMHD_SIS_STATION_NAME_SHORT_BUFFER_SIZE 7
#define FMHD_SIS_STATION_ID_BUFFER_SIZE 4
#define FMHD_SIS_STATION_LOCATION_BUFFER_SIZE 4

typedef enum FMHD_SIS_PSD_ENCODING_TYPE {
    IEC_8859_1 = 0,
    UCS2_LITTLEENDIAN = 4,
    ENCODING_RESERVED = 0xFF
} FMHD_SIS_PSD_ENCODING_TYPE;

typedef enum FMHD_SIS_PRIORITY_TYPE {
    REGULAR = 0,
    HIGH = 1,
    PRIORITY_RESERVED = 0xFF
} FMHD_SIS_PRIORITY_TYPE;

typedef struct fmhd_sis_station_message {
    FMHD_SIS_PSD_ENCODING_TYPE SM_ENCODING;
    FMHD_SIS_PRIORITY_TYPE SM_PRIORITY;
    uint8_t SM_TEXT[FMHD_SIS_STATION_MESSAGE_BUFFER_SIZE];
} fmhd_sis_station_message;

typedef struct _fmhd_sis_universal_short_name {
    FMHD_SIS_PSD_ENCODING_TYPE USN_ENCODING;
    uint8_t USN_APPEND;
    // Displayed Universal Short Name
    uint8_t USN_TEXT[FMHD_SIS_UNIVERSAL_SHORT_NAME_BUFFER_SIZE];
} fmhd_sis_universal_short_name;

typedef struct fmhd_sis_slogan {
    FMHD_SIS_PSD_ENCODING_TYPE SLGN_ENCODING;
    // Displayed Station Slogan
    uint8_t SLGN_TEXT[FMHD_SIS_SLOGAN_BUFFER_SIZE];
} fmhd_sis_slogan;

//******************************************************************
#define FMHD_PSD_TEXT_TYPE_BUFFER_SIZE 0x7F
#define FMHD_PSD_TITLE_BUFFER_SIZE FMHD_PSD_TEXT_TYPE_BUFFER_SIZE
#define FMHD_PSD_ARTIST_BUFFER_SIZE FMHD_PSD_TEXT_TYPE_BUFFER_SIZE
#define FMHD_PSD_ALBUM_BUFFER_SIZE FMHD_PSD_TEXT_TYPE_BUFFER_SIZE
#define FMHD_PSD_GENRE_BUFFER_SIZE FMHD_PSD_TEXT_TYPE_BUFFER_SIZE

#define FMHD_PSD_COMMENT_SHORT_CONTENT_DESC_BUFFER_SIZE 0x7F
#define FMHD_PSD_COMMENT_ACTUAL_TEXT_BUFFER_SIZE 0x7F
#define FMHD_PSD_UFID_OWNER_ID_BUFFER_SIZE 0x80
#define FMHD_PSD_COMMERCIAL_PRICE_BUFFER_SIZE 0x7F
#define FMHD_PSD_COMMERCIAL_CONTACT_URL_BUFFER_SIZE 0x7F
#define FMHD_PSD_COMMERCIAL_SELLER_BUFFER_SIZE 0x7F
#define FMHD_PSD_COMMERCIAL_DESCRIPTION_BUFFER_SIZE 0x7F

#define FMHD_PSD_GENERIC_BUFFER_SIZE 0x7F

#define FMHD_ALERT_BUFFER_SIZE 395

#ifdef OPTION_PSD_FORMAT_ID3
typedef enum FMHD_PSD_FIELD_DATA_TYPE {
    PSD_UNKNOWN = 0,
    TITLE = 0x01,
    ARTIST = 0x02,
    ALBUM = 0x04,
    GENRE = 0x08,
    COMMENT = 0x10,
    UFID = 0x20,
    COMMERCIAL = 0x40
} FMHD_PSD_FIELD_DATA_TYPE;

typedef enum FMHD_PSD_SUBFIELD_DATA_TYPE {
    UNSET_SUBFIELD = 0,
    PRICE_STRING = 0x01,
    VALID_UNTIL = 0x02,
    CONTACT_URL = 0x04,
    RECEIVED_AS = 0x08,
    NAME_OF_SELLER = 0x10,
    DESCRIPTION = 0x20,
    //Reserved 0x40, 0x80
} FMHD_PSD_SUBFIELD_DATA_TYPE;
#else
typedef enum FMHD_PSD_FIELD_DATA_TYPE {
    TITLE = 0,
    ARTIST = 1,
    ALBUM = 2,
    GENRE = 3,
    COMMENT_LANGUAGE = 4,
    COMMENT_SHORT_DESC = 5,
    COMMENT_TEXT = 6,
    COMMERCIAL_PRICE_STRING = 8,
    COMMERCIAL_VALID_UNTIL = 9,
    COMMERCIAL_URL = 10,
    COMMERCIAL_RECEIVED_AS = 11,
    COMMERCIAL_NAME_OF_SELLER = 12,
    COMMERCIAL_DESCRIPTION = 13,
    UFID_OWNER = 14,
    UFID_OWNER_ID = 15
} FMHD_PSD_FIELD_DATA_TYPE;
#endif

typedef enum FMHD_BER_TYPE {
    HD_P1 = 1,
    HD_P2 = 2,
    HD_P3 = 3,
    HD_PIDS_BIT = 4,
    HD_PIDS_BLOCK = 5
} FMHD_BER_TYPE;

typedef struct fmhd_psd_generic_string {
    FMHD_PSD_FIELD_DATA_TYPE FIELD;
    FMHD_SIS_PSD_ENCODING_TYPE ENCODING;
    uint8_t LENGTH;
    uint8_t DATA[FMHD_PSD_GENERIC_BUFFER_SIZE];
} fmhd_psd_generic_string;

typedef struct fmhd_alert_string {
    FMHD_SIS_PSD_ENCODING_TYPE ENCODING;
    uint16_t LENGTH;
    uint8_t DATA[FMHD_ALERT_BUFFER_SIZE];
} fmhd_alert_string;

typedef struct fmhd_psd_ufid {
    FMHD_PSD_FIELD_DATA_TYPE FIELD;
    uint8_t SUBFIELD;
    FMHD_SIS_PSD_ENCODING_TYPE ENCODING;
    uint8_t LENGTH;
    uint8_t DATA[FMHD_PSD_UFID_OWNER_ID_BUFFER_SIZE];
} fmhd_psd_ufid;
#endif	/*0*/

//******************************************************************
// DAB - Time
//******************************************************************
typedef enum DAB_TIME_REGION_TYPE {
    LOCAL = 0,
    UTC = 1
} DAB_TIME_REGION_TYPE;

typedef struct dab_time {
    uint16_t YEAR;
    uint8_t MONTH;
    uint8_t DAYS;
    uint8_t HOURS;
    uint8_t MINUTES;
    uint8_t SECONDS;
} dab_time;

//******************************************************************
// DAB - Audio Info
//******************************************************************
typedef enum DAB_AUDIO_INFO_TYPE {
    DUAL = 0,
    MONO = 1,
    STEREO = 2,
    JOINT_STEREO = 3
} DAB_AUDIO_INFO_TYPE;

//******************************************************************
// DAB - DLS - Structs
//******************************************************************

#define DAB_DLS_BUFFER_SIZE 128

typedef enum DAB_DLS_ENCODING_TYPE {
    DLS_EBU = 0,
    DLS_UCS2 = 6,
    DLS_UTF8 = 0x0F,
    DLS_ENCODING_RESERVED = 0xFF
} DAB_DLS_ENCODING_TYPE;

typedef struct dab_dls_string {
    DAB_DLS_ENCODING_TYPE ENCODING;
    uint8_t TOGGLE;
    uint8_t LENGTH;
    uint8_t DATA[DAB_DLS_BUFFER_SIZE];
} dab_dls_string;

//******************************************************************
// Service List - FMHD
//******************************************************************
typedef struct fmhd_service_list_fast {
    bool MAIN_LIST_COMPLETE;
    uint8_t AUDIO_SERVICE_PRESENT_BITMASK;
} fmhd_service_list_fast;

//This combines audio/data ("service type") and "processing" fields into one type
typedef enum FMHD_SERVICE_TYPE {
    ST_AUDIO = 0,
    ST_UNSUPPORTED = 0xFE,
    ST_UNDEFINED = 0xFF
} FMHD_SERVICE_TYPE;

#define FMHD_MAX_NUMBER_AUDIO_SERVICES 8

// May be less than the max possible length to save memory
#define FMHD_SRVC_DISP_NAME_MAX_LENGTH 255
#define HD_SRVC_LIST_MAX_ITEMS 8
//#define FMHD_SRVC_NAME_BUFFER_TOTAL_SIZE (FMHD_SRVC_DISP_NAME_MAX_LENGTH * FMHD_SRVC_LIST_MAX_ITEMS)

typedef struct fmhd_service_list_element {
    FMHD_SERVICE_TYPE service_type;
    // Component ID
    uint16_t program_port_number;

    uint8_t encoding;
    uint8_t name_length;
    uint8_t name[FMHD_SRVC_DISP_NAME_MAX_LENGTH];
} fmhd_service_list_element;

typedef struct fmhd_service_list {
    uint8_t services_count;
    fmhd_service_list_element services[HD_SRVC_LIST_MAX_ITEMS];
} fmhd_service_list;

#ifdef OPTION_SERVICE_LIST_FULL
//TODO: finish this implementation
#define SRVC_PRVD_NAME_MAX_LENGTH 255
#define SRVC_DISP_NAME_MAX_LENGTH 255
#define EXPANDED_SERVICE_ID_MAX_LENGTH 247

typedef struct fmhd_service_list_element {
    uint16_t SERVICE_NUMBER;
    uint8_t PRIORITY;
    uint16_t SEQUENCE_NUMBER;
    uint8_t STATUS_FLAG;
    uint32_t RECEIVE_TIME;
    uint8_t SRVC_PRVD_NAME_ENCODING;
    uint8_t SRVC_PRVD_NAME_LENGTH;
    uint8_t SRVC_PRVD_NAME[SRVC_PRVD_NAME_MAX_LENGTH];
    uint8_t SRVC_DISP_NAME_ENCODING;
    uint8_t SRVC_DISP_NAME_LENGTH;
    uint8_t SRVC_DISP_NAME[SRVC_DISP_NAME_MAX_LENGTH];
} fmhd_service_list_element;

typedef struct fmhd_component_list_element {
    uint8_t COMP_TYPE;
    uint16_t PROGRAM_PORT_NUMBER;
    uint16_t PROGRAM_TYPE;
    //uint8_t RESERVED_PADDING;
    uint8_t PROCESSING;
    uint8_t PROG_PRIORITY;
    uint8_t ACCESS_RIGHTS;
    uint32_t MIME_HASH_VALUE;
    uint8_t IDENTIFIER_LENGTH;
    uint32_t PROVIDER_ID;
    uint32_t SERVICE_ID;
    uint8_t EXPANDED_SERVICE_ID[EXPANDED_SERVICE_ID_MAX_LENGTH];
} fmhd_component_list_element;
#endif

//******************************************************************
// Service List - DAB
//******************************************************************
#define DAB_SERVICE_LIST_SERVICE_LABEL__SIZE 16

// All ensembles are not used, but this represents the largest set of ensembles in the default frequency list
#define DAB_STANDARD__MAX_ENSEMBLES_EUROPE 36

// Currently no region has nearly this many ensembles - max could be DAB_STANDARD__MAX_ENSEMBLES_EUROPE
#define DAB_SERVICE_LIST__MAX_ENSEMBLES_FOR_LIST 10
#define DAB_SERVICE_LIST__MAX_SERVICES 104

#define DAB_SERVICE_LIST__MAX_NUMBER_OF_LINKED_FREQUENCIES 4
#define DAB_SERVICE_LIST__LINKED_INDEX_DEFAULT 0xFF
#define DAB_SERVICE_LIST__NUM_CANDIDATE_FREQS 12

typedef struct dab_service_info {
    uint32_t service_id;
    uint16_t component_id;
    uint16_t ensemble_id;
    uint8_t freq_index;
} dab_service_info;

typedef struct dab_string {
    uint8_t encoding;
    uint8_t name[DAB_SERVICE_LIST_SERVICE_LABEL__SIZE];
} dab_service_label;

typedef enum PROGRAM_TYPE {
    PT_NONE = 0,
    PT_NEWS,
    PT_CURRENT_AFFAIRS,
    PT_INFORMATION,
    PT_SPORT,
    PT_EDUCATION,
    PT_DRAMA,
    PT_ARTS,
    PT_SCIENCE,
    PT_TALK,
    PT_POP_MUSIC,
    PT_ROCK_MUSIC,
    PT_EASY_LISTENING,
    PT_LIGHT_CLASSICAL,
    PT_CLASSICAL,
    PT_OTHER_MUSIC,
    PT_WEATHER,
    PT_FINANCE,
    PT_CHILDRENS,
    PT_FACTUAL,
    PT_RELIGION,
    PT_PHONE_IN,
    PT_TRAVEL,
    PT_LEISURE,
    PT_JAZZ_AND_BLUES,
    PT_COUNTRY_MUSIC,
    PT_NATIONAL_MUSIC,
    PT_OLDIES_MUSIC,
    PT_FOLK_MUSIC,
    PT_DOCUMENTARY,
    PT_UNKNOWN, // NOT RECOGNIZED
} PROGRAM_TYPE;

// The possible component types stored in the service list, that can be started.
typedef enum DIGITAL_COMPONENT_TYPE {
    DCT_AUDIO_DAB,
    DCT_AUDIO_DABPLUS,
    DCT_AUDIO_UNKNOWN,
    DCT_DATA_DMB,
    DCT_DATA_VIDEO,
    DCT_UNKNOWN, // unsupported / error condition
} DIGITAL_COMPONENT_TYPE;

#endif // FEATURE_TYPES_H_
