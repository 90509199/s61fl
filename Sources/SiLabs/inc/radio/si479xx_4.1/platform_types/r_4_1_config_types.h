/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef CONFIG_TYPES_H
#define CONFIG_TYPES_H

///#include "common_types.h"

//******************************************************************
// linker types
//******************************************************************
typedef enum {
    LINKER_CODE_OK = 0,

    LINKER_CODE_FAIL = -20,
    LINKER_CODE_FAIL_PART,
    // Not enough delay buffer available to align sources
    LINKER_CODE_FAIL_DELAY_BUFFER,
    // Could not measure offset between sources.
    LINKER_CODE_FAIL_MEAS,
    LINKER_CODE_FAIL_CANCELLED,
} LINKER_RETCODE;

typedef enum {
    LINKER_INPUT_INVALID = -1,
    LINKER_INPUT_EXT_FM = 0,
    LINKER_INPUT_DEMOD1,
    LINKER_INPUT_DEMOD2,
    LINKER_INPUT_TEST_TONE,
} LINKER_INPUT_SOURCE;

typedef enum {
    LINKER_INPUT_MODE_NORMAL = 0,
    LINKER_INPUT_MODE_MONO_TEST
} LINKER_INPUT_MODE;

typedef enum {
    AUDIO_MODE0 = 0,
    AUDIO_MODE1,
    AUDIO_MODE2,
    AUDIO_MODE3,
    AUDIO_MODE4,
    AUDIO_MODE5,
    AUDIO_MODE6,
    AUDIO_MODE7,
} LINKER_AUDIO_MODE;

typedef enum {
    DELAY_TYPE_ABSOLUTE = 0,
    DELAY_TYPE_RELATIVE = 1,
} LINKER_AUDIO_DELAY_TYPE;

typedef enum {
    MEASUREMENT_COARSE = 0,
    MEASUREMENT_MEDIUM,
    MEASUREMENT_FINE,
} LINKER_OFFSET_MEASUREMENT_TYPE;

typedef enum {
    MEAS_INVALID = -1,
    MEAS_COARSE = 0,
    MEAS_MEDIUM,
    MEAS_FINE,
} LINKER_MEAS_TYPE;

//******************************************************************
// board config id types
//******************************************************************

//Uniquely identifies a 'Linker'
typedef struct {
    dc_config_chip_select ic;
} linker_id;

typedef enum LINKER_DEMOD_SELECT {
    DEMOD_SELECT_SINGLE = 0,
    DEMOD_SELECT1 = 1,
    DEMOD_SELECT2 = 2,
    // Only used for faster bootup
    DEMOD_SELECT_BOTH = 3,
    DEMOD_SELECT_LINKER,
    DEMOD_SELECT_INVALID
} LINKER_DEMOD_SELECT;

typedef struct {
    dc_config_chip_select ic;
    LINKER_DEMOD_SELECT select;
} demod_id;

typedef enum {
    TUNER_ID0 = 0,
    TUNER_ID1 = 1,
    TUNER_ID_INVALID
} TUNER_SUBCMD_ID;

//Uniquely identifies a 'Tuner'
typedef struct {
    dc_config_chip_select ic;
    TUNER_SUBCMD_ID id;
} tuner_id;

#endif
