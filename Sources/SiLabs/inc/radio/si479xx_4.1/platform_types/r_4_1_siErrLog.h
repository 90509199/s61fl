/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
/*==================================================================================================
 *                                    Copyright (c) 2012-2016
 *                                       Silicon Labs, Inc.
 *                                       400 W Cesar Chavez
 *                                        Austin, TX 78701
 *
 * This file contains proprietary, unpublished source code soley owned by Silicon Labs, Inc.
 *                                      All Rights Reserved
 *================================================================================================*/

/*! \file siErrLog.h
  \verbatim
  *==================================================================================================
  * FILE:  siErrLog.h
  *==================================================================================================
  * DESCRIPTION:
  *    Error logger.
  *==================================================================================================
  \endverbatim
 */

#ifndef SI_ERR_LOG_H
#define SI_ERR_LOG_H

/*--------------------------------------------------------------------------------------------------
  * Includes
  *------------------------------------------------------------------------------------------------*/
// #include "siTypes.h"

/*--------------------------------------------------------------------------------------------------
   * Constants
   *------------------------------------------------------------------------------------------------*/
#define ERROR_LOG_CONFIG_ENABLE_MASK 0x1
#define ERROR_LOG_CONFIG_ENABLE 0x0
#define ERROR_LOG_CONFIG_DISENABLE 0x1

#define ERROR_LOG_CONFIG_WRAP_MASK 0x2
#define ERROR_LOG_CONFIG_WRAP 0x0
#define ERROR_LOG_CONFIG_NO_WRAP 0x2

#define ERROR_LOG_STATE_WRAPPED_MASK 0x1
#define ERROR_LOG_STATE_NOT_WRAPPED 0x0
#define ERROR_LOG_STATE_WRAPPED 0x1

#define ERROR_LOG_STATE_FULL_MASK 0x2
#define ERROR_LOG_STATE_NOT_FULL 0x0
#define ERROR_LOG_STATE_FULL 0x2

#define ERROR_LOG_CODE_MASK 0xFF000000
#define ERROR_LOG_CODE_SHIFT 24

#define ERROR_LOG_DATA_MASK 0x00FFFFFF
#define ERROR_LOG_DATA_SHIFT 0

/*--------------------------------------------------------------------------------------------------
    * Error Codes
    *    0x00000000 through 0x7FFFFFFF are reserved for the SDK
    *    0x80000000 through 0xFFFFFFFF are reserved for Silicon Labs
    *------------------------------------------------------------------------------------------------*/
#define ERROR_INVALID_PORT_MANAGER_PIPE_HEADER 0x80000000
#define ERROR_INVALID_PORT_MANAGER_COMMAND_COUNT 0x81000000
#define ERROR_INVALID_PORT_MANAGER_PACKET_LENGTH 0x82000000
#define ERROR_INVALID_PORT_MANAGER_COMMAND 0x83000000
#define ERROR_INVALID_PORT_MANAGER_COMMAND_LENGTH 0x84000000
#define ERROR_PORT_MANAGER_COMMAND_REFUSED 0x85000000

#define ERROR_INVALID_HUB_MANAGER_PIPE_HEADER 0x86000000
#define ERROR_INVALID_HUB_MANAGER_COMMAND_COUNT 0x87000000
#define ERROR_INVALID_HUB_MANAGER_PACKET_LENGTH 0x88000000
#define ERROR_INVALID_HUB_MANAGER_COMMAND 0x89000000
#define ERROR_INVALID_HUB_MANAGER_COMMAND_LENGTH 0x8A000000

#define ERROR_INVALID_AUD_PROC_MANAGER_PIPE_HEADER 0x8B000000
#define ERROR_INVALID_AUD_PROC_MANAGER_COMMAND_COUNT 0x8C000000
#define ERROR_INVALID_AUD_PROC_MANAGER_PACKET_LENGTH 0x8D000000
#define ERROR_INVALID_AUD_PROC_MANAGER_COMMAND 0x8E000000
#define ERROR_INVALID_AUD_PROC_MANAGER_COMMAND_LENGTH 0x8F000000

#define ERROR_OFF_BOUNDARY_MEMORY_ACCESS 0x90000000
#define ERROR_DIVIDE_BY_ZERO 0x91000000
#define ERROR_SPURIOUS_SWI 0x92000000
#define ERROR_FRAME_OVER_RUN 0x93000000
#define ERROR_I2S_LOST_CLOCK 0x94000000
#define ERROR_I2S_RX_OVER_RUN 0x95000000
#define ERROR_I2S_RX_UNDER_RUN 0x96000000
#define ERROR_I2S_TX_OVER_RUN 0x97000000
#define ERROR_I2S_TX_UNDER_RUN 0x98000000
#define ERROR_READ_NODE_UNDER_RUN 0x99000000
#define ERROR_INCOMPATIBLE_TOPOLOGY 0x9A000000
#define ERROR_AUDIO_PROCESS_OVER_RUN 0x9B000000
#define ERROR_FATAL_EXCEPTION 0x9C000000
#define ERROR_NON_FATAL_EXCEPTION 0x9D000000
#define ERROR_PIPE_IS_IN_USE 0x9E000000

#define ERROR_INVALID_WAVEFORM_FORMAT 0x9F000000
#define ERROR_INVALID_WAVEFORM_SAMPLE_COUNT 0xA0000000

#define ERROR_AUD_PROC_CONSTRUCTOR_MODULE_FAIL 0xA1000000

#define ERROR_INVALID_GPNUM 0xA2000000

/*--------------------------------------------------------------------------------------------------
     * Macros
     *------------------------------------------------------------------------------------------------*/
#define LOG_ERR(ErrCode) siLogErr(ErrCode)

#endif // SI_ERR_LOG_H
