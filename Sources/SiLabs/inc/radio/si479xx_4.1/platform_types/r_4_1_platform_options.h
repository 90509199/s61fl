/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

//  Compile options for the overall SDK project

#ifndef PLATFORM_OPTIONS_H_
#define PLATFORM_OPTIONS_H_

///#include "platform_selector.h"

//Section for choosing platform settings/options
//**************************************
/*
The following options should either be #define in order to select the option, unless otherwise stated.

IMPORTANT:
Most of these options are carryover from past work, and haven't been tested.
Please refer to the Sample Code Guide for the most up to date information
regarding the project state.

-) Image Type (select one):  OPTION_IMAGETYPE_HD |  OPTION_IMAGETYPE_DAB
     * Depending on the options selected and MMI complexity,
        a single image for all modes may not fit in the MCU code space, this option enables basic mode selection for code inclusion

     Supported:
     - OPTION_IMAGETYPE_HD: FMHD (phase + HD diversity), and AMHD radio modes implementations for Firmware_API_Manager.h .
     - OPTION_IMAGETYPE_DAB : FM/AM analog and DAB states, with additional linking examples in the console app.

-) Boot Mode (select one): OPTION_BOOT_FROM_HOST | OPTION_BOOT_FROM_FLASH
Supported:
     - OPTION_BOOT_FROM_HOST: this option selects the initialization code which expects the host MCU to load all firmware images
     - OPTION_BOOT_FROM_FLASH: this option selects the initialization code which expects all
            necessary firmware images be loaded from external NVMSPI connected flash (not applicable for ROM00)


-) OPTION_CONVERT_BIG_ENDIAN - If selected platform architecture is BIG_ENDIAN, include this option because the output of the si46xx is little endian and must be converted

-) OPTION_RDS_DECODE_ADVANCED or OPTION_RDS_DECODE_BASIC: RDS decode mode basic/advanced (select one or do not define to disable) 
     - Determines if the basic or advanced PS/RT RDS display functions are used
     - OPTION_RDS_DECODE_ADVANCED:The advanced functions attempt to detect errors and only display complete messages
     - OPTION_RDS_DECODE_BASIC: The basic functions are closely tied to the recommendations in the RBDS specification and are faster to update but more error prone

-) OPTION_DECODE_SIS - define this option to include the HD Radio SIS data handler
-) OPTION_DECODE_PSD - define this option to include the HD Radio PSD data handler
-) OPTION_PSD_FORMAT_ID3 - define this option to use the HD radio PSD in ID3 format, note: this requires a larger reply buffer to work properly and should not be selected unless modifying the SDK source
-) OPTION_DECODE_DLS - define this option to include the DAB DLS data handler


-) OPTION_DAB_SERVICE_LIST_SCVLIST_MS - the value for this option sets the amount of time, in ms, where the software will exit a failed DAB service list acquisition.  Example: 20000
-) OPTION_DAB_SERVICE_LIST_TIMEOUT_CALLBACK_RATE_MS - the value for this option sets the amount of time, in ms, between callbacks to the MMI for UI update, during the DAB scan/tune process.  Example: 100
-) OPTION_FM_TIMEOUT_SEEK_MS - the value for this option sets the amount of time, in ms, where the software will exit a failed FM Seek.  Example: 10000
-) OPTION_FM_SEEK_CALLBACK_RATE_MS - the value for this option sets the amount of time, in ms, between callbacks to the MMI for UI update, during the FM Seek process.  Example: 100
-) OPTION_DECODE_HD_ALERTS - define this option to include the HD Radio Alert handler.
-) OPTION_FMHD_SEEK_OFDM_SAMPLE_NUMBER - the value for this options sets the FM_RSQ_HD_DETECTION property SAMPLES field.  See AN649 for explanation of this property.
-) OPTION_RADIODNS - define this option to include the SDK code necessary to generate a RadioDNS address from FM-RDS and/or DAB
-) OPTION_DAB_LINK_DISQUALIFIER_CNR_SETTLING_TIME_MS - the allowed time for the CNR metric to settle before checking it for the below threshold
-) OPTION_DAB_LINK_DISQUALIFIER_CNR_THRESHOLD - this is the CNR level at which a potential DAB link will be excluded from the candidates which attempt acquisition
*/

/*
Please refer to the Sample Code Guide for the most up to date information
regarding project status  +  supported compile options.

Most have not been tested.
*/
#if defined(PLATFORM_DIGEN)		/* 210510 SGsoft */	

#define OPTION_IMAGETYPE_DAB

#define OPTION_BOOT_FROM_FLASH

//#define OPTION_BOOT_FROM_HOST

#define OPTION_DC_TYPE DC_1T

#define OPTION_AUDIO_MODE ACFG_TUNERONLY
//#define OPTION_AUDIO_MODE ACFG_HIFI_REF_DESIGN				/* 210516 SGsoft */	

#define OPTION_HDMRC_OFF

// enable DAB MRC
#define OPTION_DABMRC_OFF

// POWER_UP HD rate (744 or 650)
#define OPTION_HD_IQRATE 744
// POWER_UP HD IQSLOT (16 or 18)
#define OPTION_HD_IQSLOT 16

// Affects POWER_UP:AFS option, and some reference audio design stuff.
// Must be 48000 or 44100
#ifdef OPTION_IMAGETYPE_HD
#define OPTION_AUDIO_RATE 44100
#else
#define OPTION_AUDIO_RATE 48000
#endif

#ifndef OPTION_HAL_MAX_CHIPS
#define OPTION_HAL_MAX_CHIPS 1/*5*/									/* 210516 SGsoft */	
#endif

#ifndef OPTION_HAL_SPI_CLKRATE_HZ
#define OPTION_HAL_SPI_CLKRATE_HZ (25000000)
#endif

// Defining this will make the EVB use i2c instead of SPI to communicate with tuners.
#define OPTION_BUS_SPI						/* 210510 SGsoft */	
//#define OPTION_BUS_I2C
#ifdef OPTION_BUS_I2C		/* 210510 SGsoft */	
#define OPTION_BUS_I2C__IC0_ADDR (0xD0)
#endif	/*OPTION_BUS_I2C*/

// If using i2c, these options provide custom i2c addresses.
// Valid values : (from Si479xxapi.h):
//typedef enum SI479xx_I2C_addr_enum {
//    UseBusMuxAddress = 0,
//    Si46xx_Addr00 = 0x64,
//    Si46xx_Addr01 = 0x65,
//    Si46xx_Addr10 = 0x66,
//    Si46xx_Addr11 = 0x67,
//    Si479xx_Addr00 = 0x68,
//    Si479xx_Addr01 = 0x69,
//    Si479xx_Addr10 = 0x6A,
//    Si479xx_Addr11 = 0x6B,
//    Si4796x_Addr00 = 0x6C,
//    Si4796x_Addr01 = 0x6D,
//    Si4796x_Addr10 = 0x6E,
//    Si4796x_Addr11 = 0x6F,
//    NoI2CAddr = 0xFF
//};

// ICX_ADDR should be 0x00 if using the daughtercard,
//  with the desired address in ICX_ADDR_OVERRIDE instead.

// If using own hardware with i2c, should set ICX_ADDR to desired address,
//  and ICX_ADDR_OVERRIDE will be ignored.
// #define OPTION_BUS_I2C__IC0_ADDR (0x00)
// #define OPTION_BUS_I2C__IC0_ADDR_OVERRIDE (0x6D)
// #define OPTION_BUS_I2C__IC1_ADDR (0x00)
// #define OPTION_BUS_I2C__IC1_ADDR_OVERRIDE (0x6C)

#if 1
// Data decoding options
#define OPTION_RDS_DECODE_ADVANCED
// #define OPTION_RDS_DECODE_BASIC

#define OPTION_DECODE_SIS
#define OPTION_DECODE_PSD
//#define OPTION_PSD_FORMAT_ID3
#define OPTION_DECODE_HD_ALERTS
#define OPTION_RADIODNS
#define OPTION_DECODE_DLS

// #define OPTION_HANDLE_ADVANCED_SERVICES

//The total time to allow for DAB Service List Update before timeout
#define OPTION_DAB_SERVICE_LIST_SCVLIST_MS 5000
//The amount of time between dab bandscan callbacks to the MMI
#define OPTION_DAB_SERVICE_LIST_TIMEOUT_CALLBACK_RATE_MS 100

//The total time to allow for DAB Service Start before Timeout
#define OPTION_DAB_SERVICE_START_TIMEOUT_MS 1200
//The amount of time between attempting to start a service
#define OPTION_DAB_SERVICE_START_ATTEMPT_RATE_MS 20

#define OPTION_FM_TIMEOUT_SEEK_MS 30000
//The amount of time between fm/fmhd seek callbacks to the MMI
#define OPTION_FM_SEEK_CALLBACK_RATE_MS 10

#define OPTION_AM_TIMEOUT_SEEK_MS 30000
//The amount of time between fm/fmhd seek callbacks to the MMI
#define OPTION_AM_SEEK_CALLBACK_RATE_MS 100

#define OPTION_FMHD_SEEK_OFDM_SAMPLE_NUMBER 0x20

#define OPTION_DAB_USE_FAST_DETECT

// Code timeout checks
#define OPTION_DAB_VALID_TUNE_TIMEOUT_MS 100

// Property values
#define OPTION_DAB_VALID_SYNC_TIME_MS 1200
#define OPTION_DAB_VALID_ACQ_TIME_MS 2000

#define OPTION_DAB_LINK_DISQUALIFIER_CNR_SETTLING_TIME_MS 20
#define OPTION_DAB_LINK_DISQUALIFIER_CNR_THRESHOLD 5

// Defines if persistent storage (state files) should be used.
#ifndef OPTION_STATEFUL
#define OPTION_STATEFUL 1
#endif // !OPTION_STATEFUL
#endif	/*0*/

#elif defined(PLATFORM_SEJIN)		/* 210427 SGsoft */

#define OPTION_IMAGETYPE_DAB
//#define OPTION_IMAGETYPE_HD		/* 210503 SGsoft */

#define OPTION_BOOT_FROM_HOST
// #define OPTION_BOOT_FROM_FLASH

#define OPTION_DC_TYPE DC_1T

#define OPTION_AUDIO_MODE ACFG_TUNERONLY
//#define OPTION_AUDIO_MODE ACFG_HIFI_REF_DESIGN

#define OPTION_HDMRC_OFF

// enable DAB MRC
#define OPTION_DABMRC_OFF

// POWER_UP HD rate (744 or 650)
#define OPTION_HD_IQRATE 744
// POWER_UP HD IQSLOT (16 or 18)
#define OPTION_HD_IQSLOT 16

// Affects POWER_UP:AFS option, and some reference audio design stuff.
// Must be 48000 or 44100
#ifdef OPTION_IMAGETYPE_HD
#define OPTION_AUDIO_RATE 44100
#else
#define OPTION_AUDIO_RATE 48000
#endif

#ifndef OPTION_HAL_MAX_CHIPS
#define OPTION_HAL_MAX_CHIPS 5
#endif

#ifndef OPTION_HAL_SPI_CLKRATE_HZ
#define OPTION_HAL_SPI_CLKRATE_HZ (25000000)
#endif

// Defining this will make the EVB use i2c instead of SPI to communicate with tuners.
#define OPTION_BUS_SPI
//#define OPTION_BUS_I2C

// If using i2c, these options provide custom i2c addresses.
// Valid values : (from Si479xxapi.h):
//typedef enum SI479xx_I2C_addr_enum {
//    UseBusMuxAddress = 0,
//    Si46xx_Addr00 = 0x64,
//    Si46xx_Addr01 = 0x65,
//    Si46xx_Addr10 = 0x66,
//    Si46xx_Addr11 = 0x67,
//    Si479xx_Addr00 = 0x68,
//    Si479xx_Addr01 = 0x69,
//    Si479xx_Addr10 = 0x6A,
//    Si479xx_Addr11 = 0x6B,
//    Si4796x_Addr00 = 0x6C,
//    Si4796x_Addr01 = 0x6D,
//    Si4796x_Addr10 = 0x6E,
//    Si4796x_Addr11 = 0x6F,
//    NoI2CAddr = 0xFF
//};

// ICX_ADDR should be 0x00 if using the daughtercard,
//  with the desired address in ICX_ADDR_OVERRIDE instead.

// If using own hardware with i2c, should set ICX_ADDR to desired address,
//  and ICX_ADDR_OVERRIDE will be ignored.
// #define OPTION_BUS_I2C__IC0_ADDR (0x00)
// #define OPTION_BUS_I2C__IC0_ADDR_OVERRIDE (0x6D)
// #define OPTION_BUS_I2C__IC1_ADDR (0x00)
// #define OPTION_BUS_I2C__IC1_ADDR_OVERRIDE (0x6C)

#if 1
// Data decoding options
#define OPTION_RDS_DECODE_ADVANCED
// #define OPTION_RDS_DECODE_BASIC

#define OPTION_DECODE_SIS
#define OPTION_DECODE_PSD
//#define OPTION_PSD_FORMAT_ID3
#define OPTION_DECODE_HD_ALERTS
#define OPTION_RADIODNS
#define OPTION_DECODE_DLS

// #define OPTION_HANDLE_ADVANCED_SERVICES

//The total time to allow for DAB Service List Update before timeout
#define OPTION_DAB_SERVICE_LIST_SCVLIST_MS 5000
//The amount of time between dab bandscan callbacks to the MMI
#define OPTION_DAB_SERVICE_LIST_TIMEOUT_CALLBACK_RATE_MS 100

//The total time to allow for DAB Service Start before Timeout
#define OPTION_DAB_SERVICE_START_TIMEOUT_MS 1200
//The amount of time between attempting to start a service
#define OPTION_DAB_SERVICE_START_ATTEMPT_RATE_MS 20

#define OPTION_FM_TIMEOUT_SEEK_MS 30000
//The amount of time between fm/fmhd seek callbacks to the MMI
#define OPTION_FM_SEEK_CALLBACK_RATE_MS 10

#define OPTION_AM_TIMEOUT_SEEK_MS 30000
//The amount of time between fm/fmhd seek callbacks to the MMI
#define OPTION_AM_SEEK_CALLBACK_RATE_MS 100

#define OPTION_FMHD_SEEK_OFDM_SAMPLE_NUMBER 0x20

#define OPTION_DAB_USE_FAST_DETECT

// Code timeout checks
#define OPTION_DAB_VALID_TUNE_TIMEOUT_MS 100

// Property values
#define OPTION_DAB_VALID_SYNC_TIME_MS 1200
#define OPTION_DAB_VALID_ACQ_TIME_MS 2000

#define OPTION_DAB_LINK_DISQUALIFIER_CNR_SETTLING_TIME_MS 20
#define OPTION_DAB_LINK_DISQUALIFIER_CNR_THRESHOLD 5

// Defines if persistent storage (state files) should be used.
#ifndef OPTION_STATEFUL
#define OPTION_STATEFUL 1
#endif // !OPTION_STATEFUL
#endif	/*0*/

#elif defined(PLATFORM_EVB) || defined(PLATFORM_PI)

#if !defined(OPTION_IMAGETYPE_HD) && !defined(OPTION_IMAGETYPE_DAB)
// Currently supported image types (not supported by all daughtercards):
// For regions (like USA) that have HD digital radio
// #define OPTION_IMAGETYPE_HD
// For regions (like EU) that have DAB digital radio
#define OPTION_IMAGETYPE_DAB
#endif // !defined(OPTION_IMAGETYPE_HD) && !defined(OPTION_IMAGETYPE_DAB)

#if !defined(OPTION_BOOT_FROM_HOST) && !defined(OPTION_BOOT_FROM_FLASH)
//currently supported boot types: OPTION_BOOT_FROM_HOST , OPTION_BOOT_FROM_FLASH
#define OPTION_BOOT_FROM_HOST
// #define OPTION_BOOT_FROM_FLASH
#endif // !defined(OPTION_BOOT_FROM_HOST) && !defined(OPTION_BOOT_FROM_FLASH)

#if !defined(PLATFORM_EVB) && !defined(OPTION_DC_TYPE)
// Used to define target radio hardware, when not using PLATFORM_EVB (which uses baseboard EEPROM definitions...)
// Possible options: see enum DAUGHTERCARD_TYPE
#define OPTION_DC_TYPE DC_2DT1F
// #define OPTION_DC_TYPE DC_1T1D_84
#endif

#if !defined(OPTION_AUDIO_MODE)
// This option determines which audio mode is used (tuner only, audio_hub port-port connections, or hifi 6 channel reference design)
// Some daughtercards are only capable of certain modes. (e.g. 1T1D 48pin should only use tuner only audio).
// Possible values: see enum AUDIO_CFG_TYPE
#define OPTION_AUDIO_MODE ACFG_HIFI_REF_DESIGN
// #define OPTION_AUDIO_MODE ACFG_TUNERONLY
#endif

#define OPTION_HDMRC_OFF
// NOTE: if using HD MRC, currently must use tuner-only audio.
// The code primarily uses IC0, which cannot currently be loaded with a hifi image as well as the HD MRC image.
#if !defined(OPTION_HDMRC_OFF)
#define OPTION_AUDIO_MODE ACFG_TUNERONLY
#endif

// enable DAB MRC
#define OPTION_DABMRC_OFF

// POWER_UP HD rate (744 or 650)
#define OPTION_HD_IQRATE 744
// POWER_UP HD IQSLOT (16 or 18)
#define OPTION_HD_IQSLOT 16

// Affects POWER_UP:AFS option, and some reference audio design stuff.
// Must be 48000 or 44100
#ifdef OPTION_IMAGETYPE_HD
#define OPTION_AUDIO_RATE 44100
#else
#define OPTION_AUDIO_RATE 48000
#endif

#ifndef OPTION_HAL_MAX_CHIPS
#define OPTION_HAL_MAX_CHIPS 5
#endif

#ifndef OPTION_HAL_SPI_CLKRATE_HZ
#define OPTION_HAL_SPI_CLKRATE_HZ (25000000)
#endif

// Defining this will make the EVB use i2c instead of SPI to communicate with tuners.
#define OPTION_BUS_SPI
//#define OPTION_BUS_I2C

// If using i2c, these options provide custom i2c addresses.
// Valid values : (from Si479xxapi.h):
//typedef enum SI479xx_I2C_addr_enum {
//    UseBusMuxAddress = 0,
//    Si46xx_Addr00 = 0x64,
//    Si46xx_Addr01 = 0x65,
//    Si46xx_Addr10 = 0x66,
//    Si46xx_Addr11 = 0x67,
//    Si479xx_Addr00 = 0x68,
//    Si479xx_Addr01 = 0x69,
//    Si479xx_Addr10 = 0x6A,
//    Si479xx_Addr11 = 0x6B,
//    Si4796x_Addr00 = 0x6C,
//    Si4796x_Addr01 = 0x6D,
//    Si4796x_Addr10 = 0x6E,
//    Si4796x_Addr11 = 0x6F,
//    NoI2CAddr = 0xFF
//};

// ICX_ADDR should be 0x00 if using the daughtercard,
//  with the desired address in ICX_ADDR_OVERRIDE instead.

// If using own hardware with i2c, should set ICX_ADDR to desired address,
//  and ICX_ADDR_OVERRIDE will be ignored.
// #define OPTION_BUS_I2C__IC0_ADDR (0x00)
// #define OPTION_BUS_I2C__IC0_ADDR_OVERRIDE (0x6D)
// #define OPTION_BUS_I2C__IC1_ADDR (0x00)
// #define OPTION_BUS_I2C__IC1_ADDR_OVERRIDE (0x6C)

#if 0
// Data decoding options
#define OPTION_RDS_DECODE_ADVANCED
// #define OPTION_RDS_DECODE_BASIC

#define OPTION_DECODE_SIS
#define OPTION_DECODE_PSD
//#define OPTION_PSD_FORMAT_ID3
#define OPTION_DECODE_HD_ALERTS
#define OPTION_RADIODNS
#define OPTION_DECODE_DLS

// #define OPTION_HANDLE_ADVANCED_SERVICES

//The total time to allow for DAB Service List Update before timeout
#define OPTION_DAB_SERVICE_LIST_SCVLIST_MS 5000
//The amount of time between dab bandscan callbacks to the MMI
#define OPTION_DAB_SERVICE_LIST_TIMEOUT_CALLBACK_RATE_MS 100

//The total time to allow for DAB Service Start before Timeout
#define OPTION_DAB_SERVICE_START_TIMEOUT_MS 1200
//The amount of time between attempting to start a service
#define OPTION_DAB_SERVICE_START_ATTEMPT_RATE_MS 20
#endif	/*0*/

#define OPTION_FM_TIMEOUT_SEEK_MS 30000
//The amount of time between fm/fmhd seek callbacks to the MMI
#define OPTION_FM_SEEK_CALLBACK_RATE_MS 10

#define OPTION_AM_TIMEOUT_SEEK_MS 30000
//The amount of time between fm/fmhd seek callbacks to the MMI
#define OPTION_AM_SEEK_CALLBACK_RATE_MS 100

#if 0
#define OPTION_FMHD_SEEK_OFDM_SAMPLE_NUMBER 0x20

#define OPTION_DAB_USE_FAST_DETECT

// Code timeout checks
#define OPTION_DAB_VALID_TUNE_TIMEOUT_MS 100

// Property values
#define OPTION_DAB_VALID_SYNC_TIME_MS 1200
#define OPTION_DAB_VALID_ACQ_TIME_MS 2000

#define OPTION_DAB_LINK_DISQUALIFIER_CNR_SETTLING_TIME_MS 20
#define OPTION_DAB_LINK_DISQUALIFIER_CNR_THRESHOLD 5

// Defines if persistent storage (state files) should be used.
#ifndef OPTION_STATEFUL
#define OPTION_STATEFUL 1
#endif // !OPTION_STATEFUL
#endif	/*0*/

#endif // PLATFORM_EVB

#define OPTION_INCLUDE_MODE_DAB
#define OPTION_INCLUDE_MODE_FMHD
#define OPTION_INCLUDE_MODE_AMHD
#define OPTION_INCLUDE_MODE_AM

#if defined(OPTION_BOOT_FROM_HOST)		/* 210427 SGsoft */
#define SI479XX_CMD_BUFFER_LENGTH 	(4100 /*+ 32*/) 
#define SI479XX_RPY_BUFFER_LENGTH 	256/*60*/
#else
#define SI479XX_CMD_BUFFER_LENGTH 	256
#define SI479XX_RPY_BUFFER_LENGTH 	256
#endif	/*OPTION_BOOT_FROM_HOST*/

#endif // PLATFORM_OPTIONS_H_
