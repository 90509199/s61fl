/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef OSAL_MISC_H_
#define OSAL_MISC_H_

#ifdef _r_4_1_misc_h_global_		/* 210426 SGsoft */
#define R_4_1_MISC_EXT
#else
#define R_4_1_MISC_EXT	extern
#endif	/*_r_4_1_misc_h_global_*/

#if 0
#ifdef __cplusplus
extern "C" {
#endif
#endif	/*0*/

#include <stdbool.h>
#include <stddef.h>
///#include "common_types.h"

/**
 * @brief Sleeps for roughly number of milliseconds supplied.
 *
 * @return true when user has typed a character.
 */
R_4_1_MISC_EXT void os_delay(uint32_t delay_ms);

/**
 * @brief Returns increasing number of milliseconds , for timing.
 */
R_4_1_MISC_EXT uint32_t os_ticks_ms();

/**
 * @brief Performs a non-blocking check for user input.
 *
 * @return true when user has typed a character.
 */
R_4_1_MISC_EXT bool osio_has_user_input();

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

/**
 * @brief Reads user-input til end of line (<ENTER>, '\n').
 *
 * @param line : caller allocated buffer for line contents;
 *               Will always be null-terminated.
 * @param max_len : allocated size of line
 * @return number of characters read
 */
R_4_1_MISC_EXT size_t osio_get_line(char* line, size_t max_len);

#ifdef __cplusplus
}
#endif

#endif // OSAL_MISC_H_
