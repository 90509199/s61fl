/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
 
#ifndef si479xx_flash_utility_api_h_
#define si479xx_flash_utility_api_h_

#ifdef _r_4_1_si479xx_flash_utility_api_h_global_		/* 210419 SGsoft */
#define R_4_1_SI479XX_FU_API_EXT
#else
#define R_4_1_SI479XX_FU_API_EXT	extern
#endif	/*_r_4_1_si479xx_flash_utility_api_h_global_*/

///#include "si479xx_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#if 0		/* 210512 SGsoft */	
#ifdef _r_4_1_si479xx_flash_utility_api_h_global_		/* 210419 SGsoft */
const uint16_t SI479XX_FLASH_UTIL_PROPS[10] = {
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ADDR,
    SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_ADDR,
};
#else
// List of valid property addresses defined by this API. (mainly for debug)
extern const uint16_t SI479XX_FLASH_UTIL_PROPS[10];
#endif	/*_r_4_1_si479xx_flash_utility_api_h_global_*/
#endif	/*0*/

/*
 * Command: SI479XX_FLASH_UTIL_check_crc32
 * 
 * Description:
 *   LOAD_IMG subcommand: Verify chunk of bytes matches the CRC32
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  addr: Starting address on flash of the block to read
 *  size: Number of bytes to read. If 0 loads to the end of boot_img.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  crc32_calc: CRC32 computed with data read from flash.
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_check_crc32__command(uint8_t icNum,uint32_t addr, uint32_t size);



R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_check_crc32__reply(uint8_t icNum, uint32_t* crc32_calc);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_check_crc32(uint8_t icNum, uint32_t  addr, uint32_t  size, uint32_t*  crc32_calc);


/*
 * Command: SI479XX_FLASH_UTIL_get_prop
 * 
 * Description:
 *   LOAD_IMG subcommand: Retrieve a property's value; The value will either be the default or the value set with SET_PROP.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  prop_id: Property ID
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  prop_val: Property value
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_get_prop__command(uint8_t icNum,uint16_t prop_id);



R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_get_prop__reply(uint8_t icNum, uint16_t* prop_val);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_get_prop(uint8_t icNum, uint16_t  prop_id, uint16_t*  prop_val);



typedef struct SI479XX_FLASH_UTIL_set_prop_propset__data
{
	uint16_t PROP_ID;
	uint16_t PROP_VAL;
} SI479XX_FLASH_UTIL_set_prop_propset__data;
/*
 * Command: SI479XX_FLASH_UTIL_set_prop
 * 
 * Description:
 *   LOAD_IMG subcommand: Sets a property's value.  NOTE: Two extra bytes of 0s, here as SET_PROP:PROP_END:END_0X0000, need to be sent at the end, following the last PROP_VAL[15:8], to indicate the end of property setting.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  propset: (No Description)
 *  end_0x0000: Two extra bytes of 0s need to be sent at the end, following the last PROP_VAL[15:8], to indicate the end of property setting.
 *  propset_length (input): #elems in 'propset' array.
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_set_prop__command(uint8_t icNum, SI479XX_FLASH_UTIL_set_prop_propset__data* propset, uint16_t propset_length);

// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_set_prop(uint8_t icNum, SI479XX_FLASH_UTIL_set_prop_propset__data*  propset, uint16_t  propset_length);


/*
 * Command: SI479XX_FLASH_UTIL_read_block
 * 
 * Description:
 *   Read a block of bytes from the flash
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  addr: Starting address on flash of the block to read
 *  size: Number of bytes to read.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: Flash data byte
 *  size (input): #elems in 'data' array.
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_read_block__command(uint8_t icNum,uint32_t addr, uint32_t size);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_read_block__reply(uint8_t icNum, uint8_t* data, uint16_t size);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_read_block(uint8_t icNum, uint32_t  addr, uint32_t  arg_size, uint8_t*  data, uint16_t  rep_size);


/*
 * Command: SI479XX_FLASH_UTIL_write_sector_4096
 * 
 * Description:
 *   LOAD_IMG subcommand: WRITE_SECTOR_4096
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sector: Flash Sector
 *  data: Flash data byte
 *  data_length (input): #elems in 'data' array.
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_write_sector_4096__command(uint8_t icNum,uint16_t sector, uint8_t* data, uint16_t data_length);



// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_write_sector_4096(uint8_t icNum, uint16_t  sector, uint8_t*  data, uint16_t  data_length);


/*
 * Command: SI479XX_FLASH_UTIL_erase_sector
 * 
 * Description:
 *   LOAD_IMG subcommand: ERASE_SECTOR
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  addr: Starting address on flash of the sector to erase
 *  0xc0: Must be hex value 0xc0
 *  0xde: Must be hex value 0xde
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_erase_sector__command(uint8_t icNum,uint32_t addr);



// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_erase_sector(uint8_t icNum, uint32_t  addr);


/*
 * Command: SI479XX_FLASH_UTIL_erase_chip
 * 
 * Description:
 *   LOAD_IMG subcommand
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  0xde: Must be hex value 0xde
 *  0xc0: Must be hex value 0xc0
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_erase_chip__command(uint8_t icNum);



// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_erase_chip(uint8_t icNum);


/*
 * Command: SI479XX_FLASH_UTIL_write_block
 * 
 * Description:
 *   LOAD_IMG subcommand: Write a block of bytes to the flash. All the bytes on flash that are written must  have been previously erased to 0xFF  with the ERASE_CHIP or  ERASE_SECTOR subcommands.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  addr: Starting address on flash of the block to write
 *  size: Number of bytes to write.
 *  data: Flash data byte
 *  0x0c: Must be hex value 0x0c
 *  0xed: Must be hex value 0xed
 * 
 */
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_write_block__command(uint8_t icNum,uint32_t addr, uint32_t size, uint8_t* data);



// Refer to __command + __reply documentation.
R_4_1_SI479XX_FU_API_EXT RETURN_CODE SI479XX_FLASH_UTIL_write_block(uint8_t icNum, uint32_t  addr, uint32_t  size, uint8_t*  data);

#ifdef __cplusplus
} // extern C
#endif

#endif
