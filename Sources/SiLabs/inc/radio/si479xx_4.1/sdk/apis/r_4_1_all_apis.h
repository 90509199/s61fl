/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef ALL_APIS_H_
#define ALL_APIS_H_

#ifdef _r_4_1_all_apis_h_global_		/* 210419 SGsoft */
#define R_4_1_ALL_APIS_EXT
#else
#define R_4_1_ALL_APIS_EXT	extern
#endif	/*_r_4_1_all_apis_h_global_*/

#ifdef __cplusplus
extern "C" {
#endif 

///#include "common_types.h"
///#include "config_types.h"

// For HiFi-capable tuner parts' AUDIO_HUB, AUDIO_DSP sub-APIs
typedef struct
{
    uint8_t PIPE;
    uint8_t PACKET_COUNT;
    uint16_t LENGTH;
    uint8_t* DATA;
} SI479XX_hifi_resp__data;

struct api_hal_interface {
    // Normal part interface. Implementation should wait for CTS
    RETURN_CODE (*write_command)
    (dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);

    RETURN_CODE (*read_reply)
    (dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);

    // HiFi command interface. Implementations should wait for appropriate ACTS bit
    RETURN_CODE (*hifi_command)
    (dc_config_chip_select icNum, uint8_t pipe, uint8_t packet_count, uint16_t length, uint8_t* buffer);

    RETURN_CODE (*hifi_response)
    (dc_config_chip_select icNum, uint8_t pipe, SI479XX_hifi_resp__data* resp);

    // Optional lock callbacks to help multi-threaded programs use the generated apis.
    RETURN_CODE (*acquire_lock)(dc_config_chip_select icNum);
    RETURN_CODE (*release_lock)(dc_config_chip_select icNum);
};

#ifdef _r_4_1_all_apis_h_global_		/* 210419 SGsoft */
struct api_hal_interface g_all_apis_functions = {
    .write_command = NULL,
    .read_reply = NULL,

    .hifi_command = NULL,
    .hifi_response = NULL,

    .acquire_lock = NULL,
    .release_lock = NULL,
};
#else
// Must be initialized by caller before using API.
extern struct api_hal_interface g_all_apis_functions;
#endif	/*_r_4_1_all_apis_h_global_*/


// Wrappers around the above function pointers for generated code to call.
R_4_1_ALL_APIS_EXT RETURN_CODE iapis_writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);
R_4_1_ALL_APIS_EXT RETURN_CODE iapis_readReply(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);

R_4_1_ALL_APIS_EXT RETURN_CODE iapis_hifiWriteCommand(dc_config_chip_select icNum, uint8_t pipe, uint8_t packet_count, uint16_t length, uint8_t* buffer);
R_4_1_ALL_APIS_EXT RETURN_CODE iapis_hifiReadReply(dc_config_chip_select icNum, uint8_t pipe, SI479XX_hifi_resp__data*);

R_4_1_ALL_APIS_EXT bool iapis_isSatellite(demod_id demod);
R_4_1_ALL_APIS_EXT RETURN_CODE iapis_acquireLock(dc_config_chip_select ic);
R_4_1_ALL_APIS_EXT RETURN_CODE iapis_releaseLock(dc_config_chip_select ic);

// If platform is big-endian, we will need to swap the byte ordering
// when sending/reading the command/reply byte buffer.
#ifdef OPTION_CONVERT_BIG_ENDIAN
uint16_t _swap_16(uint16_t value);
uint32_t _swap_32(uint32_t value);
#else
#define _swap_16(x) x
#define _swap_32(x) x
#endif

// For Flash Load sub APIS
#define SUBCMD_HEADER_SIZE 4

// For HUB, DSP sub APIS
#define HIFI_HEADER_SIZE 4
#define MAX_NUM_PARTS 8

// For all APIs
uint8_t getCommandIndex(uint8_t icNum);

//
// Creating macros which can easily be replaced for
//   alternate platforms during porting - if necessary.
//**********************************************************************
#include <string.h> // Needed for memory operations
#define ClearMemory(ptr, num) memset((ptr), 0, (num))
#define CpyMemory(dPtr, sPtr, num) memcpy((dPtr), (sPtr), (num))
#define CompareMemory(ptr1, ptr2, num) memcmp((ptr1), (ptr2), (num))

#ifdef __cplusplus
}
#endif 

#endif // ALL_APIS_H_

