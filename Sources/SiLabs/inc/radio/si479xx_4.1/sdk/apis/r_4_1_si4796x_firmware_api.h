/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si4796x command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
 

#ifndef si4796x_firmware_api_h_
#define si4796x_firmware_api_h_

#ifdef _r_4_1_si4796x_firmware_api_h_global_		/* 210419 SGsoft */
#define R_4_1_SI4796X_FW_API_EXT
#else
#define R_4_1_SI4796X_FW_API_EXT	extern
#endif	/*_r_4_1_si4796x_firmware_api_h_global_*/

///#include "si4796x_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#if 0		/* 210512 SGsoft */	
#ifdef _r_4_1_si4796x_firmware_api_h_global_		/* 210419 SGsoft */
const uint16_t SI4796X_PROPS[81] = {
    SI4796X_PROP_INTERRUPT_ENABLE0_ADDR,
    SI4796X_PROP_INTERRUPT_ENABLE1_ADDR,
    SI4796X_PROP_INTERRUPT_REPEAT0_ADDR,
    SI4796X_PROP_INTERRUPT_REPEAT1_ADDR,
    SI4796X_PROP_INTERRUPT_PULSEWIDTH_ADDR,
    SI4796X_PROP_INTERRUPT_OPTIONS_ADDR,
    SI4796X_PROP_AGC_TEST_MODE_ADDR,
    SI4796X_PROP_AGC_AM_RF_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_FMB_RF_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_B3B_RF_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_AM_MS_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_FMA_RF_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_B3A_RF_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_IF0_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_IF1_INDEX_OVERRIDE_ADDR,
    SI4796X_PROP_AGC_AM_RF_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_FMB_RF_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_B3B_RF_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_AM_MS_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_FMA_RF_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_B3A_RF_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_AM_IF0_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_FM_IF0_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_B3_IF0_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_AM_IF1_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_FM_IF1_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_B3_IF1_THRESHOLD_ADDR,
    SI4796X_PROP_AGC_AM_RF_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_FMB_RF_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_B3B_RF_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_AM_MS_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_FMA_RF_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_B3A_RF_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_AM_IF0_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_FM_IF0_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_B3_IF0_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_AM_IF1_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_FM_IF1_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_B3_IF1_HYSTERESIS_ADDR,
    SI4796X_PROP_AGC_AM_RF_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_FMB_RF_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_B3B_RF_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_AM_MS_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_FMA_RF_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_B3A_RF_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_AM_IF0_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_FM_IF0_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_B3_IF0_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_AM_IF1_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_FM_IF1_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_B3_IF1_LOWER_LIMIT_ADDR,
    SI4796X_PROP_AGC_AM_RF_LIMITER_ADDR,
    SI4796X_PROP_AGC_IMPULSE_REACTION_ADDR,
    SI4796X_PROP_FRONTEND_ANTENNA_INACTIVE_ADDR,
    SI4796X_PROP_FRONTEND_TUNER0_ANTENNA_SELECT_ADDR,
    SI4796X_PROP_FRONTEND_TUNER1_ANTENNA_SELECT_ADDR,
    SI4796X_PROP_FRONTEND_LT_SELECT_LTA_ADDR,
    SI4796X_PROP_FRONTEND_LT_SELECT_LTB_ADDR,
    SI4796X_PROP_FRONTEND_LT_SELECT_AM_ADDR,
    SI4796X_PROP_I2S_ISTART_ADDR,
    SI4796X_PROP_I2S_QSTART_ADDR,
    SI4796X_PROP_I2S_MCLK_OUTPUT_ADDR,
    SI4796X_PROP_FMPHASEDIV_CHANBW_CONTROL_ADDR,
    SI4796X_PROP_FMPHASEDIV_RDS_COMBINING_ADDR,
    SI4796X_PROP_CUSTOMER_INFO0_ADDR,
    SI4796X_PROP_CUSTOMER_INFO1_ADDR,
    SI4796X_PROP_CUSTOMER_INFO2_ADDR,
    SI4796X_PROP_CUSTOMER_INFO3_ADDR,
    SI4796X_PROP_EZIQ_CONFIG_ADDR,
    SI4796X_PROP_EZIQ_FIXED_SETTINGS_ADDR,
    SI4796X_PROP_EZIQ_OUTPUT_SOURCE_ADDR,
    SI4796X_PROP_EZIQ_ZIF_FM_CONFIG0_ADDR,
    SI4796X_PROP_EZIQ_ZIF_FM_CONFIG1_ADDR,
    SI4796X_PROP_EZIQ_ZIF_AM_CONFIG0_ADDR,
    SI4796X_PROP_EZIQ_ZIF_AM_CONFIG1_ADDR,
    SI4796X_PROP_EZIQ_ZIF_DAB_CONFIG0_ADDR,
    SI4796X_PROP_EZIQ_ZIF_DAB_CONFIG1_ADDR,
    SI4796X_PROP_DRM30_DIG_AGC_CONFIG_ADDR,
    SI4796X_PROP_DRM30_DIG_AGC_BITS_ADDR,
    SI4796X_PROP_DAB_DIG_AGC_CONFIG_ADDR,
    SI4796X_PROP_DAB_DIG_AGC_BITS_ADDR,
};
#else
// List of valid property addresses defined by this API. (mainly for debug)
extern const uint16_t SI4796X_PROPS[81];
#endif	/*_r_4_1_si4796x_firmware_api_h_global_*/
#endif	/*0*/

typedef struct _SI4796X_read_status__data
{
	uint8_t CTS;
	uint8_t APIERR;
	uint8_t SWERR;
	uint8_t VDERR;
	uint8_t ALERTINT;
	uint8_t RDSINT;
	uint8_t STCINT;
	uint8_t RDS1INT;
	uint8_t STC1INT;
	uint8_t AUDINT;
	uint8_t ACTS3;
	uint8_t ACTS2;
	uint8_t ACTS1;
	uint8_t ACTS0;
	uint8_t PUP_STATE;
	uint8_t HOTWRN;
	uint8_t TMPERR;
	uint8_t CMDERR;
	uint8_t REPERR;
	uint8_t ARBERR;
	uint8_t NRERR;
	uint8_t* DATA;
}SI4796X_read_status__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_read_status__reply(uint8_t icNum, SI4796X_read_status__data* replyData);


/*
 * Command: SI4796X_read_status
 * 
 * Description:
 *   Returns the status byte and data for the last command sent to the device. This command is also used to poll the status byte as needed. To poll the status byte send the READ_REPLY command and read the status byte. This can be done regardless of the state of the CTS bit in the status register. Please refer to individual command descriptions for the format of returned data. READ_REPLY is a hardware command and can be issued while device is powered down. The number of reply bytes that are available depends on the command sent. This is a hardware command. This command may be issued while the device is powered down. If the READ_REPLY:STATUS:APIERR is set, see ERR  for a description of what error code data[0] represents.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cts: Clear-to-send (bit 7 of the STATUS field which is read by the user) Indicates whether the user may send a new firmware-interpreted command and retrieve a reply from the previous command. It has no effect for hardware-interpreted commands. When a new read or write transaction is started on the serial port, the current state of CTS is captured into a temporary register, which is considered to be the state of CTS for the duration of that serial port transaction. The captured value will govern the serial port behavior and is returned to the user in the STATUS byte on reads. Therefore, any changes made to the state of CTS by the processor will not affect a serial port transaction that is currently in progress.
 *  apierr: The command that was sent is either not a valid command in this mode, had an invalid argument, or is otherwise not allowed. See  ERR  for a description of what error code in byte 4 represents.
 *  swerr: An internal software error has occurred. If this occurs, please perform a READ_REPLY command and readback 80 bytes as debug information when contacting technical support. The only way to recover from a SWERR is to reset the part.
 *  vderr: The VD supply monitoring routine has detected a drop in the supply voltage below the recommended minimum supply voltage. A reboot is recommended if this occurs.
 *  alertint: Alert Tone interrupt
 *  rdsint: RDS interrupt
 *  stcint: Seek/Tune Complete. For set mode command, STC or "seek tune complete" indicates that the part has successfully loaded the radio image as selected by the mode option in setMode command, and is now ready to seek/tune in that mode. During setMode command, besides loading the right radio image, it is ensured that the agc for that mode has converged, that is, the gains (RF and IF) are at an optimal gain level for seek/tune performance. Once the agc has converged to an optimal gain, the STC bit is set high. For tune command, when host issues a tune/seek command, STC bit is set low, and once tune/seek finishes, the bit is set high to indicate that the part has finished tuning and is ready to accept new tune/seek command. If the host has issued a setMode or tune/seek command, and they do not get STC bit set after certain time interval, it implies that an error has occured. They should send READ_REPLY command and readback 80 bytes as debug information when contacting technical support. Besides that, the only way to recover from such a state is to reset the part.
 *  rds1int: RDS interrupt for Tuner 1
 *  stc1int: Seek/Tune Complete for Tuner 1. For set mode command, STC or "seek tune complete" indicates that the part has successfully loaded the radio image as selected by the mode option in setMode command, and is now ready to seek/tune in that mode. During setMode command, besides loading the right radio image, it is ensured that the agc for that mode has converged, that is, the gains (RF and IF) are at an optimal gain level for seek/tune performance. Once the agc has converged to an optimal gain, the STC bit is set high. For tune command, when host issues a tune/seek command, STC bit is set low, and once tune/seek finishes, the bit is set high to indicate that the part has finished tuning and is ready to accept new tune/seek command. If the host has issued a setMode or tune/seek command, and they do not get STC bit set after certain time interval, it implies that an error has occured. They should send READ_REPLY command and readback 80 bytes as debug information when contacting technical support. Besides that, the only way to recover from such a state is to reset the part.
 *  audint: The Hifi requires attention.
 *  acts3: Chip is ready to accept a  HIFI_CMD3  command
 *  acts2: Chip is ready to accept a  HIFI_CMD2  command
 *  acts1: Chip is ready to accept a  HIFI_CMD1  command
 *  acts0: Chip is ready to accept a  HIFI_CMD0  command
 *  pup_state: Power up state
 *  hotwrn: The chip's die temperature has reached a level of reduced performance.
 *  tmperr: The chip's die temperature has reached a critical temperature. It is recommended that the part be powered down to avoid long term damage.
 *  cmderr: The control interface has dropped data during a command write, which is a fatal error. This is generally caused by running at a SPI clock rate that is too fast for the given Cortex clock rate (data arbiter and memory speed).
 *  reperr: The control interface has dropped data during a reply read, which is a fatal error. This is generally caused by running at a SPI clock rate that is too fast for the given Cortex clock rate (data arbiter and memory speed).
 *  arberr: A data arbiter overflow has occurred, which has resulted in corrupted data to the Cortex or Quark memories.
 *  nrerr: A non-recoverable error has occurred. This bit is set to 1 when the Cortex's watchdog timer times out, which should not happen during normal operation. It can indicate either a software or hardware failure.
 *  data: See the command sent for a description of these bytes
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_read_status(uint8_t icNum, SI4796X_read_status__data*  replyData);


/*
 * Command: SI4796X_power_up
 * 
 * Description:
 *   Configures various power up options for the device.  Fields related to a multi-chip setup are in effect when the chip is in normal operation (after BOOT). Pin setup for a flash boot is done via the LOAD_CONFIG command.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  ctsien: The bootloader will toggle a host interrupt line when CTS is available.
 *  clko_current: The application will enable the CLKO pin for use by other chips in the system. The crystal frequency is driven on this pin.
 *  vio: The VIO nominal voltage.
 *  clkout: The application will enable the CLKO pin for use by other chips in the system. The crystal frequency is driven on this pin.
 *  clk_mode: Choose clock mode. See refclk spec sheet for more information.
 *  tr_size: Configure the crystal oscillator
 *  ctun: Controls the programmable on chip capacitors at XTAL1 & XTAL2 pads. This parameter is only required if using the crystal oscillator. Use the following equation to determin CTUN: XTALI,XTALO= 5pF + (0.381pF * CTUN) NOTE: the capacitance being programmed here is the total capacitance looking into each pin (XTALI and XTALO) to ground, and includes internal IC parasitic capacitances.
 *  xtal_freq: XTAL Frequency in Hz. The supported crystal frequencies are:    36.864 MHz This parameter is required for all valid clk_mode options.
 *  ibias: Sets oscillator bias current in 40Î¼A steps starting from 20Î¼A. During startup of the crystal, the full value of this setting is used. After the crystal is running, the ibias current is reduced to just IBIAS[4:0]. For example, if set to 0x28, the crystal will initially be provided 1620 Î¼A of current. Once the crystal is running, the current will be reduced to a steady state current of 340 Î¼A (0x08).    This parameter is only required if using the crystal oscillator.
 *  afs: Primary audio sample rate
 *  chipid: Assigns a chip identification number to this chip
 *  eziq_master: Sets the power-up configuration of the ZIF output to be Master or Slave.
 *  eziq_enable: Configuration of ZIF ports on power-up. Affects the property  EZIQ_OUTPUT_SOURCE
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  uncorrectable: Number of OTP bits that were not correctable. Part is damaged
 *  correctable: Number of OTP bits that were corrected. Part is damaged but usable
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_power_up__command(uint8_t icNum,uint8_t ctsien, uint8_t clko_current, uint8_t vio, uint8_t clkout, uint8_t clk_mode, uint8_t tr_size, uint8_t ctun, uint32_t xtal_freq, uint8_t ibias, uint8_t afs, uint8_t chipid, uint8_t eziq_master, uint8_t eziq_enable);


typedef struct _SI4796X_power_up__data
{
	uint8_t UNCORRECTABLE;
	uint8_t CORRECTABLE;
}SI4796X_power_up__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_power_up__reply(uint8_t icNum, SI4796X_power_up__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_power_up(uint8_t icNum, uint8_t  ctsien, uint8_t  clko_current, uint8_t  vio, uint8_t  clkout, uint8_t  clk_mode, uint8_t  tr_size, uint8_t  ctun, uint32_t  xtal_freq, uint8_t  ibias, uint8_t  afs, uint8_t  chipid, uint8_t  eziq_master, uint8_t  eziq_enable, SI4796X_power_up__data*  replyData);


/*
 * Command: SI4796X_err
 * 
 * Description:
 *   This is not a real command but is provided only for documentation of error codes. The error code is the first data byte in READ_REPLY if the  READ_REPLY:STATUS:APIERR is set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  code: See the command sent for a description of these bytes. This byte is normally the first response byte to the last command sent but in the event that  READ_REPLY:STATUS:APIERR  is set, the first data byte will instead be the error code.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_err__command(uint8_t icNum);



R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_err__reply(uint8_t icNum, uint8_t* code);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_err(uint8_t icNum, uint8_t*  code);


/*
 * Command: SI4796X_hifi_cmd0
 * 
 * Description:
 *   Passes commands on to the HiFi DSP and clears ACTS0
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  packet_count: Number of packets in this command
 *  length: Number of bytes of all packets in this command
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd0__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd0(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data);


/*
 * Command: SI4796X_hifi_cmd1
 * 
 * Description:
 *   Passes commands on to the HiFi DSP and clears ACTS1
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  packet_count: Number of packets in this command
 *  length: Number of bytes of all packets in this command
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd1__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd1(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data);


/*
 * Command: SI4796X_hifi_cmd2
 * 
 * Description:
 *   Passes commands on to the HiFi DSP and clears ACTS2
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  packet_count: Number of packets in this command
 *  length: Number of bytes of all packets in this command
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd2__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd2(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data);


/*
 * Command: SI4796X_hifi_cmd3
 * 
 * Description:
 *   Passes commands on to the HiFi DSP and clears ACTS3
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  packet_count: Number of packets in this command
 *  length: Number of bytes of all packets in this command
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd3__command(uint8_t icNum,uint8_t packet_count, uint16_t length, uint8_t* data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_cmd3(uint8_t icNum, uint8_t  packet_count, uint16_t  length, uint8_t*  data);


/*
 * Command: SI4796X_hifi_resp0
 * 
 * Description:
 *   Retrieves responses from the HiFi DSP in response to an earlier command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pipe: HiFi
 *  packet_count: Number of packets in this response
 *  length: Number of bytes of all packets in this response
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp0__command(uint8_t icNum);



/*
 * NOTE: return values of the xxx_hifi_respX__reply() functions.
 * 1. Rather than using these functions, it is much easer to just use
 *      the generated hifi_api or hub_api files.  Those wrap all the hub/hifi
 *      pass through commands, so you don't need to bother directly with these.
 *
 * 2. These 'malloc()' to handle dynamic sized arrays in replies. The generated
 *      hub_api, hifi_api should handle freeing memory if an error occurs.
 *
 * 3. If using these directly, here are the meanings of return values:
 *      SUCCESS - successfully read from part, memory allocation was successful.
 *                  Must free dynamic memory.
 *      otherwise - user SHOULDN'T have to free memory in for non SUCCESS return
 *                      values. Generated function should take care of it.
 * 
 *      otherwise values:
 *      MEMORY_ERROR - malloc() call has failed.
 *      COMMAND_ERROR - issue with command sent to part. (iapis_readReply implementation failed?)
 *      INVALID_INPUT - arguments were invalid
 */      


typedef SI4796X_hifi_resp__data SI4796X_hifi_resp0__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp0__reply(uint8_t icNum, SI4796X_hifi_resp0__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp0(uint8_t icNum, SI4796X_hifi_resp0__data*  replyData);


/*
 * Command: SI4796X_hifi_resp1
 * 
 * Description:
 *   Retrieves responses from the HiFi DSP in response to an earlier command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pipe: HiFi
 *  packet_count: Number of packets in this response
 *  length: Number of bytes of all packets in this response
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp1__command(uint8_t icNum);


typedef SI4796X_hifi_resp__data SI4796X_hifi_resp1__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp1__reply(uint8_t icNum, SI4796X_hifi_resp1__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp1(uint8_t icNum, SI4796X_hifi_resp1__data*  replyData);


/*
 * Command: SI4796X_hifi_resp2
 * 
 * Description:
 *   Retrieves responses from the HiFi DSP in response to an earlier command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pipe: HiFi
 *  packet_count: Number of packets in this response
 *  length: Number of bytes of all packets in this response
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp2__command(uint8_t icNum);


typedef SI4796X_hifi_resp__data SI4796X_hifi_resp2__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp2__reply(uint8_t icNum, SI4796X_hifi_resp2__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp2(uint8_t icNum, SI4796X_hifi_resp2__data*  replyData);


/*
 * Command: SI4796X_hifi_resp3
 * 
 * Description:
 *   Retrieves responses from the HiFi DSP in response to an earlier command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pipe: HiFi
 *  packet_count: Number of packets in this response
 *  length: Number of bytes of all packets in this response
 *  data: Hifi data packet
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp3__command(uint8_t icNum);


typedef SI4796X_hifi_resp__data SI4796X_hifi_resp3__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp3__reply(uint8_t icNum, SI4796X_hifi_resp3__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_resp3(uint8_t icNum, SI4796X_hifi_resp3__data*  replyData);


/*
 * Command: SI4796X_datalogger_source
 * 
 * Description:
 *   Determine which stream of data is presented on the DEBUGP/N pins for capturing by the datalogger
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  source: Selection of data source
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_datalogger_source__command(uint8_t icNum,uint8_t source);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_datalogger_source(uint8_t icNum, uint8_t  source);


/*
 * Command: SI4796X_datalogger_trace
 * 
 * Description:
 *   Route a given group of trace data to a destination channel.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  group_id: Trace group
 *  destination: Destination for the group
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_datalogger_trace__command(uint8_t icNum,uint8_t group_id, uint8_t destination);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_datalogger_trace(uint8_t icNum, uint8_t  group_id, uint8_t  destination);


/*
 * Command: SI4796X_fmhd_diversity
 * 
 * Description:
 *   Controls FMHD Diversity combining parameters. Special care should be taken while tuning while using HD diversity.  Prior to tuning  to a new station, HD MRC combining should be disabled to prevent the algorithm from attempting  to combine two disparate stations. The following figure is the recommended  procedure when tuning to a new station.  The order of the tuning is not important.
 *   hdtune Special care should be taken while tuning while using HD diversity.  Prior to tuning  to a new station, HD MRC combining should be disabled to prevent the algorithm from attempting  to combine two disparate stations. The following figure is the recommended  procedure when tuning to a new station.  The order of the tuning is not important.
 *   hdtune On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  mode: Sets the mode of operation for FMHD digital diveristy.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity__command(uint8_t icNum,uint8_t mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity(uint8_t icNum, uint8_t  mode);


/*
 * Command: SI4796X_fmhd_diversity_status
 * 
 * Description:
 *   Reports that current status and metrics of the FMHD digital combiner. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  acquired: Reports whether the FMHD signal has been acquired.
 *  mode: Sets the mode of operation for FMHD digital diveristy.
 *  psmi: FMHD primary service mode indicator
 *  comb_weight: Combiner weight metric to reflect antenna contribution. -128 is chip 0, +127 is chip 1   Combiner weight metric to reflect antenna contribution. -128 is tuner 0, +127 is tuner 1
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity_status__command(uint8_t icNum);


typedef struct _SI4796X_fmhd_diversity_status__data
{
	uint8_t ACQUIRED;
	uint8_t MODE;
	uint8_t PSMI;
	int8_t COMB_WEIGHT;
}SI4796X_fmhd_diversity_status__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity_status__reply(uint8_t icNum, SI4796X_fmhd_diversity_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity_status(uint8_t icNum, SI4796X_fmhd_diversity_status__data*  replyData);


/*
 * Command: SI4796X_fmhd_diversity_acquire
 * 
 * Description:
 *   When using the FMHD diveristy, this command informs the diversity engine reacquire the signal.  This should be done immediately after a tune to to speed up the acquisition of the digital signal. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity_acquire__command(uint8_t icNum);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fmhd_diversity_acquire(uint8_t icNum);


/*
 * Command: SI4796X_fm_phase_diversity
 * 
 * Description:
 *   Configures the diversity combining and audio stream used for the output from this chip.  When performing an AFCHECK, either chip0 or chip1 can be used to perform the check as long as the FM_VALID_AF_TIMER1/2/3 timers are all <12ms.  If a longer validation time is  required, then chip1 should be used to perform the AFCHECK to avoid any audio artifacts.  When performing an AFTUNE, it is best to use chip1 for the initial tune. Only if chip1 succeeds to stay at the new station should chip0 then be tuned to the new station. Configures the diversity combining and audio stream used for the output from this chip.  When performing an AFCHECK, either tuner0 or tuner1 can be used to perform the check as long as the FM_VALID_AF_TIMER1/2/3 timers are all <12ms.  If a longer validation time is  required, then tuner1 should be used to perform the AFCHECK to avoid any audio artifacts.  When performing an AFTUNE, it is best to use tuner1 for the initial tune. Only if tuner1 succeeds to stay at the new station should tuner0 then be tuned to the new station. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  audio: Selects the demoded audio source from the chipId specified.   Selects the demoded audio source from the tunerId specified.
 *  comb: This field specifies the number of antennae to combine in diversity. A value of 0 will not perform any combining and will only use the IQ data from chip0.   This field specifies the number of antennae to combine in diversity. A value of 0 will not perform any combining and will only use the IQ data from tuner0.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_diversity__command(uint8_t icNum,uint8_t audio, uint8_t comb);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_diversity(uint8_t icNum, uint8_t  audio, uint8_t  comb);


/*
 * Command: SI4796X_fm_phase_div_status
 * 
 * Description:
 *   Reports the phase diversity status information. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  audio: Selects the demoded audio source from the chipId specified.   Selects the demoded audio source from the tunerId specified.
 *  comb: This field specifies the number of antennae to combine in diversity. A value of 0 will not perform any combining and will only use the IQ data from chip0.   This field specifies the number of antennae to combine in diversity. A value of 0 will not perform any combining and will only use the IQ data from tuner0.
 *  phsdivcorr: Cross correlation of the audio for the two tuners in negative dB. 0 indicates a perfect match. Smaller value means better correlation.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_div_status__command(uint8_t icNum);


typedef struct _SI4796X_fm_phase_div_status__data
{
	uint8_t AUDIO;
	uint8_t COMB;
	uint8_t PHSDIVCORR;
}SI4796X_fm_phase_div_status__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_div_status__reply(uint8_t icNum, SI4796X_fm_phase_div_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_div_status(uint8_t icNum, SI4796X_fm_phase_div_status__data*  replyData);


/*
 * Command: SI4796X_fm_phase_diversity_local_metrics
 * 
 * Description:
 *   Returns the FM received signal quality metrics of local chip. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  rssidb: Received signal strength indicator
 *  rssidb_adj: Received signal strength indicator adjusted by property FM_RSQ_RSSI_OFFSET.
 *  rssincdb: Received signal strength indicator without RF compensation.
 *  rssincdb_adj: Received signal strength indicator without RF compensation adjusted by property FM_RSQ_RSSI_OFFSET.
 *  chan_bw: Channel bandwidth
 *  lassi: Low side adjacent (100kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier
 *  hassi: High side adjacent (100kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  assi100: The highest of HASSI/LASSI
 *  lassi200: Low side adjacent (200kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  hassi200: High side adjacent (200kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  assi200: The highest of HASSI200/LASSI200
 *  freqoffset_wb: Pre-Channel Filter Signed wide-band frequency offset
 *  reserved1: (No Description)
 *  freqoffset: Signed frequency offset in units of kHz.
 *  multipath: Multipath indicator
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_diversity_local_metrics__command(uint8_t icNum);


typedef struct _SI4796X_fm_phase_diversity_local_metrics__data
{
	int8_t RSSIDB;
	int8_t RSSIDB_ADJ;
	int8_t RSSINCDB;
	int8_t RSSINCDB_ADJ;
	uint8_t CHAN_BW;
	int8_t LASSI;
	int8_t HASSI;
	int8_t ASSI100;
	int8_t LASSI200;
	int8_t HASSI200;
	int8_t ASSI200;
	int8_t FREQOFFSET_WB;
	int8_t RESERVED1;
	int8_t FREQOFFSET;
	uint8_t MULTIPATH;
}SI4796X_fm_phase_diversity_local_metrics__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_diversity_local_metrics__reply(uint8_t icNum, SI4796X_fm_phase_diversity_local_metrics__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_fm_phase_diversity_local_metrics(uint8_t icNum, SI4796X_fm_phase_diversity_local_metrics__data*  replyData);


/*
 * Command: SI4796X_dab_diversity
 * 
 * Description:
 *   Controls DAB Diversity combining parameters. Special care should be taken while tuning while using DAB diversity.  Prior to tuning  to a new station, DAB MRC combining should be disabled to prevent the algorithm from attempting  to combine two disparate stations. The following figure is the recommended  procedure when tuning to a new station.  The order of the tuning is not important.
 *   dabtune Special care should be taken while tuning while using DAB diversity.  Prior to tuning  to a new station, DAB MRC combining should be disabled to prevent the algorithm from attempting  to combine two disparate stations. The following figure is the recommended  procedure when tuning to a new station.  The order of the tuning is not important.
 *   dabtune On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  mode: Sets the mode of operation for DAB digital diveristy.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity__command(uint8_t icNum,uint8_t mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity(uint8_t icNum, uint8_t  mode);


/*
 * Command: SI4796X_dab_diversity_status
 * 
 * Description:
 *   Reports that current status and metrics of the DAB digital combiner. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  acquired: Reports whether the DAB signal has been acquired.
 *  dabdetect: Reports whether a DAB signal has been detected.
 *  mode: Sets the mode of operation for DAB digital diveristy.
 *  comb_weight: Combiner weight metric to reflect antenna contribution. -128 is chip 0, +127 is chip 1   Combiner weight metric to reflect antenna contribution. -128 is tuner 0, +127 is tuner 1
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity_status__command(uint8_t icNum);


typedef struct _SI4796X_dab_diversity_status__data
{
	uint8_t ACQUIRED;
	uint8_t DABDETECT;
	uint8_t MODE;
	int8_t COMB_WEIGHT;
}SI4796X_dab_diversity_status__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity_status__reply(uint8_t icNum, SI4796X_dab_diversity_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity_status(uint8_t icNum, SI4796X_dab_diversity_status__data*  replyData);


/*
 * Command: SI4796X_dab_diversity_acquire
 * 
 * Description:
 *   When using the DAB diveristy, this command informs the diversity engine reacquire the signal.  This should be done immediately after a tune to to speed up the acquisition of the digital signal. On certain devices, this is only available if the appropriate software package has been downloaded.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tx_mode: Specifies the transmission mode for DAB detect. DAB MRC needs to be told which mode to process. This information is available from fast detect.   See DAB_RSQ_STATUS DABDETECT.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity_acquire__command(uint8_t icNum,uint8_t tx_mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_diversity_acquire(uint8_t icNum, uint8_t  tx_mode);


/*
 * Command: SI4796X_send_test_pattern
 * 
 * Description:
 *   Sends a test patterns from the selected port. The format of the test pattern is the numerical sequence 0, 1,..., N. Where N = LENGTH-1.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  port: PORT[1:0]   0 = Send the test pattern as the response of this command.   1 = Reserved   2 = Reserved   3 = Reserved   TYP = 0
 *  length: The length of the pattern to return in bytes. Note, when the port selected is the SPI_I2C port the length of the sequence is limited to 2048 bytes.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: First byte of the pattern. Only returned if the SPI_I2C is selected.
 *  length (input): #elems in 'data' array.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_send_test_pattern__command(uint8_t icNum,uint8_t port, uint8_t length);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_send_test_pattern__reply(uint8_t icNum, uint8_t* data, uint16_t length);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_send_test_pattern(uint8_t icNum, uint8_t  port, uint8_t  arg_length, uint8_t*  data, uint16_t  rep_length);


/*
 * Command: SI4796X_hifi_log
 * 
 * Description:
 *   Read HIFI log based on memory block
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  block: Read HIFI log based on the memory block selected
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  log: Returns HIFI log
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_log__command(uint8_t icNum,uint8_t block);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_log__reply(uint8_t icNum, uint8_t* log);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_log(uint8_t icNum, uint8_t  block, uint8_t*  log);


/*
 * Command: SI4796X_host_load
 * 
 * Description:
 *   HOST_LOAD loads an image from HOST over the command interface. It sends up to 4096 bytes of application image to the bootloader.  Note: This command is much more efficient when the image is sent as multiples of 4 bytes.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  size: Number of bytes sent. If 0, byte count reported by hardware is used instead.
 *  data: Data byte from boot_img data stream. Maximum of 4096 bytes of data per HOST_LOAD command. Must be a multiple of 4 bytes.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_host_load__command(uint8_t icNum,uint16_t size, uint8_t* data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_host_load(uint8_t icNum, uint16_t  size, uint8_t*  data);


/*
 * Command: SI4796X_flash_load
 * 
 * Description:
 *   FLASH_LOAD loads the firmware image from an externally attached SPI flash over the secondary SPI bus. The image must be contiguous on the flash.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  subcmd: FLASH_LOAD subcommand. See sub-API section on FLASH_LOAD.
 *  data: FLASH_LOAD subcommand specific data. See boot loader sub-API section on FLASH_LOAD
 *  data_length (input): #elems in 'data' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: Byte containing FLASH_LOAD subcommand reply
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_flash_load__command(uint8_t icNum,uint8_t subcmd, uint8_t* data, uint16_t data_length);



R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_flash_load__reply(uint8_t icNum, uint8_t* data);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_flash_load(uint8_t icNum, uint8_t  subcmd, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data);


/*
 * Command: SI4796X_load_init
 * 
 * Description:
 *   LOAD_INIT prepares the bootloader to receive a new image. It will force the bootloader state to waiting for a new LOAD command (HOST_LOAD or FLASH_LOAD.) LOAD_INIT command must always be sent prior to a HOST_LOAD or a FLASH_LOAD command.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  count: Number of GUIDs. See  LOAD_INIT:GUID:GUID
 *  guid: A GUID is a 24-bit global unique identifier and each boot object (except the patch) is assigned one. It is used to tell the bootloader which objects in the boot stream to load. The GUID can be found at the text header of a boot object, i.e. the .boot file
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_load_init__command(uint8_t icNum,uint8_t count, uint32_t* guid);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_load_init(uint8_t icNum, uint8_t  count, uint32_t*  guid);


/*
 * Command: SI4796X_boot
 * 
 * Description:
 *   BOOT command boots the image currently loaded in RAM.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_boot__command(uint8_t icNum);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_boot(uint8_t icNum);


/*
 * Command: SI4796X_get_part_info
 * 
 * Description:
 *   This command can be called to determine the part number, part version, ROM ID, etc. Using "SI47906A2GE1AM" as an example of a part number and how it maps to the response from this command.  "SI479" is always assumed and not part of this command's response  "06" is made up of GET_PART_INFO:PN0 and      GET_PART_INFO:PN1 "A" is GET_PART_INFO:MAJOR "2" is a unique mapping from GET_PART_INFO:CHIPREV and      GET_PART_INFO:ROMREV.  For A2, they are 2 and 1, respectively.  "GE" is made up of GET_PART_INFO:CUST1 and       GET_PART_INFO:CUST2
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  chiprev: Chip Mask Revision
 *  romrev: ROM Revision
 *  family: Always 0x96 for SI4796 family devices
 *  pn0: This is the first x after Si479xx in ASCII.
 *  pn1: This is the second x after Si479xx in ASCII.
 *  major: Major version number in ASCII
 *  cust1: Customer label 1
 *  cust2: Customer label 2
 *  corrected: Number of OTP bits that were corrected when booting. It may be an indication that the customer's manufacturing process is inadvertently corrupting the chip's OTP.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_part_info__command(uint8_t icNum);


typedef struct _SI4796X_get_part_info__data
{
	uint8_t CHIPREV;
	uint8_t ROMREV;
	uint8_t FAMILY;
	uint8_t PN0;
	uint8_t PN1;
	uint8_t MAJOR;
	uint8_t CUST1;
	uint8_t CUST2;
	uint8_t CORRECTED;
}SI4796X_get_part_info__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_part_info__reply(uint8_t icNum, SI4796X_get_part_info__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_part_info(uint8_t icNum, SI4796X_get_part_info__data*  replyData);


/*
 * Command: SI4796X_get_power_up_args
 * 
 * Description:
 *   Echoes what power_up arguments were used to boot the device
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  ctsien: The bootloader will toggle a host interrupt line when CTS is available.
 *  clko_current: The application will enable the CLKO pin for use by other chips in the system. The crystal frequency is driven on this pin.
 *  vio: The VIO nominal voltage.
 *  clkout: The application will enable the CLKO pin for use by other chips in the system. The crystal frequency is driven on this pin.
 *  clk_mode: Choose clock mode. See refclk spec sheet for more information.
 *  tr_size: Configure the crystal oscillator
 *  ctun: Controls the programmable on chip capacitors at XTAL1 & XTAL2 pads. This parameter is only required if using the crystal oscillator. Use the following equation to determin CTUN: XTALI,XTALO= 5pF + (0.381pF * CTUN) NOTE: the capacitance being programmed here is the total capacitance looking into each pin (XTALI and XTALO) to ground, and includes internal IC parasitic capacitances.
 *  xtal_freq: XTAL Frequency in Hz. The supported crystal frequencies are:    36.864 MHz This parameter is required for all valid clk_mode options.
 *  ibias: Sets oscillator bias current in 40Î¼A steps starting from 20Î¼A. During startup of the crystal, the full value of this setting is used. After the crystal is running, the ibias current is reduced to just IBIAS[4:0]. For example, if set to 0x28, the crystal will initially be provided 1620 Î¼A of current. Once the crystal is running, the current will be reduced to a steady state current of 340 Î¼A (0x08).    This parameter is only required if using the crystal oscillator.
 *  afs: Primary audio sample rate
 *  chipid: Assigns a chip identification number to this chip
 *  reserved1: reserved1
 *  reserved2: reserved2
 *  reserved3: reserved3
 *  eziq_master: Sets the power-up configuration of the ZIF output to be Master or Slave.
 *  eziq_enable: Configuration of ZIF ports on power-up. Affects the property  EZIQ_OUTPUT_SOURCE
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_power_up_args__command(uint8_t icNum);


typedef struct _SI4796X_get_power_up_args__data
{
	uint8_t CTSIEN;
	uint8_t CLKO_CURRENT;
	uint8_t VIO;
	uint8_t CLKOUT;
	uint8_t CLK_MODE;
	uint8_t TR_SIZE;
	uint8_t CTUN;
	uint32_t XTAL_FREQ;
	uint8_t IBIAS;
	uint8_t AFS;
	uint8_t CHIPID;
	uint8_t EZIQ_MASTER;
	uint8_t EZIQ_ENABLE;
}SI4796X_get_power_up_args__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_power_up_args__reply(uint8_t icNum, SI4796X_get_power_up_args__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_power_up_args(uint8_t icNum, SI4796X_get_power_up_args__data*  replyData);


/*
 * Command: SI4796X_part_override
 * 
 * Description:
 *   When using an EVB for software development, the Si479xx devices are not  programmed with a specific part number in order to allow them to simulate  all part numbers. If loading firmware on the Si479xx EVB, the PART_OVERRIDE  command must be sent after the POWER_UP command.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pn0: This is the first x after Si479xx in ASCII.
 *  pn1: This is the second x after Si479xx in ASCII.
 *  major: Major version number in ASCII
 *  romascii: ROM version number in ASCII
 *  cust1: Customer label 1
 *  cust2: Customer label 2
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_part_override__command(uint8_t icNum,uint8_t pn0, uint8_t pn1, uint8_t major, uint8_t romascii, uint8_t cust1, uint8_t cust2);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_part_override(uint8_t icNum, uint8_t  pn0, uint8_t  pn1, uint8_t  major, uint8_t  romascii, uint8_t  cust1, uint8_t  cust2);


/*
 * Command: SI4796X_load_config
 * 
 * Description:
 *   Configures the flash pins for multichip flash pass-through
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  numchips: Number of chips in this design
 *  chipid: Valid chip IDs for a multi-chip application are 0-31.    Chip 0 is the "northmost" chip, typically the audio hub.    Chip numbers increase to the "south".    Flash is typically connected to the highest-numbered chip via the NV* pins on the south side of the chip.
 *  role: Flash boot role
 *  mosi_in: Please consult the multiboot documentation for proper configuration.
 *  miso_in: Please consult the multiboot documentation for proper configuration.
 *  intb_in: Please consult the multiboot documentation for proper configuration.
 *  nvsclk_in: Please consult the multiboot documentation for proper configuration.
 *  nvssb_in: Please consult the multiboot documentation for proper configuration.
 *  nvmosi_in: Please consult the multiboot documentation for proper configuration.
 *  nvmiso_in: Please consult the multiboot documentation for proper configuration.
 *  rsvd_in: Please consult the multiboot documentation for proper configuration.
 *  iqfsp_in: Please consult the multiboot documentation for proper configuration.
 *  iqfsn_in: Please consult the multiboot documentation for proper configuration.
 *  ioutp_in: Please consult the multiboot documentation for proper configuration.
 *  ioutn_in: Please consult the multiboot documentation for proper configuration.
 *  qoutp_in: Please consult the multiboot documentation for proper configuration.
 *  qoutn_in: Please consult the multiboot documentation for proper configuration.
 *  ssb_in: Please consult the multiboot documentation for proper configuration.
 *  sclk_in: Please consult the multiboot documentation for proper configuration.
 *  dout0_in: Please consult the multiboot documentation for proper configuration.
 *  dclk0_in: Please consult the multiboot documentation for proper configuration.
 *  dfs0_in: Please consult the multiboot documentation for proper configuration.
 *  dout1_in: Please consult the multiboot documentation for proper configuration.
 *  dclk1_in: Please consult the multiboot documentation for proper configuration.
 *  dfs1_in: Please consult the multiboot documentation for proper configuration.
 *  iqclkp_in: Please consult the multiboot documentation for proper configuration.
 *  iqclkn_in: Please consult the multiboot documentation for proper configuration.
 *  diclk2_in: Please consult the multiboot documentation for proper configuration.
 *  difs2_in: Please consult the multiboot documentation for proper configuration.
 *  din3_in: Please consult the multiboot documentation for proper configuration.
 *  diclk3_in: Please consult the multiboot documentation for proper configuration.
 *  difs3_in: Please consult the multiboot documentation for proper configuration.
 *  din4_in: Please consult the multiboot documentation for proper configuration.
 *  diclk4_in: Please consult the multiboot documentation for proper configuration.
 *  difs4_in: Please consult the multiboot documentation for proper configuration.
 *  din0_in: Please consult the multiboot documentation for proper configuration.
 *  gp0_in: Please consult the multiboot documentation for proper configuration.
 *  gp1_in: Please consult the multiboot documentation for proper configuration.
 *  gp2_in: Please consult the multiboot documentation for proper configuration.
 *  din1_in: Please consult the multiboot documentation for proper configuration.
 *  diclk1_in: Please consult the multiboot documentation for proper configuration.
 *  difs1_in: Please consult the multiboot documentation for proper configuration.
 *  din2_in: Please consult the multiboot documentation for proper configuration.
 *  difsa_in: Please consult the multiboot documentation for proper configuration.
 *  diclka_in: Please consult the multiboot documentation for proper configuration.
 *  aout0_in: Please consult the multiboot documentation for proper configuration.
 *  aout1_in: Please consult the multiboot documentation for proper configuration.
 *  icp_in: Please consult the multiboot documentation for proper configuration.
 *  icn_in: Please consult the multiboot documentation for proper configuration.
 *  difs0_in: Please consult the multiboot documentation for proper configuration.
 *  diclk0_in: Please consult the multiboot documentation for proper configuration.
 *  zifq1_in: Please consult the multiboot documentation for proper configuration.
 *  zifi1_in: Please consult the multiboot documentation for proper configuration.
 *  mosi_out: Please consult the multiboot documentation for proper configuration.
 *  miso_out: Please consult the multiboot documentation for proper configuration.
 *  intb_out: Please consult the multiboot documentation for proper configuration.
 *  nvsclk_out: Please consult the multiboot documentation for proper configuration.
 *  nvssb_out: Please consult the multiboot documentation for proper configuration.
 *  nvmosi_out: Please consult the multiboot documentation for proper configuration.
 *  nvmiso_out: Please consult the multiboot documentation for proper configuration.
 *  rsvd_out: Please consult the multiboot documentation for proper configuration.
 *  iqfsp_out: Please consult the multiboot documentation for proper configuration.
 *  iqfsn_out: Please consult the multiboot documentation for proper configuration.
 *  ioutp_out: Please consult the multiboot documentation for proper configuration.
 *  ioutn_out: Please consult the multiboot documentation for proper configuration.
 *  qoutp_out: Please consult the multiboot documentation for proper configuration.
 *  qoutn_out: Please consult the multiboot documentation for proper configuration.
 *  ssb_out: Please consult the multiboot documentation for proper configuration.
 *  sclk_out: Please consult the multiboot documentation for proper configuration.
 *  dout0_out: Please consult the multiboot documentation for proper configuration.
 *  dclk0_out: Please consult the multiboot documentation for proper configuration.
 *  dfs0_out: Please consult the multiboot documentation for proper configuration.
 *  dout1_out: Please consult the multiboot documentation for proper configuration.
 *  dclk1_out: Please consult the multiboot documentation for proper configuration.
 *  dfs1_out: Please consult the multiboot documentation for proper configuration.
 *  iqclkp_out: Please consult the multiboot documentation for proper configuration.
 *  iqclkn_out: Please consult the multiboot documentation for proper configuration.
 *  diclk2_out: Please consult the multiboot documentation for proper configuration.
 *  difs2_out: Please consult the multiboot documentation for proper configuration.
 *  din3_out: Please consult the multiboot documentation for proper configuration.
 *  diclk3_out: Please consult the multiboot documentation for proper configuration.
 *  difs3_out: Please consult the multiboot documentation for proper configuration.
 *  din4_out: Please consult the multiboot documentation for proper configuration.
 *  diclk4_out: Please consult the multiboot documentation for proper configuration.
 *  difs4_out: Please consult the multiboot documentation for proper configuration.
 *  din0_out: Please consult the multiboot documentation for proper configuration.
 *  gp0_out: Please consult the multiboot documentation for proper configuration.
 *  gp1_out: Please consult the multiboot documentation for proper configuration.
 *  gp2_out: Please consult the multiboot documentation for proper configuration.
 *  din1_out: Please consult the multiboot documentation for proper configuration.
 *  diclk1_out: Please consult the multiboot documentation for proper configuration.
 *  difs1_out: Please consult the multiboot documentation for proper configuration.
 *  din2_out: Please consult the multiboot documentation for proper configuration.
 *  difsa_out: Please consult the multiboot documentation for proper configuration.
 *  diclka_out: Please consult the multiboot documentation for proper configuration.
 *  icp_out: Please consult the multiboot documentation for proper configuration.
 *  icn_out: Please consult the multiboot documentation for proper configuration.
 *  difs0_out: Please consult the multiboot documentation for proper configuration.
 *  diclk0_out: Please consult the multiboot documentation for proper configuration.
 *  zifq1_out: Please consult the multiboot documentation for proper configuration.
 *  zifi1_out: Please consult the multiboot documentation for proper configuration.
 *  clksel8: Please consult the multiboot documentation for proper configuration.
 *  spicfg3: Please consult the multiboot documentation for proper configuration.
 *  drvdel: Please consult the multiboot documentation for proper configuration.
 *  capdel: Please consult the multiboot documentation for proper configuration.
 *  spisctrl: Please consult the multiboot documentation for proper configuration.
 *  nvsclk: Please consult the multiboot documentation for proper configuration.
 *  nvssb: Please consult the multiboot documentation for proper configuration.
 *  nvmosi: Please consult the multiboot documentation for proper configuration.
 *  nvmiso: Please consult the multiboot documentation for proper configuration.
 *  diclk0: Please consult the multiboot documentation for proper configuration.
 *  difs0: Please consult the multiboot documentation for proper configuration.
 *  icn: Please consult the multiboot documentation for proper configuration.
 *  icp: Please consult the multiboot documentation for proper configuration.
 *  debugp: Please consult the multiboot documentation for proper configuration.
 *  debugn: Please consult the multiboot documentation for proper configuration.
 *  isfsp: Please consult the multiboot documentation for proper configuration.
 *  isfsn: Please consult the multiboot documentation for proper configuration.
 *  zifi0: Please consult the multiboot documentation for proper configuration.
 *  zifclk: Please consult the multiboot documentation for proper configuration.
 *  zifq0: Please consult the multiboot documentation for proper configuration.
 *  ziffs: Please consult the multiboot documentation for proper configuration.
 *  zifi1: Please consult the multiboot documentation for proper configuration.
 *  zifq1: Please consult the multiboot documentation for proper configuration.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_load_config__command(uint8_t icNum,uint8_t numchips, uint8_t chipid, uint8_t role, uint8_t mosi_in, uint8_t miso_in, uint8_t intb_in, uint8_t nvsclk_in, uint8_t nvssb_in, uint8_t nvmosi_in, uint8_t nvmiso_in, uint8_t rsvd_in, uint8_t iqfsp_in, uint8_t iqfsn_in, uint8_t ioutp_in, uint8_t ioutn_in, uint8_t qoutp_in, uint8_t qoutn_in, uint8_t ssb_in, uint8_t sclk_in, uint8_t dout0_in, uint8_t dclk0_in, uint8_t dfs0_in, uint8_t dout1_in, uint8_t dclk1_in, uint8_t dfs1_in, uint8_t iqclkp_in, uint8_t iqclkn_in, uint8_t diclk2_in, uint8_t difs2_in, uint8_t din3_in, uint8_t diclk3_in, uint8_t difs3_in, uint8_t din4_in, uint8_t diclk4_in, uint8_t difs4_in, uint8_t din0_in, uint8_t gp0_in, uint8_t gp1_in, uint8_t gp2_in, uint8_t din1_in, uint8_t diclk1_in, uint8_t difs1_in, uint8_t din2_in, uint8_t difsa_in, uint8_t diclka_in, uint8_t aout0_in, uint8_t aout1_in, uint8_t icp_in, uint8_t icn_in, uint8_t difs0_in, uint8_t diclk0_in, uint8_t zifq1_in, uint8_t zifi1_in, uint8_t mosi_out, uint8_t miso_out, uint8_t intb_out, uint8_t nvsclk_out, uint8_t nvssb_out, uint8_t nvmosi_out, uint8_t nvmiso_out, uint8_t rsvd_out, uint8_t iqfsp_out, uint8_t iqfsn_out, uint8_t ioutp_out, uint8_t ioutn_out, uint8_t qoutp_out, uint8_t qoutn_out, uint8_t ssb_out, uint8_t sclk_out, uint8_t dout0_out, uint8_t dclk0_out, uint8_t dfs0_out, uint8_t dout1_out, uint8_t dclk1_out, uint8_t dfs1_out, uint8_t iqclkp_out, uint8_t iqclkn_out, uint8_t diclk2_out, uint8_t difs2_out, uint8_t din3_out, uint8_t diclk3_out, uint8_t difs3_out, uint8_t din4_out, uint8_t diclk4_out, uint8_t difs4_out, uint8_t din0_out, uint8_t gp0_out, uint8_t gp1_out, uint8_t gp2_out, uint8_t din1_out, uint8_t diclk1_out, uint8_t difs1_out, uint8_t din2_out, uint8_t difsa_out, uint8_t diclka_out, uint8_t icp_out, uint8_t icn_out, uint8_t difs0_out, uint8_t diclk0_out, uint8_t zifq1_out, uint8_t zifi1_out, uint32_t clksel8, uint32_t spicfg3, uint32_t drvdel, uint32_t capdel, uint32_t spisctrl, uint32_t nvsclk, uint32_t nvssb, uint32_t nvmosi, uint32_t nvmiso, uint32_t diclk0, uint32_t difs0, uint32_t icn, uint32_t icp, uint32_t debugp, uint32_t debugn, uint32_t isfsp, uint32_t isfsn, uint32_t zifi0, uint32_t zifclk, uint32_t zifq0, uint32_t ziffs, uint32_t zifi1, uint32_t zifq1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_load_config(uint8_t icNum, uint8_t  numchips, uint8_t  chipid, uint8_t  role, uint8_t  mosi_in, uint8_t  miso_in, uint8_t  intb_in, uint8_t  nvsclk_in, uint8_t  nvssb_in, uint8_t  nvmosi_in, uint8_t  nvmiso_in, uint8_t  rsvd_in, uint8_t  iqfsp_in, uint8_t  iqfsn_in, uint8_t  ioutp_in, uint8_t  ioutn_in, uint8_t  qoutp_in, uint8_t  qoutn_in, uint8_t  ssb_in, uint8_t  sclk_in, uint8_t  dout0_in, uint8_t  dclk0_in, uint8_t  dfs0_in, uint8_t  dout1_in, uint8_t  dclk1_in, uint8_t  dfs1_in, uint8_t  iqclkp_in, uint8_t  iqclkn_in, uint8_t  diclk2_in, uint8_t  difs2_in, uint8_t  din3_in, uint8_t  diclk3_in, uint8_t  difs3_in, uint8_t  din4_in, uint8_t  diclk4_in, uint8_t  difs4_in, uint8_t  din0_in, uint8_t  gp0_in, uint8_t  gp1_in, uint8_t  gp2_in, uint8_t  din1_in, uint8_t  diclk1_in, uint8_t  difs1_in, uint8_t  din2_in, uint8_t  difsa_in, uint8_t  diclka_in, uint8_t  aout0_in, uint8_t  aout1_in, uint8_t  icp_in, uint8_t  icn_in, uint8_t  difs0_in, uint8_t  diclk0_in, uint8_t  zifq1_in, uint8_t  zifi1_in, uint8_t  mosi_out, uint8_t  miso_out, uint8_t  intb_out, uint8_t  nvsclk_out, uint8_t  nvssb_out, uint8_t  nvmosi_out, uint8_t  nvmiso_out, uint8_t  rsvd_out, uint8_t  iqfsp_out, uint8_t  iqfsn_out, uint8_t  ioutp_out, uint8_t  ioutn_out, uint8_t  qoutp_out, uint8_t  qoutn_out, uint8_t  ssb_out, uint8_t  sclk_out, uint8_t  dout0_out, uint8_t  dclk0_out, uint8_t  dfs0_out, uint8_t  dout1_out, uint8_t  dclk1_out, uint8_t  dfs1_out, uint8_t  iqclkp_out, uint8_t  iqclkn_out, uint8_t  diclk2_out, uint8_t  difs2_out, uint8_t  din3_out, uint8_t  diclk3_out, uint8_t  difs3_out, uint8_t  din4_out, uint8_t  diclk4_out, uint8_t  difs4_out, uint8_t  din0_out, uint8_t  gp0_out, uint8_t  gp1_out, uint8_t  gp2_out, uint8_t  din1_out, uint8_t  diclk1_out, uint8_t  difs1_out, uint8_t  din2_out, uint8_t  difsa_out, uint8_t  diclka_out, uint8_t  icp_out, uint8_t  icn_out, uint8_t  difs0_out, uint8_t  diclk0_out, uint8_t  zifq1_out, uint8_t  zifi1_out, uint32_t  clksel8, uint32_t  spicfg3, uint32_t  drvdel, uint32_t  capdel, uint32_t  spisctrl, uint32_t  nvsclk, uint32_t  nvssb, uint32_t  nvmosi, uint32_t  nvmiso, uint32_t  diclk0, uint32_t  difs0, uint32_t  icn, uint32_t  icp, uint32_t  debugp, uint32_t  debugn, uint32_t  isfsp, uint32_t  isfsn, uint32_t  zifi0, uint32_t  zifclk, uint32_t  zifq0, uint32_t  ziffs, uint32_t  zifi1, uint32_t  zifq1);


/*
 * Command: SI4796X_get_chip_info
 * 
 * Description:
 *   Reports information unique to a specific chip
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  info_type: This field specifies which type of information to report
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes returned for selected chip info field
 *  data: data
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_chip_info__command(uint8_t icNum,uint8_t info_type);


typedef struct _SI4796X_get_chip_info__data
{
	uint8_t LENGTH;
	uint8_t* DATA;
}SI4796X_get_chip_info__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_chip_info__reply(uint8_t icNum, SI4796X_get_chip_info__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_chip_info(uint8_t icNum, uint8_t  info_type, SI4796X_get_chip_info__data*  replyData);


/*
 * Command: SI4796X_key_exchange
 * 
 * Description:
 *   Pass in decryption key
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: Decryption key
 *  data_len (input): #elems in 'data' array.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_key_exchange__command(uint8_t icNum,uint8_t* data, uint16_t data_len);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_key_exchange(uint8_t icNum, uint8_t*  data, uint16_t  data_len);


/*
 * Command: SI4796X_get_func_info
 * 
 * Description:
 *   Returns the Function revision information of the device.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  verinfo0: Production version
 *  verinfo1: Major/milestone version digit
 *  verinfo2: Derivative version digit
 *  verinfo3: Reserved for future growth
 *  verinfo4: Minor version
 *  hifiinfo0: Still to be determined hifi info
 *  hifiinfo1: Still to be determined hifi info
 *  hifiinfo2: Still to be determined hifi info
 *  hifiinfo3: Still to be determined hifi info
 *  custinfo0: Reserved for Customer
 *  custinfo1: Reserved for Customer
 *  custinfo2: Reserved for Customer
 *  custinfo3: Reserved for Customer
 *  nosvn: If set the build was created with no SVN info. This image cannot be tracked back to the SVN repo.
 *  location: The location from which the image was built (Trunk, Branch or Tag).
 *  mixedrev: If set, the image was built with mixed revisions
 *  localmod: If set, the image has local modifications
 *  svnid: SVN ID from which the image was built.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_func_info__command(uint8_t icNum);


typedef struct _SI4796X_get_func_info__data
{
	uint8_t VERINFO0;
	uint8_t VERINFO1;
	uint8_t VERINFO2;
	uint8_t VERINFO3;
	uint8_t VERINFO4;
	uint8_t HIFIINFO0;
	uint8_t HIFIINFO1;
	uint8_t HIFIINFO2;
	uint8_t HIFIINFO3;
	uint8_t CUSTINFO0;
	uint8_t CUSTINFO1;
	uint8_t CUSTINFO2;
	uint8_t CUSTINFO3;
	uint8_t NOSVN;
	uint8_t LOCATION;
	uint8_t MIXEDREV;
	uint8_t LOCALMOD;
	uint32_t SVNID;
}SI4796X_get_func_info__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_func_info__reply(uint8_t icNum, SI4796X_get_func_info__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_func_info(uint8_t icNum, SI4796X_get_func_info__data*  replyData);


/*
 * Command: SI4796X_set_property
 * 
 * Description:
 *   Sets a property common to one or more commands. These are similar to parameters for a api command but are not expected to change frequently and may be controlled by higher layers of the user's software. Setting some properties may not cause the device to take immediate action, however the property will take affect once a command which uses it is issued.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  prop: Selects the property index to set. The available properties are determined by the part number and the current mode of operation.
 *  data: Value of the property. Acceptable values are dependent on the property index chosen.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_set_property__command(uint8_t icNum,uint16_t prop, uint16_t data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_set_property(uint8_t icNum, uint16_t  prop, uint16_t  data);


/*
 * Command: SI4796X_get_property
 * 
 * Description:
 *   Retrieve a property's value; The value will either be the default or the value set with SET_PROPERTY.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  count: Indicates the number of consecutive properties to access
 *  prop: Selects the property index to set. The available properties are determined by the part number and the current mode of operation.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: data description
 *  count (input): #elems in 'data' array.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_property__command(uint8_t icNum,uint8_t count, uint16_t prop);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_property__reply(uint8_t icNum, uint16_t* data, uint16_t count);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_property(uint8_t icNum, uint8_t  arg_count, uint16_t  prop, uint16_t*  data, uint16_t  rep_count);


/*
 * Command: SI4796X_agc_status
 * 
 * Description:
 *   Allows the host to gather information about the current status of all AGC loops
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tm_b3a_rf: (No Description)
 *  tm_fma_rf: (No Description)
 *  tm_am_ms: (No Description)
 *  tm_b3b_rf: (No Description)
 *  tm_fmb_rf: (No Description)
 *  tm_am_rf: (No Description)
 *  tm_if1: (No Description)
 *  tm_if0: (No Description)
 *  am_rf_index: Current AGC AM_RF Index
 *  am_rf_gain: Current AM_RF gain in dB
 *  fmb_rf_index: Current AGC FMB_RF Index
 *  fmb_rf_gain: Current FMB_RF gain in dB
 *  b3b_rf_index: Current AGC B3B_RF Index
 *  b3b_rf_gain: Current B3B_RF gain in dB
 *  am_ms_index: Current AGC AM_MS Index
 *  am_ms_gain: Current AM_MS gain in dB
 *  fma_rf_index: Current AGC FMA_RF Index
 *  fma_rf_gain: Current FMA_RF gain in dB
 *  b3a_rf_index: Current AGC B3A_RF Index
 *  b3a_rf_gain: Current B3A_RF gain in dB
 *  if0_index: Current AGC IF0 Index
 *  if0_gain: Current IF0 gain in dB
 *  if1_index: Current AGC IF1 Index
 *  if1_gain: Current IF1 gain in dB
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_agc_status__command(uint8_t icNum);


typedef struct _SI4796X_agc_status__data
{
	uint8_t TM_B3A_RF;
	uint8_t TM_FMA_RF;
	uint8_t TM_AM_MS;
	uint8_t TM_B3B_RF;
	uint8_t TM_FMB_RF;
	uint8_t TM_AM_RF;
	uint8_t TM_IF1;
	uint8_t TM_IF0;
	uint8_t AM_RF_INDEX;
	int8_t AM_RF_GAIN;
	uint8_t FMB_RF_INDEX;
	int8_t FMB_RF_GAIN;
	uint8_t B3B_RF_INDEX;
	int8_t B3B_RF_GAIN;
	uint8_t AM_MS_INDEX;
	int8_t AM_MS_GAIN;
	uint8_t FMA_RF_INDEX;
	int8_t FMA_RF_GAIN;
	uint8_t B3A_RF_INDEX;
	int8_t B3A_RF_GAIN;
	uint8_t IF0_INDEX;
	int8_t IF0_GAIN;
	uint8_t IF1_INDEX;
	int8_t IF1_GAIN;
}SI4796X_agc_status__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_agc_status__reply(uint8_t icNum, SI4796X_agc_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_agc_status(uint8_t icNum, SI4796X_agc_status__data*  replyData);


/*
 * Command: SI4796X_detail_status
 * 
 * Description:
 *   Retrieve miscellaneous status information.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  subcmd: DETAIL_STATUS subcommand.
 *  data: DETAIL_STATUS subcommand specific data.
 *  data_length (input): #elems in 'data' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: Byte containing DETAIL_STATUS subcommand reply
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_detail_status__command(uint8_t icNum,uint8_t subcmd, uint8_t* data, uint16_t data_length);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_detail_status__reply(uint8_t icNum, uint8_t* data);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_detail_status(uint8_t icNum, uint8_t  subcmd, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data);


/*
 * Command: SI4796X_hifi_jtag_unlock
 * 
 * Description:
 *   Configure the JTAG port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  jtag_sel: Selection of the HIFI jtag pins
 *  image_id: 128 bit value to identify this object
 *  image_id_len (input): #elems in 'image_id' array.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_jtag_unlock__command(uint8_t icNum,uint8_t jtag_sel, uint8_t* image_id, uint16_t image_id_len);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_jtag_unlock(uint8_t icNum, uint8_t  jtag_sel, uint8_t*  image_id, uint16_t  image_id_len);


/*
 * Command: SI4796X_hifi_pipe_reset
 * 
 * Description:
 *   Reset HIFI pipe 1 through 5
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  reset: DISABLED by default. When ENABLE, will Reset HIFI pipe 1 through 5
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_pipe_reset__command(uint8_t icNum,uint8_t reset);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_hifi_pipe_reset(uint8_t icNum, uint8_t  reset);


/*
 * Command: SI4796X_get_api
 * 
 * Description:
 *   Returns the API revision information of the device and capability bits
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  apiversion0: Production version
 *  apiversion1: Major/milestone version digit
 *  apiversion2: Derivative version digit
 *  apiversion3: Reserved for future growth
 *  dynamic_zif: Dynamic ZIF capabilities
 *  advanced_tuner: Advanced tuner capabilities
 *  advanced_audio: Advanced audio engine
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_api__command(uint8_t icNum);


typedef struct _SI4796X_get_api__data
{
	uint8_t APIVERSION0;
	uint8_t APIVERSION1;
	uint8_t APIVERSION2;
	uint8_t APIVERSION3;
	uint8_t DYNAMIC_ZIF;
	uint8_t ADVANCED_TUNER;
	uint8_t ADVANCED_AUDIO;
}SI4796X_get_api__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_api__reply(uint8_t icNum, SI4796X_get_api__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_get_api(uint8_t icNum, SI4796X_get_api__data*  replyData);


/*
 * Command: SI4796X_tuner
 * 
 * Description:
 *   Passes commands on to a given tuner instance.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  id: Tuner ID to communicate with.
 *  data: Subcmd argument bytes.
 *  data_length (input): #elems in 'data' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: Subcmd reply bytes.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_tuner__command(uint8_t icNum,uint8_t id, uint8_t* data, uint16_t data_length);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_tuner__reply(uint8_t icNum, uint8_t* data);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_tuner(uint8_t icNum, uint8_t  id, uint8_t*  arg_data, uint16_t  data_length, uint8_t*  rep_data);


/*
 * Command: SI4796X_dab_set_freq_list
 * 
 * Description:
 *   DAB_SET_FREQ_LIST command sets the DAB frequency table. The frequencies are in units of Hz.  The table can be populated with a single entry or a regional list (for example 5 or 6 entries).  It is recommended to make the list regional to increase scanning speed.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  num_freqs: The number of frequencies being programmed into the table.
 *  freq: Tune Frequency in Hz
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_set_freq_list__command(uint8_t icNum,uint8_t num_freqs, uint32_t* freq);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_set_freq_list(uint8_t icNum, uint8_t  num_freqs, uint32_t*  freq);


/*
 * Command: SI4796X_dab_get_freq_list
 * 
 * Description:
 *   DAB_GET_FREQ_LIST command gets the DAB frequency table. The frequencies are in units of Hz.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  num_freqs: The number of frequencies being programmed into the table.
 *  freq: Tune Frequency in Hz
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_get_freq_list__command(uint8_t icNum);


typedef struct _SI4796X_dab_get_freq_list__data
{
	uint8_t NUM_FREQS;
	uint32_t* FREQ;
}SI4796X_dab_get_freq_list__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_get_freq_list__reply(uint8_t icNum, SI4796X_dab_get_freq_list__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dab_get_freq_list(uint8_t icNum, SI4796X_dab_get_freq_list__data*  replyData);


/*
 * Command: SI4796X_pin_status
 * 
 * Description:
 *   Reports the current configuration of a group of pins See PIN_COMMANDS for a table showing pin groups and pin ID
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  group: Select which pin group to display the status
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  numpins: Reports how many pins are being reported on
 *  pin: (No Description)
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_status__command(uint8_t icNum,uint8_t group);


typedef struct _SI4796X_pin_status_pin__data
{
	uint8_t DEGLITCH;
	uint8_t INVERT;
	uint8_t INTERRUPT_TYPE;
	uint8_t DIRECTION;
	uint8_t GPI;
	uint8_t GPOUT;
	uint8_t SPDIFTX1;
	uint8_t SPDIFTX0;
	uint8_t I2STX4;
	uint8_t I2STX3;
	uint8_t I2STX2;
	uint8_t I2STX1;
	uint8_t I2STX0;
	uint8_t BLEND;
	uint8_t SPDIFRX1;
	uint8_t SPDIFRX0;
	uint8_t I2SRX4;
	uint8_t I2SRX3;
	uint8_t I2SRX2;
	uint8_t I2SRX1;
	uint8_t I2SRX0;
	uint8_t GPIB;
	uint8_t GPIA;
	uint8_t CLIP1;
	uint8_t CLIP0;
	uint8_t ICIN3;
	uint8_t ICIN2;
	uint8_t ICIN1;
	uint8_t ICIN0;
	uint8_t I2SCKFS;
	uint8_t DAC;
	uint8_t ICOUT4;
	uint8_t ICOUT3;
	uint8_t ICOUT2;
	uint8_t ICOUT1;
	uint8_t ICOUT0;
}SI4796X_pin_status_pin__data;
typedef struct _SI4796X_pin_status__data
{
	uint8_t NUMPINS;
	SI4796X_pin_status_pin__data* PIN;
}SI4796X_pin_status__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_status__reply(uint8_t icNum, SI4796X_pin_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_status(uint8_t icNum, uint8_t  group, SI4796X_pin_status__data*  replyData);


/*
 * Command: SI4796X_pin_config_hifi
 * 
 * Description:
 *   Configure a pin as a digital input to the HiFi.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pin_id_0: Pin ID, not pin number. Pin number is package specific.
 *  enable_0: Enable/Disable pin
 *  deglitch_0: Control of de-glitch logic
 *  invert_0: Control of pin logic when INTERRUPT_LEVEL=0.
 *  interrupt_type_0: Control of interrupt detection logic
 *  pin_id_1: Pin ID, not pin number. Pin number is package specific.
 *  enable_1: Enable/Disable pin
 *  deglitch_1: Control of de-glitch logic
 *  invert_1: Control of pin logic when INTERRUPT_LEVEL=0.
 *  interrupt_type_1: Control of interrupt detection logic
 *  pin_id_a: Pin ID, not pin number. Pin number is package specific.
 *  enable_a: Enable/Disable pin
 *  deglitch_a: Control of de-glitch logic
 *  invert_a: Control of pin logic when INTERRUPT_LEVEL=0.
 *  interrupt_type_a: Control of interrupt detection logic
 *  pin_id_b: Pin ID, not pin number. Pin number is package specific.
 *  enable_b: Enable/Disable pin
 *  deglitch_b: Control of de-glitch logic
 *  invert_b: Control of pin logic when INTERRUPT_LEVEL=0.
 *  interrupt_type_b: Control of interrupt detection logic
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_config_hifi__command(uint8_t icNum,uint8_t pin_id_0, uint8_t enable_0, uint8_t deglitch_0, uint8_t invert_0, uint8_t interrupt_type_0, uint8_t pin_id_1, uint8_t enable_1, uint8_t deglitch_1, uint8_t invert_1, uint8_t interrupt_type_1, uint8_t pin_id_a, uint8_t enable_a, uint8_t deglitch_a, uint8_t invert_a, uint8_t interrupt_type_a, uint8_t pin_id_b, uint8_t enable_b, uint8_t deglitch_b, uint8_t invert_b, uint8_t interrupt_type_b);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_config_hifi(uint8_t icNum, uint8_t  pin_id_0, uint8_t  enable_0, uint8_t  deglitch_0, uint8_t  invert_0, uint8_t  interrupt_type_0, uint8_t  pin_id_1, uint8_t  enable_1, uint8_t  deglitch_1, uint8_t  invert_1, uint8_t  interrupt_type_1, uint8_t  pin_id_a, uint8_t  enable_a, uint8_t  deglitch_a, uint8_t  invert_a, uint8_t  interrupt_type_a, uint8_t  pin_id_b, uint8_t  enable_b, uint8_t  deglitch_b, uint8_t  invert_b, uint8_t  interrupt_type_b);


typedef struct SI4796X_pin_config_gpo_pin__data
{
	uint8_t PIN_ID;
	uint8_t ENABLE;
	uint8_t DRIVE;
}SI4796X_pin_config_gpo_pin__data;
/*
 * Command: SI4796X_pin_config_gpo
 * 
 * Description:
 *   Configure and drive a pin as a general purpose output.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  numpins: How many pins are being adjusted.
 *  pin: (No Description)
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_config_gpo__command(uint8_t icNum,uint8_t numpins, SI4796X_pin_config_gpo_pin__data* pin);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_pin_config_gpo(uint8_t icNum, uint8_t  numpins, SI4796X_pin_config_gpo_pin__data*  pin);


/*
 * Command: SI4796X_i2stx0_config
 * 
 * Description:
 *   Sets up the parameters of I2S transmit port 0.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2stx1_config
 * 
 * Description:
 *   Sets up the parameters of I2S transmit port 1.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2stx2_config
 * 
 * Description:
 *   Sets up the parameters of I2S transmit port 2.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx2_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx2_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2stx3_config
 * 
 * Description:
 *   Sets up the parameters of I2S transmit port 3.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx3_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx3_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2stx4_config
 * 
 * Description:
 *   Sets up the parameters of I2S transmit port 4.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  lvdsdata: Enable I 2 S over LVDS on data0/data1.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx4_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t lvdsdata, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2stx4_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  lvdsdata, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2srx0_config
 * 
 * Description:
 *   Sets up the parameters of I2S receive port 0.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2srx1_config
 * 
 * Description:
 *   Sets up the parameters of I2S receive port 1.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2srx2_config
 * 
 * Description:
 *   Sets up the parameters of I2S receive port 2.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx2_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx2_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2srx3_config
 * 
 * Description:
 *   Sets up the parameters of I2S receive port 3.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx3_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx3_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2srx4_config
 * 
 * Description:
 *   Sets up the parameters of I2S receive port 4.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  enable: Enable the port after configuring.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx4_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t whitencontrol, uint8_t enable, uint8_t purpose, uint8_t force, uint8_t master, uint8_t clkfs, uint8_t data0, uint8_t fill, uint8_t slot_size, uint8_t sample_size, uint8_t bitorder, uint8_t swap, uint8_t clkinv, uint8_t framing_mode, uint8_t clockdomain, uint8_t invertfs, uint8_t hiz, uint8_t length, uint8_t data1);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2srx4_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  whitencontrol, uint8_t  enable, uint8_t  purpose, uint8_t  force, uint8_t  master, uint8_t  clkfs, uint8_t  data0, uint8_t  fill, uint8_t  slot_size, uint8_t  sample_size, uint8_t  bitorder, uint8_t  swap, uint8_t  clkinv, uint8_t  framing_mode, uint8_t  clockdomain, uint8_t  invertfs, uint8_t  hiz, uint8_t  length, uint8_t  data1);


/*
 * Command: SI4796X_i2s_control
 * 
 * Description:
 *   Enable/Disable/Query the parameters of I2S ports
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  port: Select which port to act upon/view settings for
 *  mode: Current port configuration
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  whitencontrol: In master mode, setting this option enables/disables clock whitening, and thus configures the amount of jitter on the output clock. Setting this option to disable will produce no jitter and disable the clock whitening, which may result in RF spurs. Please note that configuring multiple I2S ports with different whiten settings will result in burning of a new high precision divider for each one of those settings.
 *  purpose: Select the internal connectivity of the port
 *  force: Forces the clk and FS pin on even if the port is disabled (when mastering).
 *  master: Master or slave mode.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  clkfs: Configure the clock and frame sync pin pair that this block is connected to.   Note that when sharing the clock and frame pins between multiple ports, only one port should be configured as master and all others as slave.
 *  lvdsdata: Enable I 2 S over LVDS on data0/data1.
 *  data0: Data pin
 *  fill: Specifies the type of data to send during unused bit cycles. If the DFS frame size is larger than the sample size, there may be some unused bit cycles after the last valid data bit is sent, before the next DFS edge occurs. This field configures what data is sent during those unused cycles. The size of the valid data field is set with the SAMP[4:0] field. The purpose of this feature is to change the spectral content of the data output signal(s), to help with spur mitigation.
 *  slot_size: Width of the data channel. This is used to determine how many clock cycles there are per frame. For example, if slot_size = 16 and 2-channel I 2 S is being used, then there will be 16*2 = 32 bits per frame. The sample_size must be less than equal to the slot_size.
 *  sample_size: Number of bits in a sample. Only the specified number of bits per sample are used. The value of any bits sent above the sample size are set by FILL. The sample_size must be less than or equal to the slot_size.
 *  bitorder: Serial bit order. Data can be sent in MSB-to-LSB order (most common), or in reversed order.  BITORDER
 *  swap: Swap positions of left and right channels. This bit chooses which audio channel is sent as the â€œfirst mono sampleâ€� and â€œsecond mono sampleâ€� in various formats  SWAP
 *  clkinv: Invert the bit clock input to the block (either from a pin or internal clock generator). Typically, the bit clock is equivalent to the incoming signal in the DCLK pin. The optional inversion is shown as an XOR gate in the block diagram (also see clock_muxing spec sheet).  CLKINV
 *  framing_mode: Determines when the data is transmitted relative to frame sync. Only some framing modes are compatible with TDM. Modes that are not compatible are indicated as such.
 *  clockdomain: Specifies a clock domain enumeration to which this I2S port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  invertfs: When the frame clock is asymmetrical, setting this bit will invert the shape of the frame clock from the 'normal' behavior, and it will invert the frame trigger to the internal I2S block to start the frame on the opposite edge.
 *  mode: Current port configuration
 *  hiz: When TDM is enabled, this bit controls what happens on the data output pins during unselected slots, when the chip is not actively sending data. In the diagram below, the "last slot bit" is also the LSB data bit if the slot_size = the sample_size.  FORMAT
 *  length: The total number of TDM slots, minus one. This field is valid only in TDM framing modes..
 *  data1: Second data input/output pin
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2s_control__command(uint8_t icNum,uint8_t port, uint8_t mode);


typedef struct _SI4796X_i2s_control__data
{
	uint8_t SAMPLE_RATE;
	uint8_t WHITENCONTROL;
	uint8_t PURPOSE;
	uint8_t FORCE;
	uint8_t MASTER;
	uint8_t CLKFS;
	uint8_t LVDSDATA;
	uint8_t DATA0;
	uint8_t FILL;
	uint8_t SLOT_SIZE;
	uint8_t SAMPLE_SIZE;
	uint8_t BITORDER;
	uint8_t SWAP;
	uint8_t CLKINV;
	uint8_t FRAMING_MODE;
	uint8_t CLOCKDOMAIN;
	uint8_t INVERTFS;
	uint8_t MODE;
	uint8_t HIZ;
	uint8_t LENGTH;
	uint8_t DATA1;
}SI4796X_i2s_control__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2s_control__reply(uint8_t icNum, SI4796X_i2s_control__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_i2s_control(uint8_t icNum, uint8_t  port, uint8_t  mode, SI4796X_i2s_control__data*  replyData);


/*
 * Command: SI4796X_spdiftx0_config
 * 
 * Description:
 *   Sets up the parameters of S/PDIF transmit port 0.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  pin: S/PDIF port 0 data will be transmitted on this pin.
 *  clockdomain: Specifies a clock domain enumeration to which this spdif port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  parity: Parity selection.
 *  sample_size: Number of bits in sample.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdiftx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdiftx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size);


/*
 * Command: SI4796X_spdiftx1_config
 * 
 * Description:
 *   Sets up the parameters of S/PDIF transmit port 1.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  pin: S/PDIF port 1 data will be transmitted on this pin.
 *  clockdomain: Specifies a clock domain enumeration to which this spdif port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  parity: Parity selection.
 *  sample_size: Number of bits in sample.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdiftx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdiftx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size);


/*
 * Command: SI4796X_spdifrx0_config
 * 
 * Description:
 *   Sets up the parameters of S/PDIF receive port 0.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  pin: S/PDIF data will be received on this pin.
 *  clockdomain: Specifies a clock domain enumeration to which this spdif port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  parity: Parity selection.
 *  sample_size: Number of bits in sample.
 *  parity_err: Data handling when parity error has occurred.
 *  valid_err: Data handling when data is flagged invalid.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdifrx0_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size, uint8_t parity_err, uint8_t valid_err);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdifrx0_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size, uint8_t  parity_err, uint8_t  valid_err);


/*
 * Command: SI4796X_spdifrx1_config
 * 
 * Description:
 *   Sets up the parameters of S/PDIF receive port 1.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  pin: S/PDIF data will be received on this pin.
 *  clockdomain: Specifies a clock domain enumeration to which this spdif port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  parity: Parity selection.
 *  sample_size: Number of bits in sample.
 *  parity_err: Data handling when parity error has occurred.
 *  valid_err: Data handling when data is flagged invalid.
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdifrx1_config__command(uint8_t icNum,uint8_t sample_rate, uint8_t pin, uint8_t clockdomain, uint8_t parity, uint8_t sample_size, uint8_t parity_err, uint8_t valid_err);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdifrx1_config(uint8_t icNum, uint8_t  sample_rate, uint8_t  pin, uint8_t  clockdomain, uint8_t  parity, uint8_t  sample_size, uint8_t  parity_err, uint8_t  valid_err);


/*
 * Command: SI4796X_spdif_control
 * 
 * Description:
 *   Reports current configuration settings of the requested S/PDIF port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  port: S/PDIF port to query settings.
 *  mode: Current port configuration
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sample_rate: Specifies the sample rate
 *  pin: Pin that this S/PDIF port is assigned to.
 *  clockdomain: Specifies a clock domain enumeration to which this spdif port is connected. 0 is reserved for ports that are synchronous with the audio synth PLL. Other clock domain enumerations are defined by the system designer.
 *  parity: Parity selection.
 *  sample_size: Number of bits in sample.
 *  parity_err: Data handling when parity error has occurred.
 *  valid_err: Data handling when data is flagged invalid.
 *  mode: Current port configuration
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdif_control__command(uint8_t icNum,uint8_t port, uint8_t mode);


typedef struct _SI4796X_spdif_control__data
{
	uint8_t SAMPLE_RATE;
	uint8_t PIN;
	uint8_t CLOCKDOMAIN;
	uint8_t PARITY;
	uint8_t SAMPLE_SIZE;
	uint8_t PARITY_ERR;
	uint8_t VALID_ERR;
	uint8_t MODE;
}SI4796X_spdif_control__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdif_control__reply(uint8_t icNum, SI4796X_spdif_control__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_spdif_control(uint8_t icNum, uint8_t  port, uint8_t  mode, SI4796X_spdif_control__data*  replyData);


/*
 * Command: SI4796X_dac_config
 * 
 * Description:
 *   Configure the on-chip digital-to-analog converters.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  pumode01: Control the speed of the DAC power-down and power-up process. A fast pop will cause a short audio pop that should be muted by the host.
 *  dac01_mode: Enable/Disable a pair of dacs and select internal hardware connectivity
 *  pumode23: Control the speed of the DAC power-down and power-up process. A fast pop will cause a short audio pop that should be muted by the host.
 *  dac23_mode: Enable/Disable a pair of dacs and select internal hardware connectivity
 *  pumode45: Control the speed of the DAC power-down and power-up process. A fast pop will cause a short audio pop that should be muted by the host.
 *  dac45_mode: Enable/Disable a pair of dacs and select internal hardware connectivity
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dac_config__command(uint8_t icNum,uint8_t pumode01, uint8_t dac01_mode, uint8_t pumode23, uint8_t dac23_mode, uint8_t pumode45, uint8_t dac45_mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dac_config(uint8_t icNum, uint8_t  pumode01, uint8_t  dac01_mode, uint8_t  pumode23, uint8_t  dac23_mode, uint8_t  pumode45, uint8_t  dac45_mode);


/*
 * Command: SI4796X_dac_status
 * 
 * Description:
 *   Returns the current DAC configuration.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  dac01_mode: Enable/Disable a pair of dacs and select internal hardware connectivity
 *  dac23_mode: Enable/Disable a pair of dacs and select internal hardware connectivity
 *  dac45_mode: Enable/Disable a pair of dacs and select internal hardware connectivity
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dac_status__command(uint8_t icNum);


typedef struct _SI4796X_dac_status__data
{
	uint8_t DAC01_MODE;
	uint8_t DAC23_MODE;
	uint8_t DAC45_MODE;
}SI4796X_dac_status__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dac_status__reply(uint8_t icNum, SI4796X_dac_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_dac_status(uint8_t icNum, SI4796X_dac_status__data*  replyData);


/*
 * Command: SI4796X_adc_connect
 * 
 * Description:
 *   Connect analog input pins to on-chip ADCs
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  adc_channel: Audio ADC channel
 *  source_pins: Differential input pair pins [AIN0P/N..AIN5P/N]
 *  gain: ADC gain
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_connect__command(uint8_t icNum,uint8_t adc_channel, uint8_t source_pins, uint8_t gain);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_connect(uint8_t icNum, uint8_t  adc_channel, uint8_t  source_pins, uint8_t  gain);


/*
 * Command: SI4796X_adc_disconnect
 * 
 * Description:
 *   Disconnect analog input pins from the on-chip ADCs
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  adc_channel: Audio ADC channel
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_disconnect__command(uint8_t icNum,uint8_t adc_channel);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_disconnect(uint8_t icNum, uint8_t  adc_channel);


/*
 * Command: SI4796X_adc_status
 * 
 * Description:
 *   Check the status of the audio ADCs
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  adc: (No Description)
 * 
 */
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_status__command(uint8_t icNum);


typedef struct _SI4796X_adc_status_adc__data
{
	uint8_t SOURCE_PINS;
	uint8_t GAIN;
}SI4796X_adc_status_adc__data;

R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_status__reply(uint8_t icNum, SI4796X_adc_status_adc__data* adc);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_FW_API_EXT RETURN_CODE SI4796X_adc_status(uint8_t icNum, SI4796X_adc_status_adc__data*  adc);

#ifdef __cplusplus
} // extern C
#endif

#ifdef _r_4_1_si4796x_firmware_api_h_global_		/* 210426 SGsoft */
RETURN_CODE (* SI4796X_HifiCmdCommandFunction[4]) (uint8_t icNum, uint8_t packet_count, uint16_t length, uint8_t* data) = 
{
    SI4796X_hifi_cmd0__command,
    SI4796X_hifi_cmd1__command,
    SI4796X_hifi_cmd2__command,
    SI4796X_hifi_cmd3__command
};
RETURN_CODE (* SI4796X_HifiRespCommandFunction[4]) (uint8_t icNum) = 
{
    SI4796X_hifi_resp0__command,
    SI4796X_hifi_resp1__command,
    SI4796X_hifi_resp2__command,
    SI4796X_hifi_resp3__command
};
RETURN_CODE (* SI4796X_HifiRespReplyFunction[4]) (uint8_t icNum, SI4796X_hifi_resp__data*) = 
{
    SI4796X_hifi_resp0__reply,
    SI4796X_hifi_resp1__reply,
    SI4796X_hifi_resp2__reply,
    SI4796X_hifi_resp3__reply
};
#else
extern RETURN_CODE(*SI4796X_HifiCmdCommandFunction[4]) (uint8_t icNum, uint8_t packet_count, uint16_t length, uint8_t* data);
extern RETURN_CODE(*SI4796X_HifiRespCommandFunction[4]) (uint8_t icNum);
extern RETURN_CODE(*SI4796X_HifiRespReplyFunction[4]) (uint8_t icNum, SI4796X_hifi_resp__data*);
#endif	/*_r_4_1_si4796x_firmware_api_h_global_*/

#endif
