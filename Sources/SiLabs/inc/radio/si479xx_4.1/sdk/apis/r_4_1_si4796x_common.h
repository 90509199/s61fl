/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef _si4796X_common_h_
#define _si4796X_common_h_

#ifdef _r_4_1_si4796x_common_h_global_		/* 210419 SGsoft */
#define R_4_1_SI4796X_COMMON_EXT
#else
#define R_4_1_SI4796X_COMMON_EXT	extern
#endif	/*_r_4_1_si4796x_common_h_global_*/

#if 0		/* 210630 SGsoft */
#include <malloc.h>
#endif	/*0*/

#if 0
#include "common_types.h"
#include "platform_options.h"
#include "all_apis.h"
#endif	/*0*/

// command buffer length - this must be evenly divisible by 4
#define SI4796X_CMD_BUFFER_LENGTH (256)	

//8k DAB max payload + Digital Service Data Header - (this must be evenly divisible by 4)
#define SI4796X_RPY_BUFFER_LENGTH (8192 + 0)	



typedef struct
{
    uint8_t PIPE;
    uint8_t PACKET_COUNT;
    uint16_t LENGTH;
    uint8_t* DATA;
}SI4796X_hifi_resp__data;

#define SI4796X_MAX_NUM_PIPES 4


R_4_1_SI4796X_COMMON_EXT RETURN_CODE SI4796X_TUNER_TUNER(uint8_t icNum, uint8_t id, uint16_t length, uint8_t* subcmd);

R_4_1_SI4796X_COMMON_EXT RETURN_CODE TUNER_SUBCMD(uint8_t icNum, uint8_t id, uint16_t length, uint8_t* subcmd);


#endif
