/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
 
#ifndef si479xx_detail_status_api_h_
#define si479xx_detail_status_api_h_

#ifdef _r_4_1_si479xx_detail_status_api_h_global_		/* 210419 SGsoft */
#define R_4_1_SI479XX_DS_API_EXT
#else
#define R_4_1_SI479XX_DS_API_EXT	extern
#endif	/*_r_4_1_si479xx_detail_status_api_h_global_*/

///#include "si479xx_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Command: SI479XX_DETAIL_STATUS_die_temperature
 * 
 * Description:
 *   Returns the die temperature in increments of degrees C
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  temperature: Die temperature
 * 
 */
R_4_1_SI479XX_DS_API_EXT RETURN_CODE SI479XX_DETAIL_STATUS_die_temperature__command(uint8_t icNum);



R_4_1_SI479XX_DS_API_EXT RETURN_CODE SI479XX_DETAIL_STATUS_die_temperature__reply(uint8_t icNum, int16_t* temperature);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_DS_API_EXT RETURN_CODE SI479XX_DETAIL_STATUS_die_temperature(uint8_t icNum, int16_t*  temperature);


/*
 * Command: SI479XX_DETAIL_STATUS_swerr_info
 * 
 * Description:
 *   Returns informational codes for reporting back to Silicon Labs for diagnostics for the cause of an SWERR.  Only some sources of SWERR will be able to access  this command.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  last_swerr: Returns a code indicating the last cause of the SWERR bit being set to 1. It is possible for more than one to occur, but only the last cause is readable here. If no other command has been sent prior to the SWERR bit getting set, this same information is available in the first word of the reply stream after the STATUS word. In the case of HardFault and OSError, that is the only place to see this code.
 *  info0: Diagnostics for 0
 *  info1: Diagnostics for 1
 *  info2: Diagnostics for 2
 *  info3: Diagnostics for 3
 *  info4: Diagnostics for 4
 *  info5: Diagnostics for 5
 *  info6: Diagnostics for 6
 * 
 */
R_4_1_SI479XX_DS_API_EXT RETURN_CODE SI479XX_DETAIL_STATUS_swerr_info__command(uint8_t icNum);


typedef struct _SI479XX_DETAIL_STATUS_swerr_info__data
{
	uint32_t LAST_SWERR;
	uint32_t INFO0;
	uint32_t INFO1;
	uint32_t INFO2;
	uint32_t INFO3;
	uint32_t INFO4;
	uint32_t INFO5;
	uint32_t INFO6;
}SI479XX_DETAIL_STATUS_swerr_info__data;

R_4_1_SI479XX_DS_API_EXT RETURN_CODE SI479XX_DETAIL_STATUS_swerr_info__reply(uint8_t icNum, SI479XX_DETAIL_STATUS_swerr_info__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_DS_API_EXT RETURN_CODE SI479XX_DETAIL_STATUS_swerr_info(uint8_t icNum, SI479XX_DETAIL_STATUS_swerr_info__data*  replyData);


#ifdef __cplusplus
} // extern C
#endif

#endif
