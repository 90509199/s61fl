/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
 
 
 
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32 0x2
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_LENGTH 0xF
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_LENGTH 0x4
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_MSB 31
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_LSB 0
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_INDEX 0x7
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_ADDR_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[7], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_MSB 31
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_LSB 0
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_MAX 4096
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_MIN 4
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_INDEX 0xB
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_ARG_SIZE_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[11], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_MSB 31
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_LSB 0
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_INDEX 0x4
#define SI479XX_FLASH_UTIL_CMD_CHECK_CRC32_REP_CRC32_CALC_value _swap_32((((reply_arg_u32[1]))))


#define SI479XX_FLASH_UTIL_CMD_GET_PROP 0x11
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_LENGTH 0x3
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_LENGTH 0x2
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_MASK 0xFFFF
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_MSB 15
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_LSB 0
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_INDEX 0x1
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_ARG_PROP_ID_write_u16(x) do {uint16_t tmp = _swap_16(x); CpyMemory(&cmd_arg[1], &tmp, 2); } while(0)
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_MASK 0xFFFF
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_MSB 15
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_LSB 0
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_INDEX 0x4
#define SI479XX_FLASH_UTIL_CMD_GET_PROP_REP_PROP_VAL_value _swap_16((((reply_arg_u16[2]))))


#define SI479XX_FLASH_UTIL_CMD_SET_PROP 0x10
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_LENGTH 0x3
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_REP_LENGTH 0x0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_SIZE 4
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_MASK 0xFFFF
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_MSB 15
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_LSB 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_0_INDEX 0x3
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_N_INDEX(n) ((SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_0_INDEX) + (SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_SIZE * (n)))
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_0_write_u16(x) do { uint16_t tmp = _swap_16(x);  (CpyMemory(&cmd_arg[SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_0_INDEX], &tmp, 2));  } while(0)
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_N_write_u16(n,x) do { uint16_t tmp = _swap_16(x);  (CpyMemory(&cmd_arg[SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_ID_N_INDEX(n)], &tmp, 2));   } while(0)
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_MASK 0xFFFF
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_MSB 15
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_LSB 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_0_INDEX 0x5
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_N_INDEX(n) ((SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_0_INDEX) + (SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_SIZE * (n)))
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_0_write_u16(x) do { uint16_t tmp = _swap_16(x);  (CpyMemory(&cmd_arg[SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_0_INDEX], &tmp, 2));  } while(0)
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_N_write_u16(n,x) do { uint16_t tmp = _swap_16(x);  (CpyMemory(&cmd_arg[SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_PROP_VAL_N_INDEX(n)], &tmp, 2));   } while(0)
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_MASK 0xFFFF
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_MSB 15
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_LSB 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_MAX 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_MIN 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_INDEX 0x2B
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_END_0X0000_write_u16(x) do {uint16_t tmp = _swap_16(x); CpyMemory(&cmd_arg[43], &tmp, 2); } while(0)
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_MASK 0x0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_MSB 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_LSB 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_MAX 65535
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_MIN 0
#define SI479XX_FLASH_UTIL_CMD_SET_PROP_ARG_PROPSET_LENGTH_INDEX 0x0


#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK 0xE0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_LENGTH 0xB
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_LENGTH 0x0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_MSB 31
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_LSB 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_INDEX 0x3
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_ADDR_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[3], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MSB 31
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_LSB 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MAX 512
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_MIN 4
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_INDEX 0x7
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_ARG_SIZE_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[7], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_MSB 7
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_LSB 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_MAX 255
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_MIN 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_0_INDEX 0x4
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_N_INDEX(n) ((SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_0_INDEX) + (n))
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_0_value (((reply_arg[4])))
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_N_value(n) (((reply_arg[(SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_DATA_N_INDEX((n)))])))
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_MASK 0x0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_MSB 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_LSB 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_MAX 65535
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_MIN 0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_INDEX 0x0
#define SI479XX_FLASH_UTIL_CMD_READ_BLOCK_REP_SIZE_value _swap_16((*((uint16_t*)(&reply_arg[]))))


#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096 0xF4
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_LENGTH 0x3
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_REP_LENGTH 0x0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_MASK 0xFFFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_MSB 15
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_INDEX 0x1
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_SECTOR_write_u16(x) do {uint16_t tmp = _swap_16(x); CpyMemory(&cmd_arg[1], &tmp, 2); } while(0)
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_MSB 7
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_MAX 255
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_MIN 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_0_INDEX 0x3
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_N_INDEX(n) ((SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_0_INDEX) + (n) * (1))
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_0_write_u8(x) (cmd_arg[3] = (x))
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_N_write_u8(n,x) (cmd_arg[SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_N_INDEX(n)] = (x))
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_TYPE uint16_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_SIZE 16
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_MASK 0x0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_MSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_MAX 65535
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_MIN 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_SECTOR_4096_ARG_DATA_LENGTH_INDEX 0x0


#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR 0xFE
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_LENGTH 0x7
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_REP_LENGTH 0x0
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_MSB 31
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_LSB 0
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_INDEX 0x3
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_ADDR_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[3], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_MSB 7
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_LSB 0
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_MAX 192
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_MIN 192
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_INDEX 0x1
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XC0_write_u8(x) (cmd_arg[1] = (x))
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_MSB 7
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_LSB 0
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_MAX 222
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_MIN 222
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_INDEX 0x2
#define SI479XX_FLASH_UTIL_CMD_ERASE_SECTOR_ARG_0XDE_write_u8(x) (cmd_arg[2] = (x))


#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP 0xFF
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_LENGTH 0x3
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_REP_LENGTH 0x0
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_MSB 7
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_LSB 0
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_MAX 222
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_MIN 222
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_INDEX 0x1
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XDE_write_u8(x) (cmd_arg[1] = (x))
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_MSB 7
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_LSB 0
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_MAX 192
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_MIN 192
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_INDEX 0x2
#define SI479XX_FLASH_UTIL_CMD_ERASE_CHIP_ARG_0XC0_write_u8(x) (cmd_arg[2] = (x))


#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK 0xF0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_LENGTH 0xF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_REP_LENGTH 0x0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_MSB 31
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_INDEX 0x7
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_ADDR_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[7], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_TYPE uint32_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_SIZE 32
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_MASK 0xFFFFFFFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_MSB 31
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_MAX 4096
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_MIN 4
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_INDEX 0xB
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_SIZE_write_u32(x) do {uint32_t tmp = _swap_32(x); CpyMemory(&cmd_arg[11], &tmp, 4); } while(0)
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_MSB 7
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_MAX 255
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_MIN 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_0_INDEX 0xF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_N_INDEX(n) ((SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_0_INDEX) + (n) * (1))
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_0_write_u8(x) (cmd_arg[15] = (x))
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_N_write_u8(n,x) (cmd_arg[SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_DATA_N_INDEX(n)] = (x))
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_MSB 7
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_MAX 12
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_MIN 12
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_INDEX 0x1
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0X0C_write_u8(x) (cmd_arg[1] = (x))
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_TYPE uint8_t
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_SIZE 8
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_MASK 0xFF
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_MSB 7
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_LSB 0
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_MAX 237
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_MIN 237
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_INDEX 0x2
#define SI479XX_FLASH_UTIL_CMD_WRITE_BLOCK_ARG_0XED_write_u8(x) (cmd_arg[2] = (x))


#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_INDEX 0x1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_GROUP 0x0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_ADDR 0x1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_DEFAULT 0x0014
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_CLOCK_FREQ_MHZ_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_CLOCK_FREQ_MHZ_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_CLOCK_FREQ_MHZ_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_CLOCK_FREQ_MHZ_MAX 65
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_CLOCK_FREQ_MHZ_MIN 3
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_CLOCK_FREQ_MHZ_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_INDEX 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_GROUP 0x0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_ADDR 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_DEFAULT 0x0000
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_CLK_MODE_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_CLK_MODE_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_CLK_MODE_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_CLK_MODE_ENUM_ZERO 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_CLK_MODE_ENUM_THREE 3
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_CLK_MODE_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_MODE_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_INDEX 0x3
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_GROUP 0x0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_ADDR 0x3
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DEFAULT 0x0000
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DUAL_READ_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DUAL_READ_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DUAL_READ_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DUAL_READ_ENUM_STANDARD 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DUAL_READ_ENUM_DUAL 1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_DUAL_READ_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_INDEX 0x1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_GROUP 0x1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_ADDR 0x101
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_DEFAULT 0x0003
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_READ_CMD_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_READ_CMD_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_READ_CMD_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_READ_CMD_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_READ_CMD_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_INDEX 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_GROUP 0x1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_ADDR 0x102
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_DEFAULT 0x000B
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_HIGH_SPEED_READ_CMD_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_HIGH_SPEED_READ_CMD_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_HIGH_SPEED_READ_CMD_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_HIGH_SPEED_READ_CMD_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_INDEX 0x1
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_GROUP 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_ADDR 0x201
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_DEFAULT 0x0002
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_WRITE_CMD_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_WRITE_CMD_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_WRITE_CMD_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_WRITE_CMD_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_CMD_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_INDEX 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_GROUP 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ADDR 0x202
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_DEFAULT 0x0020
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ERASE_SECTOR_CMD_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ERASE_SECTOR_CMD_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ERASE_SECTOR_CMD_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_ERASE_SECTOR_CMD_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_CMD_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_INDEX 0x3
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_GROUP 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ADDR 0x203
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_DEFAULT 0x0004
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ERASE_SECTOR_SIZE_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ERASE_SECTOR_SIZE_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ERASE_SECTOR_SIZE_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_ERASE_SECTOR_SIZE_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_SECTOR_SIZE_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_INDEX 0x4
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_GROUP 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ADDR 0x204
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_DEFAULT 0x00C7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ERASE_CHIP_CMD_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ERASE_CHIP_CMD_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ERASE_CHIP_CMD_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_ERASE_CHIP_CMD_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_ERASE_CHIP_value()) & 0x00FF))

#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_INDEX 0x5
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_GROUP 0x2
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_ADDR 0x205
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_value() (sys_prop_ReturnProperty(SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_GROUP,SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_INDEX))
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_MASK 0x00FF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_DEFAULT 0x0000
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_WRITE_PROTECT_MASK 0xFF
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_WRITE_PROTECT_MSB 7
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_WRITE_PROTECT_LSB 0
#define SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_WRITE_PROTECT_value() (((SI479XX_FLASH_UTIL_PROP_FLASH_LOAD_WRITE_WRITE_PROTECT_value()) & 0x00FF))

