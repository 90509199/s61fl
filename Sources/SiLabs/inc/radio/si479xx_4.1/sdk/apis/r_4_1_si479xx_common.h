/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef _si479xx_common_h_
#define _si479xx_common_h_

#ifdef _r_4_1_si479xx_common_h_global_		/* 210419 SGsoft */
#define R_4_1_SI479XX_COMMON_EXT
#else
#define R_4_1_SI479XX_COMMON_EXT	extern
#endif	/*_r_4_1_si479xx_common_h_global_*/

#if 0		/* 210630 SGsoft */
#include <malloc.h>
#endif	/*0*/

#if 0
#include "common_types.h"
#include "platform_options.h"
#include "all_apis.h"
#endif	/*0*/

// for definition of TUNER_SUBCMD
///#include "si4796x_common.h"

#if 0		/* 210630 SGsoft */
// command buffer length - this must be evenly divisible by 4
#define SI479XX_CMD_BUFFER_LENGTH (4096 + 32)	

//8k DAB max payload + Digital Service Data Header - (this must be evenly divisible by 4)
#define SI479XX_RPY_BUFFER_LENGTH (8192 + 0)	
#endif	/*0*/

/* These functions are really meant only for the generated SDK. */
#define SI479XX_MAX_NUM_PIPES 4


R_4_1_SI479XX_COMMON_EXT RETURN_CODE DETAIL_STATUS_SUB(uint8_t icNum, uint16_t length, uint8_t* subcmd);

R_4_1_SI479XX_COMMON_EXT RETURN_CODE SI479XX_TUNER_TUNER(uint8_t icNum, uint8_t id, uint16_t length, uint8_t* subcmd);


R_4_1_SI479XX_COMMON_EXT RETURN_CODE SI479XX_FLASH__FLASH_LOAD_SUB(uint8_t icNum, uint16_t length, uint8_t* subcmdbuf);
R_4_1_SI479XX_COMMON_EXT RETURN_CODE SI479XX_FLASH_UTIL__FLASH_UTIL_SUB(uint8_t icNum, uint16_t length, uint8_t* subcmdbuf);


#endif
