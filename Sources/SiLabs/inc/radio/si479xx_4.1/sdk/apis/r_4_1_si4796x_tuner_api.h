/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si4796x command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
 
#ifndef si4796x_tuner_api_h_
#define si4796x_tuner_api_h_

#ifdef _r_4_1_si4796x_tuner_api_h_global_		/* 210419 SGsoft */
#define R_4_1_SI4796X_TUNER_API_EXT
#else
#define R_4_1_SI4796X_TUNER_API_EXT	extern
#endif	/*_r_4_1_si4796x_tuner_api_h_global_*/

///#include "si4796x_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#if 0		/* 210512 SGsoft */	
#ifdef _r_4_1_si4796x_tuner_api_h_global_		/* 210419 SGsoft */
const uint16_t SI4796X_TUNER_PROPS[163] = {
    SI4796X_TUNER_PROP_RADIO_ANALOG_VOLUME_ADDR,
    SI4796X_TUNER_PROP_RADIO_MUTE_ADDR,
    SI4796X_TUNER_PROP_RADIO_ANTIALIAS_FILTER_ADDR,
    SI4796X_TUNER_PROP_RADIO_AUDIO_POST_PROCESS_ADDR,
    SI4796X_TUNER_PROP_RADIO_MUTE_RAMP_ADDR,
    SI4796X_TUNER_PROP_RADIO_TUNE_MUTE_RAMP_ADDR,
    SI4796X_TUNER_PROP_RADIO_AF_CHECK_MUTE_RAMP_ADDR,
    SI4796X_TUNER_PROP_RADIO_TUNE_SERVO_HOLD_ADDR,
    SI4796X_TUNER_PROP_RADIO_AUDIO_TEST_PATTERN_MODE_ADDR,
    SI4796X_TUNER_PROP_RADIO_AUDIO_TEST_PATTERN_MAGNITUDE_ADDR,
    SI4796X_TUNER_PROP_RADIO_VOLUME_RAMP_ADDR,
    SI4796X_TUNER_PROP_RADIO_GAINBLOCK_ADJUST0_ADDR,
    SI4796X_TUNER_PROP_RADIO_GAINBLOCK_ADJUST1_ADDR,
    SI4796X_TUNER_PROP_RADIO_GAINBLOCK_ADJUST2_ADDR,
    SI4796X_TUNER_PROP_RADIO_GAINBLOCK_ADJUST3_ADDR,
    SI4796X_TUNER_PROP_RADIO_GAINBLOCK_ADJUST4_ADDR,
    SI4796X_TUNER_PROP_FM_SEEK_BAND_BOTTOM_ADDR,
    SI4796X_TUNER_PROP_FM_SEEK_BAND_TOP_ADDR,
    SI4796X_TUNER_PROP_FM_SEEK_SPACING_ADDR,
    SI4796X_TUNER_PROP_FM_RSQ_RSSI_OFFSET_ADDR,
    SI4796X_TUNER_PROP_FM_RSQ_HD_DETECTION_ADDR,
    SI4796X_TUNER_PROP_FM_RSQ_FILTER_TIME_CONSTANT_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_TIMER1_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_TIMER1_METRICS_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_TIMER2_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_TIMER2_METRICS_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_TIMER3_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_TIMER3_METRICS_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_TIMER1_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_TIMER1_METRICS_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_TIMER2_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_TIMER2_METRICS_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_TIMER3_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_TIMER3_METRICS_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_RDS_TIME_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_PI_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_AF_PI_MASK_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_FREQOFF_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_RSSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_SNR_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_ISSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_ASSI100_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_MULTIPATH_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_ASSI200_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_USN_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_HDLEVEL_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_RSSI_ADJ_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_RSSI_NC_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_VALID_RSSI_NC_ADJ_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_DEMOD_DE_EMPHASIS_ADDR,
    SI4796X_TUNER_PROP_FM_NB_CONTROL_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AUDIO_DETECTOR_SELECTOR_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AUDIO_NUM_SAMPLES_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AUDIO_LPF_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AUDIO_LPF_SLOW_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AUDIO_LPF_FAST_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AGC_BLANKING_ADDR,
    SI4796X_TUNER_PROP_FM_NB_AGC_BLANK_THRSHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_NB_FALSE_DETECTION_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_FM_CHEQ_CHEQ_CONTROL_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_CHANBW_CONTROL_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_ACI100_THLD_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_ACI200_THLD_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_ACI_HYST_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_ACI_HYST_TIME_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_CHANBW_ACI_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_BWSPEED_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_CHANBW_MAX_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_ACI100_GAIN_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_ACI100_DBOSET1_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_SNR_GAIN_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_SNR_DBOSET_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_CHANBW_WIDE_ADDR,
    SI4796X_TUNER_PROP_FM_CHANBW_CHANBW_NARROW_ADDR,
    SI4796X_TUNER_PROP_FM_RDS_INTERRUPT_SOURCE_ADDR,
    SI4796X_TUNER_PROP_FM_RDS_INTERRUPT_FIFO_COUNT_ADDR,
    SI4796X_TUNER_PROP_FM_RDS_CONFIG_ADDR,
    SI4796X_TUNER_PROP_FM_RDS_CONFIDENCE_ADDR,
    SI4796X_TUNER_PROP_FM_RDS_CHANNEL_BW_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_CONTROL_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_RSSI_DBOSET_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_D2PHI_LIM_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_XZ_SCALE_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_SIGQUAL_UP_F3DB_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_DPHI_LIMIT_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_AGC_F3DB_MIN_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_LDREF_BW_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_SIGQUAL_DWN_F3DB_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_AGC_F3DB_MAX_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_SNR_AGC_GAIN_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_AVC_GAIN_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_AVC_F3DB_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_PADJ_TMAX_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_NOISECHK_GAIN_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_MIN_AP_GAIN_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_DRDT_LIMIT_ADDR,
    SI4796X_TUNER_PROP_FM_DCNR_AVC_TONE_ADDR,
    SI4796X_TUNER_PROP_FM_VICS_CONFIG_ADDR,
    SI4796X_TUNER_PROP_FM_VICS_SCHMITT_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_SEEK_BAND_BOTTOM_ADDR,
    SI4796X_TUNER_PROP_AM_SEEK_BAND_TOP_ADDR,
    SI4796X_TUNER_PROP_AM_SEEK_SPACING_ADDR,
    SI4796X_TUNER_PROP_AM_RSQ_RSSI_OFFSET_ADDR,
    SI4796X_TUNER_PROP_AM_RSQ_HD_DETECTION_ADDR,
    SI4796X_TUNER_PROP_AM_RSQ_FILTER_TIME_CONSTANT_ADDR,
    SI4796X_TUNER_PROP_AM_RSQ_DRM30_DETECT_CONFIG_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_TIMER1_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_TIMER1_METRICS_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_TIMER2_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_TIMER2_METRICS_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_TIMER3_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_TIMER3_METRICS_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_FREQOFF_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_RSSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_SNR_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_ISSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_ASSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_HDLEVEL_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_RSSI_ADJ_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_RSSI_NC_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_VALID_RSSI_NC_ADJ_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_AM_DEMOD_OPTIONS_ADDR,
    SI4796X_TUNER_PROP_AM_DEMOD_PLL_MAX_FREQ_RANGE_ADDR,
    SI4796X_TUNER_PROP_AM_DEMOD_PLL_MIN_FREQ_RANGE_ADDR,
    SI4796X_TUNER_PROP_AM_NB_OPTIONS_ADDR,
    SI4796X_TUNER_PROP_AM_NB_IQ2_SETTINGS_ADDR,
    SI4796X_TUNER_PROP_AM_NB_IQ3_SETTINGS_ADDR,
    SI4796X_TUNER_PROP_AM_NB_AUD_SETTINGS_ADDR,
    SI4796X_TUNER_PROP_AM_NB_MUTE_HOLDOFF_ADDR,
    SI4796X_TUNER_PROP_AM_AVC_OPTIONS_ADDR,
    SI4796X_TUNER_PROP_AM_AFC_MODE_ADDR,
    SI4796X_TUNER_PROP_AM_AFC_LOOP_GAIN_ADDR,
    SI4796X_TUNER_PROP_AM_AFC_FREQ_RANGE_ADDR,
    SI4796X_TUNER_PROP_AM_AMIC_CONTROL_ADDR,
    SI4796X_TUNER_PROP_DAB_RSQ_RSSI_OFFSET_ADDR,
    SI4796X_TUNER_PROP_DAB_VALID_TIMER1_ADDR,
    SI4796X_TUNER_PROP_DAB_VALID_TIMER1_METRICS_ADDR,
    SI4796X_TUNER_PROP_DAB_VALID_RSSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_DAB_VALID_SQI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_DAB_VALID_RSSI_ADJ_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_DAB_VALID_DETECT_SENSITIVITY_ADDR,
    SI4796X_TUNER_PROP_WB_RSQ_RSSI_OFFSET_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_TIMER1_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_TIMER1_METRICS_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_TIMER2_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_TIMER2_METRICS_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_TIMER3_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_TIMER3_METRICS_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_FREQOFF_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_RSSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_SNR_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_ISSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_VALID_ASSI_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_NB_CONTROL_ADDR,
    SI4796X_TUNER_PROP_WB_NB_AUDIO_NUM_SAMPLES_ADDR,
    SI4796X_TUNER_PROP_WB_NB_AUDIO_LPF_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_NB_AUDIO_LPF_SLOW_ADDR,
    SI4796X_TUNER_PROP_WB_NB_AUDIO_LPF_FAST_ADDR,
    SI4796X_TUNER_PROP_WB_ASQ_INTERRUPT_SOURCE_ADDR,
    SI4796X_TUNER_PROP_WB_ASQ_DETECT_RANGE_ADDR,
    SI4796X_TUNER_PROP_WB_ASQ_DETECT_THRESHOLD_ADDR,
    SI4796X_TUNER_PROP_WB_AVC_OPTIONS_ADDR,
    SI4796X_TUNER_PROP_WB_AVC_REF_ADDR,
};
#else
// List of valid property addresses defined by this API. (mainly for debug)
extern const uint16_t SI4796X_TUNER_PROPS[163];
#endif	/*_r_4_1_si4796x_tuner_api_h_global_*/
#endif	/*0*/

/*
 * Command: SI4796X_TUNER_set_property
 * 
 * Description:
 *   Sets a property common to one or more commands. These are similar to parameters for a api command but are not expected to change frequently and may be controlled by higher layers of the user's software. Setting some properties may not cause the device to take immediate action, however the property will take affect once a command which uses it is issued.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  prop: Selects the property index to set. The available properties are determined by the part number and the current mode of operation.
 *  data: Value of the property. Acceptable values are dependent on the property index chosen.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_set_property__command(uint8_t icNum,uint8_t id, uint16_t prop, uint16_t data);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_set_property(uint8_t icNum, uint8_t id, uint16_t  prop, uint16_t  data);


/*
 * Command: SI4796X_TUNER_get_property
 * 
 * Description:
 *   Retrieve a property's value; The value will either be the default or the value set with SET_PROPERTY.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  count: Indicates the number of consecutive properties to access
 *  prop: Selects the property index to set. The available properties are determined by the part number and the current mode of operation.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: data description
 *  count (input): #elems in 'data' array.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_property__command(uint8_t icNum,uint8_t id, uint8_t count, uint16_t prop);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_property__reply(uint8_t icNum, uint8_t id, uint16_t* data, uint16_t count);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_property(uint8_t icNum, uint8_t id, uint8_t  arg_count, uint16_t  prop, uint16_t*  data, uint16_t  rep_count);


/*
 * Command: SI4796X_TUNER_get_mode
 * 
 * Description:
 *   Returns the current mode of operation.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  mode: The part's mode of operation.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_mode__command(uint8_t icNum,uint8_t id);



R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_mode__reply(uint8_t icNum, uint8_t id, uint8_t* mode);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_mode(uint8_t icNum, uint8_t id, uint8_t*  mode);


/*
 * Command: SI4796X_TUNER_set_mode
 * 
 * Description:
 *   Sets the mode of operation for the part.  After sending a SET_MODE command, the STC (seek-tune-complete) bit must be high prior to sending the first tune command. The part comes up in mute state until first tune is issued.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  mode: The part's mode of operation.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_set_mode__command(uint8_t icNum,uint8_t id, uint8_t mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_set_mode(uint8_t icNum, uint8_t id, uint8_t  mode);


/*
 * Command: SI4796X_TUNER_set_radio_audio_filter
 * 
 * Description:
 *   6th-order IIR audio filter control. When enabled, this filter is applied to the blended audio     based on analog FM/AM audio and HD/DAB digital audio and can can be used to shape the      audio frequency response. This filter can only be used in tuner-only mode.     The 6th-order IIR filter consists of three cascaded Biquad filters, each can be independently      configured. As the figure below shows, each filter has 5 coefficents: Bk0, Bk1 and Bk2 determine      zeros, and Ak1, Ak2 determine the position of the poles (k=0, 1 or 2 represent corresponding      Biquad filter).      NOTE: The Biquads should be configured for an audio base rate of either 44100kS/s or 48000kS/s, depending how the part is booted.     NOTE: This IIR filter has limited dynamic range. It might be necessary to gain down the signal prior to the IIR     filter using RADIO_GAINBLOCK_ADJUST0 and/or RADIO_GAINBLOCK_ADJUST1, and then gaining back     up in RADIO_GAINBLOCK_ADJUST2, RADIO_GAINBLOCK_ADJUST3, RADIO_GAINBLOCK_ADJUST4     to prevent audio clipping/wrapping withing the IIR.
 *       In AM mode, the first 2 Biquad filters, Biquad0 and Biquad1, could be used as a variable corner frequency 4th order Butterworth filter      whose coefficients are dynamically updated according to the amvarfc servo normally driven by the dLPR and SNR, to enhance the background noise mitigation. 	To enable this feature, both bit of VARFC_EN and IIR_EN should be set.  	Meanwhile, coefficients of Biquad 2 MUST be given a unit gain (directly allpass or a DC notch). Otherwise, audio can not paas the cascaded Biquad filters. 
 *       IIRIIRIIR
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  varfc_en: Enable/Disable the variable Fc 4th-order Butterworth audio filter. If enabled, the first 2 cascaded Biquad filters will be used as noise mitigation filter whose corner frequency is deduced from the amvarfc servo. Note: If enabled, the Biquad0/Biquad1 coefficients written will be neglected, and only the Biquad2 coefficients written will take effect. This feature is available only in AM mode, so please don't enable it in FM mode.
 *  hifi_en: Choose where the Hifi radio input block gets its data.
 *  i2s_en: Choose where the I2STX block gets its data. NOTE: this should not be enabled if the output of I2STX is fed back into I2SRX
 *  iir_en: Enable/Disable the 6th-order IIR audio filter
 *  biquad0_b0: BIQUAD0_B0 coefficient for Biquad filter.
 *  biquad0_b1: BIQUAD0_B1 coefficient for Biquad filter.
 *  biquad0_b2: BIQUAD0_B2 coefficient for Biquad filter.
 *  biquad0_a1: BIQUAD0_A1 coefficient for Biquad filter.
 *  biquad0_a2: BIQUAD0_A2 coefficient for Biquad filter.
 *  biquad1_b0: BIQUAD1_B0 coefficient for Biquad filter.
 *  biquad1_b1: BIQUAD1_B1 coefficient for Biquad filter.
 *  biquad1_b2: BIQUAD1_B2 coefficient for Biquad filter.
 *  biquad1_a1: BIQUAD1_A1 coefficient for Biquad filter.
 *  biquad1_a2: BIQUAD1_A2 coefficient for Biquad filter.
 *  biquad2_b0: BIQUAD2_B0 coefficient for Biquad filter.
 *  biquad2_b1: BIQUAD2_B1 coefficient for Biquad filter.
 *  biquad2_b2: BIQUAD2_B2 coefficient for Biquad filter.
 *  biquad2_a1: BIQUAD2_A1 coefficient for Biquad filter.
 *  biquad2_a2: BIQUAD2_A2 coefficient for Biquad filter.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_set_radio_audio_filter__command(uint8_t icNum,uint8_t id, uint8_t varfc_en, uint8_t hifi_en, uint8_t i2s_en, uint8_t iir_en, int32_t biquad0_b0, int32_t biquad0_b1, int32_t biquad0_b2, int32_t biquad0_a1, int32_t biquad0_a2, int32_t biquad1_b0, int32_t biquad1_b1, int32_t biquad1_b2, int32_t biquad1_a1, int32_t biquad1_a2, int32_t biquad2_b0, int32_t biquad2_b1, int32_t biquad2_b2, int32_t biquad2_a1, int32_t biquad2_a2);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_set_radio_audio_filter(uint8_t icNum, uint8_t id, uint8_t  varfc_en, uint8_t  hifi_en, uint8_t  i2s_en, uint8_t  iir_en, int32_t  biquad0_b0, int32_t  biquad0_b1, int32_t  biquad0_b2, int32_t  biquad0_a1, int32_t  biquad0_a2, int32_t  biquad1_b0, int32_t  biquad1_b1, int32_t  biquad1_b2, int32_t  biquad1_a1, int32_t  biquad1_a2, int32_t  biquad2_b0, int32_t  biquad2_b1, int32_t  biquad2_b2, int32_t  biquad2_a1, int32_t  biquad2_a2);


/*
 * Command: SI4796X_TUNER_get_radio_audio_filter
 * 
 * Description:
 *   Read out the coefficients of the 6th-order IIR audio filter. It consists of three Biquads, each can be independently configured.  	These IIR filters are implemented in Quark2 and can be used in tuner-only mode. When enabled,  	these filters are applied to the blended audio based on analog FM/AM audio and HD/DAB digital audio. 	These filters can be used to shape the audio frequency response.     IIRIIR
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  varfc_en: Reports whether the variable Fc 4th-order Butterworth audio filter is enabled or disabled
 *  hifi_en: Reports whether the 6th-order IIR audio filter is enabled or disabled for Hifi
 *  i2s_en: Reports whether the 6th-order IIR audio filter is enabled or disabled for i2s
 *  iir_en: Reports whether the 6th-order IIR audio filter is enabled or disabled
 *  biquad0_b0: BIQUAD0_B0 coefficient for Biquad filter.
 *  biquad0_b1: BIQUAD0_B1 coefficient for Biquad filter.
 *  biquad0_b2: BIQUAD0_B2 coefficient for Biquad filter.
 *  biquad0_a1: BIQUAD0_A1 coefficient for Biquad filter.
 *  biquad0_a2: BIQUAD0_A2 coefficient for Biquad filter.
 *  biquad1_b0: BIQUAD1_B0 coefficient for Biquad filter.
 *  biquad1_b1: BIQUAD1_B1 coefficient for Biquad filter.
 *  biquad1_b2: BIQUAD1_B2 coefficient for Biquad filter.
 *  biquad1_a1: BIQUAD1_A1 coefficient for Biquad filter.
 *  biquad1_a2: BIQUAD1_A2 coefficient for Biquad filter.
 *  biquad2_b0: BIQUAD2_B0 coefficient for Biquad filter.
 *  biquad2_b1: BIQUAD2_B1 coefficient for Biquad filter.
 *  biquad2_b2: BIQUAD2_B2 coefficient for Biquad filter.
 *  biquad2_a1: BIQUAD2_A1 coefficient for Biquad filter.
 *  biquad2_a2: BIQUAD2_A2 coefficient for Biquad filter.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_radio_audio_filter__command(uint8_t icNum,uint8_t id);


typedef struct _SI4796X_TUNER_get_radio_audio_filter__data
{
	uint8_t VARFC_EN;
	uint8_t HIFI_EN;
	uint8_t I2S_EN;
	uint8_t IIR_EN;
	int32_t BIQUAD0_B0;
	int32_t BIQUAD0_B1;
	int32_t BIQUAD0_B2;
	int32_t BIQUAD0_A1;
	int32_t BIQUAD0_A2;
	int32_t BIQUAD1_B0;
	int32_t BIQUAD1_B1;
	int32_t BIQUAD1_B2;
	int32_t BIQUAD1_A1;
	int32_t BIQUAD1_A2;
	int32_t BIQUAD2_B0;
	int32_t BIQUAD2_B1;
	int32_t BIQUAD2_B2;
	int32_t BIQUAD2_A1;
	int32_t BIQUAD2_A2;
}SI4796X_TUNER_get_radio_audio_filter__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_radio_audio_filter__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_get_radio_audio_filter__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_get_radio_audio_filter(uint8_t icNum, uint8_t id, SI4796X_TUNER_get_radio_audio_filter__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_tune_freq
 * 
 * Description:
 *   Tunes to the selected frequency. 
 *   The optional STC interrupt is set when the command completes the tune. Sending this command  clears any pending STCINT bit or RDSINT bit in the STATUS. Use property INTERRUPT_ENABLE0 to enable interrupts for this command. The hardware will also need to have a pin configured  as an interrupt pin. Please see FM_PHASE_DIVERSITY for the proper tuning procedure when using phase diversity.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tunemode: Set the desired tuning mode
 *  servo: Control behavior of servos during a tune.
 *  injection: Mixer injection side selection
 *  freq: Tune Frequency in 10kHz
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t tunemode, uint8_t servo, uint8_t injection, uint16_t freq);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_tune_freq(uint8_t icNum, uint8_t id, uint8_t  tunemode, uint8_t  servo, uint8_t  injection, uint16_t  freq);


/*
 * Command: SI4796X_TUNER_fm_seek_start
 * 
 * Description:
 *   Initiates a seek for a channel that meets the validation criteria for FM. If this tuner is contributing to phase diversity, FM_PHASE_DIVERSITY:OPTIONS:COMB should be set to 0 on the combining chip prior to initaiting this command and diversity should only be reenabled when both tuners are on the same frequency.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  seekup: Determines direction of seek and band limit.
 *  wrap: Determines seek behavior upon reaching a band limit.
 *  mode: Sets the seek mode to either search for a good station, or just take one step
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_seek_start__command(uint8_t icNum,uint8_t id, uint8_t seekup, uint8_t wrap, uint8_t mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_seek_start(uint8_t icNum, uint8_t id, uint8_t  seekup, uint8_t  wrap, uint8_t  mode);


/*
 * Command: SI4796X_TUNER_fm_rsq_status
 * 
 * Description:
 *   Allows the host to gather information about the currently tuned channel and the nearby channels.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  attune: Return all signal quality values as of FM_VALIDATION_TIME after tune.
 *  cancel: Aborts a seek or tune validation currently in progress
 *  stcack: Clears the STC interrupt status indicator if set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  hdsearchdone: HD Search indicator
 *  bltf: Band Limit after FM_SEEK_START. Reports if a seek hit the band limit or wrapped to the original frequency. This does not indicate that the seek failed, only that further seeks from the current location would be unproductive.
 *  t3ready: FM_valid_Timer3 status. When set, indicates that the FM_valid_timer3 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  FM_VALID_TIMER3  and  FM_VALID_TIMER3_METRICS . The reason for exit could be that we failed to validate the timer3_metrics, or the other timers were not configured.
 *  t2ready: FM_valid_Timer2 status. When set, indicates that the FM_valid_timer2 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  FM_VALID_TIMER2  and  FM_VALID_TIMER2_METRICS . The reason for exit could be that we failed to validate the timer2_metrics, or the other timers were not configured.
 *  t1ready: FM_valid_Timer1 status. When set, indicates that the FM_valid_timer1 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  FM_VALID_TIMER1  and  FM_VALID_TIMER1_METRICS . The reason for exit could be that we failed to validate the timer1_metrics, or the other timers were not configured.
 *  inject: Injection side
 *  afcrl: AFC rail indicator
 *  valid: Reports if the channel is valid based on the settings of FM_VALID_RSSI_THRESHOLD, FM_VALID_SNR_THRESHOLD, FM_VALID_ISSI, FM_VALID_MAX_TUNE_ERROR
 *  freq: Tune Frequency in 10kHz
 *  freqoff: Signed frequency offset in units of kHz.
 *  rssi: Received signal strength indicator
 *  snr: RF carrier to noise indicator
 *  lassi: Low side adjacent (100kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  hassi: High side adjacent (100kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  assi100: The highest of HASSI/LASSI
 *  lassi200: Low side adjacent (200kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  hassi200: High side adjacent (200kHz) signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  assi200: The highest of HASSI200/LASSI200
 *  multipath: Multipath indicator
 *  usn: Ultra-sonic noise indicator. 0 is full scale, 127 is 127dB down from full scale. The USN measurement is made after the channel bandwidth filter. To achieve repeatable measurements of USN, channel bandwidth must be consistent for both measurements. During a tune, channel bandwidth will be held fixed for 13ms after the start of tune. During a validated tune that lasts for more than 13ms, the channel bandwidth will change during the validation and can affect the resulting USN metric value.
 *  deviation: Frequency Deviation.
 *  cochanl: Pre-Channel Filter co-channel indicator. 0 is full scale, 127 is 127dB down from full scale.
 *  pdev: Pilot Deviation indicator
 *  rdsdev: RDS Deviation indicator
 *  ssdev: Desired channel frequency deviation when signal strength is strong in kHz.
 *  pilotlock: This metric has as inputs the energy in the I and Q phases of the pilot phase locked loop phase detector. When locked the loop tracks the pilot phase and all the pilot energy is in the I phase. Call this energy P. Noise also exists on both I and Q phases. This contributes, on average, an equal amount of energy to the I and Q phases. Call this contribution N. This metric is the ratio (P+N)/N. In an unlocked or pilot missing case, P=0 resulting in a metric value close to 1. If a pilot is present and the loop is locked, then the metric is larger than 1. Practically, expect to observe values of at least 2 for pilot lock. This represents very small pilot deviations (around 250 Hz in some cases, but this is not a design target or a guaranteed value). Larger values are expected as pilot deviation in the transmit signal increases.
 *  hdlevel: HD LEVEL Indicator. Reports a HD availability confidence factor that is normalized to the number of symbols periods examined. HD LEVEL is not calculated for DAB I/Q rate mode.
 *  rssi_dp: Received signal strength indicator of the signal on the RDS data path. This data path is not sucseptible to channel bandwith collapse or to noise from the HD sidebands. NOTE: In phase diversity mode this metric is calculated from the local antenna.
 *  snr_dp: RF carrier to noise indicator of the signal on the RDS data path. This data path is not sucseptible to channel bandwith collapse or to noise from the HD sidebands. NOTE: In phase diversity mode this metric is calculated from the local antenna.
 *  multipath_dp: Multipath indicator of the signal on the RDS data path. This data path is not sucseptible to channel bandwith collapse or to noise from the HD sidebands. NOTE: In phase diversity mode this metric is calculated from the local antenna.
 *  usn_dp: Ultra-sonic noise indicator of the signal on the RDS data path. It is the measured variance of the demodulated output for the data path around RDS frequency (57KHz). 0 is full scale, 127 is 127dB down from full scale. This data path is not sucseptible to channel bandwith collapse or to noise from the HD sidebands. NOTE: In phase diversity mode this metric is calculated from the local antenna.
 *  rssi_adj: Received signal strength indicator adjusted by  FM_RSQ_RSSI_OFFSET .
 *  phsdiv_comb_wght: Combiner weight metric to reflect antenna contribution. +127 is full weighted to the ccmbiner's antenna, -128 is fully weighted to contributing tuner. NOTE: There is a hold-off period of ~13ms after tuning the remote tuner. This metric should not be relied upon until after that hold-off period.
 *  rds_chan_bw: FM RDS channel Bandwidth.
 *  rssi_nc: Received signal strength indicator without RF compensation.
 *  rssi_nc_adj: Received signal strength indicator without RF compensation adjusted by  FM_RSQ_RSSI_OFFSET .
 *  filtered_hdlevel: HD LEVEL Indicator. Reports a filtered version HD availability confidence factor that is normalized to the number of symbols periods examined. HD LEVEL is not calculated for DAB I/Q rate mode.
 *  cleansignal: Percentage of last 10ms that was considered a clean signal
 *  maxdev: Maximum deviation. This metric is only valid when there is a clean signal.
 *  usn_mono: Measures the power present in the 17kHz to 21kHz band of the MPX signal, with a notch in the response at 19kHz so that the stereo pilot, if present, has little effect on the USN_MONO reading. Power is reported in decibels. USN_MONO is intended for use in detecting adverse reception conditions such as multipath and cochannel interference -- test -- really
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack);


typedef struct _SI4796X_TUNER_fm_rsq_status__data
{
	uint8_t HDSEARCHDONE;
	uint8_t BLTF;
	uint8_t T3READY;
	uint8_t T2READY;
	uint8_t T1READY;
	uint8_t INJECT;
	uint8_t AFCRL;
	uint8_t VALID;
	uint16_t FREQ;
	int8_t FREQOFF;
	int8_t RSSI;
	int8_t SNR;
	int8_t LASSI;
	int8_t HASSI;
	int8_t ASSI100;
	int8_t LASSI200;
	int8_t HASSI200;
	int8_t ASSI200;
	uint8_t MULTIPATH;
	uint8_t USN;
	uint8_t DEVIATION;
	uint8_t COCHANL;
	uint8_t PDEV;
	uint8_t RDSDEV;
	uint8_t SSDEV;
	uint8_t PILOTLOCK;
	uint8_t HDLEVEL;
	int8_t RSSI_DP;
	int8_t SNR_DP;
	uint8_t MULTIPATH_DP;
	uint8_t USN_DP;
	int8_t RSSI_ADJ;
	int8_t PHSDIV_COMB_WGHT;
	uint8_t RDS_CHAN_BW;
	int8_t RSSI_NC;
	int8_t RSSI_NC_ADJ;
	uint8_t FILTERED_HDLEVEL;
	uint8_t CLEANSIGNAL;
	uint8_t MAXDEV;
	int8_t USN_MONO;
}SI4796X_TUNER_fm_rsq_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rsq_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_rsq_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI4796X_TUNER_fm_rsq_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_rds_status
 * 
 * Description:
 *   This command returns RDS information for current channel and reads an entry from the RDS FIFO.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  statusonly: Determines if data should be removed from the RDS receive FIFO.
 *  mtfifo: Clears the contents of the RDS Receive FIFO if set. The FIFO will always be cleared during  FM_TUNE_FREQ  and  FM_SEEK_START .
 *  intack: Clears the STATUS RDSINT bit if set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tpptyint: Traffic Program (TP) flag and Program Type (PTY) code has changed.
 *  piint: Program Identification (PI) code has changed.
 *  syncint: RDS synchronization status changed.
 *  fifoint: RDS Data was received and the RDS receive FIFO is full or has at least FM_RDS_INTERRUPT_FIFO_COUNT entries.
 *  tpptyvalid: Indicates that the TP flag and PTY code are valid.
 *  pivalid: Indicates that the PI code is valid.
 *  sync: Indicates that RDS is currently synchronized.
 *  fifolost: One or more RDS groups have been discarded due to FIFO overrun since the last call to  FM_RDS_STATUS .
 *  tp: Current channel's TP flag if  FM_RDS_STATUS:FLAGS:TPPTYVALID  is 1
 *  pty: Current channel's PTY code if  FM_RDS_STATUS:FLAGS:TPPTYVALID  is 1
 *  pi: Current channel's Program Identification if  FM_RDS_STATUS:FLAGS:PIVALID  is 1
 *  rdsfifoused: Indicates number of groups remaining in the RDS FIFO (0 if empty). If this is non-zero, BLOCKA-BLOCKD contain the oldest entry in the FIFO and RDSFIFOUSED will decrement by one on the next call to  FM_RDS_STATUS  (assuming no new RDS Data is received in the interim).
 *  blera: Bit Errors corrected in BLOCKA
 *  blerb: Bit Errors corrected in BLOCKB
 *  blerc: Bit Errors corrected in BLOCKC
 *  blerd: Bit Errors corrected in BLOCKD
 *  blocka: Block A data from RDS FIFO if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 0. This field is not updated if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 1.
 *  blockb: Block B data from RDS FIFO if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 0. This field is not updated if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 1.
 *  blockc: Block C data from RDS FIFO if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 0. This field is not updated if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 1.
 *  blockd: Block D data from RDS FIFO if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 0. This field is not updated if  FM_RDS_STATUS:OPTIONS:STATUSONLY  is 1.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rds_status__command(uint8_t icNum,uint8_t id, uint8_t statusonly, uint8_t mtfifo, uint8_t intack);


typedef struct _SI4796X_TUNER_fm_rds_status__data
{
	uint8_t TPPTYINT;
	uint8_t PIINT;
	uint8_t SYNCINT;
	uint8_t FIFOINT;
	uint8_t TPPTYVALID;
	uint8_t PIVALID;
	uint8_t SYNC;
	uint8_t FIFOLOST;
	uint8_t TP;
	uint8_t PTY;
	uint16_t PI;
	uint8_t RDSFIFOUSED;
	uint8_t BLERA;
	uint8_t BLERB;
	uint8_t BLERC;
	uint8_t BLERD;
	uint16_t BLOCKA;
	uint16_t BLOCKB;
	uint16_t BLOCKC;
	uint16_t BLOCKD;
}SI4796X_TUNER_fm_rds_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rds_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_rds_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rds_status(uint8_t icNum, uint8_t id, uint8_t  statusonly, uint8_t  mtfifo, uint8_t  intack, SI4796X_TUNER_fm_rds_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_rds_blockcount
 * 
 * Description:
 *   Queries the block statistic info of RDS decoder. This command returns statistics on the  number of RDS blocks expected, received and uncorrectable. Information from this command  can be reset by setting FM_RDS_BLOCKCOUNT:OPTIONS:CLEAR bit or sending FM_TUNE_FREQ command.  Once FM_RDS_BLOCKCOUNT:EXPECTED:EXPECTED saturates at 65535,  all other block count statistics will be frozen until the counts are cleared.
 *   The block error rate (BLER) is calculated by:  block error rate
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  clear: Clears the block counts if set. The current block counts will be reported before they are cleared.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  expected: Number of expected RDS blocks.
 *  received: Number of received RDS blocks. Under ideal conditions, EXPECTED and RECEIVED would be identical. The difference between these two numbers is the number of blocks lost.
 *  uncorrectable: Number of uncorrectable RDS blocks. These blocks have been received, but were found to have uncorrectable errors.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rds_blockcount__command(uint8_t icNum,uint8_t id, uint8_t clear);


typedef struct _SI4796X_TUNER_fm_rds_blockcount__data
{
	uint16_t EXPECTED;
	uint16_t RECEIVED;
	uint16_t UNCORRECTABLE;
}SI4796X_TUNER_fm_rds_blockcount__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rds_blockcount__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_rds_blockcount__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_rds_blockcount(uint8_t icNum, uint8_t id, uint8_t  clear, SI4796X_TUNER_fm_rds_blockcount__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_servo_status
 * 
 * Description:
 *   Gather the status of the FM noise mitigation engines
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  chanbw: Current channel bandwidth frequency.   See  FM_CHANBW  for more options. This servo cannot be controlled by a servo transform.
 *  softmute: Current softmute attenuation.
 *  hicut: Current hi-cut cutoff frequency.
 *  lowcut: Current low-cut cutoff frequency.
 *  stblend: Current stereo blend separation.   Although displayed in dB, the computation of stereo gain is done in the linear domain using the equation  dB to linear .
 *  hiblend: Current hi-blend cutoff frequency.
 *  nbusn1thr: Current USN1 noise blanker detector threshold.
 *  nbusn2thr: Current USN2 noise blanker detector threshold.
 *  fadethresh: Current fade threshold value.   FADETHRESH works with DCNR to mitigate harsh noise. The servo configures the threshold at which FADETHRESH mitigation engages, based on the difference in IQ magnitude as a percent of it's nominal value. Tradeoff of higher percentage FADETHRESH is degraded sensitivity.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_servo_status__command(uint8_t icNum,uint8_t id);


typedef struct _SI4796X_TUNER_fm_servo_status__data
{
	uint8_t CHANBW;
	uint8_t SOFTMUTE;
	uint8_t HICUT;
	uint8_t LOWCUT;
	uint8_t STBLEND;
	uint8_t HIBLEND;
	uint8_t NBUSN1THR;
	uint8_t NBUSN2THR;
	int8_t FADETHRESH;
}SI4796X_TUNER_fm_servo_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_servo_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_servo_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_servo_status(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_servo_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_servo_trigger_status
 * 
 * Description:
 *   Reports the current status of all FM servo triggers
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trigint_ack: Acknowledge TRIGINT and clears it in the status word.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  servotrigger: (No Description)
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_servo_trigger_status__command(uint8_t icNum,uint8_t id, uint8_t trigint_ack);


typedef struct _SI4796X_TUNER_fm_servo_trigger_status_servotrigger__data
{
	int16_t X_IN;
	uint8_t ABOVE_INT;
	uint8_t BELOW_INT;
	uint8_t ACTIVE_INT;
	uint8_t ACTIVE;
	uint8_t CONFIGURED;
}SI4796X_TUNER_fm_servo_trigger_status_servotrigger__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_servo_trigger_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_servo_trigger_status_servotrigger__data* servotrigger);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_servo_trigger_status(uint8_t icNum, uint8_t id, uint8_t  trigint_ack, SI4796X_TUNER_fm_servo_trigger_status_servotrigger__data*  servotrigger);


/*
 * Command: SI4796X_TUNER_fm_signal_event_count
 * 
 * Description:
 *   Reports the status of the FM noise blankers, and the count of times the pop suppressor engages
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  nb_aud_cnt: Number of times the audio noise blanker fired in the last 2.818-seconds
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_signal_event_count__command(uint8_t icNum,uint8_t id);



R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_signal_event_count__reply(uint8_t icNum, uint8_t id, uint16_t* nb_aud_cnt);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_signal_event_count(uint8_t icNum, uint8_t id, uint16_t*  nb_aud_cnt);


/*
 * Command: SI4796X_TUNER_fm_set_servo
 * 
 * Description:
 *   Configure the settings of the FM noise mitigation servos servo The accumulator aggregates all the inputs for this servo into a single value that drives the servo.  The possible accumulators include min, max, and sum.    Typically the limits are only useful when using the 'sum' option, but are always required as absolute limits of the servo.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_servo_id: Index of the servo. See  FM_SERVO_STATUS  for more details on each servo.
 *  min: Absolute minimum value for this servo. If a servo transform that is driving this servo attempts to go below this limit, it will be limited to this value.
 *  max: Absolute maximum value for this servo. If a servo transform that is driving this servo attempts to go above this limit, it will be limited to this value.
 *  init: Initial value for the servo. When using the summation accumulator this also acts as the nominal value. The servos will be initialized to init value irrespective of the accumulator mode you are in (accmin/accmax/accsum). When you are in accmin mode, you likely want it to be intiliazed to max value, and When you are in accmax mode, you likely want it to be intiliazed to min value.
 *  accumulator: Method of combining multiple servo transforms
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_servo__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_id, int16_t min, int16_t max, int16_t init, uint8_t accumulator);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_servo(uint8_t icNum, uint8_t id, uint8_t  fm_servo_id, int16_t  min, int16_t  max, int16_t  init, uint8_t  accumulator);


/*
 * Command: SI4796X_TUNER_fm_get_servo
 * 
 * Description:
 *   Query the settings of the FM noise mitigation servos
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_servo_id: Index of the servo. See  FM_SERVO_STATUS  for more details on each servo.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  setting: The current setting of this servo. There is no difference between this and the value read back from  FM_SERVO_STATUS .
 *  min: Absolute minimum value for this servo. If a servo transform that is driving this servo attempts to go below this limit, it will be limited to this value.
 *  max: Absolute maximum value for this servo. If a servo transform that is driving this servo attempts to go above this limit, it will be limited to this value.
 *  init: Initial value for the servo. When using the summation accumulator this also acts as the nominal value. The servos will be initialized to init value irrespective of the accumulator mode you are in (accmin/accmax/accsum). When you are in accmin mode, you likely want it to be intiliazed to max value, and When you are in accmax mode, you likely want it to be intiliazed to min value.
 *  accumulator: Method of combining multiple servo transforms
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_id);


typedef struct _SI4796X_TUNER_fm_get_servo__data
{
	uint8_t SETTING;
	int16_t MIN;
	int16_t MAX;
	int16_t INIT;
	uint8_t ACCUMULATOR;
}SI4796X_TUNER_fm_get_servo__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_get_servo__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo(uint8_t icNum, uint8_t id, uint8_t  fm_servo_id, SI4796X_TUNER_fm_get_servo__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_set_servo_transform
 * 
 * Description:
 *   Configure the settings for an FM servo transform block.  servo_transform The servo transform performs multiple functions:          	It calculates what Y value is expected for a given X value.            	It then adjusts Yout at the rate specified by P1TOP2RATE or P2TOP1RATE         depending on which direction Y is traveling on the transformation function.          	The resulting Yout value is fed into the accumulator of the corresponding         servo.          	The transform loop only updates once every millisecond.  If the P1TOP2RATE or          P2TOP1RATE is too fast, the resulting output for Y will appear as steps (or a single step)          rather than a ramp. This is due to the fact that the RATEs determine the step size that          is taken on Y each ms in the given direction. While it is possible to use rates less          than 10ms, it is discouraged for this reason. The equation for computing the step size is          Y step calculation
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_servo_transform_id: FM servo transform ID
 *  fm_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.   Using a metric value of 0, disables this servo transform.
 *  fm_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  FM_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 *  enable_other0: Indicates whether or not this servo transform is linked to another servo transform's Y max
 *  other_transform_at_ymax: Servo transform ID of cascaded servo transform that must be at Y max  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  enable_other1: Indicates whether or not this servo transform is linked to another servo transform's Y min
 *  other_transform_at_ymin: Servo transform ID of cascaded servo transform thatmust be at Y min  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  enable_trigger: Indicates whether or not the trigger specified by FM_SERVO_TRIGGER_ID is controlling this servo transform.
 *  invert_trigger: Indicates whether or not the trigger specified by FM_SERVO_TRIGGER_ID should be inverted when controlling this transform.
 *  fm_servo_trigger_id: Index of an enabling trigger
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_transform_id, uint8_t fm_metric_id, uint8_t fm_servo_id, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate, uint8_t enable_other0, uint8_t other_transform_at_ymax, uint8_t enable_other1, uint8_t other_transform_at_ymin, uint8_t enable_trigger, uint8_t invert_trigger, uint8_t fm_servo_trigger_id);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_servo_transform_id, uint8_t  fm_metric_id, uint8_t  fm_servo_id, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate, uint8_t  enable_other0, uint8_t  other_transform_at_ymax, uint8_t  enable_other1, uint8_t  other_transform_at_ymin, uint8_t  enable_trigger, uint8_t  invert_trigger, uint8_t  fm_servo_trigger_id);


/*
 * Command: SI4796X_TUNER_fm_set_fast_servo_transform
 * 
 * Description:
 *   Configure the settings for an FM fast servo transform block.servo_transform The servo transform performs multiple functions:          	It calculates what Y value is expected for a given X value.            	It then adjusts Yout at the rate specified by P1TOP2RATE or P2TOP1RATE         depending on which direction Y is traveling on the transformation function.          	The resulting Yout value is fed into the accumulator of the corresponding         servo.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_fast_servo_transform_id: Channel bandwidth maxdev clean signal multiplier.
 *  enable: Disable/Enable this transform
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_fast_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_fast_servo_transform_id, uint8_t enable, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_fast_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_fast_servo_transform_id, uint8_t  enable, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate);


/*
 * Command: SI4796X_TUNER_fm_get_fast_servo_transform
 * 
 * Description:
 *   Query the settings for an FM fast servo transform block.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_fast_servo_transform_id: Channel bandwidth maxdev clean signal multiplier.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.
 *  fm_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  FM_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_fast_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_fast_servo_transform_id);


typedef struct _SI4796X_TUNER_fm_get_fast_servo_transform__data
{
	uint8_t FM_METRIC_ID;
	uint8_t FM_SERVO_ID;
	int16_t X1;
	int16_t Y1;
	int16_t X2;
	int16_t Y2;
	uint16_t P1TOP2RATE;
	uint16_t P2TOP1RATE;
}SI4796X_TUNER_fm_get_fast_servo_transform__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_fast_servo_transform__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_get_fast_servo_transform__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_fast_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_fast_servo_transform_id, SI4796X_TUNER_fm_get_fast_servo_transform__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_get_servo_transform
 * 
 * Description:
 *   Query the settings for an FM servo transform block.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_servo_transform_id: FM servo transform ID
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.
 *  fm_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  FM_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 *  other0_enabled: Indicates whether or not this servo transform is linked to another servo transform's Y max
 *  other_transform_at_ymax: Servo transform ID of cascaded servo transform that must be at Y max  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  other1_enabled: Indicates whether or not this servo transform is linked to another servo transform's Y min
 *  other_transform_at_ymin: Servo transform ID of cascaded servo transform thatmust be at Y min  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  trigger_enabled: Indicates whether or not the trigger specified by FM_SERVO_TRIGGER_ID is controlling this servo transform.
 *  trigger_inverted: Indicates whether or not the trigger specified by FM_SERVO_TRIGGER_ID should be inverted when controlling this transform.
 *  fm_servo_trigger_id: Index of an enabling trigger
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_transform_id);


typedef struct _SI4796X_TUNER_fm_get_servo_transform__data
{
	uint8_t FM_METRIC_ID;
	uint8_t FM_SERVO_ID;
	int16_t X1;
	int16_t Y1;
	int16_t X2;
	int16_t Y2;
	uint16_t P1TOP2RATE;
	uint16_t P2TOP1RATE;
	uint8_t OTHER0_ENABLED;
	uint8_t OTHER_TRANSFORM_AT_YMAX;
	uint8_t OTHER1_ENABLED;
	uint8_t OTHER_TRANSFORM_AT_YMIN;
	uint8_t TRIGGER_ENABLED;
	uint8_t TRIGGER_INVERTED;
	uint8_t FM_SERVO_TRIGGER_ID;
}SI4796X_TUNER_fm_get_servo_transform__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo_transform__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_get_servo_transform__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo_transform(uint8_t icNum, uint8_t id, uint8_t  fm_servo_transform_id, SI4796X_TUNER_fm_get_servo_transform__data*  replyData);


/*
 * Command: SI4796X_TUNER_fm_set_servo_trigger
 * 
 * Description:
 *   A trigger outputs an enable signal to an attached servo transform if FM_SET_SERVO_TRIGGER:THRESHOLD0:LOWER < METRIC <   FM_SET_SERVO_TRIGGER:THRESHOLD1:UPPERtrigger
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_servo_trigger_id: Index of an enabling trigger
 *  inactive_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes inactive. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  active_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes active. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  fm_metric_id: Index of the controlling metric. This is the input (X) of the servo trigger.   Using a metric value of 0, disables this servo trigger.
 *  lower: If the configured metric is below this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 *  upper: If the configured metric is above this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_trigger_id, uint8_t inactive_int, uint8_t active_int, uint8_t fm_metric_id, int16_t lower, int16_t upper);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_set_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  fm_servo_trigger_id, uint8_t  inactive_int, uint8_t  active_int, uint8_t  fm_metric_id, int16_t  lower, int16_t  upper);


/*
 * Command: SI4796X_TUNER_fm_get_servo_trigger
 * 
 * Description:
 *  
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  fm_servo_trigger_id: Index of an enabling trigger
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  inactive_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes inactive. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  active_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes active. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  in_range: Indicates whether this trigger is currently between the lower and upper thresholds and outputting enable to any servo transforms attached to it.
 *  fm_metric_id: Index of the controlling metric. This is the input (X) of the servo trigger.
 *  lower: If the configured metric is below this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 *  upper: If the configured metric is above this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t fm_servo_trigger_id);


typedef struct _SI4796X_TUNER_fm_get_servo_trigger__data
{
	uint8_t INACTIVE_INT;
	uint8_t ACTIVE_INT;
	uint8_t IN_RANGE;
	uint8_t FM_METRIC_ID;
	int16_t LOWER;
	int16_t UPPER;
}SI4796X_TUNER_fm_get_servo_trigger__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo_trigger__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_fm_get_servo_trigger__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_fm_get_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  fm_servo_trigger_id, SI4796X_TUNER_fm_get_servo_trigger__data*  replyData);


/*
 * Command: SI4796X_TUNER_am_set_servo
 * 
 * Description:
 *   Configure the settings of the AM noise mitigation servos servo The accumulator aggregates all the inputs for this servo into a single value that drives the servo.  The possible accumulators include min, max, and sum.    Typically the limits are only useful when using the 'sum' option, but are always required as absolute limits of the servo.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_servo_id: Index of the servo. See  AM_SERVO_STATUS  for more details on each servo.
 *  min: Absolute minimum value for this servo. If a servo transform that is driving this servo attempts to go below this limit, it will be limited to this value.
 *  max: Absolute maximum value for this servo. If a servo transform that is driving this servo attempts to go above this limit, it will be limited to this value.
 *  init: Initial value for the servo. When using the summation accumulator this also acts as the nominal value. The servos will be initialized to init value irrespective of the accumulator mode you are in (accmin/accmax/accsum). When you are in accmin mode, you likely want it to be intiliazed to max value, and When you are in accmax mode, you likely want it to be intiliazed to min value.
 *  accumulator: Method of combining multiple servo transforms
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_set_servo__command(uint8_t icNum,uint8_t id, uint8_t am_servo_id, int16_t min, int16_t max, int16_t init, uint8_t accumulator);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_set_servo(uint8_t icNum, uint8_t id, uint8_t  am_servo_id, int16_t  min, int16_t  max, int16_t  init, uint8_t  accumulator);


/*
 * Command: SI4796X_TUNER_am_get_servo
 * 
 * Description:
 *   Query the settings of the AM noise mitigation servos
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_servo_id: Index of the servo. See  AM_SERVO_STATUS  for more details on each servo.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  setting: The current setting of this servo. There is no difference between this and the value read back from  AM_SERVO_STATUS .
 *  min: Absolute minimum value for this servo. If a servo transform that is driving this servo attempts to go below this limit, it will be limited to this value.
 *  max: Absolute maximum value for this servo. If a servo transform that is driving this servo attempts to go above this limit, it will be limited to this value.
 *  init: Initial value for the servo. When using the summation accumulator this also acts as the nominal value. The servos will be initialized to init value irrespective of the accumulator mode you are in (accmin/accmax/accsum). When you are in accmin mode, you likely want it to be intiliazed to max value, and When you are in accmax mode, you likely want it to be intiliazed to min value.
 *  accumulator: Method of combining multiple servo transforms
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo__command(uint8_t icNum,uint8_t id, uint8_t am_servo_id);


typedef struct _SI4796X_TUNER_am_get_servo__data
{
	uint8_t SETTING;
	int16_t MIN;
	int16_t MAX;
	int16_t INIT;
	uint8_t ACCUMULATOR;
}SI4796X_TUNER_am_get_servo__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_get_servo__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo(uint8_t icNum, uint8_t id, uint8_t  am_servo_id, SI4796X_TUNER_am_get_servo__data*  replyData);


/*
 * Command: SI4796X_TUNER_am_set_servo_transform
 * 
 * Description:
 *   Configure the settings for an AM servo transform block.  servo_transform The servo transform performs multiple functions:          	It calculates what Y value is expected for a given X value.            	It then adjusts Yout at the rate specified by P1TOP2RATE or P2TOP1RATE         depending on which direction Y is traveling on the transformation function.          	The resulting Yout value is fed into the accumulator of the corresponding         servo.          	The transform loop only updates once every millisecond.  If the P1TOP2RATE or          P2TOP1RATE is too fast, the resulting output for Y will appear as steps (or a single step)          rather than a ramp. This is due to the fact that the RATEs determine the step size that          is taken on Y each ms in the given direction. While it is possible to use rates less          than 10ms, it is discouraged for this reason. The equation for computing the step size is          Y step calculation
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_servo_transform_id: AM servo transform ID
 *  am_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.   Using a metric value of 0, disables this servo transform.
 *  am_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  AM_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 *  enable_other0: Indicates whether or not this servo transform is linked to another servo transform's Y max
 *  other_transform_at_ymax: Servo transform ID of cascaded servo transform that must be at Y max  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  enable_other1: Indicates whether or not this servo transform is linked to another servo transform's Y min
 *  other_transform_at_ymin: Servo transform ID of cascaded servo transform thatmust be at Y min  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  enable_trigger: Indicates whether or not the trigger specified by AM_SERVO_TRIGGER_ID is controlling this servo transform.
 *  invert_trigger: Indicates whether or not the trigger specified by AM_SERVO_TRIGGER_ID should be inverted when controlling this transform.
 *  am_servo_trigger_id: Index of an enabling trigger
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_set_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t am_servo_transform_id, uint8_t am_metric_id, uint8_t am_servo_id, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate, uint8_t enable_other0, uint8_t other_transform_at_ymax, uint8_t enable_other1, uint8_t other_transform_at_ymin, uint8_t enable_trigger, uint8_t invert_trigger, uint8_t am_servo_trigger_id);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_set_servo_transform(uint8_t icNum, uint8_t id, uint8_t  am_servo_transform_id, uint8_t  am_metric_id, uint8_t  am_servo_id, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate, uint8_t  enable_other0, uint8_t  other_transform_at_ymax, uint8_t  enable_other1, uint8_t  other_transform_at_ymin, uint8_t  enable_trigger, uint8_t  invert_trigger, uint8_t  am_servo_trigger_id);


/*
 * Command: SI4796X_TUNER_am_get_servo_transform
 * 
 * Description:
 *   Query the settings for an AM servo transform block.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_servo_transform_id: AM servo transform ID
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.
 *  am_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  AM_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 *  other0_enabled: Indicates whether or not this servo transform is linked to another servo transform's Y max
 *  other_transform_at_ymax: Servo transform ID of cascaded servo transform that must be at Y max  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  other1_enabled: Indicates whether or not this servo transform is linked to another servo transform's Y min
 *  other_transform_at_ymin: Servo transform ID of cascaded servo transform thatmust be at Y min  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  trigger_enabled: Indicates whether or not the trigger specified by AM_SERVO_TRIGGER_ID is controlling this servo transform.
 *  trigger_inverted: Indicates whether or not the trigger specified by AM_SERVO_TRIGGER_ID should be inverted when controlling this transform.
 *  am_servo_trigger_id: Index of an enabling trigger
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t am_servo_transform_id);


typedef struct _SI4796X_TUNER_am_get_servo_transform__data
{
	uint8_t AM_METRIC_ID;
	uint8_t AM_SERVO_ID;
	int16_t X1;
	int16_t Y1;
	int16_t X2;
	int16_t Y2;
	uint16_t P1TOP2RATE;
	uint16_t P2TOP1RATE;
	uint8_t OTHER0_ENABLED;
	uint8_t OTHER_TRANSFORM_AT_YMAX;
	uint8_t OTHER1_ENABLED;
	uint8_t OTHER_TRANSFORM_AT_YMIN;
	uint8_t TRIGGER_ENABLED;
	uint8_t TRIGGER_INVERTED;
	uint8_t AM_SERVO_TRIGGER_ID;
}SI4796X_TUNER_am_get_servo_transform__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo_transform__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_get_servo_transform__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo_transform(uint8_t icNum, uint8_t id, uint8_t  am_servo_transform_id, SI4796X_TUNER_am_get_servo_transform__data*  replyData);


/*
 * Command: SI4796X_TUNER_am_set_servo_trigger
 * 
 * Description:
 *   A trigger outputs an enable signal to an attached servo transform if AM_SET_SERVO_TRIGGER:THRESHOLD0:LOWER < METRIC <   AM_SET_SERVO_TRIGGER:THRESHOLD1:UPPERtrigger
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_servo_trigger_id: Index of an enabling trigger
 *  inactive_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes inactive. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  active_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes active. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  am_metric_id: Index of the controlling metric. This is the input (X) of the servo trigger.   Using a metric value of 0, disables this servo trigger.
 *  lower: If the configured metric is below this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 *  upper: If the configured metric is above this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_set_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t am_servo_trigger_id, uint8_t inactive_int, uint8_t active_int, uint8_t am_metric_id, int16_t lower, int16_t upper);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_set_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  am_servo_trigger_id, uint8_t  inactive_int, uint8_t  active_int, uint8_t  am_metric_id, int16_t  lower, int16_t  upper);


/*
 * Command: SI4796X_TUNER_am_get_servo_trigger
 * 
 * Description:
 *  
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  am_servo_trigger_id: Index of an enabling trigger
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  inactive_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes inactive. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  active_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes active. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  in_range: Indicates whether this trigger is currently between the lower and upper thresholds and outputting enable to any servo transforms attached to it.
 *  am_metric_id: Index of the controlling metric. This is the input (X) of the servo trigger.
 *  lower: If the configured metric is below this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 *  upper: If the configured metric is above this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t am_servo_trigger_id);


typedef struct _SI4796X_TUNER_am_get_servo_trigger__data
{
	uint8_t INACTIVE_INT;
	uint8_t ACTIVE_INT;
	uint8_t IN_RANGE;
	uint8_t AM_METRIC_ID;
	int16_t LOWER;
	int16_t UPPER;
}SI4796X_TUNER_am_get_servo_trigger__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo_trigger__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_get_servo_trigger__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_get_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  am_servo_trigger_id, SI4796X_TUNER_am_get_servo_trigger__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_set_servo
 * 
 * Description:
 *   Configure the settings of the WB noise mitigation servos servo The accumulator aggregates all the inputs for this servo into a single value that drives the servo.  The possible accumulators include min, max, and sum.    Typically the limits are only useful when using the 'sum' option, but are always required as absolute limits of the servo.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_servo_id: Index of the servo. See  WB_SERVO_STATUS  for more details on each servo.
 *  min: Absolute minimum value for this servo. If a servo transform that is driving this servo attempts to go below this limit, it will be limited to this value.
 *  max: Absolute maximum value for this servo. If a servo transform that is driving this servo attempts to go above this limit, it will be limited to this value.
 *  init: Initial value for the servo. When using the summation accumulator this also acts as the nominal value. The servos will be initialized to init value irrespective of the accumulator mode you are in (accmin/accmax/accsum). When you are in accmin mode, you likely want it to be intiliazed to max value, and When you are in accmax mode, you likely want it to be intiliazed to min value.
 *  accumulator: Method of combining multiple servo transforms
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_set_servo__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_id, int16_t min, int16_t max, int16_t init, uint8_t accumulator);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_set_servo(uint8_t icNum, uint8_t id, uint8_t  wb_servo_id, int16_t  min, int16_t  max, int16_t  init, uint8_t  accumulator);


/*
 * Command: SI4796X_TUNER_wb_get_servo
 * 
 * Description:
 *   Query the settings of the WB noise mitigation servos
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_servo_id: Index of the servo. See  WB_SERVO_STATUS  for more details on each servo.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  setting: The current setting of this servo. There is no difference between this and the value read back from  WB_SERVO_STATUS .
 *  min: Absolute minimum value for this servo. If a servo transform that is driving this servo attempts to go below this limit, it will be limited to this value.
 *  max: Absolute maximum value for this servo. If a servo transform that is driving this servo attempts to go above this limit, it will be limited to this value.
 *  init: Initial value for the servo. When using the summation accumulator this also acts as the nominal value. The servos will be initialized to init value irrespective of the accumulator mode you are in (accmin/accmax/accsum). When you are in accmin mode, you likely want it to be intiliazed to max value, and When you are in accmax mode, you likely want it to be intiliazed to min value.
 *  accumulator: Method of combining multiple servo transforms
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_id);


typedef struct _SI4796X_TUNER_wb_get_servo__data
{
	uint8_t SETTING;
	int16_t MIN;
	int16_t MAX;
	int16_t INIT;
	uint8_t ACCUMULATOR;
}SI4796X_TUNER_wb_get_servo__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_get_servo__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo(uint8_t icNum, uint8_t id, uint8_t  wb_servo_id, SI4796X_TUNER_wb_get_servo__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_set_servo_transform
 * 
 * Description:
 *   Configure the settings for an WB servo transform block.  servo_transform The servo transform performs multiple functions:          	It calculates what Y value is expected for a given X value.            	It then adjusts Yout at the rate specified by P1TOP2RATE or P2TOP1RATE         depending on which direction Y is traveling on the transformation function.          	The resulting Yout value is fed into the accumulator of the corresponding         servo.          	The transform loop only updates once every millisecond.  If the P1TOP2RATE or          P2TOP1RATE is too fast, the resulting output for Y will appear as steps (or a single step)          rather than a ramp. This is due to the fact that the RATEs determine the step size that          is taken on Y each ms in the given direction. While it is possible to use rates less          than 10ms, it is discouraged for this reason. The equation for computing the step size is          Y step calculation
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_servo_transform_id: WB servo transform ID
 *  wb_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.   Using a metric value of 0, disables this servo transform.
 *  wb_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  WB_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 *  enable_other0: Indicates whether or not this servo transform is linked to another servo transform's Y max
 *  other_transform_at_ymax: Servo transform ID of cascaded servo transform that must be at Y max  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  enable_other1: Indicates whether or not this servo transform is linked to another servo transform's Y min
 *  other_transform_at_ymin: Servo transform ID of cascaded servo transform thatmust be at Y min  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  enable_trigger: Indicates whether or not the trigger specified by WB_SERVO_TRIGGER_ID is controlling this servo transform.
 *  invert_trigger: Indicates whether or not the trigger specified by WB_SERVO_TRIGGER_ID should be inverted when controlling this transform.
 *  wb_servo_trigger_id: Index of an enabling trigger
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_set_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_transform_id, uint8_t wb_metric_id, uint8_t wb_servo_id, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t p1top2rate, uint16_t p2top1rate, uint8_t enable_other0, uint8_t other_transform_at_ymax, uint8_t enable_other1, uint8_t other_transform_at_ymin, uint8_t enable_trigger, uint8_t invert_trigger, uint8_t wb_servo_trigger_id);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_set_servo_transform(uint8_t icNum, uint8_t id, uint8_t  wb_servo_transform_id, uint8_t  wb_metric_id, uint8_t  wb_servo_id, int16_t  x1, int16_t  y1, int16_t  x2, int16_t  y2, uint16_t  p1top2rate, uint16_t  p2top1rate, uint8_t  enable_other0, uint8_t  other_transform_at_ymax, uint8_t  enable_other1, uint8_t  other_transform_at_ymin, uint8_t  enable_trigger, uint8_t  invert_trigger, uint8_t  wb_servo_trigger_id);


/*
 * Command: SI4796X_TUNER_wb_get_servo_transform
 * 
 * Description:
 *   Query the settings for an WB servo transform block.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_servo_transform_id: WB servo transform ID
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_metric_id: Index of the controlling metric. This is the input (X) of the servo transform.
 *  wb_servo_id: Index of the driven servo. Y out  of the servo transform drives the accumulator of this servo. See  WB_SERVO_STATUS  for more details on each servo.
 *  x1: Smallest coordinate value for controlling metric. X1 < X2
 *  y1: First coordinate value for controlled servo
 *  x2: Largest coordinate value for controlling metric. X1 < X2
 *  y2: Second coordinate value for controlled servo
 *  p1top2rate: Time for Y out  to transition from point 1 (X 1 , Y 1 ) to point 2 (X 2 , Y 2 )
 *  p2top1rate: Time for Y out  to transition from point 2 (X 2 , Y 2 ) to point 1 (X 1 , Y 1 )
 *  other0_enabled: Indicates whether or not this servo transform is linked to another servo transform's Y max
 *  other_transform_at_ymax: Servo transform ID of cascaded servo transform that must be at Y max  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  other1_enabled: Indicates whether or not this servo transform is linked to another servo transform's Y min
 *  other_transform_at_ymin: Servo transform ID of cascaded servo transform thatmust be at Y min  in order for this transform to be enabled.   Set to 0 to disable this feature.
 *  trigger_enabled: Indicates whether or not the trigger specified by WB_SERVO_TRIGGER_ID is controlling this servo transform.
 *  trigger_inverted: Indicates whether or not the trigger specified by WB_SERVO_TRIGGER_ID should be inverted when controlling this transform.
 *  wb_servo_trigger_id: Index of an enabling trigger
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo_transform__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_transform_id);


typedef struct _SI4796X_TUNER_wb_get_servo_transform__data
{
	uint8_t WB_METRIC_ID;
	uint8_t WB_SERVO_ID;
	int16_t X1;
	int16_t Y1;
	int16_t X2;
	int16_t Y2;
	uint16_t P1TOP2RATE;
	uint16_t P2TOP1RATE;
	uint8_t OTHER0_ENABLED;
	uint8_t OTHER_TRANSFORM_AT_YMAX;
	uint8_t OTHER1_ENABLED;
	uint8_t OTHER_TRANSFORM_AT_YMIN;
	uint8_t TRIGGER_ENABLED;
	uint8_t TRIGGER_INVERTED;
	uint8_t WB_SERVO_TRIGGER_ID;
}SI4796X_TUNER_wb_get_servo_transform__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo_transform__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_get_servo_transform__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo_transform(uint8_t icNum, uint8_t id, uint8_t  wb_servo_transform_id, SI4796X_TUNER_wb_get_servo_transform__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_set_servo_trigger
 * 
 * Description:
 *   A trigger outputs an enable signal to an attached servo transform if WB_SET_SERVO_TRIGGER:THRESHOLD0:LOWER < METRIC <   WB_SET_SERVO_TRIGGER:THRESHOLD1:UPPERtrigger
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_servo_trigger_id: Index of an enabling trigger
 *  inactive_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes inactive. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  active_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes active. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  wb_metric_id: Index of the controlling metric. This is the input (X) of the servo trigger.   Using a metric value of 0, disables this servo trigger.
 *  lower: If the configured metric is below this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 *  upper: If the configured metric is above this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_set_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_trigger_id, uint8_t inactive_int, uint8_t active_int, uint8_t wb_metric_id, int16_t lower, int16_t upper);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_set_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  wb_servo_trigger_id, uint8_t  inactive_int, uint8_t  active_int, uint8_t  wb_metric_id, int16_t  lower, int16_t  upper);


/*
 * Command: SI4796X_TUNER_wb_get_servo_trigger
 * 
 * Description:
 *  
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wb_servo_trigger_id: Index of an enabling trigger
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  inactive_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes inactive. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  active_int: Allows this trigger to update the TRIGINT status bit when the trigger becomes active. Interrupts based on TRIGINT can be configured with the INTERRUPT_ENABLE0 TRIGIEN option.
 *  in_range: Indicates whether this trigger is currently between the lower and upper thresholds and outputting enable to any servo transforms attached to it.
 *  wb_metric_id: Index of the controlling metric. This is the input (X) of the servo trigger.
 *  lower: If the configured metric is below this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 *  upper: If the configured metric is above this value, the trigger will output a disable notification to any servo transforms that it is attached to.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo_trigger__command(uint8_t icNum,uint8_t id, uint8_t wb_servo_trigger_id);


typedef struct _SI4796X_TUNER_wb_get_servo_trigger__data
{
	uint8_t INACTIVE_INT;
	uint8_t ACTIVE_INT;
	uint8_t IN_RANGE;
	uint8_t WB_METRIC_ID;
	int16_t LOWER;
	int16_t UPPER;
}SI4796X_TUNER_wb_get_servo_trigger__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo_trigger__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_get_servo_trigger__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_get_servo_trigger(uint8_t icNum, uint8_t id, uint8_t  wb_servo_trigger_id, SI4796X_TUNER_wb_get_servo_trigger__data*  replyData);


/*
 * Command: SI4796X_TUNER_am_tune_freq
 * 
 * Description:
 *   Tunes to the selected frequency. The optional STC interrupt is set when the command completes the tune. Sending this command  clears any pending STCINT bit in the STATUS. Use property INTERRUPT_ENABLE0 to enable interrupts for this command. The hardware will also need to have a pin configured  as an interrupt pin. AM_TUNE_FREQ command requires the argument DIGITAL to inform the DSP which digital mode to  activate (AM HD Radio or DRM-30).  The main purpose of this argument is to inform the DSP  which digital detect metric should be calculated (either HDLEVEL or DRM30MODE).  However at  this time the user must also apply the correct .boot file because amfmwb.boot only supports  HD Radio (not DRM-30) and amfmdab.boot only supports DRM-30 (not HD Radio).  Therefore the  DIGITAL bit setting must be compatible with the .boot file; for example, booting amfmdab.boot  (for DRM-30) and setting the DIGITAL bit for HD Radio will not work or conversely booting  amfmwb.boot and setting the DIGITAL bit for DRM-30 will not work.  The DIGITAL mode must be  compatible with the boot image.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  digital: Enables HD or DRM30 functionality for this tune.
 *  tunemode: Set the desired tuning mode
 *  servo: Control behavior of servos during a tune.
 *  injection: Mixer injection side selection. Only valid for Short wave, there is no IF for Long wave, and medium wave.
 *  freq: Tune Frequency in kHz
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t digital, uint8_t tunemode, uint8_t servo, uint8_t injection, uint16_t freq);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_tune_freq(uint8_t icNum, uint8_t id, uint8_t  digital, uint8_t  tunemode, uint8_t  servo, uint8_t  injection, uint16_t  freq);


/*
 * Command: SI4796X_TUNER_am_seek_start
 * 
 * Description:
 *   Initiates a seek for a channel that meets the validation criteria for AM. If this tuner is contributing to phase diversity, FM_PHASE_DIVERSITY:OPTIONS:COMB should be set to 0 on the combining chip prior to initaiting this command and diversity should only be reenabled when both tuners are on the same frequency.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  seekup: Determines direction of seek and band limit.
 *  digital: Enables HD or DRM30 functionality for this tune.
 *  wrap: Determines seek behavior upon reaching a band limit.
 *  mode: Sets the seek mode to either search for a good station, or just take one step
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_seek_start__command(uint8_t icNum,uint8_t id, uint8_t seekup, uint8_t digital, uint8_t wrap, uint8_t mode);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_seek_start(uint8_t icNum, uint8_t id, uint8_t  seekup, uint8_t  digital, uint8_t  wrap, uint8_t  mode);


/*
 * Command: SI4796X_TUNER_am_rsq_status
 * 
 * Description:
 *   Allows the host to gather information about the currently tuned channel and the nearby channels.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  attune: Return all signal quality values as of AM_VALIDATION_TIME after tune.
 *  cancel: Aborts a seek or tune validation currently in progress
 *  stcack: Clears the STC interrupt status indicator if set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  hdsearchdone: HD Search indicator
 *  drm30_detect_done: DRM30 Detect results are valid
 *  bltf: Band Limit after FM_SEEK_START. Reports if a seek hit the band limit or wrapped to the original frequency. This does not indicate that the seek failed, only that further seeks from the current location would be unproductive.
 *  t3ready: AM_valid_Timer3 status. When set, indicates that the AM_valid_timer3 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  AM_VALID_TIMER3  and  AM_VALID_TIMER3_METRICS . The reason for exit could be that we failed to validate the timer3_metrics, or the other timers were not configured.
 *  t2ready: AM_valid_Timer2 status. When set, indicates that the AM_valid_timer2 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  AM_VALID_TIMER2  and  AM_VALID_TIMER2_METRICS . The reason for exit could be that we failed to validate the timer2_metrics, or the other timers were not configured.
 *  t1ready: AM_valid_Timer1 status. When set, indicates that the AM_valid_timer1 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  AM_VALID_TIMER1  and  AM_VALID_TIMER1_METRICS . The reason for exit could be that we failed to validate the timer1_metrics, or the other timers were not configured.
 *  inject: Injection side
 *  afcrl: AFC rail indicator
 *  valid: Reports if the channel is valid based on the settings of AM_VALID_RSSI_THRESHOLD, AM_VALID_SNR_THRESHOLD, AM_VALID_ISSI, AM_VALID_MAX_TUNE_ERROR
 *  freq: Tune Frequency in kHz
 *  freqoff: Signed frequency offset in units of 25Hz.
 *  rssi: Received signal strength indicator
 *  snr: RF carrier to noise indicator
 *  lassi: Low side 1st adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  hassi: High side 1st adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  lassi2: Low side the second adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  hassi2: High side the second adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  lassi3: Low side adjacent the third adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  hassi3: High side adjacent the third adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  lassi4: Low side adjacent the fourth adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  hassi4: High side adjacent the fourth adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier. NOTE: "adjacent" is defined by property 0x4102 ( AM_SEEK_SPACING ).
 *  assi: The highest of HASSI/LASSI
 *  assi2: The highest of HASSI2/LASSI2
 *  assi3: The highest of HASSI3/LASSI3
 *  assi4: The highest of HASSI4/LASSI4
 *  iqsnr: AM SNR indicator. It is based on the ratio of RMS of in-phase-signal to the RMS of quadrature-signal. This can be used when AM station has phase modulation(AMSS), and it is recommended to set AM PLL in wideband mode.
 *  modindex: AM modulation index
 *  hdlevel: HD LEVEL Indicator. Reports a HD availability confidence factor that is normalized to the number of symbols periods examined. HD LEVEL is not calculated for DAB I/Q rate mode.
 *  rssi_adj: Received signal strength indicator adjusted by  AM_RSQ_RSSI_OFFSET .
 *  rssi_nc: Received signal strength indicator without RF compensation.
 *  rssi_nc_adj: Received signal strength indicator without RF compensation adjusted by  AM_RSQ_RSSI_OFFSET .
 *  filtered_hdlevel: HD LEVEL Indicator. Reports a filtered version HD availability confidence factor that is normalized to the number of symbols periods examined. HD LEVEL is not calculated for DAB I/Q rate mode.
 *  am_symmetry: This metric reports the AM sideband symmetry as a difference in power in dBFS. A value of 0 means that the sidebands contain the same amount of power.
 *  cochanm: Measures low frequency carrier magnitude variation and reports the level of cochannel interference that would cause the measured variation. Cochannel interference is when two carriers are in the same AM channel. The cochannel measurement path is reset upon tune. Initial settling of the filters in the cochannel measurement path cause coChanM to be unreliable for the first 90ms after tune. When the AM channel has two carriers, the frequency difference between those two carriers shows up as a beat frequency. Low beat frequencies take longer to detect than faster beat frequencies. A ripple detector in the cochannel measurement path is intended to detect the beat frequency. The ripple detector uses a sliding decay rate to ignore transients in the initial 90ms and to begin tracking higher beat frequencies quickly. The sliding decay rate reaches its final value approximately 1.3 seconds after tune. coChanM reports a cochannel problem with beat frequency of Fdiff Hz within 3dB of the final cycle of coChanM values approximately (90 + 1500/Fdiff)mS after tune.
 *  amic_blend: Percentage of audio is provided by AMIC engine vs AM demod of choice(from  AM_DEMOD_OPTIONS:MODE ). Audio = AMIC*x+(1-x)*AM_DEMOD
 *  avc_status: Current AVC gain.
 *  drm30mode: Which DRM30 (if any) is detected
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack);


typedef struct _SI4796X_TUNER_am_rsq_status__data
{
	uint8_t HDSEARCHDONE;
	uint8_t DRM30_DETECT_DONE;
	uint8_t BLTF;
	uint8_t T3READY;
	uint8_t T2READY;
	uint8_t T1READY;
	uint8_t INJECT;
	uint8_t AFCRL;
	uint8_t VALID;
	uint16_t FREQ;
	int8_t FREQOFF;
	int8_t RSSI;
	int8_t SNR;
	int8_t LASSI;
	int8_t HASSI;
	int8_t LASSI2;
	int8_t HASSI2;
	int8_t LASSI3;
	int8_t HASSI3;
	int8_t LASSI4;
	int8_t HASSI4;
	int8_t ASSI;
	int8_t ASSI2;
	int8_t ASSI3;
	int8_t ASSI4;
	int8_t IQSNR;
	uint8_t MODINDEX;
	uint8_t HDLEVEL;
	int8_t RSSI_ADJ;
	int8_t RSSI_NC;
	int8_t RSSI_NC_ADJ;
	uint8_t FILTERED_HDLEVEL;
	int8_t AM_SYMMETRY;
	int8_t COCHANM;
	uint8_t AMIC_BLEND;
	int8_t AVC_STATUS;
	uint8_t DRM30MODE;
}SI4796X_TUNER_am_rsq_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_rsq_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_rsq_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI4796X_TUNER_am_rsq_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_am_servo_status
 * 
 * Description:
 *   Gather the status of the AM noise mitigation engines
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  chanbw: Current channel bandwidth frequency.
 *  softmute: Current softmute attenuation.
 *  hicut: Current hi-cut cutoff frequency.
 *  lowcut: Current low-cut cutoff frequency.
 *  nbaudth: Current audio noise blanker detector threshold.   See  AM_NB . This is the threshold used by the impulse detector-1 at ~187KS/s, when the product of this setting and the instantaneous ultrasonic noise(USN1, noise from frequency band higher than 20Khz) is larger than the estimated noise floor of USN1, an impulse is detected and the results are used by AUD impulse blanker. If (instantaneous peak USN1 * NBAUDTH) > band-limit filtered USN1, trigger noise blanker. The largest positive dB setting in the servo (100dB) is equivalent to a fractional threshold value of 0.0, and the smallest setting possible (0 dB) if equivalent to a fractional threshold value of 1.0. This threshold can be tied to transforms controlled by RSSI/IQSNR/and maximum blockers within +/-40kHz.
 *  amvarfc: Current variable Fc audio filter cutoff frequency.
 *  nbiq2th: Current I/Q noise blanker 2 detector threshold.   See  AM_NB . This is the threshold used by the impulse detector-2 at ~94KS/s, when the product of this setting and the instantaneous ultrasonic noise(USN2, noise from frequency band higher than 5Khz) is larger than the estimated noise floor of USN2, an impulse is detected and the results are used by IQ impulse blanker 2. If (instantaneous peak USN2 * NBIQ2TH) > band-limit filtered USN2, trigger noise blanker. The largest positive dB setting in the servo (100dB) is equivalent to a fractional threshold value of 0.0, and the smallest setting possible (0 dB) if equivalent to a fractional threshold value of 1.0. This threshold can be tied to transforms controlled by RSSI and or SNR.
 *  nbiq3th: Current I/Q noise blanker 3 detector threshold.   See  AM_NB . This is the threshold used by the impulse detector-3 at ~46KS/s, when the product of this setting and the instantaneous ultrasonic noise(USN3, noise from frequency band higher than 5Khz) is larger than the estimated noise floor of USN3, an impulse is detected and the results are used by impulse blanker 3. If (instantaneous peak USN3 * NBIQ3TH) > band-limit filtered USN3, trigger noise blanker. The largest positive dB setting in the servo (100dB) is equivalent to a fractional threshold value of 0.0, and the smallest setting possible (0 dB) if equivalent to a fractional threshold value of 1.0. This threshold can be tied to transforms controlled by RSSI/IQSNR/and maximum blockers within +/-30kHz.
 *  maxavc: Current Maximum AVC gain.   See  AM_AVC .
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_servo_status__command(uint8_t icNum,uint8_t id);


typedef struct _SI4796X_TUNER_am_servo_status__data
{
	uint8_t CHANBW;
	uint8_t SOFTMUTE;
	uint8_t HICUT;
	uint8_t LOWCUT;
	uint8_t NBAUDTH;
	uint8_t AMVARFC;
	uint8_t NBIQ2TH;
	uint8_t NBIQ3TH;
	int8_t MAXAVC;
}SI4796X_TUNER_am_servo_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_servo_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_servo_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_servo_status(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_servo_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_am_servo_trigger_status
 * 
 * Description:
 *   Reports the current status of all AM servo triggers
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trigint_ack: Acknowledge TRIGINT and clears it in the status word.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  servotrigger: (No Description)
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_servo_trigger_status__command(uint8_t icNum,uint8_t id, uint8_t trigint_ack);


typedef struct _SI4796X_TUNER_am_servo_trigger_status_servotrigger__data
{
	int16_t X_IN;
	uint8_t ABOVE_INT;
	uint8_t BELOW_INT;
	uint8_t ACTIVE_INT;
	uint8_t ACTIVE;
	uint8_t CONFIGURED;
}SI4796X_TUNER_am_servo_trigger_status_servotrigger__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_servo_trigger_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_servo_trigger_status_servotrigger__data* servotrigger);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_servo_trigger_status(uint8_t icNum, uint8_t id, uint8_t  trigint_ack, SI4796X_TUNER_am_servo_trigger_status_servotrigger__data*  servotrigger);


/*
 * Command: SI4796X_TUNER_am_signal_event_count
 * 
 * Description:
 *   Reports the status of the AM noise blankers
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  nb_aud_cnt: Number of times the audio noise blanker fired in the last 2.818 seconds
 *  nb_iq_cnt2: Number of times the I/Q noise blanker 2 fired in the last 2.818 seconds
 *  nb_iq_cnt3: Number of times the I/Q noise blanker 3 fired in the last 2.818 seconds
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_signal_event_count__command(uint8_t icNum,uint8_t id);


typedef struct _SI4796X_TUNER_am_signal_event_count__data
{
	uint16_t NB_AUD_CNT;
	uint16_t NB_IQ_CNT2;
	uint16_t NB_IQ_CNT3;
}SI4796X_TUNER_am_signal_event_count__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_signal_event_count__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_signal_event_count__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_am_signal_event_count(uint8_t icNum, uint8_t id, SI4796X_TUNER_am_signal_event_count__data*  replyData);


/*
 * Command: SI4796X_TUNER_dab_tune_freq
 * 
 * Description:
 *   DAB_TUNE_FREQ sets the DAB Receiver to tune to a frequency between 168.16 MHz and 239.20 MHz or between 1.452 GHz and 1.492 GHz as defined by the DAB_SET_FREQ_LIST table.
 *   The optional STC interrupt is set when the command completes the tune. Sending this command  clears any pending STCINT bit in the STATUS. 
 *   The default list that will be used by the tuner is the European frequency list. To change  this list (example: for T-DMB), the user must first call DAB_SET_FREQ_LIST before  calling the DAB_TUNE_FREQ command.
 *   During a validated tune operation, DAB Detect associates transmission modes to validate with the DETECTMODE  in the DAB_TUNE_FREQ, and DAB_VALID_TIMER1_METRICS associates a metric threshold  check at DAB_VALID_TIMER1. If a metric does not meet the threshold requirements, then the tune will be called invalid.  The DAB Detect and the DAB_VALID_TIMER1_METRICS are ran in parallel . If one of the two metrics finishes its  validation time and is valid, it will wait for the other metric to finish. If the metric does not meet its threshold  it will be called invalid, and will not run the other metric. The validation time for the DAB_VALID_TIMER1_METRICS  is set by the DAB_VALID_TIMER1. The Max validation time for the DAB Detect is determined by the transmission mode selected. The DAB Detect Transmission mode, and Valid, will be reported in DAB_RSQ_STATUS.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  detectmode1: When ENABLE, will start the DAB Detect engine for VALID transmission mode 1. The DAB Detect validation time will increase by 40ms
 *  freq_index: Frequency index for the tuned frequency, see the DAB_SET_FREQ_LIST command that sets the frequency table.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_dab_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t detectmode1, uint16_t freq_index);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_dab_tune_freq(uint8_t icNum, uint8_t id, uint8_t  detectmode1, uint16_t  freq_index);


/*
 * Command: SI4796X_TUNER_dab_rsq_status
 * 
 * Description:
 *   Allows the host to gather information about the currently tuned channel and the nearby channels.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  attune: Return all signal quality values as of DAB_VALIDATION_TIME after tune.
 *  cancel: Aborts a seek or tune validation currently in progress
 *  stcack: Clears the STC interrupt status indicator if set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  t1ready: DAB_valid_Timer1 status. When set, indicates that the DAB_valid_timer1 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  DAB_VALID_TIMER1  and  DAB_VALID_TIMER1_METRICS . The reason for exit could be that we failed to validate the timer1_metrics, or the other timers were not configured.
 *  valid: Reports if the channel is valid based on the metrics selected in  DAB_VALID_TIMER1_METRICS . If DETECTMODE is selected under OPTIONS in  DAB_TUNE_FREQ , then the channel is reported to be valid based on the metrics selected in  DAB_VALID_TIMER1_METRICS , and a valid DABDETECT value under  DAB_RSQ_STATUS
 *  freq_index: Frequency index for the tuned frequency, see the DAB_SET_FREQ_LIST command that sets the frequency table.
 *  freq: Frequency in Hz.
 *  sqi: This Signal Quality Index is the difference between the narrow-band signal strength and the wide-band signal strength. It is useful to disqualify a station with a first strong adjacent that caused a false fast dab detect.
 *  rssi: Received signal strength indicator
 *  dabdetect: Set when DAB signal is detected at the tuned frequency index. Value of 0 indicates that DAB is not detected. Value 1-4 indicates a DAB signal is detected and the transmission mode found.
 *  rssi_adj: Received signal strength indicator adjusted by  DAB_RSQ_RSSI_OFFSET .
 *  digital_gain: Reports digital AGC gain
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_dab_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack);


typedef struct _SI4796X_TUNER_dab_rsq_status__data
{
	uint8_t T1READY;
	uint8_t VALID;
	uint16_t FREQ_INDEX;
	uint32_t FREQ;
	int8_t SQI;
	int8_t RSSI;
	uint8_t DABDETECT;
	int8_t RSSI_ADJ;
	int8_t DIGITAL_GAIN;
}SI4796X_TUNER_dab_rsq_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_dab_rsq_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_dab_rsq_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_dab_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI4796X_TUNER_dab_rsq_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_tune_freq
 * 
 * Description:
 *   Tunes to the selected frequency. 
 *   The optional STC interrupt is set when the command completes the tune. Sending this command  clears any pending STCINT bit in the STATUS. Use property INTERRUPT_ENABLE0 to enable interrupts for this command. The hardware will also need to have a pin configured  as an interrupt pin.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tunemode: Set the desired tuning mode
 *  servo: Control behavior of servos during a tune.
 *  injection: Mixer injection side selection
 *  freq: Tune Frequency in multiples of 25.0 kHz added to a starting frequency of 162400000 Hz. Range: 0 to 6 - 162.4 Mhz - 162.55 Mhz
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_tune_freq__command(uint8_t icNum,uint8_t id, uint8_t tunemode, uint8_t servo, uint8_t injection, uint16_t freq);



// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_tune_freq(uint8_t icNum, uint8_t id, uint8_t  tunemode, uint8_t  servo, uint8_t  injection, uint16_t  freq);


/*
 * Command: SI4796X_TUNER_wb_rsq_status
 * 
 * Description:
 *   Allows the host to gather information about the currently tuned channel and the nearby channels.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  attune: Return all signal quality values as of WB_VALIDATION_TIME after tune.
 *  cancel: Aborts a seek or tune validation currently in progress
 *  stcack: Clears the STC interrupt status indicator if set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  t3ready: WB_valid_Timer3 status. When set, indicates that the WB_valid_timer3 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  WB_VALID_TIMER3  and  WB_VALID_TIMER3_METRICS . The reason for exit could be that we failed to validate the timer3_metrics, or the other timers were not configured.
 *  t2ready: WB_valid_Timer2 status. When set, indicates that the WB_valid_timer2 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. For example, if T1READY is set, and T2READY (if applicable for this particular mode) is zero, it indicates that we exited right after validating T1 metrics. See  WB_VALID_TIMER2  and  WB_VALID_TIMER2_METRICS . The reason for exit could be that we failed to validate the timer2_metrics, or the other timers were not configured.
 *  t1ready: Timer1 status. When set, indicates that the timer1 has completed during a tune. This flag is not used during a fast tune. This flag, along with the other t#ready flags can be used to determine at which stage the tune exited. See WB_VALID_TIMER1 and WB_VALID_TIMER1_METRICS.
 *  inject: Injection side
 *  afcrl: AFC rail indicator
 *  valid: Reports if the channel is valid based on the settings of WB_VALID_RSSI_THRESHOLD, WB_VALID_SNR_THRESHOLD, WB_VALID_ISSI, WB_VALID_MAX_TUNE_ERROR
 *  freq: Tune Frequency in multiples of 25.0 kHz added to a starting frequency of 162400000 Hz. Range: 0 to 6 - 162.4 Mhz - 162.55 Mhz
 *  freqoff: Signed frequency offset in units of 200Hz.
 *  rssi: Received signal strength indicator
 *  snr: RF carrier to noise indicator
 *  issi: Image signal strength indicator. Signal+noise power level at the image frequency reported in dB relative to the desired
 *  lassi: Low side 1st adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  hassi: High side 1st adjacent signal strength indicator. Signal + noise power level reported in dB relative to the carrier.
 *  assi: The highest of HASSI/LASSI
 *  rssi_adj: Received signal strength indicator adjusted by  WB_RSQ_RSSI_OFFSET .
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_rsq_status__command(uint8_t icNum,uint8_t id, uint8_t attune, uint8_t cancel, uint8_t stcack);


typedef struct _SI4796X_TUNER_wb_rsq_status__data
{
	uint8_t T3READY;
	uint8_t T2READY;
	uint8_t T1READY;
	uint8_t INJECT;
	uint8_t AFCRL;
	uint8_t VALID;
	uint16_t FREQ;
	int8_t FREQOFF;
	int8_t RSSI;
	int8_t SNR;
	int8_t ISSI;
	int8_t LASSI;
	int8_t HASSI;
	int8_t ASSI;
	int8_t RSSI_ADJ;
}SI4796X_TUNER_wb_rsq_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_rsq_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_rsq_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_rsq_status(uint8_t icNum, uint8_t id, uint8_t  attune, uint8_t  cancel, uint8_t  stcack, SI4796X_TUNER_wb_rsq_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_asq_status
 * 
 * Description:
 *   Retrieve ASQ status metrics and clear ASQ interrupts if ASQACK bit was set.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  asqack: Clears the ASQ interrupt status indicator if set.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  alertoffint: Interrupt Source: 1050Hz alert tone not present.
 *  alertonint: Interrupt Source: 1050Hz alert tone detected.
 *  alert: 1050Hz alert tone is present or not.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_asq_status__command(uint8_t icNum,uint8_t id, uint8_t asqack);


typedef struct _SI4796X_TUNER_wb_asq_status__data
{
	uint8_t ALERTOFFINT;
	uint8_t ALERTONINT;
	uint8_t ALERT;
}SI4796X_TUNER_wb_asq_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_asq_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_asq_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_asq_status(uint8_t icNum, uint8_t id, uint8_t  asqack, SI4796X_TUNER_wb_asq_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_servo_status
 * 
 * Description:
 *   Gather the status of the WB noise mitigation engines
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  chanbw: Current channel bandwidth frequency.
 *  softmute: Current softmute attenuation.
 *  hicut: Current hi-cut cutoff frequency.
 *  lowcut: Current low-cut cutoff frequency.
 *  nbthr: Current USN noise blanker detector threshold.
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_servo_status__command(uint8_t icNum,uint8_t id);


typedef struct _SI4796X_TUNER_wb_servo_status__data
{
	uint8_t CHANBW;
	uint8_t SOFTMUTE;
	uint8_t HICUT;
	uint8_t LOWCUT;
	uint8_t NBTHR;
}SI4796X_TUNER_wb_servo_status__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_servo_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_servo_status__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_servo_status(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_servo_status__data*  replyData);


/*
 * Command: SI4796X_TUNER_wb_servo_trigger_status
 * 
 * Description:
 *   Reports the current status of all WB servo triggers
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trigint_ack: Acknowledge TRIGINT and clears it in the status word.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  servotrigger: (No Description)
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_servo_trigger_status__command(uint8_t icNum,uint8_t id, uint8_t trigint_ack);


typedef struct _SI4796X_TUNER_wb_servo_trigger_status_servotrigger__data
{
	int16_t X_IN;
	uint8_t ABOVE_INT;
	uint8_t BELOW_INT;
	uint8_t ACTIVE_INT;
	uint8_t ACTIVE;
	uint8_t CONFIGURED;
}SI4796X_TUNER_wb_servo_trigger_status_servotrigger__data;

R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_servo_trigger_status__reply(uint8_t icNum, uint8_t id, SI4796X_TUNER_wb_servo_trigger_status_servotrigger__data* servotrigger);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_servo_trigger_status(uint8_t icNum, uint8_t id, uint8_t  trigint_ack, SI4796X_TUNER_wb_servo_trigger_status_servotrigger__data*  servotrigger);


/*
 * Command: SI4796X_TUNER_wb_signal_event_count
 * 
 * Description:
 *   Reports the status of the WB noise blankers
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  nb_aud_cnt: Number of times the audio noise blanker fired in the last 2.818 seconds
 * 
 */
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_signal_event_count__command(uint8_t icNum,uint8_t id);



R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_signal_event_count__reply(uint8_t icNum, uint8_t id, uint16_t* nb_aud_cnt);


// Refer to __command + __reply documentation.
R_4_1_SI4796X_TUNER_API_EXT RETURN_CODE SI4796X_TUNER_wb_signal_event_count(uint8_t icNum, uint8_t id, uint16_t*  nb_aud_cnt);

#ifdef __cplusplus
} // extern C
#endif

#endif
