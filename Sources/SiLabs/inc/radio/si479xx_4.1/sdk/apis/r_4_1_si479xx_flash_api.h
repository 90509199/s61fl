/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *		This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 */
 
#ifndef si479xx_flash_api_h_
#define si479xx_flash_api_h_

#ifdef _r_4_1_si479xx_flash_api_h_global_		/* 210419 SGsoft */
#define R_4_1_SI479XX_FLASH_API_EXT
#else
#define R_4_1_SI479XX_FLASH_API_EXT	extern
#endif	/*_r_4_1_si479xx_flash_api_h_global_*/

///#include "si479xx_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#if 0		/* 210512 SGsoft */	
#ifdef _r_4_1_si479xx_flash_api_h_global_		/* 210419 SGsoft */
const uint16_t SI479XX_FLASH_PROPS[5] = {
    SI479XX_FLASH_PROP_FLASH_LOAD_SPI_CLOCK_FREQ_MHZ_ADDR,
    SI479XX_FLASH_PROP_FLASH_LOAD_SPI_SPI_MODE_ADDR,
    SI479XX_FLASH_PROP_FLASH_LOAD_SPI_SPI_DUAL_READ_ADDR,
    SI479XX_FLASH_PROP_FLASH_LOAD_READ_READ_CMD_ADDR,
    SI479XX_FLASH_PROP_FLASH_LOAD_READ_HIGH_SPEED_READ_CMD_ADDR,
};
#else
// List of valid property addresses defined by this API. (mainly for debug)
extern const uint16_t SI479XX_FLASH_PROPS[5];
#endif	/*_r_4_1_si479xx_flash_api_h_global_*/
#endif	/*0*/

/*
 * Command: SI479XX_FLASH_load_img
 * 
 * Description:
 *   LOAD_IMG subcommand: LOAD_IMG
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  addr: Starting address on flash of the block to read
 *  size: Number of bytes to read.
 * 
 */
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_load_img__command(uint8_t icNum,uint32_t addr, uint32_t size);



// Refer to __command + __reply documentation.
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_load_img(uint8_t icNum, uint32_t  addr, uint32_t  size);


/*
 * Command: SI479XX_FLASH_get_prop
 * 
 * Description:
 *   LOAD_IMG subcommand: Retrieve a property's value; The value will either be the default or the value set with SET_PROP.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  prop_id: Property ID
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  prop_val: Property value
 * 
 */
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_get_prop__command(uint8_t icNum,uint16_t prop_id);



R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_get_prop__reply(uint8_t icNum, uint16_t* prop_val);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_get_prop(uint8_t icNum, uint16_t  prop_id, uint16_t*  prop_val);



typedef struct SI479XX_FLASH_set_prop_propset__data
{
	uint16_t PROP_ID;
	uint16_t PROP_VAL;
} SI479XX_FLASH_set_prop_propset__data;
/*
 * Command: SI479XX_FLASH_set_prop
 * 
 * Description:
 *   LOAD_IMG subcommand: Sets a property's value.  NOTE: Two extra bytes of 0s, here as SET_PROP:PROP_END:END_0X0000, need to be sent at the end, following the last PROP_VAL[15:8], to indicate the end of property setting.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  propset: (No Description)
 *  end_0x0000: Two extra bytes of 0s need to be sent at the end, following the last PROP_VAL[15:8], to indicate the end of property setting.
 *  propset_length (input): #elems in 'propset' array.
 * 
 */
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_set_prop__command(uint8_t icNum, SI479XX_FLASH_set_prop_propset__data* propset, uint16_t propset_length);

// Refer to __command + __reply documentation.
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_set_prop(uint8_t icNum, SI479XX_FLASH_set_prop_propset__data*  propset, uint16_t  propset_length);


/*
 * Command: SI479XX_FLASH_read_block
 * 
 * Description:
 *   Read a block of bytes from the flash
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  addr: Starting address on flash of the block to read
 *  size: Number of bytes to read.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  data: Flash data byte
 *  size (input): #elems in 'data' array.
 * 
 */
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_read_block__command(uint8_t icNum,uint32_t addr, uint32_t size);


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_read_block__reply(uint8_t icNum, uint8_t* data, uint16_t size);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_FLASH_API_EXT RETURN_CODE SI479XX_FLASH_read_block(uint8_t icNum, uint32_t  addr, uint32_t  arg_size, uint8_t*  data, uint16_t  rep_size);


#ifdef __cplusplus
} // extern C
#endif

#endif
