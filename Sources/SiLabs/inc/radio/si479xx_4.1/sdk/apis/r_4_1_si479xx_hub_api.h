/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/



/***************************************************************************************
   Generated firmware api for Si479xx command/property interface
   Date: 9/27/2019
   Firmware Version: 4_0_0_0_5
****************************************************************************************/

/*
 * Automatically generated file. Do not edit.
 *
 *	Important Note:
 *	1.	This SDK contains dynamically allocated memory objects that is not freed within
 * 		the SDK functions. Please note that it is the developer's responsibility to free 
 *		the memory of any objects dynamically allocated within this SDK. There are notes 
 *		in the function headers where memory is dynamically allocated and not freed within
 *		the SDK.
 *
 *  2. All of the xxx__reply() functions use malloc() + free(). return code meaning:
 *		SUCCESS -  okay. caller may have to free() memory (function specific, see below)
 *		
 *		otherwise(not SUCCESS) - caller shouldn't have to free memory, function should clean up.
 *		
 *		MEMORY_ERROR - malloc() failed
 *		COMMAND_ERROR - most likely, prior command was not matching function
 *		other error code - implementation of writeCommand() or readReply() likely failed.
 *	
 */
 

#ifndef si479xx_hub_api_h_
#define si479xx_hub_api_h_

#ifdef _r_4_1_si479xx_hub_api_h_global_		/* 210419 SGsoft */
#define R_4_1_SI479XX_HUB_API_EXT
#else
#define R_4_1_SI479XX_HUB_API_EXT	extern
#endif	/*_r_4_1_si479xx_hub_api_h_global_*/

///#include "si479xx_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Command: SI479XX_HUB_hub_mgr_nop
 * 
 * Description:
 *   This command generates a response but no other action is performed.  It serves a simple  check that the API-HiFi2 interface is functional.
 *   STATUS always returns 0.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_hub_mgr_nop__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_hub_mgr_nop__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_hub_mgr_nop__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_hub_mgr_nop__parse(uint8_t* buf, SI479XX_HUB_hub_mgr_nop__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_hub_mgr_nop__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_hub_mgr_nop__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_hub_mgr_nop(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_hub_mgr_nop__data*  replyData);


/*
 * Command: SI479XX_HUB_debug_enable
 * 
 * Description:
 *   On receipt of this command, the audio hub will enable the debug port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_enable__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_debug_enable__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_debug_enable__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_enable__parse(uint8_t* buf, SI479XX_HUB_debug_enable__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_enable__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_debug_enable__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_enable(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_debug_enable__data*  replyData);


/*
 * Command: SI479XX_HUB_debug_disable
 * 
 * Description:
 *   On receipt of this command, the audio hub will disable the debug port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_disable__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_debug_disable__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_debug_disable__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_disable__parse(uint8_t* buf, SI479XX_HUB_debug_disable__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_disable__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_debug_disable__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_debug_disable(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_debug_disable__data*  replyData);


/*
 * Command: SI479XX_HUB_trace_port
 * 
 * Description:
 *   On receipt of this command, the audio hub will route audio data from the specified port (all channels) to the debug port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  connect: Connect or disconnect the port to the debug port
 *  length: Number of bytes in this command
 *  port_id: Port ID numbers
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  port_id: Port ID numbers
 *  connect: Connect or disconnect the port to the debug port
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_port__command(uint8_t icNum,uint8_t pipe, uint8_t connect, uint16_t length, uint8_t port_id);


typedef struct _SI479XX_HUB_trace_port__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t PORT_ID;
	uint8_t CONNECT;
}SI479XX_HUB_trace_port__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_port__parse(uint8_t* buf, SI479XX_HUB_trace_port__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_port__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_port__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_port(uint8_t icNum, uint8_t pipe, uint8_t  connect, uint16_t  length, uint8_t  port_id, SI479XX_HUB_trace_port__data*  replyData);


/*
 * Command: SI479XX_HUB_trace_node
 * 
 * Description:
 *   On receipt of this command, the audio hub will copy audio data from the specified data node to the debug port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  connect: Connect or disconnect the node to the debug port
 *  length: Number of bytes in this command
 *  xbar_id: Specifies the input or output cross-bar (xbar)
 *  node_id: Node ID numbers: The number of nodes instantiated in any given system is a function of the topology. The maximum number of nodes on either cross-bar is 32.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  xbar_id: Specifies the input or output cross-bar (xbar)
 *  node_id: Node ID numbers: The number of nodes instantiated in any given system is a function of the topology. The maximum number of nodes on either cross-bar is 32.
 *  connect: Connect or disconnect the node to the debug port
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_node__command(uint8_t icNum,uint8_t pipe, uint8_t connect, uint16_t length, uint8_t xbar_id, uint8_t node_id);


typedef struct _SI479XX_HUB_trace_node__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t XBAR_ID;
	uint8_t NODE_ID;
	uint8_t CONNECT;
}SI479XX_HUB_trace_node__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_node__parse(uint8_t* buf, SI479XX_HUB_trace_node__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_node__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_node__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_node(uint8_t icNum, uint8_t pipe, uint8_t  connect, uint16_t  length, uint8_t  xbar_id, uint8_t  node_id, SI479XX_HUB_trace_node__data*  replyData);


/*
 * Command: SI479XX_HUB_slab_trace_mask_read
 * 
 * Description:
 *   Report the contents of the SLAB trace mask.  The trace mask consists of 16 bytes (128 bits).   Each bit controls a class of SLATE-trace SLAB_TRACE_MASK_SET_BIT:BITNO .
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  trace_mask: The trace mask array is a set of 128 bits. Each bit may be used to control (enable or disable) one or more trace statements in the code.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_read__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_slab_trace_mask_read__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TRACE_MASK[16];
}SI479XX_HUB_slab_trace_mask_read__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_read__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_read__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_read__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_read(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_slab_trace_mask_read__data*  replyData);


/*
 * Command: SI479XX_HUB_slab_trace_mask_write
 * 
 * Description:
 *   Writes the contents of the SLAB trace mask.  The trace mask consists of 16 bytes (128 bits). Each bit controls a class of SLATE-trace SLAB_TRACE_MASK_SET_BIT:BITNO.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  trace_mask: The trace mask array is a set of 128 bits. Each bit may be used to control (enable or disable) one or more trace statements in the code.
 *  trace_mask_len (input): #elems in 'trace_mask' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_write__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t* trace_mask, uint16_t trace_mask_len);


typedef struct _SI479XX_HUB_slab_trace_mask_write__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_slab_trace_mask_write__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_write__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_write__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_write__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_write(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t*  trace_mask, uint16_t  trace_mask_len, SI479XX_HUB_slab_trace_mask_write__data*  replyData);


/*
 * Command: SI479XX_HUB_slab_trace_mask_set_bit
 * 
 * Description:
 *   Sets the specified bit in the SLAB trace mask.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trace_mask_bit_number: Bit number (0 to 127) of one control bit in the trace mask. When set to '1', all trace statements associated with the given bit are enabled. When set to '0', all trace statements assocaited with the given bit are disabled.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length);


typedef struct _SI479XX_HUB_slab_trace_mask_set_bit__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_slab_trace_mask_set_bit__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_set_bit__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_set_bit__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_set_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_slab_trace_mask_set_bit__data*  replyData);


/*
 * Command: SI479XX_HUB_slab_trace_mask_clr_bit
 * 
 * Description:
 *   Clears the specified bit in the SLAB trace mask.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trace_mask_bit_number: Bit number (0 to 127) of one control bit in the trace mask. When set to '1', all trace statements associated with the given bit are enabled. When set to '0', all trace statements assocaited with the given bit are disabled.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length);


typedef struct _SI479XX_HUB_slab_trace_mask_clr_bit__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_slab_trace_mask_clr_bit__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit__parse(uint8_t* buf, SI479XX_HUB_slab_trace_mask_clr_bit__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_slab_trace_mask_clr_bit__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_slab_trace_mask_clr_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_slab_trace_mask_clr_bit__data*  replyData);


/*
 * Command: SI479XX_HUB_trace_mask_read
 * 
 * Description:
 *   Report the contents of the trace mask.  The trace mask consists of 16 bytes (128 bits).   Each bit is available to enable or disable one or more trace statements in the code.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  trace_mask: The trace mask array is a set of 128 bits. Each bit may be used to control (enable or disable) one or more trace statements in the code.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_read__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_trace_mask_read__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TRACE_MASK[16];
}SI479XX_HUB_trace_mask_read__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_read__parse(uint8_t* buf, SI479XX_HUB_trace_mask_read__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_read__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_read(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_trace_mask_read__data*  replyData);


/*
 * Command: SI479XX_HUB_trace_mask_write
 * 
 * Description:
 *   Writes the contents of the trace mask.  The trace mask consists of 16 bytes (128 bits). Each bit is avilable to enable or disable one or more trace statements in the code.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  trace_mask: The trace mask array is a set of 128 bits. Each bit may be used to control (enable or disable) one or more trace statements in the code.
 *  trace_mask_len (input): #elems in 'trace_mask' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_write__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t* trace_mask, uint16_t trace_mask_len);


typedef struct _SI479XX_HUB_trace_mask_write__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_trace_mask_write__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_write__parse(uint8_t* buf, SI479XX_HUB_trace_mask_write__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_write__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_write(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t*  trace_mask, uint16_t  trace_mask_len, SI479XX_HUB_trace_mask_write__data*  replyData);


/*
 * Command: SI479XX_HUB_trace_mask_set_bit
 * 
 * Description:
 *   Sets the specified bit in the SDK trace mask.  The trace mask consists of 16 bytes (128 bits). Each bit is available to control a class of SLATE_trace as defined by the SDK developer.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trace_mask_bit_number: Bit number (0 to 127) of one control bit in the trace mask. When set to '1', all trace statements associated with the given bit are enabled. When set to '0', all trace statements assocaited with the given bit are disabled.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_set_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length);


typedef struct _SI479XX_HUB_trace_mask_set_bit__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_trace_mask_set_bit__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_set_bit__parse(uint8_t* buf, SI479XX_HUB_trace_mask_set_bit__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_set_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_set_bit__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_set_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_trace_mask_set_bit__data*  replyData);


/*
 * Command: SI479XX_HUB_trace_mask_clr_bit
 * 
 * Description:
 *   Clears the specified bit in the SDK trace mask.  The trace mask consists of 16 bytes (128 bits). Each bit is available to control a class of SLATE-trace as defined by the SDK developer.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  trace_mask_bit_number: Bit number (0 to 127) of one control bit in the trace mask. When set to '1', all trace statements associated with the given bit are enabled. When set to '0', all trace statements assocaited with the given bit are disabled.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_clr_bit__command(uint8_t icNum,uint8_t pipe, uint8_t trace_mask_bit_number, uint16_t length);


typedef struct _SI479XX_HUB_trace_mask_clr_bit__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_trace_mask_clr_bit__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_clr_bit__parse(uint8_t* buf, SI479XX_HUB_trace_mask_clr_bit__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_clr_bit__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_trace_mask_clr_bit__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_trace_mask_clr_bit(uint8_t icNum, uint8_t pipe, uint8_t  trace_mask_bit_number, uint16_t  length, SI479XX_HUB_trace_mask_clr_bit__data*  replyData);


/*
 * Command: SI479XX_HUB_error_log_read
 * 
 * Description:
 *   On receipt of this command, the audio hub will: i. Return the contents of the error log in the response message. ii. Optionally, the hub manager will also clear the error log.
 *   Setting CLEAR_LOG to 1 will flush the error log; otherwise the command will leave the  error log unchanged. The audio hub manages an error log as a circular buffer of  tunable size.  When system faults occur, an error is recorded in the log.  Errors are  continually recorded in the log as they occur and the buffer will wrap around when full.   Each error is recorded as a 32-bit error code and a 32-bit time stamp.  The time stamp  is the system frame count since reboot.  For a typical frame interval of 1 ms, the  time stamp will record events over a ~50 day period.
 *   The response message echoes the command and reports the CLEAR_LOG option.  The log is  reported from oldest time stamp to most recent time stamp.  Only logged errors are  reported.  If the log is empty, the response will report a LENGTH of zero.  Error  codes and time stamps are reported in little endian format.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  clear_log: Clear the log
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  error: (No Description)
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_read__command(uint8_t icNum,uint8_t pipe, uint8_t clear_log, uint16_t length);



typedef struct SI479XX_HUB_error_log_read_error__data
{
    uint8_t ERROR_BYTE_0;
    uint8_t ERROR_BYTE_1;
    uint8_t ERROR_BYTE_2;
    uint8_t ERROR_CODE;
    uint8_t FRAME_NUMBER_BYTE_0;
    uint8_t FRAME_NUMBER_BYTE_1;
    uint8_t FRAME_NUMBER_BYTE_2;
    uint8_t FRAME_NUMBER_BYTE_3;
}SI479XX_HUB_error_log_read_error__data;

typedef struct SI479XX_HUB_error_log_read__data
{
    uint8_t CMD_NUM;
    int8_t STATUS;
    uint16_t LENGTH;
    // manual
    SI479XX_HUB_error_log_read_error__data* ERROR;
    uint16_t ERROR_DATA_COUNT;

}SI479XX_HUB_error_log_read__data;


/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_read__parse(uint8_t* buf, SI479XX_HUB_error_log_read__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_error_log_read__data* replyData);

// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_read(uint8_t icNum, uint8_t pipe, uint8_t  clear_log, uint16_t  length, SI479XX_HUB_error_log_read__data*  replyData);


/*
 * Command: SI479XX_HUB_error_log_options
 * 
 * Description:
 *   Control the behavior of the error log
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  nowrap: Enable/disable wrapping of the error log
 *  nolog: Enable/disable error logging
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_options__command(uint8_t icNum,uint8_t pipe, uint8_t nowrap, uint8_t nolog, uint16_t length);


typedef struct _SI479XX_HUB_error_log_options__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_error_log_options__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_options__parse(uint8_t* buf, SI479XX_HUB_error_log_options__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_options__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_error_log_options__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_error_log_options(uint8_t icNum, uint8_t pipe, uint8_t  nowrap, uint8_t  nolog, uint16_t  length, SI479XX_HUB_error_log_options__data*  replyData);


/*
 * Command: SI479XX_HUB_dsp_memory_query
 * 
 * Description:
 *   On receipt of this command, the audio hub returns the low-water mark for the stack pointer and the high-water mark for the heap pointer since reset.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  min_stack: Minimum stack pointer since reset
 *  max_heap: Maximum heap pointer since reset
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_dsp_memory_query__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_dsp_memory_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint32_t MIN_STACK;
	uint32_t MAX_HEAP;
}SI479XX_HUB_dsp_memory_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_dsp_memory_query__parse(uint8_t* buf, SI479XX_HUB_dsp_memory_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_dsp_memory_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_dsp_memory_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_dsp_memory_query(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_dsp_memory_query__data*  replyData);


/*
 * Command: SI479XX_HUB_frame_number_query
 * 
 * Description:
 *   On receipt of this command, the audio hub returns the current frame number.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  frame_number: Audio DSP frame number
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_frame_number_query__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_frame_number_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint32_t FRAME_NUMBER;
}SI479XX_HUB_frame_number_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_frame_number_query__parse(uint8_t* buf, SI479XX_HUB_frame_number_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_frame_number_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_frame_number_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_frame_number_query(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_frame_number_query__data*  replyData);


/*
 * Command: SI479XX_HUB_query_waveform_heap
 * 
 * Description:
 *   Returns the number of bytes left in the heap used to store waveform files and tone sequences.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  free_bytes: Number of bytes left in the waveform heap
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_waveform_heap__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_query_waveform_heap__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint32_t FREE_BYTES;
}SI479XX_HUB_query_waveform_heap__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_waveform_heap__parse(uint8_t* buf, SI479XX_HUB_query_waveform_heap__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_waveform_heap__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_waveform_heap__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_waveform_heap(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_query_waveform_heap__data*  replyData);


/*
 * Command: SI479XX_HUB_cpu_occupancy_query
 * 
 * Description:
 *   On receipt of this command, the audio hub returns the average and peak CPU load in %.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  reset_peak: Reset peak CPU occupancy.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  cpu_ave: CPU average occupancy (%) in S16.8 format
 *  cpu_peak: CPU peak occupancy (%) in S16.8 format
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_cpu_occupancy_query__command(uint8_t icNum,uint8_t pipe, uint8_t reset_peak, uint16_t length);


typedef struct _SI479XX_HUB_cpu_occupancy_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	int16_t CPU_AVE;
	int16_t CPU_PEAK;
}SI479XX_HUB_cpu_occupancy_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_cpu_occupancy_query__parse(uint8_t* buf, SI479XX_HUB_cpu_occupancy_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_cpu_occupancy_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_cpu_occupancy_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_cpu_occupancy_query(uint8_t icNum, uint8_t pipe, uint8_t  reset_peak, uint16_t  length, SI479XX_HUB_cpu_occupancy_query__data*  replyData);


/*
 * Command: SI479XX_HUB_set_sleep_mode
 * 
 * Description:
 *   On receipt of this command, the audio hub will set the sleep mode (i.e., the behavior of the audio DSP in idle mode).
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  sleep_mode: Specifies processor behavior in idle
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_sleep_mode__command(uint8_t icNum,uint8_t pipe, uint8_t sleep_mode, uint16_t length);


typedef struct _SI479XX_HUB_set_sleep_mode__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_set_sleep_mode__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_sleep_mode__parse(uint8_t* buf, SI479XX_HUB_set_sleep_mode__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_sleep_mode__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_sleep_mode__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_sleep_mode(uint8_t icNum, uint8_t pipe, uint8_t  sleep_mode, uint16_t  length, SI479XX_HUB_set_sleep_mode__data*  replyData);


/*
 * Command: SI479XX_HUB_sdk_enable
 * 
 * Description:
 *   On receipt of this command, the audio hub will enable code customer code developed by the SDK.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_sdk_enable__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_sdk_enable__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_sdk_enable__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_sdk_enable__parse(uint8_t* buf, SI479XX_HUB_sdk_enable__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_sdk_enable__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_sdk_enable__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_sdk_enable(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_sdk_enable__data*  replyData);



typedef struct SI479XX_HUB_xbar_connect_node_p2n_cnx__data
{
	uint8_t PORT_ID;
	uint8_t CHAN_ID;
	uint8_t NODE_ID;
	uint8_t XBAR_ID;
	int16_t GAIN_DB;
    uint8_t DECAY_RAMP;
    uint8_t ATTACK_RAMP;
	uint8_t CNX_INFO;
}SI479XX_HUB_xbar_connect_node_p2n_cnx__data;
/*
 * Command: SI479XX_HUB_xbar_connect_node
 * 
 * Description:
 *  Audio hub     On receipt of this command, the audio hub will make the requested connection between an  audio channel on a given port and a crossbar node.  The cross bar connects single-channel  audio streams to (input Xbar) or from (output Xbar) to nodes.  Prior to issuing this  command, the specified port must be configured; however, the port need not be enabled.   Source streams (e.g., input ports) may be connected to multiple nodes or output ports. An output port stream must have no more than one connection.
 *   In the argument list, PORT_ID specifies the port and CHANNEL_ID specifies the channel.  NODE_ID identifies the node to connect to the specified port and channel.   Note that both port and node must be attached to either the input crossbar or the output  crossbar.  Attempts to connect an input port to an output node (or vice versa) will be  rejected.
 *   Optionally, a gain (in dB) may be applied to the connection.  The gain (in dB) is specified  as a signed (2's complement) 16-bit word in S16.8 format.
 *   The sample-rate and clock-domain attributes of the port-node combination are used to determine  if sample-rate conversion is required and whether such conversion will require  rate estimation and asynchronous conversion. To define the sample-rate conversion logic, the  following definitions are helpful:
 *  
 *   fp = the sample-rate of the port channel stream (declared in the port configuration command).
 *   fn = the sample-rate of the node (declared in this command).
 *   Cp = the clock domain of the port channel
 *   Cn = the clock domain of the node
 *  
 *   The formal sample-rate conversion rules are as follows:
 *  
 *   fp = fn  AND  Cp = Cn  :  No conversion is applied
 *   fp â‰  fn  AND  Cp = Cn  :  Synchronous conversion from fp to fn
 *   fp = fn  AND  Cp â‰  Cn  :  Asynchronous conversion from fp to fn
 *   fp â‰  fn  AND  Cp â‰  Cn  :  Asynchronous conversion from fp to fn
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  p2n_cnx: (No Description)
 *  p2n_cnx_length (input): #elems in 'p2n_cnx' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  p2n_cnx: (No Description)
 *  p2n_cnx_LENGTH (input): #elems in 'p2n_cnx' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_node__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_connect_node_p2n_cnx__data* p2n_cnx, uint16_t p2n_cnx_length);


typedef struct _SI479XX_HUB_xbar_connect_node__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	SI479XX_HUB_xbar_connect_node_p2n_cnx__data* P2N_CNX;
	uint16_t P2N_CNX_LENGTH;
}SI479XX_HUB_xbar_connect_node__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_node__parse(uint8_t* buf, SI479XX_HUB_xbar_connect_node__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_node__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_connect_node__data* replyData);

// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_node(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_connect_node_p2n_cnx__data*  p2n_cnx, uint16_t  p2n_cnx_length, SI479XX_HUB_xbar_connect_node__data*  replyData);




typedef struct SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data
{
	uint8_t PORT_ID;
	uint8_t CHAN_ID;
	uint8_t NODE_ID;
	uint8_t XBAR_ID;
}SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data;
/*
 * Command: SI479XX_HUB_xbar_disconnect_node
 * 
 * Description:
 *   On receipt of this command, the audio hub will break the requested connection(s).  Each  connection is specified by listing the port ID, the channel number and the node number. Attempts to break a non-existent connection will result in a non-zero status return,  but otherwise does no harm.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  p2n_dnx: (No Description)
 *  p2n_dnx_length (input): #elems in 'p2n_dnx' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  p2n_dnx: (No Description)
 *  p2n_dnx_LENGTH (input): #elems in 'p2n_dnx' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_node__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data* p2n_dnx, uint16_t p2n_dnx_length);


typedef struct _SI479XX_HUB_xbar_disconnect_node__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data* P2N_DNX;
	uint16_t P2N_DNX_LENGTH;
}SI479XX_HUB_xbar_disconnect_node__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_node__parse(uint8_t* buf, SI479XX_HUB_xbar_disconnect_node__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_node__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_disconnect_node__data* replyData);

// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_node(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_disconnect_node_p2n_dnx__data*  p2n_dnx, uint16_t  p2n_dnx_length, SI479XX_HUB_xbar_disconnect_node__data*  replyData);


/*
 * Command: SI479XX_HUB_xbar_query
 * 
 * Description:
 *   On receipt of this command, the audio hub returns all of the connection descriptors on  the specified cross-bar.  A connection descriptor is a sequence of four bytes that uniquely defines  a cross-bar connection.  To understand the connection descriptor, first, note that there are three types of connections:  
 *  
 *    Port-to-Node   Node-to-Port   Port-to-Port
 *  
 *   Port-to-Node Connection
 *  
 *   A port-to-node connection establishes a connection between an input port and a node on the  input cross-bar. The figure above shows a connection between DI port 0 (PORT_ID = 6), channel 0 to node 5  (NODE_ID = 5) on the input cross-bar. A port-to-node connection is completely described by the (input) PORT_ID, the channel number (CHAN_ID) and the node number (NODE_ID) on the input cross-bar.
 *  
 *   Node-to-Port Connection
 *  
 *   A node-to-port connection establishes a connection between a node on the output cross-bar  and an output port. The figure above shows a connection between node 5 (NODE_ID = 5) to DAC-5 (PORT_ID=0x25).  Since the DAC ports are all mono, the CHAN_ID for this connection is necessarily 0. A  node-to-port connection is completely described by the node number on the output cross-bar  (NODE_ID), the output port number (PORT_ID) and the channel number (CHAN_ID).
 *  
 *   Port-to-Port Connection
 *  
 *   A port-to-port connection establishes a connection bewteen an input port and channel and an output port and channel.  Node that a port-to-port connection experiences no signal processing or conditioning with the execption of sample-rate conversion if the sample rates of the input and output ports differ or if they are in different clock domains. A port-port connection is defined by an input nubmer (PORT_ID) and channel number (CHAN_ID) and an output port number (PORT_ID) and channel number (CHAN_ID).  The figure above llustrates a port-to-port connection from DI-0 (PORT_ID = 0x6) channel 0 to DAC-1 (PORT_ID = 0x21) channel 0.
 *  
 *   Connection descriptors define connections from left-to-right.  For a port-to-node connection,  the descriptor is:
 *  
 *    [Input PORT_ID, Input CHAND_ID, 0, NODE_ID]
 *  
 *   For a node-to-port connetion, the descriptor is:
 *  
 *    [NODE_ID, 0, Output PORT_ID, Output CHAN_ID]
 *  
 *   For a port-to-port connection, the descriptor is:
 *  
 *    [Input PORT_ID, Input CHAN_ID, Output PORT_ID, OUtput CHAN_ID ]
 *  
 *   Descriptors are stored in an unsigned long integer from left to right, from MSB to LSB.  Since messages are transmitted little endian, the descriptors will appear to be backwards as read in  the query response.
 *  
 *   Note that all port-to-node and port-to-port connections reside on the input cross-bar; all  node-to-port connections reside exclusively on the output cross-bar.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  xbar_id: Specifies the input or output cross-bar (xbar)
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  xbar_id: Specifies the input or output cross-bar (xbar)
 *  number_of_connections: Number of active connections in the referenced cross-bar.
 *  cnx: (No Description)
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_query__command(uint8_t icNum,uint8_t pipe, uint8_t xbar_id, uint16_t length);


typedef struct _SI479XX_HUB_xbar_query_cnx__data
{
	uint8_t CONNECTION_BYTE_0;
	uint8_t CONNECTION_BYTE_1;
	uint8_t CONNECTION_BYTE_2;
	uint8_t CONNECTION_BYTE_3;
}SI479XX_HUB_xbar_query_cnx__data;
typedef struct _SI479XX_HUB_xbar_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t XBAR_ID;
	uint8_t NUMBER_OF_CONNECTIONS;
	SI479XX_HUB_xbar_query_cnx__data* CNX;
}SI479XX_HUB_xbar_query__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_query__parse(uint8_t* buf, SI479XX_HUB_xbar_query__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_query(uint8_t icNum, uint8_t pipe, uint8_t  xbar_id, uint16_t  length, SI479XX_HUB_xbar_query__data*  replyData);



typedef struct _SI479XX_HUB_xbar_connect_port_p2p_cnx__data
{
	uint8_t IN_PORT_ID;
	uint8_t IN_CHAN_ID;
	uint8_t OUT_PORT_ID;
	uint8_t OUT_CHAN_ID;
	int16_t GAIN_DB;
    uint8_t DECAY_RAMP;
    uint8_t ATTACK_RAMP;
    uint8_t POLARITY;
	uint8_t CNX_INFO;
}SI479XX_HUB_xbar_connect_port_p2p_cnx__data;
/*
 * Command: SI479XX_HUB_xbar_connect_port
 * 
 * Description:
 *   Directly connect an input port and channel to an output port and channel. The command will accept a single port-to-port connection or a list of port-to-port connections. In the event that a connection fails, the command will abort at the point of failure and return and error status.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  p2p_cnx: (No Description)
 *  p2p_cnx_length (input): #elems in 'p2p_cnx' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  p2p_cnx: (No Description)
 *  p2p_cnx_LENGTH (input): #elems in 'p2p_cnx' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_port__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_connect_port_p2p_cnx__data* p2p_cnx, uint16_t p2p_cnx_length);



typedef struct _SI479XX_HUB_xbar_connect_port__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	SI479XX_HUB_xbar_connect_port_p2p_cnx__data* P2P_CNX;
	uint16_t P2P_CNX_LENGTH;
}SI479XX_HUB_xbar_connect_port__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_port__parse(uint8_t* buf, SI479XX_HUB_xbar_connect_port__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_port__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_connect_port__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_connect_port(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_connect_port_p2p_cnx__data*  p2p_cnx, uint16_t  p2p_cnx_length, SI479XX_HUB_xbar_connect_port__data*  replyData);


typedef struct SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data
{
	uint8_t IN_PORT_ID;
	uint8_t IN_CHAN_ID;
	uint8_t OUT_PORT_ID;
	uint8_t OUT_CHAN_ID;
}SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data;
/*
 * Command: SI479XX_HUB_xbar_disconnect_port
 * 
 * Description:
 *   Disconnect an input port and channel from an output port and channel.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  p2p_dnx: (No Description)
 *  p2p_dnx_length (input): #elems in 'p2p_dnx' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  p2p_dnx: (No Description)
 *  p2p_dnx_LENGTH (input): #elems in 'p2p_dnx' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_port__command(uint8_t icNum,uint8_t pipe, uint16_t length, SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data* p2p_dnx, uint16_t p2p_dnx_length);



typedef struct _SI479XX_HUB_xbar_disconnect_port__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data* P2P_DNX;
	uint16_t P2P_DNX_LENGTH;
}SI479XX_HUB_xbar_disconnect_port__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_port__parse(uint8_t* buf, SI479XX_HUB_xbar_disconnect_port__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_port__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_xbar_disconnect_port__data* replyData);

// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_xbar_disconnect_port(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_xbar_disconnect_port_p2p_dnx__data*  p2p_dnx, uint16_t  p2p_dnx_length, SI479XX_HUB_xbar_disconnect_port__data*  replyData);


/*
 * Command: SI479XX_HUB_set_connection_gain
 * 
 * Description:
 *   Change the input/output gains associated with a specific cross-bar connection.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cnx_id: Specifies the connection type
 *  length: Number of bytes in this command
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 *  gain_db: Gain in S16.7 format
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  cnx_id: Specifies the connection type
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 *  gain_db: Gain in S16.7 format
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_gain__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3, int16_t gain_db);


typedef struct _SI479XX_HUB_set_connection_gain__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t CNX_ID;
	uint8_t CONNECTION_BYTE_0;
	uint8_t CONNECTION_BYTE_1;
	uint8_t CONNECTION_BYTE_2;
	uint8_t CONNECTION_BYTE_3;
	int16_t GAIN_DB;
}SI479XX_HUB_set_connection_gain__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_gain__parse(uint8_t* buf, SI479XX_HUB_set_connection_gain__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_gain__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_connection_gain__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_gain(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, int16_t  gain_db, SI479XX_HUB_set_connection_gain__data*  replyData);


/*
 * Command: SI479XX_HUB_set_connection_ramp_times
 * 
 * Description:
 *   Change the input/output ramp time for gain changes (including mute/unmute).
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cnx_id: Specifies the connection type
 *  length: Number of bytes in this command
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 *  ramp_up_time: Ramp-up time U16.0 format
 *  ramp_dn_time: Ramp-down time U16.0 format
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  cnx_id: Specifies the connection type
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 *  ramp_up_time: Ramp-up time U16.0 format
 *  ramp_dn_time: Ramp-down time U16.0 format
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_ramp_times__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3, uint16_t ramp_up_time, uint16_t ramp_dn_time);


typedef struct _SI479XX_HUB_set_connection_ramp_times__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t CNX_ID;
	uint8_t CONNECTION_BYTE_0;
	uint8_t CONNECTION_BYTE_1;
	uint8_t CONNECTION_BYTE_2;
	uint8_t CONNECTION_BYTE_3;
	uint16_t RAMP_UP_TIME;
	uint16_t RAMP_DN_TIME;
}SI479XX_HUB_set_connection_ramp_times__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_ramp_times__parse(uint8_t* buf, SI479XX_HUB_set_connection_ramp_times__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_ramp_times__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_connection_ramp_times__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_connection_ramp_times(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, uint16_t  ramp_up_time, uint16_t  ramp_dn_time, SI479XX_HUB_set_connection_ramp_times__data*  replyData);


/*
 * Command: SI479XX_HUB_query_connection_gain
 * 
 * Description:
 *   Queries the input/output gains associated with a specific cross-bar connection.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cnx_id: Specifies the connection type
 *  length: Number of bytes in this command
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  cnx_id: Specifies the connection type
 *  gain_state: Specifies the current state of a connection gain
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 *  gain_db: Gain in S16.7 format
 *  fs_err: Sample-Rate Error in S16.2 format
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_gain__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3);


typedef struct _SI479XX_HUB_query_connection_gain__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t CNX_ID;
	uint8_t GAIN_STATE;
	uint8_t CONNECTION_BYTE_0;
	uint8_t CONNECTION_BYTE_1;
	uint8_t CONNECTION_BYTE_2;
	uint8_t CONNECTION_BYTE_3;
	int16_t GAIN_DB;
	int16_t FS_ERR;
}SI479XX_HUB_query_connection_gain__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_gain__parse(uint8_t* buf, SI479XX_HUB_query_connection_gain__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_gain__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_connection_gain__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_gain(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, SI479XX_HUB_query_connection_gain__data*  replyData);


/*
 * Command: SI479XX_HUB_query_connection_ramp
 * 
 * Description:
 *   Queries the ramp type and times (attack and decay) associated with a specific cross-bar connection.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cnx_id: Specifies the connection type
 *  length: Number of bytes in this command
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  cnx_id: Specifies the connection type
 *  polarity: Selects the polarity of the connection (inverted or not inverted)
 *  decay_ramp: Specifies the decay ramp type (log or linear)
 *  attack_ramp: Specifies the attack ramp type (log or linear)
 *  connection_byte_0: Least significant byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID .  Connection Byte 0
 *  connection_byte_1: Second byte of a connection descriptor. As a function of the connection type, can be a  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or 0.  Connection Byte 1
 *  connection_byte_2: Third byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:CHAN_ID  or 0.  Connection Byte 2
 *  connection_byte_3: Most significant byte of a connection descriptor. As a function of the connection type, can be  XBAR_CONNECT_NODE:P2N_CNX:PORT_ID  or a  XBAR_CONNECT_NODE:P2N_CNX:NODE_ID .  Connection Byte 3
 *  ramp_up_time: Ramp-up time U16.0 format
 *  ramp_dn_time: Ramp-down time U16.0 format
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_ramp__command(uint8_t icNum,uint8_t pipe, uint8_t cnx_id, uint16_t length, uint8_t connection_byte_0, uint8_t connection_byte_1, uint8_t connection_byte_2, uint8_t connection_byte_3);


typedef struct _SI479XX_HUB_query_connection_ramp__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t CNX_ID;
	uint8_t POLARITY;
	uint8_t DECAY_RAMP;
	uint8_t ATTACK_RAMP;
	uint8_t CONNECTION_BYTE_0;
	uint8_t CONNECTION_BYTE_1;
	uint8_t CONNECTION_BYTE_2;
	uint8_t CONNECTION_BYTE_3;
	uint16_t RAMP_UP_TIME;
	uint16_t RAMP_DN_TIME;
}SI479XX_HUB_query_connection_ramp__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_ramp__parse(uint8_t* buf, SI479XX_HUB_query_connection_ramp__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_ramp__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_connection_ramp__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_connection_ramp(uint8_t icNum, uint8_t pipe, uint8_t  cnx_id, uint16_t  length, uint8_t  connection_byte_0, uint8_t  connection_byte_1, uint8_t  connection_byte_2, uint8_t  connection_byte_3, SI479XX_HUB_query_connection_ramp__data*  replyData);


/*
 * Command: SI479XX_HUB_node_query
 * 
 * Description:
 *   On receipt of this command, the audio hub will report the current status of the referenced node. 
 *    The response returns connection and configuration data for the referenced node. If a  node is currently idle (disconnected), the port connection and configuration will be omitted  from the response.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  xbar_id: Specifies the input or output cross-bar (xbar)
 *  node_id: Node ID numbers: The number of nodes instantiated in any given system is a function of the topology. The maximum number of nodes on either cross-bar is 32.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  xbar_id: Specifies the input or output cross-bar (xbar)
 *  node_id: Node ID numbers: The number of nodes instantiated in any given system is a function of the topology. The maximum number of nodes on either cross-bar is 32.
 *  cnx: Specifies the state of the â€˜connectednessâ€™ of the node
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_node_query__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t xbar_id, uint8_t node_id);


typedef struct _SI479XX_HUB_node_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t XBAR_ID;
	uint8_t NODE_ID;
	uint8_t CNX;
}SI479XX_HUB_node_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_node_query__parse(uint8_t* buf, SI479XX_HUB_node_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_node_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_node_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_node_query(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  xbar_id, uint8_t  node_id, SI479XX_HUB_node_query__data*  replyData);


/*
 * Command: SI479XX_HUB_port_mute
 * 
 * Description:
 *   On receipt of this command, the audio hub will mute or unmute the referenced port and channel as a function of the MUTE_CTL byte.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  port_id: Port ID numbers
 *  chan_id: Channel ID (number). For I2S and S/P-DIF ports, the Left channel is 0 and the Right channel is 1. For TDM ports, the channels are enumerated from 0 to L-1 where L is the total number of channels
 *  mutetype: Specifies the mute function
 *  mutestate: Specifies the mute state
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  port_id: Port ID numbers
 *  chan_id: Channel ID (number). For I2S and S/P-DIF ports, the Left channel is 0 and the Right channel is 1. For TDM ports, the channels are enumerated from 0 to L-1 where L is the total number of channels
 *  mutetype: Specifies the mute function
 *  mutestate: Specifies the mute state
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_mute__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t port_id, uint8_t chan_id, uint8_t mutetype, uint8_t mutestate);


typedef struct _SI479XX_HUB_port_mute__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t PORT_ID;
	uint8_t CHAN_ID;
	uint8_t MUTETYPE;
	uint8_t MUTESTATE;
}SI479XX_HUB_port_mute__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_mute__parse(uint8_t* buf, SI479XX_HUB_port_mute__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_mute__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_port_mute__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_mute(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  port_id, uint8_t  chan_id, uint8_t  mutetype, uint8_t  mutestate, SI479XX_HUB_port_mute__data*  replyData);


/*
 * Command: SI479XX_HUB_port_query
 * 
 * Description:
 *   On receipt of this command, the audio hub will report the current status of the referenced port. 
 *    The response returns the port state and channel state for the port.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  port_id: Port ID numbers
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  port_id: Port ID numbers
 *  port_state: Specifies the configuration/connection state of a port
 *  chan_state: Bit i describes the mute state of the ith channel, If the ith bit is set to 1, the ith channel is muted.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_query__command(uint8_t icNum,uint8_t pipe, uint8_t port_id, uint16_t length);


typedef struct _SI479XX_HUB_port_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t PORT_ID;
	uint8_t PORT_STATE;
	uint8_t CHAN_STATE;
}SI479XX_HUB_port_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_query__parse(uint8_t* buf, SI479XX_HUB_port_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_port_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_port_query(uint8_t icNum, uint8_t pipe, uint8_t  port_id, uint16_t  length, SI479XX_HUB_port_query__data*  replyData);


/*
 * Command: SI479XX_HUB_clock_domain_sync
 * 
 * Description:
 *   On receipt of this command, the audio hub will synchronize to the specified CLOCK_DOMAIN.   CLOCK_DOMAIN 0 is reserved for the internal audio synth PLL.  Setting the CLOCK_DOMAIN  to 0 effectively ends synchronization to any external clock domain.  The second argument is optional and specifies the preferred PORT_ID to drive the PLL.  If the specified port is active, the tracking loop will use that port to generate the error signal to correct the audio synth to synchronize with the external domain.  If the specified port is not active or if no valid I2S/TDM port is specified, then the system will select a port in the given clock domain to serve as a reference.  If no preferred port is specified, we would recommend setting the portID to 0x3F (Null Port). NB:  The maximum lock range in sync mode is approximately +/- 2000 PPM.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  clock_domain: Clock domain enumeration.    0 is reserved for ports mastered by the 4790.    Other clock domains are defined by the system designer.    Ports in a common domain must be synchronous to each other (i.e., all frame sync signals and bit clocks must be derived from a common reference).
 *  port_id: Port ID numbers
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  clock_domain: Clock domain enumeration.    0 is reserved for ports mastered by the 4790.    Other clock domains are defined by the system designer.    Ports in a common domain must be synchronous to each other (i.e., all frame sync signals and bit clocks must be derived from a common reference).
 *  port_id: Port ID numbers
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_sync__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t clock_domain, uint8_t port_id);


typedef struct _SI479XX_HUB_clock_domain_sync__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t CLOCK_DOMAIN;
	uint8_t PORT_ID;
}SI479XX_HUB_clock_domain_sync__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_sync__parse(uint8_t* buf, SI479XX_HUB_clock_domain_sync__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_sync__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_clock_domain_sync__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_sync(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  clock_domain, uint8_t  port_id, SI479XX_HUB_clock_domain_sync__data*  replyData);


/*
 * Command: SI479XX_HUB_clock_domain_query
 * 
 * Description:
 *   On receipt of this command, the audio hub returns the clock domain currently  selected as a reference.  The second field reports the port that provided the last update to the audio synth PLL, and the final field reports the last error estimate in PPM.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  clock_domain: Clock domain enumeration.    0 is reserved for ports mastered by the 4790.    Other clock domains are defined by the system designer.    Ports in a common domain must be synchronous to each other (i.e., all frame sync signals and bit clocks must be derived from a common reference).
 *  port_id: Port ID numbers
 *  err_ppm: Error in PPM between clock domain 0 and the sync clock domain. This is the last corrected value. The error is reported in S16.4 format.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_query__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_clock_domain_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t CLOCK_DOMAIN;
	uint8_t PORT_ID;
	int16_t ERR_PPM;
}SI479XX_HUB_clock_domain_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_query__parse(uint8_t* buf, SI479XX_HUB_clock_domain_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_clock_domain_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_clock_domain_query(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_clock_domain_query__data*  replyData);


typedef struct SI479XX_HUB_tonegen_request_tone_descriptor__data
{
	uint8_t SUM_OR_MPY;
	uint8_t FALLRAMP;
	uint8_t DECAYRAMP;
	uint8_t ATTACKRAMP;
	uint8_t WAVETYPE;
	uint8_t OSCID;
	uint8_t MOD_INDEX;
	uint8_t AMP_PEAK;
	uint8_t AMP_DECAY;
	uint16_t FREQUENCY;
	uint16_t START_TIME;
	uint16_t ATTACK_TIME;
	uint8_t PEAK_DURATION;
	uint16_t PEAK_TIME;
	uint16_t DECAY_TIME;
	uint16_t FALL_TIME;
}SI479XX_HUB_tonegen_request_tone_descriptor__data;
/*
 * Command: SI479XX_HUB_tonegen_request
 * 
 * Description:
 *   This command requests the generation of a tone/chime sequence.  A tone request consists of one or more tonal epochs each uniquely  defined by a set of parameters encapsulated in a tone descriptor structure.  A tonal epoch has the format shown below:
 *  
 *   Tonal Epoch
 *  
 *   A tonal epoch consists of five phases:
 *  
 *    Start (delay) phase  Attack (ramp-up) phase  Peak (constant amplitude) phase  Decay (relaxation) phase  Fall (ramp-down) phase
 *  
 *   The start phase provides a programmable delay from receipt of the tone request command and the attack phase of the first tone epoch  in the sequence, and can be used to control the time between a subsequent tone event (or epoch).  During the attack phase, the tone ramps up.  During the peak phase the amplitude is held at a constant value and during the decay phase, the amplitude is decreased to some specified target value.  During the ramp-down phase, the tone amplitude is reduced to 0 (or -90 dBFS) and the tonal epoch ends.   A tone request command may contain up to 15 tone descriptors (limited only by the size of the command buffer).  Optionally, a tone  sequence can be repeated a finite number of times or indefinitely.
 *  
 *   The tone generator incorporates four independent oscillators, each programmed as described above.  The outputs of the four oscillators  may be multiplied or summed as shown in the figure below:
 *  
 *   Tone Generator
 *  
 *   Each oscillator can produce a sequence of tone epochs with the following parameters:
 *  
 *    Frequency  Wave type (sine or square)  Start time (in ms)  Attack ramp type (log or linear)  Attack time (in ms)  Relative peak amplitude (% FS)  Peak time (in ms)  Relative decay amplitude (% FS)  Decay ramp type (log or linear)  Decay time (in ms)  Fall ramp type (log or linear)  Fall time (in ms)  Output operation (pair-wise: sum or multiply as shown in the figure)
 *  
 *   The absolute amplitude of the composite generator output can be programmed as well:
 *  
 *    Absolute peak output amplitude (mV)
 *  
 *   Oscillators 0 and 2 serve as the carrier input to the modulator block and have the structure shown below:
 *  
 *   Oscillators 0 and 2
 *  
 *   Oscillators 1 and 3 serve as the modulation input to the modulator block and have the structure shown below:
 *  
 *   Oscillators 1 and 3
 *  
 *   For a sequence of tone epochs, the tone descriptors must be placed in temporal order in the TONEGEN_REQUEST command.
 *  
 *   Specifications (single tone):
 *  
 *    Frequency Range :    20 Hz to 20,000 Hz  Frequency Accuracy:  +/- 1.0 Hz  Amplitude Accuracy:  +/- 0.1 dB  THD+N:               -85 dBFS
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  length: Number of bytes in this command
 *  amp_abs: Peak output amplitude in mV.
 *  num_tone_repeat: The number of times to repeat a tone sequence. A value of 0 indicates no repetitions, that is a single tone sequence will be generated. A value of 1 indicates one repetition (two tone sequences) and so on. A negative value will force the tone generator to repeat the current tone sequence until receipt of a halt (TONEGEN_STOP) command.
 *  tone_descriptor: (No Description)
 *  tone_descriptor_length (input): #elems in 'tone_descriptor' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  tone_queue_count: Number of tone requests in the queue.
 *  tone_queue_depth: Number of bytes currently residing in the tone request queue.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_request__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint16_t amp_abs, int16_t num_tone_repeat, SI479XX_HUB_tonegen_request_tone_descriptor__data* tone_descriptor, uint16_t tone_descriptor_length);


typedef struct _SI479XX_HUB_tonegen_request__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TONE_GEN_ID;
	uint8_t TONE_QUEUE_COUNT;
	uint16_t TONE_QUEUE_DEPTH;
}SI479XX_HUB_tonegen_request__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_request__parse(uint8_t* buf, SI479XX_HUB_tonegen_request__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_request__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_request__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_request(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint16_t  amp_abs, int16_t  num_tone_repeat, SI479XX_HUB_tonegen_request_tone_descriptor__data*  tone_descriptor, uint16_t  tone_descriptor_length, SI479XX_HUB_tonegen_request__data*  replyData);


/*
 * Command: SI479XX_HUB_tonegen_modify
 * 
 * Description:
 *   This command will dynamically alter a specified parameter of an active tone sequence.   Any parameter in any of the tone descriptors can be modified; however, the primary  application of this command is to change the start time of a repetative tone for  back-up alerts.  The new parameter value will take effect on the next iteration of the loop.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  length: Number of bytes in this command
 *  tone_des_id: The individual tonal epochs that constitute a complete tone sequence are numbered from 0 to N. The tone descriptor id is the index (order) of a given tone descriptor within the tone sequence.
 *  tone_prm_id: Tone parameter identifier. This is an index that corresponds to a specific tone parameter.
 *  tone_prm_val: Tone descriptor parameter value
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  tone_des_id: The individual tonal epochs that constitute a complete tone sequence are numbered from 0 to N. The tone descriptor id is the index (order) of a given tone descriptor within the tone sequence.
 *  tone_prm_id: Tone parameter identifier. This is an index that corresponds to a specific tone parameter.
 *  tone_prm_val: Tone descriptor parameter value
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_modify__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint8_t tone_des_id, uint8_t tone_prm_id, uint16_t tone_prm_val);


typedef struct _SI479XX_HUB_tonegen_modify__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TONE_GEN_ID;
	uint8_t TONE_DES_ID;
	uint8_t TONE_PRM_ID;
	uint16_t TONE_PRM_VAL;
}SI479XX_HUB_tonegen_modify__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_modify__parse(uint8_t* buf, SI479XX_HUB_tonegen_modify__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_modify__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_modify__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_modify(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint8_t  tone_des_id, uint8_t  tone_prm_id, uint16_t  tone_prm_val, SI479XX_HUB_tonegen_modify__data*  replyData);


/*
 * Command: SI479XX_HUB_tonegen_stop
 * 
 * Description:
 *   On receipt of this command, the audio hub will halt generation of the active tone sequence for  the specified tone generator.  Optionally, the command may also flush any queued requests.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  length: Number of bytes in this command
 *  abort_option: Specifies how to halt (abort) the active tone sequence:    Asynchronous Hard Abort (halt the current tone request without ramp)    Asynchronous Log Abort (ramp down the active tone request with a log ramp)    Synchronous Abort (halt after current repetition is complete).
 *  queue_option: Specifies queue option: Flush the queue or do not flush the queue.
 *  decay_time: Duration of the abort decay profile in ms.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  abort_option: Specifies how to halt (abort) the active tone sequence:    Asynchronous Hard Abort (halt the current tone request without ramp)    Asynchronous Log Abort (ramp down the active tone request with a log ramp)    Synchronous Abort (halt after current repetition is complete).
 *  queue_option: Specifies queue option: Flush the queue or do not flush the queue.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_stop__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint8_t abort_option, uint8_t queue_option, uint16_t decay_time);


typedef struct _SI479XX_HUB_tonegen_stop__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TONE_GEN_ID;
	uint8_t ABORT_OPTION;
	uint8_t QUEUE_OPTION;
}SI479XX_HUB_tonegen_stop__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_stop__parse(uint8_t* buf, SI479XX_HUB_tonegen_stop__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_stop__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_stop__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_stop(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint8_t  abort_option, uint8_t  queue_option, uint16_t  decay_time, SI479XX_HUB_tonegen_stop__data*  replyData);


/*
 * Command: SI479XX_HUB_tonegen_play
 * 
 * Description:
 *   On receipt of this command, the audio hub will copy the specified tone/chime sequence into the queue of the specified tone generator.  Tone sequences can be stored in available parameter memory space avoiding the necessity of repeating long tone request commands in normal operation.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  length: Number of bytes in this command
 *  tone_seq_id: Each tone/chime sequence stored in memory is assigned a unique handle or identifier. The id or handle is assigned when the sequence is loaded into memory with the TONEGEN_ADD command.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  tone_seq_id: Each tone/chime sequence stored in memory is assigned a unique handle or identifier. The id or handle is assigned when the sequence is loaded into memory with the TONEGEN_ADD command.
 *  tone_queue_depth: Number of bytes currently residing in the tone request queue.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_play__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length, uint8_t tone_seq_id);


typedef struct _SI479XX_HUB_tonegen_play__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TONE_GEN_ID;
	uint8_t TONE_SEQ_ID;
	uint16_t TONE_QUEUE_DEPTH;
}SI479XX_HUB_tonegen_play__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_play__parse(uint8_t* buf, SI479XX_HUB_tonegen_play__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_play__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_play__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_play(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, uint8_t  tone_seq_id, SI479XX_HUB_tonegen_play__data*  replyData);


typedef struct SI479XX_HUB_tonegen_add_tone_descriptor__data
{
	uint8_t SUM_OR_MPY;
	uint8_t FALLRAMP;
	uint8_t DECAYRAMP;
	uint8_t ATTACKRAMP;
	uint8_t WAVETYPE;
	uint8_t OSCID;
	uint8_t MOD_INDEX;
	uint8_t AMP_PEAK;
	uint8_t AMP_DECAY;
	uint16_t FREQUENCY;
	uint16_t START_TIME;
	uint16_t ATTACK_TIME;
	uint8_t PEAK_DURATION;
	uint16_t PEAK_TIME;
	uint16_t DECAY_TIME;
	uint16_t FALL_TIME;
}SI479XX_HUB_tonegen_add_tone_descriptor__data;
/*
 * Command: SI479XX_HUB_tonegen_add
 * 
 * Description:
 *   Creates a new entry in the tone/chime sequence file list and copies the attached tone request  into DSP memory.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  newtoneid: Tone/chime sequence identifier. This is the token used to reference a specific tone sequence stored in DSP memory. The id must be a non-negative integer from 0 to 127. The identifier must be unique with respect to all other tone sequences stored in DSP memory.
 *  length: Number of bytes in this command
 *  amp_abs: Peak output amplitude in mV.
 *  num_tone_repeat: The number of times to repeat a tone sequence. A value of 0 indicates no repetitions, that is a single tone sequence will be generated. A value of 1 indicates one repetition (two tone sequences) and so on. A negative value will force the tone generator to repeat the current tone sequence until receipt of a halt (TONEGEN_STOP) command.
 *  tone_descriptor: (No Description)
 *  tone_descriptor_length (input): #elems in 'tone_descriptor' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  newtoneid: Tone/chime sequence identifier. This is the token used to reference a specific tone sequence stored in DSP memory. The id must be a non-negative integer from 0 to 127. The identifier must be unique with respect to all other tone sequences stored in DSP memory.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_add__command(uint8_t icNum,uint8_t pipe, uint8_t newtoneid, uint16_t length, uint16_t amp_abs, int16_t num_tone_repeat, SI479XX_HUB_tonegen_add_tone_descriptor__data* tone_descriptor, uint16_t tone_descriptor_length);


typedef struct _SI479XX_HUB_tonegen_add__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t NEWTONEID;
}SI479XX_HUB_tonegen_add__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_add__parse(uint8_t* buf, SI479XX_HUB_tonegen_add__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_add__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_add__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_add(uint8_t icNum, uint8_t pipe, uint8_t  newtoneid, uint16_t  length, uint16_t  amp_abs, int16_t  num_tone_repeat, SI479XX_HUB_tonegen_add_tone_descriptor__data*  tone_descriptor, uint16_t  tone_descriptor_length, SI479XX_HUB_tonegen_add__data*  replyData);


/*
 * Command: SI479XX_HUB_tonegen_query
 * 
 * Description:
 *   Returns the state of the specified tone generator.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tone_gen_id: Tone Generator ID (0 or 1).
 *  state: Reports the current state of the specified tone generator.
 *  tone_queue_count: Number of tone requests in the queue.
 *  tone_queue_depth: Number of bytes currently residing in the tone request queue.
 *  act_tone_repeat: The current repeat count of the active tone sequence on the specified tone generator.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query__command(uint8_t icNum,uint8_t pipe, uint8_t tone_gen_id, uint16_t length);


typedef struct _SI479XX_HUB_tonegen_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TONE_GEN_ID;
	uint8_t STATE;
	uint8_t TONE_QUEUE_COUNT;
	uint16_t TONE_QUEUE_DEPTH;
	int16_t ACT_TONE_REPEAT;
}SI479XX_HUB_tonegen_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query__parse(uint8_t* buf, SI479XX_HUB_tonegen_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query(uint8_t icNum, uint8_t pipe, uint8_t  tone_gen_id, uint16_t  length, SI479XX_HUB_tonegen_query__data*  replyData);


/*
 * Command: SI479XX_HUB_tonegen_query_list
 * 
 * Description:
 *   Returns a list of the ids (or handles) of all the tone/chime sequences stored in DSP memory.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tonelist: Each tone/chime sequence stored in memory is assigned a unique handle or identifier. The id or handle is assigned when the sequence is loaded into memory with the TONEGEN_ADD command.
 *  tonelist_LENGTH (input): #elems in 'tonelist' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_list__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_tonegen_query_list__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t* TONELIST;
	uint16_t TONELIST_LENGTH;
}SI479XX_HUB_tonegen_query_list__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_list__parse(uint8_t* buf, SI479XX_HUB_tonegen_query_list__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_list__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_query_list__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_list(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_tonegen_query_list__data*  replyData);


/*
 * Command: SI479XX_HUB_tonegen_query_seq_id
 * 
 * Description:
 *   Returns the tone sequence associated with the specified ID.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  tone_seq_id: Each tone/chime sequence stored in memory is assigned a unique handle or identifier. The id or handle is assigned when the sequence is loaded into memory with the TONEGEN_ADD command.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  tone_seq_id: Each tone/chime sequence stored in memory is assigned a unique handle or identifier. The id or handle is assigned when the sequence is loaded into memory with the TONEGEN_ADD command.
 *  amp_abs: Peak output amplitude in mV.
 *  num_tone_repeat: The number of times to repeat a tone sequence. A value of 0 indicates no repetitions, that is a single tone sequence will be generated. A value of 1 indicates one repetition (two tone sequences) and so on. A negative value will force the tone generator to repeat the current tone sequence until receipt of a halt (TONEGEN_STOP) command.
 *  tone_descriptor: (No Description)
 *  tone_descriptor_LENGTH (input): #elems in 'tone_descriptor' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_seq_id__command(uint8_t icNum,uint8_t pipe, uint8_t tone_seq_id, uint16_t length);


typedef struct _SI479XX_HUB_tonegen_query_seq_id_tone_descriptor__data
{
	uint8_t SUM_OR_MPY;
	uint8_t FALLRAMP;
	uint8_t DECAYRAMP;
	uint8_t ATTACKRAMP;
	uint8_t WAVETYPE;
	uint8_t OSCID;
	uint8_t MOD_INDEX;
	uint8_t AMP_PEAK;
	uint8_t AMP_DECAY;
	uint16_t FREQUENCY;
	uint16_t START_TIME;
	uint16_t ATTACK_TIME;
	uint8_t PEAK_DURATION;
	uint16_t PEAK_TIME;
	uint16_t DECAY_TIME;
	uint16_t FALL_TIME;
}SI479XX_HUB_tonegen_query_seq_id_tone_descriptor__data;
typedef struct _SI479XX_HUB_tonegen_query_seq_id__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t TONE_SEQ_ID;
	uint16_t AMP_ABS;
	int16_t NUM_TONE_REPEAT;
	SI479XX_HUB_tonegen_query_seq_id_tone_descriptor__data* TONE_DESCRIPTOR;
	uint16_t TONE_DESCRIPTOR_LENGTH;
}SI479XX_HUB_tonegen_query_seq_id__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_seq_id__parse(uint8_t* buf, SI479XX_HUB_tonegen_query_seq_id__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_seq_id__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_tonegen_query_seq_id__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_tonegen_query_seq_id(uint8_t icNum, uint8_t pipe, uint8_t  tone_seq_id, uint16_t  length, SI479XX_HUB_tonegen_query_seq_id__data*  replyData);


typedef struct SI479XX_HUB_wave_play_wave__data
{
	int8_t WAVE_ID;
	uint8_t NOISE_LEVEL;
	uint16_t SILENCE_DURATION;
}SI479XX_HUB_wave_play_wave__data;
/*
 * Command: SI479XX_HUB_wave_play
 * 
 * Description:
 *   On receipt of this command, the audio hub will play the requested waveform list.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wave_gen_id: Wave generator ID (0 or 1).
 *  length: Number of bytes in this command
 *  wave_count: Number of waveform elements on the play list.
 *  loop_control: Enables or disables looping through a wave list.
 *  loop_count: The number of times to repeat a wave sequence. A value of 0 indicates no repetitions, that is a single pass through the waveform list. A value of 1 indicates one repetition (two passes through the list) and so on.
 *  wave: (No Description)
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  wave_gen_id: Wave generator ID (0 or 1).
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_play__command(uint8_t icNum,uint8_t pipe, uint8_t wave_gen_id, uint16_t length, uint8_t wave_count, uint8_t loop_control, uint16_t loop_count, SI479XX_HUB_wave_play_wave__data* wave);


typedef struct _SI479XX_HUB_wave_play__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t WAVE_GEN_ID;
}SI479XX_HUB_wave_play__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_play__parse(uint8_t* buf, SI479XX_HUB_wave_play__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_play__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_play__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_play(uint8_t icNum, uint8_t pipe, uint8_t  wave_gen_id, uint16_t  length, uint8_t  wave_count, uint8_t  loop_control, uint16_t  loop_count, SI479XX_HUB_wave_play_wave__data*  wave, SI479XX_HUB_wave_play__data*  replyData);


/*
 * Command: SI479XX_HUB_wave_stop
 * 
 * Description:
 *   On receipt of this command, the audio hub will halt generation of the active waveform for the specified waveform generator.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wave_gen_id: Wave generator ID (0 or 1).
 *  length: Number of bytes in this command
 *  abort_option: Specifies how to halt (abort) the active tone sequence:    Asynchronous Hard Abort (halt the current tone request without ramp)    Asynchronous Log Abort (ramp down the active tone request with a log ramp)    Synchronous Abort (halt after current repetition is complete).
 *  decay_time: Duration of the abort decay profile in ms.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  wave_gen_id: Wave generator ID (0 or 1).
 *  abort_option: Specifies how to halt (abort) the active tone sequence:    Asynchronous Hard Abort (halt the current tone request without ramp)    Asynchronous Log Abort (ramp down the active tone request with a log ramp)    Synchronous Abort (halt after current repetition is complete).
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_stop__command(uint8_t icNum,uint8_t pipe, uint8_t wave_gen_id, uint16_t length, uint8_t abort_option, uint16_t decay_time);


typedef struct _SI479XX_HUB_wave_stop__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t WAVE_GEN_ID;
	uint8_t ABORT_OPTION;
}SI479XX_HUB_wave_stop__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_stop__parse(uint8_t* buf, SI479XX_HUB_wave_stop__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_stop__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_stop__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_stop(uint8_t icNum, uint8_t pipe, uint8_t  wave_gen_id, uint16_t  length, uint8_t  abort_option, uint16_t  decay_time, SI479XX_HUB_wave_stop__data*  replyData);


/*
 * Command: SI479XX_HUB_wave_add
 * 
 * Description:
 *   Creates a new entry in the waveform file list and inserts the first data block into memory. Additional data blocks can be added with the wave_append command.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  newwaveid: Waveform identifier. This is the token used to reference a specific waveform file. The id must be a non-negative integer from 0 to 127. The identifier must be unique regarding all other waveform files stored in DSP memory.
 *  length: Number of bytes in this command
 *  wave_file_header: A file header precedes every waveform file loaded into memory. The size and contents of the header are a function of the file format.
 *  wave_data_header: A file header precedes every waveform data block loaded into memory. The size and contents of the header are a function of the file format.
 *  wave_data: Waveform data block. The size and contents of the header are a function of the file format.
 *  wave_file_header_length (input): #elems in 'wave_file_header' array.
 *  wave_data_header_length (input): #elems in 'wave_data_header' array.
 *  wave_data_length (input): #elems in 'wave_data' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  newwaveid: Waveform identifier. This is the token used to reference a specific waveform file. The id must be a non-negative integer from 0 to 127. The identifier must be unique regarding all other waveform files stored in DSP memory.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_add__command(uint8_t icNum,uint8_t pipe, uint8_t newwaveid, uint16_t length, uint8_t* wave_file_header, uint8_t* wave_data_header, uint8_t* wave_data, uint16_t wave_file_header_length, uint16_t wave_data_header_length, uint16_t wave_data_length);


typedef struct _SI479XX_HUB_wave_add__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t NEWWAVEID;
}SI479XX_HUB_wave_add__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_add__parse(uint8_t* buf, SI479XX_HUB_wave_add__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_add__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_add__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_add(uint8_t icNum, uint8_t pipe, uint8_t  newwaveid, uint16_t  length, uint8_t*  wave_file_header, uint8_t*  wave_data_header, uint8_t*  wave_data, uint16_t  wave_file_header_length, uint16_t  wave_data_header_length, uint16_t  wave_data_length, SI479XX_HUB_wave_add__data*  replyData);


/*
 * Command: SI479XX_HUB_wave_append
 * 
 * Description:
 *   Append a data block to the end of a waveform file registered in DSP RAM.  This command is necessary when loading waveform files via the API since the maximum message length supported by the API is much shorter than most waveform files.  Waveforms are loaded with the wavgen_add command to attach the first data block and then as many wavegen_append commmands as necessary to load the complete file.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  newwaveid: Waveform identifier. This is the token used to reference a specific waveform file. The id must be a non-negative integer from 0 to 127. The identifier must be unique regarding all other waveform files stored in DSP memory.
 *  length: Number of bytes in this command
 *  wave_data_header: A file header precedes every waveform data block loaded into memory. The size and contents of the header are a function of the file format.
 *  wave_data: Waveform data block. The size and contents of the header are a function of the file format.
 *  wave_data_header_length (input): #elems in 'wave_data_header' array.
 *  wave_data_length (input): #elems in 'wave_data' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  newwaveid: Waveform identifier. This is the token used to reference a specific waveform file. The id must be a non-negative integer from 0 to 127. The identifier must be unique regarding all other waveform files stored in DSP memory.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_append__command(uint8_t icNum,uint8_t pipe, uint8_t newwaveid, uint16_t length, uint8_t* wave_data_header, uint8_t* wave_data, uint16_t wave_data_header_length, uint16_t wave_data_length);


typedef struct _SI479XX_HUB_wave_append__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t NEWWAVEID;
}SI479XX_HUB_wave_append__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_append__parse(uint8_t* buf, SI479XX_HUB_wave_append__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_append__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_append__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_append(uint8_t icNum, uint8_t pipe, uint8_t  newwaveid, uint16_t  length, uint8_t*  wave_data_header, uint8_t*  wave_data, uint16_t  wave_data_header_length, uint16_t  wave_data_length, SI479XX_HUB_wave_append__data*  replyData);


/*
 * Command: SI479XX_HUB_wave_gen_query
 * 
 * Description:
 *   Returns the status of the referenced wave generator.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  wave_gen_id: Wave generator ID (0 or 1).
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  wave_gen_id: Wave generator ID (0 or 1).
 *  wave_gen_state: Wave generator state.
 *  wave_gen_loopcount: Wave generator loop count.
 *  wave_gen_play_list_index: Wave generator play list index.
 *  wave_gen_play_list_length: Wave generator play list length.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_gen_query__command(uint8_t icNum,uint8_t pipe, uint8_t wave_gen_id, uint16_t length);


typedef struct _SI479XX_HUB_wave_gen_query__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t WAVE_GEN_ID;
	uint8_t WAVE_GEN_STATE;
	uint16_t WAVE_GEN_LOOPCOUNT;
	uint8_t WAVE_GEN_PLAY_LIST_INDEX;
	uint8_t WAVE_GEN_PLAY_LIST_LENGTH;
}SI479XX_HUB_wave_gen_query__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_gen_query__parse(uint8_t* buf, SI479XX_HUB_wave_gen_query__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_gen_query__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_gen_query__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_gen_query(uint8_t icNum, uint8_t pipe, uint8_t  wave_gen_id, uint16_t  length, SI479XX_HUB_wave_gen_query__data*  replyData);


/*
 * Command: SI479XX_HUB_wave_query_list
 * 
 * Description:
 *   Returns a list of the waveform files stored in DSP memory.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  wavelist: Waveform identifier. Positive values (0 to 127) refer to waveforms stored in data memory. An ID of -1 generates a interval of silence.
 *  wavelist_LENGTH (input): #elems in 'wavelist' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_query_list__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_wave_query_list__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	int8_t* WAVELIST;
	uint16_t WAVELIST_LENGTH;
}SI479XX_HUB_wave_query_list__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_query_list__parse(uint8_t* buf, SI479XX_HUB_wave_query_list__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_query_list__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_wave_query_list__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_wave_query_list(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_wave_query_list__data*  replyData);


/*
 * Command: SI479XX_HUB_parameter_write
 * 
 * Description:
 *   On receipt of this command, the audio hub will write the contents of the attached data array to the audio hub parameter specified by the index.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  parmid: Audio hub parameter index
 *  length: Number of bytes in this command
 *  param: Parameter values. Refer to  PARAMETER_WRITE:PRMID  for a description of each parameter including size, format and units.
 *  param_length (input): #elems in 'param' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  parmid: Audio hub parameter index
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_write__command(uint8_t icNum,uint8_t pipe, uint8_t parmid, uint16_t length, uint8_t* param, uint16_t param_length);


typedef struct _SI479XX_HUB_parameter_write__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t PARMID;
}SI479XX_HUB_parameter_write__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_write__parse(uint8_t* buf, SI479XX_HUB_parameter_write__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_parameter_write__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_write(uint8_t icNum, uint8_t pipe, uint8_t  parmid, uint16_t  length, uint8_t*  param, uint16_t  param_length, SI479XX_HUB_parameter_write__data*  replyData);


/*
 * Command: SI479XX_HUB_parameter_read
 * 
 * Description:
 *   On receipt of this command, the audio hub will return the specified array of parameter  values for the specified index.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  parmid: Audio hub parameter index
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  parmid: Audio hub parameter index
 *  param: Parameter values. Refer to  PARAMETER_WRITE:PRMID  for a description of each parameter including size, format and units.
 *  param_LENGTH (input): #elems in 'param' array.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_read__command(uint8_t icNum,uint8_t pipe, uint8_t parmid, uint16_t length);


typedef struct _SI479XX_HUB_parameter_read__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t PARMID;
	uint8_t* PARAM;
	uint16_t PARAM_LENGTH;
}SI479XX_HUB_parameter_read__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_read__parse(uint8_t* buf, SI479XX_HUB_parameter_read__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_read__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_parameter_read__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_parameter_read(uint8_t icNum, uint8_t pipe, uint8_t  parmid, uint16_t  length, SI479XX_HUB_parameter_read__data*  replyData);


/*
 * Command: SI479XX_HUB_coeff_write
 * 
 * Description:
 *   On receipt of this command, the audio hub overwrites the specified ADC or DAC filter coefficient array.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  filterid: ADC/DAC Filter Index
 *  ntaps: Number of filter taps
 *  coeff: The filter coefficients are stored in S32.31 format (signed with 31 fractional bits).
 *  coeff_length (input): #elems in 'coeff' array.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  filterid: ADC/DAC Filter Index
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_coeff_write__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint8_t filterid, uint8_t ntaps, int32_t* coeff, uint16_t coeff_length);


typedef struct _SI479XX_HUB_coeff_write__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint8_t FILTERID;
}SI479XX_HUB_coeff_write__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_coeff_write__parse(uint8_t* buf, SI479XX_HUB_coeff_write__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_coeff_write__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_coeff_write__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_coeff_write(uint8_t icNum, uint8_t pipe, uint16_t  length, uint8_t  filterid, uint8_t  ntaps, int32_t*  coeff, uint16_t  coeff_length, SI479XX_HUB_coeff_write__data*  replyData);


/*
 * Command: SI479XX_HUB_set_dac_offset
 * 
 * Description:
 *   This command sets the value of a DC offset injected before the CIC filters in the DACs. The value is applied to all of the DACs.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  dac_offset: DAC offset: The value is 24-bit left justified in a 32-bit signed integer.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_dac_offset__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint32_t dac_offset);


typedef struct _SI479XX_HUB_set_dac_offset__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_set_dac_offset__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_dac_offset__parse(uint8_t* buf, SI479XX_HUB_set_dac_offset__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_dac_offset__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_dac_offset__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_dac_offset(uint8_t icNum, uint8_t pipe, uint16_t  length, uint32_t  dac_offset, SI479XX_HUB_set_dac_offset__data*  replyData);


/*
 * Command: SI479XX_HUB_query_dac_offset
 * 
 * Description:
 *   This command returns the value of DC offset injected before the CIC filters in the DACs. The value is applied to all of the DACs.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  dac_offset: DAC offset: The value is 24-bit left justified in a 32-bit signed integer.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_dac_offset__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_query_dac_offset__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint32_t DAC_OFFSET;
}SI479XX_HUB_query_dac_offset__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_dac_offset__parse(uint8_t* buf, SI479XX_HUB_query_dac_offset__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_dac_offset__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_dac_offset__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_dac_offset(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_query_dac_offset__data*  replyData);


/*
 * Command: SI479XX_HUB_set_adc_dc_notch
 * 
 * Description:
 *   This command sets the pole location of the DC notch filter in the ADC channels.  The DC notch filter consists of a first-order filter with a zero at z=1 and a pole at z=1-alpha where alpha is the parameter set by this command.  To disable the DC notch filter, set the value of alpha to 0.  A typical value of alpha to remove DC is 0x00080000.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  dc_alpha: DC notch filter parameter: The value is 24-bit left justified in a 32-bit signed integer. The pole location of the filter is z = (1 - dc_alpha).
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_adc_dc_notch__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint32_t dc_alpha);


typedef struct _SI479XX_HUB_set_adc_dc_notch__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
}SI479XX_HUB_set_adc_dc_notch__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_adc_dc_notch__parse(uint8_t* buf, SI479XX_HUB_set_adc_dc_notch__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_adc_dc_notch__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_set_adc_dc_notch__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_set_adc_dc_notch(uint8_t icNum, uint8_t pipe, uint16_t  length, uint32_t  dc_alpha, SI479XX_HUB_set_adc_dc_notch__data*  replyData);


/*
 * Command: SI479XX_HUB_query_topology_memory
 * 
 * Description:
 *   This command reports the size (in bytes) of the topology and parameter sections in DSP memory.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  top_size: Size of the topology memory section in bytes.
 *  prm_size: Size in bytes to read/write to/from the parameter section of DSP memory.
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_topology_memory__command(uint8_t icNum,uint8_t pipe, uint16_t length);


typedef struct _SI479XX_HUB_query_topology_memory__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint16_t TOP_SIZE;
	uint16_t PRM_SIZE;
}SI479XX_HUB_query_topology_memory__data;
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_topology_memory__parse(uint8_t* buf, SI479XX_HUB_query_topology_memory__data* replyData);


R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_topology_memory__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_query_topology_memory__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_query_topology_memory(uint8_t icNum, uint8_t pipe, uint16_t  length, SI479XX_HUB_query_topology_memory__data*  replyData);


/*
 * Command: SI479XX_HUB_read_topology_memory
 * 
 * Description:
 *   On receipt of this command, the audio hub will return the contents of the parameter section of the topology space.  The read commences at the specified offset relative to the base address.
 * 
 * Arguments:
 *  icNum: Bitfield selector of which chips to send command to.
 *  length: Number of bytes in this command
 *  prm_offset: Offset in bytes from the base address of the parameter section from which to commence a read or write.
 *  prm_size: Size in bytes to read/write to/from the parameter section of DSP memory.
 * 
 * Replies:
 *  icNum: Bitfield selector of which chips to send command to.
 *  cmd_num: All response messages echo the command number so that the response can be matched with the respective command--more particulary when several commands are issued simultaneously. Note that commands are processed in the order received and hence, should a specific command be issued more than once in the same command packet (batch of commands) the responses can be associated unambiguously with their respective commands by their order in the response packet.
 *  status: Command success/feedback
 *  length: Number of bytes in this response
 *  prm_offset: Offset in bytes from the base address of the parameter section from which to commence a read or write.
 *  prm_size: Size in bytes to read/write to/from the parameter section of DSP memory.
 *  data: Parameter values expressed as a sequence of bytes
 * 
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_read_topology_memory__command(uint8_t icNum,uint8_t pipe, uint16_t length, uint16_t prm_offset, uint16_t prm_size);


typedef struct _SI479XX_HUB_read_topology_memory__data
{
	uint8_t CMD_NUM;
	int8_t STATUS;
	uint16_t LENGTH;
	uint16_t PRM_OFFSET;
	uint16_t PRM_SIZE;
	uint8_t* DATA;
}SI479XX_HUB_read_topology_memory__data;
/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */
/* NOT FOR GENERAL USE. 
 * You probably want the __reply() function, 
 * or 'readHifiReply()' (ref_audio_core.h/c) which is a generic HIFI_RESPx wrapper.
 */
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_read_topology_memory__parse(uint8_t* buf, SI479XX_HUB_read_topology_memory__data* replyData);

/* !
 * ! PLEASE NOTE: This function dynamically allocates memory and it is the developer's responsibility to free it.
 * !
 */

R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_read_topology_memory__reply(uint8_t icNum, uint8_t pipe, SI479XX_HUB_read_topology_memory__data* replyData);


// Refer to __command + __reply documentation.
R_4_1_SI479XX_HUB_API_EXT RETURN_CODE SI479XX_HUB_read_topology_memory(uint8_t icNum, uint8_t pipe, uint16_t  length, uint16_t  prm_offset, uint16_t  prm_size, SI479XX_HUB_read_topology_memory__data*  replyData);


#ifdef __cplusplus
} // extern C
#endif

#endif
