/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef COMMON_TUNER_H_
#define COMMON_TUNER_H_

#ifdef _r_4_1_common_tuner_h_global_		/* 210416 SGsoft */
#define R_4_1_COMM_TUNER_EXT
#else
#define R_4_1_COMM_TUNER_EXT	extern
#endif	/*_r_4_1_common_tuner_h_global_*/

#if 0
#include "platform/System_Configuration.h"
#include "common_types.h"
#include "feature_types.h"

#include "data-handlers/FMHD_Alert_Handler.h"
#include "data-handlers/FMHD_PSD_Handler.h"
#include "data-handlers/FMHD_SIS_Handler.h"
#include "data-handlers/FMHD_Service_List_Handler.h"
#include "data-handlers/RDS_Handler.h"

#include "apis/si4796x_firmware_api.h"
#include "apis/si4796x_tuner_api.h"
#include "apis/si479xx_firmware_api.h"
#endif	/*0*/

// Forward decls for functions.
typedef struct fm_system_data fm_system_data;
typedef struct am_system_data am_system_data;

// Convenience wrapper for dual eagle frontend antennae selection.
typedef enum ANTENNA {
    ANT_A,
    ANT_B,
} ANTENNA;


R_4_1_COMM_TUNER_EXT RETURN_CODE si479xx_enable_ctsint(uint8_t ic);
//
// Initialization / Setup focused
//

// Only modifies DE prop FRONTED_TUNERx_ANTENNA_SELECT
R_4_1_COMM_TUNER_EXT RETURN_CODE set_tuner_antennae(dc_config_chip_select ic, TUNER_SUBCMD_ID id, ANTENNA fm, ANTENNA b3);

// 479xx TUNER property get/set
R_4_1_COMM_TUNER_EXT RETURN_CODE set_tuner_prop(tuner_id tuner, uint16_t addr, uint16_t data);
R_4_1_COMM_TUNER_EXT RETURN_CODE get_tuner_prop(tuner_id tuner, uint16_t addr, uint16_t* value);

// Will return TIMEOUT if not successful.
R_4_1_COMM_TUNER_EXT RETURN_CODE set_mode(tuner_id tuner, uint8_t mode);
R_4_1_COMM_TUNER_EXT RETURN_CODE set_rds_properties(tuner_id tuner);


// Sets fixed IQ settings for both FM/AM modes.
#if 0		/* 210505 SGsoft */	
R_4_1_COMM_TUNER_EXT RETURN_CODE set_hd_iq(dc_config_chip_select ic, uint8_t slotsize_enum, uint8_t rate_enum);
// Set mode dependent IQ settings.
R_4_1_COMM_TUNER_EXT RETURN_CODE set_dab_drm_iq(dc_config_chip_select ic);
#endif	/*0*/

// Only use with global eagles to avoid API errors.
R_4_1_COMM_TUNER_EXT RETURN_CODE set_zif_port(dc_config_chip_select ic, uint8_t enable, uint8_t src);

// Queries tuner's property values to read the current iq slot size (bits) from the part.
// May depend on tuner mode.
R_4_1_COMM_TUNER_EXT RETURN_CODE get_iqslot(tuner_id tuner, uint8_t* iqslot);

R_4_1_COMM_TUNER_EXT RETURN_CODE fm_chips_init(const fm_chips* c, RADIO_MODE_TYPE boot_region);

R_4_1_COMM_TUNER_EXT RETURN_CODE update_fm_metrics(tuner_id tuner, fm_metrics* metrics, bool attune, bool cancel, bool stcack);
#if 0		/* 210505 SGsoft */	
R_4_1_COMM_TUNER_EXT RETURN_CODE update_hd_mrc_metrics(tuner_id tuner, hd_mrc_metrics* metrics);
#endif	/*0*/
R_4_1_COMM_TUNER_EXT RETURN_CODE update_fm_div_metrics(tuner_id tuner, fm_diversity_metrics* pd_metrics);
R_4_1_COMM_TUNER_EXT RETURN_CODE update_am_metrics(tuner_id tuner, am_metrics* metrics, uint8_t attune, uint8_t cancel, uint8_t stcack);

//
// Receiver focused
//

R_4_1_COMM_TUNER_EXT SI479XX_read_status__data GetSi479XXStatus(uint8_t icNum);
R_4_1_COMM_TUNER_EXT SI4796X_read_status__data GetSi4796XStatus(uint8_t icNum);

#define STC_DEFAULT_TIMEOUT_MS 6000/*5000*//*1000*/					/* 210429 SGsoft *//* 210504 SGsoft */	
// Waits for appropriate STC bit, or timeout.
// returns TIMEOUT if timeout_ms is first.
R_4_1_COMM_TUNER_EXT RETURN_CODE wait_for_stc(tuner_id tuner, uint32_t timeout_ms);

// Checks current state of appropriate STC bit.
R_4_1_COMM_TUNER_EXT bool is_seek_tune_complete(tuner_id tuner);

#if 0		/* 210505 SGsoft */	
// Does an FM tune to freq(10khz units) and resets associated global data.
// If phase_div_enabled, it will do phase diversity combining, and tune both tuners.
// If hd_enabled, it will try to start HD service too.
// If hd_div_enabled, it will also do HD MRC combining steps.
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_tune(const fm_chips* chips, fm_system_data* fm, uint16_t freq_10khz);

// Tunes and waits for STC
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_tune_single(tuner_id fm_tuner, uint16_t freq_10khz);

// Polls for valid RDS PI code.
// Return codes:
//  o SUCCESS : PI code found
//  o TIMEOUT : PI code not found within timeout_ms
#if 0		/* 210505 SGsoft */	
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_rds_pi_poll(tuner_id tuner, uint16_t timeout_ms, SI4796X_TUNER_fm_rds_status__data* rds);
R_4_1_COMM_TUNER_EXT RETURN_CODE update_fm_rds(tuner_id tuner, rds_decoder* rds);
#endif	/*0*/

R_4_1_COMM_TUNER_EXT RETURN_CODE fm_seek(const fm_chips* chips, fm_system_data* fm, bool seekup, bool wrap, bool hdseek_only, bool* changed_freq);
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_tune_step(fm_chips* chips, fm_system_data* fm, bool stepup);

// Returns a normalized station 'quality' metric from some RSQ status fields.
// Output : 0 (bad) to 100 (good)
uint8_t fm_calc_quality(SI4796X_TUNER_fm_rsq_status__data* rsq);

R_4_1_COMM_TUNER_EXT RETURN_CODE fm_update_quality(tuner_id tuner, uint8_t* quality, fm_metrics* m);

typedef struct fm_scan_result {
    uint16_t freq_10khz;
    // see fm_calc_quality()
    uint8_t quality;

    // PI code information for service following.
    bool pi_valid;
    uint16_t pi_code;
} fm_scan_result;

// Callback for handling new frequencies.
// Return value:
//  o true: stop scan
//  o false: continue scan
typedef bool (*cb_fm_scan_result)(fm_scan_result);

// Perform a full band scan, and return some metrics.
// Caller must allocate array of frequencies. Will stop immediately once max reached.
// Args :
//   o pi_only : if true, will only report stations with valid PI codes.
//   o band_bottom : starting frequency (10khz), scans up to band top property.
//   o freq_count : #elements in caller allocated results
//   o results : caller allocated array of results.
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_band_scan(tuner_id tuner, bool pi_only, uint16_t band_bottom, uint8_t freq_count, fm_scan_result* results, uint8_t* freq_added, cb_fm_scan_result cb_new_freq);
// helper function
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_band_scan_check(tuner_id tuner, bool pi_only, fm_scan_result* result, bool* stop, bool* added, cb_fm_scan_result cb_freq);

typedef enum FMPD_AUDIO {
    // aka CHIP0 or TUNER0
    FMPD_PRIMARY = 0,
    // aka CHIP1 or TUNER1
    FMPD_DIVERSITY = 1,
} FMPD_AUDIO;

// Wrapper for phase diversity command, with only common arguments.
R_4_1_COMM_TUNER_EXT RETURN_CODE fm_phase_diversity(dc_config_chip_select ic, FMPD_AUDIO audio_source, bool combine);

// Does an AM tune to freq(1khz units) and resets associated global data.
// If hd_demod is not NULL, then it will reacquire on demod as well.

R_4_1_COMM_TUNER_EXT RETURN_CODE am_tune(tuner_id tuner, const demod_id* hd_demod, uint16_t freq, bool drm_tune, int16_t drm_offset, am_metrics* metrics);
R_4_1_COMM_TUNER_EXT RETURN_CODE am_seek(tuner_id tuner, const demod_id* hd_demod, am_system_data* am, bool seekup, bool wrap, bool digital_only, bool drm, bool* changed_freq);

R_4_1_COMM_TUNER_EXT RETURN_CODE get_fm_tuned_freq(tuner_id tuner, uint16_t* current_freq_10khz);
R_4_1_COMM_TUNER_EXT RETURN_CODE get_am_tuned_freq(tuner_id tuner, uint16_t* current_freq);

R_4_1_COMM_TUNER_EXT RETURN_CODE fm_confirm_seek_complete(tuner_id tuner, bool* seek_continue);
R_4_1_COMM_TUNER_EXT RETURN_CODE am_confirm_seek_complete(tuner_id tuner, bool* seek_continue);

// Modify the global band limit variables.
R_4_1_COMM_TUNER_EXT RETURN_CODE set_band_limit_properties(tuner_id tuner, BAND_LIMITS limits);
R_4_1_COMM_TUNER_EXT RETURN_CODE update_band_limits(tuner_id tuner, RADIO_MODE_TYPE band_type, uint16_t* band_top, uint16_t* band_bottom);
#endif	/*0*/

//
// System Data Structs
//

#if 0		/* 210505 SGsoft */	
typedef struct hd_system_data {
    // state information
    uint8_t audio_svc_bitmask;
    uint8_t selection;
    uint32_t audio_sid;
    uint32_t audio_cid;

    // decoders
    hd_alert_decoder alert;
    psd_decoder psd;
    sis_decoder sis;
    hdsl_decoder svc_list;
} hd_system_data;
#endif	/*0*/

typedef struct fmam_band {
    uint16_t band_bottom;
    uint16_t band_top;
    uint16_t band_step;
} fmam_band;

struct fm_system_data {
    fmam_band band;

    // for cancelling seeks
    bool continue_seek;

    // metrics about current station
    fm_metrics metrics;
    fm_diversity_metrics pd_metrics;
	#if 0		/* 210505 SGsoft */	
    hd_metrics hd_metrics;
    hd_mrc_metrics mrc_metrics;

    // digital data decoders
    rds_decoder rds;
	#endif	/*0*/
};

struct am_system_data {
    fmam_band band;

    // for cancelling seeks
    bool continue_seek;

    // metrics
    am_metrics metrics;
	#if 0		/* 210505 SGsoft */	
    hd_metrics hd_metrics;
	#endif	/*0*/

    // digital data decoders
};

#endif //COMMON_TUNER_H_
