/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef FIRMWARE_API_COMMON_H
#define FIRMWARE_API_COMMON_H

#ifdef _r_4_1_firmware_api_common_h_global_		/* 210416 SGsoft */
#define R_4_1_FIRM_API_COMM_EXT
#else
#define R_4_1_FIRM_API_COMM_EXT	extern
#endif	/*_r_4_1_firmware_api_common_h_global_*/

#if 0
#include "apis/si4796x_tuner_api.h"
#include "common/DAB_common.h"
#include "common/global_data.h"
#include "common_types.h"
#endif	/*0*/

#if 0		/* 210504 SGsoft */	
R_4_1_FIRM_API_COMM_EXT bool is_fm_mono(tuner_id tuner);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE force_mono_fm(tuner_id tuner, AUDIO_MONO_STEREO_SWITCH audio_MonStereo);

// Some 'common' implementations of firmware_manager_pimpls functions

R_4_1_FIRM_API_COMM_EXT RETURN_CODE SetBandLimits_common(const fm_chips* chips, BAND_LIMITS limits);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE SetStepSize__common(const fm_chips* chips, uint8_t stepsize_khz);

R_4_1_FIRM_API_COMM_EXT RETURN_CODE Tune__common(const fm_chips* chips, uint32_t freq_khz);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE SeekStart__common(const fm_chips* fmam_chips, bool seekup, bool wrap, bool hdseekonly);
#endif	/*0*/

#if 0		/* 210504 SGsoft */	
R_4_1_FIRM_API_COMM_EXT RETURN_CODE UpdateMetrics__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, dab_system_data* dab);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE UpdateAudioMetrics(tuner_id tuner);

R_4_1_FIRM_API_COMM_EXT RETURN_CODE UpdateServiceList__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, dab_system_data* dab, hd_system_data* hd);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE UpdateDataServiceData__common(RADIO_MODE_TYPE mode, const fm_chips* fmam_chips, rds_decoder* rds, const dab_chips* dab_chips, hd_system_data* hd, dab_system_data* dab);

R_4_1_FIRM_API_COMM_EXT RETURN_CODE StartProcessingChannel__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, GENERAL_SERVICE_TYPE type, uint32_t service_id, uint32_t component_id, uint8_t* service_buffer, dab_system_data* dab, hd_system_data* hd);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE StopProcessingChannel__common(const fm_chips* fmam_chips, const dab_chips* dab_chips, GENERAL_SERVICE_TYPE type, uint32_t service_id, uint32_t component_id, hd_system_data* hd, dab_system_data* dab);

R_4_1_FIRM_API_COMM_EXT RETURN_CODE UpdateRadioDNS_FMRDS_common(radioDNS_fm* elements);
#endif	/*0*/

// Other common functions for both hd/dab modes.

// 479xx, 468x, 4690 property get/set
R_4_1_FIRM_API_COMM_EXT RETURN_CODE set_prop(uint8_t icNum, uint16_t prop_id, uint16_t value);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE get_prop(uint8_t icNum, uint16_t prop_id, uint16_t* value);

#if 0		/* 210504 SGsoft */	
R_4_1_FIRM_API_COMM_EXT RETURN_CODE set_rds_properties(tuner_id tuner);

R_4_1_FIRM_API_COMM_EXT RETURN_CODE set_am_tune_defaults(tuner_id tuner);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE set_fm_tune_defaults(tuner_id tuner);
R_4_1_FIRM_API_COMM_EXT RETURN_CODE set_fm_aftune_defaults(tuner_id tuner);
#endif	/*0*/

#endif //FIRMWARE_API_COMMON_H
