/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef FIRMWARE_API_DATA_H
#define FIRMWARE_API_DATA_H

#ifdef _r_4_1_global_data_h_global_		/* 210416 SGsoft */
#define R_4_1_GLOBAL_DATA_EXT
#else
#define R_4_1_GLOBAL_DATA_EXT	extern
#endif	/*_r_4_1_global_data_h_global_*/

#if 0
#include "platform/System_Configuration.h"

#include "common/DAB_common.h"
#include "common/common_tuner.h"
#include "follow/service_following.h"
#include "follow/sf_cache.h"
#include "linker/linker.h"

#include "apis/si46xx_dab_firmware_api.h"
#include "apis/si4796x_firmware_api.h"
#include "apis/si479xx_firmware_api.h"
#endif	/*0*/


typedef struct dab_demod_metrics dab_demod_metrics;

#ifdef _r_4_1_global_data_h_global_		/* 210416 SGsoft */
///const char* fstate_dab_service_following_cache = ADD_RESOURCE_DIR("state/dab/saved-sfc.txt");			/* 210510 SGsoft */	
///const char* fstate_dab_service_list_file = ADD_RESOURCE_DIR("state/dab/dab_service_list.bin");

// GLOBAL Variables
system_config g_evb_system_cfg = { 0 };

hd_mode_chips g_hd_parts = { 0 };
dab_mode_chips g_dab_parts = { 0 };
dab_chips g_dab_cfg = { 0 };
fm_chips g_fm_cfg = { 0 };

board_metrics g_board_metrics = { 0 };


#if 0		/* 210504 SGsoft */	
#ifdef OPTION_HANDLE_ADVANCED_SERVICES
ServiceList* g_service_list = NULL;
#endif

RADIO_MODE_TYPE g_radio_mode = RADIO_MODE_UNKNOWN;

// currently only single RDS decoder
hd_system_data g_hd_data = { 0 };

// when loading DAB service list, the previously played service index.
uint8_t g_dab_prior_svc_index = -1;

linker_context* g_linker = NULL;
// shared state between multiple DAB receivers
///sf_context g_sf_ctx;									/* 210426 SGsoft */					
sfc_handle* g_dab_sfc = NULL;
dab_service_list g_dab_svc_list = { 0 };

dab_system_data g_dab_states[2] = { 0 };
dab_system_data* g_dab_state = NULL;
fm_system_data g_fm_state = { 0 };
am_system_data g_am_state = { 0 };
#endif	/*0*/

#else
// Some common paths
///extern const char* fstate_dab_service_following_cache;													/* 210510 SGsoft */	
///extern const char* fstate_dab_service_list_file;

// GLOBAL Variables
extern system_config g_evb_system_cfg;

// All parts for the boot mode
extern hd_mode_chips g_hd_parts;
extern dab_mode_chips g_dab_parts;
// These are short-hand for currently running system
// DAB operations
extern dab_chips g_dab_cfg;
// FM/AM operations
extern fm_chips g_fm_cfg;

extern board_metrics g_board_metrics;

#if 0		/* 210504 SGsoft */	
#ifdef OPTION_HANDLE_ADVANCED_SERVICES
extern ServiceList* g_service_list;
#endif

extern RADIO_MODE_TYPE g_radio_mode;

// This will be overwritten to force mono
extern uint16_t g_fm_blend_max;

//metrics

extern fm_system_data g_fm_state;
extern am_system_data g_am_state;

extern uint8_t g_dab_prior_svc_index;

// assumes single linker chip
extern linker_context* g_linker;

// shared state between multiple DAB receivers
///extern sf_context g_sf_ctx;								/* 210426 SGsoft */								
extern sfc_handle* g_dab_sfc;
extern dab_service_list g_dab_svc_list;

// Data for single receiver systems
extern hd_system_data g_hd_data;
extern dab_system_data g_dab_states[2];
extern dab_system_data* g_dab_state;
#endif	/*0*/

#endif	/*_r_4_1_global_data_h_global_*/

#if 0		/* 210504 SGsoft */	
//Common functions that operate on global data
R_4_1_GLOBAL_DATA_EXT void empty_fm_system_metrics(fm_system_data*);
R_4_1_GLOBAL_DATA_EXT void empty_am_system_metrics(am_system_data*);
R_4_1_GLOBAL_DATA_EXT void empty_dab_system_metrics(dab_system_data*);

R_4_1_GLOBAL_DATA_EXT void empty_fm_metrics(fm_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_fm_diversity_metrics(fm_diversity_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_am_metrics(am_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_hd_metrics(hd_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_hd_mrc_metrics(hd_mrc_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_hd_metrics(hd_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_dab_metrics(dab_metrics*);
R_4_1_GLOBAL_DATA_EXT void empty_dab_mrc_metrics(dab_mrc_metrics* metrics);

R_4_1_GLOBAL_DATA_EXT bool is_radio_mode_fmam(RADIO_MODE_TYPE);

// Use once initially to clear data.
R_4_1_GLOBAL_DATA_EXT void global_data_init();
R_4_1_GLOBAL_DATA_EXT void global_data_finalize();
#endif	/*0*/

#endif //FIRMWARE_API_DATA_H
