/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef HAL_H_
#define HAL_H_

#ifdef _r_4_1_hal_h_global_		/* 210424 SGsoft */
#define R_4_1_HAL_EXT
#else
#define R_4_1_HAL_EXT	extern
#endif	/*_r_4_1_hal_h_global_*/

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "platform/daughtercard_cfg.h"
#include "apis/all_apis.h"
#include "apis/si479xx_common.h"
#endif	/*0*/

//
// Platform specific functions that should be implemented
// according to the current PLATFORM_XXX
//
R_4_1_HAL_EXT RETURN_CODE initHardware(const struct dc_cfg* dc);
R_4_1_HAL_EXT RETURN_CODE resetIC(uint8_t icNum);
R_4_1_HAL_EXT RETURN_CODE powerDownHardware(void);

R_4_1_HAL_EXT RETURN_CODE writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);
R_4_1_HAL_EXT RETURN_CODE readReply(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);

R_4_1_HAL_EXT RETURN_CODE hifi_command(dc_config_chip_select icNum, uint8_t pipe, uint8_t packet_count, uint16_t length, uint8_t* buffer);
R_4_1_HAL_EXT RETURN_CODE hifi_response(dc_config_chip_select icNum, uint8_t pipe, SI479XX_hifi_resp__data* resp);

// These are just stubs for compatibility with other projects.
R_4_1_HAL_EXT RETURN_CODE lock_chip(dc_config_chip_select ic, void*);
R_4_1_HAL_EXT RETURN_CODE unlock_chip(dc_config_chip_select ic);

// Determines whether polling the part is used for CTS,
// or if interrupts and CTSINT are enabled and should be used.
struct hal_status_cfg {
    bool cts_polling;
    bool acts_polling;
};

R_4_1_HAL_EXT RETURN_CODE set_status_cfg(struct hal_status_cfg cfg);
struct hal_status_cfg get_status_cfg();

R_4_1_HAL_EXT RETURN_CODE ctsSetMode(bool use_polling);


//
// low-level wrappers around the bare minimum writeCommand() / readReply()
// functions defined by the platform-specific hal_platform.h
//
R_4_1_HAL_EXT RETURN_CODE command(dc_config_chip_select icNum,
    uint16_t commandLength, uint8_t* commandBuffer, uint16_t replyLength,
    uint8_t* replyBuffer, uint32_t numReadTimeout);

// Some commands take longer for CTS (e.g. before BOOT),
// so this is a helper function to set the CTS timeout value.
R_4_1_HAL_EXT RETURN_CODE writeCommand_wait(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer, uint32_t timeout_ms);

R_4_1_HAL_EXT RETURN_CODE wait_for_CTS(dc_config_chip_select icNum,
    uint32_t numReadTimeout, uint8_t statusLength, uint8_t* status);

// For HIFI_CMDx sub-api commands.
R_4_1_HAL_EXT RETURN_CODE wait_for_ACTS(dc_config_chip_select icNum, uint8_t pipe, uint32_t numReadTimeout);

// Not currently implemented for EVB / daughtercards.
R_4_1_HAL_EXT RETURN_CODE audioEnable(void);
R_4_1_HAL_EXT RETURN_CODE audioDisable(void);
R_4_1_HAL_EXT RETURN_CODE writePersistentStorage(uint16_t start_addr, uint16_t length, uint8_t* buffer);
R_4_1_HAL_EXT RETURN_CODE readPersistentStorage(uint16_t start_addr, uint16_t length, uint8_t* buffer);
R_4_1_HAL_EXT void erasePersistentStorage();


#define HIFI_MAX_CMD_LEN 252
#define HIFI_MAX_RESP_LEN HIFI_MAX_CMD_LEN
// Allows skipping actually writing commands to the device.
// Used for grouping multiple HIFI_CMDx  into one command with multiple packets.
R_4_1_HAL_EXT void startHifiCmd();
R_4_1_HAL_EXT void stopHifiCmd();
R_4_1_HAL_EXT RETURN_CODE writeHifiCmd(dc_config_chip_select ic, uint8_t pipe);

// Expected to be a fast operation when there are no errors,
//   as this may get callled very often.
// 'ok_to_send_cmds' :
//		true= this function can send commands to the board for logging information.
//			e.g. errors like AUDINT that require more information.
//		false= cannot send commands, but can still do READ_REPLY operations.
//			Necessary so that different commands are not sent in between API function call+response.
//			e.g. calling TUNER::FM_RSQ_STATUS:
//			* code calls SI4796X_TUNER_fm_rsq_status__command(...)
//			* implicitly calls writeCommand(), which calls error_check(..., ok_to_send_cmds=true) BEFORE sending command
//			* error_check() is free to call other commands if necessary
//			* returns to writeCommand() which sends FM_RSQ_STATUS command
//			* again calls error_check(..., ok_to_send_cmds=false), because we can't blow away the FM_RSQ_STATUS reply buffer.
//			* code calls SI4796X_TUNER_fm_rsq_status__reply() which calls readReply() to get the reply
//
// Returns SUCCESS if no error bits were set.
R_4_1_HAL_EXT RETURN_CODE error_check(dc_config_chip_select ic, uint16_t status_len, uint8_t* status, bool ok_to_send_cmds);

// Can enable/disable error checking, only useful when handling errors.
// Returns the prior setting.
R_4_1_HAL_EXT bool toggle_errcheck(bool enable);


//
// Creating macros which can easily be replaced for
//   alternate platforms during porting - if necessary.
//**********************************************************************
#include <string.h> // Needed for memory operations
#define ClearMemory(ptr, num) memset((ptr), 0, (num))
#define CpyMemory(dPtr, sPtr, num) memcpy((dPtr), (sPtr), (num))
#define CompareMemory(ptr1, ptr2, num) memcmp((ptr1), (ptr2), (num))

// writeCommand() calls wait_for_CTS() internally.
// Can change the timeout value (only if really necessary, e.g. LOAD_CONFIG)
R_4_1_HAL_EXT void setCtsTimeout(uint32_t timeoutMs);
R_4_1_HAL_EXT uint32_t getCtsTimeout();


#endif // HAL_H_
