/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef __FIRMWARE_LOAD_HELPERS__
#define __FIRMWARE_LOAD_HELPERS__

#ifdef _r_4_1_firmware_load_helpers_h_global_		/* 210419 SGsoft */
#define R_4_1_FIRM_LOAD_HELPERS_EXT
#else
#define R_4_1_FIRM_LOAD_HELPERS_EXT	extern
#endif	/*_r_4_1_firmware_load_helpers_h_global_*/

#if 0
#include "boot/FirmwarePaths.h"
#include "boot/boot_types.h"
#include "util/flash_parser.h"
#include "platform/System_Configuration.h"
#include "common_types.h"
#include "feature_types.h"
#endif	/*0*/

#if 0		/* 210510 SGsoft */	
struct image_node {
    uint8_t ic_mask;
    FW_IMAGE_COMPONENT component;
};
#endif	/*0*/

#if 0		/* 210510 SGsoft */	
#ifdef _r_4_1_firmware_load_helpers_h_global_		/* 210419 SGsoft */
// configuration file when booting from flash memory
struct flash_cfg g_flash_cfg;
#else
// Only valid after fw_load_system() has been called.
extern struct flash_cfg g_flash_cfg;
#endif	/*_r_4_1_firmware_load_helpers_h_global_*/
#endif	/*0*/


// Host or Flash boot the system described by 'cfg', 'opmode', and 'radio_mode'.
// The radio mode is required to determine which demod image to boot for HD (AM/FM?)
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_load_system(DAUGHTERCARD_TYPE dc_type, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode);
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_load_system_cfg(DAUGHTERCARD_TYPE dc_type, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode, const char* flash_cfg);

#if 0		/* 210505 SGsoft */
// Load the demod at 'icNum' with the firmware image specified by 'mode'.
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_load_demod_image(demod_id demod, FW_IMAGE_COMPONENT image_type);

// Load falcon firmware
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_load_falcon(dc_config_chip_select ic, FW_IMAGE_COMPONENT linker_image, FW_IMAGE_COMPONENT sat1_image, FW_IMAGE_COMPONENT sat2_image);

// Get what the current demod image is for chip 'icnum'.
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_current_demod_image(demod_id demod, FW_IMAGE_COMPONENT* image_type);
#endif	/*0*/
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_load_system_1T(bool is_48pin, OPERATION_MODE opmode, RADIO_MODE_TYPE radio_mode);						/* 210427 SGsoft */

#if 0		/* 210505 SGsoft */
// The tuner on 1T1D needs to be rebooted between fm/am and dab modes.
//  Other daughtercards use different tuners for these modes and don't need to reboot tuners.
R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE fw_load_system_1T1D(bool is_48pin, OPERATION_MODE mode, RADIO_MODE_TYPE radio_type);
#endif	/*0*/

R_4_1_FIRM_LOAD_HELPERS_EXT RETURN_CODE preBoot_get_loadconfig(DAUGHTERCARD_TYPE dc_type);

R_4_1_FIRM_LOAD_HELPERS_EXT struct si479xx_powerup_args get_default_pup_args_4790(dc_config_chip_select ic, OPERATION_MODE opmode);				/* 210430 SGsoft */

#endif
