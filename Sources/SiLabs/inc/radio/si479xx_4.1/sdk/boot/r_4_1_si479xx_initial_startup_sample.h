/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef STARTUP_SAMPLE__
#define STARTUP_SAMPLE__

#ifdef _r_4_1_si479xx_init_startup_h_global_		/* 210419 SGsoft */
#define R_4_1_SI479XX_STARTUP_EXT
#else
#define R_4_1_SI479XX_STARTUP_EXT	extern
#endif	/*_r_4_1_si479xx_init_startup_h_global_*/

#ifdef __cplusplus
extern "C" {
#endif

///#include "HAL.h"
///#include "boot/boot_types.h"
///#include "util/flash_parser.h"
///#include "common_types.h"

#include <stdio.h>


//Function to clear any preboot objects and start fresh
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_startImageSetup();

//Function to clear any preboot objects
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE postBoot_emptyImageSetup();

// Set powerup arguments for given ic_mask
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_setPowerup(dc_config_chip_select ic, struct si479xx_powerup_args powerup);

// Only works for blank development parts.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_changePartNumber(dc_config_chip_select ic_mask,
    uint8_t pn_major_ascii, uint8_t pn_minor_ascii, uint8_t cust_major_ascii, uint8_t cust_minor_ascii);

//Function to load a firmware image component
//(mcu image, hifi image, properties/servo settings, topology, topology module settings, swpkgs)
//into memory for sending to the part
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_addFirmwareImageComponent(dc_config_chip_select ic_mask, uint8_t* filepathimage);

// Function to append chip complete bytes to current end of firmware buffer.
// These bytes tell the selected chip to stop listening to the load and get ready to boot.
// Required for flash boot, optional for host boot.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_appendChipCompleteBytes(dc_config_chip_select ic_mask);

// Function to load a customer encryption key blob file to
//  enable decryption of a customer encrypted image.
// While the file is selected before loading firmware,
//  the application of the image is during the loading process.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_addEncryptionKey(dc_config_chip_select ic, uint8_t* filepathimage);

//Function to initialize flash load configuration.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_flashSetLoadConfig(dc_config_chip_select ic, struct flash_load_config_element cfg);

//Function to add guid to boot element object for flash loading.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_flashAddGuid(dc_config_chip_select ic_mask, uint32_t guid);

R_4_1_SI479XX_STARTUP_EXT RETURN_CODE preBoot_flashSetOptions(uint32_t addr, uint32_t size);


// Does the boot process for a tuner chain, from host or flash image.
// User must call preBoot_xxx functions to configure the boot process first.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE multiTunerBoot(dc_config_chip_select ic_mask, bool flash_boot);

// Wrapper around multiTunerBoot() provided for backwards compatibility.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE flashBootMultichip(dc_config_chip_select ic, uint32_t flash_start_addr, uint32_t flash_img_size);
#if 0		/* 210510 SGsoft */	
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE flashBootMultichipCfg(dc_config_chip_select ic, struct flash_cfg cfg);
#endif	/*0*/

#ifdef OPTION_BOOT_FROM_HOST			/* 210514 SGsoft */	
// Wrapper around multiTunerBoot() provided for backwards compatibility.
R_4_1_SI479XX_STARTUP_EXT RETURN_CODE hostBootSingle_479xx(dc_config_chip_select ic, dc_config_chip_type type, 
        struct si479xx_powerup_args* powerup_args, 
        uint8_t* uncorrectable, uint8_t* correctable);

R_4_1_SI479XX_STARTUP_EXT RETURN_CODE HostLoadWorker(void);								/* 210430 SGsoft */
#endif	/*OPTION_BOOT_FROM_HOST*/

#ifdef __cplusplus
}
#endif

#endif

