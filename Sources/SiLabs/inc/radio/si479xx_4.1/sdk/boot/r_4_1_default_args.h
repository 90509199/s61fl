/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef SDK_BOOT_DEFAULT_ARGS_H
#define SDK_BOOT_DEFAULT_ARGS_H

#ifdef _r_4_1_default_args_h_global_		/* 210419 SGsoft */
#define R_4_1_DEFAULT_ARGS_EXT
#else
#define R_4_1_DEFAULT_ARGS_EXT	extern
#endif	/*_r_4_1_default_args_h_global_*/

///#include "boot_types.h"

#ifdef _r_4_1_default_args_h_global_		/* 210419 SGsoft */
// Values from programming guide.
struct load_config_args load_config_2dt1f[2] = {
    { // CHIP_0
        .ic = IC0,
        .cfg = {
            .numchips = 2,
            .chipid = 0,
            .role = 0,
            .payload_word_count = 13,
            .payload = {
                0x00000000, 0x00000000, 0x00000004, 0x00000000,
                0x00000000, 0x00000000, 0x00000000, 0x00280000,
                0x00000001, 0x0f071ff1, 0x0f071ff1, 0x0f071ff2,
                0x0f071ff1, 0x0f071ff0, 0x0f071ff0, 0x0f071ff0,
                0x0f071ff0,
            },
        } 
    },
    { // CHIP_1
        .ic = IC1,
        .cfg = {
            .numchips = 2, 
            .chipid = 1, 
            .role = 1, 
            .payload_word_count = 17, 
            .payload = {
                0x00000000, 0x00000400, 0x00000000, 0x00000000,
                0x00004000, 0x00003000, 0x00000000, 0x00280000,
                0x00000001, 0x11071ff3, 0x11071ff3, 0x11071ff2,
                0x11071ff1, 0x11071ff2, 0x11079ff2, 0x11079ff1, 0x11079ff2,
            },
        }
    },
};


// Values from programming guide.
struct load_config_args load_config_1dt1t1f[2] = {
    { // CHIP_0
        .ic = IC0,
        .cfg = {
            .numchips = 2,
            .chipid = 0,
            .role = 0,
            .payload_word_count = 17,
            .payload = {
                0x00000000, 0x00000000, 0x00000004, 0x00000000,
                0x00000000, 0x00000000, 0x00000000, 0x00280000,
                0x00000001, 0x0f071ff1, 0x0f071ff1, 0x0f071ff2,
                0x0f071ff1, 0x0f071ff0, 0x0f071ff0, 0x0f071ff0,
                0x0f071ff0,
            },
        } },
    { // CHIP_1
        .ic = IC1,
        .cfg = {
            .numchips = 2, .chipid = 1, .role = 1, .payload_word_count = 17, .payload = {
                                                                                 0x00000000, 0x00000400, 0x00000000, 0x00000000, 0x00004000, 0x00003000, 0x00040004, 0x0028000e, 0x00000001, 0x000807f3, 0x000807f3, 0x000807f3, 0x000807f1, 0x000d07f2, 0x002d07f2, 0x000f07f1, 0x002d07f2,
                                                                             },
        } },
};
#else
extern struct load_config_args load_config_2dt1f[2];
extern struct load_config_args load_config_1dt1t1f[2];
#endif	/*_r_4_1_default_args_h_global_*/

#endif // SDK_BOOT_DEFAULT_ARGS_H

