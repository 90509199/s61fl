/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef SDK_BOOT_BOOT_TYPES_H
#define SDK_BOOT_BOOT_TYPES_H

///#include "sdk/platform/audio_cfg.h"

// Maximum length of a largest flash load struct
#define MAX_FLASH_LOAD_POWERUP (sizeof(struct si4796x_powerup_args) + 1)

// Maximum number of GUIDS to allow for part
#define MAX_FLASH_LOAD_IMAGE_GUIDS 12

#define LOAD_INIT_ROM1__MAX_NUM_GUID 20
#define LOAD_CONFIG_ROM1__WORD_LENGTH_MAX 0x1a

#define SI479XXFLASHVERSION 0x0001
#define SI468XFLASHVERSION 0x0001
#define SILABSFLASHVERSION (SI479XXFLASHVERSION + SI468XFLASHVERSION)
#define SI479XXSTARTADDR 0x00000000
#define SI468XSTARTADDR 0x00000000

#define ENCRYPTION_KEY__MAX_FILE_LENGTH 1/*8192*/		/* 210510 SGsoft */	
#define ENCRYPTION_KEY__MAX_BUFFER_LENGTH 1/*4096*/
#define ENCRYPTION_KEY__MAX_NUM_KEYS 1

#define FIRMWARE_COMPONENT_MAX_COUNT 10/*46*/

#define MAX_SIZE_IMAGE_BUFFER_MB 4
#define MAX_SIZE_IMAGE_BUFFER_BYTES 4/*(MAX_SIZE_IMAGE_BUFFER_MB * 1024 * 1024)*/


struct encryption_key_element {
    uint8_t buffer[ENCRYPTION_KEY__MAX_BUFFER_LENGTH];
    uint16_t length;
};


struct flash_load_config_element {
    uint8_t numchips;
    uint8_t chipid;
    uint8_t role;

    // Used for EEPROM LOAD_CONFIG
    uint8_t payload_word_count;
    uint32_t payload[LOAD_CONFIG_ROM1__WORD_LENGTH_MAX];
};

struct fw_flash_image {
    uint32_t address;
    uint32_t size;
};

struct component_header {
    uint32_t guid;
    uint8_t ic_mask;
};


struct tuner_fw_image {
    // Address X
    uint16_t num_components;

    // Address X + 0x10
    struct component_header components[FIRMWARE_COMPONENT_MAX_COUNT];

    // WILL NOT BE INCLUDED IN FLASH IMAGE
    struct encryption_key_element key;
    //

	#if 0		/* 210429 SGsoft */
    // Address X + 0x02F0
    uint32_t image_len;

    // Address X + 0x0300
    // Combined image, from all components
    uint8_t image_buffer[MAX_SIZE_IMAGE_BUFFER_BYTES];
	#endif	/*0*/
};


struct part_override_element {
    uint8_t pn_major;
    uint8_t pn_minor;
    uint8_t cust_major;
    uint8_t cust_minor;
};

//additional powerup information specific to si479xx.
struct si479xx_powerup_args {
    uint8_t clkmode;
    uint8_t trsize;
    uint8_t ibias;
    uint8_t clkout;
    uint32_t xtal_freq;
    uint8_t ctun;
    uint8_t chipid;
    uint8_t clko_current;
    uint8_t ctsien;
    uint8_t vio;
    uint8_t afs;
    uint8_t eziq_master;
    uint8_t eziq_enable;
};


struct si461x_powerup_args {
    uint8_t clkmode;
    uint8_t trsize;
    uint8_t ibias;
    uint8_t ibias_run;
    uint32_t xtal_freq;
    uint8_t ctun;
    uint8_t ctsien;
};


struct tuner_boot_cfg {
    struct si479xx_powerup_args powerup;
    struct part_override_element part_override;
    // Only for flash_load'ed firmware
    struct flash_load_config_element load_cfg;
};

struct tuner_multiboot_cfg {
    // firmware image can be shared for all tuners and broadcasted once (if hardware supports it)
    // GUIDs are managed per chip inside boot_element
    struct tuner_fw_image image;

    // for flash loading images
    struct fw_flash_image flash;

    // options that are unique to each chip.
    struct tuner_boot_cfg chips[OPTION_HAL_MAX_CHIPS];
};

// For now, si469x powerup same as single demod powerup
typedef struct si461x_powerup_args si469x_powerup_args;


// LOAD_CONFIG values for different daughtercards.
#define LOAD_CONFIG_MAX_WORDS ((SI4796X_CMD_LOAD_CONFIG_LENGTH - 4) / 4)
struct load_config_args {
    dc_config_chip_select ic;
    struct flash_load_config_element cfg;
};


#endif // SDK_BOOT_BOOT_TYPES_H

