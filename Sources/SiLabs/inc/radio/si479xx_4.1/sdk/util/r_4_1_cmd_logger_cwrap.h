/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef C_LOGGER_H_
#define C_LOGGER_H_

#ifdef _r_4_1_cmd_logger_cwrap_h_global_		/* 210427 SGsoft */
#define R_4_1_CMD_LOG_CWRAP_EXT
#else
#define R_4_1_CMD_LOG_CWRAP_EXT	extern
#endif	/*_r_4_1_cmd_logger_cwrap_h_global_*/

#if 0
#ifdef __cplusplus
extern "C" {
#endif

#include "sdk/apis/si479xx_hub_api.h"
#include "sdk/common/DAB_common.h"
#include "sdk/data-handlers/DAB_Service_List_Handler.h"
#include "common_types.h"
#include "feature_types.h"
#endif	/*0*/

#if 1		/* 210429 SGsoft */
#define c_log_command(...)
#define c_log_reply(...)
#define c_log_comment(...)					
#define c_log_commentf(...)	
#define c_log_err(...)	
#define enable_logge()
#define disable_logger()
#define c_dump(...)
#define c_log_svc(...)
#define c_log_svc_list(...)
#define c_log_xbar_status(...)

#else

R_4_1_CMD_LOG_CWRAP_EXT void c_log_command(RETURN_CODE ret, uint8_t ic, uint16_t length, const uint8_t* buffer);
R_4_1_CMD_LOG_CWRAP_EXT void c_log_reply(RETURN_CODE ret, uint8_t ic, uint16_t length, const uint8_t* buffer);

R_4_1_CMD_LOG_CWRAP_EXT void c_log_comment(const char* str);				/* 210429 SGsoft */
R_4_1_CMD_LOG_CWRAP_EXT void c_log_commentf(const char* format, ...);
R_4_1_CMD_LOG_CWRAP_EXT void c_log_err(uint8_t ic, ERR_TYPE code, uint16_t length, const uint8_t* buffer);

R_4_1_CMD_LOG_CWRAP_EXT void enable_logger();
R_4_1_CMD_LOG_CWRAP_EXT void disable_logger();

R_4_1_CMD_LOG_CWRAP_EXT void c_dump(const uint8_t* fname);

// Other more involved debugging log functions.
// These functions typically rely on cpp operator<<() overrides.  (see to_cpp.h)
R_4_1_CMD_LOG_CWRAP_EXT void c_log_svc(const dab_service_info* svc);
R_4_1_CMD_LOG_CWRAP_EXT void c_log_svc_list(const dab_service_list* list);
R_4_1_CMD_LOG_CWRAP_EXT void c_log_xbar_status(const SI479XX_HUB_xbar_query__data* data);
#endif	/*1*/

#if 0
#ifdef __cplusplus
}
#endif
#endif	/*0*/

#endif
