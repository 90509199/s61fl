/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef SDK_UTIL_SDK_MACRO_H_
#define SDK_UTIL_SDK_MACRO_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#if 0
#include "cmd_logger_cwrap.h"
#include "core/debug_macro.h"
#include "osal/io_console.h"

#include "common_types.h"
#endif	/*0*/

#include <assert.h>

// Common pthread operations
#if 1		/* 210428 SGsoft */
#define LOCK(lock, error_value)
#define UNLOCK(lock, error_value)
#define SDK_LOG(...)
#define SDK_PRINTF(...)

#else
#define LOCK(lock, error_value) do { \
    int r = pthread_mutex_lock(lock); \
    if (r != 0) { \
        SDK_PRINTF("lock: %s\n", strerror(r)); \
        return error_value; \
    }} while (0)

#define UNLOCK(lock, error_value) do { \
    int r = pthread_mutex_unlock(lock); \
    if (r != 0) { \
        SDK_PRINTF("~lock: %s\n", strerror(r)); \
        return error_value; \
    }} while (0)

#define SDK_LOG(...) do { \
    c_log_commentf("%s:%d:%s(): ",  __FILE__, __LINE__, __func__); \
    c_log_commentf(__VA_ARGS__); \
} while (0)

#define SDK_PRINTF(...) do { \
    D_PRINTF(__VA_ARGS__); \
    c_log_commentf("%s:%d:%s(): ",  __FILE__, __LINE__, __func__); \
    c_log_commentf(__VA_ARGS__); \
} while (0)
#endif	/*1*/

// Goes through the buffered output display
#if 1
#define SDK_PRINTF_SI(...)
#else
#define SDK_PRINTF_SI(...) do { \
    sio_printf(__VA_ARGS__); \
    c_log_commentf("%s:%d:%s(): ",  __FILE__, __LINE__, __func__); \
    c_log_commentf(__VA_ARGS__); \
} while (0)
#endif	/*1*/


#define SDK_ASSERT(expr, ...) do { \
    if (!(expr)) { \
        SDK_PRINTF(__VA_ARGS__); \
    } \
    assert(expr); \
} while (0)


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // SDK_UTIL_SDK_MACRO_H_

