/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef AUDIO_CFG_H_
#define AUDIO_CFG_H_

#ifdef _r_4_1_audio_cfg_h_global_		/* 210421 SGsoft */
#define R_4_1_AUD_CFG_EXT
#else
#define R_4_1_AUD_CFG_EXT	extern
#endif	/*_r_4_1_audio_cfg_h_global_*/

#if 0
#include "common_types.h"
#include "config_types.h"
#include "feature_types.h"

#include "linker/linker_types.h"
#endif	/*0*/

//
// forward decls
//

// Types for the reference audio designs.
typedef enum AUDIO_CFG_TYPE {
    // No hifi capability.
    ACFG_TUNERONLY = 0,

    // Use HIFI, but only using port-port connections, not any dsp modules.
    ACFG_HIFI_PORT_ONLY,

    // Use HIFI, and one of the reference designs (6chan, 4chan)
    ACFG_HIFI_REF_DESIGN,
} AUDIO_CFG_TYPE;

typedef struct audio_cfg {
    dc_config_chip_select ic;
    // only for daughtercards with falcon
    linker_context* falcon;
    DAUGHTERCARD_TYPE dc_type;

    AUDIO_CFG_TYPE routing;
    // 'routing' determines type of this pointer.
    void* subcfg;
} audio_cfg;


#ifdef _r_4_1_audio_cfg_h_global_
audio_cfg* g_sf_audio_cfg = NULL;
#else
extern struct audio_cfg* g_sf_audio_cfg;
#endif	/*_r_4_1_audio_cfg_h_global_*/


// Initializes the configuration struct 'cfg'.
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_init(struct audio_cfg* cfg, DAUGHTERCARD_TYPE dc_type, OPERATION_MODE opmode);
R_4_1_AUD_CFG_EXT void audcfg_free(struct audio_cfg* cfg);

// Hooks up the initial audio_hub connections.
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_setup(const struct audio_cfg* cfg);

R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_set_volume(const struct audio_cfg* cfg, uint16_t attenuation_db);
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_set_mute(const struct audio_cfg* cfg, AUDIO_MUTE mute_type);

R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_get_volume(const struct audio_cfg* cfg, uint16_t* attenuation_db);
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_get_mute(const struct audio_cfg* cfg, AUDIO_MUTE* mute_type);

// Not for tuner-only audio configurations (will do nothing).
// This changes the input audio hub port.
// It will ramp down the existing connections before changing, and ramp back up.
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_change_source_port(const struct audio_cfg* cfg, uint8_t new_port);

// Change the output audio source.
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_switch_src(const struct audio_cfg* cfg, LINKER_INPUT_SOURCE src);

// Restores volume + mute settings from a previous audio_state.
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_get_state(const struct audio_cfg* cfg, audio_state* state_out);
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_restore_state(const struct audio_cfg* cfg, audio_state state);

// Handler for the service-following callback.
// Currently this only works if there's ONE struct audio_cfg* . It saves the ptr from audcfg_init() for this call;
R_4_1_AUD_CFG_EXT RETURN_CODE audcfg_sf_switch_src(LINKER_INPUT_SOURCE src);

#endif // !AUDIO_CFG_H_
