/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef SYSTEM_CONFIG_H
#define SYSTEM_CONFIG_H

#ifdef _r_4_1_system_configuration_h_global_		/* 210421 SGsoft */
#define R_4_1_SYS_CFG_EXT
#else
#define R_4_1_SYS_CFG_EXT	extern
#endif	/*_r_4_1_system_configuration_h_global_*/

#if 0
#include "daughtercard_cfg.h"
#include "audio_cfg.h"
#include "audio/ref_audio_types.h"
#include "receiver/IFirmware_API_Manager.h"
#include "common_types.h"
#include "feature_types.h"
#include "hal/hal_platform.h"
#endif	/*0*/


struct firmware_manager_pimpls{
    RETURN_CODE (*initialize) (OPERATION_MODE mode);
    RETURN_CODE (*finalize) (void);
    RETURN_CODE (*set_radio_mode) (RADIO_MODE_TYPE mode);
};


// This struct is a wrapper around some #define's in platform_options.h
// It is also a wrapper around some EEPROM information so that is not necessary.
typedef struct system_options {
    // diversity toggles
    bool hd_use_mrc;
    bool dab_use_mrc;

    // Boot related
    bool use_host_boot;

    uint8_t hd_iqrate;
    uint8_t hd_iqslot;

    // Powerup argument overrides
    uint8_t powerup_afs;
} system_options;

// Abstracts certain aspects of boards so that code can be more generic.
typedef struct system_config {
    // Mostly platform_options.h #define's
    system_options opts;

    // Daughtercard information
    DAUGHTERCARD_TYPE dc_type;
    struct dc_cfg board;

    // Bootup mode (HD/DAB  for now)
    OPERATION_MODE opmode;

    // Information for setting volume/mute.
    struct audio_cfg audio;

    //implementation of FW API Manager functions
    struct firmware_manager_pimpls fw;
} system_config;


R_4_1_SYS_CFG_EXT void syscfg_set_api_interface();

// Required to call first, with pointer to allocated system_config.
// Loads copy of EEPROM, if available.
R_4_1_SYS_CFG_EXT RETURN_CODE syscfg_initialize(system_config* config, OPERATION_MODE mode, const struct hal_stored_cfg* hw_cfg);

// Releases resources held by a system_config.
// Invalidates the config pointer.
R_4_1_SYS_CFG_EXT void syscfg_free(system_config* config);


//
// Other syscfg_ functions only valid after syscfg_initialize()
//
R_4_1_SYS_CFG_EXT bool syscfg_is_initialized(const system_config* config);

// Information from the daughtercard type.
R_4_1_SYS_CFG_EXT bool syscfg_has_linker(const system_config* config);
R_4_1_SYS_CFG_EXT bool syscfg_has_dual_tuner(const system_config* config);

R_4_1_SYS_CFG_EXT RETURN_CODE syscfg_get_first_linker(const system_config* cfg, linker_id* linker);

// All chips needed for any particular FM operation.
typedef struct fm_chips {
    tuner_id tuner;

    bool hd_enabled;
    demod_id hd_demod;

    bool phase_div_enabled;
    bool hd_div_enabled;
    tuner_id div_tuner;
} fm_chips;

R_4_1_SYS_CFG_EXT fm_chips get_default_fm_chips();

// All chips needed for any DAB operation
typedef struct dab_chips {
    tuner_id tuner;
    demod_id demod;

    bool dab_mrc_enabled;
    tuner_id mrc_tuner;
} dab_chips;

R_4_1_SYS_CFG_EXT dab_chips get_default_dab_chips();

typedef struct hd_mode_chips {
    // Must at least have one fm/am hd chip set
    fm_chips primary;

    bool has_background;
    fm_chips back;
} hd_mode_chips;

typedef struct dab_mode_chips {
    // Must at least have one DAB chip set
    dab_chips main;

    // May have background DAB chips
    bool has_background_dab;
    dab_chips bg;

    // May also have FM/AM analog tuners.
    bool has_fm;
    fm_chips fm_am;
} dab_mode_chips;

// Get references to Tuners/Demods/Linkers that implement a specific mode/function for

// FM/AM analog and possibly HD
R_4_1_SYS_CFG_EXT RETURN_CODE syscfg_get_hd(const system_config* config, hd_mode_chips* chips);

// DAB and possibly FM/AM analog
R_4_1_SYS_CFG_EXT RETURN_CODE syscfg_get_dab(const system_config* config, dab_mode_chips* chips);

//
// Functions that assume g_evb_system_cfg is used as the system_config*
//

R_4_1_SYS_CFG_EXT RETURN_CODE syscfg_get_ic_type(dc_config_chip_select ic, dc_config_chip_type* type);
R_4_1_SYS_CFG_EXT bool syscfg_is_audio_tuneronly();
R_4_1_SYS_CFG_EXT bool syscfg_is_audio_ref6ch();

R_4_1_SYS_CFG_EXT bool syscfg_is_host_boot();
R_4_1_SYS_CFG_EXT bool syscfg_is_flash_boot();

// Checks if the current board (1T1D ALTA) is
//  the 48 pin or the 84 pin package, which determines if
//  it is capable of using HIFI features, as well as changes
//  the audio routing between the tuner and the demod.
R_4_1_SYS_CFG_EXT bool syscfg_is_1T1D_48();

#endif //SYSTEM_CONFIG_H
