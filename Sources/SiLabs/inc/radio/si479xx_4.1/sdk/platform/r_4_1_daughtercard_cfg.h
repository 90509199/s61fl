/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
/**
 * @file daughtercard_cfg.h
 * @brief Takes a daughtercard type (e.g. 2DT1F),
 * and returns information about the board.
 *
 * Meant to consolidate some daugthercard-specific information, 
 * and enable write reusable code. 
 *
 * @author erik olson
 * @date 2019-01-24
 */

#ifndef BOARD_CONFIG_H
#define BOARD_CONFIG_H

#ifdef _r_4_1_daughtercard_cfg_h_global_		/* 210421 SGsoft */
#define R_4_1_DCARD_CFG_EXT
#else
#define R_4_1_DCARD_CFG_EXT	extern
#endif	/*_r_4_1_daughtercard_cfg_h_global_*/


///#include "common_types.h"
///#include "config_types.h"
///#include "feature_types.h"


///#define BOARD_CONFIG_DESC_LEN 50							/* 210427 SGsoft */

struct dc_cfg {
    // Types of all chips on board.
    dc_config_chip_type chip_types[(OPTION_HAL_MAX_CHIPS+1)];

    //string that describes current board e.g. "1T1D_84"
    ///char description[BOARD_CONFIG_DESC_LEN];				/* 210427 SGsoft */
};

R_4_1_DCARD_CFG_EXT struct dc_cfg dcfg_default();

R_4_1_DCARD_CFG_EXT bool dcfg_is_multi_tuner(tuner_id tuner);
R_4_1_DCARD_CFG_EXT bool dcfg_is_satellite(demod_id demod);

// true for single + multi tuner parts.
R_4_1_DCARD_CFG_EXT bool dcfg_is_tuner_ic(const struct dc_cfg* cfg, dc_config_chip_select ic);
R_4_1_DCARD_CFG_EXT bool dcfg_is_demod_ic(const struct dc_cfg* cfg, dc_config_chip_select ic);

R_4_1_DCARD_CFG_EXT LINKER_INPUT_SOURCE dcfg_linker_src(demod_id demod);

R_4_1_DCARD_CFG_EXT RETURN_CODE dcfg_initialize(struct dc_cfg* cfg, DAUGHTERCARD_TYPE dc_type);

#endif
