/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef FM_AM_DAB__COMMON_H_
#define FM_AM_DAB__COMMON_H_

#ifdef _r_4_1_implementation_common_h_global_		/* 210421 SGsoft */
#define R_4_1_IMPL_COMMON_EXT
#else
#define R_4_1_IMPL_COMMON_EXT	extern
#endif	/*_r_4_1_implementation_common_h_global_*/

///#include "receiver/IFirmware_API_Manager.h"
///#include "platform/System_Configuration.h"
///#include "feature_types.h"

// negative one in SW32F22
#define M_ONE_TWOS 0xFFC00000

#define ONE_SW32F22 0x00400000
#define TWO_SW32F22 0x00800000
#define HALF_SW32F22 0x00200000

R_4_1_IMPL_COMMON_EXT void log_si479xx_part_info(dc_config_chip_select ic);
#if 0		/* 210505 SGsoft */	
R_4_1_IMPL_COMMON_EXT RETURN_CODE log_falcon_part_info(dc_config_chip_select ic);
#endif	/*0*/

// Common configuration between modes.
R_4_1_IMPL_COMMON_EXT RETURN_CODE setup_tuneronly_audio_1T(OPERATION_MODE opmode, demod_id demod, bool enable_tuner_tx);				/* 210429 SGsoft */

#if 0		/* 210505 SGsoft */	
R_4_1_IMPL_COMMON_EXT RETURN_CODE setup_tuneronly_audio_1T1D(OPERATION_MODE opmode, demod_id demod, bool enable_tuner_tx);

R_4_1_IMPL_COMMON_EXT RETURN_CODE setup_tuner_audio_2DT1F(LINKER_AUDIO_MODE audio_mode);
R_4_1_IMPL_COMMON_EXT RETURN_CODE setup_tuner_audio_HD_2DT1F(LINKER_AUDIO_MODE audio_mode);
#endif	/*0*/

#endif // FM_AM_DAB__COMMON_H_
