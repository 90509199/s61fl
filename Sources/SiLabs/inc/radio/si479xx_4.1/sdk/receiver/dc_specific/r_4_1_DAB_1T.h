/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef DAB__1T_H_
#define DAB__1T_H_

#ifdef _r_4_1_dab_1t_h_global_		/* 210427 SGsoft */
#define R_4_1_DAB_1T_EXT
#else
#define R_4_1_DAB_1T_EXT	extern
#endif	/*_r_4_1_dab_1t_h_global_*/

#if 0
#include "platform/System_Configuration.h"
#include "config_types.h"
#include "feature_types.h"
#include "HAL.h"
#endif	/*0*/

/*
 *  Radio Manager API Implementations
 */

R_4_1_DAB_1T_EXT RETURN_CODE Initialize_DAB_1T(OPERATION_MODE mode);
R_4_1_DAB_1T_EXT RETURN_CODE SetRadioMode_DAB_1T(RADIO_MODE_TYPE mode);

#endif // DAB__1T_H_
