/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef SI46XX_INTERFACE_HANDLER_TYPE__
#define SI46XX_INTERFACE_HANDLER_TYPE__

#ifdef _r_4_1_receiver_api_h_global_		/* 210421 SGsoft */
#define R_4_1_RECV_API_EXT
#else
#define R_4_1_RECV_API_EXT	extern
#endif	/*_r_4_1_receiver_api_h_global_*/

#if 0
#include "apis/si46xx_dab_firmware_api.h"
#include "apis/si479xx_firmware_api.h"
#include "common/DAB_common.h"
#include "data-handlers/DAB_Service_List_Handler.h"
#include "data-handlers/FMHD_PSD_Handler.h"
#include "data-handlers/RDS_Handler.h"
#include "platform/System_Configuration.h"
#include "feature_types.h"
#include "HAL.h"
#include "platform_options.h"
#endif	/*0*/

// Startup Functions
//************************************************************************************************************************

//************************************************************************************************************************
// Function: Initialize
// Inputs:
//     - mode: the selected image type you would like to start the radio in.
//			Note this is limited by the part's supported modes
// Outputs:
//     - RETURN_CODE
// Description:
//	This is the first function you call when starting or changing radio modes.  It handles firmware download
//     and part configuration.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Initialize(OPERATION_MODE mode);

//************************************************************************************************************************
// Function: GetSystemConfig
// Outputs:
//     - return value:  NULL if error (must come after Initialize() and before any Finalize() call.
// Description:
//	  Rarely needed by user, but provides access to the  underlying board configuration which can be used
//    to get unique identifiers for the tuners/demods/linkers in the system.
//   e.g. used to get a demod identifier to print the stored dab service list in printers.cpp
//************************************************************************************************************************
R_4_1_RECV_API_EXT const system_config* GetSystemConfig();

//************************************************************************************************************************
// Function: SetRadioMode 
// Inputs:
//     - mode: the desired radio type to set the radio to.
// Outputs:
//     - RETURN_CODE
// Description:
//	When using a board with HD support,  this switches between FMHD/AMHD modes.
//  When using a board with DAB support, this switches between FM/AM/DAB modes 
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SetRadioMode(RADIO_MODE_TYPE mode);

#if 0

//************************************************************************************************************************
// Function: Finalize
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description:
//	this is the final function you call when you are finished with the radio.
//	It will put the part in receiver  in its lowest power state.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Finalize();

// AM + FM + HD Functions
//************************************************************************************************************************
//************************************************************************************************************************

//************************************************************************************************************************
// Function: Tune
// Inputs:
//     - freq: the desired tune frequency
//          - for fm,fmhd,am,amhd  in 1kHz units.
//          - fm example: 103.5 MHz -> 103500 kHz
//          - am example: 1490 kHz
// Outputs:
//     - RETURN_CODE
// Description: This function should be called after initial startup in FM/FMHD mode to establish an initial frequency.
//     after initial tune, it is desirable to use TuneStep() and SeekStart() to move between frequencies.
//     Be sure that any calls the SetStepSize() or SetBandLimits() are performed prior to this initial Tune()
//
//     Note: because some amount of time is required for Tune to complete - the host should call tuner_is_seek_tune_complete()
//         to verify completion.  No callback is issued because all tune processing occurs in the receiver chip.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Tune(uint32_t freq_khz);

//************************************************************************************************************************
// Function: SetStepSize
// Inputs
//     - stepsize_khz: the desired frequency spacing for the region in use.  Example: 100 kHz (100) or 200 kHz (200).
// Outputs:
//     - RETURN_CODE
// Description: This function is used to configure the channel spacing for the current region.
//     Thus function should be called before Tune() or SeekStart()
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SetStepSize(uint8_t stepsize_khz);

//************************************************************************************************************************
// Function: SetBandLimits
// Inputs
//     - limits: the band limits for the region in use.
//			Defined in common_types.h: JAPAN_76_90, STANDARD_875_1079, WIDE_78_108
// Outputs:
//     - RETURN_CODE
// Description: This function is used to configure the band limits for the current region.
//     Thus function should be called before Tune() or SeekStart()
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SetBandLimits(BAND_LIMITS limits);

//************************************************************************************************************************
// Function: TuneStep
// Inputs
//     - stepup: false: step down, true: step up.  Used to select the direction for the step.
// Outputs:
//     - RETURN_CODE
// Description: This function is used to advance the current service to the next in the selected direction.
//     For fmonly mode: this will step the frequency: current_frequency + step_size (as defined in SetStepSize())
//          Note: this will also wrap to the other side of a band boundry
//     For fmhd mode:
//          - when HD is acquired on the current frequency, will move to the next service while staying on the frequency.
//          If there is no more subchannels, will advance to the next frequency
//			- when HD is not acquired on the current frequency, will move to the next frequency
//          Note: this will also wrap to the other side of a band boundry
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE TuneStep(bool stepup);

//************************************************************************************************************************
// Function: SeekStart
// Inputs
//     - seekup: false: seek down, true: seek up.  Used to select the direction for the seek.
//     - wrap: false: do not wrap/stop at band edge, true: wrap.  Used to select if the seek should wrap at the band edge or stop
//     - hdseekonly: false: seek only with analog indicators for a valid station, true: seek stop only on stations with HD.
// Outputs:
//     - RETURN_CODE
// Description: This function is used to start a seek to the next valid channel/service.
//     For fmonly mode: this will seek to the next valid frequency according to the RSSI/SNR thresholds
//     For fmhd mode:
//          - when HD is acquired on the current frequency, will move to the next service while staying on the frequency.
//          If there is no more subchannels, will seek to the next valid frequency according to the RSSI/SNR thresholds
//			- when HD is not acquired on the current frequency, will seek to the next valid frequency according to the RSSI/SNR thresholds
// 		Note: this command will start the seek process in the receiver, the user can end the seek early by called SeekStop().
//		Note: The host should keep checking tuner_is_seek_tune_complete() for a non-zero value to determine the seek is complete.
//          .  No callback is issued because all seek processing occurs in the receiver chip.
//      Calls to UpdateMetrics are valid to update the current frequency the seek is on while seeking.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SeekStart(bool seekup, bool wrap, bool hdseekonly);

//************************************************************************************************************************
// Function: SeekStop
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description: This function is used to cancel a seek currently in progress.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SeekStop();

//************************************************************************************************************************
// Function: ForceMono_FM
// Inputs:
//     - audio_MonStereo:  	AUDIO_STEREO = 0, AUDIO_MONO = 1,
// Outputs:
//     - RETURN_CODE.
// Description: It is desirable during FM testing to force mono for some tests.  This is not necessary for normal operation.
//     Note: this is only applicable to "fmonly" mode.
//************************************************************************************************************************
//Added for testing device using SDK based platform
R_4_1_RECV_API_EXT RETURN_CODE ForceMono_FM(AUDIO_MONO_STEREO_SWITCH audio_MonStereo);

//************************************************************************************************************************
// Function: BrowseServicesChangeSelection_HD
// Inputs:
//     - stepup: false: step down, true: step up.  Used to select the direction for the step in the service list.
// Outputs:
//		- new_selection : set to the value of the newly selected service.
//     - RETURN_CODE
// Description:
//		This function moves up/down the HD services (for current frequency)
//			and provides UI information for enabling a user to select a service.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE BrowseServicesChangeSelection_HD(bool stepup, fmhd_service_list_element* new_selection);

//************************************************************************************************************************
// Function: GetCurrentBrowseService_HD
// Inputs: none
// Outputs:
//     - selected_service : currently selected HD service from the Browse menu.
// Description:
//		This function provides service information for display about the current selected service for browsing.
//      This is related to "BrowseServicesChangeSelection_HD(),
//		  but enables a re-read of the information without adjusting the browse index.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE GetCurrentBrowseService_HD(fmhd_service_list_element* selected_service);

//************************************************************************************************************************
// Function: BrowseServicesStartCurrentSelection_HD
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description:
//		While in a browsing mode, this function starts the current browsed service.
//      For example: the user has browsed up 2 times with calls to BrowseServicesChangeSelection_HD(..)
//		and has decided to start that service - this function would then be called.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE BrowseServicesStartCurrentSelection_HD();

//************************************************************************************************************************
// DAB Functions
//************************************************************************************************************************
// Function: SetFrequencyList_DAB
// Inputs:
//     - num_freqs : the number of frequencies in the new scan list
//     - freqs     : a pointer to the 32 bit frequency array for the new frequency list
// Outputs:
//     - RETURN_CODE
// Description: This function changes the list of frequencies used by the software when scanning and tuning by index.
//      NOTE: after changing this list, any existing service list information becomes invalid
//				and must be regenerated (through ScanBand_DAB() or ManualTune_DAB())
//
//			Power Down
//            1) extract the current ServiceId,ComponentId - GetCurrentServiceIdentification_DAB
//            2) extract the current frequencyIndex - from dabMetrics
//			  3) extract which list mode (full/reduced) is currently being used - from MMI code
//            4) store this information + power down
//			Power Up
//			  1) extract the stored variables
//            2) set the frequency list back reduced mode if previously in that mode (the chip will default to full mode)
//			  3) manually tune to the previous index - ManualTune_DAB
//            4) manually restart the original service - StartProcessingChannel
//					(with the type as SERVICE_TYPE_DAB_AUDIO), and NULL for the bufferPtr)
//            5) to restore the full service list a rescan will be necessary - which will interrupt audio
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SetFrequencyList_DAB(uint8_t num_freqs, uint32_t* freqs);

//************************************************************************************************************************
// Function: GetFrequencyList_DAB
// Inputs:
//     - buf_sz : the total size of the frequency buffer in bytes.  Necessary to prevent memory overflow.
// Outputs:
//     - num_freqs	: the number of frequencies in the new scan list
//     - freqs		: a pointer to the 32 bit frequency array for the new frequency list
//     - RETURN_CODE
// Description: This function reads back the current frequency list being used by the radio chip.
//		It will default to the full european list upon reset
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE GetFrequencyList_DAB(uint16_t buf_sz, uint8_t* num_freqs, uint32_t* freqs);

//************************************************************************************************************************
// Function: ScanBand_DAB
// Inputs: none
// Outputs:
//     - RETURN_CODE : SUCCESS if any service was found.
// Description:
//		This function intiates a scan of the European DAB band collecting service list information of available audio
//     programs.  After each valid ensemble's service list is collected, or invalid ensemble skipped, a callback
//     will hand over control to the MMI for UI update: CALLBACK_Updated_Data(SCB_DAB_TUNE_SCAN_PROCESS_UPDATE);
//     Once the scan is complete there is a final call to CALLBACK_Updated_Data(SCB_DAB_TUNE_SCAN_PROCESS_COMPLETE);
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE ScanBand_DAB();

//************************************************************************************************************************
// Function: ScanBandCancel_DAB
// Inputs: none
// Outputs: none
// Description: This function procides a way to cancel a DAB scan currently in progress.
//************************************************************************************************************************
R_4_1_RECV_API_EXT void ScanBandCancel_DAB();

//************************************************************************************************************************
// Function: ManualTune_DAB
// Inputs:
//     - freqIndex - the frequency index desired to tune to -
//			based upon those determined by SetFrequencyList_DAB()/GetFrequencyList_DAB()
// Outputs:
//     - RETURN_CODE - will give TIMEOUT indication if the frequency was unable to tune.
// Description:
//		This function provides a way to manually set the ensemble to a user indicated preference,
//				the service list will update
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE ManualTune_DAB(uint8_t freqIndex);

//************************************************************************************************************************
// Function: UpdateServiceFollowing_DAB
// Inputs: none
// Outputs:
//      - RETURN_CODE - will give TIMEOUT/INVALID_INPUT indication if the attempt was unsuccessful.
//				Will give INVALID_MODE if currently ACQUIRED.
// Description:
//		This function provides a way to run a check if the currently playing service needs to attempt to be restarted.
//       This can be called periodically with little overhead.
//
//		If the current service was acquired on DAB, there is a call to CALLBACK_Updated_Data(SCB_SERVICE_ACQ_DAB);
//		If the current service was acquired on FM, there is a call to CALLBACK_Updated_Data(SCB_SERVICE_ACQ_FM);,
//			and the user should update the UI accordingly.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE UpdateServiceFollowing_DAB();

//************************************************************************************************************************
// Function: BrowseServicesReset_DAB
// Inputs: none
// Outputs: none
// Description: This function resets the browse index in the service list back to the current playing service.
//      For example:
//      a service list has 3 entries BBC1, BBC2, and BBC3 - the current playing service is BBC2.
//		If a user browses up one service  the UI would show information for BBC3.
//		If the user does not indicate to start the service after some time, the UI
//      would return to the "Now Playing" screen.  Without a callto BrowseServicesReset_DAB, on the next browse up,
//      the browsed service would be BBC1 instead of BBC3.
//************************************************************************************************************************
R_4_1_RECV_API_EXT void BrowseServicesReset_DAB();

//************************************************************************************************************************
// Function: BrowseServicesChangeSelection_DAB
// Inputs:
//     - stepup: false: step down, true: step up.  Used to select the direction for the step in the service list.
// Outputs:
//     - list_elem -> variable created by caller to be set to new selected service
// Description:
//		This function moves up/down the service list and provides UI information for enabling a user to select a service
//      Note: be sure to call BrowseServicesReset_DAB() after leaving the browse menu
//				in order to start browsing at the current service
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE BrowseServicesChangeSelection_DAB(bool stepup, dab_service_list_element* list_elem);

//************************************************************************************************************************
// Function: BrowseServicesStartCurrentSelection_DAB
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description: While in a browsing mode, this function starts the current browsed service.
//      For example: the user has browsed up 2 times with calls to BrowseServicesChangeSelection_DAB(..)
//		and has decided to start that service - this function would then be called.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE BrowseServicesStartCurrentSelection_DAB();

//************************************************************************************************************************
// Function: GetCurrentServiceIdentification_DAB
// Inputs: none
// Outputs:
//     - serviceId = the current playing service's service id
//     - componentId = the current playing service's component id
// Description:
//		This function returns the service id and component id of the currently playing audio service.  This is
//      necessary to manually restart the current service after power down using the
//			 ManualTune_DAB and StartProcessingChannel apis
//************************************************************************************************************************
R_4_1_RECV_API_EXT void GetCurrentServiceIdentification_DAB(uint32_t* serviceId, uint16_t* componentId);

//************************************************************************************************************************
// Function: StartLastServiceByIndex
// Inputs:
//      - serviceListIndex = the index obtained from the DAB Metrics for
//			the previously played service's location in the service list
// Outputs: none
// Description: This function will attempt to start the previously played service.
//
//		If the current service was acquired on DAB, there is a call to CALLBACK_Updated_Data(SCB_SERVICE_ACQ_DAB);
//		If the current service was acquired on FM, there is a call to CALLBACK_Updated_Data(SCB_SERVICE_ACQ_FM);,
//			and the user should update the UI accordingly.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE StartLastServiceByIndex(uint8_t serviceListIndex);

//************************************************************************************************************************
// Function: GetCurrentService_DAB
// Inputs: none
// Outputs:
//     - list_elem - a pointer to variable created by caller, to copy the currently playing.
//                   Contains information like name, component type (DAB,DMB), program type, etc...
// Description: This function provides service information for display about the current playing service.
//      For example: While in a "now playing" mode, this function would be called to populate the UI
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE GetCurrentService_DAB(dab_service_list_element* list_elem);

//************************************************************************************************************************
// Function: GetCurrentBrowseServiceString_DAB
// Inputs: none
// Outputs:
//     - list_elem - a pointer to variable created by caller, to copy the currently browsed service into.
//                   Contains information like name, component type (DAB,DMB), program type, etc...
// Description: 
//		This function provides service information for display about the current selected service for browsing.
//      This is related to "BrowseServicesChangeSelection_DAB(), 
//		but enables a re-read of the information without adjusting the browse index
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE GetCurrentBrowseService_DAB(dab_service_list_element* list_elem);


//************************************************************************************************************************
// Function: GetCurrentEnsembleNameString_DAB
// Inputs: none
// Outputs:
//     - ensemble_name - a pointer to the string for the currently tuned ensemble (length = 16 characters, encoding = EBU)
//     - RETURN_CODE
// Description: This function provides the string which describes the currently tuned ensemble.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE GetCurrentEnsembleNameString_DAB(uint8_t* ensemble_name);

//************************************************************************************************************************
// Function: GetCurrentTime_DAB
// Inputs:
//     - region - the location desired for the current time (see DAB_TIME_REGION_TYPE)
// Outputs:
//     - time_data - the current time info from the tuned ensemble (see DAB_TIME for the struct format)
//     - RETURN_CODE
// Description: This function provides the current time for the tuned ensemble in local or UTC.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE GetCurrentTime_DAB(DAB_TIME_REGION_TYPE region, dab_time* time_data);

//************************************************************************************************************************
// Function: SaveServiceList_DAB
// Inputs: none
// Outputs: none
// Description:
//	This function stores the current service list database into persistent storage for use when powering down the radio
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE SaveServiceList_DAB();

//************************************************************************************************************************
// Function: LoadServiceList_DAB
// Inputs: none
// Outputs:
//      - lastPlayedIndex = the service list index of the last played service,
//				this is the correct input for calling StartLastServiceByIndex to resume the previous service.
//
// Description: This function loads the saved service list database into RAM for use when powering up the radio
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE LoadServiceList_DAB(uint8_t* lastPlayedIndex);

//************************************************************************************************************************
// Function: EraseServiceList_DAB
// Inputs: none
// Outputs: none
// Description: This function clears the saved service list
//************************************************************************************************************************
R_4_1_RECV_API_EXT void EraseServiceList_DAB();

//************************************************************************************************************************
// Common Functions
//************************************************************************************************************************
// Function: AudioLevel
// Inputs:
//     - attenuation_db :  dB of attenuation to apply to the full scale output signal.
//				* 0  = no attenuation, loudest output
//				* 95 = 95 dB attenuation, (MAX attenuation)
// Outputs:
//     - RETURN_CODE
// Description: This function provides control of the volume level of the audio output by the receiver.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE AudioLevel(uint16_t attenuation_db);

//************************************************************************************************************************
// Function: AudioMute
// Inputs:
//     - mute: the desired mute type:
//          	AUDIO_MUTE_NOT_MUTED = 0,  AUDIO_MUTE_BOTH = 3,
// Outputs:
//     - RETURN_CODE
// Description: This function provides control of mute for the audio output by the receiver.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE AudioMute(AUDIO_MUTE mute);

//************************************************************************************************************************
// Function: UpdateMetrics
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description:
//		This function updates the receiver metrics for the current operating mode.  This function is intended to be
//      called periodically to provide up to date info for signal level indicators.
//
//      To get the metrics: call the listed function for the current mode to get a pointer to the metrics which were updated
//      - fm_metrics* MetricsGetFmPtr();
//      - hd_metrics* MetricsGetFmhdPtr();
//      - dab_metrics* MetricsGetDabPtr();
//      Note: you do not need to create a variable simply call the function and dereference the result:
//          example MetricsGetFmPtr()->FREQUENCY_10KHZ
//
// Be careful not to call this function too often as it will always read from the reciever to perform the update.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE UpdateMetrics();

//************************************************************************************************************************
// Function: UpdateServiceList
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description:
//		This function is called whenever the host expects a service list for the current freq/ensemble should be updated.
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE UpdateServiceList();

//************************************************************************************************************************
// Function: UpdateDataServiceData
// Inputs: none
// Outputs:
//     - RETURN_CODE
// Description:
//		This function handles the necessary processes to update the Data services running.  It should be called periodically.
//      dab: updates DLS
//      fmhd: updates SIS and PSD, as well as RDS
//      fmonly: updates RDS
//
//      To access the data buffers - see the pointer functions in the Data Buffer Pointers section for the desired data type.
//			example: PSDGetTitlePtr, SISGetSloganPtr, DLSGetPtr, RDSGetRadioTextPtr, etc
//
//		Note that the RDS interrupt sources need to be configured to get data. e.g. setting property FM_RDS_INTERRUPT_SOURCE
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE UpdateDataServiceData();

//************************************************************************************************************************
// Advanced Functions
R_4_1_RECV_API_EXT RETURN_CODE StartProcessingChannel(GENERAL_SERVICE_TYPE type, uint32_t service_id, uint32_t component_id, uint8_t* service_buffer);

R_4_1_RECV_API_EXT RETURN_CODE StopProcessingChannel(uint32_t service_id, uint32_t component_id);

///R_4_1_RECV_API_EXT SI479XX_read_status__data GetSi479XXStatus(uint8_t icNum);					/* 210426 SGsoft */

// **************************************************************
// The following Properties are set by other API commands, and should not be altered directly.
// FM_SEEK_BAND_BOTTOM
// FM_SEEK_BAND_TOP
// FM_SEEK_FREQUENCY_SPACING
// **************************************************************
R_4_1_RECV_API_EXT RETURN_CODE set_prop(uint8_t icNum, uint16_t prop_id, uint16_t value);
R_4_1_RECV_API_EXT RETURN_CODE get_prop(uint8_t icNum, uint16_t prop_id, uint16_t* value);

#ifdef OPTION_RADIODNS
//************************************************************************************************************************
// Function: UpdateRadioDNS_FMRDS
// Inputs:
//     - none
// Outputs:
//     - elements - all the elements needed to compose the radioDNS address string
//     - RETURN_CODE
// Description: This function calculates each of the pieces of the FM RDS RadioDNS string
//     Note: This does not support the country code form of the string
//			as that information is not available from the broadcast alone
// String format: <frequency>.<pi>.<gcc>.fm.radiodns.org
//     - frequency: in 5 characters (leading 0 for frequencies below 100MHz)
//     - pi: in 4 characters
//     - gcc: in 3 characters
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE UpdateRadioDNS_FMRDS(radioDNS_fm* elements);

//************************************************************************************************************************
// Function: UpdateRadioDNS_DAB
// Inputs:
//     - none
// Outputs:
//     - elements - all the elements needed to compose the radioDNS address string
//     - RETURN_CODE
// Description: This function calculates each of the pieces of the DAB RadioDNS string
//     Note: This does not support the <apptype-uatype>.<pa>. variant
// String format: <scids>.<sid>.<eid>.<gcc>.dab.radiodns.org
//     - scids: in 1 character
//     - sid: in 4 characters (when the upper 16 bits are 0) or
//            in 8 characters (when the upper 16 bits are non-zero)
//     - eid: in 4 characters
//     - gcc: in 3 characters
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE UpdateRadioDNS_DAB(radioDNS_dab* elements);
#endif

//************************************************************************************************************************
// Function: Test_GetBER_DAB
// Inputs:
//     - berPattern - the 8 bit pattern for the test ETI.  example 0b11111111 or 0b00000000
// Outputs:
//     - errorBits - the number of error bits recieved from the (audio) test pattern ETI
//     - totalBits - the total number of bits recieved from the (audio) test pattern ETI
//     - test_passed - the boolean (true = pass, false = fail) for the BER calculation being less than 10e-4.
//     - RETURN_CODE
// Description: This function provides the current BER.  the BER test is expected to capture 1,000,000 samples
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Test_GetBER_DAB(uint8_t berPattern, uint32_t* errorBits, uint32_t* totalBits, bool* test_passed);

//************************************************************************************************************************
// Function: Test_GetBER_FMHD
// Inputs: none, the BER test is expected to capture 1,000,000 samples per iBiquity
// Outputs:
//     - errorBits - the number of error bits recieved from IB_FMr208c_e1wfc204
//     - totalBits - the total number of bits recieved from IB_FMr208c_e1wfc204
//     - test_passed - the boolean (true = pass, false = fail) for the BER calculation being less than 5e-5.
//     - RETURN_CODE
// Description: This function provides the current BER
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Test_GetBER_FMHD(uint32_t* errorBits, uint32_t* totalBits, bool* test_passed);

//************************************************************************************************************************
// Function: Test_StartFreeRunningBER_FMHD
// Inputs: none
// Outputs: none
// Description:
//	This function enables and resets the BER counters, this is intended to be used before Test_GetFreeRunningBER_FMHD
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Test_StartFreeRunningBER_FMHD();

//************************************************************************************************************************
// Function: Test_GetFreeRunningBER_FMHD
// Inputs:
//     - bitType: an enumeration for the HD BER type to check: see definition of FMHD_BER_TYPE
// Outputs:
//     - errorBits - the number of error bits recieved
//     - totalBits - the total number of bits recieved
// Description: This function provides the current BER
//************************************************************************************************************************
R_4_1_RECV_API_EXT RETURN_CODE Test_GetFreeRunningBER_FMHD(uint8_t bitType, uint32_t* errorBits, uint32_t* totalBits);
#endif	/*0*/

//************************************************************************************************************************
// Data Buffer Pointers

#if 0		/* 210504 SGsoft */	
//Metrics
R_4_1_RECV_API_EXT const fm_metrics* MetricsGetFmPtr();
R_4_1_RECV_API_EXT const fm_diversity_metrics* MetricsGetFmDiversityPtr();
R_4_1_RECV_API_EXT const hd_metrics* MetricsGetFmhdPtr();
R_4_1_RECV_API_EXT const dab_metrics* MetricsGetDabPtr();
R_4_1_RECV_API_EXT const dab_mrc_metrics* MetricsGetDabMrcPtr();
R_4_1_RECV_API_EXT const am_metrics* MetricsGetAmPtr();
R_4_1_RECV_API_EXT const hd_metrics* MetricsGetAmhdPtr();
R_4_1_RECV_API_EXT const board_metrics* MetricsGetBoardPtr();

R_4_1_RECV_API_EXT const hd_mrc_metrics* MetricsGetHdMrcPtr();

//Service Lists
R_4_1_RECV_API_EXT const fmhd_service_list_fast* FMHDServiceListFastPtr();
R_4_1_RECV_API_EXT const fmhd_service_list* FMHDServiceListAudioPtr();

R_4_1_RECV_API_EXT dab_service_list* DABServiceListAudioPtr();

// RDS
R_4_1_RECV_API_EXT uint16_t RDSGetProgramIdentifier();
R_4_1_RECV_API_EXT bool RDSProgramIdentifierValid();
R_4_1_RECV_API_EXT uint8_t RDSGetProgramType();
#ifdef OPTION_RADIODNS
R_4_1_RECV_API_EXT uint8_t RDSGetECC();
#endif
#ifdef OPTION_RDS_BUFFER_V4L2
//TODO: add V4L2
#else
R_4_1_RECV_API_EXT void ConfigureRDSOptions(bool rds_ignore_AB_flag);

R_4_1_RECV_API_EXT const uint8_t* RDSGetRadioTextPtr(); // Displayed Radio Text (64 bytes)
R_4_1_RECV_API_EXT const uint8_t* RDSGetProgramServiceTextPtr(); // Displayed Program Service text (8 bytes)
R_4_1_RECV_API_EXT rds_time RDSGetCurrentTime();
#endif
//TODO: add BLER interface if desired

#ifdef OPTION_DECODE_SIS
R_4_1_RECV_API_EXT const fmhd_sis_slogan* SISGetSloganPtr();
R_4_1_RECV_API_EXT const fmhd_sis_universal_short_name* SISGetUSNPtr();
R_4_1_RECV_API_EXT const fmhd_sis_station_message* SISGetSMPtr();
R_4_1_RECV_API_EXT const uint8_t* SISGetSNLPtr();
R_4_1_RECV_API_EXT const uint8_t* SISGetSNSPtr();
R_4_1_RECV_API_EXT const uint8_t* SISGetStationIDPtr();
R_4_1_RECV_API_EXT const uint8_t* SISGetStationLocLatPtr();
R_4_1_RECV_API_EXT const uint8_t* SISGetStationLocLongPtr();
#endif

#ifdef OPTION_DECODE_PSD
R_4_1_RECV_API_EXT const psd_decoder* PSDGetPtr();
#endif

#ifdef OPTION_DECODE_HD_ALERTS
R_4_1_RECV_API_EXT const fmhd_alert_string* HDAlertGetPtr();
#endif

#ifdef OPTION_DECODE_DLS
R_4_1_RECV_API_EXT const dab_dls_string* DLSGetPtr();
#endif
#endif	/*0*/

#endif
