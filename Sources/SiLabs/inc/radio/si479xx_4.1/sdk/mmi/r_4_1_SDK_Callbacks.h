/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef __SI46XX_SDK_CALLBACKS__
#define __SI46XX_SDK_CALLBACKS__

#ifdef _r_4_1_sdk_callbacks_h_global_		/* 210421 SGsoft */
#define R_4_1_SDK_CALLBACK_EXT
#else
#define R_4_1_SDK_CALLBACK_EXT	extern
#endif	/*_r_4_1_sdk_callbacks_h_global_*/

#if 0
#include "common_types.h"
#include "feature_types.h"
#include "platform_options.h"
#endif	/*0*/

#include <stdlib.h>

// Optionally can set these function pointers to execute own code
//  for seek callbacks.  This can be used to be able to stop a seek while it's ongoing.

// The implementations of these functions should not wait long for user input, instead
//  just check if there is input and return 'false -> 0' if there's nothing.

//return : non-zero (true) if should stop.
//			zero  if continue
R_4_1_SDK_CALLBACK_EXT bool (*ShouldStopFmSeek)();
R_4_1_SDK_CALLBACK_EXT bool (*ShouldStopAmSeek)();
R_4_1_SDK_CALLBACK_EXT bool (*ShouldStopDabSeek)();

//possible implementation of above that doesn't check for user input and just continues seeking.
R_4_1_SDK_CALLBACK_EXT bool NeverStopSeeking();

// This function is for changing the UI mode if necessary.
R_4_1_SDK_CALLBACK_EXT void (*UiUpdateMode)(RADIO_MODE_TYPE new_type);

// This default function prints a message, and sets 'cb_radio_mode'
R_4_1_SDK_CALLBACK_EXT void UiUpdateMode_default(RADIO_MODE_TYPE new_type);
R_4_1_SDK_CALLBACK_EXT RADIO_MODE_TYPE cb_radio_mode;
R_4_1_SDK_CALLBACK_EXT bool cb_radio_mode_updated;

typedef enum SDK_CALLBACKS_UPDATED_DATA_TYPE {
    //RDS
    SCB_UPDATED_RDS_PI,
    SCB_UPDATED_RDS_PTY,
    SCB_UPDATED_RDS_RT,
    SCB_UPDATED_RDS_PST,
    SCB_UPDATED_RDS_TIME,
    SCB_UPDATED_RDS_AF,
    //SIS
    SCB_UPDATED_SIS_SLOGAN,
    SCB_UPDATED_SIS_UNIVERSAL_SHORT_NAME,
    SCB_UPDATED_SIS_STATION_MESSAGE,
    SCB_UPDATED_SIS_STATION_NAME_LONG,
    SCB_UPDATED_SIS_STATION_NAME_SHORT,
    SCB_UPDATED_SIS_STATION_ID,
    SCB_UPDATED_SIS_LOC_LAT,
    SCB_UPDATED_SIS_LOC_LON,
    //PSD
    SCB_UPDATED_PSD_TITLE,
    SCB_UPDATED_PSD_ARTIST,
    SCB_UPDATED_PSD_ALBUM,
    SCB_UPDATED_PSD_GENRE,
    //HD Alerts
    SCB_UPDATED_HD_ALERT,
    //DLS
    SCB_UPDATED_DLS_STRING,
    SCB_CLEAR_DLS_COMMAND,
    //SERVICE LIST
    //SERVICE LIST-FMHD
    SCB_UPDATED_SERVICE_LIST_FAST,
    SCB_UPDATED_SERVICE_LIST_AUDIO,
    SCB_UPDATED_SERVICE_LIST_DATA,
    //SERVICE LIST-DAB
    SCB_DAB_SERVICE_LIST_REQUIRED_UPDATE,
    SCB_UPDATED_SERVICE_LIST_DAB,
    SCB_UPDATED_SERVICE_LINKING_DAB,
    //DAB NOTIFICATIONS
    SCB_SERVICE_LIST_BUFFER_FULL_ERROR,
    SCB_DAB_TUNE_SCAN_PROCESS_UPDATE,
    SCB_DAB_TUNE_REQUIRED_UPDATE,
    SCB_DAB_TUNE_SCAN_PROCESS_COMPLETE,
    //DAB-EVENTS
    SCB_EVENT_ENSEMBLE_RECONFIGURATION,
    SCB_EVENT_ENSEMBLE_RECONFIGURATION_WARNING,
    SCB_EVENT_CURRENT_SERVICE_NO_LONGER_AVAILABLE,
    SCB_EVENT_FINDING_ALTERNATE_SERVICE,
    // Service Following
    SCB_SERVICE_ACQ_DAB, //service acquired via DAB
    SCB_SERVICE_ACQ_FM, //service acquired via FM
    //Special
    SCB_EVENT_READY_FOR_FIRMWARE_UPDATE,
    SCB_DAB_FRONT_END_CAL_PROCESS_UPDATE,
    //FM/FMHD SEEK NOTIFICATIONS
    SCB_FM_SEEK_PROCESS_UPDATE,
    SCB_FM_SEEK_PROCESS_COMPLETE,

    //am seek notifications
    SCB_AM_SEEK_PROCESS_UPDATE,
    SCB_AM_SEEK_PROCESS_COMPLETE,
    //Undefined
    SCB_UPDATED_UNKNOWN
} SDK_CALLBACKS_UPDATED_DATA_TYPE;

// for SCB_DAB_TUNE_SCAN_PROCESS_UPDATE
typedef struct cb_dab_scan {
    bool continue_scan;
    bool bg;
    uint8_t freq_index;
} cb_dab_scan;

R_4_1_SDK_CALLBACK_EXT void CALLBACK_Updated_Data_ctx(SDK_CALLBACKS_UPDATED_DATA_TYPE updatedType, void* cb_data);

// macro for backwards compatibility with code.
#define CALLBACK_Updated_Data(x)            \
    do {                                    \
        CALLBACK_Updated_Data_ctx(x, NULL); \
    } while (0)

#endif
