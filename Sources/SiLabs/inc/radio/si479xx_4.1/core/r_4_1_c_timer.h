/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef C_TIMER_H_
#define C_TIMER_H_

#ifdef _r_4_1_c_timer_h_global_		/* 210426 SGsoft */
#define R_4_1_C_TIMER_EXT
#else
#define R_4_1_C_TIMER_EXT	extern
#endif	/*_r_4_1_dab_common_h_global_*/

///#include "feature_types.h"

// opaque handle for c_timer functions
typedef struct c_timer c_timer;

R_4_1_C_TIMER_EXT void ct_start(c_timer* t);

// Returns elapsed time since ct_start() in milliseconds.
// Max time interval: 49.7 days
// If timer has overflowed, this will be uint32_t_MAX, and c_timer.has_overflowed = true.
R_4_1_C_TIMER_EXT uint32_t ct_elapsed_ms(c_timer* t);

// Same as ct_elapsed_ms(), but return value in seconds.
// Does not have longer interval.
R_4_1_C_TIMER_EXT uint32_t ct_elapsed_s(c_timer* t);

// struct definition
struct c_timer {
    bool is_running;
    uint32_t ticks_start;
    bool has_rolled_over;
    bool has_overflowed;
};

#endif // C_TIMER_H_
