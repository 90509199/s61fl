/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef C_CORE_H_
#define C_CORE_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _r_4_1_c_core_h_global_		/* 210416 SGsoft */
#define R_4_1_C_CORE_EXT
#else
#define R_4_1_C_CORE_EXT	extern
#endif	/*_r_4_1_c_core_h_global_*/

///#include "core/debug_macro.h"
///#include "common_types.h"
///#include "feature_types.h"

#include <stdio.h>
#include <stdlib.h>

// Writes perror() message and aborts.
// Used for mandatory initialization steps.
R_4_1_C_CORE_EXT void pabort(const char* s);


R_4_1_C_CORE_EXT uint32_t power_uint(uint32_t base, uint32_t exp);
R_4_1_C_CORE_EXT uint32_t power_two(uint32_t exp);


// Converts a chip index (e.g. silk screen CHIP_0, CHIP_1, etc..) into the
// 'dc_config_chip_select' enum value.
R_4_1_C_CORE_EXT RETURN_CODE get_chip_select(uint8_t ic_num, dc_config_chip_select* chip_out);

// Converts a chip select bitmask (dc_config_chip_select) into an index (CHIP_0, CHIP_1, etc..)
R_4_1_C_CORE_EXT RETURN_CODE get_ic_num(dc_config_chip_select chip, uint8_t* ic_num_out);

// Writes 'buflen' bytes of 'buf' to file 'f' starting at position 'pos'
// Returns the #bytes written.
#if 0		//YOON_211129_v2
R_4_1_C_CORE_EXT size_t fwrite_pos(FILE* f, size_t pos, uint8_t* buf, size_t buflen);
// Writes 'buflen' bytes of 'buf' to file 'f' starting at position 'pos'
// Returns the #bytes read.

R_4_1_C_CORE_EXT size_t fread_pos(FILE* f, size_t pos, uint8_t* buf, size_t buflen);
#endif
// check endianness of platform
R_4_1_C_CORE_EXT bool is_bigendian();

// modifies original buffer!
R_4_1_C_CORE_EXT void reverse_buffer(uint32_t length, uint8_t* buf);

// Get uint32_t from a little endian buffer.
R_4_1_C_CORE_EXT uint32_t get_32bit_le(const uint8_t* buffer);

// Get uint32_t from a little endian buffer at the specified offset in the array.
R_4_1_C_CORE_EXT uint32_t get_32bit_le_offset(uint8_t* buffer, int offset);

// Get uint16_t from a little endian buffer.
R_4_1_C_CORE_EXT uint16_t get_16bit_le(const uint8_t* buffer);

// Get uint16_t from a little endian buffer at the specified offset in the array.
R_4_1_C_CORE_EXT uint16_t get_16bit_le_offset(uint8_t* buffer, int offset);

// Converts an array of 'byte_size' elements into the corresponding little endian buffer.
// Caller must allocate memory for the output buffer.
R_4_1_C_CORE_EXT void convert_array_to_le(uint8_t* orig, size_t orig_elem_count, uint8_t elem_size, uint8_t* le_out);

// Reverses buffer if bigendian platform.
R_4_1_C_CORE_EXT void to_little_endian(uint32_t length, uint8_t* buffer);

// Converts an integer type into a little endian buffer.
// Buffer must already be allocated.
#define TO_LE_BUF(val, outbuf) convert_array_to_le((uint8_t*)&val, 1, sizeof(val), outbuf)

R_4_1_C_CORE_EXT bool arr_contains(uint32_t len, const uint32_t* arr, uint32_t value);

// Returns 'num' rounded up to the next multiple of 'divisor'
// (if num is not already divisible by divisor)
R_4_1_C_CORE_EXT int32_t round_up(int32_t num, int32_t divisor);

/*  Provides some generic implementations of commonly used algorithms.  */

// function that takes two elements, and swaps their values.
typedef void (*fp_swap)(void* a, void* b);

// macro definition for fp_is_equal (only works for some default types!)
#define FN_SWAP_H(type) extern void c_swap__##type(void* a, void* b);

#define FN_SWAP(type)                     \
    void c_swap__##type(void* a, void* b) \
    {                                     \
        type* pa = a;                     \
        type* pb = b;                     \
        type tmp = *pa;                   \
        D_ASSERT(a != NULL);              \
        D_ASSERT(b != NULL);              \
        *pa = *pb;                        \
        *pb = tmp;                        \
    }

// add default types as needed
FN_SWAP_H(int32_t);
FN_SWAP_H(int16_t);
FN_SWAP_H(int8_t);
FN_SWAP_H(uint8_t);

// a common/example implementation of fp_swap
R_4_1_C_CORE_EXT void swap_ints(void* a, void* b);

// function that takes two elements (same type) and returns if they are equal or not.
typedef bool (*fp_is_equal)(void* a, void* b);

// a common/example implementation of fp_is_equal
R_4_1_C_CORE_EXT bool are_ints_equal(void* a, void* b);

// macro definition for fp_is_equal (only works for some default types!)
#define FN_IS_EQUAL_H(type) extern bool c_is_equal__##type(void* a, void* b);

#define FN_IS_EQUAL(type)                       \
    bool c_is_equal__##type(void* a, void* b) \
    {                                           \
        type* pa = a;                           \
        type* pb = b;                           \
        D_ASSERT(a != NULL);                    \
        D_ASSERT(b != NULL);                    \
        return (*pa) == (*pb);                  \
    }

// other default types as needed
FN_IS_EQUAL_H(int32_t);
FN_IS_EQUAL_H(int8_t);
FN_IS_EQUAL_H(uint8_t);

// function that takes two elements and returns:
// >0 : a > b
// 0  : elements are equal
// <0 : a < b
typedef int32_t (*fp_compare)(void* a, void* b);

// macro definition for fp_COMPARE (only works for some default types!)
#define FN_COMPARE_H(type) extern int32_t c_compare__##type(void* a, void* b);

#define FN_COMPARE(type)                        \
    int32_t c_compare__##type(void* a, void* b) \
    {                                           \
        type* pa = a;                           \
        type* pb = b;                           \
        D_ASSERT(a != NULL);                    \
        D_ASSERT(b != NULL);                    \
        return (*pa < *pb) ? -1 : (*pa > *pb);  \
    }

// other default types as needed
FN_COMPARE_H(int32_t);
FN_COMPARE_H(int16_t);
FN_COMPARE_H(int8_t);
FN_COMPARE_H(uint8_t);

R_4_1_C_CORE_EXT bool array_contains(uint8_t* arr, uint32_t elem_count, uint32_t elem_size, void* test, fp_compare fn_elems_equal);

R_4_1_C_CORE_EXT void remove_array_elem(uint8_t i_remove, uint8_t* list, uint32_t elem_count, uint8_t elem_size);

// Remove duplicate elements from an array.
// input:
//		list_start - pointer to first element in array
//		elem_count - initial number of elements in array (not byte size)
//		elem_sz - number of bytes in an array element.
//		fn_is_equal - function to check if two elements are equal.
//		fn_swap - function to swap two elements.
// output:
//		elem_count - gets updated to new array size (smaller if some were removed).
//	returns SUCCESS or INVALID_INPUT.
R_4_1_C_CORE_EXT RETURN_CODE remove_duplicates(uint8_t* list_start, uint32_t* elem_count, uint8_t elem_sz,
    fp_is_equal fn_is_equal, fp_swap fn_swap);

// Sorts an array in ascending order, determined by the comparison function.
// input:
//		list_start - pointer to first element in array
//		elem_count - initial number of elements in array (not byte size)
//		elem_sz - number of bytes in an array element.
//		fn_compare - function to compare elements.
//		fn_swap - function to swap two elements.
// output:
//		list_start - memory pointed to may be reordered (sorted)
//	returns SUCCESS or INVALID_INPUT.
R_4_1_C_CORE_EXT RETURN_CODE bubble_sort(uint8_t* list_start, uint32_t elem_count, uint8_t elem_sz,
    fp_compare fn_compare, fp_swap fn_swap);

// Sorts an array in ascending order, by the corresponding 'weight' array.
// The actual and weight array must have the same number of elements,
//		but their type does not have to be the same.
// After sorting, the scores and elements should still match up.
// input:
//		elem_count - initial number of elements in array (not byte size)
//		list_start - pointer to first element in array
//		elem_sz - number of bytes in an array element.
//		fn_swap_elem - function to swap two elements.
//
//		weights_start - pointer to first element in weight array
//		weight_sz - number of bytes in a weight array element.
//		fn_swap_weight - function to swap two weight elements.
//		fn_compare_weight - function that compares weights
// output:
//		list_start - memory may be reordered.
//		weights_start - memory may be reordered.
//	returns SUCCESS or INVALID_INPUT.
R_4_1_C_CORE_EXT RETURN_CODE bubble_sort_weighted(uint32_t elem_count,
    uint8_t* list_start, uint8_t elem_sz, fp_swap fn_swap_elem,
    uint8_t* weights_start, uint8_t weight_sz, fp_swap fn_swap_weight,
    fp_compare fn_compare_weight);

#ifdef __cplusplus
}
#endif

#endif // !C_CORE_H_
