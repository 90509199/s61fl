/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef DEBUG_MACRO_H_
#define DEBUG_MACRO_H_

#ifndef DEBUG_PRINT_ENABLED
#ifdef _DEBUG
#define DEBUG_PRINT_ENABLED 1
#else 
// currently always printing debug messages
#define DEBUG_PRINT_ENABLED 0/*1*/						/* 210416 SGsoft */
#endif // _DEBUG
#endif // DEBUG_PRINT_ENABLED


///#ifdef DEBUG_PRINT_ENABLED
#if DEBUG_PRINT_ENABLED									/* 210428 SGsoft */
#include <stdio.h>
#include <assert.h>

#define D_PRINTF(...) do { \
    D_PRINTF(stderr, "D_PRINTF :%s:%d:%s(): ",  __FILE__, __LINE__, __func__); \
    D_PRINTF(stderr, __VA_ARGS__); \
} while (0)


#define D_ASSERT(expr) do { \
    assert(expr); \
} while (0)

#define D_ASSERT_MSG(expr, ...) do { \
    if (!(expr)) { \
        D_PRINTF(__VA_ARGS__); \
    } \
    assert(expr); \
} while (0)

#else 
//
// EMPTY STUBS
// 
#define D_PRINTF(fmt, ...)
#define D_ASSERT(cond)
#define D_ASSERT_MSG(cond, fmt, ...)

#endif // DEBUG_PRINT_ENABLED

#define RETURN_ERR(code)     \
    if ((code) != SUCCESS) { \
        return code;         \
    }

#endif // DEBUG_MACRO_H_
