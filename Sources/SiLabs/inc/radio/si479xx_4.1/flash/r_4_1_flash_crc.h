/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#if !defined(crc32_h_)
#define crc32_h_
//**************************************************************************************************
//                                      crc32.h
//  Description:
//      Calculate CRC32.  Uses IEEE 802.3 polynomial of 0x04C11DB7.

#ifdef _r_4_1_flash_crc_h_global_		/* 210416 SGsoft */
#define R_4_1_FLASH_CRC_EXT
#else
#define R_4_1_FLASH_CRC_EXT	extern
#endif	/*_r_4_1_flash_crc_h_global_*/

///#include "common_types.h"



// Constants

#define CRC32_INITIAL_VALUE     (0xFFFFFFFF)

#define CRC32_CANONICAL_VALUE   (0xc704DD7B)

#define CRC32_POLY_VALUE        (0x04C11DB7)


// Functions

R_4_1_FLASH_CRC_EXT void crc32_update(uint32_t *p_crc32, const void* data, uint32_t len);
        // Does a CRC32 on a block of data.  The CRC32 state is stored in *p_crc32.
        // To initialize,  set *p_crc32 = CRC32_INITIAL_VALUE.

R_4_1_FLASH_CRC_EXT void crc32_table_gen(void);
        // 

R_4_1_FLASH_CRC_EXT uint32_t crc32_n(uint32_t crc32, unsigned n);
        // Calculate a CRC with n bits, based on CRC32 value.


// Functions, HOST only

R_4_1_FLASH_CRC_EXT void crc32_table_gen_stdout(void);
        // Generate crc32_table[256] and writes to stdout.

R_4_1_FLASH_CRC_EXT void crc32_update_test(void);

//**************************************************************************************************
#endif // !defined(crc32_h_)
