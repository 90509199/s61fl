/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/
#ifndef FLASH_FLASH_H_
#define FLASH_FLASH_H_

#ifdef _r_4_1_flash_util_h_global_		/* 210416 SGsoft */
#define R_4_1_FLASH_UTIL_EXT
#else
#define R_4_1_FLASH_UTIL_EXT	extern
#endif	/*_r_4_1_flash_util_h_global_*/

///#include "platform_types/common_types.h"
///#include "sdk/apis/si479xx_flash_utility_api.h"


struct flash_props {
    SI479XX_FLASH_UTIL_set_prop_propset__data* props;
    uint8_t prop_count;
};

struct flash_chip_cfg {
    // Sizes in bytes.
    uint32_t flash_size;
    uint32_t sector_size;
};


// Boots flash_utility firmware.
R_4_1_FLASH_UTIL_EXT RETURN_CODE boot_flash_utility(dc_config_chip_select tuner, dc_config_chip_type type, const struct flash_props* opt_props);

// Wrapper around write_image_bytes().
R_4_1_FLASH_UTIL_EXT RETURN_CODE write_image(dc_config_chip_select ic, const char* file, uint32_t addr, bool erase_first, const struct flash_chip_cfg* opt_cfg);
// (Optional) erases necessary flash segments + write new image bytes.
R_4_1_FLASH_UTIL_EXT RETURN_CODE write_image_bytes(dc_config_chip_select ic, const uint8_t* image, uint32_t image_size, uint32_t addr, bool erase_first, uint32_t sector_size);

// Wrapper around read_image_bytes();
// If opt_cfg != NULL, will read flash_sz bytes, in read_block_sz chunks.
R_4_1_FLASH_UTIL_EXT RETURN_CODE read_image(dc_config_chip_select ic, const char* file, uint32_t addr, uint32_t size);
// Reads flash bytes.
R_4_1_FLASH_UTIL_EXT RETURN_CODE read_image_bytes(dc_config_chip_select ic, uint8_t* image, uint32_t addr, uint32_t size);


#endif // FLASH_FLASH_H_

