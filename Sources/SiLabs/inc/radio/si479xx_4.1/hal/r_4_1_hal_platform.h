/******************************************************************************
* Silicon Laboratories Broadcast Radio Example Code
*
* EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
* THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
* TO THIS SOURCE FILE.
* IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
* PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* subject to the following additional restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
* 4. This software may only be used with Silicon Labs audio products.
******************************************************************************/

#ifndef HAL_PLATFORM_H_
#define HAL_PLATFORM_H_

#ifdef _r_4_1_hal_platform_h_global_		/* 210427 SGsoft */
#define R_4_1_HAL_PLAT_EXT
#else
#define R_4_1_HAL_PLAT_EXT	extern
#endif	/*_r_4_1_hal_platform_h_global_*/

///#include "common_types.h"
///#include "feature_types.h"

enum hal_hw_int_type {
    HAL_HW_INT_FALLING_EDGE,
    HAL_HW_INT_RISING_EDGE,
};

struct hal_hw_cfg {
    enum hal_hw_int_type edge_type; 
};

struct hal_stored_cfg {
    DAUGHTERCARD_TYPE connected_board;
    bool supports_hd_demod_blend;
};

//
// Platform specific functions that should be implemented
// according to the current PLATFORM_XXX
//

/// @brief Read some options from on-board memory. 
/// Can be first function called.
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_readConfig(struct hal_stored_cfg* cfg);

// Arguments are optional, for configuring the interrupt configuration of the chips.
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_initHardware(uint8_t len, const struct hal_hw_cfg* chip_cfgs);
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_resetIC(uint8_t icNum);
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_powerDownHardware(void);

R_4_1_HAL_PLAT_EXT RETURN_CODE hal_writeCommand(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_readReply(dc_config_chip_select icNum, uint16_t length, uint8_t* buffer);

// Returns TIMEOUT if interrupt line is not raised within ~ timeout_ms
// Returns SUCCESS if an interrupt was caught.
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_waitForInterrupt(dc_config_chip_select icNum, uint32_t timeout_ms);

// Not currently implemented for EVB / daughtercards.
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_audioEnable(void);
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_audioDisable(void);
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_writePersistentStorage(uint16_t start_addr, uint16_t length, uint8_t* buffer);
R_4_1_HAL_PLAT_EXT RETURN_CODE hal_readPersistentStorage(uint16_t start_addr, uint16_t length, uint8_t* buffer);
R_4_1_HAL_PLAT_EXT void hal_erasePersistentStorage();

#endif // HAL_PLATFORM_H_
