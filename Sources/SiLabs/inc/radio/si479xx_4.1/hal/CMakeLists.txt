add_library(hal "")

target_sources(hal
    PRIVATE 
    hal_platform.c
    PUBLIC
    hal_platform.h
)

target_link_libraries(hal PUBLIC platform_types)
target_include_directories(hal PUBLIC ${CMAKE_CURRENT_LIST_DIR})

# Conditionally build platform specific code.
if (RM_HW_PLATFORM STREQUAL "PLATFORM_PI") 
    add_subdirectory(pi)
elseif (RM_HW_PLATFORM STREQUAL "PLATFORM_EVB")
    add_subdirectory(si_evb)
else()
    message(FATAL_ERROR "Cache variable HW_PLATFORM:${RM_HW_PLATFORM} was not one of the accepted platforms ${platforms}")
endif()

