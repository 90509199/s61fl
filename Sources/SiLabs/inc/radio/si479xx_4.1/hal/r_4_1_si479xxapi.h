/***************************************************************************************
                  Silicon Laboratories Broadcast Automotive Tuner + Demod SDK

   EVALUATION AND USE OF THIS SOFTWARE IS SUBJECT TO THE TERMS AND CONDITIONS OF
     THE SOFTWARE LICENSE AGREEMENT IN THE DOCUMENTATION FILE CORRESPONDING
     TO THIS SOURCE FILE.
   IF YOU DO NOT AGREE TO THE LIMITED LICENSE AND CONDITIONS OF SUCH AGREEMENT,
     PLEASE RETURN ALL SOURCE FILES TO SILICON LABORATORIES.

   Generic header for main drivers necessary to be implemented for host mcu to control si468x
   FILE: HAL.h
   Supported IC : Si479xx + Si461x
   Date: September 21, 2015
  (C) Copyright 2015, Silicon Laboratories, Inc. All rights reserved.
****************************************************************************************/

#ifndef __SI479XX_API__
#define __SI479XX_API__

#ifdef _r_4_1_si479xx_api_h_global_
#define R_4_1_SI479XX_API_EXT
#else
#define R_4_1_SI479XX_API_EXT	extern
#endif	/*_r_4_1_si479xx_api_h_global_*/

typedef enum return_codes_enum {			/* 210427 SGsoft */
    success = 0x00,
    mcu_error = 0x01,
    invalid_input = 0x02,
    invalid_mode = 0x04,
    timeout = 0x08,
    unknown_device = 0x10,
    command_error = 0x20,
    bbm_error = 0x40,
    unsupported_function = 0x80,
    activation_required = 0x0100,
    usb_speed_too_low = 0x0200,
    firmware_image_invalid = 0x0400,
    dll_memory_error = 0x0800,
    bad_dll = 0x1000,
} return_codes;
//typedef int return_codes;

typedef enum SI479xx_reset_enum {
    line_low = 0,
    line_high = 1,
    individual_reset = 2,
    group_reset = 3,
    do_not_reset = 0xFF
} SI479xx_reset_mode;
//typedef UINT8 SI479xx_reset_mode;


R_4_1_SI479XX_API_EXT return_codes SI479xx_readReply (dc_config_chip_select icNum, UINT16 length, UINT8 *buffer);					
R_4_1_SI479XX_API_EXT return_codes SI479xx_writeCommand (dc_config_chip_select icNum, UINT16 length, UINT8 *buffer);				
R_4_1_SI479XX_API_EXT RETURN_CODE Si479xx_wait_for_cts_timeout(dc_config_chip_select icNum);							

#endif	/*__SI479XX_API__*/
