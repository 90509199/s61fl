#ifndef _SG_DEFINES_H_
#define _SG_DEFINES_H_

#include <stdint.h>

#define BIT0	((U32)1<<0)
#define BIT1	((U32)1<<1)
#define BIT2	((U32)1<<2)
#define BIT3	((U32)1<<3)
#define BIT4	((U32)1<<4)
#define BIT5	((U32)1<<5)
#define BIT6	((U32)1<<6)
#define BIT7	((U32)1<<7)
#define BIT8	((U32)1<<8)
#define BIT9	((U32)1<<9)
#define BIT10	((U32)1<<10)
#define BIT11	((U32)1<<11)
#define BIT12	((U32)1<<12)
#define BIT13	((U32)1<<13)
#define BIT14	((U32)1<<14)
#define BIT15	((U32)1<<15)
#define BIT16	((U32)1<<16)
#define BIT17	((U32)1<<17)
#define BIT18	((U32)1<<18)
#define BIT19	((U32)1<<19)
#define BIT20	((U32)1<<20)
#define BIT21	((U32)1<<21)
#define BIT22	((U32)1<<22)
#define BIT23	((U32)1<<23)
#define BIT24	((U32)1<<24)
#define BIT25	((U32)1<<25)
#define BIT26	((U32)1<<26)
#define BIT27	((U32)1<<27)
#define BIT28	((U32)1<<28)
#define BIT29	((U32)1<<29)
#define BIT30	((U32)1<<30)
#define BIT31	((U32)1<<31)

typedef int32_t  s32;		
typedef int16_t s16;
typedef int8_t  s8;

typedef int32_t  S32;									
typedef int16_t S16;
typedef int8_t  S8;

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef uint32_t  U32;
typedef uint16_t U16;
typedef uint8_t  U8;	

typedef uint32_t UINT32;
typedef uint16_t UINT16;
typedef uint8_t  UINT8;	

typedef union {
	U8	BYTE;
	struct {
		U8	bit0	:1;
	    U8	bit1	:1;
		U8	bit2	:1;
	   	U8	bit3	:1;
	    U8	bit4	:1;
	    U8	bit5	:1;
	    U8	bit6	:1;
	    U8	bit7	:1;
	}BITS;
} FLAG8;        					
						
#define	NULL 						0

/***/

#define	FEAT_TUN_DIGEN_SEQ_4_1			1	

#if (FEAT_TUN_DIGEN_SEQ_4_1)
#define ENABLE_TUNER_SI479XX									
#if defined(ENABLE_TUNER_SI479XX)
#define SI479XX_FIRM_REL_4_1					
#endif	/*ENABLE_TUNER_SI479XX*/

#define	FEAT_TUN_DIGEN_SEQ				0
#define	FEAT_TUN_AREA_CHINA				1	
#define	FEAT_CUSTOM_TUNING			   	0					
#define	FEAT_FLASH_UTIL					1/*0*/

#define	FEAT_DIGEN_4_1_TUNING			1						/* 210803 SGsoft */

#define	FEAT_DEBUG_ARBERR				0/*1*/					/* 211125 SGsoft */

#define	FEAT_DEBUG_211125				0/*1*/						/* 211125 SGsoft *//* 211201 SGsoft */	

#define FEAT_DIGEN_4_1_TUNING_211201	0/*1*/					/* 211201 SGsoft *//* 211216 SGsoft */

#define FEAT_DIGEN_4_1_TUNING_211216	1						/* 211216 SGsoft */	

#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/



/*---------------------FreeRTOS---------------------------------------------*/
/* The maximum number of message that can be waiting for display at any one time. */
#define mainLCD_QUEUE_SIZE			(3)

#endif	/*_SG_DEFINES_H_*/
