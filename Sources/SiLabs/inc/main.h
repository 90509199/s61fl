/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

#ifdef _main_h_global_
#define MAIN_EXT
#else
#define MAIN_EXT extern
#endif	/*_main_h_global_*/


/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

#include "tunmain.h"									
#include "tuner_api_common.h"
#if defined(ENABLE_TUNER_SI479XX)
#if defined(SI479XX_FIRM_REL_4_1)		
#include "r_4_1_misc.h"														

#include "r_4_1_common_types.h"

#include "r_4_1_platform_selector.h"
#include "r_4_1_platform_options.h"
#include "r_4_1_feature_types.h"

#include "r_4_1_siErrLog.h"	
#include "r_4_1_config_types.h"

#include "r_4_1_si479xxapi.h"																
#include "r_4_1_hal_platform.h"

#include "r_4_1_debug_macro.h"
#include "r_4_1_c_core.h"
#include "r_4_1_c_util.h"
#include "r_4_1_flash_crc.h"
#include "r_4_1_c_timer.h"													

#include "r_4_1_audio_cfg.h"													
#include "r_4_1_daughtercard_cfg.h"
#include "r_4_1_System_Configuration.h"

#include "r_4_1_SDK_Callbacks.h"											
#include "r_4_1_sdk_macro.h"
#include "r_4_1_boot_types.h"																			

#include "r_4_1_Firmware_Load_Helpers.h"
#include "r_4_1_si479xx_initial_startup_sample.h"
#include "r_4_1_default_args.h"
#include "r_4_1_all_apis.h"

#include "r_4_1_si4796x_common.h"
#include "r_4_1_si4796x_firmware_api_constants.h"
#include "r_4_1_si4796x_firmware_api.h"

#include "r_4_1_si4796x_tuner_api_constants.h"
#include "r_4_1_si4796x_tuner_api.h"

#include "r_4_1_si479xx_common.h"
#include "r_4_1_si479xx_detail_status_api_constants.h"
#include "r_4_1_si479xx_detail_status_api.h"
#include "r_4_1_si479xx_firmware_api_constants.h"
#include "r_4_1_si479xx_firmware_api.h"
#include "r_4_1_si479xx_flash_api_constants.h"
#include "r_4_1_si479xx_flash_api.h"
#include "r_4_1_si479xx_flash_utility_api_constants.h"
#include "r_4_1_si479xx_flash_utility_api.h"
#include "r_4_1_si479xx_hub_api_constants.h"
#include "r_4_1_si479xx_hub_api.h"
#include "r_4_1_si479xx_tuner_api_constants.h"
#include "r_4_1_si479xx_tuner_api.h"

#include "r_4_1_flash_util.h"

#include "r_4_1_cmd_logger_cwrap.h"												

#include "r_4_1_common_tuner.h"												
#include "r_4_1_Firmware_API_Common.h"
#include "r_4_1_global_data.h"

#include "r_4_1_Receiver_API.h"
#include "r_4_1_Implementation__common.h"
#include "r_4_1_IFirmware_API_Manager.h"

#include "r_4_1_DAB_1T.h"																		
#include "r_4_1_dc_all.h"

#include "r_4_1_HAL.h"
#endif	/*SI479XX_FIRM_REL_4_1*/
#endif	/*ENABLE_TUNER_SI479XX*/

#if (FEAT_TUN_DIGEN_SEQ_4_1)		
#include "sg_tune_seq_4_1.h"
#endif	/*FEAT_TUN_DIGEN_SEQ_4_1*/	

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#if 0
#define TUN_RST_Pin 				LL_GPIO_PIN_3
#define TUN_RST_GPIO_Port 			GPIOA
#define TUN_SSB_Pin 				LL_GPIO_PIN_4
#define TUN_SSB_GPIO_Port 			GPIOA
#else
#define TUN_RST_Pin 				//LL_GPIO_PIN_3
#define TUN_RST_GPIO_Port 		//	GPIOA
#define TUN_SSB_Pin 				//LL_GPIO_PIN_4
#define TUN_SSB_GPIO_Port 		//	GPIOA
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
