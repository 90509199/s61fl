/*
 * service_proc.h
 *
 *  Created on: 2018. 12. 21.
 *      Author: Digen
 */

#ifndef SERVICE_PROC_H_
#define SERVICE_PROC_H_


typedef enum  {
	BOOT_SOC_BOOT_PS_HOLD=0,
	BOOT_SOC_BOOT_MONITOR,
	BOOT_SOC_ALIVE_MONITOR,
}SOC_MONITOR;

extern QUE_t rrm_tx_cmd_q;

void rrm_tx_cmd_Init(void);
uint16_t check_rrm_tx_cmd(void);

void Serivce_Proc(DEVICE *dev);
void Serivce_1ms_Func(DEVICE *dev);
void Serivce_2ms_Func(DEVICE *dev);
void Serivce_5ms_Func(DEVICE *dev);
void Serivce_10ms_Func(DEVICE *dev);
void Serivce_100ms_Func(DEVICE *dev);
void Serivce_1000ms_Func(DEVICE *dev);
void RESET_MCU(DEVICE *dev);

#ifdef SVCD_ENABLE
SVCD_STEP_TYPE Check_SVCD_Func(uint8 speed, uint8 direct);
#ifdef DSP_DOUBLE_QUE
void Excute_SVCD_Func(DEVICE *dev);
#else
void Excute_SVCD_Func(uint8 step);
#endif
#endif


#endif /* SERVICE_PROC_H_ */
