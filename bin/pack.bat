@echo off&SetLocal EnableDelayedExpansion

set project_name=S51HE
set build_name=SH52EV
set build_type=Debug

for /f "tokens=2 delims=;=" %%a in ('findstr "Version_1=" ..\Sources\uart_comm.c') do (set version1=%%a)
for /f "tokens=2 delims=;=" %%a in ('findstr "Version_2=" ..\Sources\uart_comm.c') do (set version2=%%a)
for /f "tokens=2 delims=;=" %%a in ('findstr "Version_3=" ..\Sources\uart_comm.c') do (set version3=%%a)

set "version_sw=000%version1%"&set "version_sw=!version_sw:~-3!"
set version_fixed=%version2%.%version3%
set release_date=%date:~0,4%%date:~5,2%%date:~8,2%
set release_file=IVI_Chery_%project_name%_FS32K144_%release_date%_V%version_sw%.%version_fixed%
set ota_file=all_update_%project_name%_CAR01_OTA01_V%version_sw%.%version_fixed%_HW0.0.1_%release_date%
set src_file=..\%build_type%\%build_name%
set dst_file=..\bin\%release_file%

echo copy %src_file%.* to %dst_file%.*
copy /Y %src_file%.elf %dst_file%.elf
copy /Y %src_file%.map %dst_file%.map
copy /Y %src_file%.bin %dst_file%.bin

echo pack %release_file%.bin into %ota_file%.zip
tar -a -c -f %ota_file%.zip %release_file%.bin

echo hash
calc_hash %ota_file%.zip

echo pack %release_file%.elf into %release_file%.zip
tar -a -c -f %release_file%.zip %release_file%.elf

echo prepare flash batch file
set FLASH_BATFILE=RunToFlash.bat
echo @ECHO OFF> %FLASH_BATFILE%
echo set path=%%path%%;D:\Tool\SEGGER\JLink;"C:\Program Files (x86)\SEGGER\JLink">> %FLASH_BATFILE%
echo ECHO Flash %release_file%.bin>> %FLASH_BATFILE%
echo JFlash.exe -openprj%%cd%%\SH52.jflash -open%release_file%.bin,0 -erasechip -programverify -startapp -exit -jflashlogSH52_Flash.log -jlinklogSH52_Link.log>> %FLASH_BATFILE%
echo IF ERRORLEVEL 1 goto ERROR>> %FLASH_BATFILE%
echo ECHO Successful>> %FLASH_BATFILE%
echo goto END>> %FLASH_BATFILE%
echo :ERROR>> %FLASH_BATFILE%
echo ECHO J-Flash Error>> %FLASH_BATFILE%
echo :END>> %FLASH_BATFILE%
echo pause>> %FLASH_BATFILE%

tar -a -c -f J-Flash.zip SH52.jflash RunToFlash.bat %release_file%.bin

tar -a -c -f V%version_sw%.%version_fixed%.zip %release_file%.zip %ota_file%.zip %ota_file%.md5 %ota_file%.sha256 J-Flash.zip release_note.html

echo archive output
tar -a -c -f %build_name%.zip %release_file%.elf %release_file%.map %release_file%.bin

set folder=V%version_sw%.%version_fixed%
mkdir %folder%
move /Y %release_file%.* %folder%
move /Y %ota_file%.* %folder%
move /Y J-Flash.zip %folder%
move /Y release_note.html %folder%

pause