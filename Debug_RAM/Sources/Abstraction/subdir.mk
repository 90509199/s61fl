################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/Abstraction/abs_can.c \
../Sources/Abstraction/abs_timer.c 

OBJS += \
./Sources/Abstraction/abs_can.o \
./Sources/Abstraction/abs_timer.o 

C_DEPS += \
./Sources/Abstraction/abs_can.d \
./Sources/Abstraction/abs_timer.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/Abstraction/%.o: ../Sources/Abstraction/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/Abstraction/abs_can.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


