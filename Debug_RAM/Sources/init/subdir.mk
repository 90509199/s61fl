################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/init/adc_init.c \
../Sources/init/can_init.c \
../Sources/init/flexio_i2c_init.c \
../Sources/init/i2c_init.c \
../Sources/init/nvic_init.c \
../Sources/init/pwm_init.c \
../Sources/init/siu_init.c \
../Sources/init/spi_init.c \
../Sources/init/sys_init.c \
../Sources/init/sysclk_init.c \
../Sources/init/uart_init.c 

OBJS += \
./Sources/init/adc_init.o \
./Sources/init/can_init.o \
./Sources/init/flexio_i2c_init.o \
./Sources/init/i2c_init.o \
./Sources/init/nvic_init.o \
./Sources/init/pwm_init.o \
./Sources/init/siu_init.o \
./Sources/init/spi_init.o \
./Sources/init/sys_init.o \
./Sources/init/sysclk_init.o \
./Sources/init/uart_init.o 

C_DEPS += \
./Sources/init/adc_init.d \
./Sources/init/can_init.d \
./Sources/init/flexio_i2c_init.d \
./Sources/init/i2c_init.d \
./Sources/init/nvic_init.d \
./Sources/init/pwm_init.d \
./Sources/init/siu_init.d \
./Sources/init/spi_init.d \
./Sources/init/sys_init.d \
./Sources/init/sysclk_init.d \
./Sources/init/uart_init.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/init/%.o: ../Sources/init/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/init/adc_init.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


