################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/Diag/diag.c \
../Sources/Diag/diag_dbi.c \
../Sources/Diag/diag_dbi_if.c \
../Sources/Diag/dtc_mgr.c \
../Sources/Diag/mnt_flt.c 

OBJS += \
./Sources/Diag/diag.o \
./Sources/Diag/diag_dbi.o \
./Sources/Diag/diag_dbi_if.o \
./Sources/Diag/dtc_mgr.o \
./Sources/Diag/mnt_flt.o 

C_DEPS += \
./Sources/Diag/diag.d \
./Sources/Diag/diag_dbi.d \
./Sources/Diag/diag_dbi_if.d \
./Sources/Diag/dtc_mgr.d \
./Sources/Diag/mnt_flt.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/Diag/%.o: ../Sources/Diag/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/Diag/diag.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


