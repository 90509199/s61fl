################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/func/gpio_func.c \
../Sources/func/power.c \
../Sources/func/timer_func.c \
../Sources/func/uart_func.c 

OBJS += \
./Sources/func/gpio_func.o \
./Sources/func/power.o \
./Sources/func/timer_func.o \
./Sources/func/uart_func.o 

C_DEPS += \
./Sources/func/gpio_func.d \
./Sources/func/power.d \
./Sources/func/timer_func.d \
./Sources/func/uart_func.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/func/%.o: ../Sources/func/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/func/gpio_func.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


