################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_Firmware_API_Common.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_common_tuner.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_global_data.c 

OBJS += \
./Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_Firmware_API_Common.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_common_tuner.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_global_data.o 

C_DEPS += \
./Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_Firmware_API_Common.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_common_tuner.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_global_data.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/SiLabs/src/si479xx_4.1/sdk/common/%.o: ../Sources/SiLabs/src/si479xx_4.1/sdk/common/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/SiLabs/src/si479xx_4.1/sdk/common/r_4_1_Firmware_API_Common.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


