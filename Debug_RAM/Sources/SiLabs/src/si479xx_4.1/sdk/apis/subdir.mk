################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_all_apis.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_common.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_firmware_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_tuner_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_common.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_detail_status_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_firmware_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_flash_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_flash_utility_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_hub_api.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_tuner_api.c 

OBJS += \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_all_apis.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_common.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_firmware_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_tuner_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_common.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_detail_status_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_firmware_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_flash_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_flash_utility_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_hub_api.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_tuner_api.o 

C_DEPS += \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_all_apis.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_common.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_firmware_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si4796x_tuner_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_common.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_detail_status_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_firmware_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_flash_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_flash_utility_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_hub_api.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_si479xx_tuner_api.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/SiLabs/src/si479xx_4.1/sdk/apis/%.o: ../Sources/SiLabs/src/si479xx_4.1/sdk/apis/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/SiLabs/src/si479xx_4.1/sdk/apis/r_4_1_all_apis.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


