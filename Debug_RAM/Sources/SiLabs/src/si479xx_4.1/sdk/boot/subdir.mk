################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_Firmware_Load_Helpers.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_default_args.c \
../Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_si479xx_initial_startup_sample.c 

OBJS += \
./Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_Firmware_Load_Helpers.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_default_args.o \
./Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_si479xx_initial_startup_sample.o 

C_DEPS += \
./Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_Firmware_Load_Helpers.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_default_args.d \
./Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_si479xx_initial_startup_sample.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/SiLabs/src/si479xx_4.1/sdk/boot/%.o: ../Sources/SiLabs/src/si479xx_4.1/sdk/boot/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/SiLabs/src/si479xx_4.1/sdk/boot/r_4_1_Firmware_Load_Helpers.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


