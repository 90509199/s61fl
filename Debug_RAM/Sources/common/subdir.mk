################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/common/debug.c \
../Sources/common/isr.c \
../Sources/common/que.c \
../Sources/common/shelldrv.c \
../Sources/common/tick.c \
../Sources/common/timerdrv.c 

OBJS += \
./Sources/common/debug.o \
./Sources/common/isr.o \
./Sources/common/que.o \
./Sources/common/shelldrv.o \
./Sources/common/tick.o \
./Sources/common/timerdrv.o 

C_DEPS += \
./Sources/common/debug.d \
./Sources/common/isr.d \
./Sources/common/que.d \
./Sources/common/shelldrv.d \
./Sources/common/tick.d \
./Sources/common/timerdrv.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/common/%.o: ../Sources/common/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/common/debug.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


