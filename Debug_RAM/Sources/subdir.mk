################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/A2B_I2C_Commandlist.c \
../Sources/AMP_TB2952.c \
../Sources/Self_Flash_drv.c \
../Sources/TEF6686.c \
../Sources/ak7739_proc.c \
../Sources/beep_proc.c \
../Sources/can_comm.c \
../Sources/change_mode.c \
../Sources/common.c \
../Sources/crc.c \
../Sources/cust.c \
../Sources/diag_proc.c \
../Sources/encoder.c \
../Sources/flash_proc.c \
../Sources/key_app.c \
../Sources/key_drv.c \
../Sources/lpuart_hw_access.c \
../Sources/main.c \
../Sources/service_proc.c \
../Sources/shellcmd.c \
../Sources/system_proc.c \
../Sources/test_mode.c \
../Sources/uart_comm.c 

OBJS += \
./Sources/A2B_I2C_Commandlist.o \
./Sources/AMP_TB2952.o \
./Sources/Self_Flash_drv.o \
./Sources/TEF6686.o \
./Sources/ak7739_proc.o \
./Sources/beep_proc.o \
./Sources/can_comm.o \
./Sources/change_mode.o \
./Sources/common.o \
./Sources/crc.o \
./Sources/cust.o \
./Sources/diag_proc.o \
./Sources/encoder.o \
./Sources/flash_proc.o \
./Sources/key_app.o \
./Sources/key_drv.o \
./Sources/lpuart_hw_access.o \
./Sources/main.o \
./Sources/service_proc.o \
./Sources/shellcmd.o \
./Sources/system_proc.o \
./Sources/test_mode.o \
./Sources/uart_comm.o 

C_DEPS += \
./Sources/A2B_I2C_Commandlist.d \
./Sources/AMP_TB2952.d \
./Sources/Self_Flash_drv.d \
./Sources/TEF6686.d \
./Sources/ak7739_proc.d \
./Sources/beep_proc.d \
./Sources/can_comm.d \
./Sources/change_mode.d \
./Sources/common.d \
./Sources/crc.d \
./Sources/cust.d \
./Sources/diag_proc.d \
./Sources/encoder.d \
./Sources/flash_proc.d \
./Sources/key_app.d \
./Sources/key_drv.d \
./Sources/lpuart_hw_access.d \
./Sources/main.d \
./Sources/service_proc.d \
./Sources/shellcmd.d \
./Sources/system_proc.d \
./Sources/test_mode.d \
./Sources/uart_comm.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/%.o: ../Sources/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/A2B_I2C_Commandlist.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


