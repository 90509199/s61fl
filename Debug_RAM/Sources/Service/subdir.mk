################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/Service/srv_mcan.c \
../Sources/Service/std_timer.c \
../Sources/Service/std_util.c 

OBJS += \
./Sources/Service/srv_mcan.o \
./Sources/Service/std_timer.o \
./Sources/Service/std_util.o 

C_DEPS += \
./Sources/Service/srv_mcan.d \
./Sources/Service/std_timer.d \
./Sources/Service/std_util.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/Service/%.o: ../Sources/Service/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/Service/srv_mcan.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


