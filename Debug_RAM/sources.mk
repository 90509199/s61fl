################################################################################
# Automatically-generated file. Do not edit!
################################################################################

TODISASSEMBLE_SRCS := 
ASM_UPPER_SRCS := 
ASM_SRCS := 
CPP_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
ELF_SRCS := 
LD_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
S_SRCS := 
CC_SRCS := 
TOPREPROCESS_SRCS := 
C_SRCS := 
CC_DEPS := 
C++_DEPS := 
EXECUTABLES := 
OBJS := 
C_UPPER_DEPS := 
CXX_DEPS := 
SECONDARY_FLASH := 
SECONDARY_SIZE := 
C_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Generated_Code \
Project_Settings/Startup_Code \
SDK/platform/devices/S32K144/startup \
SDK/platform/devices \
SDK/platform/drivers/src/clock/S32K1xx \
SDK/platform/drivers/src/edma \
SDK/platform/drivers/src/flash \
SDK/platform/drivers/src/flexcan \
SDK/platform/drivers/src/flexio \
SDK/platform/drivers/src/interrupt \
SDK/platform/drivers/src/lpi2c \
SDK/platform/drivers/src/lpspi \
SDK/platform/drivers/src/pins \
SDK/platform/drivers/src/wdog \
SDK/rtos/osif \
Sources \
Sources/Abstraction \
Sources/CanIL \
Sources/CanTp \
Sources/Diag \
Sources/Service \
Sources/SiLabs/src \
Sources/SiLabs/src/si479xx_4.1/core \
Sources/SiLabs/src/si479xx_4.1/flash \
Sources/SiLabs/src/si479xx_4.1/hal \
Sources/SiLabs/src/si479xx_4.1/osal \
Sources/SiLabs/src/si479xx_4.1/sdk/apis \
Sources/SiLabs/src/si479xx_4.1/sdk/boot \
Sources/SiLabs/src/si479xx_4.1/sdk/common \
Sources/SiLabs/src/si479xx_4.1/sdk/mmi \
Sources/SiLabs/src/si479xx_4.1/sdk/platform \
Sources/SiLabs/src/si479xx_4.1/sdk/receiver/dc_specific \
Sources/SiLabs/src/si479xx_4.1/sdk/receiver \
Sources/SiLabs/src/si479xx_4.1/sdk/sdk_hal \
Sources/SiLabs/src/si479xx_4.1/sdk/util \
Sources/common \
Sources/dsp \
Sources/func \
Sources/init \
Sources/vector/Can \
Sources/vector/CanNm \
Sources/vector/Ccl \
Sources/vector/Geny \
Sources/vector/Il \
Sources/vector/Nm \
Sources/vector/SipVersionCheck \
Sources/vector/VStdLib \
Sources/vector \

